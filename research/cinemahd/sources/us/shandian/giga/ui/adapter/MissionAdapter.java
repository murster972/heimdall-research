package us.shandian.giga.ui.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.database.entitys.MovieEntity;
import com.database.entitys.TvWatchedEpisode;
import com.movie.data.model.MovieInfo;
import com.movie.ui.activity.SourceActivity;
import com.movie.ui.helper.MoviesHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.IntentDataContainer;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import us.shandian.giga.get.DownloadManager;
import us.shandian.giga.get.DownloadMission;
import us.shandian.giga.service.DownloadManagerService;
import us.shandian.giga.ui.common.ProgressDrawable;
import us.shandian.giga.util.Utility;

public class MissionAdapter extends RecyclerView.Adapter<ViewHolder> {
    /* access modifiers changed from: private */
    public static final Map<Integer, String> h = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Activity f7088a;
    private LayoutInflater b;
    /* access modifiers changed from: private */
    public DownloadManager c;
    /* access modifiers changed from: private */
    public DownloadManagerService.DMBinder d;
    private int e;
    MoviesHelper f;
    CompositeDisposable g = null;

    private static class ChecksumTask extends AsyncTask<String, Void, String> {

        /* renamed from: a  reason: collision with root package name */
        ProgressDialog f7092a;
        WeakReference<Activity> b;

        ChecksumTask(Activity activity) {
            this.b = new WeakReference<>(activity);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String doInBackground(String... strArr) {
            return Utility.a(strArr[0], strArr[1]);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            Activity a2 = a();
            if (a2 != null) {
                this.f7092a = new ProgressDialog(a2);
                this.f7092a.setCancelable(false);
                this.f7092a.setMessage(a2.getString(R.string.msg_wait));
                this.f7092a.show();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(String str) {
            super.onPostExecute(str);
            ProgressDialog progressDialog = this.f7092a;
            if (progressDialog != null) {
                Utility.a(progressDialog.getContext(), str);
                if (a() != null) {
                    this.f7092a.dismiss();
                }
            }
        }

        private Activity a() {
            Activity activity = (Activity) this.b.get();
            if (activity == null || !activity.isFinishing()) {
                return activity;
            }
            return null;
        }
    }

    static class MissionObserver implements DownloadMission.MissionListener {
        private MissionAdapter b;
        private ViewHolder c;

        public MissionObserver(MissionAdapter missionAdapter, ViewHolder viewHolder) {
            this.b = missionAdapter;
            this.c = viewHolder;
        }

        public void a(DownloadMission downloadMission, long j, long j2) {
            this.b.d(this.c);
        }

        public void a(DownloadMission downloadMission) {
            ViewHolder viewHolder = this.c;
            DownloadMission downloadMission2 = viewHolder.f7093a;
            if (downloadMission2 != null) {
                viewHolder.f.setText(Utility.a(downloadMission2.length));
                this.b.a(this.c, true);
            }
        }

        public void a(DownloadMission downloadMission, int i) {
            this.b.d(this.c);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        public DownloadMission f7093a;
        public int b;
        public TextView c;
        public ImageView d;
        public TextView e;
        public TextView f;
        public View g;
        public ImageView h;
        public ProgressDrawable i;
        public MissionObserver j;
        public long k = -1;
        public long l = -1;
        public int m;
        public String n;

        public ViewHolder(View view) {
            super(view);
            this.c = (TextView) view.findViewById(R.id.item_status);
            this.d = (ImageView) view.findViewById(R.id.item_icon);
            this.e = (TextView) view.findViewById(R.id.item_name);
            this.f = (TextView) view.findViewById(R.id.item_size);
            this.g = view.findViewById(R.id.item_bkg);
            this.h = (ImageView) view.findViewById(R.id.item_more);
        }
    }

    static {
        h.put(Integer.valueOf(R.id.md5), "MD5");
        h.put(Integer.valueOf(R.id.sha1), "SHA1");
    }

    public MissionAdapter(Activity activity, DownloadManagerService.DMBinder dMBinder, DownloadManager downloadManager, boolean z, MoviesHelper moviesHelper) {
        this.f7088a = activity;
        this.c = downloadManager;
        this.d = dMBinder;
        this.b = (LayoutInflater) this.f7088a.getSystemService("layout_inflater");
        this.e = z ? R.layout.mission_item_linear : R.layout.mission_item;
        this.g = new CompositeDisposable();
        this.f = moviesHelper;
    }

    static /* synthetic */ void a(String str) throws Exception {
    }

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    static /* synthetic */ void b(String str) throws Exception {
    }

    static /* synthetic */ void b(Throwable th) throws Exception {
    }

    /* access modifiers changed from: private */
    public void d(ViewHolder viewHolder) {
        a(viewHolder, false);
    }

    public int getItemCount() {
        return this.c.getCount();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* access modifiers changed from: private */
    public void c(final ViewHolder viewHolder) {
        PopupMenu popupMenu = new PopupMenu(this.f7088a, viewHolder.h);
        popupMenu.inflate(R.menu.mission);
        Menu menu = popupMenu.getMenu();
        MenuItem findItem = menu.findItem(R.id.start);
        MenuItem findItem2 = menu.findItem(R.id.pause);
        MenuItem findItem3 = menu.findItem(R.id.view);
        MenuItem findItem4 = menu.findItem(R.id.delete);
        MenuItem findItem5 = menu.findItem(R.id.checksum);
        findItem.setVisible(false);
        findItem2.setVisible(false);
        findItem3.setVisible(false);
        findItem4.setVisible(false);
        findItem5.setVisible(false);
        DownloadMission downloadMission = viewHolder.f7093a;
        if (downloadMission.finished) {
            findItem3.setVisible(true);
            findItem4.setVisible(true);
            findItem5.setVisible(true);
        } else if (!downloadMission.running) {
            if (downloadMission.errCode == -1) {
                findItem.setVisible(true);
            }
            findItem4.setVisible(true);
        } else {
            findItem2.setVisible(true);
        }
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem menuItem) {
                int itemId = menuItem.getItemId();
                switch (itemId) {
                    case R.id.delete:
                        MissionAdapter.this.c.c(viewHolder.b);
                        MissionAdapter.this.notifyDataSetChanged();
                        return true;
                    case R.id.md5:
                    case R.id.sha1:
                        DownloadMission d = MissionAdapter.this.c.d(viewHolder.b);
                        ChecksumTask checksumTask = new ChecksumTask(MissionAdapter.this.f7088a);
                        checksumTask.execute(new String[]{d.location + "/" + d.name, (String) MissionAdapter.h.get(Integer.valueOf(itemId))});
                        return true;
                    case R.id.pause:
                        MissionAdapter.this.c.a(viewHolder.b);
                        MissionAdapter.this.d.b(MissionAdapter.this.c.d(viewHolder.b));
                        ViewHolder viewHolder = viewHolder;
                        viewHolder.k = -1;
                        viewHolder.l = -1;
                        return true;
                    case R.id.start:
                        MissionAdapter.this.c.b(viewHolder.b);
                        MissionAdapter.this.d.a(MissionAdapter.this.c.d(viewHolder.b));
                        return true;
                    case R.id.view:
                        DownloadMission downloadMission = viewHolder.f7093a;
                        File file = new File(downloadMission.location, downloadMission.name);
                        if (file.exists()) {
                            MissionAdapter.this.a(viewHolder.e.getText().toString(), viewHolder.n, file, "");
                        } else {
                            Log.w("MissionAdapter", "File doesn't exist");
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        final ViewHolder viewHolder = new ViewHolder(this.b.inflate(this.e, viewGroup, false));
        viewHolder.h.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MissionAdapter.this.c(viewHolder);
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DownloadMission downloadMission = viewHolder.f7093a;
                if (downloadMission.finished) {
                    File file = new File(downloadMission.location, downloadMission.name);
                    if (file.exists()) {
                        MissionAdapter.this.a(viewHolder.e.getText().toString(), viewHolder.n, file, "");
                    } else {
                        Log.w("MissionAdapter", "File doesn't exist");
                    }
                }
            }
        });
        return viewHolder;
    }

    /* renamed from: b */
    public void onViewRecycled(ViewHolder viewHolder) {
        super.onViewRecycled(viewHolder);
        viewHolder.f7093a.b((DownloadMission.MissionListener) viewHolder.j);
        viewHolder.f7093a = null;
        viewHolder.j = null;
        viewHolder.i = null;
        viewHolder.b = -1;
        viewHolder.k = -1;
        viewHolder.l = -1;
        viewHolder.m = 0;
    }

    /* renamed from: a */
    public void onViewDetachedFromWindow(ViewHolder viewHolder) {
        this.g.dispose();
        super.onViewDetachedFromWindow(viewHolder);
    }

    /* renamed from: a */
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        DownloadMission d2 = this.c.d(i);
        viewHolder.f7093a = d2;
        viewHolder.b = i;
        viewHolder.n = d2.mvInfo;
        Utility.FileType a2 = Utility.a(d2.name);
        viewHolder.d.setImageResource(Utility.c(a2));
        viewHolder.e.setText(d2.name);
        viewHolder.f.setText(Utility.a(d2.length));
        viewHolder.i = new ProgressDrawable(this.f7088a, Utility.a(a2), Utility.b(a2));
        ViewCompat.a(viewHolder.g, (Drawable) viewHolder.i);
        viewHolder.j = new MissionObserver(this, viewHolder);
        d2.a((DownloadMission.MissionListener) viewHolder.j);
        d(viewHolder);
    }

    public void b(MovieInfo movieInfo, boolean z) {
        TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
        tvWatchedEpisode.a(movieInfo.getEps().intValue());
        tvWatchedEpisode.c(movieInfo.getSession().intValue());
        tvWatchedEpisode.c(movieInfo.tmdbID);
        tvWatchedEpisode.a(movieInfo.imdbIDStr);
        tvWatchedEpisode.e(movieInfo.tvdbID);
        tvWatchedEpisode.d(movieInfo.traktID);
        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setImdbIDStr(movieInfo.imdbIDStr);
        movieEntity.setTmdbID(movieInfo.tmdbID);
        movieEntity.setTraktID(movieInfo.traktID);
        movieEntity.setTvdbID(movieInfo.tvdbID);
        movieEntity.setName(movieInfo.getName());
        String str = movieInfo.session;
        movieEntity.setTV(Boolean.valueOf(str != null && !str.isEmpty()));
        movieEntity.setGenres(movieInfo.genres);
        movieEntity.setRealeaseDate("1970-1-1");
        movieEntity.setWatched_at(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
        if (movieEntity.getTV().booleanValue()) {
            this.g.b(this.f.a(movieEntity, tvWatchedEpisode, z, false).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(d.f7097a, c.f7096a));
        } else {
            this.g.b(this.f.a(movieEntity, false).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(a.f7094a, b.f7095a));
        }
    }

    /* access modifiers changed from: private */
    public void a(ViewHolder viewHolder, boolean z) {
        ViewHolder viewHolder2 = viewHolder;
        if (viewHolder2.f7093a != null) {
            long currentTimeMillis = System.currentTimeMillis();
            if (viewHolder2.k == -1) {
                viewHolder2.k = currentTimeMillis;
            }
            if (viewHolder2.l == -1) {
                viewHolder2.l = viewHolder2.f7093a.done;
            }
            long j = currentTimeMillis - viewHolder2.k;
            long j2 = viewHolder2.f7093a.done - viewHolder2.l;
            if (j == 0 || j > 1000 || z) {
                DownloadMission downloadMission = viewHolder2.f7093a;
                if (downloadMission.errCode > 0) {
                    viewHolder2.c.setText(R.string.msg_error);
                } else {
                    float f2 = ((float) downloadMission.done) / ((float) downloadMission.length);
                    viewHolder2.c.setText(String.format(Locale.US, "%.2f%%", new Object[]{Float.valueOf(100.0f * f2)}));
                    viewHolder2.i.a(f2);
                }
            }
            if (j > 1000 && j2 > 0) {
                String a2 = Utility.a((((float) j2) / ((float) j)) * 1000.0f);
                String a3 = Utility.a(viewHolder2.f7093a.length);
                TextView textView = viewHolder2.f;
                textView.setText(a3 + " " + a2);
                viewHolder2.k = currentTimeMillis;
                viewHolder2.l = viewHolder2.f7093a.done;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, File file, String str3) {
        this.f7088a.getApplicationContext().getPackageName();
        String[] split = str2.split("_");
        if (split.length > 4) {
            a(str, split[1], Long.parseLong(split[0]), split[2], split[3], split[4], split[5], file.toString());
            return;
        }
        a(str, split[1], Long.parseLong(split[0]), split[2], split[3], "0", "0", file.toString());
    }

    public void a(String str, String str2, long j, String str3, String str4, String str5, String str6, String str7) {
        MovieInfo movieInfo = new MovieInfo(str, str2, str3, str4, str2);
        movieInfo.tmdbID = Long.valueOf(str5).longValue();
        movieInfo.imdbIDStr = str6;
        movieInfo.tempStreamLink = str7;
        a(movieInfo, false);
        b(movieInfo, true);
    }

    public void a(MovieInfo movieInfo, boolean z) {
        Intent a2 = new SourceActivity.UriSample(movieInfo.name, false, (String) null, (SourceActivity.DrmInfo) null, Uri.parse(movieInfo.tempStreamLink), "", "").a(Utils.i());
        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setName(movieInfo.name);
        movieEntity.setRealeaseDate(movieInfo.year);
        a2.putExtra("Movie", movieEntity);
        a2.putExtra("LINKID", "");
        a2.putExtra("streamID", 0);
        a2.putExtra("MovieInfo", movieInfo);
        a2.putExtra("ISLOCAL", true);
        a2.addFlags(402653184);
        ArrayList arrayList = new ArrayList();
        MediaSource mediaSource = new MediaSource("Local", "downloaded", false);
        mediaSource.setStreamLink("");
        arrayList.add(mediaSource);
        IntentDataContainer.a().a("MediaSouce", arrayList);
        if (z) {
            movieEntity.setPosition(0);
        }
        Utils.i().startActivity(a2);
    }
}
