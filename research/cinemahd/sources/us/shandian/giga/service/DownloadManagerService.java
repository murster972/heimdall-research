package us.shandian.giga.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.NotificationCompat;
import androidx.core.content.PermissionChecker;
import com.Setting;
import com.utils.download.DownloadActivity;
import com.vungle.warren.AdLoader;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.HashMap;
import us.shandian.giga.get.DownloadDataSource;
import us.shandian.giga.get.DownloadManager;
import us.shandian.giga.get.DownloadManagerImpl;
import us.shandian.giga.get.DownloadMission;
import us.shandian.giga.get.sqlite.SQLiteDownloadDataSource;

public class DownloadManagerService extends Service {
    private static final String h = DownloadManagerService.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public DMBinder f7084a;
    /* access modifiers changed from: private */
    public DownloadManager b;
    private Notification c;
    private Handler d;
    /* access modifiers changed from: private */
    public long e = System.currentTimeMillis();
    private DownloadDataSource f;
    /* access modifiers changed from: private */
    public MissionListener g = new MissionListener();

    public class DMBinder extends Binder {
        public DMBinder() {
        }

        public DownloadManager a() {
            return DownloadManagerService.this.b;
        }

        public void b(DownloadMission downloadMission) {
            downloadMission.b((DownloadMission.MissionListener) DownloadManagerService.this.g);
            DownloadManagerService.this.a();
        }

        public void a(DownloadMission downloadMission) {
            downloadMission.a((DownloadMission.MissionListener) DownloadManagerService.this.g);
            DownloadManagerService.this.a();
        }
    }

    public IBinder onBind(Intent intent) {
        if (Build.VERSION.SDK_INT >= 16 && PermissionChecker.b(this, "android.permission.READ_EXTERNAL_STORAGE") == -1) {
            Toast.makeText(this, "Permission denied (read)", 0).show();
        }
        if (PermissionChecker.b(this, "android.permission.WRITE_EXTERNAL_STORAGE") == -1) {
            Toast.makeText(this, "Permission denied (write)", 0).show();
        }
        return this.f7084a;
    }

    public void onCreate() {
        super.onCreate();
        this.f7084a = new DMBinder();
        if (this.f == null) {
            this.f = new SQLiteDownloadDataSource(this);
        }
        if (this.b == null) {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(Setting.b(this));
            this.b = new DownloadManagerImpl(arrayList, this.f);
        }
        PendingIntent activity = PendingIntent.getActivity(this, 0, new Intent(this, DownloadActivity.class).setAction("android.intent.action.MAIN"), 134217728);
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, getString(R.string.notification_channel_id));
        builder.a(activity);
        builder.b(17301633);
        builder.a(decodeResource);
        builder.b((CharSequence) getString(R.string.msg_running));
        builder.a((CharSequence) getString(R.string.msg_running_detail));
        this.c = builder.a();
        HandlerThread handlerThread = new HandlerThread("ServiceMessenger");
        handlerThread.start();
        this.d = new Handler(handlerThread.getLooper()) {
            public void handleMessage(Message message) {
                if (message.what == 0) {
                    int i = 0;
                    for (int i2 = 0; i2 < DownloadManagerService.this.b.getCount(); i2++) {
                        if (DownloadManagerService.this.b.d(i2).running) {
                            i++;
                        }
                    }
                    DownloadManagerService.this.a(i);
                }
            }
        };
    }

    public void onDestroy() {
        super.onDestroy();
        for (int i = 0; i < this.b.getCount(); i++) {
            this.b.a(i);
        }
        stopForeground(true);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        String str = h;
        Log.i(str, "Got intent: " + intent);
        String action = intent.getAction();
        if (action == null || !action.equals("android.intent.action.RUN")) {
            return 2;
        }
        String stringExtra = intent.getStringExtra("DownloadManagerService.extra.name");
        String stringExtra2 = intent.getStringExtra("DownloadManagerService.extra.location");
        int intExtra = intent.getIntExtra("DownloadManagerService.extra.threads", 1);
        a(intent.getDataString(), stringExtra2, stringExtra, intent.getBooleanExtra("DownloadManagerService.extra.is_audio", false), intExtra, (HashMap) intent.getSerializableExtra("DownloadManagerService.extra.headers"), intent.getStringExtra("DownloadManagerService.extra.mvinfo"));
        return 2;
    }

    private class MissionListener implements DownloadMission.MissionListener {
        private MissionListener() {
        }

        public void a(DownloadMission downloadMission, long j, long j2) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - DownloadManagerService.this.e > AdLoader.RETRY_DELAY) {
                DownloadManagerService.this.a();
                long unused = DownloadManagerService.this.e = currentTimeMillis;
            }
        }

        public void a(DownloadMission downloadMission) {
            DownloadManagerService.this.a();
            DownloadManagerService.this.a(downloadMission);
        }

        public void a(DownloadMission downloadMission, int i) {
            DownloadManagerService.this.a();
        }
    }

    /* access modifiers changed from: private */
    public void a(DownloadMission downloadMission) {
        sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.parse("file://" + downloadMission.location + "/" + downloadMission.name)));
    }

    private void a(String str, String str2, String str3, boolean z, int i, HashMap<String, String> hashMap, String str4) {
        final String str5 = str;
        final String str6 = str2;
        final String str7 = str3;
        final boolean z2 = z;
        final int i2 = i;
        final HashMap<String, String> hashMap2 = hashMap;
        final String str8 = str4;
        this.d.post(new Runnable() {
            public void run() {
                DownloadManagerService.this.f7084a.a(DownloadManagerService.this.b.d(DownloadManagerService.this.b.a(str5, str6, str7, z2, i2, hashMap2, str8)));
            }
        });
    }

    /* access modifiers changed from: private */
    public void a() {
        this.d.sendEmptyMessage(0);
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        if (i == 0) {
            stopForeground(true);
        } else {
            startForeground(969696, this.c);
        }
    }

    public static void a(Context context, String str, String str2, String str3, boolean z, int i, HashMap<String, String> hashMap, String str4) {
        Intent intent = new Intent(context, DownloadManagerService.class);
        intent.setAction("android.intent.action.RUN");
        intent.setData(Uri.parse(str));
        intent.putExtra("DownloadManagerService.extra.name", str3);
        intent.putExtra("DownloadManagerService.extra.location", str2);
        intent.putExtra("DownloadManagerService.extra.is_audio", z);
        intent.putExtra("DownloadManagerService.extra.threads", i);
        intent.putExtra("DownloadManagerService.extra.headers", hashMap);
        intent.putExtra("DownloadManagerService.extra.mvinfo", str4);
        context.startService(intent);
    }
}
