package us.shandian.giga.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v4.media.session.PlaybackStateCompat;
import android.widget.Toast;
import com.facebook.common.util.ByteConstants;
import com.yoku.marumovie.R;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utility {

    /* renamed from: us.shandian.giga.util.Utility$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f7100a = new int[FileType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                us.shandian.giga.util.Utility$FileType[] r0 = us.shandian.giga.util.Utility.FileType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f7100a = r0
                int[] r0 = f7100a     // Catch:{ NoSuchFieldError -> 0x0014 }
                us.shandian.giga.util.Utility$FileType r1 = us.shandian.giga.util.Utility.FileType.MUSIC     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f7100a     // Catch:{ NoSuchFieldError -> 0x001f }
                us.shandian.giga.util.Utility$FileType r1 = us.shandian.giga.util.Utility.FileType.VIDEO     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: us.shandian.giga.util.Utility.AnonymousClass1.<clinit>():void");
        }
    }

    public enum FileType {
        VIDEO,
        MUSIC,
        UNKNOWN
    }

    public static String a(long j) {
        if (j < 1024) {
            return String.format("%d B", new Object[]{Long.valueOf(j)});
        } else if (j < PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
            return String.format("%.2f kB", new Object[]{Float.valueOf(((float) j) / 1024.0f)});
        } else if (j < 1073741824) {
            return String.format("%.2f MB", new Object[]{Float.valueOf((((float) j) / 1024.0f) / 1024.0f)});
        } else {
            return String.format("%.2f GB", new Object[]{Float.valueOf(((((float) j) / 1024.0f) / 1024.0f) / 1024.0f)});
        }
    }

    public static <T> T b(String str) {
        ObjectInputStream objectInputStream;
        T t = null;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream(str));
            try {
                t = objectInputStream.readObject();
            } catch (Exception unused) {
            }
        } catch (Exception unused2) {
            objectInputStream = null;
        }
        if (objectInputStream != null) {
            try {
                objectInputStream.close();
            } catch (Exception unused3) {
            }
        }
        return t;
    }

    public static int c(FileType fileType) {
        int i = AnonymousClass1.f7100a[fileType.ordinal()];
        if (i == 1) {
            return R.drawable.music;
        }
        if (i != 2) {
        }
        return R.drawable.video;
    }

    public static int b(FileType fileType) {
        int i = AnonymousClass1.f7100a[fileType.ordinal()];
        if (i != 1) {
            return i != 2 ? R.color.gray : R.color.video_already_load_color;
        }
        return R.color.audio_already_load_color;
    }

    public static String a(float f) {
        if (f < 1024.0f) {
            return String.format("%.2f B/s", new Object[]{Float.valueOf(f)});
        } else if (f < 1048576.0f) {
            return String.format("%.2f kB/s", new Object[]{Float.valueOf(f / 1024.0f)});
        } else if (f < 1.07374182E9f) {
            return String.format("%.2f MB/s", new Object[]{Float.valueOf((f / 1024.0f) / 1024.0f)});
        } else {
            return String.format("%.2f GB/s", new Object[]{Float.valueOf(((f / 1024.0f) / 1024.0f) / 1024.0f)});
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001f A[SYNTHETIC, Splitter:B:13:0x001f] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0026 A[SYNTHETIC, Splitter:B:19:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r4, java.io.Serializable r5) {
        /*
            r0 = 0
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0023, all -> 0x001c }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x0023, all -> 0x001c }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0023, all -> 0x001c }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0023, all -> 0x001c }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0023, all -> 0x001c }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0023, all -> 0x001c }
            r1.writeObject(r5)     // Catch:{ Exception -> 0x001a, all -> 0x0017 }
            r1.close()     // Catch:{ Exception -> 0x0029 }
            goto L_0x0029
        L_0x0017:
            r4 = move-exception
            r0 = r1
            goto L_0x001d
        L_0x001a:
            r0 = r1
            goto L_0x0024
        L_0x001c:
            r4 = move-exception
        L_0x001d:
            if (r0 == 0) goto L_0x0022
            r0.close()     // Catch:{ Exception -> 0x0022 }
        L_0x0022:
            throw r4
        L_0x0023:
        L_0x0024:
            if (r0 == 0) goto L_0x0029
            r0.close()     // Catch:{ Exception -> 0x0029 }
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: us.shandian.giga.util.Utility.a(java.lang.String, java.io.Serializable):void");
    }

    public static FileType a(String str) {
        if (str.endsWith(".mp3") || str.endsWith(".wav") || str.endsWith(".flac") || str.endsWith(".m4a")) {
            return FileType.MUSIC;
        }
        if (str.endsWith(".mp4") || str.endsWith(".mpeg") || str.endsWith(".rm") || str.endsWith(".rmvb") || str.endsWith(".flv") || str.endsWith(".webp") || str.endsWith(".webm")) {
            return FileType.VIDEO;
        }
        return FileType.UNKNOWN;
    }

    public static int a(FileType fileType) {
        int i = AnonymousClass1.f7100a[fileType.ordinal()];
        if (i != 1) {
            return i != 2 ? R.color.gray : R.color.video_left_to_load_color;
        }
        return R.color.audio_left_to_load_color;
    }

    public static void a(Context context, String str) {
        ((ClipboardManager) context.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("text", str));
        Toast.makeText(context, R.string.msg_copied, 0).show();
    }

    public static String a(String str, String str2) {
        int i;
        try {
            MessageDigest instance = MessageDigest.getInstance(str2);
            try {
                FileInputStream fileInputStream = new FileInputStream(str);
                byte[] bArr = new byte[ByteConstants.KB];
                while (true) {
                    try {
                        int read = fileInputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        instance.update(bArr, 0, read);
                    } catch (IOException unused) {
                    }
                }
                byte[] digest = instance.digest();
                StringBuilder sb = new StringBuilder();
                for (byte b : digest) {
                    sb.append(Integer.toString((b & 255) + 256, 16).substring(1));
                }
                return sb.toString();
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        }
    }
}
