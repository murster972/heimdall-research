package us.shandian.giga.get;

public class DownloadRunnableFallback implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final DownloadMission f7082a;

    public DownloadRunnableFallback(DownloadMission downloadMission) {
        if (downloadMission != null) {
            this.f7082a = downloadMission;
            return;
        }
        throw new NullPointerException("mission is null");
    }

    private void a(long j) {
        synchronized (this.f7082a) {
            this.f7082a.b(j);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r8 = this;
            r0 = -1
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x00ac }
            us.shandian.giga.get.DownloadMission r2 = r8.f7082a     // Catch:{ Exception -> 0x00ac }
            java.lang.String r2 = r2.url     // Catch:{ Exception -> 0x00ac }
            r1.<init>(r2)     // Catch:{ Exception -> 0x00ac }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Exception -> 0x00ac }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ Exception -> 0x00ac }
            us.shandian.giga.get.DownloadMission r2 = r8.f7082a     // Catch:{ Exception -> 0x00ac }
            java.util.HashMap<java.lang.String, java.lang.String> r2 = r2.headers     // Catch:{ Exception -> 0x00ac }
            if (r2 == 0) goto L_0x0042
            us.shandian.giga.get.DownloadMission r2 = r8.f7082a     // Catch:{ Exception -> 0x00ac }
            java.util.HashMap<java.lang.String, java.lang.String> r2 = r2.headers     // Catch:{ Exception -> 0x00ac }
            java.util.Set r2 = r2.entrySet()     // Catch:{ Exception -> 0x00ac }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ Exception -> 0x00ac }
        L_0x0022:
            boolean r3 = r2.hasNext()     // Catch:{ Exception -> 0x00ac }
            if (r3 == 0) goto L_0x0042
            java.lang.Object r3 = r2.next()     // Catch:{ Exception -> 0x00ac }
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch:{ Exception -> 0x00ac }
            java.lang.Object r4 = r3.getKey()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00ac }
            java.lang.Object r3 = r3.getValue()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ac }
            r1.setRequestProperty(r4, r3)     // Catch:{ Exception -> 0x00ac }
            goto L_0x0022
        L_0x0042:
            int r2 = r1.getResponseCode()     // Catch:{ Exception -> 0x00ac }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 == r3) goto L_0x0056
            int r2 = r1.getResponseCode()     // Catch:{ Exception -> 0x00ac }
            r3 = 206(0xce, float:2.89E-43)
            if (r2 == r3) goto L_0x0056
            r8.a((int) r3)     // Catch:{ Exception -> 0x00ac }
            goto L_0x00b1
        L_0x0056:
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x00ac }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ac }
            r3.<init>()     // Catch:{ Exception -> 0x00ac }
            us.shandian.giga.get.DownloadMission r4 = r8.f7082a     // Catch:{ Exception -> 0x00ac }
            java.lang.String r4 = r4.location     // Catch:{ Exception -> 0x00ac }
            r3.append(r4)     // Catch:{ Exception -> 0x00ac }
            java.lang.String r4 = "/"
            r3.append(r4)     // Catch:{ Exception -> 0x00ac }
            us.shandian.giga.get.DownloadMission r4 = r8.f7082a     // Catch:{ Exception -> 0x00ac }
            java.lang.String r4 = r4.name     // Catch:{ Exception -> 0x00ac }
            r3.append(r4)     // Catch:{ Exception -> 0x00ac }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r4 = "rw"
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x00ac }
            r3 = 0
            r2.seek(r3)     // Catch:{ Exception -> 0x00ac }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00ac }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ Exception -> 0x00ac }
            r3.<init>(r1)     // Catch:{ Exception -> 0x00ac }
            r1 = 512(0x200, float:7.175E-43)
            byte[] r4 = new byte[r1]     // Catch:{ Exception -> 0x00ac }
        L_0x008b:
            r5 = 0
            int r6 = r3.read(r4, r5, r1)     // Catch:{ Exception -> 0x00ac }
            if (r6 == r0) goto L_0x00a5
            us.shandian.giga.get.DownloadMission r7 = r8.f7082a     // Catch:{ Exception -> 0x00ac }
            boolean r7 = r7.running     // Catch:{ Exception -> 0x00ac }
            if (r7 == 0) goto L_0x00a5
            r2.write(r4, r5, r6)     // Catch:{ Exception -> 0x00ac }
            long r5 = (long) r6     // Catch:{ Exception -> 0x00ac }
            r8.a((long) r5)     // Catch:{ Exception -> 0x00ac }
            boolean r5 = java.lang.Thread.interrupted()     // Catch:{ Exception -> 0x00ac }
            if (r5 == 0) goto L_0x008b
        L_0x00a5:
            r2.close()     // Catch:{ Exception -> 0x00ac }
            r3.close()     // Catch:{ Exception -> 0x00ac }
            goto L_0x00b1
        L_0x00ac:
            r1 = 233(0xe9, float:3.27E-43)
            r8.a((int) r1)
        L_0x00b1:
            us.shandian.giga.get.DownloadMission r1 = r8.f7082a
            int r2 = r1.errCode
            if (r2 != r0) goto L_0x00be
            boolean r0 = r1.running
            if (r0 == 0) goto L_0x00be
            r8.a()
        L_0x00be:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: us.shandian.giga.get.DownloadRunnableFallback.run():void");
    }

    private void a(int i) {
        synchronized (this.f7082a) {
            this.f7082a.b(i);
            this.f7082a.d();
        }
    }

    private void a() {
        synchronized (this.f7082a) {
            this.f7082a.c();
        }
    }
}
