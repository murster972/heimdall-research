package us.shandian.giga.get;

import android.os.Handler;
import android.os.Looper;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import us.shandian.giga.util.Utility;

public class DownloadMission implements Serializable {
    private static final long serialVersionUID = 0;

    /* renamed from: a  reason: collision with root package name */
    public transient boolean f7075a;
    private transient ArrayList<WeakReference<MissionListener>> b = new ArrayList<>();
    public final Map<Long, Boolean> blockState = new HashMap();
    public long blocks;
    /* access modifiers changed from: private */
    public transient boolean c;
    public long done;
    public int errCode = -1;
    public boolean fallback;
    public int finishCount;
    public boolean finished;
    public HashMap<String, String> headers;
    public long length;
    public String location;
    public String mvInfo;
    public String name;
    public boolean running;
    public int threadCount = 3;
    private List<Long> threadPositions = new ArrayList();
    public long timestamp;
    public String url;

    public interface MissionListener {

        /* renamed from: a  reason: collision with root package name */
        public static final HashMap<MissionListener, Handler> f7080a = new HashMap<>();

        void a(DownloadMission downloadMission);

        void a(DownloadMission downloadMission, int i);

        void a(DownloadMission downloadMission, long j, long j2);
    }

    static {
        Class<DownloadMission> cls = DownloadMission.class;
    }

    public DownloadMission() {
    }

    private void d(long j) {
        if (j < 0 || j >= this.blocks) {
            throw new IllegalArgumentException("illegal block identifier");
        }
    }

    private void g() {
        new File(i()).delete();
    }

    /* access modifiers changed from: private */
    public void h() {
        synchronized (this.blockState) {
            Utility.a(i(), (Serializable) this);
        }
    }

    private String i() {
        return this.location + "/" + this.name + ".giga";
    }

    private void j() {
        if (this.errCode <= 0) {
            this.running = false;
            this.finished = true;
            g();
            Iterator<WeakReference<MissionListener>> it2 = this.b.iterator();
            while (it2.hasNext()) {
                final MissionListener missionListener = (MissionListener) it2.next().get();
                if (missionListener != null) {
                    MissionListener.f7080a.get(missionListener).post(new Runnable() {
                        public void run() {
                            missionListener.a(DownloadMission.this);
                        }
                    });
                }
            }
        }
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.b = new ArrayList<>();
    }

    public synchronized void b(long j) {
        if (this.running) {
            if (this.f7075a) {
                this.f7075a = false;
            }
            this.done += j;
            if (this.done > this.length) {
                this.done = this.length;
            }
            if (this.done != this.length) {
                f();
            }
            Iterator<WeakReference<MissionListener>> it2 = this.b.iterator();
            while (it2.hasNext()) {
                final MissionListener missionListener = (MissionListener) it2.next().get();
                if (missionListener != null) {
                    MissionListener.f7080a.get(missionListener).post(new Runnable() {
                        public void run() {
                            MissionListener missionListener = missionListener;
                            DownloadMission downloadMission = DownloadMission.this;
                            missionListener.a(downloadMission, downloadMission.done, downloadMission.length);
                        }
                    });
                }
            }
        }
    }

    public void c(long j) {
        d(j);
        synchronized (this.blockState) {
            this.blockState.put(Long.valueOf(j), true);
        }
    }

    public void e() {
        if (!this.running && !this.finished) {
            this.running = true;
            if (!this.fallback) {
                for (int i = 0; i < this.threadCount; i++) {
                    if (this.threadPositions.size() <= i && !this.f7075a) {
                        this.threadPositions.add(Long.valueOf((long) i));
                    }
                    new Thread(new DownloadRunnable(this, i)).start();
                }
                return;
            }
            this.threadCount = 1;
            this.done = 0;
            this.blocks = 0;
            new Thread(new DownloadRunnableFallback(this)).start();
        }
    }

    public void f() {
        if (!this.c) {
            this.c = true;
            new Thread() {
                public void run() {
                    DownloadMission.this.h();
                    boolean unused = DownloadMission.this.c = false;
                }
            }.start();
        }
    }

    public boolean a(long j) {
        d(j);
        if (this.blockState.containsKey(Long.valueOf(j))) {
            return this.blockState.get(Long.valueOf(j)).booleanValue();
        }
        return false;
    }

    public void d() {
        if (this.running) {
            this.running = false;
            this.f7075a = true;
        }
    }

    public void a(int i, long j) {
        this.threadPositions.set(i, Long.valueOf(j));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0017, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void c() {
        /*
            r2 = this;
            monitor-enter(r2)
            int r0 = r2.errCode     // Catch:{ all -> 0x0018 }
            if (r0 <= 0) goto L_0x0007
            monitor-exit(r2)
            return
        L_0x0007:
            int r0 = r2.finishCount     // Catch:{ all -> 0x0018 }
            int r0 = r0 + 1
            r2.finishCount = r0     // Catch:{ all -> 0x0018 }
            int r0 = r2.finishCount     // Catch:{ all -> 0x0018 }
            int r1 = r2.threadCount     // Catch:{ all -> 0x0018 }
            if (r0 != r1) goto L_0x0016
            r2.j()     // Catch:{ all -> 0x0018 }
        L_0x0016:
            monitor-exit(r2)
            return
        L_0x0018:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: us.shandian.giga.get.DownloadMission.c():void");
    }

    public long a(int i) {
        return this.threadPositions.get(i).longValue();
    }

    public DownloadMission(String str, String str2, String str3, HashMap<String, String> hashMap) {
        if (str == null) {
            throw new NullPointerException("name is null");
        } else if (str.isEmpty()) {
            throw new IllegalArgumentException("name is empty");
        } else if (str2 == null) {
            throw new NullPointerException("url is null");
        } else if (str2.isEmpty()) {
            throw new IllegalArgumentException("url is empty");
        } else if (str3 == null) {
            throw new NullPointerException("location is null");
        } else if (!str3.isEmpty()) {
            this.url = str2;
            this.name = str;
            this.location = str3;
            this.headers = hashMap;
        } else {
            throw new IllegalArgumentException("location is empty");
        }
    }

    public synchronized void a(MissionListener missionListener) {
        MissionListener.f7080a.put(missionListener, new Handler(Looper.getMainLooper()));
        this.b.add(new WeakReference(missionListener));
    }

    public void a() {
        g();
        new File(this.location, this.name).delete();
    }

    public synchronized void b(int i) {
        this.errCode = i;
        f();
        Iterator<WeakReference<MissionListener>> it2 = this.b.iterator();
        while (it2.hasNext()) {
            final MissionListener missionListener = (MissionListener) it2.next().get();
            MissionListener.f7080a.get(missionListener).post(new Runnable() {
                public void run() {
                    MissionListener missionListener = missionListener;
                    DownloadMission downloadMission = DownloadMission.this;
                    missionListener.a(downloadMission, downloadMission.errCode);
                }
            });
        }
    }

    public synchronized void b(MissionListener missionListener) {
        Iterator<WeakReference<MissionListener>> it2 = this.b.iterator();
        while (it2.hasNext()) {
            WeakReference next = it2.next();
            if (missionListener != null && missionListener == next.get()) {
                it2.remove();
            }
        }
    }

    public File b() {
        return new File(this.location, this.name);
    }
}
