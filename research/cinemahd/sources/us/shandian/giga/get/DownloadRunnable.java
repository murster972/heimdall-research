package us.shandian.giga.get;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.ar.core.ImageMetadata;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadRunnable implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final DownloadMission f7081a;
    private final int b;

    static {
        Class<DownloadRunnable> cls = DownloadRunnable.class;
    }

    public DownloadRunnable(DownloadMission downloadMission, int i) {
        if (downloadMission != null) {
            this.f7081a = downloadMission;
            this.b = i;
            return;
        }
        throw new NullPointerException("mission is null");
    }

    private void a(long j) {
        synchronized (this.f7081a) {
            this.f7081a.b(j);
        }
    }

    public void run() {
        DownloadMission downloadMission = this.f7081a;
        boolean z = downloadMission.f7075a;
        long a2 = downloadMission.a(this.b);
        while (true) {
            DownloadMission downloadMission2 = this.f7081a;
            if (downloadMission2.errCode != -1 || !downloadMission2.running || a2 >= downloadMission2.blocks) {
                break;
            } else if (Thread.currentThread().isInterrupted()) {
                this.f7081a.d();
                return;
            } else {
                while (!z) {
                    DownloadMission downloadMission3 = this.f7081a;
                    if (a2 >= downloadMission3.blocks || !downloadMission3.a(a2)) {
                        break;
                    }
                    a2++;
                }
                DownloadMission downloadMission4 = this.f7081a;
                if (a2 >= downloadMission4.blocks) {
                    break;
                }
                downloadMission4.c(a2);
                this.f7081a.a(this.b, a2);
                long j = a2 * PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE_ENABLED;
                long j2 = (PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE_ENABLED + j) - 1;
                long j3 = this.f7081a.length;
                if (j2 >= j3) {
                    j2 = j3 - 1;
                }
                int i = 0;
                try {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.f7081a.url).openConnection();
                    httpURLConnection.setRequestProperty("Range", "bytes=" + j + "-" + j2);
                    if (httpURLConnection.getResponseCode() != 206) {
                        this.f7081a.errCode = 206;
                        a(206);
                        break;
                    }
                    RandomAccessFile randomAccessFile = new RandomAccessFile(this.f7081a.location + "/" + this.f7081a.name, "rw");
                    randomAccessFile.seek(j);
                    InputStream inputStream = httpURLConnection.getInputStream();
                    byte[] bArr = new byte[ImageMetadata.CONTROL_AE_ANTIBANDING_MODE];
                    int i2 = 0;
                    while (true) {
                        if (j >= j2) {
                            break;
                        }
                        try {
                            if (!this.f7081a.running) {
                                break;
                            }
                            int read = inputStream.read(bArr, 0, bArr.length);
                            if (read == -1) {
                                break;
                            }
                            long j4 = (long) read;
                            j += j4;
                            i2 += read;
                            randomAccessFile.write(bArr, 0, read);
                            a(j4);
                        } catch (Exception unused) {
                            i = i2;
                            a((long) (-i));
                            z = true;
                        }
                    }
                    randomAccessFile.close();
                    inputStream.close();
                    z = false;
                } catch (Exception unused2) {
                    a((long) (-i));
                    z = true;
                }
            }
        }
        DownloadMission downloadMission5 = this.f7081a;
        if (downloadMission5.errCode == -1 && downloadMission5.running) {
            a();
        }
    }

    private void a(int i) {
        synchronized (this.f7081a) {
            this.f7081a.b(i);
            this.f7081a.d();
        }
    }

    private void a() {
        synchronized (this.f7081a) {
            this.f7081a.c();
        }
    }
}
