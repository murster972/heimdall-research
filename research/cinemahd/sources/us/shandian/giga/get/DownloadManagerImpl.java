package us.shandian.giga.get;

import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import java.io.File;
import java.io.FilenameFilter;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import us.shandian.giga.get.DownloadMission;
import us.shandian.giga.util.Utility;

public class DownloadManagerImpl implements DownloadManager {
    private static final String c = "DownloadManagerImpl";
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final DownloadDataSource f7072a;
    private final ArrayList<DownloadMission> b = new ArrayList<>();

    private class Initializer extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private DownloadMission f7074a;

        public Initializer(DownloadManagerImpl downloadManagerImpl, DownloadMission downloadMission) {
            this.f7074a = downloadMission;
        }

        public void run() {
            try {
                URL url = new URL(this.f7074a.url);
                this.f7074a.length = (long) ((HttpURLConnection) url.openConnection()).getContentLength();
                if (this.f7074a.length <= 0) {
                    this.f7074a.errCode = 206;
                    return;
                }
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestProperty("Range", "bytes=" + (this.f7074a.length - 10) + "-" + this.f7074a.length);
                if (httpURLConnection.getResponseCode() != 206) {
                    this.f7074a.fallback = true;
                }
                this.f7074a.blocks = this.f7074a.length / PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE_ENABLED;
                if (((long) this.f7074a.threadCount) > this.f7074a.blocks) {
                    this.f7074a.threadCount = (int) this.f7074a.blocks;
                }
                if (this.f7074a.threadCount <= 0) {
                    this.f7074a.threadCount = 1;
                }
                if (this.f7074a.blocks * PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE_ENABLED < this.f7074a.length) {
                    this.f7074a.blocks++;
                }
                new File(this.f7074a.location).mkdirs();
                new File(this.f7074a.location + "/" + this.f7074a.name).createNewFile();
                RandomAccessFile randomAccessFile = new RandomAccessFile(this.f7074a.location + "/" + this.f7074a.name, "rw");
                randomAccessFile.setLength(this.f7074a.length);
                randomAccessFile.close();
                this.f7074a.e();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private class MissionListener implements DownloadMission.MissionListener {
        private final DownloadMission b;

        public void a(DownloadMission downloadMission) {
            DownloadManagerImpl.this.f7072a.b(this.b);
        }

        public void a(DownloadMission downloadMission, int i) {
        }

        public void a(DownloadMission downloadMission, long j, long j2) {
        }

        private MissionListener(DownloadMission downloadMission) {
            if (downloadMission != null) {
                this.b = downloadMission;
                return;
            }
            throw new NullPointerException("mission is null");
        }
    }

    public DownloadManagerImpl(Collection<String> collection, DownloadDataSource downloadDataSource) {
        this.f7072a = downloadDataSource;
        a((Iterable<String>) collection);
    }

    public void b(int i) {
        DownloadMission d = d(i);
        if (!d.running && d.errCode == -1) {
            d.e();
        }
    }

    public void c(int i) {
        DownloadMission d = d(i);
        if (d.finished) {
            this.f7072a.a(d);
        }
        d.a();
        this.b.remove(i);
    }

    public DownloadMission d(int i) {
        return this.b.get(i);
    }

    public int getCount() {
        return this.b.size();
    }

    public int a(String str, String str2, String str3, boolean z, int i, HashMap<String, String> hashMap, String str4) {
        DownloadMission b2 = b(str2, str3);
        if (b2 != null) {
            if (b2.finished) {
                c(this.b.indexOf(b2));
            } else {
                try {
                    str3 = a(str2, str3);
                } catch (Exception e) {
                    Log.e(c, "Unable to generate unique name", e);
                    str3 = System.currentTimeMillis() + str3;
                    Log.i(c, "Using " + str3);
                }
            }
        }
        DownloadMission downloadMission = new DownloadMission(str3, str, str2, hashMap);
        downloadMission.timestamp = System.currentTimeMillis();
        downloadMission.threadCount = i;
        downloadMission.mvInfo = str4;
        downloadMission.a((DownloadMission.MissionListener) new MissionListener(downloadMission));
        new Initializer(this, downloadMission).start();
        return a(downloadMission);
    }

    private DownloadMission b(String str, String str2) {
        Iterator<DownloadMission> it2 = this.b.iterator();
        while (it2.hasNext()) {
            DownloadMission next = it2.next();
            if (str.equals(next.location) && str2.equals(next.name)) {
                return next;
            }
        }
        return null;
    }

    private static String[] b(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf <= 0 || lastIndexOf == str.length() - 1) {
            return new String[]{str, ""};
        }
        return new String[]{str.substring(0, lastIndexOf), str.substring(lastIndexOf + 1)};
    }

    public void a(int i) {
        DownloadMission d = d(i);
        if (d.running) {
            d.d();
        }
    }

    private void a(Iterable<String> iterable) {
        this.b.clear();
        a();
        for (String a2 : iterable) {
            a(a2);
        }
    }

    static void a(List<DownloadMission> list) {
        Collections.sort(list, new Comparator<DownloadMission>() {
            /* renamed from: a */
            public int compare(DownloadMission downloadMission, DownloadMission downloadMission2) {
                return Long.valueOf(downloadMission.timestamp).compareTo(Long.valueOf(downloadMission2.timestamp));
            }
        });
    }

    private void a() {
        List<DownloadMission> a2 = this.f7072a.a();
        if (a2 == null) {
            a2 = new ArrayList<>();
        }
        a(a2);
        ArrayList<DownloadMission> arrayList = this.b;
        arrayList.ensureCapacity(arrayList.size() + a2.size());
        for (DownloadMission downloadMission : a2) {
            File b2 = downloadMission.b();
            if (!b2.isFile()) {
                this.f7072a.a(downloadMission);
            } else {
                downloadMission.length = b2.length();
                downloadMission.finished = true;
                downloadMission.running = false;
                this.b.add(downloadMission);
            }
        }
    }

    private void a(String str) {
        DownloadMission downloadMission;
        File file = new File(str);
        if (file.exists() && file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                Log.e(c, "listFiles() returned null");
                return;
            }
            for (File file2 : listFiles) {
                if (file2.isFile() && file2.getName().endsWith(".giga") && (downloadMission = (DownloadMission) Utility.b(file2.getAbsolutePath())) != null) {
                    if (!downloadMission.finished) {
                        downloadMission.running = false;
                        downloadMission.f7075a = true;
                        a(downloadMission);
                    } else if (!file2.delete()) {
                        Log.w(c, "Unable to delete .giga file: " + file2.getPath());
                    }
                }
            }
        }
    }

    private int a(DownloadMission downloadMission) {
        int i;
        if (this.b.size() > 0) {
            i = -1;
            do {
                i++;
                if (this.b.get(i).timestamp <= downloadMission.timestamp) {
                    break;
                }
            } while (i >= this.b.size() - 1);
        } else {
            i = 0;
        }
        this.b.add(i, downloadMission);
        return i;
    }

    private static String a(String str, String str2) {
        String str3;
        if (str == null) {
            throw new NullPointerException("location is null");
        } else if (str2 != null) {
            File file = new File(str);
            if (file.isDirectory()) {
                final String[] b2 = b(str2);
                String[] list = file.list(new FilenameFilter() {
                    public boolean accept(File file, String str) {
                        return str.startsWith(b2[0]);
                    }
                });
                Arrays.sort(list);
                int i = 0;
                do {
                    str3 = b2[0] + " (" + i + ")." + b2[1];
                    i++;
                    if (i == 1000) {
                        throw new RuntimeException("Too many existing files");
                    }
                } while (Arrays.binarySearch(list, str3) >= 0);
                return str3;
            }
            throw new IllegalArgumentException("location is not a directory: " + str);
        } else {
            throw new NullPointerException("name is null");
        }
    }
}
