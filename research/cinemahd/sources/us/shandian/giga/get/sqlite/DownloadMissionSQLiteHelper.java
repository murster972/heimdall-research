package us.shandian.giga.get.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.HashMap;
import java.util.Map;
import us.shandian.giga.get.DownloadMission;

public class DownloadMissionSQLiteHelper extends SQLiteOpenHelper {
    DownloadMissionSQLiteHelper(Context context) {
        super(context, "downloads.db", (SQLiteDatabase.CursorFactory) null, 3);
    }

    public static ContentValues a(DownloadMission downloadMission) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ReportDBAdapter.ReportColumns.COLUMN_URL, downloadMission.url);
        contentValues.put("location", downloadMission.location);
        contentValues.put(MediationMetaData.KEY_NAME, downloadMission.name);
        contentValues.put("bytes_downloaded", Long.valueOf(downloadMission.done));
        contentValues.put(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, Long.valueOf(downloadMission.timestamp));
        HashMap<String, String> hashMap = downloadMission.headers;
        String str = "";
        if (hashMap != null) {
            for (Map.Entry next : hashMap.entrySet()) {
                str = str + ((String) next.getKey()) + "@@" + ((String) next.getValue()) + "@@";
            }
        }
        contentValues.put("mvinfos", downloadMission.mvInfo);
        contentValues.put("headers", str);
        return contentValues;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE download_missions (location TEXT NOT NULL, name TEXT NOT NULL, url TEXT NOT NULL, bytes_downloaded INTEGER NOT NULL, timestamp INTEGER NOT NULL, headers TEXT NOT NULL, mvinfos TEXT NOT NULL,  UNIQUE(location, name));");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public static DownloadMission a(Cursor cursor) {
        if (cursor != null) {
            DownloadMission downloadMission = new DownloadMission(cursor.getString(cursor.getColumnIndexOrThrow(MediationMetaData.KEY_NAME)), cursor.getString(cursor.getColumnIndexOrThrow(ReportDBAdapter.ReportColumns.COLUMN_URL)), cursor.getString(cursor.getColumnIndexOrThrow("location")), (HashMap<String, String>) null);
            downloadMission.done = cursor.getLong(cursor.getColumnIndexOrThrow("bytes_downloaded"));
            downloadMission.timestamp = cursor.getLong(cursor.getColumnIndexOrThrow(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP));
            downloadMission.mvInfo = cursor.getString(cursor.getColumnIndexOrThrow("mvinfos"));
            String string = cursor.getString(cursor.getColumnIndexOrThrow("headers"));
            if (!string.isEmpty()) {
                String[] split = string.split("@@");
                for (int i = 0; i < split.length - 2; i++) {
                    if (downloadMission.headers == null) {
                        downloadMission.headers = new HashMap<>();
                    }
                    if (split[i] != null && !split[i].isEmpty()) {
                        int i2 = i + 1;
                        if (split[i2] != null && !split[i2].isEmpty()) {
                            downloadMission.headers.put(split[i], split[i2]);
                        }
                    }
                }
            }
            downloadMission.finished = true;
            return downloadMission;
        }
        throw new NullPointerException("cursor is null");
    }
}
