package us.shandian.giga.get.sqlite;

import android.content.Context;
import android.database.Cursor;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.ArrayList;
import java.util.List;
import us.shandian.giga.get.DownloadDataSource;
import us.shandian.giga.get.DownloadMission;

public class SQLiteDownloadDataSource implements DownloadDataSource {

    /* renamed from: a  reason: collision with root package name */
    private final DownloadMissionSQLiteHelper f7083a;

    public SQLiteDownloadDataSource(Context context) {
        this.f7083a = new DownloadMissionSQLiteHelper(context);
    }

    public List<DownloadMission> a() {
        Cursor query = this.f7083a.getReadableDatabase().query("download_missions", (String[]) null, (String) null, (String[]) null, (String) null, (String) null, VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP);
        int count = query.getCount();
        if (count == 0) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList(count);
        while (query.moveToNext()) {
            arrayList.add(DownloadMissionSQLiteHelper.a(query));
        }
        return arrayList;
    }

    public void b(DownloadMission downloadMission) {
        if (downloadMission != null) {
            this.f7083a.getWritableDatabase().insert("download_missions", (String) null, DownloadMissionSQLiteHelper.a(downloadMission));
            return;
        }
        throw new NullPointerException("downloadMission is null");
    }

    public void a(DownloadMission downloadMission) {
        if (downloadMission != null) {
            this.f7083a.getWritableDatabase().delete("download_missions", "location = ? AND name = ?", new String[]{downloadMission.location, downloadMission.name});
            return;
        }
        throw new NullPointerException("downloadMission is null");
    }
}
