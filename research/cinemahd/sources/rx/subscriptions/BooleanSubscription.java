package rx.subscriptions;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import rx.Subscription;
import rx.functions.Action0;

public final class BooleanSubscription implements Subscription {
    static final AtomicIntegerFieldUpdater<BooleanSubscription> c = AtomicIntegerFieldUpdater.newUpdater(BooleanSubscription.class, "b");

    /* renamed from: a  reason: collision with root package name */
    private final Action0 f7068a;
    volatile int b;

    public BooleanSubscription() {
        this.f7068a = null;
    }

    public static BooleanSubscription a(Action0 action0) {
        return new BooleanSubscription(action0);
    }

    public boolean isUnsubscribed() {
        return this.b != 0;
    }

    public final void unsubscribe() {
        Action0 action0;
        if (c.compareAndSet(this, 0, 1) && (action0 = this.f7068a) != null) {
            action0.call();
        }
    }

    private BooleanSubscription(Action0 action0) {
        this.f7068a = action0;
    }
}
