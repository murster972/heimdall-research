package rx.subscriptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import rx.Subscription;
import rx.exceptions.Exceptions;

public final class CompositeSubscription implements Subscription {

    /* renamed from: a  reason: collision with root package name */
    private Set<Subscription> f7069a;
    private boolean b = false;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
        r2.unsubscribe();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r0 == false) goto L_?;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(rx.Subscription r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r0 = r1.b     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x0017
            java.util.Set<rx.Subscription> r0 = r1.f7069a     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x000a
            goto L_0x0017
        L_0x000a:
            java.util.Set<rx.Subscription> r0 = r1.f7069a     // Catch:{ all -> 0x0019 }
            boolean r0 = r0.remove(r2)     // Catch:{ all -> 0x0019 }
            monitor-exit(r1)     // Catch:{ all -> 0x0019 }
            if (r0 == 0) goto L_0x0016
            r2.unsubscribe()
        L_0x0016:
            return
        L_0x0017:
            monitor-exit(r1)     // Catch:{ all -> 0x0019 }
            return
        L_0x0019:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0019 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: rx.subscriptions.CompositeSubscription.a(rx.Subscription):void");
    }

    public synchronized boolean isUnsubscribed() {
        return this.b;
    }

    public void unsubscribe() {
        synchronized (this) {
            if (!this.b) {
                this.b = true;
                a((Collection<Subscription>) this.f7069a);
            }
        }
    }

    private static void a(Collection<Subscription> collection) {
        if (collection != null) {
            ArrayList arrayList = null;
            for (Subscription unsubscribe : collection) {
                try {
                    unsubscribe.unsubscribe();
                } catch (Throwable th) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(th);
                }
            }
            Exceptions.a((List<? extends Throwable>) arrayList);
        }
    }
}
