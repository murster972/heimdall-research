package rx.subscriptions;

import rx.Subscription;
import rx.functions.Action0;

public final class Subscriptions {

    private static final class Unsubscribed implements Subscription {
        private Unsubscribed() {
        }

        public void unsubscribe() {
        }
    }

    static {
        new Unsubscribed();
    }

    private Subscriptions() {
        throw new IllegalStateException("No instances!");
    }

    public static Subscription a(Action0 action0) {
        return BooleanSubscription.a(action0);
    }
}
