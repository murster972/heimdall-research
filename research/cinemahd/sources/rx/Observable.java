package rx;

import java.util.Arrays;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.internal.operators.OnSubscribeFromIterable;
import rx.internal.operators.OperatorConcat;
import rx.internal.operators.OperatorFilter;
import rx.internal.operators.OperatorMap;
import rx.internal.util.ScalarSynchronousObservable;
import rx.plugins.RxJavaObservableExecutionHook;
import rx.plugins.RxJavaPlugins;

public class Observable<T> {

    /* renamed from: a  reason: collision with root package name */
    private static final RxJavaObservableExecutionHook f7034a = RxJavaPlugins.d().b();

    public interface OnSubscribe<T> extends Action1<Subscriber<? super T>> {
    }

    public interface Operator<R, T> extends Func1<Subscriber<? super R>, Subscriber<? super T>> {
    }

    static {
        a(new OnSubscribe<Object>() {
        });
    }

    protected Observable(OnSubscribe<T> onSubscribe) {
    }

    public static final <T> Observable<T> a(OnSubscribe<T> onSubscribe) {
        return new Observable<>(f7034a.a(onSubscribe));
    }

    public static final <T> Observable<T> b(T t) {
        return ScalarSynchronousObservable.c(t);
    }

    public final <R> Observable<R> a(Operator<? extends R, ? super T> operator) {
        return new Observable<>(new OnSubscribe<R>(this, operator) {
        });
    }

    public final <R> Observable<R> b(Func1<? super T, ? extends R> func1) {
        return a(new OperatorMap(func1));
    }

    public static final <T> Observable<T> a(Observable<? extends Observable<? extends T>> observable) {
        return observable.a((Operator<? extends R, ? super Object>) OperatorConcat.a());
    }

    public static final <T> Observable<T> a(Observable<? extends T> observable, Observable<? extends T> observable2) {
        return a(a(observable, observable2));
    }

    public static final <T> Observable<T> a(Iterable<? extends T> iterable) {
        return a(new OnSubscribeFromIterable(iterable));
    }

    public static final <T> Observable<T> a(T t, T t2) {
        return a(Arrays.asList(new Object[]{t, t2}));
    }

    public final Observable<T> a(Func1<? super T, Boolean> func1) {
        return a(new OperatorFilter(func1));
    }

    public final Observable<T> a(T t) {
        return a(b(t), this);
    }
}
