package rx.functions;

public final class Actions {

    /* renamed from: a  reason: collision with root package name */
    private static final EmptyAction f7038a = new EmptyAction((AnonymousClass1) null);

    /* renamed from: rx.functions.Actions$1  reason: invalid class name */
    final class AnonymousClass1 implements Func0<R> {
    }

    private static final class EmptyAction<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> implements Action0, Action1<T0>, Action2<T0, T1>, Action3<T0, T1, T2>, Action4<T0, T1, T2, T3>, Action5<T0, T1, T2, T3, T4>, Action6<T0, T1, T2, T3, T4, T5>, Action7<T0, T1, T2, T3, T4, T5, T6>, Action8<T0, T1, T2, T3, T4, T5, T6, T7>, Action9<T0, T1, T2, T3, T4, T5, T6, T7, T8>, ActionN {
        private EmptyAction() {
        }

        public void call() {
        }

        /* synthetic */ EmptyAction(AnonymousClass1 r1) {
            this();
        }
    }

    private Actions() {
        throw new IllegalStateException("No instances!");
    }

    public static final <T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> EmptyAction<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> a() {
        return f7038a;
    }
}
