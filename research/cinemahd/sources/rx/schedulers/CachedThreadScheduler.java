package rx.schedulers;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import rx.Scheduler;
import rx.internal.schedulers.NewThreadWorker;
import rx.internal.util.RxThreadFactory;
import rx.subscriptions.CompositeSubscription;

final class CachedThreadScheduler extends Scheduler {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final RxThreadFactory f7047a = new RxThreadFactory("RxCachedThreadScheduler-");
    /* access modifiers changed from: private */
    public static final RxThreadFactory b = new RxThreadFactory("RxCachedWorkerPoolEvictor-");

    private static final class EventLoopWorker extends Scheduler.Worker {
        static final AtomicIntegerFieldUpdater<EventLoopWorker> d = AtomicIntegerFieldUpdater.newUpdater(EventLoopWorker.class, "c");

        /* renamed from: a  reason: collision with root package name */
        private final CompositeSubscription f7050a = new CompositeSubscription();
        private final ThreadWorker b;
        volatile int c;

        EventLoopWorker(ThreadWorker threadWorker) {
            this.b = threadWorker;
        }

        public boolean isUnsubscribed() {
            return this.f7050a.isUnsubscribed();
        }

        public void unsubscribe() {
            if (d.compareAndSet(this, 0, 1)) {
                CachedWorkerPool.d.a(this.b);
            }
            this.f7050a.unsubscribe();
        }
    }

    private static final class ThreadWorker extends NewThreadWorker {
        private long g = 0;

        ThreadWorker(ThreadFactory threadFactory) {
            super(threadFactory);
        }

        public void a(long j) {
            this.g = j;
        }

        public long b() {
            return this.g;
        }
    }

    CachedThreadScheduler() {
    }

    public Scheduler.Worker createWorker() {
        return new EventLoopWorker(CachedWorkerPool.d.b());
    }

    private static final class CachedWorkerPool {
        /* access modifiers changed from: private */
        public static CachedWorkerPool d = new CachedWorkerPool(60, TimeUnit.SECONDS);

        /* renamed from: a  reason: collision with root package name */
        private final long f7048a;
        private final ConcurrentLinkedQueue<ThreadWorker> b = new ConcurrentLinkedQueue<>();
        private final ScheduledExecutorService c = Executors.newScheduledThreadPool(1, CachedThreadScheduler.b);

        CachedWorkerPool(long j, TimeUnit timeUnit) {
            this.f7048a = timeUnit.toNanos(j);
            ScheduledExecutorService scheduledExecutorService = this.c;
            AnonymousClass1 r1 = new Runnable() {
                public void run() {
                    CachedWorkerPool.this.a();
                }
            };
            long j2 = this.f7048a;
            scheduledExecutorService.scheduleWithFixedDelay(r1, j2, j2, TimeUnit.NANOSECONDS);
        }

        /* access modifiers changed from: package-private */
        public void a(ThreadWorker threadWorker) {
            threadWorker.a(c() + this.f7048a);
            this.b.offer(threadWorker);
        }

        /* access modifiers changed from: package-private */
        public ThreadWorker b() {
            while (!this.b.isEmpty()) {
                ThreadWorker poll = this.b.poll();
                if (poll != null) {
                    return poll;
                }
            }
            return new ThreadWorker(CachedThreadScheduler.f7047a);
        }

        /* access modifiers changed from: package-private */
        public long c() {
            return System.nanoTime();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (!this.b.isEmpty()) {
                long c2 = c();
                Iterator<ThreadWorker> it2 = this.b.iterator();
                while (it2.hasNext()) {
                    ThreadWorker next = it2.next();
                    if (next.b() > c2) {
                        return;
                    }
                    if (this.b.remove(next)) {
                        next.unsubscribe();
                    }
                }
            }
        }
    }
}
