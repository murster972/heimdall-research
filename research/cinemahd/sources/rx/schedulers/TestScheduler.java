package rx.schedulers;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import rx.Scheduler;
import rx.functions.Action0;
import rx.subscriptions.BooleanSubscription;

public class TestScheduler extends Scheduler {

    /* renamed from: a  reason: collision with root package name */
    private final Queue<TimedAction> f7061a = new PriorityQueue(11, new CompareActionsByTime());
    private long b;

    private static class CompareActionsByTime implements Comparator<TimedAction> {
        private CompareActionsByTime() {
        }

        /* renamed from: a */
        public int compare(TimedAction timedAction, TimedAction timedAction2) {
            if (timedAction.f7063a == timedAction2.f7063a) {
                return Long.valueOf(timedAction.d).compareTo(Long.valueOf(timedAction2.d));
            }
            return Long.valueOf(timedAction.f7063a).compareTo(Long.valueOf(timedAction2.f7063a));
        }
    }

    private final class InnerTestScheduler extends Scheduler.Worker {

        /* renamed from: a  reason: collision with root package name */
        private final BooleanSubscription f7062a;

        private InnerTestScheduler(TestScheduler testScheduler) {
            this.f7062a = new BooleanSubscription();
        }

        public boolean isUnsubscribed() {
            return this.f7062a.isUnsubscribed();
        }

        public void unsubscribe() {
            this.f7062a.unsubscribe();
        }
    }

    private static final class TimedAction {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final long f7063a;
        /* access modifiers changed from: private */
        public final Action0 b;
        /* access modifiers changed from: private */
        public final Scheduler.Worker c;
        /* access modifiers changed from: private */
        public final long d;

        public String toString() {
            return String.format("TimedAction(time = %d, action = %s)", new Object[]{Long.valueOf(this.f7063a), this.b.toString()});
        }
    }

    private void a(long j) {
        while (!this.f7061a.isEmpty()) {
            TimedAction peek = this.f7061a.peek();
            if (peek.f7063a > j) {
                break;
            }
            this.b = peek.f7063a == 0 ? this.b : peek.f7063a;
            this.f7061a.remove();
            if (!peek.c.isUnsubscribed()) {
                peek.b.call();
            }
        }
        this.b = j;
    }

    public void advanceTimeBy(long j, TimeUnit timeUnit) {
        advanceTimeTo(this.b + timeUnit.toNanos(j), TimeUnit.NANOSECONDS);
    }

    public void advanceTimeTo(long j, TimeUnit timeUnit) {
        a(timeUnit.toNanos(j));
    }

    public Scheduler.Worker createWorker() {
        return new InnerTestScheduler();
    }

    public long now() {
        return TimeUnit.NANOSECONDS.toMillis(this.b);
    }

    public void triggerActions() {
        a(this.b);
    }
}
