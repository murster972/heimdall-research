package rx.schedulers;

import rx.Scheduler;
import rx.internal.schedulers.NewThreadWorker;
import rx.internal.util.RxThreadFactory;

public final class NewThreadScheduler extends Scheduler {

    /* renamed from: a  reason: collision with root package name */
    private static final RxThreadFactory f7059a = new RxThreadFactory("RxNewThreadScheduler-");
    private static final NewThreadScheduler b = new NewThreadScheduler();

    private NewThreadScheduler() {
    }

    static NewThreadScheduler a() {
        return b;
    }

    public Scheduler.Worker createWorker() {
        return new NewThreadWorker(f7059a);
    }
}
