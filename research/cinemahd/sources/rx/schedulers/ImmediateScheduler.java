package rx.schedulers;

import rx.Scheduler;
import rx.Subscription;
import rx.subscriptions.BooleanSubscription;

public final class ImmediateScheduler extends Scheduler {

    /* renamed from: a  reason: collision with root package name */
    private static final ImmediateScheduler f7057a = new ImmediateScheduler();

    private class InnerImmediateScheduler extends Scheduler.Worker implements Subscription {

        /* renamed from: a  reason: collision with root package name */
        final BooleanSubscription f7058a;

        private InnerImmediateScheduler(ImmediateScheduler immediateScheduler) {
            this.f7058a = new BooleanSubscription();
        }

        public boolean isUnsubscribed() {
            return this.f7058a.isUnsubscribed();
        }

        public void unsubscribe() {
            this.f7058a.unsubscribe();
        }
    }

    ImmediateScheduler() {
    }

    static ImmediateScheduler a() {
        return f7057a;
    }

    public Scheduler.Worker createWorker() {
        return new InnerImmediateScheduler();
    }
}
