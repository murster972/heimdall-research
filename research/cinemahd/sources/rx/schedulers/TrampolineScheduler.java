package rx.schedulers;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import rx.Scheduler;
import rx.Subscription;
import rx.subscriptions.BooleanSubscription;

public final class TrampolineScheduler extends Scheduler {

    /* renamed from: a  reason: collision with root package name */
    private static final TrampolineScheduler f7064a = new TrampolineScheduler();

    private static class InnerCurrentThreadScheduler extends Scheduler.Worker implements Subscription {

        /* renamed from: a  reason: collision with root package name */
        volatile int f7065a;
        private final BooleanSubscription b;

        static {
            AtomicIntegerFieldUpdater.newUpdater(InnerCurrentThreadScheduler.class, "a");
        }

        private InnerCurrentThreadScheduler() {
            new PriorityBlockingQueue();
            this.b = new BooleanSubscription();
            new AtomicInteger();
        }

        public boolean isUnsubscribed() {
            return this.b.isUnsubscribed();
        }

        public void unsubscribe() {
            this.b.unsubscribe();
        }
    }

    TrampolineScheduler() {
    }

    static TrampolineScheduler a() {
        return f7064a;
    }

    public Scheduler.Worker createWorker() {
        return new InnerCurrentThreadScheduler();
    }
}
