package rx.schedulers;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

final class ExecutorScheduler extends Scheduler {

    /* renamed from: a  reason: collision with root package name */
    final Executor f7054a;

    static final class ExecutorAction implements Runnable, Subscription {
        static final AtomicIntegerFieldUpdater<ExecutorAction> d = AtomicIntegerFieldUpdater.newUpdater(ExecutorAction.class, "c");

        /* renamed from: a  reason: collision with root package name */
        final Action0 f7055a;
        final CompositeSubscription b;
        volatile int c;

        public boolean isUnsubscribed() {
            return this.c != 0;
        }

        public void run() {
            if (!isUnsubscribed()) {
                try {
                    this.f7055a.call();
                } catch (Throwable th) {
                    unsubscribe();
                    throw th;
                }
                unsubscribe();
            }
        }

        public void unsubscribe() {
            if (d.compareAndSet(this, 0, 1)) {
                this.b.a((Subscription) this);
            }
        }
    }

    static final class ExecutorSchedulerWorker extends Scheduler.Worker implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final CompositeSubscription f7056a = new CompositeSubscription();
        final ConcurrentLinkedQueue<ExecutorAction> b = new ConcurrentLinkedQueue<>();
        final AtomicInteger c = new AtomicInteger();

        public ExecutorSchedulerWorker(Executor executor) {
        }

        public boolean isUnsubscribed() {
            return this.f7056a.isUnsubscribed();
        }

        public void run() {
            do {
                this.b.poll().run();
            } while (this.c.decrementAndGet() > 0);
        }

        public void unsubscribe() {
            this.f7056a.unsubscribe();
        }
    }

    public ExecutorScheduler(Executor executor) {
        this.f7054a = executor;
    }

    public Scheduler.Worker createWorker() {
        return new ExecutorSchedulerWorker(this.f7054a);
    }
}
