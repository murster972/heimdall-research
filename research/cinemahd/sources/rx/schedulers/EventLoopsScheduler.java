package rx.schedulers;

import java.util.concurrent.ThreadFactory;
import rx.Scheduler;
import rx.internal.schedulers.NewThreadWorker;
import rx.internal.util.RxThreadFactory;
import rx.subscriptions.CompositeSubscription;

class EventLoopsScheduler extends Scheduler {
    /* access modifiers changed from: private */
    public static final RxThreadFactory b = new RxThreadFactory("RxComputationThreadPool-");
    static final int c;

    /* renamed from: a  reason: collision with root package name */
    final FixedSchedulerPool f7051a = new FixedSchedulerPool();

    private static class EventLoopWorker extends Scheduler.Worker {

        /* renamed from: a  reason: collision with root package name */
        private final CompositeSubscription f7052a = new CompositeSubscription();

        EventLoopWorker(PoolWorker poolWorker) {
        }

        public boolean isUnsubscribed() {
            return this.f7052a.isUnsubscribed();
        }

        public void unsubscribe() {
            this.f7052a.unsubscribe();
        }
    }

    static final class FixedSchedulerPool {

        /* renamed from: a  reason: collision with root package name */
        final int f7053a = EventLoopsScheduler.c;
        final PoolWorker[] b = new PoolWorker[this.f7053a];
        long c;

        FixedSchedulerPool() {
            for (int i = 0; i < this.f7053a; i++) {
                this.b[i] = new PoolWorker(EventLoopsScheduler.b);
            }
        }

        public PoolWorker a() {
            PoolWorker[] poolWorkerArr = this.b;
            long j = this.c;
            this.c = 1 + j;
            return poolWorkerArr[(int) (j % ((long) this.f7053a))];
        }
    }

    private static final class PoolWorker extends NewThreadWorker {
        PoolWorker(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }

    static {
        int intValue = Integer.getInteger("rx.scheduler.max-computation-threads", 0).intValue();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        if (intValue <= 0 || intValue > availableProcessors) {
            intValue = availableProcessors;
        }
        c = intValue;
    }

    EventLoopsScheduler() {
    }

    public Scheduler.Worker createWorker() {
        return new EventLoopWorker(this.f7051a.a());
    }
}
