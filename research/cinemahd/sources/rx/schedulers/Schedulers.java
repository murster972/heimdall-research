package rx.schedulers;

import java.util.concurrent.Executor;
import rx.Scheduler;
import rx.plugins.RxJavaPlugins;

public final class Schedulers {
    private static final Schedulers d = new Schedulers();

    /* renamed from: a  reason: collision with root package name */
    private final Scheduler f7060a;
    private final Scheduler b;
    private final Scheduler c;

    private Schedulers() {
        Scheduler a2 = RxJavaPlugins.d().c().a();
        if (a2 != null) {
            this.f7060a = a2;
        } else {
            this.f7060a = new EventLoopsScheduler();
        }
        Scheduler b2 = RxJavaPlugins.d().c().b();
        if (b2 != null) {
            this.b = b2;
        } else {
            this.b = new CachedThreadScheduler();
        }
        Scheduler c2 = RxJavaPlugins.d().c().c();
        if (c2 != null) {
            this.c = c2;
        } else {
            this.c = NewThreadScheduler.a();
        }
    }

    public static Scheduler computation() {
        return d.f7060a;
    }

    public static Scheduler from(Executor executor) {
        return new ExecutorScheduler(executor);
    }

    public static Scheduler immediate() {
        return ImmediateScheduler.a();
    }

    public static Scheduler io() {
        return d.b;
    }

    public static Scheduler newThread() {
        return d.c;
    }

    public static TestScheduler test() {
        return new TestScheduler();
    }

    public static Scheduler trampoline() {
        return TrampolineScheduler.a();
    }
}
