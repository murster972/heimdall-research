package rx.subjects;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import rx.Observable;
import rx.Observer;
import rx.functions.Action1;
import rx.functions.Actions;
import rx.internal.operators.NotificationLite;

final class SubjectSubscriptionManager<T> implements Observable.OnSubscribe<T> {

    /* renamed from: a  reason: collision with root package name */
    volatile State<T> f7066a = State.c;
    volatile Object b;
    Action1<SubjectObserver<T>> c;

    protected static final class State<T> {
        static final SubjectObserver[] b = new SubjectObserver[0];
        static final State c = new State(false, b);

        /* renamed from: a  reason: collision with root package name */
        final SubjectObserver[] f7067a;

        static {
            new State(true, b);
        }

        public State(boolean z, SubjectObserver[] subjectObserverArr) {
            this.f7067a = subjectObserverArr;
        }
    }

    protected static final class SubjectObserver<T> implements Observer<T> {
        public void onNext(T t) {
            throw null;
        }
    }

    static {
        Class<SubjectSubscriptionManager> cls = SubjectSubscriptionManager.class;
        AtomicReferenceFieldUpdater.newUpdater(cls, State.class, "a");
        AtomicReferenceFieldUpdater.newUpdater(cls, Object.class, "b");
    }

    SubjectSubscriptionManager() {
        Actions.a();
        Actions.a();
        Actions.a();
        NotificationLite.a();
    }

    /* access modifiers changed from: package-private */
    public SubjectObserver<T>[] a() {
        return this.f7066a.f7067a;
    }
}
