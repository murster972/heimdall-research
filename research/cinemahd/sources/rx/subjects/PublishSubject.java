package rx.subjects;

import rx.Observable;
import rx.functions.Action1;
import rx.internal.operators.NotificationLite;
import rx.subjects.SubjectSubscriptionManager;

public final class PublishSubject<T> extends Subject<T, T> {
    final SubjectSubscriptionManager<T> b;

    protected PublishSubject(Observable.OnSubscribe<T> onSubscribe, SubjectSubscriptionManager<T> subjectSubscriptionManager) {
        super(onSubscribe);
        NotificationLite.a();
        this.b = subjectSubscriptionManager;
    }

    public static <T> PublishSubject<T> a() {
        SubjectSubscriptionManager subjectSubscriptionManager = new SubjectSubscriptionManager();
        subjectSubscriptionManager.c = new Action1<SubjectSubscriptionManager.SubjectObserver<T>>(subjectSubscriptionManager) {
        };
        return new PublishSubject<>(subjectSubscriptionManager, subjectSubscriptionManager);
    }

    public void onNext(T t) {
        SubjectSubscriptionManager.SubjectObserver[] a2 = this.b.a();
        if (a2.length > 0) {
            a2[0].onNext(t);
            throw null;
        }
    }
}
