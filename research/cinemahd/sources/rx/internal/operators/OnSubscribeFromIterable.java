package rx.internal.operators;

import rx.Observable;

public final class OnSubscribeFromIterable<T> implements Observable.OnSubscribe<T> {
    public OnSubscribeFromIterable(Iterable<? extends T> iterable) {
        if (iterable == null) {
            throw new NullPointerException("iterable must not be null");
        }
    }
}
