package rx.internal.operators;

import java.io.Serializable;

public final class NotificationLite<T> {

    /* renamed from: a  reason: collision with root package name */
    private static final NotificationLite f7039a = new NotificationLite();

    static {
        new Serializable() {
            private static final long serialVersionUID = 1;

            public String toString() {
                return "Notification=>Completed";
            }
        };
        new Serializable() {
            private static final long serialVersionUID = 2;

            public String toString() {
                return "Notification=>NULL";
            }
        };
    }

    private NotificationLite() {
    }

    public static <T> NotificationLite<T> a() {
        return f7039a;
    }
}
