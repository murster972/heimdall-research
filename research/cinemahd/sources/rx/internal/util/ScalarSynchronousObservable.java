package rx.internal.util;

import rx.Observable;

public final class ScalarSynchronousObservable<T> extends Observable<T> {
    protected ScalarSynchronousObservable(T t) {
        super(new Observable.OnSubscribe<T>(t) {
        });
    }

    public static final <T> ScalarSynchronousObservable<T> c(T t) {
        return new ScalarSynchronousObservable<>(t);
    }
}
