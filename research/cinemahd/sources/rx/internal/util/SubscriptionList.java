package rx.internal.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import rx.Subscription;
import rx.exceptions.Exceptions;

public final class SubscriptionList implements Subscription {

    /* renamed from: a  reason: collision with root package name */
    private List<Subscription> f7043a;
    private boolean b = false;

    public void a(Subscription subscription) {
        synchronized (this) {
            if (!this.b) {
                if (this.f7043a == null) {
                    this.f7043a = new LinkedList();
                }
                this.f7043a.add(subscription);
                subscription = null;
            }
        }
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    public synchronized boolean isUnsubscribed() {
        return this.b;
    }

    public void unsubscribe() {
        synchronized (this) {
            if (!this.b) {
                this.b = true;
                List<Subscription> list = this.f7043a;
                this.f7043a = null;
                a((Collection<Subscription>) list);
            }
        }
    }

    private static void a(Collection<Subscription> collection) {
        if (collection != null) {
            ArrayList arrayList = null;
            for (Subscription unsubscribe : collection) {
                try {
                    unsubscribe.unsubscribe();
                } catch (Throwable th) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(th);
                }
            }
            Exceptions.a((List<? extends Throwable>) arrayList);
        }
    }
}
