package rx.internal.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;

public final class RxThreadFactory implements ThreadFactory {
    static final AtomicLongFieldUpdater<RxThreadFactory> c = AtomicLongFieldUpdater.newUpdater(RxThreadFactory.class, "b");

    /* renamed from: a  reason: collision with root package name */
    final String f7042a;
    volatile long b;

    public RxThreadFactory(String str) {
        this.f7042a = str;
    }

    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, this.f7042a + c.incrementAndGet(this));
        thread.setDaemon(true);
        return thread;
    }
}
