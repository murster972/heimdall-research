package rx.internal.schedulers;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.plugins.RxJavaPlugins;

public class NewThreadWorker extends Scheduler.Worker implements Subscription {
    private static final boolean c = Boolean.getBoolean("rx.scheduler.jdk6.purge-force");
    public static final int d = Integer.getInteger("rx.scheduler.jdk6.purge-frequency-millis", 1000).intValue();
    private static final ConcurrentHashMap<ScheduledThreadPoolExecutor, ScheduledThreadPoolExecutor> e = new ConcurrentHashMap<>();
    private static final AtomicReference<ScheduledExecutorService> f = new AtomicReference<>();

    /* renamed from: a  reason: collision with root package name */
    private final ScheduledExecutorService f7041a;
    volatile boolean b;

    public NewThreadWorker(ThreadFactory threadFactory) {
        ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(1, threadFactory);
        if (!b(newScheduledThreadPool) && (newScheduledThreadPool instanceof ScheduledThreadPoolExecutor)) {
            a((ScheduledThreadPoolExecutor) newScheduledThreadPool);
        }
        RxJavaPlugins.d().c();
        this.f7041a = newScheduledThreadPool;
    }

    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP_START, MTH_ENTER_BLOCK] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.util.concurrent.ScheduledThreadPoolExecutor r10) {
        /*
        L_0x0000:
            java.util.concurrent.atomic.AtomicReference<java.util.concurrent.ScheduledExecutorService> r0 = f
            java.lang.Object r0 = r0.get()
            java.util.concurrent.ScheduledExecutorService r0 = (java.util.concurrent.ScheduledExecutorService) r0
            if (r0 == 0) goto L_0x000b
            goto L_0x002e
        L_0x000b:
            r0 = 1
            rx.internal.util.RxThreadFactory r1 = new rx.internal.util.RxThreadFactory
            java.lang.String r2 = "RxSchedulerPurge-"
            r1.<init>(r2)
            java.util.concurrent.ScheduledExecutorService r3 = java.util.concurrent.Executors.newScheduledThreadPool(r0, r1)
            java.util.concurrent.atomic.AtomicReference<java.util.concurrent.ScheduledExecutorService> r0 = f
            r1 = 0
            boolean r0 = r0.compareAndSet(r1, r3)
            if (r0 == 0) goto L_0x0000
            rx.internal.schedulers.NewThreadWorker$1 r4 = new rx.internal.schedulers.NewThreadWorker$1
            r4.<init>()
            int r0 = d
            long r5 = (long) r0
            long r7 = (long) r0
            java.util.concurrent.TimeUnit r9 = java.util.concurrent.TimeUnit.MILLISECONDS
            r3.scheduleAtFixedRate(r4, r5, r7, r9)
        L_0x002e:
            java.util.concurrent.ConcurrentHashMap<java.util.concurrent.ScheduledThreadPoolExecutor, java.util.concurrent.ScheduledThreadPoolExecutor> r0 = e
            r0.putIfAbsent(r10, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: rx.internal.schedulers.NewThreadWorker.a(java.util.concurrent.ScheduledThreadPoolExecutor):void");
    }

    public static boolean b(ScheduledExecutorService scheduledExecutorService) {
        if (!c) {
            for (Method method : scheduledExecutorService.getClass().getMethods()) {
                if (method.getName().equals("setRemoveOnCancelPolicy") && method.getParameterTypes().length == 1 && method.getParameterTypes()[0] == Boolean.TYPE) {
                    try {
                        method.invoke(scheduledExecutorService, new Object[]{true});
                        return true;
                    } catch (Exception e2) {
                        RxJavaPlugins.d().a().a(e2);
                    }
                }
            }
        }
        return false;
    }

    public boolean isUnsubscribed() {
        return this.b;
    }

    public void unsubscribe() {
        this.b = true;
        this.f7041a.shutdownNow();
        a(this.f7041a);
    }

    public static void a(ScheduledExecutorService scheduledExecutorService) {
        e.remove(scheduledExecutorService);
    }

    static void a() {
        try {
            Iterator<ScheduledThreadPoolExecutor> it2 = e.keySet().iterator();
            while (it2.hasNext()) {
                ScheduledThreadPoolExecutor next = it2.next();
                if (!next.isShutdown()) {
                    next.purge();
                } else {
                    it2.remove();
                }
            }
        } catch (Throwable th) {
            Exceptions.a(th);
            RxJavaPlugins.d().a().a(th);
        }
    }
}
