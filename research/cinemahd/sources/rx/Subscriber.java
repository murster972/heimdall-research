package rx;

import com.facebook.common.time.Clock;
import rx.internal.util.SubscriptionList;

public abstract class Subscriber<T> implements Observer<T>, Subscription {
    private final SubscriptionList cs;
    private final Subscriber<?> op;
    private Producer p;
    private long requested;

    protected Subscriber() {
        this((Subscriber<?>) null, false);
    }

    public final void add(Subscription subscription) {
        this.cs.a(subscription);
    }

    public final boolean isUnsubscribed() {
        return this.cs.isUnsubscribed();
    }

    public void onStart() {
    }

    /* access modifiers changed from: protected */
    public final void request(long j) {
        if (j >= 0) {
            Producer producer = null;
            synchronized (this) {
                if (this.p != null) {
                    producer = this.p;
                } else if (this.requested == Long.MIN_VALUE) {
                    this.requested = j;
                } else {
                    long j2 = this.requested + j;
                    if (j2 < 0) {
                        this.requested = Clock.MAX_TIME;
                    } else {
                        this.requested = j2;
                    }
                }
            }
            if (producer != null) {
                producer.request(j);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("number requested cannot be negative: " + j);
    }

    public void setProducer(Producer producer) {
        long j;
        boolean z;
        synchronized (this) {
            j = this.requested;
            this.p = producer;
            z = this.op != null && j == Long.MIN_VALUE;
        }
        if (z) {
            this.op.setProducer(this.p);
        } else if (j == Long.MIN_VALUE) {
            this.p.request(Clock.MAX_TIME);
        } else {
            this.p.request(j);
        }
    }

    public final void unsubscribe() {
        this.cs.unsubscribe();
    }

    protected Subscriber(Subscriber<?> subscriber) {
        this(subscriber, true);
    }

    protected Subscriber(Subscriber<?> subscriber, boolean z) {
        this.requested = Long.MIN_VALUE;
        this.op = subscriber;
        this.cs = (!z || subscriber == null) ? new SubscriptionList() : subscriber.cs;
    }
}
