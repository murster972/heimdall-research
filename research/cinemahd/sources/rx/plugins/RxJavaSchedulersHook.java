package rx.plugins;

import rx.Scheduler;

public class RxJavaSchedulersHook {

    /* renamed from: a  reason: collision with root package name */
    private static final RxJavaSchedulersHook f7046a = new RxJavaSchedulersHook();

    protected RxJavaSchedulersHook() {
    }

    public static RxJavaSchedulersHook d() {
        return f7046a;
    }

    public Scheduler a() {
        return null;
    }

    public Scheduler b() {
        return null;
    }

    public Scheduler c() {
        return null;
    }
}
