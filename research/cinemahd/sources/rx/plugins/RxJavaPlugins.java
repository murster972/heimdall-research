package rx.plugins;

import java.util.concurrent.atomic.AtomicReference;

public class RxJavaPlugins {
    private static final RxJavaPlugins d = new RxJavaPlugins();
    static final RxJavaErrorHandler e = new RxJavaErrorHandler() {
    };

    /* renamed from: a  reason: collision with root package name */
    private final AtomicReference<RxJavaErrorHandler> f7045a = new AtomicReference<>();
    private final AtomicReference<RxJavaObservableExecutionHook> b = new AtomicReference<>();
    private final AtomicReference<RxJavaSchedulersHook> c = new AtomicReference<>();

    RxJavaPlugins() {
    }

    public static RxJavaPlugins d() {
        return d;
    }

    public RxJavaErrorHandler a() {
        if (this.f7045a.get() == null) {
            Object a2 = a(RxJavaErrorHandler.class);
            if (a2 == null) {
                this.f7045a.compareAndSet((Object) null, e);
            } else {
                this.f7045a.compareAndSet((Object) null, (RxJavaErrorHandler) a2);
            }
        }
        return this.f7045a.get();
    }

    public RxJavaObservableExecutionHook b() {
        if (this.b.get() == null) {
            Object a2 = a(RxJavaObservableExecutionHook.class);
            if (a2 == null) {
                this.b.compareAndSet((Object) null, RxJavaObservableExecutionHookDefault.a());
            } else {
                this.b.compareAndSet((Object) null, (RxJavaObservableExecutionHook) a2);
            }
        }
        return this.b.get();
    }

    public RxJavaSchedulersHook c() {
        if (this.c.get() == null) {
            Object a2 = a(RxJavaSchedulersHook.class);
            if (a2 == null) {
                this.c.compareAndSet((Object) null, RxJavaSchedulersHook.d());
            } else {
                this.c.compareAndSet((Object) null, (RxJavaSchedulersHook) a2);
            }
        }
        return this.c.get();
    }

    private static Object a(Class<?> cls) {
        String simpleName = cls.getSimpleName();
        String property = System.getProperty("rxjava.plugin." + simpleName + ".implementation");
        if (property == null) {
            return null;
        }
        try {
            return Class.forName(property).asSubclass(cls).newInstance();
        } catch (ClassCastException unused) {
            throw new RuntimeException(simpleName + " implementation is not an instance of " + simpleName + ": " + property);
        } catch (ClassNotFoundException e2) {
            throw new RuntimeException(simpleName + " implementation class not found: " + property, e2);
        } catch (InstantiationException e3) {
            throw new RuntimeException(simpleName + " implementation not able to be instantiated: " + property, e3);
        } catch (IllegalAccessException e4) {
            throw new RuntimeException(simpleName + " implementation not able to be accessed: " + property, e4);
        }
    }
}
