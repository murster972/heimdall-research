package okhttp3;

import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Interceptor;

public final class OkHttpClient$Builder$addInterceptor$$inlined$invoke$1 implements Interceptor {
    final /* synthetic */ Function1 $block$inlined;

    public OkHttpClient$Builder$addInterceptor$$inlined$invoke$1(Function1 function1) {
        this.$block$inlined = function1;
    }

    public Response intercept(Interceptor.Chain chain) {
        Intrinsics.b(chain, "chain");
        return (Response) this.$block$inlined.invoke(chain);
    }
}
