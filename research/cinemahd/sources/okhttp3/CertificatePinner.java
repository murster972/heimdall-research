package okhttp3;

import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;
import kotlin.TypeCastException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.TypeIntrinsics;
import okhttp3.internal.HostnamesKt;
import okhttp3.internal.tls.CertificateChainCleaner;
import okio.ByteString;

public final class CertificatePinner {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final CertificatePinner DEFAULT = new Builder().build();
    private final CertificateChainCleaner certificateChainCleaner;
    private final Set<Pin> pins;

    public static final class Builder {
        private final List<Pin> pins = new ArrayList();

        public final Builder add(String str, String... strArr) {
            Intrinsics.b(str, "pattern");
            Intrinsics.b(strArr, "pins");
            for (String pin : strArr) {
                this.pins.add(new Pin(str, pin));
            }
            return this;
        }

        public final CertificatePinner build() {
            return new CertificatePinner(CollectionsKt___CollectionsKt.e(this.pins), (CertificateChainCleaner) null, 2, (DefaultConstructorMarker) null);
        }

        public final List<Pin> getPins() {
            return this.pins;
        }
    }

    public static final class Companion {
        private Companion() {
        }

        public final String pin(Certificate certificate) {
            Intrinsics.b(certificate, "certificate");
            if (certificate instanceof X509Certificate) {
                return "sha256/" + sha256Hash((X509Certificate) certificate).a();
            }
            throw new IllegalArgumentException("Certificate pinning requires X509 certificates".toString());
        }

        public final ByteString sha1Hash(X509Certificate x509Certificate) {
            Intrinsics.b(x509Certificate, "$this$sha1Hash");
            ByteString.Companion companion = ByteString.d;
            PublicKey publicKey = x509Certificate.getPublicKey();
            Intrinsics.a((Object) publicKey, "publicKey");
            byte[] encoded = publicKey.getEncoded();
            Intrinsics.a((Object) encoded, "publicKey.encoded");
            return ByteString.Companion.a(companion, encoded, 0, 0, 3, (Object) null).i();
        }

        public final ByteString sha256Hash(X509Certificate x509Certificate) {
            Intrinsics.b(x509Certificate, "$this$sha256Hash");
            ByteString.Companion companion = ByteString.d;
            PublicKey publicKey = x509Certificate.getPublicKey();
            Intrinsics.a((Object) publicKey, "publicKey");
            byte[] encoded = publicKey.getEncoded();
            Intrinsics.a((Object) encoded, "publicKey.encoded");
            return ByteString.Companion.a(companion, encoded, 0, 0, 3, (Object) null).j();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public static final class Pin {
        private final ByteString hash;
        private final String hashAlgorithm;
        private final String pattern;

        public Pin(String str, String str2) {
            Intrinsics.b(str, "pattern");
            Intrinsics.b(str2, "pin");
            if ((StringsKt__StringsJVMKt.b(str, "*.", false, 2, (Object) null) && StringsKt__StringsKt.a((CharSequence) str, "*", 1, false, 4, (Object) null) == -1) || (StringsKt__StringsJVMKt.b(str, "**.", false, 2, (Object) null) && StringsKt__StringsKt.a((CharSequence) str, "*", 2, false, 4, (Object) null) == -1) || StringsKt__StringsKt.a((CharSequence) str, "*", 0, false, 6, (Object) null) == -1) {
                String canonicalHost = HostnamesKt.toCanonicalHost(str);
                if (canonicalHost != null) {
                    this.pattern = canonicalHost;
                    if (StringsKt__StringsJVMKt.b(str2, "sha1/", false, 2, (Object) null)) {
                        this.hashAlgorithm = "sha1";
                        ByteString.Companion companion = ByteString.d;
                        String substring = str2.substring(5);
                        Intrinsics.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                        ByteString a2 = companion.a(substring);
                        if (a2 != null) {
                            this.hash = a2;
                            return;
                        }
                        throw new IllegalArgumentException("Invalid pin hash: " + str2);
                    } else if (StringsKt__StringsJVMKt.b(str2, "sha256/", false, 2, (Object) null)) {
                        this.hashAlgorithm = "sha256";
                        ByteString.Companion companion2 = ByteString.d;
                        String substring2 = str2.substring(7);
                        Intrinsics.a((Object) substring2, "(this as java.lang.String).substring(startIndex)");
                        ByteString a3 = companion2.a(substring2);
                        if (a3 != null) {
                            this.hash = a3;
                            return;
                        }
                        throw new IllegalArgumentException("Invalid pin hash: " + str2);
                    } else {
                        throw new IllegalArgumentException("pins must start with 'sha256/' or 'sha1/': " + str2);
                    }
                } else {
                    throw new IllegalArgumentException("Invalid pattern: " + str);
                }
            } else {
                throw new IllegalArgumentException(("Unexpected pattern: " + str).toString());
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Pin)) {
                return false;
            }
            Pin pin = (Pin) obj;
            return !(Intrinsics.a((Object) this.pattern, (Object) pin.pattern) ^ true) && !(Intrinsics.a((Object) this.hashAlgorithm, (Object) pin.hashAlgorithm) ^ true) && !(Intrinsics.a((Object) this.hash, (Object) pin.hash) ^ true);
        }

        public final ByteString getHash() {
            return this.hash;
        }

        public final String getHashAlgorithm() {
            return this.hashAlgorithm;
        }

        public final String getPattern() {
            return this.pattern;
        }

        public int hashCode() {
            return (((this.pattern.hashCode() * 31) + this.hashAlgorithm.hashCode()) * 31) + this.hash.hashCode();
        }

        public final boolean matchesCertificate(X509Certificate x509Certificate) {
            Intrinsics.b(x509Certificate, "certificate");
            String str = this.hashAlgorithm;
            int hashCode = str.hashCode();
            if (hashCode != -903629273) {
                if (hashCode == 3528965 && str.equals("sha1")) {
                    return Intrinsics.a((Object) this.hash, (Object) CertificatePinner.Companion.sha1Hash(x509Certificate));
                }
            } else if (str.equals("sha256")) {
                return Intrinsics.a((Object) this.hash, (Object) CertificatePinner.Companion.sha256Hash(x509Certificate));
            }
            return false;
        }

        /* JADX WARNING: Removed duplicated region for block: B:7:0x003f A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean matchesHostname(java.lang.String r14) {
            /*
                r13 = this;
                java.lang.String r0 = "hostname"
                kotlin.jvm.internal.Intrinsics.b(r14, r0)
                java.lang.String r0 = r13.pattern
                r1 = 0
                r2 = 2
                r3 = 0
                java.lang.String r4 = "**."
                boolean r0 = kotlin.text.StringsKt__StringsJVMKt.b(r0, r4, r3, r2, r1)
                r4 = 1
                if (r0 == 0) goto L_0x0041
                java.lang.String r0 = r13.pattern
                int r0 = r0.length()
                int r9 = r0 + -3
                int r0 = r14.length()
                int r0 = r0 - r9
                int r1 = r14.length()
                int r6 = r1 - r9
                java.lang.String r7 = r13.pattern
                r8 = 3
                r10 = 0
                r11 = 16
                r12 = 0
                r5 = r14
                boolean r1 = kotlin.text.StringsKt__StringsJVMKt.a(r5, r6, r7, r8, r9, r10, r11, r12)
                if (r1 == 0) goto L_0x0082
                if (r0 == 0) goto L_0x003f
                int r0 = r0 - r4
                char r14 = r14.charAt(r0)
                r0 = 46
                if (r14 != r0) goto L_0x0082
            L_0x003f:
                r3 = 1
                goto L_0x0082
            L_0x0041:
                java.lang.String r0 = r13.pattern
                java.lang.String r5 = "*."
                boolean r0 = kotlin.text.StringsKt__StringsJVMKt.b(r0, r5, r3, r2, r1)
                if (r0 == 0) goto L_0x007c
                java.lang.String r0 = r13.pattern
                int r0 = r0.length()
                int r9 = r0 + -1
                int r0 = r14.length()
                int r0 = r0 - r9
                int r1 = r14.length()
                int r6 = r1 - r9
                java.lang.String r7 = r13.pattern
                r8 = 1
                r10 = 0
                r11 = 16
                r12 = 0
                r5 = r14
                boolean r1 = kotlin.text.StringsKt__StringsJVMKt.a(r5, r6, r7, r8, r9, r10, r11, r12)
                if (r1 == 0) goto L_0x0082
                r6 = 46
                int r7 = r0 + -1
                r8 = 0
                r9 = 4
                r10 = 0
                r5 = r14
                int r14 = kotlin.text.StringsKt__StringsKt.b((java.lang.CharSequence) r5, (char) r6, (int) r7, (boolean) r8, (int) r9, (java.lang.Object) r10)
                r0 = -1
                if (r14 != r0) goto L_0x0082
                goto L_0x003f
            L_0x007c:
                java.lang.String r0 = r13.pattern
                boolean r3 = kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r14, (java.lang.Object) r0)
            L_0x0082:
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.CertificatePinner.Pin.matchesHostname(java.lang.String):boolean");
        }

        public String toString() {
            return this.hashAlgorithm + '/' + this.hash.a();
        }
    }

    public CertificatePinner(Set<Pin> set, CertificateChainCleaner certificateChainCleaner2) {
        Intrinsics.b(set, "pins");
        this.pins = set;
        this.certificateChainCleaner = certificateChainCleaner2;
    }

    public static final String pin(Certificate certificate) {
        return Companion.pin(certificate);
    }

    public static final ByteString sha1Hash(X509Certificate x509Certificate) {
        return Companion.sha1Hash(x509Certificate);
    }

    public static final ByteString sha256Hash(X509Certificate x509Certificate) {
        return Companion.sha256Hash(x509Certificate);
    }

    public final void check(String str, List<? extends Certificate> list) throws SSLPeerUnverifiedException {
        Intrinsics.b(str, "hostname");
        Intrinsics.b(list, "peerCertificates");
        check$okhttp(str, new CertificatePinner$check$1(this, list, str));
    }

    public final void check$okhttp(String str, Function0<? extends List<? extends X509Certificate>> function0) {
        Pin next;
        Intrinsics.b(str, "hostname");
        Intrinsics.b(function0, "cleanedPeerCertificatesFn");
        List<Pin> findMatchingPins = findMatchingPins(str);
        if (!findMatchingPins.isEmpty()) {
            List<X509Certificate> list = (List) function0.invoke();
            loop0:
            for (X509Certificate x509Certificate : list) {
                Iterator<Pin> it2 = findMatchingPins.iterator();
                ByteString byteString = null;
                ByteString byteString2 = null;
                while (true) {
                    if (it2.hasNext()) {
                        next = it2.next();
                        String hashAlgorithm = next.getHashAlgorithm();
                        int hashCode = hashAlgorithm.hashCode();
                        if (hashCode == -903629273) {
                            if (!hashAlgorithm.equals("sha256")) {
                                break loop0;
                            }
                            if (byteString == null) {
                                byteString = Companion.sha256Hash(x509Certificate);
                            }
                            if (Intrinsics.a((Object) next.getHash(), (Object) byteString)) {
                                return;
                            }
                        } else if (hashCode != 3528965 || !hashAlgorithm.equals("sha1")) {
                            break loop0;
                        } else {
                            if (byteString2 == null) {
                                byteString2 = Companion.sha1Hash(x509Certificate);
                            }
                            if (Intrinsics.a((Object) next.getHash(), (Object) byteString2)) {
                                return;
                            }
                        }
                    }
                }
                throw new AssertionError("unsupported hashAlgorithm: " + next.getHashAlgorithm());
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Certificate pinning failure!");
            sb.append("\n  Peer certificate chain:");
            for (X509Certificate x509Certificate2 : list) {
                sb.append("\n    ");
                sb.append(Companion.pin(x509Certificate2));
                sb.append(": ");
                Principal subjectDN = x509Certificate2.getSubjectDN();
                Intrinsics.a((Object) subjectDN, "element.subjectDN");
                sb.append(subjectDN.getName());
            }
            sb.append("\n  Pinned certificates for ");
            sb.append(str);
            sb.append(":");
            for (Pin append : findMatchingPins) {
                sb.append("\n    ");
                sb.append(append);
            }
            String sb2 = sb.toString();
            Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
            throw new SSLPeerUnverifiedException(sb2);
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof CertificatePinner) {
            CertificatePinner certificatePinner = (CertificatePinner) obj;
            return Intrinsics.a((Object) certificatePinner.pins, (Object) this.pins) && Intrinsics.a((Object) certificatePinner.certificateChainCleaner, (Object) this.certificateChainCleaner);
        }
    }

    public final List<Pin> findMatchingPins(String str) {
        Intrinsics.b(str, "hostname");
        Set<Pin> set = this.pins;
        List<Pin> a2 = CollectionsKt__CollectionsKt.a();
        for (T next : set) {
            if (((Pin) next).matchesHostname(str)) {
                if (a2.isEmpty()) {
                    a2 = new ArrayList<>();
                }
                if (a2 != null) {
                    TypeIntrinsics.a((Object) a2).add(next);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableList<T>");
                }
            }
        }
        return a2;
    }

    public final CertificateChainCleaner getCertificateChainCleaner$okhttp() {
        return this.certificateChainCleaner;
    }

    public final Set<Pin> getPins() {
        return this.pins;
    }

    public int hashCode() {
        int hashCode = (1517 + this.pins.hashCode()) * 41;
        CertificateChainCleaner certificateChainCleaner2 = this.certificateChainCleaner;
        return hashCode + (certificateChainCleaner2 != null ? certificateChainCleaner2.hashCode() : 0);
    }

    public final CertificatePinner withCertificateChainCleaner$okhttp(CertificateChainCleaner certificateChainCleaner2) {
        Intrinsics.b(certificateChainCleaner2, "certificateChainCleaner");
        if (Intrinsics.a((Object) this.certificateChainCleaner, (Object) certificateChainCleaner2)) {
            return this;
        }
        return new CertificatePinner(this.pins, certificateChainCleaner2);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CertificatePinner(Set set, CertificateChainCleaner certificateChainCleaner2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(set, (i & 2) != 0 ? null : certificateChainCleaner2);
    }

    public final void check(String str, Certificate... certificateArr) throws SSLPeerUnverifiedException {
        Intrinsics.b(str, "hostname");
        Intrinsics.b(certificateArr, "peerCertificates");
        check(str, (List<? extends Certificate>) ArraysKt___ArraysKt.f(certificateArr));
    }
}
