package okhttp3;

import com.unity3d.ads.metadata.MediationMetaData;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.TypeCastException;
import kotlin.jvm.internal.ArrayIteratorKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import kotlin.jvm.internal.markers.KMappedMarker;
import kotlin.ranges.IntProgression;
import okhttp3.internal.Util;
import okhttp3.internal.http.DatesKt;

public final class Headers implements Iterable<Pair<? extends String, ? extends String>>, KMappedMarker {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final String[] namesAndValues;

    public static final class Builder {
        private final List<String> namesAndValues = new ArrayList(20);

        public final Builder add(String str) {
            Intrinsics.b(str, "line");
            int a2 = StringsKt__StringsKt.a((CharSequence) str, ':', 0, false, 6, (Object) null);
            if (a2 != -1) {
                String substring = str.substring(0, a2);
                Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                if (substring != null) {
                    String obj = StringsKt__StringsKt.f(substring).toString();
                    String substring2 = str.substring(a2 + 1);
                    Intrinsics.a((Object) substring2, "(this as java.lang.String).substring(startIndex)");
                    add(obj, substring2);
                    return this;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            }
            throw new IllegalArgumentException(("Unexpected header: " + str).toString());
        }

        public final Builder addAll(Headers headers) {
            Intrinsics.b(headers, "headers");
            int size = headers.size();
            for (int i = 0; i < size; i++) {
                addLenient$okhttp(headers.name(i), headers.value(i));
            }
            return this;
        }

        public final Builder addLenient$okhttp(String str) {
            Intrinsics.b(str, "line");
            int a2 = StringsKt__StringsKt.a((CharSequence) str, ':', 1, false, 4, (Object) null);
            if (a2 != -1) {
                String substring = str.substring(0, a2);
                Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                String substring2 = str.substring(a2 + 1);
                Intrinsics.a((Object) substring2, "(this as java.lang.String).substring(startIndex)");
                addLenient$okhttp(substring, substring2);
            } else if (str.charAt(0) == ':') {
                String substring3 = str.substring(1);
                Intrinsics.a((Object) substring3, "(this as java.lang.String).substring(startIndex)");
                addLenient$okhttp("", substring3);
            } else {
                addLenient$okhttp("", str);
            }
            return this;
        }

        public final Builder addUnsafeNonAscii(String str, String str2) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(str2, "value");
            Headers.Companion.checkName(str);
            addLenient$okhttp(str, str2);
            return this;
        }

        public final Headers build() {
            Object[] array = this.namesAndValues.toArray(new String[0]);
            if (array != null) {
                return new Headers((String[]) array, (DefaultConstructorMarker) null);
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        public final String get(String str) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            IntProgression a2 = RangesKt___RangesKt.a(RangesKt___RangesKt.c(this.namesAndValues.size() - 2, 0), 2);
            int first = a2.getFirst();
            int last = a2.getLast();
            int a3 = a2.a();
            if (a3 >= 0) {
                if (first > last) {
                    return null;
                }
            } else if (first < last) {
                return null;
            }
            while (!StringsKt__StringsJVMKt.b(str, this.namesAndValues.get(first), true)) {
                if (first == last) {
                    return null;
                }
                first += a3;
            }
            return this.namesAndValues.get(first + 1);
        }

        public final List<String> getNamesAndValues$okhttp() {
            return this.namesAndValues;
        }

        public final Builder removeAll(String str) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            int i = 0;
            while (i < this.namesAndValues.size()) {
                if (StringsKt__StringsJVMKt.b(str, this.namesAndValues.get(i), true)) {
                    this.namesAndValues.remove(i);
                    this.namesAndValues.remove(i);
                    i -= 2;
                }
                i += 2;
            }
            return this;
        }

        public final Builder set(String str, Date date) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(date, "value");
            set(str, DatesKt.toHttpDateString(date));
            return this;
        }

        public final Builder set(String str, Instant instant) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(instant, "value");
            return set(str, new Date(instant.toEpochMilli()));
        }

        public final Builder set(String str, String str2) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(str2, "value");
            Headers.Companion.checkName(str);
            Headers.Companion.checkValue(str2, str);
            removeAll(str);
            addLenient$okhttp(str, str2);
            return this;
        }

        public final Builder add(String str, String str2) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(str2, "value");
            Headers.Companion.checkName(str);
            Headers.Companion.checkValue(str2, str);
            addLenient$okhttp(str, str2);
            return this;
        }

        public final Builder addLenient$okhttp(String str, String str2) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(str2, "value");
            this.namesAndValues.add(str);
            this.namesAndValues.add(StringsKt__StringsKt.f(str2).toString());
            return this;
        }

        public final Builder add(String str, Date date) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(date, "value");
            add(str, DatesKt.toHttpDateString(date));
            return this;
        }

        public final Builder add(String str, Instant instant) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(instant, "value");
            add(str, new Date(instant.toEpochMilli()));
            return this;
        }
    }

    public static final class Companion {
        private Companion() {
        }

        /* access modifiers changed from: private */
        public final void checkName(String str) {
            if (str.length() > 0) {
                int length = str.length();
                int i = 0;
                while (i < length) {
                    char charAt = str.charAt(i);
                    if ('!' <= charAt && '~' >= charAt) {
                        i++;
                    } else {
                        throw new IllegalArgumentException(Util.format("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), str).toString());
                    }
                }
                return;
            }
            throw new IllegalArgumentException("name is empty".toString());
        }

        /* access modifiers changed from: private */
        public final void checkValue(String str, String str2) {
            int length = str.length();
            int i = 0;
            while (i < length) {
                char charAt = str.charAt(i);
                if (charAt == 9 || (' ' <= charAt && '~' >= charAt)) {
                    i++;
                } else {
                    throw new IllegalArgumentException(Util.format("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt), Integer.valueOf(i), str2, str).toString());
                }
            }
        }

        /* access modifiers changed from: private */
        public final String get(String[] strArr, String str) {
            IntProgression a2 = RangesKt___RangesKt.a(RangesKt___RangesKt.c(strArr.length - 2, 0), 2);
            int first = a2.getFirst();
            int last = a2.getLast();
            int a3 = a2.a();
            if (a3 >= 0) {
                if (first > last) {
                    return null;
                }
            } else if (first < last) {
                return null;
            }
            while (!StringsKt__StringsJVMKt.b(str, strArr[first], true)) {
                if (first == last) {
                    return null;
                }
                first += a3;
            }
            return strArr[first + 1];
        }

        /* renamed from: -deprecated_of  reason: not valid java name */
        public final Headers m430deprecated_of(String... strArr) {
            Intrinsics.b(strArr, "namesAndValues");
            return of((String[]) Arrays.copyOf(strArr, strArr.length));
        }

        public final Headers of(String... strArr) {
            Intrinsics.b(strArr, "namesAndValues");
            if (strArr.length % 2 == 0) {
                Object clone = strArr.clone();
                if (clone != null) {
                    String[] strArr2 = (String[]) clone;
                    int length = strArr2.length;
                    int i = 0;
                    while (i < length) {
                        if (strArr2[i] != null) {
                            String str = strArr2[i];
                            if (str != null) {
                                strArr2[i] = StringsKt__StringsKt.f(str).toString();
                                i++;
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }
                        } else {
                            throw new IllegalArgumentException("Headers cannot be null".toString());
                        }
                    }
                    IntProgression a2 = RangesKt___RangesKt.a((IntProgression) RangesKt___RangesKt.d(0, strArr2.length), 2);
                    int first = a2.getFirst();
                    int last = a2.getLast();
                    int a3 = a2.a();
                    if (a3 < 0 ? first >= last : first <= last) {
                        while (true) {
                            String str2 = strArr2[first];
                            String str3 = strArr2[first + 1];
                            checkName(str2);
                            checkValue(str3, str2);
                            if (first == last) {
                                break;
                            }
                            first += a3;
                        }
                    }
                    return new Headers(strArr2, (DefaultConstructorMarker) null);
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.String>");
            }
            throw new IllegalArgumentException("Expected alternating header names and values".toString());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* renamed from: -deprecated_of  reason: not valid java name */
        public final Headers m429deprecated_of(Map<String, String> map) {
            Intrinsics.b(map, "headers");
            return of(map);
        }

        public final Headers of(Map<String, String> map) {
            Intrinsics.b(map, "$this$toHeaders");
            String[] strArr = new String[(map.size() * 2)];
            int i = 0;
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                String str2 = (String) next.getValue();
                if (str != null) {
                    String obj = StringsKt__StringsKt.f(str).toString();
                    if (str2 != null) {
                        String obj2 = StringsKt__StringsKt.f(str2).toString();
                        checkName(obj);
                        checkValue(obj2, obj);
                        strArr[i] = obj;
                        strArr[i + 1] = obj2;
                        i += 2;
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
            return new Headers(strArr, (DefaultConstructorMarker) null);
        }
    }

    private Headers(String[] strArr) {
        this.namesAndValues = strArr;
    }

    public static final Headers of(Map<String, String> map) {
        return Companion.of(map);
    }

    public static final Headers of(String... strArr) {
        return Companion.of(strArr);
    }

    /* renamed from: -deprecated_size  reason: not valid java name */
    public final int m428deprecated_size() {
        return size();
    }

    public final long byteCount() {
        String[] strArr = this.namesAndValues;
        long length = (long) (strArr.length * 2);
        int length2 = strArr.length;
        for (int i = 0; i < length2; i++) {
            length += (long) this.namesAndValues[i].length();
        }
        return length;
    }

    public boolean equals(Object obj) {
        return (obj instanceof Headers) && Arrays.equals(this.namesAndValues, ((Headers) obj).namesAndValues);
    }

    public final String get(String str) {
        Intrinsics.b(str, MediationMetaData.KEY_NAME);
        return Companion.get(this.namesAndValues, str);
    }

    public final Date getDate(String str) {
        Intrinsics.b(str, MediationMetaData.KEY_NAME);
        String str2 = get(str);
        if (str2 != null) {
            return DatesKt.toHttpDateOrNull(str2);
        }
        return null;
    }

    public final Instant getInstant(String str) {
        Intrinsics.b(str, MediationMetaData.KEY_NAME);
        Date date = getDate(str);
        if (date != null) {
            return date.toInstant();
        }
        return null;
    }

    public int hashCode() {
        return Arrays.hashCode(this.namesAndValues);
    }

    public Iterator<Pair<String, String>> iterator() {
        int size = size();
        Pair[] pairArr = new Pair[size];
        for (int i = 0; i < size; i++) {
            pairArr[i] = TuplesKt.a(name(i), value(i));
        }
        return ArrayIteratorKt.a(pairArr);
    }

    public final String name(int i) {
        return this.namesAndValues[i * 2];
    }

    public final Set<String> names() {
        TreeSet treeSet = new TreeSet(StringsKt__StringsJVMKt.a(StringCompanionObject.f6941a));
        int size = size();
        for (int i = 0; i < size; i++) {
            treeSet.add(name(i));
        }
        Set<String> unmodifiableSet = Collections.unmodifiableSet(treeSet);
        Intrinsics.a((Object) unmodifiableSet, "Collections.unmodifiableSet(result)");
        return unmodifiableSet;
    }

    public final Builder newBuilder() {
        Builder builder = new Builder();
        boolean unused = CollectionsKt__MutableCollectionsKt.a(builder.getNamesAndValues$okhttp(), (T[]) this.namesAndValues);
        return builder;
    }

    public final int size() {
        return this.namesAndValues.length / 2;
    }

    public final Map<String, List<String>> toMultimap() {
        TreeMap treeMap = new TreeMap(StringsKt__StringsJVMKt.a(StringCompanionObject.f6941a));
        int size = size();
        int i = 0;
        while (i < size) {
            String name = name(i);
            Locale locale = Locale.US;
            Intrinsics.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                Intrinsics.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                List list = (List) treeMap.get(lowerCase);
                if (list == null) {
                    list = new ArrayList(2);
                    treeMap.put(lowerCase, list);
                }
                list.add(value(i));
                i++;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        return treeMap;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int size = size();
        for (int i = 0; i < size; i++) {
            sb.append(name(i));
            sb.append(": ");
            sb.append(value(i));
            sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }
        String sb2 = sb.toString();
        Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public final String value(int i) {
        return this.namesAndValues[(i * 2) + 1];
    }

    public final List<String> values(String str) {
        Intrinsics.b(str, MediationMetaData.KEY_NAME);
        int size = size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            if (StringsKt__StringsJVMKt.b(str, name(i), true)) {
                if (arrayList == null) {
                    arrayList = new ArrayList(2);
                }
                arrayList.add(value(i));
            }
        }
        if (arrayList == null) {
            return CollectionsKt__CollectionsKt.a();
        }
        List<String> unmodifiableList = Collections.unmodifiableList(arrayList);
        Intrinsics.a((Object) unmodifiableList, "Collections.unmodifiableList(result)");
        return unmodifiableList;
    }

    public /* synthetic */ Headers(String[] strArr, DefaultConstructorMarker defaultConstructorMarker) {
        this(strArr);
    }
}
