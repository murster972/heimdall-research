package okhttp3;

import kotlin.jvm.internal.Intrinsics;
import okio.ByteString;

public abstract class WebSocketListener {
    public void onClosed(WebSocket webSocket, int i, String str) {
        Intrinsics.b(webSocket, "webSocket");
        Intrinsics.b(str, "reason");
    }

    public void onClosing(WebSocket webSocket, int i, String str) {
        Intrinsics.b(webSocket, "webSocket");
        Intrinsics.b(str, "reason");
    }

    public void onFailure(WebSocket webSocket, Throwable th, Response response) {
        Intrinsics.b(webSocket, "webSocket");
        Intrinsics.b(th, "t");
    }

    public void onMessage(WebSocket webSocket, String str) {
        Intrinsics.b(webSocket, "webSocket");
        Intrinsics.b(str, "text");
    }

    public void onMessage(WebSocket webSocket, ByteString byteString) {
        Intrinsics.b(webSocket, "webSocket");
        Intrinsics.b(byteString, "bytes");
    }

    public void onOpen(WebSocket webSocket, Response response) {
        Intrinsics.b(webSocket, "webSocket");
        Intrinsics.b(response, "response");
    }
}
