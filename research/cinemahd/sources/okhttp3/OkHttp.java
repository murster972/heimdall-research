package okhttp3;

public final class OkHttp {
    public static final OkHttp INSTANCE = new OkHttp();
    public static final String VERSION = "4.7.2";

    private OkHttp() {
    }
}
