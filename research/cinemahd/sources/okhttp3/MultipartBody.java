package okhttp3;

import com.unity3d.ads.metadata.MediationMetaData;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.ByteString;

public final class MultipartBody extends RequestBody {
    public static final MediaType ALTERNATIVE = MediaType.Companion.get("multipart/alternative");
    private static final byte[] COLONSPACE = {(byte) 58, (byte) 32};
    private static final byte[] CRLF = {(byte) 13, (byte) 10};
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final byte[] DASHDASH;
    public static final MediaType DIGEST = MediaType.Companion.get("multipart/digest");
    public static final MediaType FORM = MediaType.Companion.get("multipart/form-data");
    public static final MediaType MIXED = MediaType.Companion.get("multipart/mixed");
    public static final MediaType PARALLEL = MediaType.Companion.get("multipart/parallel");
    private final ByteString boundaryByteString;
    private long contentLength = -1;
    private final MediaType contentType;
    private final List<Part> parts;
    private final MediaType type;

    public static final class Builder {
        private final ByteString boundary;
        private final List<Part> parts;
        private MediaType type;

        public Builder() {
            this((String) null, 1, (DefaultConstructorMarker) null);
        }

        public Builder(String str) {
            Intrinsics.b(str, "boundary");
            this.boundary = ByteString.d.c(str);
            this.type = MultipartBody.MIXED;
            this.parts = new ArrayList();
        }

        public final Builder addFormDataPart(String str, String str2) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(str2, "value");
            addPart(Part.Companion.createFormData(str, str2));
            return this;
        }

        public final Builder addPart(RequestBody requestBody) {
            Intrinsics.b(requestBody, "body");
            addPart(Part.Companion.create(requestBody));
            return this;
        }

        public final MultipartBody build() {
            if (!this.parts.isEmpty()) {
                return new MultipartBody(this.boundary, this.type, Util.toImmutableList(this.parts));
            }
            throw new IllegalStateException("Multipart body must have at least one part.".toString());
        }

        public final Builder setType(MediaType mediaType) {
            Intrinsics.b(mediaType, "type");
            if (Intrinsics.a((Object) mediaType.type(), (Object) "multipart")) {
                this.type = mediaType;
                return this;
            }
            throw new IllegalArgumentException(("multipart != " + mediaType).toString());
        }

        public final Builder addFormDataPart(String str, String str2, RequestBody requestBody) {
            Intrinsics.b(str, MediationMetaData.KEY_NAME);
            Intrinsics.b(requestBody, "body");
            addPart(Part.Companion.createFormData(str, str2, requestBody));
            return this;
        }

        public final Builder addPart(Headers headers, RequestBody requestBody) {
            Intrinsics.b(requestBody, "body");
            addPart(Part.Companion.create(headers, requestBody));
            return this;
        }

        public final Builder addPart(Part part) {
            Intrinsics.b(part, "part");
            this.parts.add(part);
            return this;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ Builder(java.lang.String r1, int r2, kotlin.jvm.internal.DefaultConstructorMarker r3) {
            /*
                r0 = this;
                r2 = r2 & 1
                if (r2 == 0) goto L_0x0011
                java.util.UUID r1 = java.util.UUID.randomUUID()
                java.lang.String r1 = r1.toString()
                java.lang.String r2 = "UUID.randomUUID().toString()"
                kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r1, (java.lang.String) r2)
            L_0x0011:
                r0.<init>(r1)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.MultipartBody.Builder.<init>(java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }
    }

    public static final class Companion {
        private Companion() {
        }

        public final void appendQuotedString$okhttp(StringBuilder sb, String str) {
            Intrinsics.b(sb, "$this$appendQuotedString");
            Intrinsics.b(str, "key");
            sb.append('\"');
            int length = str.length();
            for (int i = 0; i < length; i++) {
                char charAt = str.charAt(i);
                if (charAt == 10) {
                    sb.append("%0A");
                } else if (charAt == 13) {
                    sb.append("%0D");
                } else if (charAt == '\"') {
                    sb.append("%22");
                } else {
                    sb.append(charAt);
                }
            }
            sb.append('\"');
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public static final class Part {
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private final RequestBody body;
        private final Headers headers;

        public static final class Companion {
            private Companion() {
            }

            public final Part create(RequestBody requestBody) {
                Intrinsics.b(requestBody, "body");
                return create((Headers) null, requestBody);
            }

            public final Part createFormData(String str, String str2) {
                Intrinsics.b(str, MediationMetaData.KEY_NAME);
                Intrinsics.b(str2, "value");
                return createFormData(str, (String) null, RequestBody.Companion.create$default(RequestBody.Companion, str2, (MediaType) null, 1, (Object) null));
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final Part create(Headers headers, RequestBody requestBody) {
                Intrinsics.b(requestBody, "body");
                boolean z = true;
                if ((headers != null ? headers.get(TraktV2.HEADER_CONTENT_TYPE) : null) == null) {
                    if ((headers != null ? headers.get("Content-Length") : null) != null) {
                        z = false;
                    }
                    if (z) {
                        return new Part(headers, requestBody, (DefaultConstructorMarker) null);
                    }
                    throw new IllegalArgumentException("Unexpected header: Content-Length".toString());
                }
                throw new IllegalArgumentException("Unexpected header: Content-Type".toString());
            }

            public final Part createFormData(String str, String str2, RequestBody requestBody) {
                Intrinsics.b(str, MediationMetaData.KEY_NAME);
                Intrinsics.b(requestBody, "body");
                StringBuilder sb = new StringBuilder();
                sb.append("form-data; name=");
                MultipartBody.Companion.appendQuotedString$okhttp(sb, str);
                if (str2 != null) {
                    sb.append("; filename=");
                    MultipartBody.Companion.appendQuotedString$okhttp(sb, str2);
                }
                String sb2 = sb.toString();
                Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
                return create(new Headers.Builder().addUnsafeNonAscii("Content-Disposition", sb2).build(), requestBody);
            }
        }

        private Part(Headers headers2, RequestBody requestBody) {
            this.headers = headers2;
            this.body = requestBody;
        }

        public static final Part create(Headers headers2, RequestBody requestBody) {
            return Companion.create(headers2, requestBody);
        }

        public static final Part create(RequestBody requestBody) {
            return Companion.create(requestBody);
        }

        public static final Part createFormData(String str, String str2) {
            return Companion.createFormData(str, str2);
        }

        public static final Part createFormData(String str, String str2, RequestBody requestBody) {
            return Companion.createFormData(str, str2, requestBody);
        }

        /* renamed from: -deprecated_body  reason: not valid java name */
        public final RequestBody m462deprecated_body() {
            return this.body;
        }

        /* renamed from: -deprecated_headers  reason: not valid java name */
        public final Headers m463deprecated_headers() {
            return this.headers;
        }

        public final RequestBody body() {
            return this.body;
        }

        public final Headers headers() {
            return this.headers;
        }

        public /* synthetic */ Part(Headers headers2, RequestBody requestBody, DefaultConstructorMarker defaultConstructorMarker) {
            this(headers2, requestBody);
        }
    }

    static {
        byte b = (byte) 45;
        DASHDASH = new byte[]{b, b};
    }

    public MultipartBody(ByteString byteString, MediaType mediaType, List<Part> list) {
        Intrinsics.b(byteString, "boundaryByteString");
        Intrinsics.b(mediaType, "type");
        Intrinsics.b(list, "parts");
        this.boundaryByteString = byteString;
        this.type = mediaType;
        this.parts = list;
        MediaType.Companion companion = MediaType.Companion;
        this.contentType = companion.get(this.type + "; boundary=" + boundary());
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v1, resolved type: okio.BufferedSink} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: okio.Buffer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: okio.Buffer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v3, resolved type: okio.BufferedSink} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: okio.Buffer} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final long writeOrCountBytes(okio.BufferedSink r14, boolean r15) throws java.io.IOException {
        /*
            r13 = this;
            r0 = 0
            if (r15 == 0) goto L_0x000a
            okio.Buffer r14 = new okio.Buffer
            r14.<init>()
            r1 = r14
            goto L_0x000b
        L_0x000a:
            r1 = r0
        L_0x000b:
            java.util.List<okhttp3.MultipartBody$Part> r2 = r13.parts
            int r2 = r2.size()
            r3 = 0
            r4 = 0
            r5 = r4
            r4 = 0
        L_0x0016:
            if (r4 >= r2) goto L_0x00b8
            java.util.List<okhttp3.MultipartBody$Part> r7 = r13.parts
            java.lang.Object r7 = r7.get(r4)
            okhttp3.MultipartBody$Part r7 = (okhttp3.MultipartBody.Part) r7
            okhttp3.Headers r8 = r7.headers()
            okhttp3.RequestBody r7 = r7.body()
            if (r14 == 0) goto L_0x00b4
            byte[] r9 = DASHDASH
            r14.write(r9)
            okio.ByteString r9 = r13.boundaryByteString
            r14.a((okio.ByteString) r9)
            byte[] r9 = CRLF
            r14.write(r9)
            if (r8 == 0) goto L_0x0060
            int r9 = r8.size()
            r10 = 0
        L_0x0040:
            if (r10 >= r9) goto L_0x0060
            java.lang.String r11 = r8.name(r10)
            okio.BufferedSink r11 = r14.a((java.lang.String) r11)
            byte[] r12 = COLONSPACE
            okio.BufferedSink r11 = r11.write(r12)
            java.lang.String r12 = r8.value(r10)
            okio.BufferedSink r11 = r11.a((java.lang.String) r12)
            byte[] r12 = CRLF
            r11.write(r12)
            int r10 = r10 + 1
            goto L_0x0040
        L_0x0060:
            okhttp3.MediaType r8 = r7.contentType()
            if (r8 == 0) goto L_0x0079
            java.lang.String r9 = "Content-Type: "
            okio.BufferedSink r9 = r14.a((java.lang.String) r9)
            java.lang.String r8 = r8.toString()
            okio.BufferedSink r8 = r9.a((java.lang.String) r8)
            byte[] r9 = CRLF
            r8.write(r9)
        L_0x0079:
            long r8 = r7.contentLength()
            r10 = -1
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 == 0) goto L_0x0093
            java.lang.String r10 = "Content-Length: "
            okio.BufferedSink r10 = r14.a((java.lang.String) r10)
            okio.BufferedSink r10 = r10.e(r8)
            byte[] r11 = CRLF
            r10.write(r11)
            goto L_0x009f
        L_0x0093:
            if (r15 == 0) goto L_0x009f
            if (r1 == 0) goto L_0x009b
            r1.o()
            return r10
        L_0x009b:
            kotlin.jvm.internal.Intrinsics.a()
            throw r0
        L_0x009f:
            byte[] r10 = CRLF
            r14.write(r10)
            if (r15 == 0) goto L_0x00a8
            long r5 = r5 + r8
            goto L_0x00ab
        L_0x00a8:
            r7.writeTo(r14)
        L_0x00ab:
            byte[] r7 = CRLF
            r14.write(r7)
            int r4 = r4 + 1
            goto L_0x0016
        L_0x00b4:
            kotlin.jvm.internal.Intrinsics.a()
            throw r0
        L_0x00b8:
            if (r14 == 0) goto L_0x00e0
            byte[] r2 = DASHDASH
            r14.write(r2)
            okio.ByteString r2 = r13.boundaryByteString
            r14.a((okio.ByteString) r2)
            byte[] r2 = DASHDASH
            r14.write(r2)
            byte[] r2 = CRLF
            r14.write(r2)
            if (r15 == 0) goto L_0x00df
            if (r1 == 0) goto L_0x00db
            long r14 = r1.u()
            long r5 = r5 + r14
            r1.o()
            goto L_0x00df
        L_0x00db:
            kotlin.jvm.internal.Intrinsics.a()
            throw r0
        L_0x00df:
            return r5
        L_0x00e0:
            kotlin.jvm.internal.Intrinsics.a()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.MultipartBody.writeOrCountBytes(okio.BufferedSink, boolean):long");
    }

    /* renamed from: -deprecated_boundary  reason: not valid java name */
    public final String m458deprecated_boundary() {
        return boundary();
    }

    /* renamed from: -deprecated_parts  reason: not valid java name */
    public final List<Part> m459deprecated_parts() {
        return this.parts;
    }

    /* renamed from: -deprecated_size  reason: not valid java name */
    public final int m460deprecated_size() {
        return size();
    }

    /* renamed from: -deprecated_type  reason: not valid java name */
    public final MediaType m461deprecated_type() {
        return this.type;
    }

    public final String boundary() {
        return this.boundaryByteString.m();
    }

    public long contentLength() throws IOException {
        long j = this.contentLength;
        if (j != -1) {
            return j;
        }
        long writeOrCountBytes = writeOrCountBytes((BufferedSink) null, true);
        this.contentLength = writeOrCountBytes;
        return writeOrCountBytes;
    }

    public MediaType contentType() {
        return this.contentType;
    }

    public final Part part(int i) {
        return this.parts.get(i);
    }

    public final List<Part> parts() {
        return this.parts;
    }

    public final int size() {
        return this.parts.size();
    }

    public final MediaType type() {
        return this.type;
    }

    public void writeTo(BufferedSink bufferedSink) throws IOException {
        Intrinsics.b(bufferedSink, "sink");
        writeOrCountBytes(bufferedSink, false);
    }
}
