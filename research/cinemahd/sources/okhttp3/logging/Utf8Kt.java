package okhttp3.logging;

import java.io.EOFException;
import kotlin.jvm.internal.Intrinsics;
import okio.Buffer;

public final class Utf8Kt {
    public static final boolean isProbablyUtf8(Buffer buffer) {
        Intrinsics.b(buffer, "$this$isProbablyUtf8");
        try {
            Buffer buffer2 = new Buffer();
            buffer.a(buffer2, 0, RangesKt___RangesKt.a(buffer.u(), 64));
            for (int i = 0; i < 16; i++) {
                if (buffer2.e()) {
                    return true;
                }
                int t = buffer2.t();
                if (Character.isISOControl(t) && !Character.isWhitespace(t)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException unused) {
            return false;
        }
    }
}
