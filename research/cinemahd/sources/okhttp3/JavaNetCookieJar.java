package okhttp3;

import com.vungle.warren.model.ReportDBAdapter;
import java.io.IOException;
import java.net.CookieHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import kotlin.TuplesKt;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Cookie;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.platform.Platform;

public final class JavaNetCookieJar implements CookieJar {
    private final CookieHandler cookieHandler;

    public JavaNetCookieJar(CookieHandler cookieHandler2) {
        Intrinsics.b(cookieHandler2, "cookieHandler");
        this.cookieHandler = cookieHandler2;
    }

    private final List<Cookie> decodeHeaderAsJavaNetCookies(HttpUrl httpUrl, String str) {
        ArrayList arrayList = new ArrayList();
        int length = str.length();
        int i = 0;
        while (i < length) {
            int delimiterOffset = Util.delimiterOffset(str, ";,", i, length);
            int delimiterOffset2 = Util.delimiterOffset(str, '=', i, delimiterOffset);
            String trimSubstring = Util.trimSubstring(str, i, delimiterOffset2);
            if (!StringsKt__StringsJVMKt.b(trimSubstring, "$", false, 2, (Object) null)) {
                String trimSubstring2 = delimiterOffset2 < delimiterOffset ? Util.trimSubstring(str, delimiterOffset2 + 1, delimiterOffset) : "";
                if (StringsKt__StringsJVMKt.b(trimSubstring2, "\"", false, 2, (Object) null) && StringsKt__StringsJVMKt.a(trimSubstring2, "\"", false, 2, (Object) null)) {
                    int length2 = trimSubstring2.length() - 1;
                    if (trimSubstring2 != null) {
                        trimSubstring2 = trimSubstring2.substring(1, length2);
                        Intrinsics.a((Object) trimSubstring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                }
                arrayList.add(new Cookie.Builder().name(trimSubstring).value(trimSubstring2).domain(httpUrl.host()).build());
            }
            i = delimiterOffset + 1;
        }
        return arrayList;
    }

    public List<Cookie> loadForRequest(HttpUrl httpUrl) {
        Intrinsics.b(httpUrl, ReportDBAdapter.ReportColumns.COLUMN_URL);
        ArrayList arrayList = null;
        try {
            Map<String, List<String>> map = this.cookieHandler.get(httpUrl.uri(), MapsKt__MapsKt.a());
            Intrinsics.a((Object) map, "cookieHeaders");
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                List<String> list = (List) next.getValue();
                if (StringsKt__StringsJVMKt.b("Cookie", str, true) || StringsKt__StringsJVMKt.b("Cookie2", str, true)) {
                    Intrinsics.a((Object) list, "value");
                    if (!list.isEmpty()) {
                        for (String str2 : list) {
                            if (arrayList == null) {
                                arrayList = new ArrayList();
                            }
                            Intrinsics.a((Object) str2, "header");
                            arrayList.addAll(decodeHeaderAsJavaNetCookies(httpUrl, str2));
                        }
                    }
                }
            }
            if (arrayList == null) {
                return CollectionsKt__CollectionsKt.a();
            }
            List<Cookie> unmodifiableList = Collections.unmodifiableList(arrayList);
            Intrinsics.a((Object) unmodifiableList, "Collections.unmodifiableList(cookies)");
            return unmodifiableList;
        } catch (IOException e) {
            Platform platform = Platform.Companion.get();
            StringBuilder sb = new StringBuilder();
            sb.append("Loading cookies failed for ");
            HttpUrl resolve = httpUrl.resolve("/...");
            if (resolve != null) {
                sb.append(resolve);
                platform.log(sb.toString(), 5, e);
                return CollectionsKt__CollectionsKt.a();
            }
            Intrinsics.a();
            throw null;
        }
    }

    public void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
        Intrinsics.b(httpUrl, ReportDBAdapter.ReportColumns.COLUMN_URL);
        Intrinsics.b(list, "cookies");
        ArrayList arrayList = new ArrayList();
        for (Cookie cookieToString : list) {
            arrayList.add(Internal.cookieToString(cookieToString, true));
        }
        try {
            this.cookieHandler.put(httpUrl.uri(), MapsKt__MapsJVMKt.a(TuplesKt.a("Set-Cookie", arrayList)));
        } catch (IOException e) {
            Platform platform = Platform.Companion.get();
            StringBuilder sb = new StringBuilder();
            sb.append("Saving cookies failed for ");
            HttpUrl resolve = httpUrl.resolve("/...");
            if (resolve != null) {
                sb.append(resolve);
                platform.log(sb.toString(), 5, e);
                return;
            }
            Intrinsics.a();
            throw null;
        }
    }
}
