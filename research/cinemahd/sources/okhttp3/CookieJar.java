package okhttp3;

import com.vungle.warren.model.ReportDBAdapter;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;

public interface CookieJar {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final CookieJar NO_COOKIES = new Companion.NoCookies();

    public static final class Companion {
        static final /* synthetic */ Companion $$INSTANCE = null;

        private static final class NoCookies implements CookieJar {
            public List<Cookie> loadForRequest(HttpUrl httpUrl) {
                Intrinsics.b(httpUrl, ReportDBAdapter.ReportColumns.COLUMN_URL);
                return CollectionsKt__CollectionsKt.a();
            }

            public void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
                Intrinsics.b(httpUrl, ReportDBAdapter.ReportColumns.COLUMN_URL);
                Intrinsics.b(list, "cookies");
            }
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    List<Cookie> loadForRequest(HttpUrl httpUrl);

    void saveFromResponse(HttpUrl httpUrl, List<Cookie> list);
}
