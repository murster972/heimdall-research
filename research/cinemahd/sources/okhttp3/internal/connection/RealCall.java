package okhttp3.internal.connection;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Address;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.CertificatePinner;
import okhttp3.Dispatcher;
import okhttp3.EventListener;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Util;
import okhttp3.internal.http.ExchangeCodec;
import okhttp3.internal.http.RealInterceptorChain;
import okhttp3.internal.platform.Platform;
import okio.AsyncTimeout;

public final class RealCall implements Call {
    private Object callStackTrace;
    private boolean canceled;
    private final OkHttpClient client;
    private RealConnection connection;
    private final RealConnectionPool connectionPool = this.client.connectionPool().getDelegate$okhttp();
    private final EventListener eventListener = this.client.eventListenerFactory().create(this);
    private Exchange exchange;
    private ExchangeFinder exchangeFinder;
    private boolean exchangeRequestDone;
    private boolean exchangeResponseDone;
    private boolean executed;
    private final boolean forWebSocket;
    private Exchange interceptorScopedExchange;
    private boolean noMoreExchanges;
    private final Request originalRequest;
    /* access modifiers changed from: private */
    public final RealCall$timeout$1 timeout;
    private boolean timeoutEarlyExit;

    public final class AsyncCall implements Runnable {
        private volatile AtomicInteger callsPerHost = new AtomicInteger(0);
        private final Callback responseCallback;
        final /* synthetic */ RealCall this$0;

        public AsyncCall(RealCall realCall, Callback callback) {
            Intrinsics.b(callback, "responseCallback");
            this.this$0 = realCall;
            this.responseCallback = callback;
        }

        public final void executeOn(ExecutorService executorService) {
            Intrinsics.b(executorService, "executorService");
            Dispatcher dispatcher = this.this$0.getClient().dispatcher();
            if (!Util.assertionsEnabled || !Thread.holdsLock(dispatcher)) {
                try {
                    executorService.execute(this);
                } catch (RejectedExecutionException e) {
                    InterruptedIOException interruptedIOException = new InterruptedIOException("executor rejected");
                    interruptedIOException.initCause(e);
                    this.this$0.noMoreExchanges$okhttp(interruptedIOException);
                    this.responseCallback.onFailure(this.this$0, interruptedIOException);
                    this.this$0.getClient().dispatcher().finished$okhttp(this);
                } catch (Throwable th) {
                    this.this$0.getClient().dispatcher().finished$okhttp(this);
                    throw th;
                }
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("Thread ");
                Thread currentThread = Thread.currentThread();
                Intrinsics.a((Object) currentThread, "Thread.currentThread()");
                sb.append(currentThread.getName());
                sb.append(" MUST NOT hold lock on ");
                sb.append(dispatcher);
                throw new AssertionError(sb.toString());
            }
        }

        public final RealCall getCall() {
            return this.this$0;
        }

        public final AtomicInteger getCallsPerHost() {
            return this.callsPerHost;
        }

        public final String getHost() {
            return this.this$0.getOriginalRequest().url().host();
        }

        public final Request getRequest() {
            return this.this$0.getOriginalRequest();
        }

        public final void reuseCallsPerHostFrom(AsyncCall asyncCall) {
            Intrinsics.b(asyncCall, "other");
            this.callsPerHost = asyncCall.callsPerHost;
        }

        /* JADX WARNING: Removed duplicated region for block: B:18:0x005b A[Catch:{ all -> 0x007c, all -> 0x00c9 }] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0083 A[Catch:{ all -> 0x007c, all -> 0x00c9 }] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00a5 A[Catch:{ all -> 0x007c, all -> 0x00c9 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r6 = this;
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "OkHttp "
                r0.append(r1)
                okhttp3.internal.connection.RealCall r1 = r6.this$0
                java.lang.String r1 = r1.redactedUrl$okhttp()
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                java.lang.Thread r1 = java.lang.Thread.currentThread()
                java.lang.String r2 = "currentThread"
                kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r1, (java.lang.String) r2)
                java.lang.String r2 = r1.getName()
                r1.setName(r0)
                r0 = 0
                okhttp3.internal.connection.RealCall r3 = r6.this$0     // Catch:{ all -> 0x00c9 }
                okhttp3.internal.connection.RealCall$timeout$1 r3 = r3.timeout     // Catch:{ all -> 0x00c9 }
                r3.enter()     // Catch:{ all -> 0x00c9 }
                okhttp3.internal.connection.RealCall r3 = r6.this$0     // Catch:{ IOException -> 0x007e, all -> 0x0051 }
                okhttp3.Response r0 = r3.getResponseWithInterceptorChain$okhttp()     // Catch:{ IOException -> 0x007e, all -> 0x0051 }
                r3 = 1
                okhttp3.Callback r4 = r6.responseCallback     // Catch:{ IOException -> 0x004f, all -> 0x004d }
                okhttp3.internal.connection.RealCall r5 = r6.this$0     // Catch:{ IOException -> 0x004f, all -> 0x004d }
                r4.onResponse(r5, r0)     // Catch:{ IOException -> 0x004f, all -> 0x004d }
                okhttp3.internal.connection.RealCall r0 = r6.this$0     // Catch:{ all -> 0x00c9 }
                okhttp3.OkHttpClient r0 = r0.getClient()     // Catch:{ all -> 0x00c9 }
                okhttp3.Dispatcher r0 = r0.dispatcher()     // Catch:{ all -> 0x00c9 }
            L_0x0049:
                r0.finished$okhttp((okhttp3.internal.connection.RealCall.AsyncCall) r6)     // Catch:{ all -> 0x00c9 }
                goto L_0x00b7
            L_0x004d:
                r0 = move-exception
                goto L_0x0054
            L_0x004f:
                r0 = move-exception
                goto L_0x0081
            L_0x0051:
                r3 = move-exception
                r0 = r3
                r3 = 0
            L_0x0054:
                okhttp3.internal.connection.RealCall r4 = r6.this$0     // Catch:{ all -> 0x007c }
                r4.cancel()     // Catch:{ all -> 0x007c }
                if (r3 != 0) goto L_0x007b
                java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x007c }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x007c }
                r4.<init>()     // Catch:{ all -> 0x007c }
                java.lang.String r5 = "canceled due to "
                r4.append(r5)     // Catch:{ all -> 0x007c }
                r4.append(r0)     // Catch:{ all -> 0x007c }
                java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x007c }
                r3.<init>(r4)     // Catch:{ all -> 0x007c }
                r3.addSuppressed(r0)     // Catch:{ all -> 0x007c }
                okhttp3.Callback r4 = r6.responseCallback     // Catch:{ all -> 0x007c }
                okhttp3.internal.connection.RealCall r5 = r6.this$0     // Catch:{ all -> 0x007c }
                r4.onFailure(r5, r3)     // Catch:{ all -> 0x007c }
            L_0x007b:
                throw r0     // Catch:{ all -> 0x007c }
            L_0x007c:
                r0 = move-exception
                goto L_0x00bb
            L_0x007e:
                r3 = move-exception
                r0 = r3
                r3 = 0
            L_0x0081:
                if (r3 == 0) goto L_0x00a5
                okhttp3.internal.platform.Platform$Companion r3 = okhttp3.internal.platform.Platform.Companion     // Catch:{ all -> 0x007c }
                okhttp3.internal.platform.Platform r3 = r3.get()     // Catch:{ all -> 0x007c }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x007c }
                r4.<init>()     // Catch:{ all -> 0x007c }
                java.lang.String r5 = "Callback failure for "
                r4.append(r5)     // Catch:{ all -> 0x007c }
                okhttp3.internal.connection.RealCall r5 = r6.this$0     // Catch:{ all -> 0x007c }
                java.lang.String r5 = r5.toLoggableString()     // Catch:{ all -> 0x007c }
                r4.append(r5)     // Catch:{ all -> 0x007c }
                java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x007c }
                r5 = 4
                r3.log(r4, r5, r0)     // Catch:{ all -> 0x007c }
                goto L_0x00ac
            L_0x00a5:
                okhttp3.Callback r3 = r6.responseCallback     // Catch:{ all -> 0x007c }
                okhttp3.internal.connection.RealCall r4 = r6.this$0     // Catch:{ all -> 0x007c }
                r3.onFailure(r4, r0)     // Catch:{ all -> 0x007c }
            L_0x00ac:
                okhttp3.internal.connection.RealCall r0 = r6.this$0     // Catch:{ all -> 0x00c9 }
                okhttp3.OkHttpClient r0 = r0.getClient()     // Catch:{ all -> 0x00c9 }
                okhttp3.Dispatcher r0 = r0.dispatcher()     // Catch:{ all -> 0x00c9 }
                goto L_0x0049
            L_0x00b7:
                r1.setName(r2)
                return
            L_0x00bb:
                okhttp3.internal.connection.RealCall r3 = r6.this$0     // Catch:{ all -> 0x00c9 }
                okhttp3.OkHttpClient r3 = r3.getClient()     // Catch:{ all -> 0x00c9 }
                okhttp3.Dispatcher r3 = r3.dispatcher()     // Catch:{ all -> 0x00c9 }
                r3.finished$okhttp((okhttp3.internal.connection.RealCall.AsyncCall) r6)     // Catch:{ all -> 0x00c9 }
                throw r0     // Catch:{ all -> 0x00c9 }
            L_0x00c9:
                r0 = move-exception
                r1.setName(r2)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.RealCall.AsyncCall.run():void");
        }
    }

    public static final class CallReference extends WeakReference<RealCall> {
        private final Object callStackTrace;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public CallReference(RealCall realCall, Object obj) {
            super(realCall);
            Intrinsics.b(realCall, "referent");
            this.callStackTrace = obj;
        }

        public final Object getCallStackTrace() {
            return this.callStackTrace;
        }
    }

    public RealCall(OkHttpClient okHttpClient, Request request, boolean z) {
        Intrinsics.b(okHttpClient, "client");
        Intrinsics.b(request, "originalRequest");
        this.client = okHttpClient;
        this.originalRequest = request;
        this.forWebSocket = z;
        RealCall$timeout$1 realCall$timeout$1 = new RealCall$timeout$1(this);
        realCall$timeout$1.timeout((long) this.client.callTimeoutMillis(), TimeUnit.MILLISECONDS);
        this.timeout = realCall$timeout$1;
    }

    private final void callStart() {
        this.callStackTrace = Platform.Companion.get().getStackTraceForCloseable("response.body().close()");
        this.eventListener.callStart(this);
    }

    private final Address createAddress(HttpUrl httpUrl) {
        CertificatePinner certificatePinner;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (httpUrl.isHttps()) {
            SSLSocketFactory sslSocketFactory = this.client.sslSocketFactory();
            hostnameVerifier = this.client.hostnameVerifier();
            sSLSocketFactory = sslSocketFactory;
            certificatePinner = this.client.certificatePinner();
        } else {
            sSLSocketFactory = null;
            hostnameVerifier = null;
            certificatePinner = null;
        }
        return new Address(httpUrl.host(), httpUrl.port(), this.client.dns(), this.client.socketFactory(), sSLSocketFactory, hostnameVerifier, certificatePinner, this.client.proxyAuthenticator(), this.client.proxy(), this.client.protocols(), this.client.connectionSpecs(), this.client.proxySelector());
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0019 A[Catch:{ all -> 0x0013 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x007d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final <E extends java.io.IOException> E maybeReleaseConnection(E r8, boolean r9) {
        /*
            r7 = this;
            kotlin.jvm.internal.Ref$ObjectRef r0 = new kotlin.jvm.internal.Ref$ObjectRef
            r0.<init>()
            okhttp3.internal.connection.RealConnectionPool r1 = r7.connectionPool
            monitor-enter(r1)
            r2 = 0
            r3 = 1
            if (r9 == 0) goto L_0x0016
            okhttp3.internal.connection.Exchange r4 = r7.exchange     // Catch:{ all -> 0x0013 }
            if (r4 != 0) goto L_0x0011
            goto L_0x0016
        L_0x0011:
            r4 = 0
            goto L_0x0017
        L_0x0013:
            r8 = move-exception
            goto L_0x0089
        L_0x0016:
            r4 = 1
        L_0x0017:
            if (r4 == 0) goto L_0x007d
            okhttp3.internal.connection.RealConnection r4 = r7.connection     // Catch:{ all -> 0x0013 }
            r0.element = r4     // Catch:{ all -> 0x0013 }
            okhttp3.internal.connection.RealConnection r4 = r7.connection     // Catch:{ all -> 0x0013 }
            r5 = 0
            if (r4 == 0) goto L_0x0031
            okhttp3.internal.connection.Exchange r4 = r7.exchange     // Catch:{ all -> 0x0013 }
            if (r4 != 0) goto L_0x0031
            if (r9 != 0) goto L_0x002c
            boolean r9 = r7.noMoreExchanges     // Catch:{ all -> 0x0013 }
            if (r9 == 0) goto L_0x0031
        L_0x002c:
            java.net.Socket r9 = r7.releaseConnectionNoEvents$okhttp()     // Catch:{ all -> 0x0013 }
            goto L_0x0032
        L_0x0031:
            r9 = r5
        L_0x0032:
            okhttp3.internal.connection.RealConnection r4 = r7.connection     // Catch:{ all -> 0x0013 }
            if (r4 == 0) goto L_0x0038
            r0.element = r5     // Catch:{ all -> 0x0013 }
        L_0x0038:
            boolean r4 = r7.noMoreExchanges     // Catch:{ all -> 0x0013 }
            if (r4 == 0) goto L_0x0042
            okhttp3.internal.connection.Exchange r4 = r7.exchange     // Catch:{ all -> 0x0013 }
            if (r4 != 0) goto L_0x0042
            r4 = 1
            goto L_0x0043
        L_0x0042:
            r4 = 0
        L_0x0043:
            kotlin.Unit r6 = kotlin.Unit.f6917a     // Catch:{ all -> 0x0013 }
            monitor-exit(r1)
            if (r9 == 0) goto L_0x004b
            okhttp3.internal.Util.closeQuietly((java.net.Socket) r9)
        L_0x004b:
            T r9 = r0.element
            r0 = r9
            okhttp3.Connection r0 = (okhttp3.Connection) r0
            if (r0 == 0) goto L_0x0060
            okhttp3.EventListener r0 = r7.eventListener
            okhttp3.Connection r9 = (okhttp3.Connection) r9
            if (r9 == 0) goto L_0x005c
            r0.connectionReleased(r7, r9)
            goto L_0x0060
        L_0x005c:
            kotlin.jvm.internal.Intrinsics.a()
            throw r5
        L_0x0060:
            if (r4 == 0) goto L_0x007c
            if (r8 == 0) goto L_0x0065
            r2 = 1
        L_0x0065:
            java.io.IOException r8 = r7.timeoutExit(r8)
            if (r2 == 0) goto L_0x0077
            okhttp3.EventListener r9 = r7.eventListener
            if (r8 == 0) goto L_0x0073
            r9.callFailed(r7, r8)
            goto L_0x007c
        L_0x0073:
            kotlin.jvm.internal.Intrinsics.a()
            throw r5
        L_0x0077:
            okhttp3.EventListener r9 = r7.eventListener
            r9.callEnd(r7)
        L_0x007c:
            return r8
        L_0x007d:
            java.lang.String r8 = "cannot release connection while it is in use"
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0013 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0013 }
            r9.<init>(r8)     // Catch:{ all -> 0x0013 }
            throw r9     // Catch:{ all -> 0x0013 }
        L_0x0089:
            monitor-exit(r1)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.RealCall.maybeReleaseConnection(java.io.IOException, boolean):java.io.IOException");
    }

    private final <E extends IOException> E timeoutExit(E e) {
        if (this.timeoutEarlyExit || !this.timeout.exit()) {
            return e;
        }
        E interruptedIOException = new InterruptedIOException("timeout");
        if (e != null) {
            interruptedIOException.initCause(e);
        }
        return interruptedIOException;
    }

    /* access modifiers changed from: private */
    public final String toLoggableString() {
        StringBuilder sb = new StringBuilder();
        sb.append(isCanceled() ? "canceled " : "");
        sb.append(this.forWebSocket ? "web socket" : "call");
        sb.append(" to ");
        sb.append(redactedUrl$okhttp());
        return sb.toString();
    }

    public final void acquireConnectionNoEvents(RealConnection realConnection) {
        Intrinsics.b(realConnection, "connection");
        RealConnectionPool realConnectionPool = this.connectionPool;
        if (!Util.assertionsEnabled || Thread.holdsLock(realConnectionPool)) {
            if (this.connection == null) {
                this.connection = realConnection;
                realConnection.getCalls().add(new CallReference(this, this.callStackTrace));
                return;
            }
            throw new IllegalStateException("Check failed.".toString());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Thread ");
        Thread currentThread = Thread.currentThread();
        Intrinsics.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        sb.append(" MUST hold lock on ");
        sb.append(realConnectionPool);
        throw new AssertionError(sb.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x001e, code lost:
        if (r1 == null) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0020, code lost:
        r1.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0024, code lost:
        if (r2 == null) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0026, code lost:
        r2.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0029, code lost:
        r4.eventListener.canceled(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void cancel() {
        /*
            r4 = this;
            okhttp3.internal.connection.RealConnectionPool r0 = r4.connectionPool
            monitor-enter(r0)
            boolean r1 = r4.canceled     // Catch:{ all -> 0x002f }
            if (r1 == 0) goto L_0x0009
            monitor-exit(r0)
            return
        L_0x0009:
            r1 = 1
            r4.canceled = r1     // Catch:{ all -> 0x002f }
            okhttp3.internal.connection.Exchange r1 = r4.exchange     // Catch:{ all -> 0x002f }
            okhttp3.internal.connection.ExchangeFinder r2 = r4.exchangeFinder     // Catch:{ all -> 0x002f }
            if (r2 == 0) goto L_0x0019
            okhttp3.internal.connection.RealConnection r2 = r2.connectingConnection()     // Catch:{ all -> 0x002f }
            if (r2 == 0) goto L_0x0019
            goto L_0x001b
        L_0x0019:
            okhttp3.internal.connection.RealConnection r2 = r4.connection     // Catch:{ all -> 0x002f }
        L_0x001b:
            kotlin.Unit r3 = kotlin.Unit.f6917a     // Catch:{ all -> 0x002f }
            monitor-exit(r0)
            if (r1 == 0) goto L_0x0024
            r1.cancel()
            goto L_0x0029
        L_0x0024:
            if (r2 == 0) goto L_0x0029
            r2.cancel()
        L_0x0029:
            okhttp3.EventListener r0 = r4.eventListener
            r0.canceled(r4)
            return
        L_0x002f:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.RealCall.cancel():void");
    }

    public void enqueue(Callback callback) {
        Intrinsics.b(callback, "responseCallback");
        synchronized (this) {
            if (!this.executed) {
                this.executed = true;
                Unit unit = Unit.f6917a;
            } else {
                throw new IllegalStateException("Already Executed".toString());
            }
        }
        callStart();
        this.client.dispatcher().enqueue$okhttp(new AsyncCall(this, callback));
    }

    public final void enterNetworkInterceptorExchange(Request request, boolean z) {
        Intrinsics.b(request, "request");
        boolean z2 = true;
        if (this.interceptorScopedExchange == null) {
            if (this.exchange != null) {
                z2 = false;
            }
            if (!z2) {
                throw new IllegalStateException("cannot make a new request because the previous response is still open: please call response.close()".toString());
            } else if (z) {
                this.exchangeFinder = new ExchangeFinder(this.connectionPool, createAddress(request.url()), this, this.eventListener);
            }
        } else {
            throw new IllegalStateException("Check failed.".toString());
        }
    }

    public Response execute() {
        synchronized (this) {
            if (!this.executed) {
                this.executed = true;
                Unit unit = Unit.f6917a;
            } else {
                throw new IllegalStateException("Already Executed".toString());
            }
        }
        this.timeout.enter();
        callStart();
        try {
            this.client.dispatcher().executed$okhttp(this);
            return getResponseWithInterceptorChain$okhttp();
        } finally {
            this.client.dispatcher().finished$okhttp(this);
        }
    }

    public final void exitNetworkInterceptorExchange$okhttp(boolean z) {
        boolean z2 = true;
        if (!this.noMoreExchanges) {
            if (z) {
                Exchange exchange2 = this.exchange;
                if (exchange2 != null) {
                    exchange2.detachWithViolence();
                }
                if (this.exchange != null) {
                    z2 = false;
                }
                if (!z2) {
                    throw new IllegalStateException("Check failed.".toString());
                }
            }
            this.interceptorScopedExchange = null;
            return;
        }
        throw new IllegalStateException("released".toString());
    }

    public final OkHttpClient getClient() {
        return this.client;
    }

    public final RealConnection getConnection() {
        return this.connection;
    }

    public final EventListener getEventListener$okhttp() {
        return this.eventListener;
    }

    public final boolean getForWebSocket() {
        return this.forWebSocket;
    }

    public final Exchange getInterceptorScopedExchange$okhttp() {
        return this.interceptorScopedExchange;
    }

    public final Request getOriginalRequest() {
        return this.originalRequest;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final okhttp3.Response getResponseWithInterceptorChain$okhttp() throws java.io.IOException {
        /*
            r10 = this;
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            okhttp3.OkHttpClient r0 = r10.client
            java.util.List r0 = r0.interceptors()
            boolean unused = kotlin.collections.CollectionsKt__MutableCollectionsKt.a(r2, r0)
            okhttp3.internal.http.RetryAndFollowUpInterceptor r0 = new okhttp3.internal.http.RetryAndFollowUpInterceptor
            okhttp3.OkHttpClient r1 = r10.client
            r0.<init>(r1)
            r2.add(r0)
            okhttp3.internal.http.BridgeInterceptor r0 = new okhttp3.internal.http.BridgeInterceptor
            okhttp3.OkHttpClient r1 = r10.client
            okhttp3.CookieJar r1 = r1.cookieJar()
            r0.<init>(r1)
            r2.add(r0)
            okhttp3.internal.cache.CacheInterceptor r0 = new okhttp3.internal.cache.CacheInterceptor
            okhttp3.OkHttpClient r1 = r10.client
            okhttp3.Cache r1 = r1.cache()
            r0.<init>(r1)
            r2.add(r0)
            okhttp3.internal.connection.ConnectInterceptor r0 = okhttp3.internal.connection.ConnectInterceptor.INSTANCE
            r2.add(r0)
            boolean r0 = r10.forWebSocket
            if (r0 != 0) goto L_0x0046
            okhttp3.OkHttpClient r0 = r10.client
            java.util.List r0 = r0.networkInterceptors()
            boolean unused = kotlin.collections.CollectionsKt__MutableCollectionsKt.a(r2, r0)
        L_0x0046:
            okhttp3.internal.http.CallServerInterceptor r0 = new okhttp3.internal.http.CallServerInterceptor
            boolean r1 = r10.forWebSocket
            r0.<init>(r1)
            r2.add(r0)
            okhttp3.internal.http.RealInterceptorChain r9 = new okhttp3.internal.http.RealInterceptorChain
            r3 = 0
            r4 = 0
            okhttp3.Request r5 = r10.originalRequest
            okhttp3.OkHttpClient r0 = r10.client
            int r6 = r0.connectTimeoutMillis()
            okhttp3.OkHttpClient r0 = r10.client
            int r7 = r0.readTimeoutMillis()
            okhttp3.OkHttpClient r0 = r10.client
            int r8 = r0.writeTimeoutMillis()
            r0 = r9
            r1 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r0 = 0
            r1 = 0
            okhttp3.Request r2 = r10.originalRequest     // Catch:{ IOException -> 0x008c, all -> 0x008a }
            okhttp3.Response r2 = r9.proceed(r2)     // Catch:{ IOException -> 0x008c, all -> 0x008a }
            boolean r3 = r10.isCanceled()     // Catch:{ IOException -> 0x008c, all -> 0x008a }
            if (r3 != 0) goto L_0x007f
            r10.noMoreExchanges$okhttp(r1)
            return r2
        L_0x007f:
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r2)     // Catch:{ IOException -> 0x008c, all -> 0x008a }
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x008c, all -> 0x008a }
            java.lang.String r3 = "Canceled"
            r2.<init>(r3)     // Catch:{ IOException -> 0x008c, all -> 0x008a }
            throw r2     // Catch:{ IOException -> 0x008c, all -> 0x008a }
        L_0x008a:
            r2 = move-exception
            goto L_0x00a0
        L_0x008c:
            r0 = move-exception
            r2 = 1
            java.io.IOException r0 = r10.noMoreExchanges$okhttp(r0)     // Catch:{ all -> 0x009d }
            if (r0 != 0) goto L_0x009c
            kotlin.TypeCastException r0 = new kotlin.TypeCastException     // Catch:{ all -> 0x009d }
            java.lang.String r3 = "null cannot be cast to non-null type kotlin.Throwable"
            r0.<init>(r3)     // Catch:{ all -> 0x009d }
            throw r0     // Catch:{ all -> 0x009d }
        L_0x009c:
            throw r0     // Catch:{ all -> 0x009d }
        L_0x009d:
            r0 = move-exception
            r2 = r0
            r0 = 1
        L_0x00a0:
            if (r0 != 0) goto L_0x00a5
            r10.noMoreExchanges$okhttp(r1)
        L_0x00a5:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.RealCall.getResponseWithInterceptorChain$okhttp():okhttp3.Response");
    }

    public final Exchange initExchange$okhttp(RealInterceptorChain realInterceptorChain) {
        Intrinsics.b(realInterceptorChain, "chain");
        synchronized (this.connectionPool) {
            boolean z = true;
            if (!this.noMoreExchanges) {
                if (this.exchange != null) {
                    z = false;
                }
                if (z) {
                    Unit unit = Unit.f6917a;
                } else {
                    throw new IllegalStateException("Check failed.".toString());
                }
            } else {
                throw new IllegalStateException("released".toString());
            }
        }
        ExchangeFinder exchangeFinder2 = this.exchangeFinder;
        if (exchangeFinder2 != null) {
            ExchangeCodec find = exchangeFinder2.find(this.client, realInterceptorChain);
            EventListener eventListener2 = this.eventListener;
            ExchangeFinder exchangeFinder3 = this.exchangeFinder;
            if (exchangeFinder3 != null) {
                Exchange exchange2 = new Exchange(this, eventListener2, exchangeFinder3, find);
                this.interceptorScopedExchange = exchange2;
                synchronized (this.connectionPool) {
                    this.exchange = exchange2;
                    this.exchangeRequestDone = false;
                    this.exchangeResponseDone = false;
                }
                return exchange2;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a();
        throw null;
    }

    public boolean isCanceled() {
        boolean z;
        synchronized (this.connectionPool) {
            z = this.canceled;
        }
        return z;
    }

    public synchronized boolean isExecuted() {
        return this.executed;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004d, code lost:
        if (r1 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return maybeReleaseConnection(r7, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return r7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <E extends java.io.IOException> E messageDone$okhttp(okhttp3.internal.connection.Exchange r4, boolean r5, boolean r6, E r7) {
        /*
            r3 = this;
            java.lang.String r0 = "exchange"
            kotlin.jvm.internal.Intrinsics.b(r4, r0)
            okhttp3.internal.connection.RealConnectionPool r0 = r3.connectionPool
            monitor-enter(r0)
            okhttp3.internal.connection.Exchange r1 = r3.exchange     // Catch:{ all -> 0x0054 }
            boolean r4 = kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r4, (java.lang.Object) r1)     // Catch:{ all -> 0x0054 }
            r1 = 1
            r4 = r4 ^ r1
            if (r4 == 0) goto L_0x0014
            monitor-exit(r0)
            return r7
        L_0x0014:
            r4 = 0
            if (r5 == 0) goto L_0x001d
            boolean r5 = r3.exchangeRequestDone     // Catch:{ all -> 0x0054 }
            r5 = r5 ^ r1
            r3.exchangeRequestDone = r1     // Catch:{ all -> 0x0054 }
            goto L_0x001e
        L_0x001d:
            r5 = 0
        L_0x001e:
            if (r6 == 0) goto L_0x0027
            boolean r6 = r3.exchangeResponseDone     // Catch:{ all -> 0x0054 }
            if (r6 != 0) goto L_0x0025
            r5 = 1
        L_0x0025:
            r3.exchangeResponseDone = r1     // Catch:{ all -> 0x0054 }
        L_0x0027:
            boolean r6 = r3.exchangeRequestDone     // Catch:{ all -> 0x0054 }
            if (r6 == 0) goto L_0x0049
            boolean r6 = r3.exchangeResponseDone     // Catch:{ all -> 0x0054 }
            if (r6 == 0) goto L_0x0049
            if (r5 == 0) goto L_0x0049
            okhttp3.internal.connection.Exchange r5 = r3.exchange     // Catch:{ all -> 0x0054 }
            r6 = 0
            if (r5 == 0) goto L_0x0045
            okhttp3.internal.connection.RealConnection r5 = r5.getConnection$okhttp()     // Catch:{ all -> 0x0054 }
            int r2 = r5.getSuccessCount$okhttp()     // Catch:{ all -> 0x0054 }
            int r2 = r2 + r1
            r5.setSuccessCount$okhttp(r2)     // Catch:{ all -> 0x0054 }
            r3.exchange = r6     // Catch:{ all -> 0x0054 }
            goto L_0x004a
        L_0x0045:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x0054 }
            throw r6
        L_0x0049:
            r1 = 0
        L_0x004a:
            kotlin.Unit r5 = kotlin.Unit.f6917a     // Catch:{ all -> 0x0054 }
            monitor-exit(r0)
            if (r1 == 0) goto L_0x0053
            java.io.IOException r7 = r3.maybeReleaseConnection(r7, r4)
        L_0x0053:
            return r7
        L_0x0054:
            r4 = move-exception
            monitor-exit(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.RealCall.messageDone$okhttp(okhttp3.internal.connection.Exchange, boolean, boolean, java.io.IOException):java.io.IOException");
    }

    public final IOException noMoreExchanges$okhttp(IOException iOException) {
        synchronized (this.connectionPool) {
            this.noMoreExchanges = true;
            Unit unit = Unit.f6917a;
        }
        return maybeReleaseConnection(iOException, false);
    }

    public final String redactedUrl$okhttp() {
        return this.originalRequest.url().redact();
    }

    public final Socket releaseConnectionNoEvents$okhttp() {
        RealConnectionPool realConnectionPool = this.connectionPool;
        if (!Util.assertionsEnabled || Thread.holdsLock(realConnectionPool)) {
            RealConnection realConnection = this.connection;
            if (realConnection != null) {
                Iterator<Reference<RealCall>> it2 = realConnection.getCalls().iterator();
                boolean z = false;
                int i = 0;
                while (true) {
                    if (!it2.hasNext()) {
                        i = -1;
                        break;
                    } else if (Intrinsics.a((Object) (RealCall) it2.next().get(), (Object) this)) {
                        break;
                    } else {
                        i++;
                    }
                }
                if (i != -1) {
                    z = true;
                }
                if (z) {
                    RealConnection realConnection2 = this.connection;
                    if (realConnection2 != null) {
                        realConnection2.getCalls().remove(i);
                        this.connection = null;
                        if (realConnection2.getCalls().isEmpty()) {
                            realConnection2.setIdleAtNs$okhttp(System.nanoTime());
                            if (this.connectionPool.connectionBecameIdle(realConnection2)) {
                                return realConnection2.socket();
                            }
                        }
                        return null;
                    }
                    Intrinsics.a();
                    throw null;
                }
                throw new IllegalStateException("Check failed.".toString());
            }
            Intrinsics.a();
            throw null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Thread ");
        Thread currentThread = Thread.currentThread();
        Intrinsics.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        sb.append(" MUST hold lock on ");
        sb.append(realConnectionPool);
        throw new AssertionError(sb.toString());
    }

    public Request request() {
        return this.originalRequest;
    }

    public final boolean retryAfterFailure() {
        ExchangeFinder exchangeFinder2 = this.exchangeFinder;
        if (exchangeFinder2 != null) {
            return exchangeFinder2.retryAfterFailure();
        }
        Intrinsics.a();
        throw null;
    }

    public final void setConnection(RealConnection realConnection) {
        this.connection = realConnection;
    }

    public final void timeoutEarlyExit() {
        if (!this.timeoutEarlyExit) {
            this.timeoutEarlyExit = true;
            this.timeout.exit();
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    public AsyncTimeout timeout() {
        return this.timeout;
    }

    public RealCall clone() {
        return new RealCall(this.client, this.originalRequest, this.forWebSocket);
    }
}
