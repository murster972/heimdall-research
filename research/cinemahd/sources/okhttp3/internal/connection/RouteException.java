package okhttp3.internal.connection;

import java.io.IOException;
import kotlin.jvm.internal.Intrinsics;

public final class RouteException extends RuntimeException {
    private final IOException firstConnectException;
    private IOException lastConnectException = this.firstConnectException;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RouteException(IOException iOException) {
        super(iOException);
        Intrinsics.b(iOException, "firstConnectException");
        this.firstConnectException = iOException;
    }

    public final void addConnectException(IOException iOException) {
        Intrinsics.b(iOException, "e");
        this.firstConnectException.addSuppressed(iOException);
        this.lastConnectException = iOException;
    }

    public final IOException getFirstConnectException() {
        return this.firstConnectException;
    }

    public final IOException getLastConnectException() {
        return this.lastConnectException;
    }
}
