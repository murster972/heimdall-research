package okhttp3.internal.connection;

import java.lang.ref.Reference;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Address;
import okhttp3.ConnectionPool;
import okhttp3.Route;
import okhttp3.internal.Util;
import okhttp3.internal.concurrent.TaskQueue;
import okhttp3.internal.concurrent.TaskRunner;
import okhttp3.internal.connection.RealCall;
import okhttp3.internal.platform.Platform;

public final class RealConnectionPool {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final TaskQueue cleanupQueue;
    private final RealConnectionPool$cleanupTask$1 cleanupTask = new RealConnectionPool$cleanupTask$1(this, Util.okHttpName + " ConnectionPool");
    private final ArrayDeque<RealConnection> connections = new ArrayDeque<>();
    private final long keepAliveDurationNs;
    private final int maxIdleConnections;

    public static final class Companion {
        private Companion() {
        }

        public final RealConnectionPool get(ConnectionPool connectionPool) {
            Intrinsics.b(connectionPool, "connectionPool");
            return connectionPool.getDelegate$okhttp();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public RealConnectionPool(TaskRunner taskRunner, int i, long j, TimeUnit timeUnit) {
        Intrinsics.b(taskRunner, "taskRunner");
        Intrinsics.b(timeUnit, "timeUnit");
        this.maxIdleConnections = i;
        this.keepAliveDurationNs = timeUnit.toNanos(j);
        this.cleanupQueue = taskRunner.newQueue();
        if (!(j > 0)) {
            throw new IllegalArgumentException(("keepAliveDuration <= 0: " + j).toString());
        }
    }

    private final int pruneAndGetAllocationCount(RealConnection realConnection, long j) {
        List<Reference<RealCall>> calls = realConnection.getCalls();
        int i = 0;
        while (i < calls.size()) {
            Reference reference = calls.get(i);
            if (reference.get() != null) {
                i++;
            } else if (reference != null) {
                Platform.Companion.get().logCloseableLeak("A connection to " + realConnection.route().address().url() + " was leaked. " + "Did you forget to close a response body?", ((RealCall.CallReference) reference).getCallStackTrace());
                calls.remove(i);
                realConnection.setNoNewExchanges(true);
                if (calls.isEmpty()) {
                    realConnection.setIdleAtNs$okhttp(j - this.keepAliveDurationNs);
                    return 0;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type okhttp3.internal.connection.RealCall.CallReference");
            }
        }
        return calls.size();
    }

    public final boolean callAcquirePooledConnection(Address address, RealCall realCall, List<Route> list, boolean z) {
        Intrinsics.b(address, "address");
        Intrinsics.b(realCall, "call");
        if (!Util.assertionsEnabled || Thread.holdsLock(this)) {
            Iterator<RealConnection> it2 = this.connections.iterator();
            while (it2.hasNext()) {
                RealConnection next = it2.next();
                if ((!z || next.isMultiplexed()) && next.isEligible$okhttp(address, list)) {
                    Intrinsics.a((Object) next, "connection");
                    realCall.acquireConnectionNoEvents(next);
                    return true;
                }
            }
            return false;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Thread ");
        Thread currentThread = Thread.currentThread();
        Intrinsics.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        sb.append(" MUST hold lock on ");
        sb.append(this);
        throw new AssertionError(sb.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0067, code lost:
        if (r5 == null) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0069, code lost:
        okhttp3.internal.Util.closeQuietly(r5.socket());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0072, code lost:
        return 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0073, code lost:
        kotlin.jvm.internal.Intrinsics.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0076, code lost:
        throw null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long cleanup(long r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            java.util.ArrayDeque<okhttp3.internal.connection.RealConnection> r0 = r11.connections     // Catch:{ all -> 0x0077 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0077 }
            r1 = 0
            r2 = 0
            r3 = -9223372036854775808
            r5 = r1
            r6 = 0
        L_0x000d:
            boolean r7 = r0.hasNext()     // Catch:{ all -> 0x0077 }
            if (r7 == 0) goto L_0x0036
            java.lang.Object r7 = r0.next()     // Catch:{ all -> 0x0077 }
            okhttp3.internal.connection.RealConnection r7 = (okhttp3.internal.connection.RealConnection) r7     // Catch:{ all -> 0x0077 }
            java.lang.String r8 = "connection"
            kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r7, (java.lang.String) r8)     // Catch:{ all -> 0x0077 }
            int r8 = r11.pruneAndGetAllocationCount(r7, r12)     // Catch:{ all -> 0x0077 }
            if (r8 <= 0) goto L_0x0027
            int r6 = r6 + 1
            goto L_0x000d
        L_0x0027:
            int r2 = r2 + 1
            long r8 = r7.getIdleAtNs$okhttp()     // Catch:{ all -> 0x0077 }
            long r8 = r12 - r8
            int r10 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r10 <= 0) goto L_0x000d
            r5 = r7
            r3 = r8
            goto L_0x000d
        L_0x0036:
            long r12 = r11.keepAliveDurationNs     // Catch:{ all -> 0x0077 }
            int r0 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r0 >= 0) goto L_0x0052
            int r12 = r11.maxIdleConnections     // Catch:{ all -> 0x0077 }
            if (r2 <= r12) goto L_0x0041
            goto L_0x0052
        L_0x0041:
            if (r2 <= 0) goto L_0x0048
            long r12 = r11.keepAliveDurationNs     // Catch:{ all -> 0x0077 }
            long r12 = r12 - r3
            monitor-exit(r11)
            return r12
        L_0x0048:
            if (r6 <= 0) goto L_0x004e
            long r12 = r11.keepAliveDurationNs     // Catch:{ all -> 0x0077 }
            monitor-exit(r11)
            return r12
        L_0x004e:
            r12 = -1
            monitor-exit(r11)
            return r12
        L_0x0052:
            java.util.ArrayDeque<okhttp3.internal.connection.RealConnection> r12 = r11.connections     // Catch:{ all -> 0x0077 }
            r12.remove(r5)     // Catch:{ all -> 0x0077 }
            java.util.ArrayDeque<okhttp3.internal.connection.RealConnection> r12 = r11.connections     // Catch:{ all -> 0x0077 }
            boolean r12 = r12.isEmpty()     // Catch:{ all -> 0x0077 }
            if (r12 == 0) goto L_0x0064
            okhttp3.internal.concurrent.TaskQueue r12 = r11.cleanupQueue     // Catch:{ all -> 0x0077 }
            r12.cancelAll()     // Catch:{ all -> 0x0077 }
        L_0x0064:
            kotlin.Unit r12 = kotlin.Unit.f6917a     // Catch:{ all -> 0x0077 }
            monitor-exit(r11)
            if (r5 == 0) goto L_0x0073
            java.net.Socket r12 = r5.socket()
            okhttp3.internal.Util.closeQuietly((java.net.Socket) r12)
            r12 = 0
            return r12
        L_0x0073:
            kotlin.jvm.internal.Intrinsics.a()
            throw r1
        L_0x0077:
            r12 = move-exception
            monitor-exit(r11)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.RealConnectionPool.cleanup(long):long");
    }

    public final boolean connectionBecameIdle(RealConnection realConnection) {
        Intrinsics.b(realConnection, "connection");
        if (Util.assertionsEnabled && !Thread.holdsLock(this)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Thread ");
            Thread currentThread = Thread.currentThread();
            Intrinsics.a((Object) currentThread, "Thread.currentThread()");
            sb.append(currentThread.getName());
            sb.append(" MUST hold lock on ");
            sb.append(this);
            throw new AssertionError(sb.toString());
        } else if (realConnection.getNoNewExchanges() || this.maxIdleConnections == 0) {
            this.connections.remove(realConnection);
            if (this.connections.isEmpty()) {
                this.cleanupQueue.cancelAll();
            }
            return true;
        } else {
            TaskQueue.schedule$default(this.cleanupQueue, this.cleanupTask, 0, 2, (Object) null);
            return false;
        }
    }

    public final synchronized int connectionCount() {
        return this.connections.size();
    }

    public final void evictAll() {
        ArrayList<RealConnection> arrayList = new ArrayList<>();
        synchronized (this) {
            Iterator<RealConnection> it2 = this.connections.iterator();
            Intrinsics.a((Object) it2, "connections.iterator()");
            while (it2.hasNext()) {
                RealConnection next = it2.next();
                if (next.getCalls().isEmpty()) {
                    next.setNoNewExchanges(true);
                    Intrinsics.a((Object) next, "connection");
                    arrayList.add(next);
                    it2.remove();
                }
            }
            if (this.connections.isEmpty()) {
                this.cleanupQueue.cancelAll();
            }
            Unit unit = Unit.f6917a;
        }
        for (RealConnection socket : arrayList) {
            Util.closeQuietly(socket.socket());
        }
    }

    public final synchronized int idleConnectionCount() {
        int i;
        ArrayDeque<RealConnection> arrayDeque = this.connections;
        i = 0;
        if (!(arrayDeque instanceof Collection) || !arrayDeque.isEmpty()) {
            for (RealConnection calls : arrayDeque) {
                if (calls.getCalls().isEmpty()) {
                    i++;
                    if (i < 0) {
                        CollectionsKt.b();
                        throw null;
                    }
                }
            }
        }
        return i;
    }

    public final void put(RealConnection realConnection) {
        Intrinsics.b(realConnection, "connection");
        if (!Util.assertionsEnabled || Thread.holdsLock(this)) {
            this.connections.add(realConnection);
            TaskQueue.schedule$default(this.cleanupQueue, this.cleanupTask, 0, 2, (Object) null);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Thread ");
        Thread currentThread = Thread.currentThread();
        Intrinsics.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        sb.append(" MUST hold lock on ");
        sb.append(this);
        throw new AssertionError(sb.toString());
    }
}
