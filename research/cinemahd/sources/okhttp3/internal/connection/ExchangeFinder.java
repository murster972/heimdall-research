package okhttp3.internal.connection;

import com.vungle.warren.model.ReportDBAdapter;
import java.io.IOException;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Address;
import okhttp3.EventListener;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Route;
import okhttp3.internal.Util;
import okhttp3.internal.connection.RouteSelector;
import okhttp3.internal.http.ExchangeCodec;
import okhttp3.internal.http.RealInterceptorChain;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.internal.http2.ErrorCode;
import okhttp3.internal.http2.StreamResetException;

public final class ExchangeFinder {
    private final Address address;
    private final RealCall call;
    private RealConnection connectingConnection;
    private final RealConnectionPool connectionPool;
    private int connectionShutdownCount;
    private final EventListener eventListener;
    private Route nextRouteToTry;
    private int otherFailureCount;
    private int refusedStreamCount;
    private RouteSelector.Selection routeSelection;
    private RouteSelector routeSelector;

    public ExchangeFinder(RealConnectionPool realConnectionPool, Address address2, RealCall realCall, EventListener eventListener2) {
        Intrinsics.b(realConnectionPool, "connectionPool");
        Intrinsics.b(address2, "address");
        Intrinsics.b(realCall, "call");
        Intrinsics.b(eventListener2, "eventListener");
        this.connectionPool = realConnectionPool;
        this.address = address2;
        this.call = realCall;
        this.eventListener = eventListener2;
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    private final okhttp3.internal.connection.RealConnection findConnection(int r19, int r20, int r21, int r22, boolean r23) throws java.io.IOException {
        /*
            r18 = this;
            r1 = r18
            kotlin.jvm.internal.Ref$ObjectRef r0 = new kotlin.jvm.internal.Ref$ObjectRef
            r0.<init>()
            okhttp3.internal.connection.RealConnectionPool r2 = r1.connectionPool
            monitor-enter(r2)
            okhttp3.internal.connection.RealCall r3 = r1.call     // Catch:{ all -> 0x01fa }
            boolean r3 = r3.isCanceled()     // Catch:{ all -> 0x01fa }
            if (r3 != 0) goto L_0x01f2
            okhttp3.internal.connection.RealCall r3 = r1.call     // Catch:{ all -> 0x01fa }
            okhttp3.internal.connection.RealConnection r3 = r3.getConnection()     // Catch:{ all -> 0x01fa }
            r0.element = r3     // Catch:{ all -> 0x01fa }
            r4 = 0
            if (r3 == 0) goto L_0x003c
            boolean r5 = r3.getNoNewExchanges()     // Catch:{ all -> 0x01fa }
            if (r5 != 0) goto L_0x0035
            okhttp3.Route r3 = r3.route()     // Catch:{ all -> 0x01fa }
            okhttp3.Address r3 = r3.address()     // Catch:{ all -> 0x01fa }
            okhttp3.HttpUrl r3 = r3.url()     // Catch:{ all -> 0x01fa }
            boolean r3 = r1.sameHostAndPort(r3)     // Catch:{ all -> 0x01fa }
            if (r3 != 0) goto L_0x003c
        L_0x0035:
            okhttp3.internal.connection.RealCall r3 = r1.call     // Catch:{ all -> 0x01fa }
            java.net.Socket r3 = r3.releaseConnectionNoEvents$okhttp()     // Catch:{ all -> 0x01fa }
            goto L_0x003d
        L_0x003c:
            r3 = r4
        L_0x003d:
            okhttp3.internal.connection.RealCall r5 = r1.call     // Catch:{ all -> 0x01fa }
            okhttp3.internal.connection.RealConnection r5 = r5.getConnection()     // Catch:{ all -> 0x01fa }
            if (r5 == 0) goto L_0x004e
            okhttp3.internal.connection.RealCall r5 = r1.call     // Catch:{ all -> 0x01fa }
            okhttp3.internal.connection.RealConnection r5 = r5.getConnection()     // Catch:{ all -> 0x01fa }
            r0.element = r4     // Catch:{ all -> 0x01fa }
            goto L_0x004f
        L_0x004e:
            r5 = r4
        L_0x004f:
            r6 = 1
            r7 = 0
            if (r5 != 0) goto L_0x0078
            r1.refusedStreamCount = r7     // Catch:{ all -> 0x01fa }
            r1.connectionShutdownCount = r7     // Catch:{ all -> 0x01fa }
            r1.otherFailureCount = r7     // Catch:{ all -> 0x01fa }
            okhttp3.internal.connection.RealConnectionPool r8 = r1.connectionPool     // Catch:{ all -> 0x01fa }
            okhttp3.Address r9 = r1.address     // Catch:{ all -> 0x01fa }
            okhttp3.internal.connection.RealCall r10 = r1.call     // Catch:{ all -> 0x01fa }
            boolean r8 = r8.callAcquirePooledConnection(r9, r10, r4, r7)     // Catch:{ all -> 0x01fa }
            if (r8 == 0) goto L_0x006e
            okhttp3.internal.connection.RealCall r5 = r1.call     // Catch:{ all -> 0x01fa }
            okhttp3.internal.connection.RealConnection r5 = r5.getConnection()     // Catch:{ all -> 0x01fa }
            r9 = r4
            r8 = 1
            goto L_0x007a
        L_0x006e:
            okhttp3.Route r8 = r1.nextRouteToTry     // Catch:{ all -> 0x01fa }
            if (r8 == 0) goto L_0x0078
            okhttp3.Route r8 = r1.nextRouteToTry     // Catch:{ all -> 0x01fa }
            r1.nextRouteToTry = r4     // Catch:{ all -> 0x01fa }
            r9 = r8
            goto L_0x0079
        L_0x0078:
            r9 = r4
        L_0x0079:
            r8 = 0
        L_0x007a:
            kotlin.Unit r10 = kotlin.Unit.f6917a     // Catch:{ all -> 0x01fa }
            monitor-exit(r2)
            if (r3 == 0) goto L_0x0082
            okhttp3.internal.Util.closeQuietly((java.net.Socket) r3)
        L_0x0082:
            T r0 = r0.element
            r2 = r0
            okhttp3.internal.connection.RealConnection r2 = (okhttp3.internal.connection.RealConnection) r2
            if (r2 == 0) goto L_0x0099
            okhttp3.EventListener r2 = r1.eventListener
            okhttp3.internal.connection.RealCall r3 = r1.call
            okhttp3.internal.connection.RealConnection r0 = (okhttp3.internal.connection.RealConnection) r0
            if (r0 == 0) goto L_0x0095
            r2.connectionReleased(r3, r0)
            goto L_0x0099
        L_0x0095:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x0099:
            if (r8 == 0) goto L_0x00a9
            okhttp3.EventListener r0 = r1.eventListener
            okhttp3.internal.connection.RealCall r2 = r1.call
            if (r5 == 0) goto L_0x00a5
            r0.connectionAcquired(r2, r5)
            goto L_0x00a9
        L_0x00a5:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x00a9:
            if (r5 == 0) goto L_0x00b2
            if (r5 == 0) goto L_0x00ae
            return r5
        L_0x00ae:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x00b2:
            if (r9 != 0) goto L_0x00e8
            okhttp3.internal.connection.RouteSelector$Selection r0 = r1.routeSelection
            if (r0 == 0) goto L_0x00c5
            if (r0 == 0) goto L_0x00c1
            boolean r0 = r0.hasNext()
            if (r0 != 0) goto L_0x00e8
            goto L_0x00c5
        L_0x00c1:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x00c5:
            okhttp3.internal.connection.RouteSelector r0 = r1.routeSelector
            if (r0 != 0) goto L_0x00e0
            okhttp3.internal.connection.RouteSelector r0 = new okhttp3.internal.connection.RouteSelector
            okhttp3.Address r2 = r1.address
            okhttp3.internal.connection.RealCall r3 = r1.call
            okhttp3.OkHttpClient r3 = r3.getClient()
            okhttp3.internal.connection.RouteDatabase r3 = r3.getRouteDatabase()
            okhttp3.internal.connection.RealCall r10 = r1.call
            okhttp3.EventListener r11 = r1.eventListener
            r0.<init>(r2, r3, r10, r11)
            r1.routeSelector = r0
        L_0x00e0:
            okhttp3.internal.connection.RouteSelector$Selection r0 = r0.next()
            r1.routeSelection = r0
            r0 = 1
            goto L_0x00e9
        L_0x00e8:
            r0 = 0
        L_0x00e9:
            okhttp3.internal.connection.RealConnectionPool r2 = r1.connectionPool
            monitor-enter(r2)
            okhttp3.internal.connection.RealCall r3 = r1.call     // Catch:{ all -> 0x01ef }
            boolean r3 = r3.isCanceled()     // Catch:{ all -> 0x01ef }
            if (r3 != 0) goto L_0x01e7
            if (r0 == 0) goto L_0x0116
            okhttp3.internal.connection.RouteSelector$Selection r0 = r1.routeSelection     // Catch:{ all -> 0x01ef }
            if (r0 == 0) goto L_0x0112
            java.util.List r0 = r0.getRoutes()     // Catch:{ all -> 0x01ef }
            okhttp3.internal.connection.RealConnectionPool r3 = r1.connectionPool     // Catch:{ all -> 0x01ef }
            okhttp3.Address r10 = r1.address     // Catch:{ all -> 0x01ef }
            okhttp3.internal.connection.RealCall r11 = r1.call     // Catch:{ all -> 0x01ef }
            boolean r3 = r3.callAcquirePooledConnection(r10, r11, r0, r7)     // Catch:{ all -> 0x01ef }
            if (r3 == 0) goto L_0x0117
            okhttp3.internal.connection.RealCall r3 = r1.call     // Catch:{ all -> 0x01ef }
            okhttp3.internal.connection.RealConnection r5 = r3.getConnection()     // Catch:{ all -> 0x01ef }
            r8 = 1
            goto L_0x0117
        L_0x0112:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01ef }
            throw r4
        L_0x0116:
            r0 = r4
        L_0x0117:
            if (r8 != 0) goto L_0x0138
            if (r9 != 0) goto L_0x0128
            okhttp3.internal.connection.RouteSelector$Selection r3 = r1.routeSelection     // Catch:{ all -> 0x01ef }
            if (r3 == 0) goto L_0x0124
            okhttp3.Route r9 = r3.next()     // Catch:{ all -> 0x01ef }
            goto L_0x0128
        L_0x0124:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01ef }
            throw r4
        L_0x0128:
            okhttp3.internal.connection.RealConnection r5 = new okhttp3.internal.connection.RealConnection     // Catch:{ all -> 0x01ef }
            okhttp3.internal.connection.RealConnectionPool r3 = r1.connectionPool     // Catch:{ all -> 0x01ef }
            if (r9 == 0) goto L_0x0134
            r5.<init>(r3, r9)     // Catch:{ all -> 0x01ef }
            r1.connectingConnection = r5     // Catch:{ all -> 0x01ef }
            goto L_0x0138
        L_0x0134:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01ef }
            throw r4
        L_0x0138:
            kotlin.Unit r3 = kotlin.Unit.f6917a     // Catch:{ all -> 0x01ef }
            monitor-exit(r2)
            if (r8 == 0) goto L_0x0151
            okhttp3.EventListener r0 = r1.eventListener
            okhttp3.internal.connection.RealCall r2 = r1.call
            if (r5 == 0) goto L_0x014d
            r0.connectionAcquired(r2, r5)
            if (r5 == 0) goto L_0x0149
            return r5
        L_0x0149:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x014d:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x0151:
            if (r5 == 0) goto L_0x01e3
            okhttp3.internal.connection.RealCall r2 = r1.call
            okhttp3.EventListener r3 = r1.eventListener
            r10 = r5
            r11 = r19
            r12 = r20
            r13 = r21
            r14 = r22
            r15 = r23
            r16 = r2
            r17 = r3
            r10.connect(r11, r12, r13, r14, r15, r16, r17)
            okhttp3.internal.connection.RealCall r2 = r1.call
            okhttp3.OkHttpClient r2 = r2.getClient()
            okhttp3.internal.connection.RouteDatabase r2 = r2.getRouteDatabase()
            if (r5 == 0) goto L_0x01df
            okhttp3.Route r3 = r5.route()
            r2.connected(r3)
            okhttp3.internal.connection.RealConnectionPool r2 = r1.connectionPool
            monitor-enter(r2)
            r1.connectingConnection = r4     // Catch:{ all -> 0x01dc }
            okhttp3.internal.connection.RealConnectionPool r3 = r1.connectionPool     // Catch:{ all -> 0x01dc }
            okhttp3.Address r7 = r1.address     // Catch:{ all -> 0x01dc }
            okhttp3.internal.connection.RealCall r8 = r1.call     // Catch:{ all -> 0x01dc }
            boolean r0 = r3.callAcquirePooledConnection(r7, r8, r0, r6)     // Catch:{ all -> 0x01dc }
            if (r0 == 0) goto L_0x01a9
            if (r5 == 0) goto L_0x01a5
            r5.setNoNewExchanges(r6)     // Catch:{ all -> 0x01dc }
            if (r5 == 0) goto L_0x01a1
            java.net.Socket r0 = r5.socket()     // Catch:{ all -> 0x01dc }
            okhttp3.internal.connection.RealCall r3 = r1.call     // Catch:{ all -> 0x01dc }
            okhttp3.internal.connection.RealConnection r5 = r3.getConnection()     // Catch:{ all -> 0x01dc }
            r1.nextRouteToTry = r9     // Catch:{ all -> 0x01dc }
            goto L_0x01b8
        L_0x01a1:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01dc }
            throw r4
        L_0x01a5:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01dc }
            throw r4
        L_0x01a9:
            okhttp3.internal.connection.RealConnectionPool r0 = r1.connectionPool     // Catch:{ all -> 0x01dc }
            if (r5 == 0) goto L_0x01d8
            r0.put(r5)     // Catch:{ all -> 0x01dc }
            okhttp3.internal.connection.RealCall r0 = r1.call     // Catch:{ all -> 0x01dc }
            if (r5 == 0) goto L_0x01d4
            r0.acquireConnectionNoEvents(r5)     // Catch:{ all -> 0x01dc }
            r0 = r4
        L_0x01b8:
            kotlin.Unit r3 = kotlin.Unit.f6917a     // Catch:{ all -> 0x01dc }
            monitor-exit(r2)
            if (r0 == 0) goto L_0x01c0
            okhttp3.internal.Util.closeQuietly((java.net.Socket) r0)
        L_0x01c0:
            okhttp3.EventListener r0 = r1.eventListener
            okhttp3.internal.connection.RealCall r2 = r1.call
            if (r5 == 0) goto L_0x01d0
            r0.connectionAcquired(r2, r5)
            if (r5 == 0) goto L_0x01cc
            return r5
        L_0x01cc:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x01d0:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x01d4:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01dc }
            throw r4
        L_0x01d8:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01dc }
            throw r4
        L_0x01dc:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x01df:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x01e3:
            kotlin.jvm.internal.Intrinsics.a()
            throw r4
        L_0x01e7:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x01ef }
            java.lang.String r3 = "Canceled"
            r0.<init>(r3)     // Catch:{ all -> 0x01ef }
            throw r0     // Catch:{ all -> 0x01ef }
        L_0x01ef:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x01f2:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x01fa }
            java.lang.String r3 = "Canceled"
            r0.<init>(r3)     // Catch:{ all -> 0x01fa }
            throw r0     // Catch:{ all -> 0x01fa }
        L_0x01fa:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.ExchangeFinder.findConnection(int, int, int, int, boolean):okhttp3.internal.connection.RealConnection");
    }

    private final RealConnection findHealthyConnection(int i, int i2, int i3, int i4, boolean z, boolean z2) throws IOException {
        while (true) {
            RealConnection findConnection = findConnection(i, i2, i3, i4, z);
            if (findConnection.isHealthy(z2)) {
                return findConnection;
            }
            findConnection.noNewExchanges();
            synchronized (this.connectionPool) {
                if (this.nextRouteToTry == null) {
                    RouteSelector.Selection selection = this.routeSelection;
                    boolean z3 = true;
                    if (!(selection != null ? selection.hasNext() : true)) {
                        RouteSelector routeSelector2 = this.routeSelector;
                        if (routeSelector2 != null) {
                            z3 = routeSelector2.hasNext();
                        }
                        if (!z3) {
                            throw new IOException("exhausted all routes");
                        }
                    }
                }
                Unit unit = Unit.f6917a;
            }
        }
    }

    private final boolean retryCurrentRoute() {
        RealConnection connection;
        if (this.refusedStreamCount > 1 || this.connectionShutdownCount > 1 || this.otherFailureCount > 0 || (connection = this.call.getConnection()) == null || connection.getRouteFailureCount$okhttp() != 0 || !Util.canReuseConnectionFor(connection.route().address().url(), this.address.url())) {
            return false;
        }
        return true;
    }

    public final RealConnection connectingConnection() {
        RealConnectionPool realConnectionPool = this.connectionPool;
        if (!Util.assertionsEnabled || Thread.holdsLock(realConnectionPool)) {
            return this.connectingConnection;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Thread ");
        Thread currentThread = Thread.currentThread();
        Intrinsics.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        sb.append(" MUST hold lock on ");
        sb.append(realConnectionPool);
        throw new AssertionError(sb.toString());
    }

    public final ExchangeCodec find(OkHttpClient okHttpClient, RealInterceptorChain realInterceptorChain) {
        Intrinsics.b(okHttpClient, "client");
        Intrinsics.b(realInterceptorChain, "chain");
        try {
            return findHealthyConnection(realInterceptorChain.getConnectTimeoutMillis$okhttp(), realInterceptorChain.getReadTimeoutMillis$okhttp(), realInterceptorChain.getWriteTimeoutMillis$okhttp(), okHttpClient.pingIntervalMillis(), okHttpClient.retryOnConnectionFailure(), !Intrinsics.a((Object) realInterceptorChain.getRequest$okhttp().method(), (Object) "GET")).newCodec$okhttp(okHttpClient, realInterceptorChain);
        } catch (RouteException e) {
            trackFailure(e.getLastConnectException());
            throw e;
        } catch (IOException e2) {
            trackFailure(e2);
            throw new RouteException(e2);
        }
    }

    public final Address getAddress$okhttp() {
        return this.address;
    }

    public final boolean retryAfterFailure() {
        synchronized (this.connectionPool) {
            if (this.refusedStreamCount == 0 && this.connectionShutdownCount == 0 && this.otherFailureCount == 0) {
                return false;
            }
            if (this.nextRouteToTry != null) {
                return true;
            }
            if (retryCurrentRoute()) {
                RealConnection connection = this.call.getConnection();
                if (connection != null) {
                    this.nextRouteToTry = connection.route();
                    return true;
                }
                Intrinsics.a();
                throw null;
            }
            RouteSelector.Selection selection = this.routeSelection;
            if (selection != null && selection.hasNext()) {
                return true;
            }
            RouteSelector routeSelector2 = this.routeSelector;
            if (routeSelector2 == null) {
                return true;
            }
            boolean hasNext = routeSelector2.hasNext();
            return hasNext;
        }
    }

    public final boolean sameHostAndPort(HttpUrl httpUrl) {
        Intrinsics.b(httpUrl, ReportDBAdapter.ReportColumns.COLUMN_URL);
        HttpUrl url = this.address.url();
        return httpUrl.port() == url.port() && Intrinsics.a((Object) httpUrl.host(), (Object) url.host());
    }

    public final void trackFailure(IOException iOException) {
        Intrinsics.b(iOException, "e");
        RealConnectionPool realConnectionPool = this.connectionPool;
        if (!Util.assertionsEnabled || !Thread.holdsLock(realConnectionPool)) {
            synchronized (this.connectionPool) {
                this.nextRouteToTry = null;
                if ((iOException instanceof StreamResetException) && ((StreamResetException) iOException).errorCode == ErrorCode.REFUSED_STREAM) {
                    this.refusedStreamCount++;
                } else if (iOException instanceof ConnectionShutdownException) {
                    this.connectionShutdownCount++;
                } else {
                    this.otherFailureCount++;
                }
            }
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Thread ");
        Thread currentThread = Thread.currentThread();
        Intrinsics.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        sb.append(" MUST NOT hold lock on ");
        sb.append(realConnectionPool);
        throw new AssertionError(sb.toString());
    }
}
