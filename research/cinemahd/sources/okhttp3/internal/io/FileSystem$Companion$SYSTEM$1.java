package okhttp3.internal.io;

import com.facebook.common.util.UriUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import kotlin.jvm.internal.Intrinsics;
import okio.Okio;
import okio.Sink;
import okio.Source;

public final class FileSystem$Companion$SYSTEM$1 implements FileSystem {
    FileSystem$Companion$SYSTEM$1() {
    }

    public Sink appendingSink(File file) throws FileNotFoundException {
        Intrinsics.b(file, UriUtil.LOCAL_FILE_SCHEME);
        try {
            return Okio.a(file);
        } catch (FileNotFoundException unused) {
            file.getParentFile().mkdirs();
            return Okio.a(file);
        }
    }

    public void delete(File file) throws IOException {
        Intrinsics.b(file, UriUtil.LOCAL_FILE_SCHEME);
        if (!file.delete() && file.exists()) {
            throw new IOException("failed to delete " + file);
        }
    }

    public void deleteContents(File file) throws IOException {
        Intrinsics.b(file, "directory");
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            int length = listFiles.length;
            int i = 0;
            while (i < length) {
                File file2 = listFiles[i];
                Intrinsics.a((Object) file2, UriUtil.LOCAL_FILE_SCHEME);
                if (file2.isDirectory()) {
                    deleteContents(file2);
                }
                if (file2.delete()) {
                    i++;
                } else {
                    throw new IOException("failed to delete " + file2);
                }
            }
            return;
        }
        throw new IOException("not a readable directory: " + file);
    }

    public boolean exists(File file) {
        Intrinsics.b(file, UriUtil.LOCAL_FILE_SCHEME);
        return file.exists();
    }

    public void rename(File file, File file2) throws IOException {
        Intrinsics.b(file, "from");
        Intrinsics.b(file2, "to");
        delete(file2);
        if (!file.renameTo(file2)) {
            throw new IOException("failed to rename " + file + " to " + file2);
        }
    }

    public Sink sink(File file) throws FileNotFoundException {
        Intrinsics.b(file, UriUtil.LOCAL_FILE_SCHEME);
        try {
            return Okio__JvmOkioKt.a(file, false, 1, (Object) null);
        } catch (FileNotFoundException unused) {
            file.getParentFile().mkdirs();
            return Okio__JvmOkioKt.a(file, false, 1, (Object) null);
        }
    }

    public long size(File file) {
        Intrinsics.b(file, UriUtil.LOCAL_FILE_SCHEME);
        return file.length();
    }

    public Source source(File file) throws FileNotFoundException {
        Intrinsics.b(file, UriUtil.LOCAL_FILE_SCHEME);
        return Okio.c(file);
    }

    public String toString() {
        return "FileSystem.SYSTEM";
    }
}
