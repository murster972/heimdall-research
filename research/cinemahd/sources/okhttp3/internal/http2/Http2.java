package okhttp3.internal.http2;

import kotlin.jvm.internal.Intrinsics;
import okhttp3.internal.Util;
import okio.ByteString;

public final class Http2 {
    private static final String[] BINARY;
    public static final ByteString CONNECTION_PREFACE = ByteString.d.c("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");
    private static final String[] FLAGS = new String[64];
    public static final int FLAG_ACK = 1;
    public static final int FLAG_COMPRESSED = 32;
    public static final int FLAG_END_HEADERS = 4;
    public static final int FLAG_END_PUSH_PROMISE = 4;
    public static final int FLAG_END_STREAM = 1;
    public static final int FLAG_NONE = 0;
    public static final int FLAG_PADDED = 8;
    public static final int FLAG_PRIORITY = 32;
    private static final String[] FRAME_NAMES = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};
    public static final int INITIAL_MAX_FRAME_SIZE = 16384;
    public static final Http2 INSTANCE = new Http2();
    public static final int TYPE_CONTINUATION = 9;
    public static final int TYPE_DATA = 0;
    public static final int TYPE_GOAWAY = 7;
    public static final int TYPE_HEADERS = 1;
    public static final int TYPE_PING = 6;
    public static final int TYPE_PRIORITY = 2;
    public static final int TYPE_PUSH_PROMISE = 5;
    public static final int TYPE_RST_STREAM = 3;
    public static final int TYPE_SETTINGS = 4;
    public static final int TYPE_WINDOW_UPDATE = 8;

    static {
        String[] strArr = new String[256];
        for (int i = 0; i < 256; i++) {
            String binaryString = Integer.toBinaryString(i);
            Intrinsics.a((Object) binaryString, "Integer.toBinaryString(it)");
            strArr[i] = StringsKt__StringsJVMKt.a(Util.format("%8s", binaryString), ' ', '0', false, 4, (Object) null);
        }
        BINARY = strArr;
        String[] strArr2 = FLAGS;
        strArr2[0] = "";
        strArr2[1] = "END_STREAM";
        int[] iArr = {1};
        strArr2[8] = "PADDED";
        for (int i2 : iArr) {
            String[] strArr3 = FLAGS;
            strArr3[i2 | 8] = Intrinsics.a(strArr3[i2], (Object) "|PADDED");
        }
        String[] strArr4 = FLAGS;
        strArr4[4] = "END_HEADERS";
        strArr4[32] = "PRIORITY";
        strArr4[36] = "END_HEADERS|PRIORITY";
        for (int i3 : new int[]{4, 32, 36}) {
            for (int i4 : iArr) {
                int i5 = i4 | i3;
                FLAGS[i5] = FLAGS[i4] + "|" + FLAGS[i3];
                FLAGS[i5 | 8] = FLAGS[i4] + "|" + FLAGS[i3] + "|PADDED";
            }
        }
        int length = FLAGS.length;
        for (int i6 = 0; i6 < length; i6++) {
            String[] strArr5 = FLAGS;
            if (strArr5[i6] == null) {
                strArr5[i6] = BINARY[i6];
            }
        }
    }

    private Http2() {
    }

    public final String formatFlags(int i, int i2) {
        String str;
        if (i2 == 0) {
            return "";
        }
        if (!(i == 2 || i == 3)) {
            if (i == 4 || i == 6) {
                if (i2 == 1) {
                    return "ACK";
                }
                return BINARY[i2];
            } else if (!(i == 7 || i == 8)) {
                String[] strArr = FLAGS;
                if (i2 < strArr.length) {
                    str = strArr[i2];
                    if (str == null) {
                        Intrinsics.a();
                        throw null;
                    }
                } else {
                    str = BINARY[i2];
                }
                String str2 = str;
                if (i != 5 || (i2 & 4) == 0) {
                    return (i != 0 || (i2 & 32) == 0) ? str2 : StringsKt__StringsJVMKt.a(str2, "PRIORITY", "COMPRESSED", false, 4, (Object) null);
                }
                return StringsKt__StringsJVMKt.a(str2, "HEADERS", "PUSH_PROMISE", false, 4, (Object) null);
            }
        }
        return BINARY[i2];
    }

    public final String frameLog(boolean z, int i, int i2, int i3, int i4) {
        String[] strArr = FRAME_NAMES;
        return Util.format("%s 0x%08x %5d %-13s %s", z ? "<<" : ">>", Integer.valueOf(i), Integer.valueOf(i2), i3 < strArr.length ? strArr[i3] : Util.format("0x%02x", Integer.valueOf(i3)), formatFlags(i3, i4));
    }
}
