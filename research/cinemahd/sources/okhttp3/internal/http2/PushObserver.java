package okhttp3.internal.http2;

import java.io.IOException;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;
import okio.BufferedSource;

public interface PushObserver {
    public static final PushObserver CANCEL = new Companion.PushObserverCancel();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);

    public static final class Companion {
        static final /* synthetic */ Companion $$INSTANCE = null;

        private static final class PushObserverCancel implements PushObserver {
            public boolean onData(int i, BufferedSource bufferedSource, int i2, boolean z) throws IOException {
                Intrinsics.b(bufferedSource, "source");
                bufferedSource.skip((long) i2);
                return true;
            }

            public boolean onHeaders(int i, List<Header> list, boolean z) {
                Intrinsics.b(list, "responseHeaders");
                return true;
            }

            public boolean onRequest(int i, List<Header> list) {
                Intrinsics.b(list, "requestHeaders");
                return true;
            }

            public void onReset(int i, ErrorCode errorCode) {
                Intrinsics.b(errorCode, "errorCode");
            }
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    boolean onData(int i, BufferedSource bufferedSource, int i2, boolean z) throws IOException;

    boolean onHeaders(int i, List<Header> list, boolean z);

    boolean onRequest(int i, List<Header> list);

    void onReset(int i, ErrorCode errorCode);
}
