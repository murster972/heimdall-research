package okhttp3.internal.http2;

import com.unity3d.ads.metadata.MediationMetaData;
import kotlin.jvm.internal.Intrinsics;
import okio.ByteString;

public final class Header {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final ByteString PSEUDO_PREFIX = ByteString.d.c(":");
    public static final ByteString RESPONSE_STATUS = ByteString.d.c(RESPONSE_STATUS_UTF8);
    public static final String RESPONSE_STATUS_UTF8 = ":status";
    public static final ByteString TARGET_AUTHORITY = ByteString.d.c(TARGET_AUTHORITY_UTF8);
    public static final String TARGET_AUTHORITY_UTF8 = ":authority";
    public static final ByteString TARGET_METHOD = ByteString.d.c(TARGET_METHOD_UTF8);
    public static final String TARGET_METHOD_UTF8 = ":method";
    public static final ByteString TARGET_PATH = ByteString.d.c(TARGET_PATH_UTF8);
    public static final String TARGET_PATH_UTF8 = ":path";
    public static final ByteString TARGET_SCHEME = ByteString.d.c(TARGET_SCHEME_UTF8);
    public static final String TARGET_SCHEME_UTF8 = ":scheme";
    public final int hpackSize;
    public final ByteString name;
    public final ByteString value;

    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public Header(ByteString byteString, ByteString byteString2) {
        Intrinsics.b(byteString, MediationMetaData.KEY_NAME);
        Intrinsics.b(byteString2, "value");
        this.name = byteString;
        this.value = byteString2;
        this.hpackSize = this.name.size() + 32 + this.value.size();
    }

    public static /* synthetic */ Header copy$default(Header header, ByteString byteString, ByteString byteString2, int i, Object obj) {
        if ((i & 1) != 0) {
            byteString = header.name;
        }
        if ((i & 2) != 0) {
            byteString2 = header.value;
        }
        return header.copy(byteString, byteString2);
    }

    public final ByteString component1() {
        return this.name;
    }

    public final ByteString component2() {
        return this.value;
    }

    public final Header copy(ByteString byteString, ByteString byteString2) {
        Intrinsics.b(byteString, MediationMetaData.KEY_NAME);
        Intrinsics.b(byteString2, "value");
        return new Header(byteString, byteString2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Header)) {
            return false;
        }
        Header header = (Header) obj;
        return Intrinsics.a((Object) this.name, (Object) header.name) && Intrinsics.a((Object) this.value, (Object) header.value);
    }

    public int hashCode() {
        ByteString byteString = this.name;
        int i = 0;
        int hashCode = (byteString != null ? byteString.hashCode() : 0) * 31;
        ByteString byteString2 = this.value;
        if (byteString2 != null) {
            i = byteString2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return this.name.m() + ": " + this.value.m();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Header(String str, String str2) {
        this(ByteString.d.c(str), ByteString.d.c(str2));
        Intrinsics.b(str, MediationMetaData.KEY_NAME);
        Intrinsics.b(str2, "value");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Header(ByteString byteString, String str) {
        this(byteString, ByteString.d.c(str));
        Intrinsics.b(byteString, MediationMetaData.KEY_NAME);
        Intrinsics.b(str, "value");
    }
}
