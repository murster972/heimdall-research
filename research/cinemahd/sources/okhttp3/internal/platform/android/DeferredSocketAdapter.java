package okhttp3.internal.platform.android;

import java.util.List;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Protocol;
import okhttp3.internal.platform.Platform;
import okhttp3.internal.platform.android.SocketAdapter;

public final class DeferredSocketAdapter implements SocketAdapter {
    private SocketAdapter delegate;
    private boolean initialized;
    private final String socketPackage;

    public DeferredSocketAdapter(String str) {
        Intrinsics.b(str, "socketPackage");
        this.socketPackage = str;
    }

    private final synchronized SocketAdapter getDelegate(SSLSocket sSLSocket) {
        if (!this.initialized) {
            try {
                Class cls = sSLSocket.getClass();
                while (true) {
                    String name = cls.getName();
                    if (!(!Intrinsics.a((Object) name, (Object) this.socketPackage + ".OpenSSLSocketImpl"))) {
                        this.delegate = new AndroidSocketAdapter(cls);
                        break;
                    }
                    cls = cls.getSuperclass();
                    Intrinsics.a((Object) cls, "possibleClass.superclass");
                    if (cls == null) {
                        throw new AssertionError("No OpenSSLSocketImpl superclass of socket of type " + sSLSocket);
                    }
                }
            } catch (Exception e) {
                Platform platform = Platform.Companion.get();
                platform.log("Failed to initialize DeferredSocketAdapter " + this.socketPackage, 5, e);
            }
            this.initialized = true;
        }
        return this.delegate;
    }

    public void configureTlsExtensions(SSLSocket sSLSocket, String str, List<? extends Protocol> list) {
        Intrinsics.b(sSLSocket, "sslSocket");
        Intrinsics.b(list, "protocols");
        SocketAdapter delegate2 = getDelegate(sSLSocket);
        if (delegate2 != null) {
            delegate2.configureTlsExtensions(sSLSocket, str, list);
        }
    }

    public String getSelectedProtocol(SSLSocket sSLSocket) {
        Intrinsics.b(sSLSocket, "sslSocket");
        SocketAdapter delegate2 = getDelegate(sSLSocket);
        if (delegate2 != null) {
            return delegate2.getSelectedProtocol(sSLSocket);
        }
        return null;
    }

    public boolean isSupported() {
        return true;
    }

    public boolean matchesSocket(SSLSocket sSLSocket) {
        Intrinsics.b(sSLSocket, "sslSocket");
        String name = sSLSocket.getClass().getName();
        Intrinsics.a((Object) name, "sslSocket.javaClass.name");
        return StringsKt__StringsJVMKt.b(name, this.socketPackage, false, 2, (Object) null);
    }

    public boolean matchesSocketFactory(SSLSocketFactory sSLSocketFactory) {
        Intrinsics.b(sSLSocketFactory, "sslSocketFactory");
        return SocketAdapter.DefaultImpls.matchesSocketFactory(this, sSLSocketFactory);
    }

    public X509TrustManager trustManager(SSLSocketFactory sSLSocketFactory) {
        Intrinsics.b(sSLSocketFactory, "sslSocketFactory");
        return null;
    }
}
