package okhttp3.internal.platform;

import javax.net.ssl.SSLSession;
import org.conscrypt.ConscryptHostnameVerifier;

final class ConscryptPlatform$platformTrustManager$2 implements ConscryptHostnameVerifier {
    public static final ConscryptPlatform$platformTrustManager$2 INSTANCE = new ConscryptPlatform$platformTrustManager$2();

    ConscryptPlatform$platformTrustManager$2() {
    }

    public final boolean verify(String str, SSLSession sSLSession) {
        return true;
    }
}
