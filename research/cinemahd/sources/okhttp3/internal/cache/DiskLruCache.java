package okhttp3.internal.cache;

import com.facebook.cache.disk.DefaultDiskStorage;
import com.vungle.warren.model.CookieDBAdapter;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Flushable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Regex;
import okhttp3.internal.Util;
import okhttp3.internal.concurrent.TaskQueue;
import okhttp3.internal.concurrent.TaskRunner;
import okhttp3.internal.io.FileSystem;
import okhttp3.internal.platform.Platform;
import okio.BufferedSink;
import okio.Okio;
import okio.Sink;
import okio.Source;

public final class DiskLruCache implements Closeable, Flushable {
    public static final long ANY_SEQUENCE_NUMBER = -1;
    public static final String CLEAN = CLEAN;
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String DIRTY = DIRTY;
    public static final String JOURNAL_FILE = JOURNAL_FILE;
    public static final String JOURNAL_FILE_BACKUP = JOURNAL_FILE_BACKUP;
    public static final String JOURNAL_FILE_TEMP = JOURNAL_FILE_TEMP;
    public static final Regex LEGAL_KEY_PATTERN = new Regex("[a-z0-9_-]{1,120}");
    public static final String MAGIC = MAGIC;
    public static final String READ = READ;
    public static final String REMOVE = REMOVE;
    public static final String VERSION_1 = VERSION_1;
    private final int appVersion;
    /* access modifiers changed from: private */
    public boolean civilizedFileSystem;
    private final TaskQueue cleanupQueue;
    private final DiskLruCache$cleanupTask$1 cleanupTask;
    private boolean closed;
    private final File directory;
    private final FileSystem fileSystem;
    /* access modifiers changed from: private */
    public boolean hasJournalErrors;
    /* access modifiers changed from: private */
    public boolean initialized;
    private final File journalFile;
    private final File journalFileBackup;
    private final File journalFileTmp;
    /* access modifiers changed from: private */
    public BufferedSink journalWriter;
    private final LinkedHashMap<String, Entry> lruEntries = new LinkedHashMap<>(0, 0.75f, true);
    private long maxSize;
    /* access modifiers changed from: private */
    public boolean mostRecentRebuildFailed;
    /* access modifiers changed from: private */
    public boolean mostRecentTrimFailed;
    private long nextSequenceNumber;
    /* access modifiers changed from: private */
    public int redundantOpCount;
    private long size;
    private final int valueCount;

    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public final class Editor {
        private boolean done;
        private final Entry entry;
        final /* synthetic */ DiskLruCache this$0;
        private final boolean[] written;

        public Editor(DiskLruCache diskLruCache, Entry entry2) {
            Intrinsics.b(entry2, "entry");
            this.this$0 = diskLruCache;
            this.entry = entry2;
            this.written = this.entry.getReadable$okhttp() ? null : new boolean[diskLruCache.getValueCount$okhttp()];
        }

        public final void abort() throws IOException {
            synchronized (this.this$0) {
                if (!this.done) {
                    if (Intrinsics.a((Object) this.entry.getCurrentEditor$okhttp(), (Object) this)) {
                        this.this$0.completeEdit$okhttp(this, false);
                    }
                    this.done = true;
                    Unit unit = Unit.f6917a;
                } else {
                    throw new IllegalStateException("Check failed.".toString());
                }
            }
        }

        public final void commit() throws IOException {
            synchronized (this.this$0) {
                if (!this.done) {
                    if (Intrinsics.a((Object) this.entry.getCurrentEditor$okhttp(), (Object) this)) {
                        this.this$0.completeEdit$okhttp(this, true);
                    }
                    this.done = true;
                    Unit unit = Unit.f6917a;
                } else {
                    throw new IllegalStateException("Check failed.".toString());
                }
            }
        }

        public final void detach$okhttp() {
            if (!Intrinsics.a((Object) this.entry.getCurrentEditor$okhttp(), (Object) this)) {
                return;
            }
            if (this.this$0.civilizedFileSystem) {
                this.this$0.completeEdit$okhttp(this, false);
            } else {
                this.entry.setZombie$okhttp(true);
            }
        }

        public final Entry getEntry$okhttp() {
            return this.entry;
        }

        public final boolean[] getWritten$okhttp() {
            return this.written;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
            r5 = okio.Okio.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0057, code lost:
            return r5;
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0052 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final okio.Sink newSink(int r5) {
            /*
                r4 = this;
                okhttp3.internal.cache.DiskLruCache r0 = r4.this$0
                monitor-enter(r0)
                boolean r1 = r4.done     // Catch:{ all -> 0x0064 }
                r2 = 1
                r1 = r1 ^ r2
                if (r1 == 0) goto L_0x0058
                okhttp3.internal.cache.DiskLruCache$Entry r1 = r4.entry     // Catch:{ all -> 0x0064 }
                okhttp3.internal.cache.DiskLruCache$Editor r1 = r1.getCurrentEditor$okhttp()     // Catch:{ all -> 0x0064 }
                boolean r1 = kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r1, (java.lang.Object) r4)     // Catch:{ all -> 0x0064 }
                r1 = r1 ^ r2
                if (r1 == 0) goto L_0x001c
                okio.Sink r5 = okio.Okio.a()     // Catch:{ all -> 0x0064 }
                monitor-exit(r0)
                return r5
            L_0x001c:
                okhttp3.internal.cache.DiskLruCache$Entry r1 = r4.entry     // Catch:{ all -> 0x0064 }
                boolean r1 = r1.getReadable$okhttp()     // Catch:{ all -> 0x0064 }
                if (r1 != 0) goto L_0x0030
                boolean[] r1 = r4.written     // Catch:{ all -> 0x0064 }
                if (r1 == 0) goto L_0x002b
                r1[r5] = r2     // Catch:{ all -> 0x0064 }
                goto L_0x0030
            L_0x002b:
                kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x0064 }
                r5 = 0
                throw r5
            L_0x0030:
                okhttp3.internal.cache.DiskLruCache$Entry r1 = r4.entry     // Catch:{ all -> 0x0064 }
                java.util.List r1 = r1.getDirtyFiles$okhttp()     // Catch:{ all -> 0x0064 }
                java.lang.Object r1 = r1.get(r5)     // Catch:{ all -> 0x0064 }
                java.io.File r1 = (java.io.File) r1     // Catch:{ all -> 0x0064 }
                okhttp3.internal.cache.DiskLruCache r2 = r4.this$0     // Catch:{ FileNotFoundException -> 0x0052 }
                okhttp3.internal.io.FileSystem r2 = r2.getFileSystem$okhttp()     // Catch:{ FileNotFoundException -> 0x0052 }
                okio.Sink r1 = r2.sink(r1)     // Catch:{ FileNotFoundException -> 0x0052 }
                okhttp3.internal.cache.FaultHidingSink r2 = new okhttp3.internal.cache.FaultHidingSink     // Catch:{ all -> 0x0064 }
                okhttp3.internal.cache.DiskLruCache$Editor$newSink$$inlined$synchronized$lambda$1 r3 = new okhttp3.internal.cache.DiskLruCache$Editor$newSink$$inlined$synchronized$lambda$1     // Catch:{ all -> 0x0064 }
                r3.<init>(r4, r5)     // Catch:{ all -> 0x0064 }
                r2.<init>(r1, r3)     // Catch:{ all -> 0x0064 }
                monitor-exit(r0)
                return r2
            L_0x0052:
                okio.Sink r5 = okio.Okio.a()     // Catch:{ all -> 0x0064 }
                monitor-exit(r0)
                return r5
            L_0x0058:
                java.lang.String r5 = "Check failed."
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0064 }
                java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0064 }
                r1.<init>(r5)     // Catch:{ all -> 0x0064 }
                throw r1     // Catch:{ all -> 0x0064 }
            L_0x0064:
                r5 = move-exception
                monitor-exit(r0)
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.Editor.newSink(int):okio.Sink");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0042, code lost:
            return null;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final okio.Source newSource(int r5) {
            /*
                r4 = this;
                okhttp3.internal.cache.DiskLruCache r0 = r4.this$0
                monitor-enter(r0)
                boolean r1 = r4.done     // Catch:{ all -> 0x004f }
                r1 = r1 ^ 1
                if (r1 == 0) goto L_0x0043
                okhttp3.internal.cache.DiskLruCache$Entry r1 = r4.entry     // Catch:{ all -> 0x004f }
                boolean r1 = r1.getReadable$okhttp()     // Catch:{ all -> 0x004f }
                r2 = 0
                if (r1 == 0) goto L_0x0041
                okhttp3.internal.cache.DiskLruCache$Entry r1 = r4.entry     // Catch:{ all -> 0x004f }
                okhttp3.internal.cache.DiskLruCache$Editor r1 = r1.getCurrentEditor$okhttp()     // Catch:{ all -> 0x004f }
                boolean r1 = kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r1, (java.lang.Object) r4)     // Catch:{ all -> 0x004f }
                r1 = r1 ^ 1
                if (r1 != 0) goto L_0x0041
                okhttp3.internal.cache.DiskLruCache$Entry r1 = r4.entry     // Catch:{ all -> 0x004f }
                boolean r1 = r1.getZombie$okhttp()     // Catch:{ all -> 0x004f }
                if (r1 == 0) goto L_0x0029
                goto L_0x0041
            L_0x0029:
                okhttp3.internal.cache.DiskLruCache r1 = r4.this$0     // Catch:{ FileNotFoundException -> 0x003f }
                okhttp3.internal.io.FileSystem r1 = r1.getFileSystem$okhttp()     // Catch:{ FileNotFoundException -> 0x003f }
                okhttp3.internal.cache.DiskLruCache$Entry r3 = r4.entry     // Catch:{ FileNotFoundException -> 0x003f }
                java.util.List r3 = r3.getCleanFiles$okhttp()     // Catch:{ FileNotFoundException -> 0x003f }
                java.lang.Object r5 = r3.get(r5)     // Catch:{ FileNotFoundException -> 0x003f }
                java.io.File r5 = (java.io.File) r5     // Catch:{ FileNotFoundException -> 0x003f }
                okio.Source r2 = r1.source(r5)     // Catch:{ FileNotFoundException -> 0x003f }
            L_0x003f:
                monitor-exit(r0)
                return r2
            L_0x0041:
                monitor-exit(r0)
                return r2
            L_0x0043:
                java.lang.String r5 = "Check failed."
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x004f }
                java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x004f }
                r1.<init>(r5)     // Catch:{ all -> 0x004f }
                throw r1     // Catch:{ all -> 0x004f }
            L_0x004f:
                r5 = move-exception
                monitor-exit(r0)
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.Editor.newSource(int):okio.Source");
        }
    }

    public final class Entry {
        private final List<File> cleanFiles = new ArrayList();
        private Editor currentEditor;
        private final List<File> dirtyFiles = new ArrayList();
        private final String key;
        private final long[] lengths;
        private int lockingSourceCount;
        private boolean readable;
        private long sequenceNumber;
        final /* synthetic */ DiskLruCache this$0;
        private boolean zombie;

        public Entry(DiskLruCache diskLruCache, String str) {
            Intrinsics.b(str, "key");
            this.this$0 = diskLruCache;
            this.key = str;
            this.lengths = new long[diskLruCache.getValueCount$okhttp()];
            StringBuilder sb = new StringBuilder(this.key);
            sb.append('.');
            int length = sb.length();
            int valueCount$okhttp = diskLruCache.getValueCount$okhttp();
            for (int i = 0; i < valueCount$okhttp; i++) {
                sb.append(i);
                this.cleanFiles.add(new File(diskLruCache.getDirectory(), sb.toString()));
                sb.append(DefaultDiskStorage.FileType.TEMP);
                this.dirtyFiles.add(new File(diskLruCache.getDirectory(), sb.toString()));
                sb.setLength(length);
            }
        }

        private final Void invalidLengths(List<String> list) throws IOException {
            throw new IOException("unexpected journal line: " + list);
        }

        private final Source newSource(int i) {
            Source source = this.this$0.getFileSystem$okhttp().source(this.cleanFiles.get(i));
            if (this.this$0.civilizedFileSystem) {
                return source;
            }
            this.lockingSourceCount++;
            return new DiskLruCache$Entry$newSource$1(this, source, source);
        }

        public final List<File> getCleanFiles$okhttp() {
            return this.cleanFiles;
        }

        public final Editor getCurrentEditor$okhttp() {
            return this.currentEditor;
        }

        public final List<File> getDirtyFiles$okhttp() {
            return this.dirtyFiles;
        }

        public final String getKey$okhttp() {
            return this.key;
        }

        public final long[] getLengths$okhttp() {
            return this.lengths;
        }

        public final int getLockingSourceCount$okhttp() {
            return this.lockingSourceCount;
        }

        public final boolean getReadable$okhttp() {
            return this.readable;
        }

        public final long getSequenceNumber$okhttp() {
            return this.sequenceNumber;
        }

        public final boolean getZombie$okhttp() {
            return this.zombie;
        }

        public final void setCurrentEditor$okhttp(Editor editor) {
            this.currentEditor = editor;
        }

        public final void setLengths$okhttp(List<String> list) throws IOException {
            Intrinsics.b(list, CookieDBAdapter.CookieColumns.COLUMN_STRINGS);
            if (list.size() == this.this$0.getValueCount$okhttp()) {
                try {
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        this.lengths[i] = Long.parseLong(list.get(i));
                    }
                } catch (NumberFormatException unused) {
                    invalidLengths(list);
                    throw null;
                }
            } else {
                invalidLengths(list);
                throw null;
            }
        }

        public final void setLockingSourceCount$okhttp(int i) {
            this.lockingSourceCount = i;
        }

        public final void setReadable$okhttp(boolean z) {
            this.readable = z;
        }

        public final void setSequenceNumber$okhttp(long j) {
            this.sequenceNumber = j;
        }

        public final void setZombie$okhttp(boolean z) {
            this.zombie = z;
        }

        public final Snapshot snapshot$okhttp() {
            DiskLruCache diskLruCache = this.this$0;
            if (Util.assertionsEnabled && !Thread.holdsLock(diskLruCache)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Thread ");
                Thread currentThread = Thread.currentThread();
                Intrinsics.a((Object) currentThread, "Thread.currentThread()");
                sb.append(currentThread.getName());
                sb.append(" MUST hold lock on ");
                sb.append(diskLruCache);
                throw new AssertionError(sb.toString());
            } else if (!this.readable) {
                return null;
            } else {
                if (!this.this$0.civilizedFileSystem && (this.currentEditor != null || this.zombie)) {
                    return null;
                }
                ArrayList<Source> arrayList = new ArrayList<>();
                long[] jArr = (long[]) this.lengths.clone();
                try {
                    int valueCount$okhttp = this.this$0.getValueCount$okhttp();
                    for (int i = 0; i < valueCount$okhttp; i++) {
                        arrayList.add(newSource(i));
                    }
                    return new Snapshot(this.this$0, this.key, this.sequenceNumber, arrayList, jArr);
                } catch (FileNotFoundException unused) {
                    for (Source closeQuietly : arrayList) {
                        Util.closeQuietly((Closeable) closeQuietly);
                    }
                    try {
                        this.this$0.removeEntry$okhttp(this);
                    } catch (IOException unused2) {
                    }
                    return null;
                }
            }
        }

        public final void writeLengths$okhttp(BufferedSink bufferedSink) throws IOException {
            Intrinsics.b(bufferedSink, "writer");
            for (long e : this.lengths) {
                bufferedSink.writeByte(32).e(e);
            }
        }
    }

    public final class Snapshot implements Closeable {
        private final String key;
        private final long[] lengths;
        private final long sequenceNumber;
        private final List<Source> sources;
        final /* synthetic */ DiskLruCache this$0;

        public Snapshot(DiskLruCache diskLruCache, String str, long j, List<? extends Source> list, long[] jArr) {
            Intrinsics.b(str, "key");
            Intrinsics.b(list, "sources");
            Intrinsics.b(jArr, "lengths");
            this.this$0 = diskLruCache;
            this.key = str;
            this.sequenceNumber = j;
            this.sources = list;
            this.lengths = jArr;
        }

        public void close() {
            for (Source closeQuietly : this.sources) {
                Util.closeQuietly((Closeable) closeQuietly);
            }
        }

        public final Editor edit() throws IOException {
            return this.this$0.edit(this.key, this.sequenceNumber);
        }

        public final long getLength(int i) {
            return this.lengths[i];
        }

        public final Source getSource(int i) {
            return this.sources.get(i);
        }

        public final String key() {
            return this.key;
        }
    }

    public DiskLruCache(FileSystem fileSystem2, File file, int i, int i2, long j, TaskRunner taskRunner) {
        Intrinsics.b(fileSystem2, "fileSystem");
        Intrinsics.b(file, "directory");
        Intrinsics.b(taskRunner, "taskRunner");
        this.fileSystem = fileSystem2;
        this.directory = file;
        this.appVersion = i;
        this.valueCount = i2;
        this.maxSize = j;
        boolean z = true;
        this.cleanupQueue = taskRunner.newQueue();
        this.cleanupTask = new DiskLruCache$cleanupTask$1(this, Util.okHttpName + " Cache");
        if (j > 0) {
            if (this.valueCount <= 0 ? false : z) {
                this.journalFile = new File(this.directory, JOURNAL_FILE);
                this.journalFileTmp = new File(this.directory, JOURNAL_FILE_TEMP);
                this.journalFileBackup = new File(this.directory, JOURNAL_FILE_BACKUP);
                return;
            }
            throw new IllegalArgumentException("valueCount <= 0".toString());
        }
        throw new IllegalArgumentException("maxSize <= 0".toString());
    }

    private final synchronized void checkNotClosed() {
        if (!(!this.closed)) {
            throw new IllegalStateException("cache is closed".toString());
        }
    }

    public static /* synthetic */ Editor edit$default(DiskLruCache diskLruCache, String str, long j, int i, Object obj) throws IOException {
        if ((i & 2) != 0) {
            j = ANY_SEQUENCE_NUMBER;
        }
        return diskLruCache.edit(str, j);
    }

    /* access modifiers changed from: private */
    public final boolean journalRebuildRequired() {
        int i = this.redundantOpCount;
        return i >= 2000 && i >= this.lruEntries.size();
    }

    private final BufferedSink newJournalWriter() throws FileNotFoundException {
        return Okio.a((Sink) new FaultHidingSink(this.fileSystem.appendingSink(this.journalFile), new DiskLruCache$newJournalWriter$faultHidingSink$1(this)));
    }

    private final void processJournal() throws IOException {
        this.fileSystem.delete(this.journalFileTmp);
        Iterator<Entry> it2 = this.lruEntries.values().iterator();
        while (it2.hasNext()) {
            Entry next = it2.next();
            Intrinsics.a((Object) next, "i.next()");
            Entry entry = next;
            int i = 0;
            if (entry.getCurrentEditor$okhttp() == null) {
                int i2 = this.valueCount;
                while (i < i2) {
                    this.size += entry.getLengths$okhttp()[i];
                    i++;
                }
            } else {
                entry.setCurrentEditor$okhttp((Editor) null);
                int i3 = this.valueCount;
                while (i < i3) {
                    this.fileSystem.delete(entry.getCleanFiles$okhttp().get(i));
                    this.fileSystem.delete(entry.getDirtyFiles$okhttp().get(i));
                    i++;
                }
                it2.remove();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:18|19|(1:21)(1:22)|23|24|25) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r9.redundantOpCount = r7 - r9.lruEntries.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0071, code lost:
        if (r1.e() == false) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0073, code lost:
        rebuildJournal$okhttp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0077, code lost:
        r9.journalWriter = newJournalWriter();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007d, code lost:
        r0 = kotlin.Unit.f6917a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007f, code lost:
        kotlin.io.CloseableKt.a(r1, (java.lang.Throwable) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0083, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b4, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b5, code lost:
        kotlin.io.CloseableKt.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b8, code lost:
        throw r2;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0064 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0064=Splitter:B:18:0x0064, B:26:0x0084=Splitter:B:26:0x0084} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void readJournal() throws java.io.IOException {
        /*
            r9 = this;
            java.lang.String r0 = ", "
            okhttp3.internal.io.FileSystem r1 = r9.fileSystem
            java.io.File r2 = r9.journalFile
            okio.Source r1 = r1.source(r2)
            okio.BufferedSource r1 = okio.Okio.a((okio.Source) r1)
            java.lang.String r2 = r1.c()     // Catch:{ all -> 0x00b2 }
            java.lang.String r3 = r1.c()     // Catch:{ all -> 0x00b2 }
            java.lang.String r4 = r1.c()     // Catch:{ all -> 0x00b2 }
            java.lang.String r5 = r1.c()     // Catch:{ all -> 0x00b2 }
            java.lang.String r6 = r1.c()     // Catch:{ all -> 0x00b2 }
            java.lang.String r7 = MAGIC     // Catch:{ all -> 0x00b2 }
            boolean r7 = kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r7, (java.lang.Object) r2)     // Catch:{ all -> 0x00b2 }
            r8 = 1
            r7 = r7 ^ r8
            if (r7 != 0) goto L_0x0084
            java.lang.String r7 = VERSION_1     // Catch:{ all -> 0x00b2 }
            boolean r7 = kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r7, (java.lang.Object) r3)     // Catch:{ all -> 0x00b2 }
            r7 = r7 ^ r8
            if (r7 != 0) goto L_0x0084
            int r7 = r9.appVersion     // Catch:{ all -> 0x00b2 }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ all -> 0x00b2 }
            boolean r4 = kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r7, (java.lang.Object) r4)     // Catch:{ all -> 0x00b2 }
            r4 = r4 ^ r8
            if (r4 != 0) goto L_0x0084
            int r4 = r9.valueCount     // Catch:{ all -> 0x00b2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ all -> 0x00b2 }
            boolean r4 = kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r4, (java.lang.Object) r5)     // Catch:{ all -> 0x00b2 }
            r4 = r4 ^ r8
            if (r4 != 0) goto L_0x0084
            int r4 = r6.length()     // Catch:{ all -> 0x00b2 }
            r7 = 0
            if (r4 <= 0) goto L_0x0057
            goto L_0x0058
        L_0x0057:
            r8 = 0
        L_0x0058:
            if (r8 != 0) goto L_0x0084
        L_0x005a:
            java.lang.String r0 = r1.c()     // Catch:{ EOFException -> 0x0064 }
            r9.readJournalLine(r0)     // Catch:{ EOFException -> 0x0064 }
            int r7 = r7 + 1
            goto L_0x005a
        L_0x0064:
            java.util.LinkedHashMap<java.lang.String, okhttp3.internal.cache.DiskLruCache$Entry> r0 = r9.lruEntries     // Catch:{ all -> 0x00b2 }
            int r0 = r0.size()     // Catch:{ all -> 0x00b2 }
            int r7 = r7 - r0
            r9.redundantOpCount = r7     // Catch:{ all -> 0x00b2 }
            boolean r0 = r1.e()     // Catch:{ all -> 0x00b2 }
            if (r0 != 0) goto L_0x0077
            r9.rebuildJournal$okhttp()     // Catch:{ all -> 0x00b2 }
            goto L_0x007d
        L_0x0077:
            okio.BufferedSink r0 = r9.newJournalWriter()     // Catch:{ all -> 0x00b2 }
            r9.journalWriter = r0     // Catch:{ all -> 0x00b2 }
        L_0x007d:
            kotlin.Unit r0 = kotlin.Unit.f6917a     // Catch:{ all -> 0x00b2 }
            r0 = 0
            kotlin.io.CloseableKt.a(r1, r0)
            return
        L_0x0084:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ all -> 0x00b2 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b2 }
            r7.<init>()     // Catch:{ all -> 0x00b2 }
            java.lang.String r8 = "unexpected journal header: ["
            r7.append(r8)     // Catch:{ all -> 0x00b2 }
            r7.append(r2)     // Catch:{ all -> 0x00b2 }
            r7.append(r0)     // Catch:{ all -> 0x00b2 }
            r7.append(r3)     // Catch:{ all -> 0x00b2 }
            r7.append(r0)     // Catch:{ all -> 0x00b2 }
            r7.append(r5)     // Catch:{ all -> 0x00b2 }
            r7.append(r0)     // Catch:{ all -> 0x00b2 }
            r7.append(r6)     // Catch:{ all -> 0x00b2 }
            r0 = 93
            r7.append(r0)     // Catch:{ all -> 0x00b2 }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x00b2 }
            r4.<init>(r0)     // Catch:{ all -> 0x00b2 }
            throw r4     // Catch:{ all -> 0x00b2 }
        L_0x00b2:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00b4 }
        L_0x00b4:
            r2 = move-exception
            kotlin.io.CloseableKt.a(r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.readJournal():void");
    }

    private final void readJournalLine(String str) throws IOException {
        String str2;
        String str3 = str;
        int a2 = StringsKt__StringsKt.a((CharSequence) str, ' ', 0, false, 6, (Object) null);
        if (a2 != -1) {
            int i = a2 + 1;
            int a3 = StringsKt__StringsKt.a((CharSequence) str, ' ', i, false, 4, (Object) null);
            if (a3 == -1) {
                if (str3 != null) {
                    str2 = str3.substring(i);
                    Intrinsics.a((Object) str2, "(this as java.lang.String).substring(startIndex)");
                    if (a2 == REMOVE.length() && StringsKt__StringsJVMKt.b(str3, REMOVE, false, 2, (Object) null)) {
                        this.lruEntries.remove(str2);
                        return;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else if (str3 != null) {
                str2 = str3.substring(i, a3);
                Intrinsics.a((Object) str2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
            Entry entry = this.lruEntries.get(str2);
            if (entry == null) {
                entry = new Entry(this, str2);
                this.lruEntries.put(str2, entry);
            }
            if (a3 != -1 && a2 == CLEAN.length() && StringsKt__StringsJVMKt.b(str3, CLEAN, false, 2, (Object) null)) {
                int i2 = a3 + 1;
                if (str3 != null) {
                    String substring = str3.substring(i2);
                    Intrinsics.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                    List a4 = StringsKt__StringsKt.a((CharSequence) substring, new char[]{' '}, false, 0, 6, (Object) null);
                    entry.setReadable$okhttp(true);
                    entry.setCurrentEditor$okhttp((Editor) null);
                    entry.setLengths$okhttp(a4);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            } else if (a3 == -1 && a2 == DIRTY.length() && StringsKt__StringsJVMKt.b(str3, DIRTY, false, 2, (Object) null)) {
                entry.setCurrentEditor$okhttp(new Editor(this, entry));
            } else if (a3 != -1 || a2 != READ.length() || !StringsKt__StringsJVMKt.b(str3, READ, false, 2, (Object) null)) {
                throw new IOException("unexpected journal line: " + str3);
            }
        } else {
            throw new IOException("unexpected journal line: " + str3);
        }
    }

    private final boolean removeOldestEntry() {
        for (Entry next : this.lruEntries.values()) {
            if (!next.getZombie$okhttp()) {
                Intrinsics.a((Object) next, "toEvict");
                removeEntry$okhttp(next);
                return true;
            }
        }
        return false;
    }

    private final void validateKey(String str) {
        if (!LEGAL_KEY_PATTERN.a(str)) {
            throw new IllegalArgumentException(("keys must match regex [a-z0-9_-]{1,120}: \"" + str + '\"').toString());
        }
    }

    public synchronized void close() throws IOException {
        Editor currentEditor$okhttp;
        if (this.initialized) {
            if (!this.closed) {
                Collection<Entry> values = this.lruEntries.values();
                Intrinsics.a((Object) values, "lruEntries.values");
                Object[] array = values.toArray(new Entry[0]);
                if (array != null) {
                    for (Entry entry : (Entry[]) array) {
                        if (!(entry.getCurrentEditor$okhttp() == null || (currentEditor$okhttp = entry.getCurrentEditor$okhttp()) == null)) {
                            currentEditor$okhttp.detach$okhttp();
                        }
                    }
                    trimToSize();
                    BufferedSink bufferedSink = this.journalWriter;
                    if (bufferedSink != null) {
                        bufferedSink.close();
                        this.journalWriter = null;
                        this.closed = true;
                        return;
                    }
                    Intrinsics.a();
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        this.closed = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0137, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void completeEdit$okhttp(okhttp3.internal.cache.DiskLruCache.Editor r10, boolean r11) throws java.io.IOException {
        /*
            r9 = this;
            monitor-enter(r9)
            java.lang.String r0 = "editor"
            kotlin.jvm.internal.Intrinsics.b(r10, r0)     // Catch:{ all -> 0x0148 }
            okhttp3.internal.cache.DiskLruCache$Entry r0 = r10.getEntry$okhttp()     // Catch:{ all -> 0x0148 }
            okhttp3.internal.cache.DiskLruCache$Editor r1 = r0.getCurrentEditor$okhttp()     // Catch:{ all -> 0x0148 }
            boolean r1 = kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r1, (java.lang.Object) r10)     // Catch:{ all -> 0x0148 }
            if (r1 == 0) goto L_0x013c
            r1 = 0
            r2 = 0
            if (r11 == 0) goto L_0x0065
            boolean r3 = r0.getReadable$okhttp()     // Catch:{ all -> 0x0148 }
            if (r3 != 0) goto L_0x0065
            int r3 = r9.valueCount     // Catch:{ all -> 0x0148 }
            r4 = 0
        L_0x0021:
            if (r4 >= r3) goto L_0x0065
            boolean[] r5 = r10.getWritten$okhttp()     // Catch:{ all -> 0x0148 }
            if (r5 == 0) goto L_0x0061
            boolean r5 = r5[r4]     // Catch:{ all -> 0x0148 }
            if (r5 == 0) goto L_0x0047
            okhttp3.internal.io.FileSystem r5 = r9.fileSystem     // Catch:{ all -> 0x0148 }
            java.util.List r6 = r0.getDirtyFiles$okhttp()     // Catch:{ all -> 0x0148 }
            java.lang.Object r6 = r6.get(r4)     // Catch:{ all -> 0x0148 }
            java.io.File r6 = (java.io.File) r6     // Catch:{ all -> 0x0148 }
            boolean r5 = r5.exists(r6)     // Catch:{ all -> 0x0148 }
            if (r5 != 0) goto L_0x0044
            r10.abort()     // Catch:{ all -> 0x0148 }
            monitor-exit(r9)
            return
        L_0x0044:
            int r4 = r4 + 1
            goto L_0x0021
        L_0x0047:
            r10.abort()     // Catch:{ all -> 0x0148 }
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0148 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0148 }
            r11.<init>()     // Catch:{ all -> 0x0148 }
            java.lang.String r0 = "Newly created entry didn't create value for index "
            r11.append(r0)     // Catch:{ all -> 0x0148 }
            r11.append(r4)     // Catch:{ all -> 0x0148 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x0148 }
            r10.<init>(r11)     // Catch:{ all -> 0x0148 }
            throw r10     // Catch:{ all -> 0x0148 }
        L_0x0061:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x0148 }
            throw r2
        L_0x0065:
            int r10 = r9.valueCount     // Catch:{ all -> 0x0148 }
        L_0x0067:
            if (r1 >= r10) goto L_0x00b3
            java.util.List r3 = r0.getDirtyFiles$okhttp()     // Catch:{ all -> 0x0148 }
            java.lang.Object r3 = r3.get(r1)     // Catch:{ all -> 0x0148 }
            java.io.File r3 = (java.io.File) r3     // Catch:{ all -> 0x0148 }
            if (r11 == 0) goto L_0x00ab
            boolean r4 = r0.getZombie$okhttp()     // Catch:{ all -> 0x0148 }
            if (r4 != 0) goto L_0x00ab
            okhttp3.internal.io.FileSystem r4 = r9.fileSystem     // Catch:{ all -> 0x0148 }
            boolean r4 = r4.exists(r3)     // Catch:{ all -> 0x0148 }
            if (r4 == 0) goto L_0x00b0
            java.util.List r4 = r0.getCleanFiles$okhttp()     // Catch:{ all -> 0x0148 }
            java.lang.Object r4 = r4.get(r1)     // Catch:{ all -> 0x0148 }
            java.io.File r4 = (java.io.File) r4     // Catch:{ all -> 0x0148 }
            okhttp3.internal.io.FileSystem r5 = r9.fileSystem     // Catch:{ all -> 0x0148 }
            r5.rename(r3, r4)     // Catch:{ all -> 0x0148 }
            long[] r3 = r0.getLengths$okhttp()     // Catch:{ all -> 0x0148 }
            r5 = r3[r1]     // Catch:{ all -> 0x0148 }
            okhttp3.internal.io.FileSystem r3 = r9.fileSystem     // Catch:{ all -> 0x0148 }
            long r3 = r3.size(r4)     // Catch:{ all -> 0x0148 }
            long[] r7 = r0.getLengths$okhttp()     // Catch:{ all -> 0x0148 }
            r7[r1] = r3     // Catch:{ all -> 0x0148 }
            long r7 = r9.size     // Catch:{ all -> 0x0148 }
            long r7 = r7 - r5
            long r7 = r7 + r3
            r9.size = r7     // Catch:{ all -> 0x0148 }
            goto L_0x00b0
        L_0x00ab:
            okhttp3.internal.io.FileSystem r4 = r9.fileSystem     // Catch:{ all -> 0x0148 }
            r4.delete(r3)     // Catch:{ all -> 0x0148 }
        L_0x00b0:
            int r1 = r1 + 1
            goto L_0x0067
        L_0x00b3:
            r0.setCurrentEditor$okhttp(r2)     // Catch:{ all -> 0x0148 }
            boolean r10 = r0.getZombie$okhttp()     // Catch:{ all -> 0x0148 }
            if (r10 == 0) goto L_0x00c1
            r9.removeEntry$okhttp(r0)     // Catch:{ all -> 0x0148 }
            monitor-exit(r9)
            return
        L_0x00c1:
            int r10 = r9.redundantOpCount     // Catch:{ all -> 0x0148 }
            r1 = 1
            int r10 = r10 + r1
            r9.redundantOpCount = r10     // Catch:{ all -> 0x0148 }
            okio.BufferedSink r10 = r9.journalWriter     // Catch:{ all -> 0x0148 }
            if (r10 == 0) goto L_0x0138
            boolean r2 = r0.getReadable$okhttp()     // Catch:{ all -> 0x0148 }
            r3 = 10
            r4 = 32
            if (r2 != 0) goto L_0x00f5
            if (r11 == 0) goto L_0x00d8
            goto L_0x00f5
        L_0x00d8:
            java.util.LinkedHashMap<java.lang.String, okhttp3.internal.cache.DiskLruCache$Entry> r11 = r9.lruEntries     // Catch:{ all -> 0x0148 }
            java.lang.String r1 = r0.getKey$okhttp()     // Catch:{ all -> 0x0148 }
            r11.remove(r1)     // Catch:{ all -> 0x0148 }
            java.lang.String r11 = REMOVE     // Catch:{ all -> 0x0148 }
            okio.BufferedSink r11 = r10.a((java.lang.String) r11)     // Catch:{ all -> 0x0148 }
            r11.writeByte(r4)     // Catch:{ all -> 0x0148 }
            java.lang.String r11 = r0.getKey$okhttp()     // Catch:{ all -> 0x0148 }
            r10.a((java.lang.String) r11)     // Catch:{ all -> 0x0148 }
            r10.writeByte(r3)     // Catch:{ all -> 0x0148 }
            goto L_0x011a
        L_0x00f5:
            r0.setReadable$okhttp(r1)     // Catch:{ all -> 0x0148 }
            java.lang.String r1 = CLEAN     // Catch:{ all -> 0x0148 }
            okio.BufferedSink r1 = r10.a((java.lang.String) r1)     // Catch:{ all -> 0x0148 }
            r1.writeByte(r4)     // Catch:{ all -> 0x0148 }
            java.lang.String r1 = r0.getKey$okhttp()     // Catch:{ all -> 0x0148 }
            r10.a((java.lang.String) r1)     // Catch:{ all -> 0x0148 }
            r0.writeLengths$okhttp(r10)     // Catch:{ all -> 0x0148 }
            r10.writeByte(r3)     // Catch:{ all -> 0x0148 }
            if (r11 == 0) goto L_0x011a
            long r1 = r9.nextSequenceNumber     // Catch:{ all -> 0x0148 }
            r3 = 1
            long r3 = r3 + r1
            r9.nextSequenceNumber = r3     // Catch:{ all -> 0x0148 }
            r0.setSequenceNumber$okhttp(r1)     // Catch:{ all -> 0x0148 }
        L_0x011a:
            r10.flush()     // Catch:{ all -> 0x0148 }
            long r10 = r9.size     // Catch:{ all -> 0x0148 }
            long r0 = r9.maxSize     // Catch:{ all -> 0x0148 }
            int r2 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x012b
            boolean r10 = r9.journalRebuildRequired()     // Catch:{ all -> 0x0148 }
            if (r10 == 0) goto L_0x0136
        L_0x012b:
            okhttp3.internal.concurrent.TaskQueue r0 = r9.cleanupQueue     // Catch:{ all -> 0x0148 }
            okhttp3.internal.cache.DiskLruCache$cleanupTask$1 r1 = r9.cleanupTask     // Catch:{ all -> 0x0148 }
            r2 = 0
            r4 = 2
            r5 = 0
            okhttp3.internal.concurrent.TaskQueue.schedule$default(r0, r1, r2, r4, r5)     // Catch:{ all -> 0x0148 }
        L_0x0136:
            monitor-exit(r9)
            return
        L_0x0138:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x0148 }
            throw r2
        L_0x013c:
            java.lang.String r10 = "Check failed."
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0148 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0148 }
            r11.<init>(r10)     // Catch:{ all -> 0x0148 }
            throw r11     // Catch:{ all -> 0x0148 }
        L_0x0148:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.completeEdit$okhttp(okhttp3.internal.cache.DiskLruCache$Editor, boolean):void");
    }

    public final void delete() throws IOException {
        close();
        this.fileSystem.deleteContents(this.directory);
    }

    public final Editor edit(String str) throws IOException {
        return edit$default(this, str, 0, 2, (Object) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0029, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized okhttp3.internal.cache.DiskLruCache.Editor edit(java.lang.String r11, long r12) throws java.io.IOException {
        /*
            r10 = this;
            monitor-enter(r10)
            java.lang.String r0 = "key"
            kotlin.jvm.internal.Intrinsics.b(r11, r0)     // Catch:{ all -> 0x0092 }
            r10.initialize()     // Catch:{ all -> 0x0092 }
            r10.checkNotClosed()     // Catch:{ all -> 0x0092 }
            r10.validateKey(r11)     // Catch:{ all -> 0x0092 }
            java.util.LinkedHashMap<java.lang.String, okhttp3.internal.cache.DiskLruCache$Entry> r0 = r10.lruEntries     // Catch:{ all -> 0x0092 }
            java.lang.Object r0 = r0.get(r11)     // Catch:{ all -> 0x0092 }
            okhttp3.internal.cache.DiskLruCache$Entry r0 = (okhttp3.internal.cache.DiskLruCache.Entry) r0     // Catch:{ all -> 0x0092 }
            long r1 = ANY_SEQUENCE_NUMBER     // Catch:{ all -> 0x0092 }
            r3 = 0
            int r4 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r4 == 0) goto L_0x002a
            if (r0 == 0) goto L_0x0028
            long r1 = r0.getSequenceNumber$okhttp()     // Catch:{ all -> 0x0092 }
            int r4 = (r1 > r12 ? 1 : (r1 == r12 ? 0 : -1))
            if (r4 == 0) goto L_0x002a
        L_0x0028:
            monitor-exit(r10)
            return r3
        L_0x002a:
            if (r0 == 0) goto L_0x0031
            okhttp3.internal.cache.DiskLruCache$Editor r12 = r0.getCurrentEditor$okhttp()     // Catch:{ all -> 0x0092 }
            goto L_0x0032
        L_0x0031:
            r12 = r3
        L_0x0032:
            if (r12 == 0) goto L_0x0036
            monitor-exit(r10)
            return r3
        L_0x0036:
            if (r0 == 0) goto L_0x0040
            int r12 = r0.getLockingSourceCount$okhttp()     // Catch:{ all -> 0x0092 }
            if (r12 == 0) goto L_0x0040
            monitor-exit(r10)
            return r3
        L_0x0040:
            boolean r12 = r10.mostRecentTrimFailed     // Catch:{ all -> 0x0092 }
            if (r12 != 0) goto L_0x0085
            boolean r12 = r10.mostRecentRebuildFailed     // Catch:{ all -> 0x0092 }
            if (r12 == 0) goto L_0x0049
            goto L_0x0085
        L_0x0049:
            okio.BufferedSink r12 = r10.journalWriter     // Catch:{ all -> 0x0092 }
            if (r12 == 0) goto L_0x0081
            java.lang.String r13 = DIRTY     // Catch:{ all -> 0x0092 }
            okio.BufferedSink r13 = r12.a((java.lang.String) r13)     // Catch:{ all -> 0x0092 }
            r1 = 32
            okio.BufferedSink r13 = r13.writeByte(r1)     // Catch:{ all -> 0x0092 }
            okio.BufferedSink r13 = r13.a((java.lang.String) r11)     // Catch:{ all -> 0x0092 }
            r1 = 10
            r13.writeByte(r1)     // Catch:{ all -> 0x0092 }
            r12.flush()     // Catch:{ all -> 0x0092 }
            boolean r12 = r10.hasJournalErrors     // Catch:{ all -> 0x0092 }
            if (r12 == 0) goto L_0x006b
            monitor-exit(r10)
            return r3
        L_0x006b:
            if (r0 != 0) goto L_0x0077
            okhttp3.internal.cache.DiskLruCache$Entry r0 = new okhttp3.internal.cache.DiskLruCache$Entry     // Catch:{ all -> 0x0092 }
            r0.<init>(r10, r11)     // Catch:{ all -> 0x0092 }
            java.util.LinkedHashMap<java.lang.String, okhttp3.internal.cache.DiskLruCache$Entry> r12 = r10.lruEntries     // Catch:{ all -> 0x0092 }
            r12.put(r11, r0)     // Catch:{ all -> 0x0092 }
        L_0x0077:
            okhttp3.internal.cache.DiskLruCache$Editor r11 = new okhttp3.internal.cache.DiskLruCache$Editor     // Catch:{ all -> 0x0092 }
            r11.<init>(r10, r0)     // Catch:{ all -> 0x0092 }
            r0.setCurrentEditor$okhttp(r11)     // Catch:{ all -> 0x0092 }
            monitor-exit(r10)
            return r11
        L_0x0081:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x0092 }
            throw r3
        L_0x0085:
            okhttp3.internal.concurrent.TaskQueue r4 = r10.cleanupQueue     // Catch:{ all -> 0x0092 }
            okhttp3.internal.cache.DiskLruCache$cleanupTask$1 r5 = r10.cleanupTask     // Catch:{ all -> 0x0092 }
            r6 = 0
            r8 = 2
            r9 = 0
            okhttp3.internal.concurrent.TaskQueue.schedule$default(r4, r5, r6, r8, r9)     // Catch:{ all -> 0x0092 }
            monitor-exit(r10)
            return r3
        L_0x0092:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.edit(java.lang.String, long):okhttp3.internal.cache.DiskLruCache$Editor");
    }

    public final synchronized void evictAll() throws IOException {
        initialize();
        Collection<Entry> values = this.lruEntries.values();
        Intrinsics.a((Object) values, "lruEntries.values");
        Object[] array = values.toArray(new Entry[0]);
        if (array != null) {
            for (Entry entry : (Entry[]) array) {
                Intrinsics.a((Object) entry, "entry");
                removeEntry$okhttp(entry);
            }
            this.mostRecentTrimFailed = false;
        } else {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    public synchronized void flush() throws IOException {
        if (this.initialized) {
            checkNotClosed();
            trimToSize();
            BufferedSink bufferedSink = this.journalWriter;
            if (bufferedSink != null) {
                bufferedSink.flush();
            } else {
                Intrinsics.a();
                throw null;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0056, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized okhttp3.internal.cache.DiskLruCache.Snapshot get(java.lang.String r8) throws java.io.IOException {
        /*
            r7 = this;
            monitor-enter(r7)
            java.lang.String r0 = "key"
            kotlin.jvm.internal.Intrinsics.b(r8, r0)     // Catch:{ all -> 0x005f }
            r7.initialize()     // Catch:{ all -> 0x005f }
            r7.checkNotClosed()     // Catch:{ all -> 0x005f }
            r7.validateKey(r8)     // Catch:{ all -> 0x005f }
            java.util.LinkedHashMap<java.lang.String, okhttp3.internal.cache.DiskLruCache$Entry> r0 = r7.lruEntries     // Catch:{ all -> 0x005f }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x005f }
            okhttp3.internal.cache.DiskLruCache$Entry r0 = (okhttp3.internal.cache.DiskLruCache.Entry) r0     // Catch:{ all -> 0x005f }
            r1 = 0
            if (r0 == 0) goto L_0x005d
            java.lang.String r2 = "lruEntries[key] ?: return null"
            kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r0, (java.lang.String) r2)     // Catch:{ all -> 0x005f }
            okhttp3.internal.cache.DiskLruCache$Snapshot r0 = r0.snapshot$okhttp()     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x005b
            int r2 = r7.redundantOpCount     // Catch:{ all -> 0x005f }
            int r2 = r2 + 1
            r7.redundantOpCount = r2     // Catch:{ all -> 0x005f }
            okio.BufferedSink r2 = r7.journalWriter     // Catch:{ all -> 0x005f }
            if (r2 == 0) goto L_0x0057
            java.lang.String r1 = READ     // Catch:{ all -> 0x005f }
            okio.BufferedSink r1 = r2.a((java.lang.String) r1)     // Catch:{ all -> 0x005f }
            r2 = 32
            okio.BufferedSink r1 = r1.writeByte(r2)     // Catch:{ all -> 0x005f }
            okio.BufferedSink r8 = r1.a((java.lang.String) r8)     // Catch:{ all -> 0x005f }
            r1 = 10
            r8.writeByte(r1)     // Catch:{ all -> 0x005f }
            boolean r8 = r7.journalRebuildRequired()     // Catch:{ all -> 0x005f }
            if (r8 == 0) goto L_0x0055
            okhttp3.internal.concurrent.TaskQueue r1 = r7.cleanupQueue     // Catch:{ all -> 0x005f }
            okhttp3.internal.cache.DiskLruCache$cleanupTask$1 r2 = r7.cleanupTask     // Catch:{ all -> 0x005f }
            r3 = 0
            r5 = 2
            r6 = 0
            okhttp3.internal.concurrent.TaskQueue.schedule$default(r1, r2, r3, r5, r6)     // Catch:{ all -> 0x005f }
        L_0x0055:
            monitor-exit(r7)
            return r0
        L_0x0057:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x005f }
            throw r1
        L_0x005b:
            monitor-exit(r7)
            return r1
        L_0x005d:
            monitor-exit(r7)
            return r1
        L_0x005f:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.get(java.lang.String):okhttp3.internal.cache.DiskLruCache$Snapshot");
    }

    public final boolean getClosed$okhttp() {
        return this.closed;
    }

    public final File getDirectory() {
        return this.directory;
    }

    public final FileSystem getFileSystem$okhttp() {
        return this.fileSystem;
    }

    public final LinkedHashMap<String, Entry> getLruEntries$okhttp() {
        return this.lruEntries;
    }

    public final synchronized long getMaxSize() {
        return this.maxSize;
    }

    public final int getValueCount$okhttp() {
        return this.valueCount;
    }

    public final synchronized void initialize() throws IOException {
        if (Util.assertionsEnabled) {
            if (!Thread.holdsLock(this)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Thread ");
                Thread currentThread = Thread.currentThread();
                Intrinsics.a((Object) currentThread, "Thread.currentThread()");
                sb.append(currentThread.getName());
                sb.append(" MUST hold lock on ");
                sb.append(this);
                throw new AssertionError(sb.toString());
            }
        }
        if (!this.initialized) {
            if (this.fileSystem.exists(this.journalFileBackup)) {
                if (this.fileSystem.exists(this.journalFile)) {
                    this.fileSystem.delete(this.journalFileBackup);
                } else {
                    this.fileSystem.rename(this.journalFileBackup, this.journalFile);
                }
            }
            this.civilizedFileSystem = Util.isCivilized(this.fileSystem, this.journalFileBackup);
            if (this.fileSystem.exists(this.journalFile)) {
                try {
                    readJournal();
                    processJournal();
                    this.initialized = true;
                    return;
                } catch (IOException e) {
                    Platform platform = Platform.Companion.get();
                    platform.log("DiskLruCache " + this.directory + " is corrupt: " + e.getMessage() + ", removing", 5, e);
                    delete();
                    this.closed = false;
                } catch (Throwable th) {
                    this.closed = false;
                    throw th;
                }
            }
            rebuildJournal$okhttp();
            this.initialized = true;
        }
    }

    public final synchronized boolean isClosed() {
        return this.closed;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00c0, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        kotlin.io.CloseableKt.a(r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c4, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void rebuildJournal$okhttp() throws java.io.IOException {
        /*
            r7 = this;
            monitor-enter(r7)
            okio.BufferedSink r0 = r7.journalWriter     // Catch:{ all -> 0x00c5 }
            if (r0 == 0) goto L_0x0008
            r0.close()     // Catch:{ all -> 0x00c5 }
        L_0x0008:
            okhttp3.internal.io.FileSystem r0 = r7.fileSystem     // Catch:{ all -> 0x00c5 }
            java.io.File r1 = r7.journalFileTmp     // Catch:{ all -> 0x00c5 }
            okio.Sink r0 = r0.sink(r1)     // Catch:{ all -> 0x00c5 }
            okio.BufferedSink r0 = okio.Okio.a((okio.Sink) r0)     // Catch:{ all -> 0x00c5 }
            r1 = 0
            java.lang.String r2 = MAGIC     // Catch:{ all -> 0x00be }
            okio.BufferedSink r2 = r0.a((java.lang.String) r2)     // Catch:{ all -> 0x00be }
            r3 = 10
            r2.writeByte(r3)     // Catch:{ all -> 0x00be }
            java.lang.String r2 = VERSION_1     // Catch:{ all -> 0x00be }
            okio.BufferedSink r2 = r0.a((java.lang.String) r2)     // Catch:{ all -> 0x00be }
            r2.writeByte(r3)     // Catch:{ all -> 0x00be }
            int r2 = r7.appVersion     // Catch:{ all -> 0x00be }
            long r4 = (long) r2     // Catch:{ all -> 0x00be }
            okio.BufferedSink r2 = r0.e(r4)     // Catch:{ all -> 0x00be }
            r2.writeByte(r3)     // Catch:{ all -> 0x00be }
            int r2 = r7.valueCount     // Catch:{ all -> 0x00be }
            long r4 = (long) r2     // Catch:{ all -> 0x00be }
            okio.BufferedSink r2 = r0.e(r4)     // Catch:{ all -> 0x00be }
            r2.writeByte(r3)     // Catch:{ all -> 0x00be }
            r0.writeByte(r3)     // Catch:{ all -> 0x00be }
            java.util.LinkedHashMap<java.lang.String, okhttp3.internal.cache.DiskLruCache$Entry> r2 = r7.lruEntries     // Catch:{ all -> 0x00be }
            java.util.Collection r2 = r2.values()     // Catch:{ all -> 0x00be }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x00be }
        L_0x004a:
            boolean r4 = r2.hasNext()     // Catch:{ all -> 0x00be }
            if (r4 == 0) goto L_0x0089
            java.lang.Object r4 = r2.next()     // Catch:{ all -> 0x00be }
            okhttp3.internal.cache.DiskLruCache$Entry r4 = (okhttp3.internal.cache.DiskLruCache.Entry) r4     // Catch:{ all -> 0x00be }
            okhttp3.internal.cache.DiskLruCache$Editor r5 = r4.getCurrentEditor$okhttp()     // Catch:{ all -> 0x00be }
            r6 = 32
            if (r5 == 0) goto L_0x0072
            java.lang.String r5 = DIRTY     // Catch:{ all -> 0x00be }
            okio.BufferedSink r5 = r0.a((java.lang.String) r5)     // Catch:{ all -> 0x00be }
            r5.writeByte(r6)     // Catch:{ all -> 0x00be }
            java.lang.String r4 = r4.getKey$okhttp()     // Catch:{ all -> 0x00be }
            r0.a((java.lang.String) r4)     // Catch:{ all -> 0x00be }
            r0.writeByte(r3)     // Catch:{ all -> 0x00be }
            goto L_0x004a
        L_0x0072:
            java.lang.String r5 = CLEAN     // Catch:{ all -> 0x00be }
            okio.BufferedSink r5 = r0.a((java.lang.String) r5)     // Catch:{ all -> 0x00be }
            r5.writeByte(r6)     // Catch:{ all -> 0x00be }
            java.lang.String r5 = r4.getKey$okhttp()     // Catch:{ all -> 0x00be }
            r0.a((java.lang.String) r5)     // Catch:{ all -> 0x00be }
            r4.writeLengths$okhttp(r0)     // Catch:{ all -> 0x00be }
            r0.writeByte(r3)     // Catch:{ all -> 0x00be }
            goto L_0x004a
        L_0x0089:
            kotlin.Unit r2 = kotlin.Unit.f6917a     // Catch:{ all -> 0x00be }
            kotlin.io.CloseableKt.a(r0, r1)     // Catch:{ all -> 0x00c5 }
            okhttp3.internal.io.FileSystem r0 = r7.fileSystem     // Catch:{ all -> 0x00c5 }
            java.io.File r1 = r7.journalFile     // Catch:{ all -> 0x00c5 }
            boolean r0 = r0.exists(r1)     // Catch:{ all -> 0x00c5 }
            if (r0 == 0) goto L_0x00a1
            okhttp3.internal.io.FileSystem r0 = r7.fileSystem     // Catch:{ all -> 0x00c5 }
            java.io.File r1 = r7.journalFile     // Catch:{ all -> 0x00c5 }
            java.io.File r2 = r7.journalFileBackup     // Catch:{ all -> 0x00c5 }
            r0.rename(r1, r2)     // Catch:{ all -> 0x00c5 }
        L_0x00a1:
            okhttp3.internal.io.FileSystem r0 = r7.fileSystem     // Catch:{ all -> 0x00c5 }
            java.io.File r1 = r7.journalFileTmp     // Catch:{ all -> 0x00c5 }
            java.io.File r2 = r7.journalFile     // Catch:{ all -> 0x00c5 }
            r0.rename(r1, r2)     // Catch:{ all -> 0x00c5 }
            okhttp3.internal.io.FileSystem r0 = r7.fileSystem     // Catch:{ all -> 0x00c5 }
            java.io.File r1 = r7.journalFileBackup     // Catch:{ all -> 0x00c5 }
            r0.delete(r1)     // Catch:{ all -> 0x00c5 }
            okio.BufferedSink r0 = r7.newJournalWriter()     // Catch:{ all -> 0x00c5 }
            r7.journalWriter = r0     // Catch:{ all -> 0x00c5 }
            r0 = 0
            r7.hasJournalErrors = r0     // Catch:{ all -> 0x00c5 }
            r7.mostRecentRebuildFailed = r0     // Catch:{ all -> 0x00c5 }
            monitor-exit(r7)
            return
        L_0x00be:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x00c0 }
        L_0x00c0:
            r2 = move-exception
            kotlin.io.CloseableKt.a(r0, r1)     // Catch:{ all -> 0x00c5 }
            throw r2     // Catch:{ all -> 0x00c5 }
        L_0x00c5:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.rebuildJournal$okhttp():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0030, code lost:
        return r7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean remove(java.lang.String r7) throws java.io.IOException {
        /*
            r6 = this;
            monitor-enter(r6)
            java.lang.String r0 = "key"
            kotlin.jvm.internal.Intrinsics.b(r7, r0)     // Catch:{ all -> 0x0033 }
            r6.initialize()     // Catch:{ all -> 0x0033 }
            r6.checkNotClosed()     // Catch:{ all -> 0x0033 }
            r6.validateKey(r7)     // Catch:{ all -> 0x0033 }
            java.util.LinkedHashMap<java.lang.String, okhttp3.internal.cache.DiskLruCache$Entry> r0 = r6.lruEntries     // Catch:{ all -> 0x0033 }
            java.lang.Object r7 = r0.get(r7)     // Catch:{ all -> 0x0033 }
            okhttp3.internal.cache.DiskLruCache$Entry r7 = (okhttp3.internal.cache.DiskLruCache.Entry) r7     // Catch:{ all -> 0x0033 }
            r0 = 0
            if (r7 == 0) goto L_0x0031
            java.lang.String r1 = "lruEntries[key] ?: return false"
            kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r7, (java.lang.String) r1)     // Catch:{ all -> 0x0033 }
            boolean r7 = r6.removeEntry$okhttp(r7)     // Catch:{ all -> 0x0033 }
            if (r7 == 0) goto L_0x002f
            long r1 = r6.size     // Catch:{ all -> 0x0033 }
            long r3 = r6.maxSize     // Catch:{ all -> 0x0033 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 > 0) goto L_0x002f
            r6.mostRecentTrimFailed = r0     // Catch:{ all -> 0x0033 }
        L_0x002f:
            monitor-exit(r6)
            return r7
        L_0x0031:
            monitor-exit(r6)
            return r0
        L_0x0033:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.remove(java.lang.String):boolean");
    }

    public final boolean removeEntry$okhttp(Entry entry) throws IOException {
        BufferedSink bufferedSink;
        Intrinsics.b(entry, "entry");
        if (!this.civilizedFileSystem) {
            if (entry.getLockingSourceCount$okhttp() > 0 && (bufferedSink = this.journalWriter) != null) {
                bufferedSink.a(DIRTY);
                bufferedSink.writeByte(32);
                bufferedSink.a(entry.getKey$okhttp());
                bufferedSink.writeByte(10);
                bufferedSink.flush();
            }
            if (entry.getLockingSourceCount$okhttp() > 0 || entry.getCurrentEditor$okhttp() != null) {
                entry.setZombie$okhttp(true);
                return true;
            }
        }
        Editor currentEditor$okhttp = entry.getCurrentEditor$okhttp();
        if (currentEditor$okhttp != null) {
            currentEditor$okhttp.detach$okhttp();
        }
        int i = this.valueCount;
        for (int i2 = 0; i2 < i; i2++) {
            this.fileSystem.delete(entry.getCleanFiles$okhttp().get(i2));
            this.size -= entry.getLengths$okhttp()[i2];
            entry.getLengths$okhttp()[i2] = 0;
        }
        this.redundantOpCount++;
        BufferedSink bufferedSink2 = this.journalWriter;
        if (bufferedSink2 != null) {
            bufferedSink2.a(REMOVE);
            bufferedSink2.writeByte(32);
            bufferedSink2.a(entry.getKey$okhttp());
            bufferedSink2.writeByte(10);
        }
        this.lruEntries.remove(entry.getKey$okhttp());
        if (journalRebuildRequired()) {
            TaskQueue.schedule$default(this.cleanupQueue, this.cleanupTask, 0, 2, (Object) null);
        }
        return true;
    }

    public final void setClosed$okhttp(boolean z) {
        this.closed = z;
    }

    public final synchronized void setMaxSize(long j) {
        this.maxSize = j;
        if (this.initialized) {
            TaskQueue.schedule$default(this.cleanupQueue, this.cleanupTask, 0, 2, (Object) null);
        }
    }

    public final synchronized long size() throws IOException {
        initialize();
        return this.size;
    }

    public final synchronized Iterator<Snapshot> snapshots() throws IOException {
        initialize();
        return new DiskLruCache$snapshots$1(this);
    }

    public final void trimToSize() throws IOException {
        while (this.size > this.maxSize) {
            if (!removeOldestEntry()) {
                return;
            }
        }
        this.mostRecentTrimFailed = false;
    }
}
