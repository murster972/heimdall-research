package okhttp3.internal;

import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.CookieDBAdapter;
import com.vungle.warren.model.ReportDBAdapter;
import javax.net.ssl.SSLSocket;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Cache;
import okhttp3.ConnectionSpec;
import okhttp3.Cookie;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

public final class Internal {
    public static final Headers.Builder addHeaderLenient(Headers.Builder builder, String str) {
        Intrinsics.b(builder, "builder");
        Intrinsics.b(str, "line");
        return builder.addLenient$okhttp(str);
    }

    public static final void applyConnectionSpec(ConnectionSpec connectionSpec, SSLSocket sSLSocket, boolean z) {
        Intrinsics.b(connectionSpec, "connectionSpec");
        Intrinsics.b(sSLSocket, "sslSocket");
        connectionSpec.apply$okhttp(sSLSocket, z);
    }

    public static final Response cacheGet(Cache cache, Request request) {
        Intrinsics.b(cache, "cache");
        Intrinsics.b(request, "request");
        return cache.get$okhttp(request);
    }

    public static final String cookieToString(Cookie cookie, boolean z) {
        Intrinsics.b(cookie, CookieDBAdapter.CookieColumns.TABLE_NAME);
        return cookie.toString$okhttp(z);
    }

    public static final Cookie parseCookie(long j, HttpUrl httpUrl, String str) {
        Intrinsics.b(httpUrl, ReportDBAdapter.ReportColumns.COLUMN_URL);
        Intrinsics.b(str, "setCookie");
        return Cookie.Companion.parse$okhttp(j, httpUrl, str);
    }

    public static final Headers.Builder addHeaderLenient(Headers.Builder builder, String str, String str2) {
        Intrinsics.b(builder, "builder");
        Intrinsics.b(str, MediationMetaData.KEY_NAME);
        Intrinsics.b(str2, "value");
        return builder.addLenient$okhttp(str, str2);
    }
}
