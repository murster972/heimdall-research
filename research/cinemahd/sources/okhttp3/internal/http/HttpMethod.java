package okhttp3.internal.http;

import kotlin.jvm.internal.Intrinsics;

public final class HttpMethod {
    public static final HttpMethod INSTANCE = new HttpMethod();

    private HttpMethod() {
    }

    public static final boolean permitsRequestBody(String str) {
        Intrinsics.b(str, "method");
        return !Intrinsics.a((Object) str, (Object) "GET") && !Intrinsics.a((Object) str, (Object) "HEAD");
    }

    public static final boolean requiresRequestBody(String str) {
        Intrinsics.b(str, "method");
        return Intrinsics.a((Object) str, (Object) "POST") || Intrinsics.a((Object) str, (Object) "PUT") || Intrinsics.a((Object) str, (Object) "PATCH") || Intrinsics.a((Object) str, (Object) "PROPPATCH") || Intrinsics.a((Object) str, (Object) "REPORT");
    }

    public final boolean invalidatesCache(String str) {
        Intrinsics.b(str, "method");
        return Intrinsics.a((Object) str, (Object) "POST") || Intrinsics.a((Object) str, (Object) "PATCH") || Intrinsics.a((Object) str, (Object) "PUT") || Intrinsics.a((Object) str, (Object) "DELETE") || Intrinsics.a((Object) str, (Object) "MOVE");
    }

    public final boolean redirectsToGet(String str) {
        Intrinsics.b(str, "method");
        return !Intrinsics.a((Object) str, (Object) "PROPFIND");
    }

    public final boolean redirectsWithBody(String str) {
        Intrinsics.b(str, "method");
        return Intrinsics.a((Object) str, (Object) "PROPFIND");
    }
}
