package okhttp3.internal.http;

import com.vungle.warren.ui.JavascriptBridge;
import java.io.IOException;
import java.net.ProtocolException;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Util;
import okhttp3.internal.connection.Exchange;
import okio.BufferedSink;
import okio.Okio;

public final class CallServerInterceptor implements Interceptor {
    private final boolean forWebSocket;

    public CallServerInterceptor(boolean z) {
        this.forWebSocket = z;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        boolean z;
        Response.Builder builder;
        Response response;
        Intrinsics.b(chain, "chain");
        RealInterceptorChain realInterceptorChain = (RealInterceptorChain) chain;
        Exchange exchange$okhttp = realInterceptorChain.getExchange$okhttp();
        Long l = null;
        if (exchange$okhttp != null) {
            Request request$okhttp = realInterceptorChain.getRequest$okhttp();
            RequestBody body = request$okhttp.body();
            long currentTimeMillis = System.currentTimeMillis();
            exchange$okhttp.writeRequestHeaders(request$okhttp);
            if (!HttpMethod.permitsRequestBody(request$okhttp.method()) || body == null) {
                exchange$okhttp.noRequestBody();
                builder = null;
                z = true;
            } else {
                if (StringsKt__StringsJVMKt.b("100-continue", request$okhttp.header("Expect"), true)) {
                    exchange$okhttp.flushRequest();
                    builder = exchange$okhttp.readResponseHeaders(true);
                    exchange$okhttp.responseHeadersStart();
                    z = false;
                } else {
                    builder = null;
                    z = true;
                }
                if (builder != null) {
                    exchange$okhttp.noRequestBody();
                    if (!exchange$okhttp.getConnection$okhttp().isMultiplexed()) {
                        exchange$okhttp.noNewExchangesOnConnection();
                    }
                } else if (body.isDuplex()) {
                    exchange$okhttp.flushRequest();
                    body.writeTo(Okio.a(exchange$okhttp.createRequestBody(request$okhttp, true)));
                } else {
                    BufferedSink a2 = Okio.a(exchange$okhttp.createRequestBody(request$okhttp, false));
                    body.writeTo(a2);
                    a2.close();
                }
            }
            if (body == null || !body.isDuplex()) {
                exchange$okhttp.finishRequest();
            }
            if (builder == null) {
                builder = exchange$okhttp.readResponseHeaders(false);
                if (builder == null) {
                    Intrinsics.a();
                    throw null;
                } else if (z) {
                    exchange$okhttp.responseHeadersStart();
                    z = false;
                }
            }
            Response build = builder.request(request$okhttp).handshake(exchange$okhttp.getConnection$okhttp().handshake()).sentRequestAtMillis(currentTimeMillis).receivedResponseAtMillis(System.currentTimeMillis()).build();
            int code = build.code();
            if (code == 100) {
                Response.Builder readResponseHeaders = exchange$okhttp.readResponseHeaders(false);
                if (readResponseHeaders != null) {
                    if (z) {
                        exchange$okhttp.responseHeadersStart();
                    }
                    build = readResponseHeaders.request(request$okhttp).handshake(exchange$okhttp.getConnection$okhttp().handshake()).sentRequestAtMillis(currentTimeMillis).receivedResponseAtMillis(System.currentTimeMillis()).build();
                    code = build.code();
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
            exchange$okhttp.responseHeadersEnd(build);
            if (!this.forWebSocket || code != 101) {
                response = build.newBuilder().body(exchange$okhttp.openResponseBody(build)).build();
            } else {
                response = build.newBuilder().body(Util.EMPTY_RESPONSE).build();
            }
            if (StringsKt__StringsJVMKt.b(JavascriptBridge.MraidHandler.CLOSE_ACTION, response.request().header("Connection"), true) || StringsKt__StringsJVMKt.b(JavascriptBridge.MraidHandler.CLOSE_ACTION, Response.header$default(response, "Connection", (String) null, 2, (Object) null), true)) {
                exchange$okhttp.noNewExchangesOnConnection();
            }
            if (code == 204 || code == 205) {
                ResponseBody body2 = response.body();
                if ((body2 != null ? body2.contentLength() : -1) > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("HTTP ");
                    sb.append(code);
                    sb.append(" had non-zero Content-Length: ");
                    ResponseBody body3 = response.body();
                    if (body3 != null) {
                        l = Long.valueOf(body3.contentLength());
                    }
                    sb.append(l);
                    throw new ProtocolException(sb.toString());
                }
            }
            return response;
        }
        Intrinsics.a();
        throw null;
    }
}
