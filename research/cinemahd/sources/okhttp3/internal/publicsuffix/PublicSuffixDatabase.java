package okhttp3.internal.publicsuffix;

import com.facebook.imageutils.JfifUtil;
import java.net.IDN;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.internal.Util;

public final class PublicSuffixDatabase {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final char EXCEPTION_MARKER = '!';
    private static final List<String> PREVAILING_RULE = CollectionsKt__CollectionsJVMKt.a("*");
    public static final String PUBLIC_SUFFIX_RESOURCE = "publicsuffixes.gz";
    private static final byte[] WILDCARD_LABEL = {(byte) 42};
    /* access modifiers changed from: private */
    public static final PublicSuffixDatabase instance = new PublicSuffixDatabase();
    private final AtomicBoolean listRead = new AtomicBoolean(false);
    private byte[] publicSuffixExceptionListBytes;
    /* access modifiers changed from: private */
    public byte[] publicSuffixListBytes;
    private final CountDownLatch readCompleteLatch = new CountDownLatch(1);

    public static final class Companion {
        private Companion() {
        }

        /* access modifiers changed from: private */
        public final String binarySearch(byte[] bArr, byte[][] bArr2, int i) {
            int i2;
            int i3;
            int and;
            byte[] bArr3 = bArr;
            byte[][] bArr4 = bArr2;
            int length = bArr3.length;
            int i4 = 0;
            while (i4 < length) {
                int i5 = (i4 + length) / 2;
                while (i5 > -1 && bArr3[i5] != ((byte) 10)) {
                    i5--;
                }
                int i6 = i5 + 1;
                int i7 = 1;
                while (true) {
                    i2 = i6 + i7;
                    if (bArr3[i2] == ((byte) 10)) {
                        break;
                    }
                    i7++;
                }
                int i8 = i2 - i6;
                int i9 = i;
                boolean z = false;
                int i10 = 0;
                int i11 = 0;
                while (true) {
                    if (z) {
                        z = false;
                        i3 = 46;
                    } else {
                        i3 = Util.and(bArr4[i9][i10], (int) JfifUtil.MARKER_FIRST_BYTE);
                    }
                    and = i3 - Util.and(bArr3[i6 + i11], (int) JfifUtil.MARKER_FIRST_BYTE);
                    if (and == 0) {
                        i11++;
                        i10++;
                        if (i11 == i8) {
                            break;
                        } else if (bArr4[i9].length == i10) {
                            if (i9 == bArr4.length - 1) {
                                break;
                            }
                            i9++;
                            z = true;
                            i10 = -1;
                        }
                    } else {
                        break;
                    }
                }
                if (and >= 0) {
                    if (and <= 0) {
                        int i12 = i8 - i11;
                        int length2 = bArr4[i9].length - i10;
                        int length3 = bArr4.length;
                        for (int i13 = i9 + 1; i13 < length3; i13++) {
                            length2 += bArr4[i13].length;
                        }
                        if (length2 >= i12) {
                            if (length2 <= i12) {
                                Charset charset = StandardCharsets.UTF_8;
                                Intrinsics.a((Object) charset, "UTF_8");
                                return new String(bArr3, i6, i8, charset);
                            }
                        }
                    }
                    i4 = i2 + 1;
                }
                length = i6 - 1;
            }
            return null;
        }

        public final PublicSuffixDatabase get() {
            return PublicSuffixDatabase.instance;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public static final /* synthetic */ byte[] access$getPublicSuffixListBytes$p(PublicSuffixDatabase publicSuffixDatabase) {
        byte[] bArr = publicSuffixDatabase.publicSuffixListBytes;
        if (bArr != null) {
            return bArr;
        }
        Intrinsics.d("publicSuffixListBytes");
        throw null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00de  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.util.List<java.lang.String> findMatchingRule(java.util.List<java.lang.String> r15) {
        /*
            r14 = this;
            java.util.concurrent.atomic.AtomicBoolean r0 = r14.listRead
            boolean r0 = r0.get()
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0016
            java.util.concurrent.atomic.AtomicBoolean r0 = r14.listRead
            boolean r0 = r0.compareAndSet(r1, r2)
            if (r0 == 0) goto L_0x0016
            r14.readTheListUninterruptibly()
            goto L_0x0023
        L_0x0016:
            java.util.concurrent.CountDownLatch r0 = r14.readCompleteLatch     // Catch:{ InterruptedException -> 0x001c }
            r0.await()     // Catch:{ InterruptedException -> 0x001c }
            goto L_0x0023
        L_0x001c:
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x0023:
            byte[] r0 = r14.publicSuffixListBytes
            if (r0 == 0) goto L_0x0029
            r0 = 1
            goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            if (r0 == 0) goto L_0x011e
            int r0 = r15.size()
            byte[][] r3 = new byte[r0][]
            r4 = 0
        L_0x0033:
            if (r4 >= r0) goto L_0x005a
            java.lang.Object r5 = r15.get(r4)
            java.lang.String r5 = (java.lang.String) r5
            java.nio.charset.Charset r6 = java.nio.charset.StandardCharsets.UTF_8
            java.lang.String r7 = "UTF_8"
            kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r6, (java.lang.String) r7)
            if (r5 == 0) goto L_0x0052
            byte[] r5 = r5.getBytes(r6)
            java.lang.String r6 = "(this as java.lang.String).getBytes(charset)"
            kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r5, (java.lang.String) r6)
            r3[r4] = r5
            int r4 = r4 + 1
            goto L_0x0033
        L_0x0052:
            kotlin.TypeCastException r15 = new kotlin.TypeCastException
            java.lang.String r0 = "null cannot be cast to non-null type java.lang.String"
            r15.<init>(r0)
            throw r15
        L_0x005a:
            int r15 = r3.length
            r0 = 0
        L_0x005c:
            java.lang.String r4 = "publicSuffixListBytes"
            r5 = 0
            if (r0 >= r15) goto L_0x0075
            okhttp3.internal.publicsuffix.PublicSuffixDatabase$Companion r6 = Companion
            byte[] r7 = r14.publicSuffixListBytes
            if (r7 == 0) goto L_0x0071
            java.lang.String r6 = r6.binarySearch(r7, r3, r0)
            if (r6 == 0) goto L_0x006e
            goto L_0x0076
        L_0x006e:
            int r0 = r0 + 1
            goto L_0x005c
        L_0x0071:
            kotlin.jvm.internal.Intrinsics.d(r4)
            throw r5
        L_0x0075:
            r6 = r5
        L_0x0076:
            int r15 = r3.length
            if (r15 <= r2) goto L_0x009d
            java.lang.Object r15 = r3.clone()
            byte[][] r15 = (byte[][]) r15
            int r0 = r15.length
            int r0 = r0 - r2
            r7 = 0
        L_0x0082:
            if (r7 >= r0) goto L_0x009d
            byte[] r8 = WILDCARD_LABEL
            r15[r7] = r8
            okhttp3.internal.publicsuffix.PublicSuffixDatabase$Companion r8 = Companion
            byte[] r9 = r14.publicSuffixListBytes
            if (r9 == 0) goto L_0x0099
            java.lang.String r8 = r8.binarySearch(r9, r15, r7)
            if (r8 == 0) goto L_0x0096
            r15 = r8
            goto L_0x009e
        L_0x0096:
            int r7 = r7 + 1
            goto L_0x0082
        L_0x0099:
            kotlin.jvm.internal.Intrinsics.d(r4)
            throw r5
        L_0x009d:
            r15 = r5
        L_0x009e:
            if (r15 == 0) goto L_0x00bc
            int r0 = r3.length
            int r0 = r0 - r2
            r4 = 0
        L_0x00a3:
            if (r4 >= r0) goto L_0x00bc
            okhttp3.internal.publicsuffix.PublicSuffixDatabase$Companion r7 = Companion
            byte[] r8 = r14.publicSuffixExceptionListBytes
            if (r8 == 0) goto L_0x00b6
            java.lang.String r7 = r7.binarySearch(r8, r3, r4)
            if (r7 == 0) goto L_0x00b3
            r5 = r7
            goto L_0x00bc
        L_0x00b3:
            int r4 = r4 + 1
            goto L_0x00a3
        L_0x00b6:
            java.lang.String r15 = "publicSuffixExceptionListBytes"
            kotlin.jvm.internal.Intrinsics.d(r15)
            throw r5
        L_0x00bc:
            r0 = 46
            if (r5 == 0) goto L_0x00de
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r3 = 33
            r15.append(r3)
            r15.append(r5)
            java.lang.String r6 = r15.toString()
            char[] r7 = new char[r2]
            r7[r1] = r0
            r8 = 0
            r9 = 0
            r10 = 6
            r11 = 0
            java.util.List r15 = kotlin.text.StringsKt__StringsKt.a((java.lang.CharSequence) r6, (char[]) r7, (boolean) r8, (int) r9, (int) r10, (java.lang.Object) r11)
            return r15
        L_0x00de:
            if (r6 != 0) goto L_0x00e5
            if (r15 != 0) goto L_0x00e5
            java.util.List<java.lang.String> r15 = PREVAILING_RULE
            return r15
        L_0x00e5:
            if (r6 == 0) goto L_0x00f8
            char[] r5 = new char[r2]
            r5[r1] = r0
            r3 = 0
            r7 = 0
            r8 = 6
            r9 = 0
            r4 = r6
            r6 = r3
            java.util.List r3 = kotlin.text.StringsKt__StringsKt.a((java.lang.CharSequence) r4, (char[]) r5, (boolean) r6, (int) r7, (int) r8, (java.lang.Object) r9)
            if (r3 == 0) goto L_0x00f8
            goto L_0x00fc
        L_0x00f8:
            java.util.List r3 = kotlin.collections.CollectionsKt__CollectionsKt.a()
        L_0x00fc:
            if (r15 == 0) goto L_0x010e
            char[] r9 = new char[r2]
            r9[r1] = r0
            r10 = 0
            r11 = 0
            r12 = 6
            r13 = 0
            r8 = r15
            java.util.List r15 = kotlin.text.StringsKt__StringsKt.a((java.lang.CharSequence) r8, (char[]) r9, (boolean) r10, (int) r11, (int) r12, (java.lang.Object) r13)
            if (r15 == 0) goto L_0x010e
            goto L_0x0112
        L_0x010e:
            java.util.List r15 = kotlin.collections.CollectionsKt__CollectionsKt.a()
        L_0x0112:
            int r0 = r3.size()
            int r1 = r15.size()
            if (r0 <= r1) goto L_0x011d
            r15 = r3
        L_0x011d:
            return r15
        L_0x011e:
            java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unable to load publicsuffixes.gz resource from the classpath."
            java.lang.String r0 = r0.toString()
            r15.<init>(r0)
            throw r15
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.publicsuffix.PublicSuffixDatabase.findMatchingRule(java.util.List):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0050, code lost:
        kotlin.io.CloseableKt.a(r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0053, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void readTheList() throws java.io.IOException {
        /*
            r4 = this;
            java.lang.Class<okhttp3.internal.publicsuffix.PublicSuffixDatabase> r0 = okhttp3.internal.publicsuffix.PublicSuffixDatabase.class
            java.lang.String r1 = "publicsuffixes.gz"
            java.io.InputStream r0 = r0.getResourceAsStream(r1)
            if (r0 == 0) goto L_0x0054
            okio.GzipSource r1 = new okio.GzipSource
            okio.Source r0 = okio.Okio.a((java.io.InputStream) r0)
            r1.<init>(r0)
            okio.BufferedSource r0 = okio.Okio.a((okio.Source) r1)
            int r1 = r0.readInt()     // Catch:{ all -> 0x004d }
            long r1 = (long) r1     // Catch:{ all -> 0x004d }
            byte[] r1 = r0.b(r1)     // Catch:{ all -> 0x004d }
            int r2 = r0.readInt()     // Catch:{ all -> 0x004d }
            long r2 = (long) r2     // Catch:{ all -> 0x004d }
            byte[] r2 = r0.b(r2)     // Catch:{ all -> 0x004d }
            kotlin.Unit r3 = kotlin.Unit.f6917a     // Catch:{ all -> 0x004d }
            r3 = 0
            kotlin.io.CloseableKt.a(r0, r3)
            monitor-enter(r4)
            if (r1 == 0) goto L_0x0047
            r4.publicSuffixListBytes = r1     // Catch:{ all -> 0x0045 }
            if (r2 == 0) goto L_0x0041
            r4.publicSuffixExceptionListBytes = r2     // Catch:{ all -> 0x0045 }
            kotlin.Unit r0 = kotlin.Unit.f6917a     // Catch:{ all -> 0x0045 }
            monitor-exit(r4)
            java.util.concurrent.CountDownLatch r0 = r4.readCompleteLatch
            r0.countDown()
            return
        L_0x0041:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x0045 }
            throw r3
        L_0x0045:
            r0 = move-exception
            goto L_0x004b
        L_0x0047:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x0045 }
            throw r3
        L_0x004b:
            monitor-exit(r4)
            throw r0
        L_0x004d:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x004f }
        L_0x004f:
            r2 = move-exception
            kotlin.io.CloseableKt.a(r0, r1)
            throw r2
        L_0x0054:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.publicsuffix.PublicSuffixDatabase.readTheList():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0027 */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void readTheListUninterruptibly() {
        /*
            r5 = this;
            r0 = 0
        L_0x0001:
            r5.readTheList()     // Catch:{  }
            if (r0 == 0) goto L_0x000d
            java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ InterruptedIOException -> 0x0027, IOException -> 0x0010 }
            r1.interrupt()     // Catch:{ InterruptedIOException -> 0x0027, IOException -> 0x0010 }
        L_0x000d:
            return
        L_0x000e:
            r1 = move-exception
            goto L_0x002c
        L_0x0010:
            r1 = move-exception
            okhttp3.internal.platform.Platform$Companion r2 = okhttp3.internal.platform.Platform.Companion     // Catch:{ all -> 0x000e }
            okhttp3.internal.platform.Platform r2 = r2.get()     // Catch:{ all -> 0x000e }
            java.lang.String r3 = "Failed to read public suffix list"
            r4 = 5
            r2.log(r3, r4, r1)     // Catch:{ all -> 0x000e }
            if (r0 == 0) goto L_0x0026
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x0026:
            return
        L_0x0027:
            java.lang.Thread.interrupted()     // Catch:{ all -> 0x000e }
            r0 = 1
            goto L_0x0001
        L_0x002c:
            if (r0 == 0) goto L_0x0035
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x0035:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.publicsuffix.PublicSuffixDatabase.readTheListUninterruptibly():void");
    }

    public final String getEffectiveTldPlusOne(String str) {
        int i;
        int i2;
        Intrinsics.b(str, "domain");
        String unicode = IDN.toUnicode(str);
        Intrinsics.a((Object) unicode, "unicodeDomain");
        List a2 = StringsKt__StringsKt.a((CharSequence) unicode, new char[]{'.'}, false, 0, 6, (Object) null);
        List<String> findMatchingRule = findMatchingRule(a2);
        if (a2.size() == findMatchingRule.size() && findMatchingRule.get(0).charAt(0) != '!') {
            return null;
        }
        if (findMatchingRule.get(0).charAt(0) == '!') {
            i2 = a2.size();
            i = findMatchingRule.size();
        } else {
            i2 = a2.size();
            i = findMatchingRule.size() + 1;
        }
        return SequencesKt___SequencesKt.a(SequencesKt___SequencesKt.a(CollectionsKt___CollectionsKt.a(StringsKt__StringsKt.a((CharSequence) str, new char[]{'.'}, false, 0, 6, (Object) null)), i2 - i), ".", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null);
    }

    public final void setListBytes(byte[] bArr, byte[] bArr2) {
        Intrinsics.b(bArr, "publicSuffixListBytes");
        Intrinsics.b(bArr2, "publicSuffixExceptionListBytes");
        this.publicSuffixListBytes = bArr;
        this.publicSuffixExceptionListBytes = bArr2;
        this.listRead.set(true);
        this.readCompleteLatch.countDown();
    }
}
