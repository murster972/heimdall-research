package okhttp3.internal.ws;

import com.facebook.common.time.Clock;
import java.io.Closeable;
import java.io.IOException;
import java.util.zip.Inflater;
import kotlin.jvm.internal.Intrinsics;
import okio.Buffer;
import okio.InflaterSource;
import okio.Source;

public final class MessageInflater implements Closeable {
    private final Buffer deflatedBytes = new Buffer();
    private final Inflater inflater = new Inflater(true);
    private final InflaterSource inflaterSource = new InflaterSource((Source) this.deflatedBytes, this.inflater);
    private final boolean noContextTakeover;

    public MessageInflater(boolean z) {
        this.noContextTakeover = z;
    }

    public void close() throws IOException {
        this.inflaterSource.close();
    }

    public final void inflate(Buffer buffer) throws IOException {
        Intrinsics.b(buffer, "buffer");
        if (this.deflatedBytes.u() == 0) {
            if (this.noContextTakeover) {
                this.inflater.reset();
            }
            this.deflatedBytes.a((Source) buffer);
            this.deflatedBytes.writeInt(65535);
            long bytesRead = this.inflater.getBytesRead() + this.deflatedBytes.u();
            do {
                this.inflaterSource.b(buffer, Clock.MAX_TIME);
            } while (this.inflater.getBytesRead() < bytesRead);
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
}
