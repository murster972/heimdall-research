package okhttp3.internal.ws;

import kotlin.jvm.internal.Intrinsics;

public final class WebSocketExtensions {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String HEADER_WEB_SOCKET_EXTENSION = "Sec-WebSocket-Extensions";
    public final Integer clientMaxWindowBits;
    public final boolean clientNoContextTakeover;
    public final boolean perMessageDeflate;
    public final Integer serverMaxWindowBits;
    public final boolean serverNoContextTakeover;
    public final boolean unknownValues;

    public static final class Companion {
        private Companion() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0057, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0059, code lost:
            if (r15 >= r13) goto L_0x00cf;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x005b, code lost:
            r2 = okhttp3.internal.Util.delimiterOffset(r5, ';', r15, r13);
            r6 = okhttp3.internal.Util.delimiterOffset(r5, '=', r15, r2);
            r8 = okhttp3.internal.Util.trimSubstring(r5, r15, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0069, code lost:
            if (r6 >= r2) goto L_0x0078;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x006b, code lost:
            r6 = kotlin.text.StringsKt__StringsKt.c(okhttp3.internal.Util.trimSubstring(r5, r6 + 1, r2), "\"");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0078, code lost:
            r6 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0079, code lost:
            r15 = r2 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0081, code lost:
            if (kotlin.text.StringsKt__StringsJVMKt.b(r8, "client_max_window_bits", true) == false) goto L_0x0093;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0083, code lost:
            if (r7 == null) goto L_0x0087;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0087, code lost:
            if (r6 == null) goto L_0x008e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0089, code lost:
            r2 = kotlin.text.StringsKt__StringNumberConversionsKt.a(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x008e, code lost:
            r2 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x008f, code lost:
            r7 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0090, code lost:
            if (r2 != null) goto L_0x0059;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0099, code lost:
            if (kotlin.text.StringsKt__StringsJVMKt.b(r8, "client_no_context_takeover", true) == false) goto L_0x00a5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x009b, code lost:
            if (r10 == false) goto L_0x009f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x009d, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x009f, code lost:
            if (r6 == null) goto L_0x00a3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a1, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a3, code lost:
            r10 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ab, code lost:
            if (kotlin.text.StringsKt__StringsJVMKt.b(r8, "server_max_window_bits", true) == false) goto L_0x00bd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x00ad, code lost:
            if (r9 == null) goto L_0x00b1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x00af, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b1, code lost:
            if (r6 == null) goto L_0x00b8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b3, code lost:
            r2 = kotlin.text.StringsKt__StringNumberConversionsKt.a(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b8, code lost:
            r2 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b9, code lost:
            r9 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ba, code lost:
            if (r2 != null) goto L_0x0059;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c3, code lost:
            if (kotlin.text.StringsKt__StringsJVMKt.b(r8, "server_no_context_takeover", true) == false) goto L_0x0057;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c5, code lost:
            if (r11 == false) goto L_0x00c9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c7, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c9, code lost:
            if (r6 == null) goto L_0x00cd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x00cb, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x00cd, code lost:
            r11 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x00cf, code lost:
            r6 = r15;
            r8 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0055, code lost:
            if (r8 != false) goto L_0x0057;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final okhttp3.internal.ws.WebSocketExtensions parse(okhttp3.Headers r21) throws java.io.IOException {
            /*
                r20 = this;
                r0 = r21
                java.lang.String r1 = "responseHeaders"
                kotlin.jvm.internal.Intrinsics.b(r0, r1)
                int r1 = r21.size()
                r3 = 0
                r4 = 0
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 0
                r10 = 0
                r11 = 0
            L_0x0013:
                if (r4 >= r1) goto L_0x00e1
                java.lang.String r5 = r0.name(r4)
                r12 = 1
                java.lang.String r13 = "Sec-WebSocket-Extensions"
                boolean r5 = kotlin.text.StringsKt__StringsJVMKt.b(r5, r13, r12)
                if (r5 != 0) goto L_0x0024
                goto L_0x00dd
            L_0x0024:
                java.lang.String r5 = r0.value(r4)
                r19 = r11
                r11 = r10
                r10 = r8
                r8 = r6
                r6 = 0
            L_0x002e:
                int r13 = r5.length()
                if (r6 >= r13) goto L_0x00d8
                r14 = 44
                r16 = 0
                r17 = 4
                r18 = 0
                r13 = r5
                r15 = r6
                int r13 = okhttp3.internal.Util.delimiterOffset$default((java.lang.String) r13, (char) r14, (int) r15, (int) r16, (int) r17, (java.lang.Object) r18)
                r14 = 59
                int r15 = okhttp3.internal.Util.delimiterOffset((java.lang.String) r5, (char) r14, (int) r6, (int) r13)
                java.lang.String r6 = okhttp3.internal.Util.trimSubstring(r5, r6, r15)
                int r15 = r15 + r12
                java.lang.String r2 = "permessage-deflate"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r6, r2, r12)
                if (r2 == 0) goto L_0x00d3
                if (r8 == 0) goto L_0x0059
            L_0x0057:
                r19 = 1
            L_0x0059:
                if (r15 >= r13) goto L_0x00cf
                int r2 = okhttp3.internal.Util.delimiterOffset((java.lang.String) r5, (char) r14, (int) r15, (int) r13)
                r6 = 61
                int r6 = okhttp3.internal.Util.delimiterOffset((java.lang.String) r5, (char) r6, (int) r15, (int) r2)
                java.lang.String r8 = okhttp3.internal.Util.trimSubstring(r5, r15, r6)
                if (r6 >= r2) goto L_0x0078
                int r6 = r6 + 1
                java.lang.String r6 = okhttp3.internal.Util.trimSubstring(r5, r6, r2)
                java.lang.String r15 = "\""
                java.lang.String r6 = kotlin.text.StringsKt__StringsKt.c(r6, r15)
                goto L_0x0079
            L_0x0078:
                r6 = 0
            L_0x0079:
                int r15 = r2 + 1
                java.lang.String r2 = "client_max_window_bits"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r8, r2, r12)
                if (r2 == 0) goto L_0x0093
                if (r7 == 0) goto L_0x0087
                r19 = 1
            L_0x0087:
                if (r6 == 0) goto L_0x008e
                java.lang.Integer r2 = kotlin.text.StringsKt__StringNumberConversionsKt.a(r6)
                goto L_0x008f
            L_0x008e:
                r2 = 0
            L_0x008f:
                r7 = r2
                if (r2 != 0) goto L_0x0059
                goto L_0x0057
            L_0x0093:
                java.lang.String r2 = "client_no_context_takeover"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r8, r2, r12)
                if (r2 == 0) goto L_0x00a5
                if (r10 == 0) goto L_0x009f
                r19 = 1
            L_0x009f:
                if (r6 == 0) goto L_0x00a3
                r19 = 1
            L_0x00a3:
                r10 = 1
                goto L_0x0059
            L_0x00a5:
                java.lang.String r2 = "server_max_window_bits"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r8, r2, r12)
                if (r2 == 0) goto L_0x00bd
                if (r9 == 0) goto L_0x00b1
                r19 = 1
            L_0x00b1:
                if (r6 == 0) goto L_0x00b8
                java.lang.Integer r2 = kotlin.text.StringsKt__StringNumberConversionsKt.a(r6)
                goto L_0x00b9
            L_0x00b8:
                r2 = 0
            L_0x00b9:
                r9 = r2
                if (r2 != 0) goto L_0x0059
                goto L_0x0057
            L_0x00bd:
                java.lang.String r2 = "server_no_context_takeover"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r8, r2, r12)
                if (r2 == 0) goto L_0x0057
                if (r11 == 0) goto L_0x00c9
                r19 = 1
            L_0x00c9:
                if (r6 == 0) goto L_0x00cd
                r19 = 1
            L_0x00cd:
                r11 = 1
                goto L_0x0059
            L_0x00cf:
                r6 = r15
                r8 = 1
                goto L_0x002e
            L_0x00d3:
                r6 = r15
                r19 = 1
                goto L_0x002e
            L_0x00d8:
                r6 = r8
                r8 = r10
                r10 = r11
                r11 = r19
            L_0x00dd:
                int r4 = r4 + 1
                goto L_0x0013
            L_0x00e1:
                okhttp3.internal.ws.WebSocketExtensions r0 = new okhttp3.internal.ws.WebSocketExtensions
                r5 = r0
                r5.<init>(r6, r7, r8, r9, r10, r11)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.ws.WebSocketExtensions.Companion.parse(okhttp3.Headers):okhttp3.internal.ws.WebSocketExtensions");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WebSocketExtensions() {
        this(false, (Integer) null, false, (Integer) null, false, false, 63, (DefaultConstructorMarker) null);
    }

    public WebSocketExtensions(boolean z, Integer num, boolean z2, Integer num2, boolean z3, boolean z4) {
        this.perMessageDeflate = z;
        this.clientMaxWindowBits = num;
        this.clientNoContextTakeover = z2;
        this.serverMaxWindowBits = num2;
        this.serverNoContextTakeover = z3;
        this.unknownValues = z4;
    }

    public static /* synthetic */ WebSocketExtensions copy$default(WebSocketExtensions webSocketExtensions, boolean z, Integer num, boolean z2, Integer num2, boolean z3, boolean z4, int i, Object obj) {
        if ((i & 1) != 0) {
            z = webSocketExtensions.perMessageDeflate;
        }
        if ((i & 2) != 0) {
            num = webSocketExtensions.clientMaxWindowBits;
        }
        Integer num3 = num;
        if ((i & 4) != 0) {
            z2 = webSocketExtensions.clientNoContextTakeover;
        }
        boolean z5 = z2;
        if ((i & 8) != 0) {
            num2 = webSocketExtensions.serverMaxWindowBits;
        }
        Integer num4 = num2;
        if ((i & 16) != 0) {
            z3 = webSocketExtensions.serverNoContextTakeover;
        }
        boolean z6 = z3;
        if ((i & 32) != 0) {
            z4 = webSocketExtensions.unknownValues;
        }
        return webSocketExtensions.copy(z, num3, z5, num4, z6, z4);
    }

    public final boolean component1() {
        return this.perMessageDeflate;
    }

    public final Integer component2() {
        return this.clientMaxWindowBits;
    }

    public final boolean component3() {
        return this.clientNoContextTakeover;
    }

    public final Integer component4() {
        return this.serverMaxWindowBits;
    }

    public final boolean component5() {
        return this.serverNoContextTakeover;
    }

    public final boolean component6() {
        return this.unknownValues;
    }

    public final WebSocketExtensions copy(boolean z, Integer num, boolean z2, Integer num2, boolean z3, boolean z4) {
        return new WebSocketExtensions(z, num, z2, num2, z3, z4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WebSocketExtensions)) {
            return false;
        }
        WebSocketExtensions webSocketExtensions = (WebSocketExtensions) obj;
        return this.perMessageDeflate == webSocketExtensions.perMessageDeflate && Intrinsics.a((Object) this.clientMaxWindowBits, (Object) webSocketExtensions.clientMaxWindowBits) && this.clientNoContextTakeover == webSocketExtensions.clientNoContextTakeover && Intrinsics.a((Object) this.serverMaxWindowBits, (Object) webSocketExtensions.serverMaxWindowBits) && this.serverNoContextTakeover == webSocketExtensions.serverNoContextTakeover && this.unknownValues == webSocketExtensions.unknownValues;
    }

    public int hashCode() {
        boolean z = this.perMessageDeflate;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i = (z ? 1 : 0) * true;
        Integer num = this.clientMaxWindowBits;
        int i2 = 0;
        int hashCode = (i + (num != null ? num.hashCode() : 0)) * 31;
        boolean z3 = this.clientNoContextTakeover;
        if (z3) {
            z3 = true;
        }
        int i3 = (hashCode + (z3 ? 1 : 0)) * 31;
        Integer num2 = this.serverMaxWindowBits;
        if (num2 != null) {
            i2 = num2.hashCode();
        }
        int i4 = (i3 + i2) * 31;
        boolean z4 = this.serverNoContextTakeover;
        if (z4) {
            z4 = true;
        }
        int i5 = (i4 + (z4 ? 1 : 0)) * 31;
        boolean z5 = this.unknownValues;
        if (!z5) {
            z2 = z5;
        }
        return i5 + (z2 ? 1 : 0);
    }

    public final boolean noContextTakeover(boolean z) {
        if (z) {
            return this.clientNoContextTakeover;
        }
        return this.serverNoContextTakeover;
    }

    public String toString() {
        return "WebSocketExtensions(perMessageDeflate=" + this.perMessageDeflate + ", clientMaxWindowBits=" + this.clientMaxWindowBits + ", clientNoContextTakeover=" + this.clientNoContextTakeover + ", serverMaxWindowBits=" + this.serverMaxWindowBits + ", serverNoContextTakeover=" + this.serverNoContextTakeover + ", unknownValues=" + this.unknownValues + ")";
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ WebSocketExtensions(boolean r6, java.lang.Integer r7, boolean r8, java.lang.Integer r9, boolean r10, boolean r11, int r12, kotlin.jvm.internal.DefaultConstructorMarker r13) {
        /*
            r5 = this;
            r13 = r12 & 1
            r0 = 0
            if (r13 == 0) goto L_0x0007
            r13 = 0
            goto L_0x0008
        L_0x0007:
            r13 = r6
        L_0x0008:
            r6 = r12 & 2
            r1 = 0
            if (r6 == 0) goto L_0x000f
            r2 = r1
            goto L_0x0010
        L_0x000f:
            r2 = r7
        L_0x0010:
            r6 = r12 & 4
            if (r6 == 0) goto L_0x0016
            r3 = 0
            goto L_0x0017
        L_0x0016:
            r3 = r8
        L_0x0017:
            r6 = r12 & 8
            if (r6 == 0) goto L_0x001c
            goto L_0x001d
        L_0x001c:
            r1 = r9
        L_0x001d:
            r6 = r12 & 16
            if (r6 == 0) goto L_0x0023
            r4 = 0
            goto L_0x0024
        L_0x0023:
            r4 = r10
        L_0x0024:
            r6 = r12 & 32
            if (r6 == 0) goto L_0x002a
            r12 = 0
            goto L_0x002b
        L_0x002a:
            r12 = r11
        L_0x002b:
            r6 = r5
            r7 = r13
            r8 = r2
            r9 = r3
            r10 = r1
            r11 = r4
            r6.<init>(r7, r8, r9, r10, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.ws.WebSocketExtensions.<init>(boolean, java.lang.Integer, boolean, java.lang.Integer, boolean, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
