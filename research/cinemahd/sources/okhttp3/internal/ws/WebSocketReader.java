package okhttp3.internal.ws;

import com.facebook.imageutils.JfifUtil;
import java.io.Closeable;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.internal.Util;
import okio.Buffer;
import okio.BufferedSource;
import okio.ByteString;

public final class WebSocketReader implements Closeable {
    private boolean closed;
    private final Buffer controlFrameBuffer = new Buffer();
    private final FrameCallback frameCallback;
    private long frameLength;
    private final boolean isClient;
    private boolean isControlFrame;
    private boolean isFinalFrame;
    private final Buffer.UnsafeCursor maskCursor;
    private final byte[] maskKey;
    private final Buffer messageFrameBuffer = new Buffer();
    private MessageInflater messageInflater;
    private final boolean noContextTakeover;
    private int opcode;
    private final boolean perMessageDeflate;
    private boolean readingCompressedMessage;
    private final BufferedSource source;

    public interface FrameCallback {
        void onReadClose(int i, String str);

        void onReadMessage(String str) throws IOException;

        void onReadMessage(ByteString byteString) throws IOException;

        void onReadPing(ByteString byteString);

        void onReadPong(ByteString byteString);
    }

    public WebSocketReader(boolean z, BufferedSource bufferedSource, FrameCallback frameCallback2, boolean z2, boolean z3) {
        Intrinsics.b(bufferedSource, "source");
        Intrinsics.b(frameCallback2, "frameCallback");
        this.isClient = z;
        this.source = bufferedSource;
        this.frameCallback = frameCallback2;
        this.perMessageDeflate = z2;
        this.noContextTakeover = z3;
        Buffer.UnsafeCursor unsafeCursor = null;
        this.maskKey = this.isClient ? null : new byte[4];
        this.maskCursor = !this.isClient ? new Buffer.UnsafeCursor() : unsafeCursor;
    }

    private final void readControlFrame() throws IOException {
        String str;
        long j = this.frameLength;
        if (j > 0) {
            this.source.a(this.controlFrameBuffer, j);
            if (!this.isClient) {
                Buffer buffer = this.controlFrameBuffer;
                Buffer.UnsafeCursor unsafeCursor = this.maskCursor;
                if (unsafeCursor != null) {
                    buffer.a(unsafeCursor);
                    this.maskCursor.i(0);
                    WebSocketProtocol webSocketProtocol = WebSocketProtocol.INSTANCE;
                    Buffer.UnsafeCursor unsafeCursor2 = this.maskCursor;
                    byte[] bArr = this.maskKey;
                    if (bArr != null) {
                        webSocketProtocol.toggleMask(unsafeCursor2, bArr);
                        this.maskCursor.close();
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
        }
        switch (this.opcode) {
            case 8:
                short s = 1005;
                long u = this.controlFrameBuffer.u();
                if (u != 1) {
                    if (u != 0) {
                        s = this.controlFrameBuffer.readShort();
                        str = this.controlFrameBuffer.h();
                        String closeCodeExceptionMessage = WebSocketProtocol.INSTANCE.closeCodeExceptionMessage(s);
                        if (closeCodeExceptionMessage != null) {
                            throw new ProtocolException(closeCodeExceptionMessage);
                        }
                    } else {
                        str = "";
                    }
                    this.frameCallback.onReadClose(s, str);
                    this.closed = true;
                    return;
                }
                throw new ProtocolException("Malformed close payload length of 1.");
            case 9:
                this.frameCallback.onReadPing(this.controlFrameBuffer.g());
                return;
            case 10:
                this.frameCallback.onReadPong(this.controlFrameBuffer.g());
                return;
            default:
                throw new ProtocolException("Unknown control opcode: " + Util.toHexString(this.opcode));
        }
    }

    /* JADX INFO: finally extract failed */
    private final void readHeader() throws IOException, ProtocolException {
        if (!this.closed) {
            long timeoutNanos = this.source.timeout().timeoutNanos();
            this.source.timeout().clearTimeout();
            try {
                int and = Util.and(this.source.readByte(), (int) JfifUtil.MARKER_FIRST_BYTE);
                this.source.timeout().timeout(timeoutNanos, TimeUnit.NANOSECONDS);
                this.opcode = and & 15;
                boolean z = false;
                this.isFinalFrame = (and & 128) != 0;
                this.isControlFrame = (and & 8) != 0;
                if (!this.isControlFrame || this.isFinalFrame) {
                    boolean z2 = (and & 64) != 0;
                    int i = this.opcode;
                    if (i == 1 || i == 2) {
                        if (!z2) {
                            this.readingCompressedMessage = false;
                        } else if (this.perMessageDeflate) {
                            this.readingCompressedMessage = true;
                        } else {
                            throw new ProtocolException("Unexpected rsv1 flag");
                        }
                    } else if (z2) {
                        throw new ProtocolException("Unexpected rsv1 flag");
                    }
                    if (!((and & 32) != 0)) {
                        if (!((and & 16) != 0)) {
                            int and2 = Util.and(this.source.readByte(), (int) JfifUtil.MARKER_FIRST_BYTE);
                            if ((and2 & 128) != 0) {
                                z = true;
                            }
                            boolean z3 = this.isClient;
                            if (z == z3) {
                                throw new ProtocolException(z3 ? "Server-sent frames must not be masked." : "Client-sent frames must be masked.");
                            }
                            this.frameLength = (long) (and2 & 127);
                            long j = this.frameLength;
                            if (j == ((long) WebSocketProtocol.PAYLOAD_SHORT)) {
                                this.frameLength = (long) Util.and(this.source.readShort(), 65535);
                            } else if (j == ((long) 127)) {
                                this.frameLength = this.source.readLong();
                                if (this.frameLength < 0) {
                                    throw new ProtocolException("Frame length 0x" + Util.toHexString(this.frameLength) + " > 0x7FFFFFFFFFFFFFFF");
                                }
                            }
                            if (this.isControlFrame && this.frameLength > 125) {
                                throw new ProtocolException("Control frame must be less than 125B.");
                            } else if (z) {
                                BufferedSource bufferedSource = this.source;
                                byte[] bArr = this.maskKey;
                                if (bArr != null) {
                                    bufferedSource.readFully(bArr);
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            }
                        } else {
                            throw new ProtocolException("Unexpected rsv3 flag");
                        }
                    } else {
                        throw new ProtocolException("Unexpected rsv2 flag");
                    }
                } else {
                    throw new ProtocolException("Control frames must be final.");
                }
            } catch (Throwable th) {
                this.source.timeout().timeout(timeoutNanos, TimeUnit.NANOSECONDS);
                throw th;
            }
        } else {
            throw new IOException("closed");
        }
    }

    private final void readMessage() throws IOException {
        while (!this.closed) {
            long j = this.frameLength;
            if (j > 0) {
                this.source.a(this.messageFrameBuffer, j);
                if (!this.isClient) {
                    Buffer buffer = this.messageFrameBuffer;
                    Buffer.UnsafeCursor unsafeCursor = this.maskCursor;
                    if (unsafeCursor != null) {
                        buffer.a(unsafeCursor);
                        this.maskCursor.i(this.messageFrameBuffer.u() - this.frameLength);
                        WebSocketProtocol webSocketProtocol = WebSocketProtocol.INSTANCE;
                        Buffer.UnsafeCursor unsafeCursor2 = this.maskCursor;
                        byte[] bArr = this.maskKey;
                        if (bArr != null) {
                            webSocketProtocol.toggleMask(unsafeCursor2, bArr);
                            this.maskCursor.close();
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
            }
            if (!this.isFinalFrame) {
                readUntilNonControlFrame();
                if (this.opcode != 0) {
                    throw new ProtocolException("Expected continuation opcode. Got: " + Util.toHexString(this.opcode));
                }
            } else {
                return;
            }
        }
        throw new IOException("closed");
    }

    private final void readMessageFrame() throws IOException {
        int i = this.opcode;
        if (i == 1 || i == 2) {
            readMessage();
            if (this.readingCompressedMessage) {
                MessageInflater messageInflater2 = this.messageInflater;
                if (messageInflater2 == null) {
                    messageInflater2 = new MessageInflater(this.noContextTakeover);
                    this.messageInflater = messageInflater2;
                }
                messageInflater2.inflate(this.messageFrameBuffer);
            }
            if (i == 1) {
                this.frameCallback.onReadMessage(this.messageFrameBuffer.h());
            } else {
                this.frameCallback.onReadMessage(this.messageFrameBuffer.g());
            }
        } else {
            throw new ProtocolException("Unknown opcode: " + Util.toHexString(i));
        }
    }

    private final void readUntilNonControlFrame() throws IOException {
        while (!this.closed) {
            readHeader();
            if (this.isControlFrame) {
                readControlFrame();
            } else {
                return;
            }
        }
    }

    public void close() throws IOException {
        MessageInflater messageInflater2 = this.messageInflater;
        if (messageInflater2 != null) {
            messageInflater2.close();
        }
    }

    public final BufferedSource getSource() {
        return this.source;
    }

    public final void processNextFrame() throws IOException {
        readHeader();
        if (this.isControlFrame) {
            readControlFrame();
        } else {
            readMessageFrame();
        }
    }
}
