package okhttp3.internal.ws;

import com.facebook.common.util.UriUtil;
import com.unity3d.ads.metadata.MediationMetaData;
import java.io.Closeable;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Call;
import okhttp3.EventListener;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okhttp3.internal.Util;
import okhttp3.internal.concurrent.Task;
import okhttp3.internal.concurrent.TaskQueue;
import okhttp3.internal.concurrent.TaskRunner;
import okhttp3.internal.connection.Exchange;
import okhttp3.internal.connection.RealCall;
import okhttp3.internal.ws.WebSocketReader;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.ByteString;

public final class RealWebSocket implements WebSocket, WebSocketReader.FrameCallback {
    private static final long CANCEL_AFTER_CLOSE_MILLIS = 60000;
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final long DEFAULT_MINIMUM_DEFLATE_SIZE = 1024;
    private static final long MAX_QUEUE_SIZE = 16777216;
    private static final List<Protocol> ONLY_HTTP1 = CollectionsKt__CollectionsJVMKt.a(Protocol.HTTP_1_1);
    private boolean awaitingPong;
    private Call call;
    private boolean enqueuedClose;
    /* access modifiers changed from: private */
    public WebSocketExtensions extensions;
    private boolean failed;
    private final String key;
    private final WebSocketListener listener;
    /* access modifiers changed from: private */
    public final ArrayDeque<Object> messageAndCloseQueue = new ArrayDeque<>();
    private long minimumDeflateSize;
    /* access modifiers changed from: private */
    public String name;
    private final Request originalRequest;
    private final long pingIntervalMillis;
    private final ArrayDeque<ByteString> pongQueue = new ArrayDeque<>();
    private long queueSize;
    private final Random random;
    private WebSocketReader reader;
    private int receivedCloseCode = -1;
    private String receivedCloseReason;
    private int receivedPingCount;
    private int receivedPongCount;
    private int sentPingCount;
    private Streams streams;
    private TaskQueue taskQueue;
    private WebSocketWriter writer;
    private Task writerTask;

    public static final class Close {
        private final long cancelAfterCloseMillis;
        private final int code;
        private final ByteString reason;

        public Close(int i, ByteString byteString, long j) {
            this.code = i;
            this.reason = byteString;
            this.cancelAfterCloseMillis = j;
        }

        public final long getCancelAfterCloseMillis() {
            return this.cancelAfterCloseMillis;
        }

        public final int getCode() {
            return this.code;
        }

        public final ByteString getReason() {
            return this.reason;
        }
    }

    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public static final class Message {
        private final ByteString data;
        private final int formatOpcode;

        public Message(int i, ByteString byteString) {
            Intrinsics.b(byteString, UriUtil.DATA_SCHEME);
            this.formatOpcode = i;
            this.data = byteString;
        }

        public final ByteString getData() {
            return this.data;
        }

        public final int getFormatOpcode() {
            return this.formatOpcode;
        }
    }

    public static abstract class Streams implements Closeable {
        private final boolean client;
        private final BufferedSink sink;
        private final BufferedSource source;

        public Streams(boolean z, BufferedSource bufferedSource, BufferedSink bufferedSink) {
            Intrinsics.b(bufferedSource, "source");
            Intrinsics.b(bufferedSink, "sink");
            this.client = z;
            this.source = bufferedSource;
            this.sink = bufferedSink;
        }

        public final boolean getClient() {
            return this.client;
        }

        public final BufferedSink getSink() {
            return this.sink;
        }

        public final BufferedSource getSource() {
            return this.source;
        }
    }

    private final class WriterTask extends Task {
        public WriterTask() {
            super(RealWebSocket.this.name + " writer", false, 2, (DefaultConstructorMarker) null);
        }

        public long runOnce() {
            try {
                if (RealWebSocket.this.writeOneFrame$okhttp()) {
                    return 0;
                }
                return -1;
            } catch (IOException e) {
                RealWebSocket.this.failWebSocket(e, (Response) null);
                return -1;
            }
        }
    }

    public RealWebSocket(TaskRunner taskRunner, Request request, WebSocketListener webSocketListener, Random random2, long j, WebSocketExtensions webSocketExtensions, long j2) {
        Intrinsics.b(taskRunner, "taskRunner");
        Intrinsics.b(request, "originalRequest");
        Intrinsics.b(webSocketListener, "listener");
        Intrinsics.b(random2, "random");
        this.originalRequest = request;
        this.listener = webSocketListener;
        this.random = random2;
        this.pingIntervalMillis = j;
        this.extensions = webSocketExtensions;
        this.minimumDeflateSize = j2;
        this.taskQueue = taskRunner.newQueue();
        if (Intrinsics.a((Object) "GET", (Object) this.originalRequest.method())) {
            ByteString.Companion companion = ByteString.d;
            byte[] bArr = new byte[16];
            this.random.nextBytes(bArr);
            this.key = ByteString.Companion.a(companion, bArr, 0, 0, 3, (Object) null).a();
            return;
        }
        throw new IllegalArgumentException(("Request must be GET: " + this.originalRequest.method()).toString());
    }

    /* access modifiers changed from: private */
    public final boolean isValid(WebSocketExtensions webSocketExtensions) {
        if (webSocketExtensions.unknownValues || webSocketExtensions.clientMaxWindowBits != null) {
            return false;
        }
        Integer num = webSocketExtensions.serverMaxWindowBits;
        if (num == null) {
            return true;
        }
        int intValue = num.intValue();
        if (8 > intValue || 15 < intValue) {
            return false;
        }
        return true;
    }

    private final void runWriter() {
        if (!Util.assertionsEnabled || Thread.holdsLock(this)) {
            Task task = this.writerTask;
            if (task != null) {
                TaskQueue.schedule$default(this.taskQueue, task, 0, 2, (Object) null);
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Thread ");
        Thread currentThread = Thread.currentThread();
        Intrinsics.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        sb.append(" MUST hold lock on ");
        sb.append(this);
        throw new AssertionError(sb.toString());
    }

    public final void awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        Intrinsics.b(timeUnit, "timeUnit");
        this.taskQueue.idleLatch().await(j, timeUnit);
    }

    public void cancel() {
        Call call2 = this.call;
        if (call2 != null) {
            call2.cancel();
        } else {
            Intrinsics.a();
            throw null;
        }
    }

    public final void checkUpgradeSuccess$okhttp(Response response, Exchange exchange) throws IOException {
        Intrinsics.b(response, "response");
        if (response.code() == 101) {
            String header$default = Response.header$default(response, "Connection", (String) null, 2, (Object) null);
            if (StringsKt__StringsJVMKt.b("Upgrade", header$default, true)) {
                String header$default2 = Response.header$default(response, "Upgrade", (String) null, 2, (Object) null);
                if (StringsKt__StringsJVMKt.b("websocket", header$default2, true)) {
                    String header$default3 = Response.header$default(response, "Sec-WebSocket-Accept", (String) null, 2, (Object) null);
                    ByteString.Companion companion = ByteString.d;
                    String a2 = companion.c(this.key + WebSocketProtocol.ACCEPT_MAGIC).i().a();
                    if (!Intrinsics.a((Object) a2, (Object) header$default3)) {
                        throw new ProtocolException("Expected 'Sec-WebSocket-Accept' header value '" + a2 + "' but was '" + header$default3 + '\'');
                    } else if (exchange == null) {
                        throw new ProtocolException("Web Socket exchange missing: bad interceptor?");
                    }
                } else {
                    throw new ProtocolException("Expected 'Upgrade' header value 'websocket' but was '" + header$default2 + '\'');
                }
            } else {
                throw new ProtocolException("Expected 'Connection' header value 'Upgrade' but was '" + header$default + '\'');
            }
        } else {
            throw new ProtocolException("Expected HTTP 101 response but was '" + response.code() + ' ' + response.message() + '\'');
        }
    }

    public boolean close(int i, String str) {
        return close(i, str, CANCEL_AFTER_CLOSE_MILLIS);
    }

    public final void connect(OkHttpClient okHttpClient) {
        Intrinsics.b(okHttpClient, "client");
        if (this.originalRequest.header("Sec-WebSocket-Extensions") != null) {
            failWebSocket(new ProtocolException("Request header not permitted: 'Sec-WebSocket-Extensions'"), (Response) null);
            return;
        }
        OkHttpClient build = okHttpClient.newBuilder().eventListener(EventListener.NONE).protocols(ONLY_HTTP1).build();
        Request build2 = this.originalRequest.newBuilder().header("Upgrade", "websocket").header("Connection", "Upgrade").header("Sec-WebSocket-Key", this.key).header("Sec-WebSocket-Version", "13").header("Sec-WebSocket-Extensions", "permessage-deflate").build();
        this.call = new RealCall(build, build2, true);
        Call call2 = this.call;
        if (call2 != null) {
            call2.enqueue(new RealWebSocket$connect$1(this, build2));
        } else {
            Intrinsics.a();
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r4.listener.onFailure(r4, r5, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003a, code lost:
        if (r0 != null) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003c, code lost:
        okhttp3.internal.Util.closeQuietly((java.io.Closeable) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003f, code lost:
        if (r2 != null) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0041, code lost:
        okhttp3.internal.Util.closeQuietly((java.io.Closeable) r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        if (r3 != null) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0046, code lost:
        okhttp3.internal.Util.closeQuietly((java.io.Closeable) r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0049, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void failWebSocket(java.lang.Exception r5, okhttp3.Response r6) {
        /*
            r4 = this;
            java.lang.String r0 = "e"
            kotlin.jvm.internal.Intrinsics.b(r5, r0)
            monitor-enter(r4)
            boolean r0 = r4.failed     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x000c
            monitor-exit(r4)
            return
        L_0x000c:
            r0 = 1
            r4.failed = r0     // Catch:{ all -> 0x004a }
            okhttp3.internal.ws.RealWebSocket$Streams r0 = r4.streams     // Catch:{ all -> 0x004a }
            r1 = 0
            r4.streams = r1     // Catch:{ all -> 0x004a }
            okhttp3.internal.ws.WebSocketReader r2 = r4.reader     // Catch:{ all -> 0x004a }
            r4.reader = r1     // Catch:{ all -> 0x004a }
            okhttp3.internal.ws.WebSocketWriter r3 = r4.writer     // Catch:{ all -> 0x004a }
            r4.writer = r1     // Catch:{ all -> 0x004a }
            okhttp3.internal.concurrent.TaskQueue r1 = r4.taskQueue     // Catch:{ all -> 0x004a }
            r1.shutdown()     // Catch:{ all -> 0x004a }
            kotlin.Unit r1 = kotlin.Unit.f6917a     // Catch:{ all -> 0x004a }
            monitor-exit(r4)
            okhttp3.WebSocketListener r1 = r4.listener     // Catch:{ all -> 0x0039 }
            r1.onFailure(r4, r5, r6)     // Catch:{ all -> 0x0039 }
            if (r0 == 0) goto L_0x002e
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r0)
        L_0x002e:
            if (r2 == 0) goto L_0x0033
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r2)
        L_0x0033:
            if (r3 == 0) goto L_0x0038
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r3)
        L_0x0038:
            return
        L_0x0039:
            r5 = move-exception
            if (r0 == 0) goto L_0x003f
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r0)
        L_0x003f:
            if (r2 == 0) goto L_0x0044
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r2)
        L_0x0044:
            if (r3 == 0) goto L_0x0049
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r3)
        L_0x0049:
            throw r5
        L_0x004a:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.ws.RealWebSocket.failWebSocket(java.lang.Exception, okhttp3.Response):void");
    }

    public final WebSocketListener getListener$okhttp() {
        return this.listener;
    }

    public final void initReaderAndWriter(String str, Streams streams2) throws IOException {
        String str2 = str;
        Streams streams3 = streams2;
        Intrinsics.b(str2, MediationMetaData.KEY_NAME);
        Intrinsics.b(streams3, "streams");
        WebSocketExtensions webSocketExtensions = this.extensions;
        if (webSocketExtensions != null) {
            synchronized (this) {
                this.name = str2;
                this.streams = streams3;
                this.writer = new WebSocketWriter(streams2.getClient(), streams2.getSink(), this.random, webSocketExtensions.perMessageDeflate, webSocketExtensions.noContextTakeover(streams2.getClient()), this.minimumDeflateSize);
                this.writerTask = new WriterTask();
                if (this.pingIntervalMillis != 0) {
                    long nanos = TimeUnit.MILLISECONDS.toNanos(this.pingIntervalMillis);
                    TaskQueue taskQueue2 = this.taskQueue;
                    String str3 = str2 + " ping";
                    RealWebSocket$initReaderAndWriter$$inlined$synchronized$lambda$1 realWebSocket$initReaderAndWriter$$inlined$synchronized$lambda$1 = r1;
                    RealWebSocket$initReaderAndWriter$$inlined$synchronized$lambda$1 realWebSocket$initReaderAndWriter$$inlined$synchronized$lambda$12 = new RealWebSocket$initReaderAndWriter$$inlined$synchronized$lambda$1(str3, str3, nanos, this, str, streams2, webSocketExtensions);
                    taskQueue2.schedule(realWebSocket$initReaderAndWriter$$inlined$synchronized$lambda$1, nanos);
                }
                if (!this.messageAndCloseQueue.isEmpty()) {
                    runWriter();
                }
                Unit unit = Unit.f6917a;
            }
            this.reader = new WebSocketReader(streams2.getClient(), streams2.getSource(), this, webSocketExtensions.perMessageDeflate, webSocketExtensions.noContextTakeover(!streams2.getClient()));
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public final void loopReader() throws IOException {
        while (this.receivedCloseCode == -1) {
            WebSocketReader webSocketReader = this.reader;
            if (webSocketReader != null) {
                webSocketReader.processNextFrame();
            } else {
                Intrinsics.a();
                throw null;
            }
        }
    }

    /* JADX WARNING: type inference failed for: r1v1, types: [okhttp3.internal.ws.WebSocketWriter, okhttp3.internal.ws.WebSocketReader, okhttp3.internal.ws.RealWebSocket$Streams] */
    /* JADX WARNING: type inference failed for: r1v2, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r1v4 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReadClose(int r5, java.lang.String r6) {
        /*
            r4 = this;
            java.lang.String r0 = "reason"
            kotlin.jvm.internal.Intrinsics.b(r6, r0)
            r0 = 1
            r1 = 0
            r2 = -1
            if (r5 == r2) goto L_0x000c
            r3 = 1
            goto L_0x000d
        L_0x000c:
            r3 = 0
        L_0x000d:
            if (r3 == 0) goto L_0x007d
            monitor-enter(r4)
            int r3 = r4.receivedCloseCode     // Catch:{ all -> 0x007a }
            if (r3 != r2) goto L_0x0015
            goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            if (r0 == 0) goto L_0x006e
            r4.receivedCloseCode = r5     // Catch:{ all -> 0x007a }
            r4.receivedCloseReason = r6     // Catch:{ all -> 0x007a }
            boolean r0 = r4.enqueuedClose     // Catch:{ all -> 0x007a }
            r1 = 0
            if (r0 == 0) goto L_0x003c
            java.util.ArrayDeque<java.lang.Object> r0 = r4.messageAndCloseQueue     // Catch:{ all -> 0x007a }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x007a }
            if (r0 == 0) goto L_0x003c
            okhttp3.internal.ws.RealWebSocket$Streams r0 = r4.streams     // Catch:{ all -> 0x007a }
            r4.streams = r1     // Catch:{ all -> 0x007a }
            okhttp3.internal.ws.WebSocketReader r2 = r4.reader     // Catch:{ all -> 0x007a }
            r4.reader = r1     // Catch:{ all -> 0x007a }
            okhttp3.internal.ws.WebSocketWriter r3 = r4.writer     // Catch:{ all -> 0x007a }
            r4.writer = r1     // Catch:{ all -> 0x007a }
            okhttp3.internal.concurrent.TaskQueue r1 = r4.taskQueue     // Catch:{ all -> 0x007a }
            r1.shutdown()     // Catch:{ all -> 0x007a }
            r1 = r2
            goto L_0x003e
        L_0x003c:
            r0 = r1
            r3 = r0
        L_0x003e:
            kotlin.Unit r2 = kotlin.Unit.f6917a     // Catch:{ all -> 0x007a }
            monitor-exit(r4)
            okhttp3.WebSocketListener r2 = r4.listener     // Catch:{ all -> 0x005d }
            r2.onClosing(r4, r5, r6)     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x004d
            okhttp3.WebSocketListener r2 = r4.listener     // Catch:{ all -> 0x005d }
            r2.onClosed(r4, r5, r6)     // Catch:{ all -> 0x005d }
        L_0x004d:
            if (r0 == 0) goto L_0x0052
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r0)
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1)
        L_0x0057:
            if (r3 == 0) goto L_0x005c
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r3)
        L_0x005c:
            return
        L_0x005d:
            r5 = move-exception
            if (r0 == 0) goto L_0x0063
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r0)
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1)
        L_0x0068:
            if (r3 == 0) goto L_0x006d
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r3)
        L_0x006d:
            throw r5
        L_0x006e:
            java.lang.String r5 = "already closed"
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException     // Catch:{ all -> 0x007a }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x007a }
            r6.<init>(r5)     // Catch:{ all -> 0x007a }
            throw r6     // Catch:{ all -> 0x007a }
        L_0x007a:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x007d:
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r6 = "Failed requirement."
            java.lang.String r6 = r6.toString()
            r5.<init>(r6)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.ws.RealWebSocket.onReadClose(int, java.lang.String):void");
    }

    public void onReadMessage(String str) throws IOException {
        Intrinsics.b(str, "text");
        this.listener.onMessage((WebSocket) this, str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onReadPing(okio.ByteString r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            java.lang.String r0 = "payload"
            kotlin.jvm.internal.Intrinsics.b(r2, r0)     // Catch:{ all -> 0x0029 }
            boolean r0 = r1.failed     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x0027
            boolean r0 = r1.enqueuedClose     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0017
            java.util.ArrayDeque<java.lang.Object> r0 = r1.messageAndCloseQueue     // Catch:{ all -> 0x0029 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0017
            goto L_0x0027
        L_0x0017:
            java.util.ArrayDeque<okio.ByteString> r0 = r1.pongQueue     // Catch:{ all -> 0x0029 }
            r0.add(r2)     // Catch:{ all -> 0x0029 }
            r1.runWriter()     // Catch:{ all -> 0x0029 }
            int r2 = r1.receivedPingCount     // Catch:{ all -> 0x0029 }
            int r2 = r2 + 1
            r1.receivedPingCount = r2     // Catch:{ all -> 0x0029 }
            monitor-exit(r1)
            return
        L_0x0027:
            monitor-exit(r1)
            return
        L_0x0029:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.ws.RealWebSocket.onReadPing(okio.ByteString):void");
    }

    public synchronized void onReadPong(ByteString byteString) {
        Intrinsics.b(byteString, "payload");
        this.receivedPongCount++;
        this.awaitingPong = false;
    }

    public final synchronized boolean pong(ByteString byteString) {
        Intrinsics.b(byteString, "payload");
        if (!this.failed) {
            if (!this.enqueuedClose || !this.messageAndCloseQueue.isEmpty()) {
                this.pongQueue.add(byteString);
                runWriter();
                return true;
            }
        }
        return false;
    }

    public final boolean processNextFrame() throws IOException {
        try {
            WebSocketReader webSocketReader = this.reader;
            if (webSocketReader != null) {
                webSocketReader.processNextFrame();
                if (this.receivedCloseCode == -1) {
                    return true;
                }
                return false;
            }
            Intrinsics.a();
            throw null;
        } catch (Exception e) {
            failWebSocket(e, (Response) null);
            return false;
        }
    }

    public synchronized long queueSize() {
        return this.queueSize;
    }

    public final synchronized int receivedPingCount() {
        return this.receivedPingCount;
    }

    public final synchronized int receivedPongCount() {
        return this.receivedPongCount;
    }

    public Request request() {
        return this.originalRequest;
    }

    public boolean send(String str) {
        Intrinsics.b(str, "text");
        return send(ByteString.d.c(str), 1);
    }

    public final synchronized int sentPingCount() {
        return this.sentPingCount;
    }

    public final void tearDown() throws InterruptedException {
        this.taskQueue.shutdown();
        this.taskQueue.idleLatch().await(10, TimeUnit.SECONDS);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01d8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01d9, code lost:
        r3 = r22;
        r2 = r23;
        r1 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01df, code lost:
        r1 = (okhttp3.internal.ws.RealWebSocket.Streams) r1.element;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01e3, code lost:
        if (r1 != null) goto L_0x01e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01e5, code lost:
        okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01e8, code lost:
        r1 = (okhttp3.internal.ws.WebSocketReader) r2.element;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01ec, code lost:
        if (r1 != null) goto L_0x01ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01ee, code lost:
        okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x01f1, code lost:
        r1 = (okhttp3.internal.ws.WebSocketWriter) r3.element;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x01f5, code lost:
        if (r1 != null) goto L_0x01f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01f7, code lost:
        okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x01fa, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0104, code lost:
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0106, code lost:
        if (r2 == null) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0108, code lost:
        r1 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x010a, code lost:
        if (r1 == null) goto L_0x0113;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r1.writePong(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x010f, code lost:
        r1 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0113, code lost:
        kotlin.jvm.internal.Intrinsics.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0116, code lost:
        throw r27;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0117, code lost:
        r1 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x011d, code lost:
        if ((r0.element instanceof okhttp3.internal.ws.RealWebSocket.Message) == false) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x011f, code lost:
        r0 = r0.element;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0121, code lost:
        if (r0 == null) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0123, code lost:
        r0 = (okhttp3.internal.ws.RealWebSocket.Message) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0125, code lost:
        if (r1 == null) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0127, code lost:
        r1.writeMessageFrame(r0.getFormatOpcode(), r0.getData());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0132, code lost:
        monitor-enter(r28);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        r15.queueSize -= (long) r0.getData().size();
        r0 = kotlin.Unit.f6917a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        monitor-exit(r28);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0148, code lost:
        kotlin.jvm.internal.Intrinsics.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x014b, code lost:
        throw r27;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0153, code lost:
        throw new kotlin.TypeCastException("null cannot be cast to non-null type okhttp3.internal.ws.RealWebSocket.Message");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0158, code lost:
        if ((r0.element instanceof okhttp3.internal.ws.RealWebSocket.Close) == false) goto L_0x01ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x015a, code lost:
        r0 = r0.element;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x015c, code lost:
        if (r0 == null) goto L_0x01bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x015e, code lost:
        r0 = (okhttp3.internal.ws.RealWebSocket.Close) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0160, code lost:
        if (r1 == null) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0162, code lost:
        r1.writeClose(r0.getCode(), r0.getReason());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x016d, code lost:
        r1 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0173, code lost:
        if (((okhttp3.internal.ws.RealWebSocket.Streams) r1.element) == null) goto L_0x018b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0175, code lost:
        r0 = r15.listener;
        r2 = r26.element;
        r3 = (java.lang.String) r25.element;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0181, code lost:
        if (r3 == null) goto L_0x0187;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0183, code lost:
        r0.onClosed(r15, r2, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0187, code lost:
        kotlin.jvm.internal.Intrinsics.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x018a, code lost:
        throw r27;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x018b, code lost:
        r1 = (okhttp3.internal.ws.RealWebSocket.Streams) r1.element;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0190, code lost:
        if (r1 == null) goto L_0x0195;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0192, code lost:
        okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0195, code lost:
        r1 = (okhttp3.internal.ws.WebSocketReader) r23.element;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x019b, code lost:
        if (r1 == null) goto L_0x01a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x019d, code lost:
        okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01a0, code lost:
        r1 = (okhttp3.internal.ws.WebSocketWriter) r22.element;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01a6, code lost:
        if (r1 == null) goto L_0x01ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01a8, code lost:
        okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01ab, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01ac, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01ad, code lost:
        r3 = r22;
        r2 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01b2, code lost:
        r3 = r22;
        r2 = r23;
        r1 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        kotlin.jvm.internal.Intrinsics.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01bb, code lost:
        throw r27;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01bc, code lost:
        r3 = r22;
        r2 = r23;
        r1 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01c9, code lost:
        throw new kotlin.TypeCastException("null cannot be cast to non-null type okhttp3.internal.ws.RealWebSocket.Close");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01ca, code lost:
        r3 = r22;
        r2 = r23;
        r1 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01d5, code lost:
        throw new java.lang.AssertionError();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01d6, code lost:
        r0 = th;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01f7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean writeOneFrame$okhttp() throws java.io.IOException {
        /*
            r28 = this;
            r15 = r28
            kotlin.jvm.internal.Ref$ObjectRef r0 = new kotlin.jvm.internal.Ref$ObjectRef
            r0.<init>()
            r14 = 0
            r0.element = r14
            kotlin.jvm.internal.Ref$IntRef r13 = new kotlin.jvm.internal.Ref$IntRef
            r13.<init>()
            r1 = -1
            r13.element = r1
            kotlin.jvm.internal.Ref$ObjectRef r12 = new kotlin.jvm.internal.Ref$ObjectRef
            r12.<init>()
            r12.element = r14
            kotlin.jvm.internal.Ref$ObjectRef r11 = new kotlin.jvm.internal.Ref$ObjectRef
            r11.<init>()
            r11.element = r14
            kotlin.jvm.internal.Ref$ObjectRef r10 = new kotlin.jvm.internal.Ref$ObjectRef
            r10.<init>()
            r10.element = r14
            kotlin.jvm.internal.Ref$ObjectRef r9 = new kotlin.jvm.internal.Ref$ObjectRef
            r9.<init>()
            r9.element = r14
            monitor-enter(r28)
            boolean r2 = r15.failed     // Catch:{ all -> 0x01fb }
            r3 = 0
            if (r2 == 0) goto L_0x0036
            monitor-exit(r28)
            return r3
        L_0x0036:
            okhttp3.internal.ws.WebSocketWriter r8 = r15.writer     // Catch:{ all -> 0x01fb }
            java.util.ArrayDeque<okio.ByteString> r2 = r15.pongQueue     // Catch:{ all -> 0x01fb }
            java.lang.Object r2 = r2.poll()     // Catch:{ all -> 0x01fb }
            r7 = r2
            okio.ByteString r7 = (okio.ByteString) r7     // Catch:{ all -> 0x01fb }
            if (r7 != 0) goto L_0x00f1
            java.util.ArrayDeque<java.lang.Object> r2 = r15.messageAndCloseQueue     // Catch:{ all -> 0x01fb }
            java.lang.Object r2 = r2.poll()     // Catch:{ all -> 0x01fb }
            r0.element = r2     // Catch:{ all -> 0x01fb }
            T r2 = r0.element     // Catch:{ all -> 0x01fb }
            boolean r2 = r2 instanceof okhttp3.internal.ws.RealWebSocket.Close     // Catch:{ all -> 0x01fb }
            if (r2 == 0) goto L_0x00db
            int r2 = r15.receivedCloseCode     // Catch:{ all -> 0x01fb }
            r13.element = r2     // Catch:{ all -> 0x01fb }
            java.lang.String r2 = r15.receivedCloseReason     // Catch:{ all -> 0x01fb }
            r12.element = r2     // Catch:{ all -> 0x01fb }
            int r2 = r13.element     // Catch:{ all -> 0x01fb }
            if (r2 == r1) goto L_0x0076
            okhttp3.internal.ws.RealWebSocket$Streams r1 = r15.streams     // Catch:{ all -> 0x01fb }
            r11.element = r1     // Catch:{ all -> 0x01fb }
            r15.streams = r14     // Catch:{ all -> 0x01fb }
            okhttp3.internal.ws.WebSocketReader r1 = r15.reader     // Catch:{ all -> 0x01fb }
            r10.element = r1     // Catch:{ all -> 0x01fb }
            r15.reader = r14     // Catch:{ all -> 0x01fb }
            okhttp3.internal.ws.WebSocketWriter r1 = r15.writer     // Catch:{ all -> 0x01fb }
            r9.element = r1     // Catch:{ all -> 0x01fb }
            r15.writer = r14     // Catch:{ all -> 0x01fb }
            okhttp3.internal.concurrent.TaskQueue r1 = r15.taskQueue     // Catch:{ all -> 0x01fb }
            r1.shutdown()     // Catch:{ all -> 0x01fb }
            goto L_0x00f1
        L_0x0076:
            T r1 = r0.element     // Catch:{ all -> 0x01fb }
            if (r1 == 0) goto L_0x00d3
            okhttp3.internal.ws.RealWebSocket$Close r1 = (okhttp3.internal.ws.RealWebSocket.Close) r1     // Catch:{ all -> 0x01fb }
            long r1 = r1.getCancelAfterCloseMillis()     // Catch:{ all -> 0x01fb }
            okhttp3.internal.concurrent.TaskQueue r6 = r15.taskQueue     // Catch:{ all -> 0x01fb }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x01fb }
            r3.<init>()     // Catch:{ all -> 0x01fb }
            java.lang.String r4 = r15.name     // Catch:{ all -> 0x01fb }
            r3.append(r4)     // Catch:{ all -> 0x01fb }
            java.lang.String r4 = " cancel"
            r3.append(r4)     // Catch:{ all -> 0x01fb }
            java.lang.String r4 = r3.toString()     // Catch:{ all -> 0x01fb }
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x01fb }
            long r2 = r3.toNanos(r1)     // Catch:{ all -> 0x01fb }
            r5 = 1
            okhttp3.internal.ws.RealWebSocket$writeOneFrame$$inlined$synchronized$lambda$1 r1 = new okhttp3.internal.ws.RealWebSocket$writeOneFrame$$inlined$synchronized$lambda$1     // Catch:{ all -> 0x01fb }
            r16 = r1
            r1 = r16
            r17 = r2
            r2 = r4
            r3 = r5
            r19 = r6
            r6 = r28
            r20 = r7
            r7 = r8
            r21 = r8
            r8 = r20
            r22 = r9
            r9 = r0
            r23 = r10
            r10 = r13
            r24 = r11
            r11 = r12
            r25 = r12
            r12 = r24
            r26 = r13
            r13 = r23
            r27 = r14
            r14 = r22
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)     // Catch:{ all -> 0x01fb }
            r4 = r16
            r2 = r17
            r1 = r19
            r1.schedule(r4, r2)     // Catch:{ all -> 0x01fb }
            goto L_0x0101
        L_0x00d3:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException     // Catch:{ all -> 0x01fb }
            java.lang.String r1 = "null cannot be cast to non-null type okhttp3.internal.ws.RealWebSocket.Close"
            r0.<init>(r1)     // Catch:{ all -> 0x01fb }
            throw r0     // Catch:{ all -> 0x01fb }
        L_0x00db:
            r20 = r7
            r21 = r8
            r22 = r9
            r23 = r10
            r24 = r11
            r25 = r12
            r26 = r13
            r27 = r14
            T r1 = r0.element     // Catch:{ all -> 0x01fb }
            if (r1 != 0) goto L_0x0101
            monitor-exit(r28)
            return r3
        L_0x00f1:
            r20 = r7
            r21 = r8
            r22 = r9
            r23 = r10
            r24 = r11
            r25 = r12
            r26 = r13
            r27 = r14
        L_0x0101:
            kotlin.Unit r1 = kotlin.Unit.f6917a     // Catch:{ all -> 0x01fb }
            monitor-exit(r28)
            r2 = r20
            if (r2 == 0) goto L_0x0117
            r1 = r21
            if (r1 == 0) goto L_0x0113
            r1.writePong(r2)     // Catch:{ all -> 0x01d8 }
        L_0x010f:
            r1 = r24
            goto L_0x018b
        L_0x0113:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01d8 }
            throw r27
        L_0x0117:
            r1 = r21
            T r2 = r0.element     // Catch:{ all -> 0x01d8 }
            boolean r2 = r2 instanceof okhttp3.internal.ws.RealWebSocket.Message     // Catch:{ all -> 0x01d8 }
            if (r2 == 0) goto L_0x0154
            T r0 = r0.element     // Catch:{ all -> 0x01d8 }
            if (r0 == 0) goto L_0x014c
            okhttp3.internal.ws.RealWebSocket$Message r0 = (okhttp3.internal.ws.RealWebSocket.Message) r0     // Catch:{ all -> 0x01d8 }
            if (r1 == 0) goto L_0x0148
            int r2 = r0.getFormatOpcode()     // Catch:{ all -> 0x01d8 }
            okio.ByteString r3 = r0.getData()     // Catch:{ all -> 0x01d8 }
            r1.writeMessageFrame(r2, r3)     // Catch:{ all -> 0x01d8 }
            monitor-enter(r28)     // Catch:{ all -> 0x01d8 }
            long r1 = r15.queueSize     // Catch:{ all -> 0x0145 }
            okio.ByteString r0 = r0.getData()     // Catch:{ all -> 0x0145 }
            int r0 = r0.size()     // Catch:{ all -> 0x0145 }
            long r3 = (long) r0     // Catch:{ all -> 0x0145 }
            long r1 = r1 - r3
            r15.queueSize = r1     // Catch:{ all -> 0x0145 }
            kotlin.Unit r0 = kotlin.Unit.f6917a     // Catch:{ all -> 0x0145 }
            monitor-exit(r28)     // Catch:{ all -> 0x01d8 }
            goto L_0x010f
        L_0x0145:
            r0 = move-exception
            monitor-exit(r28)     // Catch:{ all -> 0x01d8 }
            throw r0     // Catch:{ all -> 0x01d8 }
        L_0x0148:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01d8 }
            throw r27
        L_0x014c:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException     // Catch:{ all -> 0x01d8 }
            java.lang.String r1 = "null cannot be cast to non-null type okhttp3.internal.ws.RealWebSocket.Message"
            r0.<init>(r1)     // Catch:{ all -> 0x01d8 }
            throw r0     // Catch:{ all -> 0x01d8 }
        L_0x0154:
            T r2 = r0.element     // Catch:{ all -> 0x01d8 }
            boolean r2 = r2 instanceof okhttp3.internal.ws.RealWebSocket.Close     // Catch:{ all -> 0x01d8 }
            if (r2 == 0) goto L_0x01ca
            T r0 = r0.element     // Catch:{ all -> 0x01d8 }
            if (r0 == 0) goto L_0x01bc
            okhttp3.internal.ws.RealWebSocket$Close r0 = (okhttp3.internal.ws.RealWebSocket.Close) r0     // Catch:{ all -> 0x01d8 }
            if (r1 == 0) goto L_0x01b2
            int r2 = r0.getCode()     // Catch:{ all -> 0x01d8 }
            okio.ByteString r0 = r0.getReason()     // Catch:{ all -> 0x01d8 }
            r1.writeClose(r2, r0)     // Catch:{ all -> 0x01d8 }
            r1 = r24
            T r0 = r1.element     // Catch:{ all -> 0x01ac }
            okhttp3.internal.ws.RealWebSocket$Streams r0 = (okhttp3.internal.ws.RealWebSocket.Streams) r0     // Catch:{ all -> 0x01ac }
            if (r0 == 0) goto L_0x018b
            okhttp3.WebSocketListener r0 = r15.listener     // Catch:{ all -> 0x01ac }
            r2 = r26
            int r2 = r2.element     // Catch:{ all -> 0x01ac }
            r3 = r25
            T r3 = r3.element     // Catch:{ all -> 0x01ac }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x01ac }
            if (r3 == 0) goto L_0x0187
            r0.onClosed(r15, r2, r3)     // Catch:{ all -> 0x01ac }
            goto L_0x018b
        L_0x0187:
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01ac }
            throw r27
        L_0x018b:
            r0 = 1
            T r1 = r1.element
            okhttp3.internal.ws.RealWebSocket$Streams r1 = (okhttp3.internal.ws.RealWebSocket.Streams) r1
            if (r1 == 0) goto L_0x0195
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1)
        L_0x0195:
            r2 = r23
            T r1 = r2.element
            okhttp3.internal.ws.WebSocketReader r1 = (okhttp3.internal.ws.WebSocketReader) r1
            if (r1 == 0) goto L_0x01a0
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1)
        L_0x01a0:
            r3 = r22
            T r1 = r3.element
            okhttp3.internal.ws.WebSocketWriter r1 = (okhttp3.internal.ws.WebSocketWriter) r1
            if (r1 == 0) goto L_0x01ab
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1)
        L_0x01ab:
            return r0
        L_0x01ac:
            r0 = move-exception
            r3 = r22
            r2 = r23
            goto L_0x01df
        L_0x01b2:
            r3 = r22
            r2 = r23
            r1 = r24
            kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x01d6 }
            throw r27
        L_0x01bc:
            r3 = r22
            r2 = r23
            r1 = r24
            kotlin.TypeCastException r0 = new kotlin.TypeCastException     // Catch:{ all -> 0x01d6 }
            java.lang.String r4 = "null cannot be cast to non-null type okhttp3.internal.ws.RealWebSocket.Close"
            r0.<init>(r4)     // Catch:{ all -> 0x01d6 }
            throw r0     // Catch:{ all -> 0x01d6 }
        L_0x01ca:
            r3 = r22
            r2 = r23
            r1 = r24
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x01d6 }
            r0.<init>()     // Catch:{ all -> 0x01d6 }
            throw r0     // Catch:{ all -> 0x01d6 }
        L_0x01d6:
            r0 = move-exception
            goto L_0x01df
        L_0x01d8:
            r0 = move-exception
            r3 = r22
            r2 = r23
            r1 = r24
        L_0x01df:
            T r1 = r1.element
            okhttp3.internal.ws.RealWebSocket$Streams r1 = (okhttp3.internal.ws.RealWebSocket.Streams) r1
            if (r1 == 0) goto L_0x01e8
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1)
        L_0x01e8:
            T r1 = r2.element
            okhttp3.internal.ws.WebSocketReader r1 = (okhttp3.internal.ws.WebSocketReader) r1
            if (r1 == 0) goto L_0x01f1
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1)
        L_0x01f1:
            T r1 = r3.element
            okhttp3.internal.ws.WebSocketWriter r1 = (okhttp3.internal.ws.WebSocketWriter) r1
            if (r1 == 0) goto L_0x01fa
            okhttp3.internal.Util.closeQuietly((java.io.Closeable) r1)
        L_0x01fa:
            throw r0
        L_0x01fb:
            r0 = move-exception
            monitor-exit(r28)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.ws.RealWebSocket.writeOneFrame$okhttp():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0020, code lost:
        if (r1 == -1) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0022, code lost:
        failWebSocket(new java.net.SocketTimeoutException("sent ping but didn't receive pong within " + r7.pingIntervalMillis + "ms (after " + (r1 - 1) + " successful ping/pongs)"), (okhttp3.Response) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r0.writePing(okio.ByteString.c);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0052, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0053, code lost:
        failWebSocket(r0, (okhttp3.Response) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void writePingFrame$okhttp() {
        /*
            r7 = this;
            monitor-enter(r7)
            boolean r0 = r7.failed     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r7)
            return
        L_0x0007:
            okhttp3.internal.ws.WebSocketWriter r0 = r7.writer     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0057
            boolean r1 = r7.awaitingPong     // Catch:{ all -> 0x0059 }
            r2 = -1
            if (r1 == 0) goto L_0x0013
            int r1 = r7.sentPingCount     // Catch:{ all -> 0x0059 }
            goto L_0x0014
        L_0x0013:
            r1 = -1
        L_0x0014:
            int r3 = r7.sentPingCount     // Catch:{ all -> 0x0059 }
            r4 = 1
            int r3 = r3 + r4
            r7.sentPingCount = r3     // Catch:{ all -> 0x0059 }
            r7.awaitingPong = r4     // Catch:{ all -> 0x0059 }
            kotlin.Unit r3 = kotlin.Unit.f6917a     // Catch:{ all -> 0x0059 }
            monitor-exit(r7)
            r3 = 0
            if (r1 == r2) goto L_0x004c
            java.net.SocketTimeoutException r0 = new java.net.SocketTimeoutException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "sent ping but didn't receive pong within "
            r2.append(r5)
            long r5 = r7.pingIntervalMillis
            r2.append(r5)
            java.lang.String r5 = "ms (after "
            r2.append(r5)
            int r1 = r1 - r4
            r2.append(r1)
            java.lang.String r1 = " successful ping/pongs)"
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            r0.<init>(r1)
            r7.failWebSocket(r0, r3)
            return
        L_0x004c:
            okio.ByteString r1 = okio.ByteString.c     // Catch:{ IOException -> 0x0052 }
            r0.writePing(r1)     // Catch:{ IOException -> 0x0052 }
            goto L_0x0056
        L_0x0052:
            r0 = move-exception
            r7.failWebSocket(r0, r3)
        L_0x0056:
            return
        L_0x0057:
            monitor-exit(r7)
            return
        L_0x0059:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.ws.RealWebSocket.writePingFrame$okhttp():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0058, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean close(int r9, java.lang.String r10, long r11) {
        /*
            r8 = this;
            monitor-enter(r8)
            okhttp3.internal.ws.WebSocketProtocol r0 = okhttp3.internal.ws.WebSocketProtocol.INSTANCE     // Catch:{ all -> 0x0059 }
            r0.validateCloseCode(r9)     // Catch:{ all -> 0x0059 }
            r0 = 0
            r1 = 0
            r2 = 1
            if (r10 == 0) goto L_0x003d
            okio.ByteString$Companion r0 = okio.ByteString.d     // Catch:{ all -> 0x0059 }
            okio.ByteString r0 = r0.c(r10)     // Catch:{ all -> 0x0059 }
            int r3 = r0.size()     // Catch:{ all -> 0x0059 }
            long r3 = (long) r3     // Catch:{ all -> 0x0059 }
            r5 = 123(0x7b, double:6.1E-322)
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 > 0) goto L_0x001e
            r3 = 1
            goto L_0x001f
        L_0x001e:
            r3 = 0
        L_0x001f:
            if (r3 == 0) goto L_0x0022
            goto L_0x003d
        L_0x0022:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0059 }
            r9.<init>()     // Catch:{ all -> 0x0059 }
            java.lang.String r11 = "reason.size() > 123: "
            r9.append(r11)     // Catch:{ all -> 0x0059 }
            r9.append(r10)     // Catch:{ all -> 0x0059 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0059 }
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0059 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0059 }
            r10.<init>(r9)     // Catch:{ all -> 0x0059 }
            throw r10     // Catch:{ all -> 0x0059 }
        L_0x003d:
            boolean r10 = r8.failed     // Catch:{ all -> 0x0059 }
            if (r10 != 0) goto L_0x0057
            boolean r10 = r8.enqueuedClose     // Catch:{ all -> 0x0059 }
            if (r10 == 0) goto L_0x0046
            goto L_0x0057
        L_0x0046:
            r8.enqueuedClose = r2     // Catch:{ all -> 0x0059 }
            java.util.ArrayDeque<java.lang.Object> r10 = r8.messageAndCloseQueue     // Catch:{ all -> 0x0059 }
            okhttp3.internal.ws.RealWebSocket$Close r1 = new okhttp3.internal.ws.RealWebSocket$Close     // Catch:{ all -> 0x0059 }
            r1.<init>(r9, r0, r11)     // Catch:{ all -> 0x0059 }
            r10.add(r1)     // Catch:{ all -> 0x0059 }
            r8.runWriter()     // Catch:{ all -> 0x0059 }
            monitor-exit(r8)
            return r2
        L_0x0057:
            monitor-exit(r8)
            return r1
        L_0x0059:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.ws.RealWebSocket.close(int, java.lang.String, long):boolean");
    }

    public void onReadMessage(ByteString byteString) throws IOException {
        Intrinsics.b(byteString, "bytes");
        this.listener.onMessage((WebSocket) this, byteString);
    }

    public boolean send(ByteString byteString) {
        Intrinsics.b(byteString, "bytes");
        return send(byteString, 2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003d, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final synchronized boolean send(okio.ByteString r7, int r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            boolean r0 = r6.failed     // Catch:{ all -> 0x003e }
            r1 = 0
            if (r0 != 0) goto L_0x003c
            boolean r0 = r6.enqueuedClose     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x000b
            goto L_0x003c
        L_0x000b:
            long r2 = r6.queueSize     // Catch:{ all -> 0x003e }
            int r0 = r7.size()     // Catch:{ all -> 0x003e }
            long r4 = (long) r0     // Catch:{ all -> 0x003e }
            long r2 = r2 + r4
            r4 = 16777216(0x1000000, double:8.289046E-317)
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0022
            r7 = 1001(0x3e9, float:1.403E-42)
            r8 = 0
            r6.close(r7, r8)     // Catch:{ all -> 0x003e }
            monitor-exit(r6)
            return r1
        L_0x0022:
            long r0 = r6.queueSize     // Catch:{ all -> 0x003e }
            int r2 = r7.size()     // Catch:{ all -> 0x003e }
            long r2 = (long) r2     // Catch:{ all -> 0x003e }
            long r0 = r0 + r2
            r6.queueSize = r0     // Catch:{ all -> 0x003e }
            java.util.ArrayDeque<java.lang.Object> r0 = r6.messageAndCloseQueue     // Catch:{ all -> 0x003e }
            okhttp3.internal.ws.RealWebSocket$Message r1 = new okhttp3.internal.ws.RealWebSocket$Message     // Catch:{ all -> 0x003e }
            r1.<init>(r8, r7)     // Catch:{ all -> 0x003e }
            r0.add(r1)     // Catch:{ all -> 0x003e }
            r6.runWriter()     // Catch:{ all -> 0x003e }
            r7 = 1
            monitor-exit(r6)
            return r7
        L_0x003c:
            monitor-exit(r6)
            return r1
        L_0x003e:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.ws.RealWebSocket.send(okio.ByteString, int):boolean");
    }
}
