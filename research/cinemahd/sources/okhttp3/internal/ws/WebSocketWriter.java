package okhttp3.internal.ws;

import com.facebook.common.util.UriUtil;
import java.io.Closeable;
import java.io.IOException;
import java.util.Random;
import kotlin.jvm.internal.Intrinsics;
import okio.Buffer;
import okio.BufferedSink;
import okio.ByteString;

public final class WebSocketWriter implements Closeable {
    private final boolean isClient;
    private final Buffer.UnsafeCursor maskCursor;
    private final byte[] maskKey;
    private final Buffer messageBuffer = new Buffer();
    private MessageDeflater messageDeflater;
    private final long minimumDeflateSize;
    private final boolean noContextTakeover;
    private final boolean perMessageDeflate;
    private final Random random;
    private final BufferedSink sink;
    private final Buffer sinkBuffer = this.sink.a();
    private boolean writerClosed;

    public WebSocketWriter(boolean z, BufferedSink bufferedSink, Random random2, boolean z2, boolean z3, long j) {
        Intrinsics.b(bufferedSink, "sink");
        Intrinsics.b(random2, "random");
        this.isClient = z;
        this.sink = bufferedSink;
        this.random = random2;
        this.perMessageDeflate = z2;
        this.noContextTakeover = z3;
        this.minimumDeflateSize = j;
        Buffer.UnsafeCursor unsafeCursor = null;
        this.maskKey = this.isClient ? new byte[4] : null;
        this.maskCursor = this.isClient ? new Buffer.UnsafeCursor() : unsafeCursor;
    }

    private final void writeControlFrame(int i, ByteString byteString) throws IOException {
        if (!this.writerClosed) {
            int size = byteString.size();
            if (((long) size) <= 125) {
                this.sinkBuffer.writeByte(i | 128);
                if (this.isClient) {
                    this.sinkBuffer.writeByte(size | 128);
                    Random random2 = this.random;
                    byte[] bArr = this.maskKey;
                    if (bArr != null) {
                        random2.nextBytes(bArr);
                        this.sinkBuffer.write(this.maskKey);
                        if (size > 0) {
                            long u = this.sinkBuffer.u();
                            this.sinkBuffer.a(byteString);
                            Buffer buffer = this.sinkBuffer;
                            Buffer.UnsafeCursor unsafeCursor = this.maskCursor;
                            if (unsafeCursor != null) {
                                buffer.a(unsafeCursor);
                                this.maskCursor.i(u);
                                WebSocketProtocol.INSTANCE.toggleMask(this.maskCursor, this.maskKey);
                                this.maskCursor.close();
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                } else {
                    this.sinkBuffer.writeByte(size);
                    this.sinkBuffer.a(byteString);
                }
                this.sink.flush();
                return;
            }
            throw new IllegalArgumentException("Payload size must be less than or equal to 125".toString());
        }
        throw new IOException("closed");
    }

    public void close() {
        MessageDeflater messageDeflater2 = this.messageDeflater;
        if (messageDeflater2 != null) {
            messageDeflater2.close();
        }
    }

    public final Random getRandom() {
        return this.random;
    }

    public final BufferedSink getSink() {
        return this.sink;
    }

    public final void writeClose(int i, ByteString byteString) throws IOException {
        ByteString byteString2 = ByteString.c;
        if (!(i == 0 && byteString == null)) {
            if (i != 0) {
                WebSocketProtocol.INSTANCE.validateCloseCode(i);
            }
            Buffer buffer = new Buffer();
            buffer.writeShort(i);
            if (byteString != null) {
                buffer.a(byteString);
            }
            byteString2 = buffer.g();
        }
        try {
            writeControlFrame(8, byteString2);
        } finally {
            this.writerClosed = true;
        }
    }

    public final void writeMessageFrame(int i, ByteString byteString) throws IOException {
        Intrinsics.b(byteString, UriUtil.DATA_SCHEME);
        if (!this.writerClosed) {
            this.messageBuffer.a(byteString);
            int i2 = i | 128;
            if (this.perMessageDeflate && ((long) byteString.size()) >= this.minimumDeflateSize) {
                MessageDeflater messageDeflater2 = this.messageDeflater;
                if (messageDeflater2 == null) {
                    messageDeflater2 = new MessageDeflater(this.noContextTakeover);
                    this.messageDeflater = messageDeflater2;
                }
                messageDeflater2.deflate(this.messageBuffer);
                i2 |= 64;
            }
            long u = this.messageBuffer.u();
            this.sinkBuffer.writeByte(i2);
            int i3 = 0;
            if (this.isClient) {
                i3 = 128;
            }
            if (u <= 125) {
                this.sinkBuffer.writeByte(i3 | ((int) u));
            } else if (u <= WebSocketProtocol.PAYLOAD_SHORT_MAX) {
                this.sinkBuffer.writeByte(i3 | WebSocketProtocol.PAYLOAD_SHORT);
                this.sinkBuffer.writeShort((int) u);
            } else {
                this.sinkBuffer.writeByte(i3 | 127);
                this.sinkBuffer.k(u);
            }
            if (this.isClient) {
                Random random2 = this.random;
                byte[] bArr = this.maskKey;
                if (bArr != null) {
                    random2.nextBytes(bArr);
                    this.sinkBuffer.write(this.maskKey);
                    if (u > 0) {
                        Buffer buffer = this.messageBuffer;
                        Buffer.UnsafeCursor unsafeCursor = this.maskCursor;
                        if (unsafeCursor != null) {
                            buffer.a(unsafeCursor);
                            this.maskCursor.i(0);
                            WebSocketProtocol.INSTANCE.toggleMask(this.maskCursor, this.maskKey);
                            this.maskCursor.close();
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
            this.sinkBuffer.write(this.messageBuffer, u);
            this.sink.k();
            return;
        }
        throw new IOException("closed");
    }

    public final void writePing(ByteString byteString) throws IOException {
        Intrinsics.b(byteString, "payload");
        writeControlFrame(9, byteString);
    }

    public final void writePong(ByteString byteString) throws IOException {
        Intrinsics.b(byteString, "payload");
        writeControlFrame(10, byteString);
    }
}
