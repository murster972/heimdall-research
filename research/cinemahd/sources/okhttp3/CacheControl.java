package okhttp3;

import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.Intrinsics;

public final class CacheControl {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final CacheControl FORCE_CACHE = new Builder().onlyIfCached().maxStale(Integer.MAX_VALUE, TimeUnit.SECONDS).build();
    public static final CacheControl FORCE_NETWORK = new Builder().noCache().build();
    private String headerValue;
    private final boolean immutable;
    private final boolean isPrivate;
    private final boolean isPublic;
    private final int maxAgeSeconds;
    private final int maxStaleSeconds;
    private final int minFreshSeconds;
    private final boolean mustRevalidate;
    private final boolean noCache;
    private final boolean noStore;
    private final boolean noTransform;
    private final boolean onlyIfCached;
    private final int sMaxAgeSeconds;

    public static final class Builder {
        private boolean immutable;
        private int maxAgeSeconds = -1;
        private int maxStaleSeconds = -1;
        private int minFreshSeconds = -1;
        private boolean noCache;
        private boolean noStore;
        private boolean noTransform;
        private boolean onlyIfCached;

        private final int clampToInt(long j) {
            if (j > ((long) Integer.MAX_VALUE)) {
                return Integer.MAX_VALUE;
            }
            return (int) j;
        }

        public final CacheControl build() {
            return new CacheControl(this.noCache, this.noStore, this.maxAgeSeconds, -1, false, false, false, this.maxStaleSeconds, this.minFreshSeconds, this.onlyIfCached, this.noTransform, this.immutable, (String) null, (DefaultConstructorMarker) null);
        }

        public final Builder immutable() {
            this.immutable = true;
            return this;
        }

        public final Builder maxAge(int i, TimeUnit timeUnit) {
            Intrinsics.b(timeUnit, "timeUnit");
            if (i >= 0) {
                this.maxAgeSeconds = clampToInt(timeUnit.toSeconds((long) i));
                return this;
            }
            throw new IllegalArgumentException(("maxAge < 0: " + i).toString());
        }

        public final Builder maxStale(int i, TimeUnit timeUnit) {
            Intrinsics.b(timeUnit, "timeUnit");
            if (i >= 0) {
                this.maxStaleSeconds = clampToInt(timeUnit.toSeconds((long) i));
                return this;
            }
            throw new IllegalArgumentException(("maxStale < 0: " + i).toString());
        }

        public final Builder minFresh(int i, TimeUnit timeUnit) {
            Intrinsics.b(timeUnit, "timeUnit");
            if (i >= 0) {
                this.minFreshSeconds = clampToInt(timeUnit.toSeconds((long) i));
                return this;
            }
            throw new IllegalArgumentException(("minFresh < 0: " + i).toString());
        }

        public final Builder noCache() {
            this.noCache = true;
            return this;
        }

        public final Builder noStore() {
            this.noStore = true;
            return this;
        }

        public final Builder noTransform() {
            this.noTransform = true;
            return this;
        }

        public final Builder onlyIfCached() {
            this.onlyIfCached = true;
            return this;
        }
    }

    public static final class Companion {
        private Companion() {
        }

        private final int indexOfElement(String str, String str2, int i) {
            int length = str.length();
            while (i < length) {
                if (StringsKt__StringsKt.a((CharSequence) str2, str.charAt(i), false, 2, (Object) null)) {
                    return i;
                }
                i++;
            }
            return str.length();
        }

        static /* synthetic */ int indexOfElement$default(Companion companion, String str, String str2, int i, int i2, Object obj) {
            if ((i2 & 2) != 0) {
                i = 0;
            }
            return companion.indexOfElement(str, str2, i);
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x00f4  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x00f8  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final okhttp3.CacheControl parse(okhttp3.Headers r33) {
            /*
                r32 = this;
                r0 = r32
                r1 = r33
                java.lang.String r2 = "headers"
                kotlin.jvm.internal.Intrinsics.b(r1, r2)
                int r2 = r33.size()
                r6 = 1
                r7 = 0
                r8 = 1
                r9 = 0
                r10 = 0
                r11 = 0
                r12 = -1
                r13 = -1
                r14 = 0
                r15 = 0
                r16 = 0
                r17 = -1
                r18 = -1
                r19 = 0
                r20 = 0
                r21 = 0
            L_0x0023:
                if (r7 >= r2) goto L_0x01b2
                java.lang.String r3 = r1.name(r7)
                java.lang.String r5 = r1.value(r7)
                java.lang.String r4 = "Cache-Control"
                boolean r4 = kotlin.text.StringsKt__StringsJVMKt.b(r3, r4, r6)
                if (r4 == 0) goto L_0x003a
                if (r9 == 0) goto L_0x0038
                goto L_0x0042
            L_0x0038:
                r9 = r5
                goto L_0x0043
            L_0x003a:
                java.lang.String r4 = "Pragma"
                boolean r3 = kotlin.text.StringsKt__StringsJVMKt.b(r3, r4, r6)
                if (r3 == 0) goto L_0x01a7
            L_0x0042:
                r8 = 0
            L_0x0043:
                r3 = 0
            L_0x0044:
                int r4 = r5.length()
                if (r3 >= r4) goto L_0x01a0
                java.lang.String r4 = "=,;"
                int r4 = r0.indexOfElement(r5, r4, r3)
                java.lang.String r6 = "null cannot be cast to non-null type java.lang.String"
                if (r5 == 0) goto L_0x019a
                java.lang.String r3 = r5.substring(r3, r4)
                java.lang.String r1 = "(this as java.lang.Strin…ing(startIndex, endIndex)"
                kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r3, (java.lang.String) r1)
                r29 = r2
                java.lang.String r2 = "null cannot be cast to non-null type kotlin.CharSequence"
                if (r3 == 0) goto L_0x0194
                java.lang.CharSequence r3 = kotlin.text.StringsKt__StringsKt.f(r3)
                java.lang.String r3 = r3.toString()
                r30 = r8
                int r8 = r5.length()
                if (r4 == r8) goto L_0x00e6
                char r8 = r5.charAt(r4)
                r31 = r9
                r9 = 44
                if (r8 == r9) goto L_0x00e8
                char r8 = r5.charAt(r4)
                r9 = 59
                if (r8 != r9) goto L_0x0086
                goto L_0x00e8
            L_0x0086:
                int r4 = r4 + 1
                int r4 = okhttp3.internal.Util.indexOfNonWhitespace(r5, r4)
                int r8 = r5.length()
                if (r4 >= r8) goto L_0x00bf
                char r8 = r5.charAt(r4)
                r9 = 34
                if (r8 != r9) goto L_0x00bf
                int r2 = r4 + 1
                r24 = 34
                r26 = 0
                r27 = 4
                r28 = 0
                r23 = r5
                r25 = r2
                int r4 = kotlin.text.StringsKt__StringsKt.a((java.lang.CharSequence) r23, (char) r24, (int) r25, (boolean) r26, (int) r27, (java.lang.Object) r28)
                if (r5 == 0) goto L_0x00b9
                java.lang.String r2 = r5.substring(r2, r4)
                kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r2, (java.lang.String) r1)
                r1 = 1
                int r4 = r4 + r1
                r1 = r2
                goto L_0x00eb
            L_0x00b9:
                kotlin.TypeCastException r1 = new kotlin.TypeCastException
                r1.<init>(r6)
                throw r1
            L_0x00bf:
                java.lang.String r8 = ",;"
                int r8 = r0.indexOfElement(r5, r8, r4)
                if (r5 == 0) goto L_0x00e0
                java.lang.String r4 = r5.substring(r4, r8)
                kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r4, (java.lang.String) r1)
                if (r4 == 0) goto L_0x00da
                java.lang.CharSequence r1 = kotlin.text.StringsKt__StringsKt.f(r4)
                java.lang.String r1 = r1.toString()
                r4 = r8
                goto L_0x00eb
            L_0x00da:
                kotlin.TypeCastException r1 = new kotlin.TypeCastException
                r1.<init>(r2)
                throw r1
            L_0x00e0:
                kotlin.TypeCastException r1 = new kotlin.TypeCastException
                r1.<init>(r6)
                throw r1
            L_0x00e6:
                r31 = r9
            L_0x00e8:
                int r4 = r4 + 1
                r1 = 0
            L_0x00eb:
                java.lang.String r2 = "no-cache"
                r6 = 1
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r2, r3, r6)
                if (r2 == 0) goto L_0x00f8
                r8 = -1
                r10 = 1
                goto L_0x0189
            L_0x00f8:
                java.lang.String r2 = "no-store"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r2, r3, r6)
                if (r2 == 0) goto L_0x0104
                r8 = -1
                r11 = 1
                goto L_0x0189
            L_0x0104:
                java.lang.String r2 = "max-age"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r2, r3, r6)
                if (r2 == 0) goto L_0x0115
                r2 = -1
                int r1 = okhttp3.internal.Util.toNonNegativeInt(r1, r2)
                r12 = r1
            L_0x0112:
                r8 = -1
                goto L_0x0189
            L_0x0115:
                r2 = -1
                java.lang.String r8 = "s-maxage"
                boolean r8 = kotlin.text.StringsKt__StringsJVMKt.b(r8, r3, r6)
                if (r8 == 0) goto L_0x0124
                int r1 = okhttp3.internal.Util.toNonNegativeInt(r1, r2)
                r13 = r1
                goto L_0x0112
            L_0x0124:
                java.lang.String r2 = "private"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r2, r3, r6)
                if (r2 == 0) goto L_0x012f
                r8 = -1
                r14 = 1
                goto L_0x0189
            L_0x012f:
                java.lang.String r2 = "public"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r2, r3, r6)
                if (r2 == 0) goto L_0x013a
                r8 = -1
                r15 = 1
                goto L_0x0189
            L_0x013a:
                java.lang.String r2 = "must-revalidate"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r2, r3, r6)
                if (r2 == 0) goto L_0x0146
                r8 = -1
                r16 = 1
                goto L_0x0189
            L_0x0146:
                java.lang.String r2 = "max-stale"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r2, r3, r6)
                if (r2 == 0) goto L_0x0158
                r2 = 2147483647(0x7fffffff, float:NaN)
                int r1 = okhttp3.internal.Util.toNonNegativeInt(r1, r2)
                r17 = r1
                goto L_0x0112
            L_0x0158:
                java.lang.String r2 = "min-fresh"
                boolean r2 = kotlin.text.StringsKt__StringsJVMKt.b(r2, r3, r6)
                if (r2 == 0) goto L_0x0168
                r8 = -1
                int r1 = okhttp3.internal.Util.toNonNegativeInt(r1, r8)
                r18 = r1
                goto L_0x0189
            L_0x0168:
                r8 = -1
                java.lang.String r1 = "only-if-cached"
                boolean r1 = kotlin.text.StringsKt__StringsJVMKt.b(r1, r3, r6)
                if (r1 == 0) goto L_0x0174
                r19 = 1
                goto L_0x0189
            L_0x0174:
                java.lang.String r1 = "no-transform"
                boolean r1 = kotlin.text.StringsKt__StringsJVMKt.b(r1, r3, r6)
                if (r1 == 0) goto L_0x017f
                r20 = 1
                goto L_0x0189
            L_0x017f:
                java.lang.String r1 = "immutable"
                boolean r1 = kotlin.text.StringsKt__StringsJVMKt.b(r1, r3, r6)
                if (r1 == 0) goto L_0x0189
                r21 = 1
            L_0x0189:
                r1 = r33
                r3 = r4
                r2 = r29
                r8 = r30
                r9 = r31
                goto L_0x0044
            L_0x0194:
                kotlin.TypeCastException r1 = new kotlin.TypeCastException
                r1.<init>(r2)
                throw r1
            L_0x019a:
                kotlin.TypeCastException r1 = new kotlin.TypeCastException
                r1.<init>(r6)
                throw r1
            L_0x01a0:
                r29 = r2
                r30 = r8
                r31 = r9
                goto L_0x01a9
            L_0x01a7:
                r29 = r2
            L_0x01a9:
                r1 = -1
                int r7 = r7 + 1
                r1 = r33
                r2 = r29
                goto L_0x0023
            L_0x01b2:
                if (r8 != 0) goto L_0x01b7
                r22 = 0
                goto L_0x01b9
            L_0x01b7:
                r22 = r9
            L_0x01b9:
                okhttp3.CacheControl r1 = new okhttp3.CacheControl
                r23 = 0
                r9 = r1
                r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.CacheControl.Companion.parse(okhttp3.Headers):okhttp3.CacheControl");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private CacheControl(boolean z, boolean z2, int i, int i2, boolean z3, boolean z4, boolean z5, int i3, int i4, boolean z6, boolean z7, boolean z8, String str) {
        this.noCache = z;
        this.noStore = z2;
        this.maxAgeSeconds = i;
        this.sMaxAgeSeconds = i2;
        this.isPrivate = z3;
        this.isPublic = z4;
        this.mustRevalidate = z5;
        this.maxStaleSeconds = i3;
        this.minFreshSeconds = i4;
        this.onlyIfCached = z6;
        this.noTransform = z7;
        this.immutable = z8;
        this.headerValue = str;
    }

    public static final CacheControl parse(Headers headers) {
        return Companion.parse(headers);
    }

    /* renamed from: -deprecated_immutable  reason: not valid java name */
    public final boolean m392deprecated_immutable() {
        return this.immutable;
    }

    /* renamed from: -deprecated_maxAgeSeconds  reason: not valid java name */
    public final int m393deprecated_maxAgeSeconds() {
        return this.maxAgeSeconds;
    }

    /* renamed from: -deprecated_maxStaleSeconds  reason: not valid java name */
    public final int m394deprecated_maxStaleSeconds() {
        return this.maxStaleSeconds;
    }

    /* renamed from: -deprecated_minFreshSeconds  reason: not valid java name */
    public final int m395deprecated_minFreshSeconds() {
        return this.minFreshSeconds;
    }

    /* renamed from: -deprecated_mustRevalidate  reason: not valid java name */
    public final boolean m396deprecated_mustRevalidate() {
        return this.mustRevalidate;
    }

    /* renamed from: -deprecated_noCache  reason: not valid java name */
    public final boolean m397deprecated_noCache() {
        return this.noCache;
    }

    /* renamed from: -deprecated_noStore  reason: not valid java name */
    public final boolean m398deprecated_noStore() {
        return this.noStore;
    }

    /* renamed from: -deprecated_noTransform  reason: not valid java name */
    public final boolean m399deprecated_noTransform() {
        return this.noTransform;
    }

    /* renamed from: -deprecated_onlyIfCached  reason: not valid java name */
    public final boolean m400deprecated_onlyIfCached() {
        return this.onlyIfCached;
    }

    /* renamed from: -deprecated_sMaxAgeSeconds  reason: not valid java name */
    public final int m401deprecated_sMaxAgeSeconds() {
        return this.sMaxAgeSeconds;
    }

    public final boolean immutable() {
        return this.immutable;
    }

    public final boolean isPrivate() {
        return this.isPrivate;
    }

    public final boolean isPublic() {
        return this.isPublic;
    }

    public final int maxAgeSeconds() {
        return this.maxAgeSeconds;
    }

    public final int maxStaleSeconds() {
        return this.maxStaleSeconds;
    }

    public final int minFreshSeconds() {
        return this.minFreshSeconds;
    }

    public final boolean mustRevalidate() {
        return this.mustRevalidate;
    }

    public final boolean noCache() {
        return this.noCache;
    }

    public final boolean noStore() {
        return this.noStore;
    }

    public final boolean noTransform() {
        return this.noTransform;
    }

    public final boolean onlyIfCached() {
        return this.onlyIfCached;
    }

    public final int sMaxAgeSeconds() {
        return this.sMaxAgeSeconds;
    }

    public String toString() {
        String str = this.headerValue;
        if (str != null) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        if (this.noCache) {
            sb.append("no-cache, ");
        }
        if (this.noStore) {
            sb.append("no-store, ");
        }
        if (this.maxAgeSeconds != -1) {
            sb.append("max-age=");
            sb.append(this.maxAgeSeconds);
            sb.append(", ");
        }
        if (this.sMaxAgeSeconds != -1) {
            sb.append("s-maxage=");
            sb.append(this.sMaxAgeSeconds);
            sb.append(", ");
        }
        if (this.isPrivate) {
            sb.append("private, ");
        }
        if (this.isPublic) {
            sb.append("public, ");
        }
        if (this.mustRevalidate) {
            sb.append("must-revalidate, ");
        }
        if (this.maxStaleSeconds != -1) {
            sb.append("max-stale=");
            sb.append(this.maxStaleSeconds);
            sb.append(", ");
        }
        if (this.minFreshSeconds != -1) {
            sb.append("min-fresh=");
            sb.append(this.minFreshSeconds);
            sb.append(", ");
        }
        if (this.onlyIfCached) {
            sb.append("only-if-cached, ");
        }
        if (this.noTransform) {
            sb.append("no-transform, ");
        }
        if (this.immutable) {
            sb.append("immutable, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        String sb2 = sb.toString();
        Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
        this.headerValue = sb2;
        return sb2;
    }

    public /* synthetic */ CacheControl(boolean z, boolean z2, int i, int i2, boolean z3, boolean z4, boolean z5, int i3, int i4, boolean z6, boolean z7, boolean z8, String str, DefaultConstructorMarker defaultConstructorMarker) {
        this(z, z2, i, i2, z3, z4, z5, i3, i4, z6, z7, z8, str);
    }
}
