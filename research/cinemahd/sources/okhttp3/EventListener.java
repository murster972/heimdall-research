package okhttp3;

import com.vungle.warren.model.ReportDBAdapter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;

public abstract class EventListener {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final EventListener NONE = new EventListener$Companion$NONE$1();

    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public interface Factory {
        EventListener create(Call call);
    }

    public void cacheConditionalHit(Call call, Response response) {
        Intrinsics.b(call, "call");
        Intrinsics.b(response, "cachedResponse");
    }

    public void cacheHit(Call call, Response response) {
        Intrinsics.b(call, "call");
        Intrinsics.b(response, "response");
    }

    public void cacheMiss(Call call) {
        Intrinsics.b(call, "call");
    }

    public void callEnd(Call call) {
        Intrinsics.b(call, "call");
    }

    public void callFailed(Call call, IOException iOException) {
        Intrinsics.b(call, "call");
        Intrinsics.b(iOException, "ioe");
    }

    public void callStart(Call call) {
        Intrinsics.b(call, "call");
    }

    public void canceled(Call call) {
        Intrinsics.b(call, "call");
    }

    public void connectEnd(Call call, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol) {
        Intrinsics.b(call, "call");
        Intrinsics.b(inetSocketAddress, "inetSocketAddress");
        Intrinsics.b(proxy, "proxy");
    }

    public void connectFailed(Call call, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol, IOException iOException) {
        Intrinsics.b(call, "call");
        Intrinsics.b(inetSocketAddress, "inetSocketAddress");
        Intrinsics.b(proxy, "proxy");
        Intrinsics.b(iOException, "ioe");
    }

    public void connectStart(Call call, InetSocketAddress inetSocketAddress, Proxy proxy) {
        Intrinsics.b(call, "call");
        Intrinsics.b(inetSocketAddress, "inetSocketAddress");
        Intrinsics.b(proxy, "proxy");
    }

    public void connectionAcquired(Call call, Connection connection) {
        Intrinsics.b(call, "call");
        Intrinsics.b(connection, "connection");
    }

    public void connectionReleased(Call call, Connection connection) {
        Intrinsics.b(call, "call");
        Intrinsics.b(connection, "connection");
    }

    public void dnsEnd(Call call, String str, List<InetAddress> list) {
        Intrinsics.b(call, "call");
        Intrinsics.b(str, "domainName");
        Intrinsics.b(list, "inetAddressList");
    }

    public void dnsStart(Call call, String str) {
        Intrinsics.b(call, "call");
        Intrinsics.b(str, "domainName");
    }

    public void proxySelectEnd(Call call, HttpUrl httpUrl, List<Proxy> list) {
        Intrinsics.b(call, "call");
        Intrinsics.b(httpUrl, ReportDBAdapter.ReportColumns.COLUMN_URL);
        Intrinsics.b(list, "proxies");
    }

    public void proxySelectStart(Call call, HttpUrl httpUrl) {
        Intrinsics.b(call, "call");
        Intrinsics.b(httpUrl, ReportDBAdapter.ReportColumns.COLUMN_URL);
    }

    public void requestBodyEnd(Call call, long j) {
        Intrinsics.b(call, "call");
    }

    public void requestBodyStart(Call call) {
        Intrinsics.b(call, "call");
    }

    public void requestFailed(Call call, IOException iOException) {
        Intrinsics.b(call, "call");
        Intrinsics.b(iOException, "ioe");
    }

    public void requestHeadersEnd(Call call, Request request) {
        Intrinsics.b(call, "call");
        Intrinsics.b(request, "request");
    }

    public void requestHeadersStart(Call call) {
        Intrinsics.b(call, "call");
    }

    public void responseBodyEnd(Call call, long j) {
        Intrinsics.b(call, "call");
    }

    public void responseBodyStart(Call call) {
        Intrinsics.b(call, "call");
    }

    public void responseFailed(Call call, IOException iOException) {
        Intrinsics.b(call, "call");
        Intrinsics.b(iOException, "ioe");
    }

    public void responseHeadersEnd(Call call, Response response) {
        Intrinsics.b(call, "call");
        Intrinsics.b(response, "response");
    }

    public void responseHeadersStart(Call call) {
        Intrinsics.b(call, "call");
    }

    public void satisfactionFailure(Call call, Response response) {
        Intrinsics.b(call, "call");
        Intrinsics.b(response, "response");
    }

    public void secureConnectEnd(Call call, Handshake handshake) {
        Intrinsics.b(call, "call");
    }

    public void secureConnectStart(Call call) {
        Intrinsics.b(call, "call");
    }
}
