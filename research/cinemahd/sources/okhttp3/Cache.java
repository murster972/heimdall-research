package okhttp3;

import com.uwetrottmann.trakt5.TraktV2;
import com.vungle.warren.model.ReportDBAdapter;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Util;
import okhttp3.internal.cache.CacheRequest;
import okhttp3.internal.cache.CacheStrategy;
import okhttp3.internal.cache.DiskLruCache;
import okhttp3.internal.concurrent.TaskRunner;
import okhttp3.internal.http.HttpMethod;
import okhttp3.internal.http.StatusLine;
import okhttp3.internal.io.FileSystem;
import okhttp3.internal.platform.Platform;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.ByteString;
import okio.ForwardingSink;
import okio.ForwardingSource;
import okio.Okio;
import okio.Sink;
import okio.Source;

public final class Cache implements Closeable, Flushable {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final int ENTRY_BODY = 1;
    private static final int ENTRY_COUNT = 2;
    private static final int ENTRY_METADATA = 0;
    private static final int VERSION = 201105;
    private final DiskLruCache cache;
    private int hitCount;
    private int networkCount;
    private int requestCount;
    private int writeAbortCount;
    private int writeSuccessCount;

    private static final class CacheResponseBody extends ResponseBody {
        private final BufferedSource bodySource;
        private final String contentLength;
        private final String contentType;
        private final DiskLruCache.Snapshot snapshot;

        public CacheResponseBody(DiskLruCache.Snapshot snapshot2, String str, String str2) {
            Intrinsics.b(snapshot2, "snapshot");
            this.snapshot = snapshot2;
            this.contentType = str;
            this.contentLength = str2;
            Source source = this.snapshot.getSource(1);
            this.bodySource = Okio.a((Source) new ForwardingSource(this, source, source) {
                final /* synthetic */ Source $source;
                final /* synthetic */ CacheResponseBody this$0;

                {
                    this.this$0 = r1;
                    this.$source = r2;
                }

                public void close() throws IOException {
                    this.this$0.getSnapshot$okhttp().close();
                    super.close();
                }
            });
        }

        public long contentLength() {
            String str = this.contentLength;
            if (str != null) {
                return Util.toLongOrDefault(str, -1);
            }
            return -1;
        }

        public MediaType contentType() {
            String str = this.contentType;
            if (str != null) {
                return MediaType.Companion.parse(str);
            }
            return null;
        }

        public final DiskLruCache.Snapshot getSnapshot$okhttp() {
            return this.snapshot;
        }

        public BufferedSource source() {
            return this.bodySource;
        }
    }

    public static final class Companion {
        private Companion() {
        }

        private final Set<String> varyFields(Headers headers) {
            int size = headers.size();
            TreeSet treeSet = null;
            for (int i = 0; i < size; i++) {
                if (StringsKt__StringsJVMKt.b("Vary", headers.name(i), true)) {
                    String value = headers.value(i);
                    if (treeSet == null) {
                        treeSet = new TreeSet(StringsKt__StringsJVMKt.a(StringCompanionObject.f6941a));
                    }
                    for (String str : StringsKt__StringsKt.a((CharSequence) value, new char[]{','}, false, 0, 6, (Object) null)) {
                        if (str != null) {
                            treeSet.add(StringsKt__StringsKt.f(str).toString());
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                        }
                    }
                    continue;
                }
            }
            return treeSet != null ? treeSet : SetsKt__SetsKt.a();
        }

        public final boolean hasVaryAll(Response response) {
            Intrinsics.b(response, "$this$hasVaryAll");
            return varyFields(response.headers()).contains("*");
        }

        public final String key(HttpUrl httpUrl) {
            Intrinsics.b(httpUrl, ReportDBAdapter.ReportColumns.COLUMN_URL);
            return ByteString.d.c(httpUrl.toString()).h().f();
        }

        public final int readInt$okhttp(BufferedSource bufferedSource) throws IOException {
            Intrinsics.b(bufferedSource, "source");
            try {
                long f = bufferedSource.f();
                String c = bufferedSource.c();
                if (f >= 0 && f <= ((long) Integer.MAX_VALUE)) {
                    if (!(c.length() > 0)) {
                        return (int) f;
                    }
                }
                throw new IOException("expected an int but was \"" + f + c + '\"');
            } catch (NumberFormatException e) {
                throw new IOException(e.getMessage());
            }
        }

        public final Headers varyHeaders(Response response) {
            Intrinsics.b(response, "$this$varyHeaders");
            Response networkResponse = response.networkResponse();
            if (networkResponse != null) {
                return varyHeaders(networkResponse.request().headers(), response.headers());
            }
            Intrinsics.a();
            throw null;
        }

        public final boolean varyMatches(Response response, Headers headers, Request request) {
            Intrinsics.b(response, "cachedResponse");
            Intrinsics.b(headers, "cachedRequest");
            Intrinsics.b(request, "newRequest");
            Set<String> varyFields = varyFields(response.headers());
            if ((varyFields instanceof Collection) && varyFields.isEmpty()) {
                return true;
            }
            for (String str : varyFields) {
                if (!Intrinsics.a((Object) headers.values(str), (Object) request.headers(str))) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private final Headers varyHeaders(Headers headers, Headers headers2) {
            Set<String> varyFields = varyFields(headers2);
            if (varyFields.isEmpty()) {
                return Util.EMPTY_HEADERS;
            }
            Headers.Builder builder = new Headers.Builder();
            int size = headers.size();
            for (int i = 0; i < size; i++) {
                String name = headers.name(i);
                if (varyFields.contains(name)) {
                    builder.add(name, headers.value(i));
                }
            }
            return builder.build();
        }
    }

    private final class RealCacheRequest implements CacheRequest {
        private final Sink body = new ForwardingSink(this, this.cacheOut) {
            final /* synthetic */ RealCacheRequest this$0;

            {
                this.this$0 = r1;
            }

            public void close() throws IOException {
                synchronized (this.this$0.this$0) {
                    if (!this.this$0.getDone$okhttp()) {
                        this.this$0.setDone$okhttp(true);
                        Cache cache = this.this$0.this$0;
                        cache.setWriteSuccessCount$okhttp(cache.getWriteSuccessCount$okhttp() + 1);
                        super.close();
                        this.this$0.editor.commit();
                    }
                }
            }
        };
        private final Sink cacheOut = this.editor.newSink(1);
        private boolean done;
        /* access modifiers changed from: private */
        public final DiskLruCache.Editor editor;
        final /* synthetic */ Cache this$0;

        public RealCacheRequest(Cache cache, DiskLruCache.Editor editor2) {
            Intrinsics.b(editor2, "editor");
            this.this$0 = cache;
            this.editor = editor2;
        }

        public void abort() {
            synchronized (this.this$0) {
                if (!this.done) {
                    this.done = true;
                    Cache cache = this.this$0;
                    cache.setWriteAbortCount$okhttp(cache.getWriteAbortCount$okhttp() + 1);
                    Util.closeQuietly((Closeable) this.cacheOut);
                    try {
                        this.editor.abort();
                    } catch (IOException unused) {
                    }
                }
            }
        }

        public Sink body() {
            return this.body;
        }

        public final boolean getDone$okhttp() {
            return this.done;
        }

        public final void setDone$okhttp(boolean z) {
            this.done = z;
        }
    }

    public Cache(File file, long j, FileSystem fileSystem) {
        Intrinsics.b(file, "directory");
        Intrinsics.b(fileSystem, "fileSystem");
        this.cache = new DiskLruCache(fileSystem, file, VERSION, 2, j, TaskRunner.INSTANCE);
    }

    private final void abortQuietly(DiskLruCache.Editor editor) {
        if (editor != null) {
            try {
                editor.abort();
            } catch (IOException unused) {
            }
        }
    }

    public static final String key(HttpUrl httpUrl) {
        return Companion.key(httpUrl);
    }

    /* renamed from: -deprecated_directory  reason: not valid java name */
    public final File m391deprecated_directory() {
        return this.cache.getDirectory();
    }

    public void close() throws IOException {
        this.cache.close();
    }

    public final void delete() throws IOException {
        this.cache.delete();
    }

    public final File directory() {
        return this.cache.getDirectory();
    }

    public final void evictAll() throws IOException {
        this.cache.evictAll();
    }

    public void flush() throws IOException {
        this.cache.flush();
    }

    public final Response get$okhttp(Request request) {
        Intrinsics.b(request, "request");
        try {
            DiskLruCache.Snapshot snapshot = this.cache.get(Companion.key(request.url()));
            if (snapshot != null) {
                try {
                    Entry entry = new Entry(snapshot.getSource(0));
                    Response response = entry.response(snapshot);
                    if (entry.matches(request, response)) {
                        return response;
                    }
                    ResponseBody body = response.body();
                    if (body != null) {
                        Util.closeQuietly((Closeable) body);
                    }
                    return null;
                } catch (IOException unused) {
                    Util.closeQuietly((Closeable) snapshot);
                }
            }
        } catch (IOException unused2) {
        }
        return null;
    }

    public final DiskLruCache getCache$okhttp() {
        return this.cache;
    }

    public final int getWriteAbortCount$okhttp() {
        return this.writeAbortCount;
    }

    public final int getWriteSuccessCount$okhttp() {
        return this.writeSuccessCount;
    }

    public final synchronized int hitCount() {
        return this.hitCount;
    }

    public final void initialize() throws IOException {
        this.cache.initialize();
    }

    public final boolean isClosed() {
        return this.cache.isClosed();
    }

    public final long maxSize() {
        return this.cache.getMaxSize();
    }

    public final synchronized int networkCount() {
        return this.networkCount;
    }

    public final CacheRequest put$okhttp(Response response) {
        DiskLruCache.Editor editor;
        Intrinsics.b(response, "response");
        String method = response.request().method();
        if (HttpMethod.INSTANCE.invalidatesCache(response.request().method())) {
            try {
                remove$okhttp(response.request());
            } catch (IOException unused) {
            }
            return null;
        } else if ((!Intrinsics.a((Object) method, (Object) "GET")) || Companion.hasVaryAll(response)) {
            return null;
        } else {
            Entry entry = new Entry(response);
            try {
                editor = DiskLruCache.edit$default(this.cache, Companion.key(response.request().url()), 0, 2, (Object) null);
                if (editor == null) {
                    return null;
                }
                try {
                    entry.writeTo(editor);
                    return new RealCacheRequest(this, editor);
                } catch (IOException unused2) {
                    abortQuietly(editor);
                    return null;
                }
            } catch (IOException unused3) {
                editor = null;
                abortQuietly(editor);
                return null;
            }
        }
    }

    public final void remove$okhttp(Request request) throws IOException {
        Intrinsics.b(request, "request");
        this.cache.remove(Companion.key(request.url()));
    }

    public final synchronized int requestCount() {
        return this.requestCount;
    }

    public final void setWriteAbortCount$okhttp(int i) {
        this.writeAbortCount = i;
    }

    public final void setWriteSuccessCount$okhttp(int i) {
        this.writeSuccessCount = i;
    }

    public final long size() throws IOException {
        return this.cache.size();
    }

    public final synchronized void trackConditionalCacheHit$okhttp() {
        this.hitCount++;
    }

    public final synchronized void trackResponse$okhttp(CacheStrategy cacheStrategy) {
        Intrinsics.b(cacheStrategy, "cacheStrategy");
        this.requestCount++;
        if (cacheStrategy.getNetworkRequest() != null) {
            this.networkCount++;
        } else if (cacheStrategy.getCacheResponse() != null) {
            this.hitCount++;
        }
    }

    public final void update$okhttp(Response response, Response response2) {
        Intrinsics.b(response, "cached");
        Intrinsics.b(response2, "network");
        Entry entry = new Entry(response2);
        ResponseBody body = response.body();
        if (body != null) {
            try {
                DiskLruCache.Editor edit = ((CacheResponseBody) body).getSnapshot$okhttp().edit();
                if (edit != null) {
                    entry.writeTo(edit);
                    edit.commit();
                }
            } catch (IOException unused) {
                abortQuietly((DiskLruCache.Editor) null);
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type okhttp3.Cache.CacheResponseBody");
        }
    }

    public final Iterator<String> urls() throws IOException {
        return new Cache$urls$1(this);
    }

    public final synchronized int writeAbortCount() {
        return this.writeAbortCount;
    }

    public final synchronized int writeSuccessCount() {
        return this.writeSuccessCount;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Cache(File file, long j) {
        this(file, j, FileSystem.SYSTEM);
        Intrinsics.b(file, "directory");
    }

    private static final class Entry {
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String RECEIVED_MILLIS = (Platform.Companion.get().getPrefix() + "-Received-Millis");
        private static final String SENT_MILLIS = (Platform.Companion.get().getPrefix() + "-Sent-Millis");
        private final int code;
        private final Handshake handshake;
        private final String message;
        private final Protocol protocol;
        private final long receivedResponseMillis;
        private final String requestMethod;
        private final Headers responseHeaders;
        private final long sentRequestMillis;
        private final String url;
        private final Headers varyHeaders;

        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Entry(Source source) throws IOException {
            TlsVersion tlsVersion;
            Intrinsics.b(source, "rawSource");
            try {
                BufferedSource a2 = Okio.a(source);
                this.url = a2.c();
                this.requestMethod = a2.c();
                Headers.Builder builder = new Headers.Builder();
                int readInt$okhttp = Cache.Companion.readInt$okhttp(a2);
                boolean z = false;
                for (int i = 0; i < readInt$okhttp; i++) {
                    builder.addLenient$okhttp(a2.c());
                }
                this.varyHeaders = builder.build();
                StatusLine parse = StatusLine.Companion.parse(a2.c());
                this.protocol = parse.protocol;
                this.code = parse.code;
                this.message = parse.message;
                Headers.Builder builder2 = new Headers.Builder();
                int readInt$okhttp2 = Cache.Companion.readInt$okhttp(a2);
                for (int i2 = 0; i2 < readInt$okhttp2; i2++) {
                    builder2.addLenient$okhttp(a2.c());
                }
                String str = builder2.get(SENT_MILLIS);
                String str2 = builder2.get(RECEIVED_MILLIS);
                builder2.removeAll(SENT_MILLIS);
                builder2.removeAll(RECEIVED_MILLIS);
                long j = 0;
                this.sentRequestMillis = str != null ? Long.parseLong(str) : 0;
                this.receivedResponseMillis = str2 != null ? Long.parseLong(str2) : j;
                this.responseHeaders = builder2.build();
                if (isHttps()) {
                    String c = a2.c();
                    if (!(c.length() > 0 ? true : z)) {
                        CipherSuite forJavaName = CipherSuite.Companion.forJavaName(a2.c());
                        List<Certificate> readCertificateList = readCertificateList(a2);
                        List<Certificate> readCertificateList2 = readCertificateList(a2);
                        if (!a2.e()) {
                            tlsVersion = TlsVersion.Companion.forJavaName(a2.c());
                        } else {
                            tlsVersion = TlsVersion.SSL_3_0;
                        }
                        this.handshake = Handshake.Companion.get(tlsVersion, forJavaName, readCertificateList, readCertificateList2);
                    } else {
                        throw new IOException("expected \"\" but was \"" + c + '\"');
                    }
                } else {
                    this.handshake = null;
                }
            } finally {
                source.close();
            }
        }

        private final boolean isHttps() {
            return StringsKt__StringsJVMKt.b(this.url, "https://", false, 2, (Object) null);
        }

        private final List<Certificate> readCertificateList(BufferedSource bufferedSource) throws IOException {
            int readInt$okhttp = Cache.Companion.readInt$okhttp(bufferedSource);
            if (readInt$okhttp == -1) {
                return CollectionsKt__CollectionsKt.a();
            }
            try {
                CertificateFactory instance = CertificateFactory.getInstance("X.509");
                ArrayList arrayList = new ArrayList(readInt$okhttp);
                int i = 0;
                while (i < readInt$okhttp) {
                    String c = bufferedSource.c();
                    Buffer buffer = new Buffer();
                    ByteString a2 = ByteString.d.a(c);
                    if (a2 != null) {
                        buffer.a(a2);
                        arrayList.add(instance.generateCertificate(buffer.b()));
                        i++;
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                return arrayList;
            } catch (CertificateException e) {
                throw new IOException(e.getMessage());
            }
        }

        private final void writeCertList(BufferedSink bufferedSink, List<? extends Certificate> list) throws IOException {
            try {
                bufferedSink.e((long) list.size()).writeByte(10);
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    byte[] encoded = ((Certificate) list.get(i)).getEncoded();
                    ByteString.Companion companion = ByteString.d;
                    Intrinsics.a((Object) encoded, "bytes");
                    bufferedSink.a(ByteString.Companion.a(companion, encoded, 0, 0, 3, (Object) null).a()).writeByte(10);
                }
            } catch (CertificateEncodingException e) {
                throw new IOException(e.getMessage());
            }
        }

        public final boolean matches(Request request, Response response) {
            Intrinsics.b(request, "request");
            Intrinsics.b(response, "response");
            return Intrinsics.a((Object) this.url, (Object) request.url().toString()) && Intrinsics.a((Object) this.requestMethod, (Object) request.method()) && Cache.Companion.varyMatches(response, this.varyHeaders, request);
        }

        public final Response response(DiskLruCache.Snapshot snapshot) {
            Intrinsics.b(snapshot, "snapshot");
            String str = this.responseHeaders.get(TraktV2.HEADER_CONTENT_TYPE);
            String str2 = this.responseHeaders.get("Content-Length");
            return new Response.Builder().request(new Request.Builder().url(this.url).method(this.requestMethod, (RequestBody) null).headers(this.varyHeaders).build()).protocol(this.protocol).code(this.code).message(this.message).headers(this.responseHeaders).body(new CacheResponseBody(snapshot, str, str2)).handshake(this.handshake).sentRequestAtMillis(this.sentRequestMillis).receivedResponseAtMillis(this.receivedResponseMillis).build();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0118, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0119, code lost:
            kotlin.io.CloseableKt.a(r8, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x011c, code lost:
            throw r1;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void writeTo(okhttp3.internal.cache.DiskLruCache.Editor r8) throws java.io.IOException {
            /*
                r7 = this;
                java.lang.String r0 = "editor"
                kotlin.jvm.internal.Intrinsics.b(r8, r0)
                r0 = 0
                okio.Sink r8 = r8.newSink(r0)
                okio.BufferedSink r8 = okio.Okio.a((okio.Sink) r8)
                java.lang.String r1 = r7.url     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r1 = r8.a((java.lang.String) r1)     // Catch:{ all -> 0x0116 }
                r2 = 10
                r1.writeByte(r2)     // Catch:{ all -> 0x0116 }
                java.lang.String r1 = r7.requestMethod     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r1 = r8.a((java.lang.String) r1)     // Catch:{ all -> 0x0116 }
                r1.writeByte(r2)     // Catch:{ all -> 0x0116 }
                okhttp3.Headers r1 = r7.varyHeaders     // Catch:{ all -> 0x0116 }
                int r1 = r1.size()     // Catch:{ all -> 0x0116 }
                long r3 = (long) r1     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r1 = r8.e(r3)     // Catch:{ all -> 0x0116 }
                r1.writeByte(r2)     // Catch:{ all -> 0x0116 }
                okhttp3.Headers r1 = r7.varyHeaders     // Catch:{ all -> 0x0116 }
                int r1 = r1.size()     // Catch:{ all -> 0x0116 }
                r3 = 0
            L_0x0037:
                java.lang.String r4 = ": "
                if (r3 >= r1) goto L_0x0059
                okhttp3.Headers r5 = r7.varyHeaders     // Catch:{ all -> 0x0116 }
                java.lang.String r5 = r5.name(r3)     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r5 = r8.a((java.lang.String) r5)     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r4 = r5.a((java.lang.String) r4)     // Catch:{ all -> 0x0116 }
                okhttp3.Headers r5 = r7.varyHeaders     // Catch:{ all -> 0x0116 }
                java.lang.String r5 = r5.value(r3)     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r4 = r4.a((java.lang.String) r5)     // Catch:{ all -> 0x0116 }
                r4.writeByte(r2)     // Catch:{ all -> 0x0116 }
                int r3 = r3 + 1
                goto L_0x0037
            L_0x0059:
                okhttp3.internal.http.StatusLine r1 = new okhttp3.internal.http.StatusLine     // Catch:{ all -> 0x0116 }
                okhttp3.Protocol r3 = r7.protocol     // Catch:{ all -> 0x0116 }
                int r5 = r7.code     // Catch:{ all -> 0x0116 }
                java.lang.String r6 = r7.message     // Catch:{ all -> 0x0116 }
                r1.<init>(r3, r5, r6)     // Catch:{ all -> 0x0116 }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r1 = r8.a((java.lang.String) r1)     // Catch:{ all -> 0x0116 }
                r1.writeByte(r2)     // Catch:{ all -> 0x0116 }
                okhttp3.Headers r1 = r7.responseHeaders     // Catch:{ all -> 0x0116 }
                int r1 = r1.size()     // Catch:{ all -> 0x0116 }
                int r1 = r1 + 2
                long r5 = (long) r1     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r1 = r8.e(r5)     // Catch:{ all -> 0x0116 }
                r1.writeByte(r2)     // Catch:{ all -> 0x0116 }
                okhttp3.Headers r1 = r7.responseHeaders     // Catch:{ all -> 0x0116 }
                int r1 = r1.size()     // Catch:{ all -> 0x0116 }
            L_0x0085:
                if (r0 >= r1) goto L_0x00a5
                okhttp3.Headers r3 = r7.responseHeaders     // Catch:{ all -> 0x0116 }
                java.lang.String r3 = r3.name(r0)     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r3 = r8.a((java.lang.String) r3)     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r3 = r3.a((java.lang.String) r4)     // Catch:{ all -> 0x0116 }
                okhttp3.Headers r5 = r7.responseHeaders     // Catch:{ all -> 0x0116 }
                java.lang.String r5 = r5.value(r0)     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r3 = r3.a((java.lang.String) r5)     // Catch:{ all -> 0x0116 }
                r3.writeByte(r2)     // Catch:{ all -> 0x0116 }
                int r0 = r0 + 1
                goto L_0x0085
            L_0x00a5:
                java.lang.String r0 = SENT_MILLIS     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r0 = r8.a((java.lang.String) r0)     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r0 = r0.a((java.lang.String) r4)     // Catch:{ all -> 0x0116 }
                long r5 = r7.sentRequestMillis     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r0 = r0.e(r5)     // Catch:{ all -> 0x0116 }
                r0.writeByte(r2)     // Catch:{ all -> 0x0116 }
                java.lang.String r0 = RECEIVED_MILLIS     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r0 = r8.a((java.lang.String) r0)     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r0 = r0.a((java.lang.String) r4)     // Catch:{ all -> 0x0116 }
                long r3 = r7.receivedResponseMillis     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r0 = r0.e(r3)     // Catch:{ all -> 0x0116 }
                r0.writeByte(r2)     // Catch:{ all -> 0x0116 }
                boolean r0 = r7.isHttps()     // Catch:{ all -> 0x0116 }
                r1 = 0
                if (r0 == 0) goto L_0x0110
                r8.writeByte(r2)     // Catch:{ all -> 0x0116 }
                okhttp3.Handshake r0 = r7.handshake     // Catch:{ all -> 0x0116 }
                if (r0 == 0) goto L_0x010c
                okhttp3.CipherSuite r0 = r0.cipherSuite()     // Catch:{ all -> 0x0116 }
                java.lang.String r0 = r0.javaName()     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r0 = r8.a((java.lang.String) r0)     // Catch:{ all -> 0x0116 }
                r0.writeByte(r2)     // Catch:{ all -> 0x0116 }
                okhttp3.Handshake r0 = r7.handshake     // Catch:{ all -> 0x0116 }
                java.util.List r0 = r0.peerCertificates()     // Catch:{ all -> 0x0116 }
                r7.writeCertList(r8, r0)     // Catch:{ all -> 0x0116 }
                okhttp3.Handshake r0 = r7.handshake     // Catch:{ all -> 0x0116 }
                java.util.List r0 = r0.localCertificates()     // Catch:{ all -> 0x0116 }
                r7.writeCertList(r8, r0)     // Catch:{ all -> 0x0116 }
                okhttp3.Handshake r0 = r7.handshake     // Catch:{ all -> 0x0116 }
                okhttp3.TlsVersion r0 = r0.tlsVersion()     // Catch:{ all -> 0x0116 }
                java.lang.String r0 = r0.javaName()     // Catch:{ all -> 0x0116 }
                okio.BufferedSink r0 = r8.a((java.lang.String) r0)     // Catch:{ all -> 0x0116 }
                r0.writeByte(r2)     // Catch:{ all -> 0x0116 }
                goto L_0x0110
            L_0x010c:
                kotlin.jvm.internal.Intrinsics.a()     // Catch:{ all -> 0x0116 }
                throw r1
            L_0x0110:
                kotlin.Unit r0 = kotlin.Unit.f6917a     // Catch:{ all -> 0x0116 }
                kotlin.io.CloseableKt.a(r8, r1)
                return
            L_0x0116:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0118 }
            L_0x0118:
                r1 = move-exception
                kotlin.io.CloseableKt.a(r8, r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.Cache.Entry.writeTo(okhttp3.internal.cache.DiskLruCache$Editor):void");
        }

        public Entry(Response response) {
            Intrinsics.b(response, "response");
            this.url = response.request().url().toString();
            this.varyHeaders = Cache.Companion.varyHeaders(response);
            this.requestMethod = response.request().method();
            this.protocol = response.protocol();
            this.code = response.code();
            this.message = response.message();
            this.responseHeaders = response.headers();
            this.handshake = response.handshake();
            this.sentRequestMillis = response.sentRequestAtMillis();
            this.receivedResponseMillis = response.receivedResponseAtMillis();
        }
    }
}
