package androidx.cardview;

public final class R$attr {
    public static final int cardBackgroundColor = 2130968698;
    public static final int cardCornerRadius = 2130968699;
    public static final int cardElevation = 2130968700;
    public static final int cardMaxElevation = 2130968701;
    public static final int cardPreventCornerOverlap = 2130968702;
    public static final int cardUseCompatPadding = 2130968703;
    public static final int cardViewStyle = 2130968704;
    public static final int contentPadding = 2130968811;
    public static final int contentPaddingBottom = 2130968812;
    public static final int contentPaddingLeft = 2130968813;
    public static final int contentPaddingRight = 2130968814;
    public static final int contentPaddingTop = 2130968815;

    private R$attr() {
    }
}
