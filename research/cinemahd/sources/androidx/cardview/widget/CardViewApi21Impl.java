package androidx.cardview.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;

class CardViewApi21Impl implements CardViewImpl {
    CardViewApi21Impl() {
    }

    private RoundRectDrawable j(CardViewDelegate cardViewDelegate) {
        return (RoundRectDrawable) cardViewDelegate.b();
    }

    public void a() {
    }

    public void a(CardViewDelegate cardViewDelegate, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        cardViewDelegate.a(new RoundRectDrawable(colorStateList, f));
        View d = cardViewDelegate.d();
        d.setClipToOutline(true);
        d.setElevation(f2);
        c(cardViewDelegate, f3);
    }

    public float b(CardViewDelegate cardViewDelegate) {
        return j(cardViewDelegate).b();
    }

    public void c(CardViewDelegate cardViewDelegate, float f) {
        j(cardViewDelegate).a(f, cardViewDelegate.a(), cardViewDelegate.c());
        h(cardViewDelegate);
    }

    public float d(CardViewDelegate cardViewDelegate) {
        return a(cardViewDelegate) * 2.0f;
    }

    public void e(CardViewDelegate cardViewDelegate) {
        c(cardViewDelegate, b(cardViewDelegate));
    }

    public float f(CardViewDelegate cardViewDelegate) {
        return cardViewDelegate.d().getElevation();
    }

    public void g(CardViewDelegate cardViewDelegate) {
        c(cardViewDelegate, b(cardViewDelegate));
    }

    public void h(CardViewDelegate cardViewDelegate) {
        if (!cardViewDelegate.a()) {
            cardViewDelegate.a(0, 0, 0, 0);
            return;
        }
        float b = b(cardViewDelegate);
        float a2 = a(cardViewDelegate);
        int ceil = (int) Math.ceil((double) RoundRectDrawableWithShadow.a(b, a2, cardViewDelegate.c()));
        int ceil2 = (int) Math.ceil((double) RoundRectDrawableWithShadow.b(b, a2, cardViewDelegate.c()));
        cardViewDelegate.a(ceil, ceil2, ceil, ceil2);
    }

    public float i(CardViewDelegate cardViewDelegate) {
        return a(cardViewDelegate) * 2.0f;
    }

    public void b(CardViewDelegate cardViewDelegate, float f) {
        cardViewDelegate.d().setElevation(f);
    }

    public ColorStateList c(CardViewDelegate cardViewDelegate) {
        return j(cardViewDelegate).a();
    }

    public void a(CardViewDelegate cardViewDelegate, float f) {
        j(cardViewDelegate).a(f);
    }

    public float a(CardViewDelegate cardViewDelegate) {
        return j(cardViewDelegate).c();
    }

    public void a(CardViewDelegate cardViewDelegate, ColorStateList colorStateList) {
        j(cardViewDelegate).a(colorStateList);
    }
}
