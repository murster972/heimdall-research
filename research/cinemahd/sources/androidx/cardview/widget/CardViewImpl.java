package androidx.cardview.widget;

import android.content.Context;
import android.content.res.ColorStateList;

interface CardViewImpl {
    float a(CardViewDelegate cardViewDelegate);

    void a();

    void a(CardViewDelegate cardViewDelegate, float f);

    void a(CardViewDelegate cardViewDelegate, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    void a(CardViewDelegate cardViewDelegate, ColorStateList colorStateList);

    float b(CardViewDelegate cardViewDelegate);

    void b(CardViewDelegate cardViewDelegate, float f);

    ColorStateList c(CardViewDelegate cardViewDelegate);

    void c(CardViewDelegate cardViewDelegate, float f);

    float d(CardViewDelegate cardViewDelegate);

    void e(CardViewDelegate cardViewDelegate);

    float f(CardViewDelegate cardViewDelegate);

    void g(CardViewDelegate cardViewDelegate);

    void h(CardViewDelegate cardViewDelegate);

    float i(CardViewDelegate cardViewDelegate);
}
