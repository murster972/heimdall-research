package androidx.cardview.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import androidx.cardview.widget.RoundRectDrawableWithShadow;

class CardViewBaseImpl implements CardViewImpl {

    /* renamed from: a  reason: collision with root package name */
    final RectF f467a = new RectF();

    CardViewBaseImpl() {
    }

    private RoundRectDrawableWithShadow j(CardViewDelegate cardViewDelegate) {
        return (RoundRectDrawableWithShadow) cardViewDelegate.b();
    }

    public void a() {
        RoundRectDrawableWithShadow.r = new RoundRectDrawableWithShadow.RoundRectHelper() {
            public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
                Canvas canvas2 = canvas;
                RectF rectF2 = rectF;
                float f2 = 2.0f * f;
                float width = (rectF.width() - f2) - 1.0f;
                float height = (rectF.height() - f2) - 1.0f;
                if (f >= 1.0f) {
                    float f3 = f + 0.5f;
                    float f4 = -f3;
                    CardViewBaseImpl.this.f467a.set(f4, f4, f3, f3);
                    int save = canvas.save();
                    canvas2.translate(rectF2.left + f3, rectF2.top + f3);
                    Paint paint2 = paint;
                    canvas.drawArc(CardViewBaseImpl.this.f467a, 180.0f, 90.0f, true, paint2);
                    canvas2.translate(width, 0.0f);
                    canvas2.rotate(90.0f);
                    canvas.drawArc(CardViewBaseImpl.this.f467a, 180.0f, 90.0f, true, paint2);
                    canvas2.translate(height, 0.0f);
                    canvas2.rotate(90.0f);
                    canvas.drawArc(CardViewBaseImpl.this.f467a, 180.0f, 90.0f, true, paint2);
                    canvas2.translate(width, 0.0f);
                    canvas2.rotate(90.0f);
                    canvas.drawArc(CardViewBaseImpl.this.f467a, 180.0f, 90.0f, true, paint2);
                    canvas2.restoreToCount(save);
                    float f5 = rectF2.top;
                    canvas.drawRect((rectF2.left + f3) - 1.0f, f5, (rectF2.right - f3) + 1.0f, f5 + f3, paint2);
                    float f6 = rectF2.bottom;
                    Canvas canvas3 = canvas;
                    canvas3.drawRect((rectF2.left + f3) - 1.0f, f6 - f3, (rectF2.right - f3) + 1.0f, f6, paint2);
                }
                canvas.drawRect(rectF2.left, rectF2.top + f, rectF2.right, rectF2.bottom - f, paint);
            }
        };
    }

    public void b(CardViewDelegate cardViewDelegate, float f) {
        j(cardViewDelegate).c(f);
    }

    public ColorStateList c(CardViewDelegate cardViewDelegate) {
        return j(cardViewDelegate).a();
    }

    public float d(CardViewDelegate cardViewDelegate) {
        return j(cardViewDelegate).d();
    }

    public void e(CardViewDelegate cardViewDelegate) {
        j(cardViewDelegate).a(cardViewDelegate.c());
        h(cardViewDelegate);
    }

    public float f(CardViewDelegate cardViewDelegate) {
        return j(cardViewDelegate).f();
    }

    public void g(CardViewDelegate cardViewDelegate) {
    }

    public void h(CardViewDelegate cardViewDelegate) {
        Rect rect = new Rect();
        j(cardViewDelegate).a(rect);
        cardViewDelegate.a((int) Math.ceil((double) i(cardViewDelegate)), (int) Math.ceil((double) d(cardViewDelegate)));
        cardViewDelegate.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    public float i(CardViewDelegate cardViewDelegate) {
        return j(cardViewDelegate).e();
    }

    public void a(CardViewDelegate cardViewDelegate, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        RoundRectDrawableWithShadow a2 = a(context, colorStateList, f, f2, f3);
        a2.a(cardViewDelegate.c());
        cardViewDelegate.a(a2);
        h(cardViewDelegate);
    }

    public float b(CardViewDelegate cardViewDelegate) {
        return j(cardViewDelegate).c();
    }

    public void c(CardViewDelegate cardViewDelegate, float f) {
        j(cardViewDelegate).b(f);
        h(cardViewDelegate);
    }

    private RoundRectDrawableWithShadow a(Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        return new RoundRectDrawableWithShadow(context.getResources(), colorStateList, f, f2, f3);
    }

    public void a(CardViewDelegate cardViewDelegate, ColorStateList colorStateList) {
        j(cardViewDelegate).a(colorStateList);
    }

    public void a(CardViewDelegate cardViewDelegate, float f) {
        j(cardViewDelegate).a(f);
        h(cardViewDelegate);
    }

    public float a(CardViewDelegate cardViewDelegate) {
        return j(cardViewDelegate).b();
    }
}
