package androidx.cardview.widget;

import android.graphics.drawable.Drawable;
import android.view.View;

interface CardViewDelegate {
    void a(int i, int i2);

    void a(int i, int i2, int i3, int i4);

    void a(Drawable drawable);

    boolean a();

    Drawable b();

    boolean c();

    View d();
}
