package androidx.recyclerview.widget;

public class BatchingListUpdateCallback implements ListUpdateCallback {

    /* renamed from: a  reason: collision with root package name */
    final ListUpdateCallback f933a;
    int b = 0;
    int c = -1;
    int d = -1;
    Object e = null;

    public BatchingListUpdateCallback(ListUpdateCallback listUpdateCallback) {
        this.f933a = listUpdateCallback;
    }

    public void a() {
        int i = this.b;
        if (i != 0) {
            if (i == 1) {
                this.f933a.a(this.c, this.d);
            } else if (i == 2) {
                this.f933a.b(this.c, this.d);
            } else if (i == 3) {
                this.f933a.a(this.c, this.d, this.e);
            }
            this.e = null;
            this.b = 0;
        }
    }

    public void b(int i, int i2) {
        int i3;
        if (this.b != 2 || (i3 = this.c) < i || i3 > i + i2) {
            a();
            this.c = i;
            this.d = i2;
            this.b = 2;
            return;
        }
        this.d += i2;
        this.c = i;
    }

    public void c(int i, int i2) {
        a();
        this.f933a.c(i, i2);
    }

    public void a(int i, int i2) {
        int i3;
        if (this.b == 1 && i >= (i3 = this.c)) {
            int i4 = this.d;
            if (i <= i3 + i4) {
                this.d = i4 + i2;
                this.c = Math.min(i, i3);
                return;
            }
        }
        a();
        this.c = i;
        this.d = i2;
        this.b = 1;
    }

    public void a(int i, int i2, Object obj) {
        int i3;
        if (this.b == 3) {
            int i4 = this.c;
            int i5 = this.d;
            if (i <= i4 + i5 && (i3 = i + i2) >= i4 && this.e == obj) {
                this.c = Math.min(i, i4);
                this.d = Math.max(i5 + i4, i3) - this.c;
                return;
            }
        }
        a();
        this.c = i;
        this.d = i2;
        this.e = obj;
        this.b = 3;
    }
}
