package androidx.recyclerview.widget;

import androidx.core.os.TraceCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.common.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

final class GapWorker implements Runnable {
    static final ThreadLocal<GapWorker> e = new ThreadLocal<>();
    static Comparator<Task> f = new Comparator<Task>() {
        /* renamed from: a */
        public int compare(Task task, Task task2) {
            if ((task.d == null) == (task2.d == null)) {
                boolean z = task.f958a;
                if (z == task2.f958a) {
                    int i = task2.b - task.b;
                    if (i != 0) {
                        return i;
                    }
                    int i2 = task.c - task2.c;
                    if (i2 != 0) {
                        return i2;
                    }
                    return 0;
                } else if (z) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (task.d == null) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    /* renamed from: a  reason: collision with root package name */
    ArrayList<RecyclerView> f956a = new ArrayList<>();
    long b;
    long c;
    private ArrayList<Task> d = new ArrayList<>();

    static class Task {

        /* renamed from: a  reason: collision with root package name */
        public boolean f958a;
        public int b;
        public int c;
        public RecyclerView d;
        public int e;

        Task() {
        }

        public void a() {
            this.f958a = false;
            this.b = 0;
            this.c = 0;
            this.d = null;
            this.e = 0;
        }
    }

    GapWorker() {
    }

    public void a(RecyclerView recyclerView) {
        this.f956a.add(recyclerView);
    }

    public void b(RecyclerView recyclerView) {
        this.f956a.remove(recyclerView);
    }

    public void run() {
        try {
            TraceCompat.a("RV Prefetch");
            if (!this.f956a.isEmpty()) {
                int size = this.f956a.size();
                long j = 0;
                for (int i = 0; i < size; i++) {
                    RecyclerView recyclerView = this.f956a.get(i);
                    if (recyclerView.getWindowVisibility() == 0) {
                        j = Math.max(recyclerView.getDrawingTime(), j);
                    }
                }
                if (j != 0) {
                    a(TimeUnit.MILLISECONDS.toNanos(j) + this.c);
                    this.b = 0;
                    TraceCompat.a();
                }
            }
        } finally {
            this.b = 0;
            TraceCompat.a();
        }
    }

    private void b(long j) {
        int i = 0;
        while (i < this.d.size()) {
            Task task = this.d.get(i);
            if (task.d != null) {
                a(task, j);
                task.a();
                i++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView recyclerView, int i, int i2) {
        if (recyclerView.isAttachedToWindow() && this.b == 0) {
            this.b = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.mPrefetchRegistry.b(i, i2);
    }

    private void a() {
        Task task;
        int size = this.f956a.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            RecyclerView recyclerView = this.f956a.get(i2);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.mPrefetchRegistry.a(recyclerView, false);
                i += recyclerView.mPrefetchRegistry.d;
            }
        }
        this.d.ensureCapacity(i);
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            RecyclerView recyclerView2 = this.f956a.get(i4);
            if (recyclerView2.getWindowVisibility() == 0) {
                LayoutPrefetchRegistryImpl layoutPrefetchRegistryImpl = recyclerView2.mPrefetchRegistry;
                int abs = Math.abs(layoutPrefetchRegistryImpl.f957a) + Math.abs(layoutPrefetchRegistryImpl.b);
                int i5 = i3;
                for (int i6 = 0; i6 < layoutPrefetchRegistryImpl.d * 2; i6 += 2) {
                    if (i5 >= this.d.size()) {
                        task = new Task();
                        this.d.add(task);
                    } else {
                        task = this.d.get(i5);
                    }
                    int i7 = layoutPrefetchRegistryImpl.c[i6 + 1];
                    task.f958a = i7 <= abs;
                    task.b = abs;
                    task.c = i7;
                    task.d = recyclerView2;
                    task.e = layoutPrefetchRegistryImpl.c[i6];
                    i5++;
                }
                i3 = i5;
            }
        }
        Collections.sort(this.d, f);
    }

    static class LayoutPrefetchRegistryImpl implements RecyclerView.LayoutManager.LayoutPrefetchRegistry {

        /* renamed from: a  reason: collision with root package name */
        int f957a;
        int b;
        int[] c;
        int d;

        LayoutPrefetchRegistryImpl() {
        }

        /* access modifiers changed from: package-private */
        public void a(RecyclerView recyclerView, boolean z) {
            this.d = 0;
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            RecyclerView.LayoutManager layoutManager = recyclerView.mLayout;
            if (recyclerView.mAdapter != null && layoutManager != null && layoutManager.isItemPrefetchEnabled()) {
                if (z) {
                    if (!recyclerView.mAdapterHelper.c()) {
                        layoutManager.collectInitialPrefetchPositions(recyclerView.mAdapter.getItemCount(), this);
                    }
                } else if (!recyclerView.hasPendingAdapterUpdates()) {
                    layoutManager.collectAdjacentPrefetchPositions(this.f957a, this.b, recyclerView.mState, this);
                }
                int i = this.d;
                if (i > layoutManager.mPrefetchMaxCountObserved) {
                    layoutManager.mPrefetchMaxCountObserved = i;
                    layoutManager.mPrefetchMaxObservedInInitialPrefetch = z;
                    recyclerView.mRecycler.j();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i, int i2) {
            this.f957a = i;
            this.b = i2;
        }

        public void a(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i2 >= 0) {
                int i3 = this.d * 2;
                int[] iArr = this.c;
                if (iArr == null) {
                    this.c = new int[4];
                    Arrays.fill(this.c, -1);
                } else if (i3 >= iArr.length) {
                    this.c = new int[(i3 * 2)];
                    System.arraycopy(iArr, 0, this.c, 0, iArr.length);
                }
                int[] iArr2 = this.c;
                iArr2[i3] = i;
                iArr2[i3 + 1] = i2;
                this.d++;
            } else {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i) {
            if (this.c != null) {
                int i2 = this.d * 2;
                for (int i3 = 0; i3 < i2; i3 += 2) {
                    if (this.c[i3] == i) {
                        return true;
                    }
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.d = 0;
        }
    }

    static boolean a(RecyclerView recyclerView, int i) {
        int b2 = recyclerView.mChildHelper.b();
        for (int i2 = 0; i2 < b2; i2++) {
            RecyclerView.ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(recyclerView.mChildHelper.d(i2));
            if (childViewHolderInt.mPosition == i && !childViewHolderInt.isInvalid()) {
                return true;
            }
        }
        return false;
    }

    private RecyclerView.ViewHolder a(RecyclerView recyclerView, int i, long j) {
        if (a(recyclerView, i)) {
            return null;
        }
        RecyclerView.Recycler recycler = recyclerView.mRecycler;
        try {
            recyclerView.onEnterLayoutOrScroll();
            RecyclerView.ViewHolder a2 = recycler.a(i, false, j);
            if (a2 != null) {
                if (!a2.isBound() || a2.isInvalid()) {
                    recycler.a(a2, false);
                } else {
                    recycler.b(a2.itemView);
                }
            }
            return a2;
        } finally {
            recyclerView.onExitLayoutOrScroll(false);
        }
    }

    private void a(RecyclerView recyclerView, long j) {
        if (recyclerView != null) {
            if (recyclerView.mDataSetHasChangedAfterLayout && recyclerView.mChildHelper.b() != 0) {
                recyclerView.removeAndRecycleViews();
            }
            LayoutPrefetchRegistryImpl layoutPrefetchRegistryImpl = recyclerView.mPrefetchRegistry;
            layoutPrefetchRegistryImpl.a(recyclerView, true);
            if (layoutPrefetchRegistryImpl.d != 0) {
                try {
                    TraceCompat.a("RV Nested Prefetch");
                    recyclerView.mState.a(recyclerView.mAdapter);
                    for (int i = 0; i < layoutPrefetchRegistryImpl.d * 2; i += 2) {
                        a(recyclerView, layoutPrefetchRegistryImpl.c[i], j);
                    }
                } finally {
                    TraceCompat.a();
                }
            }
        }
    }

    private void a(Task task, long j) {
        RecyclerView.ViewHolder a2 = a(task.d, task.e, task.f958a ? Clock.MAX_TIME : j);
        if (a2 != null && a2.mNestedRecyclerView != null && a2.isBound() && !a2.isInvalid()) {
            a((RecyclerView) a2.mNestedRecyclerView.get(), j);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        a();
        b(j);
    }
}
