package androidx.recyclerview.widget;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

public abstract class SortedListAdapterCallback<T2> extends SortedList.Callback<T2> {

    /* renamed from: a  reason: collision with root package name */
    final RecyclerView.Adapter f995a;

    public SortedListAdapterCallback(RecyclerView.Adapter adapter) {
        this.f995a = adapter;
    }

    public void a(int i, int i2) {
        this.f995a.notifyItemRangeInserted(i, i2);
    }

    public void b(int i, int i2) {
        this.f995a.notifyItemRangeRemoved(i, i2);
    }

    public void c(int i, int i2) {
        this.f995a.notifyItemMoved(i, i2);
    }

    public void d(int i, int i2) {
        this.f995a.notifyItemRangeChanged(i, i2);
    }

    public void a(int i, int i2, Object obj) {
        this.f995a.notifyItemRangeChanged(i, i2, obj);
    }
}
