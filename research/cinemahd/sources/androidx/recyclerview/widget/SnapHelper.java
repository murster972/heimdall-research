package androidx.recyclerview.widget;

import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;
import androidx.recyclerview.widget.RecyclerView;

public abstract class SnapHelper extends RecyclerView.OnFlingListener {

    /* renamed from: a  reason: collision with root package name */
    RecyclerView f990a;
    private final RecyclerView.OnScrollListener b = new RecyclerView.OnScrollListener() {

        /* renamed from: a  reason: collision with root package name */
        boolean f991a = false;

        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0 && this.f991a) {
                this.f991a = false;
                SnapHelper.this.a();
            }
        }

        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            if (i != 0 || i2 != 0) {
                this.f991a = true;
            }
        }
    };

    private void b() {
        this.f990a.removeOnScrollListener(this.b);
        this.f990a.setOnFlingListener((RecyclerView.OnFlingListener) null);
    }

    private void c() throws IllegalStateException {
        if (this.f990a.getOnFlingListener() == null) {
            this.f990a.addOnScrollListener(this.b);
            this.f990a.setOnFlingListener(this);
            return;
        }
        throw new IllegalStateException("An instance of OnFlingListener already set.");
    }

    public abstract int a(RecyclerView.LayoutManager layoutManager, int i, int i2);

    public boolean a(int i, int i2) {
        RecyclerView.LayoutManager layoutManager = this.f990a.getLayoutManager();
        if (layoutManager == null || this.f990a.getAdapter() == null) {
            return false;
        }
        int minFlingVelocity = this.f990a.getMinFlingVelocity();
        if ((Math.abs(i2) > minFlingVelocity || Math.abs(i) > minFlingVelocity) && b(layoutManager, i, i2)) {
            return true;
        }
        return false;
    }

    public abstract int[] a(RecyclerView.LayoutManager layoutManager, View view);

    public abstract View c(RecyclerView.LayoutManager layoutManager);

    private boolean b(RecyclerView.LayoutManager layoutManager, int i, int i2) {
        RecyclerView.SmoothScroller a2;
        int a3;
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider) || (a2 = a(layoutManager)) == null || (a3 = a(layoutManager, i, i2)) == -1) {
            return false;
        }
        a2.setTargetPosition(a3);
        layoutManager.startSmoothScroll(a2);
        return true;
    }

    public void a(RecyclerView recyclerView) throws IllegalStateException {
        RecyclerView recyclerView2 = this.f990a;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                b();
            }
            this.f990a = recyclerView;
            if (this.f990a != null) {
                c();
                new Scroller(this.f990a.getContext(), new DecelerateInterpolator());
                a();
            }
        }
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public LinearSmoothScroller b(RecyclerView.LayoutManager layoutManager) {
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return null;
        }
        return new LinearSmoothScroller(this.f990a.getContext()) {
            /* access modifiers changed from: protected */
            public float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return 100.0f / ((float) displayMetrics.densityDpi);
            }

            /* access modifiers changed from: protected */
            public void onTargetFound(View view, RecyclerView.State state, RecyclerView.SmoothScroller.Action action) {
                SnapHelper snapHelper = SnapHelper.this;
                RecyclerView recyclerView = snapHelper.f990a;
                if (recyclerView != null) {
                    int[] a2 = snapHelper.a(recyclerView.getLayoutManager(), view);
                    int i = a2[0];
                    int i2 = a2[1];
                    int calculateTimeForDeceleration = calculateTimeForDeceleration(Math.max(Math.abs(i), Math.abs(i2)));
                    if (calculateTimeForDeceleration > 0) {
                        action.a(i, i2, calculateTimeForDeceleration, this.mDecelerateInterpolator);
                    }
                }
            }
        };
    }

    /* access modifiers changed from: package-private */
    public void a() {
        RecyclerView.LayoutManager layoutManager;
        View c;
        RecyclerView recyclerView = this.f990a;
        if (recyclerView != null && (layoutManager = recyclerView.getLayoutManager()) != null && (c = c(layoutManager)) != null) {
            int[] a2 = a(layoutManager, c);
            if (a2[0] != 0 || a2[1] != 0) {
                this.f990a.smoothScrollBy(a2[0], a2[1]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public RecyclerView.SmoothScroller a(RecyclerView.LayoutManager layoutManager) {
        return b(layoutManager);
    }
}
