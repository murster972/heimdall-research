package androidx.recyclerview.widget;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

class ChildHelper {

    /* renamed from: a  reason: collision with root package name */
    final Callback f934a;
    final Bucket b = new Bucket();
    final List<View> c = new ArrayList();

    interface Callback {
        void a();

        void a(int i);

        void a(View view);

        void a(View view, int i);

        void a(View view, int i, ViewGroup.LayoutParams layoutParams);

        RecyclerView.ViewHolder b(View view);

        void b(int i);

        int c(View view);

        void d(View view);

        View getChildAt(int i);

        int getChildCount();
    }

    ChildHelper(Callback callback) {
        this.f934a = callback;
    }

    private int f(int i) {
        if (i < 0) {
            return -1;
        }
        int childCount = this.f934a.getChildCount();
        int i2 = i;
        while (i2 < childCount) {
            int b2 = i - (i2 - this.b.b(i2));
            if (b2 == 0) {
                while (this.b.c(i2)) {
                    i2++;
                }
                return i2;
            }
            i2 += b2;
        }
        return -1;
    }

    private void g(View view) {
        this.c.add(view);
        this.f934a.a(view);
    }

    private boolean h(View view) {
        if (!this.c.remove(view)) {
            return false;
        }
        this.f934a.d(view);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(View view, boolean z) {
        a(view, -1, z);
    }

    /* access modifiers changed from: package-private */
    public View b(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = this.c.get(i2);
            RecyclerView.ViewHolder b2 = this.f934a.b(view);
            if (b2.getLayoutPosition() == i && !b2.isInvalid() && !b2.isRemoved()) {
                return view;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public View c(int i) {
        return this.f934a.getChildAt(f(i));
    }

    /* access modifiers changed from: package-private */
    public void d(View view) {
        int c2 = this.f934a.c(view);
        if (c2 >= 0) {
            if (this.b.d(c2)) {
                h(view);
            }
            this.f934a.b(c2);
        }
    }

    /* access modifiers changed from: package-private */
    public void e(int i) {
        int f = f(i);
        View childAt = this.f934a.getChildAt(f);
        if (childAt != null) {
            if (this.b.d(f)) {
                h(childAt);
            }
            this.f934a.b(f);
        }
    }

    public String toString() {
        return this.b.toString() + ", hidden list:" + this.c.size();
    }

    static class Bucket {

        /* renamed from: a  reason: collision with root package name */
        long f935a = 0;
        Bucket b;

        Bucket() {
        }

        private void b() {
            if (this.b == null) {
                this.b = new Bucket();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            if (i >= 64) {
                Bucket bucket = this.b;
                if (bucket != null) {
                    bucket.a(i - 64);
                    return;
                }
                return;
            }
            this.f935a &= ~(1 << i);
        }

        /* access modifiers changed from: package-private */
        public boolean c(int i) {
            if (i < 64) {
                return (this.f935a & (1 << i)) != 0;
            }
            b();
            return this.b.c(i - 64);
        }

        /* access modifiers changed from: package-private */
        public boolean d(int i) {
            if (i >= 64) {
                b();
                return this.b.d(i - 64);
            }
            long j = 1 << i;
            boolean z = (this.f935a & j) != 0;
            this.f935a &= ~j;
            long j2 = j - 1;
            long j3 = this.f935a;
            this.f935a = Long.rotateRight(j3 & (~j2), 1) | (j3 & j2);
            Bucket bucket = this.b;
            if (bucket != null) {
                if (bucket.c(0)) {
                    e(63);
                }
                this.b.d(0);
            }
            return z;
        }

        /* access modifiers changed from: package-private */
        public void e(int i) {
            if (i >= 64) {
                b();
                this.b.e(i - 64);
                return;
            }
            this.f935a |= 1 << i;
        }

        public String toString() {
            if (this.b == null) {
                return Long.toBinaryString(this.f935a);
            }
            return this.b.toString() + "xx" + Long.toBinaryString(this.f935a);
        }

        /* access modifiers changed from: package-private */
        public int b(int i) {
            Bucket bucket = this.b;
            if (bucket == null) {
                if (i >= 64) {
                    return Long.bitCount(this.f935a);
                }
                return Long.bitCount(this.f935a & ((1 << i) - 1));
            } else if (i < 64) {
                return Long.bitCount(this.f935a & ((1 << i) - 1));
            } else {
                return bucket.b(i - 64) + Long.bitCount(this.f935a);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f935a = 0;
            Bucket bucket = this.b;
            if (bucket != null) {
                bucket.a();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i, boolean z) {
            if (i >= 64) {
                b();
                this.b.a(i - 64, z);
                return;
            }
            boolean z2 = (this.f935a & Long.MIN_VALUE) != 0;
            long j = (1 << i) - 1;
            long j2 = this.f935a;
            this.f935a = ((j2 & (~j)) << 1) | (j2 & j);
            if (z) {
                e(i);
            } else {
                a(i);
            }
            if (z2 || this.b != null) {
                b();
                this.b.a(0, z2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, int i, boolean z) {
        int i2;
        if (i < 0) {
            i2 = this.f934a.getChildCount();
        } else {
            i2 = f(i);
        }
        this.b.a(i2, z);
        if (z) {
            g(view);
        }
        this.f934a.a(view, i2);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.b.a();
        for (int size = this.c.size() - 1; size >= 0; size--) {
            this.f934a.d(this.c.get(size));
            this.c.remove(size);
        }
        this.f934a.a();
    }

    /* access modifiers changed from: package-private */
    public void f(View view) {
        int c2 = this.f934a.c(view);
        if (c2 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (this.b.c(c2)) {
            this.b.a(c2);
            h(view);
        } else {
            throw new RuntimeException("trying to unhide a view that was not hidden" + view);
        }
    }

    /* access modifiers changed from: package-private */
    public View d(int i) {
        return this.f934a.getChildAt(i);
    }

    /* access modifiers changed from: package-private */
    public boolean e(View view) {
        int c2 = this.f934a.c(view);
        if (c2 == -1) {
            h(view);
            return true;
        } else if (!this.b.c(c2)) {
            return false;
        } else {
            this.b.d(c2);
            h(view);
            this.f934a.b(c2);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        int i2;
        if (i < 0) {
            i2 = this.f934a.getChildCount();
        } else {
            i2 = f(i);
        }
        this.b.a(i2, z);
        if (z) {
            g(view);
        }
        this.f934a.a(view, i2, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.f934a.getChildCount();
    }

    /* access modifiers changed from: package-private */
    public int b(View view) {
        int c2 = this.f934a.c(view);
        if (c2 != -1 && !this.b.c(c2)) {
            return c2 - this.b.b(c2);
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public boolean c(View view) {
        return this.c.contains(view);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.f934a.getChildCount() - this.c.size();
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        int f = f(i);
        this.b.d(f);
        this.f934a.a(f);
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        int c2 = this.f934a.c(view);
        if (c2 >= 0) {
            this.b.e(c2);
            g(view);
            return;
        }
        throw new IllegalArgumentException("view is not a child, cannot hide " + view);
    }
}
