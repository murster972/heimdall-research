package androidx.recyclerview.widget;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public abstract class SimpleItemAnimator extends RecyclerView.ItemAnimator {
    boolean g = true;

    public boolean a(RecyclerView.ViewHolder viewHolder) {
        return !this.g || viewHolder.isInvalid();
    }

    public abstract boolean a(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4);

    public abstract boolean a(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4);

    public boolean b(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        int i = itemHolderInfo.f975a;
        int i2 = itemHolderInfo.b;
        View view = viewHolder.itemView;
        int left = itemHolderInfo2 == null ? view.getLeft() : itemHolderInfo2.f975a;
        int top = itemHolderInfo2 == null ? view.getTop() : itemHolderInfo2.b;
        if (viewHolder.isRemoved() || (i == left && i2 == top)) {
            return g(viewHolder);
        }
        view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
        return a(viewHolder, i, i2, left, top);
    }

    public void c(RecyclerView.ViewHolder viewHolder, boolean z) {
    }

    public boolean c(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        if (itemHolderInfo.f975a == itemHolderInfo2.f975a && itemHolderInfo.b == itemHolderInfo2.b) {
            j(viewHolder);
            return false;
        }
        return a(viewHolder, itemHolderInfo.f975a, itemHolderInfo.b, itemHolderInfo2.f975a, itemHolderInfo2.b);
    }

    public void d(RecyclerView.ViewHolder viewHolder, boolean z) {
    }

    public abstract boolean f(RecyclerView.ViewHolder viewHolder);

    public abstract boolean g(RecyclerView.ViewHolder viewHolder);

    public final void h(RecyclerView.ViewHolder viewHolder) {
        n(viewHolder);
        b(viewHolder);
    }

    public final void i(RecyclerView.ViewHolder viewHolder) {
        o(viewHolder);
    }

    public final void j(RecyclerView.ViewHolder viewHolder) {
        p(viewHolder);
        b(viewHolder);
    }

    public final void k(RecyclerView.ViewHolder viewHolder) {
        q(viewHolder);
    }

    public final void l(RecyclerView.ViewHolder viewHolder) {
        r(viewHolder);
        b(viewHolder);
    }

    public final void m(RecyclerView.ViewHolder viewHolder) {
        s(viewHolder);
    }

    public void n(RecyclerView.ViewHolder viewHolder) {
    }

    public void o(RecyclerView.ViewHolder viewHolder) {
    }

    public void p(RecyclerView.ViewHolder viewHolder) {
    }

    public void q(RecyclerView.ViewHolder viewHolder) {
    }

    public void r(RecyclerView.ViewHolder viewHolder) {
    }

    public void s(RecyclerView.ViewHolder viewHolder) {
    }

    public boolean a(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        if (itemHolderInfo == null || (itemHolderInfo.f975a == itemHolderInfo2.f975a && itemHolderInfo.b == itemHolderInfo2.b)) {
            return f(viewHolder);
        }
        return a(viewHolder, itemHolderInfo.f975a, itemHolderInfo.b, itemHolderInfo2.f975a, itemHolderInfo2.b);
    }

    public boolean a(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        int i;
        int i2;
        int i3 = itemHolderInfo.f975a;
        int i4 = itemHolderInfo.b;
        if (viewHolder2.shouldIgnore()) {
            int i5 = itemHolderInfo.f975a;
            i = itemHolderInfo.b;
            i2 = i5;
        } else {
            i2 = itemHolderInfo2.f975a;
            i = itemHolderInfo2.b;
        }
        return a(viewHolder, viewHolder2, i3, i4, i2, i);
    }

    public final void b(RecyclerView.ViewHolder viewHolder, boolean z) {
        d(viewHolder, z);
    }

    public final void a(RecyclerView.ViewHolder viewHolder, boolean z) {
        c(viewHolder, z);
        b(viewHolder);
    }
}
