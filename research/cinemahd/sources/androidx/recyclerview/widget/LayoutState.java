package androidx.recyclerview.widget;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

class LayoutState {

    /* renamed from: a  reason: collision with root package name */
    boolean f961a = true;
    int b;
    int c;
    int d;
    int e;
    int f = 0;
    int g = 0;
    boolean h;
    boolean i;

    LayoutState() {
    }

    /* access modifiers changed from: package-private */
    public boolean a(RecyclerView.State state) {
        int i2 = this.c;
        return i2 >= 0 && i2 < state.a();
    }

    public String toString() {
        return "LayoutState{mAvailable=" + this.b + ", mCurrentPosition=" + this.c + ", mItemDirection=" + this.d + ", mLayoutDirection=" + this.e + ", mStartLine=" + this.f + ", mEndLine=" + this.g + '}';
    }

    /* access modifiers changed from: package-private */
    public View a(RecyclerView.Recycler recycler) {
        View d2 = recycler.d(this.c);
        this.c += this.d;
        return d2;
    }
}
