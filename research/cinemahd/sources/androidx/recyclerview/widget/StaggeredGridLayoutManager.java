package androidx.recyclerview.widget;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class StaggeredGridLayoutManager extends RecyclerView.LayoutManager implements RecyclerView.SmoothScroller.ScrollVectorProvider {

    /* renamed from: a  reason: collision with root package name */
    private int f996a = -1;
    Span[] b;
    OrientationHelper c;
    OrientationHelper d;
    private int e;
    private int f;
    private final LayoutState g;
    boolean h = false;
    boolean i = false;
    private BitSet j;
    int k = -1;
    int l = Integer.MIN_VALUE;
    LazySpanLookup m = new LazySpanLookup();
    private int n = 2;
    private boolean o;
    private boolean p;
    private SavedState q;
    private int r;
    private final Rect s = new Rect();
    private final AnchorInfo t = new AnchorInfo();
    private boolean u = false;
    private boolean v = true;
    private int[] w;
    private final Runnable x = new Runnable() {
        public void run() {
            StaggeredGridLayoutManager.this.c();
        }
    };

    public static class LayoutParams extends RecyclerView.LayoutParams {
        Span e;
        boolean f;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public final int e() {
            Span span = this.e;
            if (span == null) {
                return -1;
            }
            return span.e;
        }

        public boolean f() {
            return this.f;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f1001a;
        int b;
        int c;
        int[] d;
        int e;
        int[] f;
        List<LazySpanLookup.FullSpanItem> g;
        boolean h;
        boolean i;
        boolean j;

        public SavedState() {
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.d = null;
            this.c = 0;
            this.f1001a = -1;
            this.b = -1;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.d = null;
            this.c = 0;
            this.e = 0;
            this.f = null;
            this.g = null;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeInt(this.f1001a);
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
            if (this.c > 0) {
                parcel.writeIntArray(this.d);
            }
            parcel.writeInt(this.e);
            if (this.e > 0) {
                parcel.writeIntArray(this.f);
            }
            parcel.writeInt(this.h ? 1 : 0);
            parcel.writeInt(this.i ? 1 : 0);
            parcel.writeInt(this.j ? 1 : 0);
            parcel.writeList(this.g);
        }

        SavedState(Parcel parcel) {
            this.f1001a = parcel.readInt();
            this.b = parcel.readInt();
            this.c = parcel.readInt();
            int i2 = this.c;
            if (i2 > 0) {
                this.d = new int[i2];
                parcel.readIntArray(this.d);
            }
            this.e = parcel.readInt();
            int i3 = this.e;
            if (i3 > 0) {
                this.f = new int[i3];
                parcel.readIntArray(this.f);
            }
            boolean z = false;
            this.h = parcel.readInt() == 1;
            this.i = parcel.readInt() == 1;
            this.j = parcel.readInt() == 1 ? true : z;
            this.g = parcel.readArrayList(LazySpanLookup.FullSpanItem.class.getClassLoader());
        }

        public SavedState(SavedState savedState) {
            this.c = savedState.c;
            this.f1001a = savedState.f1001a;
            this.b = savedState.b;
            this.d = savedState.d;
            this.e = savedState.e;
            this.f = savedState.f;
            this.h = savedState.h;
            this.i = savedState.i;
            this.j = savedState.j;
            this.g = savedState.g;
        }
    }

    class Span {

        /* renamed from: a  reason: collision with root package name */
        ArrayList<View> f1002a = new ArrayList<>();
        int b = Integer.MIN_VALUE;
        int c = Integer.MIN_VALUE;
        int d = 0;
        final int e;

        Span(int i) {
            this.e = i;
        }

        /* access modifiers changed from: package-private */
        public int a(int i) {
            int i2 = this.c;
            if (i2 != Integer.MIN_VALUE) {
                return i2;
            }
            if (this.f1002a.size() == 0) {
                return i;
            }
            a();
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public int b(int i) {
            int i2 = this.b;
            if (i2 != Integer.MIN_VALUE) {
                return i2;
            }
            if (this.f1002a.size() == 0) {
                return i;
            }
            b();
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public void c(View view) {
            LayoutParams b2 = b(view);
            b2.e = this;
            this.f1002a.add(0, view);
            this.b = Integer.MIN_VALUE;
            if (this.f1002a.size() == 1) {
                this.c = Integer.MIN_VALUE;
            }
            if (b2.c() || b2.b()) {
                this.d += StaggeredGridLayoutManager.this.c.b(view);
            }
        }

        /* access modifiers changed from: package-private */
        public void d(int i) {
            this.b = i;
            this.c = i;
        }

        public int e() {
            if (StaggeredGridLayoutManager.this.h) {
                return a(0, this.f1002a.size(), true);
            }
            return a(this.f1002a.size() - 1, -1, true);
        }

        public int f() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public int g() {
            int i = this.c;
            if (i != Integer.MIN_VALUE) {
                return i;
            }
            a();
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public int h() {
            int i = this.b;
            if (i != Integer.MIN_VALUE) {
                return i;
            }
            b();
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public void i() {
            this.b = Integer.MIN_VALUE;
            this.c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void j() {
            int size = this.f1002a.size();
            View remove = this.f1002a.remove(size - 1);
            LayoutParams b2 = b(remove);
            b2.e = null;
            if (b2.c() || b2.b()) {
                this.d -= StaggeredGridLayoutManager.this.c.b(remove);
            }
            if (size == 1) {
                this.b = Integer.MIN_VALUE;
            }
            this.c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void k() {
            View remove = this.f1002a.remove(0);
            LayoutParams b2 = b(remove);
            b2.e = null;
            if (this.f1002a.size() == 0) {
                this.c = Integer.MIN_VALUE;
            }
            if (b2.c() || b2.b()) {
                this.d -= StaggeredGridLayoutManager.this.c.b(remove);
            }
            this.b = Integer.MIN_VALUE;
        }

        public int d() {
            if (StaggeredGridLayoutManager.this.h) {
                return a(this.f1002a.size() - 1, -1, true);
            }
            return a(0, this.f1002a.size(), true);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            LazySpanLookup.FullSpanItem c2;
            ArrayList<View> arrayList = this.f1002a;
            View view = arrayList.get(arrayList.size() - 1);
            LayoutParams b2 = b(view);
            this.c = StaggeredGridLayoutManager.this.c.a(view);
            if (b2.f && (c2 = StaggeredGridLayoutManager.this.m.c(b2.a())) != null && c2.b == 1) {
                this.c += c2.a(this.e);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            LazySpanLookup.FullSpanItem c2;
            View view = this.f1002a.get(0);
            LayoutParams b2 = b(view);
            this.b = StaggeredGridLayoutManager.this.c.d(view);
            if (b2.f && (c2 = StaggeredGridLayoutManager.this.m.c(b2.a())) != null && c2.b == -1) {
                this.b -= c2.a(this.e);
            }
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.f1002a.clear();
            i();
            this.d = 0;
        }

        /* access modifiers changed from: package-private */
        public void c(int i) {
            int i2 = this.b;
            if (i2 != Integer.MIN_VALUE) {
                this.b = i2 + i;
            }
            int i3 = this.c;
            if (i3 != Integer.MIN_VALUE) {
                this.c = i3 + i;
            }
        }

        /* access modifiers changed from: package-private */
        public void a(View view) {
            LayoutParams b2 = b(view);
            b2.e = this;
            this.f1002a.add(view);
            this.c = Integer.MIN_VALUE;
            if (this.f1002a.size() == 1) {
                this.b = Integer.MIN_VALUE;
            }
            if (b2.c() || b2.b()) {
                this.d += StaggeredGridLayoutManager.this.c.b(view);
            }
        }

        /* access modifiers changed from: package-private */
        public LayoutParams b(View view) {
            return (LayoutParams) view.getLayoutParams();
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z, int i) {
            int i2;
            if (z) {
                i2 = a(Integer.MIN_VALUE);
            } else {
                i2 = b(Integer.MIN_VALUE);
            }
            c();
            if (i2 != Integer.MIN_VALUE) {
                if (z && i2 < StaggeredGridLayoutManager.this.c.b()) {
                    return;
                }
                if (z || i2 <= StaggeredGridLayoutManager.this.c.f()) {
                    if (i != Integer.MIN_VALUE) {
                        i2 += i;
                    }
                    this.c = i2;
                    this.b = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2, boolean z, boolean z2, boolean z3) {
            int f2 = StaggeredGridLayoutManager.this.c.f();
            int b2 = StaggeredGridLayoutManager.this.c.b();
            int i3 = i2 > i ? 1 : -1;
            while (i != i2) {
                View view = this.f1002a.get(i);
                int d2 = StaggeredGridLayoutManager.this.c.d(view);
                int a2 = StaggeredGridLayoutManager.this.c.a(view);
                boolean z4 = false;
                boolean z5 = !z3 ? d2 < b2 : d2 <= b2;
                if (!z3 ? a2 > f2 : a2 >= f2) {
                    z4 = true;
                }
                if (z5 && z4) {
                    if (!z || !z2) {
                        if (z2) {
                            return StaggeredGridLayoutManager.this.getPosition(view);
                        }
                        if (d2 < f2 || a2 > b2) {
                            return StaggeredGridLayoutManager.this.getPosition(view);
                        }
                    } else if (d2 >= f2 && a2 <= b2) {
                        return StaggeredGridLayoutManager.this.getPosition(view);
                    }
                }
                i += i3;
            }
            return -1;
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2, boolean z) {
            return a(i, i2, false, false, z);
        }

        public View a(int i, int i2) {
            View view = null;
            if (i2 != -1) {
                int size = this.f1002a.size() - 1;
                while (size >= 0) {
                    View view2 = this.f1002a.get(size);
                    StaggeredGridLayoutManager staggeredGridLayoutManager = StaggeredGridLayoutManager.this;
                    if (staggeredGridLayoutManager.h && staggeredGridLayoutManager.getPosition(view2) >= i) {
                        break;
                    }
                    StaggeredGridLayoutManager staggeredGridLayoutManager2 = StaggeredGridLayoutManager.this;
                    if ((!staggeredGridLayoutManager2.h && staggeredGridLayoutManager2.getPosition(view2) <= i) || !view2.hasFocusable()) {
                        break;
                    }
                    size--;
                    view = view2;
                }
            } else {
                int size2 = this.f1002a.size();
                int i3 = 0;
                while (i3 < size2) {
                    View view3 = this.f1002a.get(i3);
                    StaggeredGridLayoutManager staggeredGridLayoutManager3 = StaggeredGridLayoutManager.this;
                    if (staggeredGridLayoutManager3.h && staggeredGridLayoutManager3.getPosition(view3) <= i) {
                        break;
                    }
                    StaggeredGridLayoutManager staggeredGridLayoutManager4 = StaggeredGridLayoutManager.this;
                    if ((!staggeredGridLayoutManager4.h && staggeredGridLayoutManager4.getPosition(view3) >= i) || !view3.hasFocusable()) {
                        break;
                    }
                    i3++;
                    view = view3;
                }
            }
            return view;
        }
    }

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        RecyclerView.LayoutManager.Properties properties = RecyclerView.LayoutManager.getProperties(context, attributeSet, i2, i3);
        setOrientation(properties.f979a);
        c(properties.b);
        setReverseLayout(properties.c);
        this.g = new LayoutState();
        i();
    }

    private boolean a(Span span) {
        if (this.i) {
            if (span.g() < this.c.b()) {
                ArrayList<View> arrayList = span.f1002a;
                return !span.b(arrayList.get(arrayList.size() - 1)).f;
            }
        } else if (span.h() > this.c.f()) {
            return !span.b(span.f1002a.get(0)).f;
        }
        return false;
    }

    private int computeScrollExtent(RecyclerView.State state) {
        if (getChildCount() == 0) {
            return 0;
        }
        return ScrollbarHelper.a(state, this.c, b(!this.v), a(!this.v), this, this.v);
    }

    private int computeScrollOffset(RecyclerView.State state) {
        if (getChildCount() == 0) {
            return 0;
        }
        return ScrollbarHelper.a(state, this.c, b(!this.v), a(!this.v), this, this.v, this.i);
    }

    private int computeScrollRange(RecyclerView.State state) {
        if (getChildCount() == 0) {
            return 0;
        }
        return ScrollbarHelper.b(state, this.c, b(!this.v), a(!this.v), this, this.v);
    }

    private int convertFocusDirectionToLayoutDirection(int i2) {
        if (i2 == 1) {
            return (this.e != 1 && isLayoutRTL()) ? 1 : -1;
        }
        if (i2 == 2) {
            return (this.e != 1 && isLayoutRTL()) ? -1 : 1;
        }
        if (i2 != 17) {
            if (i2 != 33) {
                if (i2 != 66) {
                    return (i2 == 130 && this.e == 1) ? 1 : Integer.MIN_VALUE;
                }
                if (this.e == 0) {
                    return 1;
                }
                return Integer.MIN_VALUE;
            } else if (this.e == 1) {
                return -1;
            } else {
                return Integer.MIN_VALUE;
            }
        } else if (this.e == 0) {
            return -1;
        } else {
            return Integer.MIN_VALUE;
        }
    }

    private int e(int i2) {
        if (getChildCount() != 0) {
            if ((i2 < e()) != this.i) {
                return -1;
            }
            return 1;
        } else if (this.i) {
            return 1;
        } else {
            return -1;
        }
    }

    private LazySpanLookup.FullSpanItem f(int i2) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.c = new int[this.f996a];
        for (int i3 = 0; i3 < this.f996a; i3++) {
            fullSpanItem.c[i3] = i2 - this.b[i3].a(i2);
        }
        return fullSpanItem;
    }

    private void i() {
        this.c = OrientationHelper.a(this, this.e);
        this.d = OrientationHelper.a(this, 1 - this.e);
    }

    private void j() {
        if (this.d.d() != 1073741824) {
            int childCount = getChildCount();
            float f2 = 0.0f;
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = getChildAt(i2);
                float b2 = (float) this.d.b(childAt);
                if (b2 >= f2) {
                    if (((LayoutParams) childAt.getLayoutParams()).f()) {
                        b2 = (b2 * 1.0f) / ((float) this.f996a);
                    }
                    f2 = Math.max(f2, b2);
                }
            }
            int i3 = this.f;
            int round = Math.round(f2 * ((float) this.f996a));
            if (this.d.d() == Integer.MIN_VALUE) {
                round = Math.min(round, this.d.g());
            }
            d(round);
            if (this.f != i3) {
                for (int i4 = 0; i4 < childCount; i4++) {
                    View childAt2 = getChildAt(i4);
                    LayoutParams layoutParams = (LayoutParams) childAt2.getLayoutParams();
                    if (!layoutParams.f) {
                        if (!isLayoutRTL() || this.e != 1) {
                            int i5 = layoutParams.e.e;
                            int i6 = this.f * i5;
                            int i7 = i5 * i3;
                            if (this.e == 1) {
                                childAt2.offsetLeftAndRight(i6 - i7);
                            } else {
                                childAt2.offsetTopAndBottom(i6 - i7);
                            }
                        } else {
                            int i8 = this.f996a;
                            int i9 = layoutParams.e.e;
                            childAt2.offsetLeftAndRight(((-((i8 - 1) - i9)) * this.f) - ((-((i8 - 1) - i9)) * i3));
                        }
                    }
                }
            }
        }
    }

    private int k(int i2) {
        int b2 = this.b[0].b(i2);
        for (int i3 = 1; i3 < this.f996a; i3++) {
            int b3 = this.b[i3].b(i2);
            if (b3 > b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private int l(int i2) {
        int a2 = this.b[0].a(i2);
        for (int i3 = 1; i3 < this.f996a; i3++) {
            int a3 = this.b[i3].a(i2);
            if (a3 < a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    private int m(int i2) {
        int b2 = this.b[0].b(i2);
        for (int i3 = 1; i3 < this.f996a; i3++) {
            int b3 = this.b[i3].b(i2);
            if (b3 < b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private boolean n(int i2) {
        if (this.e == 0) {
            if ((i2 == -1) != this.i) {
                return true;
            }
            return false;
        }
        if (((i2 == -1) == this.i) == isLayoutRTL()) {
            return true;
        }
        return false;
    }

    private void o(int i2) {
        LayoutState layoutState = this.g;
        layoutState.e = i2;
        int i3 = 1;
        if (this.i != (i2 == -1)) {
            i3 = -1;
        }
        layoutState.d = i3;
    }

    private void resolveShouldLayoutReverse() {
        if (this.e == 1 || !isLayoutRTL()) {
            this.i = this.h;
        } else {
            this.i = !this.h;
        }
    }

    public void assertNotInLayoutOrScroll(String str) {
        if (this.q == null) {
            super.assertNotInLayoutOrScroll(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(RecyclerView.State state, AnchorInfo anchorInfo) {
        if (!a(state, anchorInfo) && !c(state, anchorInfo)) {
            anchorInfo.a();
            anchorInfo.f998a = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        int i2;
        int i3;
        if (getChildCount() == 0 || this.n == 0 || !isAttachedToWindow()) {
            return false;
        }
        if (this.i) {
            i3 = f();
            i2 = e();
        } else {
            i3 = e();
            i2 = f();
        }
        if (i3 == 0 && g() != null) {
            this.m.a();
            requestSimpleAnimationsInNextLayout();
            requestLayout();
            return true;
        } else if (!this.u) {
            return false;
        } else {
            int i4 = this.i ? -1 : 1;
            int i5 = i2 + 1;
            LazySpanLookup.FullSpanItem a2 = this.m.a(i3, i5, i4, true);
            if (a2 == null) {
                this.u = false;
                this.m.b(i5);
                return false;
            }
            LazySpanLookup.FullSpanItem a3 = this.m.a(i3, a2.f1000a, i4 * -1, true);
            if (a3 == null) {
                this.m.b(a2.f1000a);
            } else {
                this.m.b(a3.f1000a + 1);
            }
            requestSimpleAnimationsInNextLayout();
            requestLayout();
            return true;
        }
    }

    public boolean canScrollHorizontally() {
        return this.e == 0;
    }

    public boolean canScrollVertically() {
        return this.e == 1;
    }

    public boolean checkLayoutParams(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void collectAdjacentPrefetchPositions(int i2, int i3, RecyclerView.State state, RecyclerView.LayoutManager.LayoutPrefetchRegistry layoutPrefetchRegistry) {
        int i4;
        int i5;
        if (this.e != 0) {
            i2 = i3;
        }
        if (getChildCount() != 0 && i2 != 0) {
            a(i2, state);
            int[] iArr = this.w;
            if (iArr == null || iArr.length < this.f996a) {
                this.w = new int[this.f996a];
            }
            int i6 = 0;
            for (int i7 = 0; i7 < this.f996a; i7++) {
                LayoutState layoutState = this.g;
                if (layoutState.d == -1) {
                    i5 = layoutState.f;
                    i4 = this.b[i7].b(i5);
                } else {
                    i5 = this.b[i7].a(layoutState.g);
                    i4 = this.g.g;
                }
                int i8 = i5 - i4;
                if (i8 >= 0) {
                    this.w[i6] = i8;
                    i6++;
                }
            }
            Arrays.sort(this.w, 0, i6);
            for (int i9 = 0; i9 < i6 && this.g.a(state); i9++) {
                layoutPrefetchRegistry.a(this.g.c, this.w[i9]);
                LayoutState layoutState2 = this.g;
                layoutState2.c += layoutState2.d;
            }
        }
    }

    public int computeHorizontalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    public int computeHorizontalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    public int computeHorizontalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    public PointF computeScrollVectorForPosition(int i2) {
        int e2 = e(i2);
        PointF pointF = new PointF();
        if (e2 == 0) {
            return null;
        }
        if (this.e == 0) {
            pointF.x = (float) e2;
            pointF.y = 0.0f;
        } else {
            pointF.x = 0.0f;
            pointF.y = (float) e2;
        }
        return pointF;
    }

    public int computeVerticalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    public int computeVerticalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    public int computeVerticalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    /* access modifiers changed from: package-private */
    public void d(int i2) {
        this.f = i2 / this.f996a;
        this.r = View.MeasureSpec.makeMeasureSpec(i2, this.d.d());
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0074, code lost:
        if (r10 == r11) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0086, code lost:
        if (r10 == r11) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008a, code lost:
        r10 = false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View g() {
        /*
            r12 = this;
            int r0 = r12.getChildCount()
            r1 = 1
            int r0 = r0 - r1
            java.util.BitSet r2 = new java.util.BitSet
            int r3 = r12.f996a
            r2.<init>(r3)
            int r3 = r12.f996a
            r4 = 0
            r2.set(r4, r3, r1)
            int r3 = r12.e
            r5 = -1
            if (r3 != r1) goto L_0x0020
            boolean r3 = r12.isLayoutRTL()
            if (r3 == 0) goto L_0x0020
            r3 = 1
            goto L_0x0021
        L_0x0020:
            r3 = -1
        L_0x0021:
            boolean r6 = r12.i
            if (r6 == 0) goto L_0x0027
            r6 = -1
            goto L_0x002b
        L_0x0027:
            int r0 = r0 + 1
            r6 = r0
            r0 = 0
        L_0x002b:
            if (r0 >= r6) goto L_0x002e
            r5 = 1
        L_0x002e:
            if (r0 == r6) goto L_0x00ab
            android.view.View r7 = r12.getChildAt(r0)
            android.view.ViewGroup$LayoutParams r8 = r7.getLayoutParams()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LayoutParams r8 = (androidx.recyclerview.widget.StaggeredGridLayoutManager.LayoutParams) r8
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span r9 = r8.e
            int r9 = r9.e
            boolean r9 = r2.get(r9)
            if (r9 == 0) goto L_0x0054
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span r9 = r8.e
            boolean r9 = r12.a((androidx.recyclerview.widget.StaggeredGridLayoutManager.Span) r9)
            if (r9 == 0) goto L_0x004d
            return r7
        L_0x004d:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span r9 = r8.e
            int r9 = r9.e
            r2.clear(r9)
        L_0x0054:
            boolean r9 = r8.f
            if (r9 == 0) goto L_0x0059
            goto L_0x00a9
        L_0x0059:
            int r9 = r0 + r5
            if (r9 == r6) goto L_0x00a9
            android.view.View r9 = r12.getChildAt(r9)
            boolean r10 = r12.i
            if (r10 == 0) goto L_0x0077
            androidx.recyclerview.widget.OrientationHelper r10 = r12.c
            int r10 = r10.a((android.view.View) r7)
            androidx.recyclerview.widget.OrientationHelper r11 = r12.c
            int r11 = r11.a((android.view.View) r9)
            if (r10 >= r11) goto L_0x0074
            return r7
        L_0x0074:
            if (r10 != r11) goto L_0x008a
            goto L_0x0088
        L_0x0077:
            androidx.recyclerview.widget.OrientationHelper r10 = r12.c
            int r10 = r10.d(r7)
            androidx.recyclerview.widget.OrientationHelper r11 = r12.c
            int r11 = r11.d(r9)
            if (r10 <= r11) goto L_0x0086
            return r7
        L_0x0086:
            if (r10 != r11) goto L_0x008a
        L_0x0088:
            r10 = 1
            goto L_0x008b
        L_0x008a:
            r10 = 0
        L_0x008b:
            if (r10 == 0) goto L_0x00a9
            android.view.ViewGroup$LayoutParams r9 = r9.getLayoutParams()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LayoutParams r9 = (androidx.recyclerview.widget.StaggeredGridLayoutManager.LayoutParams) r9
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span r8 = r8.e
            int r8 = r8.e
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span r9 = r9.e
            int r9 = r9.e
            int r8 = r8 - r9
            if (r8 >= 0) goto L_0x00a0
            r8 = 1
            goto L_0x00a1
        L_0x00a0:
            r8 = 0
        L_0x00a1:
            if (r3 >= 0) goto L_0x00a5
            r9 = 1
            goto L_0x00a6
        L_0x00a5:
            r9 = 0
        L_0x00a6:
            if (r8 == r9) goto L_0x00a9
            return r7
        L_0x00a9:
            int r0 = r0 + r5
            goto L_0x002e
        L_0x00ab:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.g():android.view.View");
    }

    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        if (this.e == 0) {
            return new LayoutParams(-2, -1);
        }
        return new LayoutParams(-1, -2);
    }

    public RecyclerView.LayoutParams generateLayoutParams(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    public int getColumnCountForAccessibility(RecyclerView.Recycler recycler, RecyclerView.State state) {
        if (this.e == 1) {
            return this.f996a;
        }
        return super.getColumnCountForAccessibility(recycler, state);
    }

    public int getRowCountForAccessibility(RecyclerView.Recycler recycler, RecyclerView.State state) {
        if (this.e == 0) {
            return this.f996a;
        }
        return super.getRowCountForAccessibility(recycler, state);
    }

    public void h() {
        this.m.a();
        requestLayout();
    }

    public boolean isAutoMeasureEnabled() {
        return this.n != 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isLayoutRTL() {
        return getLayoutDirection() == 1;
    }

    public void offsetChildrenHorizontal(int i2) {
        super.offsetChildrenHorizontal(i2);
        for (int i3 = 0; i3 < this.f996a; i3++) {
            this.b[i3].c(i2);
        }
    }

    public void offsetChildrenVertical(int i2) {
        super.offsetChildrenVertical(i2);
        for (int i3 = 0; i3 < this.f996a; i3++) {
            this.b[i3].c(i2);
        }
    }

    public void onDetachedFromWindow(RecyclerView recyclerView, RecyclerView.Recycler recycler) {
        super.onDetachedFromWindow(recyclerView, recycler);
        removeCallbacks(this.x);
        for (int i2 = 0; i2 < this.f996a; i2++) {
            this.b[i2].c();
        }
        recyclerView.requestLayout();
    }

    public View onFocusSearchFailed(View view, int i2, RecyclerView.Recycler recycler, RecyclerView.State state) {
        View findContainingItemView;
        int i3;
        int i4;
        int i5;
        int i6;
        View a2;
        if (getChildCount() == 0 || (findContainingItemView = findContainingItemView(view)) == null) {
            return null;
        }
        resolveShouldLayoutReverse();
        int convertFocusDirectionToLayoutDirection = convertFocusDirectionToLayoutDirection(i2);
        if (convertFocusDirectionToLayoutDirection == Integer.MIN_VALUE) {
            return null;
        }
        LayoutParams layoutParams = (LayoutParams) findContainingItemView.getLayoutParams();
        boolean z = layoutParams.f;
        Span span = layoutParams.e;
        if (convertFocusDirectionToLayoutDirection == 1) {
            i3 = f();
        } else {
            i3 = e();
        }
        b(i3, state);
        o(convertFocusDirectionToLayoutDirection);
        LayoutState layoutState = this.g;
        layoutState.c = layoutState.d + i3;
        layoutState.b = (int) (((float) this.c.g()) * 0.33333334f);
        LayoutState layoutState2 = this.g;
        layoutState2.h = true;
        layoutState2.f961a = false;
        a(recycler, layoutState2, state);
        this.o = this.i;
        if (!z && (a2 = span.a(i3, convertFocusDirectionToLayoutDirection)) != null && a2 != findContainingItemView) {
            return a2;
        }
        if (n(convertFocusDirectionToLayoutDirection)) {
            for (int i7 = this.f996a - 1; i7 >= 0; i7--) {
                View a3 = this.b[i7].a(i3, convertFocusDirectionToLayoutDirection);
                if (a3 != null && a3 != findContainingItemView) {
                    return a3;
                }
            }
        } else {
            for (int i8 = 0; i8 < this.f996a; i8++) {
                View a4 = this.b[i8].a(i3, convertFocusDirectionToLayoutDirection);
                if (a4 != null && a4 != findContainingItemView) {
                    return a4;
                }
            }
        }
        boolean z2 = (this.h ^ true) == (convertFocusDirectionToLayoutDirection == -1);
        if (!z) {
            if (z2) {
                i6 = span.d();
            } else {
                i6 = span.e();
            }
            View findViewByPosition = findViewByPosition(i6);
            if (!(findViewByPosition == null || findViewByPosition == findContainingItemView)) {
                return findViewByPosition;
            }
        }
        if (n(convertFocusDirectionToLayoutDirection)) {
            for (int i9 = this.f996a - 1; i9 >= 0; i9--) {
                if (i9 != span.e) {
                    if (z2) {
                        i5 = this.b[i9].d();
                    } else {
                        i5 = this.b[i9].e();
                    }
                    View findViewByPosition2 = findViewByPosition(i5);
                    if (!(findViewByPosition2 == null || findViewByPosition2 == findContainingItemView)) {
                        return findViewByPosition2;
                    }
                }
            }
        } else {
            for (int i10 = 0; i10 < this.f996a; i10++) {
                if (z2) {
                    i4 = this.b[i10].d();
                } else {
                    i4 = this.b[i10].e();
                }
                View findViewByPosition3 = findViewByPosition(i4);
                if (findViewByPosition3 != null && findViewByPosition3 != findContainingItemView) {
                    return findViewByPosition3;
                }
            }
        }
        return null;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        if (getChildCount() > 0) {
            View b2 = b(false);
            View a2 = a(false);
            if (b2 != null && a2 != null) {
                int position = getPosition(b2);
                int position2 = getPosition(a2);
                if (position < position2) {
                    accessibilityEvent.setFromIndex(position);
                    accessibilityEvent.setToIndex(position2);
                    return;
                }
                accessibilityEvent.setFromIndex(position2);
                accessibilityEvent.setToIndex(position);
            }
        }
    }

    public void onInitializeAccessibilityNodeInfoForItem(RecyclerView.Recycler recycler, RecyclerView.State state, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            super.onInitializeAccessibilityNodeInfoForItem(view, accessibilityNodeInfoCompat);
            return;
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        if (this.e == 0) {
            accessibilityNodeInfoCompat.b((Object) AccessibilityNodeInfoCompat.CollectionItemInfoCompat.a(layoutParams2.e(), layoutParams2.f ? this.f996a : 1, -1, -1, layoutParams2.f, false));
        } else {
            accessibilityNodeInfoCompat.b((Object) AccessibilityNodeInfoCompat.CollectionItemInfoCompat.a(-1, -1, layoutParams2.e(), layoutParams2.f ? this.f996a : 1, layoutParams2.f, false));
        }
    }

    public void onItemsAdded(RecyclerView recyclerView, int i2, int i3) {
        a(i2, i3, 1);
    }

    public void onItemsChanged(RecyclerView recyclerView) {
        this.m.a();
        requestLayout();
    }

    public void onItemsMoved(RecyclerView recyclerView, int i2, int i3, int i4) {
        a(i2, i3, 8);
    }

    public void onItemsRemoved(RecyclerView recyclerView, int i2, int i3) {
        a(i2, i3, 2);
    }

    public void onItemsUpdated(RecyclerView recyclerView, int i2, int i3, Object obj) {
        a(i2, i3, 4);
    }

    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        c(recycler, state, true);
    }

    public void onLayoutCompleted(RecyclerView.State state) {
        super.onLayoutCompleted(state);
        this.k = -1;
        this.l = Integer.MIN_VALUE;
        this.q = null;
        this.t.b();
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.q = (SavedState) parcelable;
            requestLayout();
        }
    }

    public Parcelable onSaveInstanceState() {
        int i2;
        int i3;
        int i4;
        int[] iArr;
        SavedState savedState = this.q;
        if (savedState != null) {
            return new SavedState(savedState);
        }
        SavedState savedState2 = new SavedState();
        savedState2.h = this.h;
        savedState2.i = this.o;
        savedState2.j = this.p;
        LazySpanLookup lazySpanLookup = this.m;
        if (lazySpanLookup == null || (iArr = lazySpanLookup.f999a) == null) {
            savedState2.e = 0;
        } else {
            savedState2.f = iArr;
            savedState2.e = savedState2.f.length;
            savedState2.g = lazySpanLookup.b;
        }
        if (getChildCount() > 0) {
            if (this.o) {
                i2 = f();
            } else {
                i2 = e();
            }
            savedState2.f1001a = i2;
            savedState2.b = d();
            int i5 = this.f996a;
            savedState2.c = i5;
            savedState2.d = new int[i5];
            for (int i6 = 0; i6 < this.f996a; i6++) {
                if (this.o) {
                    i3 = this.b[i6].a(Integer.MIN_VALUE);
                    if (i3 != Integer.MIN_VALUE) {
                        i4 = this.c.b();
                    } else {
                        savedState2.d[i6] = i3;
                    }
                } else {
                    i3 = this.b[i6].b(Integer.MIN_VALUE);
                    if (i3 != Integer.MIN_VALUE) {
                        i4 = this.c.f();
                    } else {
                        savedState2.d[i6] = i3;
                    }
                }
                i3 -= i4;
                savedState2.d[i6] = i3;
            }
        } else {
            savedState2.f1001a = -1;
            savedState2.b = -1;
            savedState2.c = 0;
        }
        return savedState2;
    }

    public void onScrollStateChanged(int i2) {
        if (i2 == 0) {
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public int scrollBy(int i2, RecyclerView.Recycler recycler, RecyclerView.State state) {
        if (getChildCount() == 0 || i2 == 0) {
            return 0;
        }
        a(i2, state);
        int a2 = a(recycler, this.g, state);
        if (this.g.b >= a2) {
            i2 = i2 < 0 ? -a2 : a2;
        }
        this.c.a(-i2);
        this.o = this.i;
        LayoutState layoutState = this.g;
        layoutState.b = 0;
        a(recycler, layoutState);
        return i2;
    }

    public int scrollHorizontallyBy(int i2, RecyclerView.Recycler recycler, RecyclerView.State state) {
        return scrollBy(i2, recycler, state);
    }

    public void scrollToPosition(int i2) {
        SavedState savedState = this.q;
        if (!(savedState == null || savedState.f1001a == i2)) {
            savedState.a();
        }
        this.k = i2;
        this.l = Integer.MIN_VALUE;
        requestLayout();
    }

    public int scrollVerticallyBy(int i2, RecyclerView.Recycler recycler, RecyclerView.State state) {
        return scrollBy(i2, recycler, state);
    }

    public void setMeasuredDimension(Rect rect, int i2, int i3) {
        int i4;
        int i5;
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        if (this.e == 1) {
            i5 = RecyclerView.LayoutManager.chooseSize(i3, rect.height() + paddingTop, getMinimumHeight());
            i4 = RecyclerView.LayoutManager.chooseSize(i2, (this.f * this.f996a) + paddingLeft, getMinimumWidth());
        } else {
            i4 = RecyclerView.LayoutManager.chooseSize(i2, rect.width() + paddingLeft, getMinimumWidth());
            i5 = RecyclerView.LayoutManager.chooseSize(i3, (this.f * this.f996a) + paddingTop, getMinimumHeight());
        }
        setMeasuredDimension(i4, i5);
    }

    public void setOrientation(int i2) {
        if (i2 == 0 || i2 == 1) {
            assertNotInLayoutOrScroll((String) null);
            if (i2 != this.e) {
                this.e = i2;
                OrientationHelper orientationHelper = this.c;
                this.c = this.d;
                this.d = orientationHelper;
                requestLayout();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation.");
    }

    public void setReverseLayout(boolean z) {
        assertNotInLayoutOrScroll((String) null);
        SavedState savedState = this.q;
        if (!(savedState == null || savedState.h == z)) {
            savedState.h = z;
        }
        this.h = z;
        requestLayout();
    }

    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int i2) {
        LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext());
        linearSmoothScroller.setTargetPosition(i2);
        startSmoothScroll(linearSmoothScroller);
    }

    public boolean supportsPredictiveItemAnimations() {
        return this.q == null;
    }

    static class LazySpanLookup {

        /* renamed from: a  reason: collision with root package name */
        int[] f999a;
        List<FullSpanItem> b;

        LazySpanLookup() {
        }

        private void c(int i, int i2) {
            List<FullSpanItem> list = this.b;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.b.get(size);
                    int i3 = fullSpanItem.f1000a;
                    if (i3 >= i) {
                        fullSpanItem.f1000a = i3 + i2;
                    }
                }
            }
        }

        private int g(int i) {
            if (this.b == null) {
                return -1;
            }
            FullSpanItem c = c(i);
            if (c != null) {
                this.b.remove(c);
            }
            int size = this.b.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i2 = -1;
                    break;
                } else if (this.b.get(i2).f1000a >= i) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 == -1) {
                return -1;
            }
            this.b.remove(i2);
            return this.b.get(i2).f1000a;
        }

        /* access modifiers changed from: package-private */
        public void a(int i, Span span) {
            a(i);
            this.f999a[i] = span.e;
        }

        /* access modifiers changed from: package-private */
        public int b(int i) {
            List<FullSpanItem> list = this.b;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    if (this.b.get(size).f1000a >= i) {
                        this.b.remove(size);
                    }
                }
            }
            return e(i);
        }

        /* access modifiers changed from: package-private */
        public int d(int i) {
            int[] iArr = this.f999a;
            if (iArr == null || i >= iArr.length) {
                return -1;
            }
            return iArr[i];
        }

        /* access modifiers changed from: package-private */
        public int e(int i) {
            int[] iArr = this.f999a;
            if (iArr == null || i >= iArr.length) {
                return -1;
            }
            int g = g(i);
            if (g == -1) {
                int[] iArr2 = this.f999a;
                Arrays.fill(iArr2, i, iArr2.length, -1);
                return this.f999a.length;
            }
            int i2 = g + 1;
            Arrays.fill(this.f999a, i, i2, -1);
            return i2;
        }

        /* access modifiers changed from: package-private */
        public int f(int i) {
            int length = this.f999a.length;
            while (length <= i) {
                length *= 2;
            }
            return length;
        }

        private void d(int i, int i2) {
            List<FullSpanItem> list = this.b;
            if (list != null) {
                int i3 = i + i2;
                for (int size = list.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.b.get(size);
                    int i4 = fullSpanItem.f1000a;
                    if (i4 >= i) {
                        if (i4 < i3) {
                            this.b.remove(size);
                        } else {
                            fullSpanItem.f1000a = i4 - i2;
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            int[] iArr = this.f999a;
            if (iArr == null) {
                this.f999a = new int[(Math.max(i, 10) + 1)];
                Arrays.fill(this.f999a, -1);
            } else if (i >= iArr.length) {
                this.f999a = new int[f(i)];
                System.arraycopy(iArr, 0, this.f999a, 0, iArr.length);
                int[] iArr2 = this.f999a;
                Arrays.fill(iArr2, iArr.length, iArr2.length, -1);
            }
        }

        public FullSpanItem c(int i) {
            List<FullSpanItem> list = this.b;
            if (list == null) {
                return null;
            }
            for (int size = list.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = this.b.get(size);
                if (fullSpanItem.f1000a == i) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        static class FullSpanItem implements Parcelable {
            public static final Parcelable.Creator<FullSpanItem> CREATOR = new Parcelable.Creator<FullSpanItem>() {
                public FullSpanItem createFromParcel(Parcel parcel) {
                    return new FullSpanItem(parcel);
                }

                public FullSpanItem[] newArray(int i) {
                    return new FullSpanItem[i];
                }
            };

            /* renamed from: a  reason: collision with root package name */
            int f1000a;
            int b;
            int[] c;
            boolean d;

            FullSpanItem(Parcel parcel) {
                this.f1000a = parcel.readInt();
                this.b = parcel.readInt();
                this.d = parcel.readInt() != 1 ? false : true;
                int readInt = parcel.readInt();
                if (readInt > 0) {
                    this.c = new int[readInt];
                    parcel.readIntArray(this.c);
                }
            }

            /* access modifiers changed from: package-private */
            public int a(int i) {
                int[] iArr = this.c;
                if (iArr == null) {
                    return 0;
                }
                return iArr[i];
            }

            public int describeContents() {
                return 0;
            }

            public String toString() {
                return "FullSpanItem{mPosition=" + this.f1000a + ", mGapDir=" + this.b + ", mHasUnwantedGapAfter=" + this.d + ", mGapPerSpan=" + Arrays.toString(this.c) + '}';
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f1000a);
                parcel.writeInt(this.b);
                parcel.writeInt(this.d ? 1 : 0);
                int[] iArr = this.c;
                if (iArr == null || iArr.length <= 0) {
                    parcel.writeInt(0);
                    return;
                }
                parcel.writeInt(iArr.length);
                parcel.writeIntArray(this.c);
            }

            FullSpanItem() {
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i, int i2) {
            int[] iArr = this.f999a;
            if (iArr != null && i < iArr.length) {
                int i3 = i + i2;
                a(i3);
                int[] iArr2 = this.f999a;
                System.arraycopy(iArr2, i3, iArr2, i, (iArr2.length - i) - i2);
                int[] iArr3 = this.f999a;
                Arrays.fill(iArr3, iArr3.length - i2, iArr3.length, -1);
                d(i, i2);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            int[] iArr = this.f999a;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.b = null;
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2) {
            int[] iArr = this.f999a;
            if (iArr != null && i < iArr.length) {
                int i3 = i + i2;
                a(i3);
                int[] iArr2 = this.f999a;
                System.arraycopy(iArr2, i, iArr2, i3, (iArr2.length - i) - i2);
                Arrays.fill(this.f999a, i, i3, -1);
                c(i, i2);
            }
        }

        public void a(FullSpanItem fullSpanItem) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                FullSpanItem fullSpanItem2 = this.b.get(i);
                if (fullSpanItem2.f1000a == fullSpanItem.f1000a) {
                    this.b.remove(i);
                }
                if (fullSpanItem2.f1000a >= fullSpanItem.f1000a) {
                    this.b.add(i, fullSpanItem);
                    return;
                }
            }
            this.b.add(fullSpanItem);
        }

        public FullSpanItem a(int i, int i2, int i3, boolean z) {
            List<FullSpanItem> list = this.b;
            if (list == null) {
                return null;
            }
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                FullSpanItem fullSpanItem = this.b.get(i4);
                int i5 = fullSpanItem.f1000a;
                if (i5 >= i2) {
                    return null;
                }
                if (i5 >= i && (i3 == 0 || fullSpanItem.b == i3 || (z && fullSpanItem.d))) {
                    return fullSpanItem;
                }
            }
            return null;
        }
    }

    public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    private int h(int i2) {
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            int position = getPosition(getChildAt(i3));
            if (position >= 0 && position < i2) {
                return position;
            }
        }
        return 0;
    }

    class AnchorInfo {

        /* renamed from: a  reason: collision with root package name */
        int f998a;
        int b;
        boolean c;
        boolean d;
        boolean e;
        int[] f;

        AnchorInfo() {
            b();
        }

        /* access modifiers changed from: package-private */
        public void a(Span[] spanArr) {
            int length = spanArr.length;
            int[] iArr = this.f;
            if (iArr == null || iArr.length < length) {
                this.f = new int[StaggeredGridLayoutManager.this.b.length];
            }
            for (int i = 0; i < length; i++) {
                this.f[i] = spanArr[i].b(Integer.MIN_VALUE);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f998a = -1;
            this.b = Integer.MIN_VALUE;
            this.c = false;
            this.d = false;
            this.e = false;
            int[] iArr = this.f;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            int i;
            if (this.c) {
                i = StaggeredGridLayoutManager.this.c.b();
            } else {
                i = StaggeredGridLayoutManager.this.c.f();
            }
            this.b = i;
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            if (this.c) {
                this.b = StaggeredGridLayoutManager.this.c.b() - i;
            } else {
                this.b = StaggeredGridLayoutManager.this.c.f() + i;
            }
        }
    }

    private int i(int i2) {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            int position = getPosition(getChildAt(childCount));
            if (position >= 0 && position < i2) {
                return position;
            }
        }
        return 0;
    }

    private int b(int i2, int i3, int i4) {
        if (i3 == 0 && i4 == 0) {
            return i2;
        }
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            return View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i2) - i3) - i4), mode);
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        View view;
        if (this.i) {
            view = a(true);
        } else {
            view = b(true);
        }
        if (view == null) {
            return -1;
        }
        return getPosition(view);
    }

    /* access modifiers changed from: package-private */
    public int e() {
        if (getChildCount() == 0) {
            return 0;
        }
        return getPosition(getChildAt(0));
    }

    /* access modifiers changed from: package-private */
    public int f() {
        int childCount = getChildCount();
        if (childCount == 0) {
            return 0;
        }
        return getPosition(getChildAt(childCount - 1));
    }

    /* access modifiers changed from: package-private */
    public View b(boolean z) {
        int f2 = this.c.f();
        int b2 = this.c.b();
        int childCount = getChildCount();
        View view = null;
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            int d2 = this.c.d(childAt);
            if (this.c.a(childAt) > f2 && d2 < b2) {
                if (d2 >= f2 || !z) {
                    return childAt;
                }
                if (view == null) {
                    view = childAt;
                }
            }
        }
        return view;
    }

    private void a(AnchorInfo anchorInfo) {
        int i2;
        SavedState savedState = this.q;
        int i3 = savedState.c;
        if (i3 > 0) {
            if (i3 == this.f996a) {
                for (int i4 = 0; i4 < this.f996a; i4++) {
                    this.b[i4].c();
                    SavedState savedState2 = this.q;
                    int i5 = savedState2.d[i4];
                    if (i5 != Integer.MIN_VALUE) {
                        if (savedState2.i) {
                            i2 = this.c.b();
                        } else {
                            i2 = this.c.f();
                        }
                        i5 += i2;
                    }
                    this.b[i4].d(i5);
                }
            } else {
                savedState.b();
                SavedState savedState3 = this.q;
                savedState3.f1001a = savedState3.b;
            }
        }
        SavedState savedState4 = this.q;
        this.p = savedState4.j;
        setReverseLayout(savedState4.h);
        resolveShouldLayoutReverse();
        SavedState savedState5 = this.q;
        int i6 = savedState5.f1001a;
        if (i6 != -1) {
            this.k = i6;
            anchorInfo.c = savedState5.i;
        } else {
            anchorInfo.c = this.i;
        }
        SavedState savedState6 = this.q;
        if (savedState6.e > 1) {
            LazySpanLookup lazySpanLookup = this.m;
            lazySpanLookup.f999a = savedState6.f;
            lazySpanLookup.b = savedState6.g;
        }
    }

    private void b(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        int f2;
        int m2 = m(Integer.MAX_VALUE);
        if (m2 != Integer.MAX_VALUE && (f2 = m2 - this.c.f()) > 0) {
            int scrollBy = f2 - scrollBy(f2, recycler, state);
            if (z && scrollBy > 0) {
                this.c.a(-scrollBy);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r5, androidx.recyclerview.widget.RecyclerView.State r6) {
        /*
            r4 = this;
            androidx.recyclerview.widget.LayoutState r0 = r4.g
            r1 = 0
            r0.b = r1
            r0.c = r5
            boolean r0 = r4.isSmoothScrolling()
            r2 = 1
            if (r0 == 0) goto L_0x002e
            int r6 = r6.b()
            r0 = -1
            if (r6 == r0) goto L_0x002e
            boolean r0 = r4.i
            if (r6 >= r5) goto L_0x001b
            r5 = 1
            goto L_0x001c
        L_0x001b:
            r5 = 0
        L_0x001c:
            if (r0 != r5) goto L_0x0025
            androidx.recyclerview.widget.OrientationHelper r5 = r4.c
            int r5 = r5.g()
            goto L_0x002f
        L_0x0025:
            androidx.recyclerview.widget.OrientationHelper r5 = r4.c
            int r5 = r5.g()
            r6 = r5
            r5 = 0
            goto L_0x0030
        L_0x002e:
            r5 = 0
        L_0x002f:
            r6 = 0
        L_0x0030:
            boolean r0 = r4.getClipToPadding()
            if (r0 == 0) goto L_0x004d
            androidx.recyclerview.widget.LayoutState r0 = r4.g
            androidx.recyclerview.widget.OrientationHelper r3 = r4.c
            int r3 = r3.f()
            int r3 = r3 - r6
            r0.f = r3
            androidx.recyclerview.widget.LayoutState r6 = r4.g
            androidx.recyclerview.widget.OrientationHelper r0 = r4.c
            int r0 = r0.b()
            int r0 = r0 + r5
            r6.g = r0
            goto L_0x005d
        L_0x004d:
            androidx.recyclerview.widget.LayoutState r0 = r4.g
            androidx.recyclerview.widget.OrientationHelper r3 = r4.c
            int r3 = r3.a()
            int r3 = r3 + r5
            r0.g = r3
            androidx.recyclerview.widget.LayoutState r5 = r4.g
            int r6 = -r6
            r5.f = r6
        L_0x005d:
            androidx.recyclerview.widget.LayoutState r5 = r4.g
            r5.h = r1
            r5.f961a = r2
            androidx.recyclerview.widget.OrientationHelper r6 = r4.c
            int r6 = r6.d()
            if (r6 != 0) goto L_0x0074
            androidx.recyclerview.widget.OrientationHelper r6 = r4.c
            int r6 = r6.a()
            if (r6 != 0) goto L_0x0074
            r1 = 1
        L_0x0074:
            r5.i = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$State):void");
    }

    private LazySpanLookup.FullSpanItem g(int i2) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.c = new int[this.f996a];
        for (int i3 = 0; i3 < this.f996a; i3++) {
            fullSpanItem.c[i3] = this.b[i3].b(i2) - i2;
        }
        return fullSpanItem;
    }

    public void c(int i2) {
        assertNotInLayoutOrScroll((String) null);
        if (i2 != this.f996a) {
            h();
            this.f996a = i2;
            this.j = new BitSet(this.f996a);
            this.b = new Span[this.f996a];
            for (int i3 = 0; i3 < this.f996a; i3++) {
                this.b[i3] = new Span(i3);
            }
            requestLayout();
        }
    }

    private int j(int i2) {
        int a2 = this.b[0].a(i2);
        for (int i3 = 1; i3 < this.f996a; i3++) {
            int a3 = this.b[i3].a(i2);
            if (a3 > a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(RecyclerView.State state, AnchorInfo anchorInfo) {
        int i2;
        int i3;
        int i4;
        boolean z = false;
        if (!state.d() && (i2 = this.k) != -1) {
            if (i2 < 0 || i2 >= state.a()) {
                this.k = -1;
                this.l = Integer.MIN_VALUE;
            } else {
                SavedState savedState = this.q;
                if (savedState == null || savedState.f1001a == -1 || savedState.c < 1) {
                    View findViewByPosition = findViewByPosition(this.k);
                    if (findViewByPosition != null) {
                        if (this.i) {
                            i3 = f();
                        } else {
                            i3 = e();
                        }
                        anchorInfo.f998a = i3;
                        if (this.l != Integer.MIN_VALUE) {
                            if (anchorInfo.c) {
                                anchorInfo.b = (this.c.b() - this.l) - this.c.a(findViewByPosition);
                            } else {
                                anchorInfo.b = (this.c.f() + this.l) - this.c.d(findViewByPosition);
                            }
                            return true;
                        } else if (this.c.b(findViewByPosition) > this.c.g()) {
                            if (anchorInfo.c) {
                                i4 = this.c.b();
                            } else {
                                i4 = this.c.f();
                            }
                            anchorInfo.b = i4;
                            return true;
                        } else {
                            int d2 = this.c.d(findViewByPosition) - this.c.f();
                            if (d2 < 0) {
                                anchorInfo.b = -d2;
                                return true;
                            }
                            int b2 = this.c.b() - this.c.a(findViewByPosition);
                            if (b2 < 0) {
                                anchorInfo.b = b2;
                                return true;
                            }
                            anchorInfo.b = Integer.MIN_VALUE;
                        }
                    } else {
                        anchorInfo.f998a = this.k;
                        int i5 = this.l;
                        if (i5 == Integer.MIN_VALUE) {
                            if (e(anchorInfo.f998a) == 1) {
                                z = true;
                            }
                            anchorInfo.c = z;
                            anchorInfo.a();
                        } else {
                            anchorInfo.a(i5);
                        }
                        anchorInfo.d = true;
                    }
                } else {
                    anchorInfo.b = Integer.MIN_VALUE;
                    anchorInfo.f998a = this.k;
                }
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0157, code lost:
        if (c() != false) goto L_0x015b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c(androidx.recyclerview.widget.RecyclerView.Recycler r9, androidx.recyclerview.widget.RecyclerView.State r10, boolean r11) {
        /*
            r8 = this;
            androidx.recyclerview.widget.StaggeredGridLayoutManager$AnchorInfo r0 = r8.t
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r1 = r8.q
            r2 = -1
            if (r1 != 0) goto L_0x000b
            int r1 = r8.k
            if (r1 == r2) goto L_0x0018
        L_0x000b:
            int r1 = r10.a()
            if (r1 != 0) goto L_0x0018
            r8.removeAndRecycleAllViews(r9)
            r0.b()
            return
        L_0x0018:
            boolean r1 = r0.e
            r3 = 0
            r4 = 1
            if (r1 == 0) goto L_0x0029
            int r1 = r8.k
            if (r1 != r2) goto L_0x0029
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r1 = r8.q
            if (r1 == 0) goto L_0x0027
            goto L_0x0029
        L_0x0027:
            r1 = 0
            goto L_0x002a
        L_0x0029:
            r1 = 1
        L_0x002a:
            if (r1 == 0) goto L_0x0043
            r0.b()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r5 = r8.q
            if (r5 == 0) goto L_0x0037
            r8.a((androidx.recyclerview.widget.StaggeredGridLayoutManager.AnchorInfo) r0)
            goto L_0x003e
        L_0x0037:
            r8.resolveShouldLayoutReverse()
            boolean r5 = r8.i
            r0.c = r5
        L_0x003e:
            r8.b((androidx.recyclerview.widget.RecyclerView.State) r10, (androidx.recyclerview.widget.StaggeredGridLayoutManager.AnchorInfo) r0)
            r0.e = r4
        L_0x0043:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r5 = r8.q
            if (r5 != 0) goto L_0x0060
            int r5 = r8.k
            if (r5 != r2) goto L_0x0060
            boolean r5 = r0.c
            boolean r6 = r8.o
            if (r5 != r6) goto L_0x0059
            boolean r5 = r8.isLayoutRTL()
            boolean r6 = r8.p
            if (r5 == r6) goto L_0x0060
        L_0x0059:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r5 = r8.m
            r5.a()
            r0.d = r4
        L_0x0060:
            int r5 = r8.getChildCount()
            if (r5 <= 0) goto L_0x00c9
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r5 = r8.q
            if (r5 == 0) goto L_0x006e
            int r5 = r5.c
            if (r5 >= r4) goto L_0x00c9
        L_0x006e:
            boolean r5 = r0.d
            if (r5 == 0) goto L_0x008e
            r1 = 0
        L_0x0073:
            int r5 = r8.f996a
            if (r1 >= r5) goto L_0x00c9
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span[] r5 = r8.b
            r5 = r5[r1]
            r5.c()
            int r5 = r0.b
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r5 == r6) goto L_0x008b
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span[] r6 = r8.b
            r6 = r6[r1]
            r6.d(r5)
        L_0x008b:
            int r1 = r1 + 1
            goto L_0x0073
        L_0x008e:
            if (r1 != 0) goto L_0x00af
            androidx.recyclerview.widget.StaggeredGridLayoutManager$AnchorInfo r1 = r8.t
            int[] r1 = r1.f
            if (r1 != 0) goto L_0x0097
            goto L_0x00af
        L_0x0097:
            r1 = 0
        L_0x0098:
            int r5 = r8.f996a
            if (r1 >= r5) goto L_0x00c9
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span[] r5 = r8.b
            r5 = r5[r1]
            r5.c()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$AnchorInfo r6 = r8.t
            int[] r6 = r6.f
            r6 = r6[r1]
            r5.d(r6)
            int r1 = r1 + 1
            goto L_0x0098
        L_0x00af:
            r1 = 0
        L_0x00b0:
            int r5 = r8.f996a
            if (r1 >= r5) goto L_0x00c2
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span[] r5 = r8.b
            r5 = r5[r1]
            boolean r6 = r8.i
            int r7 = r0.b
            r5.a((boolean) r6, (int) r7)
            int r1 = r1 + 1
            goto L_0x00b0
        L_0x00c2:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$AnchorInfo r1 = r8.t
            androidx.recyclerview.widget.StaggeredGridLayoutManager$Span[] r5 = r8.b
            r1.a((androidx.recyclerview.widget.StaggeredGridLayoutManager.Span[]) r5)
        L_0x00c9:
            r8.detachAndScrapAttachedViews(r9)
            androidx.recyclerview.widget.LayoutState r1 = r8.g
            r1.f961a = r3
            r8.u = r3
            androidx.recyclerview.widget.OrientationHelper r1 = r8.d
            int r1 = r1.g()
            r8.d(r1)
            int r1 = r0.f998a
            r8.b((int) r1, (androidx.recyclerview.widget.RecyclerView.State) r10)
            boolean r1 = r0.c
            if (r1 == 0) goto L_0x00fc
            r8.o(r2)
            androidx.recyclerview.widget.LayoutState r1 = r8.g
            r8.a((androidx.recyclerview.widget.RecyclerView.Recycler) r9, (androidx.recyclerview.widget.LayoutState) r1, (androidx.recyclerview.widget.RecyclerView.State) r10)
            r8.o(r4)
            androidx.recyclerview.widget.LayoutState r1 = r8.g
            int r2 = r0.f998a
            int r5 = r1.d
            int r2 = r2 + r5
            r1.c = r2
            r8.a((androidx.recyclerview.widget.RecyclerView.Recycler) r9, (androidx.recyclerview.widget.LayoutState) r1, (androidx.recyclerview.widget.RecyclerView.State) r10)
            goto L_0x0113
        L_0x00fc:
            r8.o(r4)
            androidx.recyclerview.widget.LayoutState r1 = r8.g
            r8.a((androidx.recyclerview.widget.RecyclerView.Recycler) r9, (androidx.recyclerview.widget.LayoutState) r1, (androidx.recyclerview.widget.RecyclerView.State) r10)
            r8.o(r2)
            androidx.recyclerview.widget.LayoutState r1 = r8.g
            int r2 = r0.f998a
            int r5 = r1.d
            int r2 = r2 + r5
            r1.c = r2
            r8.a((androidx.recyclerview.widget.RecyclerView.Recycler) r9, (androidx.recyclerview.widget.LayoutState) r1, (androidx.recyclerview.widget.RecyclerView.State) r10)
        L_0x0113:
            r8.j()
            int r1 = r8.getChildCount()
            if (r1 <= 0) goto L_0x012d
            boolean r1 = r8.i
            if (r1 == 0) goto L_0x0127
            r8.a((androidx.recyclerview.widget.RecyclerView.Recycler) r9, (androidx.recyclerview.widget.RecyclerView.State) r10, (boolean) r4)
            r8.b((androidx.recyclerview.widget.RecyclerView.Recycler) r9, (androidx.recyclerview.widget.RecyclerView.State) r10, (boolean) r3)
            goto L_0x012d
        L_0x0127:
            r8.b((androidx.recyclerview.widget.RecyclerView.Recycler) r9, (androidx.recyclerview.widget.RecyclerView.State) r10, (boolean) r4)
            r8.a((androidx.recyclerview.widget.RecyclerView.Recycler) r9, (androidx.recyclerview.widget.RecyclerView.State) r10, (boolean) r3)
        L_0x012d:
            if (r11 == 0) goto L_0x015a
            boolean r11 = r10.d()
            if (r11 != 0) goto L_0x015a
            int r11 = r8.n
            if (r11 == 0) goto L_0x014b
            int r11 = r8.getChildCount()
            if (r11 <= 0) goto L_0x014b
            boolean r11 = r8.u
            if (r11 != 0) goto L_0x0149
            android.view.View r11 = r8.g()
            if (r11 == 0) goto L_0x014b
        L_0x0149:
            r11 = 1
            goto L_0x014c
        L_0x014b:
            r11 = 0
        L_0x014c:
            if (r11 == 0) goto L_0x015a
            java.lang.Runnable r11 = r8.x
            r8.removeCallbacks(r11)
            boolean r11 = r8.c()
            if (r11 == 0) goto L_0x015a
            goto L_0x015b
        L_0x015a:
            r4 = 0
        L_0x015b:
            boolean r11 = r10.d()
            if (r11 == 0) goto L_0x0166
            androidx.recyclerview.widget.StaggeredGridLayoutManager$AnchorInfo r11 = r8.t
            r11.b()
        L_0x0166:
            boolean r11 = r0.c
            r8.o = r11
            boolean r11 = r8.isLayoutRTL()
            r8.p = r11
            if (r4 == 0) goto L_0x017a
            androidx.recyclerview.widget.StaggeredGridLayoutManager$AnchorInfo r11 = r8.t
            r11.b()
            r8.c(r9, r10, r3)
        L_0x017a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.c(androidx.recyclerview.widget.RecyclerView$Recycler, androidx.recyclerview.widget.RecyclerView$State, boolean):void");
    }

    private void b(View view) {
        for (int i2 = this.f996a - 1; i2 >= 0; i2--) {
            this.b[i2].c(view);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        int b2 = this.b[0].b(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.f996a; i2++) {
            if (this.b[i2].b(Integer.MIN_VALUE) != b2) {
                return false;
            }
        }
        return true;
    }

    private void b(RecyclerView.Recycler recycler, int i2) {
        while (getChildCount() > 0) {
            View childAt = getChildAt(0);
            if (this.c.a(childAt) <= i2 && this.c.e(childAt) <= i2) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.f) {
                    int i3 = 0;
                    while (i3 < this.f996a) {
                        if (this.b[i3].f1002a.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.f996a; i4++) {
                        this.b[i4].k();
                    }
                } else if (layoutParams.e.f1002a.size() != 1) {
                    layoutParams.e.k();
                } else {
                    return;
                }
                removeAndRecycleView(childAt, recycler);
            } else {
                return;
            }
        }
    }

    private void a(View view, LayoutParams layoutParams, boolean z) {
        if (layoutParams.f) {
            if (this.e == 1) {
                a(view, this.r, RecyclerView.LayoutManager.getChildMeasureSpec(getHeight(), getHeightMode(), getPaddingTop() + getPaddingBottom(), layoutParams.height, true), z);
            } else {
                a(view, RecyclerView.LayoutManager.getChildMeasureSpec(getWidth(), getWidthMode(), getPaddingLeft() + getPaddingRight(), layoutParams.width, true), this.r, z);
            }
        } else if (this.e == 1) {
            a(view, RecyclerView.LayoutManager.getChildMeasureSpec(this.f, getWidthMode(), 0, layoutParams.width, false), RecyclerView.LayoutManager.getChildMeasureSpec(getHeight(), getHeightMode(), getPaddingTop() + getPaddingBottom(), layoutParams.height, true), z);
        } else {
            a(view, RecyclerView.LayoutManager.getChildMeasureSpec(getWidth(), getWidthMode(), getPaddingLeft() + getPaddingRight(), layoutParams.width, true), RecyclerView.LayoutManager.getChildMeasureSpec(this.f, getHeightMode(), 0, layoutParams.height, false), z);
        }
    }

    private void a(View view, int i2, int i3, boolean z) {
        boolean z2;
        calculateItemDecorationsForChild(view, this.s);
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int i4 = layoutParams.leftMargin;
        Rect rect = this.s;
        int b2 = b(i2, i4 + rect.left, layoutParams.rightMargin + rect.right);
        int i5 = layoutParams.topMargin;
        Rect rect2 = this.s;
        int b3 = b(i3, i5 + rect2.top, layoutParams.bottomMargin + rect2.bottom);
        if (z) {
            z2 = shouldReMeasureChild(view, b2, b3, layoutParams);
        } else {
            z2 = shouldMeasureChild(view, b2, b3, layoutParams);
        }
        if (z2) {
            view.measure(b2, b3);
        }
    }

    private boolean c(RecyclerView.State state, AnchorInfo anchorInfo) {
        int i2;
        if (this.o) {
            i2 = i(state.a());
        } else {
            i2 = h(state.a());
        }
        anchorInfo.f998a = i2;
        anchorInfo.b = Integer.MIN_VALUE;
        return true;
    }

    /* access modifiers changed from: package-private */
    public View a(boolean z) {
        int f2 = this.c.f();
        int b2 = this.c.b();
        View view = null;
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            int d2 = this.c.d(childAt);
            int a2 = this.c.a(childAt);
            if (a2 > f2 && d2 < b2) {
                if (a2 <= b2 || !z) {
                    return childAt;
                }
                if (view == null) {
                    view = childAt;
                }
            }
        }
        return view;
    }

    private void a(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        int b2;
        int j2 = j(Integer.MIN_VALUE);
        if (j2 != Integer.MIN_VALUE && (b2 = this.c.b() - j2) > 0) {
            int i2 = b2 - (-scrollBy(-b2, recycler, state));
            if (z && i2 > 0) {
                this.c.a(i2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r7, int r8, int r9) {
        /*
            r6 = this;
            boolean r0 = r6.i
            if (r0 == 0) goto L_0x0009
            int r0 = r6.f()
            goto L_0x000d
        L_0x0009:
            int r0 = r6.e()
        L_0x000d:
            r1 = 8
            if (r9 != r1) goto L_0x001b
            if (r7 >= r8) goto L_0x0016
            int r2 = r8 + 1
            goto L_0x001d
        L_0x0016:
            int r2 = r7 + 1
            r3 = r2
            r2 = r8
            goto L_0x001f
        L_0x001b:
            int r2 = r7 + r8
        L_0x001d:
            r3 = r2
            r2 = r7
        L_0x001f:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r4 = r6.m
            r4.e(r2)
            r4 = 1
            if (r9 == r4) goto L_0x003e
            r5 = 2
            if (r9 == r5) goto L_0x0038
            if (r9 == r1) goto L_0x002d
            goto L_0x0043
        L_0x002d:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r9 = r6.m
            r9.b(r7, r4)
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r7 = r6.m
            r7.a((int) r8, (int) r4)
            goto L_0x0043
        L_0x0038:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r9 = r6.m
            r9.b(r7, r8)
            goto L_0x0043
        L_0x003e:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r9 = r6.m
            r9.a((int) r7, (int) r8)
        L_0x0043:
            if (r3 > r0) goto L_0x0046
            return
        L_0x0046:
            boolean r7 = r6.i
            if (r7 == 0) goto L_0x004f
            int r7 = r6.e()
            goto L_0x0053
        L_0x004f:
            int r7 = r6.f()
        L_0x0053:
            if (r2 > r7) goto L_0x0058
            r6.requestLayout()
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.a(int, int, int):void");
    }

    /* JADX WARNING: type inference failed for: r9v0 */
    /* JADX WARNING: type inference failed for: r9v1, types: [boolean, int] */
    /* JADX WARNING: type inference failed for: r9v4 */
    private int a(RecyclerView.Recycler recycler, LayoutState layoutState, RecyclerView.State state) {
        int i2;
        int i3;
        int i4;
        Span span;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean z;
        int i11;
        int i12;
        int i13;
        RecyclerView.Recycler recycler2 = recycler;
        LayoutState layoutState2 = layoutState;
        ? r9 = 0;
        this.j.set(0, this.f996a, true);
        if (this.g.i) {
            i2 = layoutState2.e == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        } else {
            if (layoutState2.e == 1) {
                i13 = layoutState2.g + layoutState2.b;
            } else {
                i13 = layoutState2.f - layoutState2.b;
            }
            i2 = i13;
        }
        a(layoutState2.e, i2);
        if (this.i) {
            i3 = this.c.b();
        } else {
            i3 = this.c.f();
        }
        int i14 = i3;
        boolean z2 = false;
        while (layoutState.a(state) && (this.g.i || !this.j.isEmpty())) {
            View a2 = layoutState2.a(recycler2);
            LayoutParams layoutParams = (LayoutParams) a2.getLayoutParams();
            int a3 = layoutParams.a();
            int d2 = this.m.d(a3);
            boolean z3 = d2 == -1;
            if (z3) {
                span = layoutParams.f ? this.b[r9] : a(layoutState2);
                this.m.a(a3, span);
            } else {
                span = this.b[d2];
            }
            Span span2 = span;
            layoutParams.e = span2;
            if (layoutState2.e == 1) {
                addView(a2);
            } else {
                addView(a2, r9);
            }
            a(a2, layoutParams, (boolean) r9);
            if (layoutState2.e == 1) {
                if (layoutParams.f) {
                    i12 = j(i14);
                } else {
                    i12 = span2.a(i14);
                }
                int b2 = this.c.b(a2) + i12;
                if (z3 && layoutParams.f) {
                    LazySpanLookup.FullSpanItem f2 = f(i12);
                    f2.b = -1;
                    f2.f1000a = a3;
                    this.m.a(f2);
                }
                i5 = b2;
                i6 = i12;
            } else {
                if (layoutParams.f) {
                    i11 = m(i14);
                } else {
                    i11 = span2.b(i14);
                }
                i6 = i11 - this.c.b(a2);
                if (z3 && layoutParams.f) {
                    LazySpanLookup.FullSpanItem g2 = g(i11);
                    g2.b = 1;
                    g2.f1000a = a3;
                    this.m.a(g2);
                }
                i5 = i11;
            }
            if (layoutParams.f && layoutState2.d == -1) {
                if (z3) {
                    this.u = true;
                } else {
                    if (layoutState2.e == 1) {
                        z = a();
                    } else {
                        z = b();
                    }
                    if (!z) {
                        LazySpanLookup.FullSpanItem c2 = this.m.c(a3);
                        if (c2 != null) {
                            c2.d = true;
                        }
                        this.u = true;
                    }
                }
            }
            a(a2, layoutParams, layoutState2);
            if (!isLayoutRTL() || this.e != 1) {
                if (layoutParams.f) {
                    i9 = this.d.f();
                } else {
                    i9 = (span2.e * this.f) + this.d.f();
                }
                i8 = i9;
                i7 = this.d.b(a2) + i9;
            } else {
                if (layoutParams.f) {
                    i10 = this.d.b();
                } else {
                    i10 = this.d.b() - (((this.f996a - 1) - span2.e) * this.f);
                }
                i7 = i10;
                i8 = i10 - this.d.b(a2);
            }
            if (this.e == 1) {
                layoutDecoratedWithMargins(a2, i8, i6, i7, i5);
            } else {
                layoutDecoratedWithMargins(a2, i6, i8, i5, i7);
            }
            if (layoutParams.f) {
                a(this.g.e, i2);
            } else {
                a(span2, this.g.e, i2);
            }
            a(recycler2, this.g);
            if (this.g.h && a2.hasFocusable()) {
                if (layoutParams.f) {
                    this.j.clear();
                } else {
                    this.j.set(span2.e, false);
                    z2 = true;
                    r9 = 0;
                }
            }
            z2 = true;
            r9 = 0;
        }
        if (!z2) {
            a(recycler2, this.g);
        }
        if (this.g.e == -1) {
            i4 = this.c.f() - m(this.c.f());
        } else {
            i4 = j(this.c.b()) - this.c.b();
        }
        if (i4 > 0) {
            return Math.min(layoutState2.b, i4);
        }
        return 0;
    }

    private void a(View view, LayoutParams layoutParams, LayoutState layoutState) {
        if (layoutState.e == 1) {
            if (layoutParams.f) {
                a(view);
            } else {
                layoutParams.e.a(view);
            }
        } else if (layoutParams.f) {
            b(view);
        } else {
            layoutParams.e.c(view);
        }
    }

    private void a(RecyclerView.Recycler recycler, LayoutState layoutState) {
        int i2;
        int i3;
        if (layoutState.f961a && !layoutState.i) {
            if (layoutState.b == 0) {
                if (layoutState.e == -1) {
                    a(recycler, layoutState.g);
                } else {
                    b(recycler, layoutState.f);
                }
            } else if (layoutState.e == -1) {
                int i4 = layoutState.f;
                int k2 = i4 - k(i4);
                if (k2 < 0) {
                    i3 = layoutState.g;
                } else {
                    i3 = layoutState.g - Math.min(k2, layoutState.b);
                }
                a(recycler, i3);
            } else {
                int l2 = l(layoutState.g) - layoutState.g;
                if (l2 < 0) {
                    i2 = layoutState.f;
                } else {
                    i2 = Math.min(l2, layoutState.b) + layoutState.f;
                }
                b(recycler, i2);
            }
        }
    }

    private void a(View view) {
        for (int i2 = this.f996a - 1; i2 >= 0; i2--) {
            this.b[i2].a(view);
        }
    }

    private void a(int i2, int i3) {
        for (int i4 = 0; i4 < this.f996a; i4++) {
            if (!this.b[i4].f1002a.isEmpty()) {
                a(this.b[i4], i2, i3);
            }
        }
    }

    private void a(Span span, int i2, int i3) {
        int f2 = span.f();
        if (i2 == -1) {
            if (span.h() + f2 <= i3) {
                this.j.set(span.e, false);
            }
        } else if (span.g() - f2 >= i3) {
            this.j.set(span.e, false);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        int a2 = this.b[0].a(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.f996a; i2++) {
            if (this.b[i2].a(Integer.MIN_VALUE) != a2) {
                return false;
            }
        }
        return true;
    }

    private void a(RecyclerView.Recycler recycler, int i2) {
        int childCount = getChildCount() - 1;
        while (childCount >= 0) {
            View childAt = getChildAt(childCount);
            if (this.c.d(childAt) >= i2 && this.c.f(childAt) >= i2) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.f) {
                    int i3 = 0;
                    while (i3 < this.f996a) {
                        if (this.b[i3].f1002a.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.f996a; i4++) {
                        this.b[i4].j();
                    }
                } else if (layoutParams.e.f1002a.size() != 1) {
                    layoutParams.e.j();
                } else {
                    return;
                }
                removeAndRecycleView(childAt, recycler);
                childCount--;
            } else {
                return;
            }
        }
    }

    private Span a(LayoutState layoutState) {
        int i2;
        int i3;
        int i4 = -1;
        if (n(layoutState.e)) {
            i3 = this.f996a - 1;
            i2 = -1;
        } else {
            i3 = 0;
            i4 = this.f996a;
            i2 = 1;
        }
        Span span = null;
        if (layoutState.e == 1) {
            int i5 = Integer.MAX_VALUE;
            int f2 = this.c.f();
            while (i3 != i4) {
                Span span2 = this.b[i3];
                int a2 = span2.a(f2);
                if (a2 < i5) {
                    span = span2;
                    i5 = a2;
                }
                i3 += i2;
            }
            return span;
        }
        int i6 = Integer.MIN_VALUE;
        int b2 = this.c.b();
        while (i3 != i4) {
            Span span3 = this.b[i3];
            int b3 = span3.b(b2);
            if (b3 > i6) {
                span = span3;
                i6 = b3;
            }
            i3 += i2;
        }
        return span;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, RecyclerView.State state) {
        int i3;
        int i4;
        if (i2 > 0) {
            i4 = f();
            i3 = 1;
        } else {
            i4 = e();
            i3 = -1;
        }
        this.g.f961a = true;
        b(i4, state);
        o(i3);
        LayoutState layoutState = this.g;
        layoutState.c = i4 + layoutState.d;
        layoutState.b = Math.abs(i2);
    }
}
