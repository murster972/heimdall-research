package androidx.recyclerview.widget;

import androidx.core.util.Pools$Pool;
import androidx.core.util.Pools$SimplePool;
import androidx.recyclerview.widget.OpReorderer;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

class AdapterHelper implements OpReorderer.Callback {

    /* renamed from: a  reason: collision with root package name */
    private Pools$Pool<UpdateOp> f930a;
    final ArrayList<UpdateOp> b;
    final ArrayList<UpdateOp> c;
    final Callback d;
    Runnable e;
    final boolean f;
    final OpReorderer g;
    private int h;

    interface Callback {
        RecyclerView.ViewHolder a(int i);

        void a(int i, int i2);

        void a(int i, int i2, Object obj);

        void a(UpdateOp updateOp);

        void b(int i, int i2);

        void b(UpdateOp updateOp);

        void c(int i, int i2);

        void d(int i, int i2);
    }

    static class UpdateOp {

        /* renamed from: a  reason: collision with root package name */
        int f931a;
        int b;
        Object c;
        int d;

        UpdateOp(int i, int i2, int i3, Object obj) {
            this.f931a = i;
            this.b = i2;
            this.d = i3;
            this.c = obj;
        }

        /* access modifiers changed from: package-private */
        public String a() {
            int i = this.f931a;
            if (i == 1) {
                return "add";
            }
            if (i == 2) {
                return "rm";
            }
            if (i != 4) {
                return i != 8 ? "??" : "mv";
            }
            return "up";
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || UpdateOp.class != obj.getClass()) {
                return false;
            }
            UpdateOp updateOp = (UpdateOp) obj;
            int i = this.f931a;
            if (i != updateOp.f931a) {
                return false;
            }
            if (i == 8 && Math.abs(this.d - this.b) == 1 && this.d == updateOp.b && this.b == updateOp.d) {
                return true;
            }
            if (this.d != updateOp.d || this.b != updateOp.b) {
                return false;
            }
            Object obj2 = this.c;
            if (obj2 != null) {
                if (!obj2.equals(updateOp.c)) {
                    return false;
                }
            } else if (updateOp.c != null) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (((this.f931a * 31) + this.b) * 31) + this.d;
        }

        public String toString() {
            return Integer.toHexString(System.identityHashCode(this)) + "[" + a() + ",s:" + this.b + "c:" + this.d + ",p:" + this.c + "]";
        }
    }

    AdapterHelper(Callback callback) {
        this(callback, false);
    }

    private void b(UpdateOp updateOp) {
        g(updateOp);
    }

    private void c(UpdateOp updateOp) {
        g(updateOp);
    }

    private void d(UpdateOp updateOp) {
        char c2;
        boolean z;
        boolean z2;
        int i = updateOp.b;
        int i2 = updateOp.d + i;
        int i3 = 0;
        char c3 = 65535;
        int i4 = i;
        while (i4 < i2) {
            if (this.d.a(i4) != null || d(i4)) {
                if (c3 == 0) {
                    f(a(2, i, i3, (Object) null));
                    z2 = true;
                } else {
                    z2 = false;
                }
                c2 = 1;
            } else {
                if (c3 == 1) {
                    g(a(2, i, i3, (Object) null));
                    z = true;
                } else {
                    z = false;
                }
                c2 = 0;
            }
            if (z) {
                i4 -= i3;
                i2 -= i3;
                i3 = 1;
            } else {
                i3++;
            }
            i4++;
            c3 = c2;
        }
        if (i3 != updateOp.d) {
            a(updateOp);
            updateOp = a(2, i, i3, (Object) null);
        }
        if (c3 == 0) {
            f(updateOp);
        } else {
            g(updateOp);
        }
    }

    private void g(UpdateOp updateOp) {
        this.c.add(updateOp);
        int i = updateOp.f931a;
        if (i == 1) {
            this.d.c(updateOp.b, updateOp.d);
        } else if (i == 2) {
            this.d.b(updateOp.b, updateOp.d);
        } else if (i == 4) {
            this.d.a(updateOp.b, updateOp.d, updateOp.c);
        } else if (i == 8) {
            this.d.a(updateOp.b, updateOp.d);
        } else {
            throw new IllegalArgumentException("Unknown update op type for " + updateOp);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            this.d.b(this.c.get(i));
        }
        a((List<UpdateOp>) this.c);
        this.h = 0;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.g.a(this.b);
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            UpdateOp updateOp = this.b.get(i);
            int i2 = updateOp.f931a;
            if (i2 == 1) {
                b(updateOp);
            } else if (i2 == 2) {
                d(updateOp);
            } else if (i2 == 4) {
                e(updateOp);
            } else if (i2 == 8) {
                c(updateOp);
            }
            Runnable runnable = this.e;
            if (runnable != null) {
                runnable.run();
            }
        }
        this.b.clear();
    }

    /* access modifiers changed from: package-private */
    public void f() {
        a((List<UpdateOp>) this.b);
        a((List<UpdateOp>) this.c);
        this.h = 0;
    }

    AdapterHelper(Callback callback, boolean z) {
        this.f930a = new Pools$SimplePool(30);
        this.b = new ArrayList<>();
        this.c = new ArrayList<>();
        this.h = 0;
        this.d = callback;
        this.f = z;
        this.g = new OpReorderer(this);
    }

    /* access modifiers changed from: package-private */
    public int b(int i) {
        return a(i, 0);
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.b.size() > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i, int i2) {
        if (i2 < 1) {
            return false;
        }
        this.b.add(a(1, i, i2, (Object) null));
        this.h |= 1;
        if (this.b.size() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i) {
        return (i & this.h) != 0;
    }

    private void f(UpdateOp updateOp) {
        int i;
        int i2 = updateOp.f931a;
        if (i2 == 1 || i2 == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int d2 = d(updateOp.b, i2);
        int i3 = updateOp.b;
        int i4 = updateOp.f931a;
        if (i4 == 2) {
            i = 0;
        } else if (i4 == 4) {
            i = 1;
        } else {
            throw new IllegalArgumentException("op should be remove or update." + updateOp);
        }
        int i5 = d2;
        int i6 = i3;
        int i7 = 1;
        for (int i8 = 1; i8 < updateOp.d; i8++) {
            int d3 = d(updateOp.b + (i * i8), updateOp.f931a);
            int i9 = updateOp.f931a;
            if (i9 == 2 ? d3 == i5 : i9 == 4 && d3 == i5 + 1) {
                i7++;
            } else {
                UpdateOp a2 = a(updateOp.f931a, i5, i7, updateOp.c);
                a(a2, i6);
                a(a2);
                if (updateOp.f931a == 4) {
                    i6 += i7;
                }
                i5 = d3;
                i7 = 1;
            }
        }
        Object obj = updateOp.c;
        a(updateOp);
        if (i7 > 0) {
            UpdateOp a3 = a(updateOp.f931a, i5, i7, obj);
            a(a3, i6);
            a(a3);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i, int i2) {
        if (i2 < 1) {
            return false;
        }
        this.b.add(a(2, i, i2, (Object) null));
        this.h |= 2;
        if (this.b.size() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(UpdateOp updateOp, int i) {
        this.d.a(updateOp);
        int i2 = updateOp.f931a;
        if (i2 == 2) {
            this.d.d(i, updateOp.d);
        } else if (i2 == 4) {
            this.d.a(i, updateOp.d, updateOp.c);
        } else {
            throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a();
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            UpdateOp updateOp = this.b.get(i);
            int i2 = updateOp.f931a;
            if (i2 == 1) {
                this.d.b(updateOp);
                this.d.c(updateOp.b, updateOp.d);
            } else if (i2 == 2) {
                this.d.b(updateOp);
                this.d.d(updateOp.b, updateOp.d);
            } else if (i2 == 4) {
                this.d.b(updateOp);
                this.d.a(updateOp.b, updateOp.d, updateOp.c);
            } else if (i2 == 8) {
                this.d.b(updateOp);
                this.d.a(updateOp.b, updateOp.d);
            }
            Runnable runnable = this.e;
            if (runnable != null) {
                runnable.run();
            }
        }
        a((List<UpdateOp>) this.b);
        this.h = 0;
    }

    /* access modifiers changed from: package-private */
    public int a(int i, int i2) {
        int size = this.c.size();
        while (i2 < size) {
            UpdateOp updateOp = this.c.get(i2);
            int i3 = updateOp.f931a;
            if (i3 == 8) {
                int i4 = updateOp.b;
                if (i4 == i) {
                    i = updateOp.d;
                } else {
                    if (i4 < i) {
                        i--;
                    }
                    if (updateOp.d <= i) {
                        i++;
                    }
                }
            } else {
                int i5 = updateOp.b;
                if (i5 > i) {
                    continue;
                } else if (i3 == 2) {
                    int i6 = updateOp.d;
                    if (i < i5 + i6) {
                        return -1;
                    }
                    i -= i6;
                } else if (i3 == 1) {
                    i += updateOp.d;
                }
            }
            i2++;
        }
        return i;
    }

    private void e(UpdateOp updateOp) {
        int i = updateOp.b;
        int i2 = updateOp.d + i;
        int i3 = i;
        int i4 = 0;
        char c2 = 65535;
        while (i < i2) {
            if (this.d.a(i) != null || d(i)) {
                if (c2 == 0) {
                    f(a(4, i3, i4, updateOp.c));
                    i3 = i;
                    i4 = 0;
                }
                c2 = 1;
            } else {
                if (c2 == 1) {
                    g(a(4, i3, i4, updateOp.c));
                    i3 = i;
                    i4 = 0;
                }
                c2 = 0;
            }
            i4++;
            i++;
        }
        if (i4 != updateOp.d) {
            Object obj = updateOp.c;
            a(updateOp);
            updateOp = a(4, i3, i4, obj);
        }
        if (c2 == 0) {
            f(updateOp);
        } else {
            g(updateOp);
        }
    }

    private int d(int i, int i2) {
        for (int size = this.c.size() - 1; size >= 0; size--) {
            UpdateOp updateOp = this.c.get(size);
            int i3 = updateOp.f931a;
            if (i3 == 8) {
                int i4 = updateOp.b;
                int i5 = updateOp.d;
                if (i4 >= i5) {
                    int i6 = i5;
                    i5 = i4;
                    i4 = i6;
                }
                if (i < i4 || i > i5) {
                    int i7 = updateOp.b;
                    if (i < i7) {
                        if (i2 == 1) {
                            updateOp.b = i7 + 1;
                            updateOp.d++;
                        } else if (i2 == 2) {
                            updateOp.b = i7 - 1;
                            updateOp.d--;
                        }
                    }
                } else {
                    int i8 = updateOp.b;
                    if (i4 == i8) {
                        if (i2 == 1) {
                            updateOp.d++;
                        } else if (i2 == 2) {
                            updateOp.d--;
                        }
                        i++;
                    } else {
                        if (i2 == 1) {
                            updateOp.b = i8 + 1;
                        } else if (i2 == 2) {
                            updateOp.b = i8 - 1;
                        }
                        i--;
                    }
                }
            } else {
                int i9 = updateOp.b;
                if (i9 <= i) {
                    if (i3 == 1) {
                        i -= updateOp.d;
                    } else if (i3 == 2) {
                        i += updateOp.d;
                    }
                } else if (i2 == 1) {
                    updateOp.b = i9 + 1;
                } else if (i2 == 2) {
                    updateOp.b = i9 - 1;
                }
            }
        }
        for (int size2 = this.c.size() - 1; size2 >= 0; size2--) {
            UpdateOp updateOp2 = this.c.get(size2);
            if (updateOp2.f931a == 8) {
                int i10 = updateOp2.d;
                if (i10 == updateOp2.b || i10 < 0) {
                    this.c.remove(size2);
                    a(updateOp2);
                }
            } else if (updateOp2.d <= 0) {
                this.c.remove(size2);
                a(updateOp2);
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, Object obj) {
        if (i2 < 1) {
            return false;
        }
        this.b.add(a(4, i, i2, obj));
        this.h |= 4;
        if (this.b.size() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, int i3) {
        if (i == i2) {
            return false;
        }
        if (i3 == 1) {
            this.b.add(a(8, i, i2, (Object) null));
            this.h |= 8;
            if (this.b.size() == 1) {
                return true;
            }
            return false;
        }
        throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
    }

    public int a(int i) {
        int size = this.b.size();
        for (int i2 = 0; i2 < size; i2++) {
            UpdateOp updateOp = this.b.get(i2);
            int i3 = updateOp.f931a;
            if (i3 != 1) {
                if (i3 == 2) {
                    int i4 = updateOp.b;
                    if (i4 <= i) {
                        int i5 = updateOp.d;
                        if (i4 + i5 > i) {
                            return -1;
                        }
                        i -= i5;
                    } else {
                        continue;
                    }
                } else if (i3 == 8) {
                    int i6 = updateOp.b;
                    if (i6 == i) {
                        i = updateOp.d;
                    } else {
                        if (i6 < i) {
                            i--;
                        }
                        if (updateOp.d <= i) {
                            i++;
                        }
                    }
                }
            } else if (updateOp.b <= i) {
                i += updateOp.d;
            }
        }
        return i;
    }

    public UpdateOp a(int i, int i2, int i3, Object obj) {
        UpdateOp acquire = this.f930a.acquire();
        if (acquire == null) {
            return new UpdateOp(i, i2, i3, obj);
        }
        acquire.f931a = i;
        acquire.b = i2;
        acquire.d = i3;
        acquire.c = obj;
        return acquire;
    }

    private boolean d(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            UpdateOp updateOp = this.c.get(i2);
            int i3 = updateOp.f931a;
            if (i3 == 8) {
                if (a(updateOp.d, i2 + 1) == i) {
                    return true;
                }
            } else if (i3 == 1) {
                int i4 = updateOp.b;
                int i5 = updateOp.d + i4;
                while (i4 < i5) {
                    if (a(i4, i2 + 1) == i) {
                        return true;
                    }
                    i4++;
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    public void a(UpdateOp updateOp) {
        if (!this.f) {
            updateOp.c = null;
            this.f930a.release(updateOp);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<UpdateOp> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a(list.get(i));
        }
        list.clear();
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return !this.c.isEmpty() && !this.b.isEmpty();
    }
}
