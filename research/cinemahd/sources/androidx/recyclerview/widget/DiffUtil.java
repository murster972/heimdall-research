package androidx.recyclerview.widget;

import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DiffUtil {

    /* renamed from: a  reason: collision with root package name */
    private static final Comparator<Snake> f946a = new Comparator<Snake>() {
        /* renamed from: a */
        public int compare(Snake snake, Snake snake2) {
            int i = snake.f950a - snake2.f950a;
            return i == 0 ? snake.b - snake2.b : i;
        }
    };

    public static abstract class Callback {
        public abstract int a();

        public abstract boolean a(int i, int i2);

        public abstract int b();

        public abstract boolean b(int i, int i2);

        public Object c(int i, int i2) {
            return null;
        }
    }

    private static class PostponedUpdate {

        /* renamed from: a  reason: collision with root package name */
        int f948a;
        int b;
        boolean c;

        public PostponedUpdate(int i, int i2, boolean z) {
            this.f948a = i;
            this.b = i2;
            this.c = z;
        }
    }

    static class Range {

        /* renamed from: a  reason: collision with root package name */
        int f949a;
        int b;
        int c;
        int d;

        public Range() {
        }

        public Range(int i, int i2, int i3, int i4) {
            this.f949a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }
    }

    static class Snake {

        /* renamed from: a  reason: collision with root package name */
        int f950a;
        int b;
        int c;
        boolean d;
        boolean e;

        Snake() {
        }
    }

    private DiffUtil() {
    }

    public static DiffResult a(Callback callback) {
        return a(callback, true);
    }

    public static DiffResult a(Callback callback, boolean z) {
        Range range;
        int b = callback.b();
        int a2 = callback.a();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new Range(0, b, 0, a2));
        int abs = Math.abs(b - a2) + b + a2;
        int i = abs * 2;
        int[] iArr = new int[i];
        int[] iArr2 = new int[i];
        ArrayList arrayList3 = new ArrayList();
        while (!arrayList2.isEmpty()) {
            Range range2 = (Range) arrayList2.remove(arrayList2.size() - 1);
            Snake a3 = a(callback, range2.f949a, range2.b, range2.c, range2.d, iArr, iArr2, abs);
            if (a3 != null) {
                if (a3.c > 0) {
                    arrayList.add(a3);
                }
                a3.f950a += range2.f949a;
                a3.b += range2.c;
                if (arrayList3.isEmpty()) {
                    range = new Range();
                } else {
                    range = (Range) arrayList3.remove(arrayList3.size() - 1);
                }
                range.f949a = range2.f949a;
                range.c = range2.c;
                if (a3.e) {
                    range.b = a3.f950a;
                    range.d = a3.b;
                } else if (a3.d) {
                    range.b = a3.f950a - 1;
                    range.d = a3.b;
                } else {
                    range.b = a3.f950a;
                    range.d = a3.b - 1;
                }
                arrayList2.add(range);
                if (!a3.e) {
                    int i2 = a3.f950a;
                    int i3 = a3.c;
                    range2.f949a = i2 + i3;
                    range2.c = a3.b + i3;
                } else if (a3.d) {
                    int i4 = a3.f950a;
                    int i5 = a3.c;
                    range2.f949a = i4 + i5 + 1;
                    range2.c = a3.b + i5;
                } else {
                    int i6 = a3.f950a;
                    int i7 = a3.c;
                    range2.f949a = i6 + i7;
                    range2.c = a3.b + i7 + 1;
                }
                arrayList2.add(range2);
            } else {
                arrayList3.add(range2);
            }
        }
        Collections.sort(arrayList, f946a);
        return new DiffResult(callback, arrayList, iArr, iArr2, z);
    }

    public static class DiffResult {

        /* renamed from: a  reason: collision with root package name */
        private final List<Snake> f947a;
        private final int[] b;
        private final int[] c;
        private final Callback d;
        private final int e;
        private final int f;
        private final boolean g;

        DiffResult(Callback callback, List<Snake> list, int[] iArr, int[] iArr2, boolean z) {
            this.f947a = list;
            this.b = iArr;
            this.c = iArr2;
            Arrays.fill(this.b, 0);
            Arrays.fill(this.c, 0);
            this.d = callback;
            this.e = callback.b();
            this.f = callback.a();
            this.g = z;
            a();
            b();
        }

        private void a() {
            Snake snake = this.f947a.isEmpty() ? null : this.f947a.get(0);
            if (snake == null || snake.f950a != 0 || snake.b != 0) {
                Snake snake2 = new Snake();
                snake2.f950a = 0;
                snake2.b = 0;
                snake2.d = false;
                snake2.c = 0;
                snake2.e = false;
                this.f947a.add(0, snake2);
            }
        }

        private void b() {
            int i = this.e;
            int i2 = this.f;
            for (int size = this.f947a.size() - 1; size >= 0; size--) {
                Snake snake = this.f947a.get(size);
                int i3 = snake.f950a;
                int i4 = snake.c;
                int i5 = i3 + i4;
                int i6 = snake.b + i4;
                if (this.g) {
                    while (i > i5) {
                        a(i, i2, size);
                        i--;
                    }
                    while (i2 > i6) {
                        b(i, i2, size);
                        i2--;
                    }
                }
                for (int i7 = 0; i7 < snake.c; i7++) {
                    int i8 = snake.f950a + i7;
                    int i9 = snake.b + i7;
                    int i10 = this.d.a(i8, i9) ? 1 : 2;
                    this.b[i8] = (i9 << 5) | i10;
                    this.c[i9] = (i8 << 5) | i10;
                }
                i = snake.f950a;
                i2 = snake.b;
            }
        }

        private void a(int i, int i2, int i3) {
            if (this.b[i - 1] == 0) {
                a(i, i2, i3, false);
            }
        }

        private boolean a(int i, int i2, int i3, boolean z) {
            int i4;
            int i5;
            if (z) {
                i2--;
                i5 = i;
                i4 = i2;
            } else {
                i5 = i - 1;
                i4 = i5;
            }
            while (i3 >= 0) {
                Snake snake = this.f947a.get(i3);
                int i6 = snake.f950a;
                int i7 = snake.c;
                int i8 = i6 + i7;
                int i9 = snake.b + i7;
                int i10 = 8;
                if (z) {
                    for (int i11 = i5 - 1; i11 >= i8; i11--) {
                        if (this.d.b(i11, i4)) {
                            if (!this.d.a(i11, i4)) {
                                i10 = 4;
                            }
                            this.c[i4] = (i11 << 5) | 16;
                            this.b[i11] = (i4 << 5) | i10;
                            return true;
                        }
                    }
                    continue;
                } else {
                    for (int i12 = i2 - 1; i12 >= i9; i12--) {
                        if (this.d.b(i4, i12)) {
                            if (!this.d.a(i4, i12)) {
                                i10 = 4;
                            }
                            int i13 = i - 1;
                            this.b[i13] = (i12 << 5) | 16;
                            this.c[i12] = (i13 << 5) | i10;
                            return true;
                        }
                    }
                    continue;
                }
                i5 = snake.f950a;
                i2 = snake.b;
                i3--;
            }
            return false;
        }

        private void b(int i, int i2, int i3) {
            if (this.c[i2 - 1] == 0) {
                a(i, i2, i3, true);
            }
        }

        private void b(List<PostponedUpdate> list, ListUpdateCallback listUpdateCallback, int i, int i2, int i3) {
            if (!this.g) {
                listUpdateCallback.b(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.b[i5] & 31;
                if (i6 == 0) {
                    listUpdateCallback.b(i + i4, 1);
                    for (PostponedUpdate postponedUpdate : list) {
                        postponedUpdate.b--;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.b[i5] >> 5;
                    PostponedUpdate a2 = a(list, i7, false);
                    listUpdateCallback.c(i + i4, a2.b - 1);
                    if (i6 == 4) {
                        listUpdateCallback.a(a2.b - 1, 1, this.d.c(i5, i7));
                    }
                } else if (i6 == 16) {
                    list.add(new PostponedUpdate(i5, i + i4, true));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }

        public void a(RecyclerView.Adapter adapter) {
            a((ListUpdateCallback) new AdapterListUpdateCallback(adapter));
        }

        public void a(ListUpdateCallback listUpdateCallback) {
            BatchingListUpdateCallback batchingListUpdateCallback;
            if (listUpdateCallback instanceof BatchingListUpdateCallback) {
                batchingListUpdateCallback = (BatchingListUpdateCallback) listUpdateCallback;
            } else {
                batchingListUpdateCallback = new BatchingListUpdateCallback(listUpdateCallback);
            }
            ArrayList arrayList = new ArrayList();
            int i = this.e;
            int i2 = this.f;
            for (int size = this.f947a.size() - 1; size >= 0; size--) {
                Snake snake = this.f947a.get(size);
                int i3 = snake.c;
                int i4 = snake.f950a + i3;
                int i5 = snake.b + i3;
                if (i4 < i) {
                    b(arrayList, batchingListUpdateCallback, i4, i - i4, i4);
                }
                if (i5 < i2) {
                    a(arrayList, batchingListUpdateCallback, i4, i2 - i5, i5);
                }
                for (int i6 = i3 - 1; i6 >= 0; i6--) {
                    int[] iArr = this.b;
                    int i7 = snake.f950a;
                    if ((iArr[i7 + i6] & 31) == 2) {
                        batchingListUpdateCallback.a(i7 + i6, 1, this.d.c(i7 + i6, snake.b + i6));
                    }
                }
                i = snake.f950a;
                i2 = snake.b;
            }
            batchingListUpdateCallback.a();
        }

        private static PostponedUpdate a(List<PostponedUpdate> list, int i, boolean z) {
            int size = list.size() - 1;
            while (size >= 0) {
                PostponedUpdate postponedUpdate = list.get(size);
                if (postponedUpdate.f948a == i && postponedUpdate.c == z) {
                    list.remove(size);
                    while (size < list.size()) {
                        list.get(size).b += z ? 1 : -1;
                        size++;
                    }
                    return postponedUpdate;
                }
                size--;
            }
            return null;
        }

        private void a(List<PostponedUpdate> list, ListUpdateCallback listUpdateCallback, int i, int i2, int i3) {
            if (!this.g) {
                listUpdateCallback.a(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.c[i5] & 31;
                if (i6 == 0) {
                    listUpdateCallback.a(i, 1);
                    for (PostponedUpdate postponedUpdate : list) {
                        postponedUpdate.b++;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.c[i5] >> 5;
                    listUpdateCallback.c(a(list, i7, true).b, i);
                    if (i6 == 4) {
                        listUpdateCallback.a(i, 1, this.d.c(i7, i5));
                    }
                } else if (i6 == 16) {
                    list.add(new PostponedUpdate(i5, i, false));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        if (r1[r13 - 1] < r1[r13 + r5]) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ba, code lost:
        if (r2[r13 - 1] < r2[r13 + 1]) goto L_0x00c7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x009c A[LOOP:1: B:10:0x0033->B:33:0x009c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0081 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static androidx.recyclerview.widget.DiffUtil.Snake a(androidx.recyclerview.widget.DiffUtil.Callback r19, int r20, int r21, int r22, int r23, int[] r24, int[] r25, int r26) {
        /*
            r0 = r19
            r1 = r24
            r2 = r25
            int r3 = r21 - r20
            int r4 = r23 - r22
            r5 = 1
            if (r3 < r5) goto L_0x0133
            if (r4 >= r5) goto L_0x0011
            goto L_0x0133
        L_0x0011:
            int r6 = r3 - r4
            int r7 = r3 + r4
            int r7 = r7 + r5
            int r7 = r7 / 2
            int r8 = r26 - r7
            int r8 = r8 - r5
            int r9 = r26 + r7
            int r9 = r9 + r5
            r10 = 0
            java.util.Arrays.fill(r1, r8, r9, r10)
            int r8 = r8 + r6
            int r9 = r9 + r6
            java.util.Arrays.fill(r2, r8, r9, r3)
            int r8 = r6 % 2
            if (r8 == 0) goto L_0x002d
            r8 = 1
            goto L_0x002e
        L_0x002d:
            r8 = 0
        L_0x002e:
            r9 = 0
        L_0x002f:
            if (r9 > r7) goto L_0x012b
            int r11 = -r9
            r12 = r11
        L_0x0033:
            if (r12 > r9) goto L_0x00a2
            if (r12 == r11) goto L_0x004d
            if (r12 == r9) goto L_0x0045
            int r13 = r26 + r12
            int r14 = r13 + -1
            r14 = r1[r14]
            int r13 = r13 + r5
            r13 = r1[r13]
            if (r14 >= r13) goto L_0x0045
            goto L_0x004d
        L_0x0045:
            int r13 = r26 + r12
            int r13 = r13 - r5
            r13 = r1[r13]
            int r13 = r13 + r5
            r14 = 1
            goto L_0x0053
        L_0x004d:
            int r13 = r26 + r12
            int r13 = r13 + r5
            r13 = r1[r13]
            r14 = 0
        L_0x0053:
            int r15 = r13 - r12
        L_0x0055:
            if (r13 >= r3) goto L_0x006a
            if (r15 >= r4) goto L_0x006a
            int r10 = r20 + r13
            int r5 = r22 + r15
            boolean r5 = r0.b(r10, r5)
            if (r5 == 0) goto L_0x006a
            int r13 = r13 + 1
            int r15 = r15 + 1
            r5 = 1
            r10 = 0
            goto L_0x0055
        L_0x006a:
            int r5 = r26 + r12
            r1[r5] = r13
            if (r8 == 0) goto L_0x009c
            int r10 = r6 - r9
            r13 = 1
            int r10 = r10 + r13
            if (r12 < r10) goto L_0x009c
            int r10 = r6 + r9
            int r10 = r10 - r13
            if (r12 > r10) goto L_0x009c
            r10 = r1[r5]
            r13 = r2[r5]
            if (r10 < r13) goto L_0x009c
            androidx.recyclerview.widget.DiffUtil$Snake r0 = new androidx.recyclerview.widget.DiffUtil$Snake
            r0.<init>()
            r3 = r2[r5]
            r0.f950a = r3
            int r3 = r0.f950a
            int r3 = r3 - r12
            r0.b = r3
            r1 = r1[r5]
            r2 = r2[r5]
            int r1 = r1 - r2
            r0.c = r1
            r0.d = r14
            r5 = 0
            r0.e = r5
            return r0
        L_0x009c:
            r5 = 0
            int r12 = r12 + 2
            r5 = 1
            r10 = 0
            goto L_0x0033
        L_0x00a2:
            r5 = 0
            r10 = r11
        L_0x00a4:
            if (r10 > r9) goto L_0x0120
            int r12 = r10 + r6
            int r13 = r9 + r6
            if (r12 == r13) goto L_0x00c6
            int r13 = r11 + r6
            if (r12 == r13) goto L_0x00bd
            int r13 = r26 + r12
            int r14 = r13 + -1
            r14 = r2[r14]
            r15 = 1
            int r13 = r13 + r15
            r13 = r2[r13]
            if (r14 >= r13) goto L_0x00be
            goto L_0x00c7
        L_0x00bd:
            r15 = 1
        L_0x00be:
            int r13 = r26 + r12
            int r13 = r13 + r15
            r13 = r2[r13]
            int r13 = r13 - r15
            r14 = 1
            goto L_0x00cd
        L_0x00c6:
            r15 = 1
        L_0x00c7:
            int r13 = r26 + r12
            int r13 = r13 - r15
            r13 = r2[r13]
            r14 = 0
        L_0x00cd:
            int r16 = r13 - r12
        L_0x00cf:
            if (r13 <= 0) goto L_0x00ec
            if (r16 <= 0) goto L_0x00ec
            int r17 = r20 + r13
            int r5 = r17 + -1
            int r17 = r22 + r16
            r18 = r3
            int r3 = r17 + -1
            boolean r3 = r0.b(r5, r3)
            if (r3 == 0) goto L_0x00ee
            int r13 = r13 + -1
            int r16 = r16 + -1
            r3 = r18
            r5 = 0
            r15 = 1
            goto L_0x00cf
        L_0x00ec:
            r18 = r3
        L_0x00ee:
            int r3 = r26 + r12
            r2[r3] = r13
            if (r8 != 0) goto L_0x0119
            if (r12 < r11) goto L_0x0119
            if (r12 > r9) goto L_0x0119
            r5 = r1[r3]
            r13 = r2[r3]
            if (r5 < r13) goto L_0x0119
            androidx.recyclerview.widget.DiffUtil$Snake r0 = new androidx.recyclerview.widget.DiffUtil$Snake
            r0.<init>()
            r4 = r2[r3]
            r0.f950a = r4
            int r4 = r0.f950a
            int r4 = r4 - r12
            r0.b = r4
            r1 = r1[r3]
            r2 = r2[r3]
            int r1 = r1 - r2
            r0.c = r1
            r0.d = r14
            r3 = 1
            r0.e = r3
            return r0
        L_0x0119:
            r3 = 1
            int r10 = r10 + 2
            r3 = r18
            r5 = 0
            goto L_0x00a4
        L_0x0120:
            r18 = r3
            r3 = 1
            int r9 = r9 + 1
            r3 = r18
            r5 = 1
            r10 = 0
            goto L_0x002f
        L_0x012b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "DiffUtil hit an unexpected case while trying to calculate the optimal path. Please make sure your data is not changing during the diff calculation."
            r0.<init>(r1)
            throw r0
        L_0x0133:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.DiffUtil.a(androidx.recyclerview.widget.DiffUtil$Callback, int, int, int, int, int[], int[], int):androidx.recyclerview.widget.DiffUtil$Snake");
    }
}
