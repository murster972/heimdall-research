package androidx.recyclerview.widget;

import androidx.recyclerview.widget.RecyclerView;

public final class AdapterListUpdateCallback implements ListUpdateCallback {

    /* renamed from: a  reason: collision with root package name */
    private final RecyclerView.Adapter f932a;

    public AdapterListUpdateCallback(RecyclerView.Adapter adapter) {
        this.f932a = adapter;
    }

    public void a(int i, int i2) {
        this.f932a.notifyItemRangeInserted(i, i2);
    }

    public void b(int i, int i2) {
        this.f932a.notifyItemRangeRemoved(i, i2);
    }

    public void c(int i, int i2) {
        this.f932a.notifyItemMoved(i, i2);
    }

    public void a(int i, int i2, Object obj) {
        this.f932a.notifyItemRangeChanged(i, i2, obj);
    }
}
