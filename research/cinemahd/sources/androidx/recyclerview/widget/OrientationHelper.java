package androidx.recyclerview.widget;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public abstract class OrientationHelper {

    /* renamed from: a  reason: collision with root package name */
    protected final RecyclerView.LayoutManager f967a;
    private int b;
    final Rect c;

    public static OrientationHelper a(RecyclerView.LayoutManager layoutManager, int i) {
        if (i == 0) {
            return a(layoutManager);
        }
        if (i == 1) {
            return b(layoutManager);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    public static OrientationHelper b(RecyclerView.LayoutManager layoutManager) {
        return new OrientationHelper(layoutManager) {
            public int a() {
                return this.f967a.getHeight();
            }

            public int b() {
                return this.f967a.getHeight() - this.f967a.getPaddingBottom();
            }

            public int c(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return this.f967a.getDecoratedMeasuredWidth(view) + layoutParams.leftMargin + layoutParams.rightMargin;
            }

            public int d(View view) {
                return this.f967a.getDecoratedTop(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).topMargin;
            }

            public int e(View view) {
                this.f967a.getTransformedBoundingBox(view, true, this.c);
                return this.c.bottom;
            }

            public int f() {
                return this.f967a.getPaddingTop();
            }

            public int g() {
                return (this.f967a.getHeight() - this.f967a.getPaddingTop()) - this.f967a.getPaddingBottom();
            }

            public void a(int i) {
                this.f967a.offsetChildrenVertical(i);
            }

            public int b(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return this.f967a.getDecoratedMeasuredHeight(view) + layoutParams.topMargin + layoutParams.bottomMargin;
            }

            public int f(View view) {
                this.f967a.getTransformedBoundingBox(view, true, this.c);
                return this.c.top;
            }

            public int a(View view) {
                return this.f967a.getDecoratedBottom(view) + ((RecyclerView.LayoutParams) view.getLayoutParams()).bottomMargin;
            }

            public int c() {
                return this.f967a.getPaddingBottom();
            }

            public int d() {
                return this.f967a.getHeightMode();
            }

            public int e() {
                return this.f967a.getWidthMode();
            }
        };
    }

    public abstract int a();

    public abstract int a(View view);

    public abstract void a(int i);

    public abstract int b();

    public abstract int b(View view);

    public abstract int c();

    public abstract int c(View view);

    public abstract int d();

    public abstract int d(View view);

    public abstract int e();

    public abstract int e(View view);

    public abstract int f();

    public abstract int f(View view);

    public abstract int g();

    public int h() {
        if (Integer.MIN_VALUE == this.b) {
            return 0;
        }
        return g() - this.b;
    }

    public void i() {
        this.b = g();
    }

    private OrientationHelper(RecyclerView.LayoutManager layoutManager) {
        this.b = Integer.MIN_VALUE;
        this.c = new Rect();
        this.f967a = layoutManager;
    }

    public static OrientationHelper a(RecyclerView.LayoutManager layoutManager) {
        return new OrientationHelper(layoutManager) {
            public int a() {
                return this.f967a.getWidth();
            }

            public int b() {
                return this.f967a.getWidth() - this.f967a.getPaddingRight();
            }

            public int c(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return this.f967a.getDecoratedMeasuredHeight(view) + layoutParams.topMargin + layoutParams.bottomMargin;
            }

            public int d(View view) {
                return this.f967a.getDecoratedLeft(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).leftMargin;
            }

            public int e(View view) {
                this.f967a.getTransformedBoundingBox(view, true, this.c);
                return this.c.right;
            }

            public int f() {
                return this.f967a.getPaddingLeft();
            }

            public int g() {
                return (this.f967a.getWidth() - this.f967a.getPaddingLeft()) - this.f967a.getPaddingRight();
            }

            public void a(int i) {
                this.f967a.offsetChildrenHorizontal(i);
            }

            public int b(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return this.f967a.getDecoratedMeasuredWidth(view) + layoutParams.leftMargin + layoutParams.rightMargin;
            }

            public int f(View view) {
                this.f967a.getTransformedBoundingBox(view, true, this.c);
                return this.c.left;
            }

            public int a(View view) {
                return this.f967a.getDecoratedRight(view) + ((RecyclerView.LayoutParams) view.getLayoutParams()).rightMargin;
            }

            public int c() {
                return this.f967a.getPaddingRight();
            }

            public int d() {
                return this.f967a.getWidthMode();
            }

            public int e() {
                return this.f967a.getHeightMode();
            }
        };
    }
}
