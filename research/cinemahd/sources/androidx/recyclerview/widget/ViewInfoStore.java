package androidx.recyclerview.widget;

import androidx.collection.ArrayMap;
import androidx.collection.LongSparseArray;
import androidx.core.util.Pools$Pool;
import androidx.core.util.Pools$SimplePool;
import androidx.recyclerview.widget.RecyclerView;

class ViewInfoStore {

    /* renamed from: a  reason: collision with root package name */
    final ArrayMap<RecyclerView.ViewHolder, InfoRecord> f1005a = new ArrayMap<>();
    final LongSparseArray<RecyclerView.ViewHolder> b = new LongSparseArray<>();

    interface ProcessCallback {
        void a(RecyclerView.ViewHolder viewHolder);

        void a(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2);

        void b(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2);

        void c(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2);
    }

    ViewInfoStore() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f1005a.clear();
        this.b.a();
    }

    /* access modifiers changed from: package-private */
    public boolean b(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.f1005a.get(viewHolder);
        if (infoRecord == null || (infoRecord.f1006a & 1) == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void c(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo) {
        InfoRecord infoRecord = this.f1005a.get(viewHolder);
        if (infoRecord == null) {
            infoRecord = InfoRecord.b();
            this.f1005a.put(viewHolder, infoRecord);
        }
        infoRecord.b = itemHolderInfo;
        infoRecord.f1006a |= 4;
    }

    public void d(RecyclerView.ViewHolder viewHolder) {
        g(viewHolder);
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.ItemAnimator.ItemHolderInfo e(RecyclerView.ViewHolder viewHolder) {
        return a(viewHolder, 8);
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.ItemAnimator.ItemHolderInfo f(RecyclerView.ViewHolder viewHolder) {
        return a(viewHolder, 4);
    }

    /* access modifiers changed from: package-private */
    public void g(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.f1005a.get(viewHolder);
        if (infoRecord != null) {
            infoRecord.f1006a &= -2;
        }
    }

    /* access modifiers changed from: package-private */
    public void h(RecyclerView.ViewHolder viewHolder) {
        int b2 = this.b.b() - 1;
        while (true) {
            if (b2 < 0) {
                break;
            } else if (viewHolder == this.b.c(b2)) {
                this.b.b(b2);
                break;
            } else {
                b2--;
            }
        }
        InfoRecord remove = this.f1005a.remove(viewHolder);
        if (remove != null) {
            InfoRecord.a(remove);
        }
    }

    private RecyclerView.ItemAnimator.ItemHolderInfo a(RecyclerView.ViewHolder viewHolder, int i) {
        InfoRecord d;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo;
        int a2 = this.f1005a.a((Object) viewHolder);
        if (a2 >= 0 && (d = this.f1005a.d(a2)) != null) {
            int i2 = d.f1006a;
            if ((i2 & i) != 0) {
                d.f1006a = (~i) & i2;
                if (i == 4) {
                    itemHolderInfo = d.b;
                } else if (i == 8) {
                    itemHolderInfo = d.c;
                } else {
                    throw new IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((d.f1006a & 12) == 0) {
                    this.f1005a.c(a2);
                    InfoRecord.a(d);
                }
                return itemHolderInfo;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void b(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo) {
        InfoRecord infoRecord = this.f1005a.get(viewHolder);
        if (infoRecord == null) {
            infoRecord = InfoRecord.b();
            this.f1005a.put(viewHolder, infoRecord);
        }
        infoRecord.c = itemHolderInfo;
        infoRecord.f1006a |= 8;
    }

    static class InfoRecord {
        static Pools$Pool<InfoRecord> d = new Pools$SimplePool(20);

        /* renamed from: a  reason: collision with root package name */
        int f1006a;
        RecyclerView.ItemAnimator.ItemHolderInfo b;
        RecyclerView.ItemAnimator.ItemHolderInfo c;

        private InfoRecord() {
        }

        static void a(InfoRecord infoRecord) {
            infoRecord.f1006a = 0;
            infoRecord.b = null;
            infoRecord.c = null;
            d.release(infoRecord);
        }

        static InfoRecord b() {
            InfoRecord acquire = d.acquire();
            return acquire == null ? new InfoRecord() : acquire;
        }

        static void a() {
            do {
            } while (d.acquire() != null);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.f1005a.get(viewHolder);
        return (infoRecord == null || (infoRecord.f1006a & 4) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        InfoRecord.a();
    }

    /* access modifiers changed from: package-private */
    public void a(long j, RecyclerView.ViewHolder viewHolder) {
        this.b.c(j, viewHolder);
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo) {
        InfoRecord infoRecord = this.f1005a.get(viewHolder);
        if (infoRecord == null) {
            infoRecord = InfoRecord.b();
            this.f1005a.put(viewHolder, infoRecord);
        }
        infoRecord.f1006a |= 2;
        infoRecord.b = itemHolderInfo;
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.ViewHolder a(long j) {
        return this.b.b(j);
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.f1005a.get(viewHolder);
        if (infoRecord == null) {
            infoRecord = InfoRecord.b();
            this.f1005a.put(viewHolder, infoRecord);
        }
        infoRecord.f1006a |= 1;
    }

    /* access modifiers changed from: package-private */
    public void a(ProcessCallback processCallback) {
        for (int size = this.f1005a.size() - 1; size >= 0; size--) {
            RecyclerView.ViewHolder b2 = this.f1005a.b(size);
            InfoRecord c = this.f1005a.c(size);
            int i = c.f1006a;
            if ((i & 3) == 3) {
                processCallback.a(b2);
            } else if ((i & 1) != 0) {
                RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo = c.b;
                if (itemHolderInfo == null) {
                    processCallback.a(b2);
                } else {
                    processCallback.b(b2, itemHolderInfo, c.c);
                }
            } else if ((i & 14) == 14) {
                processCallback.a(b2, c.b, c.c);
            } else if ((i & 12) == 12) {
                processCallback.c(b2, c.b, c.c);
            } else if ((i & 4) != 0) {
                processCallback.b(b2, c.b, (RecyclerView.ItemAnimator.ItemHolderInfo) null);
            } else if ((i & 8) != 0) {
                processCallback.a(b2, c.b, c.c);
            }
            InfoRecord.a(c);
        }
    }
}
