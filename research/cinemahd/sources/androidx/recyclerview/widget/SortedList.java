package androidx.recyclerview.widget;

import java.lang.reflect.Array;
import java.util.Comparator;

public class SortedList<T> {

    /* renamed from: a  reason: collision with root package name */
    T[] f993a;
    private T[] b;
    private int c;
    private int d;
    private Callback e;
    private BatchedCallback f;
    private int g = 0;
    private final Class<T> h;

    public static class BatchedCallback<T2> extends Callback<T2> {

        /* renamed from: a  reason: collision with root package name */
        final Callback<T2> f994a;
        private final BatchingListUpdateCallback b = new BatchingListUpdateCallback(this.f994a);

        public BatchedCallback(Callback<T2> callback) {
            this.f994a = callback;
        }

        public void a(int i, int i2) {
            this.b.a(i, i2);
        }

        public void b(int i, int i2) {
            this.b.b(i, i2);
        }

        public void c(int i, int i2) {
            this.b.c(i, i2);
        }

        public int compare(T2 t2, T2 t22) {
            return this.f994a.compare(t2, t22);
        }

        public void d(int i, int i2) {
            this.b.a(i, i2, (Object) null);
        }

        public void a(int i, int i2, Object obj) {
            this.b.a(i, i2, obj);
        }

        public boolean b(T2 t2, T2 t22) {
            return this.f994a.b(t2, t22);
        }

        public Object c(T2 t2, T2 t22) {
            return this.f994a.c(t2, t22);
        }

        public boolean a(T2 t2, T2 t22) {
            return this.f994a.a(t2, t22);
        }

        public void a() {
            this.b.a();
        }
    }

    public static abstract class Callback<T2> implements Comparator<T2>, ListUpdateCallback {
        public void a(int i, int i2, Object obj) {
            d(i, i2);
        }

        public abstract boolean a(T2 t2, T2 t22);

        public abstract boolean b(T2 t2, T2 t22);

        public Object c(T2 t2, T2 t22) {
            return null;
        }

        public abstract int compare(T2 t2, T2 t22);

        public abstract void d(int i, int i2);
    }

    public SortedList(Class<T> cls, Callback<T> callback, int i) {
        this.h = cls;
        this.f993a = (Object[]) Array.newInstance(cls, i);
        this.e = callback;
    }

    private void d() {
        if (this.b != null) {
            throw new IllegalStateException("Data cannot be mutated in the middle of a batch update operation such as addAll or replaceAll.");
        }
    }

    public int a(T t) {
        d();
        return a(t, true);
    }

    public void b() {
        d();
        Callback callback = this.e;
        if (callback instanceof BatchedCallback) {
            ((BatchedCallback) callback).a();
        }
        Callback callback2 = this.e;
        BatchedCallback batchedCallback = this.f;
        if (callback2 == batchedCallback) {
            this.e = batchedCallback.f994a;
        }
    }

    public int c() {
        return this.g;
    }

    public void a() {
        d();
        Callback callback = this.e;
        if (!(callback instanceof BatchedCallback)) {
            if (this.f == null) {
                this.f = new BatchedCallback(callback);
            }
            this.e = this.f;
        }
    }

    private int a(T t, boolean z) {
        int a2 = a(t, this.f993a, 0, this.g, 1);
        if (a2 == -1) {
            a2 = 0;
        } else if (a2 < this.g) {
            T t2 = this.f993a[a2];
            if (this.e.b(t2, t)) {
                if (this.e.a(t2, t)) {
                    this.f993a[a2] = t;
                    return a2;
                }
                this.f993a[a2] = t;
                Callback callback = this.e;
                callback.a(a2, 1, callback.c(t2, t));
                return a2;
            }
        }
        a(a2, t);
        if (z) {
            this.e.a(a2, 1);
        }
        return a2;
    }

    public T a(int i) throws IndexOutOfBoundsException {
        int i2;
        if (i >= this.g || i < 0) {
            throw new IndexOutOfBoundsException("Asked to get item at " + i + " but size is " + this.g);
        }
        T[] tArr = this.b;
        if (tArr == null || i < (i2 = this.d)) {
            return this.f993a[i];
        }
        return tArr[(i - i2) + this.c];
    }

    private int a(T t, T[] tArr, int i, int i2, int i3) {
        while (i < i2) {
            int i4 = (i + i2) / 2;
            T t2 = tArr[i4];
            int compare = this.e.compare(t2, t);
            if (compare < 0) {
                i = i4 + 1;
            } else if (compare != 0) {
                i2 = i4;
            } else if (this.e.b(t2, t)) {
                return i4;
            } else {
                int a2 = a(t, i4, i, i2);
                return (i3 == 1 && a2 == -1) ? i4 : a2;
            }
        }
        if (i3 == 1) {
            return i;
        }
        return -1;
    }

    private int a(T t, int i, int i2, int i3) {
        T t2;
        int i4 = i - 1;
        while (i4 >= i2) {
            T t3 = this.f993a[i4];
            if (this.e.compare(t3, t) != 0) {
                break;
            } else if (this.e.b(t3, t)) {
                return i4;
            } else {
                i4--;
            }
        }
        do {
            i++;
            if (i >= i3) {
                return -1;
            }
            t2 = this.f993a[i];
            if (this.e.compare(t2, t) != 0) {
                return -1;
            }
        } while (!this.e.b(t2, t));
        return i;
    }

    private void a(int i, T t) {
        int i2 = this.g;
        if (i <= i2) {
            T[] tArr = this.f993a;
            if (i2 == tArr.length) {
                T[] tArr2 = (Object[]) Array.newInstance(this.h, tArr.length + 10);
                System.arraycopy(this.f993a, 0, tArr2, 0, i);
                tArr2[i] = t;
                System.arraycopy(this.f993a, i, tArr2, i + 1, this.g - i);
                this.f993a = tArr2;
            } else {
                System.arraycopy(tArr, i, tArr, i + 1, i2 - i);
                this.f993a[i] = t;
            }
            this.g++;
            return;
        }
        throw new IndexOutOfBoundsException("cannot add item to " + i + " because size is " + this.g);
    }
}
