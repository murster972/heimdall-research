package androidx.core.os;

import android.os.Build;
import android.os.Trace;

public final class TraceCompat {
    private TraceCompat() {
    }

    public static void a(String str) {
        if (Build.VERSION.SDK_INT >= 18) {
            Trace.beginSection(str);
        }
    }

    public static void a() {
        if (Build.VERSION.SDK_INT >= 18) {
            Trace.endSection();
        }
    }
}
