package androidx.core.view;

import android.view.View;
import android.view.ViewGroup;

public class NestedScrollingParentHelper {

    /* renamed from: a  reason: collision with root package name */
    private int f610a;
    private int b;

    public NestedScrollingParentHelper(ViewGroup viewGroup) {
    }

    public void a(View view, View view2, int i) {
        a(view, view2, i, 0);
    }

    public void a(View view, View view2, int i, int i2) {
        if (i2 == 1) {
            this.b = i;
        } else {
            this.f610a = i;
        }
    }

    public int a() {
        return this.f610a | this.b;
    }

    public void a(View view) {
        a(view, 0);
    }

    public void a(View view, int i) {
        if (i == 1) {
            this.b = 0;
        } else {
            this.f610a = 0;
        }
    }
}
