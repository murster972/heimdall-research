package androidx.core.view;

public interface NestedScrollingChild2 extends NestedScrollingChild {
    void stopNestedScroll(int i);
}
