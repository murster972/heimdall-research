package androidx.core.view;

import android.view.MotionEvent;

public final class MotionEventCompat {
    private MotionEventCompat() {
    }

    public static boolean a(MotionEvent motionEvent, int i) {
        return (motionEvent.getSource() & i) == i;
    }
}
