package androidx.core.view.accessibility;

import android.os.Build;
import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.ArrayList;
import java.util.List;

public class AccessibilityNodeProviderCompat {

    /* renamed from: a  reason: collision with root package name */
    private final Object f631a;

    static class AccessibilityNodeProviderApi16 extends AccessibilityNodeProvider {

        /* renamed from: a  reason: collision with root package name */
        final AccessibilityNodeProviderCompat f632a;

        AccessibilityNodeProviderApi16(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat) {
            this.f632a = accessibilityNodeProviderCompat;
        }

        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            AccessibilityNodeInfoCompat a2 = this.f632a.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.x();
        }

        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            List<AccessibilityNodeInfoCompat> a2 = this.f632a.a(str, i);
            if (a2 == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(a2.get(i2).x());
            }
            return arrayList;
        }

        public boolean performAction(int i, int i2, Bundle bundle) {
            return this.f632a.a(i, i2, bundle);
        }
    }

    static class AccessibilityNodeProviderApi19 extends AccessibilityNodeProviderApi16 {
        AccessibilityNodeProviderApi19(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat) {
            super(accessibilityNodeProviderCompat);
        }

        public AccessibilityNodeInfo findFocus(int i) {
            AccessibilityNodeInfoCompat b = this.f632a.b(i);
            if (b == null) {
                return null;
            }
            return b.x();
        }
    }

    public AccessibilityNodeProviderCompat() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            this.f631a = new AccessibilityNodeProviderApi19(this);
        } else if (i >= 16) {
            this.f631a = new AccessibilityNodeProviderApi16(this);
        } else {
            this.f631a = null;
        }
    }

    public AccessibilityNodeInfoCompat a(int i) {
        return null;
    }

    public Object a() {
        return this.f631a;
    }

    public List<AccessibilityNodeInfoCompat> a(String str, int i) {
        return null;
    }

    public boolean a(int i, int i2, Bundle bundle) {
        return false;
    }

    public AccessibilityNodeInfoCompat b(int i) {
        return null;
    }

    public AccessibilityNodeProviderCompat(Object obj) {
        this.f631a = obj;
    }
}
