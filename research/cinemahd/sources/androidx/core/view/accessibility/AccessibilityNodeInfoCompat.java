package androidx.core.view.accessibility;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.SparseArray;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.core.R$id;
import androidx.core.view.accessibility.AccessibilityViewCommand;
import com.facebook.common.util.ByteConstants;
import com.google.android.gms.ads.AdRequest;
import com.google.ar.core.ImageMetadata;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import okhttp3.internal.http2.Http2;

public class AccessibilityNodeInfoCompat {
    private static int d;

    /* renamed from: a  reason: collision with root package name */
    private final AccessibilityNodeInfo f626a;
    public int b = -1;
    private int c = -1;

    public static class AccessibilityActionCompat {
        public static final AccessibilityActionCompat d = new AccessibilityActionCompat(1, (CharSequence) null);
        public static final AccessibilityActionCompat e = new AccessibilityActionCompat(2, (CharSequence) null);
        public static final AccessibilityActionCompat f = new AccessibilityActionCompat(16, (CharSequence) null);
        public static final AccessibilityActionCompat g = new AccessibilityActionCompat(32, (CharSequence) null);
        public static final AccessibilityActionCompat h = new AccessibilityActionCompat(4096, (CharSequence) null);
        public static final AccessibilityActionCompat i = new AccessibilityActionCompat(8192, (CharSequence) null);
        public static final AccessibilityActionCompat j = new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 24 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS : null, 16908349, (CharSequence) null, (AccessibilityViewCommand) null, AccessibilityViewCommand.SetProgressArguments.class);

        /* renamed from: a  reason: collision with root package name */
        final Object f627a;
        private final Class<? extends AccessibilityViewCommand.CommandArguments> b;
        protected final AccessibilityViewCommand c;

        static {
            Class<AccessibilityViewCommand.MoveHtmlArguments> cls = AccessibilityViewCommand.MoveHtmlArguments.class;
            Class<AccessibilityViewCommand.MoveAtGranularityArguments> cls2 = AccessibilityViewCommand.MoveAtGranularityArguments.class;
            AccessibilityNodeInfo.AccessibilityAction accessibilityAction = null;
            new AccessibilityActionCompat(4, (CharSequence) null);
            new AccessibilityActionCompat(8, (CharSequence) null);
            new AccessibilityActionCompat(64, (CharSequence) null);
            new AccessibilityActionCompat(128, (CharSequence) null);
            new AccessibilityActionCompat(256, (CharSequence) null, cls2);
            new AccessibilityActionCompat(AdRequest.MAX_CONTENT_URL_LENGTH, (CharSequence) null, cls2);
            new AccessibilityActionCompat(ByteConstants.KB, (CharSequence) null, cls);
            new AccessibilityActionCompat(2048, (CharSequence) null, cls);
            new AccessibilityActionCompat(Http2.INITIAL_MAX_FRAME_SIZE, (CharSequence) null);
            new AccessibilityActionCompat(32768, (CharSequence) null);
            new AccessibilityActionCompat(ImageMetadata.CONTROL_AE_ANTIBANDING_MODE, (CharSequence) null);
            new AccessibilityActionCompat(131072, (CharSequence) null, AccessibilityViewCommand.SetSelectionArguments.class);
            new AccessibilityActionCompat(262144, (CharSequence) null);
            new AccessibilityActionCompat(ImageMetadata.LENS_APERTURE, (CharSequence) null);
            new AccessibilityActionCompat(1048576, (CharSequence) null);
            new AccessibilityActionCompat(2097152, (CharSequence) null, AccessibilityViewCommand.SetTextArguments.class);
            new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN : null, 16908342, (CharSequence) null, (AccessibilityViewCommand) null, (Class<? extends AccessibilityViewCommand.CommandArguments>) null);
            new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION : null, 16908343, (CharSequence) null, (AccessibilityViewCommand) null, AccessibilityViewCommand.ScrollToPositionArguments.class);
            new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP : null, 16908344, (CharSequence) null, (AccessibilityViewCommand) null, (Class<? extends AccessibilityViewCommand.CommandArguments>) null);
            new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT : null, 16908345, (CharSequence) null, (AccessibilityViewCommand) null, (Class<? extends AccessibilityViewCommand.CommandArguments>) null);
            new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN : null, 16908346, (CharSequence) null, (AccessibilityViewCommand) null, (Class<? extends AccessibilityViewCommand.CommandArguments>) null);
            new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT : null, 16908347, (CharSequence) null, (AccessibilityViewCommand) null, (Class<? extends AccessibilityViewCommand.CommandArguments>) null);
            new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK : null, 16908348, (CharSequence) null, (AccessibilityViewCommand) null, (Class<? extends AccessibilityViewCommand.CommandArguments>) null);
            new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 26 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_MOVE_WINDOW : null, 16908354, (CharSequence) null, (AccessibilityViewCommand) null, AccessibilityViewCommand.MoveWindowArguments.class);
            new AccessibilityActionCompat(Build.VERSION.SDK_INT >= 28 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_TOOLTIP : null, 16908356, (CharSequence) null, (AccessibilityViewCommand) null, (Class<? extends AccessibilityViewCommand.CommandArguments>) null);
            if (Build.VERSION.SDK_INT >= 28) {
                accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_HIDE_TOOLTIP;
            }
            new AccessibilityActionCompat(accessibilityAction, 16908357, (CharSequence) null, (AccessibilityViewCommand) null, (Class<? extends AccessibilityViewCommand.CommandArguments>) null);
        }

        public AccessibilityActionCompat(int i2, CharSequence charSequence) {
            this((Object) null, i2, charSequence, (AccessibilityViewCommand) null, (Class<? extends AccessibilityViewCommand.CommandArguments>) null);
        }

        public int a() {
            if (Build.VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo.AccessibilityAction) this.f627a).getId();
            }
            return 0;
        }

        private AccessibilityActionCompat(int i2, CharSequence charSequence, Class<? extends AccessibilityViewCommand.CommandArguments> cls) {
            this((Object) null, i2, charSequence, (AccessibilityViewCommand) null, cls);
        }

        AccessibilityActionCompat(Object obj, int i2, CharSequence charSequence, AccessibilityViewCommand accessibilityViewCommand, Class<? extends AccessibilityViewCommand.CommandArguments> cls) {
            this.c = accessibilityViewCommand;
            if (Build.VERSION.SDK_INT < 21 || obj != null) {
                this.f627a = obj;
            } else {
                this.f627a = new AccessibilityNodeInfo.AccessibilityAction(i2, charSequence);
            }
            this.b = cls;
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x0025  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0028  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.view.View r5, android.os.Bundle r6) {
            /*
                r4 = this;
                androidx.core.view.accessibility.AccessibilityViewCommand r0 = r4.c
                r1 = 0
                if (r0 == 0) goto L_0x0049
                r0 = 0
                java.lang.Class<? extends androidx.core.view.accessibility.AccessibilityViewCommand$CommandArguments> r2 = r4.b
                if (r2 == 0) goto L_0x0042
                java.lang.Class[] r3 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0020 }
                java.lang.reflect.Constructor r2 = r2.getDeclaredConstructor(r3)     // Catch:{ Exception -> 0x0020 }
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0020 }
                java.lang.Object r1 = r2.newInstance(r1)     // Catch:{ Exception -> 0x0020 }
                androidx.core.view.accessibility.AccessibilityViewCommand$CommandArguments r1 = (androidx.core.view.accessibility.AccessibilityViewCommand.CommandArguments) r1     // Catch:{ Exception -> 0x0020 }
                r1.a(r6)     // Catch:{ Exception -> 0x001d }
                r0 = r1
                goto L_0x0042
            L_0x001d:
                r6 = move-exception
                r0 = r1
                goto L_0x0021
            L_0x0020:
                r6 = move-exception
            L_0x0021:
                java.lang.Class<? extends androidx.core.view.accessibility.AccessibilityViewCommand$CommandArguments> r1 = r4.b
                if (r1 != 0) goto L_0x0028
                java.lang.String r1 = "null"
                goto L_0x002c
            L_0x0028:
                java.lang.String r1 = r1.getName()
            L_0x002c:
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Failed to execute command with argument class ViewCommandArgument: "
                r2.append(r3)
                r2.append(r1)
                java.lang.String r1 = r2.toString()
                java.lang.String r2 = "A11yActionCompat"
                android.util.Log.e(r2, r1, r6)
            L_0x0042:
                androidx.core.view.accessibility.AccessibilityViewCommand r6 = r4.c
                boolean r5 = r6.a(r5, r0)
                return r5
            L_0x0049:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.core.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat.a(android.view.View, android.os.Bundle):boolean");
        }
    }

    public static class CollectionInfoCompat {

        /* renamed from: a  reason: collision with root package name */
        final Object f628a;

        CollectionInfoCompat(Object obj) {
            this.f628a = obj;
        }

        public static CollectionInfoCompat a(int i, int i2, boolean z, int i3) {
            int i4 = Build.VERSION.SDK_INT;
            if (i4 >= 21) {
                return new CollectionInfoCompat(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z, i3));
            }
            if (i4 >= 19) {
                return new CollectionInfoCompat(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z));
            }
            return new CollectionInfoCompat((Object) null);
        }
    }

    public static class RangeInfoCompat {

        /* renamed from: a  reason: collision with root package name */
        final Object f630a;

        RangeInfoCompat(Object obj) {
            this.f630a = obj;
        }

        public static RangeInfoCompat a(int i, float f, float f2, float f3) {
            if (Build.VERSION.SDK_INT >= 19) {
                return new RangeInfoCompat(AccessibilityNodeInfo.RangeInfo.obtain(i, f, f2, f3));
            }
            return new RangeInfoCompat((Object) null);
        }
    }

    private AccessibilityNodeInfoCompat(AccessibilityNodeInfo accessibilityNodeInfo) {
        this.f626a = accessibilityNodeInfo;
    }

    public static AccessibilityNodeInfoCompat A() {
        return a(AccessibilityNodeInfo.obtain());
    }

    public static AccessibilityNodeInfoCompat a(AccessibilityNodeInfo accessibilityNodeInfo) {
        return new AccessibilityNodeInfoCompat(accessibilityNodeInfo);
    }

    private static String c(int i) {
        if (i == 1) {
            return "ACTION_FOCUS";
        }
        if (i == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (i) {
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case AdRequest.MAX_CONTENT_URL_LENGTH /*512*/:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case ByteConstants.KB /*1024*/:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case Http2.INITIAL_MAX_FRAME_SIZE /*16384*/:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case ImageMetadata.CONTROL_AE_ANTIBANDING_MODE /*65536*/:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }

    public static AccessibilityNodeInfoCompat f(View view) {
        return a(AccessibilityNodeInfo.obtain(view));
    }

    private void y() {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f626a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
            this.f626a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
            this.f626a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
            this.f626a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        }
    }

    private boolean z() {
        return !a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").isEmpty();
    }

    public int b() {
        return this.f626a.getChildCount();
    }

    public void c(View view) {
        this.c = -1;
        this.f626a.setSource(view);
    }

    public void d(Rect rect) {
        this.f626a.setBoundsInScreen(rect);
    }

    public void e(boolean z) {
        this.f626a.setClickable(z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AccessibilityNodeInfoCompat.class != obj.getClass()) {
            return false;
        }
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat = (AccessibilityNodeInfoCompat) obj;
        AccessibilityNodeInfo accessibilityNodeInfo = this.f626a;
        if (accessibilityNodeInfo == null) {
            if (accessibilityNodeInfoCompat.f626a != null) {
                return false;
            }
        } else if (!accessibilityNodeInfo.equals(accessibilityNodeInfoCompat.f626a)) {
            return false;
        }
        return this.c == accessibilityNodeInfoCompat.c && this.b == accessibilityNodeInfoCompat.b;
    }

    public int g() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.f626a.getMovementGranularities();
        }
        return 0;
    }

    public void h(boolean z) {
        this.f626a.setEnabled(z);
    }

    public int hashCode() {
        AccessibilityNodeInfo accessibilityNodeInfo = this.f626a;
        if (accessibilityNodeInfo == null) {
            return 0;
        }
        return accessibilityNodeInfo.hashCode();
    }

    public void i(boolean z) {
        this.f626a.setFocusable(z);
    }

    public void j(boolean z) {
        this.f626a.setFocused(z);
    }

    public boolean k() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.f626a.isAccessibilityFocused();
        }
        return false;
    }

    public boolean l() {
        return this.f626a.isCheckable();
    }

    public boolean m() {
        return this.f626a.isChecked();
    }

    public boolean n() {
        return this.f626a.isClickable();
    }

    public void o(boolean z) {
        this.f626a.setSelected(z);
    }

    public boolean p() {
        return this.f626a.isFocusable();
    }

    public boolean q() {
        return this.f626a.isFocused();
    }

    public boolean r() {
        return this.f626a.isLongClickable();
    }

    public boolean s() {
        return this.f626a.isPassword();
    }

    public boolean t() {
        return this.f626a.isScrollable();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        b(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ");
        sb.append(h());
        sb.append("; className: ");
        sb.append(c());
        sb.append("; text: ");
        sb.append(i());
        sb.append("; contentDescription: ");
        sb.append(e());
        sb.append("; viewId: ");
        sb.append(j());
        sb.append("; checkable: ");
        sb.append(l());
        sb.append("; checked: ");
        sb.append(m());
        sb.append("; focusable: ");
        sb.append(p());
        sb.append("; focused: ");
        sb.append(q());
        sb.append("; selected: ");
        sb.append(u());
        sb.append("; clickable: ");
        sb.append(n());
        sb.append("; longClickable: ");
        sb.append(r());
        sb.append("; enabled: ");
        sb.append(o());
        sb.append("; password: ");
        sb.append(s());
        sb.append("; scrollable: " + t());
        sb.append("; [");
        int a2 = a();
        while (a2 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(a2);
            a2 &= ~numberOfTrailingZeros;
            sb.append(c(numberOfTrailingZeros));
            if (a2 != 0) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public boolean u() {
        return this.f626a.isSelected();
    }

    public boolean v() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.f626a.isVisibleToUser();
        }
        return false;
    }

    public void w() {
        this.f626a.recycle();
    }

    public AccessibilityNodeInfo x() {
        return this.f626a;
    }

    public static AccessibilityNodeInfoCompat a(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        return a(AccessibilityNodeInfo.obtain(accessibilityNodeInfoCompat.f626a));
    }

    public boolean b(AccessibilityActionCompat accessibilityActionCompat) {
        if (Build.VERSION.SDK_INT >= 21) {
            return this.f626a.removeAction((AccessibilityNodeInfo.AccessibilityAction) accessibilityActionCompat.f627a);
        }
        return false;
    }

    public void d(boolean z) {
        this.f626a.setChecked(z);
    }

    public void e(CharSequence charSequence) {
        this.f626a.setPackageName(charSequence);
    }

    public void f(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f626a.setContentInvalid(z);
        }
    }

    public CharSequence h() {
        return this.f626a.getPackageName();
    }

    public CharSequence i() {
        if (!z()) {
            return this.f626a.getText();
        }
        List<Integer> a2 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
        List<Integer> a3 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
        List<Integer> a4 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
        List<Integer> a5 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        SpannableString spannableString = new SpannableString(TextUtils.substring(this.f626a.getText(), 0, this.f626a.getText().length()));
        for (int i = 0; i < a2.size(); i++) {
            spannableString.setSpan(new AccessibilityClickableSpanCompat(a5.get(i).intValue(), this, f().getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY")), a2.get(i).intValue(), a3.get(i).intValue(), a4.get(i).intValue());
        }
        return spannableString;
    }

    public String j() {
        if (Build.VERSION.SDK_INT >= 18) {
            return this.f626a.getViewIdResourceName();
        }
        return null;
    }

    public void l(boolean z) {
        this.f626a.setLongClickable(z);
    }

    public void m(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.f626a.setScreenReaderFocusable(z);
        } else {
            a(1, z);
        }
    }

    public void n(boolean z) {
        this.f626a.setScrollable(z);
    }

    public boolean o() {
        return this.f626a.isEnabled();
    }

    public void p(boolean z) {
        if (Build.VERSION.SDK_INT >= 26) {
            this.f626a.setShowingHintText(z);
        } else {
            a(4, z);
        }
    }

    public void q(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.f626a.setVisibleToUser(z);
        }
    }

    private SparseArray<WeakReference<ClickableSpan>> d(View view) {
        SparseArray<WeakReference<ClickableSpan>> e = e(view);
        if (e != null) {
            return e;
        }
        SparseArray<WeakReference<ClickableSpan>> sparseArray = new SparseArray<>();
        view.setTag(R$id.tag_accessibility_clickable_spans, sparseArray);
        return sparseArray;
    }

    private SparseArray<WeakReference<ClickableSpan>> e(View view) {
        return (SparseArray) view.getTag(R$id.tag_accessibility_clickable_spans);
    }

    private void g(View view) {
        SparseArray<WeakReference<ClickableSpan>> e = e(view);
        if (e != null) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < e.size(); i++) {
                if (e.valueAt(i).get() == null) {
                    arrayList.add(Integer.valueOf(i));
                }
            }
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                e.remove(((Integer) arrayList.get(i2)).intValue());
            }
        }
    }

    public void a(View view) {
        this.f626a.addChild(view);
    }

    public void c(View view, int i) {
        this.c = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.f626a.setSource(view, i);
        }
    }

    public void h(CharSequence charSequence) {
        this.f626a.setText(charSequence);
    }

    public void k(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.f626a.setHeading(z);
        } else {
            a(2, z);
        }
    }

    public static class CollectionItemInfoCompat {

        /* renamed from: a  reason: collision with root package name */
        final Object f629a;

        CollectionItemInfoCompat(Object obj) {
            this.f629a = obj;
        }

        public static CollectionItemInfoCompat a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            int i5 = Build.VERSION.SDK_INT;
            if (i5 >= 21) {
                return new CollectionItemInfoCompat(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z, z2));
            }
            if (i5 >= 19) {
                return new CollectionItemInfoCompat(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z));
            }
            return new CollectionItemInfoCompat((Object) null);
        }

        public int b() {
            if (Build.VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo.CollectionItemInfo) this.f629a).getColumnSpan();
            }
            return 0;
        }

        public int c() {
            if (Build.VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo.CollectionItemInfo) this.f629a).getRowIndex();
            }
            return 0;
        }

        public int d() {
            if (Build.VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo.CollectionItemInfo) this.f629a).getRowSpan();
            }
            return 0;
        }

        public boolean e() {
            if (Build.VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo.CollectionItemInfo) this.f629a).isSelected();
            }
            return false;
        }

        public static CollectionItemInfoCompat a(int i, int i2, int i3, int i4, boolean z) {
            if (Build.VERSION.SDK_INT >= 19) {
                return new CollectionItemInfoCompat(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z));
            }
            return new CollectionItemInfoCompat((Object) null);
        }

        public int a() {
            if (Build.VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo.CollectionItemInfo) this.f629a).getColumnIndex();
            }
            return 0;
        }
    }

    public void a(View view, int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.f626a.addChild(view, i);
        }
    }

    public void b(int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.f626a.setMovementGranularities(i);
        }
    }

    public CharSequence e() {
        return this.f626a.getContentDescription();
    }

    public Bundle f() {
        if (Build.VERSION.SDK_INT >= 19) {
            return this.f626a.getExtras();
        }
        return new Bundle();
    }

    public int a() {
        return this.f626a.getActions();
    }

    public void b(View view) {
        this.b = -1;
        this.f626a.setParent(view);
    }

    public void c(Rect rect) {
        this.f626a.setBoundsInParent(rect);
    }

    public CollectionItemInfoCompat d() {
        AccessibilityNodeInfo.CollectionItemInfo collectionItemInfo;
        if (Build.VERSION.SDK_INT < 19 || (collectionItemInfo = this.f626a.getCollectionItemInfo()) == null) {
            return null;
        }
        return new CollectionItemInfoCompat(collectionItemInfo);
    }

    public void a(int i) {
        this.f626a.addAction(i);
    }

    public void c(boolean z) {
        this.f626a.setCheckable(z);
    }

    public void f(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            this.f626a.setPaneTitle(charSequence);
        } else if (i >= 19) {
            this.f626a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY", charSequence);
        }
    }

    private List<Integer> a(String str) {
        if (Build.VERSION.SDK_INT < 19) {
            return new ArrayList();
        }
        ArrayList<Integer> integerArrayList = this.f626a.getExtras().getIntegerArrayList(str);
        if (integerArrayList != null) {
            return integerArrayList;
        }
        ArrayList arrayList = new ArrayList();
        this.f626a.getExtras().putIntegerArrayList(str, arrayList);
        return arrayList;
    }

    public void b(View view, int i) {
        this.b = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.f626a.setParent(view, i);
        }
    }

    public CharSequence c() {
        return this.f626a.getClassName();
    }

    public void c(CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.f626a.setError(charSequence);
        }
    }

    public void d(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            this.f626a.setHintText(charSequence);
        } else if (i >= 19) {
            this.f626a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", charSequence);
        }
    }

    public void g(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f626a.setDismissable(z);
        }
    }

    public void b(Rect rect) {
        this.f626a.getBoundsInScreen(rect);
    }

    public void b(CharSequence charSequence) {
        this.f626a.setContentDescription(charSequence);
    }

    public void g(CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f626a.getExtras().putCharSequence("AccessibilityNodeInfo.roleDescription", charSequence);
        }
    }

    public void b(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f626a.setCollectionItemInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionItemInfo) ((CollectionItemInfoCompat) obj).f629a);
        }
    }

    public void a(AccessibilityActionCompat accessibilityActionCompat) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.f626a.addAction((AccessibilityNodeInfo.AccessibilityAction) accessibilityActionCompat.f627a);
        }
    }

    public void b(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f626a.setCanOpenPopup(z);
        }
    }

    public static ClickableSpan[] i(CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return (ClickableSpan[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), ClickableSpan.class);
        }
        return null;
    }

    public boolean a(int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.f626a.performAction(i, bundle);
        }
        return false;
    }

    public void a(Rect rect) {
        this.f626a.getBoundsInParent(rect);
    }

    public void a(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.f626a.setAccessibilityFocused(z);
        }
    }

    public void a(CharSequence charSequence) {
        this.f626a.setClassName(charSequence);
    }

    public void a(CharSequence charSequence, View view) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19 && i < 26) {
            y();
            g(view);
            ClickableSpan[] i2 = i(charSequence);
            if (i2 != null && i2.length > 0) {
                f().putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY", R$id.accessibility_action_clickable_span);
                SparseArray<WeakReference<ClickableSpan>> d2 = d(view);
                int i3 = 0;
                while (i2 != null && i3 < i2.length) {
                    int a2 = a(i2[i3], d2);
                    d2.put(a2, new WeakReference(i2[i3]));
                    a(i2[i3], (Spanned) charSequence, a2);
                    i3++;
                }
            }
        }
    }

    private int a(ClickableSpan clickableSpan, SparseArray<WeakReference<ClickableSpan>> sparseArray) {
        if (sparseArray != null) {
            for (int i = 0; i < sparseArray.size(); i++) {
                if (clickableSpan.equals((ClickableSpan) sparseArray.valueAt(i).get())) {
                    return sparseArray.keyAt(i);
                }
            }
        }
        int i2 = d;
        d = i2 + 1;
        return i2;
    }

    private void a(ClickableSpan clickableSpan, Spanned spanned, int i) {
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(Integer.valueOf(spanned.getSpanStart(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(Integer.valueOf(spanned.getSpanEnd(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(Integer.valueOf(spanned.getSpanFlags(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(Integer.valueOf(i));
    }

    public void a(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f626a.setCollectionInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionInfo) ((CollectionInfoCompat) obj).f628a);
        }
    }

    public void a(RangeInfoCompat rangeInfoCompat) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f626a.setRangeInfo((AccessibilityNodeInfo.RangeInfo) rangeInfoCompat.f630a);
        }
    }

    private void a(int i, boolean z) {
        Bundle f = f();
        if (f != null) {
            int i2 = f.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & (~i);
            if (!z) {
                i = 0;
            }
            f.putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", i | i2);
        }
    }
}
