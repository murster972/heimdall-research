package androidx.core.view;

import android.content.Context;
import android.os.Build;
import android.view.PointerIcon;

public final class PointerIconCompat {

    /* renamed from: a  reason: collision with root package name */
    private Object f612a;

    private PointerIconCompat(Object obj) {
        this.f612a = obj;
    }

    public Object a() {
        return this.f612a;
    }

    public static PointerIconCompat a(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 24) {
            return new PointerIconCompat(PointerIcon.getSystemIcon(context, i));
        }
        return new PointerIconCompat((Object) null);
    }
}
