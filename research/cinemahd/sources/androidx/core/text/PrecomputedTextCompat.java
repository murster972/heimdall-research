package androidx.core.text;

import android.os.Build;
import android.text.PrecomputedText;
import android.text.Spannable;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.MetricAffectingSpan;
import androidx.core.util.ObjectsCompat;

public class PrecomputedTextCompat implements Spannable {

    /* renamed from: a  reason: collision with root package name */
    private final Spannable f589a;
    private final Params b;

    public static final class Params {

        /* renamed from: a  reason: collision with root package name */
        private final TextPaint f590a;
        private final TextDirectionHeuristic b;
        private final int c;
        private final int d;
        final PrecomputedText.Params e = null;

        public static class Builder {

            /* renamed from: a  reason: collision with root package name */
            private final TextPaint f591a;
            private TextDirectionHeuristic b;
            private int c;
            private int d;

            public Builder(TextPaint textPaint) {
                this.f591a = textPaint;
                if (Build.VERSION.SDK_INT >= 23) {
                    this.c = 1;
                    this.d = 1;
                } else {
                    this.d = 0;
                    this.c = 0;
                }
                if (Build.VERSION.SDK_INT >= 18) {
                    this.b = TextDirectionHeuristics.FIRSTSTRONG_LTR;
                } else {
                    this.b = null;
                }
            }

            public Builder a(int i) {
                this.c = i;
                return this;
            }

            public Builder b(int i) {
                this.d = i;
                return this;
            }

            public Builder a(TextDirectionHeuristic textDirectionHeuristic) {
                this.b = textDirectionHeuristic;
                return this;
            }

            public Params a() {
                return new Params(this.f591a, this.b, this.c, this.d);
            }
        }

        Params(TextPaint textPaint, TextDirectionHeuristic textDirectionHeuristic, int i, int i2) {
            this.f590a = textPaint;
            this.b = textDirectionHeuristic;
            this.c = i;
            this.d = i2;
        }

        public int a() {
            return this.c;
        }

        public int b() {
            return this.d;
        }

        public TextDirectionHeuristic c() {
            return this.b;
        }

        public TextPaint d() {
            return this.f590a;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Params)) {
                return false;
            }
            Params params = (Params) obj;
            if (!a(params)) {
                return false;
            }
            return Build.VERSION.SDK_INT < 18 || this.b == params.c();
        }

        public int hashCode() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                return ObjectsCompat.a(Float.valueOf(this.f590a.getTextSize()), Float.valueOf(this.f590a.getTextScaleX()), Float.valueOf(this.f590a.getTextSkewX()), Float.valueOf(this.f590a.getLetterSpacing()), Integer.valueOf(this.f590a.getFlags()), this.f590a.getTextLocales(), this.f590a.getTypeface(), Boolean.valueOf(this.f590a.isElegantTextHeight()), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 21) {
                return ObjectsCompat.a(Float.valueOf(this.f590a.getTextSize()), Float.valueOf(this.f590a.getTextScaleX()), Float.valueOf(this.f590a.getTextSkewX()), Float.valueOf(this.f590a.getLetterSpacing()), Integer.valueOf(this.f590a.getFlags()), this.f590a.getTextLocale(), this.f590a.getTypeface(), Boolean.valueOf(this.f590a.isElegantTextHeight()), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 18) {
                return ObjectsCompat.a(Float.valueOf(this.f590a.getTextSize()), Float.valueOf(this.f590a.getTextScaleX()), Float.valueOf(this.f590a.getTextSkewX()), Integer.valueOf(this.f590a.getFlags()), this.f590a.getTextLocale(), this.f590a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 17) {
                return ObjectsCompat.a(Float.valueOf(this.f590a.getTextSize()), Float.valueOf(this.f590a.getTextScaleX()), Float.valueOf(this.f590a.getTextSkewX()), Integer.valueOf(this.f590a.getFlags()), this.f590a.getTextLocale(), this.f590a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else {
                return ObjectsCompat.a(Float.valueOf(this.f590a.getTextSize()), Float.valueOf(this.f590a.getTextScaleX()), Float.valueOf(this.f590a.getTextSkewX()), Integer.valueOf(this.f590a.getFlags()), this.f590a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("{");
            sb.append("textSize=" + this.f590a.getTextSize());
            sb.append(", textScaleX=" + this.f590a.getTextScaleX());
            sb.append(", textSkewX=" + this.f590a.getTextSkewX());
            if (Build.VERSION.SDK_INT >= 21) {
                sb.append(", letterSpacing=" + this.f590a.getLetterSpacing());
                sb.append(", elegantTextHeight=" + this.f590a.isElegantTextHeight());
            }
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                sb.append(", textLocale=" + this.f590a.getTextLocales());
            } else if (i >= 17) {
                sb.append(", textLocale=" + this.f590a.getTextLocale());
            }
            sb.append(", typeface=" + this.f590a.getTypeface());
            if (Build.VERSION.SDK_INT >= 26) {
                sb.append(", variationSettings=" + this.f590a.getFontVariationSettings());
            }
            sb.append(", textDir=" + this.b);
            sb.append(", breakStrategy=" + this.c);
            sb.append(", hyphenationFrequency=" + this.d);
            sb.append("}");
            return sb.toString();
        }

        public boolean a(Params params) {
            PrecomputedText.Params params2 = this.e;
            if (params2 != null) {
                return params2.equals(params.e);
            }
            if ((Build.VERSION.SDK_INT >= 23 && (this.c != params.a() || this.d != params.b())) || this.f590a.getTextSize() != params.d().getTextSize() || this.f590a.getTextScaleX() != params.d().getTextScaleX() || this.f590a.getTextSkewX() != params.d().getTextSkewX()) {
                return false;
            }
            if ((Build.VERSION.SDK_INT >= 21 && (this.f590a.getLetterSpacing() != params.d().getLetterSpacing() || !TextUtils.equals(this.f590a.getFontFeatureSettings(), params.d().getFontFeatureSettings()))) || this.f590a.getFlags() != params.d().getFlags()) {
                return false;
            }
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                if (!this.f590a.getTextLocales().equals(params.d().getTextLocales())) {
                    return false;
                }
            } else if (i >= 17 && !this.f590a.getTextLocale().equals(params.d().getTextLocale())) {
                return false;
            }
            if (this.f590a.getTypeface() == null) {
                if (params.d().getTypeface() != null) {
                    return false;
                }
                return true;
            } else if (!this.f590a.getTypeface().equals(params.d().getTypeface())) {
                return false;
            } else {
                return true;
            }
        }

        public Params(PrecomputedText.Params params) {
            this.f590a = params.getTextPaint();
            this.b = params.getTextDirection();
            this.c = params.getBreakStrategy();
            this.d = params.getHyphenationFrequency();
        }
    }

    public Params a() {
        return this.b;
    }

    public char charAt(int i) {
        return this.f589a.charAt(i);
    }

    public int getSpanEnd(Object obj) {
        return this.f589a.getSpanEnd(obj);
    }

    public int getSpanFlags(Object obj) {
        return this.f589a.getSpanFlags(obj);
    }

    public int getSpanStart(Object obj) {
        return this.f589a.getSpanStart(obj);
    }

    public <T> T[] getSpans(int i, int i2, Class<T> cls) {
        return this.f589a.getSpans(i, i2, cls);
    }

    public int length() {
        return this.f589a.length();
    }

    public int nextSpanTransition(int i, int i2, Class cls) {
        return this.f589a.nextSpanTransition(i, i2, cls);
    }

    public void removeSpan(Object obj) {
        if (!(obj instanceof MetricAffectingSpan)) {
            this.f589a.removeSpan(obj);
            return;
        }
        throw new IllegalArgumentException("MetricAffectingSpan can not be removed from PrecomputedText.");
    }

    public void setSpan(Object obj, int i, int i2, int i3) {
        if (!(obj instanceof MetricAffectingSpan)) {
            this.f589a.setSpan(obj, i, i2, i3);
            return;
        }
        throw new IllegalArgumentException("MetricAffectingSpan can not be set to PrecomputedText.");
    }

    public CharSequence subSequence(int i, int i2) {
        return this.f589a.subSequence(i, i2);
    }

    public String toString() {
        return this.f589a.toString();
    }
}
