package androidx.core.text;

import android.text.SpannableStringBuilder;
import java.util.Locale;

public final class BidiFormatter {
    static final TextDirectionHeuristicCompat d = TextDirectionHeuristicsCompat.c;
    private static final String e = Character.toString(8206);
    private static final String f = Character.toString(8207);
    static final BidiFormatter g = new BidiFormatter(false, 2, d);
    static final BidiFormatter h = new BidiFormatter(true, 2, d);

    /* renamed from: a  reason: collision with root package name */
    private final boolean f585a;
    private final int b;
    private final TextDirectionHeuristicCompat c;

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private boolean f586a;
        private int b;
        private TextDirectionHeuristicCompat c;

        public Builder() {
            b(BidiFormatter.a(Locale.getDefault()));
        }

        private static BidiFormatter a(boolean z) {
            return z ? BidiFormatter.h : BidiFormatter.g;
        }

        private void b(boolean z) {
            this.f586a = z;
            this.c = BidiFormatter.d;
            this.b = 2;
        }

        public BidiFormatter a() {
            if (this.b == 2 && this.c == BidiFormatter.d) {
                return a(this.f586a);
            }
            return new BidiFormatter(this.f586a, this.b, this.c);
        }
    }

    private static class DirectionalityEstimator {
        private static final byte[] f = new byte[1792];

        /* renamed from: a  reason: collision with root package name */
        private final CharSequence f587a;
        private final boolean b;
        private final int c;
        private int d;
        private char e;

        static {
            for (int i = 0; i < 1792; i++) {
                f[i] = Character.getDirectionality(i);
            }
        }

        DirectionalityEstimator(CharSequence charSequence, boolean z) {
            this.f587a = charSequence;
            this.b = z;
            this.c = charSequence.length();
        }

        private static byte a(char c2) {
            return c2 < 1792 ? f[c2] : Character.getDirectionality(c2);
        }

        private byte e() {
            char c2;
            int i = this.d;
            do {
                int i2 = this.d;
                if (i2 <= 0) {
                    break;
                }
                CharSequence charSequence = this.f587a;
                int i3 = i2 - 1;
                this.d = i3;
                this.e = charSequence.charAt(i3);
                c2 = this.e;
                if (c2 == '&') {
                    return 12;
                }
            } while (c2 != ';');
            this.d = i;
            this.e = ';';
            return 13;
        }

        private byte f() {
            char charAt;
            do {
                int i = this.d;
                if (i >= this.c) {
                    return 12;
                }
                CharSequence charSequence = this.f587a;
                this.d = i + 1;
                charAt = charSequence.charAt(i);
                this.e = charAt;
            } while (charAt != ';');
            return 12;
        }

        private byte g() {
            char charAt;
            int i = this.d;
            while (true) {
                int i2 = this.d;
                if (i2 <= 0) {
                    break;
                }
                CharSequence charSequence = this.f587a;
                int i3 = i2 - 1;
                this.d = i3;
                this.e = charSequence.charAt(i3);
                char c2 = this.e;
                if (c2 == '<') {
                    return 12;
                }
                if (c2 == '>') {
                    break;
                } else if (c2 == '\"' || c2 == '\'') {
                    char c3 = this.e;
                    do {
                        int i4 = this.d;
                        if (i4 <= 0) {
                            break;
                        }
                        CharSequence charSequence2 = this.f587a;
                        int i5 = i4 - 1;
                        this.d = i5;
                        charAt = charSequence2.charAt(i5);
                        this.e = charAt;
                    } while (charAt != c3);
                }
            }
            this.d = i;
            this.e = '>';
            return 13;
        }

        private byte h() {
            char charAt;
            int i = this.d;
            while (true) {
                int i2 = this.d;
                if (i2 < this.c) {
                    CharSequence charSequence = this.f587a;
                    this.d = i2 + 1;
                    this.e = charSequence.charAt(i2);
                    char c2 = this.e;
                    if (c2 == '>') {
                        return 12;
                    }
                    if (c2 == '\"' || c2 == '\'') {
                        char c3 = this.e;
                        do {
                            int i3 = this.d;
                            if (i3 >= this.c) {
                                break;
                            }
                            CharSequence charSequence2 = this.f587a;
                            this.d = i3 + 1;
                            charAt = charSequence2.charAt(i3);
                            this.e = charAt;
                        } while (charAt != c3);
                    }
                } else {
                    this.d = i;
                    this.e = '<';
                    return 13;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public byte b() {
            this.e = this.f587a.charAt(this.d);
            if (Character.isHighSurrogate(this.e)) {
                int codePointAt = Character.codePointAt(this.f587a, this.d);
                this.d += Character.charCount(codePointAt);
                return Character.getDirectionality(codePointAt);
            }
            this.d++;
            byte a2 = a(this.e);
            if (!this.b) {
                return a2;
            }
            char c2 = this.e;
            if (c2 == '<') {
                return h();
            }
            return c2 == '&' ? f() : a2;
        }

        /* access modifiers changed from: package-private */
        public int c() {
            this.d = 0;
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (this.d < this.c && i == 0) {
                byte b2 = b();
                if (b2 != 0) {
                    if (b2 == 1 || b2 == 2) {
                        if (i3 == 0) {
                            return 1;
                        }
                    } else if (b2 != 9) {
                        switch (b2) {
                            case 14:
                            case 15:
                                i3++;
                                i2 = -1;
                                continue;
                            case 16:
                            case 17:
                                i3++;
                                i2 = 1;
                                continue;
                            case 18:
                                i3--;
                                i2 = 0;
                                continue;
                        }
                    }
                } else if (i3 == 0) {
                    return -1;
                }
                i = i3;
            }
            if (i == 0) {
                return 0;
            }
            if (i2 != 0) {
                return i2;
            }
            while (this.d > 0) {
                switch (a()) {
                    case 14:
                    case 15:
                        if (i == i3) {
                            return -1;
                        }
                        break;
                    case 16:
                    case 17:
                        if (i == i3) {
                            return 1;
                        }
                        break;
                    case 18:
                        i3++;
                        continue;
                }
                i3--;
            }
            return 0;
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x002b, code lost:
            r2 = r2 - 1;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int d() {
            /*
                r7 = this;
                int r0 = r7.c
                r7.d = r0
                r0 = 0
                r1 = 0
                r2 = 0
            L_0x0007:
                int r3 = r7.d
                if (r3 <= 0) goto L_0x003b
                byte r3 = r7.a()
                r4 = -1
                if (r3 == 0) goto L_0x0034
                r5 = 1
                if (r3 == r5) goto L_0x002e
                r6 = 2
                if (r3 == r6) goto L_0x002e
                r6 = 9
                if (r3 == r6) goto L_0x0007
                switch(r3) {
                    case 14: goto L_0x0028;
                    case 15: goto L_0x0028;
                    case 16: goto L_0x0025;
                    case 17: goto L_0x0025;
                    case 18: goto L_0x0022;
                    default: goto L_0x001f;
                }
            L_0x001f:
                if (r1 != 0) goto L_0x0007
                goto L_0x0039
            L_0x0022:
                int r2 = r2 + 1
                goto L_0x0007
            L_0x0025:
                if (r1 != r2) goto L_0x002b
                return r5
            L_0x0028:
                if (r1 != r2) goto L_0x002b
                return r4
            L_0x002b:
                int r2 = r2 + -1
                goto L_0x0007
            L_0x002e:
                if (r2 != 0) goto L_0x0031
                return r5
            L_0x0031:
                if (r1 != 0) goto L_0x0007
                goto L_0x0039
            L_0x0034:
                if (r2 != 0) goto L_0x0037
                return r4
            L_0x0037:
                if (r1 != 0) goto L_0x0007
            L_0x0039:
                r1 = r2
                goto L_0x0007
            L_0x003b:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.core.text.BidiFormatter.DirectionalityEstimator.d():int");
        }

        /* access modifiers changed from: package-private */
        public byte a() {
            this.e = this.f587a.charAt(this.d - 1);
            if (Character.isLowSurrogate(this.e)) {
                int codePointBefore = Character.codePointBefore(this.f587a, this.d);
                this.d -= Character.charCount(codePointBefore);
                return Character.getDirectionality(codePointBefore);
            }
            this.d--;
            byte a2 = a(this.e);
            if (!this.b) {
                return a2;
            }
            char c2 = this.e;
            if (c2 == '>') {
                return g();
            }
            return c2 == ';' ? e() : a2;
        }
    }

    BidiFormatter(boolean z, int i, TextDirectionHeuristicCompat textDirectionHeuristicCompat) {
        this.f585a = z;
        this.b = i;
        this.c = textDirectionHeuristicCompat;
    }

    public static BidiFormatter b() {
        return new Builder().a();
    }

    private static int c(CharSequence charSequence) {
        return new DirectionalityEstimator(charSequence, false).d();
    }

    public boolean a() {
        return (this.b & 2) != 0;
    }

    private String a(CharSequence charSequence, TextDirectionHeuristicCompat textDirectionHeuristicCompat) {
        boolean a2 = textDirectionHeuristicCompat.a(charSequence, 0, charSequence.length());
        if (!this.f585a && (a2 || c(charSequence) == 1)) {
            return e;
        }
        if (this.f585a) {
            return (!a2 || c(charSequence) == -1) ? f : "";
        }
        return "";
    }

    private String b(CharSequence charSequence, TextDirectionHeuristicCompat textDirectionHeuristicCompat) {
        boolean a2 = textDirectionHeuristicCompat.a(charSequence, 0, charSequence.length());
        if (!this.f585a && (a2 || b(charSequence) == 1)) {
            return e;
        }
        if (this.f585a) {
            return (!a2 || b(charSequence) == -1) ? f : "";
        }
        return "";
    }

    private static int b(CharSequence charSequence) {
        return new DirectionalityEstimator(charSequence, false).c();
    }

    public CharSequence a(CharSequence charSequence, TextDirectionHeuristicCompat textDirectionHeuristicCompat, boolean z) {
        if (charSequence == null) {
            return null;
        }
        boolean a2 = textDirectionHeuristicCompat.a(charSequence, 0, charSequence.length());
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (a() && z) {
            spannableStringBuilder.append(b(charSequence, a2 ? TextDirectionHeuristicsCompat.b : TextDirectionHeuristicsCompat.f592a));
        }
        if (a2 != this.f585a) {
            spannableStringBuilder.append(a2 ? (char) 8235 : 8234);
            spannableStringBuilder.append(charSequence);
            spannableStringBuilder.append(8236);
        } else {
            spannableStringBuilder.append(charSequence);
        }
        if (z) {
            spannableStringBuilder.append(a(charSequence, a2 ? TextDirectionHeuristicsCompat.b : TextDirectionHeuristicsCompat.f592a));
        }
        return spannableStringBuilder;
    }

    public CharSequence a(CharSequence charSequence) {
        return a(charSequence, this.c, true);
    }

    static boolean a(Locale locale) {
        return TextUtilsCompat.b(locale) == 1;
    }
}
