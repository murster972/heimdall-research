package androidx.core.text;

import java.util.Locale;

public final class TextDirectionHeuristicsCompat {

    /* renamed from: a  reason: collision with root package name */
    public static final TextDirectionHeuristicCompat f592a = new TextDirectionHeuristicInternal((TextDirectionAlgorithm) null, false);
    public static final TextDirectionHeuristicCompat b = new TextDirectionHeuristicInternal((TextDirectionAlgorithm) null, true);
    public static final TextDirectionHeuristicCompat c = new TextDirectionHeuristicInternal(FirstStrong.f594a, false);
    public static final TextDirectionHeuristicCompat d = new TextDirectionHeuristicInternal(FirstStrong.f594a, true);

    private static class AnyStrong implements TextDirectionAlgorithm {
        static final AnyStrong b = new AnyStrong(true);

        /* renamed from: a  reason: collision with root package name */
        private final boolean f593a;

        private AnyStrong(boolean z) {
            this.f593a = z;
        }

        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = i2 + i;
            boolean z = false;
            while (i < i3) {
                int a2 = TextDirectionHeuristicsCompat.a(Character.getDirectionality(charSequence.charAt(i)));
                if (a2 != 0) {
                    if (a2 != 1) {
                        continue;
                        i++;
                    } else if (!this.f593a) {
                        return 1;
                    }
                } else if (this.f593a) {
                    return 0;
                }
                z = true;
                i++;
            }
            if (z) {
                return this.f593a ? 1 : 0;
            }
            return 2;
        }
    }

    private static class FirstStrong implements TextDirectionAlgorithm {

        /* renamed from: a  reason: collision with root package name */
        static final FirstStrong f594a = new FirstStrong();

        private FirstStrong() {
        }

        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = i2 + i;
            int i4 = 2;
            while (i < i3 && i4 == 2) {
                i4 = TextDirectionHeuristicsCompat.b(Character.getDirectionality(charSequence.charAt(i)));
                i++;
            }
            return i4;
        }
    }

    private interface TextDirectionAlgorithm {
        int a(CharSequence charSequence, int i, int i2);
    }

    private static abstract class TextDirectionHeuristicImpl implements TextDirectionHeuristicCompat {

        /* renamed from: a  reason: collision with root package name */
        private final TextDirectionAlgorithm f595a;

        TextDirectionHeuristicImpl(TextDirectionAlgorithm textDirectionAlgorithm) {
            this.f595a = textDirectionAlgorithm;
        }

        private boolean b(CharSequence charSequence, int i, int i2) {
            int a2 = this.f595a.a(charSequence, i, i2);
            if (a2 == 0) {
                return true;
            }
            if (a2 != 1) {
                return a();
            }
            return false;
        }

        /* access modifiers changed from: protected */
        public abstract boolean a();

        public boolean a(CharSequence charSequence, int i, int i2) {
            if (charSequence == null || i < 0 || i2 < 0 || charSequence.length() - i2 < i) {
                throw new IllegalArgumentException();
            } else if (this.f595a == null) {
                return a();
            } else {
                return b(charSequence, i, i2);
            }
        }
    }

    private static class TextDirectionHeuristicInternal extends TextDirectionHeuristicImpl {
        private final boolean b;

        TextDirectionHeuristicInternal(TextDirectionAlgorithm textDirectionAlgorithm, boolean z) {
            super(textDirectionAlgorithm);
            this.b = z;
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return this.b;
        }
    }

    private static class TextDirectionHeuristicLocale extends TextDirectionHeuristicImpl {
        static final TextDirectionHeuristicLocale b = new TextDirectionHeuristicLocale();

        TextDirectionHeuristicLocale() {
            super((TextDirectionAlgorithm) null);
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return TextUtilsCompat.b(Locale.getDefault()) == 1;
        }
    }

    static {
        new TextDirectionHeuristicInternal(AnyStrong.b, false);
        TextDirectionHeuristicLocale textDirectionHeuristicLocale = TextDirectionHeuristicLocale.b;
    }

    private TextDirectionHeuristicsCompat() {
    }

    static int a(int i) {
        if (i != 0) {
            return (i == 1 || i == 2) ? 0 : 2;
        }
        return 1;
    }

    static int b(int i) {
        if (i != 0) {
            if (i == 1 || i == 2) {
                return 0;
            }
            switch (i) {
                case 14:
                case 15:
                    break;
                case 16:
                case 17:
                    return 0;
                default:
                    return 2;
            }
        }
        return 1;
    }
}
