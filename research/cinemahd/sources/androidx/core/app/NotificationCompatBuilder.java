package androidx.core.app;

import android.app.Notification;
import android.app.RemoteInput;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;
import android.widget.RemoteViews;
import androidx.core.app.NotificationCompat;
import com.google.android.gms.ads.AdRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class NotificationCompatBuilder implements NotificationBuilderWithBuilderAccessor {

    /* renamed from: a  reason: collision with root package name */
    private final Notification.Builder f540a;
    private final NotificationCompat.Builder b;
    private RemoteViews c;
    private RemoteViews d;
    private final List<Bundle> e = new ArrayList();
    private final Bundle f = new Bundle();
    private int g;
    private RemoteViews h;

    NotificationCompatBuilder(NotificationCompat.Builder builder) {
        ArrayList<String> arrayList;
        this.b = builder;
        if (Build.VERSION.SDK_INT >= 26) {
            this.f540a = new Notification.Builder(builder.f538a, builder.I);
        } else {
            this.f540a = new Notification.Builder(builder.f538a);
        }
        Notification notification = builder.N;
        this.f540a.setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, builder.h).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(builder.d).setContentText(builder.e).setContentInfo(builder.j).setContentIntent(builder.f).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(builder.g, (notification.flags & 128) != 0).setLargeIcon(builder.i).setNumber(builder.k).setProgress(builder.r, builder.s, builder.t);
        if (Build.VERSION.SDK_INT < 21) {
            this.f540a.setSound(notification.sound, notification.audioStreamType);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            this.f540a.setSubText(builder.p).setUsesChronometer(builder.n).setPriority(builder.l);
            Iterator<NotificationCompat.Action> it2 = builder.b.iterator();
            while (it2.hasNext()) {
                a(it2.next());
            }
            Bundle bundle = builder.B;
            if (bundle != null) {
                this.f.putAll(bundle);
            }
            if (Build.VERSION.SDK_INT < 20) {
                if (builder.x) {
                    this.f.putBoolean("android.support.localOnly", true);
                }
                String str = builder.u;
                if (str != null) {
                    this.f.putString("android.support.groupKey", str);
                    if (builder.v) {
                        this.f.putBoolean("android.support.isGroupSummary", true);
                    } else {
                        this.f.putBoolean("android.support.useSideChannel", true);
                    }
                }
                String str2 = builder.w;
                if (str2 != null) {
                    this.f.putString("android.support.sortKey", str2);
                }
            }
            this.c = builder.F;
            this.d = builder.G;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            this.f540a.setShowWhen(builder.m);
            if (Build.VERSION.SDK_INT < 21 && (arrayList = builder.O) != null && !arrayList.isEmpty()) {
                Bundle bundle2 = this.f;
                ArrayList<String> arrayList2 = builder.O;
                bundle2.putStringArray("android.people", (String[]) arrayList2.toArray(new String[arrayList2.size()]));
            }
        }
        if (Build.VERSION.SDK_INT >= 20) {
            this.f540a.setLocalOnly(builder.x).setGroup(builder.u).setGroupSummary(builder.v).setSortKey(builder.w);
            this.g = builder.M;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            this.f540a.setCategory(builder.A).setColor(builder.C).setVisibility(builder.D).setPublicVersion(builder.E).setSound(notification.sound, notification.audioAttributes);
            Iterator<String> it3 = builder.O.iterator();
            while (it3.hasNext()) {
                this.f540a.addPerson(it3.next());
            }
            this.h = builder.H;
            if (builder.c.size() > 0) {
                Bundle bundle3 = builder.c().getBundle("android.car.EXTENSIONS");
                bundle3 = bundle3 == null ? new Bundle() : bundle3;
                Bundle bundle4 = new Bundle();
                for (int i = 0; i < builder.c.size(); i++) {
                    bundle4.putBundle(Integer.toString(i), NotificationCompatJellybean.a(builder.c.get(i)));
                }
                bundle3.putBundle("invisible_actions", bundle4);
                builder.c().putBundle("android.car.EXTENSIONS", bundle3);
                this.f.putBundle("android.car.EXTENSIONS", bundle3);
            }
        }
        if (Build.VERSION.SDK_INT >= 24) {
            this.f540a.setExtras(builder.B).setRemoteInputHistory(builder.q);
            RemoteViews remoteViews = builder.F;
            if (remoteViews != null) {
                this.f540a.setCustomContentView(remoteViews);
            }
            RemoteViews remoteViews2 = builder.G;
            if (remoteViews2 != null) {
                this.f540a.setCustomBigContentView(remoteViews2);
            }
            RemoteViews remoteViews3 = builder.H;
            if (remoteViews3 != null) {
                this.f540a.setCustomHeadsUpContentView(remoteViews3);
            }
        }
        if (Build.VERSION.SDK_INT >= 26) {
            this.f540a.setBadgeIconType(builder.J).setShortcutId(builder.K).setTimeoutAfter(builder.L).setGroupAlertBehavior(builder.M);
            if (builder.z) {
                this.f540a.setColorized(builder.y);
            }
            if (!TextUtils.isEmpty(builder.I)) {
                this.f540a.setSound((Uri) null).setDefaults(0).setLights(0, 0, 0).setVibrate((long[]) null);
            }
        }
    }

    public Notification.Builder a() {
        return this.f540a;
    }

    public Notification b() {
        Bundle a2;
        RemoteViews d2;
        RemoteViews b2;
        NotificationCompat.Style style = this.b.o;
        if (style != null) {
            style.a((NotificationBuilderWithBuilderAccessor) this);
        }
        RemoteViews c2 = style != null ? style.c(this) : null;
        Notification c3 = c();
        if (c2 != null) {
            c3.contentView = c2;
        } else {
            RemoteViews remoteViews = this.b.F;
            if (remoteViews != null) {
                c3.contentView = remoteViews;
            }
        }
        if (!(Build.VERSION.SDK_INT < 16 || style == null || (b2 = style.b(this)) == null)) {
            c3.bigContentView = b2;
        }
        if (!(Build.VERSION.SDK_INT < 21 || style == null || (d2 = this.b.o.d(this)) == null)) {
            c3.headsUpContentView = d2;
        }
        if (!(Build.VERSION.SDK_INT < 16 || style == null || (a2 = NotificationCompat.a(c3)) == null)) {
            style.a(a2);
        }
        return c3;
    }

    /* access modifiers changed from: protected */
    public Notification c() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            return this.f540a.build();
        }
        if (i >= 24) {
            Notification build = this.f540a.build();
            if (this.g != 0) {
                if (!(build.getGroup() == null || (build.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 || this.g != 2)) {
                    a(build);
                }
                if (build.getGroup() != null && (build.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 && this.g == 1) {
                    a(build);
                }
            }
            return build;
        } else if (i >= 21) {
            this.f540a.setExtras(this.f);
            Notification build2 = this.f540a.build();
            RemoteViews remoteViews = this.c;
            if (remoteViews != null) {
                build2.contentView = remoteViews;
            }
            RemoteViews remoteViews2 = this.d;
            if (remoteViews2 != null) {
                build2.bigContentView = remoteViews2;
            }
            RemoteViews remoteViews3 = this.h;
            if (remoteViews3 != null) {
                build2.headsUpContentView = remoteViews3;
            }
            if (this.g != 0) {
                if (!(build2.getGroup() == null || (build2.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 || this.g != 2)) {
                    a(build2);
                }
                if (build2.getGroup() != null && (build2.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 && this.g == 1) {
                    a(build2);
                }
            }
            return build2;
        } else if (i >= 20) {
            this.f540a.setExtras(this.f);
            Notification build3 = this.f540a.build();
            RemoteViews remoteViews4 = this.c;
            if (remoteViews4 != null) {
                build3.contentView = remoteViews4;
            }
            RemoteViews remoteViews5 = this.d;
            if (remoteViews5 != null) {
                build3.bigContentView = remoteViews5;
            }
            if (this.g != 0) {
                if (!(build3.getGroup() == null || (build3.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 || this.g != 2)) {
                    a(build3);
                }
                if (build3.getGroup() != null && (build3.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 && this.g == 1) {
                    a(build3);
                }
            }
            return build3;
        } else if (i >= 19) {
            SparseArray<Bundle> a2 = NotificationCompatJellybean.a(this.e);
            if (a2 != null) {
                this.f.putSparseParcelableArray("android.support.actionExtras", a2);
            }
            this.f540a.setExtras(this.f);
            Notification build4 = this.f540a.build();
            RemoteViews remoteViews6 = this.c;
            if (remoteViews6 != null) {
                build4.contentView = remoteViews6;
            }
            RemoteViews remoteViews7 = this.d;
            if (remoteViews7 != null) {
                build4.bigContentView = remoteViews7;
            }
            return build4;
        } else if (i < 16) {
            return this.f540a.getNotification();
        } else {
            Notification build5 = this.f540a.build();
            Bundle a3 = NotificationCompat.a(build5);
            Bundle bundle = new Bundle(this.f);
            for (String str : this.f.keySet()) {
                if (a3.containsKey(str)) {
                    bundle.remove(str);
                }
            }
            a3.putAll(bundle);
            SparseArray<Bundle> a4 = NotificationCompatJellybean.a(this.e);
            if (a4 != null) {
                NotificationCompat.a(build5).putSparseParcelableArray("android.support.actionExtras", a4);
            }
            RemoteViews remoteViews8 = this.c;
            if (remoteViews8 != null) {
                build5.contentView = remoteViews8;
            }
            RemoteViews remoteViews9 = this.d;
            if (remoteViews9 != null) {
                build5.bigContentView = remoteViews9;
            }
            return build5;
        }
    }

    private void a(NotificationCompat.Action action) {
        Bundle bundle;
        int i = Build.VERSION.SDK_INT;
        if (i >= 20) {
            Notification.Action.Builder builder = new Notification.Action.Builder(action.e(), action.i(), action.a());
            if (action.f() != null) {
                for (RemoteInput addRemoteInput : RemoteInput.a(action.f())) {
                    builder.addRemoteInput(addRemoteInput);
                }
            }
            if (action.d() != null) {
                bundle = new Bundle(action.d());
            } else {
                bundle = new Bundle();
            }
            bundle.putBoolean("android.support.allowGeneratedReplies", action.b());
            if (Build.VERSION.SDK_INT >= 24) {
                builder.setAllowGeneratedReplies(action.b());
            }
            bundle.putInt("android.support.action.semanticAction", action.g());
            if (Build.VERSION.SDK_INT >= 28) {
                builder.setSemanticAction(action.g());
            }
            bundle.putBoolean("android.support.action.showsUserInterface", action.h());
            builder.addExtras(bundle);
            this.f540a.addAction(builder.build());
        } else if (i >= 16) {
            this.e.add(NotificationCompatJellybean.a(this.f540a, action));
        }
    }

    private void a(Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        notification.defaults &= -2;
        notification.defaults &= -3;
    }
}
