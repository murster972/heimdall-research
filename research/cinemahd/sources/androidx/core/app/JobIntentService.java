package androidx.core.app;

import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobServiceEngine;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class JobIntentService extends Service {
    static final Object g = new Object();
    static final HashMap<ComponentName, WorkEnqueuer> h = new HashMap<>();

    /* renamed from: a  reason: collision with root package name */
    CompatJobEngine f530a;
    WorkEnqueuer b;
    CommandProcessor c;
    boolean d = false;
    boolean e = false;
    final ArrayList<CompatWorkItem> f;

    interface CompatJobEngine {
        IBinder a();

        GenericWorkItem b();
    }

    final class CompatWorkItem implements GenericWorkItem {

        /* renamed from: a  reason: collision with root package name */
        final Intent f532a;
        final int b;

        CompatWorkItem(Intent intent, int i) {
            this.f532a = intent;
            this.b = i;
        }

        public void a() {
            JobIntentService.this.stopSelf(this.b);
        }

        public Intent getIntent() {
            return this.f532a;
        }
    }

    interface GenericWorkItem {
        void a();

        Intent getIntent();
    }

    static final class JobServiceEngineImpl extends JobServiceEngine implements CompatJobEngine {

        /* renamed from: a  reason: collision with root package name */
        final JobIntentService f533a;
        final Object b = new Object();
        JobParameters c;

        final class WrapperWorkItem implements GenericWorkItem {

            /* renamed from: a  reason: collision with root package name */
            final JobWorkItem f534a;

            WrapperWorkItem(JobWorkItem jobWorkItem) {
                this.f534a = jobWorkItem;
            }

            public void a() {
                synchronized (JobServiceEngineImpl.this.b) {
                    if (JobServiceEngineImpl.this.c != null) {
                        JobServiceEngineImpl.this.c.completeWork(this.f534a);
                    }
                }
            }

            public Intent getIntent() {
                return this.f534a.getIntent();
            }
        }

        JobServiceEngineImpl(JobIntentService jobIntentService) {
            super(jobIntentService);
            this.f533a = jobIntentService;
        }

        public IBinder a() {
            return getBinder();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
            r1.getIntent().setExtrasClassLoader(r3.f533a.getClassLoader());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
            return new androidx.core.app.JobIntentService.JobServiceEngineImpl.WrapperWorkItem(r3, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
            if (r1 == null) goto L_0x0026;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public androidx.core.app.JobIntentService.GenericWorkItem b() {
            /*
                r3 = this;
                java.lang.Object r0 = r3.b
                monitor-enter(r0)
                android.app.job.JobParameters r1 = r3.c     // Catch:{ all -> 0x0027 }
                r2 = 0
                if (r1 != 0) goto L_0x000a
                monitor-exit(r0)     // Catch:{ all -> 0x0027 }
                return r2
            L_0x000a:
                android.app.job.JobParameters r1 = r3.c     // Catch:{ all -> 0x0027 }
                android.app.job.JobWorkItem r1 = r1.dequeueWork()     // Catch:{ all -> 0x0027 }
                monitor-exit(r0)     // Catch:{ all -> 0x0027 }
                if (r1 == 0) goto L_0x0026
                android.content.Intent r0 = r1.getIntent()
                androidx.core.app.JobIntentService r2 = r3.f533a
                java.lang.ClassLoader r2 = r2.getClassLoader()
                r0.setExtrasClassLoader(r2)
                androidx.core.app.JobIntentService$JobServiceEngineImpl$WrapperWorkItem r0 = new androidx.core.app.JobIntentService$JobServiceEngineImpl$WrapperWorkItem
                r0.<init>(r1)
                return r0
            L_0x0026:
                return r2
            L_0x0027:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0027 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.core.app.JobIntentService.JobServiceEngineImpl.b():androidx.core.app.JobIntentService$GenericWorkItem");
        }

        public boolean onStartJob(JobParameters jobParameters) {
            this.c = jobParameters;
            this.f533a.a(false);
            return true;
        }

        public boolean onStopJob(JobParameters jobParameters) {
            boolean b2 = this.f533a.b();
            synchronized (this.b) {
                this.c = null;
            }
            return b2;
        }
    }

    static final class JobWorkEnqueuer extends WorkEnqueuer {
        private final JobInfo d;
        private final JobScheduler e;

        JobWorkEnqueuer(Context context, ComponentName componentName, int i) {
            super(componentName);
            a(i);
            this.d = new JobInfo.Builder(i, this.f535a).setOverrideDeadline(0).build();
            this.e = (JobScheduler) context.getApplicationContext().getSystemService("jobscheduler");
        }

        /* access modifiers changed from: package-private */
        public void a(Intent intent) {
            this.e.enqueue(this.d, new JobWorkItem(intent));
        }
    }

    static abstract class WorkEnqueuer {

        /* renamed from: a  reason: collision with root package name */
        final ComponentName f535a;
        boolean b;
        int c;

        WorkEnqueuer(ComponentName componentName) {
            this.f535a = componentName;
        }

        public void a() {
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            if (!this.b) {
                this.b = true;
                this.c = i;
            } else if (this.c != i) {
                throw new IllegalArgumentException("Given job ID " + i + " is different than previous " + this.c);
            }
        }

        /* access modifiers changed from: package-private */
        public abstract void a(Intent intent);

        public void b() {
        }

        public void c() {
        }
    }

    public JobIntentService() {
        if (Build.VERSION.SDK_INT >= 26) {
            this.f = null;
        } else {
            this.f = new ArrayList<>();
        }
    }

    public static void a(Context context, Class cls, int i, Intent intent) {
        a(context, new ComponentName(context, cls), i, intent);
    }

    /* access modifiers changed from: protected */
    public abstract void a(Intent intent);

    /* access modifiers changed from: package-private */
    public boolean b() {
        CommandProcessor commandProcessor = this.c;
        if (commandProcessor != null) {
            commandProcessor.cancel(this.d);
        }
        return c();
    }

    public boolean c() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        ArrayList<CompatWorkItem> arrayList = this.f;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.c = null;
                if (this.f != null && this.f.size() > 0) {
                    a(false);
                } else if (!this.e) {
                    this.b.a();
                }
            }
        }
    }

    public IBinder onBind(Intent intent) {
        CompatJobEngine compatJobEngine = this.f530a;
        if (compatJobEngine != null) {
            return compatJobEngine.a();
        }
        return null;
    }

    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            this.f530a = new JobServiceEngineImpl(this);
            this.b = null;
            return;
        }
        this.f530a = null;
        this.b = a((Context) this, new ComponentName(this, getClass()), false, 0);
    }

    public void onDestroy() {
        super.onDestroy();
        ArrayList<CompatWorkItem> arrayList = this.f;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.e = true;
                this.b.a();
            }
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (this.f == null) {
            return 2;
        }
        this.b.c();
        synchronized (this.f) {
            ArrayList<CompatWorkItem> arrayList = this.f;
            if (intent == null) {
                intent = new Intent();
            }
            arrayList.add(new CompatWorkItem(intent, i2));
            a(true);
        }
        return 3;
    }

    public static void a(Context context, ComponentName componentName, int i, Intent intent) {
        if (intent != null) {
            synchronized (g) {
                WorkEnqueuer a2 = a(context, componentName, true, i);
                a2.a(i);
                a2.a(intent);
            }
            return;
        }
        throw new IllegalArgumentException("work must not be null");
    }

    final class CommandProcessor extends AsyncTask<Void, Void, Void> {
        CommandProcessor() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            while (true) {
                GenericWorkItem a2 = JobIntentService.this.a();
                if (a2 == null) {
                    return null;
                }
                JobIntentService.this.a(a2.getIntent());
                a2.a();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void onPostExecute(Void voidR) {
            JobIntentService.this.d();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onCancelled(Void voidR) {
            JobIntentService.this.d();
        }
    }

    static WorkEnqueuer a(Context context, ComponentName componentName, boolean z, int i) {
        WorkEnqueuer workEnqueuer;
        WorkEnqueuer workEnqueuer2 = h.get(componentName);
        if (workEnqueuer2 != null) {
            return workEnqueuer2;
        }
        if (Build.VERSION.SDK_INT < 26) {
            workEnqueuer = new CompatWorkEnqueuer(context, componentName);
        } else if (z) {
            workEnqueuer = new JobWorkEnqueuer(context, componentName, i);
        } else {
            throw new IllegalArgumentException("Can't be here without a job id");
        }
        WorkEnqueuer workEnqueuer3 = workEnqueuer;
        h.put(componentName, workEnqueuer3);
        return workEnqueuer3;
    }

    static final class CompatWorkEnqueuer extends WorkEnqueuer {
        private final Context d;
        private final PowerManager.WakeLock e;
        private final PowerManager.WakeLock f;
        boolean g;
        boolean h;

        CompatWorkEnqueuer(Context context, ComponentName componentName) {
            super(componentName);
            this.d = context.getApplicationContext();
            PowerManager powerManager = (PowerManager) context.getSystemService("power");
            this.e = powerManager.newWakeLock(1, componentName.getClassName() + ":launch");
            this.e.setReferenceCounted(false);
            this.f = powerManager.newWakeLock(1, componentName.getClassName() + ":run");
            this.f.setReferenceCounted(false);
        }

        /* access modifiers changed from: package-private */
        public void a(Intent intent) {
            Intent intent2 = new Intent(intent);
            intent2.setComponent(this.f535a);
            if (this.d.startService(intent2) != null) {
                synchronized (this) {
                    if (!this.g) {
                        this.g = true;
                        if (!this.h) {
                            this.e.acquire(60000);
                        }
                    }
                }
            }
        }

        public void b() {
            synchronized (this) {
                if (!this.h) {
                    this.h = true;
                    this.f.acquire(600000);
                    this.e.release();
                }
            }
        }

        public void c() {
            synchronized (this) {
                this.g = false;
            }
        }

        public void a() {
            synchronized (this) {
                if (this.h) {
                    if (this.g) {
                        this.e.acquire(60000);
                    }
                    this.h = false;
                    this.f.release();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (this.c == null) {
            this.c = new CommandProcessor();
            WorkEnqueuer workEnqueuer = this.b;
            if (workEnqueuer != null && z) {
                workEnqueuer.b();
            }
            this.c.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    /* access modifiers changed from: package-private */
    public GenericWorkItem a() {
        CompatJobEngine compatJobEngine = this.f530a;
        if (compatJobEngine != null) {
            return compatJobEngine.b();
        }
        synchronized (this.f) {
            if (this.f.size() <= 0) {
                return null;
            }
            GenericWorkItem remove = this.f.remove(0);
            return remove;
        }
    }
}
