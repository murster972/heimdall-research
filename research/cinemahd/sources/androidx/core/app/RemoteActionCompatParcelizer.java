package androidx.core.app;

import android.app.PendingIntent;
import android.os.Parcelable;
import androidx.core.graphics.drawable.IconCompat;
import androidx.versionedparcelable.VersionedParcel;
import androidx.versionedparcelable.VersionedParcelable;

public class RemoteActionCompatParcelizer {
    public static RemoteActionCompat read(VersionedParcel versionedParcel) {
        RemoteActionCompat remoteActionCompat = new RemoteActionCompat();
        remoteActionCompat.f547a = (IconCompat) versionedParcel.a(remoteActionCompat.f547a, 1);
        remoteActionCompat.b = versionedParcel.a(remoteActionCompat.b, 2);
        remoteActionCompat.c = versionedParcel.a(remoteActionCompat.c, 3);
        remoteActionCompat.d = (PendingIntent) versionedParcel.a(remoteActionCompat.d, 4);
        remoteActionCompat.e = versionedParcel.a(remoteActionCompat.e, 5);
        remoteActionCompat.f = versionedParcel.a(remoteActionCompat.f, 6);
        return remoteActionCompat;
    }

    public static void write(RemoteActionCompat remoteActionCompat, VersionedParcel versionedParcel) {
        versionedParcel.a(false, false);
        versionedParcel.b((VersionedParcelable) remoteActionCompat.f547a, 1);
        versionedParcel.b(remoteActionCompat.b, 2);
        versionedParcel.b(remoteActionCompat.c, 3);
        versionedParcel.b((Parcelable) remoteActionCompat.d, 4);
        versionedParcel.b(remoteActionCompat.e, 5);
        versionedParcel.b(remoteActionCompat.f, 6);
    }
}
