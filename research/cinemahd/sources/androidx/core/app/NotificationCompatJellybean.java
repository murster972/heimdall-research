package androidx.core.app;

import android.app.Notification;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import androidx.core.app.NotificationCompat;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class NotificationCompatJellybean {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f541a = new Object();
    private static Field b;
    private static boolean c;

    private NotificationCompatJellybean() {
    }

    public static SparseArray<Bundle> a(List<Bundle> list) {
        int size = list.size();
        SparseArray<Bundle> sparseArray = null;
        for (int i = 0; i < size; i++) {
            Bundle bundle = list.get(i);
            if (bundle != null) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray<>();
                }
                sparseArray.put(i, bundle);
            }
        }
        return sparseArray;
    }

    public static Bundle a(Notification notification) {
        synchronized (f541a) {
            if (c) {
                return null;
            }
            try {
                if (b == null) {
                    Field declaredField = Notification.class.getDeclaredField("extras");
                    if (!Bundle.class.isAssignableFrom(declaredField.getType())) {
                        Log.e("NotificationCompat", "Notification.extras field is not of type Bundle");
                        c = true;
                        return null;
                    }
                    declaredField.setAccessible(true);
                    b = declaredField;
                }
                Bundle bundle = (Bundle) b.get(notification);
                if (bundle == null) {
                    bundle = new Bundle();
                    b.set(notification, bundle);
                }
                return bundle;
            } catch (IllegalAccessException e) {
                Log.e("NotificationCompat", "Unable to access notification extras", e);
                c = true;
                return null;
            } catch (NoSuchFieldException e2) {
                Log.e("NotificationCompat", "Unable to access notification extras", e2);
                c = true;
                return null;
            }
        }
    }

    public static Bundle a(Notification.Builder builder, NotificationCompat.Action action) {
        builder.addAction(action.e(), action.i(), action.a());
        Bundle bundle = new Bundle(action.d());
        if (action.f() != null) {
            bundle.putParcelableArray("android.support.remoteInputs", a(action.f()));
        }
        if (action.c() != null) {
            bundle.putParcelableArray("android.support.dataRemoteInputs", a(action.c()));
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", action.b());
        return bundle;
    }

    static Bundle a(NotificationCompat.Action action) {
        Bundle bundle;
        Bundle bundle2 = new Bundle();
        bundle2.putInt("icon", action.e());
        bundle2.putCharSequence("title", action.i());
        bundle2.putParcelable("actionIntent", action.a());
        if (action.d() != null) {
            bundle = new Bundle(action.d());
        } else {
            bundle = new Bundle();
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", action.b());
        bundle2.putBundle("extras", bundle);
        bundle2.putParcelableArray("remoteInputs", a(action.f()));
        bundle2.putBoolean("showsUserInterface", action.h());
        bundle2.putInt("semanticAction", action.g());
        return bundle2;
    }

    private static Bundle a(RemoteInput remoteInput) {
        Bundle bundle = new Bundle();
        bundle.putString("resultKey", remoteInput.f());
        bundle.putCharSequence("label", remoteInput.e());
        bundle.putCharSequenceArray("choices", remoteInput.c());
        bundle.putBoolean("allowFreeFormInput", remoteInput.a());
        bundle.putBundle("extras", remoteInput.d());
        Set<String> b2 = remoteInput.b();
        if (b2 != null && !b2.isEmpty()) {
            ArrayList arrayList = new ArrayList(b2.size());
            for (String add : b2) {
                arrayList.add(add);
            }
            bundle.putStringArrayList("allowedDataTypes", arrayList);
        }
        return bundle;
    }

    private static Bundle[] a(RemoteInput[] remoteInputArr) {
        if (remoteInputArr == null) {
            return null;
        }
        Bundle[] bundleArr = new Bundle[remoteInputArr.length];
        for (int i = 0; i < remoteInputArr.length; i++) {
            bundleArr[i] = a(remoteInputArr[i]);
        }
        return bundleArr;
    }
}
