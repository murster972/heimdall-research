package androidx.core.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class NotificationManagerCompat {
    private static final Object c = new Object();
    private static String d;
    private static Set<String> e = new HashSet();
    private static final Object f = new Object();
    private static SideChannelManager g;

    /* renamed from: a  reason: collision with root package name */
    private final Context f542a;
    private final NotificationManager b = ((NotificationManager) this.f542a.getSystemService("notification"));

    private static class NotifyTask implements Task {

        /* renamed from: a  reason: collision with root package name */
        final String f543a;
        final int b;
        final String c;
        final Notification d;

        NotifyTask(String str, int i, String str2, Notification notification) {
            this.f543a = str;
            this.b = i;
            this.c = str2;
            this.d = notification;
        }

        public void a(INotificationSideChannel iNotificationSideChannel) throws RemoteException {
            iNotificationSideChannel.notify(this.f543a, this.b, this.c, this.d);
        }

        public String toString() {
            return "NotifyTask[" + "packageName:" + this.f543a + ", id:" + this.b + ", tag:" + this.c + "]";
        }
    }

    private static class ServiceConnectedEvent {

        /* renamed from: a  reason: collision with root package name */
        final ComponentName f544a;
        final IBinder b;

        ServiceConnectedEvent(ComponentName componentName, IBinder iBinder) {
            this.f544a = componentName;
            this.b = iBinder;
        }
    }

    private static class SideChannelManager implements Handler.Callback, ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        private final Context f545a;
        private final HandlerThread b;
        private final Handler c;
        private final Map<ComponentName, ListenerRecord> d = new HashMap();
        private Set<String> e = new HashSet();

        private static class ListenerRecord {

            /* renamed from: a  reason: collision with root package name */
            final ComponentName f546a;
            boolean b = false;
            INotificationSideChannel c;
            ArrayDeque<Task> d = new ArrayDeque<>();
            int e = 0;

            ListenerRecord(ComponentName componentName) {
                this.f546a = componentName;
            }
        }

        SideChannelManager(Context context) {
            this.f545a = context;
            this.b = new HandlerThread("NotificationManagerCompat");
            this.b.start();
            this.c = new Handler(this.b.getLooper(), this);
        }

        private void b(Task task) {
            a();
            for (ListenerRecord next : this.d.values()) {
                next.d.add(task);
                c(next);
            }
        }

        private void c(ListenerRecord listenerRecord) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Processing component " + listenerRecord.f546a + ", " + listenerRecord.d.size() + " queued tasks");
            }
            if (!listenerRecord.d.isEmpty()) {
                if (!a(listenerRecord) || listenerRecord.c == null) {
                    d(listenerRecord);
                    return;
                }
                while (true) {
                    Task peek = listenerRecord.d.peek();
                    if (peek == null) {
                        break;
                    }
                    try {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Sending task " + peek);
                        }
                        peek.a(listenerRecord.c);
                        listenerRecord.d.remove();
                    } catch (DeadObjectException unused) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Remote service has died: " + listenerRecord.f546a);
                        }
                    } catch (RemoteException e2) {
                        Log.w("NotifManCompat", "RemoteException communicating with " + listenerRecord.f546a, e2);
                    }
                }
                if (!listenerRecord.d.isEmpty()) {
                    d(listenerRecord);
                }
            }
        }

        private void d(ListenerRecord listenerRecord) {
            if (!this.c.hasMessages(3, listenerRecord.f546a)) {
                listenerRecord.e++;
                int i = listenerRecord.e;
                if (i > 6) {
                    Log.w("NotifManCompat", "Giving up on delivering " + listenerRecord.d.size() + " tasks to " + listenerRecord.f546a + " after " + listenerRecord.e + " retries");
                    listenerRecord.d.clear();
                    return;
                }
                int i2 = (1 << (i - 1)) * 1000;
                if (Log.isLoggable("NotifManCompat", 3)) {
                    Log.d("NotifManCompat", "Scheduling retry for " + i2 + " ms");
                }
                this.c.sendMessageDelayed(this.c.obtainMessage(3, listenerRecord.f546a), (long) i2);
            }
        }

        public void a(Task task) {
            this.c.obtainMessage(0, task).sendToTarget();
        }

        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                b((Task) message.obj);
                return true;
            } else if (i == 1) {
                ServiceConnectedEvent serviceConnectedEvent = (ServiceConnectedEvent) message.obj;
                a(serviceConnectedEvent.f544a, serviceConnectedEvent.b);
                return true;
            } else if (i == 2) {
                b((ComponentName) message.obj);
                return true;
            } else if (i != 3) {
                return false;
            } else {
                a((ComponentName) message.obj);
                return true;
            }
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Connected to service " + componentName);
            }
            this.c.obtainMessage(1, new ServiceConnectedEvent(componentName, iBinder)).sendToTarget();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Disconnected from service " + componentName);
            }
            this.c.obtainMessage(2, componentName).sendToTarget();
        }

        private void a(ComponentName componentName, IBinder iBinder) {
            ListenerRecord listenerRecord = this.d.get(componentName);
            if (listenerRecord != null) {
                listenerRecord.c = INotificationSideChannel.Stub.asInterface(iBinder);
                listenerRecord.e = 0;
                c(listenerRecord);
            }
        }

        private void b(ComponentName componentName) {
            ListenerRecord listenerRecord = this.d.get(componentName);
            if (listenerRecord != null) {
                b(listenerRecord);
            }
        }

        private void a(ComponentName componentName) {
            ListenerRecord listenerRecord = this.d.get(componentName);
            if (listenerRecord != null) {
                c(listenerRecord);
            }
        }

        private void b(ListenerRecord listenerRecord) {
            if (listenerRecord.b) {
                this.f545a.unbindService(this);
                listenerRecord.b = false;
            }
            listenerRecord.c = null;
        }

        private void a() {
            Set<String> b2 = NotificationManagerCompat.b(this.f545a);
            if (!b2.equals(this.e)) {
                this.e = b2;
                List<ResolveInfo> queryIntentServices = this.f545a.getPackageManager().queryIntentServices(new Intent().setAction("android.support.BIND_NOTIFICATION_SIDE_CHANNEL"), 0);
                HashSet<ComponentName> hashSet = new HashSet<>();
                for (ResolveInfo next : queryIntentServices) {
                    if (b2.contains(next.serviceInfo.packageName)) {
                        ComponentName componentName = new ComponentName(next.serviceInfo.packageName, next.serviceInfo.name);
                        if (next.serviceInfo.permission != null) {
                            Log.w("NotifManCompat", "Permission present on component " + componentName + ", not adding listener record.");
                        } else {
                            hashSet.add(componentName);
                        }
                    }
                }
                for (ComponentName componentName2 : hashSet) {
                    if (!this.d.containsKey(componentName2)) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Adding listener record for " + componentName2);
                        }
                        this.d.put(componentName2, new ListenerRecord(componentName2));
                    }
                }
                Iterator<Map.Entry<ComponentName, ListenerRecord>> it2 = this.d.entrySet().iterator();
                while (it2.hasNext()) {
                    Map.Entry next2 = it2.next();
                    if (!hashSet.contains(next2.getKey())) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Removing listener record for " + next2.getKey());
                        }
                        b((ListenerRecord) next2.getValue());
                        it2.remove();
                    }
                }
            }
        }

        private boolean a(ListenerRecord listenerRecord) {
            if (listenerRecord.b) {
                return true;
            }
            listenerRecord.b = this.f545a.bindService(new Intent("android.support.BIND_NOTIFICATION_SIDE_CHANNEL").setComponent(listenerRecord.f546a), this, 33);
            if (listenerRecord.b) {
                listenerRecord.e = 0;
            } else {
                Log.w("NotifManCompat", "Unable to bind to listener " + listenerRecord.f546a);
                this.f545a.unbindService(this);
            }
            return listenerRecord.b;
        }
    }

    private interface Task {
        void a(INotificationSideChannel iNotificationSideChannel) throws RemoteException;
    }

    private NotificationManagerCompat(Context context) {
        this.f542a = context;
    }

    public static NotificationManagerCompat a(Context context) {
        return new NotificationManagerCompat(context);
    }

    public static Set<String> b(Context context) {
        Set<String> set;
        String string = Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
        synchronized (c) {
            if (string != null) {
                if (!string.equals(d)) {
                    String[] split = string.split(":", -1);
                    HashSet hashSet = new HashSet(split.length);
                    for (String unflattenFromString : split) {
                        ComponentName unflattenFromString2 = ComponentName.unflattenFromString(unflattenFromString);
                        if (unflattenFromString2 != null) {
                            hashSet.add(unflattenFromString2.getPackageName());
                        }
                    }
                    e = hashSet;
                    d = string;
                }
            }
            set = e;
        }
        return set;
    }

    public void a(int i, Notification notification) {
        a((String) null, i, notification);
    }

    public void a(String str, int i, Notification notification) {
        if (a(notification)) {
            a((Task) new NotifyTask(this.f542a.getPackageName(), i, str, notification));
            this.b.cancel(str, i);
            return;
        }
        this.b.notify(str, i, notification);
    }

    private static boolean a(Notification notification) {
        Bundle a2 = NotificationCompat.a(notification);
        return a2 != null && a2.getBoolean("android.support.useSideChannel");
    }

    private void a(Task task) {
        synchronized (f) {
            if (g == null) {
                g = new SideChannelManager(this.f542a.getApplicationContext());
            }
            g.a(task);
        }
    }
}
