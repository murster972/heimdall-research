package androidx.core.app;

import android.app.RemoteInput;
import android.os.Bundle;
import java.util.Set;

public final class RemoteInput {

    /* renamed from: a  reason: collision with root package name */
    private final String f548a;
    private final CharSequence b;
    private final CharSequence[] c;
    private final boolean d;
    private final Bundle e;
    private final Set<String> f;

    public boolean a() {
        return this.d;
    }

    public Set<String> b() {
        return this.f;
    }

    public CharSequence[] c() {
        return this.c;
    }

    public Bundle d() {
        return this.e;
    }

    public CharSequence e() {
        return this.b;
    }

    public String f() {
        return this.f548a;
    }

    public boolean g() {
        return !a() && (c() == null || c().length == 0) && b() != null && !b().isEmpty();
    }

    static android.app.RemoteInput[] a(RemoteInput[] remoteInputArr) {
        if (remoteInputArr == null) {
            return null;
        }
        android.app.RemoteInput[] remoteInputArr2 = new android.app.RemoteInput[remoteInputArr.length];
        for (int i = 0; i < remoteInputArr.length; i++) {
            remoteInputArr2[i] = a(remoteInputArr[i]);
        }
        return remoteInputArr2;
    }

    static android.app.RemoteInput a(RemoteInput remoteInput) {
        return new RemoteInput.Builder(remoteInput.f()).setLabel(remoteInput.e()).setChoices(remoteInput.c()).setAllowFreeFormInput(remoteInput.a()).addExtras(remoteInput.d()).build();
    }
}
