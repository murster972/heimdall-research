package androidx.core.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import androidx.core.R$dimen;
import androidx.core.R$drawable;
import java.util.ArrayList;
import java.util.Arrays;

public class NotificationCompat {

    public static class Action {

        /* renamed from: a  reason: collision with root package name */
        final Bundle f536a;
        private final RemoteInput[] b;
        private final RemoteInput[] c;
        private boolean d;
        boolean e;
        private final int f;
        public int g;
        public CharSequence h;
        public PendingIntent i;

        public static final class Builder {

            /* renamed from: a  reason: collision with root package name */
            private final int f537a;
            private final CharSequence b;
            private final PendingIntent c;
            private boolean d;
            private final Bundle e;
            private ArrayList<RemoteInput> f;
            private int g;
            private boolean h;

            public Builder(int i, CharSequence charSequence, PendingIntent pendingIntent) {
                this(i, charSequence, pendingIntent, new Bundle(), (RemoteInput[]) null, true, 0, true);
            }

            /* JADX WARNING: type inference failed for: r0v5, types: [java.lang.Object[]] */
            /* JADX WARNING: Multi-variable type inference failed */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public androidx.core.app.NotificationCompat.Action a() {
                /*
                    r14 = this;
                    java.util.ArrayList r0 = new java.util.ArrayList
                    r0.<init>()
                    java.util.ArrayList r1 = new java.util.ArrayList
                    r1.<init>()
                    java.util.ArrayList<androidx.core.app.RemoteInput> r2 = r14.f
                    if (r2 == 0) goto L_0x002c
                    java.util.Iterator r2 = r2.iterator()
                L_0x0012:
                    boolean r3 = r2.hasNext()
                    if (r3 == 0) goto L_0x002c
                    java.lang.Object r3 = r2.next()
                    androidx.core.app.RemoteInput r3 = (androidx.core.app.RemoteInput) r3
                    boolean r4 = r3.g()
                    if (r4 == 0) goto L_0x0028
                    r0.add(r3)
                    goto L_0x0012
                L_0x0028:
                    r1.add(r3)
                    goto L_0x0012
                L_0x002c:
                    boolean r2 = r0.isEmpty()
                    r3 = 0
                    if (r2 == 0) goto L_0x0035
                    r10 = r3
                    goto L_0x0042
                L_0x0035:
                    int r2 = r0.size()
                    androidx.core.app.RemoteInput[] r2 = new androidx.core.app.RemoteInput[r2]
                    java.lang.Object[] r0 = r0.toArray(r2)
                    androidx.core.app.RemoteInput[] r0 = (androidx.core.app.RemoteInput[]) r0
                    r10 = r0
                L_0x0042:
                    boolean r0 = r1.isEmpty()
                    if (r0 == 0) goto L_0x0049
                    goto L_0x0056
                L_0x0049:
                    int r0 = r1.size()
                    androidx.core.app.RemoteInput[] r0 = new androidx.core.app.RemoteInput[r0]
                    java.lang.Object[] r0 = r1.toArray(r0)
                    r3 = r0
                    androidx.core.app.RemoteInput[] r3 = (androidx.core.app.RemoteInput[]) r3
                L_0x0056:
                    r9 = r3
                    androidx.core.app.NotificationCompat$Action r0 = new androidx.core.app.NotificationCompat$Action
                    int r5 = r14.f537a
                    java.lang.CharSequence r6 = r14.b
                    android.app.PendingIntent r7 = r14.c
                    android.os.Bundle r8 = r14.e
                    boolean r11 = r14.d
                    int r12 = r14.g
                    boolean r13 = r14.h
                    r4 = r0
                    r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13)
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: androidx.core.app.NotificationCompat.Action.Builder.a():androidx.core.app.NotificationCompat$Action");
            }

            private Builder(int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, RemoteInput[] remoteInputArr, boolean z, int i2, boolean z2) {
                ArrayList<RemoteInput> arrayList;
                this.d = true;
                this.h = true;
                this.f537a = i;
                this.b = Builder.d(charSequence);
                this.c = pendingIntent;
                this.e = bundle;
                if (remoteInputArr == null) {
                    arrayList = null;
                } else {
                    arrayList = new ArrayList<>(Arrays.asList(remoteInputArr));
                }
                this.f = arrayList;
                this.d = z;
                this.g = i2;
                this.h = z2;
            }
        }

        public Action(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this(i2, charSequence, pendingIntent, new Bundle(), (RemoteInput[]) null, (RemoteInput[]) null, true, 0, true);
        }

        public PendingIntent a() {
            return this.i;
        }

        public boolean b() {
            return this.d;
        }

        public RemoteInput[] c() {
            return this.c;
        }

        public Bundle d() {
            return this.f536a;
        }

        public int e() {
            return this.g;
        }

        public RemoteInput[] f() {
            return this.b;
        }

        public int g() {
            return this.f;
        }

        public boolean h() {
            return this.e;
        }

        public CharSequence i() {
            return this.h;
        }

        Action(int i2, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, RemoteInput[] remoteInputArr, RemoteInput[] remoteInputArr2, boolean z, int i3, boolean z2) {
            this.e = true;
            this.g = i2;
            this.h = Builder.d(charSequence);
            this.i = pendingIntent;
            this.f536a = bundle == null ? new Bundle() : bundle;
            this.b = remoteInputArr;
            this.c = remoteInputArr2;
            this.d = z;
            this.f = i3;
            this.e = z2;
        }
    }

    public static class BigTextStyle extends Style {
        private CharSequence e;

        public BigTextStyle a(CharSequence charSequence) {
            this.e = Builder.d(charSequence);
            return this;
        }

        public void a(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigTextStyle bigText = new Notification.BigTextStyle(notificationBuilderWithBuilderAccessor.a()).setBigContentTitle(this.b).bigText(this.e);
                if (this.d) {
                    bigText.setSummaryText(this.c);
                }
            }
        }
    }

    public static class Builder {
        String A;
        Bundle B;
        int C;
        int D;
        Notification E;
        RemoteViews F;
        RemoteViews G;
        RemoteViews H;
        String I;
        int J;
        String K;
        long L;
        int M;
        Notification N;
        @Deprecated
        public ArrayList<String> O;

        /* renamed from: a  reason: collision with root package name */
        public Context f538a;
        public ArrayList<Action> b;
        ArrayList<Action> c;
        CharSequence d;
        CharSequence e;
        PendingIntent f;
        PendingIntent g;
        RemoteViews h;
        Bitmap i;
        CharSequence j;
        int k;
        int l;
        boolean m;
        boolean n;
        Style o;
        CharSequence p;
        CharSequence[] q;
        int r;
        int s;
        boolean t;
        String u;
        boolean v;
        String w;
        boolean x;
        boolean y;
        boolean z;

        public Builder(Context context, String str) {
            this.b = new ArrayList<>();
            this.c = new ArrayList<>();
            this.m = true;
            this.x = false;
            this.C = 0;
            this.D = 0;
            this.J = 0;
            this.M = 0;
            this.N = new Notification();
            this.f538a = context;
            this.I = str;
            this.N.when = System.currentTimeMillis();
            this.N.audioStreamType = -1;
            this.l = 0;
            this.O = new ArrayList<>();
        }

        public Builder a(long j2) {
            this.N.when = j2;
            return this;
        }

        public Builder b(int i2) {
            this.N.icon = i2;
            return this;
        }

        public Builder c(CharSequence charSequence) {
            this.N.tickerText = d(charSequence);
            return this;
        }

        public Builder d(boolean z2) {
            this.m = z2;
            return this;
        }

        public long e() {
            if (this.m) {
                return this.N.when;
            }
            return 0;
        }

        protected static CharSequence d(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }

        public Builder a(CharSequence charSequence) {
            this.e = d(charSequence);
            return this;
        }

        public Builder b(CharSequence charSequence) {
            this.d = d(charSequence);
            return this;
        }

        public Builder c(boolean z2) {
            a(2, z2);
            return this;
        }

        private Bitmap b(Bitmap bitmap) {
            if (bitmap == null || Build.VERSION.SDK_INT >= 27) {
                return bitmap;
            }
            Resources resources = this.f538a.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(R$dimen.compat_notification_large_icon_max_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(R$dimen.compat_notification_large_icon_max_height);
            if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
                return bitmap;
            }
            double min = Math.min(((double) dimensionPixelSize) / ((double) Math.max(1, bitmap.getWidth())), ((double) dimensionPixelSize2) / ((double) Math.max(1, bitmap.getHeight())));
            return Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * min), (int) Math.ceil(((double) bitmap.getHeight()) * min), true);
        }

        public Builder a(PendingIntent pendingIntent) {
            this.f = pendingIntent;
            return this;
        }

        public Bundle c() {
            if (this.B == null) {
                this.B = new Bundle();
            }
            return this.B;
        }

        public Builder a(Bitmap bitmap) {
            this.i = b(bitmap);
            return this;
        }

        public int d() {
            return this.l;
        }

        public Builder a(boolean z2) {
            a(16, z2);
            return this;
        }

        public Builder a(String str) {
            this.A = str;
            return this;
        }

        public Builder c(int i2) {
            this.D = i2;
            return this;
        }

        private void a(int i2, boolean z2) {
            if (z2) {
                Notification notification = this.N;
                notification.flags = i2 | notification.flags;
                return;
            }
            Notification notification2 = this.N;
            notification2.flags = (~i2) & notification2.flags;
        }

        public Builder a(int i2) {
            this.l = i2;
            return this;
        }

        public Builder a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this.b.add(new Action(i2, charSequence, pendingIntent));
            return this;
        }

        public Builder a(Action action) {
            this.b.add(action);
            return this;
        }

        public Builder a(Style style) {
            if (this.o != style) {
                this.o = style;
                Style style2 = this.o;
                if (style2 != null) {
                    style2.a(this);
                }
            }
            return this;
        }

        public Notification a() {
            return new NotificationCompatBuilder(this).b();
        }

        public Builder b(boolean z2) {
            this.x = z2;
            return this;
        }

        @Deprecated
        public Builder(Context context) {
            this(context, (String) null);
        }

        public Builder b(String str) {
            this.I = str;
            return this;
        }

        public int b() {
            return this.C;
        }
    }

    public static Bundle a(Notification notification) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return notification.extras;
        }
        if (i >= 16) {
            return NotificationCompatJellybean.a(notification);
        }
        return null;
    }

    public static abstract class Style {

        /* renamed from: a  reason: collision with root package name */
        protected Builder f539a;
        CharSequence b;
        CharSequence c;
        boolean d = false;

        public void a(Bundle bundle) {
        }

        public void a(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
        }

        public void a(Builder builder) {
            if (this.f539a != builder) {
                this.f539a = builder;
                Builder builder2 = this.f539a;
                if (builder2 != null) {
                    builder2.a(this);
                }
            }
        }

        public RemoteViews b(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }

        public RemoteViews c(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }

        public RemoteViews d(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }

        /* JADX WARNING: Removed duplicated region for block: B:63:0x0183  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x018f  */
        /* JADX WARNING: Removed duplicated region for block: B:70:0x019d  */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x01bf  */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x0204  */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x0206  */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x0210  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.widget.RemoteViews a(boolean r13, int r14, boolean r15) {
            /*
                r12 = this;
                androidx.core.app.NotificationCompat$Builder r0 = r12.f539a
                android.content.Context r0 = r0.f538a
                android.content.res.Resources r0 = r0.getResources()
                android.widget.RemoteViews r7 = new android.widget.RemoteViews
                androidx.core.app.NotificationCompat$Builder r1 = r12.f539a
                android.content.Context r1 = r1.f538a
                java.lang.String r1 = r1.getPackageName()
                r7.<init>(r1, r14)
                androidx.core.app.NotificationCompat$Builder r14 = r12.f539a
                int r14 = r14.d()
                r1 = -1
                r8 = 1
                r9 = 0
                if (r14 >= r1) goto L_0x0022
                r14 = 1
                goto L_0x0023
            L_0x0022:
                r14 = 0
            L_0x0023:
                int r2 = android.os.Build.VERSION.SDK_INT
                r3 = 21
                r10 = 16
                if (r2 < r10) goto L_0x004e
                if (r2 >= r3) goto L_0x004e
                java.lang.String r2 = "setBackgroundResource"
                if (r14 == 0) goto L_0x0040
                int r14 = androidx.core.R$id.notification_background
                int r4 = androidx.core.R$drawable.notification_bg_low
                r7.setInt(r14, r2, r4)
                int r14 = androidx.core.R$id.icon
                int r4 = androidx.core.R$drawable.notification_template_icon_low_bg
                r7.setInt(r14, r2, r4)
                goto L_0x004e
            L_0x0040:
                int r14 = androidx.core.R$id.notification_background
                int r4 = androidx.core.R$drawable.notification_bg
                r7.setInt(r14, r2, r4)
                int r14 = androidx.core.R$id.icon
                int r4 = androidx.core.R$drawable.notification_template_icon_bg
                r7.setInt(r14, r2, r4)
            L_0x004e:
                androidx.core.app.NotificationCompat$Builder r14 = r12.f539a
                android.graphics.Bitmap r2 = r14.i
                r11 = 8
                if (r2 == 0) goto L_0x00b5
                int r14 = android.os.Build.VERSION.SDK_INT
                if (r14 < r10) goto L_0x0069
                int r14 = androidx.core.R$id.icon
                r7.setViewVisibility(r14, r9)
                int r14 = androidx.core.R$id.icon
                androidx.core.app.NotificationCompat$Builder r2 = r12.f539a
                android.graphics.Bitmap r2 = r2.i
                r7.setImageViewBitmap(r14, r2)
                goto L_0x006e
            L_0x0069:
                int r14 = androidx.core.R$id.icon
                r7.setViewVisibility(r14, r11)
            L_0x006e:
                if (r13 == 0) goto L_0x00fc
                androidx.core.app.NotificationCompat$Builder r13 = r12.f539a
                android.app.Notification r13 = r13.N
                int r13 = r13.icon
                if (r13 == 0) goto L_0x00fc
                int r13 = androidx.core.R$dimen.notification_right_icon_size
                int r13 = r0.getDimensionPixelSize(r13)
                int r14 = androidx.core.R$dimen.notification_small_icon_background_padding
                int r14 = r0.getDimensionPixelSize(r14)
                int r14 = r14 * 2
                int r14 = r13 - r14
                int r2 = android.os.Build.VERSION.SDK_INT
                if (r2 < r3) goto L_0x00a0
                androidx.core.app.NotificationCompat$Builder r1 = r12.f539a
                android.app.Notification r2 = r1.N
                int r2 = r2.icon
                int r1 = r1.b()
                android.graphics.Bitmap r13 = r12.a(r2, r13, r14, r1)
                int r14 = androidx.core.R$id.right_icon
                r7.setImageViewBitmap(r14, r13)
                goto L_0x00af
            L_0x00a0:
                int r13 = androidx.core.R$id.right_icon
                androidx.core.app.NotificationCompat$Builder r14 = r12.f539a
                android.app.Notification r14 = r14.N
                int r14 = r14.icon
                android.graphics.Bitmap r14 = r12.a(r14, r1)
                r7.setImageViewBitmap(r13, r14)
            L_0x00af:
                int r13 = androidx.core.R$id.right_icon
                r7.setViewVisibility(r13, r9)
                goto L_0x00fc
            L_0x00b5:
                if (r13 == 0) goto L_0x00fc
                android.app.Notification r13 = r14.N
                int r13 = r13.icon
                if (r13 == 0) goto L_0x00fc
                int r13 = androidx.core.R$id.icon
                r7.setViewVisibility(r13, r9)
                int r13 = android.os.Build.VERSION.SDK_INT
                if (r13 < r3) goto L_0x00ed
                int r13 = androidx.core.R$dimen.notification_large_icon_width
                int r13 = r0.getDimensionPixelSize(r13)
                int r14 = androidx.core.R$dimen.notification_big_circle_margin
                int r14 = r0.getDimensionPixelSize(r14)
                int r13 = r13 - r14
                int r14 = androidx.core.R$dimen.notification_small_icon_size_as_large
                int r14 = r0.getDimensionPixelSize(r14)
                androidx.core.app.NotificationCompat$Builder r1 = r12.f539a
                android.app.Notification r2 = r1.N
                int r2 = r2.icon
                int r1 = r1.b()
                android.graphics.Bitmap r13 = r12.a(r2, r13, r14, r1)
                int r14 = androidx.core.R$id.icon
                r7.setImageViewBitmap(r14, r13)
                goto L_0x00fc
            L_0x00ed:
                int r13 = androidx.core.R$id.icon
                androidx.core.app.NotificationCompat$Builder r14 = r12.f539a
                android.app.Notification r14 = r14.N
                int r14 = r14.icon
                android.graphics.Bitmap r14 = r12.a(r14, r1)
                r7.setImageViewBitmap(r13, r14)
            L_0x00fc:
                androidx.core.app.NotificationCompat$Builder r13 = r12.f539a
                java.lang.CharSequence r13 = r13.d
                if (r13 == 0) goto L_0x0107
                int r14 = androidx.core.R$id.title
                r7.setTextViewText(r14, r13)
            L_0x0107:
                androidx.core.app.NotificationCompat$Builder r13 = r12.f539a
                java.lang.CharSequence r13 = r13.e
                if (r13 == 0) goto L_0x0114
                int r14 = androidx.core.R$id.text
                r7.setTextViewText(r14, r13)
                r13 = 1
                goto L_0x0115
            L_0x0114:
                r13 = 0
            L_0x0115:
                int r14 = android.os.Build.VERSION.SDK_INT
                if (r14 >= r3) goto L_0x0121
                androidx.core.app.NotificationCompat$Builder r14 = r12.f539a
                android.graphics.Bitmap r14 = r14.i
                if (r14 == 0) goto L_0x0121
                r14 = 1
                goto L_0x0122
            L_0x0121:
                r14 = 0
            L_0x0122:
                androidx.core.app.NotificationCompat$Builder r1 = r12.f539a
                java.lang.CharSequence r2 = r1.j
                if (r2 == 0) goto L_0x0135
                int r13 = androidx.core.R$id.f520info
                r7.setTextViewText(r13, r2)
                int r13 = androidx.core.R$id.f520info
                r7.setViewVisibility(r13, r9)
            L_0x0132:
                r13 = 1
                r14 = 1
                goto L_0x016e
            L_0x0135:
                int r1 = r1.k
                if (r1 <= 0) goto L_0x0169
                int r13 = androidx.core.R$integer.status_bar_notification_info_maxnum
                int r13 = r0.getInteger(r13)
                androidx.core.app.NotificationCompat$Builder r14 = r12.f539a
                int r14 = r14.k
                if (r14 <= r13) goto L_0x0151
                int r13 = androidx.core.R$id.f520info
                int r14 = androidx.core.R$string.status_bar_notification_info_overflow
                java.lang.String r14 = r0.getString(r14)
                r7.setTextViewText(r13, r14)
                goto L_0x0163
            L_0x0151:
                java.text.NumberFormat r13 = java.text.NumberFormat.getIntegerInstance()
                int r14 = androidx.core.R$id.f520info
                androidx.core.app.NotificationCompat$Builder r1 = r12.f539a
                int r1 = r1.k
                long r1 = (long) r1
                java.lang.String r13 = r13.format(r1)
                r7.setTextViewText(r14, r13)
            L_0x0163:
                int r13 = androidx.core.R$id.f520info
                r7.setViewVisibility(r13, r9)
                goto L_0x0132
            L_0x0169:
                int r1 = androidx.core.R$id.f520info
                r7.setViewVisibility(r1, r11)
            L_0x016e:
                androidx.core.app.NotificationCompat$Builder r1 = r12.f539a
                java.lang.CharSequence r1 = r1.p
                if (r1 == 0) goto L_0x0194
                int r2 = android.os.Build.VERSION.SDK_INT
                if (r2 < r10) goto L_0x0194
                int r2 = androidx.core.R$id.text
                r7.setTextViewText(r2, r1)
                androidx.core.app.NotificationCompat$Builder r1 = r12.f539a
                java.lang.CharSequence r1 = r1.e
                if (r1 == 0) goto L_0x018f
                int r2 = androidx.core.R$id.text2
                r7.setTextViewText(r2, r1)
                int r1 = androidx.core.R$id.text2
                r7.setViewVisibility(r1, r9)
                r1 = 1
                goto L_0x0195
            L_0x018f:
                int r1 = androidx.core.R$id.text2
                r7.setViewVisibility(r1, r11)
            L_0x0194:
                r1 = 0
            L_0x0195:
                if (r1 == 0) goto L_0x01b3
                int r1 = android.os.Build.VERSION.SDK_INT
                if (r1 < r10) goto L_0x01b3
                if (r15 == 0) goto L_0x01a9
                int r15 = androidx.core.R$dimen.notification_subtext_size
                int r15 = r0.getDimensionPixelSize(r15)
                float r15 = (float) r15
                int r0 = androidx.core.R$id.text
                r7.setTextViewTextSize(r0, r9, r15)
            L_0x01a9:
                int r2 = androidx.core.R$id.line1
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r1 = r7
                r1.setViewPadding(r2, r3, r4, r5, r6)
            L_0x01b3:
                androidx.core.app.NotificationCompat$Builder r15 = r12.f539a
                long r0 = r15.e()
                r2 = 0
                int r15 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r15 == 0) goto L_0x0200
                androidx.core.app.NotificationCompat$Builder r14 = r12.f539a
                boolean r14 = r14.n
                if (r14 == 0) goto L_0x01ed
                int r14 = android.os.Build.VERSION.SDK_INT
                if (r14 < r10) goto L_0x01ed
                int r14 = androidx.core.R$id.chronometer
                r7.setViewVisibility(r14, r9)
                int r14 = androidx.core.R$id.chronometer
                androidx.core.app.NotificationCompat$Builder r15 = r12.f539a
                long r0 = r15.e()
                long r2 = android.os.SystemClock.elapsedRealtime()
                long r4 = java.lang.System.currentTimeMillis()
                long r2 = r2 - r4
                long r0 = r0 + r2
                java.lang.String r15 = "setBase"
                r7.setLong(r14, r15, r0)
                int r14 = androidx.core.R$id.chronometer
                java.lang.String r15 = "setStarted"
                r7.setBoolean(r14, r15, r8)
                goto L_0x01ff
            L_0x01ed:
                int r14 = androidx.core.R$id.time
                r7.setViewVisibility(r14, r9)
                int r14 = androidx.core.R$id.time
                androidx.core.app.NotificationCompat$Builder r15 = r12.f539a
                long r0 = r15.e()
                java.lang.String r15 = "setTime"
                r7.setLong(r14, r15, r0)
            L_0x01ff:
                r14 = 1
            L_0x0200:
                int r15 = androidx.core.R$id.right_side
                if (r14 == 0) goto L_0x0206
                r14 = 0
                goto L_0x0208
            L_0x0206:
                r14 = 8
            L_0x0208:
                r7.setViewVisibility(r15, r14)
                int r14 = androidx.core.R$id.line3
                if (r13 == 0) goto L_0x0210
                goto L_0x0212
            L_0x0210:
                r9 = 8
            L_0x0212:
                r7.setViewVisibility(r14, r9)
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.core.app.NotificationCompat.Style.a(boolean, int, boolean):android.widget.RemoteViews");
        }

        public Bitmap a(int i, int i2) {
            return a(i, i2, 0);
        }

        private Bitmap a(int i, int i2, int i3) {
            Drawable drawable = this.f539a.f538a.getResources().getDrawable(i);
            int intrinsicWidth = i3 == 0 ? drawable.getIntrinsicWidth() : i3;
            if (i3 == 0) {
                i3 = drawable.getIntrinsicHeight();
            }
            Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, i3, Bitmap.Config.ARGB_8888);
            drawable.setBounds(0, 0, intrinsicWidth, i3);
            if (i2 != 0) {
                drawable.mutate().setColorFilter(new PorterDuffColorFilter(i2, PorterDuff.Mode.SRC_IN));
            }
            drawable.draw(new Canvas(createBitmap));
            return createBitmap;
        }

        private Bitmap a(int i, int i2, int i3, int i4) {
            int i5 = R$drawable.notification_icon_background;
            if (i4 == 0) {
                i4 = 0;
            }
            Bitmap a2 = a(i5, i4, i2);
            Canvas canvas = new Canvas(a2);
            Drawable mutate = this.f539a.f538a.getResources().getDrawable(i).mutate();
            mutate.setFilterBitmap(true);
            int i6 = (i2 - i3) / 2;
            int i7 = i3 + i6;
            mutate.setBounds(i6, i6, i7, i7);
            mutate.setColorFilter(new PorterDuffColorFilter(-1, PorterDuff.Mode.SRC_ATOP));
            mutate.draw(canvas);
            return a2;
        }
    }
}
