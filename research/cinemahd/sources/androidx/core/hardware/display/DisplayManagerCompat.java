package androidx.core.hardware.display;

import android.content.Context;
import java.util.WeakHashMap;

public final class DisplayManagerCompat {

    /* renamed from: a  reason: collision with root package name */
    private static final WeakHashMap<Context, DisplayManagerCompat> f570a = new WeakHashMap<>();

    private DisplayManagerCompat(Context context) {
    }

    public static DisplayManagerCompat a(Context context) {
        DisplayManagerCompat displayManagerCompat;
        synchronized (f570a) {
            displayManagerCompat = f570a.get(context);
            if (displayManagerCompat == null) {
                displayManagerCompat = new DisplayManagerCompat(context);
                f570a.put(context, displayManagerCompat);
            }
        }
        return displayManagerCompat;
    }
}
