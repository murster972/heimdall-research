package androidx.core;

public final class R$attr {
    public static final int alpha = 2130968627;
    public static final int font = 2130968899;
    public static final int fontProviderAuthority = 2130968901;
    public static final int fontProviderCerts = 2130968902;
    public static final int fontProviderFetchStrategy = 2130968903;
    public static final int fontProviderFetchTimeout = 2130968904;
    public static final int fontProviderPackage = 2130968905;
    public static final int fontProviderQuery = 2130968906;
    public static final int fontStyle = 2130968907;
    public static final int fontVariationSettings = 2130968908;
    public static final int fontWeight = 2130968909;
    public static final int ttcIndex = 2130969333;

    private R$attr() {
    }
}
