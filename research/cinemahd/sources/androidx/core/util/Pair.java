package androidx.core.util;

public class Pair<F, S> {

    /* renamed from: a  reason: collision with root package name */
    public final F f598a;
    public final S b;

    public Pair(F f, S s) {
        this.f598a = f;
        this.b = s;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Pair)) {
            return false;
        }
        Pair pair = (Pair) obj;
        if (!ObjectsCompat.a(pair.f598a, this.f598a) || !ObjectsCompat.a(pair.b, this.b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        F f = this.f598a;
        int i = 0;
        int hashCode = f == null ? 0 : f.hashCode();
        S s = this.b;
        if (s != null) {
            i = s.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        return "Pair{" + String.valueOf(this.f598a) + " " + String.valueOf(this.b) + "}";
    }
}
