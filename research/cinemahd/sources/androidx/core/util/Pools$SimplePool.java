package androidx.core.util;

public class Pools$SimplePool<T> implements Pools$Pool<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Object[] f599a;
    private int b;

    public Pools$SimplePool(int i) {
        if (i > 0) {
            this.f599a = new Object[i];
            return;
        }
        throw new IllegalArgumentException("The max pool size must be > 0");
    }

    private boolean a(T t) {
        for (int i = 0; i < this.b; i++) {
            if (this.f599a[i] == t) {
                return true;
            }
        }
        return false;
    }

    public T acquire() {
        int i = this.b;
        if (i <= 0) {
            return null;
        }
        int i2 = i - 1;
        T[] tArr = this.f599a;
        T t = tArr[i2];
        tArr[i2] = null;
        this.b = i - 1;
        return t;
    }

    public boolean release(T t) {
        if (!a(t)) {
            int i = this.b;
            Object[] objArr = this.f599a;
            if (i >= objArr.length) {
                return false;
            }
            objArr[i] = t;
            this.b = i + 1;
            return true;
        }
        throw new IllegalStateException("Already in the pool!");
    }
}
