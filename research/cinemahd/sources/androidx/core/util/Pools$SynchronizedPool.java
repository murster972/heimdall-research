package androidx.core.util;

public class Pools$SynchronizedPool<T> extends Pools$SimplePool<T> {
    private final Object c = new Object();

    public Pools$SynchronizedPool(int i) {
        super(i);
    }

    public T acquire() {
        T acquire;
        synchronized (this.c) {
            acquire = super.acquire();
        }
        return acquire;
    }

    public boolean release(T t) {
        boolean release;
        synchronized (this.c) {
            release = super.release(t);
        }
        return release;
    }
}
