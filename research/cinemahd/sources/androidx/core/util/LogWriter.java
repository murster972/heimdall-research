package androidx.core.util;

import android.util.Log;
import java.io.Writer;

public class LogWriter extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private final String f597a;
    private StringBuilder b = new StringBuilder(128);

    public LogWriter(String str) {
        this.f597a = str;
    }

    private void s() {
        if (this.b.length() > 0) {
            Log.d(this.f597a, this.b.toString());
            StringBuilder sb = this.b;
            sb.delete(0, sb.length());
        }
    }

    public void close() {
        s();
    }

    public void flush() {
        s();
    }

    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                s();
            } else {
                this.b.append(c);
            }
        }
    }
}
