package androidx.core.widget;

import android.os.Build;

public interface AutoSizeableTextView {
    public static final boolean Z = (Build.VERSION.SDK_INT >= 27);
}
