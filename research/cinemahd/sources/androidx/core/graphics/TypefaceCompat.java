package androidx.core.graphics;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import androidx.collection.LruCache;
import androidx.core.content.res.FontResourcesParserCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.provider.FontsContractCompat;

public class TypefaceCompat {

    /* renamed from: a  reason: collision with root package name */
    private static final TypefaceCompatBaseImpl f564a;
    private static final LruCache<String, Typeface> b = new LruCache<>(16);

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            f564a = new TypefaceCompatApi28Impl();
        } else if (i >= 26) {
            f564a = new TypefaceCompatApi26Impl();
        } else if (i >= 24 && TypefaceCompatApi24Impl.a()) {
            f564a = new TypefaceCompatApi24Impl();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f564a = new TypefaceCompatApi21Impl();
        } else {
            f564a = new TypefaceCompatBaseImpl();
        }
    }

    private TypefaceCompat() {
    }

    private static String a(Resources resources, int i, int i2) {
        return resources.getResourcePackageName(i) + "-" + i + "-" + i2;
    }

    public static Typeface b(Resources resources, int i, int i2) {
        return b.get(a(resources, i, i2));
    }

    public static Typeface a(Context context, FontResourcesParserCompat.FamilyResourceEntry familyResourceEntry, Resources resources, int i, int i2, ResourcesCompat.FontCallback fontCallback, Handler handler, boolean z) {
        Typeface typeface;
        if (familyResourceEntry instanceof FontResourcesParserCompat.ProviderResourceEntry) {
            FontResourcesParserCompat.ProviderResourceEntry providerResourceEntry = (FontResourcesParserCompat.ProviderResourceEntry) familyResourceEntry;
            boolean z2 = false;
            if (!z ? fontCallback == null : providerResourceEntry.a() == 0) {
                z2 = true;
            }
            typeface = FontsContractCompat.a(context, providerResourceEntry.b(), fontCallback, handler, z2, z ? providerResourceEntry.c() : -1, i2);
        } else {
            typeface = f564a.a(context, (FontResourcesParserCompat.FontFamilyFilesResourceEntry) familyResourceEntry, resources, i2);
            if (fontCallback != null) {
                if (typeface != null) {
                    fontCallback.a(typeface, handler);
                } else {
                    fontCallback.a(-3, handler);
                }
            }
        }
        if (typeface != null) {
            b.put(a(resources, i, i2), typeface);
        }
        return typeface;
    }

    private static Typeface b(Context context, Typeface typeface, int i) {
        FontResourcesParserCompat.FontFamilyFilesResourceEntry a2 = f564a.a(typeface);
        if (a2 == null) {
            return null;
        }
        return f564a.a(context, a2, context.getResources(), i);
    }

    public static Typeface a(Context context, Resources resources, int i, String str, int i2) {
        Typeface a2 = f564a.a(context, resources, i, str, i2);
        if (a2 != null) {
            b.put(a(resources, i, i2), a2);
        }
        return a2;
    }

    public static Typeface a(Context context, CancellationSignal cancellationSignal, FontsContractCompat.FontInfo[] fontInfoArr, int i) {
        return f564a.a(context, cancellationSignal, fontInfoArr, i);
    }

    public static Typeface a(Context context, Typeface typeface, int i) {
        Typeface b2;
        if (context == null) {
            throw new IllegalArgumentException("Context cannot be null");
        } else if (Build.VERSION.SDK_INT >= 21 || (b2 = b(context, typeface, i)) == null) {
            return Typeface.create(typeface, i);
        } else {
            return b2;
        }
    }
}
