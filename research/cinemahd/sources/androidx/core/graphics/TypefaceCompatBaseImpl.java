package androidx.core.graphics;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.util.Log;
import androidx.core.content.res.FontResourcesParserCompat;
import androidx.core.provider.FontsContractCompat;
import com.facebook.common.statfs.StatFsHelper;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

class TypefaceCompatBaseImpl {

    /* renamed from: a  reason: collision with root package name */
    private ConcurrentHashMap<Long, FontResourcesParserCompat.FontFamilyFilesResourceEntry> f565a = new ConcurrentHashMap<>();

    private interface StyleExtractor<T> {
        boolean a(T t);

        int b(T t);
    }

    TypefaceCompatBaseImpl() {
    }

    private static <T> T a(T[] tArr, int i, StyleExtractor<T> styleExtractor) {
        int i2 = (i & 1) == 0 ? StatFsHelper.DEFAULT_DISK_YELLOW_LEVEL_IN_MB : 700;
        boolean z = (i & 2) != 0;
        T t = null;
        int i3 = Integer.MAX_VALUE;
        for (T t2 : tArr) {
            int abs = (Math.abs(styleExtractor.b(t2) - i2) * 2) + (styleExtractor.a(t2) == z ? 0 : 1);
            if (t == null || i3 > abs) {
                t = t2;
                i3 = abs;
            }
        }
        return t;
    }

    private static long b(Typeface typeface) {
        if (typeface == null) {
            return 0;
        }
        try {
            Field declaredField = Typeface.class.getDeclaredField("native_instance");
            declaredField.setAccessible(true);
            return ((Number) declaredField.get(typeface)).longValue();
        } catch (NoSuchFieldException e) {
            Log.e("TypefaceCompatBaseImpl", "Could not retrieve font from family.", e);
            return 0;
        } catch (IllegalAccessException e2) {
            Log.e("TypefaceCompatBaseImpl", "Could not retrieve font from family.", e2);
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public FontsContractCompat.FontInfo a(FontsContractCompat.FontInfo[] fontInfoArr, int i) {
        return (FontsContractCompat.FontInfo) a(fontInfoArr, i, new StyleExtractor<FontsContractCompat.FontInfo>(this) {
            /* renamed from: a */
            public int b(FontsContractCompat.FontInfo fontInfo) {
                return fontInfo.d();
            }

            /* renamed from: b */
            public boolean a(FontsContractCompat.FontInfo fontInfo) {
                return fontInfo.e();
            }
        });
    }

    /* access modifiers changed from: protected */
    public Typeface a(Context context, InputStream inputStream) {
        File a2 = TypefaceCompatUtil.a(context);
        if (a2 == null) {
            return null;
        }
        try {
            if (!TypefaceCompatUtil.a(a2, inputStream)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(a2.getPath());
            a2.delete();
            return createFromFile;
        } catch (RuntimeException unused) {
            return null;
        } finally {
            a2.delete();
        }
    }

    public Typeface a(Context context, CancellationSignal cancellationSignal, FontsContractCompat.FontInfo[] fontInfoArr, int i) {
        InputStream inputStream;
        InputStream inputStream2 = null;
        if (fontInfoArr.length < 1) {
            return null;
        }
        try {
            inputStream = context.getContentResolver().openInputStream(a(fontInfoArr, i).c());
            try {
                Typeface a2 = a(context, inputStream);
                TypefaceCompatUtil.a((Closeable) inputStream);
                return a2;
            } catch (IOException unused) {
                TypefaceCompatUtil.a((Closeable) inputStream);
                return null;
            } catch (Throwable th) {
                th = th;
                inputStream2 = inputStream;
                TypefaceCompatUtil.a((Closeable) inputStream2);
                throw th;
            }
        } catch (IOException unused2) {
            inputStream = null;
            TypefaceCompatUtil.a((Closeable) inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            TypefaceCompatUtil.a((Closeable) inputStream2);
            throw th;
        }
    }

    private FontResourcesParserCompat.FontFileResourceEntry a(FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamilyFilesResourceEntry, int i) {
        return (FontResourcesParserCompat.FontFileResourceEntry) a(fontFamilyFilesResourceEntry.a(), i, new StyleExtractor<FontResourcesParserCompat.FontFileResourceEntry>(this) {
            /* renamed from: a */
            public int b(FontResourcesParserCompat.FontFileResourceEntry fontFileResourceEntry) {
                return fontFileResourceEntry.e();
            }

            /* renamed from: b */
            public boolean a(FontResourcesParserCompat.FontFileResourceEntry fontFileResourceEntry) {
                return fontFileResourceEntry.f();
            }
        });
    }

    public Typeface a(Context context, FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamilyFilesResourceEntry, Resources resources, int i) {
        FontResourcesParserCompat.FontFileResourceEntry a2 = a(fontFamilyFilesResourceEntry, i);
        if (a2 == null) {
            return null;
        }
        Typeface a3 = TypefaceCompat.a(context, resources, a2.b(), a2.a(), i);
        a(a3, fontFamilyFilesResourceEntry);
        return a3;
    }

    public Typeface a(Context context, Resources resources, int i, String str, int i2) {
        File a2 = TypefaceCompatUtil.a(context);
        if (a2 == null) {
            return null;
        }
        try {
            if (!TypefaceCompatUtil.a(a2, resources, i)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(a2.getPath());
            a2.delete();
            return createFromFile;
        } catch (RuntimeException unused) {
            return null;
        } finally {
            a2.delete();
        }
    }

    /* access modifiers changed from: package-private */
    public FontResourcesParserCompat.FontFamilyFilesResourceEntry a(Typeface typeface) {
        long b = b(typeface);
        if (b == 0) {
            return null;
        }
        return this.f565a.get(Long.valueOf(b));
    }

    private void a(Typeface typeface, FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamilyFilesResourceEntry) {
        long b = b(typeface);
        if (b != 0) {
            this.f565a.put(Long.valueOf(b), fontFamilyFilesResourceEntry);
        }
    }
}
