package androidx.room;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.IMultiInstanceInvalidationCallback;
import androidx.room.IMultiInstanceInvalidationService;
import androidx.room.InvalidationTracker;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

class MultiInstanceInvalidationClient {

    /* renamed from: a  reason: collision with root package name */
    Context f1015a;
    final String b;
    int c;
    final InvalidationTracker d;
    final InvalidationTracker.Observer e;
    IMultiInstanceInvalidationService f;
    final Executor g;
    final IMultiInstanceInvalidationCallback h = new IMultiInstanceInvalidationCallback.Stub() {
        public void a(final String[] strArr) {
            MultiInstanceInvalidationClient.this.g.execute(new Runnable() {
                public void run() {
                    MultiInstanceInvalidationClient.this.d.a(strArr);
                }
            });
        }
    };
    final AtomicBoolean i = new AtomicBoolean(false);
    final ServiceConnection j = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            MultiInstanceInvalidationClient.this.f = IMultiInstanceInvalidationService.Stub.a(iBinder);
            MultiInstanceInvalidationClient multiInstanceInvalidationClient = MultiInstanceInvalidationClient.this;
            multiInstanceInvalidationClient.g.execute(multiInstanceInvalidationClient.k);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            MultiInstanceInvalidationClient multiInstanceInvalidationClient = MultiInstanceInvalidationClient.this;
            multiInstanceInvalidationClient.g.execute(multiInstanceInvalidationClient.l);
            MultiInstanceInvalidationClient multiInstanceInvalidationClient2 = MultiInstanceInvalidationClient.this;
            multiInstanceInvalidationClient2.f = null;
            multiInstanceInvalidationClient2.f1015a = null;
        }
    };
    final Runnable k = new Runnable() {
        public void run() {
            try {
                IMultiInstanceInvalidationService iMultiInstanceInvalidationService = MultiInstanceInvalidationClient.this.f;
                if (iMultiInstanceInvalidationService != null) {
                    MultiInstanceInvalidationClient.this.c = iMultiInstanceInvalidationService.a(MultiInstanceInvalidationClient.this.h, MultiInstanceInvalidationClient.this.b);
                    MultiInstanceInvalidationClient.this.d.a(MultiInstanceInvalidationClient.this.e);
                }
            } catch (RemoteException e) {
                Log.w("ROOM", "Cannot register multi-instance invalidation callback", e);
            }
        }
    };
    final Runnable l = new Runnable() {
        public void run() {
            MultiInstanceInvalidationClient multiInstanceInvalidationClient = MultiInstanceInvalidationClient.this;
            multiInstanceInvalidationClient.d.b(multiInstanceInvalidationClient.e);
        }
    };

    MultiInstanceInvalidationClient(Context context, String str, InvalidationTracker invalidationTracker, Executor executor) {
        new Runnable() {
            public void run() {
                MultiInstanceInvalidationClient multiInstanceInvalidationClient = MultiInstanceInvalidationClient.this;
                multiInstanceInvalidationClient.d.b(multiInstanceInvalidationClient.e);
                try {
                    IMultiInstanceInvalidationService iMultiInstanceInvalidationService = MultiInstanceInvalidationClient.this.f;
                    if (iMultiInstanceInvalidationService != null) {
                        iMultiInstanceInvalidationService.a(MultiInstanceInvalidationClient.this.h, MultiInstanceInvalidationClient.this.c);
                    }
                } catch (RemoteException e) {
                    Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", e);
                }
                MultiInstanceInvalidationClient multiInstanceInvalidationClient2 = MultiInstanceInvalidationClient.this;
                Context context = multiInstanceInvalidationClient2.f1015a;
                if (context != null) {
                    context.unbindService(multiInstanceInvalidationClient2.j);
                    MultiInstanceInvalidationClient.this.f1015a = null;
                }
            }
        };
        this.f1015a = context.getApplicationContext();
        this.b = str;
        this.d = invalidationTracker;
        this.g = executor;
        this.e = new InvalidationTracker.Observer(invalidationTracker.b) {
            public void a(Set<String> set) {
                if (!MultiInstanceInvalidationClient.this.i.get()) {
                    try {
                        MultiInstanceInvalidationClient.this.f.a(MultiInstanceInvalidationClient.this.c, (String[]) set.toArray(new String[0]));
                    } catch (RemoteException e) {
                        Log.w("ROOM", "Cannot broadcast invalidation", e);
                    }
                }
            }

            /* access modifiers changed from: package-private */
            public boolean a() {
                return true;
            }
        };
        this.f1015a.bindService(new Intent(this.f1015a, MultiInstanceInvalidationService.class), this.j, 1);
    }
}
