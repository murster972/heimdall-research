package androidx.room;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;
import androidx.collection.SparseArrayCompat;
import androidx.room.IMultiInstanceInvalidationService;

public class MultiInstanceInvalidationService extends Service {

    /* renamed from: a  reason: collision with root package name */
    int f1022a = 0;
    final SparseArrayCompat<String> b = new SparseArrayCompat<>();
    final RemoteCallbackList<IMultiInstanceInvalidationCallback> c = new RemoteCallbackList<IMultiInstanceInvalidationCallback>() {
        /* renamed from: a */
        public void onCallbackDied(IMultiInstanceInvalidationCallback iMultiInstanceInvalidationCallback, Object obj) {
            MultiInstanceInvalidationService.this.b.d(((Integer) obj).intValue());
        }
    };
    private final IMultiInstanceInvalidationService.Stub d = new IMultiInstanceInvalidationService.Stub() {
        public int a(IMultiInstanceInvalidationCallback iMultiInstanceInvalidationCallback, String str) {
            if (str == null) {
                return 0;
            }
            synchronized (MultiInstanceInvalidationService.this.c) {
                MultiInstanceInvalidationService multiInstanceInvalidationService = MultiInstanceInvalidationService.this;
                int i = multiInstanceInvalidationService.f1022a + 1;
                multiInstanceInvalidationService.f1022a = i;
                if (MultiInstanceInvalidationService.this.c.register(iMultiInstanceInvalidationCallback, Integer.valueOf(i))) {
                    MultiInstanceInvalidationService.this.b.a(i, str);
                    return i;
                }
                MultiInstanceInvalidationService multiInstanceInvalidationService2 = MultiInstanceInvalidationService.this;
                multiInstanceInvalidationService2.f1022a--;
                return 0;
            }
        }

        public void a(IMultiInstanceInvalidationCallback iMultiInstanceInvalidationCallback, int i) {
            synchronized (MultiInstanceInvalidationService.this.c) {
                MultiInstanceInvalidationService.this.c.unregister(iMultiInstanceInvalidationCallback);
                MultiInstanceInvalidationService.this.b.d(i);
            }
        }

        public void a(int i, String[] strArr) {
            synchronized (MultiInstanceInvalidationService.this.c) {
                String a2 = MultiInstanceInvalidationService.this.b.a(i);
                if (a2 == null) {
                    Log.w("ROOM", "Remote invalidation client ID not registered");
                    return;
                }
                int beginBroadcast = MultiInstanceInvalidationService.this.c.beginBroadcast();
                for (int i2 = 0; i2 < beginBroadcast; i2++) {
                    try {
                        int intValue = ((Integer) MultiInstanceInvalidationService.this.c.getBroadcastCookie(i2)).intValue();
                        String a3 = MultiInstanceInvalidationService.this.b.a(intValue);
                        if (i != intValue && a2.equals(a3)) {
                            MultiInstanceInvalidationService.this.c.getBroadcastItem(i2).a(strArr);
                        }
                    } catch (RemoteException e) {
                        Log.w("ROOM", "Error invoking a remote callback", e);
                    } catch (Throwable th) {
                        MultiInstanceInvalidationService.this.c.finishBroadcast();
                        throw th;
                    }
                }
                MultiInstanceInvalidationService.this.c.finishBroadcast();
            }
        }
    };

    public IBinder onBind(Intent intent) {
        return this.d;
    }
}
