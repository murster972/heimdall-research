package androidx.room;

import androidx.sqlite.db.SupportSQLiteStatement;

public abstract class EntityDeletionOrUpdateAdapter<T> extends SharedSQLiteStatement {
    public EntityDeletionOrUpdateAdapter(RoomDatabase roomDatabase) {
        super(roomDatabase);
    }

    public final int a(T[] tArr) {
        SupportSQLiteStatement a2 = a();
        try {
            int i = 0;
            for (T a3 : tArr) {
                a(a2, a3);
                i += a2.o();
            }
            return i;
        } finally {
            a(a2);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(SupportSQLiteStatement supportSQLiteStatement, T t);
}
