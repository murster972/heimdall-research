package androidx.room;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

class TransactionExecutor implements Executor {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f1032a;
    private final ArrayDeque<Runnable> b = new ArrayDeque<>();
    private Runnable c;

    TransactionExecutor(Executor executor) {
        this.f1032a = executor;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a() {
        Runnable poll = this.b.poll();
        this.c = poll;
        if (poll != null) {
            this.f1032a.execute(this.c);
        }
    }

    public synchronized void execute(final Runnable runnable) {
        this.b.offer(new Runnable() {
            public void run() {
                try {
                    runnable.run();
                } finally {
                    TransactionExecutor.this.a();
                }
            }
        });
        if (this.c == null) {
            a();
        }
    }
}
