package androidx.room;

import androidx.sqlite.db.SupportSQLiteStatement;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class SharedSQLiteStatement {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicBoolean f1031a = new AtomicBoolean(false);
    private final RoomDatabase b;
    private volatile SupportSQLiteStatement c;

    public SharedSQLiteStatement(RoomDatabase roomDatabase) {
        this.b = roomDatabase;
    }

    private SupportSQLiteStatement a(boolean z) {
        if (!z) {
            return d();
        }
        if (this.c == null) {
            this.c = d();
        }
        return this.c;
    }

    private SupportSQLiteStatement d() {
        return this.b.a(c());
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.b.a();
    }

    /* access modifiers changed from: protected */
    public abstract String c();

    public SupportSQLiteStatement a() {
        b();
        return a(this.f1031a.compareAndSet(false, true));
    }

    public void a(SupportSQLiteStatement supportSQLiteStatement) {
        if (supportSQLiteStatement == this.c) {
            this.f1031a.set(false);
        }
    }
}
