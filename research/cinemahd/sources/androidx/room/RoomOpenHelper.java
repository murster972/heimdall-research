package androidx.room;

import android.database.Cursor;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import java.util.List;

public class RoomOpenHelper extends SupportSQLiteOpenHelper.Callback {
    private DatabaseConfiguration b;
    private final Delegate c;
    private final String d;
    private final String e;

    public static abstract class Delegate {

        /* renamed from: a  reason: collision with root package name */
        public final int f1029a;

        public Delegate(int i) {
            this.f1029a = i;
        }

        /* access modifiers changed from: protected */
        public abstract void a(SupportSQLiteDatabase supportSQLiteDatabase);

        /* access modifiers changed from: protected */
        public abstract void b(SupportSQLiteDatabase supportSQLiteDatabase);

        /* access modifiers changed from: protected */
        public abstract void c(SupportSQLiteDatabase supportSQLiteDatabase);

        /* access modifiers changed from: protected */
        public abstract void d(SupportSQLiteDatabase supportSQLiteDatabase);

        /* access modifiers changed from: protected */
        public abstract void e(SupportSQLiteDatabase supportSQLiteDatabase);

        /* access modifiers changed from: protected */
        public abstract void f(SupportSQLiteDatabase supportSQLiteDatabase);

        /* access modifiers changed from: protected */
        public abstract void g(SupportSQLiteDatabase supportSQLiteDatabase);
    }

    public RoomOpenHelper(DatabaseConfiguration databaseConfiguration, Delegate delegate, String str, String str2) {
        super(delegate.f1029a);
        this.b = databaseConfiguration;
        this.c = delegate;
        this.d = str;
        this.e = str2;
    }

    private void e(SupportSQLiteDatabase supportSQLiteDatabase) {
        String str = null;
        if (g(supportSQLiteDatabase)) {
            Cursor a2 = supportSQLiteDatabase.a(new SimpleSQLiteQuery("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                if (a2.moveToFirst()) {
                    str = a2.getString(0);
                }
            } finally {
                a2.close();
            }
        }
        if (!this.d.equals(str) && !this.e.equals(str)) {
            throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
        }
    }

    private void f(SupportSQLiteDatabase supportSQLiteDatabase) {
        supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    private static boolean g(SupportSQLiteDatabase supportSQLiteDatabase) {
        Cursor d2 = supportSQLiteDatabase.d("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            boolean z = false;
            if (d2.moveToFirst() && d2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            d2.close();
        }
    }

    private void h(SupportSQLiteDatabase supportSQLiteDatabase) {
        f(supportSQLiteDatabase);
        supportSQLiteDatabase.b(RoomMasterTable.a(this.d));
    }

    public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
        super.a(supportSQLiteDatabase);
    }

    public void b(SupportSQLiteDatabase supportSQLiteDatabase, int i, int i2) {
        boolean z;
        List<Migration> a2;
        DatabaseConfiguration databaseConfiguration = this.b;
        if (databaseConfiguration == null || (a2 = databaseConfiguration.d.a(i, i2)) == null) {
            z = false;
        } else {
            this.c.f(supportSQLiteDatabase);
            for (Migration a3 : a2) {
                a3.a(supportSQLiteDatabase);
            }
            this.c.g(supportSQLiteDatabase);
            this.c.e(supportSQLiteDatabase);
            h(supportSQLiteDatabase);
            z = true;
        }
        if (!z) {
            DatabaseConfiguration databaseConfiguration2 = this.b;
            if (databaseConfiguration2 == null || databaseConfiguration2.a(i, i2)) {
                throw new IllegalStateException("A migration from " + i + " to " + i2 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
            }
            this.c.b(supportSQLiteDatabase);
            this.c.a(supportSQLiteDatabase);
        }
    }

    public void c(SupportSQLiteDatabase supportSQLiteDatabase) {
        h(supportSQLiteDatabase);
        this.c.a(supportSQLiteDatabase);
        this.c.c(supportSQLiteDatabase);
    }

    public void d(SupportSQLiteDatabase supportSQLiteDatabase) {
        super.d(supportSQLiteDatabase);
        e(supportSQLiteDatabase);
        this.c.d(supportSQLiteDatabase);
        this.b = null;
    }

    public void a(SupportSQLiteDatabase supportSQLiteDatabase, int i, int i2) {
        b(supportSQLiteDatabase, i, i2);
    }
}
