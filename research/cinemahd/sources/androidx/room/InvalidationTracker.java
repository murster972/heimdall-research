package androidx.room;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import androidx.arch.core.internal.SafeIterableMap;
import androidx.collection.ArrayMap;
import androidx.collection.ArraySet;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;

public class InvalidationTracker {
    private static final String[] k = {"UPDATE", "DELETE", "INSERT"};

    /* renamed from: a  reason: collision with root package name */
    final ArrayMap<String, Integer> f1010a;
    final String[] b;
    private Map<String, Set<String>> c;
    final RoomDatabase d;
    AtomicBoolean e = new AtomicBoolean(false);
    private volatile boolean f = false;
    volatile SupportSQLiteStatement g;
    private ObservedTableTracker h;
    @SuppressLint({"RestrictedApi"})
    final SafeIterableMap<Observer, ObserverWrapper> i = new SafeIterableMap<>();
    Runnable j = new Runnable() {
        /* JADX INFO: finally extract failed */
        private Set<Integer> a() {
            ArraySet arraySet = new ArraySet();
            Cursor a2 = InvalidationTracker.this.d.a((SupportSQLiteQuery) new SimpleSQLiteQuery("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"));
            while (a2.moveToNext()) {
                try {
                    arraySet.add(Integer.valueOf(a2.getInt(0)));
                } catch (Throwable th) {
                    a2.close();
                    throw th;
                }
            }
            a2.close();
            if (!arraySet.isEmpty()) {
                InvalidationTracker.this.g.o();
            }
            return arraySet;
        }

        public void run() {
            SupportSQLiteDatabase a2;
            Lock f = InvalidationTracker.this.d.f();
            Set<Integer> set = null;
            try {
                f.lock();
                if (!InvalidationTracker.this.a()) {
                    f.unlock();
                } else if (!InvalidationTracker.this.e.compareAndSet(true, false)) {
                    f.unlock();
                } else if (InvalidationTracker.this.d.i()) {
                    f.unlock();
                } else {
                    if (InvalidationTracker.this.d.f) {
                        a2 = InvalidationTracker.this.d.g().a();
                        a2.beginTransaction();
                        set = a();
                        a2.setTransactionSuccessful();
                        a2.endTransaction();
                    } else {
                        set = a();
                    }
                    f.unlock();
                    if (set != null && !set.isEmpty()) {
                        synchronized (InvalidationTracker.this.i) {
                            Iterator<Map.Entry<Observer, ObserverWrapper>> it2 = InvalidationTracker.this.i.iterator();
                            while (it2.hasNext()) {
                                ((ObserverWrapper) it2.next().getValue()).a(set);
                            }
                        }
                    }
                }
            } catch (SQLiteException | IllegalStateException e) {
                try {
                    Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e);
                } catch (Throwable th) {
                    f.unlock();
                    throw th;
                }
            } catch (Throwable th2) {
                a2.endTransaction();
                throw th2;
            }
        }
    };

    public static abstract class Observer {

        /* renamed from: a  reason: collision with root package name */
        final String[] f1013a;

        public Observer(String[] strArr) {
            this.f1013a = (String[]) Arrays.copyOf(strArr, strArr.length);
        }

        public abstract void a(Set<String> set);

        /* access modifiers changed from: package-private */
        public boolean a() {
            return false;
        }
    }

    public InvalidationTracker(RoomDatabase roomDatabase, Map<String, String> map, Map<String, Set<String>> map2, String... strArr) {
        this.d = roomDatabase;
        this.h = new ObservedTableTracker(strArr.length);
        this.f1010a = new ArrayMap<>();
        this.c = map2;
        new InvalidationLiveDataContainer(this.d);
        int length = strArr.length;
        this.b = new String[length];
        for (int i2 = 0; i2 < length; i2++) {
            String lowerCase = strArr[i2].toLowerCase(Locale.US);
            this.f1010a.put(lowerCase, Integer.valueOf(i2));
            String str = map.get(strArr[i2]);
            if (str != null) {
                this.b[i2] = str.toLowerCase(Locale.US);
            } else {
                this.b[i2] = lowerCase;
            }
        }
        for (Map.Entry next : map.entrySet()) {
            String lowerCase2 = ((String) next.getValue()).toLowerCase(Locale.US);
            if (this.f1010a.containsKey(lowerCase2)) {
                String lowerCase3 = ((String) next.getKey()).toLowerCase(Locale.US);
                ArrayMap<String, Integer> arrayMap = this.f1010a;
                arrayMap.put(lowerCase3, arrayMap.get(lowerCase2));
            }
        }
    }

    private void b(SupportSQLiteDatabase supportSQLiteDatabase, int i2) {
        String str = this.b[i2];
        StringBuilder sb = new StringBuilder();
        for (String a2 : k) {
            sb.setLength(0);
            sb.append("DROP TRIGGER IF EXISTS ");
            a(sb, str, a2);
            supportSQLiteDatabase.b(sb.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
        synchronized (this) {
            if (this.f) {
                Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                return;
            }
            supportSQLiteDatabase.b("PRAGMA temp_store = MEMORY;");
            supportSQLiteDatabase.b("PRAGMA recursive_triggers='ON';");
            supportSQLiteDatabase.b("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            b(supportSQLiteDatabase);
            this.g = supportSQLiteDatabase.c("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ");
            this.f = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.d.j()) {
            b(this.d.g().a());
        }
    }

    static class ObservedTableTracker {

        /* renamed from: a  reason: collision with root package name */
        final long[] f1012a;
        final boolean[] b;
        final int[] c;
        boolean d;
        boolean e;

        ObservedTableTracker(int i) {
            this.f1012a = new long[i];
            this.b = new boolean[i];
            this.c = new int[i];
            Arrays.fill(this.f1012a, 0);
            Arrays.fill(this.b, false);
        }

        /* access modifiers changed from: package-private */
        public boolean a(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.f1012a[i];
                    this.f1012a[i] = 1 + j;
                    if (j == 0) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        /* access modifiers changed from: package-private */
        public boolean b(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.f1012a[i];
                    this.f1012a[i] = j - 1;
                    if (j == 1) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        /* access modifiers changed from: package-private */
        public int[] a() {
            synchronized (this) {
                if (this.d) {
                    if (!this.e) {
                        int length = this.f1012a.length;
                        int i = 0;
                        while (true) {
                            int i2 = 1;
                            if (i < length) {
                                boolean z = this.f1012a[i] > 0;
                                if (z != this.b[i]) {
                                    int[] iArr = this.c;
                                    if (!z) {
                                        i2 = 2;
                                    }
                                    iArr[i] = i2;
                                } else {
                                    this.c[i] = 0;
                                }
                                this.b[i] = z;
                                i++;
                            } else {
                                this.e = true;
                                this.d = false;
                                int[] iArr2 = this.c;
                                return iArr2;
                            }
                        }
                    }
                }
                return null;
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            synchronized (this) {
                this.e = false;
            }
        }
    }

    static class ObserverWrapper {

        /* renamed from: a  reason: collision with root package name */
        final int[] f1014a;
        private final String[] b;
        final Observer c;
        private final Set<String> d;

        ObserverWrapper(Observer observer, int[] iArr, String[] strArr) {
            this.c = observer;
            this.f1014a = iArr;
            this.b = strArr;
            if (iArr.length == 1) {
                ArraySet arraySet = new ArraySet();
                arraySet.add(this.b[0]);
                this.d = Collections.unmodifiableSet(arraySet);
                return;
            }
            this.d = null;
        }

        /* access modifiers changed from: package-private */
        public void a(Set<Integer> set) {
            int length = this.f1014a.length;
            Set set2 = null;
            for (int i = 0; i < length; i++) {
                if (set.contains(Integer.valueOf(this.f1014a[i]))) {
                    if (length == 1) {
                        set2 = this.d;
                    } else {
                        if (set2 == null) {
                            set2 = new ArraySet(length);
                        }
                        set2.add(this.b[i]);
                    }
                }
            }
            if (set2 != null) {
                this.c.a(set2);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(String[] strArr) {
            Set<String> set = null;
            if (this.b.length == 1) {
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (strArr[i].equalsIgnoreCase(this.b[0])) {
                        set = this.d;
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                ArraySet arraySet = new ArraySet();
                for (String str : strArr) {
                    String[] strArr2 = this.b;
                    int length2 = strArr2.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        }
                        String str2 = strArr2[i2];
                        if (str2.equalsIgnoreCase(str)) {
                            arraySet.add(str2);
                            break;
                        }
                        i2++;
                    }
                }
                if (arraySet.size() > 0) {
                    set = arraySet;
                }
            }
            if (set != null) {
                this.c.a(set);
            }
        }
    }

    private String[] b(String[] strArr) {
        ArraySet arraySet = new ArraySet();
        for (String str : strArr) {
            String lowerCase = str.toLowerCase(Locale.US);
            if (this.c.containsKey(lowerCase)) {
                arraySet.addAll(this.c.get(lowerCase));
            } else {
                arraySet.add(str);
            }
        }
        return (String[]) arraySet.toArray(new String[arraySet.size()]);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        new MultiInstanceInvalidationClient(context, str, this, this.d.h());
    }

    private static void a(StringBuilder sb, String str, String str2) {
        sb.append("`");
        sb.append("room_table_modification_trigger_");
        sb.append(str);
        sb.append("_");
        sb.append(str2);
        sb.append("`");
    }

    @SuppressLint({"RestrictedApi"})
    public void b(Observer observer) {
        ObserverWrapper remove;
        synchronized (this.i) {
            remove = this.i.remove(observer);
        }
        if (remove != null && this.h.b(remove.f1014a)) {
            c();
        }
    }

    private void a(SupportSQLiteDatabase supportSQLiteDatabase, int i2) {
        supportSQLiteDatabase.b("INSERT OR IGNORE INTO room_table_modification_log VALUES(" + i2 + ", 0)");
        String str = this.b[i2];
        StringBuilder sb = new StringBuilder();
        for (String str2 : k) {
            sb.setLength(0);
            sb.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            a(sb, str, str2);
            sb.append(" AFTER ");
            sb.append(str2);
            sb.append(" ON `");
            sb.append(str);
            sb.append("` BEGIN UPDATE ");
            sb.append("room_table_modification_log");
            sb.append(" SET ");
            sb.append("invalidated");
            sb.append(" = 1");
            sb.append(" WHERE ");
            sb.append("table_id");
            sb.append(" = ");
            sb.append(i2);
            sb.append(" AND ");
            sb.append("invalidated");
            sb.append(" = 0");
            sb.append("; END");
            supportSQLiteDatabase.b(sb.toString());
        }
    }

    public void b() {
        if (this.e.compareAndSet(false, true)) {
            this.d.h().execute(this.j);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(SupportSQLiteDatabase supportSQLiteDatabase) {
        if (!supportSQLiteDatabase.r()) {
            while (true) {
                try {
                    Lock f2 = this.d.f();
                    f2.lock();
                    try {
                        int[] a2 = this.h.a();
                        if (a2 == null) {
                            f2.unlock();
                            return;
                        }
                        int length = a2.length;
                        supportSQLiteDatabase.beginTransaction();
                        for (int i2 = 0; i2 < length; i2++) {
                            int i3 = a2[i2];
                            if (i3 == 1) {
                                a(supportSQLiteDatabase, i2);
                            } else if (i3 == 2) {
                                b(supportSQLiteDatabase, i2);
                            }
                        }
                        supportSQLiteDatabase.setTransactionSuccessful();
                        supportSQLiteDatabase.endTransaction();
                        this.h.b();
                        f2.unlock();
                    } catch (Throwable th) {
                        f2.unlock();
                        throw th;
                    }
                } catch (SQLiteException | IllegalStateException e2) {
                    Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e2);
                    return;
                }
            }
        }
    }

    @SuppressLint({"RestrictedApi"})
    public void a(Observer observer) {
        ObserverWrapper b2;
        String[] b3 = b(observer.f1013a);
        int[] iArr = new int[b3.length];
        int length = b3.length;
        int i2 = 0;
        while (i2 < length) {
            Integer num = this.f1010a.get(b3[i2].toLowerCase(Locale.US));
            if (num != null) {
                iArr[i2] = num.intValue();
                i2++;
            } else {
                throw new IllegalArgumentException("There is no table with name " + b3[i2]);
            }
        }
        ObserverWrapper observerWrapper = new ObserverWrapper(observer, iArr, b3);
        synchronized (this.i) {
            b2 = this.i.b(observer, observerWrapper);
        }
        if (b2 == null && this.h.a(iArr)) {
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        if (!this.d.j()) {
            return false;
        }
        if (!this.f) {
            this.d.g().a();
        }
        if (this.f) {
            return true;
        }
        Log.e("ROOM", "database is not initialized even though it is open");
        return false;
    }

    public void a(String... strArr) {
        synchronized (this.i) {
            Iterator<Map.Entry<Observer, ObserverWrapper>> it2 = this.i.iterator();
            while (it2.hasNext()) {
                Map.Entry next = it2.next();
                if (!((Observer) next.getKey()).a()) {
                    ((ObserverWrapper) next.getValue()).a(strArr);
                }
            }
        }
    }
}
