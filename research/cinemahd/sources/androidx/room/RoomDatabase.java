package androidx.room;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.Looper;
import android.util.Log;
import androidx.arch.core.executor.ArchTaskExecutor;
import androidx.collection.SparseArrayCompat;
import androidx.core.app.ActivityManagerCompat;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class RoomDatabase {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    protected volatile SupportSQLiteDatabase f1025a;
    private Executor b;
    private SupportSQLiteOpenHelper c;
    private final InvalidationTracker d;
    private boolean e;
    boolean f;
    @Deprecated
    protected List<Callback> g;
    private final ReentrantReadWriteLock h = new ReentrantReadWriteLock();
    private final ThreadLocal<Integer> i = new ThreadLocal<>();

    public static abstract class Callback {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
        }

        public void b(SupportSQLiteDatabase supportSQLiteDatabase) {
        }
    }

    public enum JournalMode {
        AUTOMATIC,
        TRUNCATE,
        WRITE_AHEAD_LOGGING;

        /* access modifiers changed from: package-private */
        @SuppressLint({"NewApi"})
        public JournalMode a(Context context) {
            ActivityManager activityManager;
            if (this != AUTOMATIC) {
                return this;
            }
            if (Build.VERSION.SDK_INT < 16 || (activityManager = (ActivityManager) context.getSystemService("activity")) == null || ActivityManagerCompat.a(activityManager)) {
                return TRUNCATE;
            }
            return WRITE_AHEAD_LOGGING;
        }
    }

    public RoomDatabase() {
        new ConcurrentHashMap();
        this.d = d();
    }

    private static boolean l() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    /* access modifiers changed from: protected */
    public abstract SupportSQLiteOpenHelper a(DatabaseConfiguration databaseConfiguration);

    public void a() {
        if (!this.e && l()) {
            throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.");
        }
    }

    public void b(DatabaseConfiguration databaseConfiguration) {
        this.c = a(databaseConfiguration);
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 16) {
            if (databaseConfiguration.g == JournalMode.WRITE_AHEAD_LOGGING) {
                z = true;
            }
            this.c.a(z);
        }
        this.g = databaseConfiguration.e;
        this.b = databaseConfiguration.h;
        new TransactionExecutor(databaseConfiguration.i);
        this.e = databaseConfiguration.f;
        this.f = z;
        if (databaseConfiguration.j) {
            this.d.a(databaseConfiguration.b, databaseConfiguration.c);
        }
    }

    @Deprecated
    public void c() {
        a();
        SupportSQLiteDatabase a2 = this.c.a();
        this.d.b(a2);
        a2.beginTransaction();
    }

    /* access modifiers changed from: protected */
    public abstract InvalidationTracker d();

    @Deprecated
    public void e() {
        this.c.a().endTransaction();
        if (!i()) {
            this.d.b();
        }
    }

    /* access modifiers changed from: package-private */
    public Lock f() {
        return this.h.readLock();
    }

    public SupportSQLiteOpenHelper g() {
        return this.c;
    }

    public Executor h() {
        return this.b;
    }

    public boolean i() {
        return this.c.a().r();
    }

    public boolean j() {
        SupportSQLiteDatabase supportSQLiteDatabase = this.f1025a;
        return supportSQLiteDatabase != null && supportSQLiteDatabase.isOpen();
    }

    @Deprecated
    public void k() {
        this.c.a().setTransactionSuccessful();
    }

    public static class MigrationContainer {

        /* renamed from: a  reason: collision with root package name */
        private SparseArrayCompat<SparseArrayCompat<Migration>> f1028a = new SparseArrayCompat<>();

        public void a(Migration... migrationArr) {
            for (Migration a2 : migrationArr) {
                a(a2);
            }
        }

        private void a(Migration migration) {
            int i = migration.f1034a;
            int i2 = migration.b;
            SparseArrayCompat a2 = this.f1028a.a(i);
            if (a2 == null) {
                a2 = new SparseArrayCompat();
                this.f1028a.c(i, a2);
            }
            Migration migration2 = (Migration) a2.a(i2);
            if (migration2 != null) {
                Log.w("ROOM", "Overriding migration " + migration2 + " with " + migration);
            }
            a2.a(i2, migration);
        }

        public List<Migration> a(int i, int i2) {
            if (i == i2) {
                return Collections.emptyList();
            }
            return a(new ArrayList(), i2 > i, i, i2);
        }

        private List<Migration> a(List<Migration> list, boolean z, int i, int i2) {
            boolean z2;
            int i3;
            int i4;
            int i5 = z ? -1 : 1;
            do {
                if (z) {
                    if (i >= i2) {
                        return list;
                    }
                } else if (i <= i2) {
                    return list;
                }
                SparseArrayCompat a2 = this.f1028a.a(i);
                if (a2 != null) {
                    int b = a2.b();
                    z2 = false;
                    if (z) {
                        i4 = b - 1;
                        i3 = -1;
                    } else {
                        i3 = b;
                        i4 = 0;
                    }
                    while (true) {
                        if (i4 == i3) {
                            break;
                        }
                        int c = a2.c(i4);
                        if (!z ? !(c < i2 || c >= i) : !(c > i2 || c <= i)) {
                            list.add(a2.e(i4));
                            i = c;
                            z2 = true;
                            continue;
                            break;
                        }
                        i4 += i5;
                    }
                } else {
                    return null;
                }
            } while (z2);
            return null;
        }
    }

    public Cursor a(SupportSQLiteQuery supportSQLiteQuery) {
        a();
        b();
        return this.c.a().a(supportSQLiteQuery);
    }

    public static class Builder<T extends RoomDatabase> {

        /* renamed from: a  reason: collision with root package name */
        private final Class<T> f1026a;
        private final String b;
        private final Context c;
        private ArrayList<Callback> d;
        private Executor e;
        private Executor f;
        private SupportSQLiteOpenHelper.Factory g;
        private boolean h;
        private JournalMode i = JournalMode.AUTOMATIC;
        private boolean j;
        private boolean k = true;
        private boolean l;
        private final MigrationContainer m = new MigrationContainer();
        private Set<Integer> n;
        private Set<Integer> o;

        Builder(Context context, Class<T> cls, String str) {
            this.c = context;
            this.f1026a = cls;
            this.b = str;
        }

        public Builder<T> a(Migration... migrationArr) {
            if (this.o == null) {
                this.o = new HashSet();
            }
            for (Migration migration : migrationArr) {
                this.o.add(Integer.valueOf(migration.f1034a));
                this.o.add(Integer.valueOf(migration.b));
            }
            this.m.a(migrationArr);
            return this;
        }

        @SuppressLint({"RestrictedApi"})
        public T a() {
            Executor executor;
            if (this.c == null) {
                throw new IllegalArgumentException("Cannot provide null context for the database.");
            } else if (this.f1026a != null) {
                if (this.e == null && this.f == null) {
                    Executor b2 = ArchTaskExecutor.b();
                    this.f = b2;
                    this.e = b2;
                } else {
                    Executor executor2 = this.e;
                    if (executor2 != null && this.f == null) {
                        this.f = executor2;
                    } else if (this.e == null && (executor = this.f) != null) {
                        this.e = executor;
                    }
                }
                Set<Integer> set = this.o;
                if (!(set == null || this.n == null)) {
                    for (Integer next : set) {
                        if (this.n.contains(next)) {
                            throw new IllegalArgumentException("Inconsistency detected. A Migration was supplied to addMigration(Migration... migrations) that has a start or end version equal to a start version supplied to fallbackToDestructiveMigrationFrom(int... startVersions). Start version: " + next);
                        }
                    }
                }
                if (this.g == null) {
                    this.g = new FrameworkSQLiteOpenHelperFactory();
                }
                Context context = this.c;
                DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration(context, this.b, this.g, this.m, this.d, this.h, this.i.a(context), this.e, this.f, this.j, this.k, this.l, this.n);
                T t = (RoomDatabase) Room.a(this.f1026a, "_Impl");
                t.b(databaseConfiguration);
                return t;
            } else {
                throw new IllegalArgumentException("Must provide an abstract class that extends RoomDatabase");
            }
        }
    }

    public SupportSQLiteStatement a(String str) {
        a();
        b();
        return this.c.a().c(str);
    }

    /* access modifiers changed from: protected */
    public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
        this.d.a(supportSQLiteDatabase);
    }

    public void b() {
        if (!i() && this.i.get() != null) {
            throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.");
        }
    }
}
