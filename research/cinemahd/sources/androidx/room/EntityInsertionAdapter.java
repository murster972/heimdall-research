package androidx.room;

import androidx.sqlite.db.SupportSQLiteStatement;

public abstract class EntityInsertionAdapter<T> extends SharedSQLiteStatement {
    public EntityInsertionAdapter(RoomDatabase roomDatabase) {
        super(roomDatabase);
    }

    /* access modifiers changed from: protected */
    public abstract void a(SupportSQLiteStatement supportSQLiteStatement, T t);

    public final void a(T[] tArr) {
        SupportSQLiteStatement a2 = a();
        try {
            for (T a3 : tArr) {
                a(a2, a3);
                a2.p();
            }
        } finally {
            a(a2);
        }
    }
}
