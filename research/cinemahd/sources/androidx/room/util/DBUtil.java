package androidx.room.util;

import android.database.AbstractWindowedCursor;
import android.database.Cursor;
import android.os.Build;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteQuery;
import java.util.ArrayList;

public class DBUtil {
    private DBUtil() {
    }

    public static Cursor a(RoomDatabase roomDatabase, SupportSQLiteQuery supportSQLiteQuery, boolean z) {
        Cursor a2 = roomDatabase.a(supportSQLiteQuery);
        if (!z || !(a2 instanceof AbstractWindowedCursor)) {
            return a2;
        }
        AbstractWindowedCursor abstractWindowedCursor = (AbstractWindowedCursor) a2;
        int count = abstractWindowedCursor.getCount();
        return (Build.VERSION.SDK_INT < 23 || (abstractWindowedCursor.hasWindow() ? abstractWindowedCursor.getWindow().getNumRows() : count) < count) ? CursorUtil.a(abstractWindowedCursor) : a2;
    }

    /* JADX INFO: finally extract failed */
    public static void a(SupportSQLiteDatabase supportSQLiteDatabase) {
        ArrayList<String> arrayList = new ArrayList<>();
        Cursor d = supportSQLiteDatabase.d("SELECT name FROM sqlite_master WHERE type = 'trigger'");
        while (d.moveToNext()) {
            try {
                arrayList.add(d.getString(0));
            } catch (Throwable th) {
                d.close();
                throw th;
            }
        }
        d.close();
        for (String str : arrayList) {
            if (str.startsWith("room_fts_content_sync_")) {
                supportSQLiteDatabase.b("DROP TRIGGER IF EXISTS " + str);
            }
        }
    }
}
