package androidx.room.util;

import android.database.Cursor;
import android.os.Build;
import androidx.sqlite.db.SupportSQLiteDatabase;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TableInfo {

    /* renamed from: a  reason: collision with root package name */
    public final String f1035a;
    public final Map<String, Column> b;
    public final Set<ForeignKey> c;
    public final Set<Index> d;

    public static class ForeignKey {

        /* renamed from: a  reason: collision with root package name */
        public final String f1037a;
        public final String b;
        public final String c;
        public final List<String> d;
        public final List<String> e;

        public ForeignKey(String str, String str2, String str3, List<String> list, List<String> list2) {
            this.f1037a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.e = Collections.unmodifiableList(list2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || ForeignKey.class != obj.getClass()) {
                return false;
            }
            ForeignKey foreignKey = (ForeignKey) obj;
            if (this.f1037a.equals(foreignKey.f1037a) && this.b.equals(foreignKey.b) && this.c.equals(foreignKey.c) && this.d.equals(foreignKey.d)) {
                return this.e.equals(foreignKey.e);
            }
            return false;
        }

        public int hashCode() {
            return (((((((this.f1037a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
        }

        public String toString() {
            return "ForeignKey{referenceTable='" + this.f1037a + '\'' + ", onDelete='" + this.b + '\'' + ", onUpdate='" + this.c + '\'' + ", columnNames=" + this.d + ", referenceColumnNames=" + this.e + '}';
        }
    }

    static class ForeignKeyWithSequence implements Comparable<ForeignKeyWithSequence> {

        /* renamed from: a  reason: collision with root package name */
        final int f1038a;
        final int b;
        final String c;
        final String d;

        ForeignKeyWithSequence(int i, int i2, String str, String str2) {
            this.f1038a = i;
            this.b = i2;
            this.c = str;
            this.d = str2;
        }

        /* renamed from: a */
        public int compareTo(ForeignKeyWithSequence foreignKeyWithSequence) {
            int i = this.f1038a - foreignKeyWithSequence.f1038a;
            return i == 0 ? this.b - foreignKeyWithSequence.b : i;
        }
    }

    public static class Index {

        /* renamed from: a  reason: collision with root package name */
        public final String f1039a;
        public final boolean b;
        public final List<String> c;

        public Index(String str, boolean z, List<String> list) {
            this.f1039a = str;
            this.b = z;
            this.c = list;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Index.class != obj.getClass()) {
                return false;
            }
            Index index = (Index) obj;
            if (this.b != index.b || !this.c.equals(index.c)) {
                return false;
            }
            if (this.f1039a.startsWith("index_")) {
                return index.f1039a.startsWith("index_");
            }
            return this.f1039a.equals(index.f1039a);
        }

        public int hashCode() {
            int i;
            if (this.f1039a.startsWith("index_")) {
                i = "index_".hashCode();
            } else {
                i = this.f1039a.hashCode();
            }
            return (((i * 31) + (this.b ? 1 : 0)) * 31) + this.c.hashCode();
        }

        public String toString() {
            return "Index{name='" + this.f1039a + '\'' + ", unique=" + this.b + ", columns=" + this.c + '}';
        }
    }

    public TableInfo(String str, Map<String, Column> map, Set<ForeignKey> set, Set<Index> set2) {
        Set<Index> set3;
        this.f1035a = str;
        this.b = Collections.unmodifiableMap(map);
        this.c = Collections.unmodifiableSet(set);
        if (set2 == null) {
            set3 = null;
        } else {
            set3 = Collections.unmodifiableSet(set2);
        }
        this.d = set3;
    }

    public static TableInfo a(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        return new TableInfo(str, b(supportSQLiteDatabase, str), c(supportSQLiteDatabase, str), d(supportSQLiteDatabase, str));
    }

    private static Map<String, Column> b(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        Cursor d2 = supportSQLiteDatabase.d("PRAGMA table_info(`" + str + "`)");
        HashMap hashMap = new HashMap();
        try {
            if (d2.getColumnCount() > 0) {
                int columnIndex = d2.getColumnIndex(MediationMetaData.KEY_NAME);
                int columnIndex2 = d2.getColumnIndex("type");
                int columnIndex3 = d2.getColumnIndex("notnull");
                int columnIndex4 = d2.getColumnIndex("pk");
                while (d2.moveToNext()) {
                    String string = d2.getString(columnIndex);
                    hashMap.put(string, new Column(string, d2.getString(columnIndex2), d2.getInt(columnIndex3) != 0, d2.getInt(columnIndex4)));
                }
            }
            return hashMap;
        } finally {
            d2.close();
        }
    }

    private static Set<ForeignKey> c(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        HashSet hashSet = new HashSet();
        Cursor d2 = supportSQLiteDatabase.d("PRAGMA foreign_key_list(`" + str + "`)");
        try {
            int columnIndex = d2.getColumnIndex("id");
            int columnIndex2 = d2.getColumnIndex("seq");
            int columnIndex3 = d2.getColumnIndex("table");
            int columnIndex4 = d2.getColumnIndex("on_delete");
            int columnIndex5 = d2.getColumnIndex("on_update");
            List<ForeignKeyWithSequence> a2 = a(d2);
            int count = d2.getCount();
            for (int i = 0; i < count; i++) {
                d2.moveToPosition(i);
                if (d2.getInt(columnIndex2) == 0) {
                    int i2 = d2.getInt(columnIndex);
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    for (ForeignKeyWithSequence next : a2) {
                        if (next.f1038a == i2) {
                            arrayList.add(next.c);
                            arrayList2.add(next.d);
                        }
                    }
                    hashSet.add(new ForeignKey(d2.getString(columnIndex3), d2.getString(columnIndex4), d2.getString(columnIndex5), arrayList, arrayList2));
                }
            }
            return hashSet;
        } finally {
            d2.close();
        }
    }

    private static Set<Index> d(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        Cursor d2 = supportSQLiteDatabase.d("PRAGMA index_list(`" + str + "`)");
        try {
            int columnIndex = d2.getColumnIndex(MediationMetaData.KEY_NAME);
            int columnIndex2 = d2.getColumnIndex("origin");
            int columnIndex3 = d2.getColumnIndex("unique");
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    HashSet hashSet = new HashSet();
                    while (d2.moveToNext()) {
                        if ("c".equals(d2.getString(columnIndex2))) {
                            String string = d2.getString(columnIndex);
                            boolean z = true;
                            if (d2.getInt(columnIndex3) != 1) {
                                z = false;
                            }
                            Index a2 = a(supportSQLiteDatabase, string, z);
                            if (a2 == null) {
                                d2.close();
                                return null;
                            }
                            hashSet.add(a2);
                        }
                    }
                    d2.close();
                    return hashSet;
                }
            }
            return null;
        } finally {
            d2.close();
        }
    }

    public boolean equals(Object obj) {
        Set<Index> set;
        if (this == obj) {
            return true;
        }
        if (obj == null || TableInfo.class != obj.getClass()) {
            return false;
        }
        TableInfo tableInfo = (TableInfo) obj;
        String str = this.f1035a;
        if (str == null ? tableInfo.f1035a != null : !str.equals(tableInfo.f1035a)) {
            return false;
        }
        Map<String, Column> map = this.b;
        if (map == null ? tableInfo.b != null : !map.equals(tableInfo.b)) {
            return false;
        }
        Set<ForeignKey> set2 = this.c;
        if (set2 == null ? tableInfo.c != null : !set2.equals(tableInfo.c)) {
            return false;
        }
        Set<Index> set3 = this.d;
        if (set3 == null || (set = tableInfo.d) == null) {
            return true;
        }
        return set3.equals(set);
    }

    public int hashCode() {
        String str = this.f1035a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, Column> map = this.b;
        int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
        Set<ForeignKey> set = this.c;
        if (set != null) {
            i = set.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "TableInfo{name='" + this.f1035a + '\'' + ", columns=" + this.b + ", foreignKeys=" + this.c + ", indices=" + this.d + '}';
    }

    private static List<ForeignKeyWithSequence> a(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("id");
        int columnIndex2 = cursor.getColumnIndex("seq");
        int columnIndex3 = cursor.getColumnIndex("from");
        int columnIndex4 = cursor.getColumnIndex("to");
        int count = cursor.getCount();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            arrayList.add(new ForeignKeyWithSequence(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    public static class Column {

        /* renamed from: a  reason: collision with root package name */
        public final String f1036a;
        public final String b;
        public final int c;
        public final boolean d;
        public final int e;

        public Column(String str, String str2, boolean z, int i) {
            this.f1036a = str;
            this.b = str2;
            this.d = z;
            this.e = i;
            this.c = a(str2);
        }

        private static int a(String str) {
            if (str == null) {
                return 5;
            }
            String upperCase = str.toUpperCase(Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains(AdPreferences.TYPE_TEXT)) {
                return 2;
            }
            if (upperCase.contains("BLOB")) {
                return 5;
            }
            return (upperCase.contains("REAL") || upperCase.contains("FLOA") || upperCase.contains("DOUB")) ? 4 : 1;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Column.class != obj.getClass()) {
                return false;
            }
            Column column = (Column) obj;
            if (Build.VERSION.SDK_INT >= 20) {
                if (this.e != column.e) {
                    return false;
                }
            } else if (a() != column.a()) {
                return false;
            }
            if (this.f1036a.equals(column.f1036a) && this.d == column.d && this.c == column.c) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (((((this.f1036a.hashCode() * 31) + this.c) * 31) + (this.d ? 1231 : 1237)) * 31) + this.e;
        }

        public String toString() {
            return "Column{name='" + this.f1036a + '\'' + ", type='" + this.b + '\'' + ", affinity='" + this.c + '\'' + ", notNull=" + this.d + ", primaryKeyPosition=" + this.e + '}';
        }

        public boolean a() {
            return this.e > 0;
        }
    }

    private static Index a(SupportSQLiteDatabase supportSQLiteDatabase, String str, boolean z) {
        Cursor d2 = supportSQLiteDatabase.d("PRAGMA index_xinfo(`" + str + "`)");
        try {
            int columnIndex = d2.getColumnIndex("seqno");
            int columnIndex2 = d2.getColumnIndex("cid");
            int columnIndex3 = d2.getColumnIndex(MediationMetaData.KEY_NAME);
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    TreeMap treeMap = new TreeMap();
                    while (d2.moveToNext()) {
                        if (d2.getInt(columnIndex2) >= 0) {
                            int i = d2.getInt(columnIndex);
                            treeMap.put(Integer.valueOf(i), d2.getString(columnIndex3));
                        }
                    }
                    ArrayList arrayList = new ArrayList(treeMap.size());
                    arrayList.addAll(treeMap.values());
                    Index index = new Index(str, z, arrayList);
                    d2.close();
                    return index;
                }
            }
            return null;
        } finally {
            d2.close();
        }
    }
}
