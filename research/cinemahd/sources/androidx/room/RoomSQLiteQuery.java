package androidx.room;

import androidx.sqlite.db.SupportSQLiteProgram;
import androidx.sqlite.db.SupportSQLiteQuery;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class RoomSQLiteQuery implements SupportSQLiteQuery, SupportSQLiteProgram {
    static final TreeMap<Integer, RoomSQLiteQuery> i = new TreeMap<>();

    /* renamed from: a  reason: collision with root package name */
    private volatile String f1030a;
    final long[] b;
    final double[] c;
    final String[] d;
    final byte[][] e;
    private final int[] f;
    final int g;
    int h;

    private RoomSQLiteQuery(int i2) {
        this.g = i2;
        int i3 = i2 + 1;
        this.f = new int[i3];
        this.b = new long[i3];
        this.c = new double[i3];
        this.d = new String[i3];
        this.e = new byte[i3][];
    }

    public static RoomSQLiteQuery b(String str, int i2) {
        synchronized (i) {
            Map.Entry<Integer, RoomSQLiteQuery> ceilingEntry = i.ceilingEntry(Integer.valueOf(i2));
            if (ceilingEntry != null) {
                i.remove(ceilingEntry.getKey());
                RoomSQLiteQuery value = ceilingEntry.getValue();
                value.a(str, i2);
                return value;
            }
            RoomSQLiteQuery roomSQLiteQuery = new RoomSQLiteQuery(i2);
            roomSQLiteQuery.a(str, i2);
            return roomSQLiteQuery;
        }
    }

    private static void c() {
        if (i.size() > 15) {
            int size = i.size() - 10;
            Iterator<Integer> it2 = i.descendingKeySet().iterator();
            while (true) {
                int i2 = size - 1;
                if (size > 0) {
                    it2.next();
                    it2.remove();
                    size = i2;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i2) {
        this.f1030a = str;
        this.h = i2;
    }

    public void close() {
    }

    public String a() {
        return this.f1030a;
    }

    public void a(SupportSQLiteProgram supportSQLiteProgram) {
        for (int i2 = 1; i2 <= this.h; i2++) {
            int i3 = this.f[i2];
            if (i3 == 1) {
                supportSQLiteProgram.a(i2);
            } else if (i3 == 2) {
                supportSQLiteProgram.a(i2, this.b[i2]);
            } else if (i3 == 3) {
                supportSQLiteProgram.a(i2, this.c[i2]);
            } else if (i3 == 4) {
                supportSQLiteProgram.a(i2, this.d[i2]);
            } else if (i3 == 5) {
                supportSQLiteProgram.a(i2, this.e[i2]);
            }
        }
    }

    public void a(int i2) {
        this.f[i2] = 1;
    }

    public void a(int i2, long j) {
        this.f[i2] = 2;
        this.b[i2] = j;
    }

    public void b() {
        synchronized (i) {
            i.put(Integer.valueOf(this.g), this);
            c();
        }
    }

    public void a(int i2, double d2) {
        this.f[i2] = 3;
        this.c[i2] = d2;
    }

    public void a(int i2, String str) {
        this.f[i2] = 4;
        this.d[i2] = str;
    }

    public void a(int i2, byte[] bArr) {
        this.f[i2] = 5;
        this.e[i2] = bArr;
    }
}
