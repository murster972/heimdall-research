package androidx.media.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.media.session.MediaSession;
import android.os.Build;
import android.support.v4.media.session.MediaSessionCompat;
import android.widget.RemoteViews;
import androidx.core.app.NotificationBuilderWithBuilderAccessor;
import androidx.core.app.NotificationCompat;
import androidx.media.R$id;
import androidx.media.R$integer;
import androidx.media.R$layout;

public class NotificationCompat$MediaStyle extends NotificationCompat.Style {
    int[] e = null;
    MediaSessionCompat.Token f;
    boolean g;
    PendingIntent h;

    public NotificationCompat$MediaStyle a(int... iArr) {
        this.e = iArr;
        return this;
    }

    /* access modifiers changed from: package-private */
    public RemoteViews b() {
        int i;
        RemoteViews a2 = a(false, c(), true);
        int size = this.f539a.b.size();
        int[] iArr = this.e;
        if (iArr == null) {
            i = 0;
        } else {
            i = Math.min(iArr.length, 3);
        }
        a2.removeAllViews(R$id.media_actions);
        if (i > 0) {
            int i2 = 0;
            while (i2 < i) {
                if (i2 < size) {
                    a2.addView(R$id.media_actions, a(this.f539a.b.get(this.e[i2])));
                    i2++;
                } else {
                    throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", new Object[]{Integer.valueOf(i2), Integer.valueOf(size - 1)}));
                }
            }
        }
        if (this.g) {
            a2.setViewVisibility(R$id.end_padder, 8);
            a2.setViewVisibility(R$id.cancel_action, 0);
            a2.setOnClickPendingIntent(R$id.cancel_action, this.h);
            a2.setInt(R$id.cancel_action, "setAlpha", this.f539a.f538a.getResources().getInteger(R$integer.cancel_button_image_alpha));
        } else {
            a2.setViewVisibility(R$id.end_padder, 0);
            a2.setViewVisibility(R$id.cancel_action, 8);
        }
        return a2;
    }

    public RemoteViews c(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
        if (Build.VERSION.SDK_INT >= 21) {
            return null;
        }
        return b();
    }

    public NotificationCompat$MediaStyle a(MediaSessionCompat.Token token) {
        this.f = token;
        return this;
    }

    public void a(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
        if (Build.VERSION.SDK_INT >= 21) {
            notificationBuilderWithBuilderAccessor.a().setStyle(a(new Notification.MediaStyle()));
        } else if (this.g) {
            notificationBuilderWithBuilderAccessor.a().setOngoing(true);
        }
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return R$layout.notification_template_media;
    }

    /* access modifiers changed from: package-private */
    public Notification.MediaStyle a(Notification.MediaStyle mediaStyle) {
        int[] iArr = this.e;
        if (iArr != null) {
            mediaStyle.setShowActionsInCompactView(iArr);
        }
        MediaSessionCompat.Token token = this.f;
        if (token != null) {
            mediaStyle.setMediaSession((MediaSession.Token) token.getToken());
        }
        return mediaStyle;
    }

    private RemoteViews a(NotificationCompat.Action action) {
        boolean z = action.a() == null;
        RemoteViews remoteViews = new RemoteViews(this.f539a.f538a.getPackageName(), R$layout.notification_media_action);
        remoteViews.setImageViewResource(R$id.action0, action.e());
        if (!z) {
            remoteViews.setOnClickPendingIntent(R$id.action0, action.a());
        }
        if (Build.VERSION.SDK_INT >= 15) {
            remoteViews.setContentDescription(R$id.action0, action.i());
        }
        return remoteViews;
    }

    /* access modifiers changed from: package-private */
    public RemoteViews a() {
        int min = Math.min(this.f539a.b.size(), 5);
        RemoteViews a2 = a(false, a(min), false);
        a2.removeAllViews(R$id.media_actions);
        if (min > 0) {
            for (int i = 0; i < min; i++) {
                a2.addView(R$id.media_actions, a(this.f539a.b.get(i)));
            }
        }
        if (this.g) {
            a2.setViewVisibility(R$id.cancel_action, 0);
            a2.setInt(R$id.cancel_action, "setAlpha", this.f539a.f538a.getResources().getInteger(R$integer.cancel_button_image_alpha));
            a2.setOnClickPendingIntent(R$id.cancel_action, this.h);
        } else {
            a2.setViewVisibility(R$id.cancel_action, 8);
        }
        return a2;
    }

    public RemoteViews b(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
        if (Build.VERSION.SDK_INT >= 21) {
            return null;
        }
        return a();
    }

    /* access modifiers changed from: package-private */
    public int a(int i) {
        return i <= 3 ? R$layout.notification_template_big_media_narrow : R$layout.notification_template_big_media;
    }
}
