package androidx.media;

import android.os.Build;
import androidx.media.VolumeProviderCompatApi21;

public abstract class VolumeProviderCompat {

    /* renamed from: a  reason: collision with root package name */
    private final int f771a;
    private final int b;
    private int c;
    private Callback d;
    private Object e;

    public static abstract class Callback {
        public abstract void onVolumeChanged(VolumeProviderCompat volumeProviderCompat);
    }

    public VolumeProviderCompat(int i, int i2, int i3) {
        this.f771a = i;
        this.b = i2;
        this.c = i3;
    }

    public final int a() {
        return this.c;
    }

    public abstract void a(int i);

    public final int b() {
        return this.b;
    }

    public abstract void b(int i);

    public final int c() {
        return this.f771a;
    }

    public Object d() {
        if (this.e == null && Build.VERSION.SDK_INT >= 21) {
            this.e = VolumeProviderCompatApi21.a(this.f771a, this.b, this.c, new VolumeProviderCompatApi21.Delegate() {
                public void a(int i) {
                    VolumeProviderCompat.this.b(i);
                }

                public void b(int i) {
                    VolumeProviderCompat.this.a(i);
                }
            });
        }
        return this.e;
    }

    public void a(Callback callback) {
        this.d = callback;
    }

    public final void c(int i) {
        this.c = i;
        Object d2 = d();
        if (d2 != null && Build.VERSION.SDK_INT >= 21) {
            VolumeProviderCompatApi21.a(d2, i);
        }
        Callback callback = this.d;
        if (callback != null) {
            callback.onVolumeChanged(this);
        }
    }
}
