package androidx.media;

import android.media.VolumeProvider;

class VolumeProviderCompatApi21 {

    public interface Delegate {
        void a(int i);

        void b(int i);
    }

    private VolumeProviderCompatApi21() {
    }

    public static Object a(int i, int i2, int i3, final Delegate delegate) {
        return new VolumeProvider(i, i2, i3) {
            public void onAdjustVolume(int i) {
                delegate.b(i);
            }

            public void onSetVolumeTo(int i) {
                delegate.a(i);
            }
        };
    }

    public static void a(Object obj, int i) {
        ((VolumeProvider) obj).setCurrentVolume(i);
    }
}
