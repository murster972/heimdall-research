package androidx.media;

import android.annotation.TargetApi;
import android.media.AudioAttributes;

@TargetApi(21)
class AudioAttributesImplApi21 implements AudioAttributesImpl {

    /* renamed from: a  reason: collision with root package name */
    AudioAttributes f765a;
    int b = -1;

    AudioAttributesImplApi21() {
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AudioAttributesImplApi21)) {
            return false;
        }
        return this.f765a.equals(((AudioAttributesImplApi21) obj).f765a);
    }

    public int hashCode() {
        return this.f765a.hashCode();
    }

    public String toString() {
        return "AudioAttributesCompat: audioattributes=" + this.f765a;
    }
}
