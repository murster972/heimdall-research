package androidx.media;

public final class R$layout {
    public static final int notification_action = 2131493112;
    public static final int notification_action_tombstone = 2131493113;
    public static final int notification_media_action = 2131493115;
    public static final int notification_media_cancel_action = 2131493116;
    public static final int notification_template_big_media = 2131493117;
    public static final int notification_template_big_media_custom = 2131493118;
    public static final int notification_template_big_media_narrow = 2131493119;
    public static final int notification_template_big_media_narrow_custom = 2131493120;
    public static final int notification_template_custom_big = 2131493121;
    public static final int notification_template_icon_group = 2131493122;
    public static final int notification_template_lines_media = 2131493123;
    public static final int notification_template_media = 2131493124;
    public static final int notification_template_media_custom = 2131493125;
    public static final int notification_template_part_chronometer = 2131493126;
    public static final int notification_template_part_time = 2131493127;

    private R$layout() {
    }
}
