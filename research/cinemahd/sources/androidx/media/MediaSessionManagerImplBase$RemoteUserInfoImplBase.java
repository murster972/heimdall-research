package androidx.media;

import android.text.TextUtils;
import androidx.core.util.ObjectsCompat;

class MediaSessionManagerImplBase$RemoteUserInfoImplBase implements MediaSessionManager$RemoteUserInfoImpl {

    /* renamed from: a  reason: collision with root package name */
    private String f769a;
    private int b;
    private int c;

    MediaSessionManagerImplBase$RemoteUserInfoImplBase(String str, int i, int i2) {
        this.f769a = str;
        this.b = i;
        this.c = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaSessionManagerImplBase$RemoteUserInfoImplBase)) {
            return false;
        }
        MediaSessionManagerImplBase$RemoteUserInfoImplBase mediaSessionManagerImplBase$RemoteUserInfoImplBase = (MediaSessionManagerImplBase$RemoteUserInfoImplBase) obj;
        if (TextUtils.equals(this.f769a, mediaSessionManagerImplBase$RemoteUserInfoImplBase.f769a) && this.b == mediaSessionManagerImplBase$RemoteUserInfoImplBase.b && this.c == mediaSessionManagerImplBase$RemoteUserInfoImplBase.c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ObjectsCompat.a(this.f769a, Integer.valueOf(this.b), Integer.valueOf(this.c));
    }
}
