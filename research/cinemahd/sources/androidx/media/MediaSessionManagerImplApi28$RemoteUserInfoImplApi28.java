package androidx.media;

import android.media.session.MediaSessionManager;
import androidx.core.util.ObjectsCompat;

final class MediaSessionManagerImplApi28$RemoteUserInfoImplApi28 implements MediaSessionManager$RemoteUserInfoImpl {

    /* renamed from: a  reason: collision with root package name */
    final MediaSessionManager.RemoteUserInfo f768a;

    MediaSessionManagerImplApi28$RemoteUserInfoImplApi28(String str, int i, int i2) {
        this.f768a = new MediaSessionManager.RemoteUserInfo(str, i, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaSessionManagerImplApi28$RemoteUserInfoImplApi28)) {
            return false;
        }
        return this.f768a.equals(((MediaSessionManagerImplApi28$RemoteUserInfoImplApi28) obj).f768a);
    }

    public int hashCode() {
        return ObjectsCompat.a(this.f768a);
    }

    MediaSessionManagerImplApi28$RemoteUserInfoImplApi28(MediaSessionManager.RemoteUserInfo remoteUserInfo) {
        this.f768a = remoteUserInfo;
    }
}
