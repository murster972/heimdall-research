package androidx.media;

import android.media.session.MediaSessionManager;
import android.os.Build;

public final class MediaSessionManager$RemoteUserInfo {

    /* renamed from: a  reason: collision with root package name */
    MediaSessionManager$RemoteUserInfoImpl f767a;

    public MediaSessionManager$RemoteUserInfo(String str, int i, int i2) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.f767a = new MediaSessionManagerImplApi28$RemoteUserInfoImplApi28(str, i, i2);
        } else {
            this.f767a = new MediaSessionManagerImplBase$RemoteUserInfoImplBase(str, i, i2);
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaSessionManager$RemoteUserInfo)) {
            return false;
        }
        return this.f767a.equals(((MediaSessionManager$RemoteUserInfo) obj).f767a);
    }

    public int hashCode() {
        return this.f767a.hashCode();
    }

    public MediaSessionManager$RemoteUserInfo(MediaSessionManager.RemoteUserInfo remoteUserInfo) {
        this.f767a = new MediaSessionManagerImplApi28$RemoteUserInfoImplApi28(remoteUserInfo);
    }
}
