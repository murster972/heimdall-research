package androidx.palette.graphics;

import android.graphics.Color;
import androidx.core.graphics.ColorUtils;
import androidx.palette.graphics.Palette;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

final class ColorCutQuantizer {
    private static final Comparator<Vbox> f = new Comparator<Vbox>() {
        /* renamed from: a */
        public int compare(Vbox vbox, Vbox vbox2) {
            return vbox2.g() - vbox.g();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final int[] f883a;
    final int[] b;
    final List<Palette.Swatch> c;
    final Palette.Filter[] d;
    private final float[] e = new float[3];

    private class Vbox {

        /* renamed from: a  reason: collision with root package name */
        private int f884a;
        private int b;
        private int c;
        private int d;
        private int e;
        private int f;
        private int g;
        private int h;
        private int i;

        Vbox(int i2, int i3) {
            this.f884a = i2;
            this.b = i3;
            c();
        }

        /* access modifiers changed from: package-private */
        public final boolean a() {
            return e() > 1;
        }

        /* access modifiers changed from: package-private */
        public final int b() {
            int f2 = f();
            ColorCutQuantizer colorCutQuantizer = ColorCutQuantizer.this;
            int[] iArr = colorCutQuantizer.f883a;
            int[] iArr2 = colorCutQuantizer.b;
            ColorCutQuantizer.a(iArr, f2, this.f884a, this.b);
            Arrays.sort(iArr, this.f884a, this.b + 1);
            ColorCutQuantizer.a(iArr, f2, this.f884a, this.b);
            int i2 = this.c / 2;
            int i3 = this.f884a;
            int i4 = 0;
            while (true) {
                int i5 = this.b;
                if (i3 > i5) {
                    return this.f884a;
                }
                i4 += iArr2[iArr[i3]];
                if (i4 >= i2) {
                    return Math.min(i5 - 1, i3);
                }
                i3++;
            }
        }

        /* access modifiers changed from: package-private */
        public final void c() {
            ColorCutQuantizer colorCutQuantizer = ColorCutQuantizer.this;
            int[] iArr = colorCutQuantizer.f883a;
            int[] iArr2 = colorCutQuantizer.b;
            int i2 = Integer.MAX_VALUE;
            int i3 = Integer.MIN_VALUE;
            int i4 = Integer.MAX_VALUE;
            int i5 = Integer.MIN_VALUE;
            int i6 = Integer.MAX_VALUE;
            int i7 = Integer.MIN_VALUE;
            int i8 = 0;
            for (int i9 = this.f884a; i9 <= this.b; i9++) {
                int i10 = iArr[i9];
                i8 += iArr2[i10];
                int f2 = ColorCutQuantizer.f(i10);
                int e2 = ColorCutQuantizer.e(i10);
                int d2 = ColorCutQuantizer.d(i10);
                if (f2 > i3) {
                    i3 = f2;
                }
                if (f2 < i2) {
                    i2 = f2;
                }
                if (e2 > i5) {
                    i5 = e2;
                }
                if (e2 < i4) {
                    i4 = e2;
                }
                if (d2 > i7) {
                    i7 = d2;
                }
                if (d2 < i6) {
                    i6 = d2;
                }
            }
            this.d = i2;
            this.e = i3;
            this.f = i4;
            this.g = i5;
            this.h = i6;
            this.i = i7;
            this.c = i8;
        }

        /* access modifiers changed from: package-private */
        public final Palette.Swatch d() {
            ColorCutQuantizer colorCutQuantizer = ColorCutQuantizer.this;
            int[] iArr = colorCutQuantizer.f883a;
            int[] iArr2 = colorCutQuantizer.b;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            for (int i6 = this.f884a; i6 <= this.b; i6++) {
                int i7 = iArr[i6];
                int i8 = iArr2[i7];
                i3 += i8;
                i2 += ColorCutQuantizer.f(i7) * i8;
                i4 += ColorCutQuantizer.e(i7) * i8;
                i5 += i8 * ColorCutQuantizer.d(i7);
            }
            float f2 = (float) i3;
            return new Palette.Swatch(ColorCutQuantizer.a(Math.round(((float) i2) / f2), Math.round(((float) i4) / f2), Math.round(((float) i5) / f2)), i3);
        }

        /* access modifiers changed from: package-private */
        public final int e() {
            return (this.b + 1) - this.f884a;
        }

        /* access modifiers changed from: package-private */
        public final int f() {
            int i2 = this.e - this.d;
            int i3 = this.g - this.f;
            int i4 = this.i - this.h;
            if (i2 < i3 || i2 < i4) {
                return (i3 < i2 || i3 < i4) ? -1 : -2;
            }
            return -3;
        }

        /* access modifiers changed from: package-private */
        public final int g() {
            return ((this.e - this.d) + 1) * ((this.g - this.f) + 1) * ((this.i - this.h) + 1);
        }

        /* access modifiers changed from: package-private */
        public final Vbox h() {
            if (a()) {
                int b2 = b();
                Vbox vbox = new Vbox(b2 + 1, this.b);
                this.b = b2;
                c();
                return vbox;
            }
            throw new IllegalStateException("Can not split a box with only 1 color");
        }
    }

    ColorCutQuantizer(int[] iArr, int i, Palette.Filter[] filterArr) {
        this.d = filterArr;
        int[] iArr2 = new int[32768];
        this.b = iArr2;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            int b2 = b(iArr[i2]);
            iArr[i2] = b2;
            iArr2[b2] = iArr2[b2] + 1;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < iArr2.length; i4++) {
            if (iArr2[i4] > 0 && g(i4)) {
                iArr2[i4] = 0;
            }
            if (iArr2[i4] > 0) {
                i3++;
            }
        }
        int[] iArr3 = new int[i3];
        this.f883a = iArr3;
        int i5 = 0;
        for (int i6 = 0; i6 < iArr2.length; i6++) {
            if (iArr2[i6] > 0) {
                iArr3[i5] = i6;
                i5++;
            }
        }
        if (i3 <= i) {
            this.c = new ArrayList();
            for (int i7 : iArr3) {
                this.c.add(new Palette.Swatch(a(i7), iArr2[i7]));
            }
            return;
        }
        this.c = c(i);
    }

    private static int b(int i) {
        return b(Color.blue(i), 8, 5) | (b(Color.red(i), 8, 5) << 10) | (b(Color.green(i), 8, 5) << 5);
    }

    private static int b(int i, int i2, int i3) {
        return (i3 > i2 ? i << (i3 - i2) : i >> (i2 - i3)) & ((1 << i3) - 1);
    }

    private List<Palette.Swatch> c(int i) {
        PriorityQueue priorityQueue = new PriorityQueue(i, f);
        priorityQueue.offer(new Vbox(0, this.f883a.length - 1));
        a((PriorityQueue<Vbox>) priorityQueue, i);
        return a((Collection<Vbox>) priorityQueue);
    }

    static int d(int i) {
        return i & 31;
    }

    static int e(int i) {
        return (i >> 5) & 31;
    }

    static int f(int i) {
        return (i >> 10) & 31;
    }

    private boolean g(int i) {
        int a2 = a(i);
        ColorUtils.a(a2, this.e);
        return a(a2, this.e);
    }

    /* access modifiers changed from: package-private */
    public List<Palette.Swatch> a() {
        return this.c;
    }

    private void a(PriorityQueue<Vbox> priorityQueue, int i) {
        Vbox poll;
        while (priorityQueue.size() < i && (poll = priorityQueue.poll()) != null && poll.a()) {
            priorityQueue.offer(poll.h());
            priorityQueue.offer(poll);
        }
    }

    private List<Palette.Swatch> a(Collection<Vbox> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (Vbox d2 : collection) {
            Palette.Swatch d3 = d2.d();
            if (!a(d3)) {
                arrayList.add(d3);
            }
        }
        return arrayList;
    }

    static void a(int[] iArr, int i, int i2, int i3) {
        if (i == -3) {
            return;
        }
        if (i == -2) {
            while (i2 <= i3) {
                int i4 = iArr[i2];
                iArr[i2] = d(i4) | (e(i4) << 10) | (f(i4) << 5);
                i2++;
            }
        } else if (i == -1) {
            while (i2 <= i3) {
                int i5 = iArr[i2];
                iArr[i2] = f(i5) | (d(i5) << 10) | (e(i5) << 5);
                i2++;
            }
        }
    }

    private boolean a(Palette.Swatch swatch) {
        return a(swatch.d(), swatch.b());
    }

    private boolean a(int i, float[] fArr) {
        Palette.Filter[] filterArr = this.d;
        if (filterArr != null && filterArr.length > 0) {
            int length = filterArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                if (!this.d[i2].a(i, fArr)) {
                    return true;
                }
            }
        }
        return false;
    }

    static int a(int i, int i2, int i3) {
        return Color.rgb(b(i, 5, 8), b(i2, 5, 8), b(i3, 5, 8));
    }

    private static int a(int i) {
        return a(f(i), e(i), d(i));
    }
}
