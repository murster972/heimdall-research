package androidx.palette.graphics;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseBooleanArray;
import androidx.collection.ArrayMap;
import androidx.core.graphics.ColorUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class Palette {
    static final Filter f = new Filter() {
        private boolean b(float[] fArr) {
            return fArr[0] >= 10.0f && fArr[0] <= 37.0f && fArr[1] <= 0.82f;
        }

        private boolean c(float[] fArr) {
            return fArr[2] >= 0.95f;
        }

        public boolean a(int i, float[] fArr) {
            return !c(fArr) && !a(fArr) && !b(fArr);
        }

        private boolean a(float[] fArr) {
            return fArr[2] <= 0.05f;
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final List<Swatch> f885a;
    private final List<Target> b;
    private final Map<Target, Swatch> c = new ArrayMap();
    private final SparseBooleanArray d = new SparseBooleanArray();
    private final Swatch e = i();

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final List<Swatch> f886a;
        private final Bitmap b;
        private final List<Target> c = new ArrayList();
        private int d = 16;
        private int e = 12544;
        private int f = -1;
        private final List<Filter> g = new ArrayList();
        private Rect h;

        public Builder(Bitmap bitmap) {
            if (bitmap == null || bitmap.isRecycled()) {
                throw new IllegalArgumentException("Bitmap is not valid");
            }
            this.g.add(Palette.f);
            this.b = bitmap;
            this.f886a = null;
            this.c.add(Target.e);
            this.c.add(Target.f);
            this.c.add(Target.g);
            this.c.add(Target.h);
            this.c.add(Target.i);
            this.c.add(Target.j);
        }

        private Bitmap b(Bitmap bitmap) {
            int max;
            int i;
            double d2 = -1.0d;
            if (this.e > 0) {
                int width = bitmap.getWidth() * bitmap.getHeight();
                int i2 = this.e;
                if (width > i2) {
                    d2 = Math.sqrt(((double) i2) / ((double) width));
                }
            } else if (this.f > 0 && (max = Math.max(bitmap.getWidth(), bitmap.getHeight())) > (i = this.f)) {
                d2 = ((double) i) / ((double) max);
            }
            if (d2 <= 0.0d) {
                return bitmap;
            }
            return Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * d2), (int) Math.ceil(((double) bitmap.getHeight()) * d2), false);
        }

        public Builder a(int i) {
            this.d = i;
            return this;
        }

        public Palette a() {
            List<Swatch> list;
            Filter[] filterArr;
            Bitmap bitmap = this.b;
            if (bitmap != null) {
                Bitmap b2 = b(bitmap);
                Rect rect = this.h;
                if (!(b2 == this.b || rect == null)) {
                    double width = ((double) b2.getWidth()) / ((double) this.b.getWidth());
                    rect.left = (int) Math.floor(((double) rect.left) * width);
                    rect.top = (int) Math.floor(((double) rect.top) * width);
                    rect.right = Math.min((int) Math.ceil(((double) rect.right) * width), b2.getWidth());
                    rect.bottom = Math.min((int) Math.ceil(((double) rect.bottom) * width), b2.getHeight());
                }
                int[] a2 = a(b2);
                int i = this.d;
                if (this.g.isEmpty()) {
                    filterArr = null;
                } else {
                    List<Filter> list2 = this.g;
                    filterArr = (Filter[]) list2.toArray(new Filter[list2.size()]);
                }
                ColorCutQuantizer colorCutQuantizer = new ColorCutQuantizer(a2, i, filterArr);
                if (b2 != this.b) {
                    b2.recycle();
                }
                list = colorCutQuantizer.a();
            } else {
                list = this.f886a;
                if (list == null) {
                    throw new AssertionError();
                }
            }
            Palette palette = new Palette(list, this.c);
            palette.a();
            return palette;
        }

        public AsyncTask<Bitmap, Void, Palette> a(final PaletteAsyncListener paletteAsyncListener) {
            if (paletteAsyncListener != null) {
                return new AsyncTask<Bitmap, Void, Palette>() {
                    /* access modifiers changed from: protected */
                    /* renamed from: a */
                    public Palette doInBackground(Bitmap... bitmapArr) {
                        try {
                            return Builder.this.a();
                        } catch (Exception e) {
                            Log.e("Palette", "Exception thrown during async generate", e);
                            return null;
                        }
                    }

                    /* access modifiers changed from: protected */
                    /* renamed from: a */
                    public void onPostExecute(Palette palette) {
                        paletteAsyncListener.a(palette);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Bitmap[]{this.b});
            }
            throw new IllegalArgumentException("listener can not be null");
        }

        private int[] a(Bitmap bitmap) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int[] iArr = new int[(width * height)];
            bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
            Rect rect = this.h;
            if (rect == null) {
                return iArr;
            }
            int width2 = rect.width();
            int height2 = this.h.height();
            int[] iArr2 = new int[(width2 * height2)];
            for (int i = 0; i < height2; i++) {
                Rect rect2 = this.h;
                System.arraycopy(iArr, ((rect2.top + i) * width) + rect2.left, iArr2, i * width2, width2);
            }
            return iArr2;
        }
    }

    public interface Filter {
        boolean a(int i, float[] fArr);
    }

    public interface PaletteAsyncListener {
        void a(Palette palette);
    }

    public static final class Swatch {

        /* renamed from: a  reason: collision with root package name */
        private final int f888a;
        private final int b;
        private final int c;
        private final int d;
        private final int e;
        private boolean f;
        private int g;
        private int h;
        private float[] i;

        public Swatch(int i2, int i3) {
            this.f888a = Color.red(i2);
            this.b = Color.green(i2);
            this.c = Color.blue(i2);
            this.d = i2;
            this.e = i3;
        }

        private void f() {
            int i2;
            int i3;
            if (!this.f) {
                int b2 = ColorUtils.b(-1, this.d, 4.5f);
                int b3 = ColorUtils.b(-1, this.d, 3.0f);
                if (b2 == -1 || b3 == -1) {
                    int b4 = ColorUtils.b(-16777216, this.d, 4.5f);
                    int b5 = ColorUtils.b(-16777216, this.d, 3.0f);
                    if (b4 == -1 || b5 == -1) {
                        if (b2 != -1) {
                            i2 = ColorUtils.d(-1, b2);
                        } else {
                            i2 = ColorUtils.d(-16777216, b4);
                        }
                        this.h = i2;
                        if (b3 != -1) {
                            i3 = ColorUtils.d(-1, b3);
                        } else {
                            i3 = ColorUtils.d(-16777216, b5);
                        }
                        this.g = i3;
                        this.f = true;
                        return;
                    }
                    this.h = ColorUtils.d(-16777216, b4);
                    this.g = ColorUtils.d(-16777216, b5);
                    this.f = true;
                    return;
                }
                this.h = ColorUtils.d(-1, b2);
                this.g = ColorUtils.d(-1, b3);
                this.f = true;
            }
        }

        public int a() {
            f();
            return this.h;
        }

        public float[] b() {
            if (this.i == null) {
                this.i = new float[3];
            }
            ColorUtils.a(this.f888a, this.b, this.c, this.i);
            return this.i;
        }

        public int c() {
            return this.e;
        }

        public int d() {
            return this.d;
        }

        public int e() {
            f();
            return this.g;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Swatch.class != obj.getClass()) {
                return false;
            }
            Swatch swatch = (Swatch) obj;
            if (this.e == swatch.e && this.d == swatch.d) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (this.d * 31) + this.e;
        }

        public String toString() {
            return Swatch.class.getSimpleName() + " [RGB: #" + Integer.toHexString(d()) + ']' + " [HSL: " + Arrays.toString(b()) + ']' + " [Population: " + this.e + ']' + " [Title Text: #" + Integer.toHexString(e()) + ']' + " [Body Text: #" + Integer.toHexString(a()) + ']';
        }
    }

    Palette(List<Swatch> list, List<Target> list2) {
        this.f885a = list;
        this.b = list2;
    }

    public static Builder a(Bitmap bitmap) {
        return new Builder(bitmap);
    }

    private Swatch i() {
        int size = this.f885a.size();
        int i = Integer.MIN_VALUE;
        Swatch swatch = null;
        for (int i2 = 0; i2 < size; i2++) {
            Swatch swatch2 = this.f885a.get(i2);
            if (swatch2.c() > i) {
                i = swatch2.c();
                swatch = swatch2;
            }
        }
        return swatch;
    }

    public Swatch b() {
        return a(Target.j);
    }

    public Swatch c() {
        return a(Target.g);
    }

    public Swatch d() {
        return a(Target.h);
    }

    public Swatch e() {
        return a(Target.e);
    }

    public Swatch f() {
        return a(Target.i);
    }

    public List<Swatch> g() {
        return Collections.unmodifiableList(this.f885a);
    }

    public Swatch h() {
        return a(Target.f);
    }

    private Swatch b(Target target) {
        Swatch c2 = c(target);
        if (c2 != null && target.j()) {
            this.d.append(c2.d(), true);
        }
        return c2;
    }

    private Swatch c(Target target) {
        int size = this.f885a.size();
        float f2 = 0.0f;
        Swatch swatch = null;
        for (int i = 0; i < size; i++) {
            Swatch swatch2 = this.f885a.get(i);
            if (b(swatch2, target)) {
                float a2 = a(swatch2, target);
                if (swatch == null || a2 > f2) {
                    swatch = swatch2;
                    f2 = a2;
                }
            }
        }
        return swatch;
    }

    public Swatch a(Target target) {
        return this.c.get(target);
    }

    public int a(int i) {
        Swatch swatch = this.e;
        return swatch != null ? swatch.d() : i;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            Target target = this.b.get(i);
            target.k();
            this.c.put(target, b(target));
        }
        this.d.clear();
    }

    private boolean b(Swatch swatch, Target target) {
        float[] b2 = swatch.b();
        if (b2[1] < target.e() || b2[1] > target.c() || b2[2] < target.d() || b2[2] > target.b() || this.d.get(swatch.d())) {
            return false;
        }
        return true;
    }

    private float a(Swatch swatch, Target target) {
        float[] b2 = swatch.b();
        Swatch swatch2 = this.e;
        int c2 = swatch2 != null ? swatch2.c() : 1;
        float f2 = 0.0f;
        float abs = target.g() > 0.0f ? (1.0f - Math.abs(b2[1] - target.i())) * target.g() : 0.0f;
        float a2 = target.a() > 0.0f ? target.a() * (1.0f - Math.abs(b2[2] - target.h())) : 0.0f;
        if (target.f() > 0.0f) {
            f2 = target.f() * (((float) swatch.c()) / ((float) c2));
        }
        return abs + a2 + f2;
    }
}
