package androidx.palette.graphics;

public final class Target {
    public static final Target e = new Target();
    public static final Target f = new Target();
    public static final Target g = new Target();
    public static final Target h = new Target();
    public static final Target i = new Target();
    public static final Target j = new Target();

    /* renamed from: a  reason: collision with root package name */
    final float[] f889a = new float[3];
    final float[] b = new float[3];
    final float[] c = new float[3];
    boolean d = true;

    static {
        b(e);
        e(e);
        d(f);
        e(f);
        a(g);
        e(g);
        b(h);
        c(h);
        d(i);
        c(i);
        a(j);
        c(j);
    }

    Target() {
        a(this.f889a);
        a(this.b);
        l();
    }

    private void l() {
        float[] fArr = this.c;
        fArr[0] = 0.24f;
        fArr[1] = 0.52f;
        fArr[2] = 0.24f;
    }

    public float a() {
        return this.c[1];
    }

    public float b() {
        return this.b[2];
    }

    public float c() {
        return this.f889a[2];
    }

    public float d() {
        return this.b[0];
    }

    public float e() {
        return this.f889a[0];
    }

    public float f() {
        return this.c[2];
    }

    public float g() {
        return this.c[0];
    }

    public float h() {
        return this.b[1];
    }

    public float i() {
        return this.f889a[1];
    }

    public boolean j() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void k() {
        float f2 = 0.0f;
        for (float f3 : this.c) {
            if (f3 > 0.0f) {
                f2 += f3;
            }
        }
        if (f2 != 0.0f) {
            int length = this.c.length;
            for (int i2 = 0; i2 < length; i2++) {
                float[] fArr = this.c;
                if (fArr[i2] > 0.0f) {
                    fArr[i2] = fArr[i2] / f2;
                }
            }
        }
    }

    private static void a(float[] fArr) {
        fArr[0] = 0.0f;
        fArr[1] = 0.5f;
        fArr[2] = 1.0f;
    }

    private static void b(Target target) {
        float[] fArr = target.b;
        fArr[0] = 0.55f;
        fArr[1] = 0.74f;
    }

    private static void c(Target target) {
        float[] fArr = target.f889a;
        fArr[1] = 0.3f;
        fArr[2] = 0.4f;
    }

    private static void d(Target target) {
        float[] fArr = target.b;
        fArr[0] = 0.3f;
        fArr[1] = 0.5f;
        fArr[2] = 0.7f;
    }

    private static void e(Target target) {
        float[] fArr = target.f889a;
        fArr[0] = 0.35f;
        fArr[1] = 1.0f;
    }

    private static void a(Target target) {
        float[] fArr = target.b;
        fArr[1] = 0.26f;
        fArr[2] = 0.45f;
    }
}
