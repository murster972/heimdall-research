package androidx.coordinatorlayout;

public final class R$attr {
    public static final int alpha = 2130968627;
    public static final int coordinatorLayoutStyle = 2130968819;
    public static final int font = 2130968899;
    public static final int fontProviderAuthority = 2130968901;
    public static final int fontProviderCerts = 2130968902;
    public static final int fontProviderFetchStrategy = 2130968903;
    public static final int fontProviderFetchTimeout = 2130968904;
    public static final int fontProviderPackage = 2130968905;
    public static final int fontProviderQuery = 2130968906;
    public static final int fontStyle = 2130968907;
    public static final int fontVariationSettings = 2130968908;
    public static final int fontWeight = 2130968909;
    public static final int keylines = 2130968963;
    public static final int layout_anchor = 2130968968;
    public static final int layout_anchorGravity = 2130968969;
    public static final int layout_behavior = 2130968970;
    public static final int layout_dodgeInsetEdges = 2130969014;
    public static final int layout_insetEdge = 2130969023;
    public static final int layout_keyline = 2130969024;
    public static final int statusBarBackground = 2130969215;
    public static final int ttcIndex = 2130969333;

    private R$attr() {
    }
}
