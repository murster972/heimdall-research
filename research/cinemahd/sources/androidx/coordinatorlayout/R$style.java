package androidx.coordinatorlayout;

public final class R$style {
    public static final int TextAppearance_Compat_Notification = 2131820935;
    public static final int TextAppearance_Compat_Notification_Info = 2131820936;
    public static final int TextAppearance_Compat_Notification_Line2 = 2131820938;
    public static final int TextAppearance_Compat_Notification_Time = 2131820941;
    public static final int TextAppearance_Compat_Notification_Title = 2131820943;
    public static final int Widget_Compat_NotificationActionContainer = 2131821157;
    public static final int Widget_Compat_NotificationActionText = 2131821158;
    public static final int Widget_Support_CoordinatorLayout = 2131821211;

    private R$style() {
    }
}
