package androidx.fragment.app;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.core.util.Preconditions;
import androidx.lifecycle.ViewModelStoreOwner;

public class FragmentController {

    /* renamed from: a  reason: collision with root package name */
    private final FragmentHostCallback<?> f680a;

    private FragmentController(FragmentHostCallback<?> fragmentHostCallback) {
        this.f680a = fragmentHostCallback;
    }

    public static FragmentController a(FragmentHostCallback<?> fragmentHostCallback) {
        Preconditions.a(fragmentHostCallback, "callbacks == null");
        return new FragmentController(fragmentHostCallback);
    }

    public void b() {
        this.f680a.d.e();
    }

    public void c() {
        this.f680a.d.f();
    }

    public void d() {
        this.f680a.d.h();
    }

    public void e() {
        this.f680a.d.i();
    }

    public void f() {
        this.f680a.d.k();
    }

    public void g() {
        this.f680a.d.l();
    }

    public void h() {
        this.f680a.d.m();
    }

    public boolean i() {
        return this.f680a.d.c(true);
    }

    public FragmentManager j() {
        return this.f680a.d;
    }

    public void k() {
        this.f680a.d.x();
    }

    public Parcelable l() {
        return this.f680a.d.A();
    }

    public Fragment a(String str) {
        return this.f680a.d.c(str);
    }

    public void b(boolean z) {
        this.f680a.d.b(z);
    }

    public void a(Fragment fragment) {
        FragmentHostCallback<?> fragmentHostCallback = this.f680a;
        fragmentHostCallback.d.a(fragmentHostCallback, (FragmentContainer) fragmentHostCallback, fragment);
    }

    public boolean b(Menu menu) {
        return this.f680a.d.b(menu);
    }

    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        return this.f680a.d.q().onCreateView(view, str, context, attributeSet);
    }

    public boolean b(MenuItem menuItem) {
        return this.f680a.d.b(menuItem);
    }

    public void a(Parcelable parcelable) {
        FragmentHostCallback<?> fragmentHostCallback = this.f680a;
        if (fragmentHostCallback instanceof ViewModelStoreOwner) {
            fragmentHostCallback.d.a(parcelable);
            return;
        }
        throw new IllegalStateException("Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you're still using retainNestedNonConfig().");
    }

    public void a() {
        this.f680a.d.d();
    }

    public void a(boolean z) {
        this.f680a.d.a(z);
    }

    public void a(Configuration configuration) {
        this.f680a.d.a(configuration);
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        return this.f680a.d.a(menu, menuInflater);
    }

    public boolean a(MenuItem menuItem) {
        return this.f680a.d.a(menuItem);
    }

    public void a(Menu menu) {
        this.f680a.d.a(menu);
    }
}
