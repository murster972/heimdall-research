package androidx.fragment.app;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;

class FragmentViewLifecycleOwner implements LifecycleOwner {

    /* renamed from: a  reason: collision with root package name */
    private LifecycleRegistry f717a = null;

    FragmentViewLifecycleOwner() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f717a == null) {
            this.f717a = new LifecycleRegistry(this);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.f717a != null;
    }

    public Lifecycle getLifecycle() {
        a();
        return this.f717a;
    }

    /* access modifiers changed from: package-private */
    public void a(Lifecycle.Event event) {
        this.f717a.a(event);
    }
}
