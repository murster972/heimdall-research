package androidx.fragment.app;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.R$id;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelStoreOwner;

class FragmentStateManager {

    /* renamed from: a  reason: collision with root package name */
    private final FragmentLifecycleCallbacksDispatcher f694a;
    private final Fragment b;
    private int c = -1;

    /* renamed from: androidx.fragment.app.FragmentStateManager$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f695a = new int[Lifecycle.State.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                androidx.lifecycle.Lifecycle$State[] r0 = androidx.lifecycle.Lifecycle.State.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f695a = r0
                int[] r0 = f695a     // Catch:{ NoSuchFieldError -> 0x0014 }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.RESUMED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f695a     // Catch:{ NoSuchFieldError -> 0x001f }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.STARTED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f695a     // Catch:{ NoSuchFieldError -> 0x002a }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.CREATED     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentStateManager.AnonymousClass1.<clinit>():void");
        }
    }

    FragmentStateManager(FragmentLifecycleCallbacksDispatcher fragmentLifecycleCallbacksDispatcher, Fragment fragment) {
        this.f694a = fragmentLifecycleCallbacksDispatcher;
        this.b = fragment;
    }

    private Bundle n() {
        Bundle bundle = new Bundle();
        this.b.performSaveInstanceState(bundle);
        this.f694a.d(this.b, bundle, false);
        if (bundle.isEmpty()) {
            bundle = null;
        }
        if (this.b.mView != null) {
            k();
        }
        if (this.b.mSavedViewState != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", this.b.mSavedViewState);
        }
        if (!this.b.mUserVisibleHint) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", this.b.mUserVisibleHint);
        }
        return bundle;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.c = i;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        int i = this.c;
        Fragment fragment = this.b;
        if (fragment.mFromLayout) {
            if (fragment.mInLayout) {
                i = Math.max(i, 1);
            } else if (i < 2) {
                i = Math.min(i, fragment.mState);
            } else {
                i = Math.min(i, 1);
            }
        }
        if (!this.b.mAdded) {
            i = Math.min(i, 1);
        }
        Fragment fragment2 = this.b;
        if (fragment2.mRemoving) {
            if (fragment2.isInBackStack()) {
                i = Math.min(i, 1);
            } else {
                i = Math.min(i, -1);
            }
        }
        Fragment fragment3 = this.b;
        if (fragment3.mDeferStart && fragment3.mState < 3) {
            i = Math.min(i, 2);
        }
        int i2 = AnonymousClass1.f695a[this.b.mMaxState.ordinal()];
        if (i2 == 1) {
            return i;
        }
        if (i2 == 2) {
            return Math.min(i, 3);
        }
        if (i2 != 3) {
            return Math.min(i, -1);
        }
        return Math.min(i, 1);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto CREATED: " + this.b);
        }
        Fragment fragment = this.b;
        if (!fragment.mIsCreated) {
            this.f694a.c(fragment, fragment.mSavedFragmentState, false);
            Fragment fragment2 = this.b;
            fragment2.performCreate(fragment2.mSavedFragmentState);
            FragmentLifecycleCallbacksDispatcher fragmentLifecycleCallbacksDispatcher = this.f694a;
            Fragment fragment3 = this.b;
            fragmentLifecycleCallbacksDispatcher.b(fragment3, fragment3.mSavedFragmentState, false);
            return;
        }
        fragment.restoreChildFragmentState(fragment.mSavedFragmentState);
        this.b.mState = 1;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        Fragment fragment = this.b;
        if (fragment.mFromLayout && fragment.mInLayout && !fragment.mPerformedCreateView) {
            if (FragmentManager.d(3)) {
                Log.d("FragmentManager", "moveto CREATE_VIEW: " + this.b);
            }
            Fragment fragment2 = this.b;
            fragment2.performCreateView(fragment2.performGetLayoutInflater(fragment2.mSavedFragmentState), (ViewGroup) null, this.b.mSavedFragmentState);
            View view = this.b.mView;
            if (view != null) {
                view.setSaveFromParentEnabled(false);
                Fragment fragment3 = this.b;
                fragment3.mView.setTag(R$id.fragment_container_view_tag, fragment3);
                Fragment fragment4 = this.b;
                if (fragment4.mHidden) {
                    fragment4.mView.setVisibility(8);
                }
                Fragment fragment5 = this.b;
                fragment5.onViewCreated(fragment5.mView, fragment5.mSavedFragmentState);
                FragmentLifecycleCallbacksDispatcher fragmentLifecycleCallbacksDispatcher = this.f694a;
                Fragment fragment6 = this.b;
                fragmentLifecycleCallbacksDispatcher.a(fragment6, fragment6.mView, fragment6.mSavedFragmentState, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Fragment e() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "movefrom RESUMED: " + this.b);
        }
        this.b.performPause();
        this.f694a.c(this.b, false);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto RESTORE_VIEW_STATE: " + this.b);
        }
        Fragment fragment = this.b;
        if (fragment.mView != null) {
            fragment.restoreViewState(fragment.mSavedFragmentState);
        }
        this.b.mSavedFragmentState = null;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto RESUMED: " + this.b);
        }
        this.b.performResume();
        this.f694a.d(this.b, false);
        Fragment fragment = this.b;
        fragment.mSavedFragmentState = null;
        fragment.mSavedViewState = null;
    }

    /* access modifiers changed from: package-private */
    public Fragment.SavedState i() {
        Bundle n;
        if (this.b.mState <= -1 || (n = n()) == null) {
            return null;
        }
        return new Fragment.SavedState(n);
    }

    /* access modifiers changed from: package-private */
    public FragmentState j() {
        FragmentState fragmentState = new FragmentState(this.b);
        if (this.b.mState <= -1 || fragmentState.m != null) {
            fragmentState.m = this.b.mSavedFragmentState;
        } else {
            fragmentState.m = n();
            if (this.b.mTargetWho != null) {
                if (fragmentState.m == null) {
                    fragmentState.m = new Bundle();
                }
                fragmentState.m.putString("android:target_state", this.b.mTargetWho);
                int i = this.b.mTargetRequestCode;
                if (i != 0) {
                    fragmentState.m.putInt("android:target_req_state", i);
                }
            }
        }
        return fragmentState;
    }

    /* access modifiers changed from: package-private */
    public void k() {
        if (this.b.mView != null) {
            SparseArray<Parcelable> sparseArray = new SparseArray<>();
            this.b.mView.saveHierarchyState(sparseArray);
            if (sparseArray.size() > 0) {
                this.b.mSavedViewState = sparseArray;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto STARTED: " + this.b);
        }
        this.b.performStart();
        this.f694a.e(this.b, false);
    }

    /* access modifiers changed from: package-private */
    public void m() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "movefrom STARTED: " + this.b);
        }
        this.b.performStop();
        this.f694a.f(this.b, false);
    }

    /* access modifiers changed from: package-private */
    public void a(ClassLoader classLoader) {
        Bundle bundle = this.b.mSavedFragmentState;
        if (bundle != null) {
            bundle.setClassLoader(classLoader);
            Fragment fragment = this.b;
            fragment.mSavedViewState = fragment.mSavedFragmentState.getSparseParcelableArray("android:view_state");
            Fragment fragment2 = this.b;
            fragment2.mTargetWho = fragment2.mSavedFragmentState.getString("android:target_state");
            Fragment fragment3 = this.b;
            if (fragment3.mTargetWho != null) {
                fragment3.mTargetRequestCode = fragment3.mSavedFragmentState.getInt("android:target_req_state", 0);
            }
            Fragment fragment4 = this.b;
            Boolean bool = fragment4.mSavedUserVisibleHint;
            if (bool != null) {
                fragment4.mUserVisibleHint = bool.booleanValue();
                this.b.mSavedUserVisibleHint = null;
            } else {
                fragment4.mUserVisibleHint = fragment4.mSavedFragmentState.getBoolean("android:user_visible_hint", true);
            }
            Fragment fragment5 = this.b;
            if (!fragment5.mUserVisibleHint) {
                fragment5.mDeferStart = true;
            }
        }
    }

    FragmentStateManager(FragmentLifecycleCallbacksDispatcher fragmentLifecycleCallbacksDispatcher, ClassLoader classLoader, FragmentFactory fragmentFactory, FragmentState fragmentState) {
        this.f694a = fragmentLifecycleCallbacksDispatcher;
        this.b = fragmentFactory.a(classLoader, fragmentState.f693a);
        Bundle bundle = fragmentState.j;
        if (bundle != null) {
            bundle.setClassLoader(classLoader);
        }
        this.b.setArguments(fragmentState.j);
        Fragment fragment = this.b;
        fragment.mWho = fragmentState.b;
        fragment.mFromLayout = fragmentState.c;
        fragment.mRestored = true;
        fragment.mFragmentId = fragmentState.d;
        fragment.mContainerId = fragmentState.e;
        fragment.mTag = fragmentState.f;
        fragment.mRetainInstance = fragmentState.g;
        fragment.mRemoving = fragmentState.h;
        fragment.mDetached = fragmentState.i;
        fragment.mHidden = fragmentState.k;
        fragment.mMaxState = Lifecycle.State.values()[fragmentState.l];
        Bundle bundle2 = fragmentState.m;
        if (bundle2 != null) {
            this.b.mSavedFragmentState = bundle2;
        } else {
            this.b.mSavedFragmentState = new Bundle();
        }
        if (FragmentManager.d(2)) {
            Log.v("FragmentManager", "Instantiated fragment " + this.b);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(FragmentHostCallback<?> fragmentHostCallback, FragmentManager fragmentManager, Fragment fragment) {
        Fragment fragment2 = this.b;
        fragment2.mHost = fragmentHostCallback;
        fragment2.mParentFragment = fragment;
        fragment2.mFragmentManager = fragmentManager;
        this.f694a.b(fragment2, fragmentHostCallback.c(), false);
        this.b.performAttach();
        Fragment fragment3 = this.b;
        Fragment fragment4 = fragment3.mParentFragment;
        if (fragment4 == null) {
            fragmentHostCallback.a(fragment3);
        } else {
            fragment4.onAttachFragment(fragment3);
        }
        this.f694a.a(this.b, fragmentHostCallback.c(), false);
    }

    /* JADX WARNING: type inference failed for: r5v15, types: [android.view.View] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.fragment.app.FragmentContainer r5) {
        /*
            r4 = this;
            androidx.fragment.app.Fragment r0 = r4.b
            boolean r0 = r0.mFromLayout
            if (r0 == 0) goto L_0x0007
            return
        L_0x0007:
            r0 = 3
            boolean r0 = androidx.fragment.app.FragmentManager.d((int) r0)
            if (r0 == 0) goto L_0x0026
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveto CREATE_VIEW: "
            r0.append(r1)
            androidx.fragment.app.Fragment r1 = r4.b
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "FragmentManager"
            android.util.Log.d(r1, r0)
        L_0x0026:
            r0 = 0
            androidx.fragment.app.Fragment r1 = r4.b
            android.view.ViewGroup r2 = r1.mContainer
            if (r2 == 0) goto L_0x0030
            r0 = r2
            goto L_0x00a5
        L_0x0030:
            int r1 = r1.mContainerId
            if (r1 == 0) goto L_0x00a5
            r0 = -1
            if (r1 == r0) goto L_0x0087
            android.view.View r5 = r5.a(r1)
            r0 = r5
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            if (r0 != 0) goto L_0x00a5
            androidx.fragment.app.Fragment r5 = r4.b
            boolean r1 = r5.mRestored
            if (r1 == 0) goto L_0x0047
            goto L_0x00a5
        L_0x0047:
            android.content.res.Resources r5 = r5.getResources()     // Catch:{ NotFoundException -> 0x0054 }
            androidx.fragment.app.Fragment r0 = r4.b     // Catch:{ NotFoundException -> 0x0054 }
            int r0 = r0.mContainerId     // Catch:{ NotFoundException -> 0x0054 }
            java.lang.String r5 = r5.getResourceName(r0)     // Catch:{ NotFoundException -> 0x0054 }
            goto L_0x0056
        L_0x0054:
            java.lang.String r5 = "unknown"
        L_0x0056:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "No view found for id 0x"
            r1.append(r2)
            androidx.fragment.app.Fragment r2 = r4.b
            int r2 = r2.mContainerId
            java.lang.String r2 = java.lang.Integer.toHexString(r2)
            r1.append(r2)
            java.lang.String r2 = " ("
            r1.append(r2)
            r1.append(r5)
            java.lang.String r5 = ") for fragment "
            r1.append(r5)
            androidx.fragment.app.Fragment r5 = r4.b
            r1.append(r5)
            java.lang.String r5 = r1.toString()
            r0.<init>(r5)
            throw r0
        L_0x0087:
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Cannot create fragment "
            r0.append(r1)
            androidx.fragment.app.Fragment r1 = r4.b
            r0.append(r1)
            java.lang.String r1 = " for a container view with no id"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r5.<init>(r0)
            throw r5
        L_0x00a5:
            androidx.fragment.app.Fragment r5 = r4.b
            r5.mContainer = r0
            android.os.Bundle r1 = r5.mSavedFragmentState
            android.view.LayoutInflater r1 = r5.performGetLayoutInflater(r1)
            androidx.fragment.app.Fragment r2 = r4.b
            android.os.Bundle r2 = r2.mSavedFragmentState
            r5.performCreateView(r1, r0, r2)
            androidx.fragment.app.Fragment r5 = r4.b
            android.view.View r5 = r5.mView
            if (r5 == 0) goto L_0x010d
            r1 = 0
            r5.setSaveFromParentEnabled(r1)
            androidx.fragment.app.Fragment r5 = r4.b
            android.view.View r2 = r5.mView
            int r3 = androidx.fragment.R$id.fragment_container_view_tag
            r2.setTag(r3, r5)
            if (r0 == 0) goto L_0x00d2
            androidx.fragment.app.Fragment r5 = r4.b
            android.view.View r5 = r5.mView
            r0.addView(r5)
        L_0x00d2:
            androidx.fragment.app.Fragment r5 = r4.b
            boolean r0 = r5.mHidden
            if (r0 == 0) goto L_0x00df
            android.view.View r5 = r5.mView
            r0 = 8
            r5.setVisibility(r0)
        L_0x00df:
            androidx.fragment.app.Fragment r5 = r4.b
            android.view.View r5 = r5.mView
            androidx.core.view.ViewCompat.J(r5)
            androidx.fragment.app.Fragment r5 = r4.b
            android.view.View r0 = r5.mView
            android.os.Bundle r2 = r5.mSavedFragmentState
            r5.onViewCreated(r0, r2)
            androidx.fragment.app.FragmentLifecycleCallbacksDispatcher r5 = r4.f694a
            androidx.fragment.app.Fragment r0 = r4.b
            android.view.View r2 = r0.mView
            android.os.Bundle r3 = r0.mSavedFragmentState
            r5.a(r0, r2, r3, r1)
            androidx.fragment.app.Fragment r5 = r4.b
            android.view.View r0 = r5.mView
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x010b
            androidx.fragment.app.Fragment r0 = r4.b
            android.view.ViewGroup r0 = r0.mContainer
            if (r0 == 0) goto L_0x010b
            r1 = 1
        L_0x010b:
            r5.mIsNewlyAdded = r1
        L_0x010d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentStateManager.a(androidx.fragment.app.FragmentContainer):void");
    }

    FragmentStateManager(FragmentLifecycleCallbacksDispatcher fragmentLifecycleCallbacksDispatcher, Fragment fragment, FragmentState fragmentState) {
        this.f694a = fragmentLifecycleCallbacksDispatcher;
        this.b = fragment;
        Fragment fragment2 = this.b;
        fragment2.mSavedViewState = null;
        fragment2.mBackStackNesting = 0;
        fragment2.mInLayout = false;
        fragment2.mAdded = false;
        Fragment fragment3 = fragment2.mTarget;
        fragment2.mTargetWho = fragment3 != null ? fragment3.mWho : null;
        Fragment fragment4 = this.b;
        fragment4.mTarget = null;
        Bundle bundle = fragmentState.m;
        if (bundle != null) {
            fragment4.mSavedFragmentState = bundle;
        } else {
            fragment4.mSavedFragmentState = new Bundle();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto ACTIVITY_CREATED: " + this.b);
        }
        Fragment fragment = this.b;
        fragment.performActivityCreated(fragment.mSavedFragmentState);
        FragmentLifecycleCallbacksDispatcher fragmentLifecycleCallbacksDispatcher = this.f694a;
        Fragment fragment2 = this.b;
        fragmentLifecycleCallbacksDispatcher.a(fragment2, fragment2.mSavedFragmentState, false);
    }

    /* access modifiers changed from: package-private */
    public void a(FragmentHostCallback<?> fragmentHostCallback, FragmentManagerViewModel fragmentManagerViewModel) {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "movefrom CREATED: " + this.b);
        }
        Fragment fragment = this.b;
        boolean z = true;
        boolean z2 = fragment.mRemoving && !fragment.isInBackStack();
        if (z2 || fragmentManagerViewModel.f(this.b)) {
            if (fragmentHostCallback instanceof ViewModelStoreOwner) {
                z = fragmentManagerViewModel.d();
            } else if (fragmentHostCallback.c() instanceof Activity) {
                z = true ^ ((Activity) fragmentHostCallback.c()).isChangingConfigurations();
            }
            if (z2 || z) {
                fragmentManagerViewModel.b(this.b);
            }
            this.b.performDestroy();
            this.f694a.a(this.b, false);
            return;
        }
        this.b.mState = 0;
    }

    /* access modifiers changed from: package-private */
    public void a(FragmentManagerViewModel fragmentManagerViewModel) {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "movefrom ATTACHED: " + this.b);
        }
        this.b.performDetach();
        boolean z = false;
        this.f694a.b(this.b, false);
        Fragment fragment = this.b;
        fragment.mState = -1;
        fragment.mHost = null;
        fragment.mParentFragment = null;
        fragment.mFragmentManager = null;
        if (fragment.mRemoving && !fragment.isInBackStack()) {
            z = true;
        }
        if (z || fragmentManagerViewModel.f(this.b)) {
            if (FragmentManager.d(3)) {
                Log.d("FragmentManager", "initState called for fragment: " + this.b);
            }
            this.b.initState();
        }
    }
}
