package androidx.fragment.app;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager.widget.PagerAdapter;
import java.util.ArrayList;

public abstract class FragmentStatePagerAdapter extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private final FragmentManager f696a;
    private final int b;
    private FragmentTransaction c;
    private ArrayList<Fragment.SavedState> d;
    private ArrayList<Fragment> e;
    private Fragment f;
    private boolean g;

    @Deprecated
    public FragmentStatePagerAdapter(FragmentManager fragmentManager) {
        this(fragmentManager, 0);
    }

    public abstract Fragment a(int i);

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (this.c == null) {
            this.c = this.f696a.b();
        }
        while (this.d.size() <= i) {
            this.d.add((Object) null);
        }
        this.d.set(i, fragment.isAdded() ? this.f696a.n(fragment) : null);
        this.e.set(i, (Object) null);
        this.c.a(fragment);
        if (fragment.equals(this.f)) {
            this.f = null;
        }
    }

    /* JADX INFO: finally extract failed */
    public void finishUpdate(ViewGroup viewGroup) {
        FragmentTransaction fragmentTransaction = this.c;
        if (fragmentTransaction != null) {
            if (!this.g) {
                try {
                    this.g = true;
                    fragmentTransaction.d();
                    this.g = false;
                } catch (Throwable th) {
                    this.g = false;
                    throw th;
                }
            }
            this.c = null;
        }
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        Fragment.SavedState savedState;
        Fragment fragment;
        if (this.e.size() > i && (fragment = this.e.get(i)) != null) {
            return fragment;
        }
        if (this.c == null) {
            this.c = this.f696a.b();
        }
        Fragment a2 = a(i);
        if (this.d.size() > i && (savedState = this.d.get(i)) != null) {
            a2.setInitialSavedState(savedState);
        }
        while (this.e.size() <= i) {
            this.e.add((Object) null);
        }
        a2.setMenuVisibility(false);
        if (this.b == 0) {
            a2.setUserVisibleHint(false);
        }
        this.e.set(i, a2);
        this.c.a(viewGroup.getId(), a2);
        if (this.b == 1) {
            this.c.a(a2, Lifecycle.State.STARTED);
        }
        return a2;
    }

    public boolean isViewFromObject(View view, Object obj) {
        return ((Fragment) obj).getView() == view;
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
        if (parcelable != null) {
            Bundle bundle = (Bundle) parcelable;
            bundle.setClassLoader(classLoader);
            Parcelable[] parcelableArray = bundle.getParcelableArray("states");
            this.d.clear();
            this.e.clear();
            if (parcelableArray != null) {
                for (Parcelable parcelable2 : parcelableArray) {
                    this.d.add((Fragment.SavedState) parcelable2);
                }
            }
            for (String str : bundle.keySet()) {
                if (str.startsWith("f")) {
                    int parseInt = Integer.parseInt(str.substring(1));
                    Fragment a2 = this.f696a.a(bundle, str);
                    if (a2 != null) {
                        while (this.e.size() <= parseInt) {
                            this.e.add((Object) null);
                        }
                        a2.setMenuVisibility(false);
                        this.e.set(parseInt, a2);
                    } else {
                        Log.w("FragmentStatePagerAdapt", "Bad fragment at key " + str);
                    }
                }
            }
        }
    }

    public Parcelable saveState() {
        Bundle bundle;
        if (this.d.size() > 0) {
            bundle = new Bundle();
            Fragment.SavedState[] savedStateArr = new Fragment.SavedState[this.d.size()];
            this.d.toArray(savedStateArr);
            bundle.putParcelableArray("states", savedStateArr);
        } else {
            bundle = null;
        }
        for (int i = 0; i < this.e.size(); i++) {
            Fragment fragment = this.e.get(i);
            if (fragment != null && fragment.isAdded()) {
                if (bundle == null) {
                    bundle = new Bundle();
                }
                this.f696a.a(bundle, "f" + i, fragment);
            }
        }
        return bundle;
    }

    public void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        Fragment fragment2 = this.f;
        if (fragment != fragment2) {
            if (fragment2 != null) {
                fragment2.setMenuVisibility(false);
                if (this.b == 1) {
                    if (this.c == null) {
                        this.c = this.f696a.b();
                    }
                    this.c.a(this.f, Lifecycle.State.STARTED);
                } else {
                    this.f.setUserVisibleHint(false);
                }
            }
            fragment.setMenuVisibility(true);
            if (this.b == 1) {
                if (this.c == null) {
                    this.c = this.f696a.b();
                }
                this.c.a(fragment, Lifecycle.State.RESUMED);
            } else {
                fragment.setUserVisibleHint(true);
            }
            this.f = fragment;
        }
    }

    public void startUpdate(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            throw new IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }

    public FragmentStatePagerAdapter(FragmentManager fragmentManager, int i) {
        this.c = null;
        this.d = new ArrayList<>();
        this.e = new ArrayList<>();
        this.f = null;
        this.f696a = fragmentManager;
        this.b = i;
    }
}
