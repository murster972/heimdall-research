package androidx.fragment.app;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.collection.ArraySet;
import androidx.core.os.CancellationSignal;
import androidx.core.util.LogWriter;
import androidx.fragment.R$id;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentAnim;
import androidx.fragment.app.FragmentTransition;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelStore;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class FragmentManager {
    private static boolean F = false;
    private ArrayList<Boolean> A;
    private ArrayList<Fragment> B;
    private ArrayList<StartEnterTransitionListener> C;
    private FragmentManagerViewModel D;
    private Runnable E = new Runnable() {
        public void run() {
            FragmentManager.this.c(true);
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<OpGenerator> f686a = new ArrayList<>();
    private boolean b;
    private final FragmentStore c = new FragmentStore();
    ArrayList<BackStackRecord> d;
    private ArrayList<Fragment> e;
    private final FragmentLayoutInflaterFactory f = new FragmentLayoutInflaterFactory(this);
    private OnBackPressedDispatcher g;
    private final OnBackPressedCallback h = new OnBackPressedCallback(false) {
        public void a() {
            FragmentManager.this.u();
        }
    };
    private final AtomicInteger i = new AtomicInteger();
    private ArrayList<OnBackStackChangedListener> j;
    private ConcurrentHashMap<Fragment, HashSet<CancellationSignal>> k = new ConcurrentHashMap<>();
    private final FragmentTransition.Callback l = new FragmentTransition.Callback() {
        public void a(Fragment fragment, CancellationSignal cancellationSignal) {
            if (!cancellationSignal.b()) {
                FragmentManager.this.b(fragment, cancellationSignal);
            }
        }

        public void b(Fragment fragment, CancellationSignal cancellationSignal) {
            FragmentManager.this.a(fragment, cancellationSignal);
        }
    };
    private final FragmentLifecycleCallbacksDispatcher m = new FragmentLifecycleCallbacksDispatcher(this);
    int n = -1;
    FragmentHostCallback<?> o;
    FragmentContainer p;
    private Fragment q;
    Fragment r;
    private FragmentFactory s = null;
    private FragmentFactory t = new FragmentFactory() {
        public Fragment a(ClassLoader classLoader, String str) {
            FragmentHostCallback<?> fragmentHostCallback = FragmentManager.this.o;
            return fragmentHostCallback.a(fragmentHostCallback.c(), str, (Bundle) null);
        }
    };
    private boolean u;
    private boolean v;
    private boolean w;
    private boolean x;
    private boolean y;
    private ArrayList<BackStackRecord> z;

    public interface BackStackEntry {
    }

    public static abstract class FragmentLifecycleCallbacks {
        public void a(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void a(FragmentManager fragmentManager, Fragment fragment, Context context) {
        }

        public void a(FragmentManager fragmentManager, Fragment fragment, Bundle bundle) {
        }

        public void a(FragmentManager fragmentManager, Fragment fragment, View view, Bundle bundle) {
        }

        public void b(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void b(FragmentManager fragmentManager, Fragment fragment, Context context) {
        }

        public void b(FragmentManager fragmentManager, Fragment fragment, Bundle bundle) {
        }

        public void c(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void c(FragmentManager fragmentManager, Fragment fragment, Bundle bundle) {
        }

        public void d(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void d(FragmentManager fragmentManager, Fragment fragment, Bundle bundle) {
        }

        public void e(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void f(FragmentManager fragmentManager, Fragment fragment) {
        }

        public void g(FragmentManager fragmentManager, Fragment fragment) {
        }
    }

    public interface OnBackStackChangedListener {
        void a();
    }

    interface OpGenerator {
        boolean a(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2);
    }

    private class PopBackStackState implements OpGenerator {

        /* renamed from: a  reason: collision with root package name */
        final String f690a;
        final int b;
        final int c;

        PopBackStackState(String str, int i, int i2) {
            this.f690a = str;
            this.b = i;
            this.c = i2;
        }

        public boolean a(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2) {
            Fragment fragment = FragmentManager.this.r;
            if (fragment != null && this.b < 0 && this.f690a == null && fragment.getChildFragmentManager().z()) {
                return false;
            }
            return FragmentManager.this.a(arrayList, arrayList2, this.f690a, this.b, this.c);
        }
    }

    static class StartEnterTransitionListener implements Fragment.OnStartEnterTransitionListener {

        /* renamed from: a  reason: collision with root package name */
        final boolean f691a;
        final BackStackRecord b;
        private int c;

        StartEnterTransitionListener(BackStackRecord backStackRecord, boolean z) {
            this.f691a = z;
            this.b = backStackRecord;
        }

        public void a() {
            this.c++;
        }

        public void b() {
            this.c--;
            if (this.c == 0) {
                this.b.r.B();
            }
        }

        /* access modifiers changed from: package-private */
        public void c() {
            BackStackRecord backStackRecord = this.b;
            backStackRecord.r.a(backStackRecord, this.f691a, false, false);
        }

        /* access modifiers changed from: package-private */
        public void d() {
            boolean z = this.c > 0;
            for (Fragment next : this.b.r.p()) {
                next.setOnStartEnterTransitionListener((Fragment.OnStartEnterTransitionListener) null);
                if (z && next.isPostponed()) {
                    next.startPostponedEnterTransition();
                }
            }
            BackStackRecord backStackRecord = this.b;
            backStackRecord.r.a(backStackRecord, this.f691a, !z, true);
        }

        public boolean e() {
            return this.c == 0;
        }
    }

    private void C() {
        if (w()) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }

    private void D() {
        this.b = false;
        this.A.clear();
        this.z.clear();
    }

    private void E() {
        if (this.y) {
            this.y = false;
            I();
        }
    }

    private void F() {
        if (!this.k.isEmpty()) {
            for (Fragment next : this.k.keySet()) {
                q(next);
                a(next, next.getStateAfterAnimating());
            }
        }
    }

    private void G() {
        if (this.C != null) {
            while (!this.C.isEmpty()) {
                this.C.remove(0).d();
            }
        }
    }

    private void H() {
        if (this.j != null) {
            for (int i2 = 0; i2 < this.j.size(); i2++) {
                this.j.get(i2).a();
            }
        }
    }

    private void I() {
        for (Fragment next : this.c.b()) {
            if (next != null) {
                k(next);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        if (n() <= 0) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        if (g(r3.q) == false) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0025, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0026, code lost:
        r0.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0029, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        r0 = r3.h;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void J() {
        /*
            r3 = this;
            java.util.ArrayList<androidx.fragment.app.FragmentManager$OpGenerator> r0 = r3.f686a
            monitor-enter(r0)
            java.util.ArrayList<androidx.fragment.app.FragmentManager$OpGenerator> r1 = r3.f686a     // Catch:{ all -> 0x002a }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x002a }
            r2 = 1
            if (r1 != 0) goto L_0x0013
            androidx.activity.OnBackPressedCallback r1 = r3.h     // Catch:{ all -> 0x002a }
            r1.a((boolean) r2)     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x0013:
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            androidx.activity.OnBackPressedCallback r0 = r3.h
            int r1 = r3.n()
            if (r1 <= 0) goto L_0x0025
            androidx.fragment.app.Fragment r1 = r3.q
            boolean r1 = r3.g(r1)
            if (r1 == 0) goto L_0x0025
            goto L_0x0026
        L_0x0025:
            r2 = 0
        L_0x0026:
            r0.a((boolean) r2)
            return
        L_0x002a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentManager.J():void");
    }

    private void a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new LogWriter("FragmentManager"));
        FragmentHostCallback<?> fragmentHostCallback = this.o;
        if (fragmentHostCallback != null) {
            try {
                fragmentHostCallback.a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        } else {
            try {
                a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e3) {
                Log.e("FragmentManager", "Failed dumping state", e3);
            }
        }
        throw runtimeException;
    }

    static boolean d(int i2) {
        return F || Log.isLoggable("FragmentManager", i2);
    }

    static int e(int i2) {
        if (i2 == 4097) {
            return 8194;
        }
        if (i2 != 4099) {
            return i2 != 8194 ? 0 : 4097;
        }
        return 4099;
    }

    private void q(Fragment fragment) {
        HashSet hashSet = this.k.get(fragment);
        if (hashSet != null) {
            Iterator it2 = hashSet.iterator();
            while (it2.hasNext()) {
                ((CancellationSignal) it2.next()).a();
            }
            hashSet.clear();
            s(fragment);
            this.k.remove(fragment);
        }
    }

    private void r(final Fragment fragment) {
        Animator animator;
        if (fragment.mView != null) {
            FragmentAnim.AnimationOrAnimator a2 = FragmentAnim.a(this.o.c(), this.p, fragment, !fragment.mHidden);
            if (a2 == null || (animator = a2.b) == null) {
                if (a2 != null) {
                    fragment.mView.startAnimation(a2.f677a);
                    a2.f677a.start();
                }
                fragment.mView.setVisibility((!fragment.mHidden || fragment.isHideReplaced()) ? 0 : 8);
                if (fragment.isHideReplaced()) {
                    fragment.setHideReplaced(false);
                }
            } else {
                animator.setTarget(fragment.mView);
                if (!fragment.mHidden) {
                    fragment.mView.setVisibility(0);
                } else if (fragment.isHideReplaced()) {
                    fragment.setHideReplaced(false);
                } else {
                    final ViewGroup viewGroup = fragment.mContainer;
                    final View view = fragment.mView;
                    viewGroup.startViewTransition(view);
                    a2.b.addListener(new AnimatorListenerAdapter(this) {
                        public void onAnimationEnd(Animator animator) {
                            viewGroup.endViewTransition(view);
                            animator.removeListener(this);
                            Fragment fragment = fragment;
                            View view = fragment.mView;
                            if (view != null && fragment.mHidden) {
                                view.setVisibility(8);
                            }
                        }
                    });
                }
                a2.b.start();
            }
        }
        if (fragment.mAdded && w(fragment)) {
            this.u = true;
        }
        fragment.mHiddenChanged = false;
        fragment.onHiddenChanged(fragment.mHidden);
    }

    private void s(Fragment fragment) {
        fragment.performDestroyView();
        this.m.g(fragment, false);
        fragment.mContainer = null;
        fragment.mView = null;
        fragment.mViewLifecycleOwner = null;
        fragment.mViewLifecycleOwnerLiveData.b(null);
        fragment.mInLayout = false;
    }

    private void t(Fragment fragment) {
        if (fragment != null && fragment.equals(a(fragment.mWho))) {
            fragment.performPrimaryNavigationFragmentChanged();
        }
    }

    private void x(Fragment fragment) {
        ViewGroup v2 = v(fragment);
        if (v2 != null) {
            if (v2.getTag(R$id.visible_removing_fragment_view_tag) == null) {
                v2.setTag(R$id.visible_removing_fragment_view_tag, fragment);
            }
            ((Fragment) v2.getTag(R$id.visible_removing_fragment_view_tag)).setNextAnim(fragment.getNextAnim());
        }
    }

    /* access modifiers changed from: package-private */
    public Parcelable A() {
        int size;
        G();
        F();
        c(true);
        this.v = true;
        ArrayList<FragmentState> e2 = this.c.e();
        BackStackState[] backStackStateArr = null;
        if (e2.isEmpty()) {
            if (d(2)) {
                Log.v("FragmentManager", "saveAllState: no fragments!");
            }
            return null;
        }
        ArrayList<String> f2 = this.c.f();
        ArrayList<BackStackRecord> arrayList = this.d;
        if (arrayList != null && (size = arrayList.size()) > 0) {
            backStackStateArr = new BackStackState[size];
            for (int i2 = 0; i2 < size; i2++) {
                backStackStateArr[i2] = new BackStackState(this.d.get(i2));
                if (d(2)) {
                    Log.v("FragmentManager", "saveAllState: adding back stack #" + i2 + ": " + this.d.get(i2));
                }
            }
        }
        FragmentManagerState fragmentManagerState = new FragmentManagerState();
        fragmentManagerState.f692a = e2;
        fragmentManagerState.b = f2;
        fragmentManagerState.c = backStackStateArr;
        fragmentManagerState.d = this.i.get();
        Fragment fragment = this.r;
        if (fragment != null) {
            fragmentManagerState.e = fragment.mWho;
        }
        return fragmentManagerState;
    }

    /* access modifiers changed from: package-private */
    public void B() {
        synchronized (this.f686a) {
            boolean z2 = false;
            boolean z3 = this.C != null && !this.C.isEmpty();
            if (this.f686a.size() == 1) {
                z2 = true;
            }
            if (z3 || z2) {
                this.o.d().removeCallbacks(this.E);
                this.o.d().post(this.E);
                J();
            }
        }
    }

    public FragmentTransaction b() {
        return new BackStackRecord(this);
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment) {
        if (d(2)) {
            Log.v("FragmentManager", "attach: " + fragment);
        }
        if (fragment.mDetached) {
            fragment.mDetached = false;
            if (!fragment.mAdded) {
                this.c.a(fragment);
                if (d(2)) {
                    Log.v("FragmentManager", "add from attach: " + fragment);
                }
                if (w(fragment)) {
                    this.u = true;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ViewModelStore e(Fragment fragment) {
        return this.D.d(fragment);
    }

    /* access modifiers changed from: package-private */
    public void f(Fragment fragment) {
        if (d(2)) {
            Log.v("FragmentManager", "hide: " + fragment);
        }
        if (!fragment.mHidden) {
            fragment.mHidden = true;
            fragment.mHiddenChanged = true ^ fragment.mHiddenChanged;
            x(fragment);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean g(Fragment fragment) {
        if (fragment == null) {
            return true;
        }
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (!fragment.equals(fragmentManager.t()) || !g(fragmentManager.q)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void h(Fragment fragment) {
        if (!this.c.a(fragment.mWho)) {
            FragmentStateManager fragmentStateManager = new FragmentStateManager(this.m, fragment);
            fragmentStateManager.a(this.o.c().getClassLoader());
            this.c.a(fragmentStateManager);
            if (fragment.mRetainInstanceChangedWhileDetached) {
                if (fragment.mRetainInstance) {
                    b(fragment);
                } else {
                    m(fragment);
                }
                fragment.mRetainInstanceChangedWhileDetached = false;
            }
            fragmentStateManager.a(this.n);
            if (d(2)) {
                Log.v("FragmentManager", "Added fragment to active set " + fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0049, code lost:
        r0 = r0.mView;
        r1 = r4.mContainer;
        r0 = r1.indexOfChild(r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void i(androidx.fragment.app.Fragment r4) {
        /*
            r3 = this;
            androidx.fragment.app.FragmentStore r0 = r3.c
            java.lang.String r1 = r4.mWho
            boolean r0 = r0.a((java.lang.String) r1)
            if (r0 != 0) goto L_0x003a
            r0 = 3
            boolean r0 = d((int) r0)
            if (r0 == 0) goto L_0x0039
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Ignoring moving "
            r0.append(r1)
            r0.append(r4)
            java.lang.String r4 = " to state "
            r0.append(r4)
            int r4 = r3.n
            r0.append(r4)
            java.lang.String r4 = "since it is not added to "
            r0.append(r4)
            r0.append(r3)
            java.lang.String r4 = r0.toString()
            java.lang.String r0 = "FragmentManager"
            android.util.Log.d(r0, r4)
        L_0x0039:
            return
        L_0x003a:
            r3.j(r4)
            android.view.View r0 = r4.mView
            if (r0 == 0) goto L_0x009f
            androidx.fragment.app.FragmentStore r0 = r3.c
            androidx.fragment.app.Fragment r0 = r0.b((androidx.fragment.app.Fragment) r4)
            if (r0 == 0) goto L_0x0061
            android.view.View r0 = r0.mView
            android.view.ViewGroup r1 = r4.mContainer
            int r0 = r1.indexOfChild(r0)
            android.view.View r2 = r4.mView
            int r2 = r1.indexOfChild(r2)
            if (r2 >= r0) goto L_0x0061
            r1.removeViewAt(r2)
            android.view.View r2 = r4.mView
            r1.addView(r2, r0)
        L_0x0061:
            boolean r0 = r4.mIsNewlyAdded
            if (r0 == 0) goto L_0x009f
            android.view.ViewGroup r0 = r4.mContainer
            if (r0 == 0) goto L_0x009f
            float r0 = r4.mPostponedAlpha
            r1 = 0
            int r2 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r2 <= 0) goto L_0x0075
            android.view.View r2 = r4.mView
            r2.setAlpha(r0)
        L_0x0075:
            r4.mPostponedAlpha = r1
            r0 = 0
            r4.mIsNewlyAdded = r0
            androidx.fragment.app.FragmentHostCallback<?> r0 = r3.o
            android.content.Context r0 = r0.c()
            androidx.fragment.app.FragmentContainer r1 = r3.p
            r2 = 1
            androidx.fragment.app.FragmentAnim$AnimationOrAnimator r0 = androidx.fragment.app.FragmentAnim.a(r0, r1, r4, r2)
            if (r0 == 0) goto L_0x009f
            android.view.animation.Animation r1 = r0.f677a
            if (r1 == 0) goto L_0x0093
            android.view.View r0 = r4.mView
            r0.startAnimation(r1)
            goto L_0x009f
        L_0x0093:
            android.animation.Animator r1 = r0.b
            android.view.View r2 = r4.mView
            r1.setTarget(r2)
            android.animation.Animator r0 = r0.b
            r0.start()
        L_0x009f:
            boolean r0 = r4.mHiddenChanged
            if (r0 == 0) goto L_0x00a6
            r3.r(r4)
        L_0x00a6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentManager.i(androidx.fragment.app.Fragment):void");
    }

    /* access modifiers changed from: package-private */
    public void j(Fragment fragment) {
        a(fragment, this.n);
    }

    /* access modifiers changed from: package-private */
    public void k(Fragment fragment) {
        if (!fragment.mDeferStart) {
            return;
        }
        if (this.b) {
            this.y = true;
            return;
        }
        fragment.mDeferStart = false;
        a(fragment, this.n);
    }

    /* access modifiers changed from: package-private */
    public void l(Fragment fragment) {
        if (d(2)) {
            Log.v("FragmentManager", "remove: " + fragment + " nesting=" + fragment.mBackStackNesting);
        }
        boolean z2 = !fragment.isInBackStack();
        if (!fragment.mDetached || z2) {
            this.c.c(fragment);
            if (w(fragment)) {
                this.u = true;
            }
            fragment.mRemoving = true;
            x(fragment);
        }
    }

    /* access modifiers changed from: package-private */
    public void m(Fragment fragment) {
        if (w()) {
            if (d(2)) {
                Log.v("FragmentManager", "Ignoring removeRetainedFragment as the state is already saved");
            }
        } else if (this.D.e(fragment) && d(2)) {
            Log.v("FragmentManager", "Updating retained Fragments: Removed " + fragment);
        }
    }

    public int n() {
        ArrayList<BackStackRecord> arrayList = this.d;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void o(Fragment fragment) {
        if (fragment == null || (fragment.equals(a(fragment.mWho)) && (fragment.mHost == null || fragment.mFragmentManager == this))) {
            Fragment fragment2 = this.r;
            this.r = fragment;
            t(fragment2);
            t(this.r);
            return;
        }
        throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
    }

    public List<Fragment> p() {
        return this.c.c();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        Fragment fragment = this.q;
        if (fragment != null) {
            sb.append(fragment.getClass().getSimpleName());
            sb.append("{");
            sb.append(Integer.toHexString(System.identityHashCode(this.q)));
            sb.append("}");
        } else {
            FragmentHostCallback<?> fragmentHostCallback = this.o;
            if (fragmentHostCallback != null) {
                sb.append(fragmentHostCallback.getClass().getSimpleName());
                sb.append("{");
                sb.append(Integer.toHexString(System.identityHashCode(this.o)));
                sb.append("}");
            } else {
                sb.append("null");
            }
        }
        sb.append("}}");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void u() {
        c(true);
        if (this.h.b()) {
            z();
        } else {
            this.g.a();
        }
    }

    public boolean v() {
        return this.x;
    }

    public boolean w() {
        return this.v || this.w;
    }

    public void y() {
        a((OpGenerator) new PopBackStackState((String) null, -1, 0), false);
    }

    public boolean z() {
        return a((String) null, -1, 0);
    }

    private ViewGroup v(Fragment fragment) {
        if (fragment.mContainerId > 0 && this.p.a()) {
            View a2 = this.p.a(fragment.mContainerId);
            if (a2 instanceof ViewGroup) {
                return (ViewGroup) a2;
            }
        }
        return null;
    }

    private boolean w(Fragment fragment) {
        return (fragment.mHasMenu && fragment.mMenuVisible) || fragment.mChildFragmentManager.c();
    }

    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, CancellationSignal cancellationSignal) {
        HashSet hashSet = this.k.get(fragment);
        if (hashSet != null && hashSet.remove(cancellationSignal) && hashSet.isEmpty()) {
            this.k.remove(fragment);
            if (fragment.mState < 3) {
                s(fragment);
                a(fragment, fragment.getStateAfterAnimating());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Fragment fragment) {
        if (d(2)) {
            Log.v("FragmentManager", "detach: " + fragment);
        }
        if (!fragment.mDetached) {
            fragment.mDetached = true;
            if (fragment.mAdded) {
                if (d(2)) {
                    Log.v("FragmentManager", "remove from detach: " + fragment);
                }
                this.c.c(fragment);
                if (w(fragment)) {
                    this.u = true;
                }
                x(fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.v = false;
        this.w = false;
        c(1);
    }

    /* access modifiers changed from: package-private */
    public void j() {
        J();
        t(this.r);
    }

    public Fragment.SavedState n(Fragment fragment) {
        FragmentStateManager e2 = this.c.e(fragment.mWho);
        if (e2 != null && e2.e().equals(fragment)) {
            return e2.i();
        }
        a((RuntimeException) new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        throw null;
    }

    /* access modifiers changed from: package-private */
    public void p(Fragment fragment) {
        if (d(2)) {
            Log.v("FragmentManager", "show: " + fragment);
        }
        if (fragment.mHidden) {
            fragment.mHidden = false;
            fragment.mHiddenChanged = !fragment.mHiddenChanged;
        }
    }

    public Fragment t() {
        return this.r;
    }

    private FragmentManagerViewModel u(Fragment fragment) {
        return this.D.c(fragment);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        c(1);
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.x = true;
        c(true);
        F();
        c(-1);
        this.o = null;
        this.p = null;
        this.q = null;
        if (this.g != null) {
            this.h.c();
            this.g = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        this.v = false;
        this.w = false;
        c(4);
    }

    /* access modifiers changed from: package-private */
    public void x() {
        if (this.o != null) {
            this.v = false;
            this.w = false;
            for (Fragment next : this.c.c()) {
                if (next != null) {
                    next.noteStateNotSaved();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void m() {
        this.w = true;
        c(2);
    }

    public FragmentFactory o() {
        FragmentFactory fragmentFactory = this.s;
        if (fragmentFactory != null) {
            return fragmentFactory;
        }
        Fragment fragment = this.q;
        if (fragment != null) {
            return fragment.mFragmentManager.o();
        }
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public LayoutInflater.Factory2 q() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void b(Fragment fragment) {
        if (w()) {
            if (d(2)) {
                Log.v("FragmentManager", "Ignoring addRetainedFragment as the state is already saved");
            }
        } else if (this.D.a(fragment) && d(2)) {
            Log.v("FragmentManager", "Updating retained Fragments: Added " + fragment);
        }
    }

    /* access modifiers changed from: package-private */
    public Fragment s() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public Fragment c(String str) {
        return this.c.d(str);
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public boolean c(boolean z2) {
        d(z2);
        boolean z3 = false;
        while (b(this.z, this.A)) {
            this.b = true;
            try {
                c(this.z, this.A);
                D();
                z3 = true;
            } catch (Throwable th) {
                D();
                throw th;
            }
        }
        J();
        E();
        this.c.a();
        return z3;
    }

    /* access modifiers changed from: package-private */
    public void l() {
        this.v = false;
        this.w = false;
        c(3);
    }

    private void d(boolean z2) {
        if (this.b) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        } else if (this.o == null) {
            if (this.x) {
                throw new IllegalStateException("FragmentManager has been destroyed");
            }
            throw new IllegalStateException("FragmentManager has not been attached to a host.");
        } else if (Looper.myLooper() == this.o.d().getLooper()) {
            if (!z2) {
                C();
            }
            if (this.z == null) {
                this.z = new ArrayList<>();
                this.A = new ArrayList<>();
            }
            this.b = true;
            try {
                a((ArrayList<BackStackRecord>) null, (ArrayList<Boolean>) null);
            } finally {
                this.b = false;
            }
        } else {
            throw new IllegalStateException("Must be called from main thread of fragment host");
        }
    }

    public void a(int i2, int i3) {
        if (i2 >= 0) {
            a((OpGenerator) new PopBackStackState((String) null, i2, i3), false);
            return;
        }
        throw new IllegalArgumentException("Bad id: " + i2);
    }

    /* access modifiers changed from: package-private */
    public void h() {
        for (Fragment next : this.c.c()) {
            if (next != null) {
                next.performLowMemory();
            }
        }
    }

    private boolean a(String str, int i2, int i3) {
        c(false);
        d(true);
        Fragment fragment = this.r;
        if (fragment != null && i2 < 0 && str == null && fragment.getChildFragmentManager().z()) {
            return true;
        }
        boolean a2 = a(this.z, this.A, str, i2, i3);
        if (a2) {
            this.b = true;
            try {
                c(this.z, this.A);
            } finally {
                D();
            }
        }
        J();
        E();
        this.c.a();
        return a2;
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2) {
        return this.n >= i2;
    }

    public Fragment b(String str) {
        return this.c.c(str);
    }

    /* access modifiers changed from: package-private */
    public void b(OpGenerator opGenerator, boolean z2) {
        if (!z2 || (this.o != null && !this.x)) {
            d(z2);
            if (opGenerator.a(this.z, this.A)) {
                this.b = true;
                try {
                    c(this.z, this.A);
                } finally {
                    D();
                }
            }
            J();
            E();
            this.c.a();
        }
    }

    private void c(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2) {
        if (!arrayList.isEmpty()) {
            if (arrayList.size() == arrayList2.size()) {
                a(arrayList, arrayList2);
                int size = arrayList.size();
                int i2 = 0;
                int i3 = 0;
                while (i2 < size) {
                    if (!arrayList.get(i2).p) {
                        if (i3 != i2) {
                            b(arrayList, arrayList2, i3, i2);
                        }
                        i3 = i2 + 1;
                        if (arrayList2.get(i2).booleanValue()) {
                            while (i3 < size && arrayList2.get(i3).booleanValue() && !arrayList.get(i3).p) {
                                i3++;
                            }
                        }
                        b(arrayList, arrayList2, i2, i3);
                        i2 = i3 - 1;
                    }
                    i2++;
                }
                if (i3 != size) {
                    b(arrayList, arrayList2, i3, size);
                    return;
                }
                return;
            }
            throw new IllegalStateException("Internal error with the back stack records");
        }
    }

    private void b(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        int i4;
        ArrayList<BackStackRecord> arrayList3 = arrayList;
        ArrayList<Boolean> arrayList4 = arrayList2;
        int i5 = i2;
        int i6 = i3;
        boolean z2 = arrayList3.get(i5).p;
        ArrayList<Fragment> arrayList5 = this.B;
        if (arrayList5 == null) {
            this.B = new ArrayList<>();
        } else {
            arrayList5.clear();
        }
        this.B.addAll(this.c.c());
        Fragment t2 = t();
        boolean z3 = false;
        for (int i7 = i5; i7 < i6; i7++) {
            BackStackRecord backStackRecord = arrayList3.get(i7);
            if (!arrayList4.get(i7).booleanValue()) {
                t2 = backStackRecord.a(this.B, t2);
            } else {
                t2 = backStackRecord.b(this.B, t2);
            }
            z3 = z3 || backStackRecord.g;
        }
        this.B.clear();
        if (!z2) {
            FragmentTransition.a(this, arrayList, arrayList2, i2, i3, false, this.l);
        }
        a(arrayList, arrayList2, i2, i3);
        if (z2) {
            ArraySet arraySet = new ArraySet();
            a((ArraySet<Fragment>) arraySet);
            int a2 = a(arrayList, arrayList2, i2, i3, (ArraySet<Fragment>) arraySet);
            b((ArraySet<Fragment>) arraySet);
            i4 = a2;
        } else {
            i4 = i6;
        }
        if (i4 != i5 && z2) {
            FragmentTransition.a(this, arrayList, arrayList2, i2, i4, true, this.l);
            a(this.n, true);
        }
        while (i5 < i6) {
            BackStackRecord backStackRecord2 = arrayList3.get(i5);
            if (arrayList4.get(i5).booleanValue() && backStackRecord2.t >= 0) {
                backStackRecord2.t = -1;
            }
            backStackRecord2.i();
            i5++;
        }
        if (z3) {
            H();
        }
    }

    public void a(OnBackStackChangedListener onBackStackChangedListener) {
        if (this.j == null) {
            this.j = new ArrayList<>();
        }
        this.j.add(onBackStackChangedListener);
    }

    /* access modifiers changed from: package-private */
    public FragmentLifecycleCallbacksDispatcher r() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.v = false;
        this.w = false;
        c(2);
    }

    /* access modifiers changed from: package-private */
    public void i() {
        c(3);
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, CancellationSignal cancellationSignal) {
        if (this.k.get(fragment) == null) {
            this.k.put(fragment, new HashSet());
        }
        this.k.get(fragment).add(cancellationSignal);
    }

    /* JADX INFO: finally extract failed */
    private void c(int i2) {
        try {
            this.b = true;
            this.c.a(i2);
            a(i2, false);
            this.b = false;
            c(true);
        } catch (Throwable th) {
            this.b = false;
            throw th;
        }
    }

    public void a(Bundle bundle, String str, Fragment fragment) {
        if (fragment.mFragmentManager == this) {
            bundle.putString(str, fragment.mWho);
            return;
        }
        a((RuntimeException) new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        throw null;
    }

    public Fragment a(Bundle bundle, String str) {
        String string = bundle.getString(str);
        if (string == null) {
            return null;
        }
        Fragment a2 = a(string);
        if (a2 != null) {
            return a2;
        }
        a((RuntimeException) new IllegalStateException("Fragment no longer exists for key " + str + ": unique id " + string));
        throw null;
    }

    static Fragment a(View view) {
        Object tag = view.getTag(R$id.fragment_container_view_tag);
        if (tag instanceof Fragment) {
            return (Fragment) tag;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        boolean z2 = false;
        for (Fragment next : this.c.b()) {
            if (next != null) {
                z2 = w(next);
                continue;
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        String str2 = str + "    ";
        this.c.a(str, fileDescriptor, printWriter, strArr);
        ArrayList<Fragment> arrayList = this.e;
        if (arrayList != null && (size2 = arrayList.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i2 = 0; i2 < size2; i2++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(this.e.get(i2).toString());
            }
        }
        ArrayList<BackStackRecord> arrayList2 = this.d;
        if (arrayList2 != null && (size = arrayList2.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i3 = 0; i3 < size; i3++) {
                BackStackRecord backStackRecord = this.d.get(i3);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(backStackRecord.toString());
                backStackRecord.a(str2, printWriter);
            }
        }
        printWriter.print(str);
        printWriter.println("Back Stack Index: " + this.i.get());
        synchronized (this.f686a) {
            int size3 = this.f686a.size();
            if (size3 > 0) {
                printWriter.print(str);
                printWriter.println("Pending Actions:");
                for (int i4 = 0; i4 < size3; i4++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i4);
                    printWriter.print(": ");
                    printWriter.println(this.f686a.get(i4));
                }
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.o);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.p);
        if (this.q != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.q);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.n);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.v);
        printWriter.print(" mStopped=");
        printWriter.print(this.w);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.x);
        if (this.u) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.u);
        }
    }

    private void b(ArraySet<Fragment> arraySet) {
        int size = arraySet.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment c2 = arraySet.c(i2);
            if (!c2.mAdded) {
                View requireView = c2.requireView();
                c2.mPostponedAlpha = requireView.getAlpha();
                requireView.setAlpha(0.0f);
            }
        }
    }

    private boolean b(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2) {
        synchronized (this.f686a) {
            if (this.f686a.isEmpty()) {
                return false;
            }
            int size = this.f686a.size();
            boolean z2 = false;
            for (int i2 = 0; i2 < size; i2++) {
                z2 |= this.f686a.get(i2).a(arrayList, arrayList2);
            }
            this.f686a.clear();
            this.o.d().removeCallbacks(this.E);
            return z2;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        for (Fragment next : this.c.c()) {
            if (next != null) {
                next.performPictureInPictureModeChanged(z2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(Menu menu) {
        boolean z2 = false;
        if (this.n < 1) {
            return false;
        }
        for (Fragment next : this.c.c()) {
            if (next != null && next.performPrepareOptionsMenu(menu)) {
                z2 = true;
            }
        }
        return z2;
    }

    /* access modifiers changed from: package-private */
    public boolean b(MenuItem menuItem) {
        if (this.n < 1) {
            return false;
        }
        for (Fragment next : this.c.c()) {
            if (next != null && next.performOptionsItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003e, code lost:
        if (r2 != 3) goto L_0x01f4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00fd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.fragment.app.Fragment r13, int r14) {
        /*
            r12 = this;
            androidx.fragment.app.FragmentStore r0 = r12.c
            java.lang.String r1 = r13.mWho
            androidx.fragment.app.FragmentStateManager r0 = r0.e(r1)
            r1 = 1
            if (r0 != 0) goto L_0x0015
            androidx.fragment.app.FragmentStateManager r0 = new androidx.fragment.app.FragmentStateManager
            androidx.fragment.app.FragmentLifecycleCallbacksDispatcher r2 = r12.m
            r0.<init>(r2, r13)
            r0.a((int) r1)
        L_0x0015:
            int r2 = r0.b()
            int r14 = java.lang.Math.min(r14, r2)
            int r2 = r13.mState
            r3 = 0
            java.lang.String r4 = "FragmentManager"
            r5 = -1
            r6 = 2
            r7 = 3
            if (r2 > r14) goto L_0x0102
            if (r2 >= r14) goto L_0x0034
            java.util.concurrent.ConcurrentHashMap<androidx.fragment.app.Fragment, java.util.HashSet<androidx.core.os.CancellationSignal>> r2 = r12.k
            boolean r2 = r2.isEmpty()
            if (r2 != 0) goto L_0x0034
            r12.q(r13)
        L_0x0034:
            int r2 = r13.mState
            if (r2 == r5) goto L_0x0042
            if (r2 == 0) goto L_0x00df
            if (r2 == r1) goto L_0x00e4
            if (r2 == r6) goto L_0x00f6
            if (r2 == r7) goto L_0x00fb
            goto L_0x01f4
        L_0x0042:
            if (r14 <= r5) goto L_0x00df
            boolean r2 = d((int) r7)
            if (r2 == 0) goto L_0x005e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r8 = "moveto ATTACHED: "
            r2.append(r8)
            r2.append(r13)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r4, r2)
        L_0x005e:
            androidx.fragment.app.Fragment r2 = r13.mTarget
            java.lang.String r8 = " that does not belong to this FragmentManager!"
            java.lang.String r9 = " declared target fragment "
            java.lang.String r10 = "Fragment "
            if (r2 == 0) goto L_0x00a6
            java.lang.String r11 = r2.mWho
            androidx.fragment.app.Fragment r11 = r12.a((java.lang.String) r11)
            boolean r2 = r2.equals(r11)
            if (r2 == 0) goto L_0x0086
            androidx.fragment.app.Fragment r2 = r13.mTarget
            int r11 = r2.mState
            if (r11 >= r1) goto L_0x007d
            r12.a((androidx.fragment.app.Fragment) r2, (int) r1)
        L_0x007d:
            androidx.fragment.app.Fragment r2 = r13.mTarget
            java.lang.String r2 = r2.mWho
            r13.mTargetWho = r2
            r13.mTarget = r3
            goto L_0x00a6
        L_0x0086:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r10)
            r0.append(r13)
            r0.append(r9)
            androidx.fragment.app.Fragment r13 = r13.mTarget
            r0.append(r13)
            r0.append(r8)
            java.lang.String r13 = r0.toString()
            r14.<init>(r13)
            throw r14
        L_0x00a6:
            java.lang.String r2 = r13.mTargetWho
            if (r2 == 0) goto L_0x00d8
            androidx.fragment.app.Fragment r2 = r12.a((java.lang.String) r2)
            if (r2 == 0) goto L_0x00b8
            int r3 = r2.mState
            if (r3 >= r1) goto L_0x00d8
            r12.a((androidx.fragment.app.Fragment) r2, (int) r1)
            goto L_0x00d8
        L_0x00b8:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r10)
            r0.append(r13)
            r0.append(r9)
            java.lang.String r13 = r13.mTargetWho
            r0.append(r13)
            r0.append(r8)
            java.lang.String r13 = r0.toString()
            r14.<init>(r13)
            throw r14
        L_0x00d8:
            androidx.fragment.app.FragmentHostCallback<?> r2 = r12.o
            androidx.fragment.app.Fragment r3 = r12.q
            r0.a(r2, r12, r3)
        L_0x00df:
            if (r14 <= 0) goto L_0x00e4
            r0.c()
        L_0x00e4:
            if (r14 <= r5) goto L_0x00e9
            r0.d()
        L_0x00e9:
            if (r14 <= r1) goto L_0x00f6
            androidx.fragment.app.FragmentContainer r1 = r12.p
            r0.a((androidx.fragment.app.FragmentContainer) r1)
            r0.a()
            r0.g()
        L_0x00f6:
            if (r14 <= r6) goto L_0x00fb
            r0.l()
        L_0x00fb:
            if (r14 <= r7) goto L_0x01f4
            r0.h()
            goto L_0x01f4
        L_0x0102:
            if (r2 <= r14) goto L_0x01f4
            if (r2 == 0) goto L_0x01ed
            r8 = 0
            if (r2 == r1) goto L_0x01ab
            if (r2 == r6) goto L_0x011c
            if (r2 == r7) goto L_0x0117
            r9 = 4
            if (r2 == r9) goto L_0x0112
            goto L_0x01f4
        L_0x0112:
            if (r14 >= r9) goto L_0x0117
            r0.f()
        L_0x0117:
            if (r14 >= r7) goto L_0x011c
            r0.m()
        L_0x011c:
            if (r14 >= r6) goto L_0x01ab
            boolean r2 = d((int) r7)
            if (r2 == 0) goto L_0x0138
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r6 = "movefrom ACTIVITY_CREATED: "
            r2.append(r6)
            r2.append(r13)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r4, r2)
        L_0x0138:
            android.view.View r2 = r13.mView
            if (r2 == 0) goto L_0x014b
            androidx.fragment.app.FragmentHostCallback<?> r2 = r12.o
            boolean r2 = r2.b(r13)
            if (r2 == 0) goto L_0x014b
            android.util.SparseArray<android.os.Parcelable> r2 = r13.mSavedViewState
            if (r2 != 0) goto L_0x014b
            r0.k()
        L_0x014b:
            android.view.View r2 = r13.mView
            if (r2 == 0) goto L_0x019c
            android.view.ViewGroup r6 = r13.mContainer
            if (r6 == 0) goto L_0x019c
            r6.endViewTransition(r2)
            android.view.View r2 = r13.mView
            r2.clearAnimation()
            boolean r2 = r13.isRemovingParent()
            if (r2 != 0) goto L_0x019c
            int r2 = r12.n
            r6 = 0
            if (r2 <= r5) goto L_0x0184
            boolean r2 = r12.x
            if (r2 != 0) goto L_0x0184
            android.view.View r2 = r13.mView
            int r2 = r2.getVisibility()
            if (r2 != 0) goto L_0x0184
            float r2 = r13.mPostponedAlpha
            int r2 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r2 < 0) goto L_0x0184
            androidx.fragment.app.FragmentHostCallback<?> r2 = r12.o
            android.content.Context r2 = r2.c()
            androidx.fragment.app.FragmentContainer r3 = r12.p
            androidx.fragment.app.FragmentAnim$AnimationOrAnimator r3 = androidx.fragment.app.FragmentAnim.a(r2, r3, r13, r8)
        L_0x0184:
            r13.mPostponedAlpha = r6
            android.view.ViewGroup r2 = r13.mContainer
            android.view.View r5 = r13.mView
            if (r3 == 0) goto L_0x0194
            r13.setStateAfterAnimating(r14)
            androidx.fragment.app.FragmentTransition$Callback r6 = r12.l
            androidx.fragment.app.FragmentAnim.a(r13, r3, r6)
        L_0x0194:
            r2.removeView(r5)
            android.view.ViewGroup r3 = r13.mContainer
            if (r2 == r3) goto L_0x019c
            return
        L_0x019c:
            java.util.concurrent.ConcurrentHashMap<androidx.fragment.app.Fragment, java.util.HashSet<androidx.core.os.CancellationSignal>> r2 = r12.k
            java.lang.Object r2 = r2.get(r13)
            if (r2 != 0) goto L_0x01a8
            r12.s(r13)
            goto L_0x01ab
        L_0x01a8:
            r13.setStateAfterAnimating(r14)
        L_0x01ab:
            if (r14 >= r1) goto L_0x01ed
            boolean r2 = r13.mRemoving
            if (r2 == 0) goto L_0x01b8
            boolean r2 = r13.isInBackStack()
            if (r2 != 0) goto L_0x01b8
            r8 = 1
        L_0x01b8:
            if (r8 != 0) goto L_0x01d6
            androidx.fragment.app.FragmentManagerViewModel r2 = r12.D
            boolean r2 = r2.f(r13)
            if (r2 == 0) goto L_0x01c3
            goto L_0x01d6
        L_0x01c3:
            java.lang.String r2 = r13.mTargetWho
            if (r2 == 0) goto L_0x01d9
            androidx.fragment.app.Fragment r2 = r12.a((java.lang.String) r2)
            if (r2 == 0) goto L_0x01d9
            boolean r3 = r2.getRetainInstance()
            if (r3 == 0) goto L_0x01d9
            r13.mTarget = r2
            goto L_0x01d9
        L_0x01d6:
            r12.a((androidx.fragment.app.FragmentStateManager) r0)
        L_0x01d9:
            java.util.concurrent.ConcurrentHashMap<androidx.fragment.app.Fragment, java.util.HashSet<androidx.core.os.CancellationSignal>> r2 = r12.k
            java.lang.Object r2 = r2.get(r13)
            if (r2 == 0) goto L_0x01e6
            r13.setStateAfterAnimating(r14)
            r14 = 1
            goto L_0x01ed
        L_0x01e6:
            androidx.fragment.app.FragmentHostCallback<?> r1 = r12.o
            androidx.fragment.app.FragmentManagerViewModel r2 = r12.D
            r0.a(r1, r2)
        L_0x01ed:
            if (r14 >= 0) goto L_0x01f4
            androidx.fragment.app.FragmentManagerViewModel r1 = r12.D
            r0.a((androidx.fragment.app.FragmentManagerViewModel) r1)
        L_0x01f4:
            int r0 = r13.mState
            if (r0 == r14) goto L_0x0226
            boolean r0 = d((int) r7)
            if (r0 == 0) goto L_0x0224
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveToState: Fragment state for "
            r0.append(r1)
            r0.append(r13)
            java.lang.String r1 = " not updated inline; expected state "
            r0.append(r1)
            r0.append(r14)
            java.lang.String r1 = " found "
            r0.append(r1)
            int r1 = r13.mState
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r4, r0)
        L_0x0224:
            r13.mState = r14
        L_0x0226:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentManager.a(androidx.fragment.app.Fragment, int):void");
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, boolean z2) {
        ViewGroup v2 = v(fragment);
        if (v2 != null && (v2 instanceof FragmentContainerView)) {
            ((FragmentContainerView) v2).setDrawDisappearingViewsLast(!z2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2) {
        FragmentHostCallback<?> fragmentHostCallback;
        if (this.o == null && i2 != -1) {
            throw new IllegalStateException("No activity");
        } else if (z2 || i2 != this.n) {
            this.n = i2;
            for (Fragment i3 : this.c.c()) {
                i(i3);
            }
            for (Fragment next : this.c.b()) {
                if (next != null && !next.mIsNewlyAdded) {
                    i(next);
                }
            }
            I();
            if (this.u && (fragmentHostCallback = this.o) != null && this.n == 4) {
                fragmentHostCallback.g();
                this.u = false;
            }
        }
    }

    private void a(FragmentStateManager fragmentStateManager) {
        Fragment e2 = fragmentStateManager.e();
        if (this.c.a(e2.mWho)) {
            if (d(2)) {
                Log.v("FragmentManager", "Removed fragment from active set " + e2);
            }
            this.c.b(fragmentStateManager);
            m(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
        if (d(2)) {
            Log.v("FragmentManager", "add: " + fragment);
        }
        h(fragment);
        if (!fragment.mDetached) {
            this.c.a(fragment);
            fragment.mRemoving = false;
            if (fragment.mView == null) {
                fragment.mHiddenChanged = false;
            }
            if (w(fragment)) {
                this.u = true;
            }
        }
    }

    public Fragment a(int i2) {
        return this.c.b(i2);
    }

    /* access modifiers changed from: package-private */
    public Fragment a(String str) {
        return this.c.b(str);
    }

    /* access modifiers changed from: package-private */
    public void a(OpGenerator opGenerator, boolean z2) {
        if (!z2) {
            if (this.o != null) {
                C();
            } else if (this.x) {
                throw new IllegalStateException("FragmentManager has been destroyed");
            } else {
                throw new IllegalStateException("FragmentManager has not been attached to a host.");
            }
        }
        synchronized (this.f686a) {
            if (this.o != null) {
                this.f686a.add(opGenerator);
                B();
            } else if (!z2) {
                throw new IllegalStateException("Activity has been destroyed");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.i.getAndIncrement();
    }

    private void a(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2) {
        int indexOf;
        int indexOf2;
        ArrayList<StartEnterTransitionListener> arrayList3 = this.C;
        int size = arrayList3 == null ? 0 : arrayList3.size();
        int i2 = 0;
        while (i2 < size) {
            StartEnterTransitionListener startEnterTransitionListener = this.C.get(i2);
            if (arrayList != null && !startEnterTransitionListener.f691a && (indexOf2 = arrayList.indexOf(startEnterTransitionListener.b)) != -1 && arrayList2 != null && arrayList2.get(indexOf2).booleanValue()) {
                this.C.remove(i2);
                i2--;
                size--;
                startEnterTransitionListener.c();
            } else if (startEnterTransitionListener.e() || (arrayList != null && startEnterTransitionListener.b.a(arrayList, 0, arrayList.size()))) {
                this.C.remove(i2);
                i2--;
                size--;
                if (arrayList == null || startEnterTransitionListener.f691a || (indexOf = arrayList.indexOf(startEnterTransitionListener.b)) == -1 || arrayList2 == null || !arrayList2.get(indexOf).booleanValue()) {
                    startEnterTransitionListener.d();
                } else {
                    startEnterTransitionListener.c();
                }
            }
            i2++;
        }
    }

    private int a(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3, ArraySet<Fragment> arraySet) {
        int i4 = i3;
        for (int i5 = i3 - 1; i5 >= i2; i5--) {
            BackStackRecord backStackRecord = arrayList.get(i5);
            boolean booleanValue = arrayList2.get(i5).booleanValue();
            if (backStackRecord.h() && !backStackRecord.a(arrayList, i5 + 1, i3)) {
                if (this.C == null) {
                    this.C = new ArrayList<>();
                }
                StartEnterTransitionListener startEnterTransitionListener = new StartEnterTransitionListener(backStackRecord, booleanValue);
                this.C.add(startEnterTransitionListener);
                backStackRecord.a((Fragment.OnStartEnterTransitionListener) startEnterTransitionListener);
                if (booleanValue) {
                    backStackRecord.f();
                } else {
                    backStackRecord.c(false);
                }
                i4--;
                if (i5 != i4) {
                    arrayList.remove(i5);
                    arrayList.add(i4, backStackRecord);
                }
                a(arraySet);
            }
        }
        return i4;
    }

    /* access modifiers changed from: package-private */
    public void a(BackStackRecord backStackRecord, boolean z2, boolean z3, boolean z4) {
        if (z2) {
            backStackRecord.c(z4);
        } else {
            backStackRecord.f();
        }
        ArrayList arrayList = new ArrayList(1);
        ArrayList arrayList2 = new ArrayList(1);
        arrayList.add(backStackRecord);
        arrayList2.add(Boolean.valueOf(z2));
        if (z3) {
            FragmentTransition.a(this, arrayList, arrayList2, 0, 1, true, this.l);
        }
        if (z4) {
            a(this.n, true);
        }
        for (Fragment next : this.c.b()) {
            if (next != null && next.mView != null && next.mIsNewlyAdded && backStackRecord.c(next.mContainerId)) {
                float f2 = next.mPostponedAlpha;
                if (f2 > 0.0f) {
                    next.mView.setAlpha(f2);
                }
                if (z4) {
                    next.mPostponedAlpha = 0.0f;
                } else {
                    next.mPostponedAlpha = -1.0f;
                    next.mIsNewlyAdded = false;
                }
            }
        }
    }

    private static void a(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        while (i2 < i3) {
            BackStackRecord backStackRecord = arrayList.get(i2);
            boolean z2 = true;
            if (arrayList2.get(i2).booleanValue()) {
                backStackRecord.b(-1);
                if (i2 != i3 - 1) {
                    z2 = false;
                }
                backStackRecord.c(z2);
            } else {
                backStackRecord.b(1);
                backStackRecord.f();
            }
            i2++;
        }
    }

    private void a(ArraySet<Fragment> arraySet) {
        int i2 = this.n;
        if (i2 >= 1) {
            int min = Math.min(i2, 3);
            for (Fragment next : this.c.c()) {
                if (next.mState < min) {
                    a(next, min);
                    if (next.mView != null && !next.mHidden && next.mIsNewlyAdded) {
                        arraySet.add(next);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(BackStackRecord backStackRecord) {
        if (this.d == null) {
            this.d = new ArrayList<>();
        }
        this.d.add(backStackRecord);
    }

    /* access modifiers changed from: package-private */
    public boolean a(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2, String str, int i2, int i3) {
        int i4;
        ArrayList<BackStackRecord> arrayList3 = this.d;
        if (arrayList3 == null) {
            return false;
        }
        if (str == null && i2 < 0 && (i3 & 1) == 0) {
            int size = arrayList3.size() - 1;
            if (size < 0) {
                return false;
            }
            arrayList.add(this.d.remove(size));
            arrayList2.add(true);
        } else {
            if (str != null || i2 >= 0) {
                i4 = this.d.size() - 1;
                while (i4 >= 0) {
                    BackStackRecord backStackRecord = this.d.get(i4);
                    if ((str != null && str.equals(backStackRecord.g())) || (i2 >= 0 && i2 == backStackRecord.t)) {
                        break;
                    }
                    i4--;
                }
                if (i4 < 0) {
                    return false;
                }
                if ((i3 & 1) != 0) {
                    while (true) {
                        i4--;
                        if (i4 < 0) {
                            break;
                        }
                        BackStackRecord backStackRecord2 = this.d.get(i4);
                        if ((str == null || !str.equals(backStackRecord2.g())) && (i2 < 0 || i2 != backStackRecord2.t)) {
                            break;
                        }
                    }
                }
            } else {
                i4 = -1;
            }
            if (i4 == this.d.size() - 1) {
                return false;
            }
            for (int size2 = this.d.size() - 1; size2 > i4; size2--) {
                arrayList.add(this.d.remove(size2));
                arrayList2.add(true);
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(Parcelable parcelable) {
        FragmentStateManager fragmentStateManager;
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.f692a != null) {
                this.c.d();
                Iterator<FragmentState> it2 = fragmentManagerState.f692a.iterator();
                while (it2.hasNext()) {
                    FragmentState next = it2.next();
                    if (next != null) {
                        Fragment b2 = this.D.b(next.b);
                        if (b2 != null) {
                            if (d(2)) {
                                Log.v("FragmentManager", "restoreSaveState: re-attaching retained " + b2);
                            }
                            fragmentStateManager = new FragmentStateManager(this.m, b2, next);
                        } else {
                            fragmentStateManager = new FragmentStateManager(this.m, this.o.c().getClassLoader(), o(), next);
                        }
                        Fragment e2 = fragmentStateManager.e();
                        e2.mFragmentManager = this;
                        if (d(2)) {
                            Log.v("FragmentManager", "restoreSaveState: active (" + e2.mWho + "): " + e2);
                        }
                        fragmentStateManager.a(this.o.c().getClassLoader());
                        this.c.a(fragmentStateManager);
                        fragmentStateManager.a(this.n);
                    }
                }
                for (Fragment next2 : this.D.c()) {
                    if (!this.c.a(next2.mWho)) {
                        if (d(2)) {
                            Log.v("FragmentManager", "Discarding retained Fragment " + next2 + " that was not found in the set of active Fragments " + fragmentManagerState.f692a);
                        }
                        a(next2, 1);
                        next2.mRemoving = true;
                        a(next2, -1);
                    }
                }
                this.c.a((List<String>) fragmentManagerState.b);
                BackStackState[] backStackStateArr = fragmentManagerState.c;
                if (backStackStateArr != null) {
                    this.d = new ArrayList<>(backStackStateArr.length);
                    int i2 = 0;
                    while (true) {
                        BackStackState[] backStackStateArr2 = fragmentManagerState.c;
                        if (i2 >= backStackStateArr2.length) {
                            break;
                        }
                        BackStackRecord a2 = backStackStateArr2[i2].a(this);
                        if (d(2)) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i2 + " (index " + a2.t + "): " + a2);
                            PrintWriter printWriter = new PrintWriter(new LogWriter("FragmentManager"));
                            a2.a("  ", printWriter, false);
                            printWriter.close();
                        }
                        this.d.add(a2);
                        i2++;
                    }
                } else {
                    this.d = null;
                }
                this.i.set(fragmentManagerState.d);
                String str = fragmentManagerState.e;
                if (str != null) {
                    this.r = a(str);
                    t(this.r);
                }
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: androidx.activity.OnBackPressedDispatcherOwner} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: androidx.fragment.app.Fragment} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: androidx.fragment.app.Fragment} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: androidx.fragment.app.Fragment} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.fragment.app.FragmentHostCallback<?> r3, androidx.fragment.app.FragmentContainer r4, androidx.fragment.app.Fragment r5) {
        /*
            r2 = this;
            androidx.fragment.app.FragmentHostCallback<?> r0 = r2.o
            if (r0 != 0) goto L_0x004d
            r2.o = r3
            r2.p = r4
            r2.q = r5
            androidx.fragment.app.Fragment r4 = r2.q
            if (r4 == 0) goto L_0x0011
            r2.J()
        L_0x0011:
            boolean r4 = r3 instanceof androidx.activity.OnBackPressedDispatcherOwner
            if (r4 == 0) goto L_0x0028
            r4 = r3
            androidx.activity.OnBackPressedDispatcherOwner r4 = (androidx.activity.OnBackPressedDispatcherOwner) r4
            androidx.activity.OnBackPressedDispatcher r0 = r4.getOnBackPressedDispatcher()
            r2.g = r0
            if (r5 == 0) goto L_0x0021
            r4 = r5
        L_0x0021:
            androidx.activity.OnBackPressedDispatcher r0 = r2.g
            androidx.activity.OnBackPressedCallback r1 = r2.h
            r0.a(r4, r1)
        L_0x0028:
            if (r5 == 0) goto L_0x0033
            androidx.fragment.app.FragmentManager r3 = r5.mFragmentManager
            androidx.fragment.app.FragmentManagerViewModel r3 = r3.u(r5)
            r2.D = r3
            goto L_0x004c
        L_0x0033:
            boolean r4 = r3 instanceof androidx.lifecycle.ViewModelStoreOwner
            if (r4 == 0) goto L_0x0044
            androidx.lifecycle.ViewModelStoreOwner r3 = (androidx.lifecycle.ViewModelStoreOwner) r3
            androidx.lifecycle.ViewModelStore r3 = r3.getViewModelStore()
            androidx.fragment.app.FragmentManagerViewModel r3 = androidx.fragment.app.FragmentManagerViewModel.a((androidx.lifecycle.ViewModelStore) r3)
            r2.D = r3
            goto L_0x004c
        L_0x0044:
            androidx.fragment.app.FragmentManagerViewModel r3 = new androidx.fragment.app.FragmentManagerViewModel
            r4 = 0
            r3.<init>(r4)
            r2.D = r3
        L_0x004c:
            return
        L_0x004d:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r4 = "Already attached"
            r3.<init>(r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentManager.a(androidx.fragment.app.FragmentHostCallback, androidx.fragment.app.FragmentContainer, androidx.fragment.app.Fragment):void");
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        for (Fragment next : this.c.c()) {
            if (next != null) {
                next.performMultiWindowModeChanged(z2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Configuration configuration) {
        for (Fragment next : this.c.c()) {
            if (next != null) {
                next.performConfigurationChanged(configuration);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(Menu menu, MenuInflater menuInflater) {
        if (this.n < 1) {
            return false;
        }
        ArrayList<Fragment> arrayList = null;
        boolean z2 = false;
        for (Fragment next : this.c.c()) {
            if (next != null && next.performCreateOptionsMenu(menu, menuInflater)) {
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                }
                arrayList.add(next);
                z2 = true;
            }
        }
        if (this.e != null) {
            for (int i2 = 0; i2 < this.e.size(); i2++) {
                Fragment fragment = this.e.get(i2);
                if (arrayList == null || !arrayList.contains(fragment)) {
                    fragment.onDestroyOptionsMenu();
                }
            }
        }
        this.e = arrayList;
        return z2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(MenuItem menuItem) {
        if (this.n < 1) {
            return false;
        }
        for (Fragment next : this.c.c()) {
            if (next != null && next.performContextItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(Menu menu) {
        if (this.n >= 1) {
            for (Fragment next : this.c.c()) {
                if (next != null) {
                    next.performOptionsMenuClosed(menu);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, Lifecycle.State state) {
        if (!fragment.equals(a(fragment.mWho)) || !(fragment.mHost == null || fragment.mFragmentManager == this)) {
            throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
        }
        fragment.mMaxState = state;
    }
}
