package androidx.fragment.app;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.FragmentManager;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

class FragmentLifecycleCallbacksDispatcher {

    /* renamed from: a  reason: collision with root package name */
    private final CopyOnWriteArrayList<FragmentLifecycleCallbacksHolder> f684a = new CopyOnWriteArrayList<>();
    private final FragmentManager b;

    private static final class FragmentLifecycleCallbacksHolder {

        /* renamed from: a  reason: collision with root package name */
        final FragmentManager.FragmentLifecycleCallbacks f685a;
        final boolean b;
    }

    FragmentLifecycleCallbacksDispatcher(FragmentManager fragmentManager) {
        this.b = fragmentManager;
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, Context context, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().a(fragment, context, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.a(this.b, fragment, context);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, Context context, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().b(fragment, context, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.b(this.b, fragment, context);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment, Bundle bundle, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().c(fragment, bundle, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.c(this.b, fragment, bundle);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Fragment fragment, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().d(fragment, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.d(this.b, fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e(Fragment fragment, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().e(fragment, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.e(this.b, fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f(Fragment fragment, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().f(fragment, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.f(this.b, fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void g(Fragment fragment, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().g(fragment, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.g(this.b, fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, Bundle bundle, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().a(fragment, bundle, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.a(this.b, fragment, bundle);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, Bundle bundle, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().b(fragment, bundle, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.b(this.b, fragment, bundle);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().c(fragment, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.c(this.b, fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Fragment fragment, Bundle bundle, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().d(fragment, bundle, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.d(this.b, fragment, bundle);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, View view, Bundle bundle, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().a(fragment, view, bundle, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.a(this.b, fragment, view, bundle);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().b(fragment, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.b(this.b, fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, boolean z) {
        Fragment s = this.b.s();
        if (s != null) {
            s.getParentFragmentManager().r().a(fragment, true);
        }
        Iterator<FragmentLifecycleCallbacksHolder> it2 = this.f684a.iterator();
        while (it2.hasNext()) {
            FragmentLifecycleCallbacksHolder next = it2.next();
            if (!z || next.b) {
                next.f685a.a(this.b, fragment);
            }
        }
    }
}
