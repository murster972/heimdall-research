package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import java.util.ArrayList;

@SuppressLint({"BanParcelableUsage"})
final class BackStackState implements Parcelable {
    public static final Parcelable.Creator<BackStackState> CREATOR = new Parcelable.Creator<BackStackState>() {
        public BackStackState createFromParcel(Parcel parcel) {
            return new BackStackState(parcel);
        }

        public BackStackState[] newArray(int i) {
            return new BackStackState[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final int[] f663a;
    final ArrayList<String> b;
    final int[] c;
    final int[] d;
    final int e;
    final String f;
    final int g;
    final int h;
    final CharSequence i;
    final int j;
    final CharSequence k;
    final ArrayList<String> l;
    final ArrayList<String> m;
    final boolean n;

    public BackStackState(BackStackRecord backStackRecord) {
        int size = backStackRecord.f699a.size();
        this.f663a = new int[(size * 5)];
        if (backStackRecord.g) {
            this.b = new ArrayList<>(size);
            this.c = new int[size];
            this.d = new int[size];
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                FragmentTransaction.Op op = backStackRecord.f699a.get(i2);
                int i4 = i3 + 1;
                this.f663a[i3] = op.f700a;
                ArrayList<String> arrayList = this.b;
                Fragment fragment = op.b;
                arrayList.add(fragment != null ? fragment.mWho : null);
                int[] iArr = this.f663a;
                int i5 = i4 + 1;
                iArr[i4] = op.c;
                int i6 = i5 + 1;
                iArr[i5] = op.d;
                int i7 = i6 + 1;
                iArr[i6] = op.e;
                iArr[i7] = op.f;
                this.c[i2] = op.g.ordinal();
                this.d[i2] = op.h.ordinal();
                i2++;
                i3 = i7 + 1;
            }
            this.e = backStackRecord.f;
            this.f = backStackRecord.i;
            this.g = backStackRecord.t;
            this.h = backStackRecord.j;
            this.i = backStackRecord.k;
            this.j = backStackRecord.l;
            this.k = backStackRecord.m;
            this.l = backStackRecord.n;
            this.m = backStackRecord.o;
            this.n = backStackRecord.p;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    public BackStackRecord a(FragmentManager fragmentManager) {
        BackStackRecord backStackRecord = new BackStackRecord(fragmentManager);
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.f663a.length) {
            FragmentTransaction.Op op = new FragmentTransaction.Op();
            int i4 = i2 + 1;
            op.f700a = this.f663a[i2];
            if (FragmentManager.d(2)) {
                Log.v("FragmentManager", "Instantiate " + backStackRecord + " op #" + i3 + " base fragment #" + this.f663a[i4]);
            }
            String str = this.b.get(i3);
            if (str != null) {
                op.b = fragmentManager.a(str);
            } else {
                op.b = null;
            }
            op.g = Lifecycle.State.values()[this.c[i3]];
            op.h = Lifecycle.State.values()[this.d[i3]];
            int[] iArr = this.f663a;
            int i5 = i4 + 1;
            op.c = iArr[i4];
            int i6 = i5 + 1;
            op.d = iArr[i5];
            int i7 = i6 + 1;
            op.e = iArr[i6];
            op.f = iArr[i7];
            backStackRecord.b = op.c;
            backStackRecord.c = op.d;
            backStackRecord.d = op.e;
            backStackRecord.e = op.f;
            backStackRecord.a(op);
            i3++;
            i2 = i7 + 1;
        }
        backStackRecord.f = this.e;
        backStackRecord.i = this.f;
        backStackRecord.t = this.g;
        backStackRecord.g = true;
        backStackRecord.j = this.h;
        backStackRecord.k = this.i;
        backStackRecord.l = this.j;
        backStackRecord.m = this.k;
        backStackRecord.n = this.l;
        backStackRecord.o = this.m;
        backStackRecord.p = this.n;
        backStackRecord.b(1);
        return backStackRecord;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.f663a);
        parcel.writeStringList(this.b);
        parcel.writeIntArray(this.c);
        parcel.writeIntArray(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
        parcel.writeInt(this.j);
        TextUtils.writeToParcel(this.k, parcel, 0);
        parcel.writeStringList(this.l);
        parcel.writeStringList(this.m);
        parcel.writeInt(this.n ? 1 : 0);
    }

    public BackStackState(Parcel parcel) {
        this.f663a = parcel.createIntArray();
        this.b = parcel.createStringArrayList();
        this.c = parcel.createIntArray();
        this.d = parcel.createIntArray();
        this.e = parcel.readInt();
        this.f = parcel.readString();
        this.g = parcel.readInt();
        this.h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.j = parcel.readInt();
        this.k = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.l = parcel.createStringArrayList();
        this.m = parcel.createStringArrayList();
        this.n = parcel.readInt() != 0;
    }
}
