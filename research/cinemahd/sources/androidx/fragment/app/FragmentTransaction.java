package androidx.fragment.app;

import android.view.ViewGroup;
import androidx.lifecycle.Lifecycle;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public abstract class FragmentTransaction {

    /* renamed from: a  reason: collision with root package name */
    ArrayList<Op> f699a = new ArrayList<>();
    int b;
    int c;
    int d;
    int e;
    int f;
    boolean g;
    boolean h = true;
    String i;
    int j;
    CharSequence k;
    int l;
    CharSequence m;
    ArrayList<String> n;
    ArrayList<String> o;
    boolean p = false;
    ArrayList<Runnable> q;

    static final class Op {

        /* renamed from: a  reason: collision with root package name */
        int f700a;
        Fragment b;
        int c;
        int d;
        int e;
        int f;
        Lifecycle.State g;
        Lifecycle.State h;

        Op() {
        }

        Op(int i, Fragment fragment) {
            this.f700a = i;
            this.b = fragment;
            Lifecycle.State state = Lifecycle.State.RESUMED;
            this.g = state;
            this.h = state;
        }

        Op(int i, Fragment fragment, Lifecycle.State state) {
            this.f700a = i;
            this.b = fragment;
            this.g = fragment.mMaxState;
            this.h = state;
        }
    }

    @Deprecated
    public FragmentTransaction() {
    }

    public abstract int a();

    /* access modifiers changed from: package-private */
    public void a(Op op) {
        this.f699a.add(op);
        op.c = this.b;
        op.d = this.c;
        op.e = this.d;
        op.f = this.e;
    }

    public abstract int b();

    public FragmentTransaction b(int i2, Fragment fragment) {
        return b(i2, fragment, (String) null);
    }

    public abstract void c();

    public abstract void d();

    public FragmentTransaction e() {
        if (!this.g) {
            this.h = false;
            return this;
        }
        throw new IllegalStateException("This transaction is already being added to the back stack");
    }

    public FragmentTransaction b(int i2, Fragment fragment, String str) {
        if (i2 != 0) {
            a(i2, fragment, str, 2);
            return this;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }

    FragmentTransaction(FragmentFactory fragmentFactory, ClassLoader classLoader) {
    }

    public FragmentTransaction a(Fragment fragment, String str) {
        a(0, fragment, str, 1);
        return this;
    }

    public FragmentTransaction a(int i2, Fragment fragment) {
        a(i2, fragment, (String) null, 1);
        return this;
    }

    public FragmentTransaction a(int i2, Fragment fragment, String str) {
        a(i2, fragment, str, 1);
        return this;
    }

    /* access modifiers changed from: package-private */
    public FragmentTransaction a(ViewGroup viewGroup, Fragment fragment, String str) {
        fragment.mContainer = viewGroup;
        return a(viewGroup.getId(), fragment, str);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, Fragment fragment, String str, int i3) {
        Class<?> cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from instance state.");
        }
        if (str != null) {
            String str2 = fragment.mTag;
            if (str2 == null || str.equals(str2)) {
                fragment.mTag = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.mTag + " now " + str);
            }
        }
        if (i2 != 0) {
            if (i2 != -1) {
                int i4 = fragment.mFragmentId;
                if (i4 == 0 || i4 == i2) {
                    fragment.mFragmentId = i2;
                    fragment.mContainerId = i2;
                } else {
                    throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.mFragmentId + " now " + i2);
                }
            } else {
                throw new IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            }
        }
        a(new Op(i3, fragment));
    }

    public FragmentTransaction a(Fragment fragment) {
        a(new Op(3, fragment));
        return this;
    }

    public FragmentTransaction a(Fragment fragment, Lifecycle.State state) {
        a(new Op(10, fragment, state));
        return this;
    }

    public FragmentTransaction a(int i2, int i3, int i4, int i5) {
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        return this;
    }

    public FragmentTransaction a(int i2) {
        this.f = i2;
        return this;
    }

    public FragmentTransaction a(String str) {
        if (this.h) {
            this.g = true;
            this.i = str;
            return this;
        }
        throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }

    public FragmentTransaction a(boolean z) {
        this.p = z;
        return this;
    }
}
