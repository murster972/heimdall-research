package androidx.fragment.app;

import android.util.Log;
import androidx.core.util.LogWriter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import java.io.PrintWriter;
import java.util.ArrayList;
import okhttp3.internal.cache.DiskLruCache;

final class BackStackRecord extends FragmentTransaction implements FragmentManager.BackStackEntry, FragmentManager.OpGenerator {
    final FragmentManager r;
    boolean s;
    int t;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    BackStackRecord(androidx.fragment.app.FragmentManager r3) {
        /*
            r2 = this;
            androidx.fragment.app.FragmentFactory r0 = r3.o()
            androidx.fragment.app.FragmentHostCallback<?> r1 = r3.o
            if (r1 == 0) goto L_0x0011
            android.content.Context r1 = r1.c()
            java.lang.ClassLoader r1 = r1.getClassLoader()
            goto L_0x0012
        L_0x0011:
            r1 = 0
        L_0x0012:
            r2.<init>(r0, r1)
            r0 = -1
            r2.t = r0
            r2.r = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.BackStackRecord.<init>(androidx.fragment.app.FragmentManager):void");
    }

    public void a(String str, PrintWriter printWriter) {
        a(str, printWriter, true);
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        if (this.g) {
            if (FragmentManager.d(2)) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i);
            }
            int size = this.f699a.size();
            for (int i2 = 0; i2 < size; i2++) {
                FragmentTransaction.Op op = this.f699a.get(i2);
                Fragment fragment = op.b;
                if (fragment != null) {
                    fragment.mBackStackNesting += i;
                    if (FragmentManager.d(2)) {
                        Log.v("FragmentManager", "Bump nesting of " + op.b + " to " + op.b.mBackStackNesting);
                    }
                }
            }
        }
    }

    public void c() {
        e();
        this.r.b((FragmentManager.OpGenerator) this, false);
    }

    public void d() {
        e();
        this.r.b((FragmentManager.OpGenerator) this, true);
    }

    /* access modifiers changed from: package-private */
    public void f() {
        int size = this.f699a.size();
        for (int i = 0; i < size; i++) {
            FragmentTransaction.Op op = this.f699a.get(i);
            Fragment fragment = op.b;
            if (fragment != null) {
                fragment.setNextTransition(this.f);
            }
            switch (op.f700a) {
                case 1:
                    fragment.setNextAnim(op.c);
                    this.r.a(fragment, false);
                    this.r.a(fragment);
                    break;
                case 3:
                    fragment.setNextAnim(op.d);
                    this.r.l(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(op.d);
                    this.r.f(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(op.c);
                    this.r.a(fragment, false);
                    this.r.p(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(op.d);
                    this.r.d(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(op.c);
                    this.r.a(fragment, false);
                    this.r.c(fragment);
                    break;
                case 8:
                    this.r.o(fragment);
                    break;
                case 9:
                    this.r.o((Fragment) null);
                    break;
                case 10:
                    this.r.a(fragment, op.h);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + op.f700a);
            }
            if (!(this.p || op.f700a == 1 || fragment == null)) {
                this.r.i(fragment);
            }
        }
        if (!this.p) {
            FragmentManager fragmentManager = this.r;
            fragmentManager.a(fragmentManager.n, true);
        }
    }

    public String g() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        for (int i = 0; i < this.f699a.size(); i++) {
            if (b(this.f699a.get(i))) {
                return true;
            }
        }
        return false;
    }

    public void i() {
        if (this.q != null) {
            for (int i = 0; i < this.q.size(); i++) {
                this.q.get(i).run();
            }
            this.q = null;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.t >= 0) {
            sb.append(" #");
            sb.append(this.t);
        }
        if (this.i != null) {
            sb.append(" ");
            sb.append(this.i);
        }
        sb.append("}");
        return sb.toString();
    }

    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.i);
            printWriter.print(" mIndex=");
            printWriter.print(this.t);
            printWriter.print(" mCommitted=");
            printWriter.println(this.s);
            if (this.f != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.f));
            }
            if (!(this.b == 0 && this.c == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.b));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.c));
            }
            if (!(this.d == 0 && this.e == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.d));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.e));
            }
            if (!(this.j == 0 && this.k == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.j));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.k);
            }
            if (!(this.l == 0 && this.m == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.l));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.m);
            }
        }
        if (!this.f699a.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            int size = this.f699a.size();
            for (int i = 0; i < size; i++) {
                FragmentTransaction.Op op = this.f699a.get(i);
                switch (op.f700a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = DiskLruCache.REMOVE;
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    case 10:
                        str2 = "OP_SET_MAX_LIFECYCLE";
                        break;
                    default:
                        str2 = "cmd=" + op.f700a;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(op.b);
                if (z) {
                    if (!(op.c == 0 && op.d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(op.c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(op.d));
                    }
                    if (op.e != 0 || op.f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(op.e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(op.f));
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i) {
        int size = this.f699a.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment fragment = this.f699a.get(i2).b;
            int i3 = fragment != null ? fragment.mContainerId : 0;
            if (i3 != 0 && i3 == i) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z) {
        for (int size = this.f699a.size() - 1; size >= 0; size--) {
            FragmentTransaction.Op op = this.f699a.get(size);
            Fragment fragment = op.b;
            if (fragment != null) {
                fragment.setNextTransition(FragmentManager.e(this.f));
            }
            switch (op.f700a) {
                case 1:
                    fragment.setNextAnim(op.f);
                    this.r.a(fragment, true);
                    this.r.l(fragment);
                    break;
                case 3:
                    fragment.setNextAnim(op.e);
                    this.r.a(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(op.e);
                    this.r.p(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(op.f);
                    this.r.a(fragment, true);
                    this.r.f(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(op.e);
                    this.r.c(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(op.f);
                    this.r.a(fragment, true);
                    this.r.d(fragment);
                    break;
                case 8:
                    this.r.o((Fragment) null);
                    break;
                case 9:
                    this.r.o(fragment);
                    break;
                case 10:
                    this.r.a(fragment, op.g);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + op.f700a);
            }
            if (!(this.p || op.f700a == 3 || fragment == null)) {
                this.r.i(fragment);
            }
        }
        if (!this.p && z) {
            FragmentManager fragmentManager = this.r;
            fragmentManager.a(fragmentManager.n, true);
        }
    }

    public int b() {
        return b(true);
    }

    /* access modifiers changed from: package-private */
    public int b(boolean z) {
        if (!this.s) {
            if (FragmentManager.d(2)) {
                Log.v("FragmentManager", "Commit: " + this);
                PrintWriter printWriter = new PrintWriter(new LogWriter("FragmentManager"));
                a("  ", printWriter);
                printWriter.close();
            }
            this.s = true;
            if (this.g) {
                this.t = this.r.a();
            } else {
                this.t = -1;
            }
            this.r.a((FragmentManager.OpGenerator) this, z);
            return this.t;
        }
        throw new IllegalStateException("commit already called");
    }

    /* access modifiers changed from: package-private */
    public Fragment b(ArrayList<Fragment> arrayList, Fragment fragment) {
        for (int size = this.f699a.size() - 1; size >= 0; size--) {
            FragmentTransaction.Op op = this.f699a.get(size);
            int i = op.f700a;
            if (i != 1) {
                if (i != 3) {
                    switch (i) {
                        case 6:
                            break;
                        case 7:
                            break;
                        case 8:
                            fragment = null;
                            break;
                        case 9:
                            fragment = op.b;
                            break;
                        case 10:
                            op.h = op.g;
                            break;
                    }
                }
                arrayList.add(op.b);
            }
            arrayList.remove(op.b);
        }
        return fragment;
    }

    private static boolean b(FragmentTransaction.Op op) {
        Fragment fragment = op.b;
        return fragment != null && fragment.mAdded && fragment.mView != null && !fragment.mDetached && !fragment.mHidden && fragment.isPostponed();
    }

    /* access modifiers changed from: package-private */
    public void a(int i, Fragment fragment, String str, int i2) {
        super.a(i, fragment, str, i2);
        fragment.mFragmentManager = this.r;
    }

    public FragmentTransaction a(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.a(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot remove Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    public FragmentTransaction a(Fragment fragment, Lifecycle.State state) {
        if (fragment.mFragmentManager != this.r) {
            throw new IllegalArgumentException("Cannot setMaxLifecycle for Fragment not attached to FragmentManager " + this.r);
        } else if (state.a(Lifecycle.State.CREATED)) {
            super.a(fragment, state);
            return this;
        } else {
            throw new IllegalArgumentException("Cannot set maximum Lifecycle below " + Lifecycle.State.CREATED);
        }
    }

    public int a() {
        return b(false);
    }

    public boolean a(ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2) {
        if (FragmentManager.d(2)) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(false);
        if (!this.g) {
            return true;
        }
        this.r.a(this);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(ArrayList<BackStackRecord> arrayList, int i, int i2) {
        if (i2 == i) {
            return false;
        }
        int size = this.f699a.size();
        int i3 = -1;
        for (int i4 = 0; i4 < size; i4++) {
            Fragment fragment = this.f699a.get(i4).b;
            int i5 = fragment != null ? fragment.mContainerId : 0;
            if (!(i5 == 0 || i5 == i3)) {
                for (int i6 = i; i6 < i2; i6++) {
                    BackStackRecord backStackRecord = arrayList.get(i6);
                    int size2 = backStackRecord.f699a.size();
                    for (int i7 = 0; i7 < size2; i7++) {
                        Fragment fragment2 = backStackRecord.f699a.get(i7).b;
                        if ((fragment2 != null ? fragment2.mContainerId : 0) == i5) {
                            return true;
                        }
                    }
                }
                i3 = i5;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Fragment a(ArrayList<Fragment> arrayList, Fragment fragment) {
        ArrayList<Fragment> arrayList2 = arrayList;
        Fragment fragment2 = fragment;
        int i = 0;
        while (i < this.f699a.size()) {
            FragmentTransaction.Op op = this.f699a.get(i);
            int i2 = op.f700a;
            if (i2 != 1) {
                if (i2 == 2) {
                    Fragment fragment3 = op.b;
                    int i3 = fragment3.mContainerId;
                    Fragment fragment4 = fragment2;
                    int i4 = i;
                    boolean z = false;
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        Fragment fragment5 = arrayList2.get(size);
                        if (fragment5.mContainerId == i3) {
                            if (fragment5 == fragment3) {
                                z = true;
                            } else {
                                if (fragment5 == fragment4) {
                                    this.f699a.add(i4, new FragmentTransaction.Op(9, fragment5));
                                    i4++;
                                    fragment4 = null;
                                }
                                FragmentTransaction.Op op2 = new FragmentTransaction.Op(3, fragment5);
                                op2.c = op.c;
                                op2.e = op.e;
                                op2.d = op.d;
                                op2.f = op.f;
                                this.f699a.add(i4, op2);
                                arrayList2.remove(fragment5);
                                i4++;
                            }
                        }
                    }
                    if (z) {
                        this.f699a.remove(i4);
                        i4--;
                    } else {
                        op.f700a = 1;
                        arrayList2.add(fragment3);
                    }
                    i = i4;
                    fragment2 = fragment4;
                } else if (i2 == 3 || i2 == 6) {
                    arrayList2.remove(op.b);
                    Fragment fragment6 = op.b;
                    if (fragment6 == fragment2) {
                        this.f699a.add(i, new FragmentTransaction.Op(9, fragment6));
                        i++;
                        fragment2 = null;
                    }
                } else if (i2 != 7) {
                    if (i2 == 8) {
                        this.f699a.add(i, new FragmentTransaction.Op(9, fragment2));
                        i++;
                        fragment2 = op.b;
                    }
                }
                i++;
            }
            arrayList2.add(op.b);
            i++;
        }
        return fragment2;
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment.OnStartEnterTransitionListener onStartEnterTransitionListener) {
        for (int i = 0; i < this.f699a.size(); i++) {
            FragmentTransaction.Op op = this.f699a.get(i);
            if (b(op)) {
                op.b.setOnStartEnterTransitionListener(onStartEnterTransitionListener);
            }
        }
    }
}
