package androidx.fragment.app;

import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.collection.ArrayMap;
import androidx.core.app.SharedElementCallback;
import androidx.core.os.CancellationSignal;
import androidx.core.view.OneShotPreDrawListener;
import androidx.core.view.ViewCompat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

class FragmentTransition {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f701a = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8, 10};
    private static final FragmentTransitionImpl b = (Build.VERSION.SDK_INT >= 21 ? new FragmentTransitionCompat21() : null);
    private static final FragmentTransitionImpl c = a();

    interface Callback {
        void a(Fragment fragment, CancellationSignal cancellationSignal);

        void b(Fragment fragment, CancellationSignal cancellationSignal);
    }

    static class FragmentContainerTransition {

        /* renamed from: a  reason: collision with root package name */
        public Fragment f708a;
        public boolean b;
        public BackStackRecord c;
        public Fragment d;
        public boolean e;
        public BackStackRecord f;

        FragmentContainerTransition() {
        }
    }

    private FragmentTransition() {
    }

    private static FragmentTransitionImpl a() {
        try {
            return (FragmentTransitionImpl) Class.forName("androidx.transition.FragmentTransitionSupport").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        r12 = r4.f708a;
        r13 = r4.d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void b(androidx.fragment.app.FragmentManager r17, int r18, androidx.fragment.app.FragmentTransition.FragmentContainerTransition r19, android.view.View r20, androidx.collection.ArrayMap<java.lang.String, java.lang.String> r21, androidx.fragment.app.FragmentTransition.Callback r22) {
        /*
            r0 = r17
            r4 = r19
            r9 = r20
            r10 = r22
            androidx.fragment.app.FragmentContainer r1 = r0.p
            boolean r1 = r1.a()
            if (r1 == 0) goto L_0x001b
            androidx.fragment.app.FragmentContainer r0 = r0.p
            r1 = r18
            android.view.View r0 = r0.a(r1)
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            goto L_0x001c
        L_0x001b:
            r0 = 0
        L_0x001c:
            r11 = r0
            if (r11 != 0) goto L_0x0020
            return
        L_0x0020:
            androidx.fragment.app.Fragment r12 = r4.f708a
            androidx.fragment.app.Fragment r13 = r4.d
            androidx.fragment.app.FragmentTransitionImpl r14 = a((androidx.fragment.app.Fragment) r13, (androidx.fragment.app.Fragment) r12)
            if (r14 != 0) goto L_0x002b
            return
        L_0x002b:
            boolean r15 = r4.b
            boolean r0 = r4.e
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.lang.Object r6 = a((androidx.fragment.app.FragmentTransitionImpl) r14, (androidx.fragment.app.Fragment) r12, (boolean) r15)
            java.lang.Object r5 = b((androidx.fragment.app.FragmentTransitionImpl) r14, (androidx.fragment.app.Fragment) r13, (boolean) r0)
            r0 = r14
            r1 = r11
            r2 = r20
            r3 = r21
            r4 = r19
            r17 = r5
            r5 = r7
            r18 = r6
            r6 = r8
            r16 = r11
            r11 = r7
            r7 = r18
            r10 = r8
            r8 = r17
            java.lang.Object r8 = b(r0, r1, r2, r3, r4, r5, r6, r7, r8)
            r6 = r18
            if (r6 != 0) goto L_0x0066
            if (r8 != 0) goto L_0x0066
            r7 = r17
            if (r7 != 0) goto L_0x0068
            return
        L_0x0066:
            r7 = r17
        L_0x0068:
            java.util.ArrayList r5 = a((androidx.fragment.app.FragmentTransitionImpl) r14, (java.lang.Object) r7, (androidx.fragment.app.Fragment) r13, (java.util.ArrayList<android.view.View>) r11, (android.view.View) r9)
            java.util.ArrayList r9 = a((androidx.fragment.app.FragmentTransitionImpl) r14, (java.lang.Object) r6, (androidx.fragment.app.Fragment) r12, (java.util.ArrayList<android.view.View>) r10, (android.view.View) r9)
            r0 = 4
            a((java.util.ArrayList<android.view.View>) r9, (int) r0)
            r0 = r14
            r1 = r6
            r2 = r7
            r3 = r8
            r4 = r12
            r12 = r5
            r5 = r15
            java.lang.Object r15 = a((androidx.fragment.app.FragmentTransitionImpl) r0, (java.lang.Object) r1, (java.lang.Object) r2, (java.lang.Object) r3, (androidx.fragment.app.Fragment) r4, (boolean) r5)
            if (r13 == 0) goto L_0x00a1
            if (r12 == 0) goto L_0x00a1
            int r0 = r12.size()
            if (r0 > 0) goto L_0x008f
            int r0 = r11.size()
            if (r0 <= 0) goto L_0x00a1
        L_0x008f:
            androidx.core.os.CancellationSignal r0 = new androidx.core.os.CancellationSignal
            r0.<init>()
            r1 = r22
            r1.b(r13, r0)
            androidx.fragment.app.FragmentTransition$1 r2 = new androidx.fragment.app.FragmentTransition$1
            r2.<init>(r1, r13, r0)
            r14.a(r13, r15, r0, r2)
        L_0x00a1:
            if (r15 == 0) goto L_0x00cb
            a((androidx.fragment.app.FragmentTransitionImpl) r14, (java.lang.Object) r7, (androidx.fragment.app.Fragment) r13, (java.util.ArrayList<android.view.View>) r12)
            java.util.ArrayList r13 = r14.a((java.util.ArrayList<android.view.View>) r10)
            r0 = r14
            r1 = r15
            r2 = r6
            r3 = r9
            r4 = r7
            r5 = r12
            r6 = r8
            r7 = r10
            r0.a(r1, r2, r3, r4, r5, r6, r7)
            r0 = r16
            r14.a((android.view.ViewGroup) r0, (java.lang.Object) r15)
            r1 = r14
            r2 = r0
            r3 = r11
            r4 = r10
            r5 = r13
            r6 = r21
            r1.a(r2, r3, r4, r5, r6)
            r0 = 0
            a((java.util.ArrayList<android.view.View>) r9, (int) r0)
            r14.b((java.lang.Object) r8, (java.util.ArrayList<android.view.View>) r11, (java.util.ArrayList<android.view.View>) r10)
        L_0x00cb:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentTransition.b(androidx.fragment.app.FragmentManager, int, androidx.fragment.app.FragmentTransition$FragmentContainerTransition, android.view.View, androidx.collection.ArrayMap, androidx.fragment.app.FragmentTransition$Callback):void");
    }

    static void a(FragmentManager fragmentManager, ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, boolean z, Callback callback) {
        FragmentManager fragmentManager2 = fragmentManager;
        ArrayList<BackStackRecord> arrayList3 = arrayList;
        ArrayList<Boolean> arrayList4 = arrayList2;
        int i3 = i2;
        boolean z2 = z;
        if (fragmentManager2.n >= 1) {
            SparseArray sparseArray = new SparseArray();
            for (int i4 = i; i4 < i3; i4++) {
                BackStackRecord backStackRecord = arrayList3.get(i4);
                if (arrayList4.get(i4).booleanValue()) {
                    b(backStackRecord, (SparseArray<FragmentContainerTransition>) sparseArray, z2);
                } else {
                    a(backStackRecord, (SparseArray<FragmentContainerTransition>) sparseArray, z2);
                }
            }
            if (sparseArray.size() != 0) {
                View view = new View(fragmentManager2.o.c());
                int size = sparseArray.size();
                for (int i5 = 0; i5 < size; i5++) {
                    int keyAt = sparseArray.keyAt(i5);
                    ArrayMap<String, String> a2 = a(keyAt, arrayList3, arrayList4, i, i3);
                    FragmentContainerTransition fragmentContainerTransition = (FragmentContainerTransition) sparseArray.valueAt(i5);
                    if (z2) {
                        b(fragmentManager, keyAt, fragmentContainerTransition, view, a2, callback);
                    } else {
                        a(fragmentManager, keyAt, fragmentContainerTransition, view, a2, callback);
                    }
                }
            }
        }
    }

    private static ArrayMap<String, String> a(int i, ArrayList<BackStackRecord> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        ArrayMap<String, String> arrayMap = new ArrayMap<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            BackStackRecord backStackRecord = arrayList.get(i4);
            if (backStackRecord.c(i)) {
                boolean booleanValue = arrayList2.get(i4).booleanValue();
                ArrayList<String> arrayList5 = backStackRecord.n;
                if (arrayList5 != null) {
                    int size = arrayList5.size();
                    if (booleanValue) {
                        arrayList3 = backStackRecord.n;
                        arrayList4 = backStackRecord.o;
                    } else {
                        ArrayList<String> arrayList6 = backStackRecord.n;
                        arrayList3 = backStackRecord.o;
                        arrayList4 = arrayList6;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        String str = arrayList4.get(i5);
                        String str2 = arrayList3.get(i5);
                        String remove = arrayMap.remove(str2);
                        if (remove != null) {
                            arrayMap.put(str, remove);
                        } else {
                            arrayMap.put(str, str2);
                        }
                    }
                }
            }
        }
        return arrayMap;
    }

    private static Object b(FragmentTransitionImpl fragmentTransitionImpl, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReturnTransition();
        } else {
            obj = fragment.getExitTransition();
        }
        return fragmentTransitionImpl.b(obj);
    }

    private static Object b(FragmentTransitionImpl fragmentTransitionImpl, ViewGroup viewGroup, View view, ArrayMap<String, String> arrayMap, FragmentContainerTransition fragmentContainerTransition, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        Object obj4;
        final Rect rect;
        final View view2;
        FragmentTransitionImpl fragmentTransitionImpl2 = fragmentTransitionImpl;
        View view3 = view;
        ArrayMap<String, String> arrayMap2 = arrayMap;
        FragmentContainerTransition fragmentContainerTransition2 = fragmentContainerTransition;
        ArrayList<View> arrayList3 = arrayList;
        ArrayList<View> arrayList4 = arrayList2;
        Object obj5 = obj;
        Fragment fragment = fragmentContainerTransition2.f708a;
        Fragment fragment2 = fragmentContainerTransition2.d;
        if (fragment != null) {
            fragment.requireView().setVisibility(0);
        }
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = fragmentContainerTransition2.b;
        if (arrayMap.isEmpty()) {
            obj3 = null;
        } else {
            obj3 = a(fragmentTransitionImpl, fragment, fragment2, z);
        }
        ArrayMap<String, View> b2 = b(fragmentTransitionImpl, arrayMap2, obj3, fragmentContainerTransition2);
        ArrayMap<String, View> a2 = a(fragmentTransitionImpl, arrayMap2, obj3, fragmentContainerTransition2);
        if (arrayMap.isEmpty()) {
            if (b2 != null) {
                b2.clear();
            }
            if (a2 != null) {
                a2.clear();
            }
            obj4 = null;
        } else {
            a(arrayList3, b2, (Collection<String>) arrayMap.keySet());
            a(arrayList4, a2, arrayMap.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            arrayList4.add(view3);
            fragmentTransitionImpl.b(obj4, view3, arrayList3);
            a(fragmentTransitionImpl, obj4, obj2, b2, fragmentContainerTransition2.e, fragmentContainerTransition2.f);
            Rect rect2 = new Rect();
            View a3 = a(a2, fragmentContainerTransition2, obj5, z);
            if (a3 != null) {
                fragmentTransitionImpl.a(obj5, rect2);
            }
            rect = rect2;
            view2 = a3;
        } else {
            view2 = null;
            rect = null;
        }
        final Fragment fragment3 = fragment;
        final Fragment fragment4 = fragment2;
        final boolean z2 = z;
        final ArrayMap<String, View> arrayMap3 = a2;
        final FragmentTransitionImpl fragmentTransitionImpl3 = fragmentTransitionImpl;
        OneShotPreDrawListener.a(viewGroup, new Runnable() {
            public void run() {
                FragmentTransition.a(fragment3, fragment4, z2, (ArrayMap<String, View>) arrayMap3, false);
                View view = view2;
                if (view != null) {
                    fragmentTransitionImpl3.a(view, rect);
                }
            }
        });
        return obj4;
    }

    private static void a(FragmentTransitionImpl fragmentTransitionImpl, Object obj, Fragment fragment, final ArrayList<View> arrayList) {
        if (fragment != null && obj != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            fragmentTransitionImpl.a(obj, fragment.getView(), arrayList);
            OneShotPreDrawListener.a(fragment.mContainer, new Runnable() {
                public void run() {
                    FragmentTransition.a((ArrayList<View>) arrayList, 4);
                }
            });
        }
    }

    private static void a(FragmentManager fragmentManager, int i, FragmentContainerTransition fragmentContainerTransition, View view, ArrayMap<String, String> arrayMap, Callback callback) {
        Fragment fragment;
        Fragment fragment2;
        FragmentTransitionImpl a2;
        Object obj;
        FragmentManager fragmentManager2 = fragmentManager;
        FragmentContainerTransition fragmentContainerTransition2 = fragmentContainerTransition;
        View view2 = view;
        ArrayMap<String, String> arrayMap2 = arrayMap;
        final Callback callback2 = callback;
        ViewGroup viewGroup = fragmentManager2.p.a() ? (ViewGroup) fragmentManager2.p.a(i) : null;
        if (viewGroup != null && (a2 = a(fragment2, fragment)) != null) {
            boolean z = fragmentContainerTransition2.b;
            boolean z2 = fragmentContainerTransition2.e;
            Object a3 = a(a2, (fragment = fragmentContainerTransition2.f708a), z);
            Object b2 = b(a2, (fragment2 = fragmentContainerTransition2.d), z2);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = arrayList;
            Object obj2 = b2;
            Object obj3 = a3;
            FragmentTransitionImpl fragmentTransitionImpl = a2;
            final Fragment fragment3 = fragment2;
            Object a4 = a(a2, viewGroup, view, arrayMap, fragmentContainerTransition, (ArrayList<View>) arrayList3, (ArrayList<View>) arrayList2, obj3, obj2);
            Object obj4 = obj3;
            if (obj4 == null && a4 == null) {
                obj = obj2;
                if (obj == null) {
                    return;
                }
            } else {
                obj = obj2;
            }
            ArrayList arrayList4 = arrayList3;
            ArrayList<View> a5 = a(fragmentTransitionImpl, obj, fragment3, (ArrayList<View>) arrayList4, view2);
            Object obj5 = (a5 == null || a5.isEmpty()) ? null : obj;
            fragmentTransitionImpl.a(obj4, view2);
            Object a6 = a(fragmentTransitionImpl, obj4, obj5, a4, fragment, fragmentContainerTransition2.b);
            if (!(fragment3 == null || a5 == null || (a5.size() <= 0 && arrayList4.size() <= 0))) {
                final CancellationSignal cancellationSignal = new CancellationSignal();
                callback2.b(fragment3, cancellationSignal);
                fragmentTransitionImpl.a(fragment3, a6, cancellationSignal, new Runnable() {
                    public void run() {
                        callback2.a(fragment3, cancellationSignal);
                    }
                });
            }
            if (a6 != null) {
                ArrayList arrayList5 = new ArrayList();
                FragmentTransitionImpl fragmentTransitionImpl2 = fragmentTransitionImpl;
                fragmentTransitionImpl2.a(a6, obj4, arrayList5, obj5, a5, a4, arrayList2);
                a(fragmentTransitionImpl2, viewGroup, fragment, view, (ArrayList<View>) arrayList2, obj4, (ArrayList<View>) arrayList5, obj5, a5);
                ArrayList arrayList6 = arrayList2;
                ArrayMap<String, String> arrayMap3 = arrayMap;
                fragmentTransitionImpl.a((View) viewGroup, (ArrayList<View>) arrayList6, (Map<String, String>) arrayMap3);
                fragmentTransitionImpl.a(viewGroup, a6);
                fragmentTransitionImpl.a(viewGroup, (ArrayList<View>) arrayList6, (Map<String, String>) arrayMap3);
            }
        }
    }

    private static ArrayMap<String, View> b(FragmentTransitionImpl fragmentTransitionImpl, ArrayMap<String, String> arrayMap, Object obj, FragmentContainerTransition fragmentContainerTransition) {
        SharedElementCallback sharedElementCallback;
        ArrayList<String> arrayList;
        if (arrayMap.isEmpty() || obj == null) {
            arrayMap.clear();
            return null;
        }
        Fragment fragment = fragmentContainerTransition.d;
        ArrayMap<String, View> arrayMap2 = new ArrayMap<>();
        fragmentTransitionImpl.a((Map<String, View>) arrayMap2, fragment.requireView());
        BackStackRecord backStackRecord = fragmentContainerTransition.f;
        if (fragmentContainerTransition.e) {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = backStackRecord.o;
        } else {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = backStackRecord.n;
        }
        if (arrayList != null) {
            arrayMap2.a(arrayList);
        }
        if (sharedElementCallback != null) {
            sharedElementCallback.a((List<String>) arrayList, (Map<String, View>) arrayMap2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view = arrayMap2.get(str);
                if (view == null) {
                    arrayMap.remove(str);
                } else if (!str.equals(ViewCompat.s(view))) {
                    arrayMap.put(ViewCompat.s(view), arrayMap.remove(str));
                }
            }
        } else {
            arrayMap.a(arrayMap2.keySet());
        }
        return arrayMap2;
    }

    private static void a(FragmentTransitionImpl fragmentTransitionImpl, ViewGroup viewGroup, Fragment fragment, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        final Object obj3 = obj;
        final FragmentTransitionImpl fragmentTransitionImpl2 = fragmentTransitionImpl;
        final View view2 = view;
        final Fragment fragment2 = fragment;
        final ArrayList<View> arrayList4 = arrayList;
        final ArrayList<View> arrayList5 = arrayList2;
        final ArrayList<View> arrayList6 = arrayList3;
        final Object obj4 = obj2;
        ViewGroup viewGroup2 = viewGroup;
        OneShotPreDrawListener.a(viewGroup, new Runnable() {
            public void run() {
                Object obj = obj3;
                if (obj != null) {
                    fragmentTransitionImpl2.b(obj, view2);
                    arrayList5.addAll(FragmentTransition.a(fragmentTransitionImpl2, obj3, fragment2, (ArrayList<View>) arrayList4, view2));
                }
                if (arrayList6 != null) {
                    if (obj4 != null) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(view2);
                        fragmentTransitionImpl2.a(obj4, (ArrayList<View>) arrayList6, (ArrayList<View>) arrayList);
                    }
                    arrayList6.clear();
                    arrayList6.add(view2);
                }
            }
        });
    }

    private static FragmentTransitionImpl a(Fragment fragment, Fragment fragment2) {
        ArrayList arrayList = new ArrayList();
        if (fragment != null) {
            Object exitTransition = fragment.getExitTransition();
            if (exitTransition != null) {
                arrayList.add(exitTransition);
            }
            Object returnTransition = fragment.getReturnTransition();
            if (returnTransition != null) {
                arrayList.add(returnTransition);
            }
            Object sharedElementReturnTransition = fragment.getSharedElementReturnTransition();
            if (sharedElementReturnTransition != null) {
                arrayList.add(sharedElementReturnTransition);
            }
        }
        if (fragment2 != null) {
            Object enterTransition = fragment2.getEnterTransition();
            if (enterTransition != null) {
                arrayList.add(enterTransition);
            }
            Object reenterTransition = fragment2.getReenterTransition();
            if (reenterTransition != null) {
                arrayList.add(reenterTransition);
            }
            Object sharedElementEnterTransition = fragment2.getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {
                arrayList.add(sharedElementEnterTransition);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        FragmentTransitionImpl fragmentTransitionImpl = b;
        if (fragmentTransitionImpl != null && a(fragmentTransitionImpl, (List<Object>) arrayList)) {
            return b;
        }
        FragmentTransitionImpl fragmentTransitionImpl2 = c;
        if (fragmentTransitionImpl2 != null && a(fragmentTransitionImpl2, (List<Object>) arrayList)) {
            return c;
        }
        if (b == null && c == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }

    public static void b(BackStackRecord backStackRecord, SparseArray<FragmentContainerTransition> sparseArray, boolean z) {
        if (backStackRecord.r.p.a()) {
            for (int size = backStackRecord.f699a.size() - 1; size >= 0; size--) {
                a(backStackRecord, backStackRecord.f699a.get(size), sparseArray, true, z);
            }
        }
    }

    private static boolean a(FragmentTransitionImpl fragmentTransitionImpl, List<Object> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!fragmentTransitionImpl.a(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    private static Object a(FragmentTransitionImpl fragmentTransitionImpl, Fragment fragment, Fragment fragment2, boolean z) {
        Object obj;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        if (z) {
            obj = fragment2.getSharedElementReturnTransition();
        } else {
            obj = fragment.getSharedElementEnterTransition();
        }
        return fragmentTransitionImpl.c(fragmentTransitionImpl.b(obj));
    }

    private static Object a(FragmentTransitionImpl fragmentTransitionImpl, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReenterTransition();
        } else {
            obj = fragment.getEnterTransition();
        }
        return fragmentTransitionImpl.b(obj);
    }

    private static void a(ArrayList<View> arrayList, ArrayMap<String, View> arrayMap, Collection<String> collection) {
        for (int size = arrayMap.size() - 1; size >= 0; size--) {
            View d = arrayMap.d(size);
            if (collection.contains(ViewCompat.s(d))) {
                arrayList.add(d);
            }
        }
    }

    private static Object a(FragmentTransitionImpl fragmentTransitionImpl, ViewGroup viewGroup, View view, ArrayMap<String, String> arrayMap, FragmentContainerTransition fragmentContainerTransition, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        ArrayMap<String, String> arrayMap2;
        Object obj3;
        Object obj4;
        Rect rect;
        FragmentTransitionImpl fragmentTransitionImpl2 = fragmentTransitionImpl;
        FragmentContainerTransition fragmentContainerTransition2 = fragmentContainerTransition;
        ArrayList<View> arrayList3 = arrayList;
        Object obj5 = obj;
        Fragment fragment = fragmentContainerTransition2.f708a;
        Fragment fragment2 = fragmentContainerTransition2.d;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = fragmentContainerTransition2.b;
        if (arrayMap.isEmpty()) {
            arrayMap2 = arrayMap;
            obj3 = null;
        } else {
            obj3 = a(fragmentTransitionImpl2, fragment, fragment2, z);
            arrayMap2 = arrayMap;
        }
        ArrayMap<String, View> b2 = b(fragmentTransitionImpl2, arrayMap2, obj3, fragmentContainerTransition2);
        if (arrayMap.isEmpty()) {
            obj4 = null;
        } else {
            arrayList3.addAll(b2.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            rect = new Rect();
            fragmentTransitionImpl2.b(obj4, view, arrayList3);
            a(fragmentTransitionImpl, obj4, obj2, b2, fragmentContainerTransition2.e, fragmentContainerTransition2.f);
            if (obj5 != null) {
                fragmentTransitionImpl2.a(obj5, rect);
            }
        } else {
            rect = null;
        }
        final FragmentTransitionImpl fragmentTransitionImpl3 = fragmentTransitionImpl;
        final ArrayMap<String, String> arrayMap3 = arrayMap;
        final Object obj6 = obj4;
        final FragmentContainerTransition fragmentContainerTransition3 = fragmentContainerTransition;
        AnonymousClass6 r13 = r0;
        final ArrayList<View> arrayList4 = arrayList2;
        final View view2 = view;
        final Fragment fragment3 = fragment;
        final Fragment fragment4 = fragment2;
        final boolean z2 = z;
        final ArrayList<View> arrayList5 = arrayList;
        final Object obj7 = obj;
        final Rect rect2 = rect;
        AnonymousClass6 r0 = new Runnable() {
            public void run() {
                ArrayMap<String, View> a2 = FragmentTransition.a(fragmentTransitionImpl3, (ArrayMap<String, String>) arrayMap3, obj6, fragmentContainerTransition3);
                if (a2 != null) {
                    arrayList4.addAll(a2.values());
                    arrayList4.add(view2);
                }
                FragmentTransition.a(fragment3, fragment4, z2, a2, false);
                Object obj = obj6;
                if (obj != null) {
                    fragmentTransitionImpl3.b(obj, (ArrayList<View>) arrayList5, (ArrayList<View>) arrayList4);
                    View a3 = FragmentTransition.a(a2, fragmentContainerTransition3, obj7, z2);
                    if (a3 != null) {
                        fragmentTransitionImpl3.a(a3, rect2);
                    }
                }
            }
        };
        OneShotPreDrawListener.a(viewGroup, r13);
        return obj4;
    }

    static ArrayMap<String, View> a(FragmentTransitionImpl fragmentTransitionImpl, ArrayMap<String, String> arrayMap, Object obj, FragmentContainerTransition fragmentContainerTransition) {
        SharedElementCallback sharedElementCallback;
        ArrayList<String> arrayList;
        String a2;
        Fragment fragment = fragmentContainerTransition.f708a;
        View view = fragment.getView();
        if (arrayMap.isEmpty() || obj == null || view == null) {
            arrayMap.clear();
            return null;
        }
        ArrayMap<String, View> arrayMap2 = new ArrayMap<>();
        fragmentTransitionImpl.a((Map<String, View>) arrayMap2, view);
        BackStackRecord backStackRecord = fragmentContainerTransition.c;
        if (fragmentContainerTransition.b) {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = backStackRecord.n;
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = backStackRecord.o;
        }
        if (arrayList != null) {
            arrayMap2.a(arrayList);
            arrayMap2.a(arrayMap.values());
        }
        if (sharedElementCallback != null) {
            sharedElementCallback.a((List<String>) arrayList, (Map<String, View>) arrayMap2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view2 = arrayMap2.get(str);
                if (view2 == null) {
                    String a3 = a(arrayMap, str);
                    if (a3 != null) {
                        arrayMap.remove(a3);
                    }
                } else if (!str.equals(ViewCompat.s(view2)) && (a2 = a(arrayMap, str)) != null) {
                    arrayMap.put(a2, ViewCompat.s(view2));
                }
            }
        } else {
            a(arrayMap, arrayMap2);
        }
        return arrayMap2;
    }

    private static String a(ArrayMap<String, String> arrayMap, String str) {
        int size = arrayMap.size();
        for (int i = 0; i < size; i++) {
            if (str.equals(arrayMap.d(i))) {
                return arrayMap.b(i);
            }
        }
        return null;
    }

    static View a(ArrayMap<String, View> arrayMap, FragmentContainerTransition fragmentContainerTransition, Object obj, boolean z) {
        ArrayList<String> arrayList;
        String str;
        BackStackRecord backStackRecord = fragmentContainerTransition.c;
        if (obj == null || arrayMap == null || (arrayList = backStackRecord.n) == null || arrayList.isEmpty()) {
            return null;
        }
        if (z) {
            str = backStackRecord.n.get(0);
        } else {
            str = backStackRecord.o.get(0);
        }
        return arrayMap.get(str);
    }

    private static void a(FragmentTransitionImpl fragmentTransitionImpl, Object obj, Object obj2, ArrayMap<String, View> arrayMap, boolean z, BackStackRecord backStackRecord) {
        String str;
        ArrayList<String> arrayList = backStackRecord.n;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (z) {
                str = backStackRecord.o.get(0);
            } else {
                str = backStackRecord.n.get(0);
            }
            View view = arrayMap.get(str);
            fragmentTransitionImpl.c(obj, view);
            if (obj2 != null) {
                fragmentTransitionImpl.c(obj2, view);
            }
        }
    }

    private static void a(ArrayMap<String, String> arrayMap, ArrayMap<String, View> arrayMap2) {
        for (int size = arrayMap.size() - 1; size >= 0; size--) {
            if (!arrayMap2.containsKey(arrayMap.d(size))) {
                arrayMap.c(size);
            }
        }
    }

    static void a(Fragment fragment, Fragment fragment2, boolean z, ArrayMap<String, View> arrayMap, boolean z2) {
        SharedElementCallback sharedElementCallback;
        int i;
        if (z) {
            sharedElementCallback = fragment2.getEnterTransitionCallback();
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
        }
        if (sharedElementCallback != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (arrayMap == null) {
                i = 0;
            } else {
                i = arrayMap.size();
            }
            for (int i2 = 0; i2 < i; i2++) {
                arrayList2.add(arrayMap.b(i2));
                arrayList.add(arrayMap.d(i2));
            }
            if (z2) {
                sharedElementCallback.b(arrayList2, arrayList, (List<View>) null);
            } else {
                sharedElementCallback.a((List<String>) arrayList2, (List<View>) arrayList, (List<View>) null);
            }
        }
    }

    static ArrayList<View> a(FragmentTransitionImpl fragmentTransitionImpl, Object obj, Fragment fragment, ArrayList<View> arrayList, View view) {
        if (obj == null) {
            return null;
        }
        ArrayList<View> arrayList2 = new ArrayList<>();
        View view2 = fragment.getView();
        if (view2 != null) {
            fragmentTransitionImpl.a(arrayList2, view2);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (arrayList2.isEmpty()) {
            return arrayList2;
        }
        arrayList2.add(view);
        fragmentTransitionImpl.a(obj, arrayList2);
        return arrayList2;
    }

    static void a(ArrayList<View> arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i);
            }
        }
    }

    private static Object a(FragmentTransitionImpl fragmentTransitionImpl, Object obj, Object obj2, Object obj3, Fragment fragment, boolean z) {
        boolean z2;
        if (obj == null || obj2 == null || fragment == null) {
            z2 = true;
        } else {
            z2 = z ? fragment.getAllowReturnTransitionOverlap() : fragment.getAllowEnterTransitionOverlap();
        }
        if (z2) {
            return fragmentTransitionImpl.b(obj2, obj, obj3);
        }
        return fragmentTransitionImpl.a(obj2, obj, obj3);
    }

    public static void a(BackStackRecord backStackRecord, SparseArray<FragmentContainerTransition> sparseArray, boolean z) {
        int size = backStackRecord.f699a.size();
        for (int i = 0; i < size; i++) {
            a(backStackRecord, backStackRecord.f699a.get(i), sparseArray, false, z);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0039, code lost:
        if (r0.mAdded != false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x006e, code lost:
        r9 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x008a, code lost:
        if (r0.mHidden == false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x008c, code lost:
        r9 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x00d9 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(androidx.fragment.app.BackStackRecord r8, androidx.fragment.app.FragmentTransaction.Op r9, android.util.SparseArray<androidx.fragment.app.FragmentTransition.FragmentContainerTransition> r10, boolean r11, boolean r12) {
        /*
            androidx.fragment.app.Fragment r0 = r9.b
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            int r1 = r0.mContainerId
            if (r1 != 0) goto L_0x000a
            return
        L_0x000a:
            if (r11 == 0) goto L_0x0013
            int[] r2 = f701a
            int r9 = r9.f700a
            r9 = r2[r9]
            goto L_0x0015
        L_0x0013:
            int r9 = r9.f700a
        L_0x0015:
            r2 = 0
            r3 = 1
            if (r9 == r3) goto L_0x007f
            r4 = 3
            if (r9 == r4) goto L_0x0057
            r4 = 4
            if (r9 == r4) goto L_0x003f
            r4 = 5
            if (r9 == r4) goto L_0x002d
            r4 = 6
            if (r9 == r4) goto L_0x0057
            r4 = 7
            if (r9 == r4) goto L_0x007f
            r9 = 0
        L_0x0029:
            r4 = 0
            r5 = 0
            goto L_0x0092
        L_0x002d:
            if (r12 == 0) goto L_0x003c
            boolean r9 = r0.mHiddenChanged
            if (r9 == 0) goto L_0x008e
            boolean r9 = r0.mHidden
            if (r9 != 0) goto L_0x008e
            boolean r9 = r0.mAdded
            if (r9 == 0) goto L_0x008e
            goto L_0x008c
        L_0x003c:
            boolean r9 = r0.mHidden
            goto L_0x008f
        L_0x003f:
            if (r12 == 0) goto L_0x004e
            boolean r9 = r0.mHiddenChanged
            if (r9 == 0) goto L_0x0070
            boolean r9 = r0.mAdded
            if (r9 == 0) goto L_0x0070
            boolean r9 = r0.mHidden
            if (r9 == 0) goto L_0x0070
        L_0x004d:
            goto L_0x006e
        L_0x004e:
            boolean r9 = r0.mAdded
            if (r9 == 0) goto L_0x0070
            boolean r9 = r0.mHidden
            if (r9 != 0) goto L_0x0070
            goto L_0x004d
        L_0x0057:
            if (r12 == 0) goto L_0x0072
            boolean r9 = r0.mAdded
            if (r9 != 0) goto L_0x0070
            android.view.View r9 = r0.mView
            if (r9 == 0) goto L_0x0070
            int r9 = r9.getVisibility()
            if (r9 != 0) goto L_0x0070
            float r9 = r0.mPostponedAlpha
            r4 = 0
            int r9 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r9 < 0) goto L_0x0070
        L_0x006e:
            r9 = 1
            goto L_0x007b
        L_0x0070:
            r9 = 0
            goto L_0x007b
        L_0x0072:
            boolean r9 = r0.mAdded
            if (r9 == 0) goto L_0x0070
            boolean r9 = r0.mHidden
            if (r9 != 0) goto L_0x0070
            goto L_0x006e
        L_0x007b:
            r5 = r9
            r9 = 0
            r4 = 1
            goto L_0x0092
        L_0x007f:
            if (r12 == 0) goto L_0x0084
            boolean r9 = r0.mIsNewlyAdded
            goto L_0x008f
        L_0x0084:
            boolean r9 = r0.mAdded
            if (r9 != 0) goto L_0x008e
            boolean r9 = r0.mHidden
            if (r9 != 0) goto L_0x008e
        L_0x008c:
            r9 = 1
            goto L_0x008f
        L_0x008e:
            r9 = 0
        L_0x008f:
            r2 = r9
            r9 = 1
            goto L_0x0029
        L_0x0092:
            java.lang.Object r6 = r10.get(r1)
            androidx.fragment.app.FragmentTransition$FragmentContainerTransition r6 = (androidx.fragment.app.FragmentTransition.FragmentContainerTransition) r6
            if (r2 == 0) goto L_0x00a4
            androidx.fragment.app.FragmentTransition$FragmentContainerTransition r6 = a((androidx.fragment.app.FragmentTransition.FragmentContainerTransition) r6, (android.util.SparseArray<androidx.fragment.app.FragmentTransition.FragmentContainerTransition>) r10, (int) r1)
            r6.f708a = r0
            r6.b = r11
            r6.c = r8
        L_0x00a4:
            r2 = 0
            if (r12 != 0) goto L_0x00c5
            if (r9 == 0) goto L_0x00c5
            if (r6 == 0) goto L_0x00b1
            androidx.fragment.app.Fragment r9 = r6.d
            if (r9 != r0) goto L_0x00b1
            r6.d = r2
        L_0x00b1:
            androidx.fragment.app.FragmentManager r9 = r8.r
            int r7 = r0.mState
            if (r7 >= r3) goto L_0x00c5
            int r7 = r9.n
            if (r7 < r3) goto L_0x00c5
            boolean r7 = r8.p
            if (r7 != 0) goto L_0x00c5
            r9.h(r0)
            r9.a((androidx.fragment.app.Fragment) r0, (int) r3)
        L_0x00c5:
            if (r5 == 0) goto L_0x00d7
            if (r6 == 0) goto L_0x00cd
            androidx.fragment.app.Fragment r9 = r6.d
            if (r9 != 0) goto L_0x00d7
        L_0x00cd:
            androidx.fragment.app.FragmentTransition$FragmentContainerTransition r6 = a((androidx.fragment.app.FragmentTransition.FragmentContainerTransition) r6, (android.util.SparseArray<androidx.fragment.app.FragmentTransition.FragmentContainerTransition>) r10, (int) r1)
            r6.d = r0
            r6.e = r11
            r6.f = r8
        L_0x00d7:
            if (r12 != 0) goto L_0x00e3
            if (r4 == 0) goto L_0x00e3
            if (r6 == 0) goto L_0x00e3
            androidx.fragment.app.Fragment r8 = r6.f708a
            if (r8 != r0) goto L_0x00e3
            r6.f708a = r2
        L_0x00e3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.FragmentTransition.a(androidx.fragment.app.BackStackRecord, androidx.fragment.app.FragmentTransaction$Op, android.util.SparseArray, boolean, boolean):void");
    }

    private static FragmentContainerTransition a(FragmentContainerTransition fragmentContainerTransition, SparseArray<FragmentContainerTransition> sparseArray, int i) {
        if (fragmentContainerTransition != null) {
            return fragmentContainerTransition;
        }
        FragmentContainerTransition fragmentContainerTransition2 = new FragmentContainerTransition();
        sparseArray.put(i, fragmentContainerTransition2);
        return fragmentContainerTransition2;
    }
}
