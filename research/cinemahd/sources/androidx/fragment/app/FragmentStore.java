package androidx.fragment.app;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

class FragmentStore {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<Fragment> f697a = new ArrayList<>();
    private final HashMap<String, FragmentStateManager> b = new HashMap<>();

    FragmentStore() {
    }

    /* access modifiers changed from: package-private */
    public void a(List<String> list) {
        this.f697a.clear();
        if (list != null) {
            for (String next : list) {
                Fragment b2 = b(next);
                if (b2 != null) {
                    if (FragmentManager.d(2)) {
                        Log.v("FragmentManager", "restoreSaveState: added (" + next + "): " + b2);
                    }
                    a(b2);
                } else {
                    throw new IllegalStateException("No instantiated fragment for (" + next + ")");
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(FragmentStateManager fragmentStateManager) {
        Fragment e = fragmentStateManager.e();
        for (FragmentStateManager next : this.b.values()) {
            if (next != null) {
                Fragment e2 = next.e();
                if (e.mWho.equals(e2.mTargetWho)) {
                    e2.mTarget = e;
                    e2.mTargetWho = null;
                }
            }
        }
        this.b.put(e.mWho, (Object) null);
        String str = e.mTargetWho;
        if (str != null) {
            e.mTarget = b(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment) {
        synchronized (this.f697a) {
            this.f697a.remove(fragment);
        }
        fragment.mAdded = false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.b.clear();
    }

    /* access modifiers changed from: package-private */
    public ArrayList<FragmentState> e() {
        ArrayList<FragmentState> arrayList = new ArrayList<>(this.b.size());
        for (FragmentStateManager next : this.b.values()) {
            if (next != null) {
                Fragment e = next.e();
                FragmentState j = next.j();
                arrayList.add(j);
                if (FragmentManager.d(2)) {
                    Log.v("FragmentManager", "Saved state of " + e + ": " + j.m);
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<String> f() {
        synchronized (this.f697a) {
            if (this.f697a.isEmpty()) {
                return null;
            }
            ArrayList<String> arrayList = new ArrayList<>(this.f697a.size());
            Iterator<Fragment> it2 = this.f697a.iterator();
            while (it2.hasNext()) {
                Fragment next = it2.next();
                arrayList.add(next.mWho);
                if (FragmentManager.d(2)) {
                    Log.v("FragmentManager", "saveAllState: adding fragment (" + next.mWho + "): " + next);
                }
            }
            return arrayList;
        }
    }

    /* access modifiers changed from: package-private */
    public Fragment d(String str) {
        Fragment findFragmentByWho;
        for (FragmentStateManager next : this.b.values()) {
            if (next != null && (findFragmentByWho = next.e().findFragmentByWho(str)) != null) {
                return findFragmentByWho;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public List<Fragment> c() {
        ArrayList arrayList;
        if (this.f697a.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (this.f697a) {
            arrayList = new ArrayList(this.f697a);
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public void a(FragmentStateManager fragmentStateManager) {
        this.b.put(fragmentStateManager.e().mWho, fragmentStateManager);
    }

    /* access modifiers changed from: package-private */
    public FragmentStateManager e(String str) {
        return this.b.get(str);
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
        if (!this.f697a.contains(fragment)) {
            synchronized (this.f697a) {
                this.f697a.add(fragment);
            }
            fragment.mAdded = true;
            return;
        }
        throw new IllegalStateException("Fragment already added: " + fragment);
    }

    /* access modifiers changed from: package-private */
    public List<Fragment> b() {
        ArrayList arrayList = new ArrayList();
        for (FragmentStateManager next : this.b.values()) {
            if (next != null) {
                arrayList.add(next.e());
            } else {
                arrayList.add((Object) null);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public Fragment c(String str) {
        if (str != null) {
            for (int size = this.f697a.size() - 1; size >= 0; size--) {
                Fragment fragment = this.f697a.get(size);
                if (fragment != null && str.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (str == null) {
            return null;
        }
        for (FragmentStateManager next : this.b.values()) {
            if (next != null) {
                Fragment e = next.e();
                if (str.equals(e.mTag)) {
                    return e;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public Fragment b(int i) {
        for (int size = this.f697a.size() - 1; size >= 0; size--) {
            Fragment fragment = this.f697a.get(size);
            if (fragment != null && fragment.mFragmentId == i) {
                return fragment;
            }
        }
        for (FragmentStateManager next : this.b.values()) {
            if (next != null) {
                Fragment e = next.e();
                if (e.mFragmentId == i) {
                    return e;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        Iterator<Fragment> it2 = this.f697a.iterator();
        while (it2.hasNext()) {
            FragmentStateManager fragmentStateManager = this.b.get(it2.next().mWho);
            if (fragmentStateManager != null) {
                fragmentStateManager.a(i);
            }
        }
        for (FragmentStateManager next : this.b.values()) {
            if (next != null) {
                next.a(i);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Fragment b(String str) {
        FragmentStateManager fragmentStateManager = this.b.get(str);
        if (fragmentStateManager != null) {
            return fragmentStateManager.e();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b.values().removeAll(Collections.singleton((Object) null));
    }

    /* access modifiers changed from: package-private */
    public Fragment b(Fragment fragment) {
        ViewGroup viewGroup = fragment.mContainer;
        View view = fragment.mView;
        if (!(viewGroup == null || view == null)) {
            for (int indexOf = this.f697a.indexOf(fragment) - 1; indexOf >= 0; indexOf--) {
                Fragment fragment2 = this.f697a.get(indexOf);
                if (fragment2.mContainer == viewGroup && fragment2.mView != null) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        return this.b.containsKey(str);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String str2 = str + "    ";
        if (!this.b.isEmpty()) {
            printWriter.print(str);
            printWriter.print("Active Fragments:");
            for (FragmentStateManager next : this.b.values()) {
                printWriter.print(str);
                if (next != null) {
                    Fragment e = next.e();
                    printWriter.println(e);
                    e.dump(str2, fileDescriptor, printWriter, strArr);
                } else {
                    printWriter.println("null");
                }
            }
        }
        int size = this.f697a.size();
        if (size > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i = 0; i < size; i++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.println(this.f697a.get(i).toString());
            }
        }
    }
}
