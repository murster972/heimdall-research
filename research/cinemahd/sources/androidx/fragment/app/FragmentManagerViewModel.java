package androidx.fragment.app;

import android.util.Log;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

final class FragmentManagerViewModel extends ViewModel {
    private static final ViewModelProvider.Factory i = new ViewModelProvider.Factory() {
        public <T extends ViewModel> T a(Class<T> cls) {
            return new FragmentManagerViewModel(true);
        }
    };
    private final HashMap<String, Fragment> c = new HashMap<>();
    private final HashMap<String, FragmentManagerViewModel> d = new HashMap<>();
    private final HashMap<String, ViewModelStore> e = new HashMap<>();
    private final boolean f;
    private boolean g = false;
    private boolean h = false;

    FragmentManagerViewModel(boolean z) {
        this.f = z;
    }

    static FragmentManagerViewModel a(ViewModelStore viewModelStore) {
        return (FragmentManagerViewModel) new ViewModelProvider(viewModelStore, i).a(FragmentManagerViewModel.class);
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "onCleared called for " + this);
        }
        this.g = true;
    }

    /* access modifiers changed from: package-private */
    public Collection<Fragment> c() {
        return this.c.values();
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public boolean e(Fragment fragment) {
        return this.c.remove(fragment.mWho) != null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || FragmentManagerViewModel.class != obj.getClass()) {
            return false;
        }
        FragmentManagerViewModel fragmentManagerViewModel = (FragmentManagerViewModel) obj;
        if (!this.c.equals(fragmentManagerViewModel.c) || !this.d.equals(fragmentManagerViewModel.d) || !this.e.equals(fragmentManagerViewModel.e)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean f(Fragment fragment) {
        if (!this.c.containsKey(fragment.mWho)) {
            return true;
        }
        if (this.f) {
            return this.g;
        }
        return !this.h;
    }

    public int hashCode() {
        return (((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("FragmentManagerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} Fragments (");
        Iterator<Fragment> it2 = this.c.values().iterator();
        while (it2.hasNext()) {
            sb.append(it2.next());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") Child Non Config (");
        Iterator<String> it3 = this.d.keySet().iterator();
        while (it3.hasNext()) {
            sb.append(it3.next());
            if (it3.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") ViewModelStores (");
        Iterator<String> it4 = this.e.keySet().iterator();
        while (it4.hasNext()) {
            sb.append(it4.next());
            if (it4.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public FragmentManagerViewModel c(Fragment fragment) {
        FragmentManagerViewModel fragmentManagerViewModel = this.d.get(fragment.mWho);
        if (fragmentManagerViewModel != null) {
            return fragmentManagerViewModel;
        }
        FragmentManagerViewModel fragmentManagerViewModel2 = new FragmentManagerViewModel(this.f);
        this.d.put(fragment.mWho, fragmentManagerViewModel2);
        return fragmentManagerViewModel2;
    }

    /* access modifiers changed from: package-private */
    public ViewModelStore d(Fragment fragment) {
        ViewModelStore viewModelStore = this.e.get(fragment.mWho);
        if (viewModelStore != null) {
            return viewModelStore;
        }
        ViewModelStore viewModelStore2 = new ViewModelStore();
        this.e.put(fragment.mWho, viewModelStore2);
        return viewModelStore2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Fragment fragment) {
        if (this.c.containsKey(fragment.mWho)) {
            return false;
        }
        this.c.put(fragment.mWho, fragment);
        return true;
    }

    /* access modifiers changed from: package-private */
    public Fragment b(String str) {
        return this.c.get(str);
    }

    /* access modifiers changed from: package-private */
    public void b(Fragment fragment) {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "Clearing non-config state for " + fragment);
        }
        FragmentManagerViewModel fragmentManagerViewModel = this.d.get(fragment.mWho);
        if (fragmentManagerViewModel != null) {
            fragmentManagerViewModel.b();
            this.d.remove(fragment.mWho);
        }
        ViewModelStore viewModelStore = this.e.get(fragment.mWho);
        if (viewModelStore != null) {
            viewModelStore.a();
            this.e.remove(fragment.mWho);
        }
    }
}
