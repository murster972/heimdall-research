package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import androidx.core.app.ActivityCompat;
import androidx.core.util.Preconditions;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class FragmentHostCallback<E> extends FragmentContainer {

    /* renamed from: a  reason: collision with root package name */
    private final Activity f682a;
    private final Context b;
    private final Handler c;
    final FragmentManager d;

    FragmentHostCallback(FragmentActivity fragmentActivity) {
        this(fragmentActivity, fragmentActivity, new Handler(), 0);
    }

    public View a(int i) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
    }

    public void a(Fragment fragment, @SuppressLint({"UnknownNullness"}) Intent intent, int i, Bundle bundle) {
        if (i == -1) {
            this.b.startActivity(intent);
            return;
        }
        throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
    }

    public void a(Fragment fragment, String[] strArr, int i) {
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public boolean a() {
        return true;
    }

    public boolean a(String str) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public Activity b() {
        return this.f682a;
    }

    public boolean b(Fragment fragment) {
        return true;
    }

    /* access modifiers changed from: package-private */
    public Context c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public Handler d() {
        return this.c;
    }

    public abstract E e();

    public LayoutInflater f() {
        return LayoutInflater.from(this.b);
    }

    public void g() {
    }

    FragmentHostCallback(Activity activity, Context context, Handler handler, int i) {
        this.d = new FragmentManagerImpl();
        this.f682a = activity;
        Preconditions.a(context, "context == null");
        this.b = context;
        Preconditions.a(handler, "handler == null");
        this.c = handler;
    }

    public void a(Fragment fragment, @SuppressLint({"UnknownNullness"}) IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) throws IntentSender.SendIntentException {
        if (i == -1) {
            ActivityCompat.a(this.f682a, intentSender, i, intent, i2, i3, i4, bundle);
        } else {
            throw new IllegalStateException("Starting intent sender with a requestCode requires a FragmentActivity host");
        }
    }
}
