package androidx.fragment.app;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import androidx.core.os.CancellationSignal;
import androidx.core.view.OneShotPreDrawListener;
import androidx.fragment.R$anim;
import androidx.fragment.R$id;
import androidx.fragment.app.FragmentTransition;

class FragmentAnim {
    private FragmentAnim() {
    }

    static AnimationOrAnimator a(Context context, FragmentContainer fragmentContainer, Fragment fragment, boolean z) {
        int a2;
        int nextTransition = fragment.getNextTransition();
        int nextAnim = fragment.getNextAnim();
        boolean z2 = false;
        fragment.setNextAnim(0);
        View a3 = fragmentContainer.a(fragment.mContainerId);
        if (!(a3 == null || a3.getTag(R$id.visible_removing_fragment_view_tag) == null)) {
            a3.setTag(R$id.visible_removing_fragment_view_tag, (Object) null);
        }
        ViewGroup viewGroup = fragment.mContainer;
        if (viewGroup != null && viewGroup.getLayoutTransition() != null) {
            return null;
        }
        Animation onCreateAnimation = fragment.onCreateAnimation(nextTransition, z, nextAnim);
        if (onCreateAnimation != null) {
            return new AnimationOrAnimator(onCreateAnimation);
        }
        Animator onCreateAnimator = fragment.onCreateAnimator(nextTransition, z, nextAnim);
        if (onCreateAnimator != null) {
            return new AnimationOrAnimator(onCreateAnimator);
        }
        if (nextAnim != 0) {
            boolean equals = "anim".equals(context.getResources().getResourceTypeName(nextAnim));
            if (equals) {
                try {
                    Animation loadAnimation = AnimationUtils.loadAnimation(context, nextAnim);
                    if (loadAnimation != null) {
                        return new AnimationOrAnimator(loadAnimation);
                    }
                    z2 = true;
                } catch (Resources.NotFoundException e) {
                    throw e;
                } catch (RuntimeException unused) {
                }
            }
            if (!z2) {
                try {
                    Animator loadAnimator = AnimatorInflater.loadAnimator(context, nextAnim);
                    if (loadAnimator != null) {
                        return new AnimationOrAnimator(loadAnimator);
                    }
                } catch (RuntimeException e2) {
                    if (!equals) {
                        Animation loadAnimation2 = AnimationUtils.loadAnimation(context, nextAnim);
                        if (loadAnimation2 != null) {
                            return new AnimationOrAnimator(loadAnimation2);
                        }
                    } else {
                        throw e2;
                    }
                }
            }
        }
        if (nextTransition != 0 && (a2 = a(nextTransition, z)) >= 0) {
            return new AnimationOrAnimator(AnimationUtils.loadAnimation(context, a2));
        }
        return null;
    }

    static class AnimationOrAnimator {

        /* renamed from: a  reason: collision with root package name */
        public final Animation f677a;
        public final Animator b;

        AnimationOrAnimator(Animation animation) {
            this.f677a = animation;
            this.b = null;
            if (animation == null) {
                throw new IllegalStateException("Animation cannot be null");
            }
        }

        AnimationOrAnimator(Animator animator) {
            this.f677a = null;
            this.b = animator;
            if (animator == null) {
                throw new IllegalStateException("Animator cannot be null");
            }
        }
    }

    private static class EndViewTransitionAnimation extends AnimationSet implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final ViewGroup f678a;
        private final View b;
        private boolean c;
        private boolean d;
        private boolean e = true;

        EndViewTransitionAnimation(Animation animation, ViewGroup viewGroup, View view) {
            super(false);
            this.f678a = viewGroup;
            this.b = view;
            addAnimation(animation);
            this.f678a.post(this);
        }

        public boolean getTransformation(long j, Transformation transformation) {
            this.e = true;
            if (this.c) {
                return !this.d;
            }
            if (!super.getTransformation(j, transformation)) {
                this.c = true;
                OneShotPreDrawListener.a(this.f678a, this);
            }
            return true;
        }

        public void run() {
            if (this.c || !this.e) {
                this.f678a.endViewTransition(this.b);
                this.d = true;
                return;
            }
            this.e = false;
            this.f678a.post(this);
        }

        public boolean getTransformation(long j, Transformation transformation, float f) {
            this.e = true;
            if (this.c) {
                return !this.d;
            }
            if (!super.getTransformation(j, transformation, f)) {
                this.c = true;
                OneShotPreDrawListener.a(this.f678a, this);
            }
            return true;
        }
    }

    static void a(final Fragment fragment, AnimationOrAnimator animationOrAnimator, final FragmentTransition.Callback callback) {
        final View view = fragment.mView;
        final ViewGroup viewGroup = fragment.mContainer;
        viewGroup.startViewTransition(view);
        final CancellationSignal cancellationSignal = new CancellationSignal();
        cancellationSignal.a(new CancellationSignal.OnCancelListener() {
            public void a() {
                if (fragment.getAnimatingAway() != null) {
                    View animatingAway = fragment.getAnimatingAway();
                    fragment.setAnimatingAway((View) null);
                    animatingAway.clearAnimation();
                }
                fragment.setAnimator((Animator) null);
            }
        });
        callback.b(fragment, cancellationSignal);
        Animation animation = animationOrAnimator.f677a;
        if (animation != null) {
            EndViewTransitionAnimation endViewTransitionAnimation = new EndViewTransitionAnimation(animation, viewGroup, view);
            fragment.setAnimatingAway(fragment.mView);
            endViewTransitionAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    viewGroup.post(new Runnable() {
                        public void run() {
                            if (fragment.getAnimatingAway() != null) {
                                fragment.setAnimatingAway((View) null);
                                AnonymousClass2 r0 = AnonymousClass2.this;
                                callback.a(fragment, cancellationSignal);
                            }
                        }
                    });
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            fragment.mView.startAnimation(endViewTransitionAnimation);
            return;
        }
        Animator animator = animationOrAnimator.b;
        fragment.setAnimator(animator);
        final Fragment fragment2 = fragment;
        final FragmentTransition.Callback callback2 = callback;
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                viewGroup.endViewTransition(view);
                Animator animator2 = fragment2.getAnimator();
                fragment2.setAnimator((Animator) null);
                if (animator2 != null && viewGroup.indexOfChild(view) < 0) {
                    callback2.a(fragment2, cancellationSignal);
                }
            }
        });
        animator.setTarget(fragment.mView);
        animator.start();
    }

    private static int a(int i, boolean z) {
        if (i == 4097) {
            return z ? R$anim.fragment_open_enter : R$anim.fragment_open_exit;
        }
        if (i == 4099) {
            return z ? R$anim.fragment_fade_enter : R$anim.fragment_fade_exit;
        }
        if (i != 8194) {
            return -1;
        }
        return z ? R$anim.fragment_close_enter : R$anim.fragment_close_exit;
    }
}
