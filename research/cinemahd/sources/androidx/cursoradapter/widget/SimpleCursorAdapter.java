package androidx.cursoradapter.widget;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SimpleCursorAdapter extends ResourceCursorAdapter {
    protected int[] m;
    protected int[] n;
    private int o = -1;
    private CursorToStringConverter p;
    private ViewBinder q;
    String[] r;

    public interface CursorToStringConverter {
        CharSequence a(Cursor cursor);
    }

    public interface ViewBinder {
        boolean a(View view, Cursor cursor, int i);
    }

    public SimpleCursorAdapter(Context context, int i, Cursor cursor, String[] strArr, int[] iArr, int i2) {
        super(context, i, cursor, i2);
        this.n = iArr;
        this.r = strArr;
        a(cursor, strArr);
    }

    public void a(View view, Context context, Cursor cursor) {
        ViewBinder viewBinder = this.q;
        int[] iArr = this.n;
        int length = iArr.length;
        int[] iArr2 = this.m;
        for (int i = 0; i < length; i++) {
            View findViewById = view.findViewById(iArr[i]);
            if (findViewById != null) {
                if (viewBinder != null ? viewBinder.a(findViewById, cursor, iArr2[i]) : false) {
                    continue;
                } else {
                    String string = cursor.getString(iArr2[i]);
                    if (string == null) {
                        string = "";
                    }
                    if (findViewById instanceof TextView) {
                        a((TextView) findViewById, string);
                    } else if (findViewById instanceof ImageView) {
                        a((ImageView) findViewById, string);
                    } else {
                        throw new IllegalStateException(findViewById.getClass().getName() + " is not a " + " view that can be bounds by this SimpleCursorAdapter");
                    }
                }
            }
        }
    }

    public Cursor c(Cursor cursor) {
        a(cursor, this.r);
        return super.c(cursor);
    }

    public void a(ImageView imageView, String str) {
        try {
            imageView.setImageResource(Integer.parseInt(str));
        } catch (NumberFormatException unused) {
            imageView.setImageURI(Uri.parse(str));
        }
    }

    public void a(TextView textView, String str) {
        textView.setText(str);
    }

    public CharSequence a(Cursor cursor) {
        CursorToStringConverter cursorToStringConverter = this.p;
        if (cursorToStringConverter != null) {
            return cursorToStringConverter.a(cursor);
        }
        int i = this.o;
        if (i > -1) {
            return cursor.getString(i);
        }
        return super.a(cursor);
    }

    private void a(Cursor cursor, String[] strArr) {
        if (cursor != null) {
            int length = strArr.length;
            int[] iArr = this.m;
            if (iArr == null || iArr.length != length) {
                this.m = new int[length];
            }
            for (int i = 0; i < length; i++) {
                this.m[i] = cursor.getColumnIndexOrThrow(strArr[i]);
            }
            return;
        }
        this.m = null;
    }
}
