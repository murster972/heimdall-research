package androidx.versionedparcelable;

import android.os.Parcelable;
import androidx.collection.ArrayMap;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class VersionedParcel {

    /* renamed from: a  reason: collision with root package name */
    protected final ArrayMap<String, Method> f1127a;
    protected final ArrayMap<String, Method> b;
    protected final ArrayMap<String, Class> c;

    public static class ParcelException extends RuntimeException {
    }

    public VersionedParcel(ArrayMap<String, Method> arrayMap, ArrayMap<String, Method> arrayMap2, ArrayMap<String, Class> arrayMap3) {
        this.f1127a = arrayMap;
        this.b = arrayMap2;
        this.c = arrayMap3;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void a(Parcelable parcelable);

    /* access modifiers changed from: protected */
    public abstract void a(CharSequence charSequence);

    /* access modifiers changed from: protected */
    public abstract void a(String str);

    /* access modifiers changed from: protected */
    public abstract void a(boolean z);

    public void a(boolean z, boolean z2) {
    }

    /* access modifiers changed from: protected */
    public abstract void a(byte[] bArr);

    /* access modifiers changed from: protected */
    public abstract boolean a(int i);

    public boolean a(boolean z, int i) {
        if (!a(i)) {
            return z;
        }
        return d();
    }

    /* access modifiers changed from: protected */
    public abstract VersionedParcel b();

    /* access modifiers changed from: protected */
    public abstract void b(int i);

    public void b(boolean z, int i) {
        b(i);
        a(z);
    }

    /* access modifiers changed from: protected */
    public abstract void c(int i);

    public boolean c() {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract boolean d();

    /* access modifiers changed from: protected */
    public abstract byte[] e();

    /* access modifiers changed from: protected */
    public abstract CharSequence f();

    /* access modifiers changed from: protected */
    public abstract int g();

    /* access modifiers changed from: protected */
    public abstract <T extends Parcelable> T h();

    /* access modifiers changed from: protected */
    public abstract String i();

    /* access modifiers changed from: protected */
    public <T extends VersionedParcelable> T j() {
        String i = i();
        if (i == null) {
            return null;
        }
        return a(i, b());
    }

    public int a(int i, int i2) {
        if (!a(i2)) {
            return i;
        }
        return g();
    }

    public void b(byte[] bArr, int i) {
        b(i);
        a(bArr);
    }

    public String a(String str, int i) {
        if (!a(i)) {
            return str;
        }
        return i();
    }

    public void b(CharSequence charSequence, int i) {
        b(i);
        a(charSequence);
    }

    public byte[] a(byte[] bArr, int i) {
        if (!a(i)) {
            return bArr;
        }
        return e();
    }

    public void b(int i, int i2) {
        b(i2);
        c(i);
    }

    public <T extends Parcelable> T a(T t, int i) {
        if (!a(i)) {
            return t;
        }
        return h();
    }

    public void b(String str, int i) {
        b(i);
        a(str);
    }

    public CharSequence a(CharSequence charSequence, int i) {
        if (!a(i)) {
            return charSequence;
        }
        return f();
    }

    public void b(Parcelable parcelable, int i) {
        b(i);
        a(parcelable);
    }

    /* access modifiers changed from: protected */
    public void a(VersionedParcelable versionedParcelable) {
        if (versionedParcelable == null) {
            a((String) null);
            return;
        }
        b(versionedParcelable);
        VersionedParcel b2 = b();
        a(versionedParcelable, b2);
        b2.a();
    }

    public void b(VersionedParcelable versionedParcelable, int i) {
        b(i);
        a(versionedParcelable);
    }

    private void b(VersionedParcelable versionedParcelable) {
        try {
            a(a((Class<? extends VersionedParcelable>) versionedParcelable.getClass()).getName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(versionedParcelable.getClass().getSimpleName() + " does not have a Parcelizer", e);
        }
    }

    private Method b(String str) throws IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Class<VersionedParcel> cls = VersionedParcel.class;
        Method method = this.f1127a.get(str);
        if (method != null) {
            return method;
        }
        Method declaredMethod = Class.forName(str, true, cls.getClassLoader()).getDeclaredMethod("read", new Class[]{cls});
        this.f1127a.put(str, declaredMethod);
        return declaredMethod;
    }

    public <T extends VersionedParcelable> T a(T t, int i) {
        if (!a(i)) {
            return t;
        }
        return j();
    }

    /* access modifiers changed from: protected */
    public <T extends VersionedParcelable> T a(String str, VersionedParcel versionedParcel) {
        try {
            return (VersionedParcelable) b(str).invoke((Object) null, new Object[]{versionedParcel});
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    private Method b(Class cls) throws IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Method method = this.b.get(cls.getName());
        if (method != null) {
            return method;
        }
        Method declaredMethod = a((Class<? extends VersionedParcelable>) cls).getDeclaredMethod("write", new Class[]{cls, VersionedParcel.class});
        this.b.put(cls.getName(), declaredMethod);
        return declaredMethod;
    }

    /* access modifiers changed from: protected */
    public <T extends VersionedParcelable> void a(T t, VersionedParcel versionedParcel) {
        try {
            b((Class) t.getClass()).invoke((Object) null, new Object[]{t, versionedParcel});
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    private Class a(Class<? extends VersionedParcelable> cls) throws ClassNotFoundException {
        Class cls2 = this.c.get(cls.getName());
        if (cls2 != null) {
            return cls2;
        }
        Class<?> cls3 = Class.forName(String.format("%s.%sParcelizer", new Object[]{cls.getPackage().getName(), cls.getSimpleName()}), false, cls.getClassLoader());
        this.c.put(cls.getName(), cls3);
        return cls3;
    }
}
