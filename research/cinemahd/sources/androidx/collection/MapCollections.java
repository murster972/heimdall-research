package androidx.collection;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

abstract class MapCollections<K, V> {

    /* renamed from: a  reason: collision with root package name */
    MapCollections<K, V>.EntrySet f474a;
    MapCollections<K, V>.KeySet b;
    MapCollections<K, V>.ValuesCollection c;

    final class ArrayIterator<T> implements Iterator<T> {

        /* renamed from: a  reason: collision with root package name */
        final int f475a;
        int b;
        int c;
        boolean d = false;

        ArrayIterator(int i) {
            this.f475a = i;
            this.b = MapCollections.this.c();
        }

        public boolean hasNext() {
            return this.c < this.b;
        }

        public T next() {
            if (hasNext()) {
                T a2 = MapCollections.this.a(this.c, this.f475a);
                this.c++;
                this.d = true;
                return a2;
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            if (this.d) {
                this.c--;
                this.b--;
                this.d = false;
                MapCollections.this.a(this.c);
                return;
            }
            throw new IllegalStateException();
        }
    }

    final class EntrySet implements Set<Map.Entry<K, V>> {
        EntrySet() {
        }

        public boolean a(Map.Entry<K, V> entry) {
            throw new UnsupportedOperationException();
        }

        public /* bridge */ /* synthetic */ boolean add(Object obj) {
            a((Map.Entry) obj);
            throw null;
        }

        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            int c = MapCollections.this.c();
            for (Map.Entry entry : collection) {
                MapCollections.this.a(entry.getKey(), entry.getValue());
            }
            return c != MapCollections.this.c();
        }

        public void clear() {
            MapCollections.this.a();
        }

        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            int a2 = MapCollections.this.a(entry.getKey());
            if (a2 < 0) {
                return false;
            }
            return ContainerHelpers.a(MapCollections.this.a(a2, 1), entry.getValue());
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object obj) {
            return MapCollections.a(this, obj);
        }

        public int hashCode() {
            int i;
            int i2;
            int i3 = 0;
            for (int c = MapCollections.this.c() - 1; c >= 0; c--) {
                Object a2 = MapCollections.this.a(c, 0);
                Object a3 = MapCollections.this.a(c, 1);
                if (a2 == null) {
                    i = 0;
                } else {
                    i = a2.hashCode();
                }
                if (a3 == null) {
                    i2 = 0;
                } else {
                    i2 = a3.hashCode();
                }
                i3 += i ^ i2;
            }
            return i3;
        }

        public boolean isEmpty() {
            return MapCollections.this.c() == 0;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new MapIterator();
        }

        public boolean remove(Object obj) {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public int size() {
            return MapCollections.this.c();
        }

        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }

        public <T> T[] toArray(T[] tArr) {
            throw new UnsupportedOperationException();
        }
    }

    final class KeySet implements Set<K> {
        KeySet() {
        }

        public boolean add(K k) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends K> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            MapCollections.this.a();
        }

        public boolean contains(Object obj) {
            return MapCollections.this.a(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            return MapCollections.a(MapCollections.this.b(), collection);
        }

        public boolean equals(Object obj) {
            return MapCollections.a(this, obj);
        }

        public int hashCode() {
            int i;
            int i2 = 0;
            for (int c = MapCollections.this.c() - 1; c >= 0; c--) {
                Object a2 = MapCollections.this.a(c, 0);
                if (a2 == null) {
                    i = 0;
                } else {
                    i = a2.hashCode();
                }
                i2 += i;
            }
            return i2;
        }

        public boolean isEmpty() {
            return MapCollections.this.c() == 0;
        }

        public Iterator<K> iterator() {
            return new ArrayIterator(0);
        }

        public boolean remove(Object obj) {
            int a2 = MapCollections.this.a(obj);
            if (a2 < 0) {
                return false;
            }
            MapCollections.this.a(a2);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            return MapCollections.b(MapCollections.this.b(), collection);
        }

        public boolean retainAll(Collection<?> collection) {
            return MapCollections.c(MapCollections.this.b(), collection);
        }

        public int size() {
            return MapCollections.this.c();
        }

        public Object[] toArray() {
            return MapCollections.this.b(0);
        }

        public <T> T[] toArray(T[] tArr) {
            return MapCollections.this.a(tArr, 0);
        }
    }

    final class MapIterator implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {

        /* renamed from: a  reason: collision with root package name */
        int f478a;
        int b;
        boolean c = false;

        MapIterator() {
            this.f478a = MapCollections.this.c() - 1;
            this.b = -1;
        }

        public boolean equals(Object obj) {
            if (!this.c) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(obj instanceof Map.Entry)) {
                return false;
            } else {
                Map.Entry entry = (Map.Entry) obj;
                if (!ContainerHelpers.a(entry.getKey(), MapCollections.this.a(this.b, 0)) || !ContainerHelpers.a(entry.getValue(), MapCollections.this.a(this.b, 1))) {
                    return false;
                }
                return true;
            }
        }

        public K getKey() {
            if (this.c) {
                return MapCollections.this.a(this.b, 0);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public V getValue() {
            if (this.c) {
                return MapCollections.this.a(this.b, 1);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public boolean hasNext() {
            return this.b < this.f478a;
        }

        public int hashCode() {
            int i;
            if (this.c) {
                int i2 = 0;
                Object a2 = MapCollections.this.a(this.b, 0);
                Object a3 = MapCollections.this.a(this.b, 1);
                if (a2 == null) {
                    i = 0;
                } else {
                    i = a2.hashCode();
                }
                if (a3 != null) {
                    i2 = a3.hashCode();
                }
                return i ^ i2;
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public void remove() {
            if (this.c) {
                MapCollections.this.a(this.b);
                this.b--;
                this.f478a--;
                this.c = false;
                return;
            }
            throw new IllegalStateException();
        }

        public V setValue(V v) {
            if (this.c) {
                return MapCollections.this.a(this.b, v);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public String toString() {
            return getKey() + "=" + getValue();
        }

        public Map.Entry<K, V> next() {
            if (hasNext()) {
                this.b++;
                this.c = true;
                return this;
            }
            throw new NoSuchElementException();
        }
    }

    final class ValuesCollection implements Collection<V> {
        ValuesCollection() {
        }

        public boolean add(V v) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends V> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            MapCollections.this.a();
        }

        public boolean contains(Object obj) {
            return MapCollections.this.b(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return MapCollections.this.c() == 0;
        }

        public Iterator<V> iterator() {
            return new ArrayIterator(1);
        }

        public boolean remove(Object obj) {
            int b = MapCollections.this.b(obj);
            if (b < 0) {
                return false;
            }
            MapCollections.this.a(b);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            int c = MapCollections.this.c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (collection.contains(MapCollections.this.a(i, 1))) {
                    MapCollections.this.a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        public boolean retainAll(Collection<?> collection) {
            int c = MapCollections.this.c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (!collection.contains(MapCollections.this.a(i, 1))) {
                    MapCollections.this.a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        public int size() {
            return MapCollections.this.c();
        }

        public Object[] toArray() {
            return MapCollections.this.b(1);
        }

        public <T> T[] toArray(T[] tArr) {
            return MapCollections.this.a(tArr, 1);
        }
    }

    MapCollections() {
    }

    public static <K, V> boolean a(Map<K, V> map, Collection<?> collection) {
        for (Object containsKey : collection) {
            if (!map.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    public static <K, V> boolean b(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        for (Object remove : collection) {
            map.remove(remove);
        }
        return size != map.size();
    }

    public static <K, V> boolean c(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<K> it2 = map.keySet().iterator();
        while (it2.hasNext()) {
            if (!collection.contains(it2.next())) {
                it2.remove();
            }
        }
        return size != map.size();
    }

    /* access modifiers changed from: protected */
    public abstract int a(Object obj);

    /* access modifiers changed from: protected */
    public abstract Object a(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract V a(int i, V v);

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void a(int i);

    /* access modifiers changed from: protected */
    public abstract void a(K k, V v);

    /* access modifiers changed from: protected */
    public abstract int b(Object obj);

    /* access modifiers changed from: protected */
    public abstract Map<K, V> b();

    /* access modifiers changed from: protected */
    public abstract int c();

    public Set<Map.Entry<K, V>> d() {
        if (this.f474a == null) {
            this.f474a = new EntrySet();
        }
        return this.f474a;
    }

    public Set<K> e() {
        if (this.b == null) {
            this.b = new KeySet();
        }
        return this.b;
    }

    public Collection<V> f() {
        if (this.c == null) {
            this.c = new ValuesCollection();
        }
        return this.c;
    }

    public <T> T[] a(T[] tArr, int i) {
        int c2 = c();
        if (tArr.length < c2) {
            tArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), c2);
        }
        for (int i2 = 0; i2 < c2; i2++) {
            tArr[i2] = a(i2, i);
        }
        if (tArr.length > c2) {
            tArr[c2] = null;
        }
        return tArr;
    }

    public Object[] b(int i) {
        int c2 = c();
        Object[] objArr = new Object[c2];
        for (int i2 = 0; i2 < c2; i2++) {
            objArr[i2] = a(i2, i);
        }
        return objArr;
    }

    public static <T> boolean a(Set<T> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    return false;
                }
                return true;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }
}
