package androidx.collection;

public class SparseArrayCompat<E> implements Cloneable {
    private static final Object e = new Object();

    /* renamed from: a  reason: collision with root package name */
    private boolean f481a;
    private int[] b;
    private Object[] c;
    private int d;

    public SparseArrayCompat() {
        this(10);
    }

    private void c() {
        int i = this.d;
        int[] iArr = this.b;
        Object[] objArr = this.c;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != e) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.f481a = false;
        this.d = i2;
    }

    public E a(int i) {
        return b(i, (Object) null);
    }

    public E b(int i, E e2) {
        int a2 = ContainerHelpers.a(this.b, this.d, i);
        if (a2 >= 0) {
            E[] eArr = this.c;
            if (eArr[a2] != e) {
                return eArr[a2];
            }
        }
        return e2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000a, code lost:
        r0 = r3.c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(int r4) {
        /*
            r3 = this;
            int[] r0 = r3.b
            int r1 = r3.d
            int r4 = androidx.collection.ContainerHelpers.a((int[]) r0, (int) r1, (int) r4)
            if (r4 < 0) goto L_0x0017
            java.lang.Object[] r0 = r3.c
            r1 = r0[r4]
            java.lang.Object r2 = e
            if (r1 == r2) goto L_0x0017
            r0[r4] = r2
            r4 = 1
            r3.f481a = r4
        L_0x0017:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.collection.SparseArrayCompat.d(int):void");
    }

    public E e(int i) {
        if (this.f481a) {
            c();
        }
        return this.c[i];
    }

    public String toString() {
        if (b() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.d * 28);
        sb.append('{');
        for (int i = 0; i < this.d; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(c(i));
            sb.append('=');
            Object e2 = e(i);
            if (e2 != this) {
                sb.append(e2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public SparseArrayCompat(int i) {
        this.f481a = false;
        if (i == 0) {
            this.b = ContainerHelpers.f472a;
            this.c = ContainerHelpers.c;
            return;
        }
        int b2 = ContainerHelpers.b(i);
        this.b = new int[b2];
        this.c = new Object[b2];
    }

    public int a(E e2) {
        if (this.f481a) {
            c();
        }
        for (int i = 0; i < this.d; i++) {
            if (this.c[i] == e2) {
                return i;
            }
        }
        return -1;
    }

    public SparseArrayCompat<E> clone() {
        try {
            SparseArrayCompat<E> sparseArrayCompat = (SparseArrayCompat) super.clone();
            sparseArrayCompat.b = (int[]) this.b.clone();
            sparseArrayCompat.c = (Object[]) this.c.clone();
            return sparseArrayCompat;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    public int b() {
        if (this.f481a) {
            c();
        }
        return this.d;
    }

    public void a() {
        int i = this.d;
        Object[] objArr = this.c;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.d = 0;
        this.f481a = false;
    }

    public int b(int i) {
        if (this.f481a) {
            c();
        }
        return ContainerHelpers.a(this.b, this.d, i);
    }

    public void a(int i, E e2) {
        int i2 = this.d;
        if (i2 == 0 || i > this.b[i2 - 1]) {
            if (this.f481a && this.d >= this.b.length) {
                c();
            }
            int i3 = this.d;
            if (i3 >= this.b.length) {
                int b2 = ContainerHelpers.b(i3 + 1);
                int[] iArr = new int[b2];
                Object[] objArr = new Object[b2];
                int[] iArr2 = this.b;
                System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
                Object[] objArr2 = this.c;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.b = iArr;
                this.c = objArr;
            }
            this.b[i3] = i;
            this.c[i3] = e2;
            this.d = i3 + 1;
            return;
        }
        c(i, e2);
    }

    public void c(int i, E e2) {
        int a2 = ContainerHelpers.a(this.b, this.d, i);
        if (a2 >= 0) {
            this.c[a2] = e2;
            return;
        }
        int i2 = ~a2;
        if (i2 < this.d) {
            Object[] objArr = this.c;
            if (objArr[i2] == e) {
                this.b[i2] = i;
                objArr[i2] = e2;
                return;
            }
        }
        if (this.f481a && this.d >= this.b.length) {
            c();
            i2 = ~ContainerHelpers.a(this.b, this.d, i);
        }
        int i3 = this.d;
        if (i3 >= this.b.length) {
            int b2 = ContainerHelpers.b(i3 + 1);
            int[] iArr = new int[b2];
            Object[] objArr2 = new Object[b2];
            int[] iArr2 = this.b;
            System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
            Object[] objArr3 = this.c;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.b = iArr;
            this.c = objArr2;
        }
        int i4 = this.d;
        if (i4 - i2 != 0) {
            int[] iArr3 = this.b;
            int i5 = i2 + 1;
            System.arraycopy(iArr3, i2, iArr3, i5, i4 - i2);
            Object[] objArr4 = this.c;
            System.arraycopy(objArr4, i2, objArr4, i5, this.d - i2);
        }
        this.b[i2] = i;
        this.c[i2] = e2;
        this.d++;
    }

    public int c(int i) {
        if (this.f481a) {
            c();
        }
        return this.b[i];
    }
}
