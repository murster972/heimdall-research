package androidx.collection;

public class LongSparseArray<E> implements Cloneable {
    private static final Object e = new Object();

    /* renamed from: a  reason: collision with root package name */
    private boolean f473a;
    private long[] b;
    private Object[] c;
    private int d;

    public LongSparseArray() {
        this(10);
    }

    private void c() {
        int i = this.d;
        long[] jArr = this.b;
        Object[] objArr = this.c;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != e) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.f473a = false;
        this.d = i2;
    }

    @Deprecated
    public void a(long j) {
        d(j);
    }

    public E b(long j) {
        return b(j, (Object) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000a, code lost:
        r4 = r2.c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(long r3) {
        /*
            r2 = this;
            long[] r0 = r2.b
            int r1 = r2.d
            int r3 = androidx.collection.ContainerHelpers.a((long[]) r0, (int) r1, (long) r3)
            if (r3 < 0) goto L_0x0017
            java.lang.Object[] r4 = r2.c
            r0 = r4[r3]
            java.lang.Object r1 = e
            if (r0 == r1) goto L_0x0017
            r4[r3] = r1
            r3 = 1
            r2.f473a = r3
        L_0x0017:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.collection.LongSparseArray.d(long):void");
    }

    public String toString() {
        if (b() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.d * 28);
        sb.append('{');
        for (int i = 0; i < this.d; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(a(i));
            sb.append('=');
            Object c2 = c(i);
            if (c2 != this) {
                sb.append(c2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public LongSparseArray(int i) {
        this.f473a = false;
        if (i == 0) {
            this.b = ContainerHelpers.b;
            this.c = ContainerHelpers.c;
            return;
        }
        int c2 = ContainerHelpers.c(i);
        this.b = new long[c2];
        this.c = new Object[c2];
    }

    public long a(int i) {
        if (this.f473a) {
            c();
        }
        return this.b[i];
    }

    public E b(long j, E e2) {
        int a2 = ContainerHelpers.a(this.b, this.d, j);
        if (a2 >= 0) {
            E[] eArr = this.c;
            if (eArr[a2] != e) {
                return eArr[a2];
            }
        }
        return e2;
    }

    public LongSparseArray<E> clone() {
        try {
            LongSparseArray<E> longSparseArray = (LongSparseArray) super.clone();
            longSparseArray.b = (long[]) this.b.clone();
            longSparseArray.c = (Object[]) this.c.clone();
            return longSparseArray;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    public void a() {
        int i = this.d;
        Object[] objArr = this.c;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.d = 0;
        this.f473a = false;
    }

    public void b(int i) {
        Object[] objArr = this.c;
        Object obj = objArr[i];
        Object obj2 = e;
        if (obj != obj2) {
            objArr[i] = obj2;
            this.f473a = true;
        }
    }

    public int b() {
        if (this.f473a) {
            c();
        }
        return this.d;
    }

    public void a(long j, E e2) {
        int i = this.d;
        if (i == 0 || j > this.b[i - 1]) {
            if (this.f473a && this.d >= this.b.length) {
                c();
            }
            int i2 = this.d;
            if (i2 >= this.b.length) {
                int c2 = ContainerHelpers.c(i2 + 1);
                long[] jArr = new long[c2];
                Object[] objArr = new Object[c2];
                long[] jArr2 = this.b;
                System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
                Object[] objArr2 = this.c;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.b = jArr;
                this.c = objArr;
            }
            this.b[i2] = j;
            this.c[i2] = e2;
            this.d = i2 + 1;
            return;
        }
        c(j, e2);
    }

    public void c(long j, E e2) {
        int a2 = ContainerHelpers.a(this.b, this.d, j);
        if (a2 >= 0) {
            this.c[a2] = e2;
            return;
        }
        int i = ~a2;
        if (i < this.d) {
            Object[] objArr = this.c;
            if (objArr[i] == e) {
                this.b[i] = j;
                objArr[i] = e2;
                return;
            }
        }
        if (this.f473a && this.d >= this.b.length) {
            c();
            i = ~ContainerHelpers.a(this.b, this.d, j);
        }
        int i2 = this.d;
        if (i2 >= this.b.length) {
            int c2 = ContainerHelpers.c(i2 + 1);
            long[] jArr = new long[c2];
            Object[] objArr2 = new Object[c2];
            long[] jArr2 = this.b;
            System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            Object[] objArr3 = this.c;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.b = jArr;
            this.c = objArr2;
        }
        int i3 = this.d;
        if (i3 - i != 0) {
            long[] jArr3 = this.b;
            int i4 = i + 1;
            System.arraycopy(jArr3, i, jArr3, i4, i3 - i);
            Object[] objArr4 = this.c;
            System.arraycopy(objArr4, i, objArr4, i4, this.d - i);
        }
        this.b[i] = j;
        this.c[i] = e2;
        this.d++;
    }

    public E c(int i) {
        if (this.f473a) {
            c();
        }
        return this.c[i];
    }

    public int c(long j) {
        if (this.f473a) {
            c();
        }
        return ContainerHelpers.a(this.b, this.d, j);
    }
}
