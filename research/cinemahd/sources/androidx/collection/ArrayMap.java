package androidx.collection;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class ArrayMap<K, V> extends SimpleArrayMap<K, V> implements Map<K, V> {
    MapCollections<K, V> h;

    public ArrayMap() {
    }

    private MapCollections<K, V> b() {
        if (this.h == null) {
            this.h = new MapCollections<K, V>() {
                /* access modifiers changed from: protected */
                public Object a(int i, int i2) {
                    return ArrayMap.this.b[(i << 1) + i2];
                }

                /* access modifiers changed from: protected */
                public int b(Object obj) {
                    return ArrayMap.this.b(obj);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    return ArrayMap.this.c;
                }

                /* access modifiers changed from: protected */
                public int a(Object obj) {
                    return ArrayMap.this.a(obj);
                }

                /* access modifiers changed from: protected */
                public Map<K, V> b() {
                    return ArrayMap.this;
                }

                /* access modifiers changed from: protected */
                public void a(K k, V v) {
                    ArrayMap.this.put(k, v);
                }

                /* access modifiers changed from: protected */
                public V a(int i, V v) {
                    return ArrayMap.this.a(i, v);
                }

                /* access modifiers changed from: protected */
                public void a(int i) {
                    ArrayMap.this.c(i);
                }

                /* access modifiers changed from: protected */
                public void a() {
                    ArrayMap.this.clear();
                }
            };
        }
        return this.h;
    }

    public boolean a(Collection<?> collection) {
        return MapCollections.c(this, collection);
    }

    public Set<Map.Entry<K, V>> entrySet() {
        return b().d();
    }

    public Set<K> keySet() {
        return b().e();
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        a(this.c + map.size());
        for (Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    public Collection<V> values() {
        return b().f();
    }

    public ArrayMap(int i) {
        super(i);
    }

    public ArrayMap(SimpleArrayMap simpleArrayMap) {
        super(simpleArrayMap);
    }
}
