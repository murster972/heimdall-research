package androidx.collection;

import java.util.ConcurrentModificationException;
import java.util.Map;

public class SimpleArrayMap<K, V> {
    static Object[] d;
    static int e;
    static Object[] f;
    static int g;

    /* renamed from: a  reason: collision with root package name */
    int[] f480a;
    Object[] b;
    int c;

    public SimpleArrayMap() {
        this.f480a = ContainerHelpers.f472a;
        this.b = ContainerHelpers.c;
        this.c = 0;
    }

    private static int a(int[] iArr, int i, int i2) {
        try {
            return ContainerHelpers.a(iArr, i, i2);
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    private void e(int i) {
        Class<SimpleArrayMap> cls = SimpleArrayMap.class;
        if (i == 8) {
            synchronized (cls) {
                if (f != null) {
                    Object[] objArr = f;
                    this.b = objArr;
                    f = (Object[]) objArr[0];
                    this.f480a = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    g--;
                    return;
                }
            }
        } else if (i == 4) {
            synchronized (cls) {
                if (d != null) {
                    Object[] objArr2 = d;
                    this.b = objArr2;
                    d = (Object[]) objArr2[0];
                    this.f480a = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    e--;
                    return;
                }
            }
        }
        this.f480a = new int[i];
        this.b = new Object[(i << 1)];
    }

    /* access modifiers changed from: package-private */
    public int b(Object obj) {
        int i = this.c * 2;
        Object[] objArr = this.b;
        if (obj == null) {
            for (int i2 = 1; i2 < i; i2 += 2) {
                if (objArr[i2] == null) {
                    return i2 >> 1;
                }
            }
            return -1;
        }
        for (int i3 = 1; i3 < i; i3 += 2) {
            if (obj.equals(objArr[i3])) {
                return i3 >> 1;
            }
        }
        return -1;
    }

    public V c(int i) {
        int i2;
        V[] vArr = this.b;
        int i3 = i << 1;
        V v = vArr[i3 + 1];
        int i4 = this.c;
        if (i4 <= 1) {
            a(this.f480a, (Object[]) vArr, i4);
            this.f480a = ContainerHelpers.f472a;
            this.b = ContainerHelpers.c;
            i2 = 0;
        } else {
            i2 = i4 - 1;
            int[] iArr = this.f480a;
            int i5 = 8;
            if (iArr.length <= 8 || i4 >= iArr.length / 3) {
                if (i < i2) {
                    int[] iArr2 = this.f480a;
                    int i6 = i + 1;
                    int i7 = i2 - i;
                    System.arraycopy(iArr2, i6, iArr2, i, i7);
                    Object[] objArr = this.b;
                    System.arraycopy(objArr, i6 << 1, objArr, i3, i7 << 1);
                }
                Object[] objArr2 = this.b;
                int i8 = i2 << 1;
                objArr2[i8] = null;
                objArr2[i8 + 1] = null;
            } else {
                if (i4 > 8) {
                    i5 = i4 + (i4 >> 1);
                }
                int[] iArr3 = this.f480a;
                Object[] objArr3 = this.b;
                e(i5);
                if (i4 == this.c) {
                    if (i > 0) {
                        System.arraycopy(iArr3, 0, this.f480a, 0, i);
                        System.arraycopy(objArr3, 0, this.b, 0, i3);
                    }
                    if (i < i2) {
                        int i9 = i + 1;
                        int i10 = i2 - i;
                        System.arraycopy(iArr3, i9, this.f480a, i, i10);
                        System.arraycopy(objArr3, i9 << 1, this.b, i3, i10 << 1);
                    }
                } else {
                    throw new ConcurrentModificationException();
                }
            }
        }
        if (i4 == this.c) {
            this.c = i2;
            return v;
        }
        throw new ConcurrentModificationException();
    }

    public void clear() {
        int i = this.c;
        if (i > 0) {
            int[] iArr = this.f480a;
            Object[] objArr = this.b;
            this.f480a = ContainerHelpers.f472a;
            this.b = ContainerHelpers.c;
            this.c = 0;
            a(iArr, objArr, i);
        }
        if (this.c > 0) {
            throw new ConcurrentModificationException();
        }
    }

    public boolean containsKey(Object obj) {
        return a(obj) >= 0;
    }

    public boolean containsValue(Object obj) {
        return b(obj) >= 0;
    }

    public V d(int i) {
        return this.b[(i << 1) + 1];
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof SimpleArrayMap) {
            SimpleArrayMap simpleArrayMap = (SimpleArrayMap) obj;
            if (size() != simpleArrayMap.size()) {
                return false;
            }
            int i = 0;
            while (i < this.c) {
                try {
                    Object b2 = b(i);
                    Object d2 = d(i);
                    Object obj2 = simpleArrayMap.get(b2);
                    if (d2 == null) {
                        if (obj2 != null || !simpleArrayMap.containsKey(b2)) {
                            return false;
                        }
                    } else if (!d2.equals(obj2)) {
                        return false;
                    }
                    i++;
                } catch (ClassCastException | NullPointerException unused) {
                    return false;
                }
            }
            return true;
        }
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (size() != map.size()) {
                return false;
            }
            int i2 = 0;
            while (i2 < this.c) {
                try {
                    Object b3 = b(i2);
                    Object d3 = d(i2);
                    Object obj3 = map.get(b3);
                    if (d3 == null) {
                        if (obj3 != null || !map.containsKey(b3)) {
                            return false;
                        }
                    } else if (!d3.equals(obj3)) {
                        return false;
                    }
                    i2++;
                } catch (ClassCastException | NullPointerException unused2) {
                }
            }
            return true;
        }
        return false;
    }

    public V get(Object obj) {
        return getOrDefault(obj, (Object) null);
    }

    public V getOrDefault(Object obj, V v) {
        int a2 = a(obj);
        return a2 >= 0 ? this.b[(a2 << 1) + 1] : v;
    }

    public int hashCode() {
        int[] iArr = this.f480a;
        Object[] objArr = this.b;
        int i = this.c;
        int i2 = 0;
        int i3 = 0;
        int i4 = 1;
        while (i2 < i) {
            Object obj = objArr[i4];
            i3 += (obj == null ? 0 : obj.hashCode()) ^ iArr[i2];
            i2++;
            i4 += 2;
        }
        return i3;
    }

    public boolean isEmpty() {
        return this.c <= 0;
    }

    public V put(K k, V v) {
        int i;
        int i2;
        int i3 = this.c;
        if (k == null) {
            i2 = a();
            i = 0;
        } else {
            int hashCode = k.hashCode();
            i = hashCode;
            i2 = a((Object) k, hashCode);
        }
        if (i2 >= 0) {
            int i4 = (i2 << 1) + 1;
            V[] vArr = this.b;
            V v2 = vArr[i4];
            vArr[i4] = v;
            return v2;
        }
        int i5 = ~i2;
        if (i3 >= this.f480a.length) {
            int i6 = 4;
            if (i3 >= 8) {
                i6 = (i3 >> 1) + i3;
            } else if (i3 >= 4) {
                i6 = 8;
            }
            int[] iArr = this.f480a;
            Object[] objArr = this.b;
            e(i6);
            if (i3 == this.c) {
                int[] iArr2 = this.f480a;
                if (iArr2.length > 0) {
                    System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                    System.arraycopy(objArr, 0, this.b, 0, objArr.length);
                }
                a(iArr, objArr, i3);
            } else {
                throw new ConcurrentModificationException();
            }
        }
        if (i5 < i3) {
            int[] iArr3 = this.f480a;
            int i7 = i5 + 1;
            System.arraycopy(iArr3, i5, iArr3, i7, i3 - i5);
            Object[] objArr2 = this.b;
            System.arraycopy(objArr2, i5 << 1, objArr2, i7 << 1, (this.c - i5) << 1);
        }
        int i8 = this.c;
        if (i3 == i8) {
            int[] iArr4 = this.f480a;
            if (i5 < iArr4.length) {
                iArr4[i5] = i;
                Object[] objArr3 = this.b;
                int i9 = i5 << 1;
                objArr3[i9] = k;
                objArr3[i9 + 1] = v;
                this.c = i8 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }

    public V putIfAbsent(K k, V v) {
        V v2 = get(k);
        return v2 == null ? put(k, v) : v2;
    }

    public V remove(Object obj) {
        int a2 = a(obj);
        if (a2 >= 0) {
            return c(a2);
        }
        return null;
    }

    public V replace(K k, V v) {
        int a2 = a((Object) k);
        if (a2 >= 0) {
            return a(a2, v);
        }
        return null;
    }

    public int size() {
        return this.c;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.c * 28);
        sb.append('{');
        for (int i = 0; i < this.c; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            Object b2 = b(i);
            if (b2 != this) {
                sb.append(b2);
            } else {
                sb.append("(this Map)");
            }
            sb.append('=');
            Object d2 = d(i);
            if (d2 != this) {
                sb.append(d2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public int a(Object obj, int i) {
        int i2 = this.c;
        if (i2 == 0) {
            return -1;
        }
        int a2 = a(this.f480a, i2, i);
        if (a2 < 0 || obj.equals(this.b[a2 << 1])) {
            return a2;
        }
        int i3 = a2 + 1;
        while (i3 < i2 && this.f480a[i3] == i) {
            if (obj.equals(this.b[i3 << 1])) {
                return i3;
            }
            i3++;
        }
        int i4 = a2 - 1;
        while (i4 >= 0 && this.f480a[i4] == i) {
            if (obj.equals(this.b[i4 << 1])) {
                return i4;
            }
            i4--;
        }
        return ~i3;
    }

    public boolean remove(Object obj, Object obj2) {
        int a2 = a(obj);
        if (a2 < 0) {
            return false;
        }
        Object d2 = d(a2);
        if (obj2 != d2 && (obj2 == null || !obj2.equals(d2))) {
            return false;
        }
        c(a2);
        return true;
    }

    public boolean replace(K k, V v, V v2) {
        int a2 = a((Object) k);
        if (a2 < 0) {
            return false;
        }
        V d2 = d(a2);
        if (d2 != v && (v == null || !v.equals(d2))) {
            return false;
        }
        a(a2, v2);
        return true;
    }

    public SimpleArrayMap(int i) {
        if (i == 0) {
            this.f480a = ContainerHelpers.f472a;
            this.b = ContainerHelpers.c;
        } else {
            e(i);
        }
        this.c = 0;
    }

    public K b(int i) {
        return this.b[i << 1];
    }

    public SimpleArrayMap(SimpleArrayMap<K, V> simpleArrayMap) {
        this();
        if (simpleArrayMap != null) {
            a(simpleArrayMap);
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int i = this.c;
        if (i == 0) {
            return -1;
        }
        int a2 = a(this.f480a, i, 0);
        if (a2 < 0 || this.b[a2 << 1] == null) {
            return a2;
        }
        int i2 = a2 + 1;
        while (i2 < i && this.f480a[i2] == 0) {
            if (this.b[i2 << 1] == null) {
                return i2;
            }
            i2++;
        }
        int i3 = a2 - 1;
        while (i3 >= 0 && this.f480a[i3] == 0) {
            if (this.b[i3 << 1] == null) {
                return i3;
            }
            i3--;
        }
        return ~i2;
    }

    private static void a(int[] iArr, Object[] objArr, int i) {
        Class<SimpleArrayMap> cls = SimpleArrayMap.class;
        if (iArr.length == 8) {
            synchronized (cls) {
                if (g < 10) {
                    objArr[0] = f;
                    objArr[1] = iArr;
                    for (int i2 = (i << 1) - 1; i2 >= 2; i2--) {
                        objArr[i2] = null;
                    }
                    f = objArr;
                    g++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (cls) {
                if (e < 10) {
                    objArr[0] = d;
                    objArr[1] = iArr;
                    for (int i3 = (i << 1) - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    d = objArr;
                    e++;
                }
            }
        }
    }

    public void a(int i) {
        int i2 = this.c;
        int[] iArr = this.f480a;
        if (iArr.length < i) {
            Object[] objArr = this.b;
            e(i);
            if (this.c > 0) {
                System.arraycopy(iArr, 0, this.f480a, 0, i2);
                System.arraycopy(objArr, 0, this.b, 0, i2 << 1);
            }
            a(iArr, objArr, i2);
        }
        if (this.c != i2) {
            throw new ConcurrentModificationException();
        }
    }

    public int a(Object obj) {
        return obj == null ? a() : a(obj, obj.hashCode());
    }

    public V a(int i, V v) {
        int i2 = (i << 1) + 1;
        V[] vArr = this.b;
        V v2 = vArr[i2];
        vArr[i2] = v;
        return v2;
    }

    public void a(SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        int i = simpleArrayMap.c;
        a(this.c + i);
        if (this.c != 0) {
            for (int i2 = 0; i2 < i; i2++) {
                put(simpleArrayMap.b(i2), simpleArrayMap.d(i2));
            }
        } else if (i > 0) {
            System.arraycopy(simpleArrayMap.f480a, 0, this.f480a, 0, i);
            System.arraycopy(simpleArrayMap.b, 0, this.b, 0, i << 1);
            this.c = i;
        }
    }
}
