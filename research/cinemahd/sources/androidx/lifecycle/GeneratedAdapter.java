package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;

public interface GeneratedAdapter {
    void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event, boolean z, MethodCallsLogger methodCallsLogger);
}
