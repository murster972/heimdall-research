package androidx.lifecycle;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ReportFragment;

public class ProcessLifecycleOwner implements LifecycleOwner {
    private static final ProcessLifecycleOwner i = new ProcessLifecycleOwner();

    /* renamed from: a  reason: collision with root package name */
    private int f735a = 0;
    private int b = 0;
    private boolean c = true;
    private boolean d = true;
    private Handler e;
    private final LifecycleRegistry f = new LifecycleRegistry(this);
    private Runnable g = new Runnable() {
        public void run() {
            ProcessLifecycleOwner.this.e();
            ProcessLifecycleOwner.this.f();
        }
    };
    ReportFragment.ActivityInitializationListener h = new ReportFragment.ActivityInitializationListener() {
        public void d() {
        }

        public void onResume() {
            ProcessLifecycleOwner.this.b();
        }

        public void onStart() {
            ProcessLifecycleOwner.this.c();
        }
    };

    private ProcessLifecycleOwner() {
    }

    static void b(Context context) {
        i.a(context);
    }

    public static LifecycleOwner g() {
        return i;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b--;
        if (this.b == 0) {
            this.e.postDelayed(this.g, 700);
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.f735a++;
        if (this.f735a == 1 && this.d) {
            this.f.a(Lifecycle.Event.ON_START);
            this.d = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.f735a--;
        f();
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.b == 0) {
            this.c = true;
            this.f.a(Lifecycle.Event.ON_PAUSE);
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.f735a == 0 && this.c) {
            this.f.a(Lifecycle.Event.ON_STOP);
            this.d = true;
        }
    }

    public Lifecycle getLifecycle() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.b++;
        if (this.b != 1) {
            return;
        }
        if (this.c) {
            this.f.a(Lifecycle.Event.ON_RESUME);
            this.c = false;
            return;
        }
        this.e.removeCallbacks(this.g);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        this.e = new Handler();
        this.f.a(Lifecycle.Event.ON_CREATE);
        ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new EmptyActivityLifecycleCallbacks() {
            public void onActivityCreated(Activity activity, Bundle bundle) {
                if (Build.VERSION.SDK_INT < 29) {
                    ReportFragment.a(activity).a(ProcessLifecycleOwner.this.h);
                }
            }

            public void onActivityPaused(Activity activity) {
                ProcessLifecycleOwner.this.a();
            }

            public void onActivityPreCreated(Activity activity, Bundle bundle) {
                activity.registerActivityLifecycleCallbacks(new EmptyActivityLifecycleCallbacks() {
                    public void onActivityPostResumed(Activity activity) {
                        ProcessLifecycleOwner.this.b();
                    }

                    public void onActivityPostStarted(Activity activity) {
                        ProcessLifecycleOwner.this.c();
                    }
                });
            }

            public void onActivityStopped(Activity activity) {
                ProcessLifecycleOwner.this.d();
            }
        });
    }
}
