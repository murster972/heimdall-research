package androidx.lifecycle;

import androidx.arch.core.internal.FastSafeIterableMap;
import androidx.arch.core.internal.SafeIterableMap;
import androidx.lifecycle.Lifecycle;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class LifecycleRegistry extends Lifecycle {

    /* renamed from: a  reason: collision with root package name */
    private FastSafeIterableMap<LifecycleObserver, ObserverWithState> f727a = new FastSafeIterableMap<>();
    private Lifecycle.State b;
    private final WeakReference<LifecycleOwner> c;
    private int d = 0;
    private boolean e = false;
    private boolean f = false;
    private ArrayList<Lifecycle.State> g = new ArrayList<>();

    /* renamed from: androidx.lifecycle.LifecycleRegistry$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f728a = new int[Lifecycle.Event.values().length];
        static final /* synthetic */ int[] b = new int[Lifecycle.State.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(27:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(30:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x005d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0067 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x007b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0086 */
        static {
            /*
                androidx.lifecycle.Lifecycle$State[] r0 = androidx.lifecycle.Lifecycle.State.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b = r0
                r0 = 1
                int[] r1 = b     // Catch:{ NoSuchFieldError -> 0x0014 }
                androidx.lifecycle.Lifecycle$State r2 = androidx.lifecycle.Lifecycle.State.INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x001f }
                androidx.lifecycle.Lifecycle$State r3 = androidx.lifecycle.Lifecycle.State.CREATED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = b     // Catch:{ NoSuchFieldError -> 0x002a }
                androidx.lifecycle.Lifecycle$State r4 = androidx.lifecycle.Lifecycle.State.STARTED     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = b     // Catch:{ NoSuchFieldError -> 0x0035 }
                androidx.lifecycle.Lifecycle$State r5 = androidx.lifecycle.Lifecycle.State.RESUMED     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                r4 = 5
                int[] r5 = b     // Catch:{ NoSuchFieldError -> 0x0040 }
                androidx.lifecycle.Lifecycle$State r6 = androidx.lifecycle.Lifecycle.State.DESTROYED     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r5[r6] = r4     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                androidx.lifecycle.Lifecycle$Event[] r5 = androidx.lifecycle.Lifecycle.Event.values()
                int r5 = r5.length
                int[] r5 = new int[r5]
                f728a = r5
                int[] r5 = f728a     // Catch:{ NoSuchFieldError -> 0x0053 }
                androidx.lifecycle.Lifecycle$Event r6 = androidx.lifecycle.Lifecycle.Event.ON_CREATE     // Catch:{ NoSuchFieldError -> 0x0053 }
                int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0053 }
                r5[r6] = r0     // Catch:{ NoSuchFieldError -> 0x0053 }
            L_0x0053:
                int[] r0 = f728a     // Catch:{ NoSuchFieldError -> 0x005d }
                androidx.lifecycle.Lifecycle$Event r5 = androidx.lifecycle.Lifecycle.Event.ON_STOP     // Catch:{ NoSuchFieldError -> 0x005d }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x005d }
                r0[r5] = r1     // Catch:{ NoSuchFieldError -> 0x005d }
            L_0x005d:
                int[] r0 = f728a     // Catch:{ NoSuchFieldError -> 0x0067 }
                androidx.lifecycle.Lifecycle$Event r1 = androidx.lifecycle.Lifecycle.Event.ON_START     // Catch:{ NoSuchFieldError -> 0x0067 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0067 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0067 }
            L_0x0067:
                int[] r0 = f728a     // Catch:{ NoSuchFieldError -> 0x0071 }
                androidx.lifecycle.Lifecycle$Event r1 = androidx.lifecycle.Lifecycle.Event.ON_PAUSE     // Catch:{ NoSuchFieldError -> 0x0071 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0071 }
            L_0x0071:
                int[] r0 = f728a     // Catch:{ NoSuchFieldError -> 0x007b }
                androidx.lifecycle.Lifecycle$Event r1 = androidx.lifecycle.Lifecycle.Event.ON_RESUME     // Catch:{ NoSuchFieldError -> 0x007b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007b }
                r0[r1] = r4     // Catch:{ NoSuchFieldError -> 0x007b }
            L_0x007b:
                int[] r0 = f728a     // Catch:{ NoSuchFieldError -> 0x0086 }
                androidx.lifecycle.Lifecycle$Event r1 = androidx.lifecycle.Lifecycle.Event.ON_DESTROY     // Catch:{ NoSuchFieldError -> 0x0086 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0086 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0086 }
            L_0x0086:
                int[] r0 = f728a     // Catch:{ NoSuchFieldError -> 0x0091 }
                androidx.lifecycle.Lifecycle$Event r1 = androidx.lifecycle.Lifecycle.Event.ON_ANY     // Catch:{ NoSuchFieldError -> 0x0091 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0091 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0091 }
            L_0x0091:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.lifecycle.LifecycleRegistry.AnonymousClass1.<clinit>():void");
        }
    }

    static class ObserverWithState {

        /* renamed from: a  reason: collision with root package name */
        Lifecycle.State f729a;
        LifecycleEventObserver b;

        ObserverWithState(LifecycleObserver lifecycleObserver, Lifecycle.State state) {
            this.b = Lifecycling.a((Object) lifecycleObserver);
            this.f729a = state;
        }

        /* access modifiers changed from: package-private */
        public void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
            Lifecycle.State b2 = LifecycleRegistry.b(event);
            this.f729a = LifecycleRegistry.a(this.f729a, b2);
            this.b.a(lifecycleOwner, event);
            this.f729a = b2;
        }
    }

    public LifecycleRegistry(LifecycleOwner lifecycleOwner) {
        this.c = new WeakReference<>(lifecycleOwner);
        this.b = Lifecycle.State.INITIALIZED;
    }

    private Lifecycle.State c(LifecycleObserver lifecycleObserver) {
        Map.Entry<LifecycleObserver, ObserverWithState> b2 = this.f727a.b(lifecycleObserver);
        Lifecycle.State state = null;
        Lifecycle.State state2 = b2 != null ? b2.getValue().f729a : null;
        if (!this.g.isEmpty()) {
            ArrayList<Lifecycle.State> arrayList = this.g;
            state = arrayList.get(arrayList.size() - 1);
        }
        return a(a(this.b, state2), state);
    }

    private void d(Lifecycle.State state) {
        if (this.b != state) {
            this.b = state;
            if (this.e || this.d != 0) {
                this.f = true;
                return;
            }
            this.e = true;
            d();
            this.e = false;
        }
    }

    private void e(Lifecycle.State state) {
        this.g.add(state);
    }

    private static Lifecycle.Event f(Lifecycle.State state) {
        int i = AnonymousClass1.b[state.ordinal()];
        if (i != 1) {
            if (i == 2) {
                return Lifecycle.Event.ON_START;
            }
            if (i == 3) {
                return Lifecycle.Event.ON_RESUME;
            }
            if (i == 4) {
                throw new IllegalArgumentException();
            } else if (i != 5) {
                throw new IllegalArgumentException("Unexpected state value " + state);
            }
        }
        return Lifecycle.Event.ON_CREATE;
    }

    @Deprecated
    public void a(Lifecycle.State state) {
        b(state);
    }

    public void b(Lifecycle.State state) {
        d(state);
    }

    private boolean b() {
        if (this.f727a.size() == 0) {
            return true;
        }
        Lifecycle.State state = this.f727a.a().getValue().f729a;
        Lifecycle.State state2 = this.f727a.c().getValue().f729a;
        if (state == state2 && this.b == state2) {
            return true;
        }
        return false;
    }

    public void a(Lifecycle.Event event) {
        d(b(event));
    }

    public void a(LifecycleObserver lifecycleObserver) {
        LifecycleOwner lifecycleOwner;
        Lifecycle.State state = this.b;
        Lifecycle.State state2 = Lifecycle.State.DESTROYED;
        if (state != state2) {
            state2 = Lifecycle.State.INITIALIZED;
        }
        ObserverWithState observerWithState = new ObserverWithState(lifecycleObserver, state2);
        if (this.f727a.b(lifecycleObserver, observerWithState) == null && (lifecycleOwner = (LifecycleOwner) this.c.get()) != null) {
            boolean z = this.d != 0 || this.e;
            Lifecycle.State c2 = c(lifecycleObserver);
            this.d++;
            while (observerWithState.f729a.compareTo(c2) < 0 && this.f727a.contains(lifecycleObserver)) {
                e(observerWithState.f729a);
                observerWithState.a(lifecycleOwner, f(observerWithState.f729a));
                c();
                c2 = c(lifecycleObserver);
            }
            if (!z) {
                d();
            }
            this.d--;
        }
    }

    private void c() {
        ArrayList<Lifecycle.State> arrayList = this.g;
        arrayList.remove(arrayList.size() - 1);
    }

    private static Lifecycle.Event c(Lifecycle.State state) {
        int i = AnonymousClass1.b[state.ordinal()];
        if (i == 1) {
            throw new IllegalArgumentException();
        } else if (i == 2) {
            return Lifecycle.Event.ON_DESTROY;
        } else {
            if (i == 3) {
                return Lifecycle.Event.ON_STOP;
            }
            if (i == 4) {
                return Lifecycle.Event.ON_PAUSE;
            }
            if (i != 5) {
                throw new IllegalArgumentException("Unexpected state value " + state);
            }
            throw new IllegalArgumentException();
        }
    }

    public void b(LifecycleObserver lifecycleObserver) {
        this.f727a.remove(lifecycleObserver);
    }

    static Lifecycle.State b(Lifecycle.Event event) {
        switch (AnonymousClass1.f728a[event.ordinal()]) {
            case 1:
            case 2:
                return Lifecycle.State.CREATED;
            case 3:
            case 4:
                return Lifecycle.State.STARTED;
            case 5:
                return Lifecycle.State.RESUMED;
            case 6:
                return Lifecycle.State.DESTROYED;
            default:
                throw new IllegalArgumentException("Unexpected event value " + event);
        }
    }

    private void d() {
        LifecycleOwner lifecycleOwner = (LifecycleOwner) this.c.get();
        if (lifecycleOwner != null) {
            while (!b()) {
                this.f = false;
                if (this.b.compareTo(this.f727a.a().getValue().f729a) < 0) {
                    a(lifecycleOwner);
                }
                Map.Entry<LifecycleObserver, ObserverWithState> c2 = this.f727a.c();
                if (!this.f && c2 != null && this.b.compareTo(c2.getValue().f729a) > 0) {
                    b(lifecycleOwner);
                }
            }
            this.f = false;
            return;
        }
        throw new IllegalStateException("LifecycleOwner of this LifecycleRegistry is alreadygarbage collected. It is too late to change lifecycle state.");
    }

    private void b(LifecycleOwner lifecycleOwner) {
        SafeIterableMap<K, V>.IteratorWithAdditions b2 = this.f727a.b();
        while (b2.hasNext() && !this.f) {
            Map.Entry entry = (Map.Entry) b2.next();
            ObserverWithState observerWithState = (ObserverWithState) entry.getValue();
            while (observerWithState.f729a.compareTo(this.b) < 0 && !this.f && this.f727a.contains(entry.getKey())) {
                e(observerWithState.f729a);
                observerWithState.a(lifecycleOwner, f(observerWithState.f729a));
                c();
            }
        }
    }

    public Lifecycle.State a() {
        return this.b;
    }

    private void a(LifecycleOwner lifecycleOwner) {
        Iterator<Map.Entry<LifecycleObserver, ObserverWithState>> descendingIterator = this.f727a.descendingIterator();
        while (descendingIterator.hasNext() && !this.f) {
            Map.Entry next = descendingIterator.next();
            ObserverWithState observerWithState = (ObserverWithState) next.getValue();
            while (observerWithState.f729a.compareTo(this.b) > 0 && !this.f && this.f727a.contains(next.getKey())) {
                Lifecycle.Event c2 = c(observerWithState.f729a);
                e(b(c2));
                observerWithState.a(lifecycleOwner, c2);
                c();
            }
        }
    }

    static Lifecycle.State a(Lifecycle.State state, Lifecycle.State state2) {
        return (state2 == null || state2.compareTo(state) >= 0) ? state : state2;
    }
}
