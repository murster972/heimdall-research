package androidx.lifecycle;

import android.app.Application;
import java.lang.reflect.InvocationTargetException;

public class ViewModelProvider {

    /* renamed from: a  reason: collision with root package name */
    private final Factory f747a;
    private final ViewModelStore b;

    public interface Factory {
        <T extends ViewModel> T a(Class<T> cls);
    }

    static abstract class KeyedFactory extends OnRequeryFactory implements Factory {
        KeyedFactory() {
        }

        public <T extends ViewModel> T a(Class<T> cls) {
            throw new UnsupportedOperationException("create(String, Class<?>) must be called on implementaions of KeyedFactory");
        }

        public abstract <T extends ViewModel> T a(String str, Class<T> cls);
    }

    public static class NewInstanceFactory implements Factory {
        public <T extends ViewModel> T a(Class<T> cls) {
            try {
                return (ViewModel) cls.newInstance();
            } catch (InstantiationException e) {
                throw new RuntimeException("Cannot create an instance of " + cls, e);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("Cannot create an instance of " + cls, e2);
            }
        }
    }

    static class OnRequeryFactory {
        OnRequeryFactory() {
        }

        /* access modifiers changed from: package-private */
        public void a(ViewModel viewModel) {
        }
    }

    public ViewModelProvider(ViewModelStore viewModelStore, Factory factory) {
        this.f747a = factory;
        this.b = viewModelStore;
    }

    public <T extends ViewModel> T a(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return a("androidx.lifecycle.ViewModelProvider.DefaultKey:" + canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    public static class AndroidViewModelFactory extends NewInstanceFactory {
        private static AndroidViewModelFactory b;

        /* renamed from: a  reason: collision with root package name */
        private Application f748a;

        public AndroidViewModelFactory(Application application) {
            this.f748a = application;
        }

        public static AndroidViewModelFactory a(Application application) {
            if (b == null) {
                b = new AndroidViewModelFactory(application);
            }
            return b;
        }

        public <T extends ViewModel> T a(Class<T> cls) {
            if (!AndroidViewModel.class.isAssignableFrom(cls)) {
                return super.a(cls);
            }
            try {
                return (ViewModel) cls.getConstructor(new Class[]{Application.class}).newInstance(new Object[]{this.f748a});
            } catch (NoSuchMethodException e) {
                throw new RuntimeException("Cannot create an instance of " + cls, e);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("Cannot create an instance of " + cls, e2);
            } catch (InstantiationException e3) {
                throw new RuntimeException("Cannot create an instance of " + cls, e3);
            } catch (InvocationTargetException e4) {
                throw new RuntimeException("Cannot create an instance of " + cls, e4);
            }
        }
    }

    public <T extends ViewModel> T a(String str, Class<T> cls) {
        T t;
        T a2 = this.b.a(str);
        if (cls.isInstance(a2)) {
            Factory factory = this.f747a;
            if (factory instanceof OnRequeryFactory) {
                ((OnRequeryFactory) factory).a(a2);
            }
            return a2;
        }
        Factory factory2 = this.f747a;
        if (factory2 instanceof KeyedFactory) {
            t = ((KeyedFactory) factory2).a(str, cls);
        } else {
            t = factory2.a(cls);
        }
        this.b.a(str, t);
        return t;
    }
}
