package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;

class CompositeGeneratedAdaptersObserver implements LifecycleEventObserver {

    /* renamed from: a  reason: collision with root package name */
    private final GeneratedAdapter[] f722a;

    CompositeGeneratedAdaptersObserver(GeneratedAdapter[] generatedAdapterArr) {
        this.f722a = generatedAdapterArr;
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
        MethodCallsLogger methodCallsLogger = new MethodCallsLogger();
        for (GeneratedAdapter a2 : this.f722a) {
            a2.a(lifecycleOwner, event, false, methodCallsLogger);
        }
        for (GeneratedAdapter a3 : this.f722a) {
            a3.a(lifecycleOwner, event, true, methodCallsLogger);
        }
    }
}
