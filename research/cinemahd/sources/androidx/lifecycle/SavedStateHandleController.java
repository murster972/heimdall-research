package androidx.lifecycle;

import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.savedstate.SavedStateRegistry;
import androidx.savedstate.SavedStateRegistryOwner;

final class SavedStateHandleController implements LifecycleEventObserver {

    /* renamed from: a  reason: collision with root package name */
    private final String f742a;
    private boolean b = false;
    private final SavedStateHandle c;

    static final class OnRecreation implements SavedStateRegistry.AutoRecreated {
        OnRecreation() {
        }

        public void a(SavedStateRegistryOwner savedStateRegistryOwner) {
            if (savedStateRegistryOwner instanceof ViewModelStoreOwner) {
                ViewModelStore viewModelStore = ((ViewModelStoreOwner) savedStateRegistryOwner).getViewModelStore();
                SavedStateRegistry savedStateRegistry = savedStateRegistryOwner.getSavedStateRegistry();
                for (String a2 : viewModelStore.b()) {
                    SavedStateHandleController.a(viewModelStore.a(a2), savedStateRegistry, savedStateRegistryOwner.getLifecycle());
                }
                if (!viewModelStore.b().isEmpty()) {
                    savedStateRegistry.a((Class<? extends SavedStateRegistry.AutoRecreated>) OnRecreation.class);
                    return;
                }
                return;
            }
            throw new IllegalStateException("Internal error: OnRecreation should be registered only on componentsthat implement ViewModelStoreOwner");
        }
    }

    SavedStateHandleController(String str, SavedStateHandle savedStateHandle) {
        this.f742a = str;
        this.c = savedStateHandle;
    }

    /* access modifiers changed from: package-private */
    public void a(SavedStateRegistry savedStateRegistry, Lifecycle lifecycle) {
        if (!this.b) {
            this.b = true;
            lifecycle.a(this);
            savedStateRegistry.a(this.f742a, this.c.a());
            return;
        }
        throw new IllegalStateException("Already attached to lifecycleOwner");
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.b;
    }

    private static void b(final SavedStateRegistry savedStateRegistry, final Lifecycle lifecycle) {
        Lifecycle.State a2 = lifecycle.a();
        if (a2 == Lifecycle.State.INITIALIZED || a2.a(Lifecycle.State.STARTED)) {
            savedStateRegistry.a((Class<? extends SavedStateRegistry.AutoRecreated>) OnRecreation.class);
        } else {
            lifecycle.a(new LifecycleEventObserver() {
                public void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_START) {
                        lifecycle.b(this);
                        savedStateRegistry.a((Class<? extends SavedStateRegistry.AutoRecreated>) OnRecreation.class);
                    }
                }
            });
        }
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
        if (event == Lifecycle.Event.ON_DESTROY) {
            this.b = false;
            lifecycleOwner.getLifecycle().b(this);
        }
    }

    /* access modifiers changed from: package-private */
    public SavedStateHandle a() {
        return this.c;
    }

    static SavedStateHandleController a(SavedStateRegistry savedStateRegistry, Lifecycle lifecycle, String str, Bundle bundle) {
        SavedStateHandleController savedStateHandleController = new SavedStateHandleController(str, SavedStateHandle.a(savedStateRegistry.a(str), bundle));
        savedStateHandleController.a(savedStateRegistry, lifecycle);
        b(savedStateRegistry, lifecycle);
        return savedStateHandleController;
    }

    static void a(ViewModel viewModel, SavedStateRegistry savedStateRegistry, Lifecycle lifecycle) {
        SavedStateHandleController savedStateHandleController = (SavedStateHandleController) viewModel.a("androidx.lifecycle.savedstate.vm.tag");
        if (savedStateHandleController != null && !savedStateHandleController.b()) {
            savedStateHandleController.a(savedStateRegistry, lifecycle);
            b(savedStateRegistry, lifecycle);
        }
    }
}
