package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;

public interface LifecycleEventObserver extends LifecycleObserver {
    void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event);
}
