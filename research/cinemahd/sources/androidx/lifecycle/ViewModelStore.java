package androidx.lifecycle;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ViewModelStore {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap<String, ViewModel> f749a = new HashMap<>();

    /* access modifiers changed from: package-private */
    public final void a(String str, ViewModel viewModel) {
        ViewModel put = this.f749a.put(str, viewModel);
        if (put != null) {
            put.b();
        }
    }

    /* access modifiers changed from: package-private */
    public Set<String> b() {
        return new HashSet(this.f749a.keySet());
    }

    /* access modifiers changed from: package-private */
    public final ViewModel a(String str) {
        return this.f749a.get(str);
    }

    public final void a() {
        for (ViewModel a2 : this.f749a.values()) {
            a2.a();
        }
        this.f749a.clear();
    }
}
