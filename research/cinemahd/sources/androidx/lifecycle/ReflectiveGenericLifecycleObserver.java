package androidx.lifecycle;

import androidx.lifecycle.ClassesInfoCache;
import androidx.lifecycle.Lifecycle;

class ReflectiveGenericLifecycleObserver implements LifecycleEventObserver {

    /* renamed from: a  reason: collision with root package name */
    private final Object f738a;
    private final ClassesInfoCache.CallbackInfo b = ClassesInfoCache.c.a(this.f738a.getClass());

    ReflectiveGenericLifecycleObserver(Object obj) {
        this.f738a = obj;
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
        this.b.a(lifecycleOwner, event, this.f738a);
    }
}
