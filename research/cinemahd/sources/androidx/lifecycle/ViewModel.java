package androidx.lifecycle;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class ViewModel {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, Object> f746a = new HashMap();
    private volatile boolean b = false;

    /* access modifiers changed from: package-private */
    public final void a() {
        this.b = true;
        Map<String, Object> map = this.f746a;
        if (map != null) {
            synchronized (map) {
                for (Object a2 : this.f746a.values()) {
                    a(a2);
                }
            }
        }
        b();
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    /* access modifiers changed from: package-private */
    public <T> T a(String str, T t) {
        T t2;
        synchronized (this.f746a) {
            t2 = this.f746a.get(str);
            if (t2 == null) {
                this.f746a.put(str, t);
            }
        }
        if (t2 != null) {
            t = t2;
        }
        if (this.b) {
            a((Object) t);
        }
        return t;
    }

    /* access modifiers changed from: package-private */
    public <T> T a(String str) {
        T t;
        Map<String, Object> map = this.f746a;
        if (map == null) {
            return null;
        }
        synchronized (map) {
            t = this.f746a.get(str);
        }
        return t;
    }

    private static void a(Object obj) {
        if (obj instanceof Closeable) {
            try {
                ((Closeable) obj).close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
