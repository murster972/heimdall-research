package androidx.lifecycle;

import androidx.arch.core.executor.ArchTaskExecutor;
import androidx.arch.core.internal.SafeIterableMap;
import androidx.lifecycle.Lifecycle;
import java.util.Map;

public abstract class LiveData<T> {
    static final Object j = new Object();

    /* renamed from: a  reason: collision with root package name */
    final Object f732a = new Object();
    private SafeIterableMap<Observer<? super T>, LiveData<T>.ObserverWrapper> b = new SafeIterableMap<>();
    int c = 0;
    private volatile Object d = j;
    volatile Object e = j;
    private int f = -1;
    private boolean g;
    private boolean h;
    private final Runnable i = new Runnable() {
        public void run() {
            Object obj;
            synchronized (LiveData.this.f732a) {
                obj = LiveData.this.e;
                LiveData.this.e = LiveData.j;
            }
            LiveData.this.b(obj);
        }
    };

    private abstract class ObserverWrapper {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f734a;
        boolean b;
        int c = -1;

        ObserverWrapper(Observer<? super T> observer) {
            this.f734a = observer;
        }

        /* access modifiers changed from: package-private */
        public void a() {
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            if (z != this.b) {
                this.b = z;
                int i = 1;
                boolean z2 = LiveData.this.c == 0;
                LiveData liveData = LiveData.this;
                int i2 = liveData.c;
                if (!this.b) {
                    i = -1;
                }
                liveData.c = i2 + i;
                if (z2 && this.b) {
                    LiveData.this.c();
                }
                LiveData liveData2 = LiveData.this;
                if (liveData2.c == 0 && !this.b) {
                    liveData2.d();
                }
                if (this.b) {
                    LiveData.this.a((LiveData<T>.ObserverWrapper) this);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(LifecycleOwner lifecycleOwner) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public abstract boolean b();
    }

    private void b(LiveData<T>.ObserverWrapper observerWrapper) {
        if (observerWrapper.b) {
            if (!observerWrapper.b()) {
                observerWrapper.a(false);
                return;
            }
            int i2 = observerWrapper.c;
            int i3 = this.f;
            if (i2 < i3) {
                observerWrapper.c = i3;
                observerWrapper.f734a.a(this.d);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(LiveData<T>.ObserverWrapper observerWrapper) {
        if (this.g) {
            this.h = true;
            return;
        }
        this.g = true;
        do {
            this.h = false;
            if (observerWrapper == null) {
                SafeIterableMap<K, V>.IteratorWithAdditions b2 = this.b.b();
                while (b2.hasNext()) {
                    b((LiveData<T>.ObserverWrapper) (ObserverWrapper) ((Map.Entry) b2.next()).getValue());
                    if (this.h) {
                        break;
                    }
                }
            } else {
                b(observerWrapper);
                observerWrapper = null;
            }
        } while (this.h);
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    class LifecycleBoundObserver extends LiveData<T>.ObserverWrapper implements LifecycleEventObserver {
        final LifecycleOwner e;

        LifecycleBoundObserver(LifecycleOwner lifecycleOwner, Observer<? super T> observer) {
            super(observer);
            this.e = lifecycleOwner;
        }

        public void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
            if (this.e.getLifecycle().a() == Lifecycle.State.DESTROYED) {
                LiveData.this.a(this.f734a);
            } else {
                a(b());
            }
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            return this.e.getLifecycle().a().a(Lifecycle.State.STARTED);
        }

        /* access modifiers changed from: package-private */
        public boolean a(LifecycleOwner lifecycleOwner) {
            return this.e == lifecycleOwner;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.e.getLifecycle().b(this);
        }
    }

    /* access modifiers changed from: protected */
    public void b(T t) {
        a("setValue");
        this.f++;
        this.d = t;
        a((LiveData<T>.ObserverWrapper) null);
    }

    public boolean b() {
        return this.c > 0;
    }

    public void a(LifecycleOwner lifecycleOwner, Observer<? super T> observer) {
        a("observe");
        if (lifecycleOwner.getLifecycle().a() != Lifecycle.State.DESTROYED) {
            LifecycleBoundObserver lifecycleBoundObserver = new LifecycleBoundObserver(lifecycleOwner, observer);
            ObserverWrapper b2 = this.b.b(observer, lifecycleBoundObserver);
            if (b2 != null && !b2.a(lifecycleOwner)) {
                throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
            } else if (b2 == null) {
                lifecycleOwner.getLifecycle().a(lifecycleBoundObserver);
            }
        }
    }

    public void a(Observer<? super T> observer) {
        a("removeObserver");
        ObserverWrapper remove = this.b.remove(observer);
        if (remove != null) {
            remove.a();
            remove.a(false);
        }
    }

    /* access modifiers changed from: protected */
    public void a(T t) {
        boolean z;
        synchronized (this.f732a) {
            z = this.e == j;
            this.e = t;
        }
        if (z) {
            ArchTaskExecutor.c().b(this.i);
        }
    }

    public T a() {
        T t = this.d;
        if (t != j) {
            return t;
        }
        return null;
    }

    static void a(String str) {
        if (!ArchTaskExecutor.c().a()) {
            throw new IllegalStateException("Cannot invoke " + str + " on a background thread");
        }
    }
}
