package androidx.lifecycle;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Bundle;
import androidx.lifecycle.ViewModelProvider;
import androidx.savedstate.SavedStateRegistry;
import androidx.savedstate.SavedStateRegistryOwner;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public final class SavedStateViewModelFactory extends ViewModelProvider.KeyedFactory {
    private static final Class<?>[] f;
    private static final Class<?>[] g;

    /* renamed from: a  reason: collision with root package name */
    private final Application f744a;
    private final ViewModelProvider.AndroidViewModelFactory b;
    private final Bundle c;
    private final Lifecycle d;
    private final SavedStateRegistry e;

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Class<?>[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    static {
        /*
            java.lang.Class<androidx.lifecycle.SavedStateHandle> r0 = androidx.lifecycle.SavedStateHandle.class
            r1 = 2
            java.lang.Class[] r1 = new java.lang.Class[r1]
            java.lang.Class<android.app.Application> r2 = android.app.Application.class
            r3 = 0
            r1[r3] = r2
            r2 = 1
            r1[r2] = r0
            f = r1
            java.lang.Class[] r1 = new java.lang.Class[r2]
            r1[r3] = r0
            g = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.lifecycle.SavedStateViewModelFactory.<clinit>():void");
    }

    @SuppressLint({"LambdaLast"})
    public SavedStateViewModelFactory(Application application, SavedStateRegistryOwner savedStateRegistryOwner, Bundle bundle) {
        this.e = savedStateRegistryOwner.getSavedStateRegistry();
        this.d = savedStateRegistryOwner.getLifecycle();
        this.c = bundle;
        this.f744a = application;
        this.b = ViewModelProvider.AndroidViewModelFactory.a(application);
    }

    public <T extends ViewModel> T a(String str, Class<T> cls) {
        Constructor<T> constructor;
        T t;
        boolean isAssignableFrom = AndroidViewModel.class.isAssignableFrom(cls);
        if (isAssignableFrom) {
            constructor = a(cls, f);
        } else {
            constructor = a(cls, g);
        }
        if (constructor == null) {
            return this.b.a(cls);
        }
        SavedStateHandleController a2 = SavedStateHandleController.a(this.e, this.d, str, this.c);
        if (isAssignableFrom) {
            try {
                t = (ViewModel) constructor.newInstance(new Object[]{this.f744a, a2.a()});
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("Failed to access " + cls, e2);
            } catch (InstantiationException e3) {
                throw new RuntimeException("A " + cls + " cannot be instantiated.", e3);
            } catch (InvocationTargetException e4) {
                throw new RuntimeException("An exception happened in constructor of " + cls, e4.getCause());
            }
        } else {
            t = (ViewModel) constructor.newInstance(new Object[]{a2.a()});
        }
        t.a("androidx.lifecycle.savedstate.vm.tag", a2);
        return t;
    }

    public <T extends ViewModel> T a(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return a(canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    private static <T> Constructor<T> a(Class<T> cls, Class<?>[] clsArr) {
        for (Constructor<T> constructor : cls.getConstructors()) {
            if (Arrays.equals(clsArr, constructor.getParameterTypes())) {
                return constructor;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(ViewModel viewModel) {
        SavedStateHandleController.a(viewModel, this.e, this.d);
    }
}
