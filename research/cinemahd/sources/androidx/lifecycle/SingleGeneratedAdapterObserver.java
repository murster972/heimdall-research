package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;

class SingleGeneratedAdapterObserver implements LifecycleEventObserver {

    /* renamed from: a  reason: collision with root package name */
    private final GeneratedAdapter f745a;

    SingleGeneratedAdapterObserver(GeneratedAdapter generatedAdapter) {
        this.f745a = generatedAdapter;
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
        this.f745a.a(lifecycleOwner, event, false, (MethodCallsLogger) null);
        this.f745a.a(lifecycleOwner, event, true, (MethodCallsLogger) null);
    }
}
