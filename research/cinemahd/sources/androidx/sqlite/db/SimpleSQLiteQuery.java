package androidx.sqlite.db;

public final class SimpleSQLiteQuery implements SupportSQLiteQuery {

    /* renamed from: a  reason: collision with root package name */
    private final String f1050a;
    private final Object[] b;

    public SimpleSQLiteQuery(String str, Object[] objArr) {
        this.f1050a = str;
        this.b = objArr;
    }

    public String a() {
        return this.f1050a;
    }

    public void a(SupportSQLiteProgram supportSQLiteProgram) {
        a(supportSQLiteProgram, this.b);
    }

    public static void a(SupportSQLiteProgram supportSQLiteProgram, Object[] objArr) {
        if (objArr != null) {
            int length = objArr.length;
            int i = 0;
            while (i < length) {
                Object obj = objArr[i];
                i++;
                a(supportSQLiteProgram, i, obj);
            }
        }
    }

    public SimpleSQLiteQuery(String str) {
        this(str, (Object[]) null);
    }

    private static void a(SupportSQLiteProgram supportSQLiteProgram, int i, Object obj) {
        if (obj == null) {
            supportSQLiteProgram.a(i);
        } else if (obj instanceof byte[]) {
            supportSQLiteProgram.a(i, (byte[]) obj);
        } else if (obj instanceof Float) {
            supportSQLiteProgram.a(i, (double) ((Float) obj).floatValue());
        } else if (obj instanceof Double) {
            supportSQLiteProgram.a(i, ((Double) obj).doubleValue());
        } else if (obj instanceof Long) {
            supportSQLiteProgram.a(i, ((Long) obj).longValue());
        } else if (obj instanceof Integer) {
            supportSQLiteProgram.a(i, (long) ((Integer) obj).intValue());
        } else if (obj instanceof Short) {
            supportSQLiteProgram.a(i, (long) ((Short) obj).shortValue());
        } else if (obj instanceof Byte) {
            supportSQLiteProgram.a(i, (long) ((Byte) obj).byteValue());
        } else if (obj instanceof String) {
            supportSQLiteProgram.a(i, (String) obj);
        } else if (obj instanceof Boolean) {
            supportSQLiteProgram.a(i, ((Boolean) obj).booleanValue() ? 1 : 0);
        } else {
            throw new IllegalArgumentException("Cannot bind " + obj + " at index " + i + " Supported types: null, byte[], float, double, long, int, short, byte," + " string");
        }
    }
}
