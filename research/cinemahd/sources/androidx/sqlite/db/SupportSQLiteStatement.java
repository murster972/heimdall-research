package androidx.sqlite.db;

public interface SupportSQLiteStatement extends SupportSQLiteProgram {
    int o();

    long p();
}
