package androidx.sqlite.db;

import android.database.Cursor;
import android.database.SQLException;
import android.util.Pair;
import java.io.Closeable;
import java.util.List;

public interface SupportSQLiteDatabase extends Closeable {
    Cursor a(SupportSQLiteQuery supportSQLiteQuery);

    void b(String str) throws SQLException;

    void beginTransaction();

    SupportSQLiteStatement c(String str);

    Cursor d(String str);

    void endTransaction();

    String getPath();

    boolean isOpen();

    List<Pair<String, String>> q();

    boolean r();

    void setTransactionSuccessful();
}
