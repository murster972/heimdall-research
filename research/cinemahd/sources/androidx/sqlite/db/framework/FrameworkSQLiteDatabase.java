package androidx.sqlite.db.framework;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.util.Pair;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.io.IOException;
import java.util.List;

class FrameworkSQLiteDatabase implements SupportSQLiteDatabase {
    private static final String[] b = new String[0];

    /* renamed from: a  reason: collision with root package name */
    private final SQLiteDatabase f1054a;

    static {
        new String[]{"", " OR ROLLBACK ", " OR ABORT ", " OR FAIL ", " OR IGNORE ", " OR REPLACE "};
    }

    FrameworkSQLiteDatabase(SQLiteDatabase sQLiteDatabase) {
        this.f1054a = sQLiteDatabase;
    }

    public Cursor a(final SupportSQLiteQuery supportSQLiteQuery) {
        return this.f1054a.rawQueryWithFactory(new SQLiteDatabase.CursorFactory(this) {
            public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
                supportSQLiteQuery.a(new FrameworkSQLiteProgram(sQLiteQuery));
                return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
            }
        }, supportSQLiteQuery.a(), b, (String) null);
    }

    public void b(String str) throws SQLException {
        this.f1054a.execSQL(str);
    }

    public void beginTransaction() {
        this.f1054a.beginTransaction();
    }

    public SupportSQLiteStatement c(String str) {
        return new FrameworkSQLiteStatement(this.f1054a.compileStatement(str));
    }

    public void close() throws IOException {
        this.f1054a.close();
    }

    public Cursor d(String str) {
        return a((SupportSQLiteQuery) new SimpleSQLiteQuery(str));
    }

    public void endTransaction() {
        this.f1054a.endTransaction();
    }

    public String getPath() {
        return this.f1054a.getPath();
    }

    public boolean isOpen() {
        return this.f1054a.isOpen();
    }

    public List<Pair<String, String>> q() {
        return this.f1054a.getAttachedDbs();
    }

    public boolean r() {
        return this.f1054a.inTransaction();
    }

    public void setTransactionSuccessful() {
        this.f1054a.setTransactionSuccessful();
    }

    /* access modifiers changed from: package-private */
    public boolean a(SQLiteDatabase sQLiteDatabase) {
        return this.f1054a == sQLiteDatabase;
    }
}
