package androidx.sqlite.db.framework;

import android.database.sqlite.SQLiteProgram;
import androidx.sqlite.db.SupportSQLiteProgram;

class FrameworkSQLiteProgram implements SupportSQLiteProgram {

    /* renamed from: a  reason: collision with root package name */
    private final SQLiteProgram f1059a;

    FrameworkSQLiteProgram(SQLiteProgram sQLiteProgram) {
        this.f1059a = sQLiteProgram;
    }

    public void a(int i) {
        this.f1059a.bindNull(i);
    }

    public void close() {
        this.f1059a.close();
    }

    public void a(int i, long j) {
        this.f1059a.bindLong(i, j);
    }

    public void a(int i, double d) {
        this.f1059a.bindDouble(i, d);
    }

    public void a(int i, String str) {
        this.f1059a.bindString(i, str);
    }

    public void a(int i, byte[] bArr) {
        this.f1059a.bindBlob(i, bArr);
    }
}
