package androidx.sqlite.db.framework;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

class FrameworkSQLiteOpenHelper implements SupportSQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private final OpenHelper f1056a;

    FrameworkSQLiteOpenHelper(Context context, String str, SupportSQLiteOpenHelper.Callback callback) {
        this.f1056a = a(context, str, callback);
    }

    private OpenHelper a(Context context, String str, SupportSQLiteOpenHelper.Callback callback) {
        return new OpenHelper(context, str, new FrameworkSQLiteDatabase[1], callback);
    }

    public void a(boolean z) {
        this.f1056a.setWriteAheadLoggingEnabled(z);
    }

    public SupportSQLiteDatabase a() {
        return this.f1056a.a();
    }

    static class OpenHelper extends SQLiteOpenHelper {

        /* renamed from: a  reason: collision with root package name */
        final FrameworkSQLiteDatabase[] f1057a;
        final SupportSQLiteOpenHelper.Callback b;
        private boolean c;

        OpenHelper(Context context, String str, final FrameworkSQLiteDatabase[] frameworkSQLiteDatabaseArr, final SupportSQLiteOpenHelper.Callback callback) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, callback.f1051a, new DatabaseErrorHandler() {
                public void onCorruption(SQLiteDatabase sQLiteDatabase) {
                    SupportSQLiteOpenHelper.Callback.this.b(OpenHelper.a(frameworkSQLiteDatabaseArr, sQLiteDatabase));
                }
            });
            this.b = callback;
            this.f1057a = frameworkSQLiteDatabaseArr;
        }

        /* access modifiers changed from: package-private */
        public synchronized SupportSQLiteDatabase a() {
            this.c = false;
            SQLiteDatabase writableDatabase = super.getWritableDatabase();
            if (this.c) {
                close();
                return a();
            }
            return a(writableDatabase);
        }

        public synchronized void close() {
            super.close();
            this.f1057a[0] = null;
        }

        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            this.b.a((SupportSQLiteDatabase) a(sQLiteDatabase));
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            this.b.c(a(sQLiteDatabase));
        }

        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.c = true;
            this.b.a(a(sQLiteDatabase), i, i2);
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (!this.c) {
                this.b.d(a(sQLiteDatabase));
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.c = true;
            this.b.b(a(sQLiteDatabase), i, i2);
        }

        /* access modifiers changed from: package-private */
        public FrameworkSQLiteDatabase a(SQLiteDatabase sQLiteDatabase) {
            return a(this.f1057a, sQLiteDatabase);
        }

        static FrameworkSQLiteDatabase a(FrameworkSQLiteDatabase[] frameworkSQLiteDatabaseArr, SQLiteDatabase sQLiteDatabase) {
            FrameworkSQLiteDatabase frameworkSQLiteDatabase = frameworkSQLiteDatabaseArr[0];
            if (frameworkSQLiteDatabase == null || !frameworkSQLiteDatabase.a(sQLiteDatabase)) {
                frameworkSQLiteDatabaseArr[0] = new FrameworkSQLiteDatabase(sQLiteDatabase);
            }
            return frameworkSQLiteDatabaseArr[0];
        }
    }
}
