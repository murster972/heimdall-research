package androidx.loader.app;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.collection.SparseArrayCompat;
import androidx.core.util.DebugUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

class LoaderManagerImpl extends LoaderManager {
    static boolean c = false;

    /* renamed from: a  reason: collision with root package name */
    private final LifecycleOwner f750a;
    private final LoaderViewModel b;

    static class LoaderViewModel extends ViewModel {
        private static final ViewModelProvider.Factory e = new ViewModelProvider.Factory() {
            public <T extends ViewModel> T a(Class<T> cls) {
                return new LoaderViewModel();
            }
        };
        private SparseArrayCompat<LoaderInfo> c = new SparseArrayCompat<>();
        private boolean d = false;

        LoaderViewModel() {
        }

        static LoaderViewModel a(ViewModelStore viewModelStore) {
            return (LoaderViewModel) new ViewModelProvider(viewModelStore, e).a(LoaderViewModel.class);
        }

        /* access modifiers changed from: package-private */
        public void b(int i) {
            this.c.d(i);
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.d = false;
        }

        /* access modifiers changed from: package-private */
        public boolean d() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public void e() {
            int b = this.c.b();
            for (int i = 0; i < b; i++) {
                this.c.e(i).f();
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.d = true;
        }

        /* access modifiers changed from: package-private */
        public void a(int i, LoaderInfo loaderInfo) {
            this.c.c(i, loaderInfo);
        }

        /* access modifiers changed from: protected */
        public void b() {
            super.b();
            int b = this.c.b();
            for (int i = 0; i < b; i++) {
                this.c.e(i).a(true);
            }
            this.c.a();
        }

        /* access modifiers changed from: package-private */
        public <D> LoaderInfo<D> a(int i) {
            return this.c.a(i);
        }

        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            if (this.c.b() > 0) {
                printWriter.print(str);
                printWriter.println("Loaders:");
                String str2 = str + "    ";
                for (int i = 0; i < this.c.b(); i++) {
                    LoaderInfo e2 = this.c.e(i);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(this.c.c(i));
                    printWriter.print(": ");
                    printWriter.println(e2.toString());
                    e2.a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
    }

    LoaderManagerImpl(LifecycleOwner lifecycleOwner, ViewModelStore viewModelStore) {
        this.f750a = lifecycleOwner;
        this.b = LoaderViewModel.a(viewModelStore);
    }

    /* JADX INFO: finally extract failed */
    private <D> Loader<D> a(int i, Bundle bundle, LoaderManager.LoaderCallbacks<D> loaderCallbacks, Loader<D> loader) {
        try {
            this.b.f();
            Loader<D> a2 = loaderCallbacks.a(i, bundle);
            if (a2 != null) {
                if (a2.getClass().isMemberClass()) {
                    if (!Modifier.isStatic(a2.getClass().getModifiers())) {
                        throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + a2);
                    }
                }
                LoaderInfo loaderInfo = new LoaderInfo(i, bundle, a2, loader);
                if (c) {
                    Log.v("LoaderManager", "  Created new loader " + loaderInfo);
                }
                this.b.a(i, loaderInfo);
                this.b.c();
                return loaderInfo.a(this.f750a, loaderCallbacks);
            }
            throw new IllegalArgumentException("Object returned from onCreateLoader must not be null");
        } catch (Throwable th) {
            this.b.c();
            throw th;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        DebugUtils.a(this.f750a, sb);
        sb.append("}}");
        return sb.toString();
    }

    static class LoaderObserver<D> implements Observer<D> {

        /* renamed from: a  reason: collision with root package name */
        private final Loader<D> f751a;
        private final LoaderManager.LoaderCallbacks<D> b;
        private boolean c = false;

        LoaderObserver(Loader<D> loader, LoaderManager.LoaderCallbacks<D> loaderCallbacks) {
            this.f751a = loader;
            this.b = loaderCallbacks;
        }

        public void a(D d) {
            if (LoaderManagerImpl.c) {
                Log.v("LoaderManager", "  onLoadFinished in " + this.f751a + ": " + this.f751a.a(d));
            }
            this.b.a(this.f751a, d);
            this.c = true;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (this.c) {
                if (LoaderManagerImpl.c) {
                    Log.v("LoaderManager", "  Resetting: " + this.f751a);
                }
                this.b.a(this.f751a);
            }
        }

        public String toString() {
            return this.b.toString();
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.c;
        }

        public void a(String str, PrintWriter printWriter) {
            printWriter.print(str);
            printWriter.print("mDeliveredData=");
            printWriter.println(this.c);
        }
    }

    public static class LoaderInfo<D> extends MutableLiveData<D> implements Loader.OnLoadCompleteListener<D> {
        private final int k;
        private final Bundle l;
        private final Loader<D> m;
        private LifecycleOwner n;
        private LoaderObserver<D> o;
        private Loader<D> p;

        LoaderInfo(int i, Bundle bundle, Loader<D> loader, Loader<D> loader2) {
            this.k = i;
            this.l = bundle;
            this.m = loader;
            this.p = loader2;
            this.m.a(i, this);
        }

        /* access modifiers changed from: package-private */
        public Loader<D> a(LifecycleOwner lifecycleOwner, LoaderManager.LoaderCallbacks<D> loaderCallbacks) {
            LoaderObserver<D> loaderObserver = new LoaderObserver<>(this.m, loaderCallbacks);
            a(lifecycleOwner, loaderObserver);
            LoaderObserver<D> loaderObserver2 = this.o;
            if (loaderObserver2 != null) {
                a(loaderObserver2);
            }
            this.n = lifecycleOwner;
            this.o = loaderObserver;
            return this.m;
        }

        public void b(D d) {
            super.b(d);
            Loader<D> loader = this.p;
            if (loader != null) {
                loader.n();
                this.p = null;
            }
        }

        /* access modifiers changed from: protected */
        public void c() {
            if (LoaderManagerImpl.c) {
                Log.v("LoaderManager", "  Starting: " + this);
            }
            this.m.p();
        }

        /* access modifiers changed from: protected */
        public void d() {
            if (LoaderManagerImpl.c) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.m.q();
        }

        /* access modifiers changed from: package-private */
        public Loader<D> e() {
            return this.m;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            LifecycleOwner lifecycleOwner = this.n;
            LoaderObserver<D> loaderObserver = this.o;
            if (lifecycleOwner != null && loaderObserver != null) {
                super.a(loaderObserver);
                a(lifecycleOwner, loaderObserver);
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.k);
            sb.append(" : ");
            DebugUtils.a(this.m, sb);
            sb.append("}}");
            return sb.toString();
        }

        public void a(Observer<? super D> observer) {
            super.a(observer);
            this.n = null;
            this.o = null;
        }

        /* access modifiers changed from: package-private */
        public Loader<D> a(boolean z) {
            if (LoaderManagerImpl.c) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.m.b();
            this.m.a();
            LoaderObserver<D> loaderObserver = this.o;
            if (loaderObserver != null) {
                a(loaderObserver);
                if (z) {
                    loaderObserver.b();
                }
            }
            this.m.a(this);
            if ((loaderObserver == null || loaderObserver.a()) && !z) {
                return this.m;
            }
            this.m.n();
            return this.p;
        }

        public void a(Loader<D> loader, D d) {
            if (LoaderManagerImpl.c) {
                Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                b(d);
                return;
            }
            if (LoaderManagerImpl.c) {
                Log.w("LoaderManager", "onLoadComplete was incorrectly called on a background thread");
            }
            a(d);
        }

        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.k);
            printWriter.print(" mArgs=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.m);
            Loader<D> loader = this.m;
            loader.a(str + "  ", fileDescriptor, printWriter, strArr);
            if (this.o != null) {
                printWriter.print(str);
                printWriter.print("mCallbacks=");
                printWriter.println(this.o);
                LoaderObserver<D> loaderObserver = this.o;
                loaderObserver.a(str + "  ", printWriter);
            }
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(e().a(a()));
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.println(b());
        }
    }

    public <D> Loader<D> a(int i, Bundle bundle, LoaderManager.LoaderCallbacks<D> loaderCallbacks) {
        if (this.b.d()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "restartLoader in " + this + ": args=" + bundle);
            }
            LoaderInfo a2 = this.b.a(i);
            Loader loader = null;
            if (a2 != null) {
                loader = a2.a(false);
            }
            return a(i, bundle, loaderCallbacks, loader);
        } else {
            throw new IllegalStateException("restartLoader must be called on the main thread");
        }
    }

    public void a(int i) {
        if (this.b.d()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "destroyLoader in " + this + " of " + i);
            }
            LoaderInfo a2 = this.b.a(i);
            if (a2 != null) {
                a2.a(true);
                this.b.b(i);
            }
        } else {
            throw new IllegalStateException("destroyLoader must be called on the main thread");
        }
    }

    public void a() {
        this.b.e();
    }

    @Deprecated
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.b.a(str, fileDescriptor, printWriter, strArr);
    }
}
