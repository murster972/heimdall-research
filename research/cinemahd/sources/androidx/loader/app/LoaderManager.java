package androidx.loader.app;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.loader.content.Loader;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class LoaderManager {

    public interface LoaderCallbacks<D> {
        Loader<D> a(int i, Bundle bundle);

        void a(Loader<D> loader);

        void a(Loader<D> loader, D d);
    }

    public static <T extends LifecycleOwner & ViewModelStoreOwner> LoaderManager a(T t) {
        return new LoaderManagerImpl(t, ((ViewModelStoreOwner) t).getViewModelStore());
    }

    public abstract <D> Loader<D> a(int i, Bundle bundle, LoaderCallbacks<D> loaderCallbacks);

    public abstract void a();

    public abstract void a(int i);

    @Deprecated
    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);
}
