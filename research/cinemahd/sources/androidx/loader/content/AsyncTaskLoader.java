package androidx.loader.content;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import androidx.core.os.OperationCanceledException;
import androidx.core.util.TimeUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

public abstract class AsyncTaskLoader<D> extends Loader<D> {
    private final Executor i;
    volatile AsyncTaskLoader<D>.LoadTask j;
    volatile AsyncTaskLoader<D>.LoadTask k;
    long l;
    long m;
    Handler n;

    final class LoadTask extends ModernAsyncTask<Void, Void, D> implements Runnable {
        private final CountDownLatch j = new CountDownLatch(1);
        boolean k;

        LoadTask() {
        }

        /* access modifiers changed from: protected */
        public void b(D d) {
            try {
                AsyncTaskLoader.this.a(this, d);
            } finally {
                this.j.countDown();
            }
        }

        /* access modifiers changed from: protected */
        public void c(D d) {
            try {
                AsyncTaskLoader.this.b(this, d);
            } finally {
                this.j.countDown();
            }
        }

        public void run() {
            this.k = false;
            AsyncTaskLoader.this.s();
        }

        /* access modifiers changed from: protected */
        public D a(Void... voidArr) {
            try {
                return AsyncTaskLoader.this.u();
            } catch (OperationCanceledException e) {
                if (a()) {
                    return null;
                }
                throw e;
            }
        }
    }

    public AsyncTaskLoader(Context context) {
        this(context, ModernAsyncTask.h);
    }

    /* access modifiers changed from: package-private */
    public void a(AsyncTaskLoader<D>.LoadTask loadTask, D d) {
        c(d);
        if (this.k == loadTask) {
            o();
            this.m = SystemClock.uptimeMillis();
            this.k = null;
            d();
            s();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(AsyncTaskLoader<D>.LoadTask loadTask, D d) {
        if (this.j != loadTask) {
            a(loadTask, d);
        } else if (f()) {
            c(d);
        } else {
            c();
            this.m = SystemClock.uptimeMillis();
            this.j = null;
            b(d);
        }
    }

    public void c(D d) {
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        if (this.j == null) {
            return false;
        }
        if (!this.d) {
            this.g = true;
        }
        if (this.k != null) {
            if (this.j.k) {
                this.j.k = false;
                this.n.removeCallbacks(this.j);
            }
            this.j = null;
            return false;
        } else if (this.j.k) {
            this.j.k = false;
            this.n.removeCallbacks(this.j);
            this.j = null;
            return false;
        } else {
            boolean a2 = this.j.a(false);
            if (a2) {
                this.k = this.j;
                r();
            }
            this.j = null;
            return a2;
        }
    }

    /* access modifiers changed from: protected */
    public void j() {
        super.j();
        b();
        this.j = new LoadTask();
        s();
    }

    public void r() {
    }

    /* access modifiers changed from: package-private */
    public void s() {
        if (this.k == null && this.j != null) {
            if (this.j.k) {
                this.j.k = false;
                this.n.removeCallbacks(this.j);
            }
            if (this.l <= 0 || SystemClock.uptimeMillis() >= this.m + this.l) {
                this.j.a(this.i, (Params[]) null);
                return;
            }
            this.j.k = true;
            this.n.postAtTime(this.j, this.m + this.l);
        }
    }

    public abstract D t();

    /* access modifiers changed from: protected */
    public D u() {
        return t();
    }

    private AsyncTaskLoader(Context context, Executor executor) {
        super(context);
        this.m = -10000;
        this.i = executor;
    }

    @Deprecated
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.a(str, fileDescriptor, printWriter, strArr);
        if (this.j != null) {
            printWriter.print(str);
            printWriter.print("mTask=");
            printWriter.print(this.j);
            printWriter.print(" waiting=");
            printWriter.println(this.j.k);
        }
        if (this.k != null) {
            printWriter.print(str);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.k);
            printWriter.print(" waiting=");
            printWriter.println(this.k.k);
        }
        if (this.l != 0) {
            printWriter.print(str);
            printWriter.print("mUpdateThrottle=");
            TimeUtils.a(this.l, printWriter);
            printWriter.print(" mLastLoadCompleteTime=");
            TimeUtils.a(this.m, SystemClock.uptimeMillis(), printWriter);
            printWriter.println();
        }
    }
}
