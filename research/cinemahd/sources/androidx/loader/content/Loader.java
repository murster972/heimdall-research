package androidx.loader.content;

import android.content.Context;
import androidx.core.util.DebugUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class Loader<D> {

    /* renamed from: a  reason: collision with root package name */
    int f752a;
    OnLoadCompleteListener<D> b;
    OnLoadCanceledListener<D> c;
    boolean d = false;
    boolean e = false;
    boolean f = true;
    boolean g = false;
    boolean h = false;

    public interface OnLoadCanceledListener<D> {
        void a(Loader<D> loader);
    }

    public interface OnLoadCompleteListener<D> {
        void a(Loader<D> loader, D d);
    }

    public Loader(Context context) {
        context.getApplicationContext();
    }

    public void a(int i, OnLoadCompleteListener<D> onLoadCompleteListener) {
        if (this.b == null) {
            this.b = onLoadCompleteListener;
            this.f752a = i;
            return;
        }
        throw new IllegalStateException("There is already a listener registered");
    }

    public void b(D d2) {
        OnLoadCompleteListener<D> onLoadCompleteListener = this.b;
        if (onLoadCompleteListener != null) {
            onLoadCompleteListener.a(this, d2);
        }
    }

    public void c() {
        this.h = false;
    }

    public void d() {
        OnLoadCanceledListener<D> onLoadCanceledListener = this.c;
        if (onLoadCanceledListener != null) {
            onLoadCanceledListener.a(this);
        }
    }

    public void e() {
        j();
    }

    public boolean f() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public void g() {
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        throw null;
    }

    public void i() {
        if (this.d) {
            e();
        } else {
            this.g = true;
        }
    }

    /* access modifiers changed from: protected */
    public void j() {
    }

    /* access modifiers changed from: protected */
    public void k() {
    }

    /* access modifiers changed from: protected */
    public void l() {
    }

    /* access modifiers changed from: protected */
    public void m() {
    }

    public void n() {
        k();
        this.f = true;
        this.d = false;
        this.e = false;
        this.g = false;
        this.h = false;
    }

    public void o() {
        if (this.h) {
            i();
        }
    }

    public final void p() {
        this.d = true;
        this.f = false;
        this.e = false;
        l();
    }

    public void q() {
        this.d = false;
        m();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        DebugUtils.a(this, sb);
        sb.append(" id=");
        sb.append(this.f752a);
        sb.append("}");
        return sb.toString();
    }

    public boolean b() {
        return h();
    }

    public void a(OnLoadCompleteListener<D> onLoadCompleteListener) {
        OnLoadCompleteListener<D> onLoadCompleteListener2 = this.b;
        if (onLoadCompleteListener2 == null) {
            throw new IllegalStateException("No listener register");
        } else if (onLoadCompleteListener2 == onLoadCompleteListener) {
            this.b = null;
        } else {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }

    public void a() {
        this.e = true;
        g();
    }

    public String a(D d2) {
        StringBuilder sb = new StringBuilder(64);
        DebugUtils.a(d2, sb);
        sb.append("}");
        return sb.toString();
    }

    @Deprecated
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.f752a);
        printWriter.print(" mListener=");
        printWriter.println(this.b);
        if (this.d || this.g || this.h) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.d);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.g);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.h);
        }
        if (this.e || this.f) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.e);
            printWriter.print(" mReset=");
            printWriter.println(this.f);
        }
    }
}
