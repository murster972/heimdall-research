package androidx.appcompat.resources;

public final class R$drawable {
    public static final int abc_vector_test = 2131230822;
    public static final int notification_action_background = 2131231305;
    public static final int notification_bg = 2131231306;
    public static final int notification_bg_low = 2131231307;
    public static final int notification_bg_low_normal = 2131231308;
    public static final int notification_bg_low_pressed = 2131231309;
    public static final int notification_bg_normal = 2131231310;
    public static final int notification_bg_normal_pressed = 2131231311;
    public static final int notification_icon_background = 2131231312;
    public static final int notification_template_icon_bg = 2131231313;
    public static final int notification_template_icon_low_bg = 2131231314;
    public static final int notification_tile_bg = 2131231315;
    public static final int notify_panel_notification_icon_bg = 2131231316;

    private R$drawable() {
    }
}
