package androidx.appcompat.app;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.WindowCallbackWrapper;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.widget.DecorToolbar;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.ToolbarWidgetWrapper;
import androidx.core.view.ViewCompat;
import java.util.ArrayList;

class ToolbarActionBar extends ActionBar {

    /* renamed from: a  reason: collision with root package name */
    DecorToolbar f278a;
    boolean b;
    Window.Callback c;
    private boolean d;
    private boolean e;
    private ArrayList<ActionBar.OnMenuVisibilityListener> f = new ArrayList<>();
    private final Runnable g = new Runnable() {
        public void run() {
            ToolbarActionBar.this.m();
        }
    };
    private final Toolbar.OnMenuItemClickListener h = new Toolbar.OnMenuItemClickListener() {
        public boolean onMenuItemClick(MenuItem menuItem) {
            return ToolbarActionBar.this.c.onMenuItemSelected(0, menuItem);
        }
    };

    private final class MenuBuilderCallback implements MenuBuilder.Callback {
        MenuBuilderCallback() {
        }

        public void a(MenuBuilder menuBuilder) {
            ToolbarActionBar toolbarActionBar = ToolbarActionBar.this;
            if (toolbarActionBar.c == null) {
                return;
            }
            if (toolbarActionBar.f278a.d()) {
                ToolbarActionBar.this.c.onPanelClosed(108, menuBuilder);
            } else if (ToolbarActionBar.this.c.onPreparePanel(0, (View) null, menuBuilder)) {
                ToolbarActionBar.this.c.onMenuOpened(108, menuBuilder);
            }
        }

        public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
            return false;
        }
    }

    private class ToolbarCallbackWrapper extends WindowCallbackWrapper {
        public ToolbarCallbackWrapper(Window.Callback callback) {
            super(callback);
        }

        public View onCreatePanelView(int i) {
            if (i == 0) {
                return new View(ToolbarActionBar.this.f278a.getContext());
            }
            return super.onCreatePanelView(i);
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel) {
                ToolbarActionBar toolbarActionBar = ToolbarActionBar.this;
                if (!toolbarActionBar.b) {
                    toolbarActionBar.f278a.e();
                    ToolbarActionBar.this.b = true;
                }
            }
            return onPreparePanel;
        }
    }

    ToolbarActionBar(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.f278a = new ToolbarWidgetWrapper(toolbar, false);
        this.c = new ToolbarCallbackWrapper(callback);
        this.f278a.setWindowCallback(this.c);
        toolbar.setOnMenuItemClickListener(this.h);
        this.f278a.setWindowTitle(charSequence);
    }

    private Menu n() {
        if (!this.d) {
            this.f278a.a((MenuPresenter.Callback) new ActionMenuPresenterCallback(), (MenuBuilder.Callback) new MenuBuilderCallback());
            this.d = true;
        }
        return this.f278a.h();
    }

    public void a(Drawable drawable) {
        this.f278a.a(drawable);
    }

    public void b(int i) {
        this.f278a.d(i);
    }

    public void c(int i) {
        DecorToolbar decorToolbar = this.f278a;
        decorToolbar.setTitle(i != 0 ? decorToolbar.getContext().getText(i) : null);
    }

    public void c(boolean z) {
    }

    public void d(boolean z) {
        a(z ? 4 : 0, 4);
    }

    public void e(boolean z) {
        a(z ? 2 : 0, 2);
    }

    public void f(boolean z) {
        a(z ? 8 : 0, 8);
    }

    public int g() {
        return this.f278a.m();
    }

    public void g(boolean z) {
    }

    public Context h() {
        return this.f278a.getContext();
    }

    public void h(boolean z) {
    }

    public boolean i() {
        this.f278a.j().removeCallbacks(this.g);
        ViewCompat.a((View) this.f278a.j(), this.g);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        this.f278a.j().removeCallbacks(this.g);
    }

    public boolean k() {
        return this.f278a.c();
    }

    public Window.Callback l() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void m() {
        Menu n = n();
        MenuBuilder menuBuilder = n instanceof MenuBuilder ? (MenuBuilder) n : null;
        if (menuBuilder != null) {
            menuBuilder.s();
        }
        try {
            n.clear();
            if (!this.c.onCreatePanelMenu(0, n) || !this.c.onPreparePanel(0, (View) null, n)) {
                n.clear();
            }
        } finally {
            if (menuBuilder != null) {
                menuBuilder.r();
            }
        }
    }

    private final class ActionMenuPresenterCallback implements MenuPresenter.Callback {

        /* renamed from: a  reason: collision with root package name */
        private boolean f281a;

        ActionMenuPresenterCallback() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback callback = ToolbarActionBar.this.c;
            if (callback == null) {
                return false;
            }
            callback.onMenuOpened(108, menuBuilder);
            return true;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (!this.f281a) {
                this.f281a = true;
                ToolbarActionBar.this.f278a.l();
                Window.Callback callback = ToolbarActionBar.this.c;
                if (callback != null) {
                    callback.onPanelClosed(108, menuBuilder);
                }
                this.f281a = false;
            }
        }
    }

    public void a(int i) {
        this.f278a.b(i);
    }

    public void b(CharSequence charSequence) {
        this.f278a.setTitle(charSequence);
    }

    public void c(CharSequence charSequence) {
        this.f278a.setWindowTitle(charSequence);
    }

    public boolean e() {
        return this.f278a.b();
    }

    public boolean f() {
        if (!this.f278a.g()) {
            return false;
        }
        this.f278a.collapseActionView();
        return true;
    }

    public void a(Configuration configuration) {
        super.a(configuration);
    }

    public void b(boolean z) {
        if (z != this.e) {
            this.e = z;
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                this.f.get(i).a(z);
            }
        }
    }

    public void a(CharSequence charSequence) {
        this.f278a.a(charSequence);
    }

    public void a(int i, int i2) {
        this.f278a.a((i & i2) | ((~i2) & this.f278a.m()));
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            k();
        }
        return true;
    }

    public boolean a(int i, KeyEvent keyEvent) {
        Menu n = n();
        if (n == null) {
            return false;
        }
        boolean z = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z = false;
        }
        n.setQwertyMode(z);
        return n.performShortcut(i, keyEvent, 0);
    }
}
