package androidx.appcompat.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.R$styleable;
import androidx.appcompat.view.ActionMode;

public abstract class ActionBar {

    public interface OnMenuVisibilityListener {
        void a(boolean z);
    }

    @Deprecated
    public static abstract class Tab {
        public abstract CharSequence a();

        public abstract View b();

        public abstract Drawable c();

        public abstract CharSequence d();

        public abstract void e();
    }

    public ActionMode a(ActionMode.Callback callback) {
        return null;
    }

    public void a(int i) {
    }

    public abstract void a(int i, int i2);

    public void a(Configuration configuration) {
    }

    public void a(Drawable drawable) {
    }

    public abstract void a(CharSequence charSequence);

    public boolean a(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean a(KeyEvent keyEvent) {
        return false;
    }

    public void b(int i) {
    }

    public abstract void b(CharSequence charSequence);

    public void b(boolean z) {
    }

    public abstract void c(int i);

    public void c(CharSequence charSequence) {
    }

    public void c(boolean z) {
    }

    public abstract void d(boolean z);

    public abstract void e(boolean z);

    public boolean e() {
        return false;
    }

    public abstract void f(boolean z);

    public boolean f() {
        return false;
    }

    public abstract int g();

    public void g(boolean z) {
    }

    public Context h() {
        return null;
    }

    public void h(boolean z) {
    }

    public boolean i() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void j() {
    }

    public boolean k() {
        return false;
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {

        /* renamed from: a  reason: collision with root package name */
        public int f235a;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f235a = 0;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.ActionBarLayout);
            this.f235a = obtainStyledAttributes.getInt(R$styleable.ActionBarLayout_android_layout_gravity, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.f235a = 0;
            this.f235a = 8388627;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super(layoutParams);
            this.f235a = 0;
            this.f235a = layoutParams.f235a;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.f235a = 0;
        }
    }
}
