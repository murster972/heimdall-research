package androidx.appcompat.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import androidx.appcompat.app.ActionBarDrawerToggleHoneycomb;
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

public class ActionBarDrawerToggle implements DrawerLayout.DrawerListener {

    /* renamed from: a  reason: collision with root package name */
    private final Delegate f236a;
    private final DrawerLayout b;
    private DrawerArrowDrawable c;
    private boolean d;
    boolean e;
    private final int f;
    private final int g;
    View.OnClickListener h;
    private boolean i;

    public interface Delegate {
        Context a();

        void a(int i);

        void a(Drawable drawable, int i);

        boolean b();

        Drawable c();
    }

    public interface DelegateProvider {
        Delegate getDrawerToggleDelegate();
    }

    public ActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int i2, int i3) {
        this(activity, toolbar, drawerLayout, (DrawerArrowDrawable) null, i2, i3);
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable, int i2) {
        if (!this.i && !this.f236a.b()) {
            Log.w("ActionBarDrawerToggle", "DrawerToggle may not show up because NavigationIcon is not visible. You may need to call actionbar.setDisplayHomeAsUpEnabled(true);");
            this.i = true;
        }
        this.f236a.a(drawable, i2);
    }

    public void b() {
        if (this.b.isDrawerOpen(8388611)) {
            a(1.0f);
        } else {
            a(0.0f);
        }
        if (this.e) {
            a(this.c, this.b.isDrawerOpen(8388611) ? this.g : this.f);
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        int drawerLockMode = this.b.getDrawerLockMode(8388611);
        if (this.b.isDrawerVisible(8388611) && drawerLockMode != 2) {
            this.b.closeDrawer(8388611);
        } else if (drawerLockMode != 1) {
            this.b.openDrawer(8388611);
        }
    }

    public void onDrawerClosed(View view) {
        a(0.0f);
        if (this.e) {
            a(this.f);
        }
    }

    public void onDrawerOpened(View view) {
        a(1.0f);
        if (this.e) {
            a(this.g);
        }
    }

    public void onDrawerSlide(View view, float f2) {
        if (this.d) {
            a(Math.min(1.0f, Math.max(0.0f, f2)));
        } else {
            a(0.0f);
        }
    }

    public void onDrawerStateChanged(int i2) {
    }

    static class ToolbarCompatDelegate implements Delegate {

        /* renamed from: a  reason: collision with root package name */
        final Toolbar f239a;
        final Drawable b;
        final CharSequence c;

        ToolbarCompatDelegate(Toolbar toolbar) {
            this.f239a = toolbar;
            this.b = toolbar.getNavigationIcon();
            this.c = toolbar.getNavigationContentDescription();
        }

        public void a(Drawable drawable, int i) {
            this.f239a.setNavigationIcon(drawable);
            a(i);
        }

        public boolean b() {
            return true;
        }

        public Drawable c() {
            return this.b;
        }

        public void a(int i) {
            if (i == 0) {
                this.f239a.setNavigationContentDescription(this.c);
            } else {
                this.f239a.setNavigationContentDescription(i);
            }
        }

        public Context a() {
            return this.f239a.getContext();
        }
    }

    ActionBarDrawerToggle(Activity activity, Toolbar toolbar, DrawerLayout drawerLayout, DrawerArrowDrawable drawerArrowDrawable, int i2, int i3) {
        this.d = true;
        this.e = true;
        this.i = false;
        if (toolbar != null) {
            this.f236a = new ToolbarCompatDelegate(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ActionBarDrawerToggle actionBarDrawerToggle = ActionBarDrawerToggle.this;
                    if (actionBarDrawerToggle.e) {
                        actionBarDrawerToggle.c();
                        return;
                    }
                    View.OnClickListener onClickListener = actionBarDrawerToggle.h;
                    if (onClickListener != null) {
                        onClickListener.onClick(view);
                    }
                }
            });
        } else if (activity instanceof DelegateProvider) {
            this.f236a = ((DelegateProvider) activity).getDrawerToggleDelegate();
        } else {
            this.f236a = new FrameworkActionBarDelegate(activity);
        }
        this.b = drawerLayout;
        this.f = i2;
        this.g = i3;
        if (drawerArrowDrawable == null) {
            this.c = new DrawerArrowDrawable(this.f236a.a());
        } else {
            this.c = drawerArrowDrawable;
        }
        a();
    }

    private static class FrameworkActionBarDelegate implements Delegate {

        /* renamed from: a  reason: collision with root package name */
        private final Activity f238a;
        private ActionBarDrawerToggleHoneycomb.SetIndicatorInfo b;

        FrameworkActionBarDelegate(Activity activity) {
            this.f238a = activity;
        }

        public Context a() {
            ActionBar actionBar = this.f238a.getActionBar();
            if (actionBar != null) {
                return actionBar.getThemedContext();
            }
            return this.f238a;
        }

        public boolean b() {
            ActionBar actionBar = this.f238a.getActionBar();
            return (actionBar == null || (actionBar.getDisplayOptions() & 4) == 0) ? false : true;
        }

        public Drawable c() {
            if (Build.VERSION.SDK_INT < 18) {
                return ActionBarDrawerToggleHoneycomb.a(this.f238a);
            }
            TypedArray obtainStyledAttributes = a().obtainStyledAttributes((AttributeSet) null, new int[]{16843531}, 16843470, 0);
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            obtainStyledAttributes.recycle();
            return drawable;
        }

        public void a(Drawable drawable, int i) {
            ActionBar actionBar = this.f238a.getActionBar();
            if (actionBar == null) {
                return;
            }
            if (Build.VERSION.SDK_INT >= 18) {
                actionBar.setHomeAsUpIndicator(drawable);
                actionBar.setHomeActionContentDescription(i);
                return;
            }
            actionBar.setDisplayShowHomeEnabled(true);
            this.b = ActionBarDrawerToggleHoneycomb.a(this.f238a, drawable, i);
            actionBar.setDisplayShowHomeEnabled(false);
        }

        public void a(int i) {
            if (Build.VERSION.SDK_INT >= 18) {
                ActionBar actionBar = this.f238a.getActionBar();
                if (actionBar != null) {
                    actionBar.setHomeActionContentDescription(i);
                    return;
                }
                return;
            }
            this.b = ActionBarDrawerToggleHoneycomb.a(this.b, this.f238a, i);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.f236a.a(i2);
    }

    /* access modifiers changed from: package-private */
    public Drawable a() {
        return this.f236a.c();
    }

    private void a(float f2) {
        if (f2 == 1.0f) {
            this.c.b(true);
        } else if (f2 == 0.0f) {
            this.c.b(false);
        }
        this.c.c(f2);
    }
}
