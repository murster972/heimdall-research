package androidx.appcompat.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.R$attr;
import androidx.appcompat.R$id;
import androidx.appcompat.R$styleable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.ActionBarPolicy;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.view.SupportMenuInflater;
import androidx.appcompat.view.ViewPropertyAnimatorCompatSet;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import androidx.appcompat.widget.DecorToolbar;
import androidx.appcompat.widget.ScrollingTabContainerView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.core.view.ViewPropertyAnimatorListenerAdapter;
import androidx.core.view.ViewPropertyAnimatorUpdateListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class WindowDecorActionBar extends ActionBar implements ActionBarOverlayLayout.ActionBarVisibilityCallback {
    private static final Interpolator B = new AccelerateInterpolator();
    private static final Interpolator C = new DecelerateInterpolator();
    final ViewPropertyAnimatorUpdateListener A = new ViewPropertyAnimatorUpdateListener() {
        public void a(View view) {
            ((View) WindowDecorActionBar.this.d.getParent()).invalidate();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    Context f286a;
    private Context b;
    ActionBarOverlayLayout c;
    ActionBarContainer d;
    DecorToolbar e;
    ActionBarContextView f;
    View g;
    ScrollingTabContainerView h;
    private boolean i;
    ActionModeImpl j;
    ActionMode k;
    ActionMode.Callback l;
    private boolean m;
    private ArrayList<ActionBar.OnMenuVisibilityListener> n = new ArrayList<>();
    private boolean o;
    private int p = 0;
    boolean q = true;
    boolean r;
    boolean s;
    private boolean t;
    private boolean u = true;
    ViewPropertyAnimatorCompatSet v;
    private boolean w;
    boolean x;
    final ViewPropertyAnimatorListener y = new ViewPropertyAnimatorListenerAdapter() {
        public void onAnimationEnd(View view) {
            View view2;
            WindowDecorActionBar windowDecorActionBar = WindowDecorActionBar.this;
            if (windowDecorActionBar.q && (view2 = windowDecorActionBar.g) != null) {
                view2.setTranslationY(0.0f);
                WindowDecorActionBar.this.d.setTranslationY(0.0f);
            }
            WindowDecorActionBar.this.d.setVisibility(8);
            WindowDecorActionBar.this.d.setTransitioning(false);
            WindowDecorActionBar windowDecorActionBar2 = WindowDecorActionBar.this;
            windowDecorActionBar2.v = null;
            windowDecorActionBar2.l();
            ActionBarOverlayLayout actionBarOverlayLayout = WindowDecorActionBar.this.c;
            if (actionBarOverlayLayout != null) {
                ViewCompat.J(actionBarOverlayLayout);
            }
        }
    };
    final ViewPropertyAnimatorListener z = new ViewPropertyAnimatorListenerAdapter() {
        public void onAnimationEnd(View view) {
            WindowDecorActionBar windowDecorActionBar = WindowDecorActionBar.this;
            windowDecorActionBar.v = null;
            windowDecorActionBar.d.requestLayout();
        }
    };

    public class ActionModeImpl extends ActionMode implements MenuBuilder.Callback {
        private final Context c;
        private final MenuBuilder d;
        private ActionMode.Callback e;
        private WeakReference<View> f;

        public ActionModeImpl(Context context, ActionMode.Callback callback) {
            this.c = context;
            this.e = callback;
            MenuBuilder menuBuilder = new MenuBuilder(context);
            menuBuilder.c(1);
            this.d = menuBuilder;
            this.d.a((MenuBuilder.Callback) this);
        }

        public void a() {
            WindowDecorActionBar windowDecorActionBar = WindowDecorActionBar.this;
            if (windowDecorActionBar.j == this) {
                if (!WindowDecorActionBar.a(windowDecorActionBar.r, windowDecorActionBar.s, false)) {
                    WindowDecorActionBar windowDecorActionBar2 = WindowDecorActionBar.this;
                    windowDecorActionBar2.k = this;
                    windowDecorActionBar2.l = this.e;
                } else {
                    this.e.a(this);
                }
                this.e = null;
                WindowDecorActionBar.this.i(false);
                WindowDecorActionBar.this.f.a();
                WindowDecorActionBar.this.e.j().sendAccessibilityEvent(32);
                WindowDecorActionBar windowDecorActionBar3 = WindowDecorActionBar.this;
                windowDecorActionBar3.c.setHideOnContentScrollEnabled(windowDecorActionBar3.x);
                WindowDecorActionBar.this.j = null;
            }
        }

        public void b(CharSequence charSequence) {
            WindowDecorActionBar.this.f.setTitle(charSequence);
        }

        public Menu c() {
            return this.d;
        }

        public MenuInflater d() {
            return new SupportMenuInflater(this.c);
        }

        public CharSequence e() {
            return WindowDecorActionBar.this.f.getSubtitle();
        }

        public CharSequence g() {
            return WindowDecorActionBar.this.f.getTitle();
        }

        public void i() {
            if (WindowDecorActionBar.this.j == this) {
                this.d.s();
                try {
                    this.e.b(this, this.d);
                } finally {
                    this.d.r();
                }
            }
        }

        public boolean j() {
            return WindowDecorActionBar.this.f.b();
        }

        public boolean k() {
            this.d.s();
            try {
                return this.e.a((ActionMode) this, (Menu) this.d);
            } finally {
                this.d.r();
            }
        }

        public void b(int i) {
            b((CharSequence) WindowDecorActionBar.this.f286a.getResources().getString(i));
        }

        public View b() {
            WeakReference<View> weakReference = this.f;
            if (weakReference != null) {
                return (View) weakReference.get();
            }
            return null;
        }

        public void a(View view) {
            WindowDecorActionBar.this.f.setCustomView(view);
            this.f = new WeakReference<>(view);
        }

        public void a(CharSequence charSequence) {
            WindowDecorActionBar.this.f.setSubtitle(charSequence);
        }

        public void a(int i) {
            a((CharSequence) WindowDecorActionBar.this.f286a.getResources().getString(i));
        }

        public void a(boolean z) {
            super.a(z);
            WindowDecorActionBar.this.f.setTitleOptional(z);
        }

        public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
            ActionMode.Callback callback = this.e;
            if (callback != null) {
                return callback.a((ActionMode) this, menuItem);
            }
            return false;
        }

        public void a(MenuBuilder menuBuilder) {
            if (this.e != null) {
                i();
                WindowDecorActionBar.this.f.d();
            }
        }
    }

    public WindowDecorActionBar(Activity activity, boolean z2) {
        new ArrayList();
        View decorView = activity.getWindow().getDecorView();
        b(decorView);
        if (!z2) {
            this.g = decorView.findViewById(16908290);
        }
    }

    private DecorToolbar a(View view) {
        if (view instanceof DecorToolbar) {
            return (DecorToolbar) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view != null ? view.getClass().getSimpleName() : "null");
        throw new IllegalStateException(sb.toString());
    }

    static boolean a(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        return !z2 && !z3;
    }

    private void b(View view) {
        this.c = (ActionBarOverlayLayout) view.findViewById(R$id.decor_content_parent);
        ActionBarOverlayLayout actionBarOverlayLayout = this.c;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.e = a(view.findViewById(R$id.action_bar));
        this.f = (ActionBarContextView) view.findViewById(R$id.action_context_bar);
        this.d = (ActionBarContainer) view.findViewById(R$id.action_bar_container);
        DecorToolbar decorToolbar = this.e;
        if (decorToolbar == null || this.f == null || this.d == null) {
            throw new IllegalStateException(WindowDecorActionBar.class.getSimpleName() + " can only be used with a compatible window decor layout");
        }
        this.f286a = decorToolbar.getContext();
        boolean z2 = (this.e.m() & 4) != 0;
        if (z2) {
            this.i = true;
        }
        ActionBarPolicy a2 = ActionBarPolicy.a(this.f286a);
        g(a2.a() || z2);
        m(a2.f());
        TypedArray obtainStyledAttributes = this.f286a.obtainStyledAttributes((AttributeSet) null, R$styleable.ActionBar, R$attr.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(R$styleable.ActionBar_hideOnContentScroll, false)) {
            l(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R$styleable.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    private void m(boolean z2) {
        this.o = z2;
        if (!this.o) {
            this.e.a((ScrollingTabContainerView) null);
            this.d.setTabContainer(this.h);
        } else {
            this.d.setTabContainer((ScrollingTabContainerView) null);
            this.e.a(this.h);
        }
        boolean z3 = true;
        boolean z4 = m() == 2;
        ScrollingTabContainerView scrollingTabContainerView = this.h;
        if (scrollingTabContainerView != null) {
            if (z4) {
                scrollingTabContainerView.setVisibility(0);
                ActionBarOverlayLayout actionBarOverlayLayout = this.c;
                if (actionBarOverlayLayout != null) {
                    ViewCompat.J(actionBarOverlayLayout);
                }
            } else {
                scrollingTabContainerView.setVisibility(8);
            }
        }
        this.e.b(!this.o && z4);
        ActionBarOverlayLayout actionBarOverlayLayout2 = this.c;
        if (this.o || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout2.setHasNonEmbeddedTabs(z3);
    }

    private void n() {
        if (this.t) {
            this.t = false;
            ActionBarOverlayLayout actionBarOverlayLayout = this.c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(false);
            }
            n(false);
        }
    }

    private boolean o() {
        return ViewCompat.E(this.d);
    }

    private void p() {
        if (!this.t) {
            this.t = true;
            ActionBarOverlayLayout actionBarOverlayLayout = this.c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(true);
            }
            n(false);
        }
    }

    public void b() {
    }

    public void c(int i2) {
        b((CharSequence) this.f286a.getString(i2));
    }

    public void d(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    public void e(boolean z2) {
        a(z2 ? 2 : 0, 2);
    }

    public void f(boolean z2) {
        a(z2 ? 8 : 0, 8);
    }

    public void g(boolean z2) {
        this.e.a(z2);
    }

    public void h(boolean z2) {
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet;
        this.w = z2;
        if (!z2 && (viewPropertyAnimatorCompatSet = this.v) != null) {
            viewPropertyAnimatorCompatSet.a();
        }
    }

    public void i(boolean z2) {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat;
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2;
        if (z2) {
            p();
        } else {
            n();
        }
        if (o()) {
            if (z2) {
                viewPropertyAnimatorCompat = this.e.a(4, 100);
                viewPropertyAnimatorCompat2 = this.f.a(0, 200);
            } else {
                viewPropertyAnimatorCompat2 = this.e.a(0, 200);
                viewPropertyAnimatorCompat = this.f.a(8, 100);
            }
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
            viewPropertyAnimatorCompatSet.a(viewPropertyAnimatorCompat, viewPropertyAnimatorCompat2);
            viewPropertyAnimatorCompatSet.c();
        } else if (z2) {
            this.e.e(4);
            this.f.setVisibility(0);
        } else {
            this.e.e(0);
            this.f.setVisibility(8);
        }
    }

    public void j(boolean z2) {
        View view;
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = this.v;
        if (viewPropertyAnimatorCompatSet != null) {
            viewPropertyAnimatorCompatSet.a();
        }
        if (this.p != 0 || (!this.w && !z2)) {
            this.y.onAnimationEnd((View) null);
            return;
        }
        this.d.setAlpha(1.0f);
        this.d.setTransitioning(true);
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet2 = new ViewPropertyAnimatorCompatSet();
        float f2 = (float) (-this.d.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.d.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.d);
        a2.d(f2);
        a2.a(this.A);
        viewPropertyAnimatorCompatSet2.a(a2);
        if (this.q && (view = this.g) != null) {
            ViewPropertyAnimatorCompat a3 = ViewCompat.a(view);
            a3.d(f2);
            viewPropertyAnimatorCompatSet2.a(a3);
        }
        viewPropertyAnimatorCompatSet2.a(B);
        viewPropertyAnimatorCompatSet2.a(250);
        viewPropertyAnimatorCompatSet2.a(this.y);
        this.v = viewPropertyAnimatorCompatSet2;
        viewPropertyAnimatorCompatSet2.c();
    }

    public void k(boolean z2) {
        View view;
        View view2;
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = this.v;
        if (viewPropertyAnimatorCompatSet != null) {
            viewPropertyAnimatorCompatSet.a();
        }
        this.d.setVisibility(0);
        if (this.p != 0 || (!this.w && !z2)) {
            this.d.setAlpha(1.0f);
            this.d.setTranslationY(0.0f);
            if (this.q && (view = this.g) != null) {
                view.setTranslationY(0.0f);
            }
            this.z.onAnimationEnd((View) null);
        } else {
            this.d.setTranslationY(0.0f);
            float f2 = (float) (-this.d.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.d.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            this.d.setTranslationY(f2);
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet2 = new ViewPropertyAnimatorCompatSet();
            ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.d);
            a2.d(0.0f);
            a2.a(this.A);
            viewPropertyAnimatorCompatSet2.a(a2);
            if (this.q && (view2 = this.g) != null) {
                view2.setTranslationY(f2);
                ViewPropertyAnimatorCompat a3 = ViewCompat.a(this.g);
                a3.d(0.0f);
                viewPropertyAnimatorCompatSet2.a(a3);
            }
            viewPropertyAnimatorCompatSet2.a(C);
            viewPropertyAnimatorCompatSet2.a(250);
            viewPropertyAnimatorCompatSet2.a(this.z);
            this.v = viewPropertyAnimatorCompatSet2;
            viewPropertyAnimatorCompatSet2.c();
        }
        ActionBarOverlayLayout actionBarOverlayLayout = this.c;
        if (actionBarOverlayLayout != null) {
            ViewCompat.J(actionBarOverlayLayout);
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        ActionMode.Callback callback = this.l;
        if (callback != null) {
            callback.a(this.k);
            this.k = null;
            this.l = null;
        }
    }

    public void onWindowVisibilityChanged(int i2) {
        this.p = i2;
    }

    public void c(CharSequence charSequence) {
        this.e.setWindowTitle(charSequence);
    }

    public void d() {
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = this.v;
        if (viewPropertyAnimatorCompatSet != null) {
            viewPropertyAnimatorCompatSet.a();
            this.v = null;
        }
    }

    public boolean f() {
        DecorToolbar decorToolbar = this.e;
        if (decorToolbar == null || !decorToolbar.g()) {
            return false;
        }
        this.e.collapseActionView();
        return true;
    }

    public int g() {
        return this.e.m();
    }

    public void c() {
        if (!this.s) {
            this.s = true;
            n(true);
        }
    }

    public Context h() {
        if (this.b == null) {
            TypedValue typedValue = new TypedValue();
            this.f286a.getTheme().resolveAttribute(R$attr.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.b = new ContextThemeWrapper(this.f286a, i2);
            } else {
                this.b = this.f286a;
            }
        }
        return this.b;
    }

    public void l(boolean z2) {
        if (!z2 || this.c.i()) {
            this.x = z2;
            this.c.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    private void n(boolean z2) {
        if (a(this.r, this.s, this.t)) {
            if (!this.u) {
                this.u = true;
                k(z2);
            }
        } else if (this.u) {
            this.u = false;
            j(z2);
        }
    }

    public void c(boolean z2) {
        if (!this.i) {
            d(z2);
        }
    }

    public void a(float f2) {
        ViewCompat.b((View) this.d, f2);
    }

    public void a(Configuration configuration) {
        m(ActionBarPolicy.a(this.f286a).f());
    }

    public void a(CharSequence charSequence) {
        this.e.a(charSequence);
    }

    public void a(int i2, int i3) {
        int m2 = this.e.m();
        if ((i3 & 4) != 0) {
            this.i = true;
        }
        this.e.a((i2 & i3) | ((~i3) & m2));
    }

    public ActionMode a(ActionMode.Callback callback) {
        ActionModeImpl actionModeImpl = this.j;
        if (actionModeImpl != null) {
            actionModeImpl.a();
        }
        this.c.setHideOnContentScrollEnabled(false);
        this.f.c();
        ActionModeImpl actionModeImpl2 = new ActionModeImpl(this.f.getContext(), callback);
        if (!actionModeImpl2.k()) {
            return null;
        }
        this.j = actionModeImpl2;
        actionModeImpl2.i();
        this.f.a(actionModeImpl2);
        i(true);
        this.f.sendAccessibilityEvent(32);
        return actionModeImpl2;
    }

    public WindowDecorActionBar(Dialog dialog) {
        new ArrayList();
        b(dialog.getWindow().getDecorView());
    }

    public int m() {
        return this.e.i();
    }

    public void b(boolean z2) {
        if (z2 != this.m) {
            this.m = z2;
            int size = this.n.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.n.get(i2).a(z2);
            }
        }
    }

    public void a(boolean z2) {
        this.q = z2;
    }

    public void a() {
        if (this.s) {
            this.s = false;
            n(true);
        }
    }

    public void b(CharSequence charSequence) {
        this.e.setTitle(charSequence);
    }

    public void b(int i2) {
        this.e.d(i2);
    }

    public void a(Drawable drawable) {
        this.e.a(drawable);
    }

    public void a(int i2) {
        this.e.b(i2);
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        Menu c2;
        ActionModeImpl actionModeImpl = this.j;
        if (actionModeImpl == null || (c2 = actionModeImpl.c()) == null) {
            return false;
        }
        boolean z2 = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z2 = false;
        }
        c2.setQwertyMode(z2);
        return c2.performShortcut(i2, keyEvent, 0);
    }
}
