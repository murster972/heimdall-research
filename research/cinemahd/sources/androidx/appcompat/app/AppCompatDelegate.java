package androidx.appcompat.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.collection.ArraySet;
import java.lang.ref.WeakReference;
import java.util.Iterator;

public abstract class AppCompatDelegate {

    /* renamed from: a  reason: collision with root package name */
    private static int f256a = -100;
    private static final ArraySet<WeakReference<AppCompatDelegate>> b = new ArraySet<>();
    private static final Object c = new Object();

    AppCompatDelegate() {
    }

    public static AppCompatDelegate a(Activity activity, AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(activity, appCompatCallback);
    }

    static void b(AppCompatDelegate appCompatDelegate) {
        synchronized (c) {
            c(appCompatDelegate);
        }
    }

    private static void c(AppCompatDelegate appCompatDelegate) {
        synchronized (c) {
            Iterator<WeakReference<AppCompatDelegate>> it2 = b.iterator();
            while (it2.hasNext()) {
                AppCompatDelegate appCompatDelegate2 = (AppCompatDelegate) it2.next().get();
                if (appCompatDelegate2 == appCompatDelegate || appCompatDelegate2 == null) {
                    it2.remove();
                }
            }
        }
    }

    public static int k() {
        return f256a;
    }

    public abstract <T extends View> T a(int i);

    public abstract ActionBarDrawerToggle.Delegate a();

    public abstract ActionMode a(ActionMode.Callback callback);

    public void a(Context context) {
    }

    public abstract void a(Configuration configuration);

    public abstract void a(Bundle bundle);

    public abstract void a(View view);

    public abstract void a(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void a(Toolbar toolbar);

    public abstract void a(CharSequence charSequence);

    public int b() {
        return -100;
    }

    public abstract void b(Bundle bundle);

    public abstract void b(View view, ViewGroup.LayoutParams layoutParams);

    public abstract boolean b(int i);

    public abstract MenuInflater c();

    public abstract void c(int i);

    public abstract void c(Bundle bundle);

    public abstract ActionBar d();

    public void d(int i) {
    }

    public abstract void e();

    public abstract void f();

    public abstract void g();

    public abstract void h();

    public abstract void i();

    public abstract void j();

    public static AppCompatDelegate a(Dialog dialog, AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(dialog, appCompatCallback);
    }

    static void a(AppCompatDelegate appCompatDelegate) {
        synchronized (c) {
            c(appCompatDelegate);
            b.add(new WeakReference(appCompatDelegate));
        }
    }
}
