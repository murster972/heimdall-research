package androidx.appcompat.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import androidx.core.content.PermissionChecker;
import java.util.Calendar;

class TwilightManager {
    private static TwilightManager d;

    /* renamed from: a  reason: collision with root package name */
    private final Context f284a;
    private final LocationManager b;
    private final TwilightState c = new TwilightState();

    private static class TwilightState {

        /* renamed from: a  reason: collision with root package name */
        boolean f285a;
        long b;
        long c;
        long d;
        long e;
        long f;

        TwilightState() {
        }
    }

    TwilightManager(Context context, LocationManager locationManager) {
        this.f284a = context;
        this.b = locationManager;
    }

    static TwilightManager a(Context context) {
        if (d == null) {
            Context applicationContext = context.getApplicationContext();
            d = new TwilightManager(applicationContext, (LocationManager) applicationContext.getSystemService("location"));
        }
        return d;
    }

    @SuppressLint({"MissingPermission"})
    private Location b() {
        Location location = null;
        Location a2 = PermissionChecker.b(this.f284a, "android.permission.ACCESS_COARSE_LOCATION") == 0 ? a("network") : null;
        if (PermissionChecker.b(this.f284a, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location = a("gps");
        }
        return (location == null || a2 == null) ? location != null ? location : a2 : location.getTime() > a2.getTime() ? location : a2;
    }

    private boolean c() {
        return this.c.f > System.currentTimeMillis();
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        TwilightState twilightState = this.c;
        if (c()) {
            return twilightState.f285a;
        }
        Location b2 = b();
        if (b2 != null) {
            a(b2);
            return twilightState.f285a;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }

    private Location a(String str) {
        try {
            if (this.b.isProviderEnabled(str)) {
                return this.b.getLastKnownLocation(str);
            }
            return null;
        } catch (Exception e) {
            Log.d("TwilightManager", "Failed to get last known location", e);
            return null;
        }
    }

    private void a(Location location) {
        long j;
        TwilightState twilightState = this.c;
        long currentTimeMillis = System.currentTimeMillis();
        TwilightCalculator a2 = TwilightCalculator.a();
        TwilightCalculator twilightCalculator = a2;
        twilightCalculator.a(currentTimeMillis - 86400000, location.getLatitude(), location.getLongitude());
        long j2 = a2.f283a;
        twilightCalculator.a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = a2.c == 1;
        long j3 = a2.b;
        long j4 = j2;
        long j5 = a2.f283a;
        long j6 = j3;
        boolean z2 = z;
        a2.a(86400000 + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j7 = a2.b;
        if (j6 == -1 || j5 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            j = (currentTimeMillis > j5 ? 0 + j7 : currentTimeMillis > j6 ? 0 + j5 : 0 + j6) + 60000;
        }
        twilightState.f285a = z2;
        twilightState.b = j4;
        twilightState.c = j6;
        twilightState.d = j5;
        twilightState.e = j7;
        twilightState.f = j;
    }
}
