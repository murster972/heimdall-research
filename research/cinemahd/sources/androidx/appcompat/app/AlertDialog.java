package androidx.appcompat.app;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.R$attr;
import androidx.appcompat.app.AlertController;
import com.facebook.imageutils.JfifUtil;

public class AlertDialog extends AppCompatDialog implements DialogInterface {
    final AlertController c = new AlertController(getContext(), this, getWindow());

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final AlertController.AlertParams f255a;
        private final int b;

        public Builder(Context context) {
            this(context, AlertDialog.a(context, 0));
        }

        public Builder a(View view) {
            this.f255a.g = view;
            return this;
        }

        public Context b() {
            return this.f255a.f248a;
        }

        public Builder c(int i) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.f = alertParams.f248a.getText(i);
            return this;
        }

        public Builder d(int i) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.z = null;
            alertParams.y = i;
            alertParams.E = false;
            return this;
        }

        public Builder(Context context, int i) {
            this.f255a = new AlertController.AlertParams(new ContextThemeWrapper(context, AlertDialog.a(context, i)));
            this.b = i;
        }

        public Builder a(CharSequence charSequence) {
            this.f255a.h = charSequence;
            return this;
        }

        public Builder b(CharSequence charSequence) {
            this.f255a.f = charSequence;
            return this;
        }

        public AlertDialog c() {
            AlertDialog a2 = a();
            a2.show();
            return a2;
        }

        public Builder a(int i) {
            this.f255a.c = i;
            return this;
        }

        public Builder b(int i) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.h = alertParams.f248a.getText(i);
            return this;
        }

        public Builder a(Drawable drawable) {
            this.f255a.d = drawable;
            return this;
        }

        public Builder b(int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.i = alertParams.f248a.getText(i);
            this.f255a.k = onClickListener;
            return this;
        }

        public Builder a(int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.l = alertParams.f248a.getText(i);
            this.f255a.n = onClickListener;
            return this;
        }

        public Builder b(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.i = charSequence;
            alertParams.k = onClickListener;
            return this;
        }

        public Builder a(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.l = charSequence;
            alertParams.n = onClickListener;
            return this;
        }

        public Builder b(View view) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.z = view;
            alertParams.y = 0;
            alertParams.E = false;
            return this;
        }

        public Builder a(boolean z) {
            this.f255a.r = z;
            return this;
        }

        public Builder a(DialogInterface.OnDismissListener onDismissListener) {
            this.f255a.t = onDismissListener;
            return this;
        }

        public Builder a(DialogInterface.OnKeyListener onKeyListener) {
            this.f255a.u = onKeyListener;
            return this;
        }

        public Builder a(CharSequence[] charSequenceArr, DialogInterface.OnClickListener onClickListener) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.v = charSequenceArr;
            alertParams.x = onClickListener;
            return this;
        }

        public Builder a(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.w = listAdapter;
            alertParams.x = onClickListener;
            return this;
        }

        public Builder a(CharSequence[] charSequenceArr, boolean[] zArr, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.v = charSequenceArr;
            alertParams.J = onMultiChoiceClickListener;
            alertParams.F = zArr;
            alertParams.G = true;
            return this;
        }

        public Builder a(CharSequence[] charSequenceArr, int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.v = charSequenceArr;
            alertParams.x = onClickListener;
            alertParams.I = i;
            alertParams.H = true;
            return this;
        }

        public Builder a(ListAdapter listAdapter, int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.AlertParams alertParams = this.f255a;
            alertParams.w = listAdapter;
            alertParams.x = onClickListener;
            alertParams.I = i;
            alertParams.H = true;
            return this;
        }

        public AlertDialog a() {
            AlertDialog alertDialog = new AlertDialog(this.f255a.f248a, this.b);
            this.f255a.a(alertDialog.c);
            alertDialog.setCancelable(this.f255a.r);
            if (this.f255a.r) {
                alertDialog.setCanceledOnTouchOutside(true);
            }
            alertDialog.setOnCancelListener(this.f255a.s);
            alertDialog.setOnDismissListener(this.f255a.t);
            DialogInterface.OnKeyListener onKeyListener = this.f255a.u;
            if (onKeyListener != null) {
                alertDialog.setOnKeyListener(onKeyListener);
            }
            return alertDialog;
        }
    }

    protected AlertDialog(Context context, int i) {
        super(context, a(context, i));
    }

    static int a(Context context, int i) {
        if (((i >>> 24) & JfifUtil.MARKER_FIRST_BYTE) >= 1) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R$attr.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public Button b(int i) {
        return this.c.a(i);
    }

    public void c(int i) {
        this.c.c(i);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c.b();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.c.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.c.b(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.c.b(charSequence);
    }

    public ListView b() {
        return this.c.a();
    }

    public void a(CharSequence charSequence) {
        this.c.a(charSequence);
    }

    public void a(View view) {
        this.c.b(view);
    }

    public void a(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.c.a(i, charSequence, onClickListener, (Message) null, (Drawable) null);
    }
}
