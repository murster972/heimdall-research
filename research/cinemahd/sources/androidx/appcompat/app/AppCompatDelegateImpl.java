package androidx.appcompat.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.R$attr;
import androidx.appcompat.R$color;
import androidx.appcompat.R$id;
import androidx.appcompat.R$layout;
import androidx.appcompat.R$style;
import androidx.appcompat.R$styleable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.view.SupportActionModeWrapper;
import androidx.appcompat.view.SupportMenuInflater;
import androidx.appcompat.view.WindowCallbackWrapper;
import androidx.appcompat.view.menu.ListMenuPresenter;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.AppCompatDrawableManager;
import androidx.appcompat.widget.ContentFrameLayout;
import androidx.appcompat.widget.DecorContentParent;
import androidx.appcompat.widget.FitWindowsViewGroup;
import androidx.appcompat.widget.TintTypedArray;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.VectorEnabledTintResources;
import androidx.appcompat.widget.ViewUtils;
import androidx.collection.ArrayMap;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NavUtils;
import androidx.core.view.KeyEventDispatcher;
import androidx.core.view.LayoutInflaterCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.core.view.ViewPropertyAnimatorListenerAdapter;
import androidx.core.view.WindowInsetsCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import com.facebook.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import java.lang.Thread;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;

class AppCompatDelegateImpl extends AppCompatDelegate implements MenuBuilder.Callback, LayoutInflater.Factory2 {
    private static final Map<Class<?>, Integer> d0 = new ArrayMap();
    private static final boolean e0 = (Build.VERSION.SDK_INT < 21);
    private static final int[] f0 = {16842836};
    private static boolean g0 = true;
    private static final boolean h0;
    boolean A;
    boolean B;
    boolean C;
    boolean D;
    boolean E;
    private boolean F;
    private PanelFeatureState[] G;
    private PanelFeatureState H;
    private boolean I;
    private boolean J;
    private boolean K;
    private boolean L;
    boolean M;
    private int N;
    private int O;
    private boolean P;
    private boolean Q;
    private AutoNightModeManager R;
    private AutoNightModeManager S;
    boolean T;
    int U;
    private final Runnable V;
    private boolean W;
    private Rect X;
    private Rect Y;
    private AppCompatViewInflater c0;
    final Object d;
    final Context e;
    Window f;
    private AppCompatWindowCallback g;
    final AppCompatCallback h;
    ActionBar i;
    MenuInflater j;
    private CharSequence k;
    private DecorContentParent l;
    private ActionMenuPresenterCallback m;
    private PanelMenuPresenterCallback n;
    ActionMode o;
    ActionBarContextView p;
    PopupWindow q;
    Runnable r;
    ViewPropertyAnimatorCompat s;
    private boolean t;
    private boolean u;
    private ViewGroup v;
    private TextView w;
    private View x;
    private boolean y;
    private boolean z;

    private class ActionBarDrawableToggleImpl implements ActionBarDrawerToggle.Delegate {
        ActionBarDrawableToggleImpl() {
        }

        public Context a() {
            return AppCompatDelegateImpl.this.o();
        }

        public boolean b() {
            ActionBar d = AppCompatDelegateImpl.this.d();
            return (d == null || (d.g() & 4) == 0) ? false : true;
        }

        public Drawable c() {
            TintTypedArray a2 = TintTypedArray.a(a(), (AttributeSet) null, new int[]{R$attr.homeAsUpIndicator});
            Drawable b = a2.b(0);
            a2.a();
            return b;
        }

        public void a(Drawable drawable, int i) {
            ActionBar d = AppCompatDelegateImpl.this.d();
            if (d != null) {
                d.a(drawable);
                d.a(i);
            }
        }

        public void a(int i) {
            ActionBar d = AppCompatDelegateImpl.this.d();
            if (d != null) {
                d.a(i);
            }
        }
    }

    class ActionModeCallbackWrapperV9 implements ActionMode.Callback {

        /* renamed from: a  reason: collision with root package name */
        private ActionMode.Callback f267a;

        public ActionModeCallbackWrapperV9(ActionMode.Callback callback) {
            this.f267a = callback;
        }

        public boolean a(ActionMode actionMode, Menu menu) {
            return this.f267a.a(actionMode, menu);
        }

        public boolean b(ActionMode actionMode, Menu menu) {
            return this.f267a.b(actionMode, menu);
        }

        public boolean a(ActionMode actionMode, MenuItem menuItem) {
            return this.f267a.a(actionMode, menuItem);
        }

        public void a(ActionMode actionMode) {
            this.f267a.a(actionMode);
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (appCompatDelegateImpl.q != null) {
                appCompatDelegateImpl.f.getDecorView().removeCallbacks(AppCompatDelegateImpl.this.r);
            }
            AppCompatDelegateImpl appCompatDelegateImpl2 = AppCompatDelegateImpl.this;
            if (appCompatDelegateImpl2.p != null) {
                appCompatDelegateImpl2.n();
                AppCompatDelegateImpl appCompatDelegateImpl3 = AppCompatDelegateImpl.this;
                ViewPropertyAnimatorCompat a2 = ViewCompat.a(appCompatDelegateImpl3.p);
                a2.a(0.0f);
                appCompatDelegateImpl3.s = a2;
                AppCompatDelegateImpl.this.s.a((ViewPropertyAnimatorListener) new ViewPropertyAnimatorListenerAdapter() {
                    public void onAnimationEnd(View view) {
                        AppCompatDelegateImpl.this.p.setVisibility(8);
                        AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
                        PopupWindow popupWindow = appCompatDelegateImpl.q;
                        if (popupWindow != null) {
                            popupWindow.dismiss();
                        } else if (appCompatDelegateImpl.p.getParent() instanceof View) {
                            ViewCompat.J((View) AppCompatDelegateImpl.this.p.getParent());
                        }
                        AppCompatDelegateImpl.this.p.removeAllViews();
                        AppCompatDelegateImpl.this.s.a((ViewPropertyAnimatorListener) null);
                        AppCompatDelegateImpl.this.s = null;
                    }
                });
            }
            AppCompatDelegateImpl appCompatDelegateImpl4 = AppCompatDelegateImpl.this;
            AppCompatCallback appCompatCallback = appCompatDelegateImpl4.h;
            if (appCompatCallback != null) {
                appCompatCallback.onSupportActionModeFinished(appCompatDelegateImpl4.o);
            }
            AppCompatDelegateImpl.this.o = null;
        }
    }

    private class AutoBatteryNightModeManager extends AutoNightModeManager {
        private final PowerManager c;

        AutoBatteryNightModeManager(Context context) {
            super();
            this.c = (PowerManager) context.getSystemService("power");
        }

        /* access modifiers changed from: package-private */
        public IntentFilter b() {
            if (Build.VERSION.SDK_INT < 21) {
                return null;
            }
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            return intentFilter;
        }

        public int c() {
            if (Build.VERSION.SDK_INT < 21 || !this.c.isPowerSaveMode()) {
                return 1;
            }
            return 2;
        }

        public void d() {
            AppCompatDelegateImpl.this.l();
        }
    }

    abstract class AutoNightModeManager {

        /* renamed from: a  reason: collision with root package name */
        private BroadcastReceiver f269a;

        AutoNightModeManager() {
        }

        /* access modifiers changed from: package-private */
        public void a() {
            BroadcastReceiver broadcastReceiver = this.f269a;
            if (broadcastReceiver != null) {
                try {
                    AppCompatDelegateImpl.this.e.unregisterReceiver(broadcastReceiver);
                } catch (IllegalArgumentException unused) {
                }
                this.f269a = null;
            }
        }

        /* access modifiers changed from: package-private */
        public abstract IntentFilter b();

        /* access modifiers changed from: package-private */
        public abstract int c();

        /* access modifiers changed from: package-private */
        public abstract void d();

        /* access modifiers changed from: package-private */
        public void e() {
            a();
            IntentFilter b2 = b();
            if (b2 != null && b2.countActions() != 0) {
                if (this.f269a == null) {
                    this.f269a = new BroadcastReceiver() {
                        public void onReceive(Context context, Intent intent) {
                            AutoNightModeManager.this.d();
                        }
                    };
                }
                AppCompatDelegateImpl.this.e.registerReceiver(this.f269a, b2);
            }
        }
    }

    private class AutoTimeNightModeManager extends AutoNightModeManager {
        private final TwilightManager c;

        AutoTimeNightModeManager(TwilightManager twilightManager) {
            super();
            this.c = twilightManager;
        }

        /* access modifiers changed from: package-private */
        public IntentFilter b() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIME_SET");
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            intentFilter.addAction("android.intent.action.TIME_TICK");
            return intentFilter;
        }

        public int c() {
            return this.c.a() ? 2 : 1;
        }

        public void d() {
            AppCompatDelegateImpl.this.l();
        }
    }

    private class ListMenuDecorView extends ContentFrameLayout {
        public ListMenuDecorView(Context context) {
            super(context);
        }

        private boolean a(int i2, int i3) {
            return i2 < -5 || i3 < -5 || i2 > getWidth() + 5 || i3 > getHeight() + 5;
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImpl.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            AppCompatDelegateImpl.this.e(0);
            return true;
        }

        public void setBackgroundResource(int i2) {
            setBackgroundDrawable(AppCompatResources.c(getContext(), i2));
        }
    }

    static {
        boolean z2 = false;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21 && i2 <= 25) {
            z2 = true;
        }
        h0 = z2;
        if (e0 && !g0) {
            final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                private boolean a(Throwable th) {
                    String message;
                    if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                        return false;
                    }
                    if (message.contains("drawable") || message.contains("Drawable")) {
                        return true;
                    }
                    return false;
                }

                public void uncaughtException(Thread thread, Throwable th) {
                    if (a(th)) {
                        Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                        notFoundException.initCause(th.getCause());
                        notFoundException.setStackTrace(th.getStackTrace());
                        defaultUncaughtExceptionHandler.uncaughtException(thread, notFoundException);
                        return;
                    }
                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                }
            });
        }
    }

    AppCompatDelegateImpl(Activity activity, AppCompatCallback appCompatCallback) {
        this(activity, (Window) null, appCompatCallback, activity);
    }

    private void A() {
        if (!this.u) {
            this.v = z();
            CharSequence q2 = q();
            if (!TextUtils.isEmpty(q2)) {
                DecorContentParent decorContentParent = this.l;
                if (decorContentParent != null) {
                    decorContentParent.setWindowTitle(q2);
                } else if (u() != null) {
                    u().c(q2);
                } else {
                    TextView textView = this.w;
                    if (textView != null) {
                        textView.setText(q2);
                    }
                }
            }
            w();
            a(this.v);
            this.u = true;
            PanelFeatureState a2 = a(0, false);
            if (this.M) {
                return;
            }
            if (a2 == null || a2.j == null) {
                k(108);
            }
        }
    }

    private void B() {
        if (this.f == null) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                a(((Activity) obj).getWindow());
            }
        }
        if (this.f == null) {
            throw new IllegalStateException("We have not been given a Window");
        }
    }

    private AutoNightModeManager C() {
        if (this.S == null) {
            this.S = new AutoBatteryNightModeManager(this.e);
        }
        return this.S;
    }

    private void D() {
        A();
        if (this.A && this.i == null) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                this.i = new WindowDecorActionBar((Activity) obj, this.B);
            } else if (obj instanceof Dialog) {
                this.i = new WindowDecorActionBar((Dialog) obj);
            }
            ActionBar actionBar = this.i;
            if (actionBar != null) {
                actionBar.c(this.W);
            }
        }
    }

    private boolean E() {
        if (!this.Q && (this.d instanceof Activity)) {
            PackageManager packageManager = this.e.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            try {
                ActivityInfo activityInfo = packageManager.getActivityInfo(new ComponentName(this.e, this.d.getClass()), 0);
                this.P = (activityInfo == null || (activityInfo.configChanges & AdRequest.MAX_CONTENT_URL_LENGTH) == 0) ? false : true;
            } catch (PackageManager.NameNotFoundException e2) {
                Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e2);
                this.P = false;
            }
        }
        this.Q = true;
        return this.P;
    }

    private void F() {
        if (this.u) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    private AppCompatActivity G() {
        Context context = this.e;
        while (context != null) {
            if (!(context instanceof AppCompatActivity)) {
                if (!(context instanceof ContextWrapper)) {
                    break;
                }
                context = ((ContextWrapper) context).getBaseContext();
            } else {
                return (AppCompatActivity) context;
            }
        }
        return null;
    }

    private void k(int i2) {
        this.U = (1 << i2) | this.U;
        if (!this.T) {
            ViewCompat.a(this.f.getDecorView(), this.V);
            this.T = true;
        }
    }

    private int l(int i2) {
        if (i2 == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i2 != 9) {
            return i2;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    private void w() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.v.findViewById(16908290);
        View decorView = this.f.getDecorView();
        contentFrameLayout.a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.e.obtainStyledAttributes(R$styleable.AppCompatTheme);
        obtainStyledAttributes.getValue(R$styleable.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(R$styleable.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(R$styleable.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(R$styleable.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(R$styleable.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(R$styleable.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(R$styleable.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(R$styleable.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(R$styleable.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(R$styleable.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    private int x() {
        int i2 = this.N;
        return i2 != -100 ? i2 : AppCompatDelegate.k();
    }

    private void y() {
        AutoNightModeManager autoNightModeManager = this.R;
        if (autoNightModeManager != null) {
            autoNightModeManager.a();
        }
        AutoNightModeManager autoNightModeManager2 = this.S;
        if (autoNightModeManager2 != null) {
            autoNightModeManager2.a();
        }
    }

    private ViewGroup z() {
        ViewGroup viewGroup;
        Context context;
        TypedArray obtainStyledAttributes = this.e.obtainStyledAttributes(R$styleable.AppCompatTheme);
        if (obtainStyledAttributes.hasValue(R$styleable.AppCompatTheme_windowActionBar)) {
            if (obtainStyledAttributes.getBoolean(R$styleable.AppCompatTheme_windowNoTitle, false)) {
                b(1);
            } else if (obtainStyledAttributes.getBoolean(R$styleable.AppCompatTheme_windowActionBar, false)) {
                b(108);
            }
            if (obtainStyledAttributes.getBoolean(R$styleable.AppCompatTheme_windowActionBarOverlay, false)) {
                b(109);
            }
            if (obtainStyledAttributes.getBoolean(R$styleable.AppCompatTheme_windowActionModeOverlay, false)) {
                b(10);
            }
            this.D = obtainStyledAttributes.getBoolean(R$styleable.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            B();
            this.f.getDecorView();
            LayoutInflater from = LayoutInflater.from(this.e);
            if (this.E) {
                if (this.C) {
                    viewGroup = (ViewGroup) from.inflate(R$layout.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
                } else {
                    viewGroup = (ViewGroup) from.inflate(R$layout.abc_screen_simple, (ViewGroup) null);
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    ViewCompat.a((View) viewGroup, (OnApplyWindowInsetsListener) new OnApplyWindowInsetsListener() {
                        public WindowInsetsCompat a(View view, WindowInsetsCompat windowInsetsCompat) {
                            int e = windowInsetsCompat.e();
                            int j = AppCompatDelegateImpl.this.j(e);
                            if (e != j) {
                                windowInsetsCompat = windowInsetsCompat.a(windowInsetsCompat.c(), j, windowInsetsCompat.d(), windowInsetsCompat.b());
                            }
                            return ViewCompat.b(view, windowInsetsCompat);
                        }
                    });
                } else {
                    ((FitWindowsViewGroup) viewGroup).setOnFitSystemWindowsListener(new FitWindowsViewGroup.OnFitSystemWindowsListener() {
                        public void a(Rect rect) {
                            rect.top = AppCompatDelegateImpl.this.j(rect.top);
                        }
                    });
                }
            } else if (this.D) {
                viewGroup = (ViewGroup) from.inflate(R$layout.abc_dialog_title_material, (ViewGroup) null);
                this.B = false;
                this.A = false;
            } else if (this.A) {
                TypedValue typedValue = new TypedValue();
                this.e.getTheme().resolveAttribute(R$attr.actionBarTheme, typedValue, true);
                int i2 = typedValue.resourceId;
                if (i2 != 0) {
                    context = new ContextThemeWrapper(this.e, i2);
                } else {
                    context = this.e;
                }
                viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(R$layout.abc_screen_toolbar, (ViewGroup) null);
                this.l = (DecorContentParent) viewGroup.findViewById(R$id.decor_content_parent);
                this.l.setWindowCallback(r());
                if (this.B) {
                    this.l.a(109);
                }
                if (this.y) {
                    this.l.a(2);
                }
                if (this.z) {
                    this.l.a(5);
                }
            } else {
                viewGroup = null;
            }
            if (viewGroup != null) {
                if (this.l == null) {
                    this.w = (TextView) viewGroup.findViewById(R$id.title);
                }
                ViewUtils.b(viewGroup);
                ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(R$id.action_bar_activity_content);
                ViewGroup viewGroup2 = (ViewGroup) this.f.findViewById(16908290);
                if (viewGroup2 != null) {
                    while (viewGroup2.getChildCount() > 0) {
                        View childAt = viewGroup2.getChildAt(0);
                        viewGroup2.removeViewAt(0);
                        contentFrameLayout.addView(childAt);
                    }
                    viewGroup2.setId(-1);
                    contentFrameLayout.setId(16908290);
                    if (viewGroup2 instanceof FrameLayout) {
                        ((FrameLayout) viewGroup2).setForeground((Drawable) null);
                    }
                }
                this.f.setContentView(viewGroup);
                contentFrameLayout.setAttachListener(new ContentFrameLayout.OnAttachListener() {
                    public void a() {
                    }

                    public void onDetachedFromWindow() {
                        AppCompatDelegateImpl.this.m();
                    }
                });
                return viewGroup;
            }
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.A + ", windowActionBarOverlay: " + this.B + ", android:windowIsFloating: " + this.D + ", windowActionModeOverlay: " + this.C + ", windowNoTitle: " + this.E + " }");
        }
        obtainStyledAttributes.recycle();
        throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    public void a(Context context) {
        a(false);
        this.J = true;
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup) {
    }

    public void b(Bundle bundle) {
        A();
    }

    public MenuInflater c() {
        if (this.j == null) {
            D();
            ActionBar actionBar = this.i;
            this.j = new SupportMenuInflater(actionBar != null ? actionBar.h() : this.e);
        }
        return this.j;
    }

    public ActionBar d() {
        D();
        return this.i;
    }

    public void e() {
        LayoutInflater from = LayoutInflater.from(this.e);
        if (from.getFactory() == null) {
            LayoutInflaterCompat.b(from, this);
        } else if (!(from.getFactory2() instanceof AppCompatDelegateImpl)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public void f() {
        ActionBar d2 = d();
        if (d2 == null || !d2.i()) {
            k(0);
        }
    }

    public void g() {
        AppCompatDelegate.b((AppCompatDelegate) this);
        if (this.T) {
            this.f.getDecorView().removeCallbacks(this.V);
        }
        this.L = false;
        this.M = true;
        ActionBar actionBar = this.i;
        if (actionBar != null) {
            actionBar.j();
        }
        y();
    }

    public void h() {
        ActionBar d2 = d();
        if (d2 != null) {
            d2.h(true);
        }
    }

    public void i() {
        this.L = true;
        l();
        AppCompatDelegate.a((AppCompatDelegate) this);
    }

    public void j() {
        this.L = false;
        AppCompatDelegate.b((AppCompatDelegate) this);
        ActionBar d2 = d();
        if (d2 != null) {
            d2.h(false);
        }
        if (this.d instanceof Dialog) {
            y();
        }
    }

    /* access modifiers changed from: package-private */
    public void m() {
        MenuBuilder menuBuilder;
        DecorContentParent decorContentParent = this.l;
        if (decorContentParent != null) {
            decorContentParent.g();
        }
        if (this.q != null) {
            this.f.getDecorView().removeCallbacks(this.r);
            if (this.q.isShowing()) {
                try {
                    this.q.dismiss();
                } catch (IllegalArgumentException unused) {
                }
            }
            this.q = null;
        }
        n();
        PanelFeatureState a2 = a(0, false);
        if (a2 != null && (menuBuilder = a2.j) != null) {
            menuBuilder.close();
        }
    }

    /* access modifiers changed from: package-private */
    public void n() {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = this.s;
        if (viewPropertyAnimatorCompat != null) {
            viewPropertyAnimatorCompat.a();
        }
    }

    /* access modifiers changed from: package-private */
    public final Context o() {
        ActionBar d2 = d();
        Context h2 = d2 != null ? d2.h() : null;
        return h2 == null ? this.e : h2;
    }

    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return a(view, str, context, attributeSet);
    }

    /* access modifiers changed from: package-private */
    public final AutoNightModeManager p() {
        if (this.R == null) {
            this.R = new AutoTimeNightModeManager(TwilightManager.a(this.e));
        }
        return this.R;
    }

    /* access modifiers changed from: package-private */
    public final CharSequence q() {
        Object obj = this.d;
        if (obj instanceof Activity) {
            return ((Activity) obj).getTitle();
        }
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public final Window.Callback r() {
        return this.f.getCallback();
    }

    public boolean s() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public boolean t() {
        ActionMode actionMode = this.o;
        if (actionMode != null) {
            actionMode.a();
            return true;
        }
        ActionBar d2 = d();
        if (d2 == null || !d2.f()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final ActionBar u() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r1.v;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean v() {
        /*
            r1 = this;
            boolean r0 = r1.u
            if (r0 == 0) goto L_0x0010
            android.view.ViewGroup r0 = r1.v
            if (r0 == 0) goto L_0x0010
            boolean r0 = androidx.core.view.ViewCompat.E(r0)
            if (r0 == 0) goto L_0x0010
            r0 = 1
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.app.AppCompatDelegateImpl.v():boolean");
    }

    private final class ActionMenuPresenterCallback implements MenuPresenter.Callback {
        ActionMenuPresenterCallback() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback r = AppCompatDelegateImpl.this.r();
            if (r == null) {
                return true;
            }
            r.onMenuOpened(108, menuBuilder);
            return true;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            AppCompatDelegateImpl.this.b(menuBuilder);
        }
    }

    AppCompatDelegateImpl(Dialog dialog, AppCompatCallback appCompatCallback) {
        this(dialog.getContext(), dialog.getWindow(), appCompatCallback, dialog);
    }

    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        A();
        ViewGroup viewGroup = (ViewGroup) this.v.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.g.a().onContentChanged();
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView((View) null, str, context, attributeSet);
    }

    protected static final class PanelFeatureState {

        /* renamed from: a  reason: collision with root package name */
        int f271a;
        int b;
        int c;
        int d;
        int e;
        int f;
        ViewGroup g;
        View h;
        View i;
        MenuBuilder j;
        ListMenuPresenter k;
        Context l;
        boolean m;
        boolean n;
        boolean o;
        public boolean p;
        boolean q = false;
        boolean r;
        Bundle s;

        @SuppressLint({"BanParcelableUsage"})
        private static class SavedState implements Parcelable {
            public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }

                public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return SavedState.a(parcel, classLoader);
                }

                public SavedState createFromParcel(Parcel parcel) {
                    return SavedState.a(parcel, (ClassLoader) null);
                }
            };

            /* renamed from: a  reason: collision with root package name */
            int f272a;
            boolean b;
            Bundle c;

            SavedState() {
            }

            static SavedState a(Parcel parcel, ClassLoader classLoader) {
                SavedState savedState = new SavedState();
                savedState.f272a = parcel.readInt();
                boolean z = true;
                if (parcel.readInt() != 1) {
                    z = false;
                }
                savedState.b = z;
                if (savedState.b) {
                    savedState.c = parcel.readBundle(classLoader);
                }
                return savedState;
            }

            public int describeContents() {
                return 0;
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f272a);
                parcel.writeInt(this.b ? 1 : 0);
                if (this.b) {
                    parcel.writeBundle(this.c);
                }
            }
        }

        PanelFeatureState(int i2) {
            this.f271a = i2;
        }

        public boolean a() {
            if (this.h == null) {
                return false;
            }
            if (this.i == null && this.k.c().getCount() <= 0) {
                return false;
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(R$attr.actionBarPopupTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                newTheme.applyStyle(i2, true);
            }
            newTheme.resolveAttribute(R$attr.panelMenuListTheme, typedValue, true);
            int i3 = typedValue.resourceId;
            if (i3 != 0) {
                newTheme.applyStyle(i3, true);
            } else {
                newTheme.applyStyle(R$style.Theme_AppCompat_CompactMenu, true);
            }
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, 0);
            contextThemeWrapper.getTheme().setTo(newTheme);
            this.l = contextThemeWrapper;
            TypedArray obtainStyledAttributes = contextThemeWrapper.obtainStyledAttributes(R$styleable.AppCompatTheme);
            this.b = obtainStyledAttributes.getResourceId(R$styleable.AppCompatTheme_panelBackground, 0);
            this.f = obtainStyledAttributes.getResourceId(R$styleable.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        /* access modifiers changed from: package-private */
        public void a(MenuBuilder menuBuilder) {
            ListMenuPresenter listMenuPresenter;
            MenuBuilder menuBuilder2 = this.j;
            if (menuBuilder != menuBuilder2) {
                if (menuBuilder2 != null) {
                    menuBuilder2.b((MenuPresenter) this.k);
                }
                this.j = menuBuilder;
                if (menuBuilder != null && (listMenuPresenter = this.k) != null) {
                    menuBuilder.a((MenuPresenter) listMenuPresenter);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public MenuView a(MenuPresenter.Callback callback) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                this.k = new ListMenuPresenter(this.l, R$layout.abc_list_menu_item_layout);
                this.k.a(callback);
                this.j.a((MenuPresenter) this.k);
            }
            return this.k.a(this.g);
        }
    }

    private AppCompatDelegateImpl(Context context, Window window, AppCompatCallback appCompatCallback, Object obj) {
        Integer num;
        AppCompatActivity G2;
        this.s = null;
        this.t = true;
        this.N = -100;
        this.V = new Runnable() {
            public void run() {
                AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
                if ((appCompatDelegateImpl.U & 1) != 0) {
                    appCompatDelegateImpl.f(0);
                }
                AppCompatDelegateImpl appCompatDelegateImpl2 = AppCompatDelegateImpl.this;
                if ((appCompatDelegateImpl2.U & 4096) != 0) {
                    appCompatDelegateImpl2.f(108);
                }
                AppCompatDelegateImpl appCompatDelegateImpl3 = AppCompatDelegateImpl.this;
                appCompatDelegateImpl3.T = false;
                appCompatDelegateImpl3.U = 0;
            }
        };
        this.e = context;
        this.h = appCompatCallback;
        this.d = obj;
        if (this.N == -100 && (this.d instanceof Dialog) && (G2 = G()) != null) {
            this.N = G2.getDelegate().b();
        }
        if (this.N == -100 && (num = d0.get(this.d.getClass())) != null) {
            this.N = num.intValue();
            d0.remove(this.d.getClass());
        }
        if (window != null) {
            a(window);
        }
        AppCompatDrawableManager.c();
    }

    public void a(Bundle bundle) {
        this.J = true;
        a(false);
        B();
        Object obj = this.d;
        if (obj instanceof Activity) {
            String str = null;
            try {
                str = NavUtils.b((Activity) obj);
            } catch (IllegalArgumentException unused) {
            }
            if (str != null) {
                ActionBar u2 = u();
                if (u2 == null) {
                    this.W = true;
                } else {
                    u2.c(true);
                }
            }
        }
        this.K = true;
    }

    public void d(int i2) {
        this.O = i2;
    }

    /* access modifiers changed from: package-private */
    public void h(int i2) {
        ActionBar d2;
        if (i2 == 108 && (d2 = d()) != null) {
            d2.b(true);
        }
    }

    public boolean l() {
        return a(true);
    }

    class AppCompatWindowCallback extends WindowCallbackWrapper {
        AppCompatWindowCallback(Window.Callback callback) {
            super(callback);
        }

        /* access modifiers changed from: package-private */
        public final android.view.ActionMode a(ActionMode.Callback callback) {
            SupportActionModeWrapper.CallbackWrapper callbackWrapper = new SupportActionModeWrapper.CallbackWrapper(AppCompatDelegateImpl.this.e, callback);
            androidx.appcompat.view.ActionMode a2 = AppCompatDelegateImpl.this.a((ActionMode.Callback) callbackWrapper);
            if (a2 != null) {
                return callbackWrapper.b(a2);
            }
            return null;
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImpl.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || AppCompatDelegateImpl.this.b(keyEvent.getKeyCode(), keyEvent);
        }

        public void onContentChanged() {
        }

        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof MenuBuilder)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            AppCompatDelegateImpl.this.h(i);
            return true;
        }

        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            AppCompatDelegateImpl.this.i(i);
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            MenuBuilder menuBuilder = menu instanceof MenuBuilder ? (MenuBuilder) menu : null;
            if (i == 0 && menuBuilder == null) {
                return false;
            }
            if (menuBuilder != null) {
                menuBuilder.c(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (menuBuilder != null) {
                menuBuilder.c(false);
            }
            return onPreparePanel;
        }

        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
            MenuBuilder menuBuilder;
            PanelFeatureState a2 = AppCompatDelegateImpl.this.a(0, true);
            if (a2 == null || (menuBuilder = a2.j) == null) {
                super.onProvideKeyboardShortcuts(list, menu, i);
            } else {
                super.onProvideKeyboardShortcuts(list, menuBuilder, i);
            }
        }

        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            if (AppCompatDelegateImpl.this.s()) {
                return a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            if (!AppCompatDelegateImpl.this.s() || i != 0) {
                return super.onWindowStartingActionMode(callback, i);
            }
            return a(callback);
        }
    }

    private boolean d(int i2, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() != 0) {
            return false;
        }
        PanelFeatureState a2 = a(i2, true);
        if (!a2.o) {
            return b(a2, keyEvent);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void f(int i2) {
        PanelFeatureState a2;
        PanelFeatureState a3 = a(i2, true);
        if (a3.j != null) {
            Bundle bundle = new Bundle();
            a3.j.c(bundle);
            if (bundle.size() > 0) {
                a3.s = bundle;
            }
            a3.j.s();
            a3.j.clear();
        }
        a3.r = true;
        a3.q = true;
        if ((i2 == 108 || i2 == 0) && this.l != null && (a2 = a(0, false)) != null) {
            a2.m = false;
            b(a2, (KeyEvent) null);
        }
    }

    /* access modifiers changed from: package-private */
    public void i(int i2) {
        if (i2 == 108) {
            ActionBar d2 = d();
            if (d2 != null) {
                d2.b(false);
            }
        } else if (i2 == 0) {
            PanelFeatureState a2 = a(i2, true);
            if (a2.o) {
                a(a2, false);
            }
        }
    }

    private final class PanelMenuPresenterCallback implements MenuPresenter.Callback {
        PanelMenuPresenterCallback() {
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            MenuBuilder m = menuBuilder.m();
            boolean z2 = m != menuBuilder;
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (z2) {
                menuBuilder = m;
            }
            PanelFeatureState a2 = appCompatDelegateImpl.a((Menu) menuBuilder);
            if (a2 == null) {
                return;
            }
            if (z2) {
                AppCompatDelegateImpl.this.a(a2.f271a, a2, m);
                AppCompatDelegateImpl.this.a(a2, true);
                return;
            }
            AppCompatDelegateImpl.this.a(a2, z);
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback r;
            if (menuBuilder != null) {
                return true;
            }
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (!appCompatDelegateImpl.A || (r = appCompatDelegateImpl.r()) == null || AppCompatDelegateImpl.this.M) {
                return true;
            }
            r.onMenuOpened(108, menuBuilder);
            return true;
        }
    }

    public void c(int i2) {
        A();
        ViewGroup viewGroup = (ViewGroup) this.v.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.e).inflate(i2, viewGroup);
        this.g.a().onContentChanged();
    }

    /* access modifiers changed from: package-private */
    public void e(int i2) {
        a(a(i2, true), true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean e(int r4, android.view.KeyEvent r5) {
        /*
            r3 = this;
            androidx.appcompat.view.ActionMode r0 = r3.o
            r1 = 0
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            r0 = 1
            androidx.appcompat.app.AppCompatDelegateImpl$PanelFeatureState r2 = r3.a((int) r4, (boolean) r0)
            if (r4 != 0) goto L_0x0043
            androidx.appcompat.widget.DecorContentParent r4 = r3.l
            if (r4 == 0) goto L_0x0043
            boolean r4 = r4.a()
            if (r4 == 0) goto L_0x0043
            android.content.Context r4 = r3.e
            android.view.ViewConfiguration r4 = android.view.ViewConfiguration.get(r4)
            boolean r4 = r4.hasPermanentMenuKey()
            if (r4 != 0) goto L_0x0043
            androidx.appcompat.widget.DecorContentParent r4 = r3.l
            boolean r4 = r4.d()
            if (r4 != 0) goto L_0x003c
            boolean r4 = r3.M
            if (r4 != 0) goto L_0x0063
            boolean r4 = r3.b((androidx.appcompat.app.AppCompatDelegateImpl.PanelFeatureState) r2, (android.view.KeyEvent) r5)
            if (r4 == 0) goto L_0x0063
            androidx.appcompat.widget.DecorContentParent r4 = r3.l
            boolean r4 = r4.c()
            goto L_0x006a
        L_0x003c:
            androidx.appcompat.widget.DecorContentParent r4 = r3.l
            boolean r4 = r4.b()
            goto L_0x006a
        L_0x0043:
            boolean r4 = r2.o
            if (r4 != 0) goto L_0x0065
            boolean r4 = r2.n
            if (r4 == 0) goto L_0x004c
            goto L_0x0065
        L_0x004c:
            boolean r4 = r2.m
            if (r4 == 0) goto L_0x0063
            boolean r4 = r2.r
            if (r4 == 0) goto L_0x005b
            r2.m = r1
            boolean r4 = r3.b((androidx.appcompat.app.AppCompatDelegateImpl.PanelFeatureState) r2, (android.view.KeyEvent) r5)
            goto L_0x005c
        L_0x005b:
            r4 = 1
        L_0x005c:
            if (r4 == 0) goto L_0x0063
            r3.a((androidx.appcompat.app.AppCompatDelegateImpl.PanelFeatureState) r2, (android.view.KeyEvent) r5)
            r4 = 1
            goto L_0x006a
        L_0x0063:
            r4 = 0
            goto L_0x006a
        L_0x0065:
            boolean r4 = r2.o
            r3.a((androidx.appcompat.app.AppCompatDelegateImpl.PanelFeatureState) r2, (boolean) r0)
        L_0x006a:
            if (r4 == 0) goto L_0x0083
            android.content.Context r5 = r3.e
            java.lang.String r0 = "audio"
            java.lang.Object r5 = r5.getSystemService(r0)
            android.media.AudioManager r5 = (android.media.AudioManager) r5
            if (r5 == 0) goto L_0x007c
            r5.playSoundEffect(r1)
            goto L_0x0083
        L_0x007c:
            java.lang.String r5 = "AppCompatDelegate"
            java.lang.String r0 = "Couldn't get audio manager"
            android.util.Log.w(r5, r0)
        L_0x0083:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.app.AppCompatDelegateImpl.e(int, android.view.KeyEvent):boolean");
    }

    public boolean b(int i2) {
        int l2 = l(i2);
        if (this.E && l2 == 108) {
            return false;
        }
        if (this.A && l2 == 1) {
            this.A = false;
        }
        if (l2 == 1) {
            F();
            this.E = true;
            return true;
        } else if (l2 == 2) {
            F();
            this.y = true;
            return true;
        } else if (l2 == 5) {
            F();
            this.z = true;
            return true;
        } else if (l2 == 10) {
            F();
            this.C = true;
            return true;
        } else if (l2 == 108) {
            F();
            this.A = true;
            return true;
        } else if (l2 != 109) {
            return this.f.requestFeature(l2);
        } else {
            F();
            this.B = true;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public int j(int i2) {
        boolean z2;
        boolean z3;
        boolean z4;
        ActionBarContextView actionBarContextView = this.p;
        int i3 = 0;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.p.getLayoutParams();
            z2 = true;
            if (this.p.isShown()) {
                if (this.X == null) {
                    this.X = new Rect();
                    this.Y = new Rect();
                }
                Rect rect = this.X;
                Rect rect2 = this.Y;
                rect.set(0, i2, 0, 0);
                ViewUtils.a(this.v, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i2 : 0)) {
                    marginLayoutParams.topMargin = i2;
                    View view = this.x;
                    if (view == null) {
                        this.x = new View(this.e);
                        this.x.setBackgroundColor(this.e.getResources().getColor(R$color.abc_input_method_navigation_guard));
                        this.v.addView(this.x, -1, new ViewGroup.LayoutParams(-1, i2));
                    } else {
                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        if (layoutParams.height != i2) {
                            layoutParams.height = i2;
                            this.x.setLayoutParams(layoutParams);
                        }
                    }
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (this.x == null) {
                    z2 = false;
                }
                if (!this.C && z2) {
                    i2 = 0;
                }
            } else {
                if (marginLayoutParams.topMargin != 0) {
                    marginLayoutParams.topMargin = 0;
                    z4 = true;
                } else {
                    z4 = false;
                }
                z2 = false;
            }
            if (z3) {
                this.p.setLayoutParams(marginLayoutParams);
            }
        }
        View view2 = this.x;
        if (view2 != null) {
            if (!z2) {
                i3 = 8;
            }
            view2.setVisibility(i3);
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public int g(int i2) {
        if (i2 == -100) {
            return -1;
        }
        if (i2 == -1) {
            return i2;
        }
        if (i2 != 0) {
            if (i2 == 1 || i2 == 2) {
                return i2;
            }
            if (i2 == 3) {
                return C().c();
            }
            throw new IllegalStateException("Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate.");
        } else if (Build.VERSION.SDK_INT < 23 || ((UiModeManager) this.e.getSystemService(UiModeManager.class)).getNightMode() != 0) {
            return p().c();
        } else {
            return -1;
        }
    }

    public void c(Bundle bundle) {
        if (this.N != -100) {
            d0.put(this.d.getClass(), Integer.valueOf(this.N));
        }
    }

    public void a(Toolbar toolbar) {
        if (this.d instanceof Activity) {
            ActionBar d2 = d();
            if (!(d2 instanceof WindowDecorActionBar)) {
                this.j = null;
                if (d2 != null) {
                    d2.j();
                }
                if (toolbar != null) {
                    ToolbarActionBar toolbarActionBar = new ToolbarActionBar(toolbar, q(), this.g);
                    this.i = toolbarActionBar;
                    this.f.setCallback(toolbarActionBar.l());
                } else {
                    this.i = null;
                    this.f.setCallback(this.g);
                }
                f();
                return;
            }
            throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            boolean z2 = this.I;
            this.I = false;
            PanelFeatureState a2 = a(0, false);
            if (a2 != null && a2.o) {
                if (!z2) {
                    a(a2, true);
                }
                return true;
            } else if (t()) {
                return true;
            }
        } else if (i2 == 82) {
            e(0, keyEvent);
            return true;
        }
        return false;
    }

    private boolean c(PanelFeatureState panelFeatureState) {
        Context context = this.e;
        int i2 = panelFeatureState.f271a;
        if ((i2 == 0 || i2 == 108) && this.l != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(R$attr.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(R$attr.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(R$attr.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, 0);
                contextThemeWrapper.getTheme().setTo(theme2);
                context = contextThemeWrapper;
            }
        }
        MenuBuilder menuBuilder = new MenuBuilder(context);
        menuBuilder.a((MenuBuilder.Callback) this);
        panelFeatureState.a(menuBuilder);
        return true;
    }

    public <T extends View> T a(int i2) {
        A();
        return this.f.findViewById(i2);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public androidx.appcompat.view.ActionMode b(androidx.appcompat.view.ActionMode.Callback r8) {
        /*
            r7 = this;
            r7.n()
            androidx.appcompat.view.ActionMode r0 = r7.o
            if (r0 == 0) goto L_0x000a
            r0.a()
        L_0x000a:
            boolean r0 = r8 instanceof androidx.appcompat.app.AppCompatDelegateImpl.ActionModeCallbackWrapperV9
            if (r0 != 0) goto L_0x0014
            androidx.appcompat.app.AppCompatDelegateImpl$ActionModeCallbackWrapperV9 r0 = new androidx.appcompat.app.AppCompatDelegateImpl$ActionModeCallbackWrapperV9
            r0.<init>(r8)
            r8 = r0
        L_0x0014:
            androidx.appcompat.app.AppCompatCallback r0 = r7.h
            r1 = 0
            if (r0 == 0) goto L_0x0022
            boolean r2 = r7.M
            if (r2 != 0) goto L_0x0022
            androidx.appcompat.view.ActionMode r0 = r0.onWindowStartingSupportActionMode(r8)     // Catch:{ AbstractMethodError -> 0x0022 }
            goto L_0x0023
        L_0x0022:
            r0 = r1
        L_0x0023:
            if (r0 == 0) goto L_0x0029
            r7.o = r0
            goto L_0x0165
        L_0x0029:
            androidx.appcompat.widget.ActionBarContextView r0 = r7.p
            r2 = 0
            r3 = 1
            if (r0 != 0) goto L_0x00d6
            boolean r0 = r7.D
            if (r0 == 0) goto L_0x00b7
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            android.content.Context r4 = r7.e
            android.content.res.Resources$Theme r4 = r4.getTheme()
            int r5 = androidx.appcompat.R$attr.actionBarTheme
            r4.resolveAttribute(r5, r0, r3)
            int r5 = r0.resourceId
            if (r5 == 0) goto L_0x0068
            android.content.Context r5 = r7.e
            android.content.res.Resources r5 = r5.getResources()
            android.content.res.Resources$Theme r5 = r5.newTheme()
            r5.setTo(r4)
            int r4 = r0.resourceId
            r5.applyStyle(r4, r3)
            androidx.appcompat.view.ContextThemeWrapper r4 = new androidx.appcompat.view.ContextThemeWrapper
            android.content.Context r6 = r7.e
            r4.<init>((android.content.Context) r6, (int) r2)
            android.content.res.Resources$Theme r6 = r4.getTheme()
            r6.setTo(r5)
            goto L_0x006a
        L_0x0068:
            android.content.Context r4 = r7.e
        L_0x006a:
            androidx.appcompat.widget.ActionBarContextView r5 = new androidx.appcompat.widget.ActionBarContextView
            r5.<init>(r4)
            r7.p = r5
            android.widget.PopupWindow r5 = new android.widget.PopupWindow
            int r6 = androidx.appcompat.R$attr.actionModePopupWindowStyle
            r5.<init>(r4, r1, r6)
            r7.q = r5
            android.widget.PopupWindow r5 = r7.q
            r6 = 2
            androidx.core.widget.PopupWindowCompat.a((android.widget.PopupWindow) r5, (int) r6)
            android.widget.PopupWindow r5 = r7.q
            androidx.appcompat.widget.ActionBarContextView r6 = r7.p
            r5.setContentView(r6)
            android.widget.PopupWindow r5 = r7.q
            r6 = -1
            r5.setWidth(r6)
            android.content.res.Resources$Theme r5 = r4.getTheme()
            int r6 = androidx.appcompat.R$attr.actionBarSize
            r5.resolveAttribute(r6, r0, r3)
            int r0 = r0.data
            android.content.res.Resources r4 = r4.getResources()
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()
            int r0 = android.util.TypedValue.complexToDimensionPixelSize(r0, r4)
            androidx.appcompat.widget.ActionBarContextView r4 = r7.p
            r4.setContentHeight(r0)
            android.widget.PopupWindow r0 = r7.q
            r4 = -2
            r0.setHeight(r4)
            androidx.appcompat.app.AppCompatDelegateImpl$6 r0 = new androidx.appcompat.app.AppCompatDelegateImpl$6
            r0.<init>()
            r7.r = r0
            goto L_0x00d6
        L_0x00b7:
            android.view.ViewGroup r0 = r7.v
            int r4 = androidx.appcompat.R$id.action_mode_bar_stub
            android.view.View r0 = r0.findViewById(r4)
            androidx.appcompat.widget.ViewStubCompat r0 = (androidx.appcompat.widget.ViewStubCompat) r0
            if (r0 == 0) goto L_0x00d6
            android.content.Context r4 = r7.o()
            android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r4)
            r0.setLayoutInflater(r4)
            android.view.View r0 = r0.a()
            androidx.appcompat.widget.ActionBarContextView r0 = (androidx.appcompat.widget.ActionBarContextView) r0
            r7.p = r0
        L_0x00d6:
            androidx.appcompat.widget.ActionBarContextView r0 = r7.p
            if (r0 == 0) goto L_0x0165
            r7.n()
            androidx.appcompat.widget.ActionBarContextView r0 = r7.p
            r0.c()
            androidx.appcompat.view.StandaloneActionMode r0 = new androidx.appcompat.view.StandaloneActionMode
            androidx.appcompat.widget.ActionBarContextView r4 = r7.p
            android.content.Context r4 = r4.getContext()
            androidx.appcompat.widget.ActionBarContextView r5 = r7.p
            android.widget.PopupWindow r6 = r7.q
            if (r6 != 0) goto L_0x00f1
            goto L_0x00f2
        L_0x00f1:
            r3 = 0
        L_0x00f2:
            r0.<init>(r4, r5, r8, r3)
            android.view.Menu r3 = r0.c()
            boolean r8 = r8.a((androidx.appcompat.view.ActionMode) r0, (android.view.Menu) r3)
            if (r8 == 0) goto L_0x0163
            r0.i()
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r8.a(r0)
            r7.o = r0
            boolean r8 = r7.v()
            r0 = 1065353216(0x3f800000, float:1.0)
            if (r8 == 0) goto L_0x012d
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r1 = 0
            r8.setAlpha(r1)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            androidx.core.view.ViewPropertyAnimatorCompat r8 = androidx.core.view.ViewCompat.a(r8)
            r8.a((float) r0)
            r7.s = r8
            androidx.core.view.ViewPropertyAnimatorCompat r8 = r7.s
            androidx.appcompat.app.AppCompatDelegateImpl$7 r0 = new androidx.appcompat.app.AppCompatDelegateImpl$7
            r0.<init>()
            r8.a((androidx.core.view.ViewPropertyAnimatorListener) r0)
            goto L_0x0153
        L_0x012d:
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r8.setAlpha(r0)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r8.setVisibility(r2)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r0 = 32
            r8.sendAccessibilityEvent(r0)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            android.view.ViewParent r8 = r8.getParent()
            boolean r8 = r8 instanceof android.view.View
            if (r8 == 0) goto L_0x0153
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            android.view.ViewParent r8 = r8.getParent()
            android.view.View r8 = (android.view.View) r8
            androidx.core.view.ViewCompat.J(r8)
        L_0x0153:
            android.widget.PopupWindow r8 = r7.q
            if (r8 == 0) goto L_0x0165
            android.view.Window r8 = r7.f
            android.view.View r8 = r8.getDecorView()
            java.lang.Runnable r0 = r7.r
            r8.post(r0)
            goto L_0x0165
        L_0x0163:
            r7.o = r1
        L_0x0165:
            androidx.appcompat.view.ActionMode r8 = r7.o
            if (r8 == 0) goto L_0x0170
            androidx.appcompat.app.AppCompatCallback r0 = r7.h
            if (r0 == 0) goto L_0x0170
            r0.onSupportActionModeStarted(r8)
        L_0x0170:
            androidx.appcompat.view.ActionMode r8 = r7.o
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.app.AppCompatDelegateImpl.b(androidx.appcompat.view.ActionMode$Callback):androidx.appcompat.view.ActionMode");
    }

    public void a(Configuration configuration) {
        ActionBar d2;
        if (this.A && this.u && (d2 = d()) != null) {
            d2.a(configuration);
        }
        AppCompatDrawableManager.b().a(this.e);
        a(false);
    }

    public void a(View view) {
        A();
        ViewGroup viewGroup = (ViewGroup) this.v.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.g.a().onContentChanged();
    }

    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        A();
        ((ViewGroup) this.v.findViewById(16908290)).addView(view, layoutParams);
        this.g.a().onContentChanged();
    }

    private void a(Window window) {
        if (this.f == null) {
            Window.Callback callback = window.getCallback();
            if (!(callback instanceof AppCompatWindowCallback)) {
                this.g = new AppCompatWindowCallback(callback);
                window.setCallback(this.g);
                TintTypedArray a2 = TintTypedArray.a(this.e, (AttributeSet) null, f0);
                Drawable c = a2.c(0);
                if (c != null) {
                    window.setBackgroundDrawable(c);
                }
                a2.a();
                this.f = window;
                return;
            }
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    private void c(int i2, boolean z2) {
        Resources resources = this.e.getResources();
        Configuration configuration = new Configuration(resources.getConfiguration());
        configuration.uiMode = i2 | (resources.getConfiguration().uiMode & -49);
        resources.updateConfiguration(configuration, (DisplayMetrics) null);
        if (Build.VERSION.SDK_INT < 26) {
            ResourcesFlusher.a(resources);
        }
        int i3 = this.O;
        if (i3 != 0) {
            this.e.setTheme(i3);
            if (Build.VERSION.SDK_INT >= 23) {
                this.e.getTheme().applyStyle(this.O, true);
            }
        }
        if (z2) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                Activity activity = (Activity) obj;
                if (activity instanceof LifecycleOwner) {
                    if (((LifecycleOwner) activity).getLifecycle().a().a(Lifecycle.State.STARTED)) {
                        activity.onConfigurationChanged(configuration);
                    }
                } else if (this.L) {
                    activity.onConfigurationChanged(configuration);
                }
            }
        }
    }

    public final void a(CharSequence charSequence) {
        this.k = charSequence;
        DecorContentParent decorContentParent = this.l;
        if (decorContentParent != null) {
            decorContentParent.setWindowTitle(charSequence);
        } else if (u() != null) {
            u().c(charSequence);
        } else {
            TextView textView = this.w;
            if (textView != null) {
                textView.setText(charSequence);
            }
        }
    }

    public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
        PanelFeatureState a2;
        Window.Callback r2 = r();
        if (r2 == null || this.M || (a2 = a((Menu) menuBuilder.m())) == null) {
            return false;
        }
        return r2.onMenuItemSelected(a2.f271a, menuItem);
    }

    public void a(MenuBuilder menuBuilder) {
        a(menuBuilder, true);
    }

    public androidx.appcompat.view.ActionMode a(ActionMode.Callback callback) {
        AppCompatCallback appCompatCallback;
        if (callback != null) {
            androidx.appcompat.view.ActionMode actionMode = this.o;
            if (actionMode != null) {
                actionMode.a();
            }
            ActionModeCallbackWrapperV9 actionModeCallbackWrapperV9 = new ActionModeCallbackWrapperV9(callback);
            ActionBar d2 = d();
            if (d2 != null) {
                this.o = d2.a((ActionMode.Callback) actionModeCallbackWrapperV9);
                androidx.appcompat.view.ActionMode actionMode2 = this.o;
                if (!(actionMode2 == null || (appCompatCallback = this.h) == null)) {
                    appCompatCallback.onSupportActionModeStarted(actionMode2);
                }
            }
            if (this.o == null) {
                this.o = b((ActionMode.Callback) actionModeCallbackWrapperV9);
            }
            return this.o;
        }
        throw new IllegalArgumentException("ActionMode callback can not be null.");
    }

    /* access modifiers changed from: package-private */
    public boolean a(KeyEvent keyEvent) {
        View decorView;
        Object obj = this.d;
        boolean z2 = true;
        if (((obj instanceof KeyEventDispatcher.Component) || (obj instanceof AppCompatDialog)) && (decorView = this.f.getDecorView()) != null && KeyEventDispatcher.a(decorView, keyEvent)) {
            return true;
        }
        if (keyEvent.getKeyCode() == 82 && this.g.a().dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? a(keyCode, keyEvent) : c(keyCode, keyEvent);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, KeyEvent keyEvent) {
        boolean z2 = true;
        if (i2 == 4) {
            if ((keyEvent.getFlags() & 128) == 0) {
                z2 = false;
            }
            this.I = z2;
        } else if (i2 == 82) {
            d(0, keyEvent);
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2, KeyEvent keyEvent) {
        ActionBar d2 = d();
        if (d2 != null && d2.a(i2, keyEvent)) {
            return true;
        }
        PanelFeatureState panelFeatureState = this.H;
        if (panelFeatureState == null || !a(panelFeatureState, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.H == null) {
                PanelFeatureState a2 = a(0, true);
                b(a2, keyEvent);
                boolean a3 = a(a2, keyEvent.getKeyCode(), keyEvent, 1);
                a2.m = false;
                if (a3) {
                    return true;
                }
            }
            return false;
        }
        PanelFeatureState panelFeatureState2 = this.H;
        if (panelFeatureState2 != null) {
            panelFeatureState2.n = true;
        }
        return true;
    }

    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z2;
        boolean z3 = false;
        if (this.c0 == null) {
            String string = this.e.obtainStyledAttributes(R$styleable.AppCompatTheme).getString(R$styleable.AppCompatTheme_viewInflaterClass);
            if (string == null || AppCompatViewInflater.class.getName().equals(string)) {
                this.c0 = new AppCompatViewInflater();
            } else {
                try {
                    this.c0 = (AppCompatViewInflater) Class.forName(string).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                } catch (Throwable th) {
                    Log.i("AppCompatDelegate", "Failed to instantiate custom view inflater " + string + ". Falling back to default.", th);
                    this.c0 = new AppCompatViewInflater();
                }
            }
        }
        if (e0) {
            if (!(attributeSet instanceof XmlPullParser)) {
                z3 = a((ViewParent) view);
            } else if (((XmlPullParser) attributeSet).getDepth() > 1) {
                z3 = true;
            }
            z2 = z3;
        } else {
            z2 = false;
        }
        return this.c0.createView(view, str, context, attributeSet, z2, e0, true, VectorEnabledTintResources.b());
    }

    private boolean b(PanelFeatureState panelFeatureState) {
        panelFeatureState.a(o());
        panelFeatureState.g = new ListMenuDecorView(panelFeatureState.l);
        panelFeatureState.c = 81;
        return true;
    }

    private boolean b(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        DecorContentParent decorContentParent;
        DecorContentParent decorContentParent2;
        DecorContentParent decorContentParent3;
        if (this.M) {
            return false;
        }
        if (panelFeatureState.m) {
            return true;
        }
        PanelFeatureState panelFeatureState2 = this.H;
        if (!(panelFeatureState2 == null || panelFeatureState2 == panelFeatureState)) {
            a(panelFeatureState2, false);
        }
        Window.Callback r2 = r();
        if (r2 != null) {
            panelFeatureState.i = r2.onCreatePanelView(panelFeatureState.f271a);
        }
        int i2 = panelFeatureState.f271a;
        boolean z2 = i2 == 0 || i2 == 108;
        if (z2 && (decorContentParent3 = this.l) != null) {
            decorContentParent3.e();
        }
        if (panelFeatureState.i == null && (!z2 || !(u() instanceof ToolbarActionBar))) {
            if (panelFeatureState.j == null || panelFeatureState.r) {
                if (panelFeatureState.j == null && (!c(panelFeatureState) || panelFeatureState.j == null)) {
                    return false;
                }
                if (z2 && this.l != null) {
                    if (this.m == null) {
                        this.m = new ActionMenuPresenterCallback();
                    }
                    this.l.a(panelFeatureState.j, this.m);
                }
                panelFeatureState.j.s();
                if (!r2.onCreatePanelMenu(panelFeatureState.f271a, panelFeatureState.j)) {
                    panelFeatureState.a((MenuBuilder) null);
                    if (z2 && (decorContentParent2 = this.l) != null) {
                        decorContentParent2.a((Menu) null, this.m);
                    }
                    return false;
                }
                panelFeatureState.r = false;
            }
            panelFeatureState.j.s();
            Bundle bundle = panelFeatureState.s;
            if (bundle != null) {
                panelFeatureState.j.a(bundle);
                panelFeatureState.s = null;
            }
            if (!r2.onPreparePanel(0, panelFeatureState.i, panelFeatureState.j)) {
                if (z2 && (decorContentParent = this.l) != null) {
                    decorContentParent.a((Menu) null, this.m);
                }
                panelFeatureState.j.r();
                return false;
            }
            panelFeatureState.p = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            panelFeatureState.j.setQwertyMode(panelFeatureState.p);
            panelFeatureState.j.r();
        }
        panelFeatureState.m = true;
        panelFeatureState.n = false;
        this.H = panelFeatureState;
        return true;
    }

    private boolean a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.f.getDecorView();
        while (viewParent != null) {
            if (viewParent == decorView || !(viewParent instanceof View) || ViewCompat.D((View) viewParent)) {
                return false;
            }
            viewParent = viewParent.getParent();
        }
        return true;
    }

    private void a(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        int i2;
        ViewGroup.LayoutParams layoutParams;
        if (!panelFeatureState.o && !this.M) {
            if (panelFeatureState.f271a == 0) {
                if ((this.e.getResources().getConfiguration().screenLayout & 15) == 4) {
                    return;
                }
            }
            Window.Callback r2 = r();
            if (r2 == null || r2.onMenuOpened(panelFeatureState.f271a, panelFeatureState.j)) {
                WindowManager windowManager = (WindowManager) this.e.getSystemService("window");
                if (windowManager != null && b(panelFeatureState, keyEvent)) {
                    if (panelFeatureState.g == null || panelFeatureState.q) {
                        ViewGroup viewGroup = panelFeatureState.g;
                        if (viewGroup == null) {
                            if (!b(panelFeatureState) || panelFeatureState.g == null) {
                                return;
                            }
                        } else if (panelFeatureState.q && viewGroup.getChildCount() > 0) {
                            panelFeatureState.g.removeAllViews();
                        }
                        if (a(panelFeatureState) && panelFeatureState.a()) {
                            ViewGroup.LayoutParams layoutParams2 = panelFeatureState.h.getLayoutParams();
                            if (layoutParams2 == null) {
                                layoutParams2 = new ViewGroup.LayoutParams(-2, -2);
                            }
                            panelFeatureState.g.setBackgroundResource(panelFeatureState.b);
                            ViewParent parent = panelFeatureState.h.getParent();
                            if (parent instanceof ViewGroup) {
                                ((ViewGroup) parent).removeView(panelFeatureState.h);
                            }
                            panelFeatureState.g.addView(panelFeatureState.h, layoutParams2);
                            if (!panelFeatureState.h.hasFocus()) {
                                panelFeatureState.h.requestFocus();
                            }
                        } else {
                            return;
                        }
                    } else {
                        View view = panelFeatureState.i;
                        if (!(view == null || (layoutParams = view.getLayoutParams()) == null || layoutParams.width != -1)) {
                            i2 = -1;
                            panelFeatureState.n = false;
                            WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams(i2, -2, panelFeatureState.d, panelFeatureState.e, AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE, 8519680, -3);
                            layoutParams3.gravity = panelFeatureState.c;
                            layoutParams3.windowAnimations = panelFeatureState.f;
                            windowManager.addView(panelFeatureState.g, layoutParams3);
                            panelFeatureState.o = true;
                            return;
                        }
                    }
                    i2 = -2;
                    panelFeatureState.n = false;
                    WindowManager.LayoutParams layoutParams32 = new WindowManager.LayoutParams(i2, -2, panelFeatureState.d, panelFeatureState.e, AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE, 8519680, -3);
                    layoutParams32.gravity = panelFeatureState.c;
                    layoutParams32.windowAnimations = panelFeatureState.f;
                    windowManager.addView(panelFeatureState.g, layoutParams32);
                    panelFeatureState.o = true;
                    return;
                }
                return;
            }
            a(panelFeatureState, true);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(MenuBuilder menuBuilder) {
        if (!this.F) {
            this.F = true;
            this.l.g();
            Window.Callback r2 = r();
            if (r2 != null && !this.M) {
                r2.onPanelClosed(108, menuBuilder);
            }
            this.F = false;
        }
    }

    private void a(MenuBuilder menuBuilder, boolean z2) {
        DecorContentParent decorContentParent = this.l;
        if (decorContentParent == null || !decorContentParent.a() || (ViewConfiguration.get(this.e).hasPermanentMenuKey() && !this.l.f())) {
            PanelFeatureState a2 = a(0, true);
            a2.q = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        Window.Callback r2 = r();
        if (this.l.d() && z2) {
            this.l.b();
            if (!this.M) {
                r2.onPanelClosed(108, a(0, true).j);
            }
        } else if (r2 != null && !this.M) {
            if (this.T && (this.U & 1) != 0) {
                this.f.getDecorView().removeCallbacks(this.V);
                this.V.run();
            }
            PanelFeatureState a3 = a(0, true);
            MenuBuilder menuBuilder2 = a3.j;
            if (menuBuilder2 != null && !a3.r && r2.onPreparePanel(0, a3.i, menuBuilder2)) {
                r2.onMenuOpened(108, a3.j);
                this.l.c();
            }
        }
    }

    public int b() {
        return this.N;
    }

    private boolean b(int i2, boolean z2) {
        int i3 = this.e.getApplicationContext().getResources().getConfiguration().uiMode & 48;
        boolean z3 = true;
        int i4 = i2 != 1 ? i2 != 2 ? i3 : 32 : 16;
        boolean E2 = E();
        boolean z4 = false;
        if ((h0 || i4 != i3) && !E2 && Build.VERSION.SDK_INT >= 17 && !this.J && (this.d instanceof android.view.ContextThemeWrapper)) {
            Configuration configuration = new Configuration();
            configuration.uiMode = (configuration.uiMode & -49) | i4;
            try {
                ((android.view.ContextThemeWrapper) this.d).applyOverrideConfiguration(configuration);
                z4 = true;
            } catch (IllegalStateException e2) {
                Log.e("AppCompatDelegate", "updateForNightMode. Calling applyOverrideConfiguration() failed with an exception. Will fall back to using Resources.updateConfiguration()", e2);
            }
        }
        int i5 = this.e.getResources().getConfiguration().uiMode & 48;
        if (!z4 && i5 != i4 && z2 && !E2 && this.J && (Build.VERSION.SDK_INT >= 17 || this.K)) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                ActivityCompat.d((Activity) obj);
                z4 = true;
            }
        }
        if (z4 || i5 == i4) {
            z3 = z4;
        } else {
            c(i4, E2);
        }
        if (z3) {
            Object obj2 = this.d;
            if (obj2 instanceof AppCompatActivity) {
                ((AppCompatActivity) obj2).onNightModeChanged(i2);
            }
        }
        return z3;
    }

    private boolean a(PanelFeatureState panelFeatureState) {
        View view = panelFeatureState.i;
        if (view != null) {
            panelFeatureState.h = view;
            return true;
        } else if (panelFeatureState.j == null) {
            return false;
        } else {
            if (this.n == null) {
                this.n = new PanelMenuPresenterCallback();
            }
            panelFeatureState.h = (View) panelFeatureState.a((MenuPresenter.Callback) this.n);
            if (panelFeatureState.h != null) {
                return true;
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(PanelFeatureState panelFeatureState, boolean z2) {
        ViewGroup viewGroup;
        DecorContentParent decorContentParent;
        if (!z2 || panelFeatureState.f271a != 0 || (decorContentParent = this.l) == null || !decorContentParent.d()) {
            WindowManager windowManager = (WindowManager) this.e.getSystemService("window");
            if (!(windowManager == null || !panelFeatureState.o || (viewGroup = panelFeatureState.g) == null)) {
                windowManager.removeView(viewGroup);
                if (z2) {
                    a(panelFeatureState.f271a, panelFeatureState, (Menu) null);
                }
            }
            panelFeatureState.m = false;
            panelFeatureState.n = false;
            panelFeatureState.o = false;
            panelFeatureState.h = null;
            panelFeatureState.q = true;
            if (this.H == panelFeatureState) {
                this.H = null;
                return;
            }
            return;
        }
        b(panelFeatureState.j);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, PanelFeatureState panelFeatureState, Menu menu) {
        if (menu == null) {
            if (panelFeatureState == null && i2 >= 0) {
                PanelFeatureState[] panelFeatureStateArr = this.G;
                if (i2 < panelFeatureStateArr.length) {
                    panelFeatureState = panelFeatureStateArr[i2];
                }
            }
            if (panelFeatureState != null) {
                menu = panelFeatureState.j;
            }
        }
        if ((panelFeatureState == null || panelFeatureState.o) && !this.M) {
            this.g.a().onPanelClosed(i2, menu);
        }
    }

    /* access modifiers changed from: package-private */
    public PanelFeatureState a(Menu menu) {
        PanelFeatureState[] panelFeatureStateArr = this.G;
        int length = panelFeatureStateArr != null ? panelFeatureStateArr.length : 0;
        for (int i2 = 0; i2 < length; i2++) {
            PanelFeatureState panelFeatureState = panelFeatureStateArr[i2];
            if (panelFeatureState != null && panelFeatureState.j == menu) {
                return panelFeatureState;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public PanelFeatureState a(int i2, boolean z2) {
        PanelFeatureState[] panelFeatureStateArr = this.G;
        if (panelFeatureStateArr == null || panelFeatureStateArr.length <= i2) {
            PanelFeatureState[] panelFeatureStateArr2 = new PanelFeatureState[(i2 + 1)];
            if (panelFeatureStateArr != null) {
                System.arraycopy(panelFeatureStateArr, 0, panelFeatureStateArr2, 0, panelFeatureStateArr.length);
            }
            this.G = panelFeatureStateArr2;
            panelFeatureStateArr = panelFeatureStateArr2;
        }
        PanelFeatureState panelFeatureState = panelFeatureStateArr[i2];
        if (panelFeatureState != null) {
            return panelFeatureState;
        }
        PanelFeatureState panelFeatureState2 = new PanelFeatureState(i2);
        panelFeatureStateArr[i2] = panelFeatureState2;
        return panelFeatureState2;
    }

    private boolean a(PanelFeatureState panelFeatureState, int i2, KeyEvent keyEvent, int i3) {
        MenuBuilder menuBuilder;
        boolean z2 = false;
        if (keyEvent.isSystem()) {
            return false;
        }
        if ((panelFeatureState.m || b(panelFeatureState, keyEvent)) && (menuBuilder = panelFeatureState.j) != null) {
            z2 = menuBuilder.performShortcut(i2, keyEvent, i3);
        }
        if (z2 && (i3 & 1) == 0 && this.l == null) {
            a(panelFeatureState, true);
        }
        return z2;
    }

    private boolean a(boolean z2) {
        if (this.M) {
            return false;
        }
        int x2 = x();
        boolean b = b(g(x2), z2);
        if (x2 == 0) {
            p().e();
        } else {
            AutoNightModeManager autoNightModeManager = this.R;
            if (autoNightModeManager != null) {
                autoNightModeManager.a();
            }
        }
        if (x2 == 3) {
            C().e();
        } else {
            AutoNightModeManager autoNightModeManager2 = this.S;
            if (autoNightModeManager2 != null) {
                autoNightModeManager2.a();
            }
        }
        return b;
    }

    public final ActionBarDrawerToggle.Delegate a() {
        return new ActionBarDrawableToggleImpl();
    }
}
