package androidx.appcompat.view;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public abstract class ActionMode {

    /* renamed from: a  reason: collision with root package name */
    private Object f304a;
    private boolean b;

    public interface Callback {
        void a(ActionMode actionMode);

        boolean a(ActionMode actionMode, Menu menu);

        boolean a(ActionMode actionMode, MenuItem menuItem);

        boolean b(ActionMode actionMode, Menu menu);
    }

    public abstract void a();

    public abstract void a(int i);

    public abstract void a(View view);

    public abstract void a(CharSequence charSequence);

    public void a(Object obj) {
        this.f304a = obj;
    }

    public abstract View b();

    public abstract void b(int i);

    public abstract void b(CharSequence charSequence);

    public abstract Menu c();

    public abstract MenuInflater d();

    public abstract CharSequence e();

    public Object f() {
        return this.f304a;
    }

    public abstract CharSequence g();

    public boolean h() {
        return this.b;
    }

    public abstract void i();

    public boolean j() {
        return false;
    }

    public void a(boolean z) {
        this.b = z;
    }
}
