package androidx.appcompat.view;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.ActionBarContextView;
import java.lang.ref.WeakReference;

public class StandaloneActionMode extends ActionMode implements MenuBuilder.Callback {
    private Context c;
    private ActionBarContextView d;
    private ActionMode.Callback e;
    private WeakReference<View> f;
    private boolean g;
    private MenuBuilder h;

    public StandaloneActionMode(Context context, ActionBarContextView actionBarContextView, ActionMode.Callback callback, boolean z) {
        this.c = context;
        this.d = actionBarContextView;
        this.e = callback;
        MenuBuilder menuBuilder = new MenuBuilder(actionBarContextView.getContext());
        menuBuilder.c(1);
        this.h = menuBuilder;
        this.h.a((MenuBuilder.Callback) this);
    }

    public void a(CharSequence charSequence) {
        this.d.setSubtitle(charSequence);
    }

    public void b(CharSequence charSequence) {
        this.d.setTitle(charSequence);
    }

    public Menu c() {
        return this.h;
    }

    public MenuInflater d() {
        return new SupportMenuInflater(this.d.getContext());
    }

    public CharSequence e() {
        return this.d.getSubtitle();
    }

    public CharSequence g() {
        return this.d.getTitle();
    }

    public void i() {
        this.e.b(this, this.h);
    }

    public boolean j() {
        return this.d.b();
    }

    public void a(int i) {
        a((CharSequence) this.c.getString(i));
    }

    public void b(int i) {
        b((CharSequence) this.c.getString(i));
    }

    public void a(boolean z) {
        super.a(z);
        this.d.setTitleOptional(z);
    }

    public View b() {
        WeakReference<View> weakReference = this.f;
        if (weakReference != null) {
            return (View) weakReference.get();
        }
        return null;
    }

    public void a(View view) {
        this.d.setCustomView(view);
        this.f = view != null ? new WeakReference<>(view) : null;
    }

    public void a() {
        if (!this.g) {
            this.g = true;
            this.d.sendAccessibilityEvent(32);
            this.e.a(this);
        }
    }

    public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
        return this.e.a((ActionMode) this, menuItem);
    }

    public void a(MenuBuilder menuBuilder) {
        i();
        this.d.d();
    }
}
