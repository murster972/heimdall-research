package androidx.appcompat.view.menu;

import android.widget.ListView;

public interface ShowableListMenu {
    boolean c();

    void dismiss();

    ListView f();

    void show();
}
