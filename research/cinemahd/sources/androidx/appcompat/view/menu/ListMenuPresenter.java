package androidx.appcompat.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.R$layout;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.MenuView;
import java.util.ArrayList;

public class ListMenuPresenter implements MenuPresenter, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    Context f325a;
    LayoutInflater b;
    MenuBuilder c;
    ExpandedMenuView d;
    int e;
    int f;
    int g;
    private MenuPresenter.Callback h;
    MenuAdapter i;
    private int j;

    private class MenuAdapter extends BaseAdapter {

        /* renamed from: a  reason: collision with root package name */
        private int f326a = -1;

        public MenuAdapter() {
            a();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            MenuItemImpl f = ListMenuPresenter.this.c.f();
            if (f != null) {
                ArrayList<MenuItemImpl> j = ListMenuPresenter.this.c.j();
                int size = j.size();
                for (int i = 0; i < size; i++) {
                    if (j.get(i) == f) {
                        this.f326a = i;
                        return;
                    }
                }
            }
            this.f326a = -1;
        }

        public int getCount() {
            int size = ListMenuPresenter.this.c.j().size() - ListMenuPresenter.this.e;
            return this.f326a < 0 ? size : size - 1;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                ListMenuPresenter listMenuPresenter = ListMenuPresenter.this;
                view = listMenuPresenter.b.inflate(listMenuPresenter.g, viewGroup, false);
            }
            ((MenuView.ItemView) view).a(getItem(i), 0);
            return view;
        }

        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }

        public MenuItemImpl getItem(int i) {
            ArrayList<MenuItemImpl> j = ListMenuPresenter.this.c.j();
            int i2 = i + ListMenuPresenter.this.e;
            int i3 = this.f326a;
            if (i3 >= 0 && i2 >= i3) {
                i2++;
            }
            return j.get(i2);
        }
    }

    public ListMenuPresenter(Context context, int i2) {
        this(i2, 0);
        this.f325a = context;
        this.b = LayoutInflater.from(this.f325a);
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        int i2 = this.f;
        if (i2 != 0) {
            this.f325a = new ContextThemeWrapper(context, i2);
            this.b = LayoutInflater.from(this.f325a);
        } else if (this.f325a != null) {
            this.f325a = context;
            if (this.b == null) {
                this.b = LayoutInflater.from(this.f325a);
            }
        }
        this.c = menuBuilder;
        MenuAdapter menuAdapter = this.i;
        if (menuAdapter != null) {
            menuAdapter.notifyDataSetChanged();
        }
    }

    public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public void b(Bundle bundle) {
        SparseArray sparseArray = new SparseArray();
        ExpandedMenuView expandedMenuView = this.d;
        if (expandedMenuView != null) {
            expandedMenuView.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    public boolean b() {
        return false;
    }

    public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public ListAdapter c() {
        if (this.i == null) {
            this.i = new MenuAdapter();
        }
        return this.i;
    }

    public int getId() {
        return this.j;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.c.a((MenuItem) this.i.getItem(i2), (MenuPresenter) this, 0);
    }

    public ListMenuPresenter(int i2, int i3) {
        this.g = i2;
        this.f = i3;
    }

    public MenuView a(ViewGroup viewGroup) {
        if (this.d == null) {
            this.d = (ExpandedMenuView) this.b.inflate(R$layout.abc_expanded_menu_layout, viewGroup, false);
            if (this.i == null) {
                this.i = new MenuAdapter();
            }
            this.d.setAdapter(this.i);
            this.d.setOnItemClickListener(this);
        }
        return this.d;
    }

    public void a(boolean z) {
        MenuAdapter menuAdapter = this.i;
        if (menuAdapter != null) {
            menuAdapter.notifyDataSetChanged();
        }
    }

    public void a(MenuPresenter.Callback callback) {
        this.h = callback;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        new MenuDialogHelper(subMenuBuilder).a((IBinder) null);
        MenuPresenter.Callback callback = this.h;
        if (callback == null) {
            return true;
        }
        callback.a(subMenuBuilder);
        return true;
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        MenuPresenter.Callback callback = this.h;
        if (callback != null) {
            callback.a(menuBuilder, z);
        }
    }

    public void a(Bundle bundle) {
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.d.restoreHierarchyState(sparseParcelableArray);
        }
    }

    public Parcelable a() {
        if (this.d == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        b(bundle);
        return bundle;
    }

    public void a(Parcelable parcelable) {
        a((Bundle) parcelable);
    }
}
