package androidx.appcompat.view.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.MenuView;
import java.util.ArrayList;

public abstract class BaseMenuPresenter implements MenuPresenter {

    /* renamed from: a  reason: collision with root package name */
    protected Context f316a;
    protected Context b;
    protected MenuBuilder c;
    protected LayoutInflater d;
    private MenuPresenter.Callback e;
    private int f;
    private int g;
    protected MenuView h;
    private int i;

    public BaseMenuPresenter(Context context, int i2, int i3) {
        this.f316a = context;
        this.d = LayoutInflater.from(context);
        this.f = i2;
        this.g = i3;
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        this.b = context;
        LayoutInflater.from(this.b);
        this.c = menuBuilder;
    }

    public abstract void a(MenuItemImpl menuItemImpl, MenuView.ItemView itemView);

    public abstract boolean a(int i2, MenuItemImpl menuItemImpl);

    public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public MenuView b(ViewGroup viewGroup) {
        if (this.h == null) {
            this.h = (MenuView) this.d.inflate(this.f, viewGroup, false);
            this.h.a(this.c);
            a(true);
        }
        return this.h;
    }

    public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public MenuPresenter.Callback c() {
        return this.e;
    }

    public int getId() {
        return this.i;
    }

    public void a(boolean z) {
        ViewGroup viewGroup = (ViewGroup) this.h;
        if (viewGroup != null) {
            MenuBuilder menuBuilder = this.c;
            int i2 = 0;
            if (menuBuilder != null) {
                menuBuilder.b();
                ArrayList<MenuItemImpl> n = this.c.n();
                int size = n.size();
                int i3 = 0;
                for (int i4 = 0; i4 < size; i4++) {
                    MenuItemImpl menuItemImpl = n.get(i4);
                    if (a(i3, menuItemImpl)) {
                        View childAt = viewGroup.getChildAt(i3);
                        MenuItemImpl itemData = childAt instanceof MenuView.ItemView ? ((MenuView.ItemView) childAt).getItemData() : null;
                        View a2 = a(menuItemImpl, childAt, viewGroup);
                        if (menuItemImpl != itemData) {
                            a2.setPressed(false);
                            a2.jumpDrawablesToCurrentState();
                        }
                        if (a2 != childAt) {
                            a(a2, i3);
                        }
                        i3++;
                    }
                }
                i2 = i3;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.h).addView(view, i2);
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    public void a(MenuPresenter.Callback callback) {
        this.e = callback;
    }

    public MenuView.ItemView a(ViewGroup viewGroup) {
        return (MenuView.ItemView) this.d.inflate(this.g, viewGroup, false);
    }

    public View a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        MenuView.ItemView itemView;
        if (view instanceof MenuView.ItemView) {
            itemView = (MenuView.ItemView) view;
        } else {
            itemView = a(viewGroup);
        }
        a(menuItemImpl, itemView);
        return (View) itemView;
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        MenuPresenter.Callback callback = this.e;
        if (callback != null) {
            callback.a(menuBuilder, z);
        }
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        MenuPresenter.Callback callback = this.e;
        if (callback != null) {
            return callback.a(subMenuBuilder);
        }
        return false;
    }

    public void a(int i2) {
        this.i = i2;
    }
}
