package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.R$dimen;
import androidx.appcompat.R$layout;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.widget.MenuItemHoverListener;
import androidx.appcompat.widget.MenuPopupWindow;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import java.util.ArrayList;
import java.util.List;

final class CascadingMenuPopup extends MenuPopup implements MenuPresenter, View.OnKeyListener, PopupWindow.OnDismissListener {
    private static final int B = R$layout.abc_cascading_menu_item_layout;
    boolean A;
    private final Context b;
    private final int c;
    private final int d;
    private final int e;
    private final boolean f;
    final Handler g;
    private final List<MenuBuilder> h = new ArrayList();
    final List<CascadingMenuInfo> i = new ArrayList();
    final ViewTreeObserver.OnGlobalLayoutListener j = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (CascadingMenuPopup.this.c() && CascadingMenuPopup.this.i.size() > 0 && !CascadingMenuPopup.this.i.get(0).f322a.k()) {
                View view = CascadingMenuPopup.this.p;
                if (view == null || !view.isShown()) {
                    CascadingMenuPopup.this.dismiss();
                    return;
                }
                for (CascadingMenuInfo cascadingMenuInfo : CascadingMenuPopup.this.i) {
                    cascadingMenuInfo.f322a.show();
                }
            }
        }
    };
    private final View.OnAttachStateChangeListener k = new View.OnAttachStateChangeListener() {
        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = CascadingMenuPopup.this.y;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    CascadingMenuPopup.this.y = view.getViewTreeObserver();
                }
                CascadingMenuPopup cascadingMenuPopup = CascadingMenuPopup.this;
                cascadingMenuPopup.y.removeGlobalOnLayoutListener(cascadingMenuPopup.j);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };
    private final MenuItemHoverListener l = new MenuItemHoverListener() {
        public void a(final MenuBuilder menuBuilder, final MenuItem menuItem) {
            final CascadingMenuInfo cascadingMenuInfo = null;
            CascadingMenuPopup.this.g.removeCallbacksAndMessages((Object) null);
            int size = CascadingMenuPopup.this.i.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    i = -1;
                    break;
                } else if (menuBuilder == CascadingMenuPopup.this.i.get(i).b) {
                    break;
                } else {
                    i++;
                }
            }
            if (i != -1) {
                int i2 = i + 1;
                if (i2 < CascadingMenuPopup.this.i.size()) {
                    cascadingMenuInfo = CascadingMenuPopup.this.i.get(i2);
                }
                CascadingMenuPopup.this.g.postAtTime(new Runnable() {
                    public void run() {
                        CascadingMenuInfo cascadingMenuInfo = cascadingMenuInfo;
                        if (cascadingMenuInfo != null) {
                            CascadingMenuPopup.this.A = true;
                            cascadingMenuInfo.b.a(false);
                            CascadingMenuPopup.this.A = false;
                        }
                        if (menuItem.isEnabled() && menuItem.hasSubMenu()) {
                            menuBuilder.a(menuItem, 4);
                        }
                    }
                }, menuBuilder, SystemClock.uptimeMillis() + 200);
            }
        }

        public void b(MenuBuilder menuBuilder, MenuItem menuItem) {
            CascadingMenuPopup.this.g.removeCallbacksAndMessages(menuBuilder);
        }
    };
    private int m = 0;
    private int n = 0;
    private View o;
    View p;
    private int q;
    private boolean r;
    private boolean s;
    private int t;
    private int u;
    private boolean v;
    private boolean w;
    private MenuPresenter.Callback x;
    ViewTreeObserver y;
    private PopupWindow.OnDismissListener z;

    private static class CascadingMenuInfo {

        /* renamed from: a  reason: collision with root package name */
        public final MenuPopupWindow f322a;
        public final MenuBuilder b;
        public final int c;

        public CascadingMenuInfo(MenuPopupWindow menuPopupWindow, MenuBuilder menuBuilder, int i) {
            this.f322a = menuPopupWindow;
            this.b = menuBuilder;
            this.c = i;
        }

        public ListView a() {
            return this.f322a.f();
        }
    }

    public CascadingMenuPopup(Context context, View view, int i2, int i3, boolean z2) {
        this.b = context;
        this.o = view;
        this.d = i2;
        this.e = i3;
        this.f = z2;
        this.v = false;
        this.q = h();
        Resources resources = context.getResources();
        this.c = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(R$dimen.abc_config_prefDialogWidth));
        this.g = new Handler();
    }

    private int d(int i2) {
        List<CascadingMenuInfo> list = this.i;
        ListView a2 = list.get(list.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.p.getWindowVisibleDisplayFrame(rect);
        if (this.q == 1) {
            if (iArr[0] + a2.getWidth() + i2 > rect.right) {
                return 0;
            }
            return 1;
        } else if (iArr[0] - i2 < 0) {
            return 1;
        } else {
            return 0;
        }
    }

    private MenuPopupWindow g() {
        MenuPopupWindow menuPopupWindow = new MenuPopupWindow(this.b, (AttributeSet) null, this.d, this.e);
        menuPopupWindow.a(this.l);
        menuPopupWindow.a((AdapterView.OnItemClickListener) this);
        menuPopupWindow.a((PopupWindow.OnDismissListener) this);
        menuPopupWindow.a(this.o);
        menuPopupWindow.f(this.n);
        menuPopupWindow.a(true);
        menuPopupWindow.g(2);
        return menuPopupWindow;
    }

    private int h() {
        return ViewCompat.m(this.o) == 1 ? 0 : 1;
    }

    public Parcelable a() {
        return null;
    }

    public void a(Parcelable parcelable) {
    }

    public void a(MenuBuilder menuBuilder) {
        menuBuilder.a((MenuPresenter) this, this.b);
        if (c()) {
            d(menuBuilder);
        } else {
            this.h.add(menuBuilder);
        }
    }

    public void b(boolean z2) {
        this.v = z2;
    }

    public boolean b() {
        return false;
    }

    public boolean c() {
        return this.i.size() > 0 && this.i.get(0).f322a.c();
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return false;
    }

    public void dismiss() {
        int size = this.i.size();
        if (size > 0) {
            CascadingMenuInfo[] cascadingMenuInfoArr = (CascadingMenuInfo[]) this.i.toArray(new CascadingMenuInfo[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                CascadingMenuInfo cascadingMenuInfo = cascadingMenuInfoArr[i2];
                if (cascadingMenuInfo.f322a.c()) {
                    cascadingMenuInfo.f322a.dismiss();
                }
            }
        }
    }

    public ListView f() {
        if (this.i.isEmpty()) {
            return null;
        }
        List<CascadingMenuInfo> list = this.i;
        return list.get(list.size() - 1).a();
    }

    public void onDismiss() {
        CascadingMenuInfo cascadingMenuInfo;
        int size = this.i.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                cascadingMenuInfo = null;
                break;
            }
            cascadingMenuInfo = this.i.get(i2);
            if (!cascadingMenuInfo.f322a.c()) {
                break;
            }
            i2++;
        }
        if (cascadingMenuInfo != null) {
            cascadingMenuInfo.b.a(false);
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    public void show() {
        if (!c()) {
            for (MenuBuilder d2 : this.h) {
                d(d2);
            }
            this.h.clear();
            this.p = this.o;
            if (this.p != null) {
                boolean z2 = this.y == null;
                this.y = this.p.getViewTreeObserver();
                if (z2) {
                    this.y.addOnGlobalLayoutListener(this.j);
                }
                this.p.addOnAttachStateChangeListener(this.k);
            }
        }
    }

    private int c(MenuBuilder menuBuilder) {
        int size = this.i.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (menuBuilder == this.i.get(i2).b) {
                return i2;
            }
        }
        return -1;
    }

    public void b(int i2) {
        this.r = true;
        this.t = i2;
    }

    private MenuItem a(MenuBuilder menuBuilder, MenuBuilder menuBuilder2) {
        int size = menuBuilder.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = menuBuilder.getItem(i2);
            if (item.hasSubMenu() && menuBuilder2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    public void c(int i2) {
        this.s = true;
        this.u = i2;
    }

    public void c(boolean z2) {
        this.w = z2;
    }

    private View a(CascadingMenuInfo cascadingMenuInfo, MenuBuilder menuBuilder) {
        int i2;
        MenuAdapter menuAdapter;
        int firstVisiblePosition;
        MenuItem a2 = a(cascadingMenuInfo.b, menuBuilder);
        if (a2 == null) {
            return null;
        }
        ListView a3 = cascadingMenuInfo.a();
        ListAdapter adapter = a3.getAdapter();
        int i3 = 0;
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            menuAdapter = (MenuAdapter) headerViewListAdapter.getWrappedAdapter();
        } else {
            menuAdapter = (MenuAdapter) adapter;
            i2 = 0;
        }
        int count = menuAdapter.getCount();
        while (true) {
            if (i3 >= count) {
                i3 = -1;
                break;
            } else if (a2 == menuAdapter.getItem(i3)) {
                break;
            } else {
                i3++;
            }
        }
        if (i3 != -1 && (firstVisiblePosition = (i3 + i2) - a3.getFirstVisiblePosition()) >= 0 && firstVisiblePosition < a3.getChildCount()) {
            return a3.getChildAt(firstVisiblePosition);
        }
        return null;
    }

    private void d(MenuBuilder menuBuilder) {
        View view;
        CascadingMenuInfo cascadingMenuInfo;
        int i2;
        int i3;
        int i4;
        LayoutInflater from = LayoutInflater.from(this.b);
        MenuAdapter menuAdapter = new MenuAdapter(menuBuilder, from, this.f, B);
        if (!c() && this.v) {
            menuAdapter.a(true);
        } else if (c()) {
            menuAdapter.a(MenuPopup.b(menuBuilder));
        }
        int a2 = MenuPopup.a(menuAdapter, (ViewGroup) null, this.b, this.c);
        MenuPopupWindow g2 = g();
        g2.a((ListAdapter) menuAdapter);
        g2.e(a2);
        g2.f(this.n);
        if (this.i.size() > 0) {
            List<CascadingMenuInfo> list = this.i;
            cascadingMenuInfo = list.get(list.size() - 1);
            view = a(cascadingMenuInfo, menuBuilder);
        } else {
            cascadingMenuInfo = null;
            view = null;
        }
        if (view != null) {
            g2.c(false);
            g2.a((Object) null);
            int d2 = d(a2);
            boolean z2 = d2 == 1;
            this.q = d2;
            if (Build.VERSION.SDK_INT >= 26) {
                g2.a(view);
                i3 = 0;
                i2 = 0;
            } else {
                int[] iArr = new int[2];
                this.o.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                if ((this.n & 7) == 5) {
                    iArr[0] = iArr[0] + this.o.getWidth();
                    iArr2[0] = iArr2[0] + view.getWidth();
                }
                i2 = iArr2[0] - iArr[0];
                i3 = iArr2[1] - iArr[1];
            }
            if ((this.n & 5) != 5) {
                if (z2) {
                    a2 = view.getWidth();
                }
                i4 = i2 - a2;
                g2.a(i4);
                g2.b(true);
                g2.b(i3);
            } else if (!z2) {
                a2 = view.getWidth();
                i4 = i2 - a2;
                g2.a(i4);
                g2.b(true);
                g2.b(i3);
            }
            i4 = i2 + a2;
            g2.a(i4);
            g2.b(true);
            g2.b(i3);
        } else {
            if (this.r) {
                g2.a(this.t);
            }
            if (this.s) {
                g2.b(this.u);
            }
            g2.a(e());
        }
        this.i.add(new CascadingMenuInfo(g2, menuBuilder, this.q));
        g2.show();
        ListView f2 = g2.f();
        f2.setOnKeyListener(this);
        if (cascadingMenuInfo == null && this.w && menuBuilder.h() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(R$layout.abc_popup_menu_header_item_layout, f2, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(menuBuilder.h());
            f2.addHeaderView(frameLayout, (Object) null, false);
            g2.show();
        }
    }

    public void a(boolean z2) {
        for (CascadingMenuInfo a2 : this.i) {
            MenuPopup.a(a2.a().getAdapter()).notifyDataSetChanged();
        }
    }

    public void a(MenuPresenter.Callback callback) {
        this.x = callback;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        for (CascadingMenuInfo next : this.i) {
            if (subMenuBuilder == next.b) {
                next.a().requestFocus();
                return true;
            }
        }
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        a((MenuBuilder) subMenuBuilder);
        MenuPresenter.Callback callback = this.x;
        if (callback != null) {
            callback.a(subMenuBuilder);
        }
        return true;
    }

    public void a(MenuBuilder menuBuilder, boolean z2) {
        int c2 = c(menuBuilder);
        if (c2 >= 0) {
            int i2 = c2 + 1;
            if (i2 < this.i.size()) {
                this.i.get(i2).b.a(false);
            }
            CascadingMenuInfo remove = this.i.remove(c2);
            remove.b.b((MenuPresenter) this);
            if (this.A) {
                remove.f322a.b((Object) null);
                remove.f322a.d(0);
            }
            remove.f322a.dismiss();
            int size = this.i.size();
            if (size > 0) {
                this.q = this.i.get(size - 1).c;
            } else {
                this.q = h();
            }
            if (size == 0) {
                dismiss();
                MenuPresenter.Callback callback = this.x;
                if (callback != null) {
                    callback.a(menuBuilder, true);
                }
                ViewTreeObserver viewTreeObserver = this.y;
                if (viewTreeObserver != null) {
                    if (viewTreeObserver.isAlive()) {
                        this.y.removeGlobalOnLayoutListener(this.j);
                    }
                    this.y = null;
                }
                this.p.removeOnAttachStateChangeListener(this.k);
                this.z.onDismiss();
            } else if (z2) {
                this.i.get(0).b.a(false);
            }
        }
    }

    public void a(int i2) {
        if (this.m != i2) {
            this.m = i2;
            this.n = GravityCompat.a(i2, ViewCompat.m(this.o));
        }
    }

    public void a(View view) {
        if (this.o != view) {
            this.o = view;
            this.n = GravityCompat.a(this.m, ViewCompat.m(this.o));
        }
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.z = onDismissListener;
    }
}
