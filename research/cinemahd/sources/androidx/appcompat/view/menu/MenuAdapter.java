package androidx.appcompat.view.menu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.appcompat.view.menu.MenuView;
import java.util.ArrayList;

public class MenuAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    MenuBuilder f327a;
    private int b = -1;
    private boolean c;
    private final boolean d;
    private final LayoutInflater e;
    private final int f;

    public MenuAdapter(MenuBuilder menuBuilder, LayoutInflater layoutInflater, boolean z, int i) {
        this.d = z;
        this.e = layoutInflater;
        this.f327a = menuBuilder;
        this.f = i;
        a();
    }

    public void a(boolean z) {
        this.c = z;
    }

    public MenuBuilder b() {
        return this.f327a;
    }

    public int getCount() {
        ArrayList<MenuItemImpl> j = this.d ? this.f327a.j() : this.f327a.n();
        if (this.b < 0) {
            return j.size();
        }
        return j.size() - 1;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.e.inflate(this.f, viewGroup, false);
        }
        int groupId = getItem(i).getGroupId();
        int i2 = i - 1;
        ListMenuItemView listMenuItemView = (ListMenuItemView) view;
        listMenuItemView.setGroupDividerEnabled(this.f327a.o() && groupId != (i2 >= 0 ? getItem(i2).getGroupId() : groupId));
        MenuView.ItemView itemView = (MenuView.ItemView) view;
        if (this.c) {
            listMenuItemView.setForceShowIcon(true);
        }
        itemView.a(getItem(i), 0);
        return view;
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        MenuItemImpl f2 = this.f327a.f();
        if (f2 != null) {
            ArrayList<MenuItemImpl> j = this.f327a.j();
            int size = j.size();
            for (int i = 0; i < size; i++) {
                if (j.get(i) == f2) {
                    this.b = i;
                    return;
                }
            }
        }
        this.b = -1;
    }

    public MenuItemImpl getItem(int i) {
        ArrayList<MenuItemImpl> j = this.d ? this.f327a.j() : this.f327a.n();
        int i2 = this.b;
        if (i2 >= 0 && i >= i2) {
            i++;
        }
        return j.get(i);
    }
}
