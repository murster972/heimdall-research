package androidx.appcompat.view.menu;

import android.content.DialogInterface;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.R$layout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuPresenter;

class MenuDialogHelper implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, MenuPresenter.Callback {

    /* renamed from: a  reason: collision with root package name */
    private MenuBuilder f329a;
    private AlertDialog b;
    ListMenuPresenter c;
    private MenuPresenter.Callback d;

    public MenuDialogHelper(MenuBuilder menuBuilder) {
        this.f329a = menuBuilder;
    }

    public void a(IBinder iBinder) {
        MenuBuilder menuBuilder = this.f329a;
        AlertDialog.Builder builder = new AlertDialog.Builder(menuBuilder.e());
        this.c = new ListMenuPresenter(builder.b(), R$layout.abc_list_menu_item_layout);
        this.c.a((MenuPresenter.Callback) this);
        this.f329a.a((MenuPresenter) this.c);
        builder.a(this.c.c(), (DialogInterface.OnClickListener) this);
        View i = menuBuilder.i();
        if (i != null) {
            builder.a(i);
        } else {
            builder.a(menuBuilder.g());
            builder.b(menuBuilder.h());
        }
        builder.a((DialogInterface.OnKeyListener) this);
        this.b = builder.a();
        this.b.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.b.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.b.show();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f329a.a((MenuItem) (MenuItemImpl) this.c.c().getItem(i), 0);
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.c.a(this.f329a, true);
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.b.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.b.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.f329a.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.f329a.performShortcut(i, keyEvent, 0);
    }

    public void a() {
        AlertDialog alertDialog = this.b;
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        if (z || menuBuilder == this.f329a) {
            a();
        }
        MenuPresenter.Callback callback = this.d;
        if (callback != null) {
            callback.a(menuBuilder, z);
        }
    }

    public boolean a(MenuBuilder menuBuilder) {
        MenuPresenter.Callback callback = this.d;
        if (callback != null) {
            return callback.a(menuBuilder);
        }
        return false;
    }
}
