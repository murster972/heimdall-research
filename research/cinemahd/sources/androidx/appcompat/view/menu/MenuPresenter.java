package androidx.appcompat.view.menu;

import android.content.Context;
import android.os.Parcelable;

public interface MenuPresenter {

    public interface Callback {
        void a(MenuBuilder menuBuilder, boolean z);

        boolean a(MenuBuilder menuBuilder);
    }

    Parcelable a();

    void a(Context context, MenuBuilder menuBuilder);

    void a(Parcelable parcelable);

    void a(MenuBuilder menuBuilder, boolean z);

    void a(Callback callback);

    void a(boolean z);

    boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl);

    boolean a(SubMenuBuilder subMenuBuilder);

    boolean b();

    boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl);

    int getId();
}
