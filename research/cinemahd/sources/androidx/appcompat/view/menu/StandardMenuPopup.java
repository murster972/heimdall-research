package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.R$dimen;
import androidx.appcompat.R$layout;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.widget.MenuPopupWindow;
import androidx.core.view.ViewCompat;

final class StandardMenuPopup extends MenuPopup implements PopupWindow.OnDismissListener, AdapterView.OnItemClickListener, MenuPresenter, View.OnKeyListener {
    private static final int v = R$layout.abc_popup_menu_item_layout;
    private final Context b;
    private final MenuBuilder c;
    private final MenuAdapter d;
    private final boolean e;
    private final int f;
    private final int g;
    private final int h;
    final MenuPopupWindow i;
    final ViewTreeObserver.OnGlobalLayoutListener j = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (StandardMenuPopup.this.c() && !StandardMenuPopup.this.i.k()) {
                View view = StandardMenuPopup.this.n;
                if (view == null || !view.isShown()) {
                    StandardMenuPopup.this.dismiss();
                } else {
                    StandardMenuPopup.this.i.show();
                }
            }
        }
    };
    private final View.OnAttachStateChangeListener k = new View.OnAttachStateChangeListener() {
        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = StandardMenuPopup.this.p;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    StandardMenuPopup.this.p = view.getViewTreeObserver();
                }
                StandardMenuPopup standardMenuPopup = StandardMenuPopup.this;
                standardMenuPopup.p.removeGlobalOnLayoutListener(standardMenuPopup.j);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };
    private PopupWindow.OnDismissListener l;
    private View m;
    View n;
    private MenuPresenter.Callback o;
    ViewTreeObserver p;
    private boolean q;
    private boolean r;
    private int s;
    private int t = 0;
    private boolean u;

    public StandardMenuPopup(Context context, MenuBuilder menuBuilder, View view, int i2, int i3, boolean z) {
        this.b = context;
        this.c = menuBuilder;
        this.e = z;
        this.d = new MenuAdapter(menuBuilder, LayoutInflater.from(context), this.e, v);
        this.g = i2;
        this.h = i3;
        Resources resources = context.getResources();
        this.f = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(R$dimen.abc_config_prefDialogWidth));
        this.m = view;
        this.i = new MenuPopupWindow(this.b, (AttributeSet) null, this.g, this.h);
        menuBuilder.a((MenuPresenter) this, context);
    }

    private boolean g() {
        View view;
        if (c()) {
            return true;
        }
        if (this.q || (view = this.m) == null) {
            return false;
        }
        this.n = view;
        this.i.a((PopupWindow.OnDismissListener) this);
        this.i.a((AdapterView.OnItemClickListener) this);
        this.i.a(true);
        View view2 = this.n;
        boolean z = this.p == null;
        this.p = view2.getViewTreeObserver();
        if (z) {
            this.p.addOnGlobalLayoutListener(this.j);
        }
        view2.addOnAttachStateChangeListener(this.k);
        this.i.a(view2);
        this.i.f(this.t);
        if (!this.r) {
            this.s = MenuPopup.a(this.d, (ViewGroup) null, this.b, this.f);
            this.r = true;
        }
        this.i.e(this.s);
        this.i.g(2);
        this.i.a(e());
        this.i.show();
        ListView f2 = this.i.f();
        f2.setOnKeyListener(this);
        if (this.u && this.c.h() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.b).inflate(R$layout.abc_popup_menu_header_item_layout, f2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.c.h());
            }
            frameLayout.setEnabled(false);
            f2.addHeaderView(frameLayout, (Object) null, false);
        }
        this.i.a((ListAdapter) this.d);
        this.i.show();
        return true;
    }

    public Parcelable a() {
        return null;
    }

    public void a(int i2) {
        this.t = i2;
    }

    public void a(Parcelable parcelable) {
    }

    public void a(MenuBuilder menuBuilder) {
    }

    public void b(boolean z) {
        this.d.a(z);
    }

    public boolean b() {
        return false;
    }

    public boolean c() {
        return !this.q && this.i.c();
    }

    public void dismiss() {
        if (c()) {
            this.i.dismiss();
        }
    }

    public ListView f() {
        return this.i.f();
    }

    public void onDismiss() {
        this.q = true;
        this.c.close();
        ViewTreeObserver viewTreeObserver = this.p;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.p = this.n.getViewTreeObserver();
            }
            this.p.removeGlobalOnLayoutListener(this.j);
            this.p = null;
        }
        this.n.removeOnAttachStateChangeListener(this.k);
        PopupWindow.OnDismissListener onDismissListener = this.l;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    public void show() {
        if (!g()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    public void a(boolean z) {
        this.r = false;
        MenuAdapter menuAdapter = this.d;
        if (menuAdapter != null) {
            menuAdapter.notifyDataSetChanged();
        }
    }

    public void b(int i2) {
        this.i.a(i2);
    }

    public void c(int i2) {
        this.i.b(i2);
    }

    public void c(boolean z) {
        this.u = z;
    }

    public void a(MenuPresenter.Callback callback) {
        this.o = callback;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        if (subMenuBuilder.hasVisibleItems()) {
            MenuPopupHelper menuPopupHelper = new MenuPopupHelper(this.b, subMenuBuilder, this.n, this.e, this.g, this.h);
            menuPopupHelper.a(this.o);
            menuPopupHelper.a(MenuPopup.b((MenuBuilder) subMenuBuilder));
            menuPopupHelper.a(this.l);
            this.l = null;
            this.c.a(false);
            int a2 = this.i.a();
            int e2 = this.i.e();
            if ((Gravity.getAbsoluteGravity(this.t, ViewCompat.m(this.m)) & 7) == 5) {
                a2 += this.m.getWidth();
            }
            if (menuPopupHelper.a(a2, e2)) {
                MenuPresenter.Callback callback = this.o;
                if (callback == null) {
                    return true;
                }
                callback.a(subMenuBuilder);
                return true;
            }
        }
        return false;
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        if (menuBuilder == this.c) {
            dismiss();
            MenuPresenter.Callback callback = this.o;
            if (callback != null) {
                callback.a(menuBuilder, z);
            }
        }
    }

    public void a(View view) {
        this.m = view;
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.l = onDismissListener;
    }
}
