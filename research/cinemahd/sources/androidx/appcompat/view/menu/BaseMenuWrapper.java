package androidx.appcompat.view.menu;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;
import androidx.collection.ArrayMap;
import androidx.core.internal.view.SupportMenuItem;
import androidx.core.internal.view.SupportSubMenu;
import java.util.Iterator;
import java.util.Map;

abstract class BaseMenuWrapper {

    /* renamed from: a  reason: collision with root package name */
    final Context f317a;
    private Map<SupportMenuItem, MenuItem> b;
    private Map<SupportSubMenu, SubMenu> c;

    BaseMenuWrapper(Context context) {
        this.f317a = context;
    }

    /* access modifiers changed from: package-private */
    public final MenuItem a(MenuItem menuItem) {
        if (!(menuItem instanceof SupportMenuItem)) {
            return menuItem;
        }
        SupportMenuItem supportMenuItem = (SupportMenuItem) menuItem;
        if (this.b == null) {
            this.b = new ArrayMap();
        }
        MenuItem menuItem2 = this.b.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        MenuItemWrapperICS menuItemWrapperICS = new MenuItemWrapperICS(this.f317a, supportMenuItem);
        this.b.put(supportMenuItem, menuItemWrapperICS);
        return menuItemWrapperICS;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        Map<SupportMenuItem, MenuItem> map = this.b;
        if (map != null) {
            map.clear();
        }
        Map<SupportSubMenu, SubMenu> map2 = this.c;
        if (map2 != null) {
            map2.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        Map<SupportMenuItem, MenuItem> map = this.b;
        if (map != null) {
            Iterator<SupportMenuItem> it2 = map.keySet().iterator();
            while (it2.hasNext()) {
                if (i == it2.next().getItemId()) {
                    it2.remove();
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final SubMenu a(SubMenu subMenu) {
        if (!(subMenu instanceof SupportSubMenu)) {
            return subMenu;
        }
        SupportSubMenu supportSubMenu = (SupportSubMenu) subMenu;
        if (this.c == null) {
            this.c = new ArrayMap();
        }
        SubMenu subMenu2 = this.c.get(supportSubMenu);
        if (subMenu2 != null) {
            return subMenu2;
        }
        SubMenuWrapperICS subMenuWrapperICS = new SubMenuWrapperICS(this.f317a, supportSubMenu);
        this.c.put(supportSubMenu, subMenuWrapperICS);
        return subMenuWrapperICS;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        Map<SupportMenuItem, MenuItem> map = this.b;
        if (map != null) {
            Iterator<SupportMenuItem> it2 = map.keySet().iterator();
            while (it2.hasNext()) {
                if (i == it2.next().getGroupId()) {
                    it2.remove();
                }
            }
        }
    }
}
