package androidx.appcompat.view.menu;

public interface MenuView {

    public interface ItemView {
        void a(MenuItemImpl menuItemImpl, int i);

        boolean a();

        MenuItemImpl getItemData();
    }

    void a(MenuBuilder menuBuilder);
}
