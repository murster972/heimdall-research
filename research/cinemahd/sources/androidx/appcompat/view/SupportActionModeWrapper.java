package androidx.appcompat.view;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.view.menu.MenuItemWrapperICS;
import androidx.appcompat.view.menu.MenuWrapperICS;
import androidx.collection.SimpleArrayMap;
import androidx.core.internal.view.SupportMenu;
import androidx.core.internal.view.SupportMenuItem;
import java.util.ArrayList;

public class SupportActionModeWrapper extends ActionMode {

    /* renamed from: a  reason: collision with root package name */
    final Context f306a;
    final ActionMode b;

    public SupportActionModeWrapper(Context context, ActionMode actionMode) {
        this.f306a = context;
        this.b = actionMode;
    }

    public void finish() {
        this.b.a();
    }

    public View getCustomView() {
        return this.b.b();
    }

    public Menu getMenu() {
        return new MenuWrapperICS(this.f306a, (SupportMenu) this.b.c());
    }

    public MenuInflater getMenuInflater() {
        return this.b.d();
    }

    public CharSequence getSubtitle() {
        return this.b.e();
    }

    public Object getTag() {
        return this.b.f();
    }

    public CharSequence getTitle() {
        return this.b.g();
    }

    public boolean getTitleOptionalHint() {
        return this.b.h();
    }

    public void invalidate() {
        this.b.i();
    }

    public boolean isTitleOptional() {
        return this.b.j();
    }

    public void setCustomView(View view) {
        this.b.a(view);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.b.a(charSequence);
    }

    public void setTag(Object obj) {
        this.b.a(obj);
    }

    public void setTitle(CharSequence charSequence) {
        this.b.b(charSequence);
    }

    public void setTitleOptionalHint(boolean z) {
        this.b.a(z);
    }

    public void setSubtitle(int i) {
        this.b.a(i);
    }

    public void setTitle(int i) {
        this.b.b(i);
    }

    public static class CallbackWrapper implements ActionMode.Callback {

        /* renamed from: a  reason: collision with root package name */
        final ActionMode.Callback f307a;
        final Context b;
        final ArrayList<SupportActionModeWrapper> c = new ArrayList<>();
        final SimpleArrayMap<Menu, Menu> d = new SimpleArrayMap<>();

        public CallbackWrapper(Context context, ActionMode.Callback callback) {
            this.b = context;
            this.f307a = callback;
        }

        public boolean a(ActionMode actionMode, Menu menu) {
            return this.f307a.onCreateActionMode(b(actionMode), a(menu));
        }

        public boolean b(ActionMode actionMode, Menu menu) {
            return this.f307a.onPrepareActionMode(b(actionMode), a(menu));
        }

        public boolean a(ActionMode actionMode, MenuItem menuItem) {
            return this.f307a.onActionItemClicked(b(actionMode), new MenuItemWrapperICS(this.b, (SupportMenuItem) menuItem));
        }

        public android.view.ActionMode b(ActionMode actionMode) {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                SupportActionModeWrapper supportActionModeWrapper = this.c.get(i);
                if (supportActionModeWrapper != null && supportActionModeWrapper.b == actionMode) {
                    return supportActionModeWrapper;
                }
            }
            SupportActionModeWrapper supportActionModeWrapper2 = new SupportActionModeWrapper(this.b, actionMode);
            this.c.add(supportActionModeWrapper2);
            return supportActionModeWrapper2;
        }

        public void a(ActionMode actionMode) {
            this.f307a.onDestroyActionMode(b(actionMode));
        }

        private Menu a(Menu menu) {
            Menu menu2 = this.d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            MenuWrapperICS menuWrapperICS = new MenuWrapperICS(this.b, (SupportMenu) menu);
            this.d.put(menu, menuWrapperICS);
            return menuWrapperICS;
        }
    }
}
