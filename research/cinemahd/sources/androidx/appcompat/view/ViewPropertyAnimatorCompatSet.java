package androidx.appcompat.view;

import android.view.View;
import android.view.animation.Interpolator;
import androidx.core.view.ViewPropertyAnimatorCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.core.view.ViewPropertyAnimatorListenerAdapter;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewPropertyAnimatorCompatSet {

    /* renamed from: a  reason: collision with root package name */
    final ArrayList<ViewPropertyAnimatorCompat> f311a = new ArrayList<>();
    private long b = -1;
    private Interpolator c;
    ViewPropertyAnimatorListener d;
    private boolean e;
    private final ViewPropertyAnimatorListenerAdapter f = new ViewPropertyAnimatorListenerAdapter() {

        /* renamed from: a  reason: collision with root package name */
        private boolean f312a = false;
        private int b = 0;

        public void onAnimationEnd(View view) {
            int i = this.b + 1;
            this.b = i;
            if (i == ViewPropertyAnimatorCompatSet.this.f311a.size()) {
                ViewPropertyAnimatorListener viewPropertyAnimatorListener = ViewPropertyAnimatorCompatSet.this.d;
                if (viewPropertyAnimatorListener != null) {
                    viewPropertyAnimatorListener.onAnimationEnd((View) null);
                }
                onEnd();
            }
        }

        public void onAnimationStart(View view) {
            if (!this.f312a) {
                this.f312a = true;
                ViewPropertyAnimatorListener viewPropertyAnimatorListener = ViewPropertyAnimatorCompatSet.this.d;
                if (viewPropertyAnimatorListener != null) {
                    viewPropertyAnimatorListener.onAnimationStart((View) null);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void onEnd() {
            this.b = 0;
            this.f312a = false;
            ViewPropertyAnimatorCompatSet.this.b();
        }
    };

    public ViewPropertyAnimatorCompatSet a(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat) {
        if (!this.e) {
            this.f311a.add(viewPropertyAnimatorCompat);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.e = false;
    }

    public void c() {
        if (!this.e) {
            Iterator<ViewPropertyAnimatorCompat> it2 = this.f311a.iterator();
            while (it2.hasNext()) {
                ViewPropertyAnimatorCompat next = it2.next();
                long j = this.b;
                if (j >= 0) {
                    next.a(j);
                }
                Interpolator interpolator = this.c;
                if (interpolator != null) {
                    next.a(interpolator);
                }
                if (this.d != null) {
                    next.a((ViewPropertyAnimatorListener) this.f);
                }
                next.c();
            }
            this.e = true;
        }
    }

    public ViewPropertyAnimatorCompatSet a(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2) {
        this.f311a.add(viewPropertyAnimatorCompat);
        viewPropertyAnimatorCompat2.b(viewPropertyAnimatorCompat.b());
        this.f311a.add(viewPropertyAnimatorCompat2);
        return this;
    }

    public void a() {
        if (this.e) {
            Iterator<ViewPropertyAnimatorCompat> it2 = this.f311a.iterator();
            while (it2.hasNext()) {
                it2.next().a();
            }
            this.e = false;
        }
    }

    public ViewPropertyAnimatorCompatSet a(long j) {
        if (!this.e) {
            this.b = j;
        }
        return this;
    }

    public ViewPropertyAnimatorCompatSet a(Interpolator interpolator) {
        if (!this.e) {
            this.c = interpolator;
        }
        return this;
    }

    public ViewPropertyAnimatorCompatSet a(ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        if (!this.e) {
            this.d = viewPropertyAnimatorListener;
        }
        return this;
    }
}
