package androidx.appcompat.graphics.drawable;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import androidx.core.graphics.drawable.DrawableCompat;

public class DrawableWrapper extends Drawable implements Drawable.Callback {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f300a;

    public DrawableWrapper(Drawable drawable) {
        a(drawable);
    }

    public Drawable a() {
        return this.f300a;
    }

    public void draw(Canvas canvas) {
        this.f300a.draw(canvas);
    }

    public int getChangingConfigurations() {
        return this.f300a.getChangingConfigurations();
    }

    public Drawable getCurrent() {
        return this.f300a.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.f300a.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        return this.f300a.getIntrinsicWidth();
    }

    public int getMinimumHeight() {
        return this.f300a.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.f300a.getMinimumWidth();
    }

    public int getOpacity() {
        return this.f300a.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        return this.f300a.getPadding(rect);
    }

    public int[] getState() {
        return this.f300a.getState();
    }

    public Region getTransparentRegion() {
        return this.f300a.getTransparentRegion();
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public boolean isAutoMirrored() {
        return DrawableCompat.f(this.f300a);
    }

    public boolean isStateful() {
        return this.f300a.isStateful();
    }

    public void jumpToCurrentState() {
        DrawableCompat.g(this.f300a);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.f300a.setBounds(rect);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f300a.setLevel(i);
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void setAlpha(int i) {
        this.f300a.setAlpha(i);
    }

    public void setAutoMirrored(boolean z) {
        DrawableCompat.a(this.f300a, z);
    }

    public void setChangingConfigurations(int i) {
        this.f300a.setChangingConfigurations(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f300a.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.f300a.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f300a.setFilterBitmap(z);
    }

    public void setHotspot(float f, float f2) {
        DrawableCompat.a(this.f300a, f, f2);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        DrawableCompat.a(this.f300a, i, i2, i3, i4);
    }

    public boolean setState(int[] iArr) {
        return this.f300a.setState(iArr);
    }

    public void setTint(int i) {
        DrawableCompat.b(this.f300a, i);
    }

    public void setTintList(ColorStateList colorStateList) {
        DrawableCompat.a(this.f300a, colorStateList);
    }

    public void setTintMode(PorterDuff.Mode mode) {
        DrawableCompat.a(this.f300a, mode);
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f300a.setVisible(z, z2);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    public void a(Drawable drawable) {
        Drawable drawable2 = this.f300a;
        if (drawable2 != null) {
            drawable2.setCallback((Drawable.Callback) null);
        }
        this.f300a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
