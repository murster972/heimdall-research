package androidx.appcompat.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.R$attr;
import androidx.appcompat.R$drawable;
import androidx.appcompat.R$id;
import androidx.appcompat.R$string;
import androidx.appcompat.R$styleable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.menu.ActionMenuItem;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.core.view.ViewPropertyAnimatorListenerAdapter;

public class ToolbarWidgetWrapper implements DecorToolbar {

    /* renamed from: a  reason: collision with root package name */
    Toolbar f445a;
    private int b;
    private View c;
    private View d;
    private Drawable e;
    private Drawable f;
    private Drawable g;
    private boolean h;
    CharSequence i;
    private CharSequence j;
    private CharSequence k;
    Window.Callback l;
    boolean m;
    private ActionMenuPresenter n;
    private int o;
    private int p;
    private Drawable q;

    public ToolbarWidgetWrapper(Toolbar toolbar, boolean z) {
        this(toolbar, z, R$string.abc_action_bar_up_description, R$drawable.abc_ic_ab_back_material);
    }

    private void c(CharSequence charSequence) {
        this.i = charSequence;
        if ((this.b & 8) != 0) {
            this.f445a.setTitle(charSequence);
        }
    }

    private int o() {
        if (this.f445a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.f445a.getNavigationIcon();
        return 15;
    }

    private void p() {
        if ((this.b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.k)) {
            this.f445a.setNavigationContentDescription(this.p);
        } else {
            this.f445a.setNavigationContentDescription(this.k);
        }
    }

    private void q() {
        if ((this.b & 4) != 0) {
            Toolbar toolbar = this.f445a;
            Drawable drawable = this.g;
            if (drawable == null) {
                drawable = this.q;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.f445a.setNavigationIcon((Drawable) null);
    }

    private void r() {
        Drawable drawable;
        int i2 = this.b;
        if ((i2 & 2) == 0) {
            drawable = null;
        } else if ((i2 & 1) != 0) {
            drawable = this.f;
            if (drawable == null) {
                drawable = this.e;
            }
        } else {
            drawable = this.e;
        }
        this.f445a.setLogo(drawable);
    }

    public void a(CharSequence charSequence) {
        this.j = charSequence;
        if ((this.b & 8) != 0) {
            this.f445a.setSubtitle(charSequence);
        }
    }

    public void a(boolean z) {
    }

    public void b(Drawable drawable) {
        this.f = drawable;
        r();
    }

    public void collapseActionView() {
        this.f445a.c();
    }

    public boolean d() {
        return this.f445a.i();
    }

    public void e() {
        this.m = true;
    }

    public void f(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.f445a.getNavigationContentDescription())) {
                b(this.p);
            }
        }
    }

    public boolean g() {
        return this.f445a.f();
    }

    public Context getContext() {
        return this.f445a.getContext();
    }

    public CharSequence getTitle() {
        return this.f445a.getTitle();
    }

    public Menu h() {
        return this.f445a.getMenu();
    }

    public int i() {
        return this.o;
    }

    public ViewGroup j() {
        return this.f445a;
    }

    public void k() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void l() {
        this.f445a.d();
    }

    public int m() {
        return this.b;
    }

    public void n() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void setIcon(int i2) {
        setIcon(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    public void setTitle(CharSequence charSequence) {
        this.h = true;
        c(charSequence);
    }

    public void setWindowCallback(Window.Callback callback) {
        this.l = callback;
    }

    public void setWindowTitle(CharSequence charSequence) {
        if (!this.h) {
            c(charSequence);
        }
    }

    public ToolbarWidgetWrapper(Toolbar toolbar, boolean z, int i2, int i3) {
        Drawable drawable;
        this.o = 0;
        this.p = 0;
        this.f445a = toolbar;
        this.i = toolbar.getTitle();
        this.j = toolbar.getSubtitle();
        this.h = this.i != null;
        this.g = toolbar.getNavigationIcon();
        TintTypedArray a2 = TintTypedArray.a(toolbar.getContext(), (AttributeSet) null, R$styleable.ActionBar, R$attr.actionBarStyle, 0);
        this.q = a2.b(R$styleable.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence e2 = a2.e(R$styleable.ActionBar_title);
            if (!TextUtils.isEmpty(e2)) {
                setTitle(e2);
            }
            CharSequence e3 = a2.e(R$styleable.ActionBar_subtitle);
            if (!TextUtils.isEmpty(e3)) {
                a(e3);
            }
            Drawable b2 = a2.b(R$styleable.ActionBar_logo);
            if (b2 != null) {
                b(b2);
            }
            Drawable b3 = a2.b(R$styleable.ActionBar_icon);
            if (b3 != null) {
                setIcon(b3);
            }
            if (this.g == null && (drawable = this.q) != null) {
                a(drawable);
            }
            a(a2.d(R$styleable.ActionBar_displayOptions, 0));
            int g2 = a2.g(R$styleable.ActionBar_customNavigationLayout, 0);
            if (g2 != 0) {
                a(LayoutInflater.from(this.f445a.getContext()).inflate(g2, this.f445a, false));
                a(this.b | 16);
            }
            int f2 = a2.f(R$styleable.ActionBar_height, 0);
            if (f2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.f445a.getLayoutParams();
                layoutParams.height = f2;
                this.f445a.setLayoutParams(layoutParams);
            }
            int b4 = a2.b(R$styleable.ActionBar_contentInsetStart, -1);
            int b5 = a2.b(R$styleable.ActionBar_contentInsetEnd, -1);
            if (b4 >= 0 || b5 >= 0) {
                this.f445a.a(Math.max(b4, 0), Math.max(b5, 0));
            }
            int g3 = a2.g(R$styleable.ActionBar_titleTextStyle, 0);
            if (g3 != 0) {
                Toolbar toolbar2 = this.f445a;
                toolbar2.b(toolbar2.getContext(), g3);
            }
            int g4 = a2.g(R$styleable.ActionBar_subtitleTextStyle, 0);
            if (g4 != 0) {
                Toolbar toolbar3 = this.f445a;
                toolbar3.a(toolbar3.getContext(), g4);
            }
            int g5 = a2.g(R$styleable.ActionBar_popupTheme, 0);
            if (g5 != 0) {
                this.f445a.setPopupTheme(g5);
            }
        } else {
            this.b = o();
        }
        a2.a();
        f(i2);
        this.k = this.f445a.getNavigationContentDescription();
        this.f445a.setNavigationOnClickListener(new View.OnClickListener() {

            /* renamed from: a  reason: collision with root package name */
            final ActionMenuItem f446a = new ActionMenuItem(ToolbarWidgetWrapper.this.f445a.getContext(), 0, 16908332, 0, 0, ToolbarWidgetWrapper.this.i);

            public void onClick(View view) {
                ToolbarWidgetWrapper toolbarWidgetWrapper = ToolbarWidgetWrapper.this;
                Window.Callback callback = toolbarWidgetWrapper.l;
                if (callback != null && toolbarWidgetWrapper.m) {
                    callback.onMenuItemSelected(0, this.f446a);
                }
            }
        });
    }

    public void d(int i2) {
        a(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    public void e(int i2) {
        this.f445a.setVisibility(i2);
    }

    public void setIcon(Drawable drawable) {
        this.e = drawable;
        r();
    }

    public boolean b() {
        return this.f445a.g();
    }

    public boolean a() {
        return this.f445a.b();
    }

    public void b(boolean z) {
        this.f445a.setCollapsible(z);
    }

    public void c(int i2) {
        b(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    public void a(Menu menu, MenuPresenter.Callback callback) {
        if (this.n == null) {
            this.n = new ActionMenuPresenter(this.f445a.getContext());
            this.n.a(R$id.action_menu_presenter);
        }
        this.n.a(callback);
        this.f445a.a((MenuBuilder) menu, this.n);
    }

    public void b(CharSequence charSequence) {
        this.k = charSequence;
        p();
    }

    public boolean c() {
        return this.f445a.k();
    }

    public boolean f() {
        return this.f445a.h();
    }

    public void b(int i2) {
        b((CharSequence) i2 == 0 ? null : getContext().getString(i2));
    }

    public void a(int i2) {
        View view;
        int i3 = this.b ^ i2;
        this.b = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    p();
                }
                q();
            }
            if ((i3 & 3) != 0) {
                r();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.f445a.setTitle(this.i);
                    this.f445a.setSubtitle(this.j);
                } else {
                    this.f445a.setTitle((CharSequence) null);
                    this.f445a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && (view = this.d) != null) {
                if ((i2 & 16) != 0) {
                    this.f445a.addView(view);
                } else {
                    this.f445a.removeView(view);
                }
            }
        }
    }

    public void a(ScrollingTabContainerView scrollingTabContainerView) {
        Toolbar toolbar;
        View view = this.c;
        if (view != null && view.getParent() == (toolbar = this.f445a)) {
            toolbar.removeView(this.c);
        }
        this.c = scrollingTabContainerView;
        if (scrollingTabContainerView != null && this.o == 2) {
            this.f445a.addView(this.c, 0);
            Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) this.c.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -2;
            layoutParams.f235a = 8388691;
            scrollingTabContainerView.setAllowCollapse(true);
        }
    }

    public void a(View view) {
        View view2 = this.d;
        if (!(view2 == null || (this.b & 16) == 0)) {
            this.f445a.removeView(view2);
        }
        this.d = view;
        if (view != null && (this.b & 16) != 0) {
            this.f445a.addView(this.d);
        }
    }

    public ViewPropertyAnimatorCompat a(final int i2, long j2) {
        ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.f445a);
        a2.a(i2 == 0 ? 1.0f : 0.0f);
        a2.a(j2);
        a2.a((ViewPropertyAnimatorListener) new ViewPropertyAnimatorListenerAdapter() {

            /* renamed from: a  reason: collision with root package name */
            private boolean f447a = false;

            public void onAnimationCancel(View view) {
                this.f447a = true;
            }

            public void onAnimationEnd(View view) {
                if (!this.f447a) {
                    ToolbarWidgetWrapper.this.f445a.setVisibility(i2);
                }
            }

            public void onAnimationStart(View view) {
                ToolbarWidgetWrapper.this.f445a.setVisibility(0);
            }
        });
        return a2;
    }

    public void a(Drawable drawable) {
        this.g = drawable;
        q();
    }

    public void a(MenuPresenter.Callback callback, MenuBuilder.Callback callback2) {
        this.f445a.a(callback, callback2);
    }
}
