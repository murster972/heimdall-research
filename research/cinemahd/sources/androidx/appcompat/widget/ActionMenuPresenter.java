package androidx.appcompat.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.R$attr;
import androidx.appcompat.R$layout;
import androidx.appcompat.view.ActionBarPolicy;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.view.menu.BaseMenuPresenter;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.view.menu.ShowableListMenu;
import androidx.appcompat.view.menu.SubMenuBuilder;
import androidx.appcompat.widget.ActionMenuView;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.ActionProvider;
import java.util.ArrayList;

class ActionMenuPresenter extends BaseMenuPresenter implements ActionProvider.SubUiVisibilityListener {
    private ActionMenuPopupCallback A;
    final PopupPresenterCallback B = new PopupPresenterCallback();
    int C;
    OverflowMenuButton j;
    private Drawable k;
    private boolean l;
    private boolean m;
    private boolean n;
    private int o;
    private int p;
    private int q;
    private boolean r;
    private boolean s;
    private boolean t;
    private boolean u;
    private int v;
    private final SparseBooleanArray w = new SparseBooleanArray();
    OverflowPopup x;
    ActionButtonSubmenu y;
    OpenOverflowRunnable z;

    private class ActionButtonSubmenu extends MenuPopupHelper {
        public ActionButtonSubmenu(Context context, SubMenuBuilder subMenuBuilder, View view) {
            super(context, subMenuBuilder, view, false, R$attr.actionOverflowMenuStyle);
            if (!((MenuItemImpl) subMenuBuilder.getItem()).h()) {
                View view2 = ActionMenuPresenter.this.j;
                a(view2 == null ? (View) ActionMenuPresenter.this.h : view2);
            }
            a((MenuPresenter.Callback) ActionMenuPresenter.this.B);
        }

        /* access modifiers changed from: protected */
        public void d() {
            ActionMenuPresenter actionMenuPresenter = ActionMenuPresenter.this;
            actionMenuPresenter.y = null;
            actionMenuPresenter.C = 0;
            super.d();
        }
    }

    private class ActionMenuPopupCallback extends ActionMenuItemView.PopupCallback {
        ActionMenuPopupCallback() {
        }

        public ShowableListMenu a() {
            ActionButtonSubmenu actionButtonSubmenu = ActionMenuPresenter.this.y;
            if (actionButtonSubmenu != null) {
                return actionButtonSubmenu.b();
            }
            return null;
        }
    }

    private class OpenOverflowRunnable implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private OverflowPopup f351a;

        public OpenOverflowRunnable(OverflowPopup overflowPopup) {
            this.f351a = overflowPopup;
        }

        public void run() {
            if (ActionMenuPresenter.this.c != null) {
                ActionMenuPresenter.this.c.a();
            }
            View view = (View) ActionMenuPresenter.this.h;
            if (!(view == null || view.getWindowToken() == null || !this.f351a.f())) {
                ActionMenuPresenter.this.x = this.f351a;
            }
            ActionMenuPresenter.this.z = null;
        }
    }

    private class OverflowMenuButton extends AppCompatImageView implements ActionMenuView.ActionMenuChildView {
        public OverflowMenuButton(Context context) {
            super(context, (AttributeSet) null, R$attr.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            TooltipCompat.a(this, getContentDescription());
            setOnTouchListener(new ForwardingListener(this, ActionMenuPresenter.this) {
                public ShowableListMenu a() {
                    OverflowPopup overflowPopup = ActionMenuPresenter.this.x;
                    if (overflowPopup == null) {
                        return null;
                    }
                    return overflowPopup.b();
                }

                public boolean b() {
                    ActionMenuPresenter.this.j();
                    return true;
                }

                public boolean c() {
                    ActionMenuPresenter actionMenuPresenter = ActionMenuPresenter.this;
                    if (actionMenuPresenter.z != null) {
                        return false;
                    }
                    actionMenuPresenter.f();
                    return true;
                }
            });
        }

        public boolean b() {
            return false;
        }

        public boolean c() {
            return false;
        }

        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            ActionMenuPresenter.this.j();
            return true;
        }

        /* access modifiers changed from: protected */
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                DrawableCompat.a(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    private class OverflowPopup extends MenuPopupHelper {
        public OverflowPopup(Context context, MenuBuilder menuBuilder, View view, boolean z) {
            super(context, menuBuilder, view, z, R$attr.actionOverflowMenuStyle);
            a(8388613);
            a((MenuPresenter.Callback) ActionMenuPresenter.this.B);
        }

        /* access modifiers changed from: protected */
        public void d() {
            if (ActionMenuPresenter.this.c != null) {
                ActionMenuPresenter.this.c.close();
            }
            ActionMenuPresenter.this.x = null;
            super.d();
        }
    }

    @SuppressLint({"BanParcelableUsage"})
    private static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        public int f353a;

        SavedState() {
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f353a);
        }

        SavedState(Parcel parcel) {
            this.f353a = parcel.readInt();
        }
    }

    public ActionMenuPresenter(Context context) {
        super(context, R$layout.abc_action_menu_layout, R$layout.abc_action_menu_item_layout);
    }

    public boolean g() {
        ActionButtonSubmenu actionButtonSubmenu = this.y;
        if (actionButtonSubmenu == null) {
            return false;
        }
        actionButtonSubmenu.a();
        return true;
    }

    public boolean h() {
        return this.z != null || i();
    }

    public boolean i() {
        OverflowPopup overflowPopup = this.x;
        return overflowPopup != null && overflowPopup.c();
    }

    public boolean j() {
        MenuBuilder menuBuilder;
        if (!this.m || i() || (menuBuilder = this.c) == null || this.h == null || this.z != null || menuBuilder.j().isEmpty()) {
            return false;
        }
        this.z = new OpenOverflowRunnable(new OverflowPopup(this.b, this.c, this.j, true));
        ((View) this.h).post(this.z);
        super.a((SubMenuBuilder) null);
        return true;
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        super.a(context, menuBuilder);
        Resources resources = context.getResources();
        ActionBarPolicy a2 = ActionBarPolicy.a(context);
        if (!this.n) {
            this.m = a2.g();
        }
        if (!this.t) {
            this.o = a2.b();
        }
        if (!this.r) {
            this.q = a2.c();
        }
        int i = this.o;
        if (this.m) {
            if (this.j == null) {
                this.j = new OverflowMenuButton(this.f316a);
                if (this.l) {
                    this.j.setImageDrawable(this.k);
                    this.k = null;
                    this.l = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.j.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.j.getMeasuredWidth();
        } else {
            this.j = null;
        }
        this.p = i;
        this.v = (int) (resources.getDisplayMetrics().density * 56.0f);
    }

    public MenuView b(ViewGroup viewGroup) {
        MenuView menuView = this.h;
        MenuView b = super.b(viewGroup);
        if (menuView != b) {
            ((ActionMenuView) b).setPresenter(this);
        }
        return b;
    }

    public void c(boolean z2) {
        this.u = z2;
    }

    public void d(boolean z2) {
        this.m = z2;
        this.n = true;
    }

    public Drawable e() {
        OverflowMenuButton overflowMenuButton = this.j;
        if (overflowMenuButton != null) {
            return overflowMenuButton.getDrawable();
        }
        if (this.l) {
            return this.k;
        }
        return null;
    }

    public boolean f() {
        MenuView menuView;
        OpenOverflowRunnable openOverflowRunnable = this.z;
        if (openOverflowRunnable == null || (menuView = this.h) == null) {
            OverflowPopup overflowPopup = this.x;
            if (overflowPopup == null) {
                return false;
            }
            overflowPopup.a();
            return true;
        }
        ((View) menuView).removeCallbacks(openOverflowRunnable);
        this.z = null;
        return true;
    }

    private class PopupPresenterCallback implements MenuPresenter.Callback {
        PopupPresenterCallback() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            if (menuBuilder == null) {
                return false;
            }
            ActionMenuPresenter.this.C = ((SubMenuBuilder) menuBuilder).getItem().getItemId();
            MenuPresenter.Callback c = ActionMenuPresenter.this.c();
            if (c != null) {
                return c.a(menuBuilder);
            }
            return false;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (menuBuilder instanceof SubMenuBuilder) {
                menuBuilder.m().a(false);
            }
            MenuPresenter.Callback c = ActionMenuPresenter.this.c();
            if (c != null) {
                c.a(menuBuilder, z);
            }
        }
    }

    public boolean d() {
        return f() | g();
    }

    public boolean b() {
        int i;
        ArrayList<MenuItemImpl> arrayList;
        int i2;
        int i3;
        int i4;
        ActionMenuPresenter actionMenuPresenter = this;
        MenuBuilder menuBuilder = actionMenuPresenter.c;
        View view = null;
        int i5 = 0;
        if (menuBuilder != null) {
            arrayList = menuBuilder.n();
            i = arrayList.size();
        } else {
            arrayList = null;
            i = 0;
        }
        int i6 = actionMenuPresenter.q;
        int i7 = actionMenuPresenter.p;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) actionMenuPresenter.h;
        int i8 = i6;
        boolean z2 = false;
        int i9 = 0;
        int i10 = 0;
        for (int i11 = 0; i11 < i; i11++) {
            MenuItemImpl menuItemImpl = arrayList.get(i11);
            if (menuItemImpl.k()) {
                i9++;
            } else if (menuItemImpl.j()) {
                i10++;
            } else {
                z2 = true;
            }
            if (actionMenuPresenter.u && menuItemImpl.isActionViewExpanded()) {
                i8 = 0;
            }
        }
        if (actionMenuPresenter.m && (z2 || i10 + i9 > i8)) {
            i8--;
        }
        int i12 = i8 - i9;
        SparseBooleanArray sparseBooleanArray = actionMenuPresenter.w;
        sparseBooleanArray.clear();
        if (actionMenuPresenter.s) {
            int i13 = actionMenuPresenter.v;
            i2 = i7 / i13;
            i3 = i13 + ((i7 % i13) / i2);
        } else {
            i3 = 0;
            i2 = 0;
        }
        int i14 = i7;
        int i15 = 0;
        int i16 = 0;
        while (i15 < i) {
            MenuItemImpl menuItemImpl2 = arrayList.get(i15);
            if (menuItemImpl2.k()) {
                View a2 = actionMenuPresenter.a(menuItemImpl2, view, viewGroup);
                if (actionMenuPresenter.s) {
                    i2 -= ActionMenuView.a(a2, i3, i2, makeMeasureSpec, i5);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = a2.getMeasuredWidth();
                i14 -= measuredWidth;
                if (i16 != 0) {
                    measuredWidth = i16;
                }
                int groupId = menuItemImpl2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                menuItemImpl2.d(true);
                i16 = measuredWidth;
                i4 = i;
            } else if (menuItemImpl2.j()) {
                int groupId2 = menuItemImpl2.getGroupId();
                boolean z3 = sparseBooleanArray.get(groupId2);
                boolean z4 = (i12 > 0 || z3) && i14 > 0 && (!actionMenuPresenter.s || i2 > 0);
                boolean z5 = z4;
                i4 = i;
                if (z4) {
                    View a3 = actionMenuPresenter.a(menuItemImpl2, (View) null, viewGroup);
                    if (actionMenuPresenter.s) {
                        int a4 = ActionMenuView.a(a3, i3, i2, makeMeasureSpec, 0);
                        i2 -= a4;
                        z5 = a4 == 0 ? false : z5;
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth2 = a3.getMeasuredWidth();
                    i14 -= measuredWidth2;
                    if (i16 == 0) {
                        i16 = measuredWidth2;
                    }
                    z4 = z5 & (!actionMenuPresenter.s ? i14 + i16 > 0 : i14 >= 0);
                }
                if (z4 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z3) {
                    sparseBooleanArray.put(groupId2, false);
                    int i17 = 0;
                    while (i17 < i15) {
                        MenuItemImpl menuItemImpl3 = arrayList.get(i17);
                        if (menuItemImpl3.getGroupId() == groupId2) {
                            if (menuItemImpl3.h()) {
                                i12++;
                            }
                            menuItemImpl3.d(false);
                        }
                        i17++;
                    }
                }
                if (z4) {
                    i12--;
                }
                menuItemImpl2.d(z4);
            } else {
                i4 = i;
                menuItemImpl2.d(false);
                i15++;
                i = i4;
                view = null;
                i5 = 0;
                actionMenuPresenter = this;
            }
            i15++;
            i = i4;
            view = null;
            i5 = 0;
            actionMenuPresenter = this;
        }
        return true;
    }

    public void a(Configuration configuration) {
        if (!this.r) {
            this.q = ActionBarPolicy.a(this.b).c();
        }
        MenuBuilder menuBuilder = this.c;
        if (menuBuilder != null) {
            menuBuilder.b(true);
        }
    }

    public void a(Drawable drawable) {
        OverflowMenuButton overflowMenuButton = this.j;
        if (overflowMenuButton != null) {
            overflowMenuButton.setImageDrawable(drawable);
            return;
        }
        this.l = true;
        this.k = drawable;
    }

    public View a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        View actionView = menuItemImpl.getActionView();
        if (actionView == null || menuItemImpl.f()) {
            actionView = super.a(menuItemImpl, view, viewGroup);
        }
        actionView.setVisibility(menuItemImpl.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    public void a(MenuItemImpl menuItemImpl, MenuView.ItemView itemView) {
        itemView.a(menuItemImpl, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) itemView;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.h);
        if (this.A == null) {
            this.A = new ActionMenuPopupCallback();
        }
        actionMenuItemView.setPopupCallback(this.A);
    }

    public boolean a(int i, MenuItemImpl menuItemImpl) {
        return menuItemImpl.h();
    }

    public void a(boolean z2) {
        MenuView menuView;
        super.a(z2);
        ((View) this.h).requestLayout();
        MenuBuilder menuBuilder = this.c;
        boolean z3 = false;
        if (menuBuilder != null) {
            ArrayList<MenuItemImpl> c = menuBuilder.c();
            int size = c.size();
            for (int i = 0; i < size; i++) {
                ActionProvider a2 = c.get(i).a();
                if (a2 != null) {
                    a2.setSubUiVisibilityListener(this);
                }
            }
        }
        MenuBuilder menuBuilder2 = this.c;
        ArrayList<MenuItemImpl> j2 = menuBuilder2 != null ? menuBuilder2.j() : null;
        if (this.m && j2 != null) {
            int size2 = j2.size();
            if (size2 == 1) {
                z3 = !j2.get(0).isActionViewExpanded();
            } else if (size2 > 0) {
                z3 = true;
            }
        }
        if (z3) {
            if (this.j == null) {
                this.j = new OverflowMenuButton(this.f316a);
            }
            ViewGroup viewGroup = (ViewGroup) this.j.getParent();
            if (viewGroup != this.h) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.j);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.h;
                actionMenuView.addView(this.j, actionMenuView.e());
            }
        } else {
            OverflowMenuButton overflowMenuButton = this.j;
            if (overflowMenuButton != null && overflowMenuButton.getParent() == (menuView = this.h)) {
                ((ViewGroup) menuView).removeView(this.j);
            }
        }
        ((ActionMenuView) this.h).setOverflowReserved(this.m);
    }

    public void b(boolean z2) {
        if (z2) {
            super.a((SubMenuBuilder) null);
            return;
        }
        MenuBuilder menuBuilder = this.c;
        if (menuBuilder != null) {
            menuBuilder.a(false);
        }
    }

    public boolean a(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.j) {
            return false;
        }
        return super.a(viewGroup, i);
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        boolean z2 = false;
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        SubMenuBuilder subMenuBuilder2 = subMenuBuilder;
        while (subMenuBuilder2.t() != this.c) {
            subMenuBuilder2 = (SubMenuBuilder) subMenuBuilder2.t();
        }
        View a2 = a(subMenuBuilder2.getItem());
        if (a2 == null) {
            return false;
        }
        this.C = subMenuBuilder.getItem().getItemId();
        int size = subMenuBuilder.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            MenuItem item = subMenuBuilder.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i++;
        }
        this.y = new ActionButtonSubmenu(this.b, subMenuBuilder, a2);
        this.y.a(z2);
        this.y.e();
        super.a(subMenuBuilder);
        return true;
    }

    private View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.h;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof MenuView.ItemView) && ((MenuView.ItemView) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public void a(MenuBuilder menuBuilder, boolean z2) {
        d();
        super.a(menuBuilder, z2);
    }

    public Parcelable a() {
        SavedState savedState = new SavedState();
        savedState.f353a = this.C;
        return savedState;
    }

    public void a(Parcelable parcelable) {
        int i;
        MenuItem findItem;
        if ((parcelable instanceof SavedState) && (i = ((SavedState) parcelable).f353a) > 0 && (findItem = this.c.findItem(i)) != null) {
            a((SubMenuBuilder) findItem.getSubMenu());
        }
    }

    public void a(ActionMenuView actionMenuView) {
        this.h = actionMenuView;
        actionMenuView.a(this.c);
    }
}
