package androidx.appcompat.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class TintContextWrapper extends ContextWrapper {
    private static final Object c = new Object();
    private static ArrayList<WeakReference<TintContextWrapper>> d;

    /* renamed from: a  reason: collision with root package name */
    private final Resources f437a;
    private final Resources.Theme b;

    private TintContextWrapper(Context context) {
        super(context);
        if (VectorEnabledTintResources.b()) {
            this.f437a = new VectorEnabledTintResources(this, context.getResources());
            this.b = this.f437a.newTheme();
            this.b.setTo(context.getTheme());
            return;
        }
        this.f437a = new TintResources(this, context.getResources());
        this.b = null;
    }

    private static boolean a(Context context) {
        if ((context instanceof TintContextWrapper) || (context.getResources() instanceof TintResources) || (context.getResources() instanceof VectorEnabledTintResources)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 21 || VectorEnabledTintResources.b()) {
            return true;
        }
        return false;
    }

    public static Context b(Context context) {
        if (!a(context)) {
            return context;
        }
        synchronized (c) {
            if (d == null) {
                d = new ArrayList<>();
            } else {
                for (int size = d.size() - 1; size >= 0; size--) {
                    WeakReference weakReference = d.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        d.remove(size);
                    }
                }
                for (int size2 = d.size() - 1; size2 >= 0; size2--) {
                    WeakReference weakReference2 = d.get(size2);
                    TintContextWrapper tintContextWrapper = weakReference2 != null ? (TintContextWrapper) weakReference2.get() : null;
                    if (tintContextWrapper != null && tintContextWrapper.getBaseContext() == context) {
                        return tintContextWrapper;
                    }
                }
            }
            TintContextWrapper tintContextWrapper2 = new TintContextWrapper(context);
            d.add(new WeakReference(tintContextWrapper2));
            return tintContextWrapper2;
        }
    }

    public AssetManager getAssets() {
        return this.f437a.getAssets();
    }

    public Resources getResources() {
        return this.f437a;
    }

    public Resources.Theme getTheme() {
        Resources.Theme theme = this.b;
        return theme == null ? super.getTheme() : theme;
    }

    public void setTheme(int i) {
        Resources.Theme theme = this.b;
        if (theme == null) {
            super.setTheme(i);
        } else {
            theme.applyStyle(i, true);
        }
    }
}
