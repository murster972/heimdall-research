package androidx.appcompat.widget;

import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewConfigurationCompat;

class TooltipCompatHandler implements View.OnLongClickListener, View.OnHoverListener, View.OnAttachStateChangeListener {
    private static TooltipCompatHandler j;
    private static TooltipCompatHandler k;

    /* renamed from: a  reason: collision with root package name */
    private final View f448a;
    private final CharSequence b;
    private final int c;
    private final Runnable d = new Runnable() {
        public void run() {
            TooltipCompatHandler.this.a(false);
        }
    };
    private final Runnable e = new Runnable() {
        public void run() {
            TooltipCompatHandler.this.a();
        }
    };
    private int f;
    private int g;
    private TooltipPopup h;
    private boolean i;

    private TooltipCompatHandler(View view, CharSequence charSequence) {
        this.f448a = view;
        this.b = charSequence;
        this.c = ViewConfigurationCompat.a(ViewConfiguration.get(this.f448a.getContext()));
        c();
        this.f448a.setOnLongClickListener(this);
        this.f448a.setOnHoverListener(this);
    }

    public static void a(View view, CharSequence charSequence) {
        TooltipCompatHandler tooltipCompatHandler = j;
        if (tooltipCompatHandler != null && tooltipCompatHandler.f448a == view) {
            a((TooltipCompatHandler) null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            TooltipCompatHandler tooltipCompatHandler2 = k;
            if (tooltipCompatHandler2 != null && tooltipCompatHandler2.f448a == view) {
                tooltipCompatHandler2.a();
            }
            view.setOnLongClickListener((View.OnLongClickListener) null);
            view.setLongClickable(false);
            view.setOnHoverListener((View.OnHoverListener) null);
            return;
        }
        new TooltipCompatHandler(view, charSequence);
    }

    private void b() {
        this.f448a.removeCallbacks(this.d);
    }

    private void c() {
        this.f = Integer.MAX_VALUE;
        this.g = Integer.MAX_VALUE;
    }

    private void d() {
        this.f448a.postDelayed(this.d, (long) ViewConfiguration.getLongPressTimeout());
    }

    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.h != null && this.i) {
            return false;
        }
        AccessibilityManager accessibilityManager = (AccessibilityManager) this.f448a.getContext().getSystemService("accessibility");
        if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 7) {
            if (action == 10) {
                c();
                a();
            }
        } else if (this.f448a.isEnabled() && this.h == null && a(motionEvent)) {
            a(this);
        }
        return false;
    }

    public boolean onLongClick(View view) {
        this.f = view.getWidth() / 2;
        this.g = view.getHeight() / 2;
        a(true);
        return true;
    }

    public void onViewAttachedToWindow(View view) {
    }

    public void onViewDetachedFromWindow(View view) {
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        long j2;
        int i2;
        long j3;
        if (ViewCompat.D(this.f448a)) {
            a((TooltipCompatHandler) null);
            TooltipCompatHandler tooltipCompatHandler = k;
            if (tooltipCompatHandler != null) {
                tooltipCompatHandler.a();
            }
            k = this;
            this.i = z;
            this.h = new TooltipPopup(this.f448a.getContext());
            this.h.a(this.f448a, this.f, this.g, this.i, this.b);
            this.f448a.addOnAttachStateChangeListener(this);
            if (this.i) {
                j2 = 2500;
            } else {
                if ((ViewCompat.u(this.f448a) & 1) == 1) {
                    j3 = 3000;
                    i2 = ViewConfiguration.getLongPressTimeout();
                } else {
                    j3 = 15000;
                    i2 = ViewConfiguration.getLongPressTimeout();
                }
                j2 = j3 - ((long) i2);
            }
            this.f448a.removeCallbacks(this.e);
            this.f448a.postDelayed(this.e, j2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (k == this) {
            k = null;
            TooltipPopup tooltipPopup = this.h;
            if (tooltipPopup != null) {
                tooltipPopup.a();
                this.h = null;
                c();
                this.f448a.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (j == this) {
            a((TooltipCompatHandler) null);
        }
        this.f448a.removeCallbacks(this.e);
    }

    private static void a(TooltipCompatHandler tooltipCompatHandler) {
        TooltipCompatHandler tooltipCompatHandler2 = j;
        if (tooltipCompatHandler2 != null) {
            tooltipCompatHandler2.b();
        }
        j = tooltipCompatHandler;
        TooltipCompatHandler tooltipCompatHandler3 = j;
        if (tooltipCompatHandler3 != null) {
            tooltipCompatHandler3.d();
        }
    }

    private boolean a(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (Math.abs(x - this.f) <= this.c && Math.abs(y - this.g) <= this.c) {
            return false;
        }
        this.f = x;
        this.g = y;
        return true;
    }
}
