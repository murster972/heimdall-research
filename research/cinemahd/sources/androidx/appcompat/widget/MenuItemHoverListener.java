package androidx.appcompat.widget;

import android.view.MenuItem;
import androidx.appcompat.view.menu.MenuBuilder;

public interface MenuItemHoverListener {
    void a(MenuBuilder menuBuilder, MenuItem menuItem);

    void b(MenuBuilder menuBuilder, MenuItem menuItem);
}
