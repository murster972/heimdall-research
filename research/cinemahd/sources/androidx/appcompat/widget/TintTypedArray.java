package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.res.ResourcesCompat;

public class TintTypedArray {

    /* renamed from: a  reason: collision with root package name */
    private final Context f439a;
    private final TypedArray b;
    private TypedValue c;

    private TintTypedArray(Context context, TypedArray typedArray) {
        this.f439a = context;
        this.b = typedArray;
    }

    public static TintTypedArray a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new TintTypedArray(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    public Drawable b(int i) {
        int resourceId;
        if (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0) {
            return this.b.getDrawable(i);
        }
        return AppCompatResources.c(this.f439a, resourceId);
    }

    public Drawable c(int i) {
        int resourceId;
        if (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0) {
            return null;
        }
        return AppCompatDrawableManager.b().a(this.f439a, resourceId, true);
    }

    public String d(int i) {
        return this.b.getString(i);
    }

    public CharSequence e(int i) {
        return this.b.getText(i);
    }

    public int f(int i, int i2) {
        return this.b.getLayoutDimension(i, i2);
    }

    public int g(int i, int i2) {
        return this.b.getResourceId(i, i2);
    }

    public static TintTypedArray a(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new TintTypedArray(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    public int d(int i, int i2) {
        return this.b.getInt(i, i2);
    }

    public int e(int i, int i2) {
        return this.b.getInteger(i, i2);
    }

    public CharSequence[] f(int i) {
        return this.b.getTextArray(i);
    }

    public boolean g(int i) {
        return this.b.hasValue(i);
    }

    public static TintTypedArray a(Context context, int i, int[] iArr) {
        return new TintTypedArray(context, context.obtainStyledAttributes(i, iArr));
    }

    public int c(int i, int i2) {
        return this.b.getDimensionPixelSize(i, i2);
    }

    public Typeface a(int i, int i2, ResourcesCompat.FontCallback fontCallback) {
        int resourceId = this.b.getResourceId(i, 0);
        if (resourceId == 0) {
            return null;
        }
        if (this.c == null) {
            this.c = new TypedValue();
        }
        return ResourcesCompat.a(this.f439a, resourceId, this.c, i2, fontCallback);
    }

    public float b(int i, float f) {
        return this.b.getFloat(i, f);
    }

    public int b(int i, int i2) {
        return this.b.getDimensionPixelOffset(i, i2);
    }

    public boolean a(int i, boolean z) {
        return this.b.getBoolean(i, z);
    }

    public int a(int i, int i2) {
        return this.b.getColor(i, i2);
    }

    public ColorStateList a(int i) {
        int resourceId;
        ColorStateList b2;
        if (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0 || (b2 = AppCompatResources.b(this.f439a, resourceId)) == null) {
            return this.b.getColorStateList(i);
        }
        return b2;
    }

    public float a(int i, float f) {
        return this.b.getDimension(i, f);
    }

    public void a() {
        this.b.recycle();
    }
}
