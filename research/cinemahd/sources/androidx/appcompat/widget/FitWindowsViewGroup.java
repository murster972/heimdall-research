package androidx.appcompat.widget;

import android.graphics.Rect;

public interface FitWindowsViewGroup {

    public interface OnFitSystemWindowsListener {
        void a(Rect rect);
    }

    void setOnFitSystemWindowsListener(OnFitSystemWindowsListener onFitSystemWindowsListener);
}
