package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import androidx.appcompat.R$attr;
import androidx.appcompat.R$styleable;
import androidx.appcompat.view.menu.ShowableListMenu;
import androidx.core.view.ViewCompat;
import androidx.core.widget.PopupWindowCompat;
import com.facebook.ads.AdError;
import java.lang.reflect.Method;

public class ListPopupWindow implements ShowableListMenu {
    private static Method F;
    private static Method G;
    private static Method H;
    final Handler A;
    private final Rect B;
    private Rect C;
    private boolean D;
    PopupWindow E;

    /* renamed from: a  reason: collision with root package name */
    private Context f406a;
    private ListAdapter b;
    DropDownListView c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private boolean i;
    private boolean j;
    private boolean k;
    private int l;
    private boolean m;
    private boolean n;
    int o;
    private View p;
    private int q;
    private DataSetObserver r;
    private View s;
    private Drawable t;
    private AdapterView.OnItemClickListener u;
    private AdapterView.OnItemSelectedListener v;
    final ResizePopupRunnable w;
    private final PopupTouchInterceptor x;
    private final PopupScrollListener y;
    private final ListSelectorHider z;

    private class ListSelectorHider implements Runnable {
        ListSelectorHider() {
        }

        public void run() {
            ListPopupWindow.this.g();
        }
    }

    private class PopupDataSetObserver extends DataSetObserver {
        PopupDataSetObserver() {
        }

        public void onChanged() {
            if (ListPopupWindow.this.c()) {
                ListPopupWindow.this.show();
            }
        }

        public void onInvalidated() {
            ListPopupWindow.this.dismiss();
        }
    }

    private class PopupScrollListener implements AbsListView.OnScrollListener {
        PopupScrollListener() {
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (i == 1 && !ListPopupWindow.this.j() && ListPopupWindow.this.E.getContentView() != null) {
                ListPopupWindow listPopupWindow = ListPopupWindow.this;
                listPopupWindow.A.removeCallbacks(listPopupWindow.w);
                ListPopupWindow.this.w.run();
            }
        }
    }

    private class PopupTouchInterceptor implements View.OnTouchListener {
        PopupTouchInterceptor() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            PopupWindow popupWindow;
            int action = motionEvent.getAction();
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            if (action == 0 && (popupWindow = ListPopupWindow.this.E) != null && popupWindow.isShowing() && x >= 0 && x < ListPopupWindow.this.E.getWidth() && y >= 0 && y < ListPopupWindow.this.E.getHeight()) {
                ListPopupWindow listPopupWindow = ListPopupWindow.this;
                listPopupWindow.A.postDelayed(listPopupWindow.w, 250);
                return false;
            } else if (action != 1) {
                return false;
            } else {
                ListPopupWindow listPopupWindow2 = ListPopupWindow.this;
                listPopupWindow2.A.removeCallbacks(listPopupWindow2.w);
                return false;
            }
        }
    }

    private class ResizePopupRunnable implements Runnable {
        ResizePopupRunnable() {
        }

        public void run() {
            DropDownListView dropDownListView = ListPopupWindow.this.c;
            if (dropDownListView != null && ViewCompat.D(dropDownListView) && ListPopupWindow.this.c.getCount() > ListPopupWindow.this.c.getChildCount()) {
                int childCount = ListPopupWindow.this.c.getChildCount();
                ListPopupWindow listPopupWindow = ListPopupWindow.this;
                if (childCount <= listPopupWindow.o) {
                    listPopupWindow.E.setInputMethodMode(2);
                    ListPopupWindow.this.show();
                }
            }
        }
    }

    static {
        if (Build.VERSION.SDK_INT <= 28) {
            Class<PopupWindow> cls = PopupWindow.class;
            try {
                F = cls.getDeclaredMethod("setClipToScreenEnabled", new Class[]{Boolean.TYPE});
            } catch (NoSuchMethodException unused) {
                Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
            try {
                H = PopupWindow.class.getDeclaredMethod("setEpicenterBounds", new Class[]{Rect.class});
            } catch (NoSuchMethodException unused2) {
                Log.i("ListPopupWindow", "Could not find method setEpicenterBounds(Rect) on PopupWindow. Oh well.");
            }
        }
        if (Build.VERSION.SDK_INT <= 23) {
            try {
                G = PopupWindow.class.getDeclaredMethod("getMaxAvailableHeight", new Class[]{View.class, Integer.TYPE, Boolean.TYPE});
            } catch (NoSuchMethodException unused3) {
                Log.i("ListPopupWindow", "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well.");
            }
        }
    }

    public ListPopupWindow(Context context) {
        this(context, (AttributeSet) null, R$attr.listPopupWindowStyle);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v24, resolved type: androidx.appcompat.widget.DropDownListView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v25, resolved type: androidx.appcompat.widget.DropDownListView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: android.widget.LinearLayout} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v30, resolved type: androidx.appcompat.widget.DropDownListView} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int l() {
        /*
            r12 = this;
            androidx.appcompat.widget.DropDownListView r0 = r12.c
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = -1
            r3 = 1
            r4 = 0
            if (r0 != 0) goto L_0x00bf
            android.content.Context r0 = r12.f406a
            androidx.appcompat.widget.ListPopupWindow$2 r5 = new androidx.appcompat.widget.ListPopupWindow$2
            r5.<init>()
            boolean r5 = r12.D
            r5 = r5 ^ r3
            androidx.appcompat.widget.DropDownListView r5 = r12.a(r0, r5)
            r12.c = r5
            android.graphics.drawable.Drawable r5 = r12.t
            if (r5 == 0) goto L_0x0022
            androidx.appcompat.widget.DropDownListView r6 = r12.c
            r6.setSelector(r5)
        L_0x0022:
            androidx.appcompat.widget.DropDownListView r5 = r12.c
            android.widget.ListAdapter r6 = r12.b
            r5.setAdapter(r6)
            androidx.appcompat.widget.DropDownListView r5 = r12.c
            android.widget.AdapterView$OnItemClickListener r6 = r12.u
            r5.setOnItemClickListener(r6)
            androidx.appcompat.widget.DropDownListView r5 = r12.c
            r5.setFocusable(r3)
            androidx.appcompat.widget.DropDownListView r5 = r12.c
            r5.setFocusableInTouchMode(r3)
            androidx.appcompat.widget.DropDownListView r5 = r12.c
            androidx.appcompat.widget.ListPopupWindow$3 r6 = new androidx.appcompat.widget.ListPopupWindow$3
            r6.<init>()
            r5.setOnItemSelectedListener(r6)
            androidx.appcompat.widget.DropDownListView r5 = r12.c
            androidx.appcompat.widget.ListPopupWindow$PopupScrollListener r6 = r12.y
            r5.setOnScrollListener(r6)
            android.widget.AdapterView$OnItemSelectedListener r5 = r12.v
            if (r5 == 0) goto L_0x0054
            androidx.appcompat.widget.DropDownListView r6 = r12.c
            r6.setOnItemSelectedListener(r5)
        L_0x0054:
            androidx.appcompat.widget.DropDownListView r5 = r12.c
            android.view.View r6 = r12.p
            if (r6 == 0) goto L_0x00b8
            android.widget.LinearLayout r7 = new android.widget.LinearLayout
            r7.<init>(r0)
            r7.setOrientation(r3)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r8 = 1065353216(0x3f800000, float:1.0)
            r0.<init>(r2, r4, r8)
            int r8 = r12.q
            if (r8 == 0) goto L_0x008f
            if (r8 == r3) goto L_0x0088
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r5 = "Invalid hint position "
            r0.append(r5)
            int r5 = r12.q
            r0.append(r5)
            java.lang.String r0 = r0.toString()
            java.lang.String r5 = "ListPopupWindow"
            android.util.Log.e(r5, r0)
            goto L_0x0095
        L_0x0088:
            r7.addView(r5, r0)
            r7.addView(r6)
            goto L_0x0095
        L_0x008f:
            r7.addView(r6)
            r7.addView(r5, r0)
        L_0x0095:
            int r0 = r12.e
            if (r0 < 0) goto L_0x009c
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x009e
        L_0x009c:
            r0 = 0
            r5 = 0
        L_0x009e:
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            r6.measure(r0, r4)
            android.view.ViewGroup$LayoutParams r0 = r6.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r5 = r6.getMeasuredHeight()
            int r6 = r0.topMargin
            int r5 = r5 + r6
            int r0 = r0.bottomMargin
            int r5 = r5 + r0
            r0 = r5
            r5 = r7
            goto L_0x00b9
        L_0x00b8:
            r0 = 0
        L_0x00b9:
            android.widget.PopupWindow r6 = r12.E
            r6.setContentView(r5)
            goto L_0x00dd
        L_0x00bf:
            android.widget.PopupWindow r0 = r12.E
            android.view.View r0 = r0.getContentView()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            android.view.View r0 = r12.p
            if (r0 == 0) goto L_0x00dc
            android.view.ViewGroup$LayoutParams r5 = r0.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r5 = (android.widget.LinearLayout.LayoutParams) r5
            int r0 = r0.getMeasuredHeight()
            int r6 = r5.topMargin
            int r0 = r0 + r6
            int r5 = r5.bottomMargin
            int r0 = r0 + r5
            goto L_0x00dd
        L_0x00dc:
            r0 = 0
        L_0x00dd:
            android.widget.PopupWindow r5 = r12.E
            android.graphics.drawable.Drawable r5 = r5.getBackground()
            if (r5 == 0) goto L_0x00f9
            android.graphics.Rect r6 = r12.B
            r5.getPadding(r6)
            android.graphics.Rect r5 = r12.B
            int r6 = r5.top
            int r5 = r5.bottom
            int r5 = r5 + r6
            boolean r7 = r12.i
            if (r7 != 0) goto L_0x00ff
            int r6 = -r6
            r12.g = r6
            goto L_0x00ff
        L_0x00f9:
            android.graphics.Rect r5 = r12.B
            r5.setEmpty()
            r5 = 0
        L_0x00ff:
            android.widget.PopupWindow r6 = r12.E
            int r6 = r6.getInputMethodMode()
            r7 = 2
            if (r6 != r7) goto L_0x0109
            goto L_0x010a
        L_0x0109:
            r3 = 0
        L_0x010a:
            android.view.View r4 = r12.h()
            int r6 = r12.g
            int r3 = r12.a(r4, r6, r3)
            boolean r4 = r12.m
            if (r4 != 0) goto L_0x017c
            int r4 = r12.d
            if (r4 != r2) goto L_0x011d
            goto L_0x017c
        L_0x011d:
            int r4 = r12.e
            r6 = -2
            if (r4 == r6) goto L_0x0145
            r1 = 1073741824(0x40000000, float:2.0)
            if (r4 == r2) goto L_0x012c
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r1)
        L_0x012a:
            r7 = r1
            goto L_0x015e
        L_0x012c:
            android.content.Context r2 = r12.f406a
            android.content.res.Resources r2 = r2.getResources()
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()
            int r2 = r2.widthPixels
            android.graphics.Rect r4 = r12.B
            int r6 = r4.left
            int r4 = r4.right
            int r6 = r6 + r4
            int r2 = r2 - r6
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r1)
            goto L_0x012a
        L_0x0145:
            android.content.Context r2 = r12.f406a
            android.content.res.Resources r2 = r2.getResources()
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()
            int r2 = r2.widthPixels
            android.graphics.Rect r4 = r12.B
            int r6 = r4.left
            int r4 = r4.right
            int r6 = r6 + r4
            int r2 = r2 - r6
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r1)
            goto L_0x012a
        L_0x015e:
            androidx.appcompat.widget.DropDownListView r6 = r12.c
            r8 = 0
            r9 = -1
            int r10 = r3 - r0
            r11 = -1
            int r1 = r6.a(r7, r8, r9, r10, r11)
            if (r1 <= 0) goto L_0x017a
            androidx.appcompat.widget.DropDownListView r2 = r12.c
            int r2 = r2.getPaddingTop()
            androidx.appcompat.widget.DropDownListView r3 = r12.c
            int r3 = r3.getPaddingBottom()
            int r2 = r2 + r3
            int r5 = r5 + r2
            int r0 = r0 + r5
        L_0x017a:
            int r1 = r1 + r0
            return r1
        L_0x017c:
            int r3 = r3 + r5
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.ListPopupWindow.l():int");
    }

    private void m() {
        View view = this.p;
        if (view != null) {
            ViewParent parent = view.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.p);
            }
        }
    }

    public void a(ListAdapter listAdapter) {
        DataSetObserver dataSetObserver = this.r;
        if (dataSetObserver == null) {
            this.r = new PopupDataSetObserver();
        } else {
            ListAdapter listAdapter2 = this.b;
            if (listAdapter2 != null) {
                listAdapter2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.b = listAdapter;
        if (listAdapter != null) {
            listAdapter.registerDataSetObserver(this.r);
        }
        DropDownListView dropDownListView = this.c;
        if (dropDownListView != null) {
            dropDownListView.setAdapter(this.b);
        }
    }

    public void b(int i2) {
        this.g = i2;
        this.i = true;
    }

    public boolean c() {
        return this.E.isShowing();
    }

    public Drawable d() {
        return this.E.getBackground();
    }

    public void dismiss() {
        this.E.dismiss();
        m();
        this.E.setContentView((View) null);
        this.c = null;
        this.A.removeCallbacks(this.w);
    }

    public int e() {
        if (!this.i) {
            return 0;
        }
        return this.g;
    }

    public void f(int i2) {
        this.l = i2;
    }

    public void g(int i2) {
        this.E.setInputMethodMode(i2);
    }

    public void h(int i2) {
        this.q = i2;
    }

    public int i() {
        return this.e;
    }

    public void j(int i2) {
        this.e = i2;
    }

    public boolean k() {
        return this.D;
    }

    public void show() {
        int l2 = l();
        boolean j2 = j();
        PopupWindowCompat.a(this.E, this.h);
        boolean z2 = true;
        if (!this.E.isShowing()) {
            int i2 = this.e;
            if (i2 == -1) {
                i2 = -1;
            } else if (i2 == -2) {
                i2 = h().getWidth();
            }
            int i3 = this.d;
            if (i3 == -1) {
                l2 = -1;
            } else if (i3 != -2) {
                l2 = i3;
            }
            this.E.setWidth(i2);
            this.E.setHeight(l2);
            c(true);
            this.E.setOutsideTouchable(!this.n && !this.m);
            this.E.setTouchInterceptor(this.x);
            if (this.k) {
                PopupWindowCompat.a(this.E, this.j);
            }
            if (Build.VERSION.SDK_INT <= 28) {
                Method method = H;
                if (method != null) {
                    try {
                        method.invoke(this.E, new Object[]{this.C});
                    } catch (Exception e2) {
                        Log.e("ListPopupWindow", "Could not invoke setEpicenterBounds on PopupWindow", e2);
                    }
                }
            } else {
                this.E.setEpicenterBounds(this.C);
            }
            PopupWindowCompat.a(this.E, h(), this.f, this.g, this.l);
            this.c.setSelection(-1);
            if (!this.D || this.c.isInTouchMode()) {
                g();
            }
            if (!this.D) {
                this.A.post(this.z);
            }
        } else if (ViewCompat.D(h())) {
            int i4 = this.e;
            if (i4 == -1) {
                i4 = -1;
            } else if (i4 == -2) {
                i4 = h().getWidth();
            }
            int i5 = this.d;
            if (i5 == -1) {
                if (!j2) {
                    l2 = -1;
                }
                if (j2) {
                    this.E.setWidth(this.e == -1 ? -1 : 0);
                    this.E.setHeight(0);
                } else {
                    this.E.setWidth(this.e == -1 ? -1 : 0);
                    this.E.setHeight(-1);
                }
            } else if (i5 != -2) {
                l2 = i5;
            }
            PopupWindow popupWindow = this.E;
            if (this.n || this.m) {
                z2 = false;
            }
            popupWindow.setOutsideTouchable(z2);
            this.E.update(h(), this.f, this.g, i4 < 0 ? -1 : i4, l2 < 0 ? -1 : l2);
        }
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    private void c(boolean z2) {
        if (Build.VERSION.SDK_INT <= 28) {
            Method method = F;
            if (method != null) {
                try {
                    method.invoke(this.E, new Object[]{Boolean.valueOf(z2)});
                } catch (Exception unused) {
                    Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
                }
            }
        } else {
            this.E.setIsClippedToScreen(z2);
        }
    }

    public void d(int i2) {
        this.E.setAnimationStyle(i2);
    }

    public ListView f() {
        return this.c;
    }

    public void g() {
        DropDownListView dropDownListView = this.c;
        if (dropDownListView != null) {
            dropDownListView.setListSelectionHidden(true);
            dropDownListView.requestLayout();
        }
    }

    public View h() {
        return this.s;
    }

    public void i(int i2) {
        DropDownListView dropDownListView = this.c;
        if (c() && dropDownListView != null) {
            dropDownListView.setListSelectionHidden(false);
            dropDownListView.setSelection(i2);
            if (dropDownListView.getChoiceMode() != 0) {
                dropDownListView.setItemChecked(i2, true);
            }
        }
    }

    public boolean j() {
        return this.E.getInputMethodMode() == 2;
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.d = -2;
        this.e = -2;
        this.h = AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE;
        this.l = 0;
        this.m = false;
        this.n = false;
        this.o = Integer.MAX_VALUE;
        this.q = 0;
        this.w = new ResizePopupRunnable();
        this.x = new PopupTouchInterceptor();
        this.y = new PopupScrollListener();
        this.z = new ListSelectorHider();
        this.B = new Rect();
        this.f406a = context;
        this.A = new Handler(context.getMainLooper());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.ListPopupWindow, i2, i3);
        this.f = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        this.g = obtainStyledAttributes.getDimensionPixelOffset(R$styleable.ListPopupWindow_android_dropDownVerticalOffset, 0);
        if (this.g != 0) {
            this.i = true;
        }
        obtainStyledAttributes.recycle();
        this.E = new AppCompatPopupWindow(context, attributeSet, i2, i3);
        this.E.setInputMethodMode(1);
    }

    public void b(boolean z2) {
        this.k = true;
        this.j = z2;
    }

    public void e(int i2) {
        Drawable background = this.E.getBackground();
        if (background != null) {
            background.getPadding(this.B);
            Rect rect = this.B;
            this.e = rect.left + rect.right + i2;
            return;
        }
        j(i2);
    }

    public void a(boolean z2) {
        this.D = z2;
        this.E.setFocusable(z2);
    }

    public void a(Drawable drawable) {
        this.E.setBackgroundDrawable(drawable);
    }

    public void a(View view) {
        this.s = view;
    }

    public int a() {
        return this.f;
    }

    public void a(int i2) {
        this.f = i2;
    }

    public void a(Rect rect) {
        this.C = rect != null ? new Rect(rect) : null;
    }

    public void a(AdapterView.OnItemClickListener onItemClickListener) {
        this.u = onItemClickListener;
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.E.setOnDismissListener(onDismissListener);
    }

    /* access modifiers changed from: package-private */
    public DropDownListView a(Context context, boolean z2) {
        return new DropDownListView(context, z2);
    }

    private int a(View view, int i2, boolean z2) {
        if (Build.VERSION.SDK_INT > 23) {
            return this.E.getMaxAvailableHeight(view, i2, z2);
        }
        Method method = G;
        if (method != null) {
            try {
                return ((Integer) method.invoke(this.E, new Object[]{view, Integer.valueOf(i2), Boolean.valueOf(z2)})).intValue();
            } catch (Exception unused) {
                Log.i("ListPopupWindow", "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version.");
            }
        }
        return this.E.getMaxAvailableHeight(view, i2);
    }
}
