package androidx.appcompat.widget;

import android.app.PendingIntent;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.R$attr;
import androidx.appcompat.R$dimen;
import androidx.appcompat.R$id;
import androidx.appcompat.R$layout;
import androidx.appcompat.R$string;
import androidx.appcompat.R$styleable;
import androidx.appcompat.view.CollapsibleActionView;
import androidx.core.view.ViewCompat;
import androidx.cursoradapter.widget.CursorAdapter;
import androidx.customview.view.AbsSavedState;
import com.applovin.sdk.AppLovinEventParameters;
import com.facebook.imageutils.JfifUtil;
import com.google.ar.core.ImageMetadata;
import java.lang.reflect.Method;
import java.util.WeakHashMap;

public class SearchView extends LinearLayoutCompat implements CollapsibleActionView {
    static final AutoCompleteTextViewReflector s0 = new AutoCompleteTextViewReflector();
    private Rect A;
    private int[] B;
    private int[] C;
    private final ImageView D;
    private final Drawable E;
    private final int F;
    private final int G;
    private final Intent H;
    private final Intent I;
    private final CharSequence J;
    private OnQueryTextListener K;
    private OnCloseListener L;
    View.OnFocusChangeListener M;
    private OnSuggestionListener N;
    private View.OnClickListener O;
    private boolean P;
    private boolean Q;
    CursorAdapter R;
    private boolean S;
    private CharSequence T;
    private boolean U;
    private boolean V;
    private int W;
    private boolean c0;
    private CharSequence d0;
    private CharSequence e0;
    private boolean f0;
    private int g0;
    SearchableInfo h0;
    private Bundle i0;
    private final Runnable j0;
    private Runnable k0;
    private final WeakHashMap<String, Drawable.ConstantState> l0;
    private final View.OnClickListener m0;
    View.OnKeyListener n0;
    private final TextView.OnEditorActionListener o0;
    final SearchAutoComplete p;
    private final AdapterView.OnItemClickListener p0;
    private final View q;
    private final AdapterView.OnItemSelectedListener q0;
    private final View r;
    private TextWatcher r0;
    private final View s;
    final ImageView t;
    final ImageView u;
    final ImageView v;
    final ImageView w;
    private final View x;
    private UpdatableTouchDelegate y;
    private Rect z;

    public interface OnCloseListener {
        boolean onClose();
    }

    public interface OnQueryTextListener {
        boolean a(String str);

        boolean b(String str);
    }

    public interface OnSuggestionListener {
        boolean a(int i);

        boolean b(int i);
    }

    static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }

            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, (ClassLoader) null);
            }
        };
        boolean c;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "SearchView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " isIconified=" + this.c + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeValue(Boolean.valueOf(this.c));
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.c = ((Boolean) parcel.readValue((ClassLoader) null)).booleanValue();
        }
    }

    public static class SearchAutoComplete extends AppCompatAutoCompleteTextView {
        private int d;
        private SearchView e;
        private boolean f;
        final Runnable g;

        public SearchAutoComplete(Context context) {
            this(context, (AttributeSet) null);
        }

        private int getSearchViewTextMinWidthDp() {
            Configuration configuration = getResources().getConfiguration();
            int i = configuration.screenWidthDp;
            int i2 = configuration.screenHeightDp;
            if (i >= 960 && i2 >= 720 && configuration.orientation == 2) {
                return 256;
            }
            if (i >= 600) {
                return JfifUtil.MARKER_SOFn;
            }
            if (i < 640 || i2 < 480) {
                return 160;
            }
            return JfifUtil.MARKER_SOFn;
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return TextUtils.getTrimmedLength(getText()) == 0;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (this.f) {
                ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                this.f = false;
            }
        }

        public boolean enoughToFilter() {
            return this.d <= 0 || super.enoughToFilter();
        }

        public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
            InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
            if (this.f) {
                removeCallbacks(this.g);
                post(this.g);
            }
            return onCreateInputConnection;
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            super.onFinishInflate();
            setMinWidth((int) TypedValue.applyDimension(1, (float) getSearchViewTextMinWidthDp(), getResources().getDisplayMetrics()));
        }

        /* access modifiers changed from: protected */
        public void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            this.e.j();
        }

        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (i == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState != null) {
                        keyDispatcherState.startTracking(keyEvent, this);
                    }
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.e.clearFocus();
                        setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i, keyEvent);
        }

        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.e.hasFocus() && getVisibility() == 0) {
                this.f = true;
                if (SearchView.a(getContext())) {
                    SearchView.s0.a(this, true);
                }
            }
        }

        public void performCompletion() {
        }

        /* access modifiers changed from: protected */
        public void replaceText(CharSequence charSequence) {
        }

        /* access modifiers changed from: package-private */
        public void setImeVisibility(boolean z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
            if (!z) {
                this.f = false;
                removeCallbacks(this.g);
                inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            } else if (inputMethodManager.isActive(this)) {
                this.f = false;
                removeCallbacks(this.g);
                inputMethodManager.showSoftInput(this, 0);
            } else {
                this.f = true;
            }
        }

        /* access modifiers changed from: package-private */
        public void setSearchView(SearchView searchView) {
            this.e = searchView;
        }

        public void setThreshold(int i) {
            super.setThreshold(i);
            this.d = i;
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, R$attr.autoCompleteTextViewStyle);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            this.g = new Runnable() {
                public void run() {
                    SearchAutoComplete.this.b();
                }
            };
            this.d = getThreshold();
        }
    }

    private static class UpdatableTouchDelegate extends TouchDelegate {

        /* renamed from: a  reason: collision with root package name */
        private final View f434a;
        private final Rect b = new Rect();
        private final Rect c = new Rect();
        private final Rect d = new Rect();
        private final int e;
        private boolean f;

        public UpdatableTouchDelegate(Rect rect, Rect rect2, View view) {
            super(rect, view);
            this.e = ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
            a(rect, rect2);
            this.f434a = view;
        }

        public void a(Rect rect, Rect rect2) {
            this.b.set(rect);
            this.d.set(rect);
            Rect rect3 = this.d;
            int i = this.e;
            rect3.inset(-i, -i);
            this.c.set(rect2);
        }

        /* JADX WARNING: Removed duplicated region for block: B:17:0x003d  */
        /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTouchEvent(android.view.MotionEvent r8) {
            /*
                r7 = this;
                float r0 = r8.getX()
                int r0 = (int) r0
                float r1 = r8.getY()
                int r1 = (int) r1
                int r2 = r8.getAction()
                r3 = 2
                r4 = 1
                r5 = 0
                if (r2 == 0) goto L_0x002e
                if (r2 == r4) goto L_0x0020
                if (r2 == r3) goto L_0x0020
                r6 = 3
                if (r2 == r6) goto L_0x001b
                goto L_0x003a
            L_0x001b:
                boolean r2 = r7.f
                r7.f = r5
                goto L_0x003b
            L_0x0020:
                boolean r2 = r7.f
                if (r2 == 0) goto L_0x003b
                android.graphics.Rect r6 = r7.d
                boolean r6 = r6.contains(r0, r1)
                if (r6 != 0) goto L_0x003b
                r4 = 0
                goto L_0x003b
            L_0x002e:
                android.graphics.Rect r2 = r7.b
                boolean r2 = r2.contains(r0, r1)
                if (r2 == 0) goto L_0x003a
                r7.f = r4
                r2 = 1
                goto L_0x003b
            L_0x003a:
                r2 = 0
            L_0x003b:
                if (r2 == 0) goto L_0x006e
                if (r4 == 0) goto L_0x005b
                android.graphics.Rect r2 = r7.c
                boolean r2 = r2.contains(r0, r1)
                if (r2 != 0) goto L_0x005b
                android.view.View r0 = r7.f434a
                int r0 = r0.getWidth()
                int r0 = r0 / r3
                float r0 = (float) r0
                android.view.View r1 = r7.f434a
                int r1 = r1.getHeight()
                int r1 = r1 / r3
                float r1 = (float) r1
                r8.setLocation(r0, r1)
                goto L_0x0068
            L_0x005b:
                android.graphics.Rect r2 = r7.c
                int r3 = r2.left
                int r0 = r0 - r3
                float r0 = (float) r0
                int r2 = r2.top
                int r1 = r1 - r2
                float r1 = (float) r1
                r8.setLocation(r0, r1)
            L_0x0068:
                android.view.View r0 = r7.f434a
                boolean r5 = r0.dispatchTouchEvent(r8)
            L_0x006e:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.SearchView.UpdatableTouchDelegate.onTouchEvent(android.view.MotionEvent):boolean");
        }
    }

    public SearchView(Context context) {
        this(context, (AttributeSet) null);
    }

    private void b(boolean z2) {
        this.Q = z2;
        int i = 0;
        int i2 = z2 ? 0 : 8;
        boolean z3 = !TextUtils.isEmpty(this.p.getText());
        this.t.setVisibility(i2);
        a(z3);
        this.q.setVisibility(z2 ? 8 : 0);
        if (this.D.getDrawable() == null || this.P) {
            i = 8;
        }
        this.D.setVisibility(i);
        q();
        c(!z3);
        t();
    }

    private CharSequence c(CharSequence charSequence) {
        if (!this.P || this.E == null) {
            return charSequence;
        }
        int textSize = (int) (((double) this.p.getTextSize()) * 1.25d);
        this.E.setBounds(0, 0, textSize, textSize);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
        spannableStringBuilder.setSpan(new ImageSpan(this.E), 1, 2, 33);
        spannableStringBuilder.append(charSequence);
        return spannableStringBuilder;
    }

    private void e(int i) {
        Editable text = this.p.getText();
        Cursor a2 = this.R.a();
        if (a2 != null) {
            if (a2.moveToPosition(i)) {
                CharSequence a3 = this.R.a(a2);
                if (a3 != null) {
                    setQuery(a3);
                } else {
                    setQuery(text);
                }
            } else {
                setQuery(text);
            }
        }
    }

    private int getPreferredHeight() {
        return getContext().getResources().getDimensionPixelSize(R$dimen.abc_search_view_preferred_height);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(R$dimen.abc_search_view_preferred_width);
    }

    private void m() {
        this.p.dismissDropDown();
    }

    private boolean n() {
        SearchableInfo searchableInfo = this.h0;
        if (searchableInfo == null || !searchableInfo.getVoiceSearchEnabled()) {
            return false;
        }
        Intent intent = null;
        if (this.h0.getVoiceSearchLaunchWebSearch()) {
            intent = this.H;
        } else if (this.h0.getVoiceSearchLaunchRecognizer()) {
            intent = this.I;
        }
        if (intent == null || getContext().getPackageManager().resolveActivity(intent, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE) == null) {
            return false;
        }
        return true;
    }

    private boolean o() {
        return (this.S || this.c0) && !f();
    }

    private void p() {
        post(this.j0);
    }

    private void q() {
        boolean z2 = true;
        boolean z3 = !TextUtils.isEmpty(this.p.getText());
        int i = 0;
        if (!z3 && (!this.P || this.f0)) {
            z2 = false;
        }
        ImageView imageView = this.v;
        if (!z2) {
            i = 8;
        }
        imageView.setVisibility(i);
        Drawable drawable = this.v.getDrawable();
        if (drawable != null) {
            drawable.setState(z3 ? ViewGroup.ENABLED_STATE_SET : ViewGroup.EMPTY_STATE_SET);
        }
    }

    private void r() {
        CharSequence queryHint = getQueryHint();
        SearchAutoComplete searchAutoComplete = this.p;
        if (queryHint == null) {
            queryHint = "";
        }
        searchAutoComplete.setHint(c(queryHint));
    }

    private void s() {
        this.p.setThreshold(this.h0.getSuggestThreshold());
        this.p.setImeOptions(this.h0.getImeOptions());
        int inputType = this.h0.getInputType();
        int i = 1;
        if ((inputType & 15) == 1) {
            inputType &= -65537;
            if (this.h0.getSuggestAuthority() != null) {
                inputType = inputType | ImageMetadata.CONTROL_AE_ANTIBANDING_MODE | ImageMetadata.LENS_APERTURE;
            }
        }
        this.p.setInputType(inputType);
        CursorAdapter cursorAdapter = this.R;
        if (cursorAdapter != null) {
            cursorAdapter.b((Cursor) null);
        }
        if (this.h0.getSuggestAuthority() != null) {
            this.R = new SuggestionsAdapter(getContext(), this, this.h0, this.l0);
            this.p.setAdapter(this.R);
            SuggestionsAdapter suggestionsAdapter = (SuggestionsAdapter) this.R;
            if (this.U) {
                i = 2;
            }
            suggestionsAdapter.a(i);
        }
    }

    private void setQuery(CharSequence charSequence) {
        this.p.setText(charSequence);
        this.p.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    private void t() {
        this.s.setVisibility((!o() || !(this.u.getVisibility() == 0 || this.w.getVisibility() == 0)) ? 8 : 0);
    }

    public void a(CharSequence charSequence, boolean z2) {
        this.p.setText(charSequence);
        if (charSequence != null) {
            SearchAutoComplete searchAutoComplete = this.p;
            searchAutoComplete.setSelection(searchAutoComplete.length());
            this.e0 = charSequence;
        }
        if (z2 && !TextUtils.isEmpty(charSequence)) {
            i();
        }
    }

    public void clearFocus() {
        this.V = true;
        super.clearFocus();
        this.p.clearFocus();
        this.p.setImeVisibility(false);
        this.V = false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        int i;
        if (this.x.getWidth() > 1) {
            Resources resources = getContext().getResources();
            int paddingLeft = this.r.getPaddingLeft();
            Rect rect = new Rect();
            boolean a2 = ViewUtils.a(this);
            int dimensionPixelSize = this.P ? resources.getDimensionPixelSize(R$dimen.abc_dropdownitem_icon_width) + resources.getDimensionPixelSize(R$dimen.abc_dropdownitem_text_padding_left) : 0;
            this.p.getDropDownBackground().getPadding(rect);
            if (a2) {
                i = -rect.left;
            } else {
                i = paddingLeft - (rect.left + dimensionPixelSize);
            }
            this.p.setDropDownHorizontalOffset(i);
            this.p.setDropDownWidth((((this.x.getWidth() + rect.left) + rect.right) + dimensionPixelSize) - paddingLeft);
        }
    }

    public boolean f() {
        return this.Q;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (!TextUtils.isEmpty(this.p.getText())) {
            this.p.setText("");
            this.p.requestFocus();
            this.p.setImeVisibility(true);
        } else if (this.P) {
            OnCloseListener onCloseListener = this.L;
            if (onCloseListener == null || !onCloseListener.onClose()) {
                clearFocus();
                b(true);
            }
        }
    }

    public int getImeOptions() {
        return this.p.getImeOptions();
    }

    public int getInputType() {
        return this.p.getInputType();
    }

    public int getMaxWidth() {
        return this.W;
    }

    public CharSequence getQuery() {
        return this.p.getText();
    }

    public CharSequence getQueryHint() {
        CharSequence charSequence = this.T;
        if (charSequence != null) {
            return charSequence;
        }
        SearchableInfo searchableInfo = this.h0;
        if (searchableInfo == null || searchableInfo.getHintId() == 0) {
            return this.J;
        }
        return getContext().getText(this.h0.getHintId());
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionCommitIconResId() {
        return this.G;
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionRowLayout() {
        return this.F;
    }

    public CursorAdapter getSuggestionsAdapter() {
        return this.R;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        b(false);
        this.p.requestFocus();
        this.p.setImeVisibility(true);
        View.OnClickListener onClickListener = this.O;
        if (onClickListener != null) {
            onClickListener.onClick(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        Editable text = this.p.getText();
        if (text != null && TextUtils.getTrimmedLength(text) > 0) {
            OnQueryTextListener onQueryTextListener = this.K;
            if (onQueryTextListener == null || !onQueryTextListener.b(text.toString())) {
                if (this.h0 != null) {
                    a(0, (String) null, text.toString());
                }
                this.p.setImeVisibility(false);
                m();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        b(f());
        p();
        if (this.p.hasFocus()) {
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        SearchableInfo searchableInfo = this.h0;
        if (searchableInfo != null) {
            try {
                if (searchableInfo.getVoiceSearchLaunchWebSearch()) {
                    getContext().startActivity(b(this.H, searchableInfo));
                } else if (searchableInfo.getVoiceSearchLaunchRecognizer()) {
                    getContext().startActivity(a(this.I, searchableInfo));
                }
            } catch (ActivityNotFoundException unused) {
                Log.w("SearchView", "Could not find voice search activity");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        int[] iArr = this.p.hasFocus() ? ViewGroup.FOCUSED_STATE_SET : ViewGroup.EMPTY_STATE_SET;
        Drawable background = this.r.getBackground();
        if (background != null) {
            background.setState(iArr);
        }
        Drawable background2 = this.s.getBackground();
        if (background2 != null) {
            background2.setState(iArr);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.j0);
        post(this.k0);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        super.onLayout(z2, i, i2, i3, i4);
        if (z2) {
            a((View) this.p, this.z);
            Rect rect = this.A;
            Rect rect2 = this.z;
            rect.set(rect2.left, 0, rect2.right, i4 - i2);
            UpdatableTouchDelegate updatableTouchDelegate = this.y;
            if (updatableTouchDelegate == null) {
                this.y = new UpdatableTouchDelegate(this.A, this.z, this.p);
                setTouchDelegate(this.y);
                return;
            }
            updatableTouchDelegate.a(this.A, this.z);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        if (f()) {
            super.onMeasure(i, i2);
            return;
        }
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        if (mode == Integer.MIN_VALUE) {
            int i4 = this.W;
            size = i4 > 0 ? Math.min(i4, size) : Math.min(getPreferredWidth(), size);
        } else if (mode == 0) {
            size = this.W;
            if (size <= 0) {
                size = getPreferredWidth();
            }
        } else if (mode == 1073741824 && (i3 = this.W) > 0) {
            size = Math.min(i3, size);
        }
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        if (mode2 == Integer.MIN_VALUE) {
            size2 = Math.min(getPreferredHeight(), size2);
        } else if (mode2 == 0) {
            size2 = getPreferredHeight();
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(size2, 1073741824));
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        b(savedState.c);
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.c = f();
        return savedState;
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        p();
    }

    public boolean requestFocus(int i, Rect rect) {
        if (this.V || !isFocusable()) {
            return false;
        }
        if (f()) {
            return super.requestFocus(i, rect);
        }
        boolean requestFocus = this.p.requestFocus(i, rect);
        if (requestFocus) {
            b(false);
        }
        return requestFocus;
    }

    public void setAppSearchData(Bundle bundle) {
        this.i0 = bundle;
    }

    public void setIconified(boolean z2) {
        if (z2) {
            g();
        } else {
            h();
        }
    }

    public void setIconifiedByDefault(boolean z2) {
        if (this.P != z2) {
            this.P = z2;
            b(z2);
            r();
        }
    }

    public void setImeOptions(int i) {
        this.p.setImeOptions(i);
    }

    public void setInputType(int i) {
        this.p.setInputType(i);
    }

    public void setMaxWidth(int i) {
        this.W = i;
        requestLayout();
    }

    public void setOnCloseListener(OnCloseListener onCloseListener) {
        this.L = onCloseListener;
    }

    public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.M = onFocusChangeListener;
    }

    public void setOnQueryTextListener(OnQueryTextListener onQueryTextListener) {
        this.K = onQueryTextListener;
    }

    public void setOnSearchClickListener(View.OnClickListener onClickListener) {
        this.O = onClickListener;
    }

    public void setOnSuggestionListener(OnSuggestionListener onSuggestionListener) {
        this.N = onSuggestionListener;
    }

    public void setQueryHint(CharSequence charSequence) {
        this.T = charSequence;
        r();
    }

    public void setQueryRefinementEnabled(boolean z2) {
        this.U = z2;
        CursorAdapter cursorAdapter = this.R;
        if (cursorAdapter instanceof SuggestionsAdapter) {
            ((SuggestionsAdapter) cursorAdapter).a(z2 ? 2 : 1);
        }
    }

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        this.h0 = searchableInfo;
        if (this.h0 != null) {
            s();
            r();
        }
        this.c0 = n();
        if (this.c0) {
            this.p.setPrivateImeOptions("nm");
        }
        b(f());
    }

    public void setSubmitButtonEnabled(boolean z2) {
        this.S = z2;
        b(f());
    }

    public void setSuggestionsAdapter(CursorAdapter cursorAdapter) {
        this.R = cursorAdapter;
        this.p.setAdapter(this.R);
    }

    private static class AutoCompleteTextViewReflector {

        /* renamed from: a  reason: collision with root package name */
        private Method f432a;
        private Method b;
        private Method c;

        AutoCompleteTextViewReflector() {
            try {
                this.f432a = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
                this.f432a.setAccessible(true);
            } catch (NoSuchMethodException unused) {
            }
            try {
                this.b = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
                this.b.setAccessible(true);
            } catch (NoSuchMethodException unused2) {
            }
            Class<AutoCompleteTextView> cls = AutoCompleteTextView.class;
            try {
                this.c = cls.getMethod("ensureImeVisible", new Class[]{Boolean.TYPE});
                this.c.setAccessible(true);
            } catch (NoSuchMethodException unused3) {
            }
        }

        /* access modifiers changed from: package-private */
        public void a(AutoCompleteTextView autoCompleteTextView) {
            Method method = this.b;
            if (method != null) {
                try {
                    method.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception unused) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b(AutoCompleteTextView autoCompleteTextView) {
            Method method = this.f432a;
            if (method != null) {
                try {
                    method.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception unused) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(AutoCompleteTextView autoCompleteTextView, boolean z) {
            Method method = this.c;
            if (method != null) {
                try {
                    method.invoke(autoCompleteTextView, new Object[]{Boolean.valueOf(z)});
                } catch (Exception unused) {
                }
            }
        }
    }

    public SearchView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R$attr.searchViewStyle);
    }

    public SearchView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.z = new Rect();
        this.A = new Rect();
        this.B = new int[2];
        this.C = new int[2];
        this.j0 = new Runnable() {
            public void run() {
                SearchView.this.l();
            }
        };
        this.k0 = new Runnable() {
            public void run() {
                CursorAdapter cursorAdapter = SearchView.this.R;
                if (cursorAdapter instanceof SuggestionsAdapter) {
                    cursorAdapter.b((Cursor) null);
                }
            }
        };
        this.l0 = new WeakHashMap<>();
        this.m0 = new View.OnClickListener() {
            public void onClick(View view) {
                SearchView searchView = SearchView.this;
                if (view == searchView.t) {
                    searchView.h();
                } else if (view == searchView.v) {
                    searchView.g();
                } else if (view == searchView.u) {
                    searchView.i();
                } else if (view == searchView.w) {
                    searchView.k();
                } else if (view == searchView.p) {
                    searchView.e();
                }
            }
        };
        this.n0 = new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                SearchView searchView = SearchView.this;
                if (searchView.h0 == null) {
                    return false;
                }
                if (searchView.p.isPopupShowing() && SearchView.this.p.getListSelection() != -1) {
                    return SearchView.this.a(view, i, keyEvent);
                }
                if (SearchView.this.p.a() || !keyEvent.hasNoModifiers() || keyEvent.getAction() != 1 || i != 66) {
                    return false;
                }
                view.cancelLongPress();
                SearchView searchView2 = SearchView.this;
                searchView2.a(0, (String) null, searchView2.p.getText().toString());
                return true;
            }
        };
        this.o0 = new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                SearchView.this.i();
                return true;
            }
        };
        this.p0 = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                SearchView.this.a(i, 0, (String) null);
            }
        };
        this.q0 = new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                SearchView.this.d(i);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        };
        this.r0 = new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                SearchView.this.b(charSequence);
            }
        };
        TintTypedArray a2 = TintTypedArray.a(context, attributeSet, R$styleable.SearchView, i, 0);
        LayoutInflater.from(context).inflate(a2.g(R$styleable.SearchView_layout, R$layout.abc_search_view), this, true);
        this.p = (SearchAutoComplete) findViewById(R$id.search_src_text);
        this.p.setSearchView(this);
        this.q = findViewById(R$id.search_edit_frame);
        this.r = findViewById(R$id.search_plate);
        this.s = findViewById(R$id.submit_area);
        this.t = (ImageView) findViewById(R$id.search_button);
        this.u = (ImageView) findViewById(R$id.search_go_btn);
        this.v = (ImageView) findViewById(R$id.search_close_btn);
        this.w = (ImageView) findViewById(R$id.search_voice_btn);
        this.D = (ImageView) findViewById(R$id.search_mag_icon);
        ViewCompat.a(this.r, a2.b(R$styleable.SearchView_queryBackground));
        ViewCompat.a(this.s, a2.b(R$styleable.SearchView_submitBackground));
        this.t.setImageDrawable(a2.b(R$styleable.SearchView_searchIcon));
        this.u.setImageDrawable(a2.b(R$styleable.SearchView_goIcon));
        this.v.setImageDrawable(a2.b(R$styleable.SearchView_closeIcon));
        this.w.setImageDrawable(a2.b(R$styleable.SearchView_voiceIcon));
        this.D.setImageDrawable(a2.b(R$styleable.SearchView_searchIcon));
        this.E = a2.b(R$styleable.SearchView_searchHintIcon);
        TooltipCompat.a(this.t, getResources().getString(R$string.abc_searchview_description_search));
        this.F = a2.g(R$styleable.SearchView_suggestionRowLayout, R$layout.abc_search_dropdown_item_icons_2line);
        this.G = a2.g(R$styleable.SearchView_commitIcon, 0);
        this.t.setOnClickListener(this.m0);
        this.v.setOnClickListener(this.m0);
        this.u.setOnClickListener(this.m0);
        this.w.setOnClickListener(this.m0);
        this.p.setOnClickListener(this.m0);
        this.p.addTextChangedListener(this.r0);
        this.p.setOnEditorActionListener(this.o0);
        this.p.setOnItemClickListener(this.p0);
        this.p.setOnItemSelectedListener(this.q0);
        this.p.setOnKeyListener(this.n0);
        this.p.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                SearchView searchView = SearchView.this;
                View.OnFocusChangeListener onFocusChangeListener = searchView.M;
                if (onFocusChangeListener != null) {
                    onFocusChangeListener.onFocusChange(searchView, z);
                }
            }
        });
        setIconifiedByDefault(a2.a(R$styleable.SearchView_iconifiedByDefault, true));
        int c = a2.c(R$styleable.SearchView_android_maxWidth, -1);
        if (c != -1) {
            setMaxWidth(c);
        }
        this.J = a2.e(R$styleable.SearchView_defaultQueryHint);
        this.T = a2.e(R$styleable.SearchView_queryHint);
        int d = a2.d(R$styleable.SearchView_android_imeOptions, -1);
        if (d != -1) {
            setImeOptions(d);
        }
        int d2 = a2.d(R$styleable.SearchView_android_inputType, -1);
        if (d2 != -1) {
            setInputType(d2);
        }
        setFocusable(a2.a(R$styleable.SearchView_android_focusable, true));
        a2.a();
        this.H = new Intent("android.speech.action.WEB_SEARCH");
        this.H.addFlags(268435456);
        this.H.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
        this.I = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        this.I.addFlags(268435456);
        this.x = findViewById(this.p.getDropDownAnchor());
        View view = this.x;
        if (view != null) {
            view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    SearchView.this.d();
                }
            });
        }
        b(this.P);
        r();
    }

    private void a(View view, Rect rect) {
        view.getLocationInWindow(this.B);
        getLocationInWindow(this.C);
        int[] iArr = this.B;
        int i = iArr[1];
        int[] iArr2 = this.C;
        int i2 = i - iArr2[1];
        int i3 = iArr[0] - iArr2[0];
        rect.set(i3, i2, view.getWidth() + i3, view.getHeight() + i2);
    }

    private void c(boolean z2) {
        int i;
        if (!this.c0 || f() || !z2) {
            i = 8;
        } else {
            i = 0;
            this.u.setVisibility(8);
        }
        this.w.setVisibility(i);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (Build.VERSION.SDK_INT >= 29) {
            this.p.refreshAutoCompleteResults();
            return;
        }
        s0.b(this.p);
        s0.a(this.p);
    }

    public void c() {
        a((CharSequence) "", false);
        clearFocus();
        b(true);
        this.p.setImeOptions(this.g0);
        this.f0 = false;
    }

    private void a(boolean z2) {
        this.u.setVisibility((!this.S || !o() || !hasFocus() || (!z2 && this.c0)) ? 8 : 0);
    }

    /* access modifiers changed from: package-private */
    public void b(CharSequence charSequence) {
        Editable text = this.p.getText();
        this.e0 = text;
        boolean z2 = !TextUtils.isEmpty(text);
        a(z2);
        c(!z2);
        q();
        t();
        if (this.K != null && !TextUtils.equals(charSequence, this.d0)) {
            this.K.a(charSequence.toString());
        }
        this.d0 = charSequence.toString();
    }

    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        setQuery(charSequence);
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i, KeyEvent keyEvent) {
        int i2;
        if (this.h0 != null && this.R != null && keyEvent.getAction() == 0 && keyEvent.hasNoModifiers()) {
            if (i == 66 || i == 84 || i == 61) {
                return a(this.p.getListSelection(), 0, (String) null);
            }
            if (i == 21 || i == 22) {
                if (i == 21) {
                    i2 = 0;
                } else {
                    i2 = this.p.length();
                }
                this.p.setSelection(i2);
                this.p.setListSelection(0);
                this.p.clearListSelection();
                s0.a(this.p, true);
                return true;
            } else if (i != 19 || this.p.getListSelection() == 0) {
                return false;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean d(int i) {
        OnSuggestionListener onSuggestionListener = this.N;
        if (onSuggestionListener != null && onSuggestionListener.a(i)) {
            return false;
        }
        e(i);
        return true;
    }

    public void b() {
        if (!this.f0) {
            this.f0 = true;
            this.g0 = this.p.getImeOptions();
            this.p.setImeOptions(this.g0 | 33554432);
            this.p.setText("");
            setIconified(false);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, String str) {
        OnSuggestionListener onSuggestionListener = this.N;
        if (onSuggestionListener != null && onSuggestionListener.b(i)) {
            return false;
        }
        b(i, 0, (String) null);
        this.p.setImeVisibility(false);
        m();
        return true;
    }

    private boolean b(int i, int i2, String str) {
        Cursor a2 = this.R.a();
        if (a2 == null || !a2.moveToPosition(i)) {
            return false;
        }
        a(a(a2, i2, str));
        return true;
    }

    private void a(Intent intent) {
        if (intent != null) {
            try {
                getContext().startActivity(intent);
            } catch (RuntimeException e) {
                Log.e("SearchView", "Failed launch activity: " + intent, e);
            }
        }
    }

    private Intent b(Intent intent, SearchableInfo searchableInfo) {
        String str;
        Intent intent2 = new Intent(intent);
        ComponentName searchActivity = searchableInfo.getSearchActivity();
        if (searchActivity == null) {
            str = null;
        } else {
            str = searchActivity.flattenToShortString();
        }
        intent2.putExtra("calling_package", str);
        return intent2;
    }

    /* access modifiers changed from: package-private */
    public void a(int i, String str, String str2) {
        getContext().startActivity(a("android.intent.action.SEARCH", (Uri) null, (String) null, str2, i, str));
    }

    private Intent a(String str, Uri uri, String str2, String str3, int i, String str4) {
        Intent intent = new Intent(str);
        intent.addFlags(268435456);
        if (uri != null) {
            intent.setData(uri);
        }
        intent.putExtra("user_query", this.e0);
        if (str3 != null) {
            intent.putExtra(AppLovinEventParameters.SEARCH_QUERY, str3);
        }
        if (str2 != null) {
            intent.putExtra("intent_extra_data_key", str2);
        }
        Bundle bundle = this.i0;
        if (bundle != null) {
            intent.putExtra("app_data", bundle);
        }
        if (i != 0) {
            intent.putExtra("action_key", i);
            intent.putExtra("action_msg", str4);
        }
        intent.setComponent(this.h0.getSearchActivity());
        return intent;
    }

    private Intent a(Intent intent, SearchableInfo searchableInfo) {
        ComponentName searchActivity = searchableInfo.getSearchActivity();
        Intent intent2 = new Intent("android.intent.action.SEARCH");
        intent2.setComponent(searchActivity);
        PendingIntent activity = PendingIntent.getActivity(getContext(), 0, intent2, 1073741824);
        Bundle bundle = new Bundle();
        Bundle bundle2 = this.i0;
        if (bundle2 != null) {
            bundle.putParcelable("app_data", bundle2);
        }
        Intent intent3 = new Intent(intent);
        int i = 1;
        Resources resources = getResources();
        String string = searchableInfo.getVoiceLanguageModeId() != 0 ? resources.getString(searchableInfo.getVoiceLanguageModeId()) : "free_form";
        String str = null;
        String string2 = searchableInfo.getVoicePromptTextId() != 0 ? resources.getString(searchableInfo.getVoicePromptTextId()) : null;
        String string3 = searchableInfo.getVoiceLanguageId() != 0 ? resources.getString(searchableInfo.getVoiceLanguageId()) : null;
        if (searchableInfo.getVoiceMaxResults() != 0) {
            i = searchableInfo.getVoiceMaxResults();
        }
        intent3.putExtra("android.speech.extra.LANGUAGE_MODEL", string);
        intent3.putExtra("android.speech.extra.PROMPT", string2);
        intent3.putExtra("android.speech.extra.LANGUAGE", string3);
        intent3.putExtra("android.speech.extra.MAX_RESULTS", i);
        if (searchActivity != null) {
            str = searchActivity.flattenToShortString();
        }
        intent3.putExtra("calling_package", str);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", activity);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", bundle);
        return intent3;
    }

    private Intent a(Cursor cursor, int i, String str) {
        int i2;
        Uri uri;
        String a2;
        try {
            String a3 = SuggestionsAdapter.a(cursor, "suggest_intent_action");
            if (a3 == null) {
                a3 = this.h0.getSuggestIntentAction();
            }
            if (a3 == null) {
                a3 = "android.intent.action.SEARCH";
            }
            String str2 = a3;
            String a4 = SuggestionsAdapter.a(cursor, "suggest_intent_data");
            if (a4 == null) {
                a4 = this.h0.getSuggestIntentData();
            }
            if (!(a4 == null || (a2 = SuggestionsAdapter.a(cursor, "suggest_intent_data_id")) == null)) {
                a4 = a4 + "/" + Uri.encode(a2);
            }
            if (a4 == null) {
                uri = null;
            } else {
                uri = Uri.parse(a4);
            }
            return a(str2, uri, SuggestionsAdapter.a(cursor, "suggest_intent_extra_data"), SuggestionsAdapter.a(cursor, "suggest_intent_query"), i, str);
        } catch (RuntimeException e) {
            try {
                i2 = cursor.getPosition();
            } catch (RuntimeException unused) {
                i2 = -1;
            }
            Log.w("SearchView", "Search suggestions cursor at row " + i2 + " returned exception.", e);
            return null;
        }
    }

    static boolean a(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }
}
