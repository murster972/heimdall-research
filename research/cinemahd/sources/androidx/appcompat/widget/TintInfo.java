package androidx.appcompat.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

public class TintInfo {

    /* renamed from: a  reason: collision with root package name */
    public ColorStateList f438a;
    public PorterDuff.Mode b;
    public boolean c;
    public boolean d;

    /* access modifiers changed from: package-private */
    public void a() {
        this.f438a = null;
        this.d = false;
        this.b = null;
        this.c = false;
    }
}
