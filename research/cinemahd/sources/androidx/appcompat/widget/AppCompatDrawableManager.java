package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import androidx.appcompat.R$attr;
import androidx.appcompat.R$color;
import androidx.appcompat.R$drawable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.ResourceManagerInternal;
import androidx.core.graphics.ColorUtils;

public final class AppCompatDrawableManager {
    /* access modifiers changed from: private */
    public static final PorterDuff.Mode b = PorterDuff.Mode.SRC_IN;
    private static AppCompatDrawableManager c;

    /* renamed from: a  reason: collision with root package name */
    private ResourceManagerInternal f371a;

    public static synchronized AppCompatDrawableManager b() {
        AppCompatDrawableManager appCompatDrawableManager;
        synchronized (AppCompatDrawableManager.class) {
            if (c == null) {
                c();
            }
            appCompatDrawableManager = c;
        }
        return appCompatDrawableManager;
    }

    public static synchronized void c() {
        synchronized (AppCompatDrawableManager.class) {
            if (c == null) {
                c = new AppCompatDrawableManager();
                c.f371a = ResourceManagerInternal.a();
                c.f371a.a((ResourceManagerInternal.ResourceManagerHooks) new ResourceManagerInternal.ResourceManagerHooks() {

                    /* renamed from: a  reason: collision with root package name */
                    private final int[] f372a = {R$drawable.abc_textfield_search_default_mtrl_alpha, R$drawable.abc_textfield_default_mtrl_alpha, R$drawable.abc_ab_share_pack_mtrl_alpha};
                    private final int[] b = {R$drawable.abc_ic_commit_search_api_mtrl_alpha, R$drawable.abc_seekbar_tick_mark_material, R$drawable.abc_ic_menu_share_mtrl_alpha, R$drawable.abc_ic_menu_copy_mtrl_am_alpha, R$drawable.abc_ic_menu_cut_mtrl_alpha, R$drawable.abc_ic_menu_selectall_mtrl_alpha, R$drawable.abc_ic_menu_paste_mtrl_am_alpha};
                    private final int[] c = {R$drawable.abc_textfield_activated_mtrl_alpha, R$drawable.abc_textfield_search_activated_mtrl_alpha, R$drawable.abc_cab_background_top_mtrl_alpha, R$drawable.abc_text_cursor_material, R$drawable.abc_text_select_handle_left_mtrl_dark, R$drawable.abc_text_select_handle_middle_mtrl_dark, R$drawable.abc_text_select_handle_right_mtrl_dark, R$drawable.abc_text_select_handle_left_mtrl_light, R$drawable.abc_text_select_handle_middle_mtrl_light, R$drawable.abc_text_select_handle_right_mtrl_light};
                    private final int[] d = {R$drawable.abc_popup_background_mtrl_mult, R$drawable.abc_cab_background_internal_bg, R$drawable.abc_menu_hardkey_panel_mtrl_mult};
                    private final int[] e = {R$drawable.abc_tab_indicator_material, R$drawable.abc_textfield_search_material};
                    private final int[] f = {R$drawable.abc_btn_check_material, R$drawable.abc_btn_radio_material, R$drawable.abc_btn_check_material_anim, R$drawable.abc_btn_radio_material_anim};

                    private ColorStateList a(Context context) {
                        return b(context, 0);
                    }

                    private ColorStateList b(Context context) {
                        return b(context, ThemeUtils.b(context, R$attr.colorAccent));
                    }

                    private ColorStateList c(Context context) {
                        return b(context, ThemeUtils.b(context, R$attr.colorButtonNormal));
                    }

                    private ColorStateList d(Context context) {
                        int[][] iArr = new int[3][];
                        int[] iArr2 = new int[3];
                        ColorStateList c2 = ThemeUtils.c(context, R$attr.colorSwitchThumbNormal);
                        if (c2 == null || !c2.isStateful()) {
                            iArr[0] = ThemeUtils.b;
                            iArr2[0] = ThemeUtils.a(context, R$attr.colorSwitchThumbNormal);
                            iArr[1] = ThemeUtils.e;
                            iArr2[1] = ThemeUtils.b(context, R$attr.colorControlActivated);
                            iArr[2] = ThemeUtils.f;
                            iArr2[2] = ThemeUtils.b(context, R$attr.colorSwitchThumbNormal);
                        } else {
                            iArr[0] = ThemeUtils.b;
                            iArr2[0] = c2.getColorForState(iArr[0], 0);
                            iArr[1] = ThemeUtils.e;
                            iArr2[1] = ThemeUtils.b(context, R$attr.colorControlActivated);
                            iArr[2] = ThemeUtils.f;
                            iArr2[2] = c2.getDefaultColor();
                        }
                        return new ColorStateList(iArr, iArr2);
                    }

                    public Drawable a(ResourceManagerInternal resourceManagerInternal, Context context, int i) {
                        if (i != R$drawable.abc_cab_background_top_material) {
                            return null;
                        }
                        return new LayerDrawable(new Drawable[]{resourceManagerInternal.a(context, R$drawable.abc_cab_background_internal_bg), resourceManagerInternal.a(context, R$drawable.abc_cab_background_top_mtrl_alpha)});
                    }

                    private ColorStateList b(Context context, int i) {
                        int b2 = ThemeUtils.b(context, R$attr.colorControlHighlight);
                        int a2 = ThemeUtils.a(context, R$attr.colorButtonNormal);
                        return new ColorStateList(new int[][]{ThemeUtils.b, ThemeUtils.d, ThemeUtils.c, ThemeUtils.f}, new int[]{a2, ColorUtils.c(b2, i), ColorUtils.c(b2, i), i});
                    }

                    private void a(Drawable drawable, int i, PorterDuff.Mode mode) {
                        if (DrawableUtils.a(drawable)) {
                            drawable = drawable.mutate();
                        }
                        if (mode == null) {
                            mode = AppCompatDrawableManager.b;
                        }
                        drawable.setColorFilter(AppCompatDrawableManager.a(i, mode));
                    }

                    private boolean a(int[] iArr, int i) {
                        for (int i2 : iArr) {
                            if (i2 == i) {
                                return true;
                            }
                        }
                        return false;
                    }

                    public ColorStateList a(Context context, int i) {
                        if (i == R$drawable.abc_edit_text_material) {
                            return AppCompatResources.b(context, R$color.abc_tint_edittext);
                        }
                        if (i == R$drawable.abc_switch_track_mtrl_alpha) {
                            return AppCompatResources.b(context, R$color.abc_tint_switch_track);
                        }
                        if (i == R$drawable.abc_switch_thumb_material) {
                            return d(context);
                        }
                        if (i == R$drawable.abc_btn_default_mtrl_shape) {
                            return c(context);
                        }
                        if (i == R$drawable.abc_btn_borderless_material) {
                            return a(context);
                        }
                        if (i == R$drawable.abc_btn_colored_material) {
                            return b(context);
                        }
                        if (i == R$drawable.abc_spinner_mtrl_am_alpha || i == R$drawable.abc_spinner_textfield_background_material) {
                            return AppCompatResources.b(context, R$color.abc_tint_spinner);
                        }
                        if (a(this.b, i)) {
                            return ThemeUtils.c(context, R$attr.colorControlNormal);
                        }
                        if (a(this.e, i)) {
                            return AppCompatResources.b(context, R$color.abc_tint_default);
                        }
                        if (a(this.f, i)) {
                            return AppCompatResources.b(context, R$color.abc_tint_btn_checkable);
                        }
                        if (i == R$drawable.abc_seekbar_thumb_material) {
                            return AppCompatResources.b(context, R$color.abc_tint_seek_thumb);
                        }
                        return null;
                    }

                    public boolean b(Context context, int i, Drawable drawable) {
                        if (i == R$drawable.abc_seekbar_track_material) {
                            LayerDrawable layerDrawable = (LayerDrawable) drawable;
                            a(layerDrawable.findDrawableByLayerId(16908288), ThemeUtils.b(context, R$attr.colorControlNormal), AppCompatDrawableManager.b);
                            a(layerDrawable.findDrawableByLayerId(16908303), ThemeUtils.b(context, R$attr.colorControlNormal), AppCompatDrawableManager.b);
                            a(layerDrawable.findDrawableByLayerId(16908301), ThemeUtils.b(context, R$attr.colorControlActivated), AppCompatDrawableManager.b);
                            return true;
                        } else if (i != R$drawable.abc_ratingbar_material && i != R$drawable.abc_ratingbar_indicator_material && i != R$drawable.abc_ratingbar_small_material) {
                            return false;
                        } else {
                            LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
                            a(layerDrawable2.findDrawableByLayerId(16908288), ThemeUtils.a(context, R$attr.colorControlNormal), AppCompatDrawableManager.b);
                            a(layerDrawable2.findDrawableByLayerId(16908303), ThemeUtils.b(context, R$attr.colorControlActivated), AppCompatDrawableManager.b);
                            a(layerDrawable2.findDrawableByLayerId(16908301), ThemeUtils.b(context, R$attr.colorControlActivated), AppCompatDrawableManager.b);
                            return true;
                        }
                    }

                    /* JADX WARNING: Removed duplicated region for block: B:17:0x004b  */
                    /* JADX WARNING: Removed duplicated region for block: B:24:0x0066 A[RETURN] */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public boolean a(android.content.Context r7, int r8, android.graphics.drawable.Drawable r9) {
                        /*
                            r6 = this;
                            android.graphics.PorterDuff$Mode r0 = androidx.appcompat.widget.AppCompatDrawableManager.b
                            int[] r1 = r6.f372a
                            boolean r1 = r6.a((int[]) r1, (int) r8)
                            r2 = 16842801(0x1010031, float:2.3693695E-38)
                            r3 = -1
                            r4 = 0
                            r5 = 1
                            if (r1 == 0) goto L_0x0018
                            int r2 = androidx.appcompat.R$attr.colorControlNormal
                        L_0x0014:
                            r1 = r0
                            r8 = 1
                            r0 = -1
                            goto L_0x0049
                        L_0x0018:
                            int[] r1 = r6.c
                            boolean r1 = r6.a((int[]) r1, (int) r8)
                            if (r1 == 0) goto L_0x0023
                            int r2 = androidx.appcompat.R$attr.colorControlActivated
                            goto L_0x0014
                        L_0x0023:
                            int[] r1 = r6.d
                            boolean r1 = r6.a((int[]) r1, (int) r8)
                            if (r1 == 0) goto L_0x002e
                            android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.MULTIPLY
                            goto L_0x0014
                        L_0x002e:
                            int r1 = androidx.appcompat.R$drawable.abc_list_divider_mtrl_alpha
                            if (r8 != r1) goto L_0x0040
                            r2 = 16842800(0x1010030, float:2.3693693E-38)
                            r8 = 1109603123(0x42233333, float:40.8)
                            int r8 = java.lang.Math.round(r8)
                            r1 = r0
                            r0 = r8
                            r8 = 1
                            goto L_0x0049
                        L_0x0040:
                            int r1 = androidx.appcompat.R$drawable.abc_dialog_material_background
                            if (r8 != r1) goto L_0x0045
                            goto L_0x0014
                        L_0x0045:
                            r1 = r0
                            r8 = 0
                            r0 = -1
                            r2 = 0
                        L_0x0049:
                            if (r8 == 0) goto L_0x0066
                            boolean r8 = androidx.appcompat.widget.DrawableUtils.a(r9)
                            if (r8 == 0) goto L_0x0055
                            android.graphics.drawable.Drawable r9 = r9.mutate()
                        L_0x0055:
                            int r7 = androidx.appcompat.widget.ThemeUtils.b(r7, r2)
                            android.graphics.PorterDuffColorFilter r7 = androidx.appcompat.widget.AppCompatDrawableManager.a((int) r7, (android.graphics.PorterDuff.Mode) r1)
                            r9.setColorFilter(r7)
                            if (r0 == r3) goto L_0x0065
                            r9.setAlpha(r0)
                        L_0x0065:
                            return r5
                        L_0x0066:
                            return r4
                        */
                        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.AppCompatDrawableManager.AnonymousClass1.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean");
                    }

                    public PorterDuff.Mode a(int i) {
                        if (i == R$drawable.abc_switch_thumb_material) {
                            return PorterDuff.Mode.MULTIPLY;
                        }
                        return null;
                    }
                });
            }
        }
    }

    public synchronized Drawable a(Context context, int i) {
        return this.f371a.a(context, i);
    }

    /* access modifiers changed from: package-private */
    public synchronized Drawable a(Context context, int i, boolean z) {
        return this.f371a.a(context, i, z);
    }

    public synchronized void a(Context context) {
        this.f371a.a(context);
    }

    /* access modifiers changed from: package-private */
    public synchronized ColorStateList b(Context context, int i) {
        return this.f371a.b(context, i);
    }

    static void a(Drawable drawable, TintInfo tintInfo, int[] iArr) {
        ResourceManagerInternal.a(drawable, tintInfo, iArr);
    }

    public static synchronized PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
        PorterDuffColorFilter a2;
        synchronized (AppCompatDrawableManager.class) {
            a2 = ResourceManagerInternal.a(i, mode);
        }
        return a2;
    }
}
