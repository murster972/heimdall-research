package androidx.appcompat.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.core.view.ViewPropertyAnimatorCompat;

public interface DecorToolbar {
    ViewPropertyAnimatorCompat a(int i, long j);

    void a(int i);

    void a(Drawable drawable);

    void a(Menu menu, MenuPresenter.Callback callback);

    void a(MenuPresenter.Callback callback, MenuBuilder.Callback callback2);

    void a(ScrollingTabContainerView scrollingTabContainerView);

    void a(CharSequence charSequence);

    void a(boolean z);

    boolean a();

    void b(int i);

    void b(boolean z);

    boolean b();

    void c(int i);

    boolean c();

    void collapseActionView();

    void d(int i);

    boolean d();

    void e();

    void e(int i);

    boolean f();

    boolean g();

    Context getContext();

    CharSequence getTitle();

    Menu h();

    int i();

    ViewGroup j();

    void k();

    void l();

    int m();

    void n();

    void setIcon(int i);

    void setIcon(Drawable drawable);

    void setTitle(CharSequence charSequence);

    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);
}
