package androidx.appcompat.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import androidx.appcompat.R$styleable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.widget.ImageViewCompat;

public class AppCompatImageHelper {

    /* renamed from: a  reason: collision with root package name */
    private final ImageView f374a;
    private TintInfo b;
    private TintInfo c;
    private TintInfo d;

    public AppCompatImageHelper(ImageView imageView) {
        this.f374a = imageView;
    }

    private boolean e() {
        int i = Build.VERSION.SDK_INT;
        if (i <= 21) {
            return i == 21;
        }
        if (this.b != null) {
            return true;
        }
        return false;
    }

    public void a(AttributeSet attributeSet, int i) {
        int g;
        TintTypedArray a2 = TintTypedArray.a(this.f374a.getContext(), attributeSet, R$styleable.AppCompatImageView, i, 0);
        try {
            Drawable drawable = this.f374a.getDrawable();
            if (!(drawable != null || (g = a2.g(R$styleable.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = AppCompatResources.c(this.f374a.getContext(), g)) == null)) {
                this.f374a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                DrawableUtils.b(drawable);
            }
            if (a2.g(R$styleable.AppCompatImageView_tint)) {
                ImageViewCompat.a(this.f374a, a2.a(R$styleable.AppCompatImageView_tint));
            }
            if (a2.g(R$styleable.AppCompatImageView_tintMode)) {
                ImageViewCompat.a(this.f374a, DrawableUtils.a(a2.d(R$styleable.AppCompatImageView_tintMode, -1), (PorterDuff.Mode) null));
            }
        } finally {
            a2.a();
        }
    }

    /* access modifiers changed from: package-private */
    public ColorStateList b() {
        TintInfo tintInfo = this.c;
        if (tintInfo != null) {
            return tintInfo.f438a;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode c() {
        TintInfo tintInfo = this.c;
        if (tintInfo != null) {
            return tintInfo.b;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return Build.VERSION.SDK_INT < 21 || !(this.f374a.getBackground() instanceof RippleDrawable);
    }

    public void a(int i) {
        if (i != 0) {
            Drawable c2 = AppCompatResources.c(this.f374a.getContext(), i);
            if (c2 != null) {
                DrawableUtils.b(c2);
            }
            this.f374a.setImageDrawable(c2);
        } else {
            this.f374a.setImageDrawable((Drawable) null);
        }
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (this.c == null) {
            this.c = new TintInfo();
        }
        TintInfo tintInfo = this.c;
        tintInfo.f438a = colorStateList;
        tintInfo.d = true;
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        if (this.c == null) {
            this.c = new TintInfo();
        }
        TintInfo tintInfo = this.c;
        tintInfo.b = mode;
        tintInfo.c = true;
        a();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Drawable drawable = this.f374a.getDrawable();
        if (drawable != null) {
            DrawableUtils.b(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (!e() || !a(drawable)) {
            TintInfo tintInfo = this.c;
            if (tintInfo != null) {
                AppCompatDrawableManager.a(drawable, tintInfo, this.f374a.getDrawableState());
                return;
            }
            TintInfo tintInfo2 = this.b;
            if (tintInfo2 != null) {
                AppCompatDrawableManager.a(drawable, tintInfo2, this.f374a.getDrawableState());
            }
        }
    }

    private boolean a(Drawable drawable) {
        if (this.d == null) {
            this.d = new TintInfo();
        }
        TintInfo tintInfo = this.d;
        tintInfo.a();
        ColorStateList a2 = ImageViewCompat.a(this.f374a);
        if (a2 != null) {
            tintInfo.d = true;
            tintInfo.f438a = a2;
        }
        PorterDuff.Mode b2 = ImageViewCompat.b(this.f374a);
        if (b2 != null) {
            tintInfo.c = true;
            tintInfo.b = b2;
        }
        if (!tintInfo.d && !tintInfo.c) {
            return false;
        }
        AppCompatDrawableManager.a(drawable, tintInfo, this.f374a.getDrawableState());
        return true;
    }
}
