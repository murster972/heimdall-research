package androidx.appcompat.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import androidx.appcompat.R$styleable;
import androidx.core.view.ViewCompat;

class AppCompatBackgroundHelper {

    /* renamed from: a  reason: collision with root package name */
    private final View f367a;
    private final AppCompatDrawableManager b;
    private int c = -1;
    private TintInfo d;
    private TintInfo e;
    private TintInfo f;

    AppCompatBackgroundHelper(View view) {
        this.f367a = view;
        this.b = AppCompatDrawableManager.b();
    }

    private boolean d() {
        int i = Build.VERSION.SDK_INT;
        if (i <= 21) {
            return i == 21;
        }
        if (this.d != null) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        TintTypedArray a2 = TintTypedArray.a(this.f367a.getContext(), attributeSet, R$styleable.ViewBackgroundHelper, i, 0);
        try {
            if (a2.g(R$styleable.ViewBackgroundHelper_android_background)) {
                this.c = a2.g(R$styleable.ViewBackgroundHelper_android_background, -1);
                ColorStateList b2 = this.b.b(this.f367a.getContext(), this.c);
                if (b2 != null) {
                    a(b2);
                }
            }
            if (a2.g(R$styleable.ViewBackgroundHelper_backgroundTint)) {
                ViewCompat.a(this.f367a, a2.a(R$styleable.ViewBackgroundHelper_backgroundTint));
            }
            if (a2.g(R$styleable.ViewBackgroundHelper_backgroundTintMode)) {
                ViewCompat.a(this.f367a, DrawableUtils.a(a2.d(R$styleable.ViewBackgroundHelper_backgroundTintMode, -1), (PorterDuff.Mode) null));
            }
        } finally {
            a2.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        if (this.e == null) {
            this.e = new TintInfo();
        }
        TintInfo tintInfo = this.e;
        tintInfo.f438a = colorStateList;
        tintInfo.d = true;
        a();
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode c() {
        TintInfo tintInfo = this.e;
        if (tintInfo != null) {
            return tintInfo.b;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public ColorStateList b() {
        TintInfo tintInfo = this.e;
        if (tintInfo != null) {
            return tintInfo.f438a;
        }
        return null;
    }

    private boolean b(Drawable drawable) {
        if (this.f == null) {
            this.f = new TintInfo();
        }
        TintInfo tintInfo = this.f;
        tintInfo.a();
        ColorStateList e2 = ViewCompat.e(this.f367a);
        if (e2 != null) {
            tintInfo.d = true;
            tintInfo.f438a = e2;
        }
        PorterDuff.Mode f2 = ViewCompat.f(this.f367a);
        if (f2 != null) {
            tintInfo.c = true;
            tintInfo.b = f2;
        }
        if (!tintInfo.d && !tintInfo.c) {
            return false;
        }
        AppCompatDrawableManager.a(drawable, tintInfo, this.f367a.getDrawableState());
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.c = i;
        AppCompatDrawableManager appCompatDrawableManager = this.b;
        a(appCompatDrawableManager != null ? appCompatDrawableManager.b(this.f367a.getContext(), i) : null);
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable) {
        this.c = -1;
        a((ColorStateList) null);
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        if (this.e == null) {
            this.e = new TintInfo();
        }
        TintInfo tintInfo = this.e;
        tintInfo.b = mode;
        tintInfo.c = true;
        a();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Drawable background = this.f367a.getBackground();
        if (background == null) {
            return;
        }
        if (!d() || !b(background)) {
            TintInfo tintInfo = this.e;
            if (tintInfo != null) {
                AppCompatDrawableManager.a(background, tintInfo, this.f367a.getDrawableState());
                return;
            }
            TintInfo tintInfo2 = this.d;
            if (tintInfo2 != null) {
                AppCompatDrawableManager.a(background, tintInfo2, this.f367a.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.d == null) {
                this.d = new TintInfo();
            }
            TintInfo tintInfo = this.d;
            tintInfo.f438a = colorStateList;
            tintInfo.d = true;
        } else {
            this.d = null;
        }
        a();
    }
}
