package androidx.appcompat.widget;

import android.view.Menu;
import android.view.Window;
import androidx.appcompat.view.menu.MenuPresenter;

public interface DecorContentParent {
    void a(int i);

    void a(Menu menu, MenuPresenter.Callback callback);

    boolean a();

    boolean b();

    boolean c();

    boolean d();

    void e();

    boolean f();

    void g();

    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);
}
