package androidx.constraintlayout.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.util.Xml;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Constraints;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParserException;

public class ConstraintSet {
    private static final int[] b = {0, 4, 8};
    private static SparseIntArray c = new SparseIntArray();

    /* renamed from: a  reason: collision with root package name */
    private HashMap<Integer, Constraint> f509a = new HashMap<>();

    private static class Constraint {
        public int A;
        public int B;
        public int C;
        public int D;
        public int E;
        public int F;
        public int G;
        public int H;
        public int I;
        public int J;
        public int K;
        public int L;
        public int M;
        public int N;
        public int O;
        public int P;
        public float Q;
        public float R;
        public int S;
        public int T;
        public float U;
        public boolean V;
        public float W;
        public float X;
        public float Y;
        public float Z;

        /* renamed from: a  reason: collision with root package name */
        boolean f510a;
        public float a0;
        public int b;
        public float b0;
        public int c;
        public float c0;
        int d;
        public float d0;
        public int e;
        public float e0;
        public int f;
        public float f0;
        public float g;
        public float g0;
        public int h;
        public boolean h0;
        public int i;
        public boolean i0;
        public int j;
        public int j0;
        public int k;
        public int k0;
        public int l;
        public int l0;
        public int m;
        public int m0;
        public int n;
        public int n0;
        public int o;
        public int o0;
        public int p;
        public float p0;
        public int q;
        public float q0;
        public int r;
        public boolean r0;
        public int s;
        public int s0;
        public int t;
        public int t0;
        public float u;
        public int[] u0;
        public float v;
        public String v0;
        public String w;
        public int x;
        public int y;
        public float z;

        private Constraint() {
            this.f510a = false;
            this.e = -1;
            this.f = -1;
            this.g = -1.0f;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = -1;
            this.q = -1;
            this.r = -1;
            this.s = -1;
            this.t = -1;
            this.u = 0.5f;
            this.v = 0.5f;
            this.w = null;
            this.x = -1;
            this.y = 0;
            this.z = 0.0f;
            this.A = -1;
            this.B = -1;
            this.C = -1;
            this.D = -1;
            this.E = -1;
            this.F = -1;
            this.G = -1;
            this.H = -1;
            this.I = -1;
            this.J = 0;
            this.K = -1;
            this.L = -1;
            this.M = -1;
            this.N = -1;
            this.O = -1;
            this.P = -1;
            this.Q = 0.0f;
            this.R = 0.0f;
            this.S = 0;
            this.T = 0;
            this.U = 1.0f;
            this.V = false;
            this.W = 0.0f;
            this.X = 0.0f;
            this.Y = 0.0f;
            this.Z = 0.0f;
            this.a0 = 1.0f;
            this.b0 = 1.0f;
            this.c0 = Float.NaN;
            this.d0 = Float.NaN;
            this.e0 = 0.0f;
            this.f0 = 0.0f;
            this.g0 = 0.0f;
            this.h0 = false;
            this.i0 = false;
            this.j0 = 0;
            this.k0 = 0;
            this.l0 = -1;
            this.m0 = -1;
            this.n0 = -1;
            this.o0 = -1;
            this.p0 = 1.0f;
            this.q0 = 1.0f;
            this.r0 = false;
            this.s0 = -1;
            this.t0 = -1;
        }

        public Constraint clone() {
            Constraint constraint = new Constraint();
            constraint.f510a = this.f510a;
            constraint.b = this.b;
            constraint.c = this.c;
            constraint.e = this.e;
            constraint.f = this.f;
            constraint.g = this.g;
            constraint.h = this.h;
            constraint.i = this.i;
            constraint.j = this.j;
            constraint.k = this.k;
            constraint.l = this.l;
            constraint.m = this.m;
            constraint.n = this.n;
            constraint.o = this.o;
            constraint.p = this.p;
            constraint.q = this.q;
            constraint.r = this.r;
            constraint.s = this.s;
            constraint.t = this.t;
            constraint.u = this.u;
            constraint.v = this.v;
            constraint.w = this.w;
            constraint.A = this.A;
            constraint.B = this.B;
            constraint.u = this.u;
            constraint.u = this.u;
            constraint.u = this.u;
            constraint.u = this.u;
            constraint.u = this.u;
            constraint.C = this.C;
            constraint.D = this.D;
            constraint.E = this.E;
            constraint.F = this.F;
            constraint.G = this.G;
            constraint.H = this.H;
            constraint.I = this.I;
            constraint.J = this.J;
            constraint.K = this.K;
            constraint.L = this.L;
            constraint.M = this.M;
            constraint.N = this.N;
            constraint.O = this.O;
            constraint.P = this.P;
            constraint.Q = this.Q;
            constraint.R = this.R;
            constraint.S = this.S;
            constraint.T = this.T;
            constraint.U = this.U;
            constraint.V = this.V;
            constraint.W = this.W;
            constraint.X = this.X;
            constraint.Y = this.Y;
            constraint.Z = this.Z;
            constraint.a0 = this.a0;
            constraint.b0 = this.b0;
            constraint.c0 = this.c0;
            constraint.d0 = this.d0;
            constraint.e0 = this.e0;
            constraint.f0 = this.f0;
            constraint.g0 = this.g0;
            constraint.h0 = this.h0;
            constraint.i0 = this.i0;
            constraint.j0 = this.j0;
            constraint.k0 = this.k0;
            constraint.l0 = this.l0;
            constraint.m0 = this.m0;
            constraint.n0 = this.n0;
            constraint.o0 = this.o0;
            constraint.p0 = this.p0;
            constraint.q0 = this.q0;
            constraint.s0 = this.s0;
            constraint.t0 = this.t0;
            int[] iArr = this.u0;
            if (iArr != null) {
                constraint.u0 = Arrays.copyOf(iArr, iArr.length);
            }
            constraint.x = this.x;
            constraint.y = this.y;
            constraint.z = this.z;
            constraint.r0 = this.r0;
            return constraint;
        }

        /* access modifiers changed from: private */
        public void a(ConstraintHelper constraintHelper, int i2, Constraints.LayoutParams layoutParams) {
            a(i2, layoutParams);
            if (constraintHelper instanceof Barrier) {
                this.t0 = 1;
                Barrier barrier = (Barrier) constraintHelper;
                this.s0 = barrier.getType();
                this.u0 = barrier.getReferencedIds();
            }
        }

        /* access modifiers changed from: private */
        public void a(int i2, Constraints.LayoutParams layoutParams) {
            a(i2, (ConstraintLayout.LayoutParams) layoutParams);
            this.U = layoutParams.m0;
            this.X = layoutParams.p0;
            this.Y = layoutParams.q0;
            this.Z = layoutParams.r0;
            this.a0 = layoutParams.s0;
            this.b0 = layoutParams.t0;
            this.c0 = layoutParams.u0;
            this.d0 = layoutParams.v0;
            this.e0 = layoutParams.w0;
            this.f0 = layoutParams.x0;
            this.g0 = layoutParams.y0;
            this.W = layoutParams.o0;
            this.V = layoutParams.n0;
        }

        private void a(int i2, ConstraintLayout.LayoutParams layoutParams) {
            this.d = i2;
            this.h = layoutParams.d;
            this.i = layoutParams.e;
            this.j = layoutParams.f;
            this.k = layoutParams.g;
            this.l = layoutParams.h;
            this.m = layoutParams.i;
            this.n = layoutParams.j;
            this.o = layoutParams.k;
            this.p = layoutParams.l;
            this.q = layoutParams.p;
            this.r = layoutParams.q;
            this.s = layoutParams.r;
            this.t = layoutParams.s;
            this.u = layoutParams.z;
            this.v = layoutParams.A;
            this.w = layoutParams.B;
            this.x = layoutParams.m;
            this.y = layoutParams.n;
            this.z = layoutParams.o;
            this.A = layoutParams.P;
            this.B = layoutParams.Q;
            this.C = layoutParams.R;
            this.g = layoutParams.c;
            this.e = layoutParams.f507a;
            this.f = layoutParams.b;
            this.b = layoutParams.width;
            this.c = layoutParams.height;
            this.D = layoutParams.leftMargin;
            this.E = layoutParams.rightMargin;
            this.F = layoutParams.topMargin;
            this.G = layoutParams.bottomMargin;
            this.Q = layoutParams.E;
            this.R = layoutParams.D;
            this.T = layoutParams.G;
            this.S = layoutParams.F;
            boolean z2 = layoutParams.S;
            this.h0 = z2;
            this.i0 = layoutParams.T;
            this.j0 = layoutParams.H;
            this.k0 = layoutParams.I;
            this.h0 = z2;
            this.l0 = layoutParams.L;
            this.m0 = layoutParams.M;
            this.n0 = layoutParams.J;
            this.o0 = layoutParams.K;
            this.p0 = layoutParams.N;
            this.q0 = layoutParams.O;
            if (Build.VERSION.SDK_INT >= 17) {
                this.H = layoutParams.getMarginEnd();
                this.I = layoutParams.getMarginStart();
            }
        }

        public void a(ConstraintLayout.LayoutParams layoutParams) {
            layoutParams.d = this.h;
            layoutParams.e = this.i;
            layoutParams.f = this.j;
            layoutParams.g = this.k;
            layoutParams.h = this.l;
            layoutParams.i = this.m;
            layoutParams.j = this.n;
            layoutParams.k = this.o;
            layoutParams.l = this.p;
            layoutParams.p = this.q;
            layoutParams.q = this.r;
            layoutParams.r = this.s;
            layoutParams.s = this.t;
            layoutParams.leftMargin = this.D;
            layoutParams.rightMargin = this.E;
            layoutParams.topMargin = this.F;
            layoutParams.bottomMargin = this.G;
            layoutParams.x = this.P;
            layoutParams.y = this.O;
            layoutParams.z = this.u;
            layoutParams.A = this.v;
            layoutParams.m = this.x;
            layoutParams.n = this.y;
            layoutParams.o = this.z;
            layoutParams.B = this.w;
            layoutParams.P = this.A;
            layoutParams.Q = this.B;
            layoutParams.E = this.Q;
            layoutParams.D = this.R;
            layoutParams.G = this.T;
            layoutParams.F = this.S;
            layoutParams.S = this.h0;
            layoutParams.T = this.i0;
            layoutParams.H = this.j0;
            layoutParams.I = this.k0;
            layoutParams.L = this.l0;
            layoutParams.M = this.m0;
            layoutParams.J = this.n0;
            layoutParams.K = this.o0;
            layoutParams.N = this.p0;
            layoutParams.O = this.q0;
            layoutParams.R = this.C;
            layoutParams.c = this.g;
            layoutParams.f507a = this.e;
            layoutParams.b = this.f;
            layoutParams.width = this.b;
            layoutParams.height = this.c;
            if (Build.VERSION.SDK_INT >= 17) {
                layoutParams.setMarginStart(this.I);
                layoutParams.setMarginEnd(this.H);
            }
            layoutParams.a();
        }
    }

    static {
        c.append(R$styleable.ConstraintSet_layout_constraintLeft_toLeftOf, 25);
        c.append(R$styleable.ConstraintSet_layout_constraintLeft_toRightOf, 26);
        c.append(R$styleable.ConstraintSet_layout_constraintRight_toLeftOf, 29);
        c.append(R$styleable.ConstraintSet_layout_constraintRight_toRightOf, 30);
        c.append(R$styleable.ConstraintSet_layout_constraintTop_toTopOf, 36);
        c.append(R$styleable.ConstraintSet_layout_constraintTop_toBottomOf, 35);
        c.append(R$styleable.ConstraintSet_layout_constraintBottom_toTopOf, 4);
        c.append(R$styleable.ConstraintSet_layout_constraintBottom_toBottomOf, 3);
        c.append(R$styleable.ConstraintSet_layout_constraintBaseline_toBaselineOf, 1);
        c.append(R$styleable.ConstraintSet_layout_editor_absoluteX, 6);
        c.append(R$styleable.ConstraintSet_layout_editor_absoluteY, 7);
        c.append(R$styleable.ConstraintSet_layout_constraintGuide_begin, 17);
        c.append(R$styleable.ConstraintSet_layout_constraintGuide_end, 18);
        c.append(R$styleable.ConstraintSet_layout_constraintGuide_percent, 19);
        c.append(R$styleable.ConstraintSet_android_orientation, 27);
        c.append(R$styleable.ConstraintSet_layout_constraintStart_toEndOf, 32);
        c.append(R$styleable.ConstraintSet_layout_constraintStart_toStartOf, 33);
        c.append(R$styleable.ConstraintSet_layout_constraintEnd_toStartOf, 10);
        c.append(R$styleable.ConstraintSet_layout_constraintEnd_toEndOf, 9);
        c.append(R$styleable.ConstraintSet_layout_goneMarginLeft, 13);
        c.append(R$styleable.ConstraintSet_layout_goneMarginTop, 16);
        c.append(R$styleable.ConstraintSet_layout_goneMarginRight, 14);
        c.append(R$styleable.ConstraintSet_layout_goneMarginBottom, 11);
        c.append(R$styleable.ConstraintSet_layout_goneMarginStart, 15);
        c.append(R$styleable.ConstraintSet_layout_goneMarginEnd, 12);
        c.append(R$styleable.ConstraintSet_layout_constraintVertical_weight, 40);
        c.append(R$styleable.ConstraintSet_layout_constraintHorizontal_weight, 39);
        c.append(R$styleable.ConstraintSet_layout_constraintHorizontal_chainStyle, 41);
        c.append(R$styleable.ConstraintSet_layout_constraintVertical_chainStyle, 42);
        c.append(R$styleable.ConstraintSet_layout_constraintHorizontal_bias, 20);
        c.append(R$styleable.ConstraintSet_layout_constraintVertical_bias, 37);
        c.append(R$styleable.ConstraintSet_layout_constraintDimensionRatio, 5);
        c.append(R$styleable.ConstraintSet_layout_constraintLeft_creator, 75);
        c.append(R$styleable.ConstraintSet_layout_constraintTop_creator, 75);
        c.append(R$styleable.ConstraintSet_layout_constraintRight_creator, 75);
        c.append(R$styleable.ConstraintSet_layout_constraintBottom_creator, 75);
        c.append(R$styleable.ConstraintSet_layout_constraintBaseline_creator, 75);
        c.append(R$styleable.ConstraintSet_android_layout_marginLeft, 24);
        c.append(R$styleable.ConstraintSet_android_layout_marginRight, 28);
        c.append(R$styleable.ConstraintSet_android_layout_marginStart, 31);
        c.append(R$styleable.ConstraintSet_android_layout_marginEnd, 8);
        c.append(R$styleable.ConstraintSet_android_layout_marginTop, 34);
        c.append(R$styleable.ConstraintSet_android_layout_marginBottom, 2);
        c.append(R$styleable.ConstraintSet_android_layout_width, 23);
        c.append(R$styleable.ConstraintSet_android_layout_height, 21);
        c.append(R$styleable.ConstraintSet_android_visibility, 22);
        c.append(R$styleable.ConstraintSet_android_alpha, 43);
        c.append(R$styleable.ConstraintSet_android_elevation, 44);
        c.append(R$styleable.ConstraintSet_android_rotationX, 45);
        c.append(R$styleable.ConstraintSet_android_rotationY, 46);
        c.append(R$styleable.ConstraintSet_android_rotation, 60);
        c.append(R$styleable.ConstraintSet_android_scaleX, 47);
        c.append(R$styleable.ConstraintSet_android_scaleY, 48);
        c.append(R$styleable.ConstraintSet_android_transformPivotX, 49);
        c.append(R$styleable.ConstraintSet_android_transformPivotY, 50);
        c.append(R$styleable.ConstraintSet_android_translationX, 51);
        c.append(R$styleable.ConstraintSet_android_translationY, 52);
        c.append(R$styleable.ConstraintSet_android_translationZ, 53);
        c.append(R$styleable.ConstraintSet_layout_constraintWidth_default, 54);
        c.append(R$styleable.ConstraintSet_layout_constraintHeight_default, 55);
        c.append(R$styleable.ConstraintSet_layout_constraintWidth_max, 56);
        c.append(R$styleable.ConstraintSet_layout_constraintHeight_max, 57);
        c.append(R$styleable.ConstraintSet_layout_constraintWidth_min, 58);
        c.append(R$styleable.ConstraintSet_layout_constraintHeight_min, 59);
        c.append(R$styleable.ConstraintSet_layout_constraintCircle, 61);
        c.append(R$styleable.ConstraintSet_layout_constraintCircleRadius, 62);
        c.append(R$styleable.ConstraintSet_layout_constraintCircleAngle, 63);
        c.append(R$styleable.ConstraintSet_android_id, 38);
        c.append(R$styleable.ConstraintSet_layout_constraintWidth_percent, 69);
        c.append(R$styleable.ConstraintSet_layout_constraintHeight_percent, 70);
        c.append(R$styleable.ConstraintSet_chainUseRtl, 71);
        c.append(R$styleable.ConstraintSet_barrierDirection, 72);
        c.append(R$styleable.ConstraintSet_constraint_referenced_ids, 73);
        c.append(R$styleable.ConstraintSet_barrierAllowsGoneWidgets, 74);
    }

    public void a(Constraints constraints) {
        int childCount = constraints.getChildCount();
        this.f509a.clear();
        int i = 0;
        while (i < childCount) {
            View childAt = constraints.getChildAt(i);
            Constraints.LayoutParams layoutParams = (Constraints.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (id != -1) {
                if (!this.f509a.containsKey(Integer.valueOf(id))) {
                    this.f509a.put(Integer.valueOf(id), new Constraint());
                }
                Constraint constraint = this.f509a.get(Integer.valueOf(id));
                if (childAt instanceof ConstraintHelper) {
                    constraint.a((ConstraintHelper) childAt, id, layoutParams);
                }
                constraint.a(id, layoutParams);
                i++;
            } else {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ConstraintLayout constraintLayout) {
        int childCount = constraintLayout.getChildCount();
        HashSet hashSet = new HashSet(this.f509a.keySet());
        int i = 0;
        while (i < childCount) {
            View childAt = constraintLayout.getChildAt(i);
            int id = childAt.getId();
            if (id != -1) {
                if (this.f509a.containsKey(Integer.valueOf(id))) {
                    hashSet.remove(Integer.valueOf(id));
                    Constraint constraint = this.f509a.get(Integer.valueOf(id));
                    if (childAt instanceof Barrier) {
                        constraint.t0 = 1;
                    }
                    int i2 = constraint.t0;
                    if (i2 != -1 && i2 == 1) {
                        Barrier barrier = (Barrier) childAt;
                        barrier.setId(id);
                        barrier.setType(constraint.s0);
                        barrier.setAllowsGoneWidget(constraint.r0);
                        int[] iArr = constraint.u0;
                        if (iArr != null) {
                            barrier.setReferencedIds(iArr);
                        } else {
                            String str = constraint.v0;
                            if (str != null) {
                                constraint.u0 = a((View) barrier, str);
                                barrier.setReferencedIds(constraint.u0);
                            }
                        }
                    }
                    ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) childAt.getLayoutParams();
                    constraint.a(layoutParams);
                    childAt.setLayoutParams(layoutParams);
                    childAt.setVisibility(constraint.J);
                    if (Build.VERSION.SDK_INT >= 17) {
                        childAt.setAlpha(constraint.U);
                        childAt.setRotation(constraint.X);
                        childAt.setRotationX(constraint.Y);
                        childAt.setRotationY(constraint.Z);
                        childAt.setScaleX(constraint.a0);
                        childAt.setScaleY(constraint.b0);
                        if (!Float.isNaN(constraint.c0)) {
                            childAt.setPivotX(constraint.c0);
                        }
                        if (!Float.isNaN(constraint.d0)) {
                            childAt.setPivotY(constraint.d0);
                        }
                        childAt.setTranslationX(constraint.e0);
                        childAt.setTranslationY(constraint.f0);
                        if (Build.VERSION.SDK_INT >= 21) {
                            childAt.setTranslationZ(constraint.g0);
                            if (constraint.V) {
                                childAt.setElevation(constraint.W);
                            }
                        }
                    }
                }
                i++;
            } else {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
        Iterator it2 = hashSet.iterator();
        while (it2.hasNext()) {
            Integer num = (Integer) it2.next();
            Constraint constraint2 = this.f509a.get(num);
            int i3 = constraint2.t0;
            if (i3 != -1 && i3 == 1) {
                Barrier barrier2 = new Barrier(constraintLayout.getContext());
                barrier2.setId(num.intValue());
                int[] iArr2 = constraint2.u0;
                if (iArr2 != null) {
                    barrier2.setReferencedIds(iArr2);
                } else {
                    String str2 = constraint2.v0;
                    if (str2 != null) {
                        constraint2.u0 = a((View) barrier2, str2);
                        barrier2.setReferencedIds(constraint2.u0);
                    }
                }
                barrier2.setType(constraint2.s0);
                ConstraintLayout.LayoutParams generateDefaultLayoutParams = constraintLayout.generateDefaultLayoutParams();
                barrier2.a();
                constraint2.a(generateDefaultLayoutParams);
                constraintLayout.addView(barrier2, generateDefaultLayoutParams);
            }
            if (constraint2.f510a) {
                Guideline guideline = new Guideline(constraintLayout.getContext());
                guideline.setId(num.intValue());
                ConstraintLayout.LayoutParams generateDefaultLayoutParams2 = constraintLayout.generateDefaultLayoutParams();
                constraint2.a(generateDefaultLayoutParams2);
                constraintLayout.addView(guideline, generateDefaultLayoutParams2);
            }
        }
    }

    public void a(Context context, int i) {
        XmlResourceParser xml = context.getResources().getXml(i);
        try {
            for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                if (eventType == 0) {
                    xml.getName();
                } else if (eventType == 2) {
                    String name = xml.getName();
                    Constraint a2 = a(context, Xml.asAttributeSet(xml));
                    if (name.equalsIgnoreCase("Guideline")) {
                        a2.f510a = true;
                    }
                    this.f509a.put(Integer.valueOf(a2.d), a2);
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private static int a(TypedArray typedArray, int i, int i2) {
        int resourceId = typedArray.getResourceId(i, i2);
        return resourceId == -1 ? typedArray.getInt(i, -1) : resourceId;
    }

    private Constraint a(Context context, AttributeSet attributeSet) {
        Constraint constraint = new Constraint();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.ConstraintSet);
        a(constraint, obtainStyledAttributes);
        obtainStyledAttributes.recycle();
        return constraint;
    }

    private void a(Constraint constraint, TypedArray typedArray) {
        int indexCount = typedArray.getIndexCount();
        for (int i = 0; i < indexCount; i++) {
            int index = typedArray.getIndex(i);
            int i2 = c.get(index);
            switch (i2) {
                case 1:
                    constraint.p = a(typedArray, index, constraint.p);
                    break;
                case 2:
                    constraint.G = typedArray.getDimensionPixelSize(index, constraint.G);
                    break;
                case 3:
                    constraint.o = a(typedArray, index, constraint.o);
                    break;
                case 4:
                    constraint.n = a(typedArray, index, constraint.n);
                    break;
                case 5:
                    constraint.w = typedArray.getString(index);
                    break;
                case 6:
                    constraint.A = typedArray.getDimensionPixelOffset(index, constraint.A);
                    break;
                case 7:
                    constraint.B = typedArray.getDimensionPixelOffset(index, constraint.B);
                    break;
                case 8:
                    constraint.H = typedArray.getDimensionPixelSize(index, constraint.H);
                    break;
                case 9:
                    constraint.t = a(typedArray, index, constraint.t);
                    break;
                case 10:
                    constraint.s = a(typedArray, index, constraint.s);
                    break;
                case 11:
                    constraint.N = typedArray.getDimensionPixelSize(index, constraint.N);
                    break;
                case 12:
                    constraint.O = typedArray.getDimensionPixelSize(index, constraint.O);
                    break;
                case 13:
                    constraint.K = typedArray.getDimensionPixelSize(index, constraint.K);
                    break;
                case 14:
                    constraint.M = typedArray.getDimensionPixelSize(index, constraint.M);
                    break;
                case 15:
                    constraint.P = typedArray.getDimensionPixelSize(index, constraint.P);
                    break;
                case 16:
                    constraint.L = typedArray.getDimensionPixelSize(index, constraint.L);
                    break;
                case 17:
                    constraint.e = typedArray.getDimensionPixelOffset(index, constraint.e);
                    break;
                case 18:
                    constraint.f = typedArray.getDimensionPixelOffset(index, constraint.f);
                    break;
                case 19:
                    constraint.g = typedArray.getFloat(index, constraint.g);
                    break;
                case 20:
                    constraint.u = typedArray.getFloat(index, constraint.u);
                    break;
                case 21:
                    constraint.c = typedArray.getLayoutDimension(index, constraint.c);
                    break;
                case 22:
                    constraint.J = typedArray.getInt(index, constraint.J);
                    constraint.J = b[constraint.J];
                    break;
                case 23:
                    constraint.b = typedArray.getLayoutDimension(index, constraint.b);
                    break;
                case 24:
                    constraint.D = typedArray.getDimensionPixelSize(index, constraint.D);
                    break;
                case 25:
                    constraint.h = a(typedArray, index, constraint.h);
                    break;
                case 26:
                    constraint.i = a(typedArray, index, constraint.i);
                    break;
                case 27:
                    constraint.C = typedArray.getInt(index, constraint.C);
                    break;
                case 28:
                    constraint.E = typedArray.getDimensionPixelSize(index, constraint.E);
                    break;
                case 29:
                    constraint.j = a(typedArray, index, constraint.j);
                    break;
                case 30:
                    constraint.k = a(typedArray, index, constraint.k);
                    break;
                case 31:
                    constraint.I = typedArray.getDimensionPixelSize(index, constraint.I);
                    break;
                case 32:
                    constraint.q = a(typedArray, index, constraint.q);
                    break;
                case 33:
                    constraint.r = a(typedArray, index, constraint.r);
                    break;
                case 34:
                    constraint.F = typedArray.getDimensionPixelSize(index, constraint.F);
                    break;
                case 35:
                    constraint.m = a(typedArray, index, constraint.m);
                    break;
                case 36:
                    constraint.l = a(typedArray, index, constraint.l);
                    break;
                case 37:
                    constraint.v = typedArray.getFloat(index, constraint.v);
                    break;
                case 38:
                    constraint.d = typedArray.getResourceId(index, constraint.d);
                    break;
                case 39:
                    constraint.R = typedArray.getFloat(index, constraint.R);
                    break;
                case 40:
                    constraint.Q = typedArray.getFloat(index, constraint.Q);
                    break;
                case 41:
                    constraint.S = typedArray.getInt(index, constraint.S);
                    break;
                case 42:
                    constraint.T = typedArray.getInt(index, constraint.T);
                    break;
                case 43:
                    constraint.U = typedArray.getFloat(index, constraint.U);
                    break;
                case 44:
                    constraint.V = true;
                    constraint.W = typedArray.getDimension(index, constraint.W);
                    break;
                case 45:
                    constraint.Y = typedArray.getFloat(index, constraint.Y);
                    break;
                case 46:
                    constraint.Z = typedArray.getFloat(index, constraint.Z);
                    break;
                case 47:
                    constraint.a0 = typedArray.getFloat(index, constraint.a0);
                    break;
                case 48:
                    constraint.b0 = typedArray.getFloat(index, constraint.b0);
                    break;
                case 49:
                    constraint.c0 = typedArray.getFloat(index, constraint.c0);
                    break;
                case 50:
                    constraint.d0 = typedArray.getFloat(index, constraint.d0);
                    break;
                case 51:
                    constraint.e0 = typedArray.getDimension(index, constraint.e0);
                    break;
                case 52:
                    constraint.f0 = typedArray.getDimension(index, constraint.f0);
                    break;
                case 53:
                    constraint.g0 = typedArray.getDimension(index, constraint.g0);
                    break;
                default:
                    switch (i2) {
                        case 60:
                            constraint.X = typedArray.getFloat(index, constraint.X);
                            break;
                        case 61:
                            constraint.x = a(typedArray, index, constraint.x);
                            break;
                        case 62:
                            constraint.y = typedArray.getDimensionPixelSize(index, constraint.y);
                            break;
                        case 63:
                            constraint.z = typedArray.getFloat(index, constraint.z);
                            break;
                        default:
                            switch (i2) {
                                case 69:
                                    constraint.p0 = typedArray.getFloat(index, 1.0f);
                                    break;
                                case 70:
                                    constraint.q0 = typedArray.getFloat(index, 1.0f);
                                    break;
                                case 71:
                                    Log.e("ConstraintSet", "CURRENTLY UNSUPPORTED");
                                    break;
                                case 72:
                                    constraint.s0 = typedArray.getInt(index, constraint.s0);
                                    break;
                                case 73:
                                    constraint.v0 = typedArray.getString(index);
                                    break;
                                case 74:
                                    constraint.r0 = typedArray.getBoolean(index, constraint.r0);
                                    break;
                                case 75:
                                    Log.w("ConstraintSet", "unused attribute 0x" + Integer.toHexString(index) + "   " + c.get(index));
                                    break;
                                default:
                                    Log.w("ConstraintSet", "Unknown attribute 0x" + Integer.toHexString(index) + "   " + c.get(index));
                                    break;
                            }
                    }
            }
        }
    }

    private int[] a(View view, String str) {
        int i;
        Object a2;
        String[] split = str.split(",");
        Context context = view.getContext();
        int[] iArr = new int[split.length];
        int i2 = 0;
        int i3 = 0;
        while (i2 < split.length) {
            String trim = split[i2].trim();
            try {
                i = R$id.class.getField(trim).getInt((Object) null);
            } catch (Exception unused) {
                i = 0;
            }
            if (i == 0) {
                i = context.getResources().getIdentifier(trim, "id", context.getPackageName());
            }
            if (i == 0 && view.isInEditMode() && (view.getParent() instanceof ConstraintLayout) && (a2 = ((ConstraintLayout) view.getParent()).a(0, (Object) trim)) != null && (a2 instanceof Integer)) {
                i = ((Integer) a2).intValue();
            }
            iArr[i3] = i;
            i2++;
            i3++;
        }
        return i3 != split.length ? Arrays.copyOf(iArr, i3) : iArr;
    }
}
