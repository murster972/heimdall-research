package androidx.constraintlayout.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.solver.Metrics;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer;
import androidx.constraintlayout.solver.widgets.Guideline;
import java.util.ArrayList;
import java.util.HashMap;

public class ConstraintLayout extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    SparseArray<View> f506a = new SparseArray<>();
    private ArrayList<ConstraintHelper> b = new ArrayList<>(4);
    private final ArrayList<ConstraintWidget> c = new ArrayList<>(100);
    ConstraintWidgetContainer d = new ConstraintWidgetContainer();
    private int e = 0;
    private int f = 0;
    private int g = Integer.MAX_VALUE;
    private int h = Integer.MAX_VALUE;
    private boolean i = true;
    private int j = 7;
    private ConstraintSet k = null;
    private int l = -1;
    private HashMap<String, Integer> m = new HashMap<>();
    private int n = -1;
    private int o = -1;
    private Metrics p;

    public ConstraintLayout(Context context) {
        super(context);
        a((AttributeSet) null);
    }

    private void b() {
        int childCount = getChildCount();
        boolean z = false;
        int i2 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            } else if (getChildAt(i2).isLayoutRequested()) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            this.c.clear();
            a();
        }
    }

    private void c() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt instanceof Placeholder) {
                ((Placeholder) childAt).a(this);
            }
        }
        int size = this.b.size();
        if (size > 0) {
            for (int i3 = 0; i3 < size; i3++) {
                this.b.get(i3).b(this);
            }
        }
    }

    public void a(int i2, Object obj, Object obj2) {
        if (i2 == 0 && (obj instanceof String) && (obj2 instanceof Integer)) {
            if (this.m == null) {
                this.m = new HashMap<>();
            }
            String str = (String) obj;
            int indexOf = str.indexOf("/");
            if (indexOf != -1) {
                str = str.substring(indexOf + 1);
            }
            this.m.put(str, Integer.valueOf(((Integer) obj2).intValue()));
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i2, layoutParams);
        if (Build.VERSION.SDK_INT < 14) {
            onViewAdded(view);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void dispatchDraw(Canvas canvas) {
        Object tag;
        super.dispatchDraw(canvas);
        if (isInEditMode()) {
            int childCount = getChildCount();
            float width = (float) getWidth();
            float height = (float) getHeight();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = getChildAt(i2);
                if (!(childAt.getVisibility() == 8 || (tag = childAt.getTag()) == null || !(tag instanceof String))) {
                    String[] split = ((String) tag).split(",");
                    if (split.length == 4) {
                        int parseInt = Integer.parseInt(split[0]);
                        int parseInt2 = Integer.parseInt(split[1]);
                        int parseInt3 = Integer.parseInt(split[2]);
                        int i3 = (int) ((((float) parseInt) / 1080.0f) * width);
                        int i4 = (int) ((((float) parseInt2) / 1920.0f) * height);
                        Paint paint = new Paint();
                        paint.setColor(-65536);
                        float f2 = (float) i3;
                        float f3 = (float) (i3 + ((int) ((((float) parseInt3) / 1080.0f) * width)));
                        Canvas canvas2 = canvas;
                        float f4 = (float) i4;
                        float f5 = f2;
                        float f6 = f2;
                        float f7 = f4;
                        Paint paint2 = paint;
                        float f8 = f3;
                        Paint paint3 = paint2;
                        canvas2.drawLine(f5, f7, f8, f4, paint3);
                        float parseInt4 = (float) (i4 + ((int) ((((float) Integer.parseInt(split[3])) / 1920.0f) * height)));
                        float f9 = f3;
                        float f10 = parseInt4;
                        canvas2.drawLine(f9, f7, f8, f10, paint3);
                        float f11 = parseInt4;
                        float f12 = f6;
                        canvas2.drawLine(f9, f11, f12, f10, paint3);
                        float f13 = f6;
                        canvas2.drawLine(f13, f11, f12, f4, paint3);
                        Paint paint4 = paint2;
                        paint4.setColor(-16711936);
                        Paint paint5 = paint4;
                        float f14 = f3;
                        Paint paint6 = paint5;
                        canvas2.drawLine(f13, f4, f14, parseInt4, paint6);
                        canvas2.drawLine(f13, parseInt4, f14, f4, paint6);
                    }
                }
            }
        }
    }

    public int getMaxHeight() {
        return this.h;
    }

    public int getMaxWidth() {
        return this.g;
    }

    public int getMinHeight() {
        return this.f;
    }

    public int getMinWidth() {
        return this.e;
    }

    public int getOptimizationLevel() {
        return this.d.M();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        View content;
        int childCount = getChildCount();
        boolean isInEditMode = isInEditMode();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            ConstraintWidget constraintWidget = layoutParams.k0;
            if ((childAt.getVisibility() != 8 || layoutParams.X || layoutParams.Y || isInEditMode) && !layoutParams.Z) {
                int g2 = constraintWidget.g();
                int h2 = constraintWidget.h();
                int s = constraintWidget.s() + g2;
                int i7 = constraintWidget.i() + h2;
                childAt.layout(g2, h2, s, i7);
                if ((childAt instanceof Placeholder) && (content = ((Placeholder) childAt).getContent()) != null) {
                    content.setVisibility(0);
                    content.layout(g2, h2, s, i7);
                }
            }
        }
        int size = this.b.size();
        if (size > 0) {
            for (int i8 = 0; i8 < size; i8++) {
                this.b.get(i8).a(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0358  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x036d  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x03a6  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0138  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r25, int r26) {
        /*
            r24 = this;
            r0 = r24
            r1 = r25
            r2 = r26
            int r3 = android.view.View.MeasureSpec.getMode(r25)
            int r4 = android.view.View.MeasureSpec.getSize(r25)
            int r5 = android.view.View.MeasureSpec.getMode(r26)
            int r6 = android.view.View.MeasureSpec.getSize(r26)
            int r7 = r24.getPaddingLeft()
            int r8 = r24.getPaddingTop()
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r9 = r0.d
            r9.r(r7)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r9 = r0.d
            r9.s(r8)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r9 = r0.d
            int r10 = r0.g
            r9.j(r10)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r9 = r0.d
            int r10 = r0.h
            r9.i(r10)
            int r9 = android.os.Build.VERSION.SDK_INT
            r10 = 0
            r11 = 1
            r12 = 17
            if (r9 < r12) goto L_0x004c
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r9 = r0.d
            int r12 = r24.getLayoutDirection()
            if (r12 != r11) goto L_0x0048
            r12 = 1
            goto L_0x0049
        L_0x0048:
            r12 = 0
        L_0x0049:
            r9.c(r12)
        L_0x004c:
            r24.c(r25, r26)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r9 = r0.d
            int r9 = r9.s()
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r12 = r0.d
            int r12 = r12.i()
            boolean r13 = r0.i
            if (r13 == 0) goto L_0x0066
            r0.i = r10
            r24.b()
            r13 = 1
            goto L_0x0067
        L_0x0066:
            r13 = 0
        L_0x0067:
            int r14 = r0.j
            r15 = 8
            r14 = r14 & r15
            if (r14 != r15) goto L_0x0070
            r14 = 1
            goto L_0x0071
        L_0x0070:
            r14 = 0
        L_0x0071:
            if (r14 == 0) goto L_0x0081
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r15 = r0.d
            r15.T()
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r15 = r0.d
            r15.f(r9, r12)
            r24.b(r25, r26)
            goto L_0x0084
        L_0x0081:
            r24.a((int) r25, (int) r26)
        L_0x0084:
            r24.c()
            int r15 = r24.getChildCount()
            if (r15 <= 0) goto L_0x0094
            if (r13 == 0) goto L_0x0094
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r13 = r0.d
            androidx.constraintlayout.solver.widgets.Analyzer.a((androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer) r13)
        L_0x0094:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r13 = r0.d
            boolean r15 = r13.x0
            if (r15 == 0) goto L_0x00c6
            boolean r15 = r13.y0
            r11 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r15 == 0) goto L_0x00b0
            if (r3 != r11) goto L_0x00b0
            int r15 = r13.A0
            if (r15 >= r4) goto L_0x00a9
            r13.o(r15)
        L_0x00a9:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r13 = r0.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r15 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            r13.a((androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour) r15)
        L_0x00b0:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r13 = r0.d
            boolean r15 = r13.z0
            if (r15 == 0) goto L_0x00c6
            if (r5 != r11) goto L_0x00c6
            int r11 = r13.B0
            if (r11 >= r6) goto L_0x00bf
            r13.g(r11)
        L_0x00bf:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r11 = r0.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r13 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            r11.b((androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour) r13)
        L_0x00c6:
            int r11 = r0.j
            r13 = 32
            r11 = r11 & r13
            r15 = 1073741824(0x40000000, float:2.0)
            if (r11 != r13) goto L_0x011a
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r11 = r0.d
            int r11 = r11.s()
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r13 = r0.d
            int r13 = r13.i()
            int r10 = r0.n
            if (r10 == r11) goto L_0x00e9
            if (r3 != r15) goto L_0x00e9
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r3 = r0.d
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r3 = r3.w0
            r10 = 0
            androidx.constraintlayout.solver.widgets.Analyzer.a((java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup>) r3, (int) r10, (int) r11)
        L_0x00e9:
            int r3 = r0.o
            if (r3 == r13) goto L_0x00f7
            if (r5 != r15) goto L_0x00f7
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r3 = r0.d
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r3 = r3.w0
            r5 = 1
            androidx.constraintlayout.solver.widgets.Analyzer.a((java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup>) r3, (int) r5, (int) r13)
        L_0x00f7:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r3 = r0.d
            boolean r5 = r3.y0
            if (r5 == 0) goto L_0x0108
            int r5 = r3.A0
            if (r5 <= r4) goto L_0x0108
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r3 = r3.w0
            r10 = 0
            androidx.constraintlayout.solver.widgets.Analyzer.a((java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup>) r3, (int) r10, (int) r4)
            goto L_0x0109
        L_0x0108:
            r10 = 0
        L_0x0109:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r3 = r0.d
            boolean r4 = r3.z0
            if (r4 == 0) goto L_0x011a
            int r4 = r3.B0
            if (r4 <= r6) goto L_0x011a
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r3 = r3.w0
            r4 = 1
            androidx.constraintlayout.solver.widgets.Analyzer.a((java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup>) r3, (int) r4, (int) r6)
            goto L_0x011b
        L_0x011a:
            r4 = 1
        L_0x011b:
            int r3 = r24.getChildCount()
            if (r3 <= 0) goto L_0x0126
            java.lang.String r3 = "First pass"
            r0.a((java.lang.String) r3)
        L_0x0126:
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r3 = r0.c
            int r3 = r3.size()
            int r5 = r24.getPaddingBottom()
            int r8 = r8 + r5
            int r5 = r24.getPaddingRight()
            int r7 = r7 + r5
            if (r3 <= 0) goto L_0x0358
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r6 = r0.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r6 = r6.j()
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r11 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r6 != r11) goto L_0x0144
            r6 = 1
            goto L_0x0145
        L_0x0144:
            r6 = 0
        L_0x0145:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r11 = r0.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r11 = r11.q()
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r13 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r11 != r13) goto L_0x0151
            r11 = 1
            goto L_0x0152
        L_0x0151:
            r11 = 0
        L_0x0152:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r13 = r0.d
            int r13 = r13.s()
            int r4 = r0.e
            int r4 = java.lang.Math.max(r13, r4)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r13 = r0.d
            int r13 = r13.i()
            int r10 = r0.f
            int r10 = java.lang.Math.max(r13, r10)
            r13 = r4
            r5 = r10
            r4 = 0
            r10 = 0
            r17 = 0
        L_0x0170:
            r18 = 1
            if (r4 >= r3) goto L_0x02b1
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r15 = r0.c
            java.lang.Object r15 = r15.get(r4)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r15 = (androidx.constraintlayout.solver.widgets.ConstraintWidget) r15
            java.lang.Object r20 = r15.e()
            r21 = r3
            r3 = r20
            android.view.View r3 = (android.view.View) r3
            if (r3 != 0) goto L_0x0190
            r20 = r9
            r23 = r10
            r22 = r12
            goto L_0x029d
        L_0x0190:
            android.view.ViewGroup$LayoutParams r20 = r3.getLayoutParams()
            r22 = r12
            r12 = r20
            androidx.constraintlayout.widget.ConstraintLayout$LayoutParams r12 = (androidx.constraintlayout.widget.ConstraintLayout.LayoutParams) r12
            r20 = r9
            boolean r9 = r12.Y
            if (r9 != 0) goto L_0x029b
            boolean r9 = r12.X
            if (r9 == 0) goto L_0x01a6
            goto L_0x029b
        L_0x01a6:
            int r9 = r3.getVisibility()
            r23 = r10
            r10 = 8
            if (r9 != r10) goto L_0x01b2
        L_0x01b0:
            goto L_0x029d
        L_0x01b2:
            if (r14 == 0) goto L_0x01c9
            androidx.constraintlayout.solver.widgets.ResolutionDimension r9 = r15.m()
            boolean r9 = r9.c()
            if (r9 == 0) goto L_0x01c9
            androidx.constraintlayout.solver.widgets.ResolutionDimension r9 = r15.l()
            boolean r9 = r9.c()
            if (r9 == 0) goto L_0x01c9
            goto L_0x01b0
        L_0x01c9:
            int r9 = r12.width
            r10 = -2
            if (r9 != r10) goto L_0x01d9
            boolean r9 = r12.U
            if (r9 == 0) goto L_0x01d9
            int r9 = r12.width
            int r9 = android.view.ViewGroup.getChildMeasureSpec(r1, r7, r9)
            goto L_0x01e3
        L_0x01d9:
            int r9 = r15.s()
            r10 = 1073741824(0x40000000, float:2.0)
            int r9 = android.view.View.MeasureSpec.makeMeasureSpec(r9, r10)
        L_0x01e3:
            int r10 = r12.height
            r1 = -2
            if (r10 != r1) goto L_0x01f3
            boolean r1 = r12.V
            if (r1 == 0) goto L_0x01f3
            int r1 = r12.height
            int r1 = android.view.ViewGroup.getChildMeasureSpec(r2, r8, r1)
            goto L_0x01fd
        L_0x01f3:
            int r1 = r15.i()
            r10 = 1073741824(0x40000000, float:2.0)
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r10)
        L_0x01fd:
            r3.measure(r9, r1)
            androidx.constraintlayout.solver.Metrics r1 = r0.p
            if (r1 == 0) goto L_0x020a
            long r9 = r1.b
            long r9 = r9 + r18
            r1.b = r9
        L_0x020a:
            int r1 = r3.getMeasuredWidth()
            int r9 = r3.getMeasuredHeight()
            int r10 = r15.s()
            if (r1 == r10) goto L_0x0241
            r15.o(r1)
            if (r14 == 0) goto L_0x0224
            androidx.constraintlayout.solver.widgets.ResolutionDimension r10 = r15.m()
            r10.a(r1)
        L_0x0224:
            if (r6 == 0) goto L_0x023f
            int r1 = r15.n()
            if (r1 <= r13) goto L_0x023f
            int r1 = r15.n()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r10 = r15.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r10)
            int r10 = r10.b()
            int r1 = r1 + r10
            int r13 = java.lang.Math.max(r13, r1)
        L_0x023f:
            r23 = 1
        L_0x0241:
            int r1 = r15.i()
            if (r9 == r1) goto L_0x0271
            r15.g(r9)
            if (r14 == 0) goto L_0x0253
            androidx.constraintlayout.solver.widgets.ResolutionDimension r1 = r15.l()
            r1.a(r9)
        L_0x0253:
            if (r11 == 0) goto L_0x026f
            int r1 = r15.d()
            if (r1 <= r5) goto L_0x026f
            int r1 = r15.d()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r9 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r15.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r9)
            int r9 = r9.b()
            int r1 = r1 + r9
            int r1 = java.lang.Math.max(r5, r1)
            r5 = r1
        L_0x026f:
            r23 = 1
        L_0x0271:
            boolean r1 = r12.W
            if (r1 == 0) goto L_0x0287
            int r1 = r3.getBaseline()
            r9 = -1
            if (r1 == r9) goto L_0x0287
            int r9 = r15.c()
            if (r1 == r9) goto L_0x0287
            r15.f(r1)
            r23 = 1
        L_0x0287:
            int r1 = android.os.Build.VERSION.SDK_INT
            r9 = 11
            if (r1 < r9) goto L_0x0298
            int r1 = r3.getMeasuredState()
            r3 = r17
            int r17 = android.view.ViewGroup.combineMeasuredStates(r3, r1)
            goto L_0x02a1
        L_0x0298:
            r3 = r17
            goto L_0x02a1
        L_0x029b:
            r23 = r10
        L_0x029d:
            r3 = r17
            r17 = r3
        L_0x02a1:
            r10 = r23
            int r4 = r4 + 1
            r1 = r25
            r9 = r20
            r3 = r21
            r12 = r22
            r15 = 1073741824(0x40000000, float:2.0)
            goto L_0x0170
        L_0x02b1:
            r21 = r3
            r20 = r9
            r23 = r10
            r22 = r12
            r3 = r17
            if (r23 == 0) goto L_0x0300
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r1 = r0.d
            r4 = r20
            r1.o(r4)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r1 = r0.d
            r4 = r22
            r1.g(r4)
            if (r14 == 0) goto L_0x02d2
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r1 = r0.d
            r1.U()
        L_0x02d2:
            java.lang.String r1 = "2nd pass"
            r0.a((java.lang.String) r1)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r1 = r0.d
            int r1 = r1.s()
            if (r1 >= r13) goto L_0x02e6
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r1 = r0.d
            r1.o(r13)
            r11 = 1
            goto L_0x02e7
        L_0x02e6:
            r11 = 0
        L_0x02e7:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r1 = r0.d
            int r1 = r1.i()
            if (r1 >= r5) goto L_0x02f7
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r1 = r0.d
            r1.g(r5)
            r16 = 1
            goto L_0x02f9
        L_0x02f7:
            r16 = r11
        L_0x02f9:
            if (r16 == 0) goto L_0x0300
            java.lang.String r1 = "3rd pass"
            r0.a((java.lang.String) r1)
        L_0x0300:
            r1 = r21
            r4 = 0
        L_0x0303:
            if (r4 >= r1) goto L_0x0359
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r5 = r0.c
            java.lang.Object r5 = r5.get(r4)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r5 = (androidx.constraintlayout.solver.widgets.ConstraintWidget) r5
            java.lang.Object r6 = r5.e()
            android.view.View r6 = (android.view.View) r6
            if (r6 != 0) goto L_0x031a
        L_0x0315:
            r10 = 8
        L_0x0317:
            r11 = 1073741824(0x40000000, float:2.0)
            goto L_0x0355
        L_0x031a:
            int r9 = r6.getMeasuredWidth()
            int r10 = r5.s()
            if (r9 != r10) goto L_0x032e
            int r9 = r6.getMeasuredHeight()
            int r10 = r5.i()
            if (r9 == r10) goto L_0x0315
        L_0x032e:
            int r9 = r5.r()
            r10 = 8
            if (r9 == r10) goto L_0x0317
            int r9 = r5.s()
            r11 = 1073741824(0x40000000, float:2.0)
            int r9 = android.view.View.MeasureSpec.makeMeasureSpec(r9, r11)
            int r5 = r5.i()
            int r5 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r11)
            r6.measure(r9, r5)
            androidx.constraintlayout.solver.Metrics r5 = r0.p
            if (r5 == 0) goto L_0x0355
            long r12 = r5.b
            long r12 = r12 + r18
            r5.b = r12
        L_0x0355:
            int r4 = r4 + 1
            goto L_0x0303
        L_0x0358:
            r3 = 0
        L_0x0359:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r1 = r0.d
            int r1 = r1.s()
            int r1 = r1 + r7
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r4 = r0.d
            int r4 = r4.i()
            int r4 = r4 + r8
            int r5 = android.os.Build.VERSION.SDK_INT
            r6 = 11
            if (r5 < r6) goto L_0x03a6
            r5 = r25
            int r1 = android.view.ViewGroup.resolveSizeAndState(r1, r5, r3)
            int r3 = r3 << 16
            int r2 = android.view.ViewGroup.resolveSizeAndState(r4, r2, r3)
            r3 = 16777215(0xffffff, float:2.3509886E-38)
            r1 = r1 & r3
            r2 = r2 & r3
            int r3 = r0.g
            int r1 = java.lang.Math.min(r3, r1)
            int r3 = r0.h
            int r2 = java.lang.Math.min(r3, r2)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r3 = r0.d
            boolean r3 = r3.Q()
            r4 = 16777216(0x1000000, float:2.3509887E-38)
            if (r3 == 0) goto L_0x0395
            r1 = r1 | r4
        L_0x0395:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r3 = r0.d
            boolean r3 = r3.O()
            if (r3 == 0) goto L_0x039e
            r2 = r2 | r4
        L_0x039e:
            r0.setMeasuredDimension(r1, r2)
            r0.n = r1
            r0.o = r2
            goto L_0x03ad
        L_0x03a6:
            r0.setMeasuredDimension(r1, r4)
            r0.n = r1
            r0.o = r4
        L_0x03ad:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.widget.ConstraintLayout.onMeasure(int, int):void");
    }

    public void onViewAdded(View view) {
        if (Build.VERSION.SDK_INT >= 14) {
            super.onViewAdded(view);
        }
        ConstraintWidget a2 = a(view);
        if ((view instanceof Guideline) && !(a2 instanceof Guideline)) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            layoutParams.k0 = new Guideline();
            layoutParams.X = true;
            ((Guideline) layoutParams.k0).v(layoutParams.R);
        }
        if (view instanceof ConstraintHelper) {
            ConstraintHelper constraintHelper = (ConstraintHelper) view;
            constraintHelper.a();
            ((LayoutParams) view.getLayoutParams()).Y = true;
            if (!this.b.contains(constraintHelper)) {
                this.b.add(constraintHelper);
            }
        }
        this.f506a.put(view.getId(), view);
        this.i = true;
    }

    public void onViewRemoved(View view) {
        if (Build.VERSION.SDK_INT >= 14) {
            super.onViewRemoved(view);
        }
        this.f506a.remove(view.getId());
        ConstraintWidget a2 = a(view);
        this.d.c(a2);
        this.b.remove(view);
        this.c.remove(a2);
        this.i = true;
    }

    public void removeView(View view) {
        super.removeView(view);
        if (Build.VERSION.SDK_INT < 14) {
            onViewRemoved(view);
        }
    }

    public void requestLayout() {
        super.requestLayout();
        this.i = true;
        this.n = -1;
        this.o = -1;
    }

    public void setConstraintSet(ConstraintSet constraintSet) {
        this.k = constraintSet;
    }

    public void setId(int i2) {
        this.f506a.remove(getId());
        super.setId(i2);
        this.f506a.put(getId(), this);
    }

    public void setMaxHeight(int i2) {
        if (i2 != this.h) {
            this.h = i2;
            requestLayout();
        }
    }

    public void setMaxWidth(int i2) {
        if (i2 != this.g) {
            this.g = i2;
            requestLayout();
        }
    }

    public void setMinHeight(int i2) {
        if (i2 != this.f) {
            this.f = i2;
            requestLayout();
        }
    }

    public void setMinWidth(int i2) {
        if (i2 != this.e) {
            this.e = i2;
            requestLayout();
        }
    }

    public void setOptimizationLevel(int i2) {
        this.d.u(i2);
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    private final ConstraintWidget b(int i2) {
        if (i2 == 0) {
            return this.d;
        }
        View view = this.f506a.get(i2);
        if (view == null && (view = findViewById(i2)) != null && view != this && view.getParent() == this) {
            onViewAdded(view);
        }
        if (view == this) {
            return this.d;
        }
        if (view == null) {
            return null;
        }
        return ((LayoutParams) view.getLayoutParams()).k0;
    }

    private void c(int i2, int i3) {
        ConstraintWidget.DimensionBehaviour dimensionBehaviour;
        int i4;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.FIXED;
        getLayoutParams();
        if (mode != Integer.MIN_VALUE) {
            if (mode == 0) {
                dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            } else if (mode != 1073741824) {
                dimensionBehaviour = dimensionBehaviour2;
            } else {
                i4 = Math.min(this.g, size) - paddingLeft;
                dimensionBehaviour = dimensionBehaviour2;
            }
            i4 = 0;
        } else {
            i4 = size;
            dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        }
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 == 0) {
                dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            } else if (mode2 == 1073741824) {
                size2 = Math.min(this.h, size2) - paddingTop;
            }
            size2 = 0;
        } else {
            dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        }
        this.d.l(0);
        this.d.k(0);
        this.d.a(dimensionBehaviour);
        this.d.o(i4);
        this.d.b(dimensionBehaviour2);
        this.d.g(size2);
        this.d.l((this.e - getPaddingLeft()) - getPaddingRight());
        this.d.k((this.f - getPaddingTop()) - getPaddingBottom());
    }

    public Object a(int i2, Object obj) {
        if (i2 != 0 || !(obj instanceof String)) {
            return null;
        }
        String str = (String) obj;
        HashMap<String, Integer> hashMap = this.m;
        if (hashMap == null || !hashMap.containsKey(str)) {
            return null;
        }
        return this.m.get(str);
    }

    private void a(AttributeSet attributeSet) {
        this.d.a((Object) this);
        this.f506a.put(getId(), this);
        this.k = null;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R$styleable.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i2 = 0; i2 < indexCount; i2++) {
                int index = obtainStyledAttributes.getIndex(i2);
                if (index == R$styleable.ConstraintLayout_Layout_android_minWidth) {
                    this.e = obtainStyledAttributes.getDimensionPixelOffset(index, this.e);
                } else if (index == R$styleable.ConstraintLayout_Layout_android_minHeight) {
                    this.f = obtainStyledAttributes.getDimensionPixelOffset(index, this.f);
                } else if (index == R$styleable.ConstraintLayout_Layout_android_maxWidth) {
                    this.g = obtainStyledAttributes.getDimensionPixelOffset(index, this.g);
                } else if (index == R$styleable.ConstraintLayout_Layout_android_maxHeight) {
                    this.h = obtainStyledAttributes.getDimensionPixelOffset(index, this.h);
                } else if (index == R$styleable.ConstraintLayout_Layout_layout_optimizationLevel) {
                    this.j = obtainStyledAttributes.getInt(index, this.j);
                } else if (index == R$styleable.ConstraintLayout_Layout_constraintSet) {
                    int resourceId = obtainStyledAttributes.getResourceId(index, 0);
                    try {
                        this.k = new ConstraintSet();
                        this.k.a(getContext(), resourceId);
                    } catch (Resources.NotFoundException unused) {
                        this.k = null;
                    }
                    this.l = resourceId;
                }
            }
            obtainStyledAttributes.recycle();
        }
        this.d.u(this.j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x0204  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x023e  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x026c  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0271  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0273  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0279  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x027b  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x028f  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0294  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0299  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x02a1  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x02aa  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x02b2  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x02bf  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x02ca  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r24, int r25) {
        /*
            r23 = this;
            r0 = r23
            r1 = r24
            r2 = r25
            int r3 = r23.getPaddingTop()
            int r4 = r23.getPaddingBottom()
            int r3 = r3 + r4
            int r4 = r23.getPaddingLeft()
            int r5 = r23.getPaddingRight()
            int r4 = r4 + r5
            int r5 = r23.getChildCount()
            r7 = 0
        L_0x001d:
            r8 = 1
            r10 = 8
            r12 = -2
            if (r7 >= r5) goto L_0x00dc
            android.view.View r14 = r0.getChildAt(r7)
            int r15 = r14.getVisibility()
            if (r15 != r10) goto L_0x0030
            goto L_0x00d4
        L_0x0030:
            android.view.ViewGroup$LayoutParams r10 = r14.getLayoutParams()
            androidx.constraintlayout.widget.ConstraintLayout$LayoutParams r10 = (androidx.constraintlayout.widget.ConstraintLayout.LayoutParams) r10
            androidx.constraintlayout.solver.widgets.ConstraintWidget r15 = r10.k0
            boolean r6 = r10.X
            if (r6 != 0) goto L_0x00d4
            boolean r6 = r10.Y
            if (r6 == 0) goto L_0x0042
            goto L_0x00d4
        L_0x0042:
            int r6 = r14.getVisibility()
            r15.n(r6)
            int r6 = r10.width
            int r13 = r10.height
            if (r6 == 0) goto L_0x00c4
            if (r13 != 0) goto L_0x0053
            goto L_0x00c4
        L_0x0053:
            if (r6 != r12) goto L_0x0058
            r16 = 1
            goto L_0x005a
        L_0x0058:
            r16 = 0
        L_0x005a:
            int r11 = android.view.ViewGroup.getChildMeasureSpec(r1, r4, r6)
            if (r13 != r12) goto L_0x0063
            r17 = 1
            goto L_0x0065
        L_0x0063:
            r17 = 0
        L_0x0065:
            int r12 = android.view.ViewGroup.getChildMeasureSpec(r2, r3, r13)
            r14.measure(r11, r12)
            androidx.constraintlayout.solver.Metrics r11 = r0.p
            r12 = r3
            if (r11 == 0) goto L_0x0076
            long r2 = r11.f486a
            long r2 = r2 + r8
            r11.f486a = r2
        L_0x0076:
            r2 = -2
            if (r6 != r2) goto L_0x007b
            r3 = 1
            goto L_0x007c
        L_0x007b:
            r3 = 0
        L_0x007c:
            r15.b((boolean) r3)
            if (r13 != r2) goto L_0x0083
            r2 = 1
            goto L_0x0084
        L_0x0083:
            r2 = 0
        L_0x0084:
            r15.a((boolean) r2)
            int r2 = r14.getMeasuredWidth()
            int r3 = r14.getMeasuredHeight()
            r15.o(r2)
            r15.g(r3)
            if (r16 == 0) goto L_0x009a
            r15.q(r2)
        L_0x009a:
            if (r17 == 0) goto L_0x009f
            r15.p(r3)
        L_0x009f:
            boolean r6 = r10.W
            if (r6 == 0) goto L_0x00ad
            int r6 = r14.getBaseline()
            r8 = -1
            if (r6 == r8) goto L_0x00ad
            r15.f(r6)
        L_0x00ad:
            boolean r6 = r10.U
            if (r6 == 0) goto L_0x00d5
            boolean r6 = r10.V
            if (r6 == 0) goto L_0x00d5
            androidx.constraintlayout.solver.widgets.ResolutionDimension r6 = r15.m()
            r6.a(r2)
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r15.l()
            r2.a(r3)
            goto L_0x00d5
        L_0x00c4:
            r12 = r3
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r15.m()
            r2.b()
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r15.l()
            r2.b()
            goto L_0x00d5
        L_0x00d4:
            r12 = r3
        L_0x00d5:
            int r7 = r7 + 1
            r2 = r25
            r3 = r12
            goto L_0x001d
        L_0x00dc:
            r12 = r3
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r2 = r0.d
            r2.U()
            r2 = 0
        L_0x00e3:
            if (r2 >= r5) goto L_0x02e0
            android.view.View r3 = r0.getChildAt(r2)
            int r6 = r3.getVisibility()
            if (r6 != r10) goto L_0x00f1
            goto L_0x02cc
        L_0x00f1:
            android.view.ViewGroup$LayoutParams r6 = r3.getLayoutParams()
            androidx.constraintlayout.widget.ConstraintLayout$LayoutParams r6 = (androidx.constraintlayout.widget.ConstraintLayout.LayoutParams) r6
            androidx.constraintlayout.solver.widgets.ConstraintWidget r7 = r6.k0
            boolean r11 = r6.X
            if (r11 != 0) goto L_0x02cc
            boolean r11 = r6.Y
            if (r11 == 0) goto L_0x0103
            goto L_0x02cc
        L_0x0103:
            int r11 = r3.getVisibility()
            r7.n(r11)
            int r11 = r6.width
            int r13 = r6.height
            if (r11 == 0) goto L_0x0114
            if (r13 == 0) goto L_0x0114
            goto L_0x02cc
        L_0x0114:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r14 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r14 = r7.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r14)
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r14 = r14.d()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r15 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r15 = r7.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r15)
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r15 = r15.d()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r10 = r7.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r10)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r10 = r10.g()
            if (r10 == 0) goto L_0x0142
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r10 = r7.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r10)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r10 = r10.g()
            if (r10 == 0) goto L_0x0142
            r10 = 1
            goto L_0x0143
        L_0x0142:
            r10 = 0
        L_0x0143:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r8 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r8 = r7.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r8)
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r8 = r8.d()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r9 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r7.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r9)
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r9 = r9.d()
            r17 = r5
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r5 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r7.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r5)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r5.g()
            if (r5 == 0) goto L_0x0173
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r5 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r7.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r5)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r5.g()
            if (r5 == 0) goto L_0x0173
            r5 = 1
            goto L_0x0174
        L_0x0173:
            r5 = 0
        L_0x0174:
            if (r11 != 0) goto L_0x0186
            if (r13 != 0) goto L_0x0186
            if (r10 == 0) goto L_0x0186
            if (r5 == 0) goto L_0x0186
            r5 = r25
            r20 = r2
            r3 = -1
            r10 = -2
            r18 = 1
            goto L_0x02d6
        L_0x0186:
            r20 = r2
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r2 = r0.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r2 = r2.j()
            r21 = r6
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r6 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r2 == r6) goto L_0x0196
            r6 = 1
            goto L_0x0197
        L_0x0196:
            r6 = 0
        L_0x0197:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r2 = r0.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r2 = r2.q()
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r0 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r2 == r0) goto L_0x01a3
            r0 = 1
            goto L_0x01a4
        L_0x01a3:
            r0 = 0
        L_0x01a4:
            if (r6 != 0) goto L_0x01ad
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r7.m()
            r2.b()
        L_0x01ad:
            if (r0 != 0) goto L_0x01b6
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r7.l()
            r2.b()
        L_0x01b6:
            if (r11 != 0) goto L_0x01ee
            if (r6 == 0) goto L_0x01e5
            boolean r2 = r7.C()
            if (r2 == 0) goto L_0x01e5
            if (r10 == 0) goto L_0x01e5
            boolean r2 = r14.c()
            if (r2 == 0) goto L_0x01e5
            boolean r2 = r15.c()
            if (r2 == 0) goto L_0x01e5
            float r2 = r15.f()
            float r10 = r14.f()
            float r2 = r2 - r10
            int r11 = (int) r2
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r7.m()
            r2.a(r11)
            int r2 = android.view.ViewGroup.getChildMeasureSpec(r1, r4, r11)
            r14 = r2
            goto L_0x01f6
        L_0x01e5:
            r2 = -2
            int r6 = android.view.ViewGroup.getChildMeasureSpec(r1, r4, r2)
            r14 = r6
            r2 = 1
            r6 = 0
            goto L_0x0202
        L_0x01ee:
            r2 = -2
            r10 = -1
            if (r11 != r10) goto L_0x01f8
            int r14 = android.view.ViewGroup.getChildMeasureSpec(r1, r4, r10)
        L_0x01f6:
            r2 = 0
            goto L_0x0202
        L_0x01f8:
            if (r11 != r2) goto L_0x01fc
            r2 = 1
            goto L_0x01fd
        L_0x01fc:
            r2 = 0
        L_0x01fd:
            int r10 = android.view.ViewGroup.getChildMeasureSpec(r1, r4, r11)
            r14 = r10
        L_0x0202:
            if (r13 != 0) goto L_0x023e
            if (r0 == 0) goto L_0x0234
            boolean r10 = r7.B()
            if (r10 == 0) goto L_0x0234
            if (r5 == 0) goto L_0x0234
            boolean r5 = r8.c()
            if (r5 == 0) goto L_0x0234
            boolean r5 = r9.c()
            if (r5 == 0) goto L_0x0234
            float r5 = r9.f()
            float r8 = r8.f()
            float r5 = r5 - r8
            int r13 = (int) r5
            androidx.constraintlayout.solver.widgets.ResolutionDimension r5 = r7.l()
            r5.a(r13)
            r5 = r25
            int r8 = android.view.ViewGroup.getChildMeasureSpec(r5, r12, r13)
            r9 = r0
            r0 = r8
            goto L_0x024a
        L_0x0234:
            r5 = r25
            r8 = -2
            int r0 = android.view.ViewGroup.getChildMeasureSpec(r5, r12, r8)
            r8 = 1
            r9 = 0
            goto L_0x025a
        L_0x023e:
            r5 = r25
            r8 = -2
            r9 = -1
            if (r13 != r9) goto L_0x024c
            int r10 = android.view.ViewGroup.getChildMeasureSpec(r5, r12, r9)
            r9 = r0
            r0 = r10
        L_0x024a:
            r8 = 0
            goto L_0x025a
        L_0x024c:
            if (r13 != r8) goto L_0x0250
            r8 = 1
            goto L_0x0251
        L_0x0250:
            r8 = 0
        L_0x0251:
            int r9 = android.view.ViewGroup.getChildMeasureSpec(r5, r12, r13)
            r22 = r9
            r9 = r0
            r0 = r22
        L_0x025a:
            r3.measure(r14, r0)
            r0 = r23
            androidx.constraintlayout.solver.Metrics r10 = r0.p
            if (r10 == 0) goto L_0x026c
            long r14 = r10.f486a
            r18 = 1
            long r14 = r14 + r18
            r10.f486a = r14
            goto L_0x026e
        L_0x026c:
            r18 = 1
        L_0x026e:
            r10 = -2
            if (r11 != r10) goto L_0x0273
            r11 = 1
            goto L_0x0274
        L_0x0273:
            r11 = 0
        L_0x0274:
            r7.b((boolean) r11)
            if (r13 != r10) goto L_0x027b
            r11 = 1
            goto L_0x027c
        L_0x027b:
            r11 = 0
        L_0x027c:
            r7.a((boolean) r11)
            int r11 = r3.getMeasuredWidth()
            int r13 = r3.getMeasuredHeight()
            r7.o(r11)
            r7.g(r13)
            if (r2 == 0) goto L_0x0292
            r7.q(r11)
        L_0x0292:
            if (r8 == 0) goto L_0x0297
            r7.p(r13)
        L_0x0297:
            if (r6 == 0) goto L_0x02a1
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r7.m()
            r2.a(r11)
            goto L_0x02a8
        L_0x02a1:
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r7.m()
            r2.f()
        L_0x02a8:
            if (r9 == 0) goto L_0x02b2
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r7.l()
            r2.a(r13)
            goto L_0x02b9
        L_0x02b2:
            androidx.constraintlayout.solver.widgets.ResolutionDimension r2 = r7.l()
            r2.f()
        L_0x02b9:
            r6 = r21
            boolean r2 = r6.W
            if (r2 == 0) goto L_0x02ca
            int r2 = r3.getBaseline()
            r3 = -1
            if (r2 == r3) goto L_0x02d6
            r7.f(r2)
            goto L_0x02d6
        L_0x02ca:
            r3 = -1
            goto L_0x02d6
        L_0x02cc:
            r20 = r2
            r17 = r5
            r18 = r8
            r3 = -1
            r10 = -2
            r5 = r25
        L_0x02d6:
            int r2 = r20 + 1
            r5 = r17
            r8 = r18
            r10 = 8
            goto L_0x00e3
        L_0x02e0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.widget.ConstraintLayout.b(int, int):void");
    }

    public ConstraintLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    public ConstraintLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(attributeSet);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:118:0x01d6, code lost:
        if (r11 != -1) goto L_0x01da;
     */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x01e3  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x01e7  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0205  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0214  */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x0346  */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x036e  */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x037c  */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x03a5  */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x03b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r26 = this;
            r0 = r26
            boolean r1 = r26.isInEditMode()
            int r2 = r26.getChildCount()
            r3 = 0
            r4 = -1
            if (r1 == 0) goto L_0x0048
            r5 = 0
        L_0x000f:
            if (r5 >= r2) goto L_0x0048
            android.view.View r6 = r0.getChildAt(r5)
            android.content.res.Resources r7 = r26.getResources()     // Catch:{ NotFoundException -> 0x0045 }
            int r8 = r6.getId()     // Catch:{ NotFoundException -> 0x0045 }
            java.lang.String r7 = r7.getResourceName(r8)     // Catch:{ NotFoundException -> 0x0045 }
            int r8 = r6.getId()     // Catch:{ NotFoundException -> 0x0045 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ NotFoundException -> 0x0045 }
            r0.a(r3, r7, r8)     // Catch:{ NotFoundException -> 0x0045 }
            r8 = 47
            int r8 = r7.indexOf(r8)     // Catch:{ NotFoundException -> 0x0045 }
            if (r8 == r4) goto L_0x003a
            int r8 = r8 + 1
            java.lang.String r7 = r7.substring(r8)     // Catch:{ NotFoundException -> 0x0045 }
        L_0x003a:
            int r6 = r6.getId()     // Catch:{ NotFoundException -> 0x0045 }
            androidx.constraintlayout.solver.widgets.ConstraintWidget r6 = r0.b(r6)     // Catch:{ NotFoundException -> 0x0045 }
            r6.a((java.lang.String) r7)     // Catch:{ NotFoundException -> 0x0045 }
        L_0x0045:
            int r5 = r5 + 1
            goto L_0x000f
        L_0x0048:
            r5 = 0
        L_0x0049:
            if (r5 >= r2) goto L_0x005c
            android.view.View r6 = r0.getChildAt(r5)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r6 = r0.a((android.view.View) r6)
            if (r6 != 0) goto L_0x0056
            goto L_0x0059
        L_0x0056:
            r6.D()
        L_0x0059:
            int r5 = r5 + 1
            goto L_0x0049
        L_0x005c:
            int r5 = r0.l
            if (r5 == r4) goto L_0x007e
            r5 = 0
        L_0x0061:
            if (r5 >= r2) goto L_0x007e
            android.view.View r6 = r0.getChildAt(r5)
            int r7 = r6.getId()
            int r8 = r0.l
            if (r7 != r8) goto L_0x007b
            boolean r7 = r6 instanceof androidx.constraintlayout.widget.Constraints
            if (r7 == 0) goto L_0x007b
            androidx.constraintlayout.widget.Constraints r6 = (androidx.constraintlayout.widget.Constraints) r6
            androidx.constraintlayout.widget.ConstraintSet r6 = r6.getConstraintSet()
            r0.k = r6
        L_0x007b:
            int r5 = r5 + 1
            goto L_0x0061
        L_0x007e:
            androidx.constraintlayout.widget.ConstraintSet r5 = r0.k
            if (r5 == 0) goto L_0x0085
            r5.a((androidx.constraintlayout.widget.ConstraintLayout) r0)
        L_0x0085:
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r5 = r0.d
            r5.L()
            java.util.ArrayList<androidx.constraintlayout.widget.ConstraintHelper> r5 = r0.b
            int r5 = r5.size()
            if (r5 <= 0) goto L_0x00a3
            r6 = 0
        L_0x0093:
            if (r6 >= r5) goto L_0x00a3
            java.util.ArrayList<androidx.constraintlayout.widget.ConstraintHelper> r7 = r0.b
            java.lang.Object r7 = r7.get(r6)
            androidx.constraintlayout.widget.ConstraintHelper r7 = (androidx.constraintlayout.widget.ConstraintHelper) r7
            r7.c(r0)
            int r6 = r6 + 1
            goto L_0x0093
        L_0x00a3:
            r5 = 0
        L_0x00a4:
            if (r5 >= r2) goto L_0x00b6
            android.view.View r6 = r0.getChildAt(r5)
            boolean r7 = r6 instanceof androidx.constraintlayout.widget.Placeholder
            if (r7 == 0) goto L_0x00b3
            androidx.constraintlayout.widget.Placeholder r6 = (androidx.constraintlayout.widget.Placeholder) r6
            r6.b(r0)
        L_0x00b3:
            int r5 = r5 + 1
            goto L_0x00a4
        L_0x00b6:
            r5 = 0
        L_0x00b7:
            if (r5 >= r2) goto L_0x03e5
            android.view.View r6 = r0.getChildAt(r5)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r13 = r0.a((android.view.View) r6)
            if (r13 != 0) goto L_0x00c5
            goto L_0x03e1
        L_0x00c5:
            android.view.ViewGroup$LayoutParams r7 = r6.getLayoutParams()
            r14 = r7
            androidx.constraintlayout.widget.ConstraintLayout$LayoutParams r14 = (androidx.constraintlayout.widget.ConstraintLayout.LayoutParams) r14
            r14.a()
            boolean r7 = r14.l0
            if (r7 == 0) goto L_0x00d6
            r14.l0 = r3
            goto L_0x0108
        L_0x00d6:
            if (r1 == 0) goto L_0x0108
            android.content.res.Resources r7 = r26.getResources()     // Catch:{ NotFoundException -> 0x0107 }
            int r8 = r6.getId()     // Catch:{ NotFoundException -> 0x0107 }
            java.lang.String r7 = r7.getResourceName(r8)     // Catch:{ NotFoundException -> 0x0107 }
            int r8 = r6.getId()     // Catch:{ NotFoundException -> 0x0107 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ NotFoundException -> 0x0107 }
            r0.a(r3, r7, r8)     // Catch:{ NotFoundException -> 0x0107 }
            java.lang.String r8 = "id/"
            int r8 = r7.indexOf(r8)     // Catch:{ NotFoundException -> 0x0107 }
            int r8 = r8 + 3
            java.lang.String r7 = r7.substring(r8)     // Catch:{ NotFoundException -> 0x0107 }
            int r8 = r6.getId()     // Catch:{ NotFoundException -> 0x0107 }
            androidx.constraintlayout.solver.widgets.ConstraintWidget r8 = r0.b(r8)     // Catch:{ NotFoundException -> 0x0107 }
            r8.a((java.lang.String) r7)     // Catch:{ NotFoundException -> 0x0107 }
            goto L_0x0108
        L_0x0107:
        L_0x0108:
            int r7 = r6.getVisibility()
            r13.n(r7)
            boolean r7 = r14.Z
            if (r7 == 0) goto L_0x0118
            r7 = 8
            r13.n(r7)
        L_0x0118:
            r13.a((java.lang.Object) r6)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r6 = r0.d
            r6.b(r13)
            boolean r6 = r14.V
            if (r6 == 0) goto L_0x0128
            boolean r6 = r14.U
            if (r6 != 0) goto L_0x012d
        L_0x0128:
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r6 = r0.c
            r6.add(r13)
        L_0x012d:
            boolean r6 = r14.X
            r7 = 17
            if (r6 == 0) goto L_0x015e
            androidx.constraintlayout.solver.widgets.Guideline r13 = (androidx.constraintlayout.solver.widgets.Guideline) r13
            int r6 = r14.h0
            int r8 = r14.i0
            float r9 = r14.j0
            int r10 = android.os.Build.VERSION.SDK_INT
            if (r10 >= r7) goto L_0x0145
            int r6 = r14.f507a
            int r8 = r14.b
            float r9 = r14.c
        L_0x0145:
            r7 = -1082130432(0xffffffffbf800000, float:-1.0)
            int r7 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r7 == 0) goto L_0x0150
            r13.e(r9)
            goto L_0x03e1
        L_0x0150:
            if (r6 == r4) goto L_0x0157
            r13.t(r6)
            goto L_0x03e1
        L_0x0157:
            if (r8 == r4) goto L_0x03e1
            r13.u(r8)
            goto L_0x03e1
        L_0x015e:
            int r6 = r14.d
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.e
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.f
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.g
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.q
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.p
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.r
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.s
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.h
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.i
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.j
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.k
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.l
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.P
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.Q
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.m
            if (r6 != r4) goto L_0x01a6
            int r6 = r14.width
            if (r6 == r4) goto L_0x01a6
            int r6 = r14.height
            if (r6 != r4) goto L_0x03e1
        L_0x01a6:
            int r6 = r14.a0
            int r8 = r14.b0
            int r9 = r14.c0
            int r10 = r14.d0
            int r11 = r14.e0
            int r12 = r14.f0
            float r15 = r14.g0
            int r3 = android.os.Build.VERSION.SDK_INT
            if (r3 >= r7) goto L_0x01f7
            int r3 = r14.d
            int r6 = r14.e
            int r9 = r14.f
            int r10 = r14.g
            int r7 = r14.t
            int r8 = r14.v
            float r15 = r14.z
            if (r3 != r4) goto L_0x01d9
            if (r6 != r4) goto L_0x01d9
            int r11 = r14.q
            if (r11 == r4) goto L_0x01d4
            r25 = r11
            r11 = r6
            r6 = r25
            goto L_0x01db
        L_0x01d4:
            int r11 = r14.p
            if (r11 == r4) goto L_0x01d9
            goto L_0x01da
        L_0x01d9:
            r11 = r6
        L_0x01da:
            r6 = r3
        L_0x01db:
            if (r9 != r4) goto L_0x01f2
            if (r10 != r4) goto L_0x01f2
            int r3 = r14.r
            if (r3 == r4) goto L_0x01e7
            r12 = r7
            r16 = r8
            goto L_0x01fc
        L_0x01e7:
            int r3 = r14.s
            if (r3 == r4) goto L_0x01f2
            r12 = r7
            r16 = r8
            r10 = r15
            r15 = r3
            r3 = r9
            goto L_0x0201
        L_0x01f2:
            r12 = r7
            r16 = r8
            r3 = r9
            goto L_0x01fc
        L_0x01f7:
            r3 = r9
            r16 = r12
            r12 = r11
            r11 = r8
        L_0x01fc:
            r25 = r15
            r15 = r10
            r10 = r25
        L_0x0201:
            int r7 = r14.m
            if (r7 == r4) goto L_0x0214
            androidx.constraintlayout.solver.widgets.ConstraintWidget r3 = r0.b(r7)
            if (r3 == 0) goto L_0x0331
            float r6 = r14.o
            int r7 = r14.n
            r13.a((androidx.constraintlayout.solver.widgets.ConstraintWidget) r3, (float) r6, (int) r7)
            goto L_0x0331
        L_0x0214:
            if (r6 == r4) goto L_0x022c
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r0.b(r6)
            if (r9 == 0) goto L_0x0229
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r6 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT
            int r11 = r14.leftMargin
            r7 = r13
            r8 = r6
            r17 = r10
            r10 = r6
            r7.a(r8, r9, r10, r11, r12)
            goto L_0x0240
        L_0x0229:
            r17 = r10
            goto L_0x0240
        L_0x022c:
            r17 = r10
            if (r11 == r4) goto L_0x0240
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r0.b(r11)
            if (r9 == 0) goto L_0x0240
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r8 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT
            int r11 = r14.leftMargin
            r7 = r13
            r7.a(r8, r9, r10, r11, r12)
        L_0x0240:
            if (r3 == r4) goto L_0x0255
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r0.b(r3)
            if (r9 == 0) goto L_0x0268
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r8 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT
            int r11 = r14.rightMargin
            r7 = r13
            r12 = r16
            r7.a(r8, r9, r10, r11, r12)
            goto L_0x0268
        L_0x0255:
            if (r15 == r4) goto L_0x0268
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r0.b(r15)
            if (r9 == 0) goto L_0x0268
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT
            int r11 = r14.rightMargin
            r7 = r13
            r8 = r10
            r12 = r16
            r7.a(r8, r9, r10, r11, r12)
        L_0x0268:
            int r3 = r14.h
            if (r3 == r4) goto L_0x027e
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r0.b(r3)
            if (r9 == 0) goto L_0x0294
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP
            int r11 = r14.topMargin
            int r12 = r14.u
            r7 = r13
            r8 = r10
            r7.a(r8, r9, r10, r11, r12)
            goto L_0x0294
        L_0x027e:
            int r3 = r14.i
            if (r3 == r4) goto L_0x0294
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r0.b(r3)
            if (r9 == 0) goto L_0x0294
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r8 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            int r11 = r14.topMargin
            int r12 = r14.u
            r7 = r13
            r7.a(r8, r9, r10, r11, r12)
        L_0x0294:
            int r3 = r14.j
            if (r3 == r4) goto L_0x02ab
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r0.b(r3)
            if (r9 == 0) goto L_0x02c0
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r8 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP
            int r11 = r14.bottomMargin
            int r12 = r14.w
            r7 = r13
            r7.a(r8, r9, r10, r11, r12)
            goto L_0x02c0
        L_0x02ab:
            int r3 = r14.k
            if (r3 == r4) goto L_0x02c0
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r0.b(r3)
            if (r9 == 0) goto L_0x02c0
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r10 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            int r11 = r14.bottomMargin
            int r12 = r14.w
            r7 = r13
            r8 = r10
            r7.a(r8, r9, r10, r11, r12)
        L_0x02c0:
            int r3 = r14.l
            if (r3 == r4) goto L_0x0314
            android.util.SparseArray<android.view.View> r6 = r0.f506a
            java.lang.Object r3 = r6.get(r3)
            android.view.View r3 = (android.view.View) r3
            int r6 = r14.l
            androidx.constraintlayout.solver.widgets.ConstraintWidget r6 = r0.b(r6)
            if (r6 == 0) goto L_0x0314
            if (r3 == 0) goto L_0x0314
            android.view.ViewGroup$LayoutParams r7 = r3.getLayoutParams()
            boolean r7 = r7 instanceof androidx.constraintlayout.widget.ConstraintLayout.LayoutParams
            if (r7 == 0) goto L_0x0314
            android.view.ViewGroup$LayoutParams r3 = r3.getLayoutParams()
            androidx.constraintlayout.widget.ConstraintLayout$LayoutParams r3 = (androidx.constraintlayout.widget.ConstraintLayout.LayoutParams) r3
            r7 = 1
            r14.W = r7
            r3.W = r7
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r3 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BASELINE
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r18 = r13.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r3)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r3 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BASELINE
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r19 = r6.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r3)
            r20 = 0
            r21 = -1
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Strength r22 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Strength.STRONG
            r23 = 0
            r24 = 1
            r18.a(r19, r20, r21, r22, r23, r24)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r3 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r13.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r3)
            r3.j()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r3 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r13.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r3)
            r3.j()
        L_0x0314:
            r3 = 1056964608(0x3f000000, float:0.5)
            r6 = 0
            r15 = r17
            int r7 = (r15 > r6 ? 1 : (r15 == r6 ? 0 : -1))
            if (r7 < 0) goto L_0x0324
            int r7 = (r15 > r3 ? 1 : (r15 == r3 ? 0 : -1))
            if (r7 == 0) goto L_0x0324
            r13.a((float) r15)
        L_0x0324:
            float r7 = r14.A
            int r6 = (r7 > r6 ? 1 : (r7 == r6 ? 0 : -1))
            if (r6 < 0) goto L_0x0331
            int r3 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r3 == 0) goto L_0x0331
            r13.c((float) r7)
        L_0x0331:
            if (r1 == 0) goto L_0x0342
            int r3 = r14.P
            if (r3 != r4) goto L_0x033b
            int r3 = r14.Q
            if (r3 == r4) goto L_0x0342
        L_0x033b:
            int r3 = r14.P
            int r6 = r14.Q
            r13.c(r3, r6)
        L_0x0342:
            boolean r3 = r14.U
            if (r3 != 0) goto L_0x036e
            int r3 = r14.width
            if (r3 != r4) goto L_0x0364
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_PARENT
            r13.a((androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour) r3)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r3 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r13.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r3)
            int r6 = r14.leftMargin
            r3.e = r6
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r3 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r13.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r3)
            int r6 = r14.rightMargin
            r3.e = r6
            goto L_0x0378
        L_0x0364:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            r13.a((androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour) r3)
            r3 = 0
            r13.o(r3)
            goto L_0x0378
        L_0x036e:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            r13.a((androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour) r3)
            int r3 = r14.width
            r13.o(r3)
        L_0x0378:
            boolean r3 = r14.V
            if (r3 != 0) goto L_0x03a5
            int r3 = r14.height
            if (r3 != r4) goto L_0x039b
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_PARENT
            r13.b((androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour) r3)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r3 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r13.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r3)
            int r6 = r14.topMargin
            r3.e = r6
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r3 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r13.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r3)
            int r6 = r14.bottomMargin
            r3.e = r6
            r3 = 0
            goto L_0x03b0
        L_0x039b:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            r13.b((androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour) r3)
            r3 = 0
            r13.g(r3)
            goto L_0x03b0
        L_0x03a5:
            r3 = 0
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r6 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            r13.b((androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour) r6)
            int r6 = r14.height
            r13.g(r6)
        L_0x03b0:
            java.lang.String r6 = r14.B
            if (r6 == 0) goto L_0x03b7
            r13.b((java.lang.String) r6)
        L_0x03b7:
            float r6 = r14.D
            r13.b((float) r6)
            float r6 = r14.E
            r13.d((float) r6)
            int r6 = r14.F
            r13.h(r6)
            int r6 = r14.G
            r13.m(r6)
            int r6 = r14.H
            int r7 = r14.J
            int r8 = r14.L
            float r9 = r14.N
            r13.a((int) r6, (int) r7, (int) r8, (float) r9)
            int r6 = r14.I
            int r7 = r14.K
            int r8 = r14.M
            float r9 = r14.O
            r13.b(r6, r7, r8, r9)
        L_0x03e1:
            int r5 = r5 + 1
            goto L_0x00b7
        L_0x03e5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.widget.ConstraintLayout.a():void");
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public float A = 0.5f;
        public String B = null;
        int C = 1;
        public float D = -1.0f;
        public float E = -1.0f;
        public int F = 0;
        public int G = 0;
        public int H = 0;
        public int I = 0;
        public int J = 0;
        public int K = 0;
        public int L = 0;
        public int M = 0;
        public float N = 1.0f;
        public float O = 1.0f;
        public int P = -1;
        public int Q = -1;
        public int R = -1;
        public boolean S = false;
        public boolean T = false;
        boolean U = true;
        boolean V = true;
        boolean W = false;
        boolean X = false;
        boolean Y = false;
        boolean Z = false;

        /* renamed from: a  reason: collision with root package name */
        public int f507a = -1;
        int a0 = -1;
        public int b = -1;
        int b0 = -1;
        public float c = -1.0f;
        int c0 = -1;
        public int d = -1;
        int d0 = -1;
        public int e = -1;
        int e0 = -1;
        public int f = -1;
        int f0 = -1;
        public int g = -1;
        float g0 = 0.5f;
        public int h = -1;
        int h0;
        public int i = -1;
        int i0;
        public int j = -1;
        float j0;
        public int k = -1;
        ConstraintWidget k0 = new ConstraintWidget();
        public int l = -1;
        public boolean l0 = false;
        public int m = -1;
        public int n = 0;
        public float o = 0.0f;
        public int p = -1;
        public int q = -1;
        public int r = -1;
        public int s = -1;
        public int t = -1;
        public int u = -1;
        public int v = -1;
        public int w = -1;
        public int x = -1;
        public int y = -1;
        public float z = 0.5f;

        private static class Table {

            /* renamed from: a  reason: collision with root package name */
            public static final SparseIntArray f508a = new SparseIntArray();

            static {
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintLeft_toLeftOf, 8);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintLeft_toRightOf, 9);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintRight_toLeftOf, 10);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintRight_toRightOf, 11);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintTop_toTopOf, 12);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintTop_toBottomOf, 13);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintBottom_toTopOf, 14);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintBottom_toBottomOf, 15);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf, 16);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintCircle, 2);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintCircleRadius, 3);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintCircleAngle, 4);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_editor_absoluteX, 49);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_editor_absoluteY, 50);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintGuide_begin, 5);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintGuide_end, 6);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintGuide_percent, 7);
                f508a.append(R$styleable.ConstraintLayout_Layout_android_orientation, 1);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintStart_toEndOf, 17);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintStart_toStartOf, 18);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintEnd_toStartOf, 19);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintEnd_toEndOf, 20);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_goneMarginLeft, 21);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_goneMarginTop, 22);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_goneMarginRight, 23);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_goneMarginBottom, 24);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_goneMarginStart, 25);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_goneMarginEnd, 26);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintHorizontal_bias, 29);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintVertical_bias, 30);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintDimensionRatio, 44);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintHorizontal_weight, 45);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintVertical_weight, 46);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle, 47);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintVertical_chainStyle, 48);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constrainedWidth, 27);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constrainedHeight, 28);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintWidth_default, 31);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintHeight_default, 32);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintWidth_min, 33);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintWidth_max, 34);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintWidth_percent, 35);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintHeight_min, 36);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintHeight_max, 37);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintHeight_percent, 38);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintLeft_creator, 39);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintTop_creator, 40);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintRight_creator, 41);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintBottom_creator, 42);
                f508a.append(R$styleable.ConstraintLayout_Layout_layout_constraintBaseline_creator, 43);
            }

            private Table() {
            }
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            int i2;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i3 = 0; i3 < indexCount; i3++) {
                int index = obtainStyledAttributes.getIndex(i3);
                switch (Table.f508a.get(index)) {
                    case 1:
                        this.R = obtainStyledAttributes.getInt(index, this.R);
                        break;
                    case 2:
                        this.m = obtainStyledAttributes.getResourceId(index, this.m);
                        if (this.m != -1) {
                            break;
                        } else {
                            this.m = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 3:
                        this.n = obtainStyledAttributes.getDimensionPixelSize(index, this.n);
                        break;
                    case 4:
                        this.o = obtainStyledAttributes.getFloat(index, this.o) % 360.0f;
                        float f2 = this.o;
                        if (f2 >= 0.0f) {
                            break;
                        } else {
                            this.o = (360.0f - f2) % 360.0f;
                            break;
                        }
                    case 5:
                        this.f507a = obtainStyledAttributes.getDimensionPixelOffset(index, this.f507a);
                        break;
                    case 6:
                        this.b = obtainStyledAttributes.getDimensionPixelOffset(index, this.b);
                        break;
                    case 7:
                        this.c = obtainStyledAttributes.getFloat(index, this.c);
                        break;
                    case 8:
                        this.d = obtainStyledAttributes.getResourceId(index, this.d);
                        if (this.d != -1) {
                            break;
                        } else {
                            this.d = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 9:
                        this.e = obtainStyledAttributes.getResourceId(index, this.e);
                        if (this.e != -1) {
                            break;
                        } else {
                            this.e = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 10:
                        this.f = obtainStyledAttributes.getResourceId(index, this.f);
                        if (this.f != -1) {
                            break;
                        } else {
                            this.f = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 11:
                        this.g = obtainStyledAttributes.getResourceId(index, this.g);
                        if (this.g != -1) {
                            break;
                        } else {
                            this.g = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 12:
                        this.h = obtainStyledAttributes.getResourceId(index, this.h);
                        if (this.h != -1) {
                            break;
                        } else {
                            this.h = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 13:
                        this.i = obtainStyledAttributes.getResourceId(index, this.i);
                        if (this.i != -1) {
                            break;
                        } else {
                            this.i = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 14:
                        this.j = obtainStyledAttributes.getResourceId(index, this.j);
                        if (this.j != -1) {
                            break;
                        } else {
                            this.j = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 15:
                        this.k = obtainStyledAttributes.getResourceId(index, this.k);
                        if (this.k != -1) {
                            break;
                        } else {
                            this.k = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 16:
                        this.l = obtainStyledAttributes.getResourceId(index, this.l);
                        if (this.l != -1) {
                            break;
                        } else {
                            this.l = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 17:
                        this.p = obtainStyledAttributes.getResourceId(index, this.p);
                        if (this.p != -1) {
                            break;
                        } else {
                            this.p = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 18:
                        this.q = obtainStyledAttributes.getResourceId(index, this.q);
                        if (this.q != -1) {
                            break;
                        } else {
                            this.q = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 19:
                        this.r = obtainStyledAttributes.getResourceId(index, this.r);
                        if (this.r != -1) {
                            break;
                        } else {
                            this.r = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 20:
                        this.s = obtainStyledAttributes.getResourceId(index, this.s);
                        if (this.s != -1) {
                            break;
                        } else {
                            this.s = obtainStyledAttributes.getInt(index, -1);
                            break;
                        }
                    case 21:
                        this.t = obtainStyledAttributes.getDimensionPixelSize(index, this.t);
                        break;
                    case 22:
                        this.u = obtainStyledAttributes.getDimensionPixelSize(index, this.u);
                        break;
                    case 23:
                        this.v = obtainStyledAttributes.getDimensionPixelSize(index, this.v);
                        break;
                    case 24:
                        this.w = obtainStyledAttributes.getDimensionPixelSize(index, this.w);
                        break;
                    case 25:
                        this.x = obtainStyledAttributes.getDimensionPixelSize(index, this.x);
                        break;
                    case 26:
                        this.y = obtainStyledAttributes.getDimensionPixelSize(index, this.y);
                        break;
                    case 27:
                        this.S = obtainStyledAttributes.getBoolean(index, this.S);
                        break;
                    case 28:
                        this.T = obtainStyledAttributes.getBoolean(index, this.T);
                        break;
                    case 29:
                        this.z = obtainStyledAttributes.getFloat(index, this.z);
                        break;
                    case 30:
                        this.A = obtainStyledAttributes.getFloat(index, this.A);
                        break;
                    case 31:
                        this.H = obtainStyledAttributes.getInt(index, 0);
                        if (this.H != 1) {
                            break;
                        } else {
                            Log.e("ConstraintLayout", "layout_constraintWidth_default=\"wrap\" is deprecated.\nUse layout_width=\"WRAP_CONTENT\" and layout_constrainedWidth=\"true\" instead.");
                            break;
                        }
                    case 32:
                        this.I = obtainStyledAttributes.getInt(index, 0);
                        if (this.I != 1) {
                            break;
                        } else {
                            Log.e("ConstraintLayout", "layout_constraintHeight_default=\"wrap\" is deprecated.\nUse layout_height=\"WRAP_CONTENT\" and layout_constrainedHeight=\"true\" instead.");
                            break;
                        }
                    case 33:
                        try {
                            this.J = obtainStyledAttributes.getDimensionPixelSize(index, this.J);
                            break;
                        } catch (Exception unused) {
                            if (obtainStyledAttributes.getInt(index, this.J) != -2) {
                                break;
                            } else {
                                this.J = -2;
                                break;
                            }
                        }
                    case 34:
                        try {
                            this.L = obtainStyledAttributes.getDimensionPixelSize(index, this.L);
                            break;
                        } catch (Exception unused2) {
                            if (obtainStyledAttributes.getInt(index, this.L) != -2) {
                                break;
                            } else {
                                this.L = -2;
                                break;
                            }
                        }
                    case 35:
                        this.N = Math.max(0.0f, obtainStyledAttributes.getFloat(index, this.N));
                        break;
                    case 36:
                        try {
                            this.K = obtainStyledAttributes.getDimensionPixelSize(index, this.K);
                            break;
                        } catch (Exception unused3) {
                            if (obtainStyledAttributes.getInt(index, this.K) != -2) {
                                break;
                            } else {
                                this.K = -2;
                                break;
                            }
                        }
                    case 37:
                        try {
                            this.M = obtainStyledAttributes.getDimensionPixelSize(index, this.M);
                            break;
                        } catch (Exception unused4) {
                            if (obtainStyledAttributes.getInt(index, this.M) != -2) {
                                break;
                            } else {
                                this.M = -2;
                                break;
                            }
                        }
                    case 38:
                        this.O = Math.max(0.0f, obtainStyledAttributes.getFloat(index, this.O));
                        break;
                    case 44:
                        this.B = obtainStyledAttributes.getString(index);
                        this.C = -1;
                        String str = this.B;
                        if (str == null) {
                            break;
                        } else {
                            int length = str.length();
                            int indexOf = this.B.indexOf(44);
                            if (indexOf <= 0 || indexOf >= length - 1) {
                                i2 = 0;
                            } else {
                                String substring = this.B.substring(0, indexOf);
                                if (substring.equalsIgnoreCase("W")) {
                                    this.C = 0;
                                } else if (substring.equalsIgnoreCase("H")) {
                                    this.C = 1;
                                }
                                i2 = indexOf + 1;
                            }
                            int indexOf2 = this.B.indexOf(58);
                            if (indexOf2 >= 0 && indexOf2 < length - 1) {
                                String substring2 = this.B.substring(i2, indexOf2);
                                String substring3 = this.B.substring(indexOf2 + 1);
                                if (substring2.length() > 0 && substring3.length() > 0) {
                                    try {
                                        float parseFloat = Float.parseFloat(substring2);
                                        float parseFloat2 = Float.parseFloat(substring3);
                                        if (parseFloat > 0.0f && parseFloat2 > 0.0f) {
                                            int i4 = this.C;
                                            break;
                                        }
                                    } catch (NumberFormatException unused5) {
                                        break;
                                    }
                                }
                            } else {
                                int length2 = this.B.substring(i2).length();
                                break;
                            }
                        }
                        break;
                    case 45:
                        this.D = obtainStyledAttributes.getFloat(index, this.D);
                        break;
                    case 46:
                        this.E = obtainStyledAttributes.getFloat(index, this.E);
                        break;
                    case 47:
                        this.F = obtainStyledAttributes.getInt(index, 0);
                        break;
                    case 48:
                        this.G = obtainStyledAttributes.getInt(index, 0);
                        break;
                    case 49:
                        this.P = obtainStyledAttributes.getDimensionPixelOffset(index, this.P);
                        break;
                    case 50:
                        this.Q = obtainStyledAttributes.getDimensionPixelOffset(index, this.Q);
                        break;
                }
            }
            obtainStyledAttributes.recycle();
            a();
        }

        public void a() {
            this.X = false;
            this.U = true;
            this.V = true;
            if (this.width == -2 && this.S) {
                this.U = false;
                this.H = 1;
            }
            if (this.height == -2 && this.T) {
                this.V = false;
                this.I = 1;
            }
            if (this.width == 0 || this.width == -1) {
                this.U = false;
                if (this.width == 0 && this.H == 1) {
                    this.width = -2;
                    this.S = true;
                }
            }
            if (this.height == 0 || this.height == -1) {
                this.V = false;
                if (this.height == 0 && this.I == 1) {
                    this.height = -2;
                    this.T = true;
                }
            }
            if (this.c != -1.0f || this.f507a != -1 || this.b != -1) {
                this.X = true;
                this.U = true;
                this.V = true;
                if (!(this.k0 instanceof Guideline)) {
                    this.k0 = new Guideline();
                }
                ((Guideline) this.k0).v(this.R);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x004c  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x005a  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0060  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0066  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x007c  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0084  */
        @android.annotation.TargetApi(17)
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void resolveLayoutDirection(int r7) {
            /*
                r6 = this;
                int r0 = r6.leftMargin
                int r1 = r6.rightMargin
                super.resolveLayoutDirection(r7)
                r7 = -1
                r6.c0 = r7
                r6.d0 = r7
                r6.a0 = r7
                r6.b0 = r7
                r6.e0 = r7
                r6.f0 = r7
                int r2 = r6.t
                r6.e0 = r2
                int r2 = r6.v
                r6.f0 = r2
                float r2 = r6.z
                r6.g0 = r2
                int r2 = r6.f507a
                r6.h0 = r2
                int r2 = r6.b
                r6.i0 = r2
                float r2 = r6.c
                r6.j0 = r2
                int r2 = r6.getLayoutDirection()
                r3 = 0
                r4 = 1
                if (r4 != r2) goto L_0x0036
                r2 = 1
                goto L_0x0037
            L_0x0036:
                r2 = 0
            L_0x0037:
                if (r2 == 0) goto L_0x009a
                int r2 = r6.p
                if (r2 == r7) goto L_0x0041
                r6.c0 = r2
            L_0x003f:
                r3 = 1
                goto L_0x0048
            L_0x0041:
                int r2 = r6.q
                if (r2 == r7) goto L_0x0048
                r6.d0 = r2
                goto L_0x003f
            L_0x0048:
                int r2 = r6.r
                if (r2 == r7) goto L_0x004f
                r6.b0 = r2
                r3 = 1
            L_0x004f:
                int r2 = r6.s
                if (r2 == r7) goto L_0x0056
                r6.a0 = r2
                r3 = 1
            L_0x0056:
                int r2 = r6.x
                if (r2 == r7) goto L_0x005c
                r6.f0 = r2
            L_0x005c:
                int r2 = r6.y
                if (r2 == r7) goto L_0x0062
                r6.e0 = r2
            L_0x0062:
                r2 = 1065353216(0x3f800000, float:1.0)
                if (r3 == 0) goto L_0x006c
                float r3 = r6.z
                float r3 = r2 - r3
                r6.g0 = r3
            L_0x006c:
                boolean r3 = r6.X
                if (r3 == 0) goto L_0x00be
                int r3 = r6.R
                if (r3 != r4) goto L_0x00be
                float r3 = r6.c
                r4 = -1082130432(0xffffffffbf800000, float:-1.0)
                int r5 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
                if (r5 == 0) goto L_0x0084
                float r2 = r2 - r3
                r6.j0 = r2
                r6.h0 = r7
                r6.i0 = r7
                goto L_0x00be
            L_0x0084:
                int r2 = r6.f507a
                if (r2 == r7) goto L_0x008f
                r6.i0 = r2
                r6.h0 = r7
                r6.j0 = r4
                goto L_0x00be
            L_0x008f:
                int r2 = r6.b
                if (r2 == r7) goto L_0x00be
                r6.h0 = r2
                r6.i0 = r7
                r6.j0 = r4
                goto L_0x00be
            L_0x009a:
                int r2 = r6.p
                if (r2 == r7) goto L_0x00a0
                r6.b0 = r2
            L_0x00a0:
                int r2 = r6.q
                if (r2 == r7) goto L_0x00a6
                r6.a0 = r2
            L_0x00a6:
                int r2 = r6.r
                if (r2 == r7) goto L_0x00ac
                r6.c0 = r2
            L_0x00ac:
                int r2 = r6.s
                if (r2 == r7) goto L_0x00b2
                r6.d0 = r2
            L_0x00b2:
                int r2 = r6.x
                if (r2 == r7) goto L_0x00b8
                r6.e0 = r2
            L_0x00b8:
                int r2 = r6.y
                if (r2 == r7) goto L_0x00be
                r6.f0 = r2
            L_0x00be:
                int r2 = r6.r
                if (r2 != r7) goto L_0x0108
                int r2 = r6.s
                if (r2 != r7) goto L_0x0108
                int r2 = r6.q
                if (r2 != r7) goto L_0x0108
                int r2 = r6.p
                if (r2 != r7) goto L_0x0108
                int r2 = r6.f
                if (r2 == r7) goto L_0x00dd
                r6.c0 = r2
                int r2 = r6.rightMargin
                if (r2 > 0) goto L_0x00eb
                if (r1 <= 0) goto L_0x00eb
                r6.rightMargin = r1
                goto L_0x00eb
            L_0x00dd:
                int r2 = r6.g
                if (r2 == r7) goto L_0x00eb
                r6.d0 = r2
                int r2 = r6.rightMargin
                if (r2 > 0) goto L_0x00eb
                if (r1 <= 0) goto L_0x00eb
                r6.rightMargin = r1
            L_0x00eb:
                int r1 = r6.d
                if (r1 == r7) goto L_0x00fa
                r6.a0 = r1
                int r7 = r6.leftMargin
                if (r7 > 0) goto L_0x0108
                if (r0 <= 0) goto L_0x0108
                r6.leftMargin = r0
                goto L_0x0108
            L_0x00fa:
                int r1 = r6.e
                if (r1 == r7) goto L_0x0108
                r6.b0 = r1
                int r7 = r6.leftMargin
                if (r7 > 0) goto L_0x0108
                if (r0 <= 0) goto L_0x0108
                r6.leftMargin = r0
            L_0x0108:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.resolveLayoutDirection(int):void");
        }

        public LayoutParams(int i2, int i3) {
            super(i2, i3);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public final ConstraintWidget a(View view) {
        if (view == this) {
            return this.d;
        }
        if (view == null) {
            return null;
        }
        return ((LayoutParams) view.getLayoutParams()).k0;
    }

    private void a(int i2, int i3) {
        boolean z;
        boolean z2;
        int baseline;
        int i4;
        int i5;
        int i6 = i2;
        int i7 = i3;
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int childCount = getChildCount();
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                ConstraintWidget constraintWidget = layoutParams.k0;
                if (!layoutParams.X && !layoutParams.Y) {
                    constraintWidget.n(childAt.getVisibility());
                    int i9 = layoutParams.width;
                    int i10 = layoutParams.height;
                    boolean z3 = layoutParams.U;
                    if (z3 || layoutParams.V || (!z3 && layoutParams.H == 1) || layoutParams.width == -1 || (!layoutParams.V && (layoutParams.I == 1 || layoutParams.height == -1))) {
                        if (i9 == 0) {
                            i4 = ViewGroup.getChildMeasureSpec(i6, paddingLeft, -2);
                            z2 = true;
                        } else if (i9 == -1) {
                            i4 = ViewGroup.getChildMeasureSpec(i6, paddingLeft, -1);
                            z2 = false;
                        } else {
                            z2 = i9 == -2;
                            i4 = ViewGroup.getChildMeasureSpec(i6, paddingLeft, i9);
                        }
                        if (i10 == 0) {
                            i5 = ViewGroup.getChildMeasureSpec(i7, paddingTop, -2);
                            z = true;
                        } else if (i10 == -1) {
                            i5 = ViewGroup.getChildMeasureSpec(i7, paddingTop, -1);
                            z = false;
                        } else {
                            z = i10 == -2;
                            i5 = ViewGroup.getChildMeasureSpec(i7, paddingTop, i10);
                        }
                        childAt.measure(i4, i5);
                        Metrics metrics = this.p;
                        if (metrics != null) {
                            metrics.f486a++;
                        }
                        constraintWidget.b(i9 == -2);
                        constraintWidget.a(i10 == -2);
                        i9 = childAt.getMeasuredWidth();
                        i10 = childAt.getMeasuredHeight();
                    } else {
                        z2 = false;
                        z = false;
                    }
                    constraintWidget.o(i9);
                    constraintWidget.g(i10);
                    if (z2) {
                        constraintWidget.q(i9);
                    }
                    if (z) {
                        constraintWidget.p(i10);
                    }
                    if (layoutParams.W && (baseline = childAt.getBaseline()) != -1) {
                        constraintWidget.f(baseline);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.d.K();
        Metrics metrics = this.p;
        if (metrics != null) {
            metrics.c++;
        }
    }

    public View a(int i2) {
        return this.f506a.get(i2);
    }
}
