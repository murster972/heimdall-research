package androidx.constraintlayout.widget;

public final class R$id {
    public static final int bottom = 2131296408;
    public static final int end = 2131296587;
    public static final int gone = 2131296659;
    public static final int invisible = 2131296710;
    public static final int left = 2131296742;
    public static final int packed = 2131296934;
    public static final int parent = 2131296937;
    public static final int percent = 2131296946;
    public static final int right = 2131296985;
    public static final int spread = 2131297060;
    public static final int spread_inside = 2131297061;
    public static final int start = 2131297066;
    public static final int top = 2131297165;
    public static final int wrap = 2131297242;

    private R$id() {
    }
}
