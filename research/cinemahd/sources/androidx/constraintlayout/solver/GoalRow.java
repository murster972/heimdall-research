package androidx.constraintlayout.solver;

public class GoalRow extends ArrayRow {
    public GoalRow(Cache cache) {
        super(cache);
    }

    public void a(SolverVariable solverVariable) {
        super.a(solverVariable);
        solverVariable.j--;
    }
}
