package androidx.constraintlayout.solver;

interface Pools$Pool<T> {
    void a(T[] tArr, int i);

    T acquire();

    boolean release(T t);
}
