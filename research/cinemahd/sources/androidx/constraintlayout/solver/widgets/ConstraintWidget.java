package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.Cache;
import androidx.constraintlayout.solver.LinearSystem;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import java.util.ArrayList;

public class ConstraintWidget {
    public static float j0 = 0.5f;
    protected ConstraintAnchor[] A = {this.s, this.u, this.t, this.v, this.w, this.z};
    protected ArrayList<ConstraintAnchor> B = new ArrayList<>();
    protected DimensionBehaviour[] C;
    ConstraintWidget D;
    int E;
    int F;
    protected float G;
    protected int H;
    protected int I;
    protected int J;
    int K;
    int L;
    private int M;
    private int N;
    protected int O;
    protected int P;
    int Q;
    protected int R;
    protected int S;
    private int T;
    private int U;
    float V;
    float W;
    private Object X;
    private int Y;
    private String Z;

    /* renamed from: a  reason: collision with root package name */
    public int f496a = -1;
    private String a0;
    public int b = -1;
    boolean b0;
    ResolutionDimension c;
    boolean c0;
    ResolutionDimension d;
    boolean d0;
    int e = 0;
    int e0;
    int f = 0;
    int f0;
    int[] g = new int[2];
    float[] g0;
    int h = 0;
    protected ConstraintWidget[] h0;
    int i = 0;
    protected ConstraintWidget[] i0;
    float j = 1.0f;
    int k = 0;
    int l = 0;
    float m = 1.0f;
    int n = -1;
    float o = 1.0f;
    ConstraintWidgetGroup p = null;
    private int[] q = {Integer.MAX_VALUE, Integer.MAX_VALUE};
    private float r = 0.0f;
    ConstraintAnchor s = new ConstraintAnchor(this, ConstraintAnchor.Type.LEFT);
    ConstraintAnchor t = new ConstraintAnchor(this, ConstraintAnchor.Type.TOP);
    ConstraintAnchor u = new ConstraintAnchor(this, ConstraintAnchor.Type.RIGHT);
    ConstraintAnchor v = new ConstraintAnchor(this, ConstraintAnchor.Type.BOTTOM);
    ConstraintAnchor w = new ConstraintAnchor(this, ConstraintAnchor.Type.BASELINE);
    ConstraintAnchor x = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_X);
    ConstraintAnchor y = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_Y);
    ConstraintAnchor z = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER);

    /* renamed from: androidx.constraintlayout.solver.widgets.ConstraintWidget$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f497a = new int[ConstraintAnchor.Type.values().length];
        static final /* synthetic */ int[] b = new int[DimensionBehaviour.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|(3:33|34|36)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|(3:33|34|36)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(31:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0093 */
        static {
            /*
                androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b = r0
                r0 = 1
                int[] r1 = b     // Catch:{ NoSuchFieldError -> 0x0014 }
                androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r2 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x001f }
                androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = b     // Catch:{ NoSuchFieldError -> 0x002a }
                androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r4 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_PARENT     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = b     // Catch:{ NoSuchFieldError -> 0x0035 }
                androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r5 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type[] r4 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                f497a = r4
                int[] r4 = f497a     // Catch:{ NoSuchFieldError -> 0x0048 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r5 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT     // Catch:{ NoSuchFieldError -> 0x0048 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0048 }
                r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0048 }
            L_0x0048:
                int[] r0 = f497a     // Catch:{ NoSuchFieldError -> 0x0052 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r4 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP     // Catch:{ NoSuchFieldError -> 0x0052 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
                r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0052 }
            L_0x0052:
                int[] r0 = f497a     // Catch:{ NoSuchFieldError -> 0x005c }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT     // Catch:{ NoSuchFieldError -> 0x005c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005c }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005c }
            L_0x005c:
                int[] r0 = f497a     // Catch:{ NoSuchFieldError -> 0x0066 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM     // Catch:{ NoSuchFieldError -> 0x0066 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0066 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0066 }
            L_0x0066:
                int[] r0 = f497a     // Catch:{ NoSuchFieldError -> 0x0071 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BASELINE     // Catch:{ NoSuchFieldError -> 0x0071 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0071 }
            L_0x0071:
                int[] r0 = f497a     // Catch:{ NoSuchFieldError -> 0x007c }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER     // Catch:{ NoSuchFieldError -> 0x007c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007c }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007c }
            L_0x007c:
                int[] r0 = f497a     // Catch:{ NoSuchFieldError -> 0x0087 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER_X     // Catch:{ NoSuchFieldError -> 0x0087 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0087 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0087 }
            L_0x0087:
                int[] r0 = f497a     // Catch:{ NoSuchFieldError -> 0x0093 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER_Y     // Catch:{ NoSuchFieldError -> 0x0093 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0093 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0093 }
            L_0x0093:
                int[] r0 = f497a     // Catch:{ NoSuchFieldError -> 0x009f }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.NONE     // Catch:{ NoSuchFieldError -> 0x009f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009f }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009f }
            L_0x009f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.ConstraintWidget.AnonymousClass1.<clinit>():void");
        }
    }

    public enum DimensionBehaviour {
        FIXED,
        WRAP_CONTENT,
        MATCH_CONSTRAINT,
        MATCH_PARENT
    }

    public ConstraintWidget() {
        DimensionBehaviour dimensionBehaviour = DimensionBehaviour.FIXED;
        this.C = new DimensionBehaviour[]{dimensionBehaviour, dimensionBehaviour};
        this.D = null;
        this.E = 0;
        this.F = 0;
        this.G = 0.0f;
        this.H = -1;
        this.I = 0;
        this.J = 0;
        this.K = 0;
        this.L = 0;
        this.M = 0;
        this.N = 0;
        this.O = 0;
        this.P = 0;
        this.Q = 0;
        float f2 = j0;
        this.V = f2;
        this.W = f2;
        this.Y = 0;
        this.Z = null;
        this.a0 = null;
        this.b0 = false;
        this.c0 = false;
        this.d0 = false;
        this.e0 = 0;
        this.f0 = 0;
        this.g0 = new float[]{-1.0f, -1.0f};
        this.h0 = new ConstraintWidget[]{null, null};
        this.i0 = new ConstraintWidget[]{null, null};
        J();
    }

    private void J() {
        this.B.add(this.s);
        this.B.add(this.t);
        this.B.add(this.u);
        this.B.add(this.v);
        this.B.add(this.x);
        this.B.add(this.y);
        this.B.add(this.z);
        this.B.add(this.w);
    }

    public boolean A() {
        ConstraintAnchor constraintAnchor = this.t;
        ConstraintAnchor constraintAnchor2 = constraintAnchor.d;
        if (constraintAnchor2 != null && constraintAnchor2.d == constraintAnchor) {
            return true;
        }
        ConstraintAnchor constraintAnchor3 = this.v;
        ConstraintAnchor constraintAnchor4 = constraintAnchor3.d;
        return constraintAnchor4 != null && constraintAnchor4.d == constraintAnchor3;
    }

    public boolean B() {
        return this.f == 0 && this.G == 0.0f && this.k == 0 && this.l == 0 && this.C[1] == DimensionBehaviour.MATCH_CONSTRAINT;
    }

    public boolean C() {
        return this.e == 0 && this.G == 0.0f && this.h == 0 && this.i == 0 && this.C[0] == DimensionBehaviour.MATCH_CONSTRAINT;
    }

    public void D() {
        this.s.j();
        this.t.j();
        this.u.j();
        this.v.j();
        this.w.j();
        this.x.j();
        this.y.j();
        this.z.j();
        this.D = null;
        this.r = 0.0f;
        this.E = 0;
        this.F = 0;
        this.G = 0.0f;
        this.H = -1;
        this.I = 0;
        this.J = 0;
        this.M = 0;
        this.N = 0;
        this.O = 0;
        this.P = 0;
        this.Q = 0;
        this.R = 0;
        this.S = 0;
        this.T = 0;
        this.U = 0;
        float f2 = j0;
        this.V = f2;
        this.W = f2;
        DimensionBehaviour[] dimensionBehaviourArr = this.C;
        DimensionBehaviour dimensionBehaviour = DimensionBehaviour.FIXED;
        dimensionBehaviourArr[0] = dimensionBehaviour;
        dimensionBehaviourArr[1] = dimensionBehaviour;
        this.X = null;
        this.Y = 0;
        this.a0 = null;
        this.e0 = 0;
        this.f0 = 0;
        float[] fArr = this.g0;
        fArr[0] = -1.0f;
        fArr[1] = -1.0f;
        this.f496a = -1;
        this.b = -1;
        int[] iArr = this.q;
        iArr[0] = Integer.MAX_VALUE;
        iArr[1] = Integer.MAX_VALUE;
        this.e = 0;
        this.f = 0;
        this.j = 1.0f;
        this.m = 1.0f;
        this.i = Integer.MAX_VALUE;
        this.l = Integer.MAX_VALUE;
        this.h = 0;
        this.k = 0;
        this.n = -1;
        this.o = 1.0f;
        ResolutionDimension resolutionDimension = this.c;
        if (resolutionDimension != null) {
            resolutionDimension.d();
        }
        ResolutionDimension resolutionDimension2 = this.d;
        if (resolutionDimension2 != null) {
            resolutionDimension2.d();
        }
        this.p = null;
        this.b0 = false;
        this.c0 = false;
        this.d0 = false;
    }

    public void E() {
        ConstraintWidget k2 = k();
        if (k2 == null || !(k2 instanceof ConstraintWidgetContainer) || !((ConstraintWidgetContainer) k()).N()) {
            int size = this.B.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.B.get(i2).j();
            }
        }
    }

    public void F() {
        for (int i2 = 0; i2 < 6; i2++) {
            this.A[i2].d().d();
        }
    }

    public void G() {
    }

    public void H() {
        int i2 = this.I;
        int i3 = this.J;
        this.M = i2;
        this.N = i3;
    }

    public void I() {
        for (int i2 = 0; i2 < 6; i2++) {
            this.A[i2].d().g();
        }
    }

    public void a(int i2) {
        Optimizer.a(i2, this);
    }

    public void a(boolean z2) {
    }

    public void b(LinearSystem linearSystem) {
        linearSystem.a((Object) this.s);
        linearSystem.a((Object) this.t);
        linearSystem.a((Object) this.u);
        linearSystem.a((Object) this.v);
        if (this.Q > 0) {
            linearSystem.a((Object) this.w);
        }
    }

    public void b(boolean z2) {
    }

    public int c() {
        return this.Q;
    }

    public int d(int i2) {
        if (i2 == 0) {
            return s();
        }
        if (i2 == 1) {
            return i();
        }
        return 0;
    }

    public Object e() {
        return this.X;
    }

    public String f() {
        return this.Z;
    }

    public int g() {
        return this.M + this.O;
    }

    public int h() {
        return this.N + this.P;
    }

    public void i(int i2) {
        this.q[1] = i2;
    }

    public void j(int i2) {
        this.q[0] = i2;
    }

    public ConstraintWidget k() {
        return this.D;
    }

    public ResolutionDimension l() {
        if (this.d == null) {
            this.d = new ResolutionDimension();
        }
        return this.d;
    }

    public ResolutionDimension m() {
        if (this.c == null) {
            this.c = new ResolutionDimension();
        }
        return this.c;
    }

    public void n(int i2) {
        this.Y = i2;
    }

    /* access modifiers changed from: protected */
    public int o() {
        return this.I + this.O;
    }

    /* access modifiers changed from: protected */
    public int p() {
        return this.J + this.P;
    }

    public void q(int i2) {
        this.T = i2;
    }

    public int r() {
        return this.Y;
    }

    public int s() {
        if (this.Y == 8) {
            return 0;
        }
        return this.E;
    }

    public int t() {
        return this.U;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        if (this.a0 != null) {
            str = "type: " + this.a0 + " ";
        } else {
            str = str2;
        }
        sb.append(str);
        if (this.Z != null) {
            str2 = "id: " + this.Z + " ";
        }
        sb.append(str2);
        sb.append("(");
        sb.append(this.I);
        sb.append(", ");
        sb.append(this.J);
        sb.append(") - (");
        sb.append(this.E);
        sb.append(" x ");
        sb.append(this.F);
        sb.append(") wrap: (");
        sb.append(this.T);
        sb.append(" x ");
        sb.append(this.U);
        sb.append(")");
        return sb.toString();
    }

    public int u() {
        return this.T;
    }

    public int v() {
        return this.I;
    }

    public int w() {
        return this.J;
    }

    public boolean x() {
        return this.Q > 0;
    }

    public boolean y() {
        if (this.s.d().b == 1 && this.u.d().b == 1 && this.t.d().b == 1 && this.v.d().b == 1) {
            return true;
        }
        return false;
    }

    public boolean z() {
        ConstraintAnchor constraintAnchor = this.s;
        ConstraintAnchor constraintAnchor2 = constraintAnchor.d;
        if (constraintAnchor2 != null && constraintAnchor2.d == constraintAnchor) {
            return true;
        }
        ConstraintAnchor constraintAnchor3 = this.u;
        ConstraintAnchor constraintAnchor4 = constraintAnchor3.d;
        return constraintAnchor4 != null && constraintAnchor4.d == constraintAnchor3;
    }

    private boolean t(int i2) {
        int i3 = i2 * 2;
        ConstraintAnchor[] constraintAnchorArr = this.A;
        if (!(constraintAnchorArr[i3].d == null || constraintAnchorArr[i3].d.d == constraintAnchorArr[i3])) {
            int i4 = i3 + 1;
            return constraintAnchorArr[i4].d != null && constraintAnchorArr[i4].d.d == constraintAnchorArr[i4];
        }
    }

    public void a(Cache cache) {
        this.s.a(cache);
        this.t.a(cache);
        this.u.a(cache);
        this.v.a(cache);
        this.w.a(cache);
        this.z.a(cache);
        this.x.a(cache);
        this.y.a(cache);
    }

    public void c(int i2, int i3) {
        this.I = i2;
        this.J = i3;
    }

    public void e(int i2, int i3) {
        this.J = i2;
        this.F = i3 - i2;
        int i4 = this.F;
        int i5 = this.S;
        if (i4 < i5) {
            this.F = i5;
        }
    }

    public void f(int i2) {
        this.Q = i2;
    }

    public void g(int i2) {
        this.F = i2;
        int i3 = this.F;
        int i4 = this.S;
        if (i3 < i4) {
            this.F = i4;
        }
    }

    public void h(int i2) {
        this.e0 = i2;
    }

    public int i() {
        if (this.Y == 8) {
            return 0;
        }
        return this.F;
    }

    public DimensionBehaviour j() {
        return this.C[0];
    }

    public void k(int i2) {
        if (i2 < 0) {
            this.S = 0;
        } else {
            this.S = i2;
        }
    }

    public int n() {
        return v() + this.E;
    }

    public void o(int i2) {
        this.E = i2;
        int i3 = this.E;
        int i4 = this.R;
        if (i3 < i4) {
            this.E = i4;
        }
    }

    public void p(int i2) {
        this.U = i2;
    }

    public DimensionBehaviour q() {
        return this.C[1];
    }

    public void r(int i2) {
        this.I = i2;
    }

    public int d() {
        return w() + this.F;
    }

    public void s(int i2) {
        this.J = i2;
    }

    public void c(float f2) {
        this.W = f2;
    }

    /* access modifiers changed from: package-private */
    public void d(int i2, int i3) {
        if (i3 == 0) {
            this.K = i2;
        } else if (i3 == 1) {
            this.L = i2;
        }
    }

    public void l(int i2) {
        if (i2 < 0) {
            this.R = 0;
        } else {
            this.R = i2;
        }
    }

    public void m(int i2) {
        this.f0 = i2;
    }

    public DimensionBehaviour c(int i2) {
        if (i2 == 0) {
            return j();
        }
        if (i2 == 1) {
            return q();
        }
        return null;
    }

    public void d(float f2) {
        this.g0[1] = f2;
    }

    /* access modifiers changed from: package-private */
    public int e(int i2) {
        if (i2 == 0) {
            return this.K;
        }
        if (i2 == 1) {
            return this.L;
        }
        return 0;
    }

    public float b(int i2) {
        if (i2 == 0) {
            return this.V;
        }
        if (i2 == 1) {
            return this.W;
        }
        return -1.0f;
    }

    public void c(LinearSystem linearSystem) {
        int b2 = linearSystem.b((Object) this.s);
        int b3 = linearSystem.b((Object) this.t);
        int b4 = linearSystem.b((Object) this.u);
        int b5 = linearSystem.b((Object) this.v);
        int i2 = b5 - b3;
        if (b4 - b2 < 0 || i2 < 0 || b2 == Integer.MIN_VALUE || b2 == Integer.MAX_VALUE || b3 == Integer.MIN_VALUE || b3 == Integer.MAX_VALUE || b4 == Integer.MIN_VALUE || b4 == Integer.MAX_VALUE || b5 == Integer.MIN_VALUE || b5 == Integer.MAX_VALUE) {
            b5 = 0;
            b2 = 0;
            b3 = 0;
            b4 = 0;
        }
        a(b2, b3, b4, b5);
    }

    public ArrayList<ConstraintAnchor> b() {
        return this.B;
    }

    public void a(ConstraintWidget constraintWidget) {
        this.D = constraintWidget;
    }

    public void b(int i2, int i3) {
        this.O = i2;
        this.P = i3;
    }

    public void a(ConstraintWidget constraintWidget, float f2, int i2) {
        ConstraintAnchor.Type type = ConstraintAnchor.Type.CENTER;
        a(type, constraintWidget, type, i2, 0);
        this.r = f2;
    }

    public void b(int i2, int i3, int i4, float f2) {
        this.f = i2;
        this.k = i3;
        this.l = i4;
        this.m = f2;
        if (f2 < 1.0f && this.f == 0) {
            this.f = 2;
        }
    }

    public void a(String str) {
        this.Z = str;
    }

    public void a(int i2, int i3, int i4, float f2) {
        this.e = i2;
        this.h = i3;
        this.i = i4;
        this.j = f2;
        if (f2 < 1.0f && this.e == 0) {
            this.e = 2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(java.lang.String r9) {
        /*
            r8 = this;
            r0 = 0
            if (r9 == 0) goto L_0x008e
            int r1 = r9.length()
            if (r1 != 0) goto L_0x000b
            goto L_0x008e
        L_0x000b:
            r1 = -1
            int r2 = r9.length()
            r3 = 44
            int r3 = r9.indexOf(r3)
            r4 = 0
            r5 = 1
            if (r3 <= 0) goto L_0x0037
            int r6 = r2 + -1
            if (r3 >= r6) goto L_0x0037
            java.lang.String r6 = r9.substring(r4, r3)
            java.lang.String r7 = "W"
            boolean r7 = r6.equalsIgnoreCase(r7)
            if (r7 == 0) goto L_0x002c
            r1 = 0
            goto L_0x0035
        L_0x002c:
            java.lang.String r4 = "H"
            boolean r4 = r6.equalsIgnoreCase(r4)
            if (r4 == 0) goto L_0x0035
            r1 = 1
        L_0x0035:
            int r4 = r3 + 1
        L_0x0037:
            r3 = 58
            int r3 = r9.indexOf(r3)
            if (r3 < 0) goto L_0x0075
            int r2 = r2 - r5
            if (r3 >= r2) goto L_0x0075
            java.lang.String r2 = r9.substring(r4, r3)
            int r3 = r3 + r5
            java.lang.String r9 = r9.substring(r3)
            int r3 = r2.length()
            if (r3 <= 0) goto L_0x0084
            int r3 = r9.length()
            if (r3 <= 0) goto L_0x0084
            float r2 = java.lang.Float.parseFloat(r2)     // Catch:{ NumberFormatException -> 0x0084 }
            float r9 = java.lang.Float.parseFloat(r9)     // Catch:{ NumberFormatException -> 0x0084 }
            int r3 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r3 <= 0) goto L_0x0084
            int r3 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
            if (r3 <= 0) goto L_0x0084
            if (r1 != r5) goto L_0x006f
            float r9 = r9 / r2
            float r9 = java.lang.Math.abs(r9)     // Catch:{ NumberFormatException -> 0x0084 }
            goto L_0x0085
        L_0x006f:
            float r2 = r2 / r9
            float r9 = java.lang.Math.abs(r2)     // Catch:{ NumberFormatException -> 0x0084 }
            goto L_0x0085
        L_0x0075:
            java.lang.String r9 = r9.substring(r4)
            int r2 = r9.length()
            if (r2 <= 0) goto L_0x0084
            float r9 = java.lang.Float.parseFloat(r9)     // Catch:{ NumberFormatException -> 0x0084 }
            goto L_0x0085
        L_0x0084:
            r9 = 0
        L_0x0085:
            int r0 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x008d
            r8.G = r9
            r8.H = r1
        L_0x008d:
            return
        L_0x008e:
            r8.G = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.ConstraintWidget.b(java.lang.String):void");
    }

    public void a(float f2) {
        this.V = f2;
    }

    public void a(int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8 = i4 - i2;
        int i9 = i5 - i3;
        this.I = i2;
        this.J = i3;
        if (this.Y == 8) {
            this.E = 0;
            this.F = 0;
            return;
        }
        if (this.C[0] != DimensionBehaviour.FIXED || i8 >= (i6 = this.E)) {
            i6 = i8;
        }
        if (this.C[1] != DimensionBehaviour.FIXED || i9 >= (i7 = this.F)) {
            i7 = i9;
        }
        this.E = i6;
        this.F = i7;
        int i10 = this.F;
        int i11 = this.S;
        if (i10 < i11) {
            this.F = i11;
        }
        int i12 = this.E;
        int i13 = this.R;
        if (i12 < i13) {
            this.E = i13;
        }
        this.c0 = true;
    }

    public void a(int i2, int i3, int i4) {
        if (i4 == 0) {
            a(i2, i3);
        } else if (i4 == 1) {
            e(i2, i3);
        }
        this.c0 = true;
    }

    public void a(int i2, int i3) {
        this.I = i2;
        this.E = i3 - i2;
        int i4 = this.E;
        int i5 = this.R;
        if (i4 < i5) {
            this.E = i5;
        }
    }

    public void b(float f2) {
        this.g0[0] = f2;
    }

    public void b(DimensionBehaviour dimensionBehaviour) {
        this.C[1] = dimensionBehaviour;
        if (dimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
            g(this.U);
        }
    }

    public void a(Object obj) {
        this.X = obj;
    }

    public boolean a() {
        return this.Y != 8;
    }

    public void a(ConstraintAnchor.Type type, ConstraintWidget constraintWidget, ConstraintAnchor.Type type2, int i2, int i3) {
        a(type).a(constraintWidget.a(type2), i2, i3, ConstraintAnchor.Strength.STRONG, 0, true);
    }

    public ConstraintAnchor a(ConstraintAnchor.Type type) {
        switch (AnonymousClass1.f497a[type.ordinal()]) {
            case 1:
                return this.s;
            case 2:
                return this.t;
            case 3:
                return this.u;
            case 4:
                return this.v;
            case 5:
                return this.w;
            case 6:
                return this.z;
            case 7:
                return this.x;
            case 8:
                return this.y;
            case 9:
                return null;
            default:
                throw new AssertionError(type.name());
        }
    }

    public void a(DimensionBehaviour dimensionBehaviour) {
        this.C[0] = dimensionBehaviour;
        if (dimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
            o(this.T);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01bb  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01be  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x01d0  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0237  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0248 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0249  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x02aa  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x02b3  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x02b9  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x02c1  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x02f8  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0321  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x032b  */
    /* JADX WARNING: Removed duplicated region for block: B:170:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.constraintlayout.solver.LinearSystem r39) {
        /*
            r38 = this;
            r15 = r38
            r14 = r39
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r15.s
            androidx.constraintlayout.solver.SolverVariable r21 = r14.a((java.lang.Object) r0)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r15.u
            androidx.constraintlayout.solver.SolverVariable r10 = r14.a((java.lang.Object) r0)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r15.t
            androidx.constraintlayout.solver.SolverVariable r6 = r14.a((java.lang.Object) r0)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r15.v
            androidx.constraintlayout.solver.SolverVariable r4 = r14.a((java.lang.Object) r0)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r15.w
            androidx.constraintlayout.solver.SolverVariable r3 = r14.a((java.lang.Object) r0)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r0 = r15.D
            r1 = 8
            r2 = 1
            r13 = 0
            if (r0 == 0) goto L_0x00b0
            if (r0 == 0) goto L_0x0036
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r0.C
            r0 = r0[r13]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r5 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r0 != r5) goto L_0x0036
            r0 = 1
            goto L_0x0037
        L_0x0036:
            r0 = 0
        L_0x0037:
            androidx.constraintlayout.solver.widgets.ConstraintWidget r5 = r15.D
            if (r5 == 0) goto L_0x0045
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r5 = r5.C
            r5 = r5[r2]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r7 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r5 != r7) goto L_0x0045
            r5 = 1
            goto L_0x0046
        L_0x0045:
            r5 = 0
        L_0x0046:
            boolean r7 = r15.t(r13)
            if (r7 == 0) goto L_0x0055
            androidx.constraintlayout.solver.widgets.ConstraintWidget r7 = r15.D
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r7 = (androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer) r7
            r7.a((androidx.constraintlayout.solver.widgets.ConstraintWidget) r15, (int) r13)
            r7 = 1
            goto L_0x0059
        L_0x0055:
            boolean r7 = r38.z()
        L_0x0059:
            boolean r8 = r15.t(r2)
            if (r8 == 0) goto L_0x0068
            androidx.constraintlayout.solver.widgets.ConstraintWidget r8 = r15.D
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r8 = (androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer) r8
            r8.a((androidx.constraintlayout.solver.widgets.ConstraintWidget) r15, (int) r2)
            r8 = 1
            goto L_0x006c
        L_0x0068:
            boolean r8 = r38.A()
        L_0x006c:
            if (r0 == 0) goto L_0x0089
            int r9 = r15.Y
            if (r9 == r1) goto L_0x0089
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r15.s
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r9.d
            if (r9 != 0) goto L_0x0089
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r15.u
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r9.d
            if (r9 != 0) goto L_0x0089
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r15.D
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r9.u
            androidx.constraintlayout.solver.SolverVariable r9 = r14.a((java.lang.Object) r9)
            r14.b(r9, r10, r13, r2)
        L_0x0089:
            if (r5 == 0) goto L_0x00aa
            int r9 = r15.Y
            if (r9 == r1) goto L_0x00aa
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r15.t
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r9.d
            if (r9 != 0) goto L_0x00aa
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r15.v
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r9.d
            if (r9 != 0) goto L_0x00aa
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r15.w
            if (r9 != 0) goto L_0x00aa
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r15.D
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r9 = r9.v
            androidx.constraintlayout.solver.SolverVariable r9 = r14.a((java.lang.Object) r9)
            r14.b(r9, r4, r13, r2)
        L_0x00aa:
            r12 = r5
            r16 = r7
            r22 = r8
            goto L_0x00b6
        L_0x00b0:
            r0 = 0
            r12 = 0
            r16 = 0
            r22 = 0
        L_0x00b6:
            int r5 = r15.E
            int r7 = r15.R
            if (r5 >= r7) goto L_0x00bd
            r5 = r7
        L_0x00bd:
            int r7 = r15.F
            int r8 = r15.S
            if (r7 >= r8) goto L_0x00c4
            r7 = r8
        L_0x00c4:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r8 = r15.C
            r8 = r8[r13]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r9 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r8 == r9) goto L_0x00ce
            r8 = 1
            goto L_0x00cf
        L_0x00ce:
            r8 = 0
        L_0x00cf:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r9 = r15.C
            r9 = r9[r2]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r11 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r9 == r11) goto L_0x00d9
            r9 = 1
            goto L_0x00da
        L_0x00d9:
            r9 = 0
        L_0x00da:
            int r11 = r15.H
            r15.n = r11
            float r11 = r15.G
            r15.o = r11
            int r2 = r15.e
            int r13 = r15.f
            r18 = 0
            r19 = 4
            int r11 = (r11 > r18 ? 1 : (r11 == r18 ? 0 : -1))
            if (r11 <= 0) goto L_0x018b
            int r11 = r15.Y
            r1 = 8
            if (r11 == r1) goto L_0x018b
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r1 = r15.C
            r11 = 0
            r1 = r1[r11]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r11 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            r23 = r3
            if (r1 != r11) goto L_0x0102
            if (r2 != 0) goto L_0x0102
            r2 = 3
        L_0x0102:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r1 = r15.C
            r11 = 1
            r1 = r1[r11]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r11 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r1 != r11) goto L_0x010e
            if (r13 != 0) goto L_0x010e
            r13 = 3
        L_0x010e:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r1 = r15.C
            r11 = 0
            r3 = r1[r11]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r11 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r3 != r11) goto L_0x0125
            r3 = 1
            r1 = r1[r3]
            if (r1 != r11) goto L_0x0125
            r1 = 3
            if (r2 != r1) goto L_0x0126
            if (r13 != r1) goto L_0x0126
            r15.a((boolean) r0, (boolean) r12, (boolean) r8, (boolean) r9)
            goto L_0x0180
        L_0x0125:
            r1 = 3
        L_0x0126:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r3 = r15.C
            r8 = 0
            r9 = r3[r8]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r11 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r9 != r11) goto L_0x014c
            if (r2 != r1) goto L_0x014c
            r15.n = r8
            float r1 = r15.o
            int r5 = r15.F
            float r5 = (float) r5
            float r1 = r1 * r5
            int r1 = (int) r1
            r8 = 1
            r3 = r3[r8]
            r28 = r1
            if (r3 == r11) goto L_0x0149
            r29 = r7
            r26 = r13
            r25 = 4
            goto L_0x0195
        L_0x0149:
            r25 = r2
            goto L_0x0184
        L_0x014c:
            r8 = 1
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r1 = r15.C
            r1 = r1[r8]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r1 != r3) goto L_0x0180
            r1 = 3
            if (r13 != r1) goto L_0x0180
            r15.n = r8
            int r1 = r15.H
            r3 = -1
            if (r1 != r3) goto L_0x0166
            r1 = 1065353216(0x3f800000, float:1.0)
            float r3 = r15.o
            float r1 = r1 / r3
            r15.o = r1
        L_0x0166:
            float r1 = r15.o
            int r3 = r15.E
            float r3 = (float) r3
            float r1 = r1 * r3
            int r1 = (int) r1
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r3 = r15.C
            r7 = 0
            r3 = r3[r7]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r7 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            r29 = r1
            r25 = r2
            r28 = r5
            if (r3 == r7) goto L_0x0186
            r26 = 4
            goto L_0x0195
        L_0x0180:
            r25 = r2
            r28 = r5
        L_0x0184:
            r29 = r7
        L_0x0186:
            r26 = r13
            r27 = 1
            goto L_0x0197
        L_0x018b:
            r23 = r3
            r25 = r2
            r28 = r5
            r29 = r7
            r26 = r13
        L_0x0195:
            r27 = 0
        L_0x0197:
            int[] r1 = r15.g
            r2 = 0
            r1[r2] = r25
            r2 = 1
            r1[r2] = r26
            if (r27 == 0) goto L_0x01ab
            int r1 = r15.n
            r2 = -1
            if (r1 == 0) goto L_0x01a8
            if (r1 != r2) goto L_0x01ac
        L_0x01a8:
            r24 = 1
            goto L_0x01ae
        L_0x01ab:
            r2 = -1
        L_0x01ac:
            r24 = 0
        L_0x01ae:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r1 = r15.C
            r3 = 0
            r1 = r1[r3]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r1 != r3) goto L_0x01be
            boolean r1 = r15 instanceof androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer
            if (r1 == 0) goto L_0x01be
            r30 = 1
            goto L_0x01c0
        L_0x01be:
            r30 = 0
        L_0x01c0:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r1 = r15.z
            boolean r1 = r1.i()
            r3 = 1
            r31 = r1 ^ 1
            int r1 = r15.f496a
            r13 = 2
            r32 = 0
            if (r1 == r13) goto L_0x0237
            androidx.constraintlayout.solver.widgets.ConstraintWidget r1 = r15.D
            if (r1 == 0) goto L_0x01dd
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r1 = r1.u
            androidx.constraintlayout.solver.SolverVariable r1 = r14.a((java.lang.Object) r1)
            r20 = r1
            goto L_0x01df
        L_0x01dd:
            r20 = r32
        L_0x01df:
            androidx.constraintlayout.solver.widgets.ConstraintWidget r1 = r15.D
            if (r1 == 0) goto L_0x01ec
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r1 = r1.s
            androidx.constraintlayout.solver.SolverVariable r1 = r14.a((java.lang.Object) r1)
            r33 = r1
            goto L_0x01ee
        L_0x01ec:
            r33 = r32
        L_0x01ee:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r1 = r15.C
            r17 = 0
            r5 = r1[r17]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r7 = r15.s
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r8 = r15.u
            int r9 = r15.I
            int r11 = r15.R
            int[] r1 = r15.q
            r1 = r1[r17]
            r34 = r12
            r12 = r1
            float r1 = r15.V
            r13 = r1
            int r1 = r15.h
            r17 = r1
            int r1 = r15.i
            r18 = r1
            float r1 = r15.j
            r19 = r1
            r35 = r0
            r0 = r38
            r1 = r39
            r3 = -1
            r2 = r35
            r36 = r23
            r3 = r33
            r23 = r4
            r4 = r20
            r37 = r6
            r6 = r30
            r30 = r10
            r10 = r28
            r14 = r24
            r15 = r16
            r16 = r25
            r20 = r31
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            goto L_0x0241
        L_0x0237:
            r37 = r6
            r30 = r10
            r34 = r12
            r36 = r23
            r23 = r4
        L_0x0241:
            r15 = r38
            int r0 = r15.b
            r1 = 2
            if (r0 != r1) goto L_0x0249
            return
        L_0x0249:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r15.C
            r14 = 1
            r0 = r0[r14]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r1 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r0 != r1) goto L_0x0258
            boolean r0 = r15 instanceof androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer
            if (r0 == 0) goto L_0x0258
            r6 = 1
            goto L_0x0259
        L_0x0258:
            r6 = 0
        L_0x0259:
            if (r27 == 0) goto L_0x0265
            int r0 = r15.n
            if (r0 == r14) goto L_0x0262
            r1 = -1
            if (r0 != r1) goto L_0x0265
        L_0x0262:
            r16 = 1
            goto L_0x0267
        L_0x0265:
            r16 = 0
        L_0x0267:
            int r0 = r15.Q
            if (r0 <= 0) goto L_0x02a0
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r15.w
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r0 = r0.d()
            int r0 = r0.b
            if (r0 != r14) goto L_0x0281
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r15.w
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r0 = r0.d()
            r10 = r39
            r0.a((androidx.constraintlayout.solver.LinearSystem) r10)
            goto L_0x02a2
        L_0x0281:
            r10 = r39
            int r0 = r38.c()
            r1 = 6
            r2 = r36
            r4 = r37
            r10.a((androidx.constraintlayout.solver.SolverVariable) r2, (androidx.constraintlayout.solver.SolverVariable) r4, (int) r0, (int) r1)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r15.w
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r0.d
            if (r0 == 0) goto L_0x02a4
            androidx.constraintlayout.solver.SolverVariable r0 = r10.a((java.lang.Object) r0)
            r3 = 0
            r10.a((androidx.constraintlayout.solver.SolverVariable) r2, (androidx.constraintlayout.solver.SolverVariable) r0, (int) r3, (int) r1)
            r20 = 0
            goto L_0x02a6
        L_0x02a0:
            r10 = r39
        L_0x02a2:
            r4 = r37
        L_0x02a4:
            r20 = r31
        L_0x02a6:
            androidx.constraintlayout.solver.widgets.ConstraintWidget r0 = r15.D
            if (r0 == 0) goto L_0x02b3
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r0.v
            androidx.constraintlayout.solver.SolverVariable r0 = r10.a((java.lang.Object) r0)
            r24 = r0
            goto L_0x02b5
        L_0x02b3:
            r24 = r32
        L_0x02b5:
            androidx.constraintlayout.solver.widgets.ConstraintWidget r0 = r15.D
            if (r0 == 0) goto L_0x02c1
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r0.t
            androidx.constraintlayout.solver.SolverVariable r0 = r10.a((java.lang.Object) r0)
            r3 = r0
            goto L_0x02c3
        L_0x02c1:
            r3 = r32
        L_0x02c3:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r15.C
            r5 = r0[r14]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r7 = r15.t
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r8 = r15.v
            int r9 = r15.J
            int r11 = r15.S
            int[] r0 = r15.q
            r12 = r0[r14]
            float r13 = r15.W
            int r0 = r15.k
            r17 = r0
            int r0 = r15.l
            r18 = r0
            float r0 = r15.m
            r19 = r0
            r0 = r38
            r1 = r39
            r2 = r34
            r25 = r4
            r4 = r24
            r10 = r29
            r14 = r16
            r15 = r22
            r16 = r26
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            if (r27 == 0) goto L_0x0321
            r6 = 6
            r7 = r38
            int r0 = r7.n
            r1 = 1
            if (r0 != r1) goto L_0x0310
            float r5 = r7.o
            r0 = r39
            r1 = r23
            r2 = r25
            r3 = r30
            r4 = r21
            r0.a((androidx.constraintlayout.solver.SolverVariable) r1, (androidx.constraintlayout.solver.SolverVariable) r2, (androidx.constraintlayout.solver.SolverVariable) r3, (androidx.constraintlayout.solver.SolverVariable) r4, (float) r5, (int) r6)
            goto L_0x0323
        L_0x0310:
            float r5 = r7.o
            r6 = 6
            r0 = r39
            r1 = r30
            r2 = r21
            r3 = r23
            r4 = r25
            r0.a((androidx.constraintlayout.solver.SolverVariable) r1, (androidx.constraintlayout.solver.SolverVariable) r2, (androidx.constraintlayout.solver.SolverVariable) r3, (androidx.constraintlayout.solver.SolverVariable) r4, (float) r5, (int) r6)
            goto L_0x0323
        L_0x0321:
            r7 = r38
        L_0x0323:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r7.z
            boolean r0 = r0.i()
            if (r0 == 0) goto L_0x034b
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r7.z
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r0.g()
            androidx.constraintlayout.solver.widgets.ConstraintWidget r0 = r0.c()
            float r1 = r7.r
            r2 = 1119092736(0x42b40000, float:90.0)
            float r1 = r1 + r2
            double r1 = (double) r1
            double r1 = java.lang.Math.toRadians(r1)
            float r1 = (float) r1
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r7.z
            int r2 = r2.b()
            r3 = r39
            r3.a((androidx.constraintlayout.solver.widgets.ConstraintWidget) r7, (androidx.constraintlayout.solver.widgets.ConstraintWidget) r0, (float) r1, (int) r2)
        L_0x034b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.ConstraintWidget.a(androidx.constraintlayout.solver.LinearSystem):void");
    }

    public void a(boolean z2, boolean z3, boolean z4, boolean z5) {
        if (this.n == -1) {
            if (z4 && !z5) {
                this.n = 0;
            } else if (!z4 && z5) {
                this.n = 1;
                if (this.H == -1) {
                    this.o = 1.0f / this.o;
                }
            }
        }
        if (this.n == 0 && (!this.t.i() || !this.v.i())) {
            this.n = 1;
        } else if (this.n == 1 && (!this.s.i() || !this.u.i())) {
            this.n = 0;
        }
        if (this.n == -1 && (!this.t.i() || !this.v.i() || !this.s.i() || !this.u.i())) {
            if (this.t.i() && this.v.i()) {
                this.n = 0;
            } else if (this.s.i() && this.u.i()) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1) {
            if (z2 && !z3) {
                this.n = 0;
            } else if (!z2 && z3) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1) {
            if (this.h > 0 && this.k == 0) {
                this.n = 0;
            } else if (this.h == 0 && this.k > 0) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1 && z2 && z3) {
            this.o = 1.0f / this.o;
            this.n = 1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:154:0x02a2  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x02e5  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x02f4  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x0313  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x031c  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0323  */
    /* JADX WARNING: Removed duplicated region for block: B:189:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01d8 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(androidx.constraintlayout.solver.LinearSystem r26, boolean r27, androidx.constraintlayout.solver.SolverVariable r28, androidx.constraintlayout.solver.SolverVariable r29, androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour r30, boolean r31, androidx.constraintlayout.solver.widgets.ConstraintAnchor r32, androidx.constraintlayout.solver.widgets.ConstraintAnchor r33, int r34, int r35, int r36, int r37, float r38, boolean r39, boolean r40, int r41, int r42, int r43, float r44, boolean r45) {
        /*
            r25 = this;
            r0 = r25
            r10 = r26
            r11 = r28
            r12 = r29
            r13 = r32
            r14 = r33
            r1 = r36
            r2 = r37
            androidx.constraintlayout.solver.SolverVariable r15 = r10.a((java.lang.Object) r13)
            androidx.constraintlayout.solver.SolverVariable r9 = r10.a((java.lang.Object) r14)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r32.g()
            androidx.constraintlayout.solver.SolverVariable r8 = r10.a((java.lang.Object) r3)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r33.g()
            androidx.constraintlayout.solver.SolverVariable r7 = r10.a((java.lang.Object) r3)
            boolean r3 = r10.g
            r6 = 1
            r4 = 6
            r5 = 0
            if (r3 == 0) goto L_0x0066
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r3 = r32.d()
            int r3 = r3.b
            if (r3 != r6) goto L_0x0066
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r3 = r33.d()
            int r3 = r3.b
            if (r3 != r6) goto L_0x0066
            androidx.constraintlayout.solver.Metrics r1 = androidx.constraintlayout.solver.LinearSystem.h()
            if (r1 == 0) goto L_0x0050
            androidx.constraintlayout.solver.Metrics r1 = androidx.constraintlayout.solver.LinearSystem.h()
            long r2 = r1.s
            r6 = 1
            long r2 = r2 + r6
            r1.s = r2
        L_0x0050:
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r1 = r32.d()
            r1.a((androidx.constraintlayout.solver.LinearSystem) r10)
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r1 = r33.d()
            r1.a((androidx.constraintlayout.solver.LinearSystem) r10)
            if (r40 != 0) goto L_0x0065
            if (r27 == 0) goto L_0x0065
            r10.b(r12, r9, r5, r4)
        L_0x0065:
            return
        L_0x0066:
            androidx.constraintlayout.solver.Metrics r3 = androidx.constraintlayout.solver.LinearSystem.h()
            if (r3 == 0) goto L_0x0078
            androidx.constraintlayout.solver.Metrics r3 = androidx.constraintlayout.solver.LinearSystem.h()
            long r4 = r3.B
            r16 = 1
            long r4 = r4 + r16
            r3.B = r4
        L_0x0078:
            boolean r16 = r32.i()
            boolean r17 = r33.i()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r0.z
            boolean r20 = r3.i()
            if (r16 == 0) goto L_0x008a
            r3 = 1
            goto L_0x008b
        L_0x008a:
            r3 = 0
        L_0x008b:
            if (r17 == 0) goto L_0x008f
            int r3 = r3 + 1
        L_0x008f:
            if (r20 == 0) goto L_0x0093
            int r3 = r3 + 1
        L_0x0093:
            r5 = r3
            if (r39 == 0) goto L_0x0098
            r3 = 3
            goto L_0x009a
        L_0x0098:
            r3 = r41
        L_0x009a:
            int[] r21 = androidx.constraintlayout.solver.widgets.ConstraintWidget.AnonymousClass1.b
            int r22 = r30.ordinal()
            r4 = r21[r22]
            r14 = 2
            r13 = 4
            if (r4 == r6) goto L_0x00ad
            if (r4 == r14) goto L_0x00ad
            r14 = 3
            if (r4 == r14) goto L_0x00ad
            if (r4 == r13) goto L_0x00af
        L_0x00ad:
            r4 = 0
            goto L_0x00b3
        L_0x00af:
            if (r3 != r13) goto L_0x00b2
            goto L_0x00ad
        L_0x00b2:
            r4 = 1
        L_0x00b3:
            int r14 = r0.Y
            r13 = 8
            if (r14 != r13) goto L_0x00bc
            r4 = 0
            r13 = 0
            goto L_0x00bf
        L_0x00bc:
            r13 = r4
            r4 = r35
        L_0x00bf:
            if (r45 == 0) goto L_0x00da
            if (r16 != 0) goto L_0x00cd
            if (r17 != 0) goto L_0x00cd
            if (r20 != 0) goto L_0x00cd
            r14 = r34
            r10.a((androidx.constraintlayout.solver.SolverVariable) r15, (int) r14)
            goto L_0x00da
        L_0x00cd:
            if (r16 == 0) goto L_0x00da
            if (r17 != 0) goto L_0x00da
            int r14 = r32.b()
            r6 = 6
            r10.a((androidx.constraintlayout.solver.SolverVariable) r15, (androidx.constraintlayout.solver.SolverVariable) r8, (int) r14, (int) r6)
            goto L_0x00db
        L_0x00da:
            r6 = 6
        L_0x00db:
            if (r13 != 0) goto L_0x0107
            if (r31 == 0) goto L_0x00f4
            r6 = 0
            r14 = 3
            r10.a((androidx.constraintlayout.solver.SolverVariable) r9, (androidx.constraintlayout.solver.SolverVariable) r15, (int) r6, (int) r14)
            r4 = 6
            if (r1 <= 0) goto L_0x00ea
            r10.b(r9, r15, r1, r4)
        L_0x00ea:
            r6 = 2147483647(0x7fffffff, float:NaN)
            if (r2 >= r6) goto L_0x00f2
            r10.c(r9, r15, r2, r4)
        L_0x00f2:
            r6 = 6
            goto L_0x00f8
        L_0x00f4:
            r14 = 3
            r10.a((androidx.constraintlayout.solver.SolverVariable) r9, (androidx.constraintlayout.solver.SolverVariable) r15, (int) r4, (int) r6)
        L_0x00f8:
            r14 = r43
            r34 = r3
            r0 = r5
            r24 = r7
            r23 = r8
            r1 = 0
            r2 = 2
            r3 = r42
            goto L_0x01ef
        L_0x0107:
            r14 = 3
            r2 = -2
            r14 = r42
            if (r14 != r2) goto L_0x0111
            r14 = r43
            r6 = r4
            goto L_0x0114
        L_0x0111:
            r6 = r14
            r14 = r43
        L_0x0114:
            if (r14 != r2) goto L_0x0117
            r14 = r4
        L_0x0117:
            r2 = 6
            if (r6 <= 0) goto L_0x0121
            r10.b(r9, r15, r6, r2)
            int r4 = java.lang.Math.max(r4, r6)
        L_0x0121:
            if (r14 <= 0) goto L_0x012a
            r10.c(r9, r15, r14, r2)
            int r4 = java.lang.Math.min(r4, r14)
        L_0x012a:
            r2 = 1
            if (r3 != r2) goto L_0x0155
            if (r27 == 0) goto L_0x0141
            r2 = 6
            r10.a((androidx.constraintlayout.solver.SolverVariable) r9, (androidx.constraintlayout.solver.SolverVariable) r15, (int) r4, (int) r2)
            r34 = r3
            r0 = r5
            r24 = r7
            r23 = r8
            r35 = r13
            r1 = 0
            r8 = r4
            r13 = r6
            goto L_0x01d3
        L_0x0141:
            r2 = 6
            if (r40 == 0) goto L_0x014c
            r35 = r13
            r13 = 4
            r10.a((androidx.constraintlayout.solver.SolverVariable) r9, (androidx.constraintlayout.solver.SolverVariable) r15, (int) r4, (int) r13)
            goto L_0x01c9
        L_0x014c:
            r35 = r13
            r2 = 1
            r13 = 4
            r10.a((androidx.constraintlayout.solver.SolverVariable) r9, (androidx.constraintlayout.solver.SolverVariable) r15, (int) r4, (int) r2)
            goto L_0x01c9
        L_0x0155:
            r35 = r13
            r2 = 2
            r13 = 4
            if (r3 != r2) goto L_0x01c9
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r2 = r32.h()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r13 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP
            if (r2 == r13) goto L_0x0187
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r2 = r32.h()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r13 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            if (r2 != r13) goto L_0x016c
            goto L_0x0187
        L_0x016c:
            androidx.constraintlayout.solver.widgets.ConstraintWidget r2 = r0.D
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r13 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r2.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r13)
            androidx.constraintlayout.solver.SolverVariable r2 = r10.a((java.lang.Object) r2)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r13 = r0.D
            r31 = r2
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r2 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r13.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r2)
            androidx.constraintlayout.solver.SolverVariable r2 = r10.a((java.lang.Object) r2)
            goto L_0x01a1
        L_0x0187:
            androidx.constraintlayout.solver.widgets.ConstraintWidget r2 = r0.D
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r13 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r2.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r13)
            androidx.constraintlayout.solver.SolverVariable r2 = r10.a((java.lang.Object) r2)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r13 = r0.D
            r31 = r2
            androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r2 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r13.a((androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type) r2)
            androidx.constraintlayout.solver.SolverVariable r2 = r10.a((java.lang.Object) r2)
        L_0x01a1:
            r22 = r31
            r13 = r2
            androidx.constraintlayout.solver.ArrayRow r2 = r26.b()
            r31 = r2
            r18 = 1
            r21 = 6
            r0 = r3
            r3 = r9
            r34 = r0
            r23 = r8
            r0 = 6
            r8 = r4
            r4 = r15
            r0 = r5
            r1 = 0
            r5 = r13
            r13 = r6
            r6 = r22
            r24 = r7
            r7 = r44
            r2.a(r3, r4, r5, r6, r7)
            r10.a((androidx.constraintlayout.solver.ArrayRow) r2)
            r5 = 0
            goto L_0x01d5
        L_0x01c9:
            r34 = r3
            r0 = r5
            r13 = r6
            r24 = r7
            r23 = r8
            r1 = 0
            r8 = r4
        L_0x01d3:
            r5 = r35
        L_0x01d5:
            r2 = 2
            if (r5 == 0) goto L_0x01ed
            if (r0 == r2) goto L_0x01ed
            if (r39 != 0) goto L_0x01ed
            int r3 = java.lang.Math.max(r13, r8)
            if (r14 <= 0) goto L_0x01e6
            int r3 = java.lang.Math.min(r14, r3)
        L_0x01e6:
            r4 = 6
            r10.a((androidx.constraintlayout.solver.SolverVariable) r9, (androidx.constraintlayout.solver.SolverVariable) r15, (int) r3, (int) r4)
            r3 = r13
            r13 = 0
            goto L_0x01ef
        L_0x01ed:
            r3 = r13
            r13 = r5
        L_0x01ef:
            if (r45 == 0) goto L_0x0329
            if (r40 == 0) goto L_0x01f5
            goto L_0x0329
        L_0x01f5:
            r0 = 5
            if (r16 != 0) goto L_0x0203
            if (r17 != 0) goto L_0x0203
            if (r20 != 0) goto L_0x0203
            if (r27 == 0) goto L_0x031e
            r10.b(r12, r9, r1, r0)
            goto L_0x031e
        L_0x0203:
            if (r16 == 0) goto L_0x020e
            if (r17 != 0) goto L_0x020e
            if (r27 == 0) goto L_0x031e
            r10.b(r12, r9, r1, r0)
            goto L_0x031e
        L_0x020e:
            if (r16 != 0) goto L_0x0224
            if (r17 == 0) goto L_0x0224
            int r2 = r33.b()
            int r2 = -r2
            r8 = r24
            r3 = 6
            r10.a((androidx.constraintlayout.solver.SolverVariable) r9, (androidx.constraintlayout.solver.SolverVariable) r8, (int) r2, (int) r3)
            if (r27 == 0) goto L_0x031e
            r10.b(r15, r11, r1, r0)
            goto L_0x031e
        L_0x0224:
            r8 = r24
            if (r16 == 0) goto L_0x031e
            if (r17 == 0) goto L_0x031e
            if (r13 == 0) goto L_0x0296
            r7 = 0
            if (r27 == 0) goto L_0x0235
            if (r36 != 0) goto L_0x0235
            r1 = 6
            r10.b(r9, r15, r7, r1)
        L_0x0235:
            if (r34 != 0) goto L_0x025f
            if (r14 > 0) goto L_0x023f
            if (r3 <= 0) goto L_0x023c
            goto L_0x023f
        L_0x023c:
            r1 = 6
            r6 = 0
            goto L_0x0241
        L_0x023f:
            r1 = 4
            r6 = 1
        L_0x0241:
            int r2 = r32.b()
            r5 = r23
            r10.a((androidx.constraintlayout.solver.SolverVariable) r15, (androidx.constraintlayout.solver.SolverVariable) r5, (int) r2, (int) r1)
            int r2 = r33.b()
            int r2 = -r2
            r10.a((androidx.constraintlayout.solver.SolverVariable) r9, (androidx.constraintlayout.solver.SolverVariable) r8, (int) r2, (int) r1)
            if (r14 > 0) goto L_0x0259
            if (r3 <= 0) goto L_0x0257
            goto L_0x0259
        L_0x0257:
            r1 = 0
            goto L_0x025a
        L_0x0259:
            r1 = 1
        L_0x025a:
            r16 = r6
            r6 = 1
            r14 = 5
            goto L_0x026a
        L_0x025f:
            r4 = r34
            r5 = r23
            r6 = 1
            if (r4 != r6) goto L_0x026d
            r1 = 1
            r14 = 6
            r16 = 1
        L_0x026a:
            r4 = r25
            goto L_0x02a0
        L_0x026d:
            r1 = 3
            if (r4 != r1) goto L_0x0292
            r4 = r25
            if (r39 != 0) goto L_0x027d
            int r1 = r4.n
            r2 = -1
            if (r1 == r2) goto L_0x027d
            if (r14 > 0) goto L_0x027d
            r1 = 6
            goto L_0x027e
        L_0x027d:
            r1 = 4
        L_0x027e:
            int r2 = r32.b()
            r10.a((androidx.constraintlayout.solver.SolverVariable) r15, (androidx.constraintlayout.solver.SolverVariable) r5, (int) r2, (int) r1)
            int r2 = r33.b()
            int r2 = -r2
            r10.a((androidx.constraintlayout.solver.SolverVariable) r9, (androidx.constraintlayout.solver.SolverVariable) r8, (int) r2, (int) r1)
            r1 = 1
            r14 = 5
            r16 = 1
            goto L_0x02a0
        L_0x0292:
            r4 = r25
            r1 = 0
            goto L_0x029d
        L_0x0296:
            r5 = r23
            r6 = 1
            r7 = 0
            r4 = r25
            r1 = 1
        L_0x029d:
            r14 = 5
            r16 = 0
        L_0x02a0:
            if (r1 == 0) goto L_0x02e5
            int r17 = r32.b()
            int r18 = r33.b()
            r1 = r26
            r2 = r15
            r3 = r5
            r4 = r17
            r17 = r5
            r5 = r38
            r19 = 1
            r6 = r8
            r0 = 0
            r7 = r9
            r12 = r8
            r0 = r17
            r8 = r18
            r11 = r9
            r9 = r14
            r1.a(r2, r3, r4, r5, r6, r7, r8, r9)
            r1 = r32
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r1.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget r2 = r2.b
            boolean r2 = r2 instanceof androidx.constraintlayout.solver.widgets.Barrier
            r3 = r33
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r4 = r3.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget r4 = r4.b
            boolean r4 = r4 instanceof androidx.constraintlayout.solver.widgets.Barrier
            if (r2 == 0) goto L_0x02dd
            if (r4 != 0) goto L_0x02dd
            r19 = r27
            r2 = 1
            r4 = 5
            r5 = 6
            goto L_0x02f2
        L_0x02dd:
            if (r2 != 0) goto L_0x02ec
            if (r4 == 0) goto L_0x02ec
            r2 = r27
            r4 = 6
            goto L_0x02f1
        L_0x02e5:
            r1 = r32
            r3 = r33
            r0 = r5
            r12 = r8
            r11 = r9
        L_0x02ec:
            r2 = r27
            r19 = r2
            r4 = 5
        L_0x02f1:
            r5 = 5
        L_0x02f2:
            if (r16 == 0) goto L_0x02f6
            r4 = 6
            r5 = 6
        L_0x02f6:
            if (r13 != 0) goto L_0x02fa
            if (r19 != 0) goto L_0x02fc
        L_0x02fa:
            if (r16 == 0) goto L_0x0303
        L_0x02fc:
            int r1 = r32.b()
            r10.b(r15, r0, r1, r4)
        L_0x0303:
            if (r13 != 0) goto L_0x0307
            if (r2 != 0) goto L_0x0309
        L_0x0307:
            if (r16 == 0) goto L_0x0311
        L_0x0309:
            int r0 = r33.b()
            int r0 = -r0
            r10.c(r11, r12, r0, r5)
        L_0x0311:
            if (r27 == 0) goto L_0x031c
            r0 = r28
            r1 = r11
            r2 = 6
            r3 = 0
            r10.b(r15, r0, r3, r2)
            goto L_0x0321
        L_0x031c:
            r1 = r11
            goto L_0x031f
        L_0x031e:
            r1 = r9
        L_0x031f:
            r2 = 6
            r3 = 0
        L_0x0321:
            if (r27 == 0) goto L_0x0328
            r4 = r29
            r10.b(r4, r1, r3, r2)
        L_0x0328:
            return
        L_0x0329:
            r5 = r0
            r1 = r9
            r0 = r11
            r4 = r12
            r2 = 6
            r3 = 0
            r6 = 2
            if (r5 >= r6) goto L_0x033a
            if (r27 == 0) goto L_0x033a
            r10.b(r15, r0, r3, r2)
            r10.b(r4, r1, r3, r2)
        L_0x033a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.ConstraintWidget.a(androidx.constraintlayout.solver.LinearSystem, boolean, androidx.constraintlayout.solver.SolverVariable, androidx.constraintlayout.solver.SolverVariable, androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour, boolean, androidx.constraintlayout.solver.widgets.ConstraintAnchor, androidx.constraintlayout.solver.widgets.ConstraintAnchor, int, int, int, int, float, boolean, boolean, int, int, int, float, boolean):void");
    }
}
