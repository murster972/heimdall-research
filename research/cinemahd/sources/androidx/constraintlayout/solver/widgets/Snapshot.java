package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import java.util.ArrayList;

public class Snapshot {

    /* renamed from: a  reason: collision with root package name */
    private int f503a;
    private int b;
    private int c;
    private int d;
    private ArrayList<Connection> e = new ArrayList<>();

    static class Connection {

        /* renamed from: a  reason: collision with root package name */
        private ConstraintAnchor f504a;
        private ConstraintAnchor b;
        private int c;
        private ConstraintAnchor.Strength d;
        private int e;

        public Connection(ConstraintAnchor constraintAnchor) {
            this.f504a = constraintAnchor;
            this.b = constraintAnchor.g();
            this.c = constraintAnchor.b();
            this.d = constraintAnchor.f();
            this.e = constraintAnchor.a();
        }

        public void a(ConstraintWidget constraintWidget) {
            constraintWidget.a(this.f504a.h()).a(this.b, this.c, this.d, this.e);
        }

        public void b(ConstraintWidget constraintWidget) {
            this.f504a = constraintWidget.a(this.f504a.h());
            ConstraintAnchor constraintAnchor = this.f504a;
            if (constraintAnchor != null) {
                this.b = constraintAnchor.g();
                this.c = this.f504a.b();
                this.d = this.f504a.f();
                this.e = this.f504a.a();
                return;
            }
            this.b = null;
            this.c = 0;
            this.d = ConstraintAnchor.Strength.STRONG;
            this.e = 0;
        }
    }

    public Snapshot(ConstraintWidget constraintWidget) {
        this.f503a = constraintWidget.v();
        this.b = constraintWidget.w();
        this.c = constraintWidget.s();
        this.d = constraintWidget.i();
        ArrayList<ConstraintAnchor> b2 = constraintWidget.b();
        int size = b2.size();
        for (int i = 0; i < size; i++) {
            this.e.add(new Connection(b2.get(i)));
        }
    }

    public void a(ConstraintWidget constraintWidget) {
        constraintWidget.r(this.f503a);
        constraintWidget.s(this.b);
        constraintWidget.o(this.c);
        constraintWidget.g(this.d);
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).a(constraintWidget);
        }
    }

    public void b(ConstraintWidget constraintWidget) {
        this.f503a = constraintWidget.v();
        this.b = constraintWidget.w();
        this.c = constraintWidget.s();
        this.d = constraintWidget.i();
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).b(constraintWidget);
        }
    }
}
