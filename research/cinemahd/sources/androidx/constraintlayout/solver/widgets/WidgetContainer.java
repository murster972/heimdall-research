package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.Cache;
import java.util.ArrayList;

public class WidgetContainer extends ConstraintWidget {
    protected ArrayList<ConstraintWidget> k0 = new ArrayList<>();

    public void D() {
        this.k0.clear();
        super.D();
    }

    public void H() {
        super.H();
        ArrayList<ConstraintWidget> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ConstraintWidget constraintWidget = this.k0.get(i);
                constraintWidget.b(g(), h());
                if (!(constraintWidget instanceof ConstraintWidgetContainer)) {
                    constraintWidget.H();
                }
            }
        }
    }

    public ConstraintWidgetContainer J() {
        ConstraintWidget k = k();
        ConstraintWidgetContainer constraintWidgetContainer = this instanceof ConstraintWidgetContainer ? (ConstraintWidgetContainer) this : null;
        while (k != null) {
            ConstraintWidget k2 = k.k();
            if (k instanceof ConstraintWidgetContainer) {
                constraintWidgetContainer = (ConstraintWidgetContainer) k;
            }
            k = k2;
        }
        return constraintWidgetContainer;
    }

    public void K() {
        H();
        ArrayList<ConstraintWidget> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ConstraintWidget constraintWidget = this.k0.get(i);
                if (constraintWidget instanceof WidgetContainer) {
                    ((WidgetContainer) constraintWidget).K();
                }
            }
        }
    }

    public void L() {
        this.k0.clear();
    }

    public void a(Cache cache) {
        super.a(cache);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            this.k0.get(i).a(cache);
        }
    }

    public void b(ConstraintWidget constraintWidget) {
        this.k0.add(constraintWidget);
        if (constraintWidget.k() != null) {
            ((WidgetContainer) constraintWidget.k()).c(constraintWidget);
        }
        constraintWidget.a((ConstraintWidget) this);
    }

    public void c(ConstraintWidget constraintWidget) {
        this.k0.remove(constraintWidget);
        constraintWidget.a((ConstraintWidget) null);
    }

    public void b(int i, int i2) {
        super.b(i, i2);
        int size = this.k0.size();
        for (int i3 = 0; i3 < size; i3++) {
            this.k0.get(i3).b(o(), p());
        }
    }
}
