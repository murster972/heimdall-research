package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.LinearSystem;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConstraintWidgetContainer extends WidgetContainer {
    public int A0 = 0;
    public int B0 = 0;
    private int C0 = 7;
    public boolean D0 = false;
    private boolean E0 = false;
    private boolean F0 = false;
    private boolean l0 = false;
    protected LinearSystem m0 = new LinearSystem();
    private Snapshot n0;
    int o0;
    int p0;
    int q0;
    int r0;
    int s0 = 0;
    int t0 = 0;
    ChainHead[] u0 = new ChainHead[4];
    ChainHead[] v0 = new ChainHead[4];
    public List<ConstraintWidgetGroup> w0 = new ArrayList();
    public boolean x0 = false;
    public boolean y0 = false;
    public boolean z0 = false;

    private void V() {
        this.s0 = 0;
        this.t0 = 0;
    }

    private void e(ConstraintWidget constraintWidget) {
        int i = this.t0 + 1;
        ChainHead[] chainHeadArr = this.u0;
        if (i >= chainHeadArr.length) {
            this.u0 = (ChainHead[]) Arrays.copyOf(chainHeadArr, chainHeadArr.length * 2);
        }
        this.u0[this.t0] = new ChainHead(constraintWidget, 1, P());
        this.t0++;
    }

    public void D() {
        this.m0.f();
        this.o0 = 0;
        this.q0 = 0;
        this.p0 = 0;
        this.r0 = 0;
        this.w0.clear();
        this.D0 = false;
        super.D();
    }

    /* JADX WARNING: type inference failed for: r8v15, types: [boolean] */
    /* JADX WARNING: type inference failed for: r8v16 */
    /* JADX WARNING: type inference failed for: r8v17 */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0252  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0267  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0283  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0290  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0293  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0186  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01e4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void K() {
        /*
            r21 = this;
            r1 = r21
            int r2 = r1.I
            int r3 = r1.J
            int r0 = r21.s()
            r4 = 0
            int r5 = java.lang.Math.max(r4, r0)
            int r0 = r21.i()
            int r6 = java.lang.Math.max(r4, r0)
            r1.E0 = r4
            r1.F0 = r4
            androidx.constraintlayout.solver.widgets.ConstraintWidget r0 = r1.D
            if (r0 == 0) goto L_0x0046
            androidx.constraintlayout.solver.widgets.Snapshot r0 = r1.n0
            if (r0 != 0) goto L_0x002a
            androidx.constraintlayout.solver.widgets.Snapshot r0 = new androidx.constraintlayout.solver.widgets.Snapshot
            r0.<init>(r1)
            r1.n0 = r0
        L_0x002a:
            androidx.constraintlayout.solver.widgets.Snapshot r0 = r1.n0
            r0.b(r1)
            int r0 = r1.o0
            r1.r(r0)
            int r0 = r1.p0
            r1.s(r0)
            r21.E()
            androidx.constraintlayout.solver.LinearSystem r0 = r1.m0
            androidx.constraintlayout.solver.Cache r0 = r0.d()
            r1.a(r0)
            goto L_0x004a
        L_0x0046:
            r1.I = r4
            r1.J = r4
        L_0x004a:
            int r0 = r1.C0
            r7 = 32
            r8 = 8
            r9 = 1
            if (r0 == 0) goto L_0x006a
            boolean r0 = r1.t(r8)
            if (r0 != 0) goto L_0x005c
            r21.S()
        L_0x005c:
            boolean r0 = r1.t(r7)
            if (r0 != 0) goto L_0x0065
            r21.R()
        L_0x0065:
            androidx.constraintlayout.solver.LinearSystem r0 = r1.m0
            r0.g = r9
            goto L_0x006e
        L_0x006a:
            androidx.constraintlayout.solver.LinearSystem r0 = r1.m0
            r0.g = r4
        L_0x006e:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r1.C
            r10 = r0[r9]
            r11 = r0[r4]
            r21.V()
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r0 = r1.w0
            int r0 = r0.size()
            if (r0 != 0) goto L_0x0090
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r0 = r1.w0
            r0.clear()
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r0 = r1.w0
            androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup r12 = new androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r13 = r1.k0
            r12.<init>(r13)
            r0.add(r4, r12)
        L_0x0090:
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r0 = r1.w0
            int r12 = r0.size()
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r13 = r1.k0
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r0 = r21.j()
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r14 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r0 == r14) goto L_0x00ab
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r0 = r21.q()
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r14 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r0 != r14) goto L_0x00a9
            goto L_0x00ab
        L_0x00a9:
            r14 = 0
            goto L_0x00ac
        L_0x00ab:
            r14 = 1
        L_0x00ac:
            r0 = 0
            r15 = 0
        L_0x00ae:
            if (r15 >= r12) goto L_0x02f4
            boolean r8 = r1.D0
            if (r8 != 0) goto L_0x02f4
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r8 = r1.w0
            java.lang.Object r8 = r8.get(r15)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup r8 = (androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup) r8
            boolean r8 = r8.d
            if (r8 == 0) goto L_0x00c4
            r19 = r12
            goto L_0x02e8
        L_0x00c4:
            boolean r8 = r1.t(r7)
            if (r8 == 0) goto L_0x00f9
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r8 = r21.j()
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r7 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            if (r8 != r7) goto L_0x00eb
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r7 = r21.q()
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r8 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            if (r7 != r8) goto L_0x00eb
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r7 = r1.w0
            java.lang.Object r7 = r7.get(r15)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup r7 = (androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup) r7
            java.util.List r7 = r7.a()
            java.util.ArrayList r7 = (java.util.ArrayList) r7
            r1.k0 = r7
            goto L_0x00f9
        L_0x00eb:
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r7 = r1.w0
            java.lang.Object r7 = r7.get(r15)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup r7 = (androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup) r7
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> r7 = r7.f499a
            java.util.ArrayList r7 = (java.util.ArrayList) r7
            r1.k0 = r7
        L_0x00f9:
            r21.V()
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r7 = r1.k0
            int r7 = r7.size()
            r8 = 0
        L_0x0103:
            if (r8 >= r7) goto L_0x011b
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r4 = r1.k0
            java.lang.Object r4 = r4.get(r8)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r4 = (androidx.constraintlayout.solver.widgets.ConstraintWidget) r4
            boolean r9 = r4 instanceof androidx.constraintlayout.solver.widgets.WidgetContainer
            if (r9 == 0) goto L_0x0116
            androidx.constraintlayout.solver.widgets.WidgetContainer r4 = (androidx.constraintlayout.solver.widgets.WidgetContainer) r4
            r4.K()
        L_0x0116:
            int r8 = r8 + 1
            r4 = 0
            r9 = 1
            goto L_0x0103
        L_0x011b:
            r9 = r0
            r0 = 0
            r4 = 1
        L_0x011e:
            if (r4 == 0) goto L_0x02d7
            r17 = r4
            r8 = 1
            int r4 = r0 + 1
            androidx.constraintlayout.solver.LinearSystem r0 = r1.m0     // Catch:{ Exception -> 0x0162 }
            r0.f()     // Catch:{ Exception -> 0x0162 }
            r21.V()     // Catch:{ Exception -> 0x0162 }
            androidx.constraintlayout.solver.LinearSystem r0 = r1.m0     // Catch:{ Exception -> 0x0162 }
            r1.b((androidx.constraintlayout.solver.LinearSystem) r0)     // Catch:{ Exception -> 0x0162 }
            r0 = 0
        L_0x0133:
            if (r0 >= r7) goto L_0x0149
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r8 = r1.k0     // Catch:{ Exception -> 0x0162 }
            java.lang.Object r8 = r8.get(r0)     // Catch:{ Exception -> 0x0162 }
            androidx.constraintlayout.solver.widgets.ConstraintWidget r8 = (androidx.constraintlayout.solver.widgets.ConstraintWidget) r8     // Catch:{ Exception -> 0x0162 }
            r18 = r9
            androidx.constraintlayout.solver.LinearSystem r9 = r1.m0     // Catch:{ Exception -> 0x0160 }
            r8.b((androidx.constraintlayout.solver.LinearSystem) r9)     // Catch:{ Exception -> 0x0160 }
            int r0 = r0 + 1
            r9 = r18
            goto L_0x0133
        L_0x0149:
            r18 = r9
            androidx.constraintlayout.solver.LinearSystem r0 = r1.m0     // Catch:{ Exception -> 0x0160 }
            boolean r8 = r1.d((androidx.constraintlayout.solver.LinearSystem) r0)     // Catch:{ Exception -> 0x0160 }
            if (r8 == 0) goto L_0x015b
            androidx.constraintlayout.solver.LinearSystem r0 = r1.m0     // Catch:{ Exception -> 0x0159 }
            r0.e()     // Catch:{ Exception -> 0x0159 }
            goto L_0x015b
        L_0x0159:
            r0 = move-exception
            goto L_0x0167
        L_0x015b:
            r17 = r8
            r19 = r12
            goto L_0x0184
        L_0x0160:
            r0 = move-exception
            goto L_0x0165
        L_0x0162:
            r0 = move-exception
            r18 = r9
        L_0x0165:
            r8 = r17
        L_0x0167:
            r0.printStackTrace()
            java.io.PrintStream r9 = java.lang.System.out
            r17 = r8
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r19 = r12
            java.lang.String r12 = "EXCEPTION : "
            r8.append(r12)
            r8.append(r0)
            java.lang.String r0 = r8.toString()
            r9.println(r0)
        L_0x0184:
            if (r17 == 0) goto L_0x018f
            androidx.constraintlayout.solver.LinearSystem r8 = r1.m0
            boolean[] r9 = androidx.constraintlayout.solver.widgets.Optimizer.f501a
            r1.a((androidx.constraintlayout.solver.LinearSystem) r8, (boolean[]) r9)
        L_0x018d:
            r9 = 2
            goto L_0x01d8
        L_0x018f:
            androidx.constraintlayout.solver.LinearSystem r8 = r1.m0
            r1.c((androidx.constraintlayout.solver.LinearSystem) r8)
            r8 = 0
        L_0x0195:
            if (r8 >= r7) goto L_0x018d
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r9 = r1.k0
            java.lang.Object r9 = r9.get(r8)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = (androidx.constraintlayout.solver.widgets.ConstraintWidget) r9
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r12 = r9.C
            r16 = 0
            r12 = r12[r16]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r0 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r12 != r0) goto L_0x01ba
            int r0 = r9.s()
            int r12 = r9.u()
            if (r0 >= r12) goto L_0x01ba
            boolean[] r0 = androidx.constraintlayout.solver.widgets.Optimizer.f501a
            r8 = 2
            r12 = 1
            r0[r8] = r12
            goto L_0x018d
        L_0x01ba:
            r12 = 1
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r9.C
            r0 = r0[r12]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r12 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r0 != r12) goto L_0x01d4
            int r0 = r9.i()
            int r9 = r9.t()
            if (r0 >= r9) goto L_0x01d4
            boolean[] r0 = androidx.constraintlayout.solver.widgets.Optimizer.f501a
            r8 = 1
            r9 = 2
            r0[r9] = r8
            goto L_0x01d8
        L_0x01d4:
            r9 = 2
            int r8 = r8 + 1
            goto L_0x0195
        L_0x01d8:
            if (r14 == 0) goto L_0x0252
            r8 = 8
            if (r4 >= r8) goto L_0x0252
            boolean[] r0 = androidx.constraintlayout.solver.widgets.Optimizer.f501a
            boolean r0 = r0[r9]
            if (r0 == 0) goto L_0x0252
            r0 = 0
            r9 = 0
            r12 = 0
        L_0x01e7:
            if (r0 >= r7) goto L_0x0211
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r8 = r1.k0
            java.lang.Object r8 = r8.get(r0)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r8 = (androidx.constraintlayout.solver.widgets.ConstraintWidget) r8
            r17 = r4
            int r4 = r8.I
            int r20 = r8.s()
            int r4 = r4 + r20
            int r9 = java.lang.Math.max(r9, r4)
            int r4 = r8.J
            int r8 = r8.i()
            int r4 = r4 + r8
            int r12 = java.lang.Math.max(r12, r4)
            int r0 = r0 + 1
            r4 = r17
            r8 = 8
            goto L_0x01e7
        L_0x0211:
            r17 = r4
            int r0 = r1.R
            int r0 = java.lang.Math.max(r0, r9)
            int r4 = r1.S
            int r4 = java.lang.Math.max(r4, r12)
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r8 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r11 != r8) goto L_0x0237
            int r8 = r21.s()
            if (r8 >= r0) goto L_0x0237
            r1.o(r0)
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r1.C
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r8 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            r9 = 0
            r0[r9] = r8
            r0 = 1
            r18 = 1
            goto L_0x0238
        L_0x0237:
            r0 = 0
        L_0x0238:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r8 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r10 != r8) goto L_0x024f
            int r8 = r21.i()
            if (r8 >= r4) goto L_0x024f
            r1.g(r4)
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r1.C
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r4 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            r8 = 1
            r0[r8] = r4
            r0 = 1
            r9 = 1
            goto L_0x0257
        L_0x024f:
            r9 = r18
            goto L_0x0257
        L_0x0252:
            r17 = r4
            r9 = r18
            r0 = 0
        L_0x0257:
            int r4 = r1.R
            int r8 = r21.s()
            int r4 = java.lang.Math.max(r4, r8)
            int r8 = r21.s()
            if (r4 <= r8) goto L_0x0273
            r1.o(r4)
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r1.C
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r4 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            r8 = 0
            r0[r8] = r4
            r0 = 1
            r9 = 1
        L_0x0273:
            int r4 = r1.S
            int r8 = r21.i()
            int r4 = java.lang.Math.max(r4, r8)
            int r8 = r21.i()
            if (r4 <= r8) goto L_0x0290
            r1.g(r4)
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r1.C
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r4 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            r8 = 1
            r0[r8] = r4
            r0 = 1
            r9 = 1
            goto L_0x0291
        L_0x0290:
            r8 = 1
        L_0x0291:
            if (r9 != 0) goto L_0x02d0
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r4 = r1.C
            r12 = 0
            r4 = r4[r12]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r12 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r4 != r12) goto L_0x02b2
            if (r5 <= 0) goto L_0x02b2
            int r4 = r21.s()
            if (r4 <= r5) goto L_0x02b2
            r1.E0 = r8
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r1.C
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r4 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            r9 = 0
            r0[r9] = r4
            r1.o(r5)
            r0 = 1
            r9 = 1
        L_0x02b2:
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r4 = r1.C
            r4 = r4[r8]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r12 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r4 != r12) goto L_0x02d0
            if (r6 <= 0) goto L_0x02d0
            int r4 = r21.i()
            if (r4 <= r6) goto L_0x02d0
            r1.F0 = r8
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r1.C
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r4 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED
            r0[r8] = r4
            r1.g(r6)
            r4 = 1
            r9 = 1
            goto L_0x02d1
        L_0x02d0:
            r4 = r0
        L_0x02d1:
            r0 = r17
            r12 = r19
            goto L_0x011e
        L_0x02d7:
            r18 = r9
            r19 = r12
            java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup> r0 = r1.w0
            java.lang.Object r0 = r0.get(r15)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup r0 = (androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup) r0
            r0.b()
            r0 = r18
        L_0x02e8:
            int r15 = r15 + 1
            r12 = r19
            r4 = 0
            r7 = 32
            r8 = 8
            r9 = 1
            goto L_0x00ae
        L_0x02f4:
            r1.k0 = r13
            androidx.constraintlayout.solver.widgets.ConstraintWidget r4 = r1.D
            if (r4 == 0) goto L_0x0326
            int r2 = r1.R
            int r3 = r21.s()
            int r2 = java.lang.Math.max(r2, r3)
            int r3 = r1.S
            int r4 = r21.i()
            int r3 = java.lang.Math.max(r3, r4)
            androidx.constraintlayout.solver.widgets.Snapshot r4 = r1.n0
            r4.a(r1)
            int r4 = r1.o0
            int r2 = r2 + r4
            int r4 = r1.q0
            int r2 = r2 + r4
            r1.o(r2)
            int r2 = r1.p0
            int r3 = r3 + r2
            int r2 = r1.r0
            int r3 = r3 + r2
            r1.g(r3)
            goto L_0x032a
        L_0x0326:
            r1.I = r2
            r1.J = r3
        L_0x032a:
            if (r0 == 0) goto L_0x0334
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r1.C
            r2 = 0
            r0[r2] = r11
            r2 = 1
            r0[r2] = r10
        L_0x0334:
            androidx.constraintlayout.solver.LinearSystem r0 = r1.m0
            androidx.constraintlayout.solver.Cache r0 = r0.d()
            r1.a(r0)
            androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r0 = r21.J()
            if (r1 != r0) goto L_0x0346
            r21.H()
        L_0x0346:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer.K():void");
    }

    public int M() {
        return this.C0;
    }

    public boolean N() {
        return false;
    }

    public boolean O() {
        return this.F0;
    }

    public boolean P() {
        return this.l0;
    }

    public boolean Q() {
        return this.E0;
    }

    public void R() {
        if (!t(8)) {
            a(this.C0);
        }
        U();
    }

    public void S() {
        int size = this.k0.size();
        F();
        for (int i = 0; i < size; i++) {
            this.k0.get(i).F();
        }
    }

    public void T() {
        S();
        a(this.C0);
    }

    public void U() {
        ResolutionAnchor d = a(ConstraintAnchor.Type.LEFT).d();
        ResolutionAnchor d2 = a(ConstraintAnchor.Type.TOP).d();
        d.a((ResolutionAnchor) null, 0.0f);
        d2.a((ResolutionAnchor) null, 0.0f);
    }

    public void a(LinearSystem linearSystem, boolean[] zArr) {
        zArr[2] = false;
        c(linearSystem);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            ConstraintWidget constraintWidget = this.k0.get(i);
            constraintWidget.c(linearSystem);
            if (constraintWidget.C[0] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.s() < constraintWidget.u()) {
                zArr[2] = true;
            }
            if (constraintWidget.C[1] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.i() < constraintWidget.t()) {
                zArr[2] = true;
            }
        }
    }

    public void c(boolean z) {
        this.l0 = z;
    }

    public boolean d(LinearSystem linearSystem) {
        a(linearSystem);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            ConstraintWidget constraintWidget = this.k0.get(i);
            if (constraintWidget instanceof ConstraintWidgetContainer) {
                ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = constraintWidget.C;
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = dimensionBehaviourArr[0];
                ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = dimensionBehaviourArr[1];
                if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.a(ConstraintWidget.DimensionBehaviour.FIXED);
                }
                if (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.b(ConstraintWidget.DimensionBehaviour.FIXED);
                }
                constraintWidget.a(linearSystem);
                if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.a(dimensionBehaviour);
                }
                if (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    constraintWidget.b(dimensionBehaviour2);
                }
            } else {
                Optimizer.a(this, linearSystem, constraintWidget);
                constraintWidget.a(linearSystem);
            }
        }
        if (this.s0 > 0) {
            Chain.a(this, linearSystem, 0);
        }
        if (this.t0 > 0) {
            Chain.a(this, linearSystem, 1);
        }
        return true;
    }

    public void f(int i, int i2) {
        ResolutionDimension resolutionDimension;
        ResolutionDimension resolutionDimension2;
        if (!(this.C[0] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || (resolutionDimension2 = this.c) == null)) {
            resolutionDimension2.a(i);
        }
        if (this.C[1] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && (resolutionDimension = this.d) != null) {
            resolutionDimension.a(i2);
        }
    }

    public boolean t(int i) {
        return (this.C0 & i) == i;
    }

    public void u(int i) {
        this.C0 = i;
    }

    public void a(int i) {
        super.a(i);
        int size = this.k0.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.k0.get(i2).a(i);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ConstraintWidget constraintWidget, int i) {
        if (i == 0) {
            d(constraintWidget);
        } else if (i == 1) {
            e(constraintWidget);
        }
    }

    private void d(ConstraintWidget constraintWidget) {
        int i = this.s0 + 1;
        ChainHead[] chainHeadArr = this.v0;
        if (i >= chainHeadArr.length) {
            this.v0 = (ChainHead[]) Arrays.copyOf(chainHeadArr, chainHeadArr.length * 2);
        }
        this.v0[this.s0] = new ChainHead(constraintWidget, 0, P());
        this.s0++;
    }
}
