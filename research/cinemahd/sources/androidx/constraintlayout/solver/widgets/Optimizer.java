package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.LinearSystem;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;

public class Optimizer {

    /* renamed from: a  reason: collision with root package name */
    static boolean[] f501a = new boolean[3];

    static void a(ConstraintWidgetContainer constraintWidgetContainer, LinearSystem linearSystem, ConstraintWidget constraintWidget) {
        if (constraintWidgetContainer.C[0] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && constraintWidget.C[0] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            int i = constraintWidget.s.e;
            int s = constraintWidgetContainer.s() - constraintWidget.u.e;
            ConstraintAnchor constraintAnchor = constraintWidget.s;
            constraintAnchor.i = linearSystem.a((Object) constraintAnchor);
            ConstraintAnchor constraintAnchor2 = constraintWidget.u;
            constraintAnchor2.i = linearSystem.a((Object) constraintAnchor2);
            linearSystem.a(constraintWidget.s.i, i);
            linearSystem.a(constraintWidget.u.i, s);
            constraintWidget.f496a = 2;
            constraintWidget.a(i, s);
        }
        if (constraintWidgetContainer.C[1] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && constraintWidget.C[1] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            int i2 = constraintWidget.t.e;
            int i3 = constraintWidgetContainer.i() - constraintWidget.v.e;
            ConstraintAnchor constraintAnchor3 = constraintWidget.t;
            constraintAnchor3.i = linearSystem.a((Object) constraintAnchor3);
            ConstraintAnchor constraintAnchor4 = constraintWidget.v;
            constraintAnchor4.i = linearSystem.a((Object) constraintAnchor4);
            linearSystem.a(constraintWidget.t.i, i2);
            linearSystem.a(constraintWidget.v.i, i3);
            if (constraintWidget.Q > 0 || constraintWidget.r() == 8) {
                ConstraintAnchor constraintAnchor5 = constraintWidget.w;
                constraintAnchor5.i = linearSystem.a((Object) constraintAnchor5);
                linearSystem.a(constraintWidget.w.i, constraintWidget.Q + i2);
            }
            constraintWidget.b = 2;
            constraintWidget.e(i2, i3);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x003b A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(androidx.constraintlayout.solver.widgets.ConstraintWidget r5, int r6) {
        /*
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r0 = r5.C
            r1 = r0[r6]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r2 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            r3 = 0
            if (r1 == r2) goto L_0x000a
            return r3
        L_0x000a:
            float r1 = r5.G
            r2 = 0
            r4 = 1
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 == 0) goto L_0x001d
            if (r6 != 0) goto L_0x0015
            goto L_0x0016
        L_0x0015:
            r4 = 0
        L_0x0016:
            r5 = r0[r4]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r6 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r5 != r6) goto L_0x001c
        L_0x001c:
            return r3
        L_0x001d:
            if (r6 != 0) goto L_0x002d
            int r6 = r5.e
            if (r6 == 0) goto L_0x0024
            return r3
        L_0x0024:
            int r6 = r5.h
            if (r6 != 0) goto L_0x002c
            int r5 = r5.i
            if (r5 == 0) goto L_0x003b
        L_0x002c:
            return r3
        L_0x002d:
            int r6 = r5.f
            if (r6 == 0) goto L_0x0032
            return r3
        L_0x0032:
            int r6 = r5.k
            if (r6 != 0) goto L_0x003c
            int r5 = r5.l
            if (r5 == 0) goto L_0x003b
            goto L_0x003c
        L_0x003b:
            return r4
        L_0x003c:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.Optimizer.a(androidx.constraintlayout.solver.widgets.ConstraintWidget, int):boolean");
    }

    static void a(int i, ConstraintWidget constraintWidget) {
        ConstraintWidget constraintWidget2 = constraintWidget;
        constraintWidget.I();
        ResolutionAnchor d = constraintWidget2.s.d();
        ResolutionAnchor d2 = constraintWidget2.t.d();
        ResolutionAnchor d3 = constraintWidget2.u.d();
        ResolutionAnchor d4 = constraintWidget2.v.d();
        boolean z = (i & 8) == 8;
        boolean z2 = constraintWidget2.C[0] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && a(constraintWidget2, 0);
        if (!(d.h == 4 || d3.h == 4)) {
            if (constraintWidget2.C[0] == ConstraintWidget.DimensionBehaviour.FIXED || (z2 && constraintWidget.r() == 8)) {
                if (constraintWidget2.s.d == null && constraintWidget2.u.d == null) {
                    d.b(1);
                    d3.b(1);
                    if (z) {
                        d3.a(d, 1, constraintWidget.m());
                    } else {
                        d3.a(d, constraintWidget.s());
                    }
                } else if (constraintWidget2.s.d != null && constraintWidget2.u.d == null) {
                    d.b(1);
                    d3.b(1);
                    if (z) {
                        d3.a(d, 1, constraintWidget.m());
                    } else {
                        d3.a(d, constraintWidget.s());
                    }
                } else if (constraintWidget2.s.d == null && constraintWidget2.u.d != null) {
                    d.b(1);
                    d3.b(1);
                    d.a(d3, -constraintWidget.s());
                    if (z) {
                        d.a(d3, -1, constraintWidget.m());
                    } else {
                        d.a(d3, -constraintWidget.s());
                    }
                } else if (!(constraintWidget2.s.d == null || constraintWidget2.u.d == null)) {
                    d.b(2);
                    d3.b(2);
                    if (z) {
                        constraintWidget.m().a(d);
                        constraintWidget.m().a(d3);
                        d.b(d3, -1, constraintWidget.m());
                        d3.b(d, 1, constraintWidget.m());
                    } else {
                        d.b(d3, (float) (-constraintWidget.s()));
                        d3.b(d, (float) constraintWidget.s());
                    }
                }
            } else if (z2) {
                int s = constraintWidget.s();
                d.b(1);
                d3.b(1);
                if (constraintWidget2.s.d == null && constraintWidget2.u.d == null) {
                    if (z) {
                        d3.a(d, 1, constraintWidget.m());
                    } else {
                        d3.a(d, s);
                    }
                } else if (constraintWidget2.s.d == null || constraintWidget2.u.d != null) {
                    if (constraintWidget2.s.d != null || constraintWidget2.u.d == null) {
                        if (!(constraintWidget2.s.d == null || constraintWidget2.u.d == null)) {
                            if (z) {
                                constraintWidget.m().a(d);
                                constraintWidget.m().a(d3);
                            }
                            if (constraintWidget2.G == 0.0f) {
                                d.b(3);
                                d3.b(3);
                                d.b(d3, 0.0f);
                                d3.b(d, 0.0f);
                            } else {
                                d.b(2);
                                d3.b(2);
                                d.b(d3, (float) (-s));
                                d3.b(d, (float) s);
                                constraintWidget2.o(s);
                            }
                        }
                    } else if (z) {
                        d.a(d3, -1, constraintWidget.m());
                    } else {
                        d.a(d3, -s);
                    }
                } else if (z) {
                    d3.a(d, 1, constraintWidget.m());
                } else {
                    d3.a(d, s);
                }
            }
        }
        boolean z3 = constraintWidget2.C[1] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && a(constraintWidget2, 1);
        if (d2.h != 4 && d4.h != 4) {
            if (constraintWidget2.C[1] == ConstraintWidget.DimensionBehaviour.FIXED || (z3 && constraintWidget.r() == 8)) {
                if (constraintWidget2.t.d == null && constraintWidget2.v.d == null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d4.a(d2, 1, constraintWidget.l());
                    } else {
                        d4.a(d2, constraintWidget.i());
                    }
                    ConstraintAnchor constraintAnchor = constraintWidget2.w;
                    if (constraintAnchor.d != null) {
                        constraintAnchor.d().b(1);
                        d2.a(1, constraintWidget2.w.d(), -constraintWidget2.Q);
                    }
                } else if (constraintWidget2.t.d != null && constraintWidget2.v.d == null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d4.a(d2, 1, constraintWidget.l());
                    } else {
                        d4.a(d2, constraintWidget.i());
                    }
                    if (constraintWidget2.Q > 0) {
                        constraintWidget2.w.d().a(1, d2, constraintWidget2.Q);
                    }
                } else if (constraintWidget2.t.d == null && constraintWidget2.v.d != null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d2.a(d4, -1, constraintWidget.l());
                    } else {
                        d2.a(d4, -constraintWidget.i());
                    }
                    if (constraintWidget2.Q > 0) {
                        constraintWidget2.w.d().a(1, d2, constraintWidget2.Q);
                    }
                } else if (constraintWidget2.t.d != null && constraintWidget2.v.d != null) {
                    d2.b(2);
                    d4.b(2);
                    if (z) {
                        d2.b(d4, -1, constraintWidget.l());
                        d4.b(d2, 1, constraintWidget.l());
                        constraintWidget.l().a(d2);
                        constraintWidget.m().a(d4);
                    } else {
                        d2.b(d4, (float) (-constraintWidget.i()));
                        d4.b(d2, (float) constraintWidget.i());
                    }
                    if (constraintWidget2.Q > 0) {
                        constraintWidget2.w.d().a(1, d2, constraintWidget2.Q);
                    }
                }
            } else if (z3) {
                int i2 = constraintWidget.i();
                d2.b(1);
                d4.b(1);
                if (constraintWidget2.t.d == null && constraintWidget2.v.d == null) {
                    if (z) {
                        d4.a(d2, 1, constraintWidget.l());
                    } else {
                        d4.a(d2, i2);
                    }
                } else if (constraintWidget2.t.d == null || constraintWidget2.v.d != null) {
                    if (constraintWidget2.t.d != null || constraintWidget2.v.d == null) {
                        if (constraintWidget2.t.d != null && constraintWidget2.v.d != null) {
                            if (z) {
                                constraintWidget.l().a(d2);
                                constraintWidget.m().a(d4);
                            }
                            if (constraintWidget2.G == 0.0f) {
                                d2.b(3);
                                d4.b(3);
                                d2.b(d4, 0.0f);
                                d4.b(d2, 0.0f);
                                return;
                            }
                            d2.b(2);
                            d4.b(2);
                            d2.b(d4, (float) (-i2));
                            d4.b(d2, (float) i2);
                            constraintWidget2.g(i2);
                            if (constraintWidget2.Q > 0) {
                                constraintWidget2.w.d().a(1, d2, constraintWidget2.Q);
                            }
                        }
                    } else if (z) {
                        d2.a(d4, -1, constraintWidget.l());
                    } else {
                        d2.a(d4, -i2);
                    }
                } else if (z) {
                    d4.a(d2, 1, constraintWidget.l());
                } else {
                    d4.a(d2, i2);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r7.e0 == 2) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        if (r7.f0 == 2) goto L_0x0034;
     */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0109  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean a(androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r24, androidx.constraintlayout.solver.LinearSystem r25, int r26, int r27, androidx.constraintlayout.solver.widgets.ChainHead r28) {
        /*
            r0 = r25
            r1 = r26
            r2 = r28
            androidx.constraintlayout.solver.widgets.ConstraintWidget r3 = r2.f490a
            androidx.constraintlayout.solver.widgets.ConstraintWidget r4 = r2.c
            androidx.constraintlayout.solver.widgets.ConstraintWidget r5 = r2.b
            androidx.constraintlayout.solver.widgets.ConstraintWidget r6 = r2.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget r7 = r2.e
            float r8 = r2.k
            androidx.constraintlayout.solver.widgets.ConstraintWidget r9 = r2.f
            androidx.constraintlayout.solver.widgets.ConstraintWidget r2 = r2.g
            r9 = r24
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r2 = r9.C
            r2 = r2[r1]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r9 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            r2 = 2
            r10 = 1
            if (r1 != 0) goto L_0x0038
            int r11 = r7.e0
            if (r11 != 0) goto L_0x0028
            r11 = 1
            goto L_0x0029
        L_0x0028:
            r11 = 0
        L_0x0029:
            int r12 = r7.e0
            if (r12 != r10) goto L_0x002f
            r12 = 1
            goto L_0x0030
        L_0x002f:
            r12 = 0
        L_0x0030:
            int r7 = r7.e0
            if (r7 != r2) goto L_0x0036
        L_0x0034:
            r2 = 1
            goto L_0x004b
        L_0x0036:
            r2 = 0
            goto L_0x004b
        L_0x0038:
            int r11 = r7.f0
            if (r11 != 0) goto L_0x003e
            r11 = 1
            goto L_0x003f
        L_0x003e:
            r11 = 0
        L_0x003f:
            int r12 = r7.f0
            if (r12 != r10) goto L_0x0045
            r12 = 1
            goto L_0x0046
        L_0x0045:
            r12 = 0
        L_0x0046:
            int r7 = r7.f0
            if (r7 != r2) goto L_0x0036
            goto L_0x0034
        L_0x004b:
            r14 = r3
            r10 = 0
            r13 = 0
            r15 = 0
            r16 = 0
            r17 = 0
        L_0x0053:
            r7 = 8
            if (r13 != 0) goto L_0x010c
            int r9 = r14.r()
            if (r9 == r7) goto L_0x00a1
            int r15 = r15 + 1
            if (r1 != 0) goto L_0x0066
            int r9 = r14.s()
            goto L_0x006a
        L_0x0066:
            int r9 = r14.i()
        L_0x006a:
            float r9 = (float) r9
            float r16 = r16 + r9
            if (r14 == r5) goto L_0x007a
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r9 = r14.A
            r9 = r9[r27]
            int r9 = r9.b()
            float r9 = (float) r9
            float r16 = r16 + r9
        L_0x007a:
            if (r14 == r6) goto L_0x0089
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r9 = r14.A
            int r19 = r27 + 1
            r9 = r9[r19]
            int r9 = r9.b()
            float r9 = (float) r9
            float r16 = r16 + r9
        L_0x0089:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r9 = r14.A
            r9 = r9[r27]
            int r9 = r9.b()
            float r9 = (float) r9
            float r17 = r17 + r9
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r9 = r14.A
            int r19 = r27 + 1
            r9 = r9[r19]
            int r9 = r9.b()
            float r9 = (float) r9
            float r17 = r17 + r9
        L_0x00a1:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r9 = r14.A
            r9 = r9[r27]
            int r9 = r14.r()
            if (r9 == r7) goto L_0x00df
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r7 = r14.C
            r7 = r7[r1]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r9 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r7 != r9) goto L_0x00df
            int r10 = r10 + 1
            if (r1 != 0) goto L_0x00c7
            int r7 = r14.e
            if (r7 == 0) goto L_0x00bd
            r7 = 0
            return r7
        L_0x00bd:
            r7 = 0
            int r9 = r14.h
            if (r9 != 0) goto L_0x00c6
            int r9 = r14.i
            if (r9 == 0) goto L_0x00d6
        L_0x00c6:
            return r7
        L_0x00c7:
            r7 = 0
            int r9 = r14.f
            if (r9 == 0) goto L_0x00cd
            return r7
        L_0x00cd:
            int r9 = r14.k
            if (r9 != 0) goto L_0x00de
            int r9 = r14.l
            if (r9 == 0) goto L_0x00d6
            goto L_0x00de
        L_0x00d6:
            float r9 = r14.G
            r18 = 0
            int r9 = (r9 > r18 ? 1 : (r9 == r18 ? 0 : -1))
            if (r9 == 0) goto L_0x00df
        L_0x00de:
            return r7
        L_0x00df:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r7 = r14.A
            int r9 = r27 + 1
            r7 = r7[r9]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r7 = r7.d
            if (r7 == 0) goto L_0x0101
            androidx.constraintlayout.solver.widgets.ConstraintWidget r7 = r7.b
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r9 = r7.A
            r20 = r7
            r7 = r9[r27]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r7 = r7.d
            if (r7 == 0) goto L_0x0101
            r7 = r9[r27]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r7 = r7.d
            androidx.constraintlayout.solver.widgets.ConstraintWidget r7 = r7.b
            if (r7 == r14) goto L_0x00fe
            goto L_0x0101
        L_0x00fe:
            r19 = r20
            goto L_0x0103
        L_0x0101:
            r19 = 0
        L_0x0103:
            if (r19 == 0) goto L_0x0109
            r14 = r19
            goto L_0x0053
        L_0x0109:
            r13 = 1
            goto L_0x0053
        L_0x010c:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r9 = r3.A
            r9 = r9[r27]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r9 = r9.d()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r13 = r4.A
            int r19 = r27 + 1
            r13 = r13[r19]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r13 = r13.d()
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r7 = r9.d
            if (r7 == 0) goto L_0x0382
            r21 = r3
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r3 = r13.d
            if (r3 != 0) goto L_0x012a
            goto L_0x0382
        L_0x012a:
            int r7 = r7.b
            r0 = 1
            if (r7 != r0) goto L_0x0380
            int r3 = r3.b
            if (r3 == r0) goto L_0x0135
            goto L_0x0380
        L_0x0135:
            if (r10 <= 0) goto L_0x013b
            if (r10 == r15) goto L_0x013b
            r0 = 0
            return r0
        L_0x013b:
            if (r2 != 0) goto L_0x0144
            if (r11 != 0) goto L_0x0144
            if (r12 == 0) goto L_0x0142
            goto L_0x0144
        L_0x0142:
            r7 = 0
            goto L_0x015d
        L_0x0144:
            if (r5 == 0) goto L_0x0150
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r5.A
            r0 = r0[r27]
            int r0 = r0.b()
            float r7 = (float) r0
            goto L_0x0151
        L_0x0150:
            r7 = 0
        L_0x0151:
            if (r6 == 0) goto L_0x015d
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r6.A
            r0 = r0[r19]
            int r0 = r0.b()
            float r0 = (float) r0
            float r7 = r7 + r0
        L_0x015d:
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r0 = r9.d
            float r0 = r0.g
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r3 = r13.d
            float r3 = r3.g
            int r6 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r6 >= 0) goto L_0x016b
            float r3 = r3 - r0
            goto L_0x016d
        L_0x016b:
            float r3 = r0 - r3
        L_0x016d:
            float r3 = r3 - r16
            r22 = 1
            if (r10 <= 0) goto L_0x0223
            if (r10 != r15) goto L_0x0223
            androidx.constraintlayout.solver.widgets.ConstraintWidget r2 = r14.k()
            if (r2 == 0) goto L_0x0189
            androidx.constraintlayout.solver.widgets.ConstraintWidget r2 = r14.k()
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r2 = r2.C
            r2 = r2[r1]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r5 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            if (r2 != r5) goto L_0x0189
            r2 = 0
            return r2
        L_0x0189:
            float r3 = r3 + r16
            float r3 = r3 - r17
            r2 = r0
            r0 = r21
        L_0x0190:
            if (r0 == 0) goto L_0x0221
            androidx.constraintlayout.solver.Metrics r5 = androidx.constraintlayout.solver.LinearSystem.q
            if (r5 == 0) goto L_0x01a8
            long r6 = r5.B
            long r6 = r6 - r22
            r5.B = r6
            long r6 = r5.s
            long r6 = r6 + r22
            r5.s = r6
            long r6 = r5.y
            long r6 = r6 + r22
            r5.y = r6
        L_0x01a8:
            androidx.constraintlayout.solver.widgets.ConstraintWidget[] r5 = r0.i0
            r5 = r5[r1]
            if (r5 != 0) goto L_0x01b4
            if (r0 != r4) goto L_0x01b1
            goto L_0x01b4
        L_0x01b1:
            r13 = r25
            goto L_0x021e
        L_0x01b4:
            float r6 = (float) r10
            float r6 = r3 / r6
            r7 = 0
            int r11 = (r8 > r7 ? 1 : (r8 == r7 ? 0 : -1))
            if (r11 <= 0) goto L_0x01cd
            float[] r6 = r0.g0
            r7 = r6[r1]
            r11 = -1082130432(0xffffffffbf800000, float:-1.0)
            int r7 = (r7 > r11 ? 1 : (r7 == r11 ? 0 : -1))
            if (r7 != 0) goto L_0x01c8
            r7 = 0
            goto L_0x01ce
        L_0x01c8:
            r6 = r6[r1]
            float r6 = r6 * r3
            float r6 = r6 / r8
        L_0x01cd:
            r7 = r6
        L_0x01ce:
            int r6 = r0.r()
            r11 = 8
            if (r6 != r11) goto L_0x01d7
            r7 = 0
        L_0x01d7:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r6 = r0.A
            r6 = r6[r27]
            int r6 = r6.b()
            float r6 = (float) r6
            float r2 = r2 + r6
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r6 = r0.A
            r6 = r6[r27]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r6 = r6.d()
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r11 = r9.f
            r6.a((androidx.constraintlayout.solver.widgets.ResolutionAnchor) r11, (float) r2)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r6 = r0.A
            r6 = r6[r19]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r6 = r6.d()
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r11 = r9.f
            float r2 = r2 + r7
            r6.a((androidx.constraintlayout.solver.widgets.ResolutionAnchor) r11, (float) r2)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r6 = r0.A
            r6 = r6[r27]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r6 = r6.d()
            r13 = r25
            r6.a((androidx.constraintlayout.solver.LinearSystem) r13)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r6 = r0.A
            r6 = r6[r19]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r6 = r6.d()
            r6.a((androidx.constraintlayout.solver.LinearSystem) r13)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r0.A
            r0 = r0[r19]
            int r0 = r0.b()
            float r0 = (float) r0
            float r2 = r2 + r0
        L_0x021e:
            r0 = r5
            goto L_0x0190
        L_0x0221:
            r0 = 1
            return r0
        L_0x0223:
            r13 = r25
            r6 = 0
            int r6 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r6 >= 0) goto L_0x022d
            r2 = 1
            r11 = 0
            r12 = 0
        L_0x022d:
            if (r2 == 0) goto L_0x02ac
            float r3 = r3 - r7
            r2 = r21
            float r5 = r2.b((int) r1)
            float r3 = r3 * r5
            float r0 = r0 + r3
        L_0x0239:
            if (r2 == 0) goto L_0x02b3
            androidx.constraintlayout.solver.Metrics r3 = androidx.constraintlayout.solver.LinearSystem.q
            if (r3 == 0) goto L_0x0251
            long r5 = r3.B
            long r5 = r5 - r22
            r3.B = r5
            long r5 = r3.s
            long r5 = r5 + r22
            r3.s = r5
            long r5 = r3.y
            long r5 = r5 + r22
            r3.y = r5
        L_0x0251:
            androidx.constraintlayout.solver.widgets.ConstraintWidget[] r3 = r2.i0
            r3 = r3[r1]
            if (r3 != 0) goto L_0x0259
            if (r2 != r4) goto L_0x02aa
        L_0x0259:
            if (r1 != 0) goto L_0x0260
            int r5 = r2.s()
            goto L_0x0264
        L_0x0260:
            int r5 = r2.i()
        L_0x0264:
            float r5 = (float) r5
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r6 = r2.A
            r6 = r6[r27]
            int r6 = r6.b()
            float r6 = (float) r6
            float r0 = r0 + r6
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r6 = r2.A
            r6 = r6[r27]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r6 = r6.d()
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r7 = r9.f
            r6.a((androidx.constraintlayout.solver.widgets.ResolutionAnchor) r7, (float) r0)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r6 = r2.A
            r6 = r6[r19]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r6 = r6.d()
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r7 = r9.f
            float r0 = r0 + r5
            r6.a((androidx.constraintlayout.solver.widgets.ResolutionAnchor) r7, (float) r0)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r5 = r2.A
            r5 = r5[r27]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r5 = r5.d()
            r5.a((androidx.constraintlayout.solver.LinearSystem) r13)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r5 = r2.A
            r5 = r5[r19]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r5 = r5.d()
            r5.a((androidx.constraintlayout.solver.LinearSystem) r13)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r2.A
            r2 = r2[r19]
            int r2 = r2.b()
            float r2 = (float) r2
            float r0 = r0 + r2
        L_0x02aa:
            r2 = r3
            goto L_0x0239
        L_0x02ac:
            r2 = r21
            if (r11 != 0) goto L_0x02b6
            if (r12 == 0) goto L_0x02b3
            goto L_0x02b6
        L_0x02b3:
            r0 = 1
            goto L_0x037f
        L_0x02b6:
            if (r11 == 0) goto L_0x02ba
        L_0x02b8:
            float r3 = r3 - r7
            goto L_0x02bd
        L_0x02ba:
            if (r12 == 0) goto L_0x02bd
            goto L_0x02b8
        L_0x02bd:
            int r6 = r15 + 1
            float r6 = (float) r6
            float r6 = r3 / r6
            if (r12 == 0) goto L_0x02cf
            r7 = 1
            if (r15 <= r7) goto L_0x02cb
            int r6 = r15 + -1
            float r6 = (float) r6
            goto L_0x02cd
        L_0x02cb:
            r6 = 1073741824(0x40000000, float:2.0)
        L_0x02cd:
            float r6 = r3 / r6
        L_0x02cf:
            int r3 = r2.r()
            r7 = 8
            if (r3 == r7) goto L_0x02da
            float r3 = r0 + r6
            goto L_0x02db
        L_0x02da:
            r3 = r0
        L_0x02db:
            if (r12 == 0) goto L_0x02ea
            r7 = 1
            if (r15 <= r7) goto L_0x02ea
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r3 = r5.A
            r3 = r3[r27]
            int r3 = r3.b()
            float r3 = (float) r3
            float r3 = r3 + r0
        L_0x02ea:
            if (r11 == 0) goto L_0x02f8
            if (r5 == 0) goto L_0x02f8
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r5.A
            r0 = r0[r27]
            int r0 = r0.b()
            float r0 = (float) r0
            float r3 = r3 + r0
        L_0x02f8:
            if (r2 == 0) goto L_0x02b3
            androidx.constraintlayout.solver.Metrics r0 = androidx.constraintlayout.solver.LinearSystem.q
            if (r0 == 0) goto L_0x0310
            long r7 = r0.B
            long r7 = r7 - r22
            r0.B = r7
            long r7 = r0.s
            long r7 = r7 + r22
            r0.s = r7
            long r7 = r0.y
            long r7 = r7 + r22
            r0.y = r7
        L_0x0310:
            androidx.constraintlayout.solver.widgets.ConstraintWidget[] r0 = r2.i0
            r0 = r0[r1]
            if (r0 != 0) goto L_0x031c
            if (r2 != r4) goto L_0x0319
            goto L_0x031c
        L_0x0319:
            r7 = 8
            goto L_0x037c
        L_0x031c:
            if (r1 != 0) goto L_0x0323
            int r7 = r2.s()
            goto L_0x0327
        L_0x0323:
            int r7 = r2.i()
        L_0x0327:
            float r7 = (float) r7
            if (r2 == r5) goto L_0x0334
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r8 = r2.A
            r8 = r8[r27]
            int r8 = r8.b()
            float r8 = (float) r8
            float r3 = r3 + r8
        L_0x0334:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r8 = r2.A
            r8 = r8[r27]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r8 = r8.d()
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r10 = r9.f
            r8.a((androidx.constraintlayout.solver.widgets.ResolutionAnchor) r10, (float) r3)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r8 = r2.A
            r8 = r8[r19]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r8 = r8.d()
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r10 = r9.f
            float r11 = r3 + r7
            r8.a((androidx.constraintlayout.solver.widgets.ResolutionAnchor) r10, (float) r11)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r8 = r2.A
            r8 = r8[r27]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r8 = r8.d()
            r8.a((androidx.constraintlayout.solver.LinearSystem) r13)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r8 = r2.A
            r8 = r8[r19]
            androidx.constraintlayout.solver.widgets.ResolutionAnchor r8 = r8.d()
            r8.a((androidx.constraintlayout.solver.LinearSystem) r13)
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r2.A
            r2 = r2[r19]
            int r2 = r2.b()
            float r2 = (float) r2
            float r7 = r7 + r2
            float r3 = r3 + r7
            if (r0 == 0) goto L_0x0319
            int r2 = r0.r()
            r7 = 8
            if (r2 == r7) goto L_0x037c
            float r3 = r3 + r6
        L_0x037c:
            r2 = r0
            goto L_0x02f8
        L_0x037f:
            return r0
        L_0x0380:
            r0 = 0
            return r0
        L_0x0382:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.Optimizer.a(androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer, androidx.constraintlayout.solver.LinearSystem, int, int, androidx.constraintlayout.solver.widgets.ChainHead):boolean");
    }

    static void a(ConstraintWidget constraintWidget, int i, int i2) {
        int i3 = i * 2;
        int i4 = i3 + 1;
        constraintWidget.A[i3].d().f = constraintWidget.k().s.d();
        constraintWidget.A[i3].d().g = (float) i2;
        constraintWidget.A[i3].d().b = 1;
        constraintWidget.A[i4].d().f = constraintWidget.A[i3].d();
        constraintWidget.A[i4].d().g = (float) constraintWidget.d(i);
        constraintWidget.A[i4].d().b = 1;
    }
}
