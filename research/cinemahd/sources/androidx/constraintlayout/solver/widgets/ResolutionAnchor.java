package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.LinearSystem;
import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;

public class ResolutionAnchor extends ResolutionNode {
    ConstraintAnchor c;
    ResolutionAnchor d;
    float e;
    ResolutionAnchor f;
    float g;
    int h = 0;
    private ResolutionAnchor i;
    private ResolutionDimension j = null;
    private int k = 1;
    private ResolutionDimension l = null;
    private int m = 1;

    public ResolutionAnchor(ConstraintAnchor constraintAnchor) {
        this.c = constraintAnchor;
    }

    /* access modifiers changed from: package-private */
    public String a(int i2) {
        return i2 == 1 ? "DIRECT" : i2 == 2 ? "CENTER" : i2 == 3 ? "MATCH" : i2 == 4 ? "CHAIN" : i2 == 5 ? "BARRIER" : "UNCONNECTED";
    }

    public void a(ResolutionAnchor resolutionAnchor, float f2) {
        if (this.b == 0 || !(this.f == resolutionAnchor || this.g == f2)) {
            this.f = resolutionAnchor;
            this.g = f2;
            if (this.b == 1) {
                b();
            }
            a();
        }
    }

    public void b(int i2) {
        this.h = i2;
    }

    public void d() {
        super.d();
        this.d = null;
        this.e = 0.0f;
        this.j = null;
        this.k = 1;
        this.l = null;
        this.m = 1;
        this.f = null;
        this.g = 0.0f;
        this.i = null;
        this.h = 0;
    }

    public void e() {
        ResolutionAnchor resolutionAnchor;
        ResolutionAnchor resolutionAnchor2;
        ResolutionAnchor resolutionAnchor3;
        ResolutionAnchor resolutionAnchor4;
        ResolutionAnchor resolutionAnchor5;
        ResolutionAnchor resolutionAnchor6;
        float f2;
        float f3;
        float f4;
        float f5;
        ResolutionAnchor resolutionAnchor7;
        boolean z = true;
        if (this.b != 1 && this.h != 4) {
            ResolutionDimension resolutionDimension = this.j;
            if (resolutionDimension != null) {
                if (resolutionDimension.b == 1) {
                    this.e = ((float) this.k) * resolutionDimension.c;
                } else {
                    return;
                }
            }
            ResolutionDimension resolutionDimension2 = this.l;
            if (resolutionDimension2 != null) {
                if (resolutionDimension2.b == 1) {
                    float f6 = resolutionDimension2.c;
                } else {
                    return;
                }
            }
            if (this.h == 1 && ((resolutionAnchor7 = this.d) == null || resolutionAnchor7.b == 1)) {
                ResolutionAnchor resolutionAnchor8 = this.d;
                if (resolutionAnchor8 == null) {
                    this.f = this;
                    this.g = this.e;
                } else {
                    this.f = resolutionAnchor8.f;
                    this.g = resolutionAnchor8.g + this.e;
                }
                a();
            } else if (this.h == 2 && (resolutionAnchor4 = this.d) != null && resolutionAnchor4.b == 1 && (resolutionAnchor5 = this.i) != null && (resolutionAnchor6 = resolutionAnchor5.d) != null && resolutionAnchor6.b == 1) {
                if (LinearSystem.h() != null) {
                    LinearSystem.h().w++;
                }
                this.f = this.d.f;
                ResolutionAnchor resolutionAnchor9 = this.i;
                resolutionAnchor9.f = resolutionAnchor9.d.f;
                ConstraintAnchor.Type type = this.c.c;
                int i2 = 0;
                if (!(type == ConstraintAnchor.Type.RIGHT || type == ConstraintAnchor.Type.BOTTOM)) {
                    z = false;
                }
                if (z) {
                    f3 = this.d.g;
                    f2 = this.i.d.g;
                } else {
                    f3 = this.i.d.g;
                    f2 = this.d.g;
                }
                float f7 = f3 - f2;
                ConstraintAnchor constraintAnchor = this.c;
                ConstraintAnchor.Type type2 = constraintAnchor.c;
                if (type2 == ConstraintAnchor.Type.LEFT || type2 == ConstraintAnchor.Type.RIGHT) {
                    f5 = f7 - ((float) this.c.b.s());
                    f4 = this.c.b.V;
                } else {
                    f5 = f7 - ((float) constraintAnchor.b.i());
                    f4 = this.c.b.W;
                }
                int b = this.c.b();
                int b2 = this.i.c.b();
                if (this.c.g() == this.i.c.g()) {
                    f4 = 0.5f;
                    b2 = 0;
                } else {
                    i2 = b;
                }
                float f8 = (float) i2;
                float f9 = (float) b2;
                float f10 = (f5 - f8) - f9;
                if (z) {
                    ResolutionAnchor resolutionAnchor10 = this.i;
                    resolutionAnchor10.g = resolutionAnchor10.d.g + f9 + (f10 * f4);
                    this.g = (this.d.g - f8) - (f10 * (1.0f - f4));
                } else {
                    this.g = this.d.g + f8 + (f10 * f4);
                    ResolutionAnchor resolutionAnchor11 = this.i;
                    resolutionAnchor11.g = (resolutionAnchor11.d.g - f9) - (f10 * (1.0f - f4));
                }
                a();
                this.i.a();
            } else if (this.h == 3 && (resolutionAnchor = this.d) != null && resolutionAnchor.b == 1 && (resolutionAnchor2 = this.i) != null && (resolutionAnchor3 = resolutionAnchor2.d) != null && resolutionAnchor3.b == 1) {
                if (LinearSystem.h() != null) {
                    LinearSystem.h().x++;
                }
                ResolutionAnchor resolutionAnchor12 = this.d;
                this.f = resolutionAnchor12.f;
                ResolutionAnchor resolutionAnchor13 = this.i;
                ResolutionAnchor resolutionAnchor14 = resolutionAnchor13.d;
                resolutionAnchor13.f = resolutionAnchor14.f;
                this.g = resolutionAnchor12.g + this.e;
                resolutionAnchor13.g = resolutionAnchor14.g + resolutionAnchor13.e;
                a();
                this.i.a();
            } else if (this.h == 5) {
                this.c.b.G();
            }
        }
    }

    public float f() {
        return this.g;
    }

    public void g() {
        ConstraintAnchor g2 = this.c.g();
        if (g2 != null) {
            if (g2.g() == this.c) {
                this.h = 4;
                g2.d().h = 4;
            }
            int b = this.c.b();
            ConstraintAnchor.Type type = this.c.c;
            if (type == ConstraintAnchor.Type.RIGHT || type == ConstraintAnchor.Type.BOTTOM) {
                b = -b;
            }
            a(g2.d(), b);
        }
    }

    public String toString() {
        if (this.b != 1) {
            return "{ " + this.c + " UNRESOLVED} type: " + a(this.h);
        } else if (this.f == this) {
            return "[" + this.c + ", RESOLVED: " + this.g + "]  type: " + a(this.h);
        } else {
            return "[" + this.c + ", RESOLVED: " + this.f + ":" + this.g + "] type: " + a(this.h);
        }
    }

    public void b(ResolutionAnchor resolutionAnchor, float f2) {
        this.i = resolutionAnchor;
    }

    public void b(ResolutionAnchor resolutionAnchor, int i2, ResolutionDimension resolutionDimension) {
        this.i = resolutionAnchor;
        this.l = resolutionDimension;
        this.m = i2;
    }

    public void a(int i2, ResolutionAnchor resolutionAnchor, int i3) {
        this.h = i2;
        this.d = resolutionAnchor;
        this.e = (float) i3;
        this.d.a(this);
    }

    public void a(ResolutionAnchor resolutionAnchor, int i2) {
        this.d = resolutionAnchor;
        this.e = (float) i2;
        this.d.a(this);
    }

    public void a(ResolutionAnchor resolutionAnchor, int i2, ResolutionDimension resolutionDimension) {
        this.d = resolutionAnchor;
        this.d.a(this);
        this.j = resolutionDimension;
        this.k = i2;
        this.j.a(this);
    }

    /* access modifiers changed from: package-private */
    public void a(LinearSystem linearSystem) {
        SolverVariable e2 = this.c.e();
        ResolutionAnchor resolutionAnchor = this.f;
        if (resolutionAnchor == null) {
            linearSystem.a(e2, (int) (this.g + 0.5f));
        } else {
            linearSystem.a(e2, linearSystem.a((Object) resolutionAnchor.c), (int) (this.g + 0.5f), 6);
        }
    }
}
