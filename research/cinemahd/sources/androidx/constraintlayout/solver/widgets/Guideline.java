package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.LinearSystem;
import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.ArrayList;

public class Guideline extends ConstraintWidget {
    protected float k0 = -1.0f;
    protected int l0 = -1;
    protected int m0 = -1;
    private ConstraintAnchor n0 = this.t;
    private int o0;
    private boolean p0;

    /* renamed from: androidx.constraintlayout.solver.widgets.Guideline$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f500a = new int[ConstraintAnchor.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|20) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type[] r0 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f500a = r0
                int[] r0 = f500a     // Catch:{ NoSuchFieldError -> 0x0014 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f500a     // Catch:{ NoSuchFieldError -> 0x001f }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f500a     // Catch:{ NoSuchFieldError -> 0x002a }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f500a     // Catch:{ NoSuchFieldError -> 0x0035 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f500a     // Catch:{ NoSuchFieldError -> 0x0040 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BASELINE     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f500a     // Catch:{ NoSuchFieldError -> 0x004b }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = f500a     // Catch:{ NoSuchFieldError -> 0x0056 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER_X     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = f500a     // Catch:{ NoSuchFieldError -> 0x0062 }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER_Y     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = f500a     // Catch:{ NoSuchFieldError -> 0x006e }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor$Type r1 = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.NONE     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.Guideline.AnonymousClass1.<clinit>():void");
        }
    }

    public Guideline() {
        this.o0 = 0;
        this.p0 = false;
        new Rectangle();
        this.B.clear();
        this.B.add(this.n0);
        int length = this.A.length;
        for (int i = 0; i < length; i++) {
            this.A[i] = this.n0;
        }
    }

    public int J() {
        return this.o0;
    }

    public ConstraintAnchor a(ConstraintAnchor.Type type) {
        switch (AnonymousClass1.f500a[type.ordinal()]) {
            case 1:
            case 2:
                if (this.o0 == 1) {
                    return this.n0;
                }
                break;
            case 3:
            case 4:
                if (this.o0 == 0) {
                    return this.n0;
                }
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return null;
        }
        throw new AssertionError(type.name());
    }

    public boolean a() {
        return true;
    }

    public ArrayList<ConstraintAnchor> b() {
        return this.B;
    }

    public void c(LinearSystem linearSystem) {
        if (k() != null) {
            int b = linearSystem.b((Object) this.n0);
            if (this.o0 == 1) {
                r(b);
                s(0);
                g(k().i());
                o(0);
                return;
            }
            r(0);
            s(b);
            o(k().s());
            g(0);
        }
    }

    public void e(float f) {
        if (f > -1.0f) {
            this.k0 = f;
            this.l0 = -1;
            this.m0 = -1;
        }
    }

    public void t(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = i;
            this.m0 = -1;
        }
    }

    public void u(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = -1;
            this.m0 = i;
        }
    }

    public void v(int i) {
        if (this.o0 != i) {
            this.o0 = i;
            this.B.clear();
            if (this.o0 == 1) {
                this.n0 = this.s;
            } else {
                this.n0 = this.t;
            }
            this.B.add(this.n0);
            int length = this.A.length;
            for (int i2 = 0; i2 < length; i2++) {
                this.A[i2] = this.n0;
            }
        }
    }

    public void a(int i) {
        ConstraintWidget k = k();
        if (k != null) {
            if (J() == 1) {
                this.t.d().a(1, k.t.d(), 0);
                this.v.d().a(1, k.t.d(), 0);
                if (this.l0 != -1) {
                    this.s.d().a(1, k.s.d(), this.l0);
                    this.u.d().a(1, k.s.d(), this.l0);
                } else if (this.m0 != -1) {
                    this.s.d().a(1, k.u.d(), -this.m0);
                    this.u.d().a(1, k.u.d(), -this.m0);
                } else if (this.k0 != -1.0f && k.j() == ConstraintWidget.DimensionBehaviour.FIXED) {
                    int i2 = (int) (((float) k.E) * this.k0);
                    this.s.d().a(1, k.s.d(), i2);
                    this.u.d().a(1, k.s.d(), i2);
                }
            } else {
                this.s.d().a(1, k.s.d(), 0);
                this.u.d().a(1, k.s.d(), 0);
                if (this.l0 != -1) {
                    this.t.d().a(1, k.t.d(), this.l0);
                    this.v.d().a(1, k.t.d(), this.l0);
                } else if (this.m0 != -1) {
                    this.t.d().a(1, k.v.d(), -this.m0);
                    this.v.d().a(1, k.v.d(), -this.m0);
                } else if (this.k0 != -1.0f && k.q() == ConstraintWidget.DimensionBehaviour.FIXED) {
                    int i3 = (int) (((float) k.F) * this.k0);
                    this.t.d().a(1, k.t.d(), i3);
                    this.v.d().a(1, k.t.d(), i3);
                }
            }
        }
    }

    public void a(LinearSystem linearSystem) {
        ConstraintWidgetContainer constraintWidgetContainer = (ConstraintWidgetContainer) k();
        if (constraintWidgetContainer != null) {
            ConstraintAnchor a2 = constraintWidgetContainer.a(ConstraintAnchor.Type.LEFT);
            ConstraintAnchor a3 = constraintWidgetContainer.a(ConstraintAnchor.Type.RIGHT);
            ConstraintWidget constraintWidget = this.D;
            boolean z = constraintWidget != null && constraintWidget.C[0] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            if (this.o0 == 0) {
                a2 = constraintWidgetContainer.a(ConstraintAnchor.Type.TOP);
                a3 = constraintWidgetContainer.a(ConstraintAnchor.Type.BOTTOM);
                ConstraintWidget constraintWidget2 = this.D;
                z = constraintWidget2 != null && constraintWidget2.C[1] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            }
            if (this.l0 != -1) {
                SolverVariable a4 = linearSystem.a((Object) this.n0);
                linearSystem.a(a4, linearSystem.a((Object) a2), this.l0, 6);
                if (z) {
                    linearSystem.b(linearSystem.a((Object) a3), a4, 0, 5);
                }
            } else if (this.m0 != -1) {
                SolverVariable a5 = linearSystem.a((Object) this.n0);
                SolverVariable a6 = linearSystem.a((Object) a3);
                linearSystem.a(a5, a6, -this.m0, 6);
                if (z) {
                    linearSystem.b(a5, linearSystem.a((Object) a2), 0, 5);
                    linearSystem.b(a6, a5, 0, 5);
                }
            } else if (this.k0 != -1.0f) {
                linearSystem.a(LinearSystem.a(linearSystem, linearSystem.a((Object) this.n0), linearSystem.a((Object) a2), linearSystem.a((Object) a3), this.k0, this.p0));
            }
        }
    }
}
