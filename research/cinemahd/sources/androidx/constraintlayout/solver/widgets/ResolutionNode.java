package androidx.constraintlayout.solver.widgets;

import java.util.HashSet;
import java.util.Iterator;

public class ResolutionNode {

    /* renamed from: a  reason: collision with root package name */
    HashSet<ResolutionNode> f502a = new HashSet<>(2);
    int b = 0;

    public void a(ResolutionNode resolutionNode) {
        this.f502a.add(resolutionNode);
    }

    public void b() {
        this.b = 0;
        Iterator<ResolutionNode> it2 = this.f502a.iterator();
        while (it2.hasNext()) {
            it2.next().b();
        }
    }

    public boolean c() {
        return this.b == 1;
    }

    public void d() {
        this.b = 0;
        this.f502a.clear();
    }

    public void e() {
    }

    public void a() {
        this.b = 1;
        Iterator<ResolutionNode> it2 = this.f502a.iterator();
        while (it2.hasNext()) {
            it2.next().e();
        }
    }
}
