package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.LinearSystem;
import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.ArrayList;

public class Barrier extends Helper {
    private int m0 = 0;
    private ArrayList<ResolutionAnchor> n0 = new ArrayList<>(4);
    private boolean o0 = true;

    public void F() {
        super.F();
        this.n0.clear();
    }

    public void G() {
        ResolutionAnchor resolutionAnchor;
        float f;
        ResolutionAnchor resolutionAnchor2;
        int i = this.m0;
        float f2 = Float.MAX_VALUE;
        if (i != 0) {
            if (i == 1) {
                resolutionAnchor = this.u.d();
            } else if (i == 2) {
                resolutionAnchor = this.t.d();
            } else if (i == 3) {
                resolutionAnchor = this.v.d();
            } else {
                return;
            }
            f2 = 0.0f;
        } else {
            resolutionAnchor = this.s.d();
        }
        int size = this.n0.size();
        ResolutionAnchor resolutionAnchor3 = null;
        int i2 = 0;
        while (i2 < size) {
            ResolutionAnchor resolutionAnchor4 = this.n0.get(i2);
            if (resolutionAnchor4.b == 1) {
                int i3 = this.m0;
                if (i3 == 0 || i3 == 2) {
                    f = resolutionAnchor4.g;
                    if (f < f2) {
                        resolutionAnchor2 = resolutionAnchor4.f;
                    } else {
                        i2++;
                    }
                } else {
                    f = resolutionAnchor4.g;
                    if (f > f2) {
                        resolutionAnchor2 = resolutionAnchor4.f;
                    } else {
                        i2++;
                    }
                }
                resolutionAnchor3 = resolutionAnchor2;
                f2 = f;
                i2++;
            } else {
                return;
            }
        }
        if (LinearSystem.h() != null) {
            LinearSystem.h().z++;
        }
        resolutionAnchor.f = resolutionAnchor3;
        resolutionAnchor.g = f2;
        resolutionAnchor.a();
        int i4 = this.m0;
        if (i4 == 0) {
            this.u.d().a(resolutionAnchor3, f2);
        } else if (i4 == 1) {
            this.s.d().a(resolutionAnchor3, f2);
        } else if (i4 == 2) {
            this.v.d().a(resolutionAnchor3, f2);
        } else if (i4 == 3) {
            this.t.d().a(resolutionAnchor3, f2);
        }
    }

    public void a(int i) {
        ResolutionAnchor resolutionAnchor;
        ResolutionAnchor resolutionAnchor2;
        ConstraintWidget constraintWidget = this.D;
        if (constraintWidget != null && ((ConstraintWidgetContainer) constraintWidget).t(2)) {
            int i2 = this.m0;
            if (i2 == 0) {
                resolutionAnchor = this.s.d();
            } else if (i2 == 1) {
                resolutionAnchor = this.u.d();
            } else if (i2 == 2) {
                resolutionAnchor = this.t.d();
            } else if (i2 == 3) {
                resolutionAnchor = this.v.d();
            } else {
                return;
            }
            resolutionAnchor.b(5);
            int i3 = this.m0;
            if (i3 == 0 || i3 == 1) {
                this.t.d().a((ResolutionAnchor) null, 0.0f);
                this.v.d().a((ResolutionAnchor) null, 0.0f);
            } else {
                this.s.d().a((ResolutionAnchor) null, 0.0f);
                this.u.d().a((ResolutionAnchor) null, 0.0f);
            }
            this.n0.clear();
            for (int i4 = 0; i4 < this.l0; i4++) {
                ConstraintWidget constraintWidget2 = this.k0[i4];
                if (this.o0 || constraintWidget2.a()) {
                    int i5 = this.m0;
                    if (i5 == 0) {
                        resolutionAnchor2 = constraintWidget2.s.d();
                    } else if (i5 == 1) {
                        resolutionAnchor2 = constraintWidget2.u.d();
                    } else if (i5 == 2) {
                        resolutionAnchor2 = constraintWidget2.t.d();
                    } else if (i5 != 3) {
                        resolutionAnchor2 = null;
                    } else {
                        resolutionAnchor2 = constraintWidget2.v.d();
                    }
                    if (resolutionAnchor2 != null) {
                        this.n0.add(resolutionAnchor2);
                        resolutionAnchor2.a(resolutionAnchor);
                    }
                }
            }
        }
    }

    public boolean a() {
        return true;
    }

    public void c(boolean z) {
        this.o0 = z;
    }

    public void t(int i) {
        this.m0 = i;
    }

    public void a(LinearSystem linearSystem) {
        ConstraintAnchor[] constraintAnchorArr;
        boolean z;
        int i;
        int i2;
        ConstraintAnchor[] constraintAnchorArr2 = this.A;
        constraintAnchorArr2[0] = this.s;
        constraintAnchorArr2[2] = this.t;
        constraintAnchorArr2[1] = this.u;
        constraintAnchorArr2[3] = this.v;
        int i3 = 0;
        while (true) {
            constraintAnchorArr = this.A;
            if (i3 >= constraintAnchorArr.length) {
                break;
            }
            constraintAnchorArr[i3].i = linearSystem.a((Object) constraintAnchorArr[i3]);
            i3++;
        }
        int i4 = this.m0;
        if (i4 >= 0 && i4 < 4) {
            ConstraintAnchor constraintAnchor = constraintAnchorArr[i4];
            int i5 = 0;
            while (true) {
                if (i5 >= this.l0) {
                    z = false;
                    break;
                }
                ConstraintWidget constraintWidget = this.k0[i5];
                if ((this.o0 || constraintWidget.a()) && ((((i = this.m0) == 0 || i == 1) && constraintWidget.j() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) || (((i2 = this.m0) == 2 || i2 == 3) && constraintWidget.q() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT))) {
                    z = true;
                } else {
                    i5++;
                }
            }
            int i6 = this.m0;
            if (i6 == 0 || i6 == 1 ? k().j() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT : k().q() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                z = false;
            }
            for (int i7 = 0; i7 < this.l0; i7++) {
                ConstraintWidget constraintWidget2 = this.k0[i7];
                if (this.o0 || constraintWidget2.a()) {
                    SolverVariable a2 = linearSystem.a((Object) constraintWidget2.A[this.m0]);
                    ConstraintAnchor[] constraintAnchorArr3 = constraintWidget2.A;
                    int i8 = this.m0;
                    constraintAnchorArr3[i8].i = a2;
                    if (i8 == 0 || i8 == 2) {
                        linearSystem.b(constraintAnchor.i, a2, z);
                    } else {
                        linearSystem.a(constraintAnchor.i, a2, z);
                    }
                }
            }
            int i9 = this.m0;
            if (i9 == 0) {
                linearSystem.a(this.u.i, this.s.i, 0, 6);
                if (!z) {
                    linearSystem.a(this.s.i, this.D.u.i, 0, 5);
                }
            } else if (i9 == 1) {
                linearSystem.a(this.s.i, this.u.i, 0, 6);
                if (!z) {
                    linearSystem.a(this.s.i, this.D.s.i, 0, 5);
                }
            } else if (i9 == 2) {
                linearSystem.a(this.v.i, this.t.i, 0, 6);
                if (!z) {
                    linearSystem.a(this.t.i, this.D.v.i, 0, 5);
                }
            } else if (i9 == 3) {
                linearSystem.a(this.t.i, this.v.i, 0, 6);
                if (!z) {
                    linearSystem.a(this.t.i, this.D.t.i, 0, 5);
                }
            }
        }
    }
}
