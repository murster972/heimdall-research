package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Analyzer {
    private Analyzer() {
    }

    public static void a(ConstraintWidgetContainer constraintWidgetContainer) {
        if ((constraintWidgetContainer.M() & 32) != 32) {
            b(constraintWidgetContainer);
            return;
        }
        constraintWidgetContainer.D0 = true;
        constraintWidgetContainer.x0 = false;
        constraintWidgetContainer.y0 = false;
        constraintWidgetContainer.z0 = false;
        ArrayList<ConstraintWidget> arrayList = constraintWidgetContainer.k0;
        List<ConstraintWidgetGroup> list = constraintWidgetContainer.w0;
        boolean z = constraintWidgetContainer.j() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean z2 = constraintWidgetContainer.q() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean z3 = z || z2;
        list.clear();
        for (ConstraintWidget next : arrayList) {
            next.p = null;
            next.d0 = false;
            next.F();
        }
        for (ConstraintWidget next2 : arrayList) {
            if (next2.p == null && !a(next2, list, z3)) {
                b(constraintWidgetContainer);
                constraintWidgetContainer.D0 = false;
                return;
            }
        }
        int i = 0;
        int i2 = 0;
        for (ConstraintWidgetGroup next3 : list) {
            i = Math.max(i, a(next3, 0));
            i2 = Math.max(i2, a(next3, 1));
        }
        if (z) {
            constraintWidgetContainer.a(ConstraintWidget.DimensionBehaviour.FIXED);
            constraintWidgetContainer.o(i);
            constraintWidgetContainer.x0 = true;
            constraintWidgetContainer.y0 = true;
            constraintWidgetContainer.A0 = i;
        }
        if (z2) {
            constraintWidgetContainer.b(ConstraintWidget.DimensionBehaviour.FIXED);
            constraintWidgetContainer.g(i2);
            constraintWidgetContainer.x0 = true;
            constraintWidgetContainer.z0 = true;
            constraintWidgetContainer.B0 = i2;
        }
        a(list, 0, constraintWidgetContainer.s());
        a(list, 1, constraintWidgetContainer.i());
    }

    private static void b(ConstraintWidgetContainer constraintWidgetContainer) {
        constraintWidgetContainer.w0.clear();
        constraintWidgetContainer.w0.add(0, new ConstraintWidgetGroup(constraintWidgetContainer.k0));
    }

    private static boolean a(ConstraintWidget constraintWidget, List<ConstraintWidgetGroup> list, boolean z) {
        ConstraintWidgetGroup constraintWidgetGroup = new ConstraintWidgetGroup(new ArrayList(), true);
        list.add(constraintWidgetGroup);
        return a(constraintWidget, constraintWidgetGroup, list, z);
    }

    private static boolean a(ConstraintWidget constraintWidget, ConstraintWidgetGroup constraintWidgetGroup, List<ConstraintWidgetGroup> list, boolean z) {
        ConstraintAnchor constraintAnchor;
        ConstraintAnchor constraintAnchor2;
        ConstraintAnchor constraintAnchor3;
        ConstraintWidget constraintWidget2;
        ConstraintAnchor constraintAnchor4;
        ConstraintAnchor constraintAnchor5;
        ConstraintAnchor constraintAnchor6;
        ConstraintAnchor constraintAnchor7;
        ConstraintWidget constraintWidget3;
        ConstraintAnchor constraintAnchor8;
        if (constraintWidget == null) {
            return true;
        }
        constraintWidget.c0 = false;
        ConstraintWidgetContainer constraintWidgetContainer = (ConstraintWidgetContainer) constraintWidget.k();
        ConstraintWidgetGroup constraintWidgetGroup2 = constraintWidget.p;
        if (constraintWidgetGroup2 == null) {
            constraintWidget.b0 = true;
            constraintWidgetGroup.f499a.add(constraintWidget);
            constraintWidget.p = constraintWidgetGroup;
            if (constraintWidget.s.d == null && constraintWidget.u.d == null && constraintWidget.t.d == null && constraintWidget.v.d == null && constraintWidget.w.d == null && constraintWidget.z.d == null) {
                a(constraintWidgetContainer, constraintWidget, constraintWidgetGroup);
                if (z) {
                    return false;
                }
            }
            if (!(constraintWidget.t.d == null || constraintWidget.v.d == null)) {
                ConstraintWidget.DimensionBehaviour q = constraintWidgetContainer.q();
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (z) {
                    a(constraintWidgetContainer, constraintWidget, constraintWidgetGroup);
                    return false;
                } else if (!(constraintWidget.t.d.b == constraintWidget.k() && constraintWidget.v.d.b == constraintWidget.k())) {
                    a(constraintWidgetContainer, constraintWidget, constraintWidgetGroup);
                }
            }
            if (!(constraintWidget.s.d == null || constraintWidget.u.d == null)) {
                ConstraintWidget.DimensionBehaviour j = constraintWidgetContainer.j();
                ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (z) {
                    a(constraintWidgetContainer, constraintWidget, constraintWidgetGroup);
                    return false;
                } else if (!(constraintWidget.s.d.b == constraintWidget.k() && constraintWidget.u.d.b == constraintWidget.k())) {
                    a(constraintWidgetContainer, constraintWidget, constraintWidgetGroup);
                }
            }
            if (((constraintWidget.j() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) ^ (constraintWidget.q() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT)) && constraintWidget.G != 0.0f) {
                a(constraintWidget);
            } else if (constraintWidget.j() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.q() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                a(constraintWidgetContainer, constraintWidget, constraintWidgetGroup);
                if (z) {
                    return false;
                }
            }
            if (((constraintWidget.s.d == null && constraintWidget.u.d == null) || (((constraintAnchor5 = constraintWidget.s.d) != null && constraintAnchor5.b == constraintWidget.D && constraintWidget.u.d == null) || (((constraintAnchor6 = constraintWidget.u.d) != null && constraintAnchor6.b == constraintWidget.D && constraintWidget.s.d == null) || ((constraintAnchor7 = constraintWidget.s.d) != null && constraintAnchor7.b == (constraintWidget3 = constraintWidget.D) && (constraintAnchor8 = constraintWidget.u.d) != null && constraintAnchor8.b == constraintWidget3)))) && constraintWidget.z.d == null && !(constraintWidget instanceof Guideline) && !(constraintWidget instanceof Helper)) {
                constraintWidgetGroup.f.add(constraintWidget);
            }
            if (((constraintWidget.t.d == null && constraintWidget.v.d == null) || (((constraintAnchor = constraintWidget.t.d) != null && constraintAnchor.b == constraintWidget.D && constraintWidget.v.d == null) || (((constraintAnchor2 = constraintWidget.v.d) != null && constraintAnchor2.b == constraintWidget.D && constraintWidget.t.d == null) || ((constraintAnchor3 = constraintWidget.t.d) != null && constraintAnchor3.b == (constraintWidget2 = constraintWidget.D) && (constraintAnchor4 = constraintWidget.v.d) != null && constraintAnchor4.b == constraintWidget2)))) && constraintWidget.z.d == null && constraintWidget.w.d == null && !(constraintWidget instanceof Guideline) && !(constraintWidget instanceof Helper)) {
                constraintWidgetGroup.g.add(constraintWidget);
            }
            if (constraintWidget instanceof Helper) {
                a(constraintWidgetContainer, constraintWidget, constraintWidgetGroup);
                if (z) {
                    return false;
                }
                Helper helper = (Helper) constraintWidget;
                for (int i = 0; i < helper.l0; i++) {
                    if (!a(helper.k0[i], constraintWidgetGroup, list, z)) {
                        return false;
                    }
                }
            }
            for (ConstraintAnchor constraintAnchor9 : constraintWidget.A) {
                ConstraintAnchor constraintAnchor10 = constraintAnchor9.d;
                if (!(constraintAnchor10 == null || constraintAnchor10.b == constraintWidget.k())) {
                    if (constraintAnchor9.c == ConstraintAnchor.Type.CENTER) {
                        a(constraintWidgetContainer, constraintWidget, constraintWidgetGroup);
                        if (z) {
                            return false;
                        }
                    } else {
                        a(constraintAnchor9);
                    }
                    if (!a(constraintAnchor9.d.b, constraintWidgetGroup, list, z)) {
                        return false;
                    }
                }
            }
            return true;
        }
        if (constraintWidgetGroup2 != constraintWidgetGroup) {
            constraintWidgetGroup.f499a.addAll(constraintWidgetGroup2.f499a);
            constraintWidgetGroup.f.addAll(constraintWidget.p.f);
            constraintWidgetGroup.g.addAll(constraintWidget.p.g);
            if (!constraintWidget.p.d) {
                constraintWidgetGroup.d = false;
            }
            list.remove(constraintWidget.p);
            for (ConstraintWidget constraintWidget4 : constraintWidget.p.f499a) {
                constraintWidget4.p = constraintWidgetGroup;
            }
        }
        return true;
    }

    private static void a(ConstraintWidgetContainer constraintWidgetContainer, ConstraintWidget constraintWidget, ConstraintWidgetGroup constraintWidgetGroup) {
        constraintWidgetGroup.d = false;
        constraintWidgetContainer.D0 = false;
        constraintWidget.b0 = false;
    }

    private static int a(ConstraintWidgetGroup constraintWidgetGroup, int i) {
        int i2 = i * 2;
        List<ConstraintWidget> a2 = constraintWidgetGroup.a(i);
        int size = a2.size();
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            ConstraintWidget constraintWidget = a2.get(i4);
            ConstraintAnchor[] constraintAnchorArr = constraintWidget.A;
            int i5 = i2 + 1;
            i3 = Math.max(i3, a(constraintWidget, i, constraintAnchorArr[i5].d == null || !(constraintAnchorArr[i2].d == null || constraintAnchorArr[i5].d == null), 0));
        }
        constraintWidgetGroup.e[i] = i3;
        return i3;
    }

    private static int a(ConstraintWidget constraintWidget, int i, boolean z, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int s;
        int i9;
        int i10;
        int i11;
        ConstraintWidget constraintWidget2 = constraintWidget;
        int i12 = i;
        boolean z2 = z;
        int i13 = 0;
        if (!constraintWidget2.b0) {
            return 0;
        }
        boolean z3 = constraintWidget2.w.d != null && i12 == 1;
        if (z2) {
            i6 = constraintWidget.c();
            i5 = constraintWidget.i() - constraintWidget.c();
            i4 = i12 * 2;
            i3 = i4 + 1;
        } else {
            i6 = constraintWidget.i() - constraintWidget.c();
            i5 = constraintWidget.c();
            i3 = i12 * 2;
            i4 = i3 + 1;
        }
        ConstraintAnchor[] constraintAnchorArr = constraintWidget2.A;
        if (constraintAnchorArr[i3].d == null || constraintAnchorArr[i4].d != null) {
            i7 = i3;
            i8 = 1;
        } else {
            i7 = i4;
            i4 = i3;
            i8 = -1;
        }
        int i14 = z3 ? i2 - i6 : i2;
        int b = (constraintWidget2.A[i4].b() * i8) + a(constraintWidget, i);
        int i15 = i14 + b;
        int s2 = (i12 == 0 ? constraintWidget.s() : constraintWidget.i()) * i8;
        Iterator<ResolutionNode> it2 = constraintWidget2.A[i4].d().f502a.iterator();
        while (it2.hasNext()) {
            i13 = Math.max(i13, a(((ResolutionAnchor) it2.next()).c.b, i12, z2, i15));
        }
        int i16 = 0;
        for (Iterator<ResolutionNode> it3 = constraintWidget2.A[i7].d().f502a.iterator(); it3.hasNext(); it3 = it3) {
            i16 = Math.max(i16, a(((ResolutionAnchor) it3.next()).c.b, i12, z2, s2 + i15));
        }
        if (z3) {
            i13 -= i6;
            s = i16 + i5;
        } else {
            s = i16 + ((i12 == 0 ? constraintWidget.s() : constraintWidget.i()) * i8);
        }
        int i17 = 1;
        if (i12 == 1) {
            Iterator<ResolutionNode> it4 = constraintWidget2.w.d().f502a.iterator();
            int i18 = 0;
            while (it4.hasNext()) {
                Iterator<ResolutionNode> it5 = it4;
                ResolutionAnchor resolutionAnchor = (ResolutionAnchor) it4.next();
                if (i8 == i17) {
                    i18 = Math.max(i18, a(resolutionAnchor.c.b, i12, z2, i6 + i15));
                    i11 = i7;
                } else {
                    i11 = i7;
                    i18 = Math.max(i18, a(resolutionAnchor.c.b, i12, z2, (i5 * i8) + i15));
                }
                it4 = it5;
                i7 = i11;
                i17 = 1;
            }
            i9 = i7;
            int i19 = i18;
            i10 = (constraintWidget2.w.d().f502a.size() <= 0 || z3) ? i19 : i8 == 1 ? i19 + i6 : i19 - i5;
        } else {
            i9 = i7;
            i10 = 0;
        }
        int max = b + Math.max(i13, Math.max(s, i10));
        int i20 = i15 + s2;
        if (i8 != -1) {
            int i21 = i15;
            i15 = i20;
            i20 = i21;
        }
        if (z2) {
            Optimizer.a(constraintWidget2, i12, i20);
            constraintWidget2.a(i20, i15, i12);
        } else {
            constraintWidget2.p.a(constraintWidget2, i12);
            constraintWidget2.d(i20, i12);
        }
        if (constraintWidget.c(i) == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget2.G != 0.0f) {
            constraintWidget2.p.a(constraintWidget2, i12);
        }
        ConstraintAnchor[] constraintAnchorArr2 = constraintWidget2.A;
        if (!(constraintAnchorArr2[i4].d == null || constraintAnchorArr2[i9].d == null)) {
            ConstraintWidget k = constraintWidget.k();
            ConstraintAnchor[] constraintAnchorArr3 = constraintWidget2.A;
            if (constraintAnchorArr3[i4].d.b == k && constraintAnchorArr3[i9].d.b == k) {
                constraintWidget2.p.a(constraintWidget2, i12);
            }
        }
        return max;
    }

    private static void a(ConstraintAnchor constraintAnchor) {
        ResolutionAnchor d = constraintAnchor.d();
        ConstraintAnchor constraintAnchor2 = constraintAnchor.d;
        if (constraintAnchor2 != null && constraintAnchor2.d != constraintAnchor) {
            constraintAnchor2.d().a(d);
        }
    }

    public static void a(List<ConstraintWidgetGroup> list, int i, int i2) {
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            for (ConstraintWidget next : list.get(i3).b(i)) {
                if (next.b0) {
                    a(next, i, i2);
                }
            }
        }
    }

    private static void a(ConstraintWidget constraintWidget, int i, int i2) {
        int i3 = i * 2;
        ConstraintAnchor[] constraintAnchorArr = constraintWidget.A;
        ConstraintAnchor constraintAnchor = constraintAnchorArr[i3];
        ConstraintAnchor constraintAnchor2 = constraintAnchorArr[i3 + 1];
        if ((constraintAnchor.d == null || constraintAnchor2.d == null) ? false : true) {
            Optimizer.a(constraintWidget, i, a(constraintWidget, i) + constraintAnchor.b());
        } else if (constraintWidget.G == 0.0f || constraintWidget.c(i) != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            int e = i2 - constraintWidget.e(i);
            int d = e - constraintWidget.d(i);
            constraintWidget.a(d, e, i);
            Optimizer.a(constraintWidget, i, d);
        } else {
            int a2 = a(constraintWidget);
            int i4 = (int) constraintWidget.A[i3].d().g;
            constraintAnchor2.d().f = constraintAnchor.d();
            constraintAnchor2.d().g = (float) a2;
            constraintAnchor2.d().b = 1;
            constraintWidget.a(i4, i4 + a2, i);
        }
    }

    private static int a(ConstraintWidget constraintWidget, int i) {
        ConstraintWidget constraintWidget2;
        ConstraintAnchor constraintAnchor;
        int i2 = i * 2;
        ConstraintAnchor[] constraintAnchorArr = constraintWidget.A;
        ConstraintAnchor constraintAnchor2 = constraintAnchorArr[i2];
        ConstraintAnchor constraintAnchor3 = constraintAnchorArr[i2 + 1];
        ConstraintAnchor constraintAnchor4 = constraintAnchor2.d;
        if (constraintAnchor4 == null || constraintAnchor4.b != (constraintWidget2 = constraintWidget.D) || (constraintAnchor = constraintAnchor3.d) == null || constraintAnchor.b != constraintWidget2) {
            return 0;
        }
        return (int) (((float) (((constraintWidget2.d(i) - constraintAnchor2.b()) - constraintAnchor3.b()) - constraintWidget.d(i))) * (i == 0 ? constraintWidget.V : constraintWidget.W));
    }

    private static int a(ConstraintWidget constraintWidget) {
        float f;
        float f2;
        if (constraintWidget.j() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            if (constraintWidget.H == 0) {
                f2 = ((float) constraintWidget.i()) * constraintWidget.G;
            } else {
                f2 = ((float) constraintWidget.i()) / constraintWidget.G;
            }
            int i = (int) f2;
            constraintWidget.o(i);
            return i;
        } else if (constraintWidget.q() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            return -1;
        } else {
            if (constraintWidget.H == 1) {
                f = ((float) constraintWidget.s()) * constraintWidget.G;
            } else {
                f = ((float) constraintWidget.s()) / constraintWidget.G;
            }
            int i2 = (int) f;
            constraintWidget.g(i2);
            return i2;
        }
    }
}
