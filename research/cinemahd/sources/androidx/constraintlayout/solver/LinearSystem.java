package androidx.constraintlayout.solver;

import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.Arrays;
import java.util.HashMap;

public class LinearSystem {
    private static int p = 1000;
    public static Metrics q;

    /* renamed from: a  reason: collision with root package name */
    int f485a = 0;
    private HashMap<String, SolverVariable> b = null;
    private Row c;
    private int d = 32;
    private int e;
    ArrayRow[] f;
    public boolean g;
    private boolean[] h;
    int i;
    int j;
    private int k;
    final Cache l;
    private SolverVariable[] m;
    private int n;
    private final Row o;

    interface Row {
        SolverVariable a(LinearSystem linearSystem, boolean[] zArr);

        void a(Row row);

        void a(SolverVariable solverVariable);

        void clear();

        SolverVariable getKey();
    }

    public LinearSystem() {
        int i2 = this.d;
        this.e = i2;
        this.f = null;
        this.g = false;
        this.h = new boolean[i2];
        this.i = 1;
        this.j = 0;
        this.k = i2;
        this.m = new SolverVariable[p];
        this.n = 0;
        ArrayRow[] arrayRowArr = new ArrayRow[i2];
        this.f = new ArrayRow[i2];
        j();
        this.l = new Cache();
        this.c = new GoalRow(this.l);
        this.o = new ArrayRow(this.l);
    }

    private final void d(ArrayRow arrayRow) {
        if (this.j > 0) {
            arrayRow.d.a(arrayRow, this.f);
            if (arrayRow.d.f482a == 0) {
                arrayRow.e = true;
            }
        }
    }

    private void g() {
        for (int i2 = 0; i2 < this.j; i2++) {
            ArrayRow arrayRow = this.f[i2];
            arrayRow.f483a.e = arrayRow.b;
        }
    }

    public static Metrics h() {
        return q;
    }

    private void i() {
        this.d *= 2;
        this.f = (ArrayRow[]) Arrays.copyOf(this.f, this.d);
        Cache cache = this.l;
        cache.c = (SolverVariable[]) Arrays.copyOf(cache.c, this.d);
        int i2 = this.d;
        this.h = new boolean[i2];
        this.e = i2;
        this.k = i2;
        Metrics metrics = q;
        if (metrics != null) {
            metrics.d++;
            metrics.p = Math.max(metrics.p, (long) i2);
            Metrics metrics2 = q;
            metrics2.D = metrics2.p;
        }
    }

    private void j() {
        int i2 = 0;
        while (true) {
            ArrayRow[] arrayRowArr = this.f;
            if (i2 < arrayRowArr.length) {
                ArrayRow arrayRow = arrayRowArr[i2];
                if (arrayRow != null) {
                    this.l.f484a.release(arrayRow);
                }
                this.f[i2] = null;
                i2++;
            } else {
                return;
            }
        }
    }

    public SolverVariable a(Object obj) {
        SolverVariable solverVariable = null;
        if (obj == null) {
            return null;
        }
        if (this.i + 1 >= this.e) {
            i();
        }
        if (obj instanceof ConstraintAnchor) {
            ConstraintAnchor constraintAnchor = (ConstraintAnchor) obj;
            solverVariable = constraintAnchor.e();
            if (solverVariable == null) {
                constraintAnchor.a(this.l);
                solverVariable = constraintAnchor.e();
            }
            int i2 = solverVariable.b;
            if (i2 == -1 || i2 > this.f485a || this.l.c[i2] == null) {
                if (solverVariable.b != -1) {
                    solverVariable.a();
                }
                this.f485a++;
                this.i++;
                int i3 = this.f485a;
                solverVariable.b = i3;
                solverVariable.g = SolverVariable.Type.UNRESTRICTED;
                this.l.c[i3] = solverVariable;
            }
        }
        return solverVariable;
    }

    public ArrayRow b() {
        ArrayRow acquire = this.l.f484a.acquire();
        if (acquire == null) {
            acquire = new ArrayRow(this.l);
        } else {
            acquire.d();
        }
        SolverVariable.b();
        return acquire;
    }

    public SolverVariable c() {
        Metrics metrics = q;
        if (metrics != null) {
            metrics.n++;
        }
        if (this.i + 1 >= this.e) {
            i();
        }
        SolverVariable a2 = a(SolverVariable.Type.SLACK, (String) null);
        this.f485a++;
        this.i++;
        int i2 = this.f485a;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    public void e() throws Exception {
        Metrics metrics = q;
        if (metrics != null) {
            metrics.e++;
        }
        if (this.g) {
            Metrics metrics2 = q;
            if (metrics2 != null) {
                metrics2.r++;
            }
            boolean z = false;
            int i2 = 0;
            while (true) {
                if (i2 >= this.j) {
                    z = true;
                    break;
                } else if (!this.f[i2].e) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                a(this.c);
                return;
            }
            Metrics metrics3 = q;
            if (metrics3 != null) {
                metrics3.q++;
            }
            g();
            return;
        }
        a(this.c);
    }

    public void f() {
        Cache cache;
        int i2 = 0;
        while (true) {
            cache = this.l;
            SolverVariable[] solverVariableArr = cache.c;
            if (i2 >= solverVariableArr.length) {
                break;
            }
            SolverVariable solverVariable = solverVariableArr[i2];
            if (solverVariable != null) {
                solverVariable.a();
            }
            i2++;
        }
        cache.b.a(this.m, this.n);
        this.n = 0;
        Arrays.fill(this.l.c, (Object) null);
        HashMap<String, SolverVariable> hashMap = this.b;
        if (hashMap != null) {
            hashMap.clear();
        }
        this.f485a = 0;
        this.c.clear();
        this.i = 1;
        for (int i3 = 0; i3 < this.j; i3++) {
            this.f[i3].c = false;
        }
        j();
        this.j = 0;
    }

    private void b(ArrayRow arrayRow) {
        arrayRow.a(this, 0);
    }

    public Cache d() {
        return this.l;
    }

    public int b(Object obj) {
        SolverVariable e2 = ((ConstraintAnchor) obj).e();
        if (e2 != null) {
            return (int) (e2.e + 0.5f);
        }
        return 0;
    }

    private int b(Row row) throws Exception {
        float f2;
        boolean z;
        int i2 = 0;
        while (true) {
            f2 = 0.0f;
            if (i2 >= this.j) {
                z = false;
                break;
            }
            ArrayRow[] arrayRowArr = this.f;
            if (arrayRowArr[i2].f483a.g != SolverVariable.Type.UNRESTRICTED && arrayRowArr[i2].b < 0.0f) {
                z = true;
                break;
            }
            i2++;
        }
        if (!z) {
            return 0;
        }
        boolean z2 = false;
        int i3 = 0;
        while (!z2) {
            Metrics metrics = q;
            if (metrics != null) {
                metrics.k++;
            }
            i3++;
            int i4 = 0;
            int i5 = -1;
            int i6 = -1;
            float f3 = Float.MAX_VALUE;
            int i7 = 0;
            while (i4 < this.j) {
                ArrayRow arrayRow = this.f[i4];
                if (arrayRow.f483a.g != SolverVariable.Type.UNRESTRICTED && !arrayRow.e && arrayRow.b < f2) {
                    int i8 = 1;
                    while (i8 < this.i) {
                        SolverVariable solverVariable = this.l.c[i8];
                        float b2 = arrayRow.d.b(solverVariable);
                        if (b2 > f2) {
                            int i9 = i7;
                            float f4 = f3;
                            int i10 = i6;
                            int i11 = i5;
                            for (int i12 = 0; i12 < 7; i12++) {
                                float f5 = solverVariable.f[i12] / b2;
                                if ((f5 < f4 && i12 == i9) || i12 > i9) {
                                    i10 = i8;
                                    i11 = i4;
                                    f4 = f5;
                                    i9 = i12;
                                }
                            }
                            i5 = i11;
                            i6 = i10;
                            f3 = f4;
                            i7 = i9;
                        }
                        i8++;
                        f2 = 0.0f;
                    }
                }
                i4++;
                f2 = 0.0f;
            }
            if (i5 != -1) {
                ArrayRow arrayRow2 = this.f[i5];
                arrayRow2.f483a.c = -1;
                Metrics metrics2 = q;
                if (metrics2 != null) {
                    metrics2.j++;
                }
                arrayRow2.d(this.l.c[i6]);
                SolverVariable solverVariable2 = arrayRow2.f483a;
                solverVariable2.c = i5;
                solverVariable2.c(arrayRow2);
            } else {
                z2 = true;
            }
            if (i3 > this.i / 2) {
                z2 = true;
            }
            f2 = 0.0f;
        }
        return i3;
    }

    private final void c(ArrayRow arrayRow) {
        ArrayRow[] arrayRowArr = this.f;
        int i2 = this.j;
        if (arrayRowArr[i2] != null) {
            this.l.f484a.release(arrayRowArr[i2]);
        }
        ArrayRow[] arrayRowArr2 = this.f;
        int i3 = this.j;
        arrayRowArr2[i3] = arrayRow;
        SolverVariable solverVariable = arrayRow.f483a;
        solverVariable.c = i3;
        this.j = i3 + 1;
        solverVariable.c(arrayRow);
    }

    public SolverVariable a() {
        Metrics metrics = q;
        if (metrics != null) {
            metrics.o++;
        }
        if (this.i + 1 >= this.e) {
            i();
        }
        SolverVariable a2 = a(SolverVariable.Type.SLACK, (String) null);
        this.f485a++;
        this.i++;
        int i2 = this.f485a;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    public void c(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, int i3) {
        ArrayRow b2 = b();
        SolverVariable c2 = c();
        c2.d = 0;
        b2.b(solverVariable, solverVariable2, c2, i2);
        if (i3 != 6) {
            a(b2, (int) (b2.d.b(c2) * -1.0f), i3);
        }
        a(b2);
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayRow arrayRow, int i2, int i3) {
        arrayRow.a(a(i3, (String) null), i2);
    }

    public SolverVariable a(int i2, String str) {
        Metrics metrics = q;
        if (metrics != null) {
            metrics.m++;
        }
        if (this.i + 1 >= this.e) {
            i();
        }
        SolverVariable a2 = a(SolverVariable.Type.ERROR, str);
        this.f485a++;
        this.i++;
        int i3 = this.f485a;
        a2.b = i3;
        a2.d = i2;
        this.l.c[i3] = a2;
        this.c.a(a2);
        return a2;
    }

    public void b(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, int i3) {
        ArrayRow b2 = b();
        SolverVariable c2 = c();
        c2.d = 0;
        b2.a(solverVariable, solverVariable2, c2, i2);
        if (i3 != 6) {
            a(b2, (int) (b2.d.b(c2) * -1.0f), i3);
        }
        a(b2);
    }

    private SolverVariable a(SolverVariable.Type type, String str) {
        SolverVariable acquire = this.l.b.acquire();
        if (acquire == null) {
            acquire = new SolverVariable(type, str);
            acquire.a(type, str);
        } else {
            acquire.a();
            acquire.a(type, str);
        }
        int i2 = this.n;
        int i3 = p;
        if (i2 >= i3) {
            p = i3 * 2;
            this.m = (SolverVariable[]) Arrays.copyOf(this.m, p);
        }
        SolverVariable[] solverVariableArr = this.m;
        int i4 = this.n;
        this.n = i4 + 1;
        solverVariableArr[i4] = acquire;
        return acquire;
    }

    public void b(SolverVariable solverVariable, SolverVariable solverVariable2, boolean z) {
        ArrayRow b2 = b();
        SolverVariable c2 = c();
        c2.d = 0;
        b2.b(solverVariable, solverVariable2, c2, 0);
        if (z) {
            a(b2, (int) (b2.d.b(c2) * -1.0f), 1);
        }
        a(b2);
    }

    /* access modifiers changed from: package-private */
    public void a(Row row) throws Exception {
        Metrics metrics = q;
        if (metrics != null) {
            metrics.t++;
            metrics.u = Math.max(metrics.u, (long) this.i);
            Metrics metrics2 = q;
            metrics2.v = Math.max(metrics2.v, (long) this.j);
        }
        d((ArrayRow) row);
        b(row);
        a(row, false);
        g();
    }

    public void a(ArrayRow arrayRow) {
        SolverVariable c2;
        if (arrayRow != null) {
            Metrics metrics = q;
            if (metrics != null) {
                metrics.f++;
                if (arrayRow.e) {
                    metrics.g++;
                }
            }
            if (this.j + 1 >= this.k || this.i + 1 >= this.e) {
                i();
            }
            boolean z = false;
            if (!arrayRow.e) {
                d(arrayRow);
                if (!arrayRow.c()) {
                    arrayRow.a();
                    if (arrayRow.a(this)) {
                        SolverVariable a2 = a();
                        arrayRow.f483a = a2;
                        c(arrayRow);
                        this.o.a((Row) arrayRow);
                        a(this.o, true);
                        if (a2.c == -1) {
                            if (arrayRow.f483a == a2 && (c2 = arrayRow.c(a2)) != null) {
                                Metrics metrics2 = q;
                                if (metrics2 != null) {
                                    metrics2.j++;
                                }
                                arrayRow.d(c2);
                            }
                            if (!arrayRow.e) {
                                arrayRow.f483a.c(arrayRow);
                            }
                            this.j--;
                        }
                        z = true;
                    }
                    if (!arrayRow.b()) {
                        return;
                    }
                } else {
                    return;
                }
            }
            if (!z) {
                c(arrayRow);
            }
        }
    }

    private final int a(Row row, boolean z) {
        Metrics metrics = q;
        if (metrics != null) {
            metrics.h++;
        }
        for (int i2 = 0; i2 < this.i; i2++) {
            this.h[i2] = false;
        }
        boolean z2 = false;
        int i3 = 0;
        while (!z2) {
            Metrics metrics2 = q;
            if (metrics2 != null) {
                metrics2.i++;
            }
            i3++;
            if (i3 >= this.i * 2) {
                return i3;
            }
            if (row.getKey() != null) {
                this.h[row.getKey().b] = true;
            }
            SolverVariable a2 = row.a(this, this.h);
            if (a2 != null) {
                boolean[] zArr = this.h;
                int i4 = a2.b;
                if (zArr[i4]) {
                    return i3;
                }
                zArr[i4] = true;
            }
            if (a2 != null) {
                int i5 = -1;
                float f2 = Float.MAX_VALUE;
                for (int i6 = 0; i6 < this.j; i6++) {
                    ArrayRow arrayRow = this.f[i6];
                    if (arrayRow.f483a.g != SolverVariable.Type.UNRESTRICTED && !arrayRow.e && arrayRow.b(a2)) {
                        float b2 = arrayRow.d.b(a2);
                        if (b2 < 0.0f) {
                            float f3 = (-arrayRow.b) / b2;
                            if (f3 < f2) {
                                i5 = i6;
                                f2 = f3;
                            }
                        }
                    }
                }
                if (i5 > -1) {
                    ArrayRow arrayRow2 = this.f[i5];
                    arrayRow2.f483a.c = -1;
                    Metrics metrics3 = q;
                    if (metrics3 != null) {
                        metrics3.j++;
                    }
                    arrayRow2.d(a2);
                    SolverVariable solverVariable = arrayRow2.f483a;
                    solverVariable.c = i5;
                    solverVariable.c(arrayRow2);
                }
            }
            z2 = true;
        }
        return i3;
    }

    public void a(SolverVariable solverVariable, SolverVariable solverVariable2, boolean z) {
        ArrayRow b2 = b();
        SolverVariable c2 = c();
        c2.d = 0;
        b2.a(solverVariable, solverVariable2, c2, 0);
        if (z) {
            a(b2, (int) (b2.d.b(c2) * -1.0f), 1);
        }
        a(b2);
    }

    public void a(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, float f2, SolverVariable solverVariable3, SolverVariable solverVariable4, int i3, int i4) {
        int i5 = i4;
        ArrayRow b2 = b();
        b2.a(solverVariable, solverVariable2, i2, f2, solverVariable3, solverVariable4, i3);
        if (i5 != 6) {
            b2.a(this, i5);
        }
        a(b2);
    }

    public void a(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f2, int i2) {
        ArrayRow b2 = b();
        b2.a(solverVariable, solverVariable2, solverVariable3, solverVariable4, f2);
        if (i2 != 6) {
            b2.a(this, i2);
        }
        a(b2);
    }

    public ArrayRow a(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, int i3) {
        ArrayRow b2 = b();
        b2.a(solverVariable, solverVariable2, i2);
        if (i3 != 6) {
            b2.a(this, i3);
        }
        a(b2);
        return b2;
    }

    public void a(SolverVariable solverVariable, int i2) {
        int i3 = solverVariable.c;
        if (i3 != -1) {
            ArrayRow arrayRow = this.f[i3];
            if (arrayRow.e) {
                arrayRow.b = (float) i2;
            } else if (arrayRow.d.f482a == 0) {
                arrayRow.e = true;
                arrayRow.b = (float) i2;
            } else {
                ArrayRow b2 = b();
                b2.c(solverVariable, i2);
                a(b2);
            }
        } else {
            ArrayRow b3 = b();
            b3.b(solverVariable, i2);
            a(b3);
        }
    }

    public static ArrayRow a(LinearSystem linearSystem, SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, float f2, boolean z) {
        ArrayRow b2 = linearSystem.b();
        if (z) {
            linearSystem.b(b2);
        }
        b2.a(solverVariable, solverVariable2, solverVariable3, f2);
        return b2;
    }

    public void a(ConstraintWidget constraintWidget, ConstraintWidget constraintWidget2, float f2, int i2) {
        ConstraintWidget constraintWidget3 = constraintWidget;
        ConstraintWidget constraintWidget4 = constraintWidget2;
        SolverVariable a2 = a((Object) constraintWidget3.a(ConstraintAnchor.Type.LEFT));
        SolverVariable a3 = a((Object) constraintWidget3.a(ConstraintAnchor.Type.TOP));
        SolverVariable a4 = a((Object) constraintWidget3.a(ConstraintAnchor.Type.RIGHT));
        SolverVariable a5 = a((Object) constraintWidget3.a(ConstraintAnchor.Type.BOTTOM));
        SolverVariable a6 = a((Object) constraintWidget4.a(ConstraintAnchor.Type.LEFT));
        SolverVariable a7 = a((Object) constraintWidget4.a(ConstraintAnchor.Type.TOP));
        SolverVariable a8 = a((Object) constraintWidget4.a(ConstraintAnchor.Type.RIGHT));
        SolverVariable a9 = a((Object) constraintWidget4.a(ConstraintAnchor.Type.BOTTOM));
        ArrayRow b2 = b();
        double d2 = (double) f2;
        SolverVariable solverVariable = a4;
        double d3 = (double) i2;
        b2.b(a3, a5, a7, a9, (float) (Math.sin(d2) * d3));
        a(b2);
        ArrayRow b3 = b();
        b3.b(a2, solverVariable, a6, a8, (float) (Math.cos(d2) * d3));
        a(b3);
    }
}
