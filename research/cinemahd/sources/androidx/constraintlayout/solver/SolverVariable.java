package androidx.constraintlayout.solver;

import java.util.Arrays;

public class SolverVariable {
    private static int k = 1;

    /* renamed from: a  reason: collision with root package name */
    private String f488a;
    public int b = -1;
    int c = -1;
    public int d = 0;
    public float e;
    float[] f = new float[7];
    Type g;
    ArrayRow[] h = new ArrayRow[8];
    int i = 0;
    public int j = 0;

    public enum Type {
        UNRESTRICTED,
        CONSTANT,
        SLACK,
        ERROR,
        UNKNOWN
    }

    public SolverVariable(Type type, String str) {
        this.g = type;
    }

    static void b() {
        k++;
    }

    public final void a(ArrayRow arrayRow) {
        int i2 = 0;
        while (true) {
            int i3 = this.i;
            if (i2 >= i3) {
                ArrayRow[] arrayRowArr = this.h;
                if (i3 >= arrayRowArr.length) {
                    this.h = (ArrayRow[]) Arrays.copyOf(arrayRowArr, arrayRowArr.length * 2);
                }
                ArrayRow[] arrayRowArr2 = this.h;
                int i4 = this.i;
                arrayRowArr2[i4] = arrayRow;
                this.i = i4 + 1;
                return;
            } else if (this.h[i2] != arrayRow) {
                i2++;
            } else {
                return;
            }
        }
    }

    public final void c(ArrayRow arrayRow) {
        int i2 = this.i;
        for (int i3 = 0; i3 < i2; i3++) {
            ArrayRow[] arrayRowArr = this.h;
            arrayRowArr[i3].d.a(arrayRowArr[i3], arrayRow, false);
        }
        this.i = 0;
    }

    public String toString() {
        return "" + this.f488a;
    }

    public final void b(ArrayRow arrayRow) {
        int i2 = this.i;
        for (int i3 = 0; i3 < i2; i3++) {
            if (this.h[i3] == arrayRow) {
                for (int i4 = 0; i4 < (i2 - i3) - 1; i4++) {
                    ArrayRow[] arrayRowArr = this.h;
                    int i5 = i3 + i4;
                    arrayRowArr[i5] = arrayRowArr[i5 + 1];
                }
                this.i--;
                return;
            }
        }
    }

    public void a() {
        this.f488a = null;
        this.g = Type.UNKNOWN;
        this.d = 0;
        this.b = -1;
        this.c = -1;
        this.e = 0.0f;
        this.i = 0;
        this.j = 0;
    }

    public void a(Type type, String str) {
        this.g = type;
    }
}
