package androidx.transition;

import android.os.IBinder;

class WindowIdApi14 implements WindowIdImpl {

    /* renamed from: a  reason: collision with root package name */
    private final IBinder f1111a;

    WindowIdApi14(IBinder iBinder) {
        this.f1111a = iBinder;
    }

    public boolean equals(Object obj) {
        return (obj instanceof WindowIdApi14) && ((WindowIdApi14) obj).f1111a.equals(this.f1111a);
    }

    public int hashCode() {
        return this.f1111a.hashCode();
    }
}
