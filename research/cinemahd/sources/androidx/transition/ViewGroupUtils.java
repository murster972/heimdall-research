package androidx.transition;

import android.os.Build;
import android.view.ViewGroup;

class ViewGroupUtils {
    private ViewGroupUtils() {
    }

    static ViewGroupOverlayImpl a(ViewGroup viewGroup) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new ViewGroupOverlayApi18(viewGroup);
        }
        return ViewGroupOverlayApi14.a(viewGroup);
    }

    static void a(ViewGroup viewGroup, boolean z) {
        if (Build.VERSION.SDK_INT >= 18) {
            ViewGroupUtilsApi18.a(viewGroup, z);
        } else {
            ViewGroupUtilsApi14.a(viewGroup, z);
        }
    }
}
