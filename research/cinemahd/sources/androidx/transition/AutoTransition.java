package androidx.transition;

public class AutoTransition extends TransitionSet {
    public AutoTransition() {
        q();
    }

    private void q() {
        b(1);
        a((Transition) new Fade(2)).a((Transition) new ChangeBounds()).a((Transition) new Fade(1));
    }
}
