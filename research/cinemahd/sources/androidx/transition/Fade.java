package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.ViewCompat;
import androidx.transition.Transition;

public class Fade extends Visibility {

    private static class FadeAnimatorListener extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        private final View f1081a;
        private boolean b = false;

        FadeAnimatorListener(View view) {
            this.f1081a = view;
        }

        public void onAnimationEnd(Animator animator) {
            ViewUtils.a(this.f1081a, 1.0f);
            if (this.b) {
                this.f1081a.setLayerType(0, (Paint) null);
            }
        }

        public void onAnimationStart(Animator animator) {
            if (ViewCompat.A(this.f1081a) && this.f1081a.getLayerType() == 0) {
                this.b = true;
                this.f1081a.setLayerType(2, (Paint) null);
            }
        }
    }

    public Fade(int i) {
        a(i);
    }

    private Animator a(final View view, float f, float f2) {
        if (f == f2) {
            return null;
        }
        ViewUtils.a(view, f);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, ViewUtils.d, new float[]{f2});
        ofFloat.addListener(new FadeAnimatorListener(view));
        a((Transition.TransitionListener) new TransitionListenerAdapter(this) {
            public void d(Transition transition) {
                ViewUtils.a(view, 1.0f);
                ViewUtils.a(view);
                transition.b((Transition.TransitionListener) this);
            }
        });
        return ofFloat;
    }

    public Animator b(ViewGroup viewGroup, View view, TransitionValues transitionValues, TransitionValues transitionValues2) {
        ViewUtils.e(view);
        return a(view, a(transitionValues, 1.0f), 0.0f);
    }

    public void c(TransitionValues transitionValues) {
        super.c(transitionValues);
        transitionValues.f1098a.put("android:fade:transitionAlpha", Float.valueOf(ViewUtils.c(transitionValues.b)));
    }

    public Fade() {
    }

    public Animator a(ViewGroup viewGroup, View view, TransitionValues transitionValues, TransitionValues transitionValues2) {
        float f = 0.0f;
        float a2 = a(transitionValues, 0.0f);
        if (a2 != 1.0f) {
            f = a2;
        }
        return a(view, f, 1.0f);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r1 = (java.lang.Float) r1.f1098a.get("android:fade:transitionAlpha");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static float a(androidx.transition.TransitionValues r1, float r2) {
        /*
            if (r1 == 0) goto L_0x0012
            java.util.Map<java.lang.String, java.lang.Object> r1 = r1.f1098a
            java.lang.String r0 = "android:fade:transitionAlpha"
            java.lang.Object r1 = r1.get(r0)
            java.lang.Float r1 = (java.lang.Float) r1
            if (r1 == 0) goto L_0x0012
            float r2 = r1.floatValue()
        L_0x0012:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.transition.Fade.a(androidx.transition.TransitionValues, float):float");
    }
}
