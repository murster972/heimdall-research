package androidx.transition;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

class ViewGroupOverlayApi14 extends ViewOverlayApi14 implements ViewGroupOverlayImpl {
    ViewGroupOverlayApi14(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    static ViewGroupOverlayApi14 a(ViewGroup viewGroup) {
        return (ViewGroupOverlayApi14) ViewOverlayApi14.c(viewGroup);
    }

    public void b(View view) {
        this.f1103a.b(view);
    }

    public void a(View view) {
        this.f1103a.a(view);
    }
}
