package androidx.transition;

import android.util.Log;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ViewGroupUtilsApi18 {

    /* renamed from: a  reason: collision with root package name */
    private static Method f1102a;
    private static boolean b;

    private ViewGroupUtilsApi18() {
    }

    static void a(ViewGroup viewGroup, boolean z) {
        a();
        Method method = f1102a;
        if (method != null) {
            try {
                method.invoke(viewGroup, new Object[]{Boolean.valueOf(z)});
            } catch (IllegalAccessException e) {
                Log.i("ViewUtilsApi18", "Failed to invoke suppressLayout method", e);
            } catch (InvocationTargetException e2) {
                Log.i("ViewUtilsApi18", "Error invoking suppressLayout method", e2);
            }
        }
    }

    private static void a() {
        if (!b) {
            Class<ViewGroup> cls = ViewGroup.class;
            try {
                f1102a = cls.getDeclaredMethod("suppressLayout", new Class[]{Boolean.TYPE});
                f1102a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi18", "Failed to retrieve suppressLayout method", e);
            }
            b = true;
        }
    }
}
