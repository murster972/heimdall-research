package androidx.transition;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.collection.ArrayMap;
import androidx.core.view.ViewCompat;
import androidx.transition.Transition;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class TransitionManager {

    /* renamed from: a  reason: collision with root package name */
    private static Transition f1092a = new AutoTransition();
    private static ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>> b = new ThreadLocal<>();
    static ArrayList<ViewGroup> c = new ArrayList<>();

    private static class MultiListener implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {

        /* renamed from: a  reason: collision with root package name */
        Transition f1093a;
        ViewGroup b;

        MultiListener(Transition transition, ViewGroup viewGroup) {
            this.f1093a = transition;
            this.b = viewGroup;
        }

        private void a() {
            this.b.getViewTreeObserver().removeOnPreDrawListener(this);
            this.b.removeOnAttachStateChangeListener(this);
        }

        public boolean onPreDraw() {
            a();
            if (!TransitionManager.c.remove(this.b)) {
                return true;
            }
            final ArrayMap<ViewGroup, ArrayList<Transition>> a2 = TransitionManager.a();
            ArrayList arrayList = a2.get(this.b);
            ArrayList arrayList2 = null;
            if (arrayList == null) {
                arrayList = new ArrayList();
                a2.put(this.b, arrayList);
            } else if (arrayList.size() > 0) {
                arrayList2 = new ArrayList(arrayList);
            }
            arrayList.add(this.f1093a);
            this.f1093a.a((Transition.TransitionListener) new TransitionListenerAdapter() {
                public void d(Transition transition) {
                    ((ArrayList) a2.get(MultiListener.this.b)).remove(transition);
                }
            });
            this.f1093a.a(this.b, false);
            if (arrayList2 != null) {
                Iterator it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    ((Transition) it2.next()).e(this.b);
                }
            }
            this.f1093a.a(this.b);
            return true;
        }

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            a();
            TransitionManager.c.remove(this.b);
            ArrayList arrayList = TransitionManager.a().get(this.b);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    ((Transition) it2.next()).e(this.b);
                }
            }
            this.f1093a.a(true);
        }
    }

    public TransitionManager() {
        new ArrayMap();
        new ArrayMap();
    }

    static ArrayMap<ViewGroup, ArrayList<Transition>> a() {
        ArrayMap<ViewGroup, ArrayList<Transition>> arrayMap;
        WeakReference weakReference = b.get();
        if (weakReference != null && (arrayMap = (ArrayMap) weakReference.get()) != null) {
            return arrayMap;
        }
        ArrayMap<ViewGroup, ArrayList<Transition>> arrayMap2 = new ArrayMap<>();
        b.set(new WeakReference(arrayMap2));
        return arrayMap2;
    }

    private static void b(ViewGroup viewGroup, Transition transition) {
        if (transition != null && viewGroup != null) {
            MultiListener multiListener = new MultiListener(transition, viewGroup);
            viewGroup.addOnAttachStateChangeListener(multiListener);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(multiListener);
        }
    }

    private static void c(ViewGroup viewGroup, Transition transition) {
        ArrayList arrayList = a().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                ((Transition) it2.next()).c((View) viewGroup);
            }
        }
        if (transition != null) {
            transition.a(viewGroup, true);
        }
        Scene a2 = Scene.a(viewGroup);
        if (a2 != null) {
            a2.a();
        }
    }

    public static void a(ViewGroup viewGroup, Transition transition) {
        if (!c.contains(viewGroup) && ViewCompat.E(viewGroup)) {
            c.add(viewGroup);
            if (transition == null) {
                transition = f1092a;
            }
            Transition clone = transition.clone();
            c(viewGroup, clone);
            Scene.a(viewGroup, (Scene) null);
            b(viewGroup, clone);
        }
    }
}
