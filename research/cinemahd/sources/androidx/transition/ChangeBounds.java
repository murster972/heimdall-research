package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.ViewCompat;
import androidx.transition.Transition;
import com.facebook.react.uimanager.ViewProps;
import java.util.Map;

public class ChangeBounds extends Transition {
    private static final String[] M = {"android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY"};
    private static final Property<Drawable, PointF> N = new Property<Drawable, PointF>(PointF.class, "boundsOrigin") {

        /* renamed from: a  reason: collision with root package name */
        private Rect f1074a = new Rect();

        /* renamed from: a */
        public void set(Drawable drawable, PointF pointF) {
            drawable.copyBounds(this.f1074a);
            this.f1074a.offsetTo(Math.round(pointF.x), Math.round(pointF.y));
            drawable.setBounds(this.f1074a);
        }

        /* renamed from: a */
        public PointF get(Drawable drawable) {
            drawable.copyBounds(this.f1074a);
            Rect rect = this.f1074a;
            return new PointF((float) rect.left, (float) rect.top);
        }
    };
    private static final Property<ViewBounds, PointF> O = new Property<ViewBounds, PointF>(PointF.class, "topLeft") {
        /* renamed from: a */
        public PointF get(ViewBounds viewBounds) {
            return null;
        }

        /* renamed from: a */
        public void set(ViewBounds viewBounds, PointF pointF) {
            viewBounds.b(pointF);
        }
    };
    private static final Property<ViewBounds, PointF> P = new Property<ViewBounds, PointF>(PointF.class, "bottomRight") {
        /* renamed from: a */
        public PointF get(ViewBounds viewBounds) {
            return null;
        }

        /* renamed from: a */
        public void set(ViewBounds viewBounds, PointF pointF) {
            viewBounds.a(pointF);
        }
    };
    private static final Property<View, PointF> Q = new Property<View, PointF>(PointF.class, "bottomRight") {
        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }

        /* renamed from: a */
        public void set(View view, PointF pointF) {
            ViewUtils.a(view, view.getLeft(), view.getTop(), Math.round(pointF.x), Math.round(pointF.y));
        }
    };
    private static final Property<View, PointF> R = new Property<View, PointF>(PointF.class, "topLeft") {
        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }

        /* renamed from: a */
        public void set(View view, PointF pointF) {
            ViewUtils.a(view, Math.round(pointF.x), Math.round(pointF.y), view.getRight(), view.getBottom());
        }
    };
    private static final Property<View, PointF> S = new Property<View, PointF>(PointF.class, ViewProps.POSITION) {
        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }

        /* renamed from: a */
        public void set(View view, PointF pointF) {
            int round = Math.round(pointF.x);
            int round2 = Math.round(pointF.y);
            ViewUtils.a(view, round, round2, view.getWidth() + round, view.getHeight() + round2);
        }
    };
    private static RectEvaluator T = new RectEvaluator();
    private int[] J = new int[2];
    private boolean K = false;
    private boolean L = false;

    private void d(TransitionValues transitionValues) {
        View view = transitionValues.b;
        if (ViewCompat.E(view) || view.getWidth() != 0 || view.getHeight() != 0) {
            transitionValues.f1098a.put("android:changeBounds:bounds", new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()));
            transitionValues.f1098a.put("android:changeBounds:parent", transitionValues.b.getParent());
            if (this.L) {
                transitionValues.b.getLocationInWindow(this.J);
                transitionValues.f1098a.put("android:changeBounds:windowX", Integer.valueOf(this.J[0]));
                transitionValues.f1098a.put("android:changeBounds:windowY", Integer.valueOf(this.J[1]));
            }
            if (this.K) {
                transitionValues.f1098a.put("android:changeBounds:clip", ViewCompat.g(view));
            }
        }
    }

    public void a(TransitionValues transitionValues) {
        d(transitionValues);
    }

    public void c(TransitionValues transitionValues) {
        d(transitionValues);
    }

    public String[] m() {
        return M;
    }

    private boolean a(View view, View view2) {
        if (!this.L) {
            return true;
        }
        TransitionValues a2 = a(view, true);
        if (a2 == null) {
            if (view == view2) {
                return true;
            }
        } else if (view2 == a2.b) {
            return true;
        }
        return false;
    }

    private static class ViewBounds {

        /* renamed from: a  reason: collision with root package name */
        private int f1079a;
        private int b;
        private int c;
        private int d;
        private View e;
        private int f;
        private int g;

        ViewBounds(View view) {
            this.e = view;
        }

        /* access modifiers changed from: package-private */
        public void a(PointF pointF) {
            this.c = Math.round(pointF.x);
            this.d = Math.round(pointF.y);
            this.g++;
            if (this.f == this.g) {
                a();
            }
        }

        /* access modifiers changed from: package-private */
        public void b(PointF pointF) {
            this.f1079a = Math.round(pointF.x);
            this.b = Math.round(pointF.y);
            this.f++;
            if (this.f == this.g) {
                a();
            }
        }

        private void a() {
            ViewUtils.a(this.e, this.f1079a, this.b, this.c, this.d);
            this.f = 0;
            this.g = 0;
        }
    }

    public Animator a(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        int i;
        View view;
        Animator animator;
        ObjectAnimator objectAnimator;
        int i2;
        Rect rect;
        ObjectAnimator objectAnimator2;
        TransitionValues transitionValues3 = transitionValues;
        TransitionValues transitionValues4 = transitionValues2;
        if (transitionValues3 == null || transitionValues4 == null) {
            return null;
        }
        Map<String, Object> map = transitionValues3.f1098a;
        Map<String, Object> map2 = transitionValues4.f1098a;
        ViewGroup viewGroup2 = (ViewGroup) map.get("android:changeBounds:parent");
        ViewGroup viewGroup3 = (ViewGroup) map2.get("android:changeBounds:parent");
        if (viewGroup2 == null || viewGroup3 == null) {
            return null;
        }
        View view2 = transitionValues4.b;
        if (a(viewGroup2, viewGroup3)) {
            Rect rect2 = (Rect) transitionValues3.f1098a.get("android:changeBounds:bounds");
            Rect rect3 = (Rect) transitionValues4.f1098a.get("android:changeBounds:bounds");
            int i3 = rect2.left;
            int i4 = rect3.left;
            int i5 = rect2.top;
            int i6 = rect3.top;
            int i7 = rect2.right;
            int i8 = rect3.right;
            int i9 = rect2.bottom;
            int i10 = rect3.bottom;
            int i11 = i7 - i3;
            int i12 = i9 - i5;
            int i13 = i8 - i4;
            int i14 = i10 - i6;
            View view3 = view2;
            Rect rect4 = (Rect) transitionValues3.f1098a.get("android:changeBounds:clip");
            Rect rect5 = (Rect) transitionValues4.f1098a.get("android:changeBounds:clip");
            if ((i11 == 0 || i12 == 0) && (i13 == 0 || i14 == 0)) {
                i = 0;
            } else {
                i = (i3 == i4 && i5 == i6) ? 0 : 1;
                if (!(i7 == i8 && i9 == i10)) {
                    i++;
                }
            }
            if ((rect4 != null && !rect4.equals(rect5)) || (rect4 == null && rect5 != null)) {
                i++;
            }
            if (i <= 0) {
                return null;
            }
            Rect rect6 = rect5;
            Rect rect7 = rect4;
            if (!this.K) {
                view = view3;
                ViewUtils.a(view, i3, i5, i7, i9);
                if (i == 2) {
                    if (i11 == i13 && i12 == i14) {
                        animator = ObjectAnimatorUtils.a(view, S, f().a((float) i3, (float) i5, (float) i4, (float) i6));
                    } else {
                        final ViewBounds viewBounds = new ViewBounds(view);
                        ObjectAnimator a2 = ObjectAnimatorUtils.a(viewBounds, O, f().a((float) i3, (float) i5, (float) i4, (float) i6));
                        ObjectAnimator a3 = ObjectAnimatorUtils.a(viewBounds, P, f().a((float) i7, (float) i9, (float) i8, (float) i10));
                        AnimatorSet animatorSet = new AnimatorSet();
                        animatorSet.playTogether(new Animator[]{a2, a3});
                        animatorSet.addListener(new AnimatorListenerAdapter(this) {
                            private ViewBounds mViewBounds = viewBounds;
                        });
                        animator = animatorSet;
                    }
                } else if (i3 == i4 && i5 == i6) {
                    animator = ObjectAnimatorUtils.a(view, Q, f().a((float) i7, (float) i9, (float) i8, (float) i10));
                } else {
                    animator = ObjectAnimatorUtils.a(view, R, f().a((float) i3, (float) i5, (float) i4, (float) i6));
                }
            } else {
                view = view3;
                ViewUtils.a(view, i3, i5, Math.max(i11, i13) + i3, Math.max(i12, i14) + i5);
                if (i3 == i4 && i5 == i6) {
                    objectAnimator = null;
                } else {
                    objectAnimator = ObjectAnimatorUtils.a(view, S, f().a((float) i3, (float) i5, (float) i4, (float) i6));
                }
                if (rect7 == null) {
                    i2 = 0;
                    rect = new Rect(0, 0, i11, i12);
                } else {
                    i2 = 0;
                    rect = rect7;
                }
                Rect rect8 = rect6 == null ? new Rect(i2, i2, i13, i14) : rect6;
                if (!rect.equals(rect8)) {
                    ViewCompat.a(view, rect);
                    RectEvaluator rectEvaluator = T;
                    Object[] objArr = new Object[2];
                    objArr[i2] = rect;
                    objArr[1] = rect8;
                    objectAnimator2 = ObjectAnimator.ofObject(view, "clipBounds", rectEvaluator, objArr);
                    final View view4 = view;
                    final Rect rect9 = rect6;
                    final int i15 = i4;
                    final int i16 = i6;
                    final int i17 = i8;
                    final int i18 = i10;
                    objectAnimator2.addListener(new AnimatorListenerAdapter(this) {

                        /* renamed from: a  reason: collision with root package name */
                        private boolean f1077a;

                        public void onAnimationCancel(Animator animator) {
                            this.f1077a = true;
                        }

                        public void onAnimationEnd(Animator animator) {
                            if (!this.f1077a) {
                                ViewCompat.a(view4, rect9);
                                ViewUtils.a(view4, i15, i16, i17, i18);
                            }
                        }
                    });
                } else {
                    objectAnimator2 = null;
                }
                animator = TransitionUtils.a(objectAnimator, objectAnimator2);
            }
            if (view.getParent() instanceof ViewGroup) {
                final ViewGroup viewGroup4 = (ViewGroup) view.getParent();
                ViewGroupUtils.a(viewGroup4, true);
                a((Transition.TransitionListener) new TransitionListenerAdapter(this) {

                    /* renamed from: a  reason: collision with root package name */
                    boolean f1078a = false;

                    public void a(Transition transition) {
                        ViewGroupUtils.a(viewGroup4, true);
                    }

                    public void c(Transition transition) {
                        ViewGroupUtils.a(viewGroup4, false);
                    }

                    public void d(Transition transition) {
                        if (!this.f1078a) {
                            ViewGroupUtils.a(viewGroup4, false);
                        }
                        transition.b((Transition.TransitionListener) this);
                    }
                });
            }
            return animator;
        }
        int intValue = ((Integer) transitionValues3.f1098a.get("android:changeBounds:windowX")).intValue();
        int intValue2 = ((Integer) transitionValues3.f1098a.get("android:changeBounds:windowY")).intValue();
        int intValue3 = ((Integer) transitionValues4.f1098a.get("android:changeBounds:windowX")).intValue();
        int intValue4 = ((Integer) transitionValues4.f1098a.get("android:changeBounds:windowY")).intValue();
        if (intValue == intValue3 && intValue2 == intValue4) {
            return null;
        }
        viewGroup.getLocationInWindow(this.J);
        Bitmap createBitmap = Bitmap.createBitmap(view2.getWidth(), view2.getHeight(), Bitmap.Config.ARGB_8888);
        view2.draw(new Canvas(createBitmap));
        BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
        float c = ViewUtils.c(view2);
        ViewUtils.a(view2, 0.0f);
        ViewUtils.b(viewGroup).a(bitmapDrawable);
        PathMotion f = f();
        int[] iArr = this.J;
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(bitmapDrawable, new PropertyValuesHolder[]{PropertyValuesHolderUtils.a(N, f.a((float) (intValue - iArr[0]), (float) (intValue2 - iArr[1]), (float) (intValue3 - iArr[0]), (float) (intValue4 - iArr[1])))});
        final ViewGroup viewGroup5 = viewGroup;
        final BitmapDrawable bitmapDrawable2 = bitmapDrawable;
        final View view5 = view2;
        final float f2 = c;
        ofPropertyValuesHolder.addListener(new AnimatorListenerAdapter(this) {
            public void onAnimationEnd(Animator animator) {
                ViewUtils.b(viewGroup5).b(bitmapDrawable2);
                ViewUtils.a(view5, f2);
            }
        });
        return ofPropertyValuesHolder;
    }
}
