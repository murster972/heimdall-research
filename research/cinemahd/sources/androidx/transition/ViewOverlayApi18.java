package androidx.transition;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;

class ViewOverlayApi18 implements ViewOverlayImpl {

    /* renamed from: a  reason: collision with root package name */
    private final ViewOverlay f1105a;

    ViewOverlayApi18(View view) {
        this.f1105a = view.getOverlay();
    }

    public void a(Drawable drawable) {
        this.f1105a.add(drawable);
    }

    public void b(Drawable drawable) {
        this.f1105a.remove(drawable);
    }
}
