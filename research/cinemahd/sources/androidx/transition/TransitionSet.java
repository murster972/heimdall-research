package androidx.transition;

import android.animation.TimeInterpolator;
import android.util.AndroidRuntimeException;
import android.view.View;
import android.view.ViewGroup;
import androidx.transition.Transition;
import java.util.ArrayList;
import java.util.Iterator;

public class TransitionSet extends Transition {
    private ArrayList<Transition> J = new ArrayList<>();
    private boolean K = true;
    int L;
    boolean M = false;
    private int N = 0;

    static class TransitionSetListener extends TransitionListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        TransitionSet f1096a;

        TransitionSetListener(TransitionSet transitionSet) {
            this.f1096a = transitionSet;
        }

        public void b(Transition transition) {
            TransitionSet transitionSet = this.f1096a;
            if (!transitionSet.M) {
                transitionSet.o();
                this.f1096a.M = true;
            }
        }

        public void d(Transition transition) {
            TransitionSet transitionSet = this.f1096a;
            transitionSet.L--;
            if (transitionSet.L == 0) {
                transitionSet.M = false;
                transitionSet.a();
            }
            transition.b((Transition.TransitionListener) this);
        }
    }

    private void q() {
        TransitionSetListener transitionSetListener = new TransitionSetListener(this);
        Iterator<Transition> it2 = this.J.iterator();
        while (it2.hasNext()) {
            it2.next().a((Transition.TransitionListener) transitionSetListener);
        }
        this.L = this.J.size();
    }

    public void c(TransitionValues transitionValues) {
        if (b(transitionValues.b)) {
            Iterator<Transition> it2 = this.J.iterator();
            while (it2.hasNext()) {
                Transition next = it2.next();
                if (next.b(transitionValues.b)) {
                    next.c(transitionValues);
                    transitionValues.c.add(next);
                }
            }
        }
    }

    public void e(View view) {
        super.e(view);
        int size = this.J.size();
        for (int i = 0; i < size; i++) {
            this.J.get(i).e(view);
        }
    }

    /* access modifiers changed from: protected */
    public void n() {
        if (this.J.isEmpty()) {
            o();
            a();
            return;
        }
        q();
        if (!this.K) {
            for (int i = 1; i < this.J.size(); i++) {
                final Transition transition = this.J.get(i);
                this.J.get(i - 1).a((Transition.TransitionListener) new TransitionListenerAdapter(this) {
                    public void d(Transition transition) {
                        transition.n();
                        transition.b((Transition.TransitionListener) this);
                    }
                });
            }
            Transition transition2 = this.J.get(0);
            if (transition2 != null) {
                transition2.n();
                return;
            }
            return;
        }
        Iterator<Transition> it2 = this.J.iterator();
        while (it2.hasNext()) {
            it2.next().n();
        }
    }

    public int p() {
        return this.J.size();
    }

    public Transition clone() {
        TransitionSet transitionSet = (TransitionSet) super.clone();
        transitionSet.J = new ArrayList<>();
        int size = this.J.size();
        for (int i = 0; i < size; i++) {
            transitionSet.a(this.J.get(i).clone());
        }
        return transitionSet;
    }

    public TransitionSet d(View view) {
        for (int i = 0; i < this.J.size(); i++) {
            this.J.get(i).d(view);
        }
        super.d(view);
        return this;
    }

    public TransitionSet b(int i) {
        if (i == 0) {
            this.K = true;
        } else if (i == 1) {
            this.K = false;
        } else {
            throw new AndroidRuntimeException("Invalid parameter for TransitionSet ordering: " + i);
        }
        return this;
    }

    public TransitionSet a(Transition transition) {
        this.J.add(transition);
        transition.r = this;
        long j = this.c;
        if (j >= 0) {
            transition.a(j);
        }
        if ((this.N & 1) != 0) {
            transition.a(d());
        }
        if ((this.N & 2) != 0) {
            transition.a(g());
        }
        if ((this.N & 4) != 0) {
            transition.a(f());
        }
        if ((this.N & 8) != 0) {
            transition.a(c());
        }
        return this;
    }

    public TransitionSet b(long j) {
        super.b(j);
        return this;
    }

    public void c(View view) {
        super.c(view);
        int size = this.J.size();
        for (int i = 0; i < size; i++) {
            this.J.get(i).c(view);
        }
    }

    public TransitionSet b(Transition.TransitionListener transitionListener) {
        super.b(transitionListener);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void b(TransitionValues transitionValues) {
        super.b(transitionValues);
        int size = this.J.size();
        for (int i = 0; i < size; i++) {
            this.J.get(i).b(transitionValues);
        }
    }

    public Transition a(int i) {
        if (i < 0 || i >= this.J.size()) {
            return null;
        }
        return this.J.get(i);
    }

    public TransitionSet a(long j) {
        super.a(j);
        if (this.c >= 0) {
            int size = this.J.size();
            for (int i = 0; i < size; i++) {
                this.J.get(i).a(j);
            }
        }
        return this;
    }

    public TransitionSet a(TimeInterpolator timeInterpolator) {
        this.N |= 1;
        ArrayList<Transition> arrayList = this.J;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                this.J.get(i).a(timeInterpolator);
            }
        }
        super.a(timeInterpolator);
        return this;
    }

    public TransitionSet a(View view) {
        for (int i = 0; i < this.J.size(); i++) {
            this.J.get(i).a(view);
        }
        super.a(view);
        return this;
    }

    public TransitionSet a(Transition.TransitionListener transitionListener) {
        super.a(transitionListener);
        return this;
    }

    public void a(PathMotion pathMotion) {
        super.a(pathMotion);
        this.N |= 4;
        for (int i = 0; i < this.J.size(); i++) {
            this.J.get(i).a(pathMotion);
        }
    }

    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup, TransitionValuesMaps transitionValuesMaps, TransitionValuesMaps transitionValuesMaps2, ArrayList<TransitionValues> arrayList, ArrayList<TransitionValues> arrayList2) {
        long h = h();
        int size = this.J.size();
        for (int i = 0; i < size; i++) {
            Transition transition = this.J.get(i);
            if (h > 0 && (this.K || i == 0)) {
                long h2 = transition.h();
                if (h2 > 0) {
                    transition.b(h2 + h);
                } else {
                    transition.b(h);
                }
            }
            transition.a(viewGroup, transitionValuesMaps, transitionValuesMaps2, arrayList, arrayList2);
        }
    }

    public void a(TransitionValues transitionValues) {
        if (b(transitionValues.b)) {
            Iterator<Transition> it2 = this.J.iterator();
            while (it2.hasNext()) {
                Transition next = it2.next();
                if (next.b(transitionValues.b)) {
                    next.a(transitionValues);
                    transitionValues.c.add(next);
                }
            }
        }
    }

    public void a(TransitionPropagation transitionPropagation) {
        super.a(transitionPropagation);
        this.N |= 2;
        int size = this.J.size();
        for (int i = 0; i < size; i++) {
            this.J.get(i).a(transitionPropagation);
        }
    }

    public void a(Transition.EpicenterCallback epicenterCallback) {
        super.a(epicenterCallback);
        this.N |= 8;
        int size = this.J.size();
        for (int i = 0; i < size; i++) {
            this.J.get(i).a(epicenterCallback);
        }
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        String a2 = super.a(str);
        for (int i = 0; i < this.J.size(); i++) {
            StringBuilder sb = new StringBuilder();
            sb.append(a2);
            sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            sb.append(this.J.get(i).a(str + "  "));
            a2 = sb.toString();
        }
        return a2;
    }
}
