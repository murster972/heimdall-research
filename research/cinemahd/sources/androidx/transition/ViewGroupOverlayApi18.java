package androidx.transition;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;

class ViewGroupOverlayApi18 implements ViewGroupOverlayImpl {

    /* renamed from: a  reason: collision with root package name */
    private final ViewGroupOverlay f1100a;

    ViewGroupOverlayApi18(ViewGroup viewGroup) {
        this.f1100a = viewGroup.getOverlay();
    }

    public void a(Drawable drawable) {
        this.f1100a.add(drawable);
    }

    public void b(Drawable drawable) {
        this.f1100a.remove(drawable);
    }

    public void a(View view) {
        this.f1100a.add(view);
    }

    public void b(View view) {
        this.f1100a.remove(view);
    }
}
