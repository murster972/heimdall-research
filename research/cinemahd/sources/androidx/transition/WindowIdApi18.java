package androidx.transition;

import android.view.View;
import android.view.WindowId;

class WindowIdApi18 implements WindowIdImpl {

    /* renamed from: a  reason: collision with root package name */
    private final WindowId f1112a;

    WindowIdApi18(View view) {
        this.f1112a = view.getWindowId();
    }

    public boolean equals(Object obj) {
        return (obj instanceof WindowIdApi18) && ((WindowIdApi18) obj).f1112a.equals(this.f1112a);
    }

    public int hashCode() {
        return this.f1112a.hashCode();
    }
}
