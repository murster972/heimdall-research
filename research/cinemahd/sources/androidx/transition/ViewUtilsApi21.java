package androidx.transition;

import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ViewUtilsApi21 extends ViewUtilsApi19 {
    private static Method e;
    private static boolean f;
    private static Method g;
    private static boolean h;

    ViewUtilsApi21() {
    }

    public void a(View view, Matrix matrix) {
        a();
        Method method = e;
        if (method != null) {
            try {
                method.invoke(view, new Object[]{matrix});
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    public void b(View view, Matrix matrix) {
        b();
        Method method = g;
        if (method != null) {
            try {
                method.invoke(view, new Object[]{matrix});
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    private void a() {
        if (!f) {
            try {
                e = View.class.getDeclaredMethod("transformMatrixToGlobal", new Class[]{Matrix.class});
                e.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToGlobal method", e2);
            }
            f = true;
        }
    }

    private void b() {
        if (!h) {
            try {
                g = View.class.getDeclaredMethod("transformMatrixToLocal", new Class[]{Matrix.class});
                g.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToLocal method", e2);
            }
            h = true;
        }
    }
}
