package androidx.transition;

import android.view.View;
import android.view.ViewGroup;

public class Scene {

    /* renamed from: a  reason: collision with root package name */
    private ViewGroup f1087a;
    private Runnable b;

    public void a() {
        Runnable runnable;
        if (a(this.f1087a) == this && (runnable = this.b) != null) {
            runnable.run();
        }
    }

    static void a(View view, Scene scene) {
        view.setTag(R$id.transition_current_scene, scene);
    }

    static Scene a(View view) {
        return (Scene) view.getTag(R$id.transition_current_scene);
    }
}
