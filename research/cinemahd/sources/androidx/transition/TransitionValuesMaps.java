package androidx.transition;

import android.util.SparseArray;
import android.view.View;
import androidx.collection.ArrayMap;
import androidx.collection.LongSparseArray;

class TransitionValuesMaps {

    /* renamed from: a  reason: collision with root package name */
    final ArrayMap<View, TransitionValues> f1099a = new ArrayMap<>();
    final SparseArray<View> b = new SparseArray<>();
    final LongSparseArray<View> c = new LongSparseArray<>();
    final ArrayMap<String, View> d = new ArrayMap<>();

    TransitionValuesMaps() {
    }
}
