package androidx.transition;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.util.Property;
import android.view.View;
import androidx.core.view.ViewCompat;
import java.lang.reflect.Field;

class ViewUtils {

    /* renamed from: a  reason: collision with root package name */
    private static final ViewUtilsBase f1106a;
    private static Field b;
    private static boolean c;
    static final Property<View, Float> d = new Property<View, Float>(Float.class, "translationAlpha") {
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(ViewUtils.c(view));
        }

        /* renamed from: a */
        public void set(View view, Float f) {
            ViewUtils.a(view, f.floatValue());
        }
    };

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 22) {
            f1106a = new ViewUtilsApi22();
        } else if (i >= 21) {
            f1106a = new ViewUtilsApi21();
        } else if (i >= 19) {
            f1106a = new ViewUtilsApi19();
        } else {
            f1106a = new ViewUtilsBase();
        }
        new Property<View, Rect>(Rect.class, "clipBounds") {
            /* renamed from: a */
            public Rect get(View view) {
                return ViewCompat.g(view);
            }

            /* renamed from: a */
            public void set(View view, Rect rect) {
                ViewCompat.a(view, rect);
            }
        };
    }

    private ViewUtils() {
    }

    static void a(View view, float f) {
        f1106a.a(view, f);
    }

    static ViewOverlayImpl b(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new ViewOverlayApi18(view);
        }
        return ViewOverlayApi14.c(view);
    }

    static float c(View view) {
        return f1106a.b(view);
    }

    static WindowIdImpl d(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new WindowIdApi18(view);
        }
        return new WindowIdApi14(view.getWindowToken());
    }

    static void e(View view) {
        f1106a.c(view);
    }

    static void a(View view) {
        f1106a.a(view);
    }

    static void a(View view, int i) {
        a();
        Field field = b;
        if (field != null) {
            try {
                b.setInt(view, i | (field.getInt(view) & -13));
            } catch (IllegalAccessException unused) {
            }
        }
    }

    static void b(View view, Matrix matrix) {
        f1106a.b(view, matrix);
    }

    static void a(View view, Matrix matrix) {
        f1106a.a(view, matrix);
    }

    static void a(View view, int i, int i2, int i3, int i4) {
        f1106a.a(view, i, i2, i3, i4);
    }

    private static void a() {
        if (!c) {
            try {
                b = View.class.getDeclaredField("mViewFlags");
                b.setAccessible(true);
            } catch (NoSuchFieldException unused) {
                Log.i("ViewUtils", "fetchViewFlagsField: ");
            }
            c = true;
        }
    }
}
