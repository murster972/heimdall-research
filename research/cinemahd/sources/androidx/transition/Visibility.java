package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewGroup;
import androidx.transition.AnimatorUtils;
import androidx.transition.Transition;

public abstract class Visibility extends Transition {
    private static final String[] K = {"android:visibility:visibility", "android:visibility:parent"};
    private int J = 3;

    private static class DisappearListener extends AnimatorListenerAdapter implements Transition.TransitionListener, AnimatorUtils.AnimatorPauseListenerCompat {

        /* renamed from: a  reason: collision with root package name */
        private final View f1109a;
        private final int b;
        private final ViewGroup c;
        private final boolean d;
        private boolean e;
        boolean f = false;

        DisappearListener(View view, int i, boolean z) {
            this.f1109a = view;
            this.b = i;
            this.c = (ViewGroup) view.getParent();
            this.d = z;
            a(true);
        }

        public void a(Transition transition) {
            a(true);
        }

        public void b(Transition transition) {
        }

        public void c(Transition transition) {
            a(false);
        }

        public void d(Transition transition) {
            a();
            transition.b((Transition.TransitionListener) this);
        }

        public void onAnimationCancel(Animator animator) {
            this.f = true;
        }

        public void onAnimationEnd(Animator animator) {
            a();
        }

        public void onAnimationPause(Animator animator) {
            if (!this.f) {
                ViewUtils.a(this.f1109a, this.b);
            }
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationResume(Animator animator) {
            if (!this.f) {
                ViewUtils.a(this.f1109a, 0);
            }
        }

        public void onAnimationStart(Animator animator) {
        }

        private void a() {
            if (!this.f) {
                ViewUtils.a(this.f1109a, this.b);
                ViewGroup viewGroup = this.c;
                if (viewGroup != null) {
                    viewGroup.invalidate();
                }
            }
            a(false);
        }

        private void a(boolean z) {
            ViewGroup viewGroup;
            if (this.d && this.e != z && (viewGroup = this.c) != null) {
                this.e = z;
                ViewGroupUtils.a(viewGroup, z);
            }
        }
    }

    private static class VisibilityInfo {

        /* renamed from: a  reason: collision with root package name */
        boolean f1110a;
        boolean b;
        int c;
        int d;
        ViewGroup e;
        ViewGroup f;

        VisibilityInfo() {
        }
    }

    private VisibilityInfo b(TransitionValues transitionValues, TransitionValues transitionValues2) {
        VisibilityInfo visibilityInfo = new VisibilityInfo();
        visibilityInfo.f1110a = false;
        visibilityInfo.b = false;
        if (transitionValues == null || !transitionValues.f1098a.containsKey("android:visibility:visibility")) {
            visibilityInfo.c = -1;
            visibilityInfo.e = null;
        } else {
            visibilityInfo.c = ((Integer) transitionValues.f1098a.get("android:visibility:visibility")).intValue();
            visibilityInfo.e = (ViewGroup) transitionValues.f1098a.get("android:visibility:parent");
        }
        if (transitionValues2 == null || !transitionValues2.f1098a.containsKey("android:visibility:visibility")) {
            visibilityInfo.d = -1;
            visibilityInfo.f = null;
        } else {
            visibilityInfo.d = ((Integer) transitionValues2.f1098a.get("android:visibility:visibility")).intValue();
            visibilityInfo.f = (ViewGroup) transitionValues2.f1098a.get("android:visibility:parent");
        }
        if (transitionValues == null || transitionValues2 == null) {
            if (transitionValues == null && visibilityInfo.d == 0) {
                visibilityInfo.b = true;
                visibilityInfo.f1110a = true;
            } else if (transitionValues2 == null && visibilityInfo.c == 0) {
                visibilityInfo.b = false;
                visibilityInfo.f1110a = true;
            }
        } else if (visibilityInfo.c == visibilityInfo.d && visibilityInfo.e == visibilityInfo.f) {
            return visibilityInfo;
        } else {
            int i = visibilityInfo.c;
            int i2 = visibilityInfo.d;
            if (i != i2) {
                if (i == 0) {
                    visibilityInfo.b = false;
                    visibilityInfo.f1110a = true;
                } else if (i2 == 0) {
                    visibilityInfo.b = true;
                    visibilityInfo.f1110a = true;
                }
            } else if (visibilityInfo.f == null) {
                visibilityInfo.b = false;
                visibilityInfo.f1110a = true;
            } else if (visibilityInfo.e == null) {
                visibilityInfo.b = true;
                visibilityInfo.f1110a = true;
            }
        }
        return visibilityInfo;
    }

    private void d(TransitionValues transitionValues) {
        transitionValues.f1098a.put("android:visibility:visibility", Integer.valueOf(transitionValues.b.getVisibility()));
        transitionValues.f1098a.put("android:visibility:parent", transitionValues.b.getParent());
        int[] iArr = new int[2];
        transitionValues.b.getLocationOnScreen(iArr);
        transitionValues.f1098a.put("android:visibility:screenLocation", iArr);
    }

    public Animator a(ViewGroup viewGroup, View view, TransitionValues transitionValues, TransitionValues transitionValues2) {
        return null;
    }

    public void a(int i) {
        if ((i & -4) == 0) {
            this.J = i;
            return;
        }
        throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
    }

    public Animator b(ViewGroup viewGroup, View view, TransitionValues transitionValues, TransitionValues transitionValues2) {
        return null;
    }

    public void c(TransitionValues transitionValues) {
        d(transitionValues);
    }

    public String[] m() {
        return K;
    }

    public void a(TransitionValues transitionValues) {
        d(transitionValues);
    }

    public Animator a(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        VisibilityInfo b = b(transitionValues, transitionValues2);
        if (!b.f1110a) {
            return null;
        }
        if (b.e == null && b.f == null) {
            return null;
        }
        if (b.b) {
            return a(viewGroup, transitionValues, b.c, transitionValues2, b.d);
        }
        return b(viewGroup, transitionValues, b.c, transitionValues2, b.d);
    }

    public Animator a(ViewGroup viewGroup, TransitionValues transitionValues, int i, TransitionValues transitionValues2, int i2) {
        if ((this.J & 1) != 1 || transitionValues2 == null) {
            return null;
        }
        if (transitionValues == null) {
            View view = (View) transitionValues2.b.getParent();
            if (b(a(view, false), b(view, false)).f1110a) {
                return null;
            }
        }
        return a(viewGroup, transitionValues2.b, transitionValues, transitionValues2);
    }

    public boolean a(TransitionValues transitionValues, TransitionValues transitionValues2) {
        if (transitionValues == null && transitionValues2 == null) {
            return false;
        }
        if (transitionValues != null && transitionValues2 != null && transitionValues2.f1098a.containsKey("android:visibility:visibility") != transitionValues.f1098a.containsKey("android:visibility:visibility")) {
            return false;
        }
        VisibilityInfo b = b(transitionValues, transitionValues2);
        if (!b.f1110a) {
            return false;
        }
        if (b.c == 0 || b.d == 0) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0087 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ee A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.animation.Animator b(android.view.ViewGroup r7, androidx.transition.TransitionValues r8, int r9, androidx.transition.TransitionValues r10, int r11) {
        /*
            r6 = this;
            int r9 = r6.J
            r0 = 2
            r9 = r9 & r0
            r1 = 0
            if (r9 == r0) goto L_0x0008
            return r1
        L_0x0008:
            if (r8 == 0) goto L_0x000d
            android.view.View r9 = r8.b
            goto L_0x000e
        L_0x000d:
            r9 = r1
        L_0x000e:
            if (r10 == 0) goto L_0x0013
            android.view.View r2 = r10.b
            goto L_0x0014
        L_0x0013:
            r2 = r1
        L_0x0014:
            r3 = 1
            if (r2 == 0) goto L_0x0037
            android.view.ViewParent r4 = r2.getParent()
            if (r4 != 0) goto L_0x001e
            goto L_0x0037
        L_0x001e:
            r4 = 4
            if (r11 != r4) goto L_0x0022
            goto L_0x0024
        L_0x0022:
            if (r9 != r2) goto L_0x0027
        L_0x0024:
            r9 = r1
            goto L_0x0084
        L_0x0027:
            boolean r2 = r6.v
            if (r2 == 0) goto L_0x002c
            goto L_0x0044
        L_0x002c:
            android.view.ViewParent r2 = r9.getParent()
            android.view.View r2 = (android.view.View) r2
            android.view.View r9 = androidx.transition.TransitionUtils.a(r7, r9, r2)
            goto L_0x003a
        L_0x0037:
            if (r2 == 0) goto L_0x003c
            r9 = r2
        L_0x003a:
            r2 = r1
            goto L_0x0084
        L_0x003c:
            if (r9 == 0) goto L_0x0082
            android.view.ViewParent r2 = r9.getParent()
            if (r2 != 0) goto L_0x0045
        L_0x0044:
            goto L_0x003a
        L_0x0045:
            android.view.ViewParent r2 = r9.getParent()
            boolean r2 = r2 instanceof android.view.View
            if (r2 == 0) goto L_0x0082
            android.view.ViewParent r2 = r9.getParent()
            android.view.View r2 = (android.view.View) r2
            androidx.transition.TransitionValues r4 = r6.b((android.view.View) r2, (boolean) r3)
            androidx.transition.TransitionValues r5 = r6.a((android.view.View) r2, (boolean) r3)
            androidx.transition.Visibility$VisibilityInfo r4 = r6.b(r4, r5)
            boolean r4 = r4.f1110a
            if (r4 != 0) goto L_0x0068
            android.view.View r9 = androidx.transition.TransitionUtils.a(r7, r9, r2)
            goto L_0x003a
        L_0x0068:
            android.view.ViewParent r4 = r2.getParent()
            if (r4 != 0) goto L_0x0080
            int r2 = r2.getId()
            r4 = -1
            if (r2 == r4) goto L_0x0080
            android.view.View r2 = r7.findViewById(r2)
            if (r2 == 0) goto L_0x0080
            boolean r2 = r6.v
            if (r2 == 0) goto L_0x0080
            goto L_0x003a
        L_0x0080:
            r9 = r1
            goto L_0x003a
        L_0x0082:
            r9 = r1
            r2 = r9
        L_0x0084:
            r4 = 0
            if (r9 == 0) goto L_0x00cc
            if (r8 == 0) goto L_0x00cc
            java.util.Map<java.lang.String, java.lang.Object> r11 = r8.f1098a
            java.lang.String r1 = "android:visibility:screenLocation"
            java.lang.Object r11 = r11.get(r1)
            int[] r11 = (int[]) r11
            r1 = r11[r4]
            r11 = r11[r3]
            int[] r0 = new int[r0]
            r7.getLocationOnScreen(r0)
            r2 = r0[r4]
            int r1 = r1 - r2
            int r2 = r9.getLeft()
            int r1 = r1 - r2
            r9.offsetLeftAndRight(r1)
            r0 = r0[r3]
            int r11 = r11 - r0
            int r0 = r9.getTop()
            int r11 = r11 - r0
            r9.offsetTopAndBottom(r11)
            androidx.transition.ViewGroupOverlayImpl r11 = androidx.transition.ViewGroupUtils.a(r7)
            r11.a(r9)
            android.animation.Animator r7 = r6.b(r7, r9, r8, r10)
            if (r7 != 0) goto L_0x00c3
            r11.b(r9)
            goto L_0x00cb
        L_0x00c3:
            androidx.transition.Visibility$1 r8 = new androidx.transition.Visibility$1
            r8.<init>(r6, r11, r9)
            r7.addListener(r8)
        L_0x00cb:
            return r7
        L_0x00cc:
            if (r2 == 0) goto L_0x00ee
            int r9 = r2.getVisibility()
            androidx.transition.ViewUtils.a((android.view.View) r2, (int) r4)
            android.animation.Animator r7 = r6.b(r7, r2, r8, r10)
            if (r7 == 0) goto L_0x00ea
            androidx.transition.Visibility$DisappearListener r8 = new androidx.transition.Visibility$DisappearListener
            r8.<init>(r2, r11, r3)
            r7.addListener(r8)
            androidx.transition.AnimatorUtils.a(r7, r8)
            r6.a((androidx.transition.Transition.TransitionListener) r8)
            goto L_0x00ed
        L_0x00ea:
            androidx.transition.ViewUtils.a((android.view.View) r2, (int) r9)
        L_0x00ed:
            return r7
        L_0x00ee:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.transition.Visibility.b(android.view.ViewGroup, androidx.transition.TransitionValues, int, androidx.transition.TransitionValues, int):android.animation.Animator");
    }
}
