package androidx.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.graphics.Path;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import androidx.collection.ArrayMap;
import androidx.collection.LongSparseArray;
import androidx.collection.SimpleArrayMap;
import androidx.core.view.ViewCompat;
import com.facebook.common.time.Clock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Transition implements Cloneable {
    private static final int[] G = {2, 1, 3, 4};
    private static final PathMotion H = new PathMotion() {
        public Path a(float f, float f2, float f3, float f4) {
            Path path = new Path();
            path.moveTo(f, f2);
            path.lineTo(f3, f4);
            return path;
        }
    };
    private static ThreadLocal<ArrayMap<Animator, AnimationInfo>> I = new ThreadLocal<>();
    private ArrayList<TransitionListener> A = null;
    private ArrayList<Animator> B = new ArrayList<>();
    TransitionPropagation C;
    private EpicenterCallback D;
    private ArrayMap<String, String> E;
    private PathMotion F = H;

    /* renamed from: a  reason: collision with root package name */
    private String f1088a = getClass().getName();
    private long b = -1;
    long c = -1;
    private TimeInterpolator d = null;
    ArrayList<Integer> e = new ArrayList<>();
    ArrayList<View> f = new ArrayList<>();
    private ArrayList<String> g = null;
    private ArrayList<Class> h = null;
    private ArrayList<Integer> i = null;
    private ArrayList<View> j = null;
    private ArrayList<Class> k = null;
    private ArrayList<String> l = null;
    private ArrayList<Integer> m = null;
    private ArrayList<View> n = null;
    private ArrayList<Class> o = null;
    private TransitionValuesMaps p = new TransitionValuesMaps();
    private TransitionValuesMaps q = new TransitionValuesMaps();
    TransitionSet r = null;
    private int[] s = G;
    private ArrayList<TransitionValues> t;
    private ArrayList<TransitionValues> u;
    boolean v = false;
    ArrayList<Animator> w = new ArrayList<>();
    private int x = 0;
    private boolean y = false;
    private boolean z = false;

    private static class AnimationInfo {

        /* renamed from: a  reason: collision with root package name */
        View f1091a;
        String b;
        TransitionValues c;
        WindowIdImpl d;
        Transition e;

        AnimationInfo(View view, String str, Transition transition, WindowIdImpl windowIdImpl, TransitionValues transitionValues) {
            this.f1091a = view;
            this.b = str;
            this.c = transitionValues;
            this.d = windowIdImpl;
            this.e = transition;
        }
    }

    public static abstract class EpicenterCallback {
    }

    public interface TransitionListener {
        void a(Transition transition);

        void b(Transition transition);

        void c(Transition transition);

        void d(Transition transition);
    }

    private void c(View view, boolean z2) {
        if (view != null) {
            int id = view.getId();
            ArrayList<Integer> arrayList = this.i;
            if (arrayList == null || !arrayList.contains(Integer.valueOf(id))) {
                ArrayList<View> arrayList2 = this.j;
                if (arrayList2 == null || !arrayList2.contains(view)) {
                    ArrayList<Class> arrayList3 = this.k;
                    if (arrayList3 != null) {
                        int size = arrayList3.size();
                        int i2 = 0;
                        while (i2 < size) {
                            if (!this.k.get(i2).isInstance(view)) {
                                i2++;
                            } else {
                                return;
                            }
                        }
                    }
                    if (view.getParent() instanceof ViewGroup) {
                        TransitionValues transitionValues = new TransitionValues();
                        transitionValues.b = view;
                        if (z2) {
                            c(transitionValues);
                        } else {
                            a(transitionValues);
                        }
                        transitionValues.c.add(this);
                        b(transitionValues);
                        if (z2) {
                            a(this.p, view, transitionValues);
                        } else {
                            a(this.q, view, transitionValues);
                        }
                    }
                    if (view instanceof ViewGroup) {
                        ArrayList<Integer> arrayList4 = this.m;
                        if (arrayList4 == null || !arrayList4.contains(Integer.valueOf(id))) {
                            ArrayList<View> arrayList5 = this.n;
                            if (arrayList5 == null || !arrayList5.contains(view)) {
                                ArrayList<Class> arrayList6 = this.o;
                                if (arrayList6 != null) {
                                    int size2 = arrayList6.size();
                                    int i3 = 0;
                                    while (i3 < size2) {
                                        if (!this.o.get(i3).isInstance(view)) {
                                            i3++;
                                        } else {
                                            return;
                                        }
                                    }
                                }
                                ViewGroup viewGroup = (ViewGroup) view;
                                for (int i4 = 0; i4 < viewGroup.getChildCount(); i4++) {
                                    c(viewGroup.getChildAt(i4), z2);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static ArrayMap<Animator, AnimationInfo> p() {
        ArrayMap<Animator, AnimationInfo> arrayMap = I.get();
        if (arrayMap != null) {
            return arrayMap;
        }
        ArrayMap<Animator, AnimationInfo> arrayMap2 = new ArrayMap<>();
        I.set(arrayMap2);
        return arrayMap2;
    }

    public Animator a(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        return null;
    }

    public Transition a(long j2) {
        this.c = j2;
        return this;
    }

    public abstract void a(TransitionValues transitionValues);

    public long b() {
        return this.c;
    }

    public abstract void c(TransitionValues transitionValues);

    public TimeInterpolator d() {
        return this.d;
    }

    public void e(View view) {
        if (this.y) {
            if (!this.z) {
                ArrayMap<Animator, AnimationInfo> p2 = p();
                int size = p2.size();
                WindowIdImpl d2 = ViewUtils.d(view);
                for (int i2 = size - 1; i2 >= 0; i2--) {
                    AnimationInfo d3 = p2.d(i2);
                    if (d3.f1091a != null && d2.equals(d3.d)) {
                        AnimatorUtils.b(p2.b(i2));
                    }
                }
                ArrayList<TransitionListener> arrayList = this.A;
                if (arrayList != null && arrayList.size() > 0) {
                    ArrayList arrayList2 = (ArrayList) this.A.clone();
                    int size2 = arrayList2.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        ((TransitionListener) arrayList2.get(i3)).a(this);
                    }
                }
            }
            this.y = false;
        }
    }

    public PathMotion f() {
        return this.F;
    }

    public TransitionPropagation g() {
        return this.C;
    }

    public long h() {
        return this.b;
    }

    public List<Integer> i() {
        return this.e;
    }

    public List<String> j() {
        return this.g;
    }

    public List<Class> k() {
        return this.h;
    }

    public List<View> l() {
        return this.f;
    }

    public String[] m() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void n() {
        o();
        ArrayMap<Animator, AnimationInfo> p2 = p();
        Iterator<Animator> it2 = this.B.iterator();
        while (it2.hasNext()) {
            Animator next = it2.next();
            if (p2.containsKey(next)) {
                o();
                a(next, p2);
            }
        }
        this.B.clear();
        a();
    }

    /* access modifiers changed from: protected */
    public void o() {
        if (this.x == 0) {
            ArrayList<TransitionListener> arrayList = this.A;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.A.clone();
                int size = arrayList2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((TransitionListener) arrayList2.get(i2)).b(this);
                }
            }
            this.z = false;
        }
        this.x++;
    }

    public String toString() {
        return a("");
    }

    public Transition a(TimeInterpolator timeInterpolator) {
        this.d = timeInterpolator;
        return this;
    }

    public Transition b(long j2) {
        this.b = j2;
        return this;
    }

    public Transition clone() {
        try {
            Transition transition = (Transition) super.clone();
            transition.B = new ArrayList<>();
            transition.p = new TransitionValuesMaps();
            transition.q = new TransitionValuesMaps();
            transition.t = null;
            transition.u = null;
            return transition;
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }

    public Transition d(View view) {
        this.f.remove(view);
        return this;
    }

    private void a(ArrayMap<View, TransitionValues> arrayMap, ArrayMap<View, TransitionValues> arrayMap2, LongSparseArray<View> longSparseArray, LongSparseArray<View> longSparseArray2) {
        View b2;
        int b3 = longSparseArray.b();
        for (int i2 = 0; i2 < b3; i2++) {
            View c2 = longSparseArray.c(i2);
            if (c2 != null && b(c2) && (b2 = longSparseArray2.b(longSparseArray.a(i2))) != null && b(b2)) {
                TransitionValues transitionValues = arrayMap.get(c2);
                TransitionValues transitionValues2 = arrayMap2.get(b2);
                if (!(transitionValues == null || transitionValues2 == null)) {
                    this.t.add(transitionValues);
                    this.u.add(transitionValues2);
                    arrayMap.remove(c2);
                    arrayMap2.remove(b2);
                }
            }
        }
    }

    private void b(ArrayMap<View, TransitionValues> arrayMap, ArrayMap<View, TransitionValues> arrayMap2) {
        TransitionValues remove;
        View view;
        for (int size = arrayMap.size() - 1; size >= 0; size--) {
            View b2 = arrayMap.b(size);
            if (!(b2 == null || !b(b2) || (remove = arrayMap2.remove(b2)) == null || (view = remove.b) == null || !b(view))) {
                this.t.add(arrayMap.c(size));
                this.u.add(remove);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(View view) {
        ArrayList<Class> arrayList;
        ArrayList<String> arrayList2;
        int id = view.getId();
        ArrayList<Integer> arrayList3 = this.i;
        if (arrayList3 != null && arrayList3.contains(Integer.valueOf(id))) {
            return false;
        }
        ArrayList<View> arrayList4 = this.j;
        if (arrayList4 != null && arrayList4.contains(view)) {
            return false;
        }
        ArrayList<Class> arrayList5 = this.k;
        if (arrayList5 != null) {
            int size = arrayList5.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.k.get(i2).isInstance(view)) {
                    return false;
                }
            }
        }
        if (this.l != null && ViewCompat.s(view) != null && this.l.contains(ViewCompat.s(view))) {
            return false;
        }
        if ((this.e.size() == 0 && this.f.size() == 0 && (((arrayList = this.h) == null || arrayList.isEmpty()) && ((arrayList2 = this.g) == null || arrayList2.isEmpty()))) || this.e.contains(Integer.valueOf(id)) || this.f.contains(view)) {
            return true;
        }
        ArrayList<String> arrayList6 = this.g;
        if (arrayList6 != null && arrayList6.contains(ViewCompat.s(view))) {
            return true;
        }
        if (this.h != null) {
            for (int i3 = 0; i3 < this.h.size(); i3++) {
                if (this.h.get(i3).isInstance(view)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void a(ArrayMap<View, TransitionValues> arrayMap, ArrayMap<View, TransitionValues> arrayMap2, SparseArray<View> sparseArray, SparseArray<View> sparseArray2) {
        View view;
        int size = sparseArray.size();
        for (int i2 = 0; i2 < size; i2++) {
            View valueAt = sparseArray.valueAt(i2);
            if (valueAt != null && b(valueAt) && (view = sparseArray2.get(sparseArray.keyAt(i2))) != null && b(view)) {
                TransitionValues transitionValues = arrayMap.get(valueAt);
                TransitionValues transitionValues2 = arrayMap2.get(view);
                if (!(transitionValues == null || transitionValues2 == null)) {
                    this.t.add(transitionValues);
                    this.u.add(transitionValues2);
                    arrayMap.remove(valueAt);
                    arrayMap2.remove(view);
                }
            }
        }
    }

    public String e() {
        return this.f1088a;
    }

    private void a(ArrayMap<View, TransitionValues> arrayMap, ArrayMap<View, TransitionValues> arrayMap2, ArrayMap<String, View> arrayMap3, ArrayMap<String, View> arrayMap4) {
        View view;
        int size = arrayMap3.size();
        for (int i2 = 0; i2 < size; i2++) {
            View d2 = arrayMap3.d(i2);
            if (d2 != null && b(d2) && (view = arrayMap4.get(arrayMap3.b(i2))) != null && b(view)) {
                TransitionValues transitionValues = arrayMap.get(d2);
                TransitionValues transitionValues2 = arrayMap2.get(view);
                if (!(transitionValues == null || transitionValues2 == null)) {
                    this.t.add(transitionValues);
                    this.u.add(transitionValues2);
                    arrayMap.remove(d2);
                    arrayMap2.remove(view);
                }
            }
        }
    }

    public void c(View view) {
        if (!this.z) {
            ArrayMap<Animator, AnimationInfo> p2 = p();
            int size = p2.size();
            WindowIdImpl d2 = ViewUtils.d(view);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                AnimationInfo d3 = p2.d(i2);
                if (d3.f1091a != null && d2.equals(d3.d)) {
                    AnimatorUtils.a(p2.b(i2));
                }
            }
            ArrayList<TransitionListener> arrayList = this.A;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.A.clone();
                int size2 = arrayList2.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    ((TransitionListener) arrayList2.get(i3)).c(this);
                }
            }
            this.y = true;
        }
    }

    public TransitionValues b(View view, boolean z2) {
        TransitionSet transitionSet = this.r;
        if (transitionSet != null) {
            return transitionSet.b(view, z2);
        }
        return (z2 ? this.p : this.q).f1099a.get(view);
    }

    public Transition b(TransitionListener transitionListener) {
        ArrayList<TransitionListener> arrayList = this.A;
        if (arrayList == null) {
            return this;
        }
        arrayList.remove(transitionListener);
        if (this.A.size() == 0) {
            this.A = null;
        }
        return this;
    }

    private void a(ArrayMap<View, TransitionValues> arrayMap, ArrayMap<View, TransitionValues> arrayMap2) {
        for (int i2 = 0; i2 < arrayMap.size(); i2++) {
            TransitionValues d2 = arrayMap.d(i2);
            if (b(d2.b)) {
                this.t.add(d2);
                this.u.add((Object) null);
            }
        }
        for (int i3 = 0; i3 < arrayMap2.size(); i3++) {
            TransitionValues d3 = arrayMap2.d(i3);
            if (b(d3.b)) {
                this.u.add(d3);
                this.t.add((Object) null);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(TransitionValues transitionValues) {
        String[] a2;
        if (this.C != null && !transitionValues.f1098a.isEmpty() && (a2 = this.C.a()) != null) {
            boolean z2 = false;
            int i2 = 0;
            while (true) {
                if (i2 >= a2.length) {
                    z2 = true;
                    break;
                } else if (!transitionValues.f1098a.containsKey(a2[i2])) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z2) {
                this.C.a(transitionValues);
            }
        }
    }

    public EpicenterCallback c() {
        return this.D;
    }

    private void a(TransitionValuesMaps transitionValuesMaps, TransitionValuesMaps transitionValuesMaps2) {
        ArrayMap arrayMap = new ArrayMap((SimpleArrayMap) transitionValuesMaps.f1099a);
        ArrayMap arrayMap2 = new ArrayMap((SimpleArrayMap) transitionValuesMaps2.f1099a);
        int i2 = 0;
        while (true) {
            int[] iArr = this.s;
            if (i2 < iArr.length) {
                int i3 = iArr[i2];
                if (i3 == 1) {
                    b((ArrayMap<View, TransitionValues>) arrayMap, (ArrayMap<View, TransitionValues>) arrayMap2);
                } else if (i3 == 2) {
                    a((ArrayMap<View, TransitionValues>) arrayMap, (ArrayMap<View, TransitionValues>) arrayMap2, transitionValuesMaps.d, transitionValuesMaps2.d);
                } else if (i3 == 3) {
                    a((ArrayMap<View, TransitionValues>) arrayMap, (ArrayMap<View, TransitionValues>) arrayMap2, transitionValuesMaps.b, transitionValuesMaps2.b);
                } else if (i3 == 4) {
                    a((ArrayMap<View, TransitionValues>) arrayMap, (ArrayMap<View, TransitionValues>) arrayMap2, transitionValuesMaps.c, transitionValuesMaps2.c);
                }
                i2++;
            } else {
                a((ArrayMap<View, TransitionValues>) arrayMap, (ArrayMap<View, TransitionValues>) arrayMap2);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup, TransitionValuesMaps transitionValuesMaps, TransitionValuesMaps transitionValuesMaps2, ArrayList<TransitionValues> arrayList, ArrayList<TransitionValues> arrayList2) {
        int i2;
        int i3;
        Animator a2;
        View view;
        Animator animator;
        TransitionValues transitionValues;
        TransitionValues transitionValues2;
        Animator animator2;
        ViewGroup viewGroup2 = viewGroup;
        ArrayMap<Animator, AnimationInfo> p2 = p();
        SparseIntArray sparseIntArray = new SparseIntArray();
        int size = arrayList.size();
        long j2 = Clock.MAX_TIME;
        int i4 = 0;
        while (i4 < size) {
            TransitionValues transitionValues3 = arrayList.get(i4);
            TransitionValues transitionValues4 = arrayList2.get(i4);
            if (transitionValues3 != null && !transitionValues3.c.contains(this)) {
                transitionValues3 = null;
            }
            if (transitionValues4 != null && !transitionValues4.c.contains(this)) {
                transitionValues4 = null;
            }
            if (!(transitionValues3 == null && transitionValues4 == null)) {
                if ((transitionValues3 == null || transitionValues4 == null || a(transitionValues3, transitionValues4)) && (a2 = a(viewGroup2, transitionValues3, transitionValues4)) != null) {
                    if (transitionValues4 != null) {
                        view = transitionValues4.b;
                        String[] m2 = m();
                        if (view != null && m2 != null && m2.length > 0) {
                            transitionValues2 = new TransitionValues();
                            transitionValues2.b = view;
                            Animator animator3 = a2;
                            i3 = size;
                            TransitionValues transitionValues5 = transitionValuesMaps2.f1099a.get(view);
                            if (transitionValues5 != null) {
                                int i5 = 0;
                                while (i5 < m2.length) {
                                    transitionValues2.f1098a.put(m2[i5], transitionValues5.f1098a.get(m2[i5]));
                                    i5++;
                                    ArrayList<TransitionValues> arrayList3 = arrayList2;
                                    i4 = i4;
                                    transitionValues5 = transitionValues5;
                                }
                            }
                            i2 = i4;
                            int size2 = p2.size();
                            int i6 = 0;
                            while (true) {
                                if (i6 >= size2) {
                                    animator2 = animator3;
                                    break;
                                }
                                AnimationInfo animationInfo = p2.get(p2.b(i6));
                                if (animationInfo.c != null && animationInfo.f1091a == view && animationInfo.b.equals(e()) && animationInfo.c.equals(transitionValues2)) {
                                    animator2 = null;
                                    break;
                                }
                                i6++;
                            }
                        } else {
                            i3 = size;
                            i2 = i4;
                            animator2 = a2;
                            transitionValues2 = null;
                        }
                        animator = animator2;
                        transitionValues = transitionValues2;
                    } else {
                        i3 = size;
                        i2 = i4;
                        view = transitionValues3.b;
                        animator = a2;
                        transitionValues = null;
                    }
                    if (animator != null) {
                        TransitionPropagation transitionPropagation = this.C;
                        if (transitionPropagation != null) {
                            long a3 = transitionPropagation.a(viewGroup2, this, transitionValues3, transitionValues4);
                            sparseIntArray.put(this.B.size(), (int) a3);
                            j2 = Math.min(a3, j2);
                        }
                        p2.put(animator, new AnimationInfo(view, e(), this, ViewUtils.d(viewGroup), transitionValues));
                        this.B.add(animator);
                        j2 = j2;
                    }
                    i4 = i2 + 1;
                    size = i3;
                }
            }
            i3 = size;
            i2 = i4;
            i4 = i2 + 1;
            size = i3;
        }
        if (j2 != 0) {
            for (int i7 = 0; i7 < sparseIntArray.size(); i7++) {
                Animator animator4 = this.B.get(sparseIntArray.keyAt(i7));
                animator4.setStartDelay((((long) sparseIntArray.valueAt(i7)) - j2) + animator4.getStartDelay());
            }
        }
    }

    private void a(Animator animator, final ArrayMap<Animator, AnimationInfo> arrayMap) {
        if (animator != null) {
            animator.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    arrayMap.remove(animator);
                    Transition.this.w.remove(animator);
                }

                public void onAnimationStart(Animator animator) {
                    Transition.this.w.add(animator);
                }
            });
            a(animator);
        }
    }

    public Transition a(View view) {
        this.f.add(view);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup, boolean z2) {
        ArrayMap<String, String> arrayMap;
        ArrayList<String> arrayList;
        ArrayList<Class> arrayList2;
        a(z2);
        if ((this.e.size() > 0 || this.f.size() > 0) && (((arrayList = this.g) == null || arrayList.isEmpty()) && ((arrayList2 = this.h) == null || arrayList2.isEmpty()))) {
            for (int i2 = 0; i2 < this.e.size(); i2++) {
                View findViewById = viewGroup.findViewById(this.e.get(i2).intValue());
                if (findViewById != null) {
                    TransitionValues transitionValues = new TransitionValues();
                    transitionValues.b = findViewById;
                    if (z2) {
                        c(transitionValues);
                    } else {
                        a(transitionValues);
                    }
                    transitionValues.c.add(this);
                    b(transitionValues);
                    if (z2) {
                        a(this.p, findViewById, transitionValues);
                    } else {
                        a(this.q, findViewById, transitionValues);
                    }
                }
            }
            for (int i3 = 0; i3 < this.f.size(); i3++) {
                View view = this.f.get(i3);
                TransitionValues transitionValues2 = new TransitionValues();
                transitionValues2.b = view;
                if (z2) {
                    c(transitionValues2);
                } else {
                    a(transitionValues2);
                }
                transitionValues2.c.add(this);
                b(transitionValues2);
                if (z2) {
                    a(this.p, view, transitionValues2);
                } else {
                    a(this.q, view, transitionValues2);
                }
            }
        } else {
            c(viewGroup, z2);
        }
        if (!z2 && (arrayMap = this.E) != null) {
            int size = arrayMap.size();
            ArrayList arrayList3 = new ArrayList(size);
            for (int i4 = 0; i4 < size; i4++) {
                arrayList3.add(this.p.d.remove(this.E.b(i4)));
            }
            for (int i5 = 0; i5 < size; i5++) {
                View view2 = (View) arrayList3.get(i5);
                if (view2 != null) {
                    this.p.d.put(this.E.d(i5), view2);
                }
            }
        }
    }

    private static void a(TransitionValuesMaps transitionValuesMaps, View view, TransitionValues transitionValues) {
        transitionValuesMaps.f1099a.put(view, transitionValues);
        int id = view.getId();
        if (id >= 0) {
            if (transitionValuesMaps.b.indexOfKey(id) >= 0) {
                transitionValuesMaps.b.put(id, (Object) null);
            } else {
                transitionValuesMaps.b.put(id, view);
            }
        }
        String s2 = ViewCompat.s(view);
        if (s2 != null) {
            if (transitionValuesMaps.d.containsKey(s2)) {
                transitionValuesMaps.d.put(s2, null);
            } else {
                transitionValuesMaps.d.put(s2, view);
            }
        }
        if (view.getParent() instanceof ListView) {
            ListView listView = (ListView) view.getParent();
            if (listView.getAdapter().hasStableIds()) {
                long itemIdAtPosition = listView.getItemIdAtPosition(listView.getPositionForView(view));
                if (transitionValuesMaps.c.c(itemIdAtPosition) >= 0) {
                    View b2 = transitionValuesMaps.c.b(itemIdAtPosition);
                    if (b2 != null) {
                        ViewCompat.b(b2, false);
                        transitionValuesMaps.c.c(itemIdAtPosition, null);
                        return;
                    }
                    return;
                }
                ViewCompat.b(view, true);
                transitionValuesMaps.c.c(itemIdAtPosition, view);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        if (z2) {
            this.p.f1099a.clear();
            this.p.b.clear();
            this.p.c.a();
            return;
        }
        this.q.f1099a.clear();
        this.q.b.clear();
        this.q.c.a();
    }

    /* access modifiers changed from: package-private */
    public TransitionValues a(View view, boolean z2) {
        TransitionSet transitionSet = this.r;
        if (transitionSet != null) {
            return transitionSet.a(view, z2);
        }
        ArrayList<TransitionValues> arrayList = z2 ? this.t : this.u;
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        int i2 = -1;
        int i3 = 0;
        while (true) {
            if (i3 >= size) {
                break;
            }
            TransitionValues transitionValues = arrayList.get(i3);
            if (transitionValues == null) {
                return null;
            }
            if (transitionValues.b == view) {
                i2 = i3;
                break;
            }
            i3++;
        }
        if (i2 < 0) {
            return null;
        }
        return (z2 ? this.u : this.t).get(i2);
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup) {
        AnimationInfo animationInfo;
        this.t = new ArrayList<>();
        this.u = new ArrayList<>();
        a(this.p, this.q);
        ArrayMap<Animator, AnimationInfo> p2 = p();
        int size = p2.size();
        WindowIdImpl d2 = ViewUtils.d(viewGroup);
        for (int i2 = size - 1; i2 >= 0; i2--) {
            Animator b2 = p2.b(i2);
            if (!(b2 == null || (animationInfo = p2.get(b2)) == null || animationInfo.f1091a == null || !d2.equals(animationInfo.d))) {
                TransitionValues transitionValues = animationInfo.c;
                View view = animationInfo.f1091a;
                TransitionValues b3 = b(view, true);
                TransitionValues a2 = a(view, true);
                if (!(b3 == null && a2 == null) && animationInfo.e.a(transitionValues, a2)) {
                    if (b2.isRunning() || b2.isStarted()) {
                        b2.cancel();
                    } else {
                        p2.remove(b2);
                    }
                }
            }
        }
        a(viewGroup, this.p, this.q, this.t, this.u);
        n();
    }

    public boolean a(TransitionValues transitionValues, TransitionValues transitionValues2) {
        if (transitionValues == null || transitionValues2 == null) {
            return false;
        }
        String[] m2 = m();
        if (m2 != null) {
            int length = m2.length;
            int i2 = 0;
            while (i2 < length) {
                if (!a(transitionValues, transitionValues2, m2[i2])) {
                    i2++;
                }
            }
            return false;
        }
        for (String a2 : transitionValues.f1098a.keySet()) {
            if (a(transitionValues, transitionValues2, a2)) {
            }
        }
        return false;
        return true;
    }

    private static boolean a(TransitionValues transitionValues, TransitionValues transitionValues2, String str) {
        Object obj = transitionValues.f1098a.get(str);
        Object obj2 = transitionValues2.f1098a.get(str);
        if (obj == null && obj2 == null) {
            return false;
        }
        if (obj == null || obj2 == null) {
            return true;
        }
        return true ^ obj.equals(obj2);
    }

    /* access modifiers changed from: protected */
    public void a(Animator animator) {
        if (animator == null) {
            a();
            return;
        }
        if (b() >= 0) {
            animator.setDuration(b());
        }
        if (h() >= 0) {
            animator.setStartDelay(h());
        }
        if (d() != null) {
            animator.setInterpolator(d());
        }
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                Transition.this.a();
                animator.removeListener(this);
            }
        });
        animator.start();
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.x--;
        if (this.x == 0) {
            ArrayList<TransitionListener> arrayList = this.A;
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList arrayList2 = (ArrayList) this.A.clone();
                int size = arrayList2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((TransitionListener) arrayList2.get(i2)).d(this);
                }
            }
            for (int i3 = 0; i3 < this.p.c.b(); i3++) {
                View c2 = this.p.c.c(i3);
                if (c2 != null) {
                    ViewCompat.b(c2, false);
                }
            }
            for (int i4 = 0; i4 < this.q.c.b(); i4++) {
                View c3 = this.q.c.c(i4);
                if (c3 != null) {
                    ViewCompat.b(c3, false);
                }
            }
            this.z = true;
        }
    }

    public Transition a(TransitionListener transitionListener) {
        if (this.A == null) {
            this.A = new ArrayList<>();
        }
        this.A.add(transitionListener);
        return this;
    }

    public void a(PathMotion pathMotion) {
        if (pathMotion == null) {
            this.F = H;
        } else {
            this.F = pathMotion;
        }
    }

    public void a(EpicenterCallback epicenterCallback) {
        this.D = epicenterCallback;
    }

    public void a(TransitionPropagation transitionPropagation) {
        this.C = transitionPropagation;
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        String str2 = str + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ": ";
        if (this.c != -1) {
            str2 = str2 + "dur(" + this.c + ") ";
        }
        if (this.b != -1) {
            str2 = str2 + "dly(" + this.b + ") ";
        }
        if (this.d != null) {
            str2 = str2 + "interp(" + this.d + ") ";
        }
        if (this.e.size() <= 0 && this.f.size() <= 0) {
            return str2;
        }
        String str3 = str2 + "tgts(";
        if (this.e.size() > 0) {
            String str4 = str3;
            for (int i2 = 0; i2 < this.e.size(); i2++) {
                if (i2 > 0) {
                    str4 = str4 + ", ";
                }
                str4 = str4 + this.e.get(i2);
            }
            str3 = str4;
        }
        if (this.f.size() > 0) {
            for (int i3 = 0; i3 < this.f.size(); i3++) {
                if (i3 > 0) {
                    str3 = str3 + ", ";
                }
                str3 = str3 + this.f.get(i3);
            }
        }
        return str3 + ")";
    }
}
