package androidx.transition;

import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TransitionValues {

    /* renamed from: a  reason: collision with root package name */
    public final Map<String, Object> f1098a = new HashMap();
    public View b;
    final ArrayList<Transition> c = new ArrayList<>();

    public boolean equals(Object obj) {
        if (!(obj instanceof TransitionValues)) {
            return false;
        }
        TransitionValues transitionValues = (TransitionValues) obj;
        return this.b == transitionValues.b && this.f1098a.equals(transitionValues.f1098a);
    }

    public int hashCode() {
        return (this.b.hashCode() * 31) + this.f1098a.hashCode();
    }

    public String toString() {
        String str = (("TransitionValues@" + Integer.toHexString(hashCode()) + ":\n") + "    view = " + this.b + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE) + "    values:";
        for (String next : this.f1098a.keySet()) {
            str = str + "    " + next + ": " + this.f1098a.get(next) + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE;
        }
        return str;
    }
}
