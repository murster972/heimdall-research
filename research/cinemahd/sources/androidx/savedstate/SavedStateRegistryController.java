package androidx.savedstate;

import android.os.Bundle;
import androidx.lifecycle.Lifecycle;

public final class SavedStateRegistryController {

    /* renamed from: a  reason: collision with root package name */
    private final SavedStateRegistryOwner f1044a;
    private final SavedStateRegistry b = new SavedStateRegistry();

    private SavedStateRegistryController(SavedStateRegistryOwner savedStateRegistryOwner) {
        this.f1044a = savedStateRegistryOwner;
    }

    public SavedStateRegistry a() {
        return this.b;
    }

    public void b(Bundle bundle) {
        this.b.a(bundle);
    }

    public void a(Bundle bundle) {
        Lifecycle lifecycle = this.f1044a.getLifecycle();
        if (lifecycle.a() == Lifecycle.State.INITIALIZED) {
            lifecycle.a(new Recreator(this.f1044a));
            this.b.a(lifecycle, bundle);
            return;
        }
        throw new IllegalStateException("Restarter must be created only during owner's initialization stage");
    }

    public static SavedStateRegistryController a(SavedStateRegistryOwner savedStateRegistryOwner) {
        return new SavedStateRegistryController(savedStateRegistryOwner);
    }
}
