package androidx.customview.widget;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import androidx.collection.SparseArrayCompat;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewParentCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.core.view.accessibility.AccessibilityNodeProviderCompat;
import androidx.core.view.accessibility.AccessibilityRecordCompat;
import androidx.customview.widget.FocusStrategy;
import com.google.ar.core.ImageMetadata;
import java.util.ArrayList;
import java.util.List;

public abstract class ExploreByTouchHelper extends AccessibilityDelegateCompat {
    private static final Rect k = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
    private static final FocusStrategy.BoundsAdapter<AccessibilityNodeInfoCompat> l = new FocusStrategy.BoundsAdapter<AccessibilityNodeInfoCompat>() {
        public void a(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, Rect rect) {
            accessibilityNodeInfoCompat.a(rect);
        }
    };
    private static final FocusStrategy.CollectionAdapter<SparseArrayCompat<AccessibilityNodeInfoCompat>, AccessibilityNodeInfoCompat> m = new FocusStrategy.CollectionAdapter<SparseArrayCompat<AccessibilityNodeInfoCompat>, AccessibilityNodeInfoCompat>() {
        public AccessibilityNodeInfoCompat a(SparseArrayCompat<AccessibilityNodeInfoCompat> sparseArrayCompat, int i) {
            return sparseArrayCompat.e(i);
        }

        public int a(SparseArrayCompat<AccessibilityNodeInfoCompat> sparseArrayCompat) {
            return sparseArrayCompat.b();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Rect f648a = new Rect();
    private final Rect b = new Rect();
    private final Rect c = new Rect();
    private final int[] d = new int[2];
    private final AccessibilityManager e;
    private final View f;
    private MyNodeProvider g;
    int h = Integer.MIN_VALUE;
    int i = Integer.MIN_VALUE;
    private int j = Integer.MIN_VALUE;

    public ExploreByTouchHelper(View view) {
        if (view != null) {
            this.f = view;
            this.e = (AccessibilityManager) view.getContext().getSystemService("accessibility");
            view.setFocusable(true);
            if (ViewCompat.k(view) == 0) {
                ViewCompat.h(view, 1);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("View may not be null");
    }

    private boolean b(int i2, Rect rect) {
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat;
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2;
        SparseArrayCompat<AccessibilityNodeInfoCompat> c2 = c();
        int i3 = this.i;
        int i4 = Integer.MIN_VALUE;
        if (i3 == Integer.MIN_VALUE) {
            accessibilityNodeInfoCompat = null;
        } else {
            accessibilityNodeInfoCompat = c2.a(i3);
        }
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat3 = accessibilityNodeInfoCompat;
        if (i2 == 1 || i2 == 2) {
            accessibilityNodeInfoCompat2 = (AccessibilityNodeInfoCompat) FocusStrategy.a(c2, m, l, accessibilityNodeInfoCompat3, i2, ViewCompat.m(this.f) == 1, false);
        } else if (i2 == 17 || i2 == 33 || i2 == 66 || i2 == 130) {
            Rect rect2 = new Rect();
            int i5 = this.i;
            if (i5 != Integer.MIN_VALUE) {
                a(i5, rect2);
            } else if (rect != null) {
                rect2.set(rect);
            } else {
                a(this.f, i2, rect2);
            }
            accessibilityNodeInfoCompat2 = (AccessibilityNodeInfoCompat) FocusStrategy.a(c2, m, l, accessibilityNodeInfoCompat3, rect2, i2);
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD, FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        if (accessibilityNodeInfoCompat2 != null) {
            i4 = c2.c(c2.a(accessibilityNodeInfoCompat2));
        }
        return c(i4);
    }

    private SparseArrayCompat<AccessibilityNodeInfoCompat> c() {
        ArrayList arrayList = new ArrayList();
        a((List<Integer>) arrayList);
        SparseArrayCompat<AccessibilityNodeInfoCompat> sparseArrayCompat = new SparseArrayCompat<>();
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            sparseArrayCompat.c(i2, f(i2));
        }
        return sparseArrayCompat;
    }

    private boolean d(int i2) {
        if (this.h != i2) {
            return false;
        }
        this.h = Integer.MIN_VALUE;
        this.f.invalidate();
        a(i2, (int) ImageMetadata.CONTROL_AE_ANTIBANDING_MODE);
        return true;
    }

    private AccessibilityEvent e(int i2) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
        this.f.onInitializeAccessibilityEvent(obtain);
        return obtain;
    }

    private AccessibilityNodeInfoCompat f(int i2) {
        AccessibilityNodeInfoCompat A = AccessibilityNodeInfoCompat.A();
        A.h(true);
        A.i(true);
        A.a((CharSequence) "android.view.View");
        A.c(k);
        A.d(k);
        A.b(this.f);
        a(i2, A);
        if (A.i() == null && A.e() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        A.a(this.b);
        if (!this.b.equals(k)) {
            int a2 = A.a();
            if ((a2 & 64) != 0) {
                throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            } else if ((a2 & 128) == 0) {
                A.e((CharSequence) this.f.getContext().getPackageName());
                A.c(this.f, i2);
                if (this.h == i2) {
                    A.a(true);
                    A.a(128);
                } else {
                    A.a(false);
                    A.a(64);
                }
                boolean z = this.i == i2;
                if (z) {
                    A.a(2);
                } else if (A.p()) {
                    A.a(1);
                }
                A.j(z);
                this.f.getLocationOnScreen(this.d);
                A.b(this.f648a);
                if (this.f648a.equals(k)) {
                    A.a(this.f648a);
                    if (A.b != -1) {
                        AccessibilityNodeInfoCompat A2 = AccessibilityNodeInfoCompat.A();
                        for (int i3 = A.b; i3 != -1; i3 = A2.b) {
                            A2.b(this.f, -1);
                            A2.c(k);
                            a(i3, A2);
                            A2.a(this.b);
                            Rect rect = this.f648a;
                            Rect rect2 = this.b;
                            rect.offset(rect2.left, rect2.top);
                        }
                        A2.w();
                    }
                    this.f648a.offset(this.d[0] - this.f.getScrollX(), this.d[1] - this.f.getScrollY());
                }
                if (this.f.getLocalVisibleRect(this.c)) {
                    this.c.offset(this.d[0] - this.f.getScrollX(), this.d[1] - this.f.getScrollY());
                    if (this.f648a.intersect(this.c)) {
                        A.d(this.f648a);
                        if (a(this.f648a)) {
                            A.q(true);
                        }
                    }
                }
                return A;
            } else {
                throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            }
        } else {
            throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
    }

    private static int g(int i2) {
        if (i2 == 19) {
            return 33;
        }
        if (i2 != 21) {
            return i2 != 22 ? 130 : 66;
        }
        return 17;
    }

    private boolean h(int i2) {
        int i3;
        if (!this.e.isEnabled() || !this.e.isTouchExplorationEnabled() || (i3 = this.h) == i2) {
            return false;
        }
        if (i3 != Integer.MIN_VALUE) {
            d(i3);
        }
        this.h = i2;
        this.f.invalidate();
        a(i2, 32768);
        return true;
    }

    private void i(int i2) {
        int i3 = this.j;
        if (i3 != i2) {
            this.j = i2;
            a(i2, 128);
            a(i3, 256);
        }
    }

    /* access modifiers changed from: protected */
    public abstract int a(float f2, float f3);

    /* access modifiers changed from: protected */
    public void a(int i2, AccessibilityEvent accessibilityEvent) {
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i2, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat);

    /* access modifiers changed from: protected */
    public void a(int i2, boolean z) {
    }

    /* access modifiers changed from: protected */
    public void a(AccessibilityEvent accessibilityEvent) {
    }

    /* access modifiers changed from: protected */
    public void a(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
    }

    /* access modifiers changed from: protected */
    public abstract void a(List<Integer> list);

    /* access modifiers changed from: protected */
    public abstract boolean a(int i2, int i3, Bundle bundle);

    public final boolean a(MotionEvent motionEvent) {
        if (!this.e.isEnabled() || !this.e.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 7 || action == 9) {
            int a2 = a(motionEvent.getX(), motionEvent.getY());
            i(a2);
            if (a2 != Integer.MIN_VALUE) {
                return true;
            }
            return false;
        } else if (action != 10 || this.j == Integer.MIN_VALUE) {
            return false;
        } else {
            i(Integer.MIN_VALUE);
            return true;
        }
    }

    public AccessibilityNodeProviderCompat getAccessibilityNodeProvider(View view) {
        if (this.g == null) {
            this.g = new MyNodeProvider();
        }
        return this.g;
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        a(accessibilityEvent);
    }

    public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
        a(accessibilityNodeInfoCompat);
    }

    private class MyNodeProvider extends AccessibilityNodeProviderCompat {
        MyNodeProvider() {
        }

        public AccessibilityNodeInfoCompat a(int i) {
            return AccessibilityNodeInfoCompat.a(ExploreByTouchHelper.this.b(i));
        }

        public AccessibilityNodeInfoCompat b(int i) {
            int i2 = i == 2 ? ExploreByTouchHelper.this.h : ExploreByTouchHelper.this.i;
            if (i2 == Integer.MIN_VALUE) {
                return null;
            }
            return a(i2);
        }

        public boolean a(int i, int i2, Bundle bundle) {
            return ExploreByTouchHelper.this.b(i, i2, bundle);
        }
    }

    private AccessibilityEvent c(int i2, int i3) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i3);
        AccessibilityNodeInfoCompat b2 = b(i2);
        obtain.getText().add(b2.i());
        obtain.setContentDescription(b2.e());
        obtain.setScrollable(b2.t());
        obtain.setPassword(b2.s());
        obtain.setEnabled(b2.o());
        obtain.setChecked(b2.m());
        a(i2, obtain);
        if (!obtain.getText().isEmpty() || obtain.getContentDescription() != null) {
            obtain.setClassName(b2.c());
            AccessibilityRecordCompat.a(obtain, this.f, i2);
            obtain.setPackageName(this.f.getContext().getPackageName());
            return obtain;
        }
        throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
    }

    public final boolean a(KeyEvent keyEvent) {
        int i2 = 0;
        if (keyEvent.getAction() == 1) {
            return false;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyCode != 61) {
            if (keyCode != 66) {
                switch (keyCode) {
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                        if (!keyEvent.hasNoModifiers()) {
                            return false;
                        }
                        int g2 = g(keyCode);
                        int repeatCount = keyEvent.getRepeatCount() + 1;
                        boolean z = false;
                        while (i2 < repeatCount && b(g2, (Rect) null)) {
                            i2++;
                            z = true;
                        }
                        return z;
                    case 23:
                        break;
                    default:
                        return false;
                }
            }
            if (!keyEvent.hasNoModifiers() || keyEvent.getRepeatCount() != 0) {
                return false;
            }
            a();
            return true;
        } else if (keyEvent.hasNoModifiers()) {
            return b(2, (Rect) null);
        } else {
            if (keyEvent.hasModifiers(1)) {
                return b(1, (Rect) null);
            }
            return false;
        }
    }

    private AccessibilityEvent b(int i2, int i3) {
        if (i2 != -1) {
            return c(i2, i3);
        }
        return e(i3);
    }

    /* access modifiers changed from: package-private */
    public AccessibilityNodeInfoCompat b(int i2) {
        if (i2 == -1) {
            return b();
        }
        return f(i2);
    }

    public final void a(boolean z, int i2, Rect rect) {
        int i3 = this.i;
        if (i3 != Integer.MIN_VALUE) {
            a(i3);
        }
        if (z) {
            b(i2, rect);
        }
    }

    private AccessibilityNodeInfoCompat b() {
        AccessibilityNodeInfoCompat f2 = AccessibilityNodeInfoCompat.f(this.f);
        ViewCompat.a(this.f, f2);
        ArrayList arrayList = new ArrayList();
        a((List<Integer>) arrayList);
        if (f2.b() <= 0 || arrayList.size() <= 0) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                f2.a(this.f, ((Integer) arrayList.get(i2)).intValue());
            }
            return f2;
        }
        throw new RuntimeException("Views cannot have both real and virtual children");
    }

    private boolean c(int i2, int i3, Bundle bundle) {
        if (i3 == 1) {
            return c(i2);
        }
        if (i3 == 2) {
            return a(i2);
        }
        if (i3 == 64) {
            return h(i2);
        }
        if (i3 != 128) {
            return a(i2, i3, bundle);
        }
        return d(i2);
    }

    private void a(int i2, Rect rect) {
        b(i2).a(rect);
    }

    private static Rect a(View view, int i2, Rect rect) {
        int width = view.getWidth();
        int height = view.getHeight();
        if (i2 == 17) {
            rect.set(width, 0, width, height);
        } else if (i2 == 33) {
            rect.set(0, height, width, height);
        } else if (i2 == 66) {
            rect.set(-1, 0, -1, height);
        } else if (i2 == 130) {
            rect.set(0, -1, width, -1);
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return rect;
    }

    public final boolean c(int i2) {
        int i3;
        if ((!this.f.isFocused() && !this.f.requestFocus()) || (i3 = this.i) == i2) {
            return false;
        }
        if (i3 != Integer.MIN_VALUE) {
            a(i3);
        }
        this.i = i2;
        a(i2, true);
        a(i2, 8);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2, int i3, Bundle bundle) {
        if (i2 != -1) {
            return c(i2, i3, bundle);
        }
        return a(i3, bundle);
    }

    private boolean a() {
        int i2 = this.i;
        return i2 != Integer.MIN_VALUE && a(i2, 16, (Bundle) null);
    }

    public final boolean a(int i2, int i3) {
        ViewParent parent;
        if (i2 == Integer.MIN_VALUE || !this.e.isEnabled() || (parent = this.f.getParent()) == null) {
            return false;
        }
        return ViewParentCompat.a(parent, this.f, b(i2, i3));
    }

    private boolean a(int i2, Bundle bundle) {
        return ViewCompat.a(this.f, i2, bundle);
    }

    private boolean a(Rect rect) {
        if (rect == null || rect.isEmpty() || this.f.getWindowVisibility() != 0) {
            return false;
        }
        ViewParent parent = this.f.getParent();
        while (parent instanceof View) {
            View view = (View) parent;
            if (view.getAlpha() <= 0.0f || view.getVisibility() != 0) {
                return false;
            }
            parent = view.getParent();
        }
        if (parent != null) {
            return true;
        }
        return false;
    }

    public final boolean a(int i2) {
        if (this.i != i2) {
            return false;
        }
        this.i = Integer.MIN_VALUE;
        a(i2, false);
        a(i2, 8);
        return true;
    }
}
