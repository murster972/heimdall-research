package androidx.swiperefreshlayout.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import androidx.core.util.Preconditions;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import com.facebook.imageutils.JfifUtil;

public class CircularProgressDrawable extends Drawable implements Animatable {
    private static final Interpolator g = new LinearInterpolator();
    private static final Interpolator h = new FastOutSlowInInterpolator();
    private static final int[] i = {-16777216};

    /* renamed from: a  reason: collision with root package name */
    private final Ring f1062a = new Ring();
    private float b;
    private Resources c;
    private Animator d;
    float e;
    boolean f;

    private static class Ring {

        /* renamed from: a  reason: collision with root package name */
        final RectF f1065a = new RectF();
        final Paint b = new Paint();
        final Paint c = new Paint();
        final Paint d = new Paint();
        float e = 0.0f;
        float f = 0.0f;
        float g = 0.0f;
        float h = 5.0f;
        int[] i;
        int j;
        float k;
        float l;
        float m;
        boolean n;
        Path o;
        float p = 1.0f;
        float q;
        int r;
        int s;
        int t = JfifUtil.MARKER_FIRST_BYTE;
        int u;

        Ring() {
            this.b.setStrokeCap(Paint.Cap.SQUARE);
            this.b.setAntiAlias(true);
            this.b.setStyle(Paint.Style.STROKE);
            this.c.setStyle(Paint.Style.FILL);
            this.c.setAntiAlias(true);
            this.d.setColor(0);
        }

        /* access modifiers changed from: package-private */
        public void a(float f2, float f3) {
            this.r = (int) f2;
            this.s = (int) f3;
        }

        /* access modifiers changed from: package-private */
        public void b(int i2) {
            this.u = i2;
        }

        /* access modifiers changed from: package-private */
        public void c(int i2) {
            this.j = i2;
            this.u = this.i[this.j];
        }

        /* access modifiers changed from: package-private */
        public int d() {
            return (this.j + 1) % this.i.length;
        }

        /* access modifiers changed from: package-private */
        public void e(float f2) {
            this.e = f2;
        }

        /* access modifiers changed from: package-private */
        public void f(float f2) {
            this.h = f2;
            this.b.setStrokeWidth(f2);
        }

        /* access modifiers changed from: package-private */
        public float g() {
            return this.l;
        }

        /* access modifiers changed from: package-private */
        public float h() {
            return this.m;
        }

        /* access modifiers changed from: package-private */
        public float i() {
            return this.k;
        }

        /* access modifiers changed from: package-private */
        public void j() {
            c(d());
        }

        /* access modifiers changed from: package-private */
        public void k() {
            this.k = 0.0f;
            this.l = 0.0f;
            this.m = 0.0f;
            e(0.0f);
            c(0.0f);
            d(0.0f);
        }

        /* access modifiers changed from: package-private */
        public void l() {
            this.k = this.e;
            this.l = this.f;
            this.m = this.g;
        }

        /* access modifiers changed from: package-private */
        public float b() {
            return this.f;
        }

        /* access modifiers changed from: package-private */
        public void d(float f2) {
            this.g = f2;
        }

        /* access modifiers changed from: package-private */
        public float e() {
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public void a(Canvas canvas, Rect rect) {
            RectF rectF = this.f1065a;
            float f2 = this.q;
            float f3 = (this.h / 2.0f) + f2;
            if (f2 <= 0.0f) {
                f3 = (((float) Math.min(rect.width(), rect.height())) / 2.0f) - Math.max((((float) this.r) * this.p) / 2.0f, this.h / 2.0f);
            }
            rectF.set(((float) rect.centerX()) - f3, ((float) rect.centerY()) - f3, ((float) rect.centerX()) + f3, ((float) rect.centerY()) + f3);
            float f4 = this.e;
            float f5 = this.g;
            float f6 = (f4 + f5) * 360.0f;
            float f7 = ((this.f + f5) * 360.0f) - f6;
            this.b.setColor(this.u);
            this.b.setAlpha(this.t);
            float f8 = this.h / 2.0f;
            rectF.inset(f8, f8);
            canvas.drawCircle(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, this.d);
            float f9 = -f8;
            rectF.inset(f9, f9);
            canvas.drawArc(rectF, f6, f7, false, this.b);
            a(canvas, f6, f7, rectF);
        }

        /* access modifiers changed from: package-private */
        public void b(float f2) {
            this.q = f2;
        }

        /* access modifiers changed from: package-private */
        public int c() {
            return this.i[d()];
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.i[this.j];
        }

        /* access modifiers changed from: package-private */
        public void c(float f2) {
            this.f = f2;
        }

        /* access modifiers changed from: package-private */
        public void a(Canvas canvas, float f2, float f3, RectF rectF) {
            if (this.n) {
                Path path = this.o;
                if (path == null) {
                    this.o = new Path();
                    this.o.setFillType(Path.FillType.EVEN_ODD);
                } else {
                    path.reset();
                }
                this.o.moveTo(0.0f, 0.0f);
                this.o.lineTo(((float) this.r) * this.p, 0.0f);
                Path path2 = this.o;
                float f4 = this.p;
                path2.lineTo((((float) this.r) * f4) / 2.0f, ((float) this.s) * f4);
                this.o.offset(((Math.min(rectF.width(), rectF.height()) / 2.0f) + rectF.centerX()) - ((((float) this.r) * this.p) / 2.0f), rectF.centerY() + (this.h / 2.0f));
                this.o.close();
                this.c.setColor(this.u);
                this.c.setAlpha(this.t);
                canvas.save();
                canvas.rotate(f2 + f3, rectF.centerX(), rectF.centerY());
                canvas.drawPath(this.o, this.c);
                canvas.restore();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int[] iArr) {
            this.i = iArr;
            c(0);
        }

        /* access modifiers changed from: package-private */
        public void a(ColorFilter colorFilter) {
            this.b.setColorFilter(colorFilter);
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            this.t = i2;
        }

        /* access modifiers changed from: package-private */
        public int a() {
            return this.t;
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            if (this.n != z) {
                this.n = z;
            }
        }

        /* access modifiers changed from: package-private */
        public void a(float f2) {
            if (f2 != this.p) {
                this.p = f2;
            }
        }
    }

    public CircularProgressDrawable(Context context) {
        Preconditions.a(context);
        this.c = context.getResources();
        this.f1062a.a(i);
        c(2.5f);
        a();
    }

    private int a(float f2, int i2, int i3) {
        int i4 = (i2 >> 24) & JfifUtil.MARKER_FIRST_BYTE;
        int i5 = (i2 >> 16) & JfifUtil.MARKER_FIRST_BYTE;
        int i6 = (i2 >> 8) & JfifUtil.MARKER_FIRST_BYTE;
        int i7 = i2 & JfifUtil.MARKER_FIRST_BYTE;
        return ((i4 + ((int) (((float) (((i3 >> 24) & JfifUtil.MARKER_FIRST_BYTE) - i4)) * f2))) << 24) | ((i5 + ((int) (((float) (((i3 >> 16) & JfifUtil.MARKER_FIRST_BYTE) - i5)) * f2))) << 16) | ((i6 + ((int) (((float) (((i3 >> 8) & JfifUtil.MARKER_FIRST_BYTE) - i6)) * f2))) << 8) | (i7 + ((int) (f2 * ((float) ((i3 & JfifUtil.MARKER_FIRST_BYTE) - i7)))));
    }

    private void a(float f2, float f3, float f4, float f5) {
        Ring ring = this.f1062a;
        float f6 = this.c.getDisplayMetrics().density;
        ring.f(f3 * f6);
        ring.b(f2 * f6);
        ring.c(0);
        ring.a(f4 * f6, f5 * f6);
    }

    private void d(float f2) {
        this.b = f2;
    }

    public void b(float f2) {
        this.f1062a.d(f2);
        invalidateSelf();
    }

    public void c(float f2) {
        this.f1062a.f(f2);
        invalidateSelf();
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        canvas.save();
        canvas.rotate(this.b, bounds.exactCenterX(), bounds.exactCenterY());
        this.f1062a.a(canvas, bounds);
        canvas.restore();
    }

    public int getAlpha() {
        return this.f1062a.a();
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isRunning() {
        return this.d.isRunning();
    }

    public void setAlpha(int i2) {
        this.f1062a.a(i2);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f1062a.a(colorFilter);
        invalidateSelf();
    }

    public void start() {
        this.d.cancel();
        this.f1062a.l();
        if (this.f1062a.b() != this.f1062a.e()) {
            this.f = true;
            this.d.setDuration(666);
            this.d.start();
            return;
        }
        this.f1062a.c(0);
        this.f1062a.k();
        this.d.setDuration(1332);
        this.d.start();
    }

    public void stop() {
        this.d.cancel();
        d(0.0f);
        this.f1062a.a(false);
        this.f1062a.c(0);
        this.f1062a.k();
        invalidateSelf();
    }

    private void b(float f2, Ring ring) {
        a(f2, ring);
        ring.e(ring.i() + (((ring.g() - 0.01f) - ring.i()) * f2));
        ring.c(ring.g());
        ring.d(ring.h() + ((((float) (Math.floor((double) (ring.h() / 0.8f)) + 1.0d)) - ring.h()) * f2));
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(11.0f, 3.0f, 12.0f, 6.0f);
        } else {
            a(7.5f, 2.5f, 10.0f, 5.0f);
        }
        invalidateSelf();
    }

    public void a(boolean z) {
        this.f1062a.a(z);
        invalidateSelf();
    }

    public void a(float f2) {
        this.f1062a.a(f2);
        invalidateSelf();
    }

    public void a(float f2, float f3) {
        this.f1062a.e(f2);
        this.f1062a.c(f3);
        invalidateSelf();
    }

    public void a(int... iArr) {
        this.f1062a.a(iArr);
        this.f1062a.c(0);
        invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    public void a(float f2, Ring ring) {
        if (f2 > 0.75f) {
            ring.b(a((f2 - 0.75f) / 0.25f, ring.f(), ring.c()));
        } else {
            ring.b(ring.f());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(float f2, Ring ring, boolean z) {
        float f3;
        float f4;
        if (this.f) {
            b(f2, ring);
        } else if (f2 != 1.0f || z) {
            float h2 = ring.h();
            if (f2 < 0.5f) {
                float i2 = ring.i();
                float f5 = i2;
                f3 = (h.getInterpolation(f2 / 0.5f) * 0.79f) + 0.01f + i2;
                f4 = f5;
            } else {
                f3 = ring.i() + 0.79f;
                f4 = f3 - (((1.0f - h.getInterpolation((f2 - 0.5f) / 0.5f)) * 0.79f) + 0.01f);
            }
            ring.e(f4);
            ring.c(f3);
            ring.d(h2 + (0.20999998f * f2));
            d((f2 + this.e) * 216.0f);
        }
    }

    private void a() {
        final Ring ring = this.f1062a;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                CircularProgressDrawable.this.a(floatValue, ring);
                CircularProgressDrawable.this.a(floatValue, ring, false);
                CircularProgressDrawable.this.invalidateSelf();
            }
        });
        ofFloat.setRepeatCount(-1);
        ofFloat.setRepeatMode(1);
        ofFloat.setInterpolator(g);
        ofFloat.addListener(new Animator.AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
                CircularProgressDrawable.this.a(1.0f, ring, true);
                ring.l();
                ring.j();
                CircularProgressDrawable circularProgressDrawable = CircularProgressDrawable.this;
                if (circularProgressDrawable.f) {
                    circularProgressDrawable.f = false;
                    animator.cancel();
                    animator.setDuration(1332);
                    animator.start();
                    ring.a(false);
                    return;
                }
                circularProgressDrawable.e += 1.0f;
            }

            public void onAnimationStart(Animator animator) {
                CircularProgressDrawable.this.e = 0.0f;
            }
        });
        this.d = ofFloat;
    }
}
