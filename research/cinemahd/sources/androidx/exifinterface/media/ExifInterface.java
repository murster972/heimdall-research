package androidx.exifinterface.media;

import android.content.res.AssetManager;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imageutils.JfifUtil;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

public class ExifInterface {
    private static final byte[] A = {-119, 80, 78, 71, 13, 10, 26, 10};
    private static final byte[] B = {101, 88, 73, 102};
    private static final byte[] C = {73, 72, 68, 82};
    private static final byte[] D = {73, 69, 78, 68};
    private static final byte[] E = {82, 73, 70, 70};
    private static final byte[] F = {87, 69, 66, 80};
    private static final byte[] G = {69, 88, 73, 70};
    private static SimpleDateFormat H = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
    static final String[] I = {"", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD"};
    static final int[] J = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1};
    static final byte[] K = {65, 83, 67, 73, 73, 0, 0, 0};
    private static final ExifTag[] L = {new ExifTag("NewSubfileType", 254, 4), new ExifTag("SubfileType", JfifUtil.MARKER_FIRST_BYTE, 4), new ExifTag("ImageWidth", 256, 3, 4), new ExifTag("ImageLength", 257, 3, 4), new ExifTag("BitsPerSample", 258, 3), new ExifTag("Compression", 259, 3), new ExifTag("PhotometricInterpretation", 262, 3), new ExifTag("ImageDescription", RotationOptions.ROTATE_270, 2), new ExifTag("Make", 271, 2), new ExifTag("Model", 272, 2), new ExifTag("StripOffsets", 273, 3, 4), new ExifTag("Orientation", TiffUtil.TIFF_TAG_ORIENTATION, 3), new ExifTag("SamplesPerPixel", 277, 3), new ExifTag("RowsPerStrip", 278, 3, 4), new ExifTag("StripByteCounts", 279, 3, 4), new ExifTag("XResolution", 282, 5), new ExifTag("YResolution", 283, 5), new ExifTag("PlanarConfiguration", 284, 3), new ExifTag("ResolutionUnit", 296, 3), new ExifTag("TransferFunction", 301, 3), new ExifTag("Software", 305, 2), new ExifTag("DateTime", 306, 2), new ExifTag("Artist", 315, 2), new ExifTag("WhitePoint", 318, 5), new ExifTag("PrimaryChromaticities", 319, 5), new ExifTag("SubIFDPointer", 330, 4), new ExifTag("JPEGInterchangeFormat", 513, 4), new ExifTag("JPEGInterchangeFormatLength", 514, 4), new ExifTag("YCbCrCoefficients", 529, 5), new ExifTag("YCbCrSubSampling", 530, 3), new ExifTag("YCbCrPositioning", 531, 3), new ExifTag("ReferenceBlackWhite", 532, 5), new ExifTag("Copyright", 33432, 2), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("SensorTopBorder", 4, 4), new ExifTag("SensorLeftBorder", 5, 4), new ExifTag("SensorBottomBorder", 6, 4), new ExifTag("SensorRightBorder", 7, 4), new ExifTag("ISO", 23, 3), new ExifTag("JpgFromRaw", 46, 7), new ExifTag("Xmp", 700, 1)};
    private static final ExifTag[] M = {new ExifTag("ExposureTime", 33434, 5), new ExifTag("FNumber", 33437, 5), new ExifTag("ExposureProgram", 34850, 3), new ExifTag("SpectralSensitivity", 34852, 2), new ExifTag("PhotographicSensitivity", 34855, 3), new ExifTag("OECF", 34856, 7), new ExifTag("SensitivityType", 34864, 3), new ExifTag("StandardOutputSensitivity", 34865, 4), new ExifTag("RecommendedExposureIndex", 34866, 4), new ExifTag("ISOSpeed", 34867, 4), new ExifTag("ISOSpeedLatitudeyyy", 34868, 4), new ExifTag("ISOSpeedLatitudezzz", 34869, 4), new ExifTag("ExifVersion", 36864, 2), new ExifTag("DateTimeOriginal", 36867, 2), new ExifTag("DateTimeDigitized", 36868, 2), new ExifTag("OffsetTime", 36880, 2), new ExifTag("OffsetTimeOriginal", 36881, 2), new ExifTag("OffsetTimeDigitized", 36882, 2), new ExifTag("ComponentsConfiguration", 37121, 7), new ExifTag("CompressedBitsPerPixel", 37122, 5), new ExifTag("ShutterSpeedValue", 37377, 10), new ExifTag("ApertureValue", 37378, 5), new ExifTag("BrightnessValue", 37379, 10), new ExifTag("ExposureBiasValue", 37380, 10), new ExifTag("MaxApertureValue", 37381, 5), new ExifTag("SubjectDistance", 37382, 5), new ExifTag("MeteringMode", 37383, 3), new ExifTag("LightSource", 37384, 3), new ExifTag("Flash", 37385, 3), new ExifTag("FocalLength", 37386, 5), new ExifTag("SubjectArea", 37396, 3), new ExifTag("MakerNote", 37500, 7), new ExifTag("UserComment", 37510, 7), new ExifTag("SubSecTime", 37520, 2), new ExifTag("SubSecTimeOriginal", 37521, 2), new ExifTag("SubSecTimeDigitized", 37522, 2), new ExifTag("FlashpixVersion", 40960, 7), new ExifTag("ColorSpace", 40961, 3), new ExifTag("PixelXDimension", 40962, 3, 4), new ExifTag("PixelYDimension", 40963, 3, 4), new ExifTag("RelatedSoundFile", 40964, 2), new ExifTag("InteroperabilityIFDPointer", 40965, 4), new ExifTag("FlashEnergy", 41483, 5), new ExifTag("SpatialFrequencyResponse", 41484, 7), new ExifTag("FocalPlaneXResolution", 41486, 5), new ExifTag("FocalPlaneYResolution", 41487, 5), new ExifTag("FocalPlaneResolutionUnit", 41488, 3), new ExifTag("SubjectLocation", 41492, 3), new ExifTag("ExposureIndex", 41493, 5), new ExifTag("SensingMethod", 41495, 3), new ExifTag("FileSource", 41728, 7), new ExifTag("SceneType", 41729, 7), new ExifTag("CFAPattern", 41730, 7), new ExifTag("CustomRendered", 41985, 3), new ExifTag("ExposureMode", 41986, 3), new ExifTag("WhiteBalance", 41987, 3), new ExifTag("DigitalZoomRatio", 41988, 5), new ExifTag("FocalLengthIn35mmFilm", 41989, 3), new ExifTag("SceneCaptureType", 41990, 3), new ExifTag("GainControl", 41991, 3), new ExifTag("Contrast", 41992, 3), new ExifTag("Saturation", 41993, 3), new ExifTag("Sharpness", 41994, 3), new ExifTag("DeviceSettingDescription", 41995, 7), new ExifTag("SubjectDistanceRange", 41996, 3), new ExifTag("ImageUniqueID", 42016, 2), new ExifTag("CameraOwnerName", 42032, 2), new ExifTag("BodySerialNumber", 42033, 2), new ExifTag("LensSpecification", 42034, 5), new ExifTag("LensMake", 42035, 2), new ExifTag("LensModel", 42036, 2), new ExifTag("Gamma", 42240, 5), new ExifTag("DNGVersion", 50706, 1), new ExifTag("DefaultCropSize", 50720, 3, 4)};
    private static final ExifTag[] N = {new ExifTag("GPSVersionID", 0, 1), new ExifTag("GPSLatitudeRef", 1, 2), new ExifTag("GPSLatitude", 2, 5), new ExifTag("GPSLongitudeRef", 3, 2), new ExifTag("GPSLongitude", 4, 5), new ExifTag("GPSAltitudeRef", 5, 1), new ExifTag("GPSAltitude", 6, 5), new ExifTag("GPSTimeStamp", 7, 5), new ExifTag("GPSSatellites", 8, 2), new ExifTag("GPSStatus", 9, 2), new ExifTag("GPSMeasureMode", 10, 2), new ExifTag("GPSDOP", 11, 5), new ExifTag("GPSSpeedRef", 12, 2), new ExifTag("GPSSpeed", 13, 5), new ExifTag("GPSTrackRef", 14, 2), new ExifTag("GPSTrack", 15, 5), new ExifTag("GPSImgDirectionRef", 16, 2), new ExifTag("GPSImgDirection", 17, 5), new ExifTag("GPSMapDatum", 18, 2), new ExifTag("GPSDestLatitudeRef", 19, 2), new ExifTag("GPSDestLatitude", 20, 5), new ExifTag("GPSDestLongitudeRef", 21, 2), new ExifTag("GPSDestLongitude", 22, 5), new ExifTag("GPSDestBearingRef", 23, 2), new ExifTag("GPSDestBearing", 24, 5), new ExifTag("GPSDestDistanceRef", 25, 2), new ExifTag("GPSDestDistance", 26, 5), new ExifTag("GPSProcessingMethod", 27, 7), new ExifTag("GPSAreaInformation", 28, 7), new ExifTag("GPSDateStamp", 29, 2), new ExifTag("GPSDifferential", 30, 3), new ExifTag("GPSHPositioningError", 31, 5)};
    private static final ExifTag[] O = {new ExifTag("InteroperabilityIndex", 1, 2)};
    private static final ExifTag[] P = {new ExifTag("NewSubfileType", 254, 4), new ExifTag("SubfileType", JfifUtil.MARKER_FIRST_BYTE, 4), new ExifTag("ThumbnailImageWidth", 256, 3, 4), new ExifTag("ThumbnailImageLength", 257, 3, 4), new ExifTag("BitsPerSample", 258, 3), new ExifTag("Compression", 259, 3), new ExifTag("PhotometricInterpretation", 262, 3), new ExifTag("ImageDescription", RotationOptions.ROTATE_270, 2), new ExifTag("Make", 271, 2), new ExifTag("Model", 272, 2), new ExifTag("StripOffsets", 273, 3, 4), new ExifTag("ThumbnailOrientation", TiffUtil.TIFF_TAG_ORIENTATION, 3), new ExifTag("SamplesPerPixel", 277, 3), new ExifTag("RowsPerStrip", 278, 3, 4), new ExifTag("StripByteCounts", 279, 3, 4), new ExifTag("XResolution", 282, 5), new ExifTag("YResolution", 283, 5), new ExifTag("PlanarConfiguration", 284, 3), new ExifTag("ResolutionUnit", 296, 3), new ExifTag("TransferFunction", 301, 3), new ExifTag("Software", 305, 2), new ExifTag("DateTime", 306, 2), new ExifTag("Artist", 315, 2), new ExifTag("WhitePoint", 318, 5), new ExifTag("PrimaryChromaticities", 319, 5), new ExifTag("SubIFDPointer", 330, 4), new ExifTag("JPEGInterchangeFormat", 513, 4), new ExifTag("JPEGInterchangeFormatLength", 514, 4), new ExifTag("YCbCrCoefficients", 529, 5), new ExifTag("YCbCrSubSampling", 530, 3), new ExifTag("YCbCrPositioning", 531, 3), new ExifTag("ReferenceBlackWhite", 532, 5), new ExifTag("Copyright", 33432, 2), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("DNGVersion", 50706, 1), new ExifTag("DefaultCropSize", 50720, 3, 4)};
    private static final ExifTag Q = new ExifTag("StripOffsets", 273, 3);
    private static final ExifTag[] R = {new ExifTag("ThumbnailImage", 256, 7), new ExifTag("CameraSettingsIFDPointer", 8224, 4), new ExifTag("ImageProcessingIFDPointer", 8256, 4)};
    private static final ExifTag[] S = {new ExifTag("PreviewImageStart", 257, 4), new ExifTag("PreviewImageLength", 258, 4)};
    private static final ExifTag[] T = {new ExifTag("AspectFrame", 4371, 3)};
    private static final ExifTag[] U = {new ExifTag("ColorSpace", 55, 3)};
    static final ExifTag[][] V;
    private static final ExifTag[] W = {new ExifTag("SubIFDPointer", 330, 4), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("InteroperabilityIFDPointer", 40965, 4), new ExifTag("CameraSettingsIFDPointer", 8224, 1), new ExifTag("ImageProcessingIFDPointer", 8256, 1)};
    private static final HashMap<Integer, ExifTag>[] X;
    private static final HashMap<String, ExifTag>[] Y;
    private static final HashSet<String> Z = new HashSet<>(Arrays.asList(new String[]{"FNumber", "DigitalZoomRatio", "ExposureTime", "SubjectDistance", "GPSTimeStamp"}));
    private static final HashMap<Integer, Integer> a0 = new HashMap<>();
    static final Charset b0 = Charset.forName("US-ASCII");
    static final byte[] c0 = "Exif\u0000\u0000".getBytes(b0);
    private static final byte[] d0 = "http://ns.adobe.com/xap/1.0/\u0000".getBytes(b0);
    private static final boolean r = Log.isLoggable("ExifInterface", 3);
    public static final int[] s = {8, 8, 8};
    public static final int[] t = {8};
    static final byte[] u = {-1, -40, -1};
    private static final byte[] v = {102, 116, 121, 112};
    private static final byte[] w = {109, 105, 102, 49};
    private static final byte[] x = {104, 101, 105, 99};
    private static final byte[] y = {79, 76, 89, 77, 80, 0};
    private static final byte[] z = {79, 76, 89, 77, 80, 85, 83, 0, 73, 73};

    /* renamed from: a  reason: collision with root package name */
    private String f656a;
    private FileDescriptor b;
    private AssetManager.AssetInputStream c;
    private int d;
    private boolean e;
    private final HashMap<String, ExifAttribute>[] f;
    private Set<Integer> g;
    private ByteOrder h;
    private boolean i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;

    private static class ByteOrderedDataInputStream extends InputStream implements DataInput {
        private static final ByteOrder e = ByteOrder.LITTLE_ENDIAN;
        private static final ByteOrder f = ByteOrder.BIG_ENDIAN;

        /* renamed from: a  reason: collision with root package name */
        private DataInputStream f658a;
        private ByteOrder b;
        final int c;
        int d;

        public ByteOrderedDataInputStream(InputStream inputStream) throws IOException {
            this(inputStream, ByteOrder.BIG_ENDIAN);
        }

        public void a(ByteOrder byteOrder) {
            this.b = byteOrder;
        }

        public int available() throws IOException {
            return this.f658a.available();
        }

        public void h(long j) throws IOException {
            int i = this.d;
            if (((long) i) > j) {
                this.d = 0;
                this.f658a.reset();
                this.f658a.mark(this.c);
            } else {
                j -= (long) i;
            }
            int i2 = (int) j;
            if (skipBytes(i2) != i2) {
                throw new IOException("Couldn't seek up to the byteCount");
            }
        }

        public int peek() {
            return this.d;
        }

        public int read() throws IOException {
            this.d++;
            return this.f658a.read();
        }

        public boolean readBoolean() throws IOException {
            this.d++;
            return this.f658a.readBoolean();
        }

        public byte readByte() throws IOException {
            this.d++;
            if (this.d <= this.c) {
                int read = this.f658a.read();
                if (read >= 0) {
                    return (byte) read;
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        public char readChar() throws IOException {
            this.d += 2;
            return this.f658a.readChar();
        }

        public double readDouble() throws IOException {
            return Double.longBitsToDouble(readLong());
        }

        public float readFloat() throws IOException {
            return Float.intBitsToFloat(readInt());
        }

        public void readFully(byte[] bArr, int i, int i2) throws IOException {
            this.d += i2;
            if (this.d > this.c) {
                throw new EOFException();
            } else if (this.f658a.read(bArr, i, i2) != i2) {
                throw new IOException("Couldn't read up to the length of buffer");
            }
        }

        public int readInt() throws IOException {
            this.d += 4;
            if (this.d <= this.c) {
                int read = this.f658a.read();
                int read2 = this.f658a.read();
                int read3 = this.f658a.read();
                int read4 = this.f658a.read();
                if ((read | read2 | read3 | read4) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (read4 << 24) + (read3 << 16) + (read2 << 8) + read;
                    }
                    if (byteOrder == f) {
                        return (read << 24) + (read2 << 16) + (read3 << 8) + read4;
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        public String readLine() throws IOException {
            Log.d("ExifInterface", "Currently unsupported");
            return null;
        }

        public long readLong() throws IOException {
            this.d += 8;
            if (this.d <= this.c) {
                int read = this.f658a.read();
                int read2 = this.f658a.read();
                int read3 = this.f658a.read();
                int read4 = this.f658a.read();
                int read5 = this.f658a.read();
                int read6 = this.f658a.read();
                int read7 = this.f658a.read();
                int read8 = this.f658a.read();
                if ((read | read2 | read3 | read4 | read5 | read6 | read7 | read8) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (((long) read8) << 56) + (((long) read7) << 48) + (((long) read6) << 40) + (((long) read5) << 32) + (((long) read4) << 24) + (((long) read3) << 16) + (((long) read2) << 8) + ((long) read);
                    }
                    int i = read2;
                    if (byteOrder == f) {
                        return (((long) read) << 56) + (((long) i) << 48) + (((long) read3) << 40) + (((long) read4) << 32) + (((long) read5) << 24) + (((long) read6) << 16) + (((long) read7) << 8) + ((long) read8);
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        public short readShort() throws IOException {
            this.d += 2;
            if (this.d <= this.c) {
                int read = this.f658a.read();
                int read2 = this.f658a.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (short) ((read2 << 8) + read);
                    }
                    if (byteOrder == f) {
                        return (short) ((read << 8) + read2);
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        public String readUTF() throws IOException {
            this.d += 2;
            return this.f658a.readUTF();
        }

        public int readUnsignedByte() throws IOException {
            this.d++;
            return this.f658a.readUnsignedByte();
        }

        public int readUnsignedShort() throws IOException {
            this.d += 2;
            if (this.d <= this.c) {
                int read = this.f658a.read();
                int read2 = this.f658a.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (read2 << 8) + read;
                    }
                    if (byteOrder == f) {
                        return (read << 8) + read2;
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        public int s() {
            return this.c;
        }

        public int skipBytes(int i) throws IOException {
            int min = Math.min(i, this.c - this.d);
            int i2 = 0;
            while (i2 < min) {
                i2 += this.f658a.skipBytes(min - i2);
            }
            this.d += i2;
            return i2;
        }

        public long t() throws IOException {
            return ((long) readInt()) & 4294967295L;
        }

        ByteOrderedDataInputStream(InputStream inputStream, ByteOrder byteOrder) throws IOException {
            this.b = ByteOrder.BIG_ENDIAN;
            this.f658a = new DataInputStream(inputStream);
            this.c = this.f658a.available();
            this.d = 0;
            this.f658a.mark(this.c);
            this.b = byteOrder;
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = this.f658a.read(bArr, i, i2);
            this.d += read;
            return read;
        }

        public void readFully(byte[] bArr) throws IOException {
            this.d += bArr.length;
            if (this.d > this.c) {
                throw new EOFException();
            } else if (this.f658a.read(bArr, 0, bArr.length) != bArr.length) {
                throw new IOException("Couldn't read up to the length of buffer");
            }
        }

        public ByteOrderedDataInputStream(byte[] bArr) throws IOException {
            this((InputStream) new ByteArrayInputStream(bArr));
        }
    }

    private static class ExifAttribute {

        /* renamed from: a  reason: collision with root package name */
        public final int f659a;
        public final int b;
        public final byte[] c;

        ExifAttribute(int i, int i2, byte[] bArr) {
            this(i, i2, -1, bArr);
        }

        public static ExifAttribute a(int[] iArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(ExifInterface.J[3] * iArr.length)]);
            wrap.order(byteOrder);
            for (int i : iArr) {
                wrap.putShort((short) i);
            }
            return new ExifAttribute(3, iArr.length, wrap.array());
        }

        public int b(ByteOrder byteOrder) {
            Object d = d(byteOrder);
            if (d == null) {
                throw new NumberFormatException("NULL can't be converted to a integer value");
            } else if (d instanceof String) {
                return Integer.parseInt((String) d);
            } else {
                if (d instanceof long[]) {
                    long[] jArr = (long[]) d;
                    if (jArr.length == 1) {
                        return (int) jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof int[]) {
                    int[] iArr = (int[]) d;
                    if (iArr.length == 1) {
                        return iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a integer value");
                }
            }
        }

        public String c(ByteOrder byteOrder) {
            Object d = d(byteOrder);
            if (d == null) {
                return null;
            }
            if (d instanceof String) {
                return (String) d;
            }
            StringBuilder sb = new StringBuilder();
            int i = 0;
            if (d instanceof long[]) {
                long[] jArr = (long[]) d;
                while (i < jArr.length) {
                    sb.append(jArr[i]);
                    i++;
                    if (i != jArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (d instanceof int[]) {
                int[] iArr = (int[]) d;
                while (i < iArr.length) {
                    sb.append(iArr[i]);
                    i++;
                    if (i != iArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (d instanceof double[]) {
                double[] dArr = (double[]) d;
                while (i < dArr.length) {
                    sb.append(dArr[i]);
                    i++;
                    if (i != dArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (!(d instanceof Rational[])) {
                return null;
            } else {
                Rational[] rationalArr = (Rational[]) d;
                while (i < rationalArr.length) {
                    sb.append(rationalArr[i].f661a);
                    sb.append('/');
                    sb.append(rationalArr[i].b);
                    i++;
                    if (i != rationalArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:164:0x01ab A[SYNTHETIC, Splitter:B:164:0x01ab] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object d(java.nio.ByteOrder r11) {
            /*
                r10 = this;
                java.lang.String r0 = "IOException occurred while closing InputStream"
                java.lang.String r1 = "ExifInterface"
                r2 = 0
                androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream r3 = new androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream     // Catch:{ IOException -> 0x0196, all -> 0x0193 }
                byte[] r4 = r10.c     // Catch:{ IOException -> 0x0196, all -> 0x0193 }
                r3.<init>((byte[]) r4)     // Catch:{ IOException -> 0x0196, all -> 0x0193 }
                r3.a(r11)     // Catch:{ IOException -> 0x0191 }
                int r11 = r10.f659a     // Catch:{ IOException -> 0x0191 }
                r4 = 1
                r5 = 0
                switch(r11) {
                    case 1: goto L_0x014c;
                    case 2: goto L_0x00fd;
                    case 3: goto L_0x00e3;
                    case 4: goto L_0x00c9;
                    case 5: goto L_0x00a6;
                    case 6: goto L_0x014c;
                    case 7: goto L_0x00fd;
                    case 8: goto L_0x008c;
                    case 9: goto L_0x0072;
                    case 10: goto L_0x004d;
                    case 11: goto L_0x0032;
                    case 12: goto L_0x0018;
                    default: goto L_0x0016;
                }     // Catch:{ IOException -> 0x0191 }
            L_0x0016:
                goto L_0x0188
            L_0x0018:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                double[] r11 = new double[r11]     // Catch:{ IOException -> 0x0191 }
            L_0x001c:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x0029
                double r6 = r3.readDouble()     // Catch:{ IOException -> 0x0191 }
                r11[r5] = r6     // Catch:{ IOException -> 0x0191 }
                int r5 = r5 + 1
                goto L_0x001c
            L_0x0029:
                r3.close()     // Catch:{ IOException -> 0x002d }
                goto L_0x0031
            L_0x002d:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x0031:
                return r11
            L_0x0032:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                double[] r11 = new double[r11]     // Catch:{ IOException -> 0x0191 }
            L_0x0036:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x0044
                float r4 = r3.readFloat()     // Catch:{ IOException -> 0x0191 }
                double r6 = (double) r4     // Catch:{ IOException -> 0x0191 }
                r11[r5] = r6     // Catch:{ IOException -> 0x0191 }
                int r5 = r5 + 1
                goto L_0x0036
            L_0x0044:
                r3.close()     // Catch:{ IOException -> 0x0048 }
                goto L_0x004c
            L_0x0048:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x004c:
                return r11
            L_0x004d:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                androidx.exifinterface.media.ExifInterface$Rational[] r11 = new androidx.exifinterface.media.ExifInterface.Rational[r11]     // Catch:{ IOException -> 0x0191 }
            L_0x0051:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x0069
                int r4 = r3.readInt()     // Catch:{ IOException -> 0x0191 }
                long r6 = (long) r4     // Catch:{ IOException -> 0x0191 }
                int r4 = r3.readInt()     // Catch:{ IOException -> 0x0191 }
                long r8 = (long) r4     // Catch:{ IOException -> 0x0191 }
                androidx.exifinterface.media.ExifInterface$Rational r4 = new androidx.exifinterface.media.ExifInterface$Rational     // Catch:{ IOException -> 0x0191 }
                r4.<init>(r6, r8)     // Catch:{ IOException -> 0x0191 }
                r11[r5] = r4     // Catch:{ IOException -> 0x0191 }
                int r5 = r5 + 1
                goto L_0x0051
            L_0x0069:
                r3.close()     // Catch:{ IOException -> 0x006d }
                goto L_0x0071
            L_0x006d:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x0071:
                return r11
            L_0x0072:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                int[] r11 = new int[r11]     // Catch:{ IOException -> 0x0191 }
            L_0x0076:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x0083
                int r4 = r3.readInt()     // Catch:{ IOException -> 0x0191 }
                r11[r5] = r4     // Catch:{ IOException -> 0x0191 }
                int r5 = r5 + 1
                goto L_0x0076
            L_0x0083:
                r3.close()     // Catch:{ IOException -> 0x0087 }
                goto L_0x008b
            L_0x0087:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x008b:
                return r11
            L_0x008c:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                int[] r11 = new int[r11]     // Catch:{ IOException -> 0x0191 }
            L_0x0090:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x009d
                short r4 = r3.readShort()     // Catch:{ IOException -> 0x0191 }
                r11[r5] = r4     // Catch:{ IOException -> 0x0191 }
                int r5 = r5 + 1
                goto L_0x0090
            L_0x009d:
                r3.close()     // Catch:{ IOException -> 0x00a1 }
                goto L_0x00a5
            L_0x00a1:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x00a5:
                return r11
            L_0x00a6:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                androidx.exifinterface.media.ExifInterface$Rational[] r11 = new androidx.exifinterface.media.ExifInterface.Rational[r11]     // Catch:{ IOException -> 0x0191 }
            L_0x00aa:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x00c0
                long r6 = r3.t()     // Catch:{ IOException -> 0x0191 }
                long r8 = r3.t()     // Catch:{ IOException -> 0x0191 }
                androidx.exifinterface.media.ExifInterface$Rational r4 = new androidx.exifinterface.media.ExifInterface$Rational     // Catch:{ IOException -> 0x0191 }
                r4.<init>(r6, r8)     // Catch:{ IOException -> 0x0191 }
                r11[r5] = r4     // Catch:{ IOException -> 0x0191 }
                int r5 = r5 + 1
                goto L_0x00aa
            L_0x00c0:
                r3.close()     // Catch:{ IOException -> 0x00c4 }
                goto L_0x00c8
            L_0x00c4:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x00c8:
                return r11
            L_0x00c9:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                long[] r11 = new long[r11]     // Catch:{ IOException -> 0x0191 }
            L_0x00cd:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x00da
                long r6 = r3.t()     // Catch:{ IOException -> 0x0191 }
                r11[r5] = r6     // Catch:{ IOException -> 0x0191 }
                int r5 = r5 + 1
                goto L_0x00cd
            L_0x00da:
                r3.close()     // Catch:{ IOException -> 0x00de }
                goto L_0x00e2
            L_0x00de:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x00e2:
                return r11
            L_0x00e3:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                int[] r11 = new int[r11]     // Catch:{ IOException -> 0x0191 }
            L_0x00e7:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x00f4
                int r4 = r3.readUnsignedShort()     // Catch:{ IOException -> 0x0191 }
                r11[r5] = r4     // Catch:{ IOException -> 0x0191 }
                int r5 = r5 + 1
                goto L_0x00e7
            L_0x00f4:
                r3.close()     // Catch:{ IOException -> 0x00f8 }
                goto L_0x00fc
            L_0x00f8:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x00fc:
                return r11
            L_0x00fd:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                byte[] r6 = androidx.exifinterface.media.ExifInterface.K     // Catch:{ IOException -> 0x0191 }
                int r6 = r6.length     // Catch:{ IOException -> 0x0191 }
                if (r11 < r6) goto L_0x011e
                r11 = 0
            L_0x0105:
                byte[] r6 = androidx.exifinterface.media.ExifInterface.K     // Catch:{ IOException -> 0x0191 }
                int r6 = r6.length     // Catch:{ IOException -> 0x0191 }
                if (r11 >= r6) goto L_0x0119
                byte[] r6 = r10.c     // Catch:{ IOException -> 0x0191 }
                byte r6 = r6[r11]     // Catch:{ IOException -> 0x0191 }
                byte[] r7 = androidx.exifinterface.media.ExifInterface.K     // Catch:{ IOException -> 0x0191 }
                byte r7 = r7[r11]     // Catch:{ IOException -> 0x0191 }
                if (r6 == r7) goto L_0x0116
                r4 = 0
                goto L_0x0119
            L_0x0116:
                int r11 = r11 + 1
                goto L_0x0105
            L_0x0119:
                if (r4 == 0) goto L_0x011e
                byte[] r11 = androidx.exifinterface.media.ExifInterface.K     // Catch:{ IOException -> 0x0191 }
                int r5 = r11.length     // Catch:{ IOException -> 0x0191 }
            L_0x011e:
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0191 }
                r11.<init>()     // Catch:{ IOException -> 0x0191 }
            L_0x0123:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x013f
                byte[] r4 = r10.c     // Catch:{ IOException -> 0x0191 }
                byte r4 = r4[r5]     // Catch:{ IOException -> 0x0191 }
                if (r4 != 0) goto L_0x012e
                goto L_0x013f
            L_0x012e:
                r6 = 32
                if (r4 < r6) goto L_0x0137
                char r4 = (char) r4     // Catch:{ IOException -> 0x0191 }
                r11.append(r4)     // Catch:{ IOException -> 0x0191 }
                goto L_0x013c
            L_0x0137:
                r4 = 63
                r11.append(r4)     // Catch:{ IOException -> 0x0191 }
            L_0x013c:
                int r5 = r5 + 1
                goto L_0x0123
            L_0x013f:
                java.lang.String r11 = r11.toString()     // Catch:{ IOException -> 0x0191 }
                r3.close()     // Catch:{ IOException -> 0x0147 }
                goto L_0x014b
            L_0x0147:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x014b:
                return r11
            L_0x014c:
                byte[] r11 = r10.c     // Catch:{ IOException -> 0x0191 }
                int r11 = r11.length     // Catch:{ IOException -> 0x0191 }
                if (r11 != r4) goto L_0x0176
                byte[] r11 = r10.c     // Catch:{ IOException -> 0x0191 }
                byte r11 = r11[r5]     // Catch:{ IOException -> 0x0191 }
                if (r11 < 0) goto L_0x0176
                byte[] r11 = r10.c     // Catch:{ IOException -> 0x0191 }
                byte r11 = r11[r5]     // Catch:{ IOException -> 0x0191 }
                if (r11 > r4) goto L_0x0176
                java.lang.String r11 = new java.lang.String     // Catch:{ IOException -> 0x0191 }
                char[] r4 = new char[r4]     // Catch:{ IOException -> 0x0191 }
                byte[] r6 = r10.c     // Catch:{ IOException -> 0x0191 }
                byte r6 = r6[r5]     // Catch:{ IOException -> 0x0191 }
                int r6 = r6 + 48
                char r6 = (char) r6     // Catch:{ IOException -> 0x0191 }
                r4[r5] = r6     // Catch:{ IOException -> 0x0191 }
                r11.<init>(r4)     // Catch:{ IOException -> 0x0191 }
                r3.close()     // Catch:{ IOException -> 0x0171 }
                goto L_0x0175
            L_0x0171:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x0175:
                return r11
            L_0x0176:
                java.lang.String r11 = new java.lang.String     // Catch:{ IOException -> 0x0191 }
                byte[] r4 = r10.c     // Catch:{ IOException -> 0x0191 }
                java.nio.charset.Charset r5 = androidx.exifinterface.media.ExifInterface.b0     // Catch:{ IOException -> 0x0191 }
                r11.<init>(r4, r5)     // Catch:{ IOException -> 0x0191 }
                r3.close()     // Catch:{ IOException -> 0x0183 }
                goto L_0x0187
            L_0x0183:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x0187:
                return r11
            L_0x0188:
                r3.close()     // Catch:{ IOException -> 0x018c }
                goto L_0x0190
            L_0x018c:
                r11 = move-exception
                android.util.Log.e(r1, r0, r11)
            L_0x0190:
                return r2
            L_0x0191:
                r11 = move-exception
                goto L_0x0198
            L_0x0193:
                r11 = move-exception
                r3 = r2
                goto L_0x01a9
            L_0x0196:
                r11 = move-exception
                r3 = r2
            L_0x0198:
                java.lang.String r4 = "IOException occurred during reading a value"
                android.util.Log.w(r1, r4, r11)     // Catch:{ all -> 0x01a8 }
                if (r3 == 0) goto L_0x01a7
                r3.close()     // Catch:{ IOException -> 0x01a3 }
                goto L_0x01a7
            L_0x01a3:
                r11 = move-exception
                android.util.Log.e(r1, r0, r11)
            L_0x01a7:
                return r2
            L_0x01a8:
                r11 = move-exception
            L_0x01a9:
                if (r3 == 0) goto L_0x01b3
                r3.close()     // Catch:{ IOException -> 0x01af }
                goto L_0x01b3
            L_0x01af:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x01b3:
                throw r11
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.exifinterface.media.ExifInterface.ExifAttribute.d(java.nio.ByteOrder):java.lang.Object");
        }

        public String toString() {
            return "(" + ExifInterface.I[this.f659a] + ", data length:" + this.c.length + ")";
        }

        ExifAttribute(int i, int i2, long j, byte[] bArr) {
            this.f659a = i;
            this.b = i2;
            this.c = bArr;
        }

        public static ExifAttribute a(int i, ByteOrder byteOrder) {
            return a(new int[]{i}, byteOrder);
        }

        public static ExifAttribute a(long[] jArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(ExifInterface.J[4] * jArr.length)]);
            wrap.order(byteOrder);
            for (long j : jArr) {
                wrap.putInt((int) j);
            }
            return new ExifAttribute(4, jArr.length, wrap.array());
        }

        public static ExifAttribute a(long j, ByteOrder byteOrder) {
            return a(new long[]{j}, byteOrder);
        }

        public static ExifAttribute a(String str) {
            byte[] bytes = (str + 0).getBytes(ExifInterface.b0);
            return new ExifAttribute(2, bytes.length, bytes);
        }

        public static ExifAttribute a(Rational[] rationalArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(ExifInterface.J[5] * rationalArr.length)]);
            wrap.order(byteOrder);
            for (Rational rational : rationalArr) {
                wrap.putInt((int) rational.f661a);
                wrap.putInt((int) rational.b);
            }
            return new ExifAttribute(5, rationalArr.length, wrap.array());
        }

        public static ExifAttribute a(Rational rational, ByteOrder byteOrder) {
            return a(new Rational[]{rational}, byteOrder);
        }

        public double a(ByteOrder byteOrder) {
            Object d = d(byteOrder);
            if (d == null) {
                throw new NumberFormatException("NULL can't be converted to a double value");
            } else if (d instanceof String) {
                return Double.parseDouble((String) d);
            } else {
                if (d instanceof long[]) {
                    long[] jArr = (long[]) d;
                    if (jArr.length == 1) {
                        return (double) jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof int[]) {
                    int[] iArr = (int[]) d;
                    if (iArr.length == 1) {
                        return (double) iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof double[]) {
                    double[] dArr = (double[]) d;
                    if (dArr.length == 1) {
                        return dArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof Rational[]) {
                    Rational[] rationalArr = (Rational[]) d;
                    if (rationalArr.length == 1) {
                        return rationalArr[0].a();
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a double value");
                }
            }
        }
    }

    private static class Rational {

        /* renamed from: a  reason: collision with root package name */
        public final long f661a;
        public final long b;

        Rational(long j, long j2) {
            if (j2 == 0) {
                this.f661a = 0;
                this.b = 1;
                return;
            }
            this.f661a = j;
            this.b = j2;
        }

        public double a() {
            return ((double) this.f661a) / ((double) this.b);
        }

        public String toString() {
            return this.f661a + "/" + this.b;
        }
    }

    static {
        Arrays.asList(new Integer[]{1, 6, 3, 8});
        Arrays.asList(new Integer[]{2, 7, 4, 5});
        new int[1][0] = 4;
        "VP8X".getBytes(Charset.defaultCharset());
        "VP8L".getBytes(Charset.defaultCharset());
        "VP8 ".getBytes(Charset.defaultCharset());
        "ANIM".getBytes(Charset.defaultCharset());
        "ANMF".getBytes(Charset.defaultCharset());
        "XMP ".getBytes(Charset.defaultCharset());
        ExifTag[] exifTagArr = L;
        V = new ExifTag[][]{exifTagArr, M, N, O, P, exifTagArr, R, S, T, U};
        new ExifTag("JPEGInterchangeFormat", 513, 4);
        new ExifTag("JPEGInterchangeFormatLength", 514, 4);
        ExifTag[][] exifTagArr2 = V;
        X = new HashMap[exifTagArr2.length];
        Y = new HashMap[exifTagArr2.length];
        H.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (int i2 = 0; i2 < V.length; i2++) {
            X[i2] = new HashMap<>();
            Y[i2] = new HashMap<>();
            for (ExifTag exifTag : V[i2]) {
                X[i2].put(Integer.valueOf(exifTag.f660a), exifTag);
                Y[i2].put(exifTag.b, exifTag);
            }
        }
        a0.put(Integer.valueOf(W[0].f660a), 5);
        a0.put(Integer.valueOf(W[1].f660a), 1);
        a0.put(Integer.valueOf(W[2].f660a), 2);
        a0.put(Integer.valueOf(W[3].f660a), 3);
        a0.put(Integer.valueOf(W[4].f660a), 7);
        a0.put(Integer.valueOf(W[5].f660a), 8);
        Pattern.compile(".*[1-9].*");
        Pattern.compile("^([0-9][0-9]):([0-9][0-9]):([0-9][0-9])$");
    }

    public ExifInterface(String str) throws IOException {
        ExifTag[][] exifTagArr = V;
        this.f = new HashMap[exifTagArr.length];
        this.g = new HashSet(exifTagArr.length);
        this.h = ByteOrder.BIG_ENDIAN;
        if (str != null) {
            c(str);
            return;
        }
        throw new NullPointerException("filename cannot be null");
    }

    private ExifAttribute b(String str) {
        if (str != null) {
            if ("ISOSpeedRatings".equals(str)) {
                if (r) {
                    Log.d("ExifInterface", "getExifAttribute: Replacing TAG_ISO_SPEED_RATINGS with TAG_PHOTOGRAPHIC_SENSITIVITY.");
                }
                str = "PhotographicSensitivity";
            }
            for (int i2 = 0; i2 < V.length; i2++) {
                ExifAttribute exifAttribute = this.f[i2].get(str);
                if (exifAttribute != null) {
                    return exifAttribute;
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    private void c(String str) throws IOException {
        FileInputStream fileInputStream;
        if (str != null) {
            this.c = null;
            this.f656a = str;
            try {
                fileInputStream = new FileInputStream(str);
                try {
                    if (a(fileInputStream.getFD())) {
                        this.b = fileInputStream.getFD();
                    } else {
                        this.b = null;
                    }
                    a((InputStream) fileInputStream);
                    a((Closeable) fileInputStream);
                } catch (Throwable th) {
                    th = th;
                    a((Closeable) fileInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = null;
                a((Closeable) fileInputStream);
                throw th;
            }
        } else {
            throw new NullPointerException("filename cannot be null");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean d(byte[] r4) throws java.io.IOException {
        /*
            r3 = this;
            r0 = 0
            r1 = 0
            androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream r2 = new androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream     // Catch:{ Exception -> 0x002f, all -> 0x0027 }
            r2.<init>((byte[]) r4)     // Catch:{ Exception -> 0x002f, all -> 0x0027 }
            java.nio.ByteOrder r4 = r3.i(r2)     // Catch:{ Exception -> 0x0025, all -> 0x0023 }
            r3.h = r4     // Catch:{ Exception -> 0x0025, all -> 0x0023 }
            java.nio.ByteOrder r4 = r3.h     // Catch:{ Exception -> 0x0025, all -> 0x0023 }
            r2.a(r4)     // Catch:{ Exception -> 0x0025, all -> 0x0023 }
            short r4 = r2.readShort()     // Catch:{ Exception -> 0x0025, all -> 0x0023 }
            r1 = 20306(0x4f52, float:2.8455E-41)
            if (r4 == r1) goto L_0x001e
            r1 = 21330(0x5352, float:2.989E-41)
            if (r4 != r1) goto L_0x001f
        L_0x001e:
            r0 = 1
        L_0x001f:
            r2.close()
            return r0
        L_0x0023:
            r4 = move-exception
            goto L_0x0029
        L_0x0025:
            goto L_0x0030
        L_0x0027:
            r4 = move-exception
            r2 = r1
        L_0x0029:
            if (r2 == 0) goto L_0x002e
            r2.close()
        L_0x002e:
            throw r4
        L_0x002f:
            r2 = r1
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()
        L_0x0035:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.exifinterface.media.ExifInterface.d(byte[]):boolean");
    }

    private boolean e(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = A;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    private boolean f(byte[] bArr) throws IOException {
        byte[] bytes = "FUJIFILMCCD-RAW".getBytes(Charset.defaultCharset());
        for (int i2 = 0; i2 < bytes.length; i2++) {
            if (bArr[i2] != bytes[i2]) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean g(byte[] r4) throws java.io.IOException {
        /*
            r3 = this;
            r0 = 0
            r1 = 0
            androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream r2 = new androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream     // Catch:{ Exception -> 0x002b, all -> 0x0023 }
            r2.<init>((byte[]) r4)     // Catch:{ Exception -> 0x002b, all -> 0x0023 }
            java.nio.ByteOrder r4 = r3.i(r2)     // Catch:{ Exception -> 0x0021, all -> 0x001f }
            r3.h = r4     // Catch:{ Exception -> 0x0021, all -> 0x001f }
            java.nio.ByteOrder r4 = r3.h     // Catch:{ Exception -> 0x0021, all -> 0x001f }
            r2.a(r4)     // Catch:{ Exception -> 0x0021, all -> 0x001f }
            short r4 = r2.readShort()     // Catch:{ Exception -> 0x0021, all -> 0x001f }
            r1 = 85
            if (r4 != r1) goto L_0x001b
            r0 = 1
        L_0x001b:
            r2.close()
            return r0
        L_0x001f:
            r4 = move-exception
            goto L_0x0025
        L_0x0021:
            goto L_0x002c
        L_0x0023:
            r4 = move-exception
            r2 = r1
        L_0x0025:
            if (r2 == 0) goto L_0x002a
            r2.close()
        L_0x002a:
            throw r4
        L_0x002b:
            r2 = r1
        L_0x002c:
            if (r2 == 0) goto L_0x0031
            r2.close()
        L_0x0031:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.exifinterface.media.ExifInterface.g(byte[]):boolean");
    }

    private boolean h(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = E;
            if (i2 >= bArr2.length) {
                int i3 = 0;
                while (true) {
                    byte[] bArr3 = F;
                    if (i3 >= bArr3.length) {
                        return true;
                    }
                    if (bArr[E.length + i3 + 4] != bArr3[i3]) {
                        return false;
                    }
                    i3++;
                }
            } else if (bArr[i2] != bArr2[i2]) {
                return false;
            } else {
                i2++;
            }
        }
    }

    private ByteOrder i(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        short readShort = byteOrderedDataInputStream.readShort();
        if (readShort == 18761) {
            if (r) {
                Log.d("ExifInterface", "readExifSegment: Byte Align II");
            }
            return ByteOrder.LITTLE_ENDIAN;
        } else if (readShort == 19789) {
            if (r) {
                Log.d("ExifInterface", "readExifSegment: Byte Align MM");
            }
            return ByteOrder.BIG_ENDIAN;
        } else {
            throw new IOException("Invalid byte order: " + Integer.toHexString(readShort));
        }
    }

    private void j(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        HashMap<String, ExifAttribute> hashMap = this.f[4];
        ExifAttribute exifAttribute = hashMap.get("Compression");
        if (exifAttribute != null) {
            this.l = exifAttribute.b(this.h);
            int i2 = this.l;
            if (i2 != 1) {
                if (i2 == 6) {
                    a(byteOrderedDataInputStream, (HashMap) hashMap);
                    return;
                } else if (i2 != 7) {
                    return;
                }
            }
            if (a((HashMap) hashMap)) {
                b(byteOrderedDataInputStream, (HashMap) hashMap);
                return;
            }
            return;
        }
        this.l = 6;
        a(byteOrderedDataInputStream, (HashMap) hashMap);
    }

    public String a(String str) {
        if (str != null) {
            ExifAttribute b2 = b(str);
            if (b2 != null) {
                if (!Z.contains(str)) {
                    return b2.c(this.h);
                }
                if (str.equals("GPSTimeStamp")) {
                    int i2 = b2.f659a;
                    if (i2 == 5 || i2 == 10) {
                        Rational[] rationalArr = (Rational[]) b2.d(this.h);
                        if (rationalArr == null || rationalArr.length != 3) {
                            Log.w("ExifInterface", "Invalid GPS Timestamp array. array=" + Arrays.toString(rationalArr));
                            return null;
                        }
                        return String.format("%02d:%02d:%02d", new Object[]{Integer.valueOf((int) (((float) rationalArr[0].f661a) / ((float) rationalArr[0].b))), Integer.valueOf((int) (((float) rationalArr[1].f661a) / ((float) rationalArr[1].b))), Integer.valueOf((int) (((float) rationalArr[2].f661a) / ((float) rationalArr[2].b)))});
                    }
                    Log.w("ExifInterface", "GPS Timestamp format is not rational. format=" + b2.f659a);
                    return null;
                }
                try {
                    return Double.toString(b2.a(this.h));
                } catch (NumberFormatException unused) {
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    private void e(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        ExifAttribute exifAttribute;
        a(byteOrderedDataInputStream, byteOrderedDataInputStream.available());
        b(byteOrderedDataInputStream, 0);
        d(byteOrderedDataInputStream, 0);
        d(byteOrderedDataInputStream, 5);
        d(byteOrderedDataInputStream, 4);
        c();
        if (this.d == 8 && (exifAttribute = this.f[1].get("MakerNote")) != null) {
            ByteOrderedDataInputStream byteOrderedDataInputStream2 = new ByteOrderedDataInputStream(exifAttribute.c);
            byteOrderedDataInputStream2.a(this.h);
            byteOrderedDataInputStream2.h(6);
            b(byteOrderedDataInputStream2, 9);
            ExifAttribute exifAttribute2 = this.f[9].get("ColorSpace");
            if (exifAttribute2 != null) {
                this.f[1].put("ColorSpace", exifAttribute2);
            }
        }
    }

    private void f(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        e(byteOrderedDataInputStream);
        if (this.f[0].get("JpgFromRaw") != null) {
            a(byteOrderedDataInputStream, this.q, 5);
        }
        ExifAttribute exifAttribute = this.f[0].get("ISO");
        ExifAttribute exifAttribute2 = this.f[1].get("PhotographicSensitivity");
        if (exifAttribute != null && exifAttribute2 == null) {
            this.f[1].put("PhotographicSensitivity", exifAttribute);
        }
    }

    static class ExifTag {

        /* renamed from: a  reason: collision with root package name */
        public final int f660a;
        public final String b;
        public final int c;
        public final int d;

        ExifTag(String str, int i, int i2) {
            this.b = str;
            this.f660a = i;
            this.c = i2;
            this.d = -1;
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i) {
            int i2;
            int i3 = this.c;
            if (i3 == 7 || i == 7 || i3 == i || (i2 = this.d) == i) {
                return true;
            }
            if ((i3 == 4 || i2 == 4) && i == 3) {
                return true;
            }
            if ((this.c == 9 || this.d == 9) && i == 8) {
                return true;
            }
            if ((this.c == 12 || this.d == 12) && i == 11) {
                return true;
            }
            return false;
        }

        ExifTag(String str, int i, int i2, int i3) {
            this.b = str;
            this.f660a = i;
            this.c = i2;
            this.d = i3;
        }
    }

    private void h(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        if (r) {
            Log.d("ExifInterface", "getWebpAttributes starting with: " + byteOrderedDataInputStream);
        }
        byteOrderedDataInputStream.a(ByteOrder.LITTLE_ENDIAN);
        byteOrderedDataInputStream.skipBytes(E.length);
        int readInt = byteOrderedDataInputStream.readInt() + 8;
        int skipBytes = byteOrderedDataInputStream.skipBytes(F.length) + 8;
        while (true) {
            try {
                byte[] bArr = new byte[4];
                if (byteOrderedDataInputStream.read(bArr) == bArr.length) {
                    int readInt2 = byteOrderedDataInputStream.readInt();
                    int i2 = skipBytes + 4 + 4;
                    if (Arrays.equals(G, bArr)) {
                        byte[] bArr2 = new byte[readInt2];
                        if (byteOrderedDataInputStream.read(bArr2) == readInt2) {
                            this.m = i2;
                            a(bArr2, 0);
                            this.m = i2;
                            return;
                        }
                        throw new IOException("Failed to read given length for given PNG chunk type: " + a(bArr));
                    }
                    if (readInt2 % 2 == 1) {
                        readInt2++;
                    }
                    int i3 = i2 + readInt2;
                    if (i3 != readInt) {
                        if (i3 <= readInt) {
                            int skipBytes2 = byteOrderedDataInputStream.skipBytes(readInt2);
                            if (skipBytes2 == readInt2) {
                                skipBytes = i2 + skipBytes2;
                            } else {
                                throw new IOException("Encountered WebP file with invalid chunk size");
                            }
                        } else {
                            throw new IOException("Encountered WebP file with invalid chunk size");
                        }
                    } else {
                        return;
                    }
                } else {
                    throw new IOException("Encountered invalid length while parsing WebP chunktype");
                }
            } catch (EOFException unused) {
                throw new IOException("Encountered corrupt WebP file.");
            }
        }
    }

    public ExifInterface(InputStream inputStream) throws IOException {
        this(inputStream, false);
    }

    private void b() {
        for (int i2 = 0; i2 < this.f.length; i2++) {
            Log.d("ExifInterface", "The size of tag group[" + i2 + "]: " + this.f[i2].size());
            for (Map.Entry next : this.f[i2].entrySet()) {
                ExifAttribute exifAttribute = (ExifAttribute) next.getValue();
                Log.d("ExifInterface", "tagName: " + ((String) next.getKey()) + ", tagType: " + exifAttribute.toString() + ", tagValue: '" + exifAttribute.c(this.h) + "'");
            }
        }
    }

    private ExifInterface(InputStream inputStream, boolean z2) throws IOException {
        ExifTag[][] exifTagArr = V;
        this.f = new HashMap[exifTagArr.length];
        this.g = new HashSet(exifTagArr.length);
        this.h = ByteOrder.BIG_ENDIAN;
        if (inputStream != null) {
            this.f656a = null;
            if (z2) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 5000);
                if (!b(bufferedInputStream)) {
                    Log.w("ExifInterface", "Given data does not follow the structure of an Exif-only data.");
                    return;
                }
                this.e = true;
                this.c = null;
                this.b = null;
                inputStream = bufferedInputStream;
            } else if (inputStream instanceof AssetManager.AssetInputStream) {
                this.c = (AssetManager.AssetInputStream) inputStream;
                this.b = null;
            } else {
                if (inputStream instanceof FileInputStream) {
                    FileInputStream fileInputStream = (FileInputStream) inputStream;
                    if (a(fileInputStream.getFD())) {
                        this.c = null;
                        this.b = fileInputStream.getFD();
                    }
                }
                this.c = null;
                this.b = null;
            }
            a(inputStream);
            return;
        }
        throw new NullPointerException("inputStream cannot be null");
    }

    private void d(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        byteOrderedDataInputStream.skipBytes(84);
        byte[] bArr = new byte[4];
        byte[] bArr2 = new byte[4];
        byteOrderedDataInputStream.read(bArr);
        byteOrderedDataInputStream.skipBytes(4);
        byteOrderedDataInputStream.read(bArr2);
        int i2 = ByteBuffer.wrap(bArr).getInt();
        int i3 = ByteBuffer.wrap(bArr2).getInt();
        a(byteOrderedDataInputStream, i2, 5);
        byteOrderedDataInputStream.h((long) i3);
        byteOrderedDataInputStream.a(ByteOrder.BIG_ENDIAN);
        int readInt = byteOrderedDataInputStream.readInt();
        if (r) {
            Log.d("ExifInterface", "numberOfDirectoryEntry: " + readInt);
        }
        for (int i4 = 0; i4 < readInt; i4++) {
            int readUnsignedShort = byteOrderedDataInputStream.readUnsignedShort();
            int readUnsignedShort2 = byteOrderedDataInputStream.readUnsignedShort();
            if (readUnsignedShort == Q.f660a) {
                short readShort = byteOrderedDataInputStream.readShort();
                short readShort2 = byteOrderedDataInputStream.readShort();
                ExifAttribute a2 = ExifAttribute.a((int) readShort, this.h);
                ExifAttribute a3 = ExifAttribute.a((int) readShort2, this.h);
                this.f[0].put("ImageLength", a2);
                this.f[0].put("ImageWidth", a3);
                if (r) {
                    Log.d("ExifInterface", "Updated to length: " + readShort + ", width: " + readShort2);
                    return;
                }
                return;
            }
            byteOrderedDataInputStream.skipBytes(readUnsignedShort2);
        }
    }

    private void g(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        byteOrderedDataInputStream.skipBytes(c0.length);
        byte[] bArr = new byte[byteOrderedDataInputStream.available()];
        byteOrderedDataInputStream.readFully(bArr);
        this.m = c0.length;
        a(bArr, 0);
    }

    private static boolean c(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = u;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    private void c(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        if (r) {
            Log.d("ExifInterface", "getPngAttributes starting with: " + byteOrderedDataInputStream);
        }
        byteOrderedDataInputStream.a(ByteOrder.BIG_ENDIAN);
        byteOrderedDataInputStream.skipBytes(A.length);
        int length = A.length + 0;
        while (true) {
            try {
                int readInt = byteOrderedDataInputStream.readInt();
                int i2 = length + 4;
                byte[] bArr = new byte[4];
                if (byteOrderedDataInputStream.read(bArr) == bArr.length) {
                    int i3 = i2 + 4;
                    if (i3 == 16) {
                        if (!Arrays.equals(bArr, C)) {
                            throw new IOException("Encountered invalid PNG file--IHDR chunk should appearas the first chunk");
                        }
                    }
                    if (!Arrays.equals(bArr, D)) {
                        if (Arrays.equals(bArr, B)) {
                            byte[] bArr2 = new byte[readInt];
                            if (byteOrderedDataInputStream.read(bArr2) == readInt) {
                                int readInt2 = byteOrderedDataInputStream.readInt();
                                CRC32 crc32 = new CRC32();
                                crc32.update(bArr);
                                crc32.update(bArr2);
                                if (((int) crc32.getValue()) == readInt2) {
                                    this.m = i3;
                                    a(bArr2, 0);
                                    c();
                                    return;
                                }
                                throw new IOException("Encountered invalid CRC value for PNG-EXIF chunk.\n recorded CRC value: " + readInt2 + ", calculated CRC value: " + crc32.getValue());
                            }
                            throw new IOException("Failed to read given length for given PNG chunk type: " + a(bArr));
                        }
                        int i4 = readInt + 4;
                        byteOrderedDataInputStream.skipBytes(i4);
                        length = i3 + i4;
                    } else {
                        return;
                    }
                } else {
                    throw new IOException("Encountered invalid length while parsing PNG chunktype");
                }
            } catch (EOFException unused) {
                throw new IOException("Encountered corrupt PNG file.");
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x0093 A[Catch:{ all -> 0x008b }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(byte[] r15) throws java.io.IOException {
        /*
            r14 = this;
            r0 = 0
            r1 = 0
            androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream r2 = new androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream     // Catch:{ Exception -> 0x008e }
            r2.<init>((byte[]) r15)     // Catch:{ Exception -> 0x008e }
            int r1 = r2.readInt()     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            long r3 = (long) r1     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            r1 = 4
            byte[] r5 = new byte[r1]     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            r2.read(r5)     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            byte[] r6 = v     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            boolean r5 = java.util.Arrays.equals(r5, r6)     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            if (r5 != 0) goto L_0x001e
            r2.close()
            return r0
        L_0x001e:
            r5 = 16
            r7 = 8
            r9 = 1
            int r11 = (r3 > r9 ? 1 : (r3 == r9 ? 0 : -1))
            if (r11 != 0) goto L_0x0034
            long r3 = r2.readLong()     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            int r11 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r11 >= 0) goto L_0x0035
            r2.close()
            return r0
        L_0x0034:
            r5 = r7
        L_0x0035:
            int r11 = r15.length     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            long r11 = (long) r11     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            int r13 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r13 <= 0) goto L_0x003d
            int r15 = r15.length     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            long r3 = (long) r15
        L_0x003d:
            long r3 = r3 - r5
            int r15 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r15 >= 0) goto L_0x0046
            r2.close()
            return r0
        L_0x0046:
            byte[] r15 = new byte[r1]     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            r5 = 0
            r1 = 0
            r7 = 0
        L_0x004c:
            r11 = 4
            long r11 = r3 / r11
            int r8 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r8 >= 0) goto L_0x0082
            int r8 = r2.read(r15)     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            int r11 = r15.length     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            if (r8 == r11) goto L_0x005f
            r2.close()
            return r0
        L_0x005f:
            int r8 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r8 != 0) goto L_0x0064
            goto L_0x0080
        L_0x0064:
            byte[] r8 = w     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            boolean r8 = java.util.Arrays.equals(r15, r8)     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            r11 = 1
            if (r8 == 0) goto L_0x006f
            r1 = 1
            goto L_0x0078
        L_0x006f:
            byte[] r8 = x     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            boolean r8 = java.util.Arrays.equals(r15, r8)     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            if (r8 == 0) goto L_0x0078
            r7 = 1
        L_0x0078:
            if (r1 == 0) goto L_0x0080
            if (r7 == 0) goto L_0x0080
            r2.close()
            return r11
        L_0x0080:
            long r5 = r5 + r9
            goto L_0x004c
        L_0x0082:
            r2.close()
            goto L_0x009f
        L_0x0086:
            r15 = move-exception
            goto L_0x00a0
        L_0x0088:
            r15 = move-exception
            r1 = r2
            goto L_0x008f
        L_0x008b:
            r15 = move-exception
            r2 = r1
            goto L_0x00a0
        L_0x008e:
            r15 = move-exception
        L_0x008f:
            boolean r2 = r     // Catch:{ all -> 0x008b }
            if (r2 == 0) goto L_0x009a
            java.lang.String r2 = "ExifInterface"
            java.lang.String r3 = "Exception parsing HEIF file type box."
            android.util.Log.d(r2, r3, r15)     // Catch:{ all -> 0x008b }
        L_0x009a:
            if (r1 == 0) goto L_0x009f
            r1.close()
        L_0x009f:
            return r0
        L_0x00a0:
            if (r2 == 0) goto L_0x00a5
            r2.close()
        L_0x00a5:
            throw r15
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.exifinterface.media.ExifInterface.b(byte[]):boolean");
    }

    public int a(String str, int i2) {
        if (str != null) {
            ExifAttribute b2 = b(str);
            if (b2 == null) {
                return i2;
            }
            try {
                return b2.b(this.h);
            } catch (NumberFormatException unused) {
                return i2;
            }
        } else {
            throw new NullPointerException("tag shouldn't be null");
        }
    }

    private void a(InputStream inputStream) {
        if (inputStream != null) {
            int i2 = 0;
            while (i2 < V.length) {
                try {
                    this.f[i2] = new HashMap<>();
                    i2++;
                } catch (IOException e2) {
                    if (r) {
                        Log.w("ExifInterface", "Invalid image: ExifInterface got an unsupported image format file(ExifInterface supports JPEG and some RAW image formats only) or a corrupted JPEG file to ExifInterface.", e2);
                    }
                    a();
                    if (!r) {
                        return;
                    }
                } catch (Throwable th) {
                    a();
                    if (r) {
                        b();
                    }
                    throw th;
                }
            }
            if (!this.e) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 5000);
                this.d = a(bufferedInputStream);
                inputStream = bufferedInputStream;
            }
            ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(inputStream);
            if (!this.e) {
                switch (this.d) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 5:
                    case 6:
                    case 8:
                    case 11:
                        e(byteOrderedDataInputStream);
                        break;
                    case 4:
                        a(byteOrderedDataInputStream, 0, 0);
                        break;
                    case 7:
                        b(byteOrderedDataInputStream);
                        break;
                    case 9:
                        d(byteOrderedDataInputStream);
                        break;
                    case 10:
                        f(byteOrderedDataInputStream);
                        break;
                    case 12:
                        a(byteOrderedDataInputStream);
                        break;
                    case 13:
                        c(byteOrderedDataInputStream);
                        break;
                    case 14:
                        h(byteOrderedDataInputStream);
                        break;
                }
            } else {
                g(byteOrderedDataInputStream);
            }
            j(byteOrderedDataInputStream);
            a();
            if (!r) {
                return;
            }
            b();
            return;
        }
        throw new NullPointerException("inputstream shouldn't be null");
    }

    private static boolean b(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(c0.length);
        byte[] bArr = new byte[c0.length];
        bufferedInputStream.read(bArr);
        bufferedInputStream.reset();
        int i2 = 0;
        while (true) {
            byte[] bArr2 = c0;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    private void d(ByteOrderedDataInputStream byteOrderedDataInputStream, int i2) throws IOException {
        ExifAttribute exifAttribute;
        ExifAttribute exifAttribute2;
        ExifAttribute exifAttribute3 = this.f[i2].get("DefaultCropSize");
        ExifAttribute exifAttribute4 = this.f[i2].get("SensorTopBorder");
        ExifAttribute exifAttribute5 = this.f[i2].get("SensorLeftBorder");
        ExifAttribute exifAttribute6 = this.f[i2].get("SensorBottomBorder");
        ExifAttribute exifAttribute7 = this.f[i2].get("SensorRightBorder");
        if (exifAttribute3 != null) {
            if (exifAttribute3.f659a == 5) {
                Rational[] rationalArr = (Rational[]) exifAttribute3.d(this.h);
                if (rationalArr == null || rationalArr.length != 2) {
                    Log.w("ExifInterface", "Invalid crop size values. cropSize=" + Arrays.toString(rationalArr));
                    return;
                }
                exifAttribute2 = ExifAttribute.a(rationalArr[0], this.h);
                exifAttribute = ExifAttribute.a(rationalArr[1], this.h);
            } else {
                int[] iArr = (int[]) exifAttribute3.d(this.h);
                if (iArr == null || iArr.length != 2) {
                    Log.w("ExifInterface", "Invalid crop size values. cropSize=" + Arrays.toString(iArr));
                    return;
                }
                exifAttribute2 = ExifAttribute.a(iArr[0], this.h);
                exifAttribute = ExifAttribute.a(iArr[1], this.h);
            }
            this.f[i2].put("ImageWidth", exifAttribute2);
            this.f[i2].put("ImageLength", exifAttribute);
        } else if (exifAttribute4 == null || exifAttribute5 == null || exifAttribute6 == null || exifAttribute7 == null) {
            c(byteOrderedDataInputStream, i2);
        } else {
            int b2 = exifAttribute4.b(this.h);
            int b3 = exifAttribute6.b(this.h);
            int b4 = exifAttribute7.b(this.h);
            int b5 = exifAttribute5.b(this.h);
            if (b3 > b2 && b4 > b5) {
                ExifAttribute a2 = ExifAttribute.a(b3 - b2, this.h);
                ExifAttribute a3 = ExifAttribute.a(b4 - b5, this.h);
                this.f[i2].put("ImageLength", a2);
                this.f[i2].put("ImageWidth", a3);
            }
        }
    }

    private void b(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        e(byteOrderedDataInputStream);
        ExifAttribute exifAttribute = this.f[1].get("MakerNote");
        if (exifAttribute != null) {
            ByteOrderedDataInputStream byteOrderedDataInputStream2 = new ByteOrderedDataInputStream(exifAttribute.c);
            byteOrderedDataInputStream2.a(this.h);
            byte[] bArr = new byte[y.length];
            byteOrderedDataInputStream2.readFully(bArr);
            byteOrderedDataInputStream2.h(0);
            byte[] bArr2 = new byte[z.length];
            byteOrderedDataInputStream2.readFully(bArr2);
            if (Arrays.equals(bArr, y)) {
                byteOrderedDataInputStream2.h(8);
            } else if (Arrays.equals(bArr2, z)) {
                byteOrderedDataInputStream2.h(12);
            }
            b(byteOrderedDataInputStream2, 6);
            ExifAttribute exifAttribute2 = this.f[7].get("PreviewImageStart");
            ExifAttribute exifAttribute3 = this.f[7].get("PreviewImageLength");
            if (!(exifAttribute2 == null || exifAttribute3 == null)) {
                this.f[5].put("JPEGInterchangeFormat", exifAttribute2);
                this.f[5].put("JPEGInterchangeFormatLength", exifAttribute3);
            }
            ExifAttribute exifAttribute4 = this.f[8].get("AspectFrame");
            if (exifAttribute4 != null) {
                int[] iArr = (int[]) exifAttribute4.d(this.h);
                if (iArr == null || iArr.length != 4) {
                    Log.w("ExifInterface", "Invalid aspect frame values. frame=" + Arrays.toString(iArr));
                } else if (iArr[2] > iArr[0] && iArr[3] > iArr[1]) {
                    int i2 = (iArr[2] - iArr[0]) + 1;
                    int i3 = (iArr[3] - iArr[1]) + 1;
                    if (i2 < i3) {
                        int i4 = i2 + i3;
                        i3 = i4 - i3;
                        i2 = i4 - i3;
                    }
                    ExifAttribute a2 = ExifAttribute.a(i2, this.h);
                    ExifAttribute a3 = ExifAttribute.a(i3, this.h);
                    this.f[0].put("ImageWidth", a2);
                    this.f[0].put("ImageLength", a3);
                }
            }
        }
    }

    private void c(ByteOrderedDataInputStream byteOrderedDataInputStream, int i2) throws IOException {
        ExifAttribute exifAttribute;
        ExifAttribute exifAttribute2 = this.f[i2].get("ImageLength");
        ExifAttribute exifAttribute3 = this.f[i2].get("ImageWidth");
        if ((exifAttribute2 == null || exifAttribute3 == null) && (exifAttribute = this.f[i2].get("JPEGInterchangeFormat")) != null) {
            a(byteOrderedDataInputStream, exifAttribute.b(this.h), i2);
        }
    }

    private static boolean a(FileDescriptor fileDescriptor) {
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                Os.lseek(fileDescriptor, 0, OsConstants.SEEK_CUR);
                return true;
            } catch (Exception unused) {
                if (r) {
                    Log.d("ExifInterface", "The file descriptor for the given input is not seekable");
                }
            }
        }
        return false;
    }

    private void c() throws IOException {
        a(0, 5);
        a(0, 4);
        a(5, 4);
        ExifAttribute exifAttribute = this.f[1].get("PixelXDimension");
        ExifAttribute exifAttribute2 = this.f[1].get("PixelYDimension");
        if (!(exifAttribute == null || exifAttribute2 == null)) {
            this.f[0].put("ImageWidth", exifAttribute);
            this.f[0].put("ImageLength", exifAttribute2);
        }
        if (this.f[4].isEmpty() && b((HashMap) this.f[5])) {
            HashMap<String, ExifAttribute>[] hashMapArr = this.f;
            hashMapArr[4] = hashMapArr[5];
            hashMapArr[5] = new HashMap<>();
        }
        if (!b((HashMap) this.f[4])) {
            Log.d("ExifInterface", "No image meets the size requirements of a thumbnail image.");
        }
    }

    private int a(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(5000);
        byte[] bArr = new byte[5000];
        bufferedInputStream.read(bArr);
        bufferedInputStream.reset();
        if (c(bArr)) {
            return 4;
        }
        if (f(bArr)) {
            return 9;
        }
        if (b(bArr)) {
            return 12;
        }
        if (d(bArr)) {
            return 7;
        }
        if (g(bArr)) {
            return 10;
        }
        if (e(bArr)) {
            return 13;
        }
        return h(bArr) ? 14 : 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00f4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x018d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(androidx.exifinterface.media.ExifInterface.ByteOrderedDataInputStream r20, int r21, int r22) throws java.io.IOException {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r22
            boolean r4 = r
            java.lang.String r5 = "ExifInterface"
            if (r4 == 0) goto L_0x0022
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "getJpegAttributes starting with: "
            r4.append(r6)
            r4.append(r1)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r5, r4)
        L_0x0022:
            java.nio.ByteOrder r4 = java.nio.ByteOrder.BIG_ENDIAN
            r1.a(r4)
            long r6 = (long) r2
            r1.h(r6)
            byte r4 = r20.readByte()
            java.lang.String r6 = "Invalid marker: "
            r7 = -1
            if (r4 != r7) goto L_0x01d7
            r8 = 1
            int r2 = r2 + r8
            byte r9 = r20.readByte()
            r10 = -40
            if (r9 != r10) goto L_0x01bc
            int r2 = r2 + r8
        L_0x003f:
            byte r4 = r20.readByte()
            if (r4 != r7) goto L_0x019f
            int r2 = r2 + r8
            byte r4 = r20.readByte()
            boolean r6 = r
            if (r6 == 0) goto L_0x0068
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r9 = "Found JPEG segment indicator: "
            r6.append(r9)
            r9 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r9 = java.lang.Integer.toHexString(r9)
            r6.append(r9)
            java.lang.String r6 = r6.toString()
            android.util.Log.d(r5, r6)
        L_0x0068:
            int r2 = r2 + r8
            r6 = -39
            if (r4 == r6) goto L_0x0199
            r6 = -38
            if (r4 != r6) goto L_0x0073
            goto L_0x0199
        L_0x0073:
            int r6 = r20.readUnsignedShort()
            int r6 = r6 + -2
            int r2 = r2 + 2
            boolean r9 = r
            if (r9 == 0) goto L_0x00a8
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "JPEG segment: "
            r9.append(r10)
            r10 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r10 = java.lang.Integer.toHexString(r10)
            r9.append(r10)
            java.lang.String r10 = " (length: "
            r9.append(r10)
            int r10 = r6 + 2
            r9.append(r10)
            java.lang.String r10 = ")"
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            android.util.Log.d(r5, r9)
        L_0x00a8:
            java.lang.String r9 = "Invalid length"
            if (r6 < 0) goto L_0x0193
            r10 = -31
            r11 = 0
            if (r4 == r10) goto L_0x0127
            r10 = -2
            if (r4 == r10) goto L_0x00fc
            switch(r4) {
                case -64: goto L_0x00c2;
                case -63: goto L_0x00c2;
                case -62: goto L_0x00c2;
                case -61: goto L_0x00c2;
                default: goto L_0x00b7;
            }
        L_0x00b7:
            switch(r4) {
                case -59: goto L_0x00c2;
                case -58: goto L_0x00c2;
                case -57: goto L_0x00c2;
                default: goto L_0x00ba;
            }
        L_0x00ba:
            switch(r4) {
                case -55: goto L_0x00c2;
                case -54: goto L_0x00c2;
                case -53: goto L_0x00c2;
                default: goto L_0x00bd;
            }
        L_0x00bd:
            switch(r4) {
                case -51: goto L_0x00c2;
                case -50: goto L_0x00c2;
                case -49: goto L_0x00c2;
                default: goto L_0x00c0;
            }
        L_0x00c0:
            goto L_0x0178
        L_0x00c2:
            int r4 = r1.skipBytes(r8)
            if (r4 != r8) goto L_0x00f4
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r4 = r0.f
            r4 = r4[r3]
            int r10 = r20.readUnsignedShort()
            long r10 = (long) r10
            java.nio.ByteOrder r12 = r0.h
            androidx.exifinterface.media.ExifInterface$ExifAttribute r10 = androidx.exifinterface.media.ExifInterface.ExifAttribute.a((long) r10, (java.nio.ByteOrder) r12)
            java.lang.String r11 = "ImageLength"
            r4.put(r11, r10)
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r4 = r0.f
            r4 = r4[r3]
            int r10 = r20.readUnsignedShort()
            long r10 = (long) r10
            java.nio.ByteOrder r12 = r0.h
            androidx.exifinterface.media.ExifInterface$ExifAttribute r10 = androidx.exifinterface.media.ExifInterface.ExifAttribute.a((long) r10, (java.nio.ByteOrder) r12)
            java.lang.String r11 = "ImageWidth"
            r4.put(r11, r10)
            int r6 = r6 + -5
            goto L_0x0178
        L_0x00f4:
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r2 = "Invalid SOFx"
            r1.<init>(r2)
            throw r1
        L_0x00fc:
            byte[] r4 = new byte[r6]
            int r10 = r1.read(r4)
            if (r10 != r6) goto L_0x011f
            java.lang.String r6 = "UserComment"
            java.lang.String r10 = r0.a((java.lang.String) r6)
            if (r10 != 0) goto L_0x0177
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r10 = r0.f
            r10 = r10[r8]
            java.lang.String r12 = new java.lang.String
            java.nio.charset.Charset r13 = b0
            r12.<init>(r4, r13)
            androidx.exifinterface.media.ExifInterface$ExifAttribute r4 = androidx.exifinterface.media.ExifInterface.ExifAttribute.a((java.lang.String) r12)
            r10.put(r6, r4)
            goto L_0x0177
        L_0x011f:
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r2 = "Invalid exif"
            r1.<init>(r2)
            throw r1
        L_0x0127:
            byte[] r4 = new byte[r6]
            r1.readFully(r4)
            int r6 = r6 + r2
            byte[] r10 = c0
            boolean r10 = a((byte[]) r4, (byte[]) r10)
            if (r10 == 0) goto L_0x0145
            byte[] r10 = c0
            int r12 = r10.length
            int r2 = r2 + r12
            int r10 = r10.length
            int r12 = r4.length
            byte[] r4 = java.util.Arrays.copyOfRange(r4, r10, r12)
            r0.m = r2
            r0.a((byte[]) r4, (int) r3)
            goto L_0x0176
        L_0x0145:
            byte[] r10 = d0
            boolean r10 = a((byte[]) r4, (byte[]) r10)
            if (r10 == 0) goto L_0x0176
            byte[] r10 = d0
            int r12 = r10.length
            int r2 = r2 + r12
            int r10 = r10.length
            int r12 = r4.length
            byte[] r4 = java.util.Arrays.copyOfRange(r4, r10, r12)
            java.lang.String r10 = "Xmp"
            java.lang.String r12 = r0.a((java.lang.String) r10)
            if (r12 != 0) goto L_0x0176
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r12 = r0.f
            r12 = r12[r11]
            androidx.exifinterface.media.ExifInterface$ExifAttribute r15 = new androidx.exifinterface.media.ExifInterface$ExifAttribute
            r14 = 1
            int r13 = r4.length
            long r7 = (long) r2
            r2 = r13
            r13 = r15
            r11 = r15
            r15 = r2
            r16 = r7
            r18 = r4
            r13.<init>(r14, r15, r16, r18)
            r12.put(r10, r11)
        L_0x0176:
            r2 = r6
        L_0x0177:
            r6 = 0
        L_0x0178:
            if (r6 < 0) goto L_0x018d
            int r4 = r1.skipBytes(r6)
            if (r4 != r6) goto L_0x0185
            int r2 = r2 + r6
            r7 = -1
            r8 = 1
            goto L_0x003f
        L_0x0185:
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r2 = "Invalid JPEG segment"
            r1.<init>(r2)
            throw r1
        L_0x018d:
            java.io.IOException r1 = new java.io.IOException
            r1.<init>(r9)
            throw r1
        L_0x0193:
            java.io.IOException r1 = new java.io.IOException
            r1.<init>(r9)
            throw r1
        L_0x0199:
            java.nio.ByteOrder r2 = r0.h
            r1.a(r2)
            return
        L_0x019f:
            java.io.IOException r1 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid marker:"
            r2.append(r3)
            r3 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r3 = java.lang.Integer.toHexString(r3)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x01bc:
            java.io.IOException r1 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r6)
            r3 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r3 = java.lang.Integer.toHexString(r3)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x01d7:
            java.io.IOException r1 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r6)
            r3 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r3 = java.lang.Integer.toHexString(r3)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.exifinterface.media.ExifInterface.a(androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream, int, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:102:0x0294  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x02ba  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02f8  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x015b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(androidx.exifinterface.media.ExifInterface.ByteOrderedDataInputStream r28, int r29) throws java.io.IOException {
        /*
            r27 = this;
            r0 = r27
            r1 = r28
            r2 = r29
            java.util.Set<java.lang.Integer> r3 = r0.g
            int r4 = r1.d
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r3.add(r4)
            int r3 = r1.d
            r4 = 2
            int r3 = r3 + r4
            int r5 = r1.c
            if (r3 <= r5) goto L_0x001a
            return
        L_0x001a:
            short r3 = r28.readShort()
            boolean r5 = r
            java.lang.String r6 = "ExifInterface"
            if (r5 == 0) goto L_0x0038
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "numberOfDirectoryEntry: "
            r5.append(r7)
            r5.append(r3)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r6, r5)
        L_0x0038:
            int r5 = r1.d
            int r7 = r3 * 12
            int r5 = r5 + r7
            int r7 = r1.c
            if (r5 > r7) goto L_0x0433
            if (r3 > 0) goto L_0x0045
            goto L_0x0433
        L_0x0045:
            r5 = 0
            r7 = 0
        L_0x0047:
            r8 = 5
            if (r7 >= r3) goto L_0x03a2
            int r13 = r28.readUnsignedShort()
            int r14 = r28.readUnsignedShort()
            int r15 = r28.readInt()
            int r9 = r28.peek()
            long r9 = (long) r9
            r18 = 4
            long r9 = r9 + r18
            java.util.HashMap<java.lang.Integer, androidx.exifinterface.media.ExifInterface$ExifTag>[] r20 = X
            r12 = r20[r2]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r13)
            java.lang.Object r4 = r12.get(r4)
            androidx.exifinterface.media.ExifInterface$ExifTag r4 = (androidx.exifinterface.media.ExifInterface.ExifTag) r4
            boolean r12 = r
            r11 = 3
            if (r12 == 0) goto L_0x00a3
            java.lang.Object[] r8 = new java.lang.Object[r8]
            java.lang.Integer r12 = java.lang.Integer.valueOf(r29)
            r8[r5] = r12
            java.lang.Integer r12 = java.lang.Integer.valueOf(r13)
            r20 = 1
            r8[r20] = r12
            if (r4 == 0) goto L_0x0087
            java.lang.String r12 = r4.b
            goto L_0x0088
        L_0x0087:
            r12 = 0
        L_0x0088:
            r22 = 2
            r8[r22] = r12
            java.lang.Integer r12 = java.lang.Integer.valueOf(r14)
            r8[r11] = r12
            java.lang.Integer r12 = java.lang.Integer.valueOf(r15)
            r21 = 4
            r8[r21] = r12
            java.lang.String r12 = "ifdType: %d, tagNumber: %d, tagName: %s, dataFormat: %d, numberOfComponents: %d"
            java.lang.String r8 = java.lang.String.format(r12, r8)
            android.util.Log.d(r6, r8)
        L_0x00a3:
            r8 = 7
            if (r4 != 0) goto L_0x00c2
            boolean r12 = r
            if (r12 == 0) goto L_0x00be
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r5 = "Skip the tag entry since tag number is not defined: "
            r12.append(r5)
            r12.append(r13)
            java.lang.String r5 = r12.toString()
            android.util.Log.d(r6, r5)
        L_0x00be:
            r24 = r9
            goto L_0x0149
        L_0x00c2:
            if (r14 <= 0) goto L_0x012f
            int[] r5 = J
            int r5 = r5.length
            if (r14 < r5) goto L_0x00ca
            goto L_0x012f
        L_0x00ca:
            boolean r5 = r4.a(r14)
            if (r5 != 0) goto L_0x00f7
            boolean r5 = r
            if (r5 == 0) goto L_0x00be
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r12 = "Skip the tag entry since data format ("
            r5.append(r12)
            java.lang.String[] r12 = I
            r12 = r12[r14]
            r5.append(r12)
            java.lang.String r12 = ") is unexpected for tag: "
            r5.append(r12)
            java.lang.String r12 = r4.b
            r5.append(r12)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r6, r5)
            goto L_0x00be
        L_0x00f7:
            if (r14 != r8) goto L_0x00fb
            int r14 = r4.c
        L_0x00fb:
            long r11 = (long) r15
            int[] r23 = J
            r5 = r23[r14]
            r24 = r9
            long r8 = (long) r5
            long r8 = r8 * r11
            r11 = 0
            int r5 = (r8 > r11 ? 1 : (r8 == r11 ? 0 : -1))
            if (r5 < 0) goto L_0x0115
            r11 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r5 = (r8 > r11 ? 1 : (r8 == r11 ? 0 : -1))
            if (r5 <= 0) goto L_0x0113
            goto L_0x0115
        L_0x0113:
            r5 = 1
            goto L_0x014c
        L_0x0115:
            boolean r5 = r
            if (r5 == 0) goto L_0x012d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r11 = "Skip the tag entry since the number of components is invalid: "
            r5.append(r11)
            r5.append(r15)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r6, r5)
        L_0x012d:
            r5 = 0
            goto L_0x014c
        L_0x012f:
            r24 = r9
            boolean r5 = r
            if (r5 == 0) goto L_0x0149
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r8 = "Skip the tag entry since data format is invalid: "
            r5.append(r8)
            r5.append(r14)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r6, r5)
        L_0x0149:
            r5 = 0
            r8 = 0
        L_0x014c:
            if (r5 != 0) goto L_0x015b
            r11 = r24
            r1.h(r11)
            r23 = r3
            r13 = r6
            r24 = r7
        L_0x0158:
            r7 = 2
            goto L_0x0395
        L_0x015b:
            r11 = r24
            java.lang.String r5 = "Compression"
            int r23 = (r8 > r18 ? 1 : (r8 == r18 ? 0 : -1))
            if (r23 <= 0) goto L_0x022a
            int r10 = r28.readInt()
            boolean r19 = r
            r23 = r3
            if (r19 == 0) goto L_0x0184
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r24 = r7
            java.lang.String r7 = "seek to data offset: "
            r3.append(r7)
            r3.append(r10)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r6, r3)
            goto L_0x0186
        L_0x0184:
            r24 = r7
        L_0x0186:
            int r3 = r0.d
            r7 = 7
            if (r3 != r7) goto L_0x01e6
            java.lang.String r3 = r4.b
            java.lang.String r7 = "MakerNote"
            boolean r3 = r7.equals(r3)
            if (r3 == 0) goto L_0x0198
            r0.n = r10
            goto L_0x01e1
        L_0x0198:
            r3 = 6
            if (r2 != r3) goto L_0x01e1
            java.lang.String r7 = r4.b
            java.lang.String r3 = "ThumbnailImage"
            boolean r3 = r3.equals(r7)
            if (r3 == 0) goto L_0x01e1
            r0.o = r10
            r0.p = r15
            java.nio.ByteOrder r3 = r0.h
            r7 = 6
            androidx.exifinterface.media.ExifInterface$ExifAttribute r3 = androidx.exifinterface.media.ExifInterface.ExifAttribute.a((int) r7, (java.nio.ByteOrder) r3)
            int r7 = r0.o
            r19 = r14
            r18 = r15
            long r14 = (long) r7
            java.nio.ByteOrder r7 = r0.h
            androidx.exifinterface.media.ExifInterface$ExifAttribute r7 = androidx.exifinterface.media.ExifInterface.ExifAttribute.a((long) r14, (java.nio.ByteOrder) r7)
            int r14 = r0.p
            long r14 = (long) r14
            java.nio.ByteOrder r2 = r0.h
            androidx.exifinterface.media.ExifInterface$ExifAttribute r2 = androidx.exifinterface.media.ExifInterface.ExifAttribute.a((long) r14, (java.nio.ByteOrder) r2)
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r14 = r0.f
            r15 = 4
            r14 = r14[r15]
            r14.put(r5, r3)
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r3 = r0.f
            r3 = r3[r15]
            java.lang.String r14 = "JPEGInterchangeFormat"
            r3.put(r14, r7)
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r3 = r0.f
            r3 = r3[r15]
            java.lang.String r7 = "JPEGInterchangeFormatLength"
            r3.put(r7, r2)
            goto L_0x01fa
        L_0x01e1:
            r19 = r14
            r18 = r15
            goto L_0x01fa
        L_0x01e6:
            r19 = r14
            r18 = r15
            r2 = 10
            if (r3 != r2) goto L_0x01fa
            java.lang.String r2 = r4.b
            java.lang.String r3 = "JpgFromRaw"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x01fa
            r0.q = r10
        L_0x01fa:
            long r2 = (long) r10
            long r14 = r2 + r8
            int r7 = r1.c
            r25 = r4
            r26 = r5
            long r4 = (long) r7
            int r7 = (r14 > r4 ? 1 : (r14 == r4 ? 0 : -1))
            if (r7 > 0) goto L_0x020c
            r1.h(r2)
            goto L_0x0236
        L_0x020c:
            boolean r2 = r
            if (r2 == 0) goto L_0x0224
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Skip the tag entry since data offset is invalid: "
            r2.append(r3)
            r2.append(r10)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r6, r2)
        L_0x0224:
            r1.h(r11)
            r13 = r6
            goto L_0x0158
        L_0x022a:
            r23 = r3
            r25 = r4
            r26 = r5
            r24 = r7
            r19 = r14
            r18 = r15
        L_0x0236:
            java.util.HashMap<java.lang.Integer, java.lang.Integer> r2 = a0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r13)
            java.lang.Object r2 = r2.get(r3)
            java.lang.Integer r2 = (java.lang.Integer) r2
            boolean r3 = r
            if (r3 == 0) goto L_0x0262
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "nextIfdType: "
            r3.append(r4)
            r3.append(r2)
            java.lang.String r4 = " byteCount: "
            r3.append(r4)
            r3.append(r8)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r6, r3)
        L_0x0262:
            r3 = 8
            if (r2 == 0) goto L_0x0316
            r4 = -1
            r14 = r19
            r7 = 3
            if (r14 == r7) goto L_0x028a
            r7 = 4
            if (r14 == r7) goto L_0x0285
            if (r14 == r3) goto L_0x0280
            r3 = 9
            if (r14 == r3) goto L_0x027b
            r3 = 13
            if (r14 == r3) goto L_0x027b
            goto L_0x028f
        L_0x027b:
            int r3 = r28.readInt()
            goto L_0x028e
        L_0x0280:
            short r3 = r28.readShort()
            goto L_0x028e
        L_0x0285:
            long r4 = r28.t()
            goto L_0x028f
        L_0x028a:
            int r3 = r28.readUnsignedShort()
        L_0x028e:
            long r4 = (long) r3
        L_0x028f:
            boolean r3 = r
            r7 = 2
            if (r3 == 0) goto L_0x02ad
            java.lang.Object[] r3 = new java.lang.Object[r7]
            java.lang.Long r8 = java.lang.Long.valueOf(r4)
            r9 = 0
            r3[r9] = r8
            r10 = r25
            java.lang.String r8 = r10.b
            r9 = 1
            r3[r9] = r8
            java.lang.String r8 = "Offset: %d, tagName: %s"
            java.lang.String r3 = java.lang.String.format(r8, r3)
            android.util.Log.d(r6, r3)
        L_0x02ad:
            r8 = 0
            int r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r3 <= 0) goto L_0x02f8
            int r3 = r1.c
            long r8 = (long) r3
            int r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x02f8
            java.util.Set<java.lang.Integer> r3 = r0.g
            int r8 = (int) r4
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            boolean r3 = r3.contains(r8)
            if (r3 != 0) goto L_0x02d2
            r1.h(r4)
            int r2 = r2.intValue()
            r0.b((androidx.exifinterface.media.ExifInterface.ByteOrderedDataInputStream) r1, (int) r2)
            goto L_0x0310
        L_0x02d2:
            boolean r3 = r
            if (r3 == 0) goto L_0x0310
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r8 = "Skip jump into the IFD since it has already been read: IfdType "
            r3.append(r8)
            r3.append(r2)
            java.lang.String r2 = " (at "
            r3.append(r2)
            r3.append(r4)
            java.lang.String r2 = ")"
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            android.util.Log.d(r6, r2)
            goto L_0x0310
        L_0x02f8:
            boolean r2 = r
            if (r2 == 0) goto L_0x0310
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Skip jump into the IFD since its offset is invalid: "
            r2.append(r3)
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r6, r2)
        L_0x0310:
            r1.h(r11)
            r13 = r6
            goto L_0x0395
        L_0x0316:
            r14 = r19
            r10 = r25
            r2 = r26
            r7 = 2
            int r4 = r28.peek()
            int r13 = r0.m
            int r4 = r4 + r13
            int r9 = (int) r8
            byte[] r8 = new byte[r9]
            r1.readFully(r8)
            androidx.exifinterface.media.ExifInterface$ExifAttribute r9 = new androidx.exifinterface.media.ExifInterface$ExifAttribute
            r13 = r6
            long r5 = (long) r4
            r4 = r18
            r15 = r9
            r16 = r14
            r17 = r4
            r18 = r5
            r20 = r8
            r15.<init>(r16, r17, r18, r20)
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r4 = r0.f
            r4 = r4[r29]
            java.lang.String r5 = r10.b
            r4.put(r5, r9)
            java.lang.String r4 = r10.b
            java.lang.String r5 = "DNGVersion"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x0352
            r4 = 3
            r0.d = r4
        L_0x0352:
            java.lang.String r4 = r10.b
            java.lang.String r5 = "Make"
            boolean r4 = r5.equals(r4)
            if (r4 != 0) goto L_0x0366
            java.lang.String r4 = r10.b
            java.lang.String r5 = "Model"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x0374
        L_0x0366:
            java.nio.ByteOrder r4 = r0.h
            java.lang.String r4 = r9.c(r4)
            java.lang.String r5 = "PENTAX"
            boolean r4 = r4.contains(r5)
            if (r4 != 0) goto L_0x0387
        L_0x0374:
            java.lang.String r4 = r10.b
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0389
            java.nio.ByteOrder r2 = r0.h
            int r2 = r9.b(r2)
            r4 = 65535(0xffff, float:9.1834E-41)
            if (r2 != r4) goto L_0x0389
        L_0x0387:
            r0.d = r3
        L_0x0389:
            int r2 = r28.peek()
            long r2 = (long) r2
            int r4 = (r2 > r11 ? 1 : (r2 == r11 ? 0 : -1))
            if (r4 == 0) goto L_0x0395
            r1.h(r11)
        L_0x0395:
            int r2 = r24 + 1
            short r2 = (short) r2
            r7 = r2
            r6 = r13
            r3 = r23
            r4 = 2
            r5 = 0
            r2 = r29
            goto L_0x0047
        L_0x03a2:
            r13 = r6
            int r2 = r28.peek()
            r3 = 4
            int r2 = r2 + r3
            int r3 = r1.c
            if (r2 > r3) goto L_0x0433
            int r2 = r28.readInt()
            boolean r3 = r
            if (r3 == 0) goto L_0x03ca
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            r5 = 0
            r3[r5] = r4
            java.lang.String r4 = "nextIfdOffset: %d"
            java.lang.String r3 = java.lang.String.format(r4, r3)
            r4 = r13
            android.util.Log.d(r4, r3)
            goto L_0x03cb
        L_0x03ca:
            r4 = r13
        L_0x03cb:
            long r5 = (long) r2
            r9 = 0
            int r3 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r3 <= 0) goto L_0x041b
            int r3 = r1.c
            if (r2 >= r3) goto L_0x041b
            java.util.Set<java.lang.Integer> r3 = r0.g
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)
            boolean r3 = r3.contains(r7)
            if (r3 != 0) goto L_0x0402
            r1.h(r5)
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r2 = r0.f
            r3 = 4
            r2 = r2[r3]
            boolean r2 = r2.isEmpty()
            if (r2 == 0) goto L_0x03f4
            r0.b((androidx.exifinterface.media.ExifInterface.ByteOrderedDataInputStream) r1, (int) r3)
            goto L_0x0433
        L_0x03f4:
            java.util.HashMap<java.lang.String, androidx.exifinterface.media.ExifInterface$ExifAttribute>[] r2 = r0.f
            r2 = r2[r8]
            boolean r2 = r2.isEmpty()
            if (r2 == 0) goto L_0x0433
            r0.b((androidx.exifinterface.media.ExifInterface.ByteOrderedDataInputStream) r1, (int) r8)
            goto L_0x0433
        L_0x0402:
            boolean r1 = r
            if (r1 == 0) goto L_0x0433
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Stop reading file since re-reading an IFD may cause an infinite loop: "
            r1.append(r3)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r4, r1)
            goto L_0x0433
        L_0x041b:
            boolean r1 = r
            if (r1 == 0) goto L_0x0433
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Stop reading file since a wrong offset may cause an infinite loop: "
            r1.append(r3)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r4, r1)
        L_0x0433:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.exifinterface.media.ExifInterface.b(androidx.exifinterface.media.ExifInterface$ByteOrderedDataInputStream, int):void");
    }

    private void a(final ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        String str;
        String str2;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mediaMetadataRetriever.setDataSource(new MediaDataSource(this) {

                    /* renamed from: a  reason: collision with root package name */
                    long f657a;

                    public void close() throws IOException {
                    }

                    public long getSize() throws IOException {
                        return -1;
                    }

                    public int readAt(long j, byte[] bArr, int i, int i2) throws IOException {
                        if (i2 == 0) {
                            return 0;
                        }
                        if (j < 0) {
                            return -1;
                        }
                        try {
                            if (this.f657a != j) {
                                if (this.f657a >= 0 && j >= this.f657a + ((long) byteOrderedDataInputStream.available())) {
                                    return -1;
                                }
                                byteOrderedDataInputStream.h(j);
                                this.f657a = j;
                            }
                            if (i2 > byteOrderedDataInputStream.available()) {
                                i2 = byteOrderedDataInputStream.available();
                            }
                            int read = byteOrderedDataInputStream.read(bArr, i, i2);
                            if (read >= 0) {
                                this.f657a += (long) read;
                                return read;
                            }
                        } catch (IOException unused) {
                        }
                        this.f657a = -1;
                        return -1;
                    }
                });
            } else if (this.b != null) {
                mediaMetadataRetriever.setDataSource(this.b);
            } else if (this.f656a != null) {
                mediaMetadataRetriever.setDataSource(this.f656a);
            } else {
                mediaMetadataRetriever.release();
                return;
            }
            String extractMetadata = mediaMetadataRetriever.extractMetadata(33);
            String extractMetadata2 = mediaMetadataRetriever.extractMetadata(34);
            String extractMetadata3 = mediaMetadataRetriever.extractMetadata(26);
            String extractMetadata4 = mediaMetadataRetriever.extractMetadata(17);
            String str3 = null;
            if ("yes".equals(extractMetadata3)) {
                str3 = mediaMetadataRetriever.extractMetadata(29);
                str2 = mediaMetadataRetriever.extractMetadata(30);
                str = mediaMetadataRetriever.extractMetadata(31);
            } else if ("yes".equals(extractMetadata4)) {
                str3 = mediaMetadataRetriever.extractMetadata(18);
                str2 = mediaMetadataRetriever.extractMetadata(19);
                str = mediaMetadataRetriever.extractMetadata(24);
            } else {
                str2 = null;
                str = null;
            }
            if (str3 != null) {
                this.f[0].put("ImageWidth", ExifAttribute.a(Integer.parseInt(str3), this.h));
            }
            if (str2 != null) {
                this.f[0].put("ImageLength", ExifAttribute.a(Integer.parseInt(str2), this.h));
            }
            if (str != null) {
                int i2 = 1;
                int parseInt = Integer.parseInt(str);
                if (parseInt == 90) {
                    i2 = 6;
                } else if (parseInt == 180) {
                    i2 = 3;
                } else if (parseInt == 270) {
                    i2 = 8;
                }
                this.f[0].put("Orientation", ExifAttribute.a(i2, this.h));
            }
            if (!(extractMetadata == null || extractMetadata2 == null)) {
                int parseInt2 = Integer.parseInt(extractMetadata);
                int parseInt3 = Integer.parseInt(extractMetadata2);
                if (parseInt3 > 6) {
                    byteOrderedDataInputStream.h((long) parseInt2);
                    byte[] bArr = new byte[6];
                    if (byteOrderedDataInputStream.read(bArr) == 6) {
                        int i3 = parseInt2 + 6;
                        int i4 = parseInt3 - 6;
                        if (Arrays.equals(bArr, c0)) {
                            byte[] bArr2 = new byte[i4];
                            if (byteOrderedDataInputStream.read(bArr2) == i4) {
                                this.m = i3;
                                a(bArr2, 0);
                            } else {
                                throw new IOException("Can't read exif");
                            }
                        } else {
                            throw new IOException("Invalid identifier");
                        }
                    } else {
                        throw new IOException("Can't read identifier");
                    }
                } else {
                    throw new IOException("Invalid exif length");
                }
            }
            if (r) {
                Log.d("ExifInterface", "Heif meta: " + str3 + "x" + str2 + ", rotation " + str);
            }
        } finally {
            mediaMetadataRetriever.release();
        }
    }

    private void a(byte[] bArr, int i2) throws IOException {
        ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(bArr);
        a(byteOrderedDataInputStream, bArr.length);
        b(byteOrderedDataInputStream, i2);
    }

    private void a() {
        String a2 = a("DateTimeOriginal");
        if (a2 != null && a("DateTime") == null) {
            this.f[0].put("DateTime", ExifAttribute.a(a2));
        }
        if (a("ImageWidth") == null) {
            this.f[0].put("ImageWidth", ExifAttribute.a(0, this.h));
        }
        if (a("ImageLength") == null) {
            this.f[0].put("ImageLength", ExifAttribute.a(0, this.h));
        }
        if (a("Orientation") == null) {
            this.f[0].put("Orientation", ExifAttribute.a(0, this.h));
        }
        if (a("LightSource") == null) {
            this.f[1].put("LightSource", ExifAttribute.a(0, this.h));
        }
    }

    private void b(ByteOrderedDataInputStream byteOrderedDataInputStream, HashMap hashMap) throws IOException {
        ByteOrderedDataInputStream byteOrderedDataInputStream2 = byteOrderedDataInputStream;
        HashMap hashMap2 = hashMap;
        ExifAttribute exifAttribute = (ExifAttribute) hashMap2.get("StripOffsets");
        ExifAttribute exifAttribute2 = (ExifAttribute) hashMap2.get("StripByteCounts");
        if (exifAttribute != null && exifAttribute2 != null) {
            long[] a2 = a(exifAttribute.d(this.h));
            long[] a3 = a(exifAttribute2.d(this.h));
            if (a2 == null || a2.length == 0) {
                Log.w("ExifInterface", "stripOffsets should not be null or have zero length.");
            } else if (a3 == null || a3.length == 0) {
                Log.w("ExifInterface", "stripByteCounts should not be null or have zero length.");
            } else if (a2.length != a3.length) {
                Log.w("ExifInterface", "stripOffsets and stripByteCounts should have same length.");
            } else {
                long j2 = 0;
                for (long j3 : a3) {
                    j2 += j3;
                }
                byte[] bArr = new byte[((int) j2)];
                this.i = true;
                int i2 = 0;
                int i3 = 0;
                for (int i4 = 0; i4 < a2.length; i4++) {
                    int i5 = (int) a2[i4];
                    int i6 = (int) a3[i4];
                    if (i4 < a2.length - 1 && ((long) (i5 + i6)) != a2[i4 + 1]) {
                        this.i = false;
                    }
                    int i7 = i5 - i2;
                    if (i7 < 0) {
                        Log.d("ExifInterface", "Invalid strip offset value");
                    }
                    byteOrderedDataInputStream2.h((long) i7);
                    int i8 = i2 + i7;
                    byte[] bArr2 = new byte[i6];
                    byteOrderedDataInputStream2.read(bArr2);
                    i2 = i8 + i6;
                    System.arraycopy(bArr2, 0, bArr, i3, bArr2.length);
                    i3 += bArr2.length;
                }
                if (this.i) {
                    this.j = ((int) a2[0]) + this.m;
                    this.k = bArr.length;
                }
            }
        }
    }

    private void a(ByteOrderedDataInputStream byteOrderedDataInputStream, int i2) throws IOException {
        this.h = i(byteOrderedDataInputStream);
        byteOrderedDataInputStream.a(this.h);
        int readUnsignedShort = byteOrderedDataInputStream.readUnsignedShort();
        int i3 = this.d;
        if (i3 == 7 || i3 == 10 || readUnsignedShort == 42) {
            int readInt = byteOrderedDataInputStream.readInt();
            if (readInt < 8 || readInt >= i2) {
                throw new IOException("Invalid first Ifd offset: " + readInt);
            }
            int i4 = readInt - 8;
            if (i4 > 0 && byteOrderedDataInputStream.skipBytes(i4) != i4) {
                throw new IOException("Couldn't jump to first Ifd: " + i4);
            }
            return;
        }
        throw new IOException("Invalid start code: " + Integer.toHexString(readUnsignedShort));
    }

    private void a(ByteOrderedDataInputStream byteOrderedDataInputStream, HashMap hashMap) throws IOException {
        ExifAttribute exifAttribute = (ExifAttribute) hashMap.get("JPEGInterchangeFormat");
        ExifAttribute exifAttribute2 = (ExifAttribute) hashMap.get("JPEGInterchangeFormatLength");
        if (exifAttribute != null && exifAttribute2 != null) {
            int b2 = exifAttribute.b(this.h);
            int b3 = exifAttribute2.b(this.h);
            if (this.d == 7) {
                b2 += this.n;
            }
            int min = Math.min(b3, byteOrderedDataInputStream.s() - b2);
            if (b2 > 0 && min > 0) {
                this.j = this.m + b2;
                this.k = min;
                if (this.f656a == null && this.c == null && this.b == null) {
                    byteOrderedDataInputStream.h((long) this.j);
                    byteOrderedDataInputStream.readFully(new byte[this.k]);
                }
            }
            if (r) {
                Log.d("ExifInterface", "Setting thumbnail attributes with offset: " + b2 + ", length: " + min);
            }
        }
    }

    private boolean a(HashMap hashMap) throws IOException {
        ExifAttribute exifAttribute;
        int b2;
        ExifAttribute exifAttribute2 = (ExifAttribute) hashMap.get("BitsPerSample");
        if (exifAttribute2 != null) {
            int[] iArr = (int[]) exifAttribute2.d(this.h);
            if (Arrays.equals(s, iArr)) {
                return true;
            }
            if (this.d == 3 && (exifAttribute = (ExifAttribute) hashMap.get("PhotometricInterpretation")) != null && (((b2 = exifAttribute.b(this.h)) == 1 && Arrays.equals(iArr, t)) || (b2 == 6 && Arrays.equals(iArr, s)))) {
                return true;
            }
        }
        if (!r) {
            return false;
        }
        Log.d("ExifInterface", "Unsupported data type value");
        return false;
    }

    private boolean b(HashMap hashMap) throws IOException {
        ExifAttribute exifAttribute = (ExifAttribute) hashMap.get("ImageLength");
        ExifAttribute exifAttribute2 = (ExifAttribute) hashMap.get("ImageWidth");
        if (exifAttribute == null || exifAttribute2 == null) {
            return false;
        }
        return exifAttribute.b(this.h) <= 512 && exifAttribute2.b(this.h) <= 512;
    }

    private void a(int i2, int i3) throws IOException {
        if (!this.f[i2].isEmpty() && !this.f[i3].isEmpty()) {
            ExifAttribute exifAttribute = this.f[i2].get("ImageLength");
            ExifAttribute exifAttribute2 = this.f[i2].get("ImageWidth");
            ExifAttribute exifAttribute3 = this.f[i3].get("ImageLength");
            ExifAttribute exifAttribute4 = this.f[i3].get("ImageWidth");
            if (exifAttribute == null || exifAttribute2 == null) {
                if (r) {
                    Log.d("ExifInterface", "First image does not contain valid size information");
                }
            } else if (exifAttribute3 != null && exifAttribute4 != null) {
                int b2 = exifAttribute.b(this.h);
                int b3 = exifAttribute2.b(this.h);
                int b4 = exifAttribute3.b(this.h);
                int b5 = exifAttribute4.b(this.h);
                if (b2 < b4 && b3 < b5) {
                    HashMap<String, ExifAttribute>[] hashMapArr = this.f;
                    HashMap<String, ExifAttribute> hashMap = hashMapArr[i2];
                    hashMapArr[i2] = hashMapArr[i3];
                    hashMapArr[i3] = hashMap;
                }
            } else if (r) {
                Log.d("ExifInterface", "Second image does not contain valid size information");
            }
        } else if (r) {
            Log.d("ExifInterface", "Cannot perform swap since only one image data exists");
        }
    }

    private static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception unused) {
            }
        }
    }

    private static long[] a(Object obj) {
        if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            long[] jArr = new long[iArr.length];
            for (int i2 = 0; i2 < iArr.length; i2++) {
                jArr[i2] = (long) iArr[i2];
            }
            return jArr;
        } else if (obj instanceof long[]) {
            return (long[]) obj;
        } else {
            return null;
        }
    }

    private static boolean a(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null || bArr.length < bArr2.length) {
            return false;
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
        }
        return true;
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (int i2 = 0; i2 < bArr.length; i2++) {
            sb.append(String.format("%02x", new Object[]{Byte.valueOf(bArr[i2])}));
        }
        return sb.toString();
    }
}
