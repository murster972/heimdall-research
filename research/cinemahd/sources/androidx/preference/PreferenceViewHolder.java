package androidx.preference;

import android.util.SparseArray;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public class PreferenceViewHolder extends RecyclerView.ViewHolder {

    /* renamed from: a  reason: collision with root package name */
    private final SparseArray<View> f921a = new SparseArray<>(4);
    private boolean b;
    private boolean c;

    PreferenceViewHolder(View view) {
        super(view);
        this.f921a.put(16908310, view.findViewById(16908310));
        this.f921a.put(16908304, view.findViewById(16908304));
        this.f921a.put(16908294, view.findViewById(16908294));
        SparseArray<View> sparseArray = this.f921a;
        int i = R$id.icon_frame;
        sparseArray.put(i, view.findViewById(i));
        this.f921a.put(16908350, view.findViewById(16908350));
    }

    public View a(int i) {
        View view = this.f921a.get(i);
        if (view != null) {
            return view;
        }
        View findViewById = this.itemView.findViewById(i);
        if (findViewById != null) {
            this.f921a.put(i, findViewById);
        }
        return findViewById;
    }

    public boolean b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }

    public void b(boolean z) {
        this.c = z;
    }

    public void a(boolean z) {
        this.b = z;
    }
}
