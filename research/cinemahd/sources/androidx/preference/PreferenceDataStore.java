package androidx.preference;

import java.util.Set;

public abstract class PreferenceDataStore {
    public float a(String str, float f) {
        return f;
    }

    public int a(String str, int i) {
        return i;
    }

    public long a(String str, long j) {
        return j;
    }

    public String a(String str, String str2) {
        return str2;
    }

    public Set<String> a(String str, Set<String> set) {
        return set;
    }

    public boolean a(String str, boolean z) {
        return z;
    }

    public void b(String str, String str2) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }

    public void b(String str, Set<String> set) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }

    public void b(String str, int i) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }

    public void b(String str, long j) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }

    public void b(String str, float f) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }

    public void b(String str, boolean z) {
        throw new UnsupportedOperationException("Not implemented on this data store");
    }
}
