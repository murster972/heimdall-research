package androidx.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import androidx.core.content.ContextCompat;

public class PreferenceManager {

    /* renamed from: a  reason: collision with root package name */
    private Context f919a;
    private long b = 0;
    private SharedPreferences c;
    private PreferenceDataStore d;
    private SharedPreferences.Editor e;
    private boolean f;
    private String g;
    private int h;
    private int i = 0;
    private PreferenceScreen j;
    private PreferenceComparisonCallback k;
    private OnPreferenceTreeClickListener l;
    private OnDisplayPreferenceDialogListener m;
    private OnNavigateToScreenListener n;

    public interface OnDisplayPreferenceDialogListener {
        void onDisplayPreferenceDialog(Preference preference);
    }

    public interface OnNavigateToScreenListener {
        void onNavigateToScreen(PreferenceScreen preferenceScreen);
    }

    public interface OnPreferenceTreeClickListener {
        boolean onPreferenceTreeClick(Preference preference);
    }

    public static abstract class PreferenceComparisonCallback {
        public abstract boolean arePreferenceContentsTheSame(Preference preference, Preference preference2);

        public abstract boolean arePreferenceItemsTheSame(Preference preference, Preference preference2);
    }

    public static class SimplePreferenceComparisonCallback extends PreferenceComparisonCallback {
        public boolean arePreferenceContentsTheSame(Preference preference, Preference preference2) {
            if (preference.getClass() != preference2.getClass()) {
                return false;
            }
            if ((preference == preference2 && preference.wasDetached()) || !TextUtils.equals(preference.getTitle(), preference2.getTitle()) || !TextUtils.equals(preference.getSummary(), preference2.getSummary())) {
                return false;
            }
            Drawable icon = preference.getIcon();
            Drawable icon2 = preference2.getIcon();
            if ((icon != icon2 && (icon == null || !icon.equals(icon2))) || preference.isEnabled() != preference2.isEnabled() || preference.isSelectable() != preference2.isSelectable()) {
                return false;
            }
            if ((preference instanceof TwoStatePreference) && ((TwoStatePreference) preference).isChecked() != ((TwoStatePreference) preference2).isChecked()) {
                return false;
            }
            if (!(preference instanceof DropDownPreference) || preference == preference2) {
                return true;
            }
            return false;
        }

        public boolean arePreferenceItemsTheSame(Preference preference, Preference preference2) {
            return preference.getId() == preference2.getId();
        }
    }

    public PreferenceManager(Context context) {
        this.f919a = context;
        a(b(context));
    }

    public static SharedPreferences a(Context context) {
        return context.getSharedPreferences(b(context), j());
    }

    private static String b(Context context) {
        return context.getPackageName() + "_preferences";
    }

    private static int j() {
        return 0;
    }

    public OnNavigateToScreenListener c() {
        return this.n;
    }

    public OnPreferenceTreeClickListener d() {
        return this.l;
    }

    public PreferenceComparisonCallback e() {
        return this.k;
    }

    public PreferenceDataStore f() {
        return this.d;
    }

    public PreferenceScreen g() {
        return this.j;
    }

    public SharedPreferences h() {
        Context context;
        if (f() != null) {
            return null;
        }
        if (this.c == null) {
            if (this.i != 1) {
                context = this.f919a;
            } else {
                context = ContextCompat.a(this.f919a);
            }
            this.c = context.getSharedPreferences(this.g, this.h);
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return !this.f;
    }

    /* access modifiers changed from: package-private */
    public long b() {
        long j2;
        synchronized (this) {
            j2 = this.b;
            this.b = 1 + j2;
        }
        return j2;
    }

    public PreferenceScreen a(Context context, int i2, PreferenceScreen preferenceScreen) {
        a(true);
        PreferenceScreen preferenceScreen2 = (PreferenceScreen) new PreferenceInflater(context, this).a(i2, (PreferenceGroup) preferenceScreen);
        preferenceScreen2.onAttachedToHierarchy(this);
        a(false);
        return preferenceScreen2;
    }

    public void a(String str) {
        this.g = str;
        this.c = null;
    }

    public void a(int i2) {
        this.h = i2;
        this.c = null;
    }

    public boolean a(PreferenceScreen preferenceScreen) {
        PreferenceScreen preferenceScreen2 = this.j;
        if (preferenceScreen == preferenceScreen2) {
            return false;
        }
        if (preferenceScreen2 != null) {
            preferenceScreen2.onDetached();
        }
        this.j = preferenceScreen;
        return true;
    }

    public <T extends Preference> T a(CharSequence charSequence) {
        PreferenceScreen preferenceScreen = this.j;
        if (preferenceScreen == null) {
            return null;
        }
        return preferenceScreen.findPreference(charSequence);
    }

    /* access modifiers changed from: package-private */
    public SharedPreferences.Editor a() {
        if (this.d != null) {
            return null;
        }
        if (!this.f) {
            return h().edit();
        }
        if (this.e == null) {
            this.e = h().edit();
        }
        return this.e;
    }

    private void a(boolean z) {
        SharedPreferences.Editor editor;
        if (!z && (editor = this.e) != null) {
            editor.apply();
        }
        this.f = z;
    }

    public void a(OnDisplayPreferenceDialogListener onDisplayPreferenceDialogListener) {
        this.m = onDisplayPreferenceDialogListener;
    }

    public void a(Preference preference) {
        OnDisplayPreferenceDialogListener onDisplayPreferenceDialogListener = this.m;
        if (onDisplayPreferenceDialogListener != null) {
            onDisplayPreferenceDialogListener.onDisplayPreferenceDialog(preference);
        }
    }

    public void a(OnPreferenceTreeClickListener onPreferenceTreeClickListener) {
        this.l = onPreferenceTreeClickListener;
    }

    public void a(OnNavigateToScreenListener onNavigateToScreenListener) {
        this.n = onNavigateToScreenListener;
    }
}
