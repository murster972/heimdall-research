package androidx.mediarouter.app;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatDialog;
import androidx.mediarouter.R$id;
import androidx.mediarouter.R$layout;
import androidx.mediarouter.R$string;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class MediaRouteDevicePickerDialog extends AppCompatDialog {
    private final MediaRouter c;
    private final MediaRouterCallback d;
    Context e;
    private MediaRouteSelector f;
    List<MediaRouter.RouteInfo> g;
    private ImageButton h;
    private RecyclerAdapter i;
    private RecyclerView j;
    private boolean k;
    private long l;
    private long m;
    private final Handler n;

    private final class MediaRouterCallback extends MediaRouter.Callback {
        MediaRouterCallback() {
        }

        public void onRouteAdded(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteDevicePickerDialog.this.b();
        }

        public void onRouteChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteDevicePickerDialog.this.b();
        }

        public void onRouteRemoved(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteDevicePickerDialog.this.b();
        }

        public void onRouteSelected(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteDevicePickerDialog.this.dismiss();
        }
    }

    static final class RouteComparator implements Comparator<MediaRouter.RouteInfo> {

        /* renamed from: a  reason: collision with root package name */
        public static final RouteComparator f824a = new RouteComparator();

        RouteComparator() {
        }

        /* renamed from: a */
        public int compare(MediaRouter.RouteInfo routeInfo, MediaRouter.RouteInfo routeInfo2) {
            return routeInfo.i().compareToIgnoreCase(routeInfo2.i());
        }
    }

    public MediaRouteDevicePickerDialog(Context context) {
        this(context, 0);
    }

    public void a(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        } else if (!this.f.equals(mediaRouteSelector)) {
            this.f = mediaRouteSelector;
            if (this.k) {
                this.c.a((MediaRouter.Callback) this.d);
                this.c.a(mediaRouteSelector, this.d, 1);
            }
            b();
        }
    }

    public void b() {
        if (this.k) {
            ArrayList arrayList = new ArrayList(this.c.c());
            a((List<MediaRouter.RouteInfo>) arrayList);
            Collections.sort(arrayList, RouteComparator.f824a);
            if (SystemClock.uptimeMillis() - this.m >= this.l) {
                b(arrayList);
                return;
            }
            this.n.removeMessages(1);
            Handler handler = this.n;
            handler.sendMessageAtTime(handler.obtainMessage(1, arrayList), this.m + this.l);
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        getWindow().setLayout(-1, -1);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.k = true;
        this.c.a(this.f, this.d, 1);
        b();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.mr_picker_dialog);
        this.g = new ArrayList();
        this.h = (ImageButton) findViewById(R$id.mr_picker_close_button);
        this.h.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaRouteDevicePickerDialog.this.dismiss();
            }
        });
        this.i = new RecyclerAdapter();
        this.j = (RecyclerView) findViewById(R$id.mr_picker_list);
        this.j.setAdapter(this.i);
        this.j.setLayoutManager(new LinearLayoutManager(this.e));
        c();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.k = false;
        this.c.a((MediaRouter.Callback) this.d);
        this.n.removeMessages(1);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MediaRouteDevicePickerDialog(android.content.Context r2, int r3) {
        /*
            r1 = this;
            r0 = 0
            android.content.Context r2 = androidx.mediarouter.app.MediaRouterThemeHelper.a((android.content.Context) r2, (int) r3, (boolean) r0)
            int r3 = androidx.mediarouter.app.MediaRouterThemeHelper.b(r2)
            r1.<init>(r2, r3)
            androidx.mediarouter.media.MediaRouteSelector r2 = androidx.mediarouter.media.MediaRouteSelector.c
            r1.f = r2
            androidx.mediarouter.app.MediaRouteDevicePickerDialog$1 r2 = new androidx.mediarouter.app.MediaRouteDevicePickerDialog$1
            r2.<init>()
            r1.n = r2
            android.content.Context r2 = r1.getContext()
            androidx.mediarouter.media.MediaRouter r3 = androidx.mediarouter.media.MediaRouter.a((android.content.Context) r2)
            r1.c = r3
            androidx.mediarouter.app.MediaRouteDevicePickerDialog$MediaRouterCallback r3 = new androidx.mediarouter.app.MediaRouteDevicePickerDialog$MediaRouterCallback
            r3.<init>()
            r1.d = r3
            r1.e = r2
            android.content.res.Resources r2 = r2.getResources()
            int r3 = androidx.mediarouter.R$integer.mr_update_routes_delay_ms
            int r2 = r2.getInteger(r3)
            long r2 = (long) r2
            r1.l = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.mediarouter.app.MediaRouteDevicePickerDialog.<init>(android.content.Context, int):void");
    }

    public void a(List<MediaRouter.RouteInfo> list) {
        int size = list.size();
        while (true) {
            int i2 = size - 1;
            if (size > 0) {
                if (!a(list.get(i2))) {
                    list.remove(i2);
                }
                size = i2;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(List<MediaRouter.RouteInfo> list) {
        this.m = SystemClock.uptimeMillis();
        this.g.clear();
        this.g.addAll(list);
        this.i.b();
    }

    public boolean a(MediaRouter.RouteInfo routeInfo) {
        return !routeInfo.t() && routeInfo.u() && routeInfo.a(this.f);
    }

    private final class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        /* renamed from: a  reason: collision with root package name */
        ArrayList<Item> f819a;
        private final LayoutInflater b;
        private final Drawable c;
        private final Drawable d;
        private final Drawable e;
        private final Drawable f;

        private class HeaderViewHolder extends RecyclerView.ViewHolder {

            /* renamed from: a  reason: collision with root package name */
            TextView f820a;

            HeaderViewHolder(RecyclerAdapter recyclerAdapter, View view) {
                super(view);
                this.f820a = (TextView) view.findViewById(R$id.mr_dialog_header_name);
            }

            public void a(Item item) {
                this.f820a.setText(item.a().toString());
            }
        }

        private class Item {

            /* renamed from: a  reason: collision with root package name */
            private final Object f821a;
            private final int b;

            Item(RecyclerAdapter recyclerAdapter, Object obj) {
                this.f821a = obj;
                if (obj instanceof String) {
                    this.b = 1;
                } else if (obj instanceof MediaRouter.RouteInfo) {
                    this.b = 2;
                } else {
                    this.b = 0;
                    Log.w("RecyclerAdapter", "Wrong type of data passed to Item constructor");
                }
            }

            public Object a() {
                return this.f821a;
            }

            public int b() {
                return this.b;
            }
        }

        private class RouteViewHolder extends RecyclerView.ViewHolder {

            /* renamed from: a  reason: collision with root package name */
            View f822a;
            TextView b;
            ImageView c;

            RouteViewHolder(View view) {
                super(view);
                this.f822a = view;
                this.b = (TextView) view.findViewById(R$id.mr_picker_route_name);
                this.c = (ImageView) view.findViewById(R$id.mr_picker_route_icon);
            }

            public void a(Item item) {
                final MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) item.a();
                this.f822a.setOnClickListener(new View.OnClickListener(this) {
                    public void onClick(View view) {
                        routeInfo.x();
                    }
                });
                this.b.setText(routeInfo.i());
                this.c.setImageDrawable(RecyclerAdapter.this.a(routeInfo));
            }
        }

        RecyclerAdapter() {
            this.b = LayoutInflater.from(MediaRouteDevicePickerDialog.this.e);
            this.c = MediaRouterThemeHelper.d(MediaRouteDevicePickerDialog.this.e);
            this.d = MediaRouterThemeHelper.i(MediaRouteDevicePickerDialog.this.e);
            this.e = MediaRouterThemeHelper.g(MediaRouteDevicePickerDialog.this.e);
            this.f = MediaRouterThemeHelper.h(MediaRouteDevicePickerDialog.this.e);
            b();
        }

        /* access modifiers changed from: package-private */
        public Drawable a(MediaRouter.RouteInfo routeInfo) {
            Uri g2 = routeInfo.g();
            if (g2 != null) {
                try {
                    Drawable createFromStream = Drawable.createFromStream(MediaRouteDevicePickerDialog.this.e.getContentResolver().openInputStream(g2), (String) null);
                    if (createFromStream != null) {
                        return createFromStream;
                    }
                } catch (IOException e2) {
                    Log.w("RecyclerAdapter", "Failed to load " + g2, e2);
                }
            }
            return b(routeInfo);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f819a = new ArrayList<>();
            ArrayList arrayList = new ArrayList();
            for (int size = MediaRouteDevicePickerDialog.this.g.size() - 1; size >= 0; size--) {
                MediaRouter.RouteInfo routeInfo = MediaRouteDevicePickerDialog.this.g.get(size);
                if (routeInfo instanceof MediaRouter.RouteGroup) {
                    arrayList.add(routeInfo);
                    MediaRouteDevicePickerDialog.this.g.remove(size);
                }
            }
            this.f819a.add(new Item(this, MediaRouteDevicePickerDialog.this.e.getString(R$string.mr_dialog_device_header)));
            for (MediaRouter.RouteInfo item : MediaRouteDevicePickerDialog.this.g) {
                this.f819a.add(new Item(this, item));
            }
            this.f819a.add(new Item(this, MediaRouteDevicePickerDialog.this.e.getString(R$string.mr_dialog_route_header)));
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                this.f819a.add(new Item(this, (MediaRouter.RouteInfo) it2.next()));
            }
            notifyDataSetChanged();
        }

        public Item getItem(int i) {
            return this.f819a.get(i);
        }

        public int getItemCount() {
            return this.f819a.size();
        }

        public int getItemViewType(int i) {
            return this.f819a.get(i).b();
        }

        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
            int itemViewType = getItemViewType(i);
            Item item = getItem(i);
            if (itemViewType == 1) {
                ((HeaderViewHolder) viewHolder).a(item);
            } else if (itemViewType != 2) {
                Log.w("RecyclerAdapter", "Cannot bind item to ViewHolder because of wrong view type");
            } else {
                ((RouteViewHolder) viewHolder).a(item);
            }
        }

        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            if (i == 1) {
                return new HeaderViewHolder(this, this.b.inflate(R$layout.mr_dialog_header_item, viewGroup, false));
            }
            if (i == 2) {
                return new RouteViewHolder(this.b.inflate(R$layout.mr_picker_route_item, viewGroup, false));
            }
            Log.w("RecyclerAdapter", "Cannot create ViewHolder because of wrong view type");
            return null;
        }

        private Drawable b(MediaRouter.RouteInfo routeInfo) {
            int e2 = routeInfo.e();
            if (e2 == 1) {
                return this.d;
            }
            if (e2 == 2) {
                return this.e;
            }
            if (routeInfo instanceof MediaRouter.RouteGroup) {
                return this.f;
            }
            return this.c;
        }
    }
}
