package androidx.mediarouter.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatDialog;
import androidx.mediarouter.R$attr;
import androidx.mediarouter.R$id;
import androidx.mediarouter.R$layout;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MediaRouteChooserDialog extends AppCompatDialog {
    private final MediaRouter c;
    private final MediaRouterCallback d;
    private TextView e;
    private MediaRouteSelector f;
    private ArrayList<MediaRouter.RouteInfo> g;
    private RouteAdapter h;
    private ListView i;
    private boolean j;
    private long k;
    private final Handler l;

    private final class MediaRouterCallback extends MediaRouter.Callback {
        MediaRouterCallback() {
        }

        public void onRouteAdded(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteChooserDialog.this.b();
        }

        public void onRouteChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteChooserDialog.this.b();
        }

        public void onRouteRemoved(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteChooserDialog.this.b();
        }

        public void onRouteSelected(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteChooserDialog.this.dismiss();
        }
    }

    private final class RouteAdapter extends ArrayAdapter<MediaRouter.RouteInfo> implements AdapterView.OnItemClickListener {

        /* renamed from: a  reason: collision with root package name */
        private final LayoutInflater f794a;
        private final Drawable b;
        private final Drawable c;
        private final Drawable d;
        private final Drawable e;

        public RouteAdapter(Context context, List<MediaRouter.RouteInfo> list) {
            super(context, 0, list);
            this.f794a = LayoutInflater.from(context);
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(new int[]{R$attr.mediaRouteDefaultIconDrawable, R$attr.mediaRouteTvIconDrawable, R$attr.mediaRouteSpeakerIconDrawable, R$attr.mediaRouteSpeakerGroupIconDrawable});
            this.b = obtainStyledAttributes.getDrawable(0);
            this.c = obtainStyledAttributes.getDrawable(1);
            this.d = obtainStyledAttributes.getDrawable(2);
            this.e = obtainStyledAttributes.getDrawable(3);
            obtainStyledAttributes.recycle();
        }

        private Drawable a(MediaRouter.RouteInfo routeInfo) {
            int e2 = routeInfo.e();
            if (e2 == 1) {
                return this.c;
            }
            if (e2 == 2) {
                return this.d;
            }
            if (routeInfo instanceof MediaRouter.RouteGroup) {
                return this.e;
            }
            return this.b;
        }

        private Drawable b(MediaRouter.RouteInfo routeInfo) {
            Uri g = routeInfo.g();
            if (g != null) {
                try {
                    Drawable createFromStream = Drawable.createFromStream(getContext().getContentResolver().openInputStream(g), (String) null);
                    if (createFromStream != null) {
                        return createFromStream;
                    }
                } catch (IOException e2) {
                    Log.w("MediaRouteChooserDialog", "Failed to load " + g, e2);
                }
            }
            return a(routeInfo);
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = this.f794a.inflate(R$layout.mr_chooser_list_item, viewGroup, false);
            }
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) getItem(i);
            TextView textView = (TextView) view.findViewById(R$id.mr_chooser_route_name);
            TextView textView2 = (TextView) view.findViewById(R$id.mr_chooser_route_desc);
            textView.setText(routeInfo.i());
            String c2 = routeInfo.c();
            boolean z = true;
            if (!(routeInfo.b() == 2 || routeInfo.b() == 1)) {
                z = false;
            }
            if (!z || TextUtils.isEmpty(c2)) {
                textView.setGravity(16);
                textView2.setVisibility(8);
                textView2.setText("");
            } else {
                textView.setGravity(80);
                textView2.setVisibility(0);
                textView2.setText(c2);
            }
            view.setEnabled(routeInfo.u());
            ImageView imageView = (ImageView) view.findViewById(R$id.mr_chooser_route_icon);
            if (imageView != null) {
                imageView.setImageDrawable(b(routeInfo));
            }
            return view;
        }

        public boolean isEnabled(int i) {
            return ((MediaRouter.RouteInfo) getItem(i)).u();
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) getItem(i);
            if (routeInfo.u()) {
                routeInfo.x();
                MediaRouteChooserDialog.this.dismiss();
            }
        }
    }

    static final class RouteComparator implements Comparator<MediaRouter.RouteInfo> {

        /* renamed from: a  reason: collision with root package name */
        public static final RouteComparator f795a = new RouteComparator();

        RouteComparator() {
        }

        /* renamed from: a */
        public int compare(MediaRouter.RouteInfo routeInfo, MediaRouter.RouteInfo routeInfo2) {
            return routeInfo.i().compareToIgnoreCase(routeInfo2.i());
        }
    }

    public MediaRouteChooserDialog(Context context) {
        this(context, 0);
    }

    public void a(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        } else if (!this.f.equals(mediaRouteSelector)) {
            this.f = mediaRouteSelector;
            if (this.j) {
                this.c.a((MediaRouter.Callback) this.d);
                this.c.a(mediaRouteSelector, this.d, 1);
            }
            b();
        }
    }

    public void b() {
        if (this.j) {
            ArrayList arrayList = new ArrayList(this.c.c());
            a((List<MediaRouter.RouteInfo>) arrayList);
            Collections.sort(arrayList, RouteComparator.f795a);
            if (SystemClock.uptimeMillis() - this.k >= 300) {
                b(arrayList);
                return;
            }
            this.l.removeMessages(1);
            Handler handler = this.l;
            handler.sendMessageAtTime(handler.obtainMessage(1, arrayList), this.k + 300);
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        getWindow().setLayout(MediaRouteDialogHelper.a(getContext()), -2);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.j = true;
        this.c.a(this.f, this.d, 1);
        b();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.mr_chooser_dialog);
        this.g = new ArrayList<>();
        this.h = new RouteAdapter(getContext(), this.g);
        this.i = (ListView) findViewById(R$id.mr_chooser_list);
        this.i.setAdapter(this.h);
        this.i.setOnItemClickListener(this.h);
        this.i.setEmptyView(findViewById(16908292));
        this.e = (TextView) findViewById(R$id.mr_chooser_title);
        c();
    }

    public void onDetachedFromWindow() {
        this.j = false;
        this.c.a((MediaRouter.Callback) this.d);
        this.l.removeMessages(1);
        super.onDetachedFromWindow();
    }

    public void setTitle(CharSequence charSequence) {
        this.e.setText(charSequence);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MediaRouteChooserDialog(android.content.Context r2, int r3) {
        /*
            r1 = this;
            r0 = 0
            android.content.Context r2 = androidx.mediarouter.app.MediaRouterThemeHelper.a((android.content.Context) r2, (int) r3, (boolean) r0)
            int r3 = androidx.mediarouter.app.MediaRouterThemeHelper.b(r2)
            r1.<init>(r2, r3)
            androidx.mediarouter.media.MediaRouteSelector r2 = androidx.mediarouter.media.MediaRouteSelector.c
            r1.f = r2
            androidx.mediarouter.app.MediaRouteChooserDialog$1 r2 = new androidx.mediarouter.app.MediaRouteChooserDialog$1
            r2.<init>()
            r1.l = r2
            android.content.Context r2 = r1.getContext()
            androidx.mediarouter.media.MediaRouter r2 = androidx.mediarouter.media.MediaRouter.a((android.content.Context) r2)
            r1.c = r2
            androidx.mediarouter.app.MediaRouteChooserDialog$MediaRouterCallback r2 = new androidx.mediarouter.app.MediaRouteChooserDialog$MediaRouterCallback
            r2.<init>()
            r1.d = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.mediarouter.app.MediaRouteChooserDialog.<init>(android.content.Context, int):void");
    }

    public void setTitle(int i2) {
        this.e.setText(i2);
    }

    public void a(List<MediaRouter.RouteInfo> list) {
        int size = list.size();
        while (true) {
            int i2 = size - 1;
            if (size > 0) {
                if (!a(list.get(i2))) {
                    list.remove(i2);
                }
                size = i2;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(List<MediaRouter.RouteInfo> list) {
        this.k = SystemClock.uptimeMillis();
        this.g.clear();
        this.g.addAll(list);
        this.h.notifyDataSetChanged();
    }

    public boolean a(MediaRouter.RouteInfo routeInfo) {
        return !routeInfo.t() && routeInfo.u() && routeInfo.a(this.f);
    }
}
