package androidx.mediarouter.app;

public class MediaRouteDialogFactory {

    /* renamed from: a  reason: collision with root package name */
    private static final MediaRouteDialogFactory f825a = new MediaRouteDialogFactory();

    public static MediaRouteDialogFactory c() {
        return f825a;
    }

    public MediaRouteChooserDialogFragment a() {
        return new MediaRouteChooserDialogFragment();
    }

    public MediaRouteControllerDialogFragment b() {
        return new MediaRouteControllerDialogFragment();
    }
}
