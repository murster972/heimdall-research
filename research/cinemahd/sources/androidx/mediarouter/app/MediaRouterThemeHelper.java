package androidx.mediarouter.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import androidx.core.graphics.ColorUtils;
import androidx.mediarouter.R$attr;
import androidx.mediarouter.R$style;

final class MediaRouterThemeHelper {

    /* renamed from: a  reason: collision with root package name */
    static Drawable f829a;
    static Drawable b;
    static Drawable c;
    static Drawable d;

    private MediaRouterThemeHelper() {
    }

    static Context a(Context context) {
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, f(context));
        int c2 = c(contextThemeWrapper, R$attr.mediaRouteTheme);
        return c2 != 0 ? new ContextThemeWrapper(contextThemeWrapper, c2) : contextThemeWrapper;
    }

    private static Drawable b(Context context, int i) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[]{R$attr.mediaRouteDefaultIconDrawable, R$attr.mediaRouteTvIconDrawable, R$attr.mediaRouteSpeakerIconDrawable, R$attr.mediaRouteSpeakerGroupIconDrawable});
        Drawable drawable = obtainStyledAttributes.getDrawable(i);
        obtainStyledAttributes.recycle();
        return drawable;
    }

    static int c(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return typedValue.resourceId;
        }
        return 0;
    }

    static Drawable d(Context context) {
        if (f829a == null) {
            f829a = b(context, 0);
        }
        return f829a;
    }

    static float e(Context context) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(16842803, typedValue, true)) {
            return typedValue.getFloat();
        }
        return 0.5f;
    }

    private static int f(Context context) {
        if (j(context)) {
            if (a(context, 0) == -570425344) {
                return R$style.Theme_MediaRouter_Light;
            }
            return R$style.Theme_MediaRouter_Light_DarkControlPanel;
        } else if (a(context, 0) == -570425344) {
            return R$style.Theme_MediaRouter_LightControlPanel;
        } else {
            return R$style.Theme_MediaRouter;
        }
    }

    static Drawable g(Context context) {
        if (c == null) {
            c = b(context, 2);
        }
        return c;
    }

    static Drawable h(Context context) {
        if (d == null) {
            d = b(context, 3);
        }
        return d;
    }

    static Drawable i(Context context) {
        if (b == null) {
            b = b(context, 1);
        }
        return b;
    }

    private static boolean j(Context context) {
        TypedValue typedValue = new TypedValue();
        return context.getTheme().resolveAttribute(androidx.appcompat.R$attr.isLightTheme, typedValue, true) && typedValue.data != 0;
    }

    static int c(Context context) {
        int a2 = a(context, 0, androidx.appcompat.R$attr.colorPrimary);
        return ColorUtils.a(a2, a(context, 0, 16842801)) < 3.0d ? a(context, 0, androidx.appcompat.R$attr.colorAccent) : a2;
    }

    static Context a(Context context, int i, boolean z) {
        if (i == 0) {
            i = c(context, !z ? androidx.appcompat.R$attr.dialogTheme : androidx.appcompat.R$attr.alertDialogTheme);
        }
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, i);
        return c(contextThemeWrapper, R$attr.mediaRouteTheme) != 0 ? new ContextThemeWrapper(contextThemeWrapper, f(contextThemeWrapper)) : contextThemeWrapper;
    }

    static int b(Context context) {
        int c2 = c(context, R$attr.mediaRouteTheme);
        return c2 == 0 ? f(context) : c2;
    }

    static int a(Context context, int i) {
        if (ColorUtils.a(-1, a(context, i, androidx.appcompat.R$attr.colorPrimary)) >= 3.0d) {
            return -1;
        }
        return -570425344;
    }

    static void a(Context context, View view, View view2, boolean z) {
        int i;
        int a2 = a(context, 0, androidx.appcompat.R$attr.colorPrimary);
        int a3 = a(context, 0, androidx.appcompat.R$attr.colorPrimaryDark);
        if (!z || a(context, 0) != -570425344) {
            i = a2;
            a2 = a3;
        } else {
            i = -1;
        }
        view.setBackgroundColor(i);
        view2.setBackgroundColor(a2);
        view.setTag(Integer.valueOf(i));
        view2.setTag(Integer.valueOf(a2));
    }

    static void a(Context context, MediaRouteVolumeSlider mediaRouteVolumeSlider, View view) {
        int a2 = a(context, 0);
        if (Color.alpha(a2) != 255) {
            a2 = ColorUtils.c(a2, ((Integer) view.getTag()).intValue());
        }
        mediaRouteVolumeSlider.a(a2);
    }

    private static int a(Context context, int i, int i2) {
        if (i != 0) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, new int[]{i2});
            int color = obtainStyledAttributes.getColor(0, 0);
            obtainStyledAttributes.recycle();
            if (color != 0) {
                return color;
            }
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i2, typedValue, true);
        if (typedValue.resourceId != 0) {
            return context.getResources().getColor(typedValue.resourceId);
        }
        return typedValue.data;
    }
}
