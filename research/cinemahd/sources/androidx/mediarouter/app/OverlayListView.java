package androidx.mediarouter.app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.animation.Interpolator;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class OverlayListView extends ListView {

    /* renamed from: a  reason: collision with root package name */
    private final List<OverlayObject> f830a = new ArrayList();

    public static class OverlayObject {

        /* renamed from: a  reason: collision with root package name */
        private BitmapDrawable f831a;
        private float b = 1.0f;
        private Rect c;
        private Interpolator d;
        private long e;
        private Rect f;
        private int g;
        private float h = 1.0f;
        private float i = 1.0f;
        private long j;
        private boolean k;
        private boolean l;
        private OnAnimationEndListener m;

        public interface OnAnimationEndListener {
            void onAnimationEnd();
        }

        public OverlayObject(BitmapDrawable bitmapDrawable, Rect rect) {
            this.f831a = bitmapDrawable;
            this.f = rect;
            this.c = new Rect(rect);
            BitmapDrawable bitmapDrawable2 = this.f831a;
            if (bitmapDrawable2 != null && this.c != null) {
                bitmapDrawable2.setAlpha((int) (this.b * 255.0f));
                this.f831a.setBounds(this.c);
            }
        }

        public BitmapDrawable a() {
            return this.f831a;
        }

        public boolean b() {
            return this.k;
        }

        public void c() {
            this.k = true;
            this.l = true;
            OnAnimationEndListener onAnimationEndListener = this.m;
            if (onAnimationEndListener != null) {
                onAnimationEndListener.onAnimationEnd();
            }
        }

        public OverlayObject a(float f2, float f3) {
            this.h = f2;
            this.i = f3;
            return this;
        }

        public void b(long j2) {
            this.j = j2;
            this.k = true;
        }

        public OverlayObject a(int i2) {
            this.g = i2;
            return this;
        }

        public OverlayObject a(long j2) {
            this.e = j2;
            return this;
        }

        public boolean c(long j2) {
            float f2;
            if (this.l) {
                return false;
            }
            float max = Math.max(0.0f, Math.min(1.0f, ((float) (j2 - this.j)) / ((float) this.e)));
            if (!this.k) {
                max = 0.0f;
            }
            Interpolator interpolator = this.d;
            if (interpolator == null) {
                f2 = max;
            } else {
                f2 = interpolator.getInterpolation(max);
            }
            int i2 = (int) (((float) this.g) * f2);
            Rect rect = this.c;
            Rect rect2 = this.f;
            rect.top = rect2.top + i2;
            rect.bottom = rect2.bottom + i2;
            float f3 = this.h;
            this.b = f3 + ((this.i - f3) * f2);
            BitmapDrawable bitmapDrawable = this.f831a;
            if (!(bitmapDrawable == null || rect == null)) {
                bitmapDrawable.setAlpha((int) (this.b * 255.0f));
                this.f831a.setBounds(this.c);
            }
            if (this.k && max >= 1.0f) {
                this.l = true;
                OnAnimationEndListener onAnimationEndListener = this.m;
                if (onAnimationEndListener != null) {
                    onAnimationEndListener.onAnimationEnd();
                }
            }
            return !this.l;
        }

        public OverlayObject a(Interpolator interpolator) {
            this.d = interpolator;
            return this;
        }

        public OverlayObject a(OnAnimationEndListener onAnimationEndListener) {
            this.m = onAnimationEndListener;
            return this;
        }
    }

    public OverlayListView(Context context) {
        super(context);
    }

    public void a(OverlayObject overlayObject) {
        this.f830a.add(overlayObject);
    }

    public void b() {
        for (OverlayObject c : this.f830a) {
            c.c();
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f830a.size() > 0) {
            Iterator<OverlayObject> it2 = this.f830a.iterator();
            while (it2.hasNext()) {
                OverlayObject next = it2.next();
                BitmapDrawable a2 = next.a();
                if (a2 != null) {
                    a2.draw(canvas);
                }
                if (!next.c(getDrawingTime())) {
                    it2.remove();
                }
            }
        }
    }

    public void a() {
        for (OverlayObject next : this.f830a) {
            if (!next.b()) {
                next.b(getDrawingTime());
            }
        }
    }

    public OverlayListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public OverlayListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
