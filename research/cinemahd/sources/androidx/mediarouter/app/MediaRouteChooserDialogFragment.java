package androidx.mediarouter.app;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import androidx.fragment.app.DialogFragment;
import androidx.mediarouter.media.MediaRouteSelector;

public class MediaRouteChooserDialogFragment extends DialogFragment {
    private static final boolean c = Log.isLoggable("UseSupportDynamicGroup", 3);

    /* renamed from: a  reason: collision with root package name */
    private Dialog f796a;
    private MediaRouteSelector b;

    public MediaRouteChooserDialogFragment() {
        setCancelable(true);
    }

    private void f() {
        if (this.b == null) {
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.b = MediaRouteSelector.a(arguments.getBundle("selector"));
            }
            if (this.b == null) {
                this.b = MediaRouteSelector.c;
            }
        }
    }

    public void a(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector != null) {
            f();
            if (!this.b.equals(mediaRouteSelector)) {
                this.b = mediaRouteSelector;
                Bundle arguments = getArguments();
                if (arguments == null) {
                    arguments = new Bundle();
                }
                arguments.putBundle("selector", mediaRouteSelector.a());
                setArguments(arguments);
                Dialog dialog = this.f796a;
                if (dialog == null) {
                    return;
                }
                if (c) {
                    ((MediaRouteDevicePickerDialog) dialog).a(mediaRouteSelector);
                } else {
                    ((MediaRouteChooserDialog) dialog).a(mediaRouteSelector);
                }
            }
        } else {
            throw new IllegalArgumentException("selector must not be null");
        }
    }

    public MediaRouteSelector e() {
        f();
        return this.b;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        Dialog dialog = this.f796a;
        if (dialog != null) {
            if (c) {
                ((MediaRouteDevicePickerDialog) dialog).c();
            } else {
                ((MediaRouteChooserDialog) dialog).c();
            }
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        if (c) {
            this.f796a = a(getContext());
            ((MediaRouteDevicePickerDialog) this.f796a).a(e());
        } else {
            this.f796a = a(getContext(), bundle);
            ((MediaRouteChooserDialog) this.f796a).a(e());
        }
        return this.f796a;
    }

    public MediaRouteDevicePickerDialog a(Context context) {
        return new MediaRouteDevicePickerDialog(context);
    }

    public MediaRouteChooserDialog a(Context context, Bundle bundle) {
        return new MediaRouteChooserDialog(context);
    }
}
