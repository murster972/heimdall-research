package androidx.mediarouter.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatDialog;
import androidx.core.util.ObjectsCompat;
import androidx.mediarouter.R$id;
import androidx.mediarouter.R$layout;
import androidx.mediarouter.R$string;
import androidx.mediarouter.app.MediaRouteChooserDialog;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.common.util.UriUtil;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MediaRouteCastDialog extends AppCompatDialog {
    static final int G = ((int) TimeUnit.SECONDS.toMillis(30));
    FetchArtTask A;
    Bitmap B;
    Uri C;
    boolean D;
    Bitmap E;
    int F;
    final MediaRouter c;
    private final MediaRouterCallback d;
    private MediaRouteSelector e;
    final MediaRouter.RouteInfo f;
    final List<MediaRouter.RouteInfo> g;
    Context h;
    private boolean i;
    private boolean j;
    private long k;
    private final Handler l;
    private RecyclerView m;
    private RecyclerAdapter n;
    VolumeChangeListener o;
    int p;
    private ImageButton q;
    private Button r;
    private RelativeLayout s;
    private ImageView t;
    private TextView u;
    private TextView v;
    private String w;
    MediaControllerCompat x;
    MediaControllerCallback y;
    MediaDescriptionCompat z;

    private class FetchArtTask extends AsyncTask<Void, Void, Bitmap> {

        /* renamed from: a  reason: collision with root package name */
        private final Bitmap f783a;
        private final Uri b;
        private int c;

        FetchArtTask() {
            MediaDescriptionCompat mediaDescriptionCompat = MediaRouteCastDialog.this.z;
            Uri uri = null;
            Bitmap iconBitmap = mediaDescriptionCompat == null ? null : mediaDescriptionCompat.getIconBitmap();
            if (MediaRouteCastDialog.a(iconBitmap)) {
                Log.w("MediaRouteCastDialog", "Can't fetch the given art bitmap because it's already recycled.");
                iconBitmap = null;
            }
            this.f783a = iconBitmap;
            MediaDescriptionCompat mediaDescriptionCompat2 = MediaRouteCastDialog.this.z;
            this.b = mediaDescriptionCompat2 != null ? mediaDescriptionCompat2.getIconUri() : uri;
        }

        public Bitmap a() {
            return this.f783a;
        }

        public Uri b() {
            return this.b;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MediaRouteCastDialog.this.b();
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0049 */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0054 A[Catch:{ IOException -> 0x00a5 }] */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x00c2 A[SYNTHETIC, Splitter:B:58:0x00c2] */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x00c9 A[SYNTHETIC, Splitter:B:62:0x00c9] */
        /* JADX WARNING: Removed duplicated region for block: B:70:0x00d4  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x00e9 A[ADDED_TO_REGION] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.graphics.Bitmap doInBackground(java.lang.Void... r10) {
            /*
                r9 = this;
                java.lang.String r10 = "Unable to open: "
                android.graphics.Bitmap r0 = r9.f783a
                r1 = 0
                r2 = 1
                java.lang.String r3 = "MediaRouteCastDialog"
                r4 = 0
                if (r0 == 0) goto L_0x000e
                r10 = r0
                goto L_0x00ce
            L_0x000e:
                android.net.Uri r0 = r9.b
                if (r0 == 0) goto L_0x00cd
                java.io.InputStream r0 = r9.a((android.net.Uri) r0)     // Catch:{ IOException -> 0x00aa, all -> 0x00a7 }
                if (r0 != 0) goto L_0x0032
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a5 }
                r5.<init>()     // Catch:{ IOException -> 0x00a5 }
                r5.append(r10)     // Catch:{ IOException -> 0x00a5 }
                android.net.Uri r6 = r9.b     // Catch:{ IOException -> 0x00a5 }
                r5.append(r6)     // Catch:{ IOException -> 0x00a5 }
                java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x00a5 }
                android.util.Log.w(r3, r5)     // Catch:{ IOException -> 0x00a5 }
                if (r0 == 0) goto L_0x0031
                r0.close()     // Catch:{ IOException -> 0x0031 }
            L_0x0031:
                return r4
            L_0x0032:
                android.graphics.BitmapFactory$Options r5 = new android.graphics.BitmapFactory$Options     // Catch:{ IOException -> 0x00a5 }
                r5.<init>()     // Catch:{ IOException -> 0x00a5 }
                r5.inJustDecodeBounds = r2     // Catch:{ IOException -> 0x00a5 }
                android.graphics.BitmapFactory.decodeStream(r0, r4, r5)     // Catch:{ IOException -> 0x00a5 }
                int r6 = r5.outWidth     // Catch:{ IOException -> 0x00a5 }
                if (r6 == 0) goto L_0x009f
                int r6 = r5.outHeight     // Catch:{ IOException -> 0x00a5 }
                if (r6 != 0) goto L_0x0045
                goto L_0x009f
            L_0x0045:
                r0.reset()     // Catch:{ IOException -> 0x0049 }
                goto L_0x006e
            L_0x0049:
                r0.close()     // Catch:{ IOException -> 0x00a5 }
                android.net.Uri r6 = r9.b     // Catch:{ IOException -> 0x00a5 }
                java.io.InputStream r0 = r9.a((android.net.Uri) r6)     // Catch:{ IOException -> 0x00a5 }
                if (r0 != 0) goto L_0x006e
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a5 }
                r5.<init>()     // Catch:{ IOException -> 0x00a5 }
                r5.append(r10)     // Catch:{ IOException -> 0x00a5 }
                android.net.Uri r6 = r9.b     // Catch:{ IOException -> 0x00a5 }
                r5.append(r6)     // Catch:{ IOException -> 0x00a5 }
                java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x00a5 }
                android.util.Log.w(r3, r5)     // Catch:{ IOException -> 0x00a5 }
                if (r0 == 0) goto L_0x006d
                r0.close()     // Catch:{ IOException -> 0x006d }
            L_0x006d:
                return r4
            L_0x006e:
                r5.inJustDecodeBounds = r1     // Catch:{ IOException -> 0x00a5 }
                androidx.mediarouter.app.MediaRouteCastDialog r6 = androidx.mediarouter.app.MediaRouteCastDialog.this     // Catch:{ IOException -> 0x00a5 }
                int r7 = r5.outWidth     // Catch:{ IOException -> 0x00a5 }
                int r8 = r5.outHeight     // Catch:{ IOException -> 0x00a5 }
                int r6 = r6.a(r7, r8)     // Catch:{ IOException -> 0x00a5 }
                int r7 = r5.outHeight     // Catch:{ IOException -> 0x00a5 }
                int r7 = r7 / r6
                int r6 = java.lang.Integer.highestOneBit(r7)     // Catch:{ IOException -> 0x00a5 }
                int r6 = java.lang.Math.max(r2, r6)     // Catch:{ IOException -> 0x00a5 }
                r5.inSampleSize = r6     // Catch:{ IOException -> 0x00a5 }
                boolean r6 = r9.isCancelled()     // Catch:{ IOException -> 0x00a5 }
                if (r6 == 0) goto L_0x0093
                if (r0 == 0) goto L_0x0092
                r0.close()     // Catch:{ IOException -> 0x0092 }
            L_0x0092:
                return r4
            L_0x0093:
                android.graphics.Bitmap r10 = android.graphics.BitmapFactory.decodeStream(r0, r4, r5)     // Catch:{ IOException -> 0x00a5 }
                if (r0 == 0) goto L_0x00ce
                r0.close()     // Catch:{ IOException -> 0x009d }
                goto L_0x00ce
            L_0x009d:
                goto L_0x00ce
            L_0x009f:
                if (r0 == 0) goto L_0x00a4
                r0.close()     // Catch:{ IOException -> 0x00a4 }
            L_0x00a4:
                return r4
            L_0x00a5:
                r5 = move-exception
                goto L_0x00ac
            L_0x00a7:
                r10 = move-exception
                r0 = r4
                goto L_0x00c7
            L_0x00aa:
                r5 = move-exception
                r0 = r4
            L_0x00ac:
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c6 }
                r6.<init>()     // Catch:{ all -> 0x00c6 }
                r6.append(r10)     // Catch:{ all -> 0x00c6 }
                android.net.Uri r10 = r9.b     // Catch:{ all -> 0x00c6 }
                r6.append(r10)     // Catch:{ all -> 0x00c6 }
                java.lang.String r10 = r6.toString()     // Catch:{ all -> 0x00c6 }
                android.util.Log.w(r3, r10, r5)     // Catch:{ all -> 0x00c6 }
                if (r0 == 0) goto L_0x00cd
                r0.close()     // Catch:{ IOException -> 0x00cd }
                goto L_0x00cd
            L_0x00c6:
                r10 = move-exception
            L_0x00c7:
                if (r0 == 0) goto L_0x00cc
                r0.close()     // Catch:{ IOException -> 0x00cc }
            L_0x00cc:
                throw r10
            L_0x00cd:
                r10 = r4
            L_0x00ce:
                boolean r0 = androidx.mediarouter.app.MediaRouteCastDialog.a((android.graphics.Bitmap) r10)
                if (r0 == 0) goto L_0x00e9
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "Can't use recycled bitmap: "
                r0.append(r1)
                r0.append(r10)
                java.lang.String r10 = r0.toString()
                android.util.Log.w(r3, r10)
                return r4
            L_0x00e9:
                if (r10 == 0) goto L_0x011c
                int r0 = r10.getWidth()
                int r3 = r10.getHeight()
                if (r0 >= r3) goto L_0x011c
                androidx.palette.graphics.Palette$Builder r0 = new androidx.palette.graphics.Palette$Builder
                r0.<init>(r10)
                r0.a((int) r2)
                androidx.palette.graphics.Palette r0 = r0.a()
                java.util.List r2 = r0.g()
                boolean r2 = r2.isEmpty()
                if (r2 == 0) goto L_0x010c
                goto L_0x011a
            L_0x010c:
                java.util.List r0 = r0.g()
                java.lang.Object r0 = r0.get(r1)
                androidx.palette.graphics.Palette$Swatch r0 = (androidx.palette.graphics.Palette.Swatch) r0
                int r1 = r0.d()
            L_0x011a:
                r9.c = r1
            L_0x011c:
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.mediarouter.app.MediaRouteCastDialog.FetchArtTask.doInBackground(java.lang.Void[]):android.graphics.Bitmap");
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Bitmap bitmap) {
            MediaRouteCastDialog mediaRouteCastDialog = MediaRouteCastDialog.this;
            mediaRouteCastDialog.A = null;
            if (!ObjectsCompat.a(mediaRouteCastDialog.B, this.f783a) || !ObjectsCompat.a(MediaRouteCastDialog.this.C, this.b)) {
                MediaRouteCastDialog mediaRouteCastDialog2 = MediaRouteCastDialog.this;
                mediaRouteCastDialog2.B = this.f783a;
                mediaRouteCastDialog2.E = bitmap;
                mediaRouteCastDialog2.C = this.b;
                mediaRouteCastDialog2.F = this.c;
                mediaRouteCastDialog2.D = true;
                mediaRouteCastDialog2.d();
            }
        }

        private InputStream a(Uri uri) throws IOException {
            InputStream inputStream;
            String lowerCase = uri.getScheme().toLowerCase();
            if (UriUtil.QUALIFIED_RESOURCE_SCHEME.equals(lowerCase) || "content".equals(lowerCase) || UriUtil.LOCAL_FILE_SCHEME.equals(lowerCase)) {
                inputStream = MediaRouteCastDialog.this.h.getContentResolver().openInputStream(uri);
            } else {
                URLConnection openConnection = new URL(uri.toString()).openConnection();
                openConnection.setConnectTimeout(MediaRouteCastDialog.G);
                openConnection.setReadTimeout(MediaRouteCastDialog.G);
                inputStream = openConnection.getInputStream();
            }
            if (inputStream == null) {
                return null;
            }
            return new BufferedInputStream(inputStream);
        }
    }

    private final class MediaControllerCallback extends MediaControllerCompat.Callback {
        MediaControllerCallback() {
        }

        public void onMetadataChanged(MediaMetadataCompat mediaMetadataCompat) {
            MediaRouteCastDialog.this.z = mediaMetadataCompat == null ? null : mediaMetadataCompat.getDescription();
            MediaRouteCastDialog.this.e();
            MediaRouteCastDialog.this.d();
        }

        public void onSessionDestroyed() {
            MediaRouteCastDialog mediaRouteCastDialog = MediaRouteCastDialog.this;
            MediaControllerCompat mediaControllerCompat = mediaRouteCastDialog.x;
            if (mediaControllerCompat != null) {
                mediaControllerCompat.unregisterCallback(mediaRouteCastDialog.y);
                MediaRouteCastDialog.this.x = null;
            }
        }
    }

    private final class MediaRouterCallback extends MediaRouter.Callback {
        MediaRouterCallback() {
        }

        public void onRouteAdded(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteCastDialog.this.c();
        }

        public void onRouteChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteCastDialog.this.c();
            MediaRouteCastDialog.this.d();
        }

        public void onRouteRemoved(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteCastDialog.this.c();
        }

        public void onRouteSelected(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteCastDialog.this.d();
        }

        public void onRouteUnselected(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteCastDialog.this.d();
        }
    }

    private class VolumeChangeListener implements SeekBar.OnSeekBarChangeListener {
        VolumeChangeListener(MediaRouteCastDialog mediaRouteCastDialog) {
        }

        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    public MediaRouteCastDialog(Context context) {
        this(context, 0);
    }

    private void a(MediaSessionCompat.Token token) {
        MediaMetadataCompat mediaMetadataCompat;
        MediaControllerCompat mediaControllerCompat = this.x;
        MediaDescriptionCompat mediaDescriptionCompat = null;
        if (mediaControllerCompat != null) {
            mediaControllerCompat.unregisterCallback(this.y);
            this.x = null;
        }
        if (token != null && this.j) {
            try {
                this.x = new MediaControllerCompat(this.h, token);
            } catch (RemoteException e2) {
                Log.e("MediaRouteCastDialog", "Error creating media controller in setMediaSession.", e2);
            }
            MediaControllerCompat mediaControllerCompat2 = this.x;
            if (mediaControllerCompat2 != null) {
                mediaControllerCompat2.registerCallback(this.y);
            }
            MediaControllerCompat mediaControllerCompat3 = this.x;
            if (mediaControllerCompat3 == null) {
                mediaMetadataCompat = null;
            } else {
                mediaMetadataCompat = mediaControllerCompat3.getMetadata();
            }
            if (mediaMetadataCompat != null) {
                mediaDescriptionCompat = mediaMetadataCompat.getDescription();
            }
            this.z = mediaDescriptionCompat;
            e();
            d();
        }
    }

    private boolean g() {
        MediaDescriptionCompat mediaDescriptionCompat = this.z;
        Uri uri = null;
        Bitmap iconBitmap = mediaDescriptionCompat == null ? null : mediaDescriptionCompat.getIconBitmap();
        MediaDescriptionCompat mediaDescriptionCompat2 = this.z;
        if (mediaDescriptionCompat2 != null) {
            uri = mediaDescriptionCompat2.getIconUri();
        }
        FetchArtTask fetchArtTask = this.A;
        Bitmap a2 = fetchArtTask == null ? this.B : fetchArtTask.a();
        FetchArtTask fetchArtTask2 = this.A;
        Uri b = fetchArtTask2 == null ? this.C : fetchArtTask2.b();
        if (a2 != iconBitmap) {
            return true;
        }
        if (a2 != null || !ObjectsCompat.a(b, uri)) {
            return false;
        }
        return true;
    }

    private void h() {
        MediaDescriptionCompat mediaDescriptionCompat = this.z;
        CharSequence charSequence = null;
        CharSequence title = mediaDescriptionCompat == null ? null : mediaDescriptionCompat.getTitle();
        boolean z2 = !TextUtils.isEmpty(title);
        MediaDescriptionCompat mediaDescriptionCompat2 = this.z;
        if (mediaDescriptionCompat2 != null) {
            charSequence = mediaDescriptionCompat2.getSubtitle();
        }
        boolean z3 = !TextUtils.isEmpty(charSequence);
        if (z2) {
            this.u.setText(title);
        } else {
            this.u.setText(this.w);
        }
        if (z3) {
            this.v.setText(charSequence);
            this.v.setVisibility(0);
            return;
        }
        this.v.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.D = false;
        this.E = null;
        this.F = 0;
    }

    public void c() {
        if (this.j) {
            ArrayList arrayList = new ArrayList(this.c.c());
            a((List<MediaRouter.RouteInfo>) arrayList);
            Collections.sort(arrayList, MediaRouteChooserDialog.RouteComparator.f795a);
            if (SystemClock.uptimeMillis() - this.k >= 300) {
                b(arrayList);
                return;
            }
            this.l.removeMessages(1);
            Handler handler = this.l;
            handler.sendMessageAtTime(handler.obtainMessage(1, arrayList), this.k + 300);
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (!this.f.w() || this.f.t()) {
            dismiss();
        } else if (this.i) {
            if (this.D) {
                if (a(this.E)) {
                    this.t.setVisibility(8);
                    Log.w("MediaRouteCastDialog", "Can't set artwork image with recycled bitmap: " + this.E);
                } else {
                    this.t.setVisibility(0);
                    this.t.setImageBitmap(this.E);
                    this.t.setBackgroundColor(this.F);
                    this.s.setBackgroundDrawable(new BitmapDrawable(this.E));
                }
                b();
            } else {
                this.t.setVisibility(8);
            }
            h();
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (g()) {
            FetchArtTask fetchArtTask = this.A;
            if (fetchArtTask != null) {
                fetchArtTask.cancel(true);
            }
            this.A = new FetchArtTask();
            this.A.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        getWindow().setLayout(-1, -1);
        this.B = null;
        this.C = null;
        e();
        d();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.j = true;
        this.c.a(this.e, this.d, 1);
        c();
        a(this.c.b());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.mr_cast_dialog);
        this.q = (ImageButton) findViewById(R$id.mr_cast_close_button);
        this.q.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaRouteCastDialog.this.dismiss();
            }
        });
        this.r = (Button) findViewById(R$id.mr_cast_stop_button);
        this.r.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (MediaRouteCastDialog.this.f.w()) {
                    MediaRouteCastDialog.this.c.a(2);
                }
                MediaRouteCastDialog.this.dismiss();
            }
        });
        this.n = new RecyclerAdapter();
        this.m = (RecyclerView) findViewById(R$id.mr_cast_list);
        this.m.setAdapter(this.n);
        this.m.setLayoutManager(new LinearLayoutManager(this.h));
        this.o = new VolumeChangeListener(this);
        this.p = MediaRouterThemeHelper.a(this.h, 0);
        this.s = (RelativeLayout) findViewById(R$id.mr_cast_meta);
        this.t = (ImageView) findViewById(R$id.mr_cast_meta_art);
        this.u = (TextView) findViewById(R$id.mr_cast_meta_title);
        this.v = (TextView) findViewById(R$id.mr_cast_meta_subtitle);
        this.w = this.h.getResources().getString(R$string.mr_cast_dialog_title_view_placeholder);
        this.i = true;
        f();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.j = false;
        this.c.a((MediaRouter.Callback) this.d);
        this.l.removeMessages(1);
        a((MediaSessionCompat.Token) null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MediaRouteCastDialog(android.content.Context r2, int r3) {
        /*
            r1 = this;
            r0 = 0
            android.content.Context r2 = androidx.mediarouter.app.MediaRouterThemeHelper.a((android.content.Context) r2, (int) r3, (boolean) r0)
            int r3 = androidx.mediarouter.app.MediaRouterThemeHelper.b(r2)
            r1.<init>(r2, r3)
            androidx.mediarouter.media.MediaRouteSelector r2 = androidx.mediarouter.media.MediaRouteSelector.c
            r1.e = r2
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r1.g = r2
            androidx.mediarouter.app.MediaRouteCastDialog$1 r2 = new androidx.mediarouter.app.MediaRouteCastDialog$1
            r2.<init>()
            r1.l = r2
            android.content.Context r2 = r1.getContext()
            r1.h = r2
            android.content.Context r2 = r1.h
            androidx.mediarouter.media.MediaRouter r2 = androidx.mediarouter.media.MediaRouter.a((android.content.Context) r2)
            r1.c = r2
            androidx.mediarouter.app.MediaRouteCastDialog$MediaRouterCallback r2 = new androidx.mediarouter.app.MediaRouteCastDialog$MediaRouterCallback
            r2.<init>()
            r1.d = r2
            androidx.mediarouter.media.MediaRouter r2 = r1.c
            androidx.mediarouter.media.MediaRouter$RouteInfo r2 = r2.d()
            r1.f = r2
            androidx.mediarouter.app.MediaRouteCastDialog$MediaControllerCallback r2 = new androidx.mediarouter.app.MediaRouteCastDialog$MediaControllerCallback
            r2.<init>()
            r1.y = r2
            androidx.mediarouter.media.MediaRouter r2 = r1.c
            android.support.v4.media.session.MediaSessionCompat$Token r2 = r2.b()
            r1.a((android.support.v4.media.session.MediaSessionCompat.Token) r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.mediarouter.app.MediaRouteCastDialog.<init>(android.content.Context, int):void");
    }

    /* access modifiers changed from: package-private */
    public void b(List<MediaRouter.RouteInfo> list) {
        this.k = SystemClock.uptimeMillis();
        this.g.clear();
        this.g.addAll(list);
        this.n.b();
    }

    private final class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        /* renamed from: a  reason: collision with root package name */
        private final ArrayList<Item> f786a = new ArrayList<>();
        private final ArrayList<MediaRouter.RouteInfo> b = new ArrayList<>();
        private final ArrayList<MediaRouter.RouteInfo> c = new ArrayList<>();
        private final LayoutInflater d;
        private final Drawable e;
        private final Drawable f;
        private final Drawable g;
        private final Drawable h;

        private class GroupViewHolder extends RecyclerView.ViewHolder {

            /* renamed from: a  reason: collision with root package name */
            ImageView f787a;
            TextView b;

            GroupViewHolder(View view) {
                super(view);
                this.f787a = (ImageView) view.findViewById(R$id.mr_cast_group_icon);
                this.b = (TextView) view.findViewById(R$id.mr_cast_group_name);
            }

            public void a(Item item) {
                MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) item.a();
                this.f787a.setImageDrawable(RecyclerAdapter.this.a(routeInfo));
                this.b.setText(routeInfo.i());
            }
        }

        private class GroupVolumeViewHolder extends RecyclerView.ViewHolder {

            /* renamed from: a  reason: collision with root package name */
            TextView f788a;
            MediaRouteVolumeSlider b;

            GroupVolumeViewHolder(View view) {
                super(view);
                this.f788a = (TextView) view.findViewById(R$id.mr_group_volume_route_name);
                this.b = (MediaRouteVolumeSlider) view.findViewById(R$id.mr_group_volume_slider);
            }

            public void a(Item item) {
                MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) item.a();
                this.f788a.setText(routeInfo.i().toUpperCase());
                this.b.a(MediaRouteCastDialog.this.p);
                this.b.setTag(routeInfo);
                this.b.setProgress(MediaRouteCastDialog.this.f.o());
                this.b.setOnSeekBarChangeListener(MediaRouteCastDialog.this.o);
            }
        }

        private class HeaderViewHolder extends RecyclerView.ViewHolder {

            /* renamed from: a  reason: collision with root package name */
            TextView f789a;

            HeaderViewHolder(RecyclerAdapter recyclerAdapter, View view) {
                super(view);
                this.f789a = (TextView) view.findViewById(R$id.mr_dialog_header_name);
            }

            public void a(Item item) {
                this.f789a.setText(item.a().toString().toUpperCase());
            }
        }

        private class Item {

            /* renamed from: a  reason: collision with root package name */
            private final Object f790a;
            private final int b;

            Item(RecyclerAdapter recyclerAdapter, Object obj, int i) {
                this.f790a = obj;
                this.b = i;
            }

            public Object a() {
                return this.f790a;
            }

            public int b() {
                return this.b;
            }
        }

        private class RouteViewHolder extends RecyclerView.ViewHolder {

            /* renamed from: a  reason: collision with root package name */
            ImageView f791a;
            TextView b;
            CheckBox c;
            MediaRouteVolumeSlider d;

            RouteViewHolder(View view) {
                super(view);
                this.f791a = (ImageView) view.findViewById(R$id.mr_cast_route_icon);
                this.b = (TextView) view.findViewById(R$id.mr_cast_route_name);
                this.c = (CheckBox) view.findViewById(R$id.mr_cast_checkbox);
                this.d = (MediaRouteVolumeSlider) view.findViewById(R$id.mr_cast_volume_slider);
            }

            public void a(Item item) {
                MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) item.a();
                this.f791a.setImageDrawable(RecyclerAdapter.this.a(routeInfo));
                this.b.setText(routeInfo.i());
                this.c.setChecked(RecyclerAdapter.this.b(routeInfo));
                this.d.a(MediaRouteCastDialog.this.p);
                this.d.setTag(routeInfo);
                this.d.setProgress(routeInfo.o());
                this.d.setOnSeekBarChangeListener(MediaRouteCastDialog.this.o);
            }
        }

        RecyclerAdapter() {
            this.d = LayoutInflater.from(MediaRouteCastDialog.this.h);
            this.e = MediaRouterThemeHelper.d(MediaRouteCastDialog.this.h);
            this.f = MediaRouterThemeHelper.i(MediaRouteCastDialog.this.h);
            this.g = MediaRouterThemeHelper.g(MediaRouteCastDialog.this.h);
            this.h = MediaRouterThemeHelper.h(MediaRouteCastDialog.this.h);
            b();
        }

        private Drawable c(MediaRouter.RouteInfo routeInfo) {
            int e2 = routeInfo.e();
            if (e2 == 1) {
                return this.f;
            }
            if (e2 == 2) {
                return this.g;
            }
            if (routeInfo instanceof MediaRouter.RouteGroup) {
                return this.h;
            }
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public Drawable a(MediaRouter.RouteInfo routeInfo) {
            Uri g2 = routeInfo.g();
            if (g2 != null) {
                try {
                    Drawable createFromStream = Drawable.createFromStream(MediaRouteCastDialog.this.h.getContentResolver().openInputStream(g2), (String) null);
                    if (createFromStream != null) {
                        return createFromStream;
                    }
                } catch (IOException e2) {
                    Log.w("RecyclerAdapter", "Failed to load " + g2, e2);
                }
            }
            return c(routeInfo);
        }

        /* access modifiers changed from: package-private */
        public boolean b(MediaRouter.RouteInfo routeInfo) {
            if (routeInfo.w()) {
                return true;
            }
            MediaRouter.RouteInfo routeInfo2 = MediaRouteCastDialog.this.f;
            if (!(routeInfo2 instanceof MediaRouter.RouteGroup)) {
                return false;
            }
            for (MediaRouter.RouteInfo h2 : ((MediaRouter.RouteGroup) routeInfo2).y()) {
                if (h2.h().equals(routeInfo.h())) {
                    return true;
                }
            }
            return false;
        }

        public Item getItem(int i2) {
            return this.f786a.get(i2);
        }

        public int getItemCount() {
            return this.f786a.size();
        }

        public int getItemViewType(int i2) {
            return this.f786a.get(i2).b();
        }

        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
            int itemViewType = getItemViewType(i2);
            Item item = getItem(i2);
            if (itemViewType == 1) {
                ((GroupVolumeViewHolder) viewHolder).a(item);
            } else if (itemViewType == 2) {
                ((HeaderViewHolder) viewHolder).a(item);
            } else if (itemViewType == 3) {
                ((RouteViewHolder) viewHolder).a(item);
            } else if (itemViewType != 4) {
                Log.w("RecyclerAdapter", "Cannot bind item to ViewHolder because of wrong view type");
            } else {
                ((GroupViewHolder) viewHolder).a(item);
            }
        }

        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
            if (i2 == 1) {
                return new GroupVolumeViewHolder(this.d.inflate(R$layout.mr_cast_group_volume_item, viewGroup, false));
            }
            if (i2 == 2) {
                return new HeaderViewHolder(this, this.d.inflate(R$layout.mr_dialog_header_item, viewGroup, false));
            }
            if (i2 == 3) {
                return new RouteViewHolder(this.d.inflate(R$layout.mr_cast_route_item, viewGroup, false));
            }
            if (i2 == 4) {
                return new GroupViewHolder(this.d.inflate(R$layout.mr_cast_group_item, viewGroup, false));
            }
            Log.w("RecyclerAdapter", "Cannot create ViewHolder because of wrong view type");
            return null;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f786a.clear();
            MediaRouter.RouteInfo routeInfo = MediaRouteCastDialog.this.f;
            if (routeInfo instanceof MediaRouter.RouteGroup) {
                this.f786a.add(new Item(this, routeInfo, 1));
                for (MediaRouter.RouteInfo item : ((MediaRouter.RouteGroup) MediaRouteCastDialog.this.f).y()) {
                    this.f786a.add(new Item(this, item, 3));
                }
            } else {
                this.f786a.add(new Item(this, routeInfo, 3));
            }
            this.b.clear();
            this.c.clear();
            for (MediaRouter.RouteInfo next : MediaRouteCastDialog.this.g) {
                if (!b(next)) {
                    if (next instanceof MediaRouter.RouteGroup) {
                        this.c.add(next);
                    } else {
                        this.b.add(next);
                    }
                }
            }
            if (this.b.size() > 0) {
                this.f786a.add(new Item(this, MediaRouteCastDialog.this.h.getString(R$string.mr_dialog_device_header), 2));
                Iterator<MediaRouter.RouteInfo> it2 = this.b.iterator();
                while (it2.hasNext()) {
                    this.f786a.add(new Item(this, it2.next(), 3));
                }
            }
            if (this.c.size() > 0) {
                this.f786a.add(new Item(this, MediaRouteCastDialog.this.h.getString(R$string.mr_dialog_route_header), 2));
                Iterator<MediaRouter.RouteInfo> it3 = this.c.iterator();
                while (it3.hasNext()) {
                    this.f786a.add(new Item(this, it3.next(), 4));
                }
            }
            notifyDataSetChanged();
        }
    }

    public void a(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        } else if (!this.e.equals(mediaRouteSelector)) {
            this.e = mediaRouteSelector;
            if (this.j) {
                this.c.a((MediaRouter.Callback) this.d);
                this.c.a(mediaRouteSelector, this.d, 1);
            }
            c();
        }
    }

    public void a(List<MediaRouter.RouteInfo> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            if (!a(list.get(size))) {
                list.remove(size);
            }
        }
    }

    public boolean a(MediaRouter.RouteInfo routeInfo) {
        return !routeInfo.t() && routeInfo.u() && routeInfo.a(this.e);
    }

    static boolean a(Bitmap bitmap) {
        return bitmap != null && bitmap.isRecycled();
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3) {
        return this.t.getHeight();
    }
}
