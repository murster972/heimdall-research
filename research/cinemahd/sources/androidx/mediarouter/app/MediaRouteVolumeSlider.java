package androidx.mediarouter.app;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import androidx.appcompat.R$attr;
import androidx.appcompat.widget.AppCompatSeekBar;
import com.facebook.imageutils.JfifUtil;

class MediaRouteVolumeSlider extends AppCompatSeekBar {

    /* renamed from: a  reason: collision with root package name */
    private final float f828a;
    private boolean b;
    private Drawable c;
    private int d;

    public MediaRouteVolumeSlider(Context context) {
        this(context, (AttributeSet) null);
    }

    public void a(boolean z) {
        if (this.b != z) {
            this.b = z;
            super.setThumb(this.b ? null : this.c);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int i = isEnabled() ? JfifUtil.MARKER_FIRST_BYTE : (int) (this.f828a * 255.0f);
        this.c.setColorFilter(this.d, PorterDuff.Mode.SRC_IN);
        this.c.setAlpha(i);
        getProgressDrawable().setColorFilter(this.d, PorterDuff.Mode.SRC_IN);
        getProgressDrawable().setAlpha(i);
    }

    public void setThumb(Drawable drawable) {
        this.c = drawable;
        super.setThumb(this.b ? null : this.c);
    }

    public MediaRouteVolumeSlider(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R$attr.seekBarStyle);
    }

    public MediaRouteVolumeSlider(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f828a = MediaRouterThemeHelper.e(context);
    }

    public void a(int i) {
        if (this.d != i) {
            if (Color.alpha(i) != 255) {
                Log.e("MediaRouteVolumeSlider", "Volume slider color cannot be translucent: #" + Integer.toHexString(i));
            }
            this.d = i;
        }
    }
}
