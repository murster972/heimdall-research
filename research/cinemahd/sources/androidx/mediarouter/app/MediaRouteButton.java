package androidx.mediarouter.app;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import androidx.appcompat.widget.TooltipCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.mediarouter.R$attr;
import androidx.mediarouter.R$string;
import androidx.mediarouter.R$styleable;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;

public class MediaRouteButton extends View {
    static final SparseArray<Drawable.ConstantState> m = new SparseArray<>(2);
    private static final int[] n = {16842912};
    private static final int[] o = {16842911};

    /* renamed from: a  reason: collision with root package name */
    private final MediaRouter f777a;
    private final MediaRouterCallback b;
    private MediaRouteSelector c;
    private MediaRouteDialogFactory d;
    private boolean e;
    RemoteIndicatorLoader f;
    private Drawable g;
    private boolean h;
    private boolean i;
    private ColorStateList j;
    private int k;
    private int l;

    private final class MediaRouterCallback extends MediaRouter.Callback {
        MediaRouterCallback() {
        }

        public void onProviderAdded(MediaRouter mediaRouter, MediaRouter.ProviderInfo providerInfo) {
            MediaRouteButton.this.a();
        }

        public void onProviderChanged(MediaRouter mediaRouter, MediaRouter.ProviderInfo providerInfo) {
            MediaRouteButton.this.a();
        }

        public void onProviderRemoved(MediaRouter mediaRouter, MediaRouter.ProviderInfo providerInfo) {
            MediaRouteButton.this.a();
        }

        public void onRouteAdded(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.a();
        }

        public void onRouteChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.a();
        }

        public void onRouteRemoved(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.a();
        }

        public void onRouteSelected(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.a();
        }

        public void onRouteUnselected(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.a();
        }
    }

    private final class RemoteIndicatorLoader extends AsyncTask<Void, Void, Drawable> {

        /* renamed from: a  reason: collision with root package name */
        private final int f779a;

        RemoteIndicatorLoader(int i) {
            this.f779a = i;
        }

        private void c(Drawable drawable) {
            if (drawable != null) {
                MediaRouteButton.m.put(this.f779a, drawable.getConstantState());
            }
            MediaRouteButton.this.f = null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Drawable doInBackground(Void... voidArr) {
            return MediaRouteButton.this.getContext().getResources().getDrawable(this.f779a);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void onPostExecute(Drawable drawable) {
            c(drawable);
            MediaRouteButton.this.setRemoteIndicatorDrawable(drawable);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onCancelled(Drawable drawable) {
            c(drawable);
        }
    }

    public MediaRouteButton(Context context) {
        this(context, (AttributeSet) null);
    }

    private void c() {
        int i2;
        if (this.i) {
            i2 = R$string.mr_cast_button_connecting;
        } else if (this.h) {
            i2 = R$string.mr_cast_button_connected;
        } else {
            i2 = R$string.mr_cast_button_disconnected;
        }
        setContentDescription(getContext().getString(i2));
    }

    private Activity getActivity() {
        for (Context context = getContext(); context instanceof ContextWrapper; context = ((ContextWrapper) context).getBaseContext()) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
        }
        return null;
    }

    private FragmentManager getFragmentManager() {
        Activity activity = getActivity();
        if (activity instanceof FragmentActivity) {
            return ((FragmentActivity) activity).getSupportFragmentManager();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        MediaRouter.RouteInfo d2 = this.f777a.d();
        boolean z = false;
        boolean z2 = !d2.t() && d2.a(this.c);
        boolean z3 = z2 && d2.r();
        if (this.h != z2) {
            this.h = z2;
            z = true;
        }
        if (this.i != z3) {
            this.i = z3;
            z = true;
        }
        if (z) {
            c();
            refreshDrawableState();
        }
        if (this.e) {
            setEnabled(this.f777a.a(this.c, 1));
        }
        Drawable drawable = this.g;
        if (drawable != null && (drawable.getCurrent() instanceof AnimationDrawable)) {
            AnimationDrawable animationDrawable = (AnimationDrawable) this.g.getCurrent();
            if (this.e) {
                if ((z || z3) && !animationDrawable.isRunning()) {
                    animationDrawable.start();
                }
            } else if (z2 && !z3) {
                if (animationDrawable.isRunning()) {
                    animationDrawable.stop();
                }
                animationDrawable.selectDrawable(animationDrawable.getNumberOfFrames() - 1);
            }
        }
    }

    public boolean b() {
        if (!this.e) {
            return false;
        }
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            MediaRouter.RouteInfo d2 = this.f777a.d();
            if (d2.t() || !d2.a(this.c)) {
                if (fragmentManager.b("android.support.v7.mediarouter:MediaRouteChooserDialogFragment") != null) {
                    Log.w("MediaRouteButton", "showDialog(): Route chooser dialog already showing!");
                    return false;
                }
                MediaRouteChooserDialogFragment a2 = this.d.a();
                a2.a(this.c);
                a2.show(fragmentManager, "android.support.v7.mediarouter:MediaRouteChooserDialogFragment");
                return true;
            } else if (fragmentManager.b("android.support.v7.mediarouter:MediaRouteControllerDialogFragment") != null) {
                Log.w("MediaRouteButton", "showDialog(): Route controller dialog already showing!");
                return false;
            } else {
                MediaRouteControllerDialogFragment b2 = this.d.b();
                b2.a(this.c);
                b2.show(fragmentManager, "android.support.v7.mediarouter:MediaRouteControllerDialogFragment");
                return true;
            }
        } else {
            throw new IllegalStateException("The activity must be a subclass of FragmentActivity");
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.g != null) {
            this.g.setState(getDrawableState());
            invalidate();
        }
    }

    public MediaRouteDialogFactory getDialogFactory() {
        return this.d;
    }

    public MediaRouteSelector getRouteSelector() {
        return this.c;
    }

    public void jumpDrawablesToCurrentState() {
        if (getBackground() != null) {
            DrawableCompat.g(getBackground());
        }
        Drawable drawable = this.g;
        if (drawable != null) {
            DrawableCompat.g(drawable);
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.e = true;
        if (!this.c.d()) {
            this.f777a.a(this.c, (MediaRouter.Callback) this.b);
        }
        a();
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i2) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + 1);
        if (this.i) {
            View.mergeDrawableStates(onCreateDrawableState, o);
        } else if (this.h) {
            View.mergeDrawableStates(onCreateDrawableState, n);
        }
        return onCreateDrawableState;
    }

    public void onDetachedFromWindow() {
        this.e = false;
        if (!this.c.d()) {
            this.f777a.a((MediaRouter.Callback) this.b);
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.g != null) {
            int paddingLeft = getPaddingLeft();
            int width = getWidth() - getPaddingRight();
            int paddingTop = getPaddingTop();
            int height = getHeight() - getPaddingBottom();
            int intrinsicWidth = this.g.getIntrinsicWidth();
            int intrinsicHeight = this.g.getIntrinsicHeight();
            int i2 = paddingLeft + (((width - paddingLeft) - intrinsicWidth) / 2);
            int i3 = paddingTop + (((height - paddingTop) - intrinsicHeight) / 2);
            this.g.setBounds(i2, i3, intrinsicWidth + i2, intrinsicHeight + i3);
            this.g.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int i4 = this.k;
        Drawable drawable = this.g;
        int i5 = 0;
        int max = Math.max(i4, drawable != null ? drawable.getIntrinsicWidth() + getPaddingLeft() + getPaddingRight() : 0);
        int i6 = this.l;
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            i5 = drawable2.getIntrinsicHeight() + getPaddingTop() + getPaddingBottom();
        }
        int max2 = Math.max(i6, i5);
        if (mode == Integer.MIN_VALUE) {
            size = Math.min(size, max);
        } else if (mode != 1073741824) {
            size = max;
        }
        if (mode2 == Integer.MIN_VALUE) {
            size2 = Math.min(size2, max2);
        } else if (mode2 != 1073741824) {
            size2 = max2;
        }
        setMeasuredDimension(size, size2);
    }

    public boolean performClick() {
        boolean performClick = super.performClick();
        if (!performClick) {
            playSoundEffect(0);
        }
        if (b() || performClick) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void setCheatSheetEnabled(boolean z) {
        TooltipCompat.a(this, z ? getContext().getString(R$string.mr_button_content_description) : null);
    }

    public void setDialogFactory(MediaRouteDialogFactory mediaRouteDialogFactory) {
        if (mediaRouteDialogFactory != null) {
            this.d = mediaRouteDialogFactory;
            return;
        }
        throw new IllegalArgumentException("factory must not be null");
    }

    public void setRemoteIndicatorDrawable(Drawable drawable) {
        Drawable drawable2;
        RemoteIndicatorLoader remoteIndicatorLoader = this.f;
        if (remoteIndicatorLoader != null) {
            remoteIndicatorLoader.cancel(false);
        }
        Drawable drawable3 = this.g;
        if (drawable3 != null) {
            drawable3.setCallback((Drawable.Callback) null);
            unscheduleDrawable(this.g);
        }
        if (drawable != null) {
            if (this.j != null) {
                drawable = DrawableCompat.i(drawable.mutate());
                DrawableCompat.a(drawable, this.j);
            }
            drawable.setCallback(this);
            drawable.setState(getDrawableState());
            drawable.setVisible(getVisibility() == 0, false);
        }
        this.g = drawable;
        refreshDrawableState();
        if (this.e && (drawable2 = this.g) != null && (drawable2.getCurrent() instanceof AnimationDrawable)) {
            AnimationDrawable animationDrawable = (AnimationDrawable) this.g.getCurrent();
            if (this.i) {
                if (!animationDrawable.isRunning()) {
                    animationDrawable.start();
                }
            } else if (this.h) {
                if (animationDrawable.isRunning()) {
                    animationDrawable.stop();
                }
                animationDrawable.selectDrawable(animationDrawable.getNumberOfFrames() - 1);
            }
        }
    }

    public void setRouteSelector(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        } else if (!this.c.equals(mediaRouteSelector)) {
            if (this.e) {
                if (!this.c.d()) {
                    this.f777a.a((MediaRouter.Callback) this.b);
                }
                if (!mediaRouteSelector.d()) {
                    this.f777a.a(mediaRouteSelector, (MediaRouter.Callback) this.b);
                }
            }
            this.c = mediaRouteSelector;
            a();
        }
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
        Drawable drawable = this.g;
        if (drawable != null) {
            drawable.setVisible(getVisibility() == 0, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.g;
    }

    public MediaRouteButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R$attr.mediaRouteButtonStyle);
    }

    public MediaRouteButton(Context context, AttributeSet attributeSet, int i2) {
        super(MediaRouterThemeHelper.a(context), attributeSet, i2);
        this.c = MediaRouteSelector.c;
        this.d = MediaRouteDialogFactory.c();
        Context context2 = getContext();
        this.f777a = MediaRouter.a(context2);
        this.b = new MediaRouterCallback();
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, R$styleable.MediaRouteButton, i2, 0);
        this.j = obtainStyledAttributes.getColorStateList(R$styleable.MediaRouteButton_mediaRouteButtonTint);
        this.k = obtainStyledAttributes.getDimensionPixelSize(R$styleable.MediaRouteButton_android_minWidth, 0);
        this.l = obtainStyledAttributes.getDimensionPixelSize(R$styleable.MediaRouteButton_android_minHeight, 0);
        int resourceId = obtainStyledAttributes.getResourceId(R$styleable.MediaRouteButton_externalRouteEnabledDrawable, 0);
        obtainStyledAttributes.recycle();
        if (resourceId != 0) {
            Drawable.ConstantState constantState = m.get(resourceId);
            if (constantState != null) {
                setRemoteIndicatorDrawable(constantState.newDrawable());
            } else {
                this.f = new RemoteIndicatorLoader(resourceId);
                this.f.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            }
        }
        c();
        setClickable(true);
    }
}
