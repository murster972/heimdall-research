package androidx.mediarouter.app;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import androidx.core.content.ContextCompat;
import androidx.mediarouter.R$drawable;
import androidx.mediarouter.R$string;

class MediaRouteExpandCollapseButton extends ImageButton {

    /* renamed from: a  reason: collision with root package name */
    final AnimationDrawable f826a;
    final AnimationDrawable b;
    final String c;
    final String d;
    boolean e;
    View.OnClickListener f;

    public MediaRouteExpandCollapseButton(Context context) {
        this(context, (AttributeSet) null);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.f = onClickListener;
    }

    public MediaRouteExpandCollapseButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MediaRouteExpandCollapseButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f826a = (AnimationDrawable) ContextCompat.c(context, R$drawable.mr_group_expand);
        this.b = (AnimationDrawable) ContextCompat.c(context, R$drawable.mr_group_collapse);
        PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(MediaRouterThemeHelper.a(context, i), PorterDuff.Mode.SRC_IN);
        this.f826a.setColorFilter(porterDuffColorFilter);
        this.b.setColorFilter(porterDuffColorFilter);
        this.c = context.getString(R$string.mr_controller_expand_group);
        this.d = context.getString(R$string.mr_controller_collapse_group);
        setImageDrawable(this.f826a.getFrame(0));
        setContentDescription(this.c);
        super.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaRouteExpandCollapseButton mediaRouteExpandCollapseButton = MediaRouteExpandCollapseButton.this;
                mediaRouteExpandCollapseButton.e = !mediaRouteExpandCollapseButton.e;
                if (mediaRouteExpandCollapseButton.e) {
                    mediaRouteExpandCollapseButton.setImageDrawable(mediaRouteExpandCollapseButton.f826a);
                    MediaRouteExpandCollapseButton.this.f826a.start();
                    MediaRouteExpandCollapseButton mediaRouteExpandCollapseButton2 = MediaRouteExpandCollapseButton.this;
                    mediaRouteExpandCollapseButton2.setContentDescription(mediaRouteExpandCollapseButton2.d);
                } else {
                    mediaRouteExpandCollapseButton.setImageDrawable(mediaRouteExpandCollapseButton.b);
                    MediaRouteExpandCollapseButton.this.b.start();
                    MediaRouteExpandCollapseButton mediaRouteExpandCollapseButton3 = MediaRouteExpandCollapseButton.this;
                    mediaRouteExpandCollapseButton3.setContentDescription(mediaRouteExpandCollapseButton3.c);
                }
                View.OnClickListener onClickListener = MediaRouteExpandCollapseButton.this.f;
                if (onClickListener != null) {
                    onClickListener.onClick(view);
                }
            }
        });
    }
}
