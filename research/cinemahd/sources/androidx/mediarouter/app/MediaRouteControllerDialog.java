package androidx.mediarouter.app;

import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.core.util.ObjectsCompat;
import androidx.mediarouter.R$dimen;
import androidx.mediarouter.R$id;
import androidx.mediarouter.R$integer;
import androidx.mediarouter.R$layout;
import androidx.mediarouter.R$string;
import androidx.mediarouter.app.OverlayListView;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;
import com.facebook.common.util.UriUtil;
import com.facebook.imageutils.JfifUtil;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.http2.Http2;

public class MediaRouteControllerDialog extends AlertDialog {
    static final boolean s0 = Log.isLoggable("MediaRouteCtrlDialog", 3);
    static final int t0 = ((int) TimeUnit.SECONDS.toMillis(30));
    private RelativeLayout A;
    private LinearLayout B;
    private View C;
    OverlayListView D;
    VolumeGroupAdapter E;
    private List<MediaRouter.RouteInfo> F;
    Set<MediaRouter.RouteInfo> G;
    private Set<MediaRouter.RouteInfo> H;
    Set<MediaRouter.RouteInfo> I;
    SeekBar J;
    VolumeChangeListener K;
    MediaRouter.RouteInfo L;
    private int M;
    private int N;
    private int O;
    private final int P;
    Map<MediaRouter.RouteInfo, SeekBar> Q;
    MediaControllerCompat R;
    MediaControllerCallback S;
    PlaybackStateCompat T;
    MediaDescriptionCompat U;
    FetchArtTask V;
    Bitmap W;
    Uri X;
    boolean Y;
    Bitmap c0;
    final MediaRouter d;
    int d0;
    private final MediaRouterCallback e;
    boolean e0;
    final MediaRouter.RouteInfo f;
    boolean f0;
    Context g;
    boolean g0;
    private boolean h;
    boolean h0;
    private boolean i;
    boolean i0;
    private int j;
    int j0;
    private View k;
    private int k0;
    private Button l;
    private int l0;
    private Button m;
    private Interpolator m0;
    private ImageButton n;
    private Interpolator n0;
    private ImageButton o;
    private Interpolator o0;
    private MediaRouteExpandCollapseButton p;
    private Interpolator p0;
    private FrameLayout q;
    final AccessibilityManager q0;
    private LinearLayout r;
    Runnable r0;
    FrameLayout s;
    private FrameLayout t;
    private ImageView u;
    private TextView v;
    private TextView w;
    private TextView x;
    private boolean y;
    private LinearLayout z;

    private final class ClickListener implements View.OnClickListener {
        ClickListener() {
        }

        public void onClick(View view) {
            PlaybackStateCompat playbackStateCompat;
            int id = view.getId();
            int i = 1;
            if (id == 16908313 || id == 16908314) {
                if (MediaRouteControllerDialog.this.f.w()) {
                    MediaRouter mediaRouter = MediaRouteControllerDialog.this.d;
                    if (id == 16908313) {
                        i = 2;
                    }
                    mediaRouter.a(i);
                }
                MediaRouteControllerDialog.this.dismiss();
            } else if (id == R$id.mr_control_playback_ctrl) {
                MediaRouteControllerDialog mediaRouteControllerDialog = MediaRouteControllerDialog.this;
                if (mediaRouteControllerDialog.R != null && (playbackStateCompat = mediaRouteControllerDialog.T) != null) {
                    int i2 = 0;
                    if (playbackStateCompat.getState() != 3) {
                        i = 0;
                    }
                    if (i != 0 && MediaRouteControllerDialog.this.d()) {
                        MediaRouteControllerDialog.this.R.getTransportControls().pause();
                        i2 = R$string.mr_controller_pause;
                    } else if (i != 0 && MediaRouteControllerDialog.this.f()) {
                        MediaRouteControllerDialog.this.R.getTransportControls().stop();
                        i2 = R$string.mr_controller_stop;
                    } else if (i == 0 && MediaRouteControllerDialog.this.e()) {
                        MediaRouteControllerDialog.this.R.getTransportControls().play();
                        i2 = R$string.mr_controller_play;
                    }
                    AccessibilityManager accessibilityManager = MediaRouteControllerDialog.this.q0;
                    if (accessibilityManager != null && accessibilityManager.isEnabled() && i2 != 0) {
                        AccessibilityEvent obtain = AccessibilityEvent.obtain(Http2.INITIAL_MAX_FRAME_SIZE);
                        obtain.setPackageName(MediaRouteControllerDialog.this.g.getPackageName());
                        obtain.setClassName(ClickListener.class.getName());
                        obtain.getText().add(MediaRouteControllerDialog.this.g.getString(i2));
                        MediaRouteControllerDialog.this.q0.sendAccessibilityEvent(obtain);
                    }
                }
            } else if (id == R$id.mr_close) {
                MediaRouteControllerDialog.this.dismiss();
            }
        }
    }

    private class FetchArtTask extends AsyncTask<Void, Void, Bitmap> {

        /* renamed from: a  reason: collision with root package name */
        private final Bitmap f809a;
        private final Uri b;
        private int c;
        private long d;

        FetchArtTask() {
            MediaDescriptionCompat mediaDescriptionCompat = MediaRouteControllerDialog.this.U;
            Uri uri = null;
            Bitmap iconBitmap = mediaDescriptionCompat == null ? null : mediaDescriptionCompat.getIconBitmap();
            if (MediaRouteControllerDialog.a(iconBitmap)) {
                Log.w("MediaRouteCtrlDialog", "Can't fetch the given art bitmap because it's already recycled.");
                iconBitmap = null;
            }
            this.f809a = iconBitmap;
            MediaDescriptionCompat mediaDescriptionCompat2 = MediaRouteControllerDialog.this.U;
            this.b = mediaDescriptionCompat2 != null ? mediaDescriptionCompat2.getIconUri() : uri;
        }

        public Bitmap a() {
            return this.f809a;
        }

        public Uri b() {
            return this.b;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.d = SystemClock.uptimeMillis();
            MediaRouteControllerDialog.this.c();
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0049 */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0054 A[Catch:{ IOException -> 0x00a5 }] */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x00c2 A[SYNTHETIC, Splitter:B:58:0x00c2] */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x00c9 A[SYNTHETIC, Splitter:B:62:0x00c9] */
        /* JADX WARNING: Removed duplicated region for block: B:70:0x00d4  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x00e9 A[ADDED_TO_REGION] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.graphics.Bitmap doInBackground(java.lang.Void... r10) {
            /*
                r9 = this;
                java.lang.String r10 = "Unable to open: "
                android.graphics.Bitmap r0 = r9.f809a
                r1 = 0
                r2 = 1
                java.lang.String r3 = "MediaRouteCtrlDialog"
                r4 = 0
                if (r0 == 0) goto L_0x000e
                r10 = r0
                goto L_0x00ce
            L_0x000e:
                android.net.Uri r0 = r9.b
                if (r0 == 0) goto L_0x00cd
                java.io.InputStream r0 = r9.a((android.net.Uri) r0)     // Catch:{ IOException -> 0x00aa, all -> 0x00a7 }
                if (r0 != 0) goto L_0x0032
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a5 }
                r5.<init>()     // Catch:{ IOException -> 0x00a5 }
                r5.append(r10)     // Catch:{ IOException -> 0x00a5 }
                android.net.Uri r6 = r9.b     // Catch:{ IOException -> 0x00a5 }
                r5.append(r6)     // Catch:{ IOException -> 0x00a5 }
                java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x00a5 }
                android.util.Log.w(r3, r5)     // Catch:{ IOException -> 0x00a5 }
                if (r0 == 0) goto L_0x0031
                r0.close()     // Catch:{ IOException -> 0x0031 }
            L_0x0031:
                return r4
            L_0x0032:
                android.graphics.BitmapFactory$Options r5 = new android.graphics.BitmapFactory$Options     // Catch:{ IOException -> 0x00a5 }
                r5.<init>()     // Catch:{ IOException -> 0x00a5 }
                r5.inJustDecodeBounds = r2     // Catch:{ IOException -> 0x00a5 }
                android.graphics.BitmapFactory.decodeStream(r0, r4, r5)     // Catch:{ IOException -> 0x00a5 }
                int r6 = r5.outWidth     // Catch:{ IOException -> 0x00a5 }
                if (r6 == 0) goto L_0x009f
                int r6 = r5.outHeight     // Catch:{ IOException -> 0x00a5 }
                if (r6 != 0) goto L_0x0045
                goto L_0x009f
            L_0x0045:
                r0.reset()     // Catch:{ IOException -> 0x0049 }
                goto L_0x006e
            L_0x0049:
                r0.close()     // Catch:{ IOException -> 0x00a5 }
                android.net.Uri r6 = r9.b     // Catch:{ IOException -> 0x00a5 }
                java.io.InputStream r0 = r9.a((android.net.Uri) r6)     // Catch:{ IOException -> 0x00a5 }
                if (r0 != 0) goto L_0x006e
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a5 }
                r5.<init>()     // Catch:{ IOException -> 0x00a5 }
                r5.append(r10)     // Catch:{ IOException -> 0x00a5 }
                android.net.Uri r6 = r9.b     // Catch:{ IOException -> 0x00a5 }
                r5.append(r6)     // Catch:{ IOException -> 0x00a5 }
                java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x00a5 }
                android.util.Log.w(r3, r5)     // Catch:{ IOException -> 0x00a5 }
                if (r0 == 0) goto L_0x006d
                r0.close()     // Catch:{ IOException -> 0x006d }
            L_0x006d:
                return r4
            L_0x006e:
                r5.inJustDecodeBounds = r1     // Catch:{ IOException -> 0x00a5 }
                androidx.mediarouter.app.MediaRouteControllerDialog r6 = androidx.mediarouter.app.MediaRouteControllerDialog.this     // Catch:{ IOException -> 0x00a5 }
                int r7 = r5.outWidth     // Catch:{ IOException -> 0x00a5 }
                int r8 = r5.outHeight     // Catch:{ IOException -> 0x00a5 }
                int r6 = r6.a((int) r7, (int) r8)     // Catch:{ IOException -> 0x00a5 }
                int r7 = r5.outHeight     // Catch:{ IOException -> 0x00a5 }
                int r7 = r7 / r6
                int r6 = java.lang.Integer.highestOneBit(r7)     // Catch:{ IOException -> 0x00a5 }
                int r6 = java.lang.Math.max(r2, r6)     // Catch:{ IOException -> 0x00a5 }
                r5.inSampleSize = r6     // Catch:{ IOException -> 0x00a5 }
                boolean r6 = r9.isCancelled()     // Catch:{ IOException -> 0x00a5 }
                if (r6 == 0) goto L_0x0093
                if (r0 == 0) goto L_0x0092
                r0.close()     // Catch:{ IOException -> 0x0092 }
            L_0x0092:
                return r4
            L_0x0093:
                android.graphics.Bitmap r10 = android.graphics.BitmapFactory.decodeStream(r0, r4, r5)     // Catch:{ IOException -> 0x00a5 }
                if (r0 == 0) goto L_0x00ce
                r0.close()     // Catch:{ IOException -> 0x009d }
                goto L_0x00ce
            L_0x009d:
                goto L_0x00ce
            L_0x009f:
                if (r0 == 0) goto L_0x00a4
                r0.close()     // Catch:{ IOException -> 0x00a4 }
            L_0x00a4:
                return r4
            L_0x00a5:
                r5 = move-exception
                goto L_0x00ac
            L_0x00a7:
                r10 = move-exception
                r0 = r4
                goto L_0x00c7
            L_0x00aa:
                r5 = move-exception
                r0 = r4
            L_0x00ac:
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c6 }
                r6.<init>()     // Catch:{ all -> 0x00c6 }
                r6.append(r10)     // Catch:{ all -> 0x00c6 }
                android.net.Uri r10 = r9.b     // Catch:{ all -> 0x00c6 }
                r6.append(r10)     // Catch:{ all -> 0x00c6 }
                java.lang.String r10 = r6.toString()     // Catch:{ all -> 0x00c6 }
                android.util.Log.w(r3, r10, r5)     // Catch:{ all -> 0x00c6 }
                if (r0 == 0) goto L_0x00cd
                r0.close()     // Catch:{ IOException -> 0x00cd }
                goto L_0x00cd
            L_0x00c6:
                r10 = move-exception
            L_0x00c7:
                if (r0 == 0) goto L_0x00cc
                r0.close()     // Catch:{ IOException -> 0x00cc }
            L_0x00cc:
                throw r10
            L_0x00cd:
                r10 = r4
            L_0x00ce:
                boolean r0 = androidx.mediarouter.app.MediaRouteControllerDialog.a((android.graphics.Bitmap) r10)
                if (r0 == 0) goto L_0x00e9
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "Can't use recycled bitmap: "
                r0.append(r1)
                r0.append(r10)
                java.lang.String r10 = r0.toString()
                android.util.Log.w(r3, r10)
                return r4
            L_0x00e9:
                if (r10 == 0) goto L_0x011c
                int r0 = r10.getWidth()
                int r3 = r10.getHeight()
                if (r0 >= r3) goto L_0x011c
                androidx.palette.graphics.Palette$Builder r0 = new androidx.palette.graphics.Palette$Builder
                r0.<init>(r10)
                r0.a((int) r2)
                androidx.palette.graphics.Palette r0 = r0.a()
                java.util.List r2 = r0.g()
                boolean r2 = r2.isEmpty()
                if (r2 == 0) goto L_0x010c
                goto L_0x011a
            L_0x010c:
                java.util.List r0 = r0.g()
                java.lang.Object r0 = r0.get(r1)
                androidx.palette.graphics.Palette$Swatch r0 = (androidx.palette.graphics.Palette.Swatch) r0
                int r1 = r0.d()
            L_0x011a:
                r9.c = r1
            L_0x011c:
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.mediarouter.app.MediaRouteControllerDialog.FetchArtTask.doInBackground(java.lang.Void[]):android.graphics.Bitmap");
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Bitmap bitmap) {
            MediaRouteControllerDialog mediaRouteControllerDialog = MediaRouteControllerDialog.this;
            mediaRouteControllerDialog.V = null;
            if (!ObjectsCompat.a(mediaRouteControllerDialog.W, this.f809a) || !ObjectsCompat.a(MediaRouteControllerDialog.this.X, this.b)) {
                MediaRouteControllerDialog mediaRouteControllerDialog2 = MediaRouteControllerDialog.this;
                mediaRouteControllerDialog2.W = this.f809a;
                mediaRouteControllerDialog2.c0 = bitmap;
                mediaRouteControllerDialog2.X = this.b;
                mediaRouteControllerDialog2.d0 = this.c;
                boolean z = true;
                mediaRouteControllerDialog2.Y = true;
                long uptimeMillis = SystemClock.uptimeMillis() - this.d;
                MediaRouteControllerDialog mediaRouteControllerDialog3 = MediaRouteControllerDialog.this;
                if (uptimeMillis <= 120) {
                    z = false;
                }
                mediaRouteControllerDialog3.c(z);
            }
        }

        private InputStream a(Uri uri) throws IOException {
            InputStream inputStream;
            String lowerCase = uri.getScheme().toLowerCase();
            if (UriUtil.QUALIFIED_RESOURCE_SCHEME.equals(lowerCase) || "content".equals(lowerCase) || UriUtil.LOCAL_FILE_SCHEME.equals(lowerCase)) {
                inputStream = MediaRouteControllerDialog.this.g.getContentResolver().openInputStream(uri);
            } else {
                URLConnection openConnection = new URL(uri.toString()).openConnection();
                openConnection.setConnectTimeout(MediaRouteControllerDialog.t0);
                openConnection.setReadTimeout(MediaRouteControllerDialog.t0);
                inputStream = openConnection.getInputStream();
            }
            if (inputStream == null) {
                return null;
            }
            return new BufferedInputStream(inputStream);
        }
    }

    private final class MediaControllerCallback extends MediaControllerCompat.Callback {
        MediaControllerCallback() {
        }

        public void onMetadataChanged(MediaMetadataCompat mediaMetadataCompat) {
            MediaRouteControllerDialog.this.U = mediaMetadataCompat == null ? null : mediaMetadataCompat.getDescription();
            MediaRouteControllerDialog.this.j();
            MediaRouteControllerDialog.this.c(false);
        }

        public void onPlaybackStateChanged(PlaybackStateCompat playbackStateCompat) {
            MediaRouteControllerDialog mediaRouteControllerDialog = MediaRouteControllerDialog.this;
            mediaRouteControllerDialog.T = playbackStateCompat;
            mediaRouteControllerDialog.c(false);
        }

        public void onSessionDestroyed() {
            MediaRouteControllerDialog mediaRouteControllerDialog = MediaRouteControllerDialog.this;
            MediaControllerCompat mediaControllerCompat = mediaRouteControllerDialog.R;
            if (mediaControllerCompat != null) {
                mediaControllerCompat.unregisterCallback(mediaRouteControllerDialog.S);
                MediaRouteControllerDialog.this.R = null;
            }
        }
    }

    private final class MediaRouterCallback extends MediaRouter.Callback {
        MediaRouterCallback() {
        }

        public void onRouteChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteControllerDialog.this.c(true);
        }

        public void onRouteUnselected(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteControllerDialog.this.c(false);
        }

        public void onRouteVolumeChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            SeekBar seekBar = MediaRouteControllerDialog.this.Q.get(routeInfo);
            int o = routeInfo.o();
            if (MediaRouteControllerDialog.s0) {
                Log.d("MediaRouteCtrlDialog", "onRouteVolumeChanged(), route.getVolume:" + o);
            }
            if (seekBar != null && MediaRouteControllerDialog.this.L != routeInfo) {
                seekBar.setProgress(o);
            }
        }
    }

    private class VolumeChangeListener implements SeekBar.OnSeekBarChangeListener {

        /* renamed from: a  reason: collision with root package name */
        private final Runnable f812a = new Runnable() {
            public void run() {
                MediaRouteControllerDialog mediaRouteControllerDialog = MediaRouteControllerDialog.this;
                if (mediaRouteControllerDialog.L != null) {
                    mediaRouteControllerDialog.L = null;
                    if (mediaRouteControllerDialog.e0) {
                        mediaRouteControllerDialog.c(mediaRouteControllerDialog.f0);
                    }
                }
            }
        };

        VolumeChangeListener() {
        }

        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            if (z) {
                MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) seekBar.getTag();
                if (MediaRouteControllerDialog.s0) {
                    Log.d("MediaRouteCtrlDialog", "onProgressChanged(): calling MediaRouter.RouteInfo.requestSetVolume(" + i + ")");
                }
                routeInfo.a(i);
            }
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            MediaRouteControllerDialog mediaRouteControllerDialog = MediaRouteControllerDialog.this;
            if (mediaRouteControllerDialog.L != null) {
                mediaRouteControllerDialog.J.removeCallbacks(this.f812a);
            }
            MediaRouteControllerDialog.this.L = (MediaRouter.RouteInfo) seekBar.getTag();
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            MediaRouteControllerDialog.this.J.postDelayed(this.f812a, 500);
        }
    }

    private class VolumeGroupAdapter extends ArrayAdapter<MediaRouter.RouteInfo> {

        /* renamed from: a  reason: collision with root package name */
        final float f814a;

        public VolumeGroupAdapter(Context context, List<MediaRouter.RouteInfo> list) {
            super(context, 0, list);
            this.f814a = MediaRouterThemeHelper.e(context);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            int i2;
            int i3 = 0;
            if (view == null) {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R$layout.mr_controller_volume_item, viewGroup, false);
            } else {
                MediaRouteControllerDialog.this.b(view);
            }
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) getItem(i);
            if (routeInfo != null) {
                boolean u = routeInfo.u();
                TextView textView = (TextView) view.findViewById(R$id.mr_name);
                textView.setEnabled(u);
                textView.setText(routeInfo.i());
                MediaRouteVolumeSlider mediaRouteVolumeSlider = (MediaRouteVolumeSlider) view.findViewById(R$id.mr_volume_slider);
                MediaRouterThemeHelper.a(viewGroup.getContext(), mediaRouteVolumeSlider, (View) MediaRouteControllerDialog.this.D);
                mediaRouteVolumeSlider.setTag(routeInfo);
                MediaRouteControllerDialog.this.Q.put(routeInfo, mediaRouteVolumeSlider);
                mediaRouteVolumeSlider.a(!u);
                mediaRouteVolumeSlider.setEnabled(u);
                if (u) {
                    if (MediaRouteControllerDialog.this.a(routeInfo)) {
                        mediaRouteVolumeSlider.setMax(routeInfo.q());
                        mediaRouteVolumeSlider.setProgress(routeInfo.o());
                        mediaRouteVolumeSlider.setOnSeekBarChangeListener(MediaRouteControllerDialog.this.K);
                    } else {
                        mediaRouteVolumeSlider.setMax(100);
                        mediaRouteVolumeSlider.setProgress(100);
                        mediaRouteVolumeSlider.setEnabled(false);
                    }
                }
                ImageView imageView = (ImageView) view.findViewById(R$id.mr_volume_item_icon);
                if (u) {
                    i2 = JfifUtil.MARKER_FIRST_BYTE;
                } else {
                    i2 = (int) (this.f814a * 255.0f);
                }
                imageView.setAlpha(i2);
                LinearLayout linearLayout = (LinearLayout) view.findViewById(R$id.volume_item_container);
                if (MediaRouteControllerDialog.this.I.contains(routeInfo)) {
                    i3 = 4;
                }
                linearLayout.setVisibility(i3);
                Set<MediaRouter.RouteInfo> set = MediaRouteControllerDialog.this.G;
                if (set != null && set.contains(routeInfo)) {
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 0.0f);
                    alphaAnimation.setDuration(0);
                    alphaAnimation.setFillEnabled(true);
                    alphaAnimation.setFillAfter(true);
                    view.clearAnimation();
                    view.startAnimation(alphaAnimation);
                }
            }
            return view;
        }

        public boolean isEnabled(int i) {
            return false;
        }
    }

    public MediaRouteControllerDialog(Context context) {
        this(context, 0);
    }

    private void a(MediaSessionCompat.Token token) {
        MediaMetadataCompat mediaMetadataCompat;
        MediaDescriptionCompat mediaDescriptionCompat;
        MediaControllerCompat mediaControllerCompat = this.R;
        PlaybackStateCompat playbackStateCompat = null;
        if (mediaControllerCompat != null) {
            mediaControllerCompat.unregisterCallback(this.S);
            this.R = null;
        }
        if (token != null && this.i) {
            try {
                this.R = new MediaControllerCompat(this.g, token);
            } catch (RemoteException e2) {
                Log.e("MediaRouteCtrlDialog", "Error creating media controller in setMediaSession.", e2);
            }
            MediaControllerCompat mediaControllerCompat2 = this.R;
            if (mediaControllerCompat2 != null) {
                mediaControllerCompat2.registerCallback(this.S);
            }
            MediaControllerCompat mediaControllerCompat3 = this.R;
            if (mediaControllerCompat3 == null) {
                mediaMetadataCompat = null;
            } else {
                mediaMetadataCompat = mediaControllerCompat3.getMetadata();
            }
            if (mediaMetadataCompat == null) {
                mediaDescriptionCompat = null;
            } else {
                mediaDescriptionCompat = mediaMetadataCompat.getDescription();
            }
            this.U = mediaDescriptionCompat;
            MediaControllerCompat mediaControllerCompat4 = this.R;
            if (mediaControllerCompat4 != null) {
                playbackStateCompat = mediaControllerCompat4.getPlaybackState();
            }
            this.T = playbackStateCompat;
            j();
            c(false);
        }
    }

    private int f(boolean z2) {
        if (!z2 && this.B.getVisibility() != 0) {
            return 0;
        }
        int paddingTop = 0 + this.z.getPaddingTop() + this.z.getPaddingBottom();
        if (z2) {
            paddingTop += this.A.getMeasuredHeight();
        }
        if (this.B.getVisibility() == 0) {
            paddingTop += this.B.getMeasuredHeight();
        }
        return (!z2 || this.B.getVisibility() != 0) ? paddingTop : paddingTop + this.C.getMeasuredHeight();
    }

    private void h(boolean z2) {
        int i2 = 0;
        this.C.setVisibility((this.B.getVisibility() != 0 || !z2) ? 8 : 0);
        LinearLayout linearLayout = this.z;
        if (this.B.getVisibility() == 8 && !z2) {
            i2 = 8;
        }
        linearLayout.setVisibility(i2);
    }

    private boolean l() {
        return this.k == null && !(this.U == null && this.T == null);
    }

    private void m() {
        AnonymousClass12 r02 = new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                MediaRouteControllerDialog.this.b(true);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        };
        int firstVisiblePosition = this.D.getFirstVisiblePosition();
        boolean z2 = false;
        for (int i2 = 0; i2 < this.D.getChildCount(); i2++) {
            View childAt = this.D.getChildAt(i2);
            VolumeGroupAdapter volumeGroupAdapter = this.E;
            if (this.G.contains((MediaRouter.RouteInfo) volumeGroupAdapter.getItem(firstVisiblePosition + i2))) {
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration((long) this.k0);
                alphaAnimation.setFillEnabled(true);
                alphaAnimation.setFillAfter(true);
                if (!z2) {
                    alphaAnimation.setAnimationListener(r02);
                    z2 = true;
                }
                childAt.clearAnimation();
                childAt.startAnimation(alphaAnimation);
            }
        }
    }

    private MediaRouter.RouteGroup n() {
        MediaRouter.RouteInfo routeInfo = this.f;
        if (routeInfo instanceof MediaRouter.RouteGroup) {
            return (MediaRouter.RouteGroup) routeInfo;
        }
        return null;
    }

    private boolean o() {
        MediaDescriptionCompat mediaDescriptionCompat = this.U;
        Uri uri = null;
        Bitmap iconBitmap = mediaDescriptionCompat == null ? null : mediaDescriptionCompat.getIconBitmap();
        MediaDescriptionCompat mediaDescriptionCompat2 = this.U;
        if (mediaDescriptionCompat2 != null) {
            uri = mediaDescriptionCompat2.getIconUri();
        }
        FetchArtTask fetchArtTask = this.V;
        Bitmap a2 = fetchArtTask == null ? this.W : fetchArtTask.a();
        FetchArtTask fetchArtTask2 = this.V;
        Uri b = fetchArtTask2 == null ? this.X : fetchArtTask2.b();
        if (a2 != iconBitmap) {
            return true;
        }
        if (a2 != null || a(b, uri)) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void p() {
        /*
            r8 = this;
            boolean r0 = r8.l()
            if (r0 == 0) goto L_0x00eb
            android.support.v4.media.MediaDescriptionCompat r0 = r8.U
            r1 = 0
            if (r0 != 0) goto L_0x000d
            r0 = r1
            goto L_0x0011
        L_0x000d:
            java.lang.CharSequence r0 = r0.getTitle()
        L_0x0011:
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            r3 = 1
            r2 = r2 ^ r3
            android.support.v4.media.MediaDescriptionCompat r4 = r8.U
            if (r4 != 0) goto L_0x001c
            goto L_0x0020
        L_0x001c:
            java.lang.CharSequence r1 = r4.getSubtitle()
        L_0x0020:
            boolean r4 = android.text.TextUtils.isEmpty(r1)
            r4 = r4 ^ r3
            androidx.mediarouter.media.MediaRouter$RouteInfo r5 = r8.f
            int r5 = r5.l()
            r6 = -1
            r7 = 0
            if (r5 == r6) goto L_0x0039
            android.widget.TextView r0 = r8.v
            int r1 = androidx.mediarouter.R$string.mr_controller_casting_screen
            r0.setText(r1)
        L_0x0036:
            r0 = 1
        L_0x0037:
            r1 = 0
            goto L_0x006b
        L_0x0039:
            android.support.v4.media.session.PlaybackStateCompat r5 = r8.T
            if (r5 == 0) goto L_0x0063
            int r5 = r5.getState()
            if (r5 != 0) goto L_0x0044
            goto L_0x0063
        L_0x0044:
            if (r2 != 0) goto L_0x0050
            if (r4 != 0) goto L_0x0050
            android.widget.TextView r0 = r8.v
            int r1 = androidx.mediarouter.R$string.mr_controller_no_info_available
            r0.setText(r1)
            goto L_0x0036
        L_0x0050:
            if (r2 == 0) goto L_0x0059
            android.widget.TextView r2 = r8.v
            r2.setText(r0)
            r0 = 1
            goto L_0x005a
        L_0x0059:
            r0 = 0
        L_0x005a:
            if (r4 == 0) goto L_0x0037
            android.widget.TextView r2 = r8.w
            r2.setText(r1)
            r1 = 1
            goto L_0x006b
        L_0x0063:
            android.widget.TextView r0 = r8.v
            int r1 = androidx.mediarouter.R$string.mr_controller_no_media_selected
            r0.setText(r1)
            goto L_0x0036
        L_0x006b:
            android.widget.TextView r2 = r8.v
            r4 = 8
            if (r0 == 0) goto L_0x0073
            r0 = 0
            goto L_0x0075
        L_0x0073:
            r0 = 8
        L_0x0075:
            r2.setVisibility(r0)
            android.widget.TextView r0 = r8.w
            if (r1 == 0) goto L_0x007e
            r1 = 0
            goto L_0x0080
        L_0x007e:
            r1 = 8
        L_0x0080:
            r0.setVisibility(r1)
            android.support.v4.media.session.PlaybackStateCompat r0 = r8.T
            if (r0 == 0) goto L_0x00eb
            int r0 = r0.getState()
            r1 = 6
            if (r0 == r1) goto L_0x009a
            android.support.v4.media.session.PlaybackStateCompat r0 = r8.T
            int r0 = r0.getState()
            r1 = 3
            if (r0 != r1) goto L_0x0098
            goto L_0x009a
        L_0x0098:
            r0 = 0
            goto L_0x009b
        L_0x009a:
            r0 = 1
        L_0x009b:
            android.widget.ImageButton r1 = r8.n
            android.content.Context r1 = r1.getContext()
            if (r0 == 0) goto L_0x00ae
            boolean r2 = r8.d()
            if (r2 == 0) goto L_0x00ae
            int r0 = androidx.mediarouter.R$attr.mediaRoutePauseDrawable
            int r2 = androidx.mediarouter.R$string.mr_controller_pause
            goto L_0x00cb
        L_0x00ae:
            if (r0 == 0) goto L_0x00bb
            boolean r2 = r8.f()
            if (r2 == 0) goto L_0x00bb
            int r0 = androidx.mediarouter.R$attr.mediaRouteStopDrawable
            int r2 = androidx.mediarouter.R$string.mr_controller_stop
            goto L_0x00cb
        L_0x00bb:
            if (r0 != 0) goto L_0x00c8
            boolean r0 = r8.e()
            if (r0 == 0) goto L_0x00c8
            int r0 = androidx.mediarouter.R$attr.mediaRoutePlayDrawable
            int r2 = androidx.mediarouter.R$string.mr_controller_play
            goto L_0x00cb
        L_0x00c8:
            r0 = 0
            r2 = 0
            r3 = 0
        L_0x00cb:
            android.widget.ImageButton r5 = r8.n
            if (r3 == 0) goto L_0x00d0
            r4 = 0
        L_0x00d0:
            r5.setVisibility(r4)
            if (r3 == 0) goto L_0x00eb
            android.widget.ImageButton r3 = r8.n
            int r0 = androidx.mediarouter.app.MediaRouterThemeHelper.c(r1, r0)
            r3.setImageResource(r0)
            android.widget.ImageButton r0 = r8.n
            android.content.res.Resources r1 = r1.getResources()
            java.lang.CharSequence r1 = r1.getText(r2)
            r0.setContentDescription(r1)
        L_0x00eb:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.mediarouter.app.MediaRouteControllerDialog.p():void");
    }

    private void q() {
        int i2 = 8;
        if (!a(this.f)) {
            this.B.setVisibility(8);
        } else if (this.B.getVisibility() == 8) {
            this.B.setVisibility(0);
            this.J.setMax(this.f.q());
            this.J.setProgress(this.f.o());
            MediaRouteExpandCollapseButton mediaRouteExpandCollapseButton = this.p;
            if (n() != null) {
                i2 = 0;
            }
            mediaRouteExpandCollapseButton.setVisibility(i2);
        }
    }

    public View a(Bundle bundle) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public void b(View view) {
        b((View) (LinearLayout) view.findViewById(R$id.volume_item_container), this.N);
        View findViewById = view.findViewById(R$id.mr_volume_item_icon);
        ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
        int i2 = this.M;
        layoutParams.width = i2;
        layoutParams.height = i2;
        findViewById.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z2) {
        if (this.L != null) {
            this.e0 = true;
            this.f0 = z2 | this.f0;
            return;
        }
        int i2 = 0;
        this.e0 = false;
        this.f0 = false;
        if (!this.f.w() || this.f.t()) {
            dismiss();
        } else if (this.h) {
            this.x.setText(this.f.i());
            Button button = this.l;
            if (!this.f.a()) {
                i2 = 8;
            }
            button.setVisibility(i2);
            if (this.k == null && this.Y) {
                if (a(this.c0)) {
                    Log.w("MediaRouteCtrlDialog", "Can't set artwork image with recycled bitmap: " + this.c0);
                } else {
                    this.u.setImageBitmap(this.c0);
                    this.u.setBackgroundColor(this.d0);
                }
                c();
            }
            q();
            p();
            d(z2);
        }
    }

    /* access modifiers changed from: package-private */
    public void d(final boolean z2) {
        this.s.requestLayout();
        this.s.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                MediaRouteControllerDialog.this.s.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                MediaRouteControllerDialog mediaRouteControllerDialog = MediaRouteControllerDialog.this;
                if (mediaRouteControllerDialog.h0) {
                    mediaRouteControllerDialog.i0 = true;
                } else {
                    mediaRouteControllerDialog.e(z2);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void e(boolean z2) {
        int i2;
        int i3;
        Bitmap bitmap;
        int c = c((View) this.z);
        b((View) this.z, -1);
        h(l());
        View decorView = getWindow().getDecorView();
        decorView.measure(View.MeasureSpec.makeMeasureSpec(getWindow().getAttributes().width, 1073741824), 0);
        b((View) this.z, c);
        if (this.k != null || !(this.u.getDrawable() instanceof BitmapDrawable) || (bitmap = ((BitmapDrawable) this.u.getDrawable()).getBitmap()) == null) {
            i2 = 0;
        } else {
            i2 = a(bitmap.getWidth(), bitmap.getHeight());
            this.u.setScaleType(bitmap.getWidth() >= bitmap.getHeight() ? ImageView.ScaleType.FIT_XY : ImageView.ScaleType.FIT_CENTER);
        }
        int f2 = f(l());
        int size = this.F.size();
        if (n() == null) {
            i3 = 0;
        } else {
            i3 = this.N * n().y().size();
        }
        if (size > 0) {
            i3 += this.P;
        }
        int min = Math.min(i3, this.O);
        if (!this.g0) {
            min = 0;
        }
        int max = Math.max(i2, min) + f2;
        Rect rect = new Rect();
        decorView.getWindowVisibleDisplayFrame(rect);
        int height = rect.height() - (this.r.getMeasuredHeight() - this.s.getMeasuredHeight());
        if (this.k != null || i2 <= 0 || max > height) {
            if (c((View) this.D) + this.z.getMeasuredHeight() >= this.s.getMeasuredHeight()) {
                this.u.setVisibility(8);
            }
            max = min + f2;
            i2 = 0;
        } else {
            this.u.setVisibility(0);
            b((View) this.u, i2);
        }
        if (!l() || max > height) {
            this.A.setVisibility(8);
        } else {
            this.A.setVisibility(0);
        }
        boolean z3 = true;
        h(this.A.getVisibility() == 0);
        if (this.A.getVisibility() != 0) {
            z3 = false;
        }
        int f3 = f(z3);
        int max2 = Math.max(i2, min) + f3;
        if (max2 > height) {
            min -= max2 - height;
            max2 = height;
        }
        this.z.clearAnimation();
        this.D.clearAnimation();
        this.s.clearAnimation();
        if (z2) {
            a((View) this.z, f3);
            a((View) this.D, min);
            a((View) this.s, max2);
        } else {
            b((View) this.z, f3);
            b((View) this.D, min);
            b((View) this.s, max2);
        }
        b((View) this.q, rect.height());
        g(z2);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (Build.VERSION.SDK_INT >= 21) {
            this.m0 = this.g0 ? this.n0 : this.o0;
        } else {
            this.m0 = this.p0;
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        Set<MediaRouter.RouteInfo> set = this.G;
        if (set == null || set.size() == 0) {
            b(true);
        } else {
            m();
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (this.k == null && o()) {
            FetchArtTask fetchArtTask = this.V;
            if (fetchArtTask != null) {
                fetchArtTask.cancel(true);
            }
            this.V = new FetchArtTask();
            this.V.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        int a2 = MediaRouteDialogHelper.a(this.g);
        getWindow().setLayout(a2, -2);
        View decorView = getWindow().getDecorView();
        this.j = (a2 - decorView.getPaddingLeft()) - decorView.getPaddingRight();
        Resources resources = this.g.getResources();
        this.M = resources.getDimensionPixelSize(R$dimen.mr_controller_volume_group_list_item_icon_size);
        this.N = resources.getDimensionPixelSize(R$dimen.mr_controller_volume_group_list_item_height);
        this.O = resources.getDimensionPixelSize(R$dimen.mr_controller_volume_group_list_max_height);
        this.W = null;
        this.X = null;
        j();
        c(false);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.i = true;
        this.d.a(MediaRouteSelector.c, this.e, 2);
        a(this.d.b());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setBackgroundDrawableResource(17170445);
        setContentView(R$layout.mr_controller_material_dialog_b);
        findViewById(16908315).setVisibility(8);
        ClickListener clickListener = new ClickListener();
        this.q = (FrameLayout) findViewById(R$id.mr_expandable_area);
        this.q.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaRouteControllerDialog.this.dismiss();
            }
        });
        this.r = (LinearLayout) findViewById(R$id.mr_dialog_area);
        this.r.setOnClickListener(new View.OnClickListener(this) {
            public void onClick(View view) {
            }
        });
        int c = MediaRouterThemeHelper.c(this.g);
        this.l = (Button) findViewById(16908314);
        this.l.setText(R$string.mr_controller_disconnect);
        this.l.setTextColor(c);
        this.l.setOnClickListener(clickListener);
        this.m = (Button) findViewById(16908313);
        this.m.setText(R$string.mr_controller_stop_casting);
        this.m.setTextColor(c);
        this.m.setOnClickListener(clickListener);
        this.x = (TextView) findViewById(R$id.mr_name);
        this.o = (ImageButton) findViewById(R$id.mr_close);
        this.o.setOnClickListener(clickListener);
        this.t = (FrameLayout) findViewById(R$id.mr_custom_control);
        this.s = (FrameLayout) findViewById(R$id.mr_default_control);
        AnonymousClass4 r2 = new View.OnClickListener() {
            public void onClick(View view) {
                PendingIntent sessionActivity;
                MediaControllerCompat mediaControllerCompat = MediaRouteControllerDialog.this.R;
                if (mediaControllerCompat != null && (sessionActivity = mediaControllerCompat.getSessionActivity()) != null) {
                    try {
                        sessionActivity.send();
                        MediaRouteControllerDialog.this.dismiss();
                    } catch (PendingIntent.CanceledException unused) {
                        Log.e("MediaRouteCtrlDialog", sessionActivity + " was not sent, it had been canceled.");
                    }
                }
            }
        };
        this.u = (ImageView) findViewById(R$id.mr_art);
        this.u.setOnClickListener(r2);
        findViewById(R$id.mr_control_title_container).setOnClickListener(r2);
        this.z = (LinearLayout) findViewById(R$id.mr_media_main_control);
        this.C = findViewById(R$id.mr_control_divider);
        this.A = (RelativeLayout) findViewById(R$id.mr_playback_control);
        this.v = (TextView) findViewById(R$id.mr_control_title);
        this.w = (TextView) findViewById(R$id.mr_control_subtitle);
        this.n = (ImageButton) findViewById(R$id.mr_control_playback_ctrl);
        this.n.setOnClickListener(clickListener);
        this.B = (LinearLayout) findViewById(R$id.mr_volume_control);
        this.B.setVisibility(8);
        this.J = (SeekBar) findViewById(R$id.mr_volume_slider);
        this.J.setTag(this.f);
        this.K = new VolumeChangeListener();
        this.J.setOnSeekBarChangeListener(this.K);
        this.D = (OverlayListView) findViewById(R$id.mr_volume_group_list);
        this.F = new ArrayList();
        this.E = new VolumeGroupAdapter(this.D.getContext(), this.F);
        this.D.setAdapter(this.E);
        this.I = new HashSet();
        MediaRouterThemeHelper.a(this.g, this.z, this.D, n() != null);
        MediaRouterThemeHelper.a(this.g, (MediaRouteVolumeSlider) this.J, (View) this.z);
        this.Q = new HashMap();
        this.Q.put(this.f, this.J);
        this.p = (MediaRouteExpandCollapseButton) findViewById(R$id.mr_group_expand_collapse);
        this.p.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaRouteControllerDialog mediaRouteControllerDialog = MediaRouteControllerDialog.this;
                mediaRouteControllerDialog.g0 = !mediaRouteControllerDialog.g0;
                if (mediaRouteControllerDialog.g0) {
                    mediaRouteControllerDialog.D.setVisibility(0);
                }
                MediaRouteControllerDialog.this.g();
                MediaRouteControllerDialog.this.d(true);
            }
        });
        g();
        this.j0 = this.g.getResources().getInteger(R$integer.mr_controller_volume_group_list_animation_duration_ms);
        this.k0 = this.g.getResources().getInteger(R$integer.mr_controller_volume_group_list_fade_in_duration_ms);
        this.l0 = this.g.getResources().getInteger(R$integer.mr_controller_volume_group_list_fade_out_duration_ms);
        this.k = a(bundle);
        View view = this.k;
        if (view != null) {
            this.t.addView(view);
            this.t.setVisibility(0);
        }
        this.h = true;
        k();
    }

    public void onDetachedFromWindow() {
        this.d.a((MediaRouter.Callback) this.e);
        a((MediaSessionCompat.Token) null);
        this.i = false;
        super.onDetachedFromWindow();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 25 && i2 != 24) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.f.b(i2 == 25 ? -1 : 1);
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 25 || i2 == 24) {
            return true;
        }
        return super.onKeyUp(i2, keyEvent);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MediaRouteControllerDialog(android.content.Context r2, int r3) {
        /*
            r1 = this;
            r0 = 1
            android.content.Context r2 = androidx.mediarouter.app.MediaRouterThemeHelper.a((android.content.Context) r2, (int) r3, (boolean) r0)
            int r3 = androidx.mediarouter.app.MediaRouterThemeHelper.b(r2)
            r1.<init>(r2, r3)
            r1.y = r0
            androidx.mediarouter.app.MediaRouteControllerDialog$1 r3 = new androidx.mediarouter.app.MediaRouteControllerDialog$1
            r3.<init>()
            r1.r0 = r3
            android.content.Context r3 = r1.getContext()
            r1.g = r3
            androidx.mediarouter.app.MediaRouteControllerDialog$MediaControllerCallback r3 = new androidx.mediarouter.app.MediaRouteControllerDialog$MediaControllerCallback
            r3.<init>()
            r1.S = r3
            android.content.Context r3 = r1.g
            androidx.mediarouter.media.MediaRouter r3 = androidx.mediarouter.media.MediaRouter.a((android.content.Context) r3)
            r1.d = r3
            androidx.mediarouter.app.MediaRouteControllerDialog$MediaRouterCallback r3 = new androidx.mediarouter.app.MediaRouteControllerDialog$MediaRouterCallback
            r3.<init>()
            r1.e = r3
            androidx.mediarouter.media.MediaRouter r3 = r1.d
            androidx.mediarouter.media.MediaRouter$RouteInfo r3 = r3.d()
            r1.f = r3
            androidx.mediarouter.media.MediaRouter r3 = r1.d
            android.support.v4.media.session.MediaSessionCompat$Token r3 = r3.b()
            r1.a((android.support.v4.media.session.MediaSessionCompat.Token) r3)
            android.content.Context r3 = r1.g
            android.content.res.Resources r3 = r3.getResources()
            int r0 = androidx.mediarouter.R$dimen.mr_controller_volume_group_list_padding_top
            int r3 = r3.getDimensionPixelSize(r0)
            r1.P = r3
            android.content.Context r3 = r1.g
            java.lang.String r0 = "accessibility"
            java.lang.Object r3 = r3.getSystemService(r0)
            android.view.accessibility.AccessibilityManager r3 = (android.view.accessibility.AccessibilityManager) r3
            r1.q0 = r3
            int r3 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r3 < r0) goto L_0x0072
            int r3 = androidx.mediarouter.R$interpolator.mr_linear_out_slow_in
            android.view.animation.Interpolator r3 = android.view.animation.AnimationUtils.loadInterpolator(r2, r3)
            r1.n0 = r3
            int r3 = androidx.mediarouter.R$interpolator.mr_fast_out_slow_in
            android.view.animation.Interpolator r2 = android.view.animation.AnimationUtils.loadInterpolator(r2, r3)
            r1.o0 = r2
        L_0x0072:
            android.view.animation.AccelerateDecelerateInterpolator r2 = new android.view.animation.AccelerateDecelerateInterpolator
            r2.<init>()
            r1.p0 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.mediarouter.app.MediaRouteControllerDialog.<init>(android.content.Context, int):void");
    }

    /* access modifiers changed from: package-private */
    public void h() {
        a(true);
        this.D.requestLayout();
        this.D.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                MediaRouteControllerDialog.this.D.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                MediaRouteControllerDialog.this.i();
            }
        });
    }

    private void g(boolean z2) {
        List<MediaRouter.RouteInfo> y2 = n() == null ? null : n().y();
        if (y2 == null) {
            this.F.clear();
            this.E.notifyDataSetChanged();
        } else if (MediaRouteDialogHelper.c(this.F, y2)) {
            this.E.notifyDataSetChanged();
        } else {
            HashMap a2 = z2 ? MediaRouteDialogHelper.a((ListView) this.D, this.E) : null;
            HashMap a3 = z2 ? MediaRouteDialogHelper.a(this.g, this.D, this.E) : null;
            this.G = MediaRouteDialogHelper.a(this.F, y2);
            this.H = MediaRouteDialogHelper.b(this.F, y2);
            this.F.addAll(0, this.G);
            this.F.removeAll(this.H);
            this.E.notifyDataSetChanged();
            if (!z2 || !this.g0 || this.G.size() + this.H.size() <= 0) {
                this.G = null;
                this.H = null;
                return;
            }
            b((Map<MediaRouter.RouteInfo, Rect>) a2, (Map<MediaRouter.RouteInfo, BitmapDrawable>) a3);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return (this.T.getActions() & 514) != 0;
    }

    private void b(final Map<MediaRouter.RouteInfo, Rect> map, final Map<MediaRouter.RouteInfo, BitmapDrawable> map2) {
        this.D.setEnabled(false);
        this.D.requestLayout();
        this.h0 = true;
        this.D.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                MediaRouteControllerDialog.this.D.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                MediaRouteControllerDialog.this.a((Map<MediaRouter.RouteInfo, Rect>) map, (Map<MediaRouter.RouteInfo, BitmapDrawable>) map2);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return (this.T.getActions() & 1) != 0;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.G = null;
        this.H = null;
        this.h0 = false;
        if (this.i0) {
            this.i0 = false;
            d(z2);
        }
        this.D.setEnabled(true);
    }

    static boolean a(Bitmap bitmap) {
        return bitmap != null && bitmap.isRecycled();
    }

    private void a(final View view, final int i2) {
        final int c = c(view);
        AnonymousClass7 r1 = new Animation(this) {
            /* access modifiers changed from: protected */
            public void applyTransformation(float f, Transformation transformation) {
                int i = c;
                MediaRouteControllerDialog.b(view, i - ((int) (((float) (i - i2)) * f)));
            }
        };
        r1.setDuration((long) this.j0);
        if (Build.VERSION.SDK_INT >= 21) {
            r1.setInterpolator(this.m0);
        }
        view.startAnimation(r1);
    }

    static void b(View view, int i2) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = i2;
        view.setLayoutParams(layoutParams);
    }

    private static int c(View view) {
        return view.getLayoutParams().height;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.Y = false;
        this.c0 = null;
        this.d0 = 0;
    }

    /* access modifiers changed from: package-private */
    public void a(Map<MediaRouter.RouteInfo, Rect> map, Map<MediaRouter.RouteInfo, BitmapDrawable> map2) {
        OverlayListView.OverlayObject overlayObject;
        Map<MediaRouter.RouteInfo, Rect> map3 = map;
        Set<MediaRouter.RouteInfo> set = this.G;
        if (set != null && this.H != null) {
            int size = set.size() - this.H.size();
            AnonymousClass9 r3 = new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                    MediaRouteControllerDialog.this.D.a();
                    MediaRouteControllerDialog mediaRouteControllerDialog = MediaRouteControllerDialog.this;
                    mediaRouteControllerDialog.D.postDelayed(mediaRouteControllerDialog.r0, (long) mediaRouteControllerDialog.j0);
                }
            };
            int firstVisiblePosition = this.D.getFirstVisiblePosition();
            boolean z2 = false;
            for (int i2 = 0; i2 < this.D.getChildCount(); i2++) {
                View childAt = this.D.getChildAt(i2);
                MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) this.E.getItem(firstVisiblePosition + i2);
                Rect rect = map3.get(routeInfo);
                int top = childAt.getTop();
                int i3 = rect != null ? rect.top : (this.N * size) + top;
                AnimationSet animationSet = new AnimationSet(true);
                Set<MediaRouter.RouteInfo> set2 = this.G;
                if (set2 != null && set2.contains(routeInfo)) {
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 0.0f);
                    alphaAnimation.setDuration((long) this.k0);
                    animationSet.addAnimation(alphaAnimation);
                    i3 = top;
                }
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (i3 - top), 0.0f);
                translateAnimation.setDuration((long) this.j0);
                animationSet.addAnimation(translateAnimation);
                animationSet.setFillAfter(true);
                animationSet.setFillEnabled(true);
                animationSet.setInterpolator(this.m0);
                if (!z2) {
                    animationSet.setAnimationListener(r3);
                    z2 = true;
                }
                childAt.clearAnimation();
                childAt.startAnimation(animationSet);
                map3.remove(routeInfo);
                map2.remove(routeInfo);
            }
            Map<MediaRouter.RouteInfo, BitmapDrawable> map4 = map2;
            for (Map.Entry next : map2.entrySet()) {
                final MediaRouter.RouteInfo routeInfo2 = (MediaRouter.RouteInfo) next.getKey();
                BitmapDrawable bitmapDrawable = (BitmapDrawable) next.getValue();
                Rect rect2 = map3.get(routeInfo2);
                if (this.H.contains(routeInfo2)) {
                    overlayObject = new OverlayListView.OverlayObject(bitmapDrawable, rect2);
                    overlayObject.a(1.0f, 0.0f);
                    overlayObject.a((long) this.l0);
                    overlayObject.a(this.m0);
                } else {
                    OverlayListView.OverlayObject overlayObject2 = new OverlayListView.OverlayObject(bitmapDrawable, rect2);
                    overlayObject2.a(this.N * size);
                    overlayObject2.a((long) this.j0);
                    overlayObject2.a(this.m0);
                    overlayObject2.a((OverlayListView.OverlayObject.OnAnimationEndListener) new OverlayListView.OverlayObject.OnAnimationEndListener() {
                        public void onAnimationEnd() {
                            MediaRouteControllerDialog.this.I.remove(routeInfo2);
                            MediaRouteControllerDialog.this.E.notifyDataSetChanged();
                        }
                    });
                    this.I.add(routeInfo2);
                    overlayObject = overlayObject2;
                }
                this.D.a(overlayObject);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return (this.T.getActions() & 516) != 0;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        Set<MediaRouter.RouteInfo> set;
        int firstVisiblePosition = this.D.getFirstVisiblePosition();
        for (int i2 = 0; i2 < this.D.getChildCount(); i2++) {
            View childAt = this.D.getChildAt(i2);
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) this.E.getItem(firstVisiblePosition + i2);
            if (!z2 || (set = this.G) == null || !set.contains(routeInfo)) {
                ((LinearLayout) childAt.findViewById(R$id.volume_item_container)).setVisibility(0);
                AnimationSet animationSet = new AnimationSet(true);
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 1.0f);
                alphaAnimation.setDuration(0);
                animationSet.addAnimation(alphaAnimation);
                new TranslateAnimation(0.0f, 0.0f, 0.0f, 0.0f).setDuration(0);
                animationSet.setFillAfter(true);
                animationSet.setFillEnabled(true);
                childAt.clearAnimation();
                childAt.startAnimation(animationSet);
            }
        }
        this.D.b();
        if (!z2) {
            b(false);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(MediaRouter.RouteInfo routeInfo) {
        return this.y && routeInfo.p() == 1;
    }

    private static boolean a(Uri uri, Uri uri2) {
        if (uri == null || !uri.equals(uri2)) {
            return uri == null && uri2 == null;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3) {
        if (i2 >= i3) {
            return (int) (((((float) this.j) * ((float) i3)) / ((float) i2)) + 0.5f);
        }
        return (int) (((((float) this.j) * 9.0f) / 16.0f) + 0.5f);
    }
}
