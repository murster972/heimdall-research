package androidx.mediarouter.app;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import androidx.fragment.app.DialogFragment;
import androidx.mediarouter.media.MediaRouteSelector;

public class MediaRouteControllerDialogFragment extends DialogFragment {
    private static final boolean c = Log.isLoggable("UseSupportDynamicGroup", 3);

    /* renamed from: a  reason: collision with root package name */
    private Dialog f815a;
    private MediaRouteSelector b;

    public MediaRouteControllerDialogFragment() {
        setCancelable(true);
    }

    private void e() {
        if (this.b == null) {
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.b = MediaRouteSelector.a(arguments.getBundle("selector"));
            }
            if (this.b == null) {
                this.b = MediaRouteSelector.c;
            }
        }
    }

    public void a(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector != null) {
            e();
            if (!this.b.equals(mediaRouteSelector)) {
                this.b = mediaRouteSelector;
                Bundle arguments = getArguments();
                if (arguments == null) {
                    arguments = new Bundle();
                }
                arguments.putBundle("selector", mediaRouteSelector.a());
                setArguments(arguments);
                Dialog dialog = this.f815a;
                if (dialog != null && c) {
                    ((MediaRouteCastDialog) dialog).a(mediaRouteSelector);
                    return;
                }
                return;
            }
            return;
        }
        throw new IllegalArgumentException("selector must not be null");
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        Dialog dialog = this.f815a;
        if (dialog == null) {
            return;
        }
        if (c) {
            ((MediaRouteCastDialog) dialog).f();
        } else {
            ((MediaRouteControllerDialog) dialog).k();
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        if (c) {
            this.f815a = a(getContext());
            ((MediaRouteCastDialog) this.f815a).a(this.b);
        } else {
            this.f815a = a(getContext(), bundle);
        }
        return this.f815a;
    }

    public void onStop() {
        super.onStop();
        Dialog dialog = this.f815a;
        if (dialog != null && !c) {
            ((MediaRouteControllerDialog) dialog).a(false);
        }
    }

    public MediaRouteCastDialog a(Context context) {
        return new MediaRouteCastDialog(context);
    }

    public MediaRouteControllerDialog a(Context context, Bundle bundle) {
        return new MediaRouteControllerDialog(context);
    }
}
