package androidx.mediarouter;

public final class R$string {
    public static final int abc_action_bar_home_description = 2131755014;
    public static final int abc_action_bar_up_description = 2131755015;
    public static final int abc_action_menu_overflow_description = 2131755016;
    public static final int abc_action_mode_done = 2131755017;
    public static final int abc_activity_chooser_view_see_all = 2131755018;
    public static final int abc_activitychooserview_choose_application = 2131755019;
    public static final int abc_capital_off = 2131755020;
    public static final int abc_capital_on = 2131755021;
    public static final int abc_menu_alt_shortcut_label = 2131755022;
    public static final int abc_menu_ctrl_shortcut_label = 2131755023;
    public static final int abc_menu_delete_shortcut_label = 2131755024;
    public static final int abc_menu_enter_shortcut_label = 2131755025;
    public static final int abc_menu_function_shortcut_label = 2131755026;
    public static final int abc_menu_meta_shortcut_label = 2131755027;
    public static final int abc_menu_shift_shortcut_label = 2131755028;
    public static final int abc_menu_space_shortcut_label = 2131755029;
    public static final int abc_menu_sym_shortcut_label = 2131755030;
    public static final int abc_prepend_shortcut_label = 2131755031;
    public static final int abc_search_hint = 2131755032;
    public static final int abc_searchview_description_clear = 2131755033;
    public static final int abc_searchview_description_query = 2131755034;
    public static final int abc_searchview_description_search = 2131755035;
    public static final int abc_searchview_description_submit = 2131755036;
    public static final int abc_searchview_description_voice = 2131755037;
    public static final int abc_shareactionprovider_share_with = 2131755038;
    public static final int abc_shareactionprovider_share_with_application = 2131755039;
    public static final int abc_toolbar_collapse_description = 2131755040;
    public static final int mr_button_content_description = 2131755405;
    public static final int mr_cast_button_connected = 2131755406;
    public static final int mr_cast_button_connecting = 2131755407;
    public static final int mr_cast_button_disconnected = 2131755408;
    public static final int mr_cast_dialog_title_view_placeholder = 2131755409;
    public static final int mr_chooser_searching = 2131755410;
    public static final int mr_chooser_title = 2131755411;
    public static final int mr_controller_album_art = 2131755412;
    public static final int mr_controller_casting_screen = 2131755413;
    public static final int mr_controller_close_description = 2131755414;
    public static final int mr_controller_collapse_group = 2131755415;
    public static final int mr_controller_disconnect = 2131755416;
    public static final int mr_controller_expand_group = 2131755417;
    public static final int mr_controller_no_info_available = 2131755418;
    public static final int mr_controller_no_media_selected = 2131755419;
    public static final int mr_controller_pause = 2131755420;
    public static final int mr_controller_play = 2131755421;
    public static final int mr_controller_stop = 2131755422;
    public static final int mr_controller_stop_casting = 2131755423;
    public static final int mr_controller_volume_slider = 2131755424;
    public static final int mr_dialog_device_header = 2131755425;
    public static final int mr_dialog_route_header = 2131755426;
    public static final int mr_system_route_name = 2131755427;
    public static final int mr_user_route_category_name = 2131755428;
    public static final int search_menu_title = 2131755523;
    public static final int status_bar_notification_info_overflow = 2131755551;

    private R$string() {
    }
}
