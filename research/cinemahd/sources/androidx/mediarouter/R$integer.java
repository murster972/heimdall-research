package androidx.mediarouter;

public final class R$integer {
    public static final int abc_config_activityDefaultDur = 2131361792;
    public static final int abc_config_activityShortDur = 2131361793;
    public static final int cancel_button_image_alpha = 2131361796;
    public static final int config_tooltipAnimTime = 2131361799;
    public static final int mr_controller_volume_group_list_animation_duration_ms = 2131361807;
    public static final int mr_controller_volume_group_list_fade_in_duration_ms = 2131361808;
    public static final int mr_controller_volume_group_list_fade_out_duration_ms = 2131361809;
    public static final int mr_update_routes_delay_ms = 2131361810;
    public static final int status_bar_notification_info_maxnum = 2131361818;

    private R$integer() {
    }
}
