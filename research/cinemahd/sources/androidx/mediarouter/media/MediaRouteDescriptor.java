package androidx.mediarouter.media;

import android.content.IntentFilter;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.react.uimanager.ViewProps;
import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class MediaRouteDescriptor {

    /* renamed from: a  reason: collision with root package name */
    final Bundle f832a;
    List<IntentFilter> b;

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final Bundle f833a;
        private ArrayList<String> b;
        private ArrayList<IntentFilter> c;

        public Builder(String str, String str2) {
            this.f833a = new Bundle();
            b(str);
            c(str2);
        }

        public Builder a(String str) {
            this.f833a.putString(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, str);
            return this;
        }

        public Builder b(String str) {
            this.f833a.putString("id", str);
            return this;
        }

        public Builder c(String str) {
            this.f833a.putString(MediationMetaData.KEY_NAME, str);
            return this;
        }

        public Builder d(int i) {
            this.f833a.putInt("presentationDisplayId", i);
            return this;
        }

        public Builder e(int i) {
            this.f833a.putInt("volume", i);
            return this;
        }

        public Builder f(int i) {
            this.f833a.putInt("volumeHandling", i);
            return this;
        }

        public Builder g(int i) {
            this.f833a.putInt("volumeMax", i);
            return this;
        }

        @Deprecated
        public Builder a(boolean z) {
            this.f833a.putBoolean("connecting", z);
            return this;
        }

        public Builder b(boolean z) {
            this.f833a.putBoolean(ViewProps.ENABLED, z);
            return this;
        }

        public Builder c(int i) {
            this.f833a.putInt("playbackType", i);
            return this;
        }

        public Builder a(IntentFilter intentFilter) {
            if (intentFilter != null) {
                if (this.c == null) {
                    this.c = new ArrayList<>();
                }
                if (!this.c.contains(intentFilter)) {
                    this.c.add(intentFilter);
                }
                return this;
            }
            throw new IllegalArgumentException("filter must not be null");
        }

        public Builder b(int i) {
            this.f833a.putInt("playbackStream", i);
            return this;
        }

        public Builder(MediaRouteDescriptor mediaRouteDescriptor) {
            if (mediaRouteDescriptor != null) {
                this.f833a = new Bundle(mediaRouteDescriptor.f832a);
                mediaRouteDescriptor.c();
                if (!mediaRouteDescriptor.b.isEmpty()) {
                    this.c = new ArrayList<>(mediaRouteDescriptor.b);
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("descriptor must not be null");
        }

        public Builder a(Collection<IntentFilter> collection) {
            if (collection != null) {
                if (!collection.isEmpty()) {
                    for (IntentFilter a2 : collection) {
                        a(a2);
                    }
                }
                return this;
            }
            throw new IllegalArgumentException("filters must not be null");
        }

        public Builder a(int i) {
            this.f833a.putInt("deviceType", i);
            return this;
        }

        public MediaRouteDescriptor a() {
            ArrayList<IntentFilter> arrayList = this.c;
            if (arrayList != null) {
                this.f833a.putParcelableArrayList("controlFilters", arrayList);
            }
            ArrayList<String> arrayList2 = this.b;
            if (arrayList2 != null) {
                this.f833a.putStringArrayList("groupMemberIds", arrayList2);
            }
            return new MediaRouteDescriptor(this.f833a, this.c);
        }
    }

    MediaRouteDescriptor(Bundle bundle, List<IntentFilter> list) {
        this.f832a = bundle;
        this.b = list;
    }

    public Bundle a() {
        return this.f832a;
    }

    public boolean b() {
        return this.f832a.getBoolean("canDisconnect", false);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.b == null) {
            this.b = this.f832a.getParcelableArrayList("controlFilters");
            if (this.b == null) {
                this.b = Collections.emptyList();
            }
        }
    }

    public int d() {
        return this.f832a.getInt("connectionState", 0);
    }

    public List<IntentFilter> e() {
        c();
        return this.b;
    }

    public String f() {
        return this.f832a.getString(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS);
    }

    public int g() {
        return this.f832a.getInt("deviceType");
    }

    public Bundle h() {
        return this.f832a.getBundle("extras");
    }

    public List<String> i() {
        return this.f832a.getStringArrayList("groupMemberIds");
    }

    public Uri j() {
        String string = this.f832a.getString("iconUri");
        if (string == null) {
            return null;
        }
        return Uri.parse(string);
    }

    public String k() {
        return this.f832a.getString("id");
    }

    public int l() {
        return this.f832a.getInt("maxClientVersion", Integer.MAX_VALUE);
    }

    public int m() {
        return this.f832a.getInt("minClientVersion", 1);
    }

    public String n() {
        return this.f832a.getString(MediationMetaData.KEY_NAME);
    }

    public int o() {
        return this.f832a.getInt("playbackStream", -1);
    }

    public int p() {
        return this.f832a.getInt("playbackType", 1);
    }

    public int q() {
        return this.f832a.getInt("presentationDisplayId", -1);
    }

    public IntentSender r() {
        return (IntentSender) this.f832a.getParcelable("settingsIntent");
    }

    public int s() {
        return this.f832a.getInt("volume");
    }

    public int t() {
        return this.f832a.getInt("volumeHandling", 0);
    }

    public String toString() {
        return "MediaRouteDescriptor{ " + "id=" + k() + ", groupMemberIds=" + i() + ", name=" + n() + ", description=" + f() + ", iconUri=" + j() + ", isEnabled=" + w() + ", isConnecting=" + v() + ", connectionState=" + d() + ", controlFilters=" + Arrays.toString(e().toArray()) + ", playbackType=" + p() + ", playbackStream=" + o() + ", deviceType=" + g() + ", volume=" + s() + ", volumeMax=" + u() + ", volumeHandling=" + t() + ", presentationDisplayId=" + q() + ", extras=" + h() + ", isValid=" + x() + ", minClientVersion=" + m() + ", maxClientVersion=" + l() + " }";
    }

    public int u() {
        return this.f832a.getInt("volumeMax");
    }

    @Deprecated
    public boolean v() {
        return this.f832a.getBoolean("connecting", false);
    }

    public boolean w() {
        return this.f832a.getBoolean(ViewProps.ENABLED, true);
    }

    public boolean x() {
        c();
        return !TextUtils.isEmpty(k()) && !TextUtils.isEmpty(n()) && !this.b.contains((Object) null);
    }

    public static MediaRouteDescriptor a(Bundle bundle) {
        if (bundle != null) {
            return new MediaRouteDescriptor(bundle, (List<IntentFilter>) null);
        }
        return null;
    }
}
