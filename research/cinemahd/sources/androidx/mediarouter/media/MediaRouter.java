package androidx.mediarouter.media;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.media.session.MediaSessionCompat;
import android.text.TextUtils;
import android.util.Log;
import androidx.core.app.ActivityManagerCompat;
import androidx.core.hardware.display.DisplayManagerCompat;
import androidx.core.util.ObjectsCompat;
import androidx.core.util.Pair;
import androidx.media.VolumeProviderCompat;
import androidx.mediarouter.media.MediaRouteProvider;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.RegisteredMediaRouteProviderWatcher;
import androidx.mediarouter.media.RemoteControlClientCompat;
import androidx.mediarouter.media.SystemMediaRouteProvider;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class MediaRouter {
    static final boolean c = Log.isLoggable("MediaRouter", 3);
    static GlobalMediaRouter d;

    /* renamed from: a  reason: collision with root package name */
    final Context f842a;
    final ArrayList<CallbackRecord> b = new ArrayList<>();

    public static abstract class Callback {
        public void onProviderAdded(MediaRouter mediaRouter, ProviderInfo providerInfo) {
        }

        public void onProviderChanged(MediaRouter mediaRouter, ProviderInfo providerInfo) {
        }

        public void onProviderRemoved(MediaRouter mediaRouter, ProviderInfo providerInfo) {
        }

        public void onRouteAdded(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        public void onRouteChanged(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        public void onRoutePresentationDisplayChanged(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        public void onRouteRemoved(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        public void onRouteSelected(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        public void onRouteUnselected(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        public void onRouteUnselected(MediaRouter mediaRouter, RouteInfo routeInfo, int i) {
            onRouteUnselected(mediaRouter, routeInfo);
        }

        public void onRouteVolumeChanged(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }
    }

    private static final class CallbackRecord {

        /* renamed from: a  reason: collision with root package name */
        public final MediaRouter f843a;
        public final Callback b;
        public MediaRouteSelector c = MediaRouteSelector.c;
        public int d;

        public CallbackRecord(MediaRouter mediaRouter, Callback callback) {
            this.f843a = mediaRouter;
            this.b = callback;
        }

        public boolean a(RouteInfo routeInfo) {
            return (this.d & 2) != 0 || routeInfo.a(this.c);
        }
    }

    public static abstract class ControlRequestCallback {
        public void a(Bundle bundle) {
        }

        public void a(String str, Bundle bundle) {
        }
    }

    public static final class ProviderInfo {

        /* renamed from: a  reason: collision with root package name */
        final MediaRouteProvider f852a;
        final List<RouteInfo> b = new ArrayList();
        private final MediaRouteProvider.ProviderMetadata c;
        private MediaRouteProviderDescriptor d;

        ProviderInfo(MediaRouteProvider mediaRouteProvider) {
            this.f852a = mediaRouteProvider;
            this.c = mediaRouteProvider.g();
        }

        public ComponentName a() {
            return this.c.a();
        }

        public String b() {
            return this.c.b();
        }

        public MediaRouteProvider c() {
            MediaRouter.e();
            return this.f852a;
        }

        public String toString() {
            return "MediaRouter.RouteProviderInfo{ packageName=" + b() + " }";
        }

        /* access modifiers changed from: package-private */
        public boolean a(MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
            if (this.d == mediaRouteProviderDescriptor) {
                return false;
            }
            this.d = mediaRouteProviderDescriptor;
            return true;
        }

        /* access modifiers changed from: package-private */
        public int a(String str) {
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                if (this.b.get(i).b.equals(str)) {
                    return i;
                }
            }
            return -1;
        }
    }

    public static class RouteGroup extends RouteInfo {
        private List<RouteInfo> v = new ArrayList();

        RouteGroup(ProviderInfo providerInfo, String str, String str2) {
            super(providerInfo, str, str2);
        }

        /* access modifiers changed from: package-private */
        public int a(MediaRouteDescriptor mediaRouteDescriptor) {
            int i = 0;
            if (this.u != mediaRouteDescriptor) {
                this.u = mediaRouteDescriptor;
                if (mediaRouteDescriptor != null) {
                    List<String> i2 = mediaRouteDescriptor.i();
                    ArrayList arrayList = new ArrayList();
                    if (i2 == null) {
                        Log.w("MediaRouter", "groupMemberIds shouldn't be null.");
                        i = 1;
                    } else {
                        if (i2.size() != this.v.size()) {
                            i = 1;
                        }
                        for (String a2 : i2) {
                            RouteInfo b = MediaRouter.d.b(MediaRouter.d.a(m(), a2));
                            if (b != null) {
                                arrayList.add(b);
                                if (i == 0 && !this.v.contains(b)) {
                                    i = 1;
                                }
                            }
                        }
                    }
                    if (i != 0) {
                        this.v = arrayList;
                    }
                }
            }
            return super.b(mediaRouteDescriptor) | i;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(super.toString());
            sb.append('[');
            int size = this.v.size();
            for (int i = 0; i < size; i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(this.v.get(i));
            }
            sb.append(']');
            return sb.toString();
        }

        public List<RouteInfo> y() {
            return this.v;
        }
    }

    public static class RouteInfo {

        /* renamed from: a  reason: collision with root package name */
        private final ProviderInfo f853a;
        final String b;
        final String c;
        private String d;
        private String e;
        private Uri f;
        boolean g;
        private boolean h;
        private int i;
        private boolean j;
        private final ArrayList<IntentFilter> k = new ArrayList<>();
        private int l;
        private int m;
        private int n;
        private int o;
        private int p;
        private int q;
        private int r = -1;
        private Bundle s;
        private IntentSender t;
        MediaRouteDescriptor u;

        RouteInfo(ProviderInfo providerInfo, String str, String str2) {
            this.f853a = providerInfo;
            this.b = str;
            this.c = str2;
        }

        public boolean a(MediaRouteSelector mediaRouteSelector) {
            if (mediaRouteSelector != null) {
                MediaRouter.e();
                return mediaRouteSelector.a((List<IntentFilter>) this.k);
            }
            throw new IllegalArgumentException("selector must not be null");
        }

        public int b() {
            return this.i;
        }

        public String c() {
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public String d() {
            return this.b;
        }

        public int e() {
            return this.n;
        }

        public Bundle f() {
            return this.s;
        }

        public Uri g() {
            return this.f;
        }

        public String h() {
            return this.c;
        }

        public String i() {
            return this.d;
        }

        public int j() {
            return this.m;
        }

        public int k() {
            return this.l;
        }

        public int l() {
            return this.r;
        }

        public ProviderInfo m() {
            return this.f853a;
        }

        public MediaRouteProvider n() {
            return this.f853a.c();
        }

        public int o() {
            return this.p;
        }

        public int p() {
            return this.o;
        }

        public int q() {
            return this.q;
        }

        public boolean r() {
            return this.h;
        }

        public boolean s() {
            MediaRouter.e();
            return MediaRouter.d.c() == this;
        }

        public boolean t() {
            if (s() || this.n == 3) {
                return true;
            }
            if (!a(this) || !a("android.media.intent.category.LIVE_AUDIO") || a("android.media.intent.category.LIVE_VIDEO")) {
                return false;
            }
            return true;
        }

        public String toString() {
            return "MediaRouter.RouteInfo{ uniqueId=" + this.c + ", name=" + this.d + ", description=" + this.e + ", iconUri=" + this.f + ", enabled=" + this.g + ", connecting=" + this.h + ", connectionState=" + this.i + ", canDisconnect=" + this.j + ", playbackType=" + this.l + ", playbackStream=" + this.m + ", deviceType=" + this.n + ", volumeHandling=" + this.o + ", volume=" + this.p + ", volumeMax=" + this.q + ", presentationDisplayId=" + this.r + ", extras=" + this.s + ", settingsIntent=" + this.t + ", providerPackageName=" + this.f853a.b() + " }";
        }

        public boolean u() {
            return this.g;
        }

        /* access modifiers changed from: package-private */
        public boolean v() {
            return this.u != null && this.g;
        }

        public boolean w() {
            MediaRouter.e();
            return MediaRouter.d.f() == this;
        }

        public void x() {
            MediaRouter.e();
            MediaRouter.d.a(this);
        }

        public void b(int i2) {
            MediaRouter.e();
            if (i2 != 0) {
                MediaRouter.d.b(this, i2);
            }
        }

        public boolean a(String str) {
            if (str != null) {
                MediaRouter.e();
                int size = this.k.size();
                for (int i2 = 0; i2 < size; i2++) {
                    if (this.k.get(i2).hasCategory(str)) {
                        return true;
                    }
                }
                return false;
            }
            throw new IllegalArgumentException("category must not be null");
        }

        /* access modifiers changed from: package-private */
        public int b(MediaRouteDescriptor mediaRouteDescriptor) {
            this.u = mediaRouteDescriptor;
            int i2 = 0;
            if (mediaRouteDescriptor == null) {
                return 0;
            }
            if (!ObjectsCompat.a(this.d, mediaRouteDescriptor.n())) {
                this.d = mediaRouteDescriptor.n();
                i2 = 1;
            }
            if (!ObjectsCompat.a(this.e, mediaRouteDescriptor.f())) {
                this.e = mediaRouteDescriptor.f();
                i2 |= 1;
            }
            if (!ObjectsCompat.a(this.f, mediaRouteDescriptor.j())) {
                this.f = mediaRouteDescriptor.j();
                i2 |= 1;
            }
            if (this.g != mediaRouteDescriptor.w()) {
                this.g = mediaRouteDescriptor.w();
                i2 |= 1;
            }
            if (this.h != mediaRouteDescriptor.v()) {
                this.h = mediaRouteDescriptor.v();
                i2 |= 1;
            }
            if (this.i != mediaRouteDescriptor.d()) {
                this.i = mediaRouteDescriptor.d();
                i2 |= 1;
            }
            if (!this.k.equals(mediaRouteDescriptor.e())) {
                this.k.clear();
                this.k.addAll(mediaRouteDescriptor.e());
                i2 |= 1;
            }
            if (this.l != mediaRouteDescriptor.p()) {
                this.l = mediaRouteDescriptor.p();
                i2 |= 1;
            }
            if (this.m != mediaRouteDescriptor.o()) {
                this.m = mediaRouteDescriptor.o();
                i2 |= 1;
            }
            if (this.n != mediaRouteDescriptor.g()) {
                this.n = mediaRouteDescriptor.g();
                i2 |= 1;
            }
            if (this.o != mediaRouteDescriptor.t()) {
                this.o = mediaRouteDescriptor.t();
                i2 |= 3;
            }
            if (this.p != mediaRouteDescriptor.s()) {
                this.p = mediaRouteDescriptor.s();
                i2 |= 3;
            }
            if (this.q != mediaRouteDescriptor.u()) {
                this.q = mediaRouteDescriptor.u();
                i2 |= 3;
            }
            if (this.r != mediaRouteDescriptor.q()) {
                this.r = mediaRouteDescriptor.q();
                i2 |= 5;
            }
            if (!ObjectsCompat.a(this.s, mediaRouteDescriptor.h())) {
                this.s = mediaRouteDescriptor.h();
                i2 |= 1;
            }
            if (!ObjectsCompat.a(this.t, mediaRouteDescriptor.r())) {
                this.t = mediaRouteDescriptor.r();
                i2 |= 1;
            }
            if (this.j == mediaRouteDescriptor.b()) {
                return i2;
            }
            this.j = mediaRouteDescriptor.b();
            return i2 | 5;
        }

        private static boolean a(RouteInfo routeInfo) {
            return TextUtils.equals(routeInfo.n().g().b(), "android");
        }

        public boolean a() {
            return this.j;
        }

        public void a(int i2) {
            MediaRouter.e();
            MediaRouter.d.a(this, Math.min(this.q, Math.max(0, i2)));
        }

        /* access modifiers changed from: package-private */
        public int a(MediaRouteDescriptor mediaRouteDescriptor) {
            if (this.u != mediaRouteDescriptor) {
                return b(mediaRouteDescriptor);
            }
            return 0;
        }
    }

    MediaRouter(Context context) {
        this.f842a = context;
    }

    public static MediaRouter a(Context context) {
        if (context != null) {
            e();
            if (d == null) {
                d = new GlobalMediaRouter(context.getApplicationContext());
                d.g();
            }
            return d.a(context);
        }
        throw new IllegalArgumentException("context must not be null");
    }

    private int b(Callback callback) {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            if (this.b.get(i).b == callback) {
                return i;
            }
        }
        return -1;
    }

    static void e() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("The media router service must only be accessed on the application's main thread.");
        }
    }

    public List<RouteInfo> c() {
        e();
        return d.e();
    }

    public RouteInfo d() {
        e();
        return d.f();
    }

    private static final class GlobalMediaRouter implements SystemMediaRouteProvider.SyncCallback, RegisteredMediaRouteProviderWatcher.Callback {

        /* renamed from: a  reason: collision with root package name */
        final Context f844a;
        final ArrayList<WeakReference<MediaRouter>> b = new ArrayList<>();
        private final ArrayList<RouteInfo> c = new ArrayList<>();
        private final Map<Pair<String, String>, String> d = new HashMap();
        private final ArrayList<ProviderInfo> e = new ArrayList<>();
        private final ArrayList<RemoteControlClientRecord> f = new ArrayList<>();
        final RemoteControlClientCompat.PlaybackInfo g = new RemoteControlClientCompat.PlaybackInfo();
        private final ProviderCallback h = new ProviderCallback();
        final CallbackHandler i = new CallbackHandler();
        final SystemMediaRouteProvider j;
        private final boolean k;
        private RegisteredMediaRouteProviderWatcher l;
        private RouteInfo m;
        private RouteInfo n;
        RouteInfo o;
        private MediaRouteProvider.RouteController p;
        private final Map<String, MediaRouteProvider.RouteController> q = new HashMap();
        private MediaRouteDiscoveryRequest r;
        private MediaSessionRecord s;
        MediaSessionCompat t;
        private MediaSessionCompat u;
        private MediaSessionCompat.OnActiveChangeListener v = new MediaSessionCompat.OnActiveChangeListener() {
            public void onActiveChanged() {
                MediaSessionCompat mediaSessionCompat = GlobalMediaRouter.this.t;
                if (mediaSessionCompat == null) {
                    return;
                }
                if (mediaSessionCompat.isActive()) {
                    GlobalMediaRouter globalMediaRouter = GlobalMediaRouter.this;
                    globalMediaRouter.a(globalMediaRouter.t.getRemoteControlClient());
                    return;
                }
                GlobalMediaRouter globalMediaRouter2 = GlobalMediaRouter.this;
                globalMediaRouter2.b(globalMediaRouter2.t.getRemoteControlClient());
            }
        };

        private final class CallbackHandler extends Handler {

            /* renamed from: a  reason: collision with root package name */
            private final ArrayList<CallbackRecord> f846a = new ArrayList<>();

            CallbackHandler() {
            }

            private void b(int i, Object obj) {
                if (i != 262) {
                    switch (i) {
                        case 257:
                            GlobalMediaRouter.this.j.a((RouteInfo) obj);
                            return;
                        case 258:
                            GlobalMediaRouter.this.j.c((RouteInfo) obj);
                            return;
                        case 259:
                            GlobalMediaRouter.this.j.b((RouteInfo) obj);
                            return;
                        default:
                            return;
                    }
                } else {
                    GlobalMediaRouter.this.j.d((RouteInfo) obj);
                }
            }

            public void a(int i, Object obj) {
                obtainMessage(i, obj).sendToTarget();
            }

            public void handleMessage(Message message) {
                int i = message.what;
                Object obj = message.obj;
                int i2 = message.arg1;
                if (i == 259 && GlobalMediaRouter.this.f().h().equals(((RouteInfo) obj).h())) {
                    GlobalMediaRouter.this.a(true);
                }
                b(i, obj);
                try {
                    int size = GlobalMediaRouter.this.b.size();
                    while (true) {
                        size--;
                        if (size < 0) {
                            break;
                        }
                        MediaRouter mediaRouter = (MediaRouter) GlobalMediaRouter.this.b.get(size).get();
                        if (mediaRouter == null) {
                            GlobalMediaRouter.this.b.remove(size);
                        } else {
                            this.f846a.addAll(mediaRouter.b);
                        }
                    }
                    int size2 = this.f846a.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        a(this.f846a.get(i3), i, obj, i2);
                    }
                } finally {
                    this.f846a.clear();
                }
            }

            public void a(int i, Object obj, int i2) {
                Message obtainMessage = obtainMessage(i, obj);
                obtainMessage.arg1 = i2;
                obtainMessage.sendToTarget();
            }

            private void a(CallbackRecord callbackRecord, int i, Object obj, int i2) {
                MediaRouter mediaRouter = callbackRecord.f843a;
                Callback callback = callbackRecord.b;
                int i3 = 65280 & i;
                if (i3 == 256) {
                    RouteInfo routeInfo = (RouteInfo) obj;
                    if (callbackRecord.a(routeInfo)) {
                        switch (i) {
                            case 257:
                                callback.onRouteAdded(mediaRouter, routeInfo);
                                return;
                            case 258:
                                callback.onRouteRemoved(mediaRouter, routeInfo);
                                return;
                            case 259:
                                callback.onRouteChanged(mediaRouter, routeInfo);
                                return;
                            case 260:
                                callback.onRouteVolumeChanged(mediaRouter, routeInfo);
                                return;
                            case 261:
                                callback.onRoutePresentationDisplayChanged(mediaRouter, routeInfo);
                                return;
                            case 262:
                                callback.onRouteSelected(mediaRouter, routeInfo);
                                return;
                            case 263:
                                callback.onRouteUnselected(mediaRouter, routeInfo, i2);
                                return;
                            default:
                                return;
                        }
                    }
                } else if (i3 == 512) {
                    ProviderInfo providerInfo = (ProviderInfo) obj;
                    switch (i) {
                        case 513:
                            callback.onProviderAdded(mediaRouter, providerInfo);
                            return;
                        case 514:
                            callback.onProviderRemoved(mediaRouter, providerInfo);
                            return;
                        case 515:
                            callback.onProviderChanged(mediaRouter, providerInfo);
                            return;
                        default:
                            return;
                    }
                }
            }
        }

        private final class ProviderCallback extends MediaRouteProvider.Callback {
            ProviderCallback() {
            }

            public void a(MediaRouteProvider mediaRouteProvider, MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
                GlobalMediaRouter.this.a(mediaRouteProvider, mediaRouteProviderDescriptor);
            }
        }

        private final class RemoteControlClientRecord implements RemoteControlClientCompat.VolumeCallback {

            /* renamed from: a  reason: collision with root package name */
            private final RemoteControlClientCompat f851a;
            private boolean b;

            public RemoteControlClientRecord(Object obj) {
                this.f851a = RemoteControlClientCompat.a(GlobalMediaRouter.this.f844a, obj);
                this.f851a.a((RemoteControlClientCompat.VolumeCallback) this);
                c();
            }

            public void a() {
                this.b = true;
                this.f851a.a((RemoteControlClientCompat.VolumeCallback) null);
            }

            public Object b() {
                return this.f851a.a();
            }

            public void c() {
                this.f851a.a(GlobalMediaRouter.this.g);
            }

            public void b(int i) {
                RouteInfo routeInfo;
                if (!this.b && (routeInfo = GlobalMediaRouter.this.o) != null) {
                    routeInfo.b(i);
                }
            }

            public void a(int i) {
                RouteInfo routeInfo;
                if (!this.b && (routeInfo = GlobalMediaRouter.this.o) != null) {
                    routeInfo.a(i);
                }
            }
        }

        GlobalMediaRouter(Context context) {
            this.f844a = context;
            DisplayManagerCompat.a(context);
            this.k = ActivityManagerCompat.a((ActivityManager) context.getSystemService("activity"));
            this.j = SystemMediaRouteProvider.a(context, this);
        }

        private void d(RouteInfo routeInfo, int i2) {
            if (MediaRouter.d == null || (this.n != null && routeInfo.s())) {
                StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
                StringBuilder sb = new StringBuilder();
                for (int i3 = 3; i3 < stackTrace.length; i3++) {
                    StackTraceElement stackTraceElement = stackTrace[i3];
                    sb.append(stackTraceElement.getClassName());
                    sb.append(".");
                    sb.append(stackTraceElement.getMethodName());
                    sb.append(":");
                    sb.append(stackTraceElement.getLineNumber());
                    sb.append("  ");
                }
                if (MediaRouter.d == null) {
                    Log.w("MediaRouter", "setSelectedRouteInternal is called while sGlobal is null: pkgName=" + this.f844a.getPackageName() + ", callers=" + sb.toString());
                } else {
                    Log.w("MediaRouter", "Default route is selected while a BT route is available: pkgName=" + this.f844a.getPackageName() + ", callers=" + sb.toString());
                }
            }
            RouteInfo routeInfo2 = this.o;
            if (routeInfo2 != routeInfo) {
                if (routeInfo2 != null) {
                    if (MediaRouter.c) {
                        Log.d("MediaRouter", "Route unselected: " + this.o + " reason: " + i2);
                    }
                    this.i.a(263, this.o, i2);
                    MediaRouteProvider.RouteController routeController = this.p;
                    if (routeController != null) {
                        routeController.b(i2);
                        this.p.a();
                        this.p = null;
                    }
                    if (!this.q.isEmpty()) {
                        for (MediaRouteProvider.RouteController next : this.q.values()) {
                            next.b(i2);
                            next.a();
                        }
                        this.q.clear();
                    }
                }
                this.o = routeInfo;
                this.p = routeInfo.n().a(routeInfo.b);
                MediaRouteProvider.RouteController routeController2 = this.p;
                if (routeController2 != null) {
                    routeController2.b();
                }
                if (MediaRouter.c) {
                    Log.d("MediaRouter", "Route selected: " + this.o);
                }
                this.i.a(262, this.o);
                RouteInfo routeInfo3 = this.o;
                if (routeInfo3 instanceof RouteGroup) {
                    List<RouteInfo> y = ((RouteGroup) routeInfo3).y();
                    this.q.clear();
                    for (RouteInfo next2 : y) {
                        MediaRouteProvider.RouteController a2 = next2.n().a(next2.b, this.o.b);
                        a2.b();
                        this.q.put(next2.b, a2);
                    }
                }
                i();
            }
        }

        private void i() {
            RouteInfo routeInfo = this.o;
            if (routeInfo != null) {
                this.g.f870a = routeInfo.o();
                this.g.b = this.o.q();
                this.g.c = this.o.p();
                this.g.d = this.o.j();
                this.g.e = this.o.k();
                int size = this.f.size();
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    this.f.get(i3).c();
                }
                if (this.s == null) {
                    return;
                }
                if (this.o == c() || this.o == b()) {
                    this.s.a();
                    return;
                }
                if (this.g.c == 1) {
                    i2 = 2;
                }
                MediaSessionRecord mediaSessionRecord = this.s;
                RemoteControlClientCompat.PlaybackInfo playbackInfo = this.g;
                mediaSessionRecord.a(i2, playbackInfo.b, playbackInfo.f870a);
                return;
            }
            MediaSessionRecord mediaSessionRecord2 = this.s;
            if (mediaSessionRecord2 != null) {
                mediaSessionRecord2.a();
            }
        }

        public MediaRouter a(Context context) {
            int size = this.b.size();
            while (true) {
                size--;
                if (size >= 0) {
                    MediaRouter mediaRouter = (MediaRouter) this.b.get(size).get();
                    if (mediaRouter == null) {
                        this.b.remove(size);
                    } else if (mediaRouter.f842a == context) {
                        return mediaRouter;
                    }
                } else {
                    MediaRouter mediaRouter2 = new MediaRouter(context);
                    this.b.add(new WeakReference(mediaRouter2));
                    return mediaRouter2;
                }
            }
        }

        public void b(RouteInfo routeInfo, int i2) {
            MediaRouteProvider.RouteController routeController;
            if (routeInfo == this.o && (routeController = this.p) != null) {
                routeController.c(i2);
            }
        }

        /* access modifiers changed from: package-private */
        public RouteInfo c() {
            RouteInfo routeInfo = this.m;
            if (routeInfo != null) {
                return routeInfo;
            }
            throw new IllegalStateException("There is no default route.  The media router has not yet been fully initialized.");
        }

        public List<RouteInfo> e() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public RouteInfo f() {
            RouteInfo routeInfo = this.o;
            if (routeInfo != null) {
                return routeInfo;
            }
            throw new IllegalStateException("There is no currently selected route.  The media router has not yet been fully initialized.");
        }

        public void g() {
            a((MediaRouteProvider) this.j);
            this.l = new RegisteredMediaRouteProviderWatcher(this.f844a, this);
            this.l.b();
        }

        public void h() {
            MediaRouteSelector.Builder builder = new MediaRouteSelector.Builder();
            int size = this.b.size();
            boolean z = false;
            boolean z2 = false;
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                MediaRouter mediaRouter = (MediaRouter) this.b.get(size).get();
                if (mediaRouter == null) {
                    this.b.remove(size);
                } else {
                    int size2 = mediaRouter.b.size();
                    boolean z3 = z2;
                    boolean z4 = z;
                    for (int i2 = 0; i2 < size2; i2++) {
                        CallbackRecord callbackRecord = mediaRouter.b.get(i2);
                        builder.a(callbackRecord.c);
                        if ((callbackRecord.d & 1) != 0) {
                            z4 = true;
                            z3 = true;
                        }
                        if ((callbackRecord.d & 4) != 0 && !this.k) {
                            z4 = true;
                        }
                        if ((callbackRecord.d & 8) != 0) {
                            z4 = true;
                        }
                    }
                    z = z4;
                    z2 = z3;
                }
            }
            MediaRouteSelector a2 = z ? builder.a() : MediaRouteSelector.c;
            MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest = this.r;
            if (mediaRouteDiscoveryRequest == null || !mediaRouteDiscoveryRequest.b().equals(a2) || this.r.c() != z2) {
                if (!a2.d() || z2) {
                    this.r = new MediaRouteDiscoveryRequest(a2, z2);
                } else if (this.r != null) {
                    this.r = null;
                } else {
                    return;
                }
                if (MediaRouter.c) {
                    Log.d("MediaRouter", "Updated discovery request: " + this.r);
                }
                if (z && !z2 && this.k) {
                    Log.i("MediaRouter", "Forcing passive route discovery on a low-RAM device, system performance may be affected.  Please consider using CALLBACK_FLAG_REQUEST_DISCOVERY instead of CALLBACK_FLAG_FORCE_DISCOVERY.");
                }
                int size3 = this.e.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    this.e.get(i3).f852a.b(this.r);
                }
            }
        }

        public RouteInfo b(String str) {
            Iterator<RouteInfo> it2 = this.c.iterator();
            while (it2.hasNext()) {
                RouteInfo next = it2.next();
                if (next.c.equals(str)) {
                    return next;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public void c(RouteInfo routeInfo, int i2) {
            if (!this.c.contains(routeInfo)) {
                Log.w("MediaRouter", "Ignoring attempt to select removed route: " + routeInfo);
            } else if (!routeInfo.g) {
                Log.w("MediaRouter", "Ignoring attempt to select disabled route: " + routeInfo);
            } else {
                d(routeInfo, i2);
            }
        }

        private final class MediaSessionRecord {

            /* renamed from: a  reason: collision with root package name */
            private final MediaSessionCompat f847a;
            private int b;
            private int c;
            private VolumeProviderCompat d;

            MediaSessionRecord(MediaSessionCompat mediaSessionCompat) {
                this.f847a = mediaSessionCompat;
            }

            public void a(int i, int i2, int i3) {
                if (this.f847a != null) {
                    VolumeProviderCompat volumeProviderCompat = this.d;
                    if (volumeProviderCompat != null && i == this.b && i2 == this.c) {
                        volumeProviderCompat.c(i3);
                        return;
                    }
                    this.d = new VolumeProviderCompat(i, i2, i3) {
                        public void a(final int i) {
                            GlobalMediaRouter.this.i.post(new Runnable() {
                                public void run() {
                                    RouteInfo routeInfo = GlobalMediaRouter.this.o;
                                    if (routeInfo != null) {
                                        routeInfo.b(i);
                                    }
                                }
                            });
                        }

                        public void b(final int i) {
                            GlobalMediaRouter.this.i.post(new Runnable() {
                                public void run() {
                                    RouteInfo routeInfo = GlobalMediaRouter.this.o;
                                    if (routeInfo != null) {
                                        routeInfo.a(i);
                                    }
                                }
                            });
                        }
                    };
                    this.f847a.setPlaybackToRemote(this.d);
                }
            }

            public MediaSessionCompat.Token b() {
                MediaSessionCompat mediaSessionCompat = this.f847a;
                if (mediaSessionCompat != null) {
                    return mediaSessionCompat.getSessionToken();
                }
                return null;
            }

            public void a() {
                MediaSessionCompat mediaSessionCompat = this.f847a;
                if (mediaSessionCompat != null) {
                    mediaSessionCompat.setPlaybackToLocal(GlobalMediaRouter.this.g.d);
                    this.d = null;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public RouteInfo b() {
            return this.n;
        }

        public void b(MediaRouteProvider mediaRouteProvider) {
            int c2 = c(mediaRouteProvider);
            if (c2 >= 0) {
                mediaRouteProvider.a((MediaRouteProvider.Callback) null);
                mediaRouteProvider.b((MediaRouteDiscoveryRequest) null);
                ProviderInfo providerInfo = this.e.get(c2);
                a(providerInfo, (MediaRouteProviderDescriptor) null);
                if (MediaRouter.c) {
                    Log.d("MediaRouter", "Provider removed: " + providerInfo);
                }
                this.i.a(514, providerInfo);
                this.e.remove(c2);
            }
        }

        public void a(RouteInfo routeInfo, int i2) {
            MediaRouteProvider.RouteController routeController;
            MediaRouteProvider.RouteController routeController2;
            if (routeInfo == this.o && (routeController2 = this.p) != null) {
                routeController2.a(i2);
            } else if (!this.q.isEmpty() && (routeController = this.q.get(routeInfo.b)) != null) {
                routeController.a(i2);
            }
        }

        private int c(MediaRouteProvider mediaRouteProvider) {
            int size = this.e.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.e.get(i2).f852a == mediaRouteProvider) {
                    return i2;
                }
            }
            return -1;
        }

        private int c(String str) {
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.c.get(i2).c.equals(str)) {
                    return i2;
                }
            }
            return -1;
        }

        private boolean c(RouteInfo routeInfo) {
            return routeInfo.n() == this.j && routeInfo.a("android.media.intent.category.LIVE_AUDIO") && !routeInfo.a("android.media.intent.category.LIVE_VIDEO");
        }

        /* access modifiers changed from: package-private */
        public void a(RouteInfo routeInfo) {
            c(routeInfo, 3);
        }

        public boolean a(MediaRouteSelector mediaRouteSelector, int i2) {
            if (mediaRouteSelector.d()) {
                return false;
            }
            if ((i2 & 2) == 0 && this.k) {
                return true;
            }
            int size = this.c.size();
            for (int i3 = 0; i3 < size; i3++) {
                RouteInfo routeInfo = this.c.get(i3);
                if (((i2 & 1) == 0 || !routeInfo.t()) && routeInfo.a(mediaRouteSelector)) {
                    return true;
                }
            }
            return false;
        }

        private String b(ProviderInfo providerInfo, String str) {
            String flattenToShortString = providerInfo.a().flattenToShortString();
            String str2 = flattenToShortString + ":" + str;
            if (c(str2) < 0) {
                this.d.put(new Pair(flattenToShortString, str), str2);
                return str2;
            }
            Log.w("MediaRouter", "Either " + str + " isn't unique in " + flattenToShortString + " or we're trying to assign a unique ID for an already added route");
            int i2 = 2;
            while (true) {
                String format = String.format(Locale.US, "%s_%d", new Object[]{str2, Integer.valueOf(i2)});
                if (c(format) < 0) {
                    this.d.put(new Pair(flattenToShortString, str), format);
                    return format;
                }
                i2++;
            }
        }

        private int c(Object obj) {
            int size = this.f.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.f.get(i2).b() == obj) {
                    return i2;
                }
            }
            return -1;
        }

        public void a(MediaRouteProvider mediaRouteProvider) {
            if (c(mediaRouteProvider) < 0) {
                ProviderInfo providerInfo = new ProviderInfo(mediaRouteProvider);
                this.e.add(providerInfo);
                if (MediaRouter.c) {
                    Log.d("MediaRouter", "Provider added: " + providerInfo);
                }
                this.i.a(513, providerInfo);
                a(providerInfo, mediaRouteProvider.d());
                mediaRouteProvider.a((MediaRouteProvider.Callback) this.h);
                mediaRouteProvider.b(this.r);
            }
        }

        private boolean b(RouteInfo routeInfo) {
            return routeInfo.n() == this.j && routeInfo.b.equals("DEFAULT_ROUTE");
        }

        public void b(Object obj) {
            int c2 = c(obj);
            if (c2 >= 0) {
                this.f.remove(c2).a();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(MediaRouteProvider mediaRouteProvider, MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
            int c2 = c(mediaRouteProvider);
            if (c2 >= 0) {
                a(this.e.get(c2), mediaRouteProviderDescriptor);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:63:0x0174 A[LOOP:3: B:62:0x0172->B:63:0x0174, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:66:0x0195  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x01c3  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void a(androidx.mediarouter.media.MediaRouter.ProviderInfo r18, androidx.mediarouter.media.MediaRouteProviderDescriptor r19) {
            /*
                r17 = this;
                r0 = r17
                r1 = r18
                r2 = r19
                boolean r3 = r18.a((androidx.mediarouter.media.MediaRouteProviderDescriptor) r19)
                if (r3 == 0) goto L_0x01de
                java.lang.String r4 = "MediaRouter"
                if (r2 == 0) goto L_0x0168
                boolean r6 = r19.c()
                if (r6 == 0) goto L_0x0154
                java.util.List r2 = r19.b()
                int r6 = r2.size()
                java.util.ArrayList r7 = new java.util.ArrayList
                r7.<init>()
                java.util.ArrayList r8 = new java.util.ArrayList
                r8.<init>()
                r9 = 0
                r10 = 0
                r11 = 0
            L_0x002b:
                java.lang.String r12 = "Route added: "
                if (r9 >= r6) goto L_0x00f6
                java.lang.Object r14 = r2.get(r9)
                androidx.mediarouter.media.MediaRouteDescriptor r14 = (androidx.mediarouter.media.MediaRouteDescriptor) r14
                java.lang.String r15 = r14.k()
                int r3 = r1.a((java.lang.String) r15)
                java.util.List r16 = r14.i()
                if (r16 == 0) goto L_0x0045
                r5 = 1
                goto L_0x0046
            L_0x0045:
                r5 = 0
            L_0x0046:
                if (r3 >= 0) goto L_0x0092
                java.lang.String r3 = r0.b((androidx.mediarouter.media.MediaRouter.ProviderInfo) r1, (java.lang.String) r15)
                if (r5 == 0) goto L_0x0054
                androidx.mediarouter.media.MediaRouter$RouteGroup r13 = new androidx.mediarouter.media.MediaRouter$RouteGroup
                r13.<init>(r1, r15, r3)
                goto L_0x0059
            L_0x0054:
                androidx.mediarouter.media.MediaRouter$RouteInfo r13 = new androidx.mediarouter.media.MediaRouter$RouteInfo
                r13.<init>(r1, r15, r3)
            L_0x0059:
                java.util.List<androidx.mediarouter.media.MediaRouter$RouteInfo> r3 = r1.b
                int r15 = r10 + 1
                r3.add(r10, r13)
                java.util.ArrayList<androidx.mediarouter.media.MediaRouter$RouteInfo> r3 = r0.c
                r3.add(r13)
                if (r5 == 0) goto L_0x0070
                androidx.core.util.Pair r3 = new androidx.core.util.Pair
                r3.<init>(r13, r14)
                r7.add(r3)
                goto L_0x0090
            L_0x0070:
                r13.a((androidx.mediarouter.media.MediaRouteDescriptor) r14)
                boolean r3 = androidx.mediarouter.media.MediaRouter.c
                if (r3 == 0) goto L_0x0089
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                r3.append(r12)
                r3.append(r13)
                java.lang.String r3 = r3.toString()
                android.util.Log.d(r4, r3)
            L_0x0089:
                androidx.mediarouter.media.MediaRouter$GlobalMediaRouter$CallbackHandler r3 = r0.i
                r5 = 257(0x101, float:3.6E-43)
                r3.a(r5, r13)
            L_0x0090:
                r10 = r15
                goto L_0x00f2
            L_0x0092:
                if (r3 >= r10) goto L_0x00a9
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r5 = "Ignoring route descriptor with duplicate id: "
                r3.append(r5)
                r3.append(r14)
                java.lang.String r3 = r3.toString()
                android.util.Log.w(r4, r3)
                goto L_0x00f2
            L_0x00a9:
                java.util.List<androidx.mediarouter.media.MediaRouter$RouteInfo> r12 = r1.b
                java.lang.Object r12 = r12.get(r3)
                androidx.mediarouter.media.MediaRouter$RouteInfo r12 = (androidx.mediarouter.media.MediaRouter.RouteInfo) r12
                boolean r13 = r12 instanceof androidx.mediarouter.media.MediaRouter.RouteGroup
                if (r13 == r5) goto L_0x00d0
                if (r5 == 0) goto L_0x00c1
                androidx.mediarouter.media.MediaRouter$RouteGroup r5 = new androidx.mediarouter.media.MediaRouter$RouteGroup
                java.lang.String r12 = r12.h()
                r5.<init>(r1, r15, r12)
                goto L_0x00ca
            L_0x00c1:
                androidx.mediarouter.media.MediaRouter$RouteInfo r5 = new androidx.mediarouter.media.MediaRouter$RouteInfo
                java.lang.String r12 = r12.h()
                r5.<init>(r1, r15, r12)
            L_0x00ca:
                r12 = r5
                java.util.List<androidx.mediarouter.media.MediaRouter$RouteInfo> r5 = r1.b
                r5.set(r3, r12)
            L_0x00d0:
                java.util.List<androidx.mediarouter.media.MediaRouter$RouteInfo> r5 = r1.b
                int r13 = r10 + 1
                java.util.Collections.swap(r5, r3, r10)
                boolean r3 = r12 instanceof androidx.mediarouter.media.MediaRouter.RouteGroup
                if (r3 == 0) goto L_0x00e4
                androidx.core.util.Pair r3 = new androidx.core.util.Pair
                r3.<init>(r12, r14)
                r8.add(r3)
                goto L_0x00f1
            L_0x00e4:
                int r3 = r0.a((androidx.mediarouter.media.MediaRouter.RouteInfo) r12, (androidx.mediarouter.media.MediaRouteDescriptor) r14)
                if (r3 == 0) goto L_0x00f1
                androidx.mediarouter.media.MediaRouter$RouteInfo r3 = r0.o
                if (r12 != r3) goto L_0x00f1
                r10 = r13
                r11 = 1
                goto L_0x00f2
            L_0x00f1:
                r10 = r13
            L_0x00f2:
                int r9 = r9 + 1
                goto L_0x002b
            L_0x00f6:
                java.util.Iterator r2 = r7.iterator()
            L_0x00fa:
                boolean r3 = r2.hasNext()
                if (r3 == 0) goto L_0x012f
                java.lang.Object r3 = r2.next()
                androidx.core.util.Pair r3 = (androidx.core.util.Pair) r3
                F r5 = r3.f598a
                androidx.mediarouter.media.MediaRouter$RouteInfo r5 = (androidx.mediarouter.media.MediaRouter.RouteInfo) r5
                S r3 = r3.b
                androidx.mediarouter.media.MediaRouteDescriptor r3 = (androidx.mediarouter.media.MediaRouteDescriptor) r3
                r5.a((androidx.mediarouter.media.MediaRouteDescriptor) r3)
                boolean r3 = androidx.mediarouter.media.MediaRouter.c
                if (r3 == 0) goto L_0x0127
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                r3.append(r12)
                r3.append(r5)
                java.lang.String r3 = r3.toString()
                android.util.Log.d(r4, r3)
            L_0x0127:
                androidx.mediarouter.media.MediaRouter$GlobalMediaRouter$CallbackHandler r3 = r0.i
                r6 = 257(0x101, float:3.6E-43)
                r3.a(r6, r5)
                goto L_0x00fa
            L_0x012f:
                java.util.Iterator r2 = r8.iterator()
                r3 = r11
            L_0x0134:
                boolean r5 = r2.hasNext()
                if (r5 == 0) goto L_0x016a
                java.lang.Object r5 = r2.next()
                androidx.core.util.Pair r5 = (androidx.core.util.Pair) r5
                F r6 = r5.f598a
                androidx.mediarouter.media.MediaRouter$RouteInfo r6 = (androidx.mediarouter.media.MediaRouter.RouteInfo) r6
                S r5 = r5.b
                androidx.mediarouter.media.MediaRouteDescriptor r5 = (androidx.mediarouter.media.MediaRouteDescriptor) r5
                int r5 = r0.a((androidx.mediarouter.media.MediaRouter.RouteInfo) r6, (androidx.mediarouter.media.MediaRouteDescriptor) r5)
                if (r5 == 0) goto L_0x0134
                androidx.mediarouter.media.MediaRouter$RouteInfo r5 = r0.o
                if (r6 != r5) goto L_0x0134
                r3 = 1
                goto L_0x0134
            L_0x0154:
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r5 = "Ignoring invalid provider descriptor: "
                r3.append(r5)
                r3.append(r2)
                java.lang.String r2 = r3.toString()
                android.util.Log.w(r4, r2)
            L_0x0168:
                r3 = 0
                r10 = 0
            L_0x016a:
                java.util.List<androidx.mediarouter.media.MediaRouter$RouteInfo> r2 = r1.b
                int r2 = r2.size()
                r5 = 1
                int r2 = r2 - r5
            L_0x0172:
                if (r2 < r10) goto L_0x0188
                java.util.List<androidx.mediarouter.media.MediaRouter$RouteInfo> r5 = r1.b
                java.lang.Object r5 = r5.get(r2)
                androidx.mediarouter.media.MediaRouter$RouteInfo r5 = (androidx.mediarouter.media.MediaRouter.RouteInfo) r5
                r6 = 0
                r5.a((androidx.mediarouter.media.MediaRouteDescriptor) r6)
                java.util.ArrayList<androidx.mediarouter.media.MediaRouter$RouteInfo> r6 = r0.c
                r6.remove(r5)
                int r2 = r2 + -1
                goto L_0x0172
            L_0x0188:
                r0.a((boolean) r3)
                java.util.List<androidx.mediarouter.media.MediaRouter$RouteInfo> r2 = r1.b
                int r2 = r2.size()
                r3 = 1
                int r2 = r2 - r3
            L_0x0193:
                if (r2 < r10) goto L_0x01bf
                java.util.List<androidx.mediarouter.media.MediaRouter$RouteInfo> r3 = r1.b
                java.lang.Object r3 = r3.remove(r2)
                androidx.mediarouter.media.MediaRouter$RouteInfo r3 = (androidx.mediarouter.media.MediaRouter.RouteInfo) r3
                boolean r5 = androidx.mediarouter.media.MediaRouter.c
                if (r5 == 0) goto L_0x01b5
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "Route removed: "
                r5.append(r6)
                r5.append(r3)
                java.lang.String r5 = r5.toString()
                android.util.Log.d(r4, r5)
            L_0x01b5:
                androidx.mediarouter.media.MediaRouter$GlobalMediaRouter$CallbackHandler r5 = r0.i
                r6 = 258(0x102, float:3.62E-43)
                r5.a(r6, r3)
                int r2 = r2 + -1
                goto L_0x0193
            L_0x01bf:
                boolean r2 = androidx.mediarouter.media.MediaRouter.c
                if (r2 == 0) goto L_0x01d7
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Provider changed: "
                r2.append(r3)
                r2.append(r1)
                java.lang.String r2 = r2.toString()
                android.util.Log.d(r4, r2)
            L_0x01d7:
                androidx.mediarouter.media.MediaRouter$GlobalMediaRouter$CallbackHandler r2 = r0.i
                r3 = 515(0x203, float:7.22E-43)
                r2.a(r3, r1)
            L_0x01de:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.mediarouter.media.MediaRouter.GlobalMediaRouter.a(androidx.mediarouter.media.MediaRouter$ProviderInfo, androidx.mediarouter.media.MediaRouteProviderDescriptor):void");
        }

        public MediaSessionCompat.Token d() {
            MediaSessionRecord mediaSessionRecord = this.s;
            if (mediaSessionRecord != null) {
                return mediaSessionRecord.b();
            }
            MediaSessionCompat mediaSessionCompat = this.u;
            if (mediaSessionCompat != null) {
                return mediaSessionCompat.getSessionToken();
            }
            return null;
        }

        private int a(RouteInfo routeInfo, MediaRouteDescriptor mediaRouteDescriptor) {
            int a2 = routeInfo.a(mediaRouteDescriptor);
            if (a2 != 0) {
                if ((a2 & 1) != 0) {
                    if (MediaRouter.c) {
                        Log.d("MediaRouter", "Route changed: " + routeInfo);
                    }
                    this.i.a(259, routeInfo);
                }
                if ((a2 & 2) != 0) {
                    if (MediaRouter.c) {
                        Log.d("MediaRouter", "Route volume changed: " + routeInfo);
                    }
                    this.i.a(260, routeInfo);
                }
                if ((a2 & 4) != 0) {
                    if (MediaRouter.c) {
                        Log.d("MediaRouter", "Route presentation display changed: " + routeInfo);
                    }
                    this.i.a(261, routeInfo);
                }
            }
            return a2;
        }

        /* access modifiers changed from: package-private */
        public String a(ProviderInfo providerInfo, String str) {
            return this.d.get(new Pair(providerInfo.a().flattenToShortString(), str));
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            RouteInfo routeInfo = this.m;
            if (routeInfo != null && !routeInfo.v()) {
                Log.i("MediaRouter", "Clearing the default route because it is no longer selectable: " + this.m);
                this.m = null;
            }
            if (this.m == null && !this.c.isEmpty()) {
                Iterator<RouteInfo> it2 = this.c.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    RouteInfo next = it2.next();
                    if (b(next) && next.v()) {
                        this.m = next;
                        Log.i("MediaRouter", "Found default route: " + this.m);
                        break;
                    }
                }
            }
            RouteInfo routeInfo2 = this.n;
            if (routeInfo2 != null && !routeInfo2.v()) {
                Log.i("MediaRouter", "Clearing the bluetooth route because it is no longer selectable: " + this.n);
                this.n = null;
            }
            if (this.n == null && !this.c.isEmpty()) {
                Iterator<RouteInfo> it3 = this.c.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    RouteInfo next2 = it3.next();
                    if (c(next2) && next2.v()) {
                        this.n = next2;
                        Log.i("MediaRouter", "Found bluetooth route: " + this.n);
                        break;
                    }
                }
            }
            RouteInfo routeInfo3 = this.o;
            if (routeInfo3 == null || !routeInfo3.v()) {
                Log.i("MediaRouter", "Unselecting the current route because it is no longer selectable: " + this.o);
                d(a(), 0);
            } else if (z) {
                RouteInfo routeInfo4 = this.o;
                if (routeInfo4 instanceof RouteGroup) {
                    List<RouteInfo> y = ((RouteGroup) routeInfo4).y();
                    HashSet hashSet = new HashSet();
                    for (RouteInfo routeInfo5 : y) {
                        hashSet.add(routeInfo5.b);
                    }
                    Iterator<Map.Entry<String, MediaRouteProvider.RouteController>> it4 = this.q.entrySet().iterator();
                    while (it4.hasNext()) {
                        Map.Entry next3 = it4.next();
                        if (!hashSet.contains(next3.getKey())) {
                            MediaRouteProvider.RouteController routeController = (MediaRouteProvider.RouteController) next3.getValue();
                            routeController.c();
                            routeController.a();
                            it4.remove();
                        }
                    }
                    for (RouteInfo next4 : y) {
                        if (!this.q.containsKey(next4.b)) {
                            MediaRouteProvider.RouteController a2 = next4.n().a(next4.b, this.o.b);
                            a2.b();
                            this.q.put(next4.b, a2);
                        }
                    }
                }
                i();
            }
        }

        /* access modifiers changed from: package-private */
        public RouteInfo a() {
            Iterator<RouteInfo> it2 = this.c.iterator();
            while (it2.hasNext()) {
                RouteInfo next = it2.next();
                if (next != this.m && c(next) && next.v()) {
                    return next;
                }
            }
            return this.m;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:2:0x000f, code lost:
            r0 = r2.e.get(r0);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(java.lang.String r3) {
            /*
                r2 = this;
                androidx.mediarouter.media.MediaRouter$GlobalMediaRouter$CallbackHandler r0 = r2.i
                r1 = 262(0x106, float:3.67E-43)
                r0.removeMessages(r1)
                androidx.mediarouter.media.SystemMediaRouteProvider r0 = r2.j
                int r0 = r2.c((androidx.mediarouter.media.MediaRouteProvider) r0)
                if (r0 < 0) goto L_0x0028
                java.util.ArrayList<androidx.mediarouter.media.MediaRouter$ProviderInfo> r1 = r2.e
                java.lang.Object r0 = r1.get(r0)
                androidx.mediarouter.media.MediaRouter$ProviderInfo r0 = (androidx.mediarouter.media.MediaRouter.ProviderInfo) r0
                int r3 = r0.a((java.lang.String) r3)
                if (r3 < 0) goto L_0x0028
                java.util.List<androidx.mediarouter.media.MediaRouter$RouteInfo> r0 = r0.b
                java.lang.Object r3 = r0.get(r3)
                androidx.mediarouter.media.MediaRouter$RouteInfo r3 = (androidx.mediarouter.media.MediaRouter.RouteInfo) r3
                r3.x()
            L_0x0028:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.mediarouter.media.MediaRouter.GlobalMediaRouter.a(java.lang.String):void");
        }

        public void a(Object obj) {
            if (c(obj) < 0) {
                this.f.add(new RemoteControlClientRecord(obj));
            }
        }

        public void a(MediaSessionCompat mediaSessionCompat) {
            this.u = mediaSessionCompat;
            int i2 = Build.VERSION.SDK_INT;
            if (i2 >= 21) {
                a(mediaSessionCompat != null ? new MediaSessionRecord(mediaSessionCompat) : null);
            } else if (i2 >= 14) {
                MediaSessionCompat mediaSessionCompat2 = this.t;
                if (mediaSessionCompat2 != null) {
                    b(mediaSessionCompat2.getRemoteControlClient());
                    this.t.removeOnActiveChangeListener(this.v);
                }
                this.t = mediaSessionCompat;
                if (mediaSessionCompat != null) {
                    mediaSessionCompat.addOnActiveChangeListener(this.v);
                    if (mediaSessionCompat.isActive()) {
                        a(mediaSessionCompat.getRemoteControlClient());
                    }
                }
            }
        }

        private void a(MediaSessionRecord mediaSessionRecord) {
            MediaSessionRecord mediaSessionRecord2 = this.s;
            if (mediaSessionRecord2 != null) {
                mediaSessionRecord2.a();
            }
            this.s = mediaSessionRecord;
            if (mediaSessionRecord != null) {
                i();
            }
        }
    }

    public MediaSessionCompat.Token b() {
        return d.d();
    }

    public RouteInfo a() {
        e();
        return d.c();
    }

    public void a(RouteInfo routeInfo) {
        if (routeInfo != null) {
            e();
            if (c) {
                Log.d("MediaRouter", "selectRoute: " + routeInfo);
            }
            d.a(routeInfo);
            return;
        }
        throw new IllegalArgumentException("route must not be null");
    }

    public void a(int i) {
        if (i < 0 || i > 3) {
            throw new IllegalArgumentException("Unsupported reason to unselect route");
        }
        e();
        RouteInfo a2 = d.a();
        if (d.f() != a2) {
            d.c(a2, i);
            return;
        }
        GlobalMediaRouter globalMediaRouter = d;
        globalMediaRouter.c(globalMediaRouter.c(), i);
    }

    public boolean a(MediaRouteSelector mediaRouteSelector, int i) {
        if (mediaRouteSelector != null) {
            e();
            return d.a(mediaRouteSelector, i);
        }
        throw new IllegalArgumentException("selector must not be null");
    }

    public void a(MediaRouteSelector mediaRouteSelector, Callback callback) {
        a(mediaRouteSelector, callback, 0);
    }

    public void a(MediaRouteSelector mediaRouteSelector, Callback callback, int i) {
        CallbackRecord callbackRecord;
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        } else if (callback != null) {
            e();
            if (c) {
                Log.d("MediaRouter", "addCallback: selector=" + mediaRouteSelector + ", callback=" + callback + ", flags=" + Integer.toHexString(i));
            }
            int b2 = b(callback);
            if (b2 < 0) {
                callbackRecord = new CallbackRecord(this, callback);
                this.b.add(callbackRecord);
            } else {
                callbackRecord = this.b.get(b2);
            }
            boolean z = false;
            int i2 = callbackRecord.d;
            if (((~i2) & i) != 0) {
                callbackRecord.d = i2 | i;
                z = true;
            }
            if (!callbackRecord.c.a(mediaRouteSelector)) {
                callbackRecord.c = new MediaRouteSelector.Builder(callbackRecord.c).a(mediaRouteSelector).a();
                z = true;
            }
            if (z) {
                d.h();
            }
        } else {
            throw new IllegalArgumentException("callback must not be null");
        }
    }

    public void a(Callback callback) {
        if (callback != null) {
            e();
            if (c) {
                Log.d("MediaRouter", "removeCallback: callback=" + callback);
            }
            int b2 = b(callback);
            if (b2 >= 0) {
                this.b.remove(b2);
                d.h();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("callback must not be null");
    }

    public void a(MediaSessionCompat mediaSessionCompat) {
        if (c) {
            Log.d("MediaRouter", "addMediaSessionCompat: " + mediaSessionCompat);
        }
        d.a(mediaSessionCompat);
    }
}
