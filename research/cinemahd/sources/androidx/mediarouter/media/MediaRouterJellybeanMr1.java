package androidx.mediarouter.media;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.media.MediaRouter;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import androidx.mediarouter.media.MediaRouterJellybean;
import com.facebook.react.uimanager.ViewProps;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

final class MediaRouterJellybeanMr1 {

    public static final class ActiveScanWorkaround implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final DisplayManager f858a;
        private final Handler b;
        private Method c;
        private boolean d;

        public ActiveScanWorkaround(Context context, Handler handler) {
            if (Build.VERSION.SDK_INT == 17) {
                this.f858a = (DisplayManager) context.getSystemService(ViewProps.DISPLAY);
                this.b = handler;
                try {
                    this.c = DisplayManager.class.getMethod("scanWifiDisplays", new Class[0]);
                } catch (NoSuchMethodException unused) {
                }
            } else {
                throw new UnsupportedOperationException();
            }
        }

        public void a(int i) {
            if ((i & 2) != 0) {
                if (this.d) {
                    return;
                }
                if (this.c != null) {
                    this.d = true;
                    this.b.post(this);
                    return;
                }
                Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays because the DisplayManager.scanWifiDisplays() method is not available on this device.");
            } else if (this.d) {
                this.d = false;
                this.b.removeCallbacks(this);
            }
        }

        public void run() {
            if (this.d) {
                try {
                    this.c.invoke(this.f858a, new Object[0]);
                } catch (IllegalAccessException e) {
                    Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays.", e);
                } catch (InvocationTargetException e2) {
                    Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays.", e2);
                }
                this.b.postDelayed(this, 15000);
            }
        }
    }

    public interface Callback extends MediaRouterJellybean.Callback {
        void c(Object obj);
    }

    static class CallbackProxy<T extends Callback> extends MediaRouterJellybean.CallbackProxy<T> {
        public CallbackProxy(T t) {
            super(t);
        }

        public void onRoutePresentationDisplayChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            ((Callback) this.f854a).c(routeInfo);
        }
    }

    public static final class IsConnectingWorkaround {

        /* renamed from: a  reason: collision with root package name */
        private Method f859a;
        private int b;

        public IsConnectingWorkaround() {
            if (Build.VERSION.SDK_INT == 17) {
                try {
                    this.b = MediaRouter.RouteInfo.class.getField("STATUS_CONNECTING").getInt((Object) null);
                    this.f859a = MediaRouter.RouteInfo.class.getMethod("getStatusCode", new Class[0]);
                } catch (IllegalAccessException | NoSuchFieldException | NoSuchMethodException unused) {
                }
            } else {
                throw new UnsupportedOperationException();
            }
        }

        public boolean a(Object obj) {
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) obj;
            Method method = this.f859a;
            if (method == null) {
                return false;
            }
            try {
                if (((Integer) method.invoke(routeInfo, new Object[0])).intValue() == this.b) {
                    return true;
                }
                return false;
            } catch (IllegalAccessException | InvocationTargetException unused) {
                return false;
            }
        }
    }

    public static final class RouteInfo {
        private RouteInfo() {
        }

        public static Display a(Object obj) {
            try {
                return ((MediaRouter.RouteInfo) obj).getPresentationDisplay();
            } catch (NoSuchMethodError e) {
                Log.w("MediaRouterJellybeanMr1", "Cannot get presentation display for the route.", e);
                return null;
            }
        }

        public static boolean b(Object obj) {
            return ((MediaRouter.RouteInfo) obj).isEnabled();
        }
    }

    private MediaRouterJellybeanMr1() {
    }

    public static Object a(Callback callback) {
        return new CallbackProxy(callback);
    }
}
