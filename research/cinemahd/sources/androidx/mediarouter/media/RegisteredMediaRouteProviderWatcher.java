package androidx.mediarouter.media;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Handler;
import java.util.ArrayList;
import java.util.Collections;

final class RegisteredMediaRouteProviderWatcher {

    /* renamed from: a  reason: collision with root package name */
    private final Context f865a;
    private final Callback b;
    private final Handler c;
    private final PackageManager d;
    private final ArrayList<RegisteredMediaRouteProvider> e = new ArrayList<>();
    private boolean f;
    private final BroadcastReceiver g = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            RegisteredMediaRouteProviderWatcher.this.a();
        }
    };
    private final Runnable h = new Runnable() {
        public void run() {
            RegisteredMediaRouteProviderWatcher.this.a();
        }
    };

    public interface Callback {
        void a(MediaRouteProvider mediaRouteProvider);

        void b(MediaRouteProvider mediaRouteProvider);
    }

    public RegisteredMediaRouteProviderWatcher(Context context, Callback callback) {
        this.f865a = context;
        this.b = callback;
        this.c = new Handler();
        this.d = context.getPackageManager();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int i;
        if (this.f) {
            int i2 = 0;
            for (ResolveInfo resolveInfo : this.d.queryIntentServices(new Intent("android.media.MediaRouteProviderService"), 0)) {
                ServiceInfo serviceInfo = resolveInfo.serviceInfo;
                if (serviceInfo != null) {
                    int a2 = a(serviceInfo.packageName, serviceInfo.name);
                    if (a2 < 0) {
                        RegisteredMediaRouteProvider registeredMediaRouteProvider = new RegisteredMediaRouteProvider(this.f865a, new ComponentName(serviceInfo.packageName, serviceInfo.name));
                        registeredMediaRouteProvider.i();
                        i = i2 + 1;
                        this.e.add(i2, registeredMediaRouteProvider);
                        this.b.a(registeredMediaRouteProvider);
                    } else if (a2 >= i2) {
                        RegisteredMediaRouteProvider registeredMediaRouteProvider2 = this.e.get(a2);
                        registeredMediaRouteProvider2.i();
                        registeredMediaRouteProvider2.h();
                        i = i2 + 1;
                        Collections.swap(this.e, a2, i2);
                    }
                    i2 = i;
                }
            }
            if (i2 < this.e.size()) {
                for (int size = this.e.size() - 1; size >= i2; size--) {
                    RegisteredMediaRouteProvider registeredMediaRouteProvider3 = this.e.get(size);
                    this.b.b(registeredMediaRouteProvider3);
                    this.e.remove(registeredMediaRouteProvider3);
                    registeredMediaRouteProvider3.j();
                }
            }
        }
    }

    public void b() {
        if (!this.f) {
            this.f = true;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
            intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
            intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
            intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
            intentFilter.addAction("android.intent.action.PACKAGE_RESTARTED");
            intentFilter.addDataScheme("package");
            this.f865a.registerReceiver(this.g, intentFilter, (String) null, this.c);
            this.c.post(this.h);
        }
    }

    private int a(String str, String str2) {
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            if (this.e.get(i).b(str, str2)) {
                return i;
            }
        }
        return -1;
    }
}
