package androidx.mediarouter.media;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Build;
import android.view.Display;
import androidx.mediarouter.R$string;
import androidx.mediarouter.media.MediaRouteDescriptor;
import androidx.mediarouter.media.MediaRouteProvider;
import androidx.mediarouter.media.MediaRouteProviderDescriptor;
import androidx.mediarouter.media.MediaRouter;
import androidx.mediarouter.media.MediaRouterJellybean;
import androidx.mediarouter.media.MediaRouterJellybeanMr1;
import androidx.mediarouter.media.MediaRouterJellybeanMr2;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

abstract class SystemMediaRouteProvider extends MediaRouteProvider {

    private static class Api24Impl extends JellybeanMr2Impl {
        public Api24Impl(Context context, SyncCallback syncCallback) {
            super(context, syncCallback);
        }

        /* access modifiers changed from: protected */
        public void a(JellybeanImpl.SystemRouteRecord systemRouteRecord, MediaRouteDescriptor.Builder builder) {
            super.a(systemRouteRecord, builder);
            builder.a(MediaRouterApi24$RouteInfo.a(systemRouteRecord.f872a));
        }
    }

    static class JellybeanImpl extends SystemMediaRouteProvider implements MediaRouterJellybean.Callback, MediaRouterJellybean.VolumeCallback {
        private static final ArrayList<IntentFilter> u = new ArrayList<>();
        private static final ArrayList<IntentFilter> v = new ArrayList<>();
        private final SyncCallback i;
        protected final Object j;
        protected final Object k;
        protected final Object l;
        protected final Object m;
        protected int n;
        protected boolean o;
        protected boolean p;
        protected final ArrayList<SystemRouteRecord> q = new ArrayList<>();
        protected final ArrayList<UserRouteRecord> r = new ArrayList<>();
        private MediaRouterJellybean.SelectRouteWorkaround s;
        private MediaRouterJellybean.GetDefaultRouteWorkaround t;

        protected static final class SystemRouteController extends MediaRouteProvider.RouteController {

            /* renamed from: a  reason: collision with root package name */
            private final Object f871a;

            public SystemRouteController(Object obj) {
                this.f871a = obj;
            }

            public void a(int i) {
                MediaRouterJellybean.RouteInfo.a(this.f871a, i);
            }

            public void c(int i) {
                MediaRouterJellybean.RouteInfo.b(this.f871a, i);
            }
        }

        protected static final class SystemRouteRecord {

            /* renamed from: a  reason: collision with root package name */
            public final Object f872a;
            public final String b;
            public MediaRouteDescriptor c;

            public SystemRouteRecord(Object obj, String str) {
                this.f872a = obj;
                this.b = str;
            }
        }

        protected static final class UserRouteRecord {

            /* renamed from: a  reason: collision with root package name */
            public final MediaRouter.RouteInfo f873a;
            public final Object b;

            public UserRouteRecord(MediaRouter.RouteInfo routeInfo, Object obj) {
                this.f873a = routeInfo;
                this.b = obj;
            }
        }

        static {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addCategory("android.media.intent.category.LIVE_AUDIO");
            u.add(intentFilter);
            IntentFilter intentFilter2 = new IntentFilter();
            intentFilter2.addCategory("android.media.intent.category.LIVE_VIDEO");
            v.add(intentFilter2);
        }

        public JellybeanImpl(Context context, SyncCallback syncCallback) {
            super(context);
            this.i = syncCallback;
            this.j = MediaRouterJellybean.a(context);
            this.k = h();
            this.l = i();
            this.m = MediaRouterJellybean.a(this.j, context.getResources().getString(R$string.mr_user_route_category_name), false);
            m();
        }

        private boolean j(Object obj) {
            if (h(obj) != null || f(obj) >= 0) {
                return false;
            }
            SystemRouteRecord systemRouteRecord = new SystemRouteRecord(obj, k(obj));
            a(systemRouteRecord);
            this.q.add(systemRouteRecord);
            return true;
        }

        private String k(Object obj) {
            String str;
            if (j() == obj) {
                str = "DEFAULT_ROUTE";
            } else {
                str = String.format(Locale.US, "ROUTE_%08x", new Object[]{Integer.valueOf(g(obj).hashCode())});
            }
            if (b(str) < 0) {
                return str;
            }
            int i2 = 2;
            while (true) {
                String format = String.format(Locale.US, "%s_%d", new Object[]{str, Integer.valueOf(i2)});
                if (b(format) < 0) {
                    return format;
                }
                i2++;
            }
        }

        private void m() {
            l();
            boolean z = false;
            for (Object j2 : MediaRouterJellybean.a(this.j)) {
                z |= j(j2);
            }
            if (z) {
                k();
            }
        }

        public MediaRouteProvider.RouteController a(String str) {
            int b = b(str);
            if (b >= 0) {
                return new SystemRouteController(this.q.get(b).f872a);
            }
            return null;
        }

        public void a(int i2, Object obj) {
        }

        public void a(Object obj, Object obj2) {
        }

        public void a(Object obj, Object obj2, int i2) {
        }

        public void b(Object obj) {
            int f;
            if (h(obj) == null && (f = f(obj)) >= 0) {
                this.q.remove(f);
                k();
            }
        }

        public void c(MediaRouter.RouteInfo routeInfo) {
            int e;
            if (routeInfo.n() != this && (e = e(routeInfo)) >= 0) {
                UserRouteRecord remove = this.r.remove(e);
                MediaRouterJellybean.RouteInfo.a(remove.b, (Object) null);
                MediaRouterJellybean.UserRouteInfo.b(remove.b, (Object) null);
                MediaRouterJellybean.d(this.j, remove.b);
            }
        }

        public void d(Object obj) {
            if (j(obj)) {
                k();
            }
        }

        public void e(Object obj) {
            int f;
            if (h(obj) == null && (f = f(obj)) >= 0) {
                SystemRouteRecord systemRouteRecord = this.q.get(f);
                int e = MediaRouterJellybean.RouteInfo.e(obj);
                if (e != systemRouteRecord.c.s()) {
                    MediaRouteDescriptor.Builder builder = new MediaRouteDescriptor.Builder(systemRouteRecord.c);
                    builder.e(e);
                    systemRouteRecord.c = builder.a();
                    k();
                }
            }
        }

        /* access modifiers changed from: protected */
        public int f(Object obj) {
            int size = this.q.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.q.get(i2).f872a == obj) {
                    return i2;
                }
            }
            return -1;
        }

        /* access modifiers changed from: protected */
        public String g(Object obj) {
            CharSequence a2 = MediaRouterJellybean.RouteInfo.a(obj, c());
            return a2 != null ? a2.toString() : "";
        }

        /* access modifiers changed from: protected */
        public UserRouteRecord h(Object obj) {
            Object d = MediaRouterJellybean.RouteInfo.d(obj);
            if (d instanceof UserRouteRecord) {
                return (UserRouteRecord) d;
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public Object i() {
            return MediaRouterJellybean.a((MediaRouterJellybean.VolumeCallback) this);
        }

        /* access modifiers changed from: protected */
        public void l() {
            if (this.p) {
                this.p = false;
                MediaRouterJellybean.c(this.j, this.k);
            }
            int i2 = this.n;
            if (i2 != 0) {
                this.p = true;
                MediaRouterJellybean.a(this.j, i2, this.k);
            }
        }

        /* access modifiers changed from: protected */
        public void i(Object obj) {
            if (this.s == null) {
                this.s = new MediaRouterJellybean.SelectRouteWorkaround();
            }
            this.s.a(this.j, 8388611, obj);
        }

        public void d(MediaRouter.RouteInfo routeInfo) {
            if (routeInfo.w()) {
                if (routeInfo.n() != this) {
                    int e = e(routeInfo);
                    if (e >= 0) {
                        i(this.r.get(e).b);
                        return;
                    }
                    return;
                }
                int b = b(routeInfo.d());
                if (b >= 0) {
                    i(this.q.get(b).f872a);
                }
            }
        }

        /* access modifiers changed from: protected */
        public Object h() {
            return MediaRouterJellybean.a((MediaRouterJellybean.Callback) this);
        }

        public void a(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
            boolean z;
            int i2 = 0;
            if (mediaRouteDiscoveryRequest != null) {
                List<String> c = mediaRouteDiscoveryRequest.b().c();
                int size = c.size();
                int i3 = 0;
                while (i2 < size) {
                    String str = c.get(i2);
                    if (str.equals("android.media.intent.category.LIVE_AUDIO")) {
                        i3 |= 1;
                    } else {
                        i3 = str.equals("android.media.intent.category.LIVE_VIDEO") ? i3 | 2 : i3 | 8388608;
                    }
                    i2++;
                }
                z = mediaRouteDiscoveryRequest.c();
                i2 = i3;
            } else {
                z = false;
            }
            if (this.n != i2 || this.o != z) {
                this.n = i2;
                this.o = z;
                m();
            }
        }

        public void b(int i2, Object obj) {
            if (obj == MediaRouterJellybean.a(this.j, 8388611)) {
                UserRouteRecord h = h(obj);
                if (h != null) {
                    h.f873a.x();
                    return;
                }
                int f = f(obj);
                if (f >= 0) {
                    this.i.a(this.q.get(f).b);
                }
            }
        }

        /* access modifiers changed from: protected */
        public Object j() {
            if (this.t == null) {
                this.t = new MediaRouterJellybean.GetDefaultRouteWorkaround();
            }
            return this.t.a(this.j);
        }

        /* access modifiers changed from: protected */
        public void k() {
            MediaRouteProviderDescriptor.Builder builder = new MediaRouteProviderDescriptor.Builder();
            int size = this.q.size();
            for (int i2 = 0; i2 < size; i2++) {
                builder.a(this.q.get(i2).c);
            }
            a(builder.a());
        }

        /* access modifiers changed from: protected */
        public int e(MediaRouter.RouteInfo routeInfo) {
            int size = this.r.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.r.get(i2).f873a == routeInfo) {
                    return i2;
                }
            }
            return -1;
        }

        public void b(Object obj, int i2) {
            UserRouteRecord h = h(obj);
            if (h != null) {
                h.f873a.a(i2);
            }
        }

        public void b(MediaRouter.RouteInfo routeInfo) {
            int e;
            if (routeInfo.n() != this && (e = e(routeInfo)) >= 0) {
                a(this.r.get(e));
            }
        }

        public void a(Object obj) {
            int f;
            if (h(obj) == null && (f = f(obj)) >= 0) {
                a(this.q.get(f));
                k();
            }
        }

        /* access modifiers changed from: protected */
        public int b(String str) {
            int size = this.q.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.q.get(i2).b.equals(str)) {
                    return i2;
                }
            }
            return -1;
        }

        public void a(Object obj, int i2) {
            UserRouteRecord h = h(obj);
            if (h != null) {
                h.f873a.b(i2);
            }
        }

        public void a(MediaRouter.RouteInfo routeInfo) {
            if (routeInfo.n() != this) {
                Object b = MediaRouterJellybean.b(this.j, this.m);
                UserRouteRecord userRouteRecord = new UserRouteRecord(routeInfo, b);
                MediaRouterJellybean.RouteInfo.a(b, (Object) userRouteRecord);
                MediaRouterJellybean.UserRouteInfo.b(b, this.l);
                a(userRouteRecord);
                this.r.add(userRouteRecord);
                MediaRouterJellybean.a(this.j, b);
                return;
            }
            int f = f(MediaRouterJellybean.a(this.j, 8388611));
            if (f >= 0 && this.q.get(f).b.equals(routeInfo.d())) {
                routeInfo.x();
            }
        }

        /* access modifiers changed from: protected */
        public void a(SystemRouteRecord systemRouteRecord) {
            MediaRouteDescriptor.Builder builder = new MediaRouteDescriptor.Builder(systemRouteRecord.b, g(systemRouteRecord.f872a));
            a(systemRouteRecord, builder);
            systemRouteRecord.c = builder.a();
        }

        /* access modifiers changed from: protected */
        public void a(SystemRouteRecord systemRouteRecord, MediaRouteDescriptor.Builder builder) {
            int c = MediaRouterJellybean.RouteInfo.c(systemRouteRecord.f872a);
            if ((c & 1) != 0) {
                builder.a((Collection<IntentFilter>) u);
            }
            if ((c & 2) != 0) {
                builder.a((Collection<IntentFilter>) v);
            }
            builder.c(MediaRouterJellybean.RouteInfo.b(systemRouteRecord.f872a));
            builder.b(MediaRouterJellybean.RouteInfo.a(systemRouteRecord.f872a));
            builder.e(MediaRouterJellybean.RouteInfo.e(systemRouteRecord.f872a));
            builder.g(MediaRouterJellybean.RouteInfo.g(systemRouteRecord.f872a));
            builder.f(MediaRouterJellybean.RouteInfo.f(systemRouteRecord.f872a));
        }

        /* access modifiers changed from: protected */
        public void a(UserRouteRecord userRouteRecord) {
            MediaRouterJellybean.UserRouteInfo.a(userRouteRecord.b, (CharSequence) userRouteRecord.f873a.i());
            MediaRouterJellybean.UserRouteInfo.b(userRouteRecord.b, userRouteRecord.f873a.k());
            MediaRouterJellybean.UserRouteInfo.a(userRouteRecord.b, userRouteRecord.f873a.j());
            MediaRouterJellybean.UserRouteInfo.c(userRouteRecord.b, userRouteRecord.f873a.o());
            MediaRouterJellybean.UserRouteInfo.e(userRouteRecord.b, userRouteRecord.f873a.q());
            MediaRouterJellybean.UserRouteInfo.d(userRouteRecord.b, userRouteRecord.f873a.p());
        }
    }

    private static class JellybeanMr1Impl extends JellybeanImpl implements MediaRouterJellybeanMr1.Callback {
        private MediaRouterJellybeanMr1.ActiveScanWorkaround w;
        private MediaRouterJellybeanMr1.IsConnectingWorkaround x;

        public JellybeanMr1Impl(Context context, SyncCallback syncCallback) {
            super(context, syncCallback);
        }

        /* access modifiers changed from: protected */
        public void a(JellybeanImpl.SystemRouteRecord systemRouteRecord, MediaRouteDescriptor.Builder builder) {
            super.a(systemRouteRecord, builder);
            if (!MediaRouterJellybeanMr1.RouteInfo.b(systemRouteRecord.f872a)) {
                builder.b(false);
            }
            if (b(systemRouteRecord)) {
                builder.a(true);
            }
            Display a2 = MediaRouterJellybeanMr1.RouteInfo.a(systemRouteRecord.f872a);
            if (a2 != null) {
                builder.d(a2.getDisplayId());
            }
        }

        /* access modifiers changed from: protected */
        public boolean b(JellybeanImpl.SystemRouteRecord systemRouteRecord) {
            if (this.x == null) {
                this.x = new MediaRouterJellybeanMr1.IsConnectingWorkaround();
            }
            return this.x.a(systemRouteRecord.f872a);
        }

        public void c(Object obj) {
            int f = f(obj);
            if (f >= 0) {
                JellybeanImpl.SystemRouteRecord systemRouteRecord = this.q.get(f);
                Display a2 = MediaRouterJellybeanMr1.RouteInfo.a(obj);
                int displayId = a2 != null ? a2.getDisplayId() : -1;
                if (displayId != systemRouteRecord.c.q()) {
                    MediaRouteDescriptor.Builder builder = new MediaRouteDescriptor.Builder(systemRouteRecord.c);
                    builder.d(displayId);
                    systemRouteRecord.c = builder.a();
                    k();
                }
            }
        }

        /* access modifiers changed from: protected */
        public Object h() {
            return MediaRouterJellybeanMr1.a(this);
        }

        /* access modifiers changed from: protected */
        public void l() {
            super.l();
            if (this.w == null) {
                this.w = new MediaRouterJellybeanMr1.ActiveScanWorkaround(c(), f());
            }
            this.w.a(this.o ? this.n : 0);
        }
    }

    static class LegacyImpl extends SystemMediaRouteProvider {
        private static final ArrayList<IntentFilter> l = new ArrayList<>();
        final AudioManager i;
        private final VolumeChangeReceiver j;
        int k = -1;

        final class DefaultRouteController extends MediaRouteProvider.RouteController {
            DefaultRouteController() {
            }

            public void a(int i) {
                LegacyImpl.this.i.setStreamVolume(3, i, 0);
                LegacyImpl.this.h();
            }

            public void c(int i) {
                int streamVolume = LegacyImpl.this.i.getStreamVolume(3);
                if (Math.min(LegacyImpl.this.i.getStreamMaxVolume(3), Math.max(0, i + streamVolume)) != streamVolume) {
                    LegacyImpl.this.i.setStreamVolume(3, streamVolume, 0);
                }
                LegacyImpl.this.h();
            }
        }

        final class VolumeChangeReceiver extends BroadcastReceiver {
            VolumeChangeReceiver() {
            }

            public void onReceive(Context context, Intent intent) {
                int intExtra;
                if (intent.getAction().equals("android.media.VOLUME_CHANGED_ACTION") && intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1) == 3 && (intExtra = intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", -1)) >= 0) {
                    LegacyImpl legacyImpl = LegacyImpl.this;
                    if (intExtra != legacyImpl.k) {
                        legacyImpl.h();
                    }
                }
            }
        }

        static {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addCategory("android.media.intent.category.LIVE_AUDIO");
            intentFilter.addCategory("android.media.intent.category.LIVE_VIDEO");
            l.add(intentFilter);
        }

        public LegacyImpl(Context context) {
            super(context);
            this.i = (AudioManager) context.getSystemService("audio");
            this.j = new VolumeChangeReceiver();
            context.registerReceiver(this.j, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
            h();
        }

        public MediaRouteProvider.RouteController a(String str) {
            if (str.equals("DEFAULT_ROUTE")) {
                return new DefaultRouteController();
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public void h() {
            Resources resources = c().getResources();
            int streamMaxVolume = this.i.getStreamMaxVolume(3);
            this.k = this.i.getStreamVolume(3);
            MediaRouteDescriptor.Builder builder = new MediaRouteDescriptor.Builder("DEFAULT_ROUTE", resources.getString(R$string.mr_system_route_name));
            builder.a((Collection<IntentFilter>) l);
            builder.b(3);
            builder.c(0);
            builder.f(1);
            builder.g(streamMaxVolume);
            builder.e(this.k);
            a(new MediaRouteProviderDescriptor.Builder().a(builder.a()).a());
        }
    }

    public interface SyncCallback {
        void a(String str);
    }

    protected SystemMediaRouteProvider(Context context) {
        super(context, new MediaRouteProvider.ProviderMetadata(new ComponentName("android", SystemMediaRouteProvider.class.getName())));
    }

    public static SystemMediaRouteProvider a(Context context, SyncCallback syncCallback) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            return new Api24Impl(context, syncCallback);
        }
        if (i >= 18) {
            return new JellybeanMr2Impl(context, syncCallback);
        }
        if (i >= 17) {
            return new JellybeanMr1Impl(context, syncCallback);
        }
        if (i >= 16) {
            return new JellybeanImpl(context, syncCallback);
        }
        return new LegacyImpl(context);
    }

    public void a(MediaRouter.RouteInfo routeInfo) {
    }

    public void b(MediaRouter.RouteInfo routeInfo) {
    }

    public void c(MediaRouter.RouteInfo routeInfo) {
    }

    public void d(MediaRouter.RouteInfo routeInfo) {
    }

    private static class JellybeanMr2Impl extends JellybeanMr1Impl {
        public JellybeanMr2Impl(Context context, SyncCallback syncCallback) {
            super(context, syncCallback);
        }

        /* access modifiers changed from: protected */
        public void a(JellybeanImpl.SystemRouteRecord systemRouteRecord, MediaRouteDescriptor.Builder builder) {
            super.a(systemRouteRecord, builder);
            CharSequence a2 = MediaRouterJellybeanMr2.RouteInfo.a(systemRouteRecord.f872a);
            if (a2 != null) {
                builder.a(a2.toString());
            }
        }

        /* access modifiers changed from: protected */
        public boolean b(JellybeanImpl.SystemRouteRecord systemRouteRecord) {
            return MediaRouterJellybeanMr2.RouteInfo.b(systemRouteRecord.f872a);
        }

        /* access modifiers changed from: protected */
        public void i(Object obj) {
            MediaRouterJellybean.b(this.j, 8388611, obj);
        }

        /* access modifiers changed from: protected */
        public Object j() {
            return MediaRouterJellybeanMr2.a(this.j);
        }

        /* access modifiers changed from: protected */
        public void l() {
            if (this.p) {
                MediaRouterJellybean.c(this.j, this.k);
            }
            this.p = true;
            MediaRouterJellybeanMr2.a(this.j, this.n, this.k, this.o | true ? 1 : 0);
        }

        /* access modifiers changed from: protected */
        public void a(JellybeanImpl.UserRouteRecord userRouteRecord) {
            super.a(userRouteRecord);
            MediaRouterJellybeanMr2.UserRouteInfo.a(userRouteRecord.b, userRouteRecord.f873a.c());
        }
    }
}
