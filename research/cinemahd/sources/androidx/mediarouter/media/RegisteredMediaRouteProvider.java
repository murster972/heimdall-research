package androidx.mediarouter.media;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import androidx.mediarouter.media.MediaRouteProvider;
import androidx.mediarouter.media.MediaRouter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

final class RegisteredMediaRouteProvider extends MediaRouteProvider implements ServiceConnection {
    static final boolean p = Log.isLoggable("MediaRouteProviderProxy", 3);
    private final ComponentName i;
    final PrivateHandler j;
    private final ArrayList<Controller> k = new ArrayList<>();
    private boolean l;
    private boolean m;
    private Connection n;
    private boolean o;

    private final class Controller extends MediaRouteProvider.RouteController {

        /* renamed from: a  reason: collision with root package name */
        private final String f863a;
        private final String b;
        private boolean c;
        private int d = -1;
        private int e;
        private Connection f;
        private int g;

        public Controller(String str, String str2) {
            this.f863a = str;
            this.b = str2;
        }

        public void a(Connection connection) {
            this.f = connection;
            this.g = connection.a(this.f863a, this.b);
            if (this.c) {
                connection.d(this.g);
                int i = this.d;
                if (i >= 0) {
                    connection.a(this.g, i);
                    this.d = -1;
                }
                int i2 = this.e;
                if (i2 != 0) {
                    connection.c(this.g, i2);
                    this.e = 0;
                }
            }
        }

        public void b() {
            this.c = true;
            Connection connection = this.f;
            if (connection != null) {
                connection.d(this.g);
            }
        }

        public void c() {
            b(0);
        }

        public void d() {
            Connection connection = this.f;
            if (connection != null) {
                connection.c(this.g);
                this.f = null;
                this.g = 0;
            }
        }

        public void c(int i) {
            Connection connection = this.f;
            if (connection != null) {
                connection.c(this.g, i);
            } else {
                this.e += i;
            }
        }

        public void b(int i) {
            this.c = false;
            Connection connection = this.f;
            if (connection != null) {
                connection.b(this.g, i);
            }
        }

        public void a() {
            RegisteredMediaRouteProvider.this.a(this);
        }

        public void a(int i) {
            Connection connection = this.f;
            if (connection != null) {
                connection.a(this.g, i);
                return;
            }
            this.d = i;
            this.e = 0;
        }
    }

    private static final class PrivateHandler extends Handler {
        PrivateHandler() {
        }
    }

    private static final class ReceiveHandler extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<Connection> f864a;

        public ReceiveHandler(Connection connection) {
            this.f864a = new WeakReference<>(connection);
        }

        public void a() {
            this.f864a.clear();
        }

        public void handleMessage(Message message) {
            Connection connection = (Connection) this.f864a.get();
            if (connection != null) {
                if (!a(connection, message.what, message.arg1, message.arg2, message.obj, message.peekData()) && RegisteredMediaRouteProvider.p) {
                    Log.d("MediaRouteProviderProxy", "Unhandled message from server: " + message);
                }
            }
        }

        private boolean a(Connection connection, int i, int i2, int i3, Object obj, Bundle bundle) {
            String str;
            if (i == 0) {
                connection.a(i2);
                return true;
            } else if (i == 1) {
                connection.b(i2);
                return true;
            } else if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        if (i != 5) {
                            return false;
                        }
                        if (obj == null || (obj instanceof Bundle)) {
                            return connection.a((Bundle) obj);
                        }
                        return false;
                    } else if (obj != null && !(obj instanceof Bundle)) {
                        return false;
                    } else {
                        if (bundle == null) {
                            str = null;
                        } else {
                            str = bundle.getString("error");
                        }
                        return connection.a(i2, str, (Bundle) obj);
                    }
                } else if (obj == null || (obj instanceof Bundle)) {
                    return connection.a(i2, (Bundle) obj);
                } else {
                    return false;
                }
            } else if (obj == null || (obj instanceof Bundle)) {
                return connection.a(i2, i3, (Bundle) obj);
            } else {
                return false;
            }
        }
    }

    public RegisteredMediaRouteProvider(Context context, ComponentName componentName) {
        super(context, new MediaRouteProvider.ProviderMetadata(componentName));
        this.i = componentName;
        this.j = new PrivateHandler();
    }

    private MediaRouteProvider.RouteController c(String str, String str2) {
        MediaRouteProviderDescriptor d = d();
        if (d == null) {
            return null;
        }
        List<MediaRouteDescriptor> b = d.b();
        int size = b.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (b.get(i2).k().equals(str)) {
                Controller controller = new Controller(str, str2);
                this.k.add(controller);
                if (this.o) {
                    controller.a(this.n);
                }
                q();
                return controller;
            }
        }
        return null;
    }

    private void k() {
        int size = this.k.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.k.get(i2).a(this.n);
        }
    }

    private void l() {
        if (!this.m) {
            if (p) {
                Log.d("MediaRouteProviderProxy", this + ": Binding");
            }
            Intent intent = new Intent("android.media.MediaRouteProviderService");
            intent.setComponent(this.i);
            try {
                this.m = c().bindService(intent, this, 1);
                if (!this.m && p) {
                    Log.d("MediaRouteProviderProxy", this + ": Bind failed");
                }
            } catch (SecurityException e) {
                if (p) {
                    Log.d("MediaRouteProviderProxy", this + ": Bind failed", e);
                }
            }
        }
    }

    private void m() {
        int size = this.k.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.k.get(i2).d();
        }
    }

    private void n() {
        if (this.n != null) {
            a((MediaRouteProviderDescriptor) null);
            this.o = false;
            m();
            this.n.a();
            this.n = null;
        }
    }

    private boolean o() {
        if (!this.l) {
            return false;
        }
        if (e() == null && this.k.isEmpty()) {
            return false;
        }
        return true;
    }

    private void p() {
        if (this.m) {
            if (p) {
                Log.d("MediaRouteProviderProxy", this + ": Unbinding");
            }
            this.m = false;
            n();
            c().unbindService(this);
        }
    }

    private void q() {
        if (o()) {
            l();
        } else {
            p();
        }
    }

    public MediaRouteProvider.RouteController a(String str) {
        if (str != null) {
            return c(str, (String) null);
        }
        throw new IllegalArgumentException("routeId cannot be null");
    }

    public boolean b(String str, String str2) {
        return this.i.getPackageName().equals(str) && this.i.getClassName().equals(str2);
    }

    public void h() {
        if (this.n == null && o()) {
            p();
            l();
        }
    }

    public void i() {
        if (!this.l) {
            if (p) {
                Log.d("MediaRouteProviderProxy", this + ": Starting");
            }
            this.l = true;
            q();
        }
    }

    public void j() {
        if (this.l) {
            if (p) {
                Log.d("MediaRouteProviderProxy", this + ": Stopping");
            }
            this.l = false;
            q();
        }
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (p) {
            Log.d("MediaRouteProviderProxy", this + ": Connected");
        }
        if (this.m) {
            n();
            Messenger messenger = iBinder != null ? new Messenger(iBinder) : null;
            if (MediaRouteProviderProtocol.a(messenger)) {
                Connection connection = new Connection(messenger);
                if (connection.c()) {
                    this.n = connection;
                } else if (p) {
                    Log.d("MediaRouteProviderProxy", this + ": Registration failed");
                }
            } else {
                Log.e("MediaRouteProviderProxy", this + ": Service returned invalid messenger binder");
            }
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (p) {
            Log.d("MediaRouteProviderProxy", this + ": Service disconnected");
        }
        n();
    }

    public String toString() {
        return "Service connection " + this.i.flattenToShortString();
    }

    private final class Connection implements IBinder.DeathRecipient {

        /* renamed from: a  reason: collision with root package name */
        private final Messenger f860a;
        private final ReceiveHandler b;
        private final Messenger c;
        private int d = 1;
        private int e = 1;
        private int f;
        private int g;
        private final SparseArray<MediaRouter.ControlRequestCallback> h = new SparseArray<>();

        public Connection(Messenger messenger) {
            this.f860a = messenger;
            this.b = new ReceiveHandler(this);
            this.c = new Messenger(this.b);
        }

        public void a() {
            a(2, 0, 0, (Object) null, (Bundle) null);
            this.b.a();
            this.f860a.getBinder().unlinkToDeath(this, 0);
            RegisteredMediaRouteProvider.this.j.post(new Runnable() {
                public void run() {
                    Connection.this.b();
                }
            });
        }

        /* access modifiers changed from: package-private */
        public void b() {
            for (int i2 = 0; i2 < this.h.size(); i2++) {
                this.h.valueAt(i2).a((String) null, (Bundle) null);
            }
            this.h.clear();
        }

        public boolean b(int i2) {
            return true;
        }

        public void binderDied() {
            RegisteredMediaRouteProvider.this.j.post(new Runnable() {
                public void run() {
                    Connection connection = Connection.this;
                    RegisteredMediaRouteProvider.this.a(connection);
                }
            });
        }

        public boolean c() {
            int i2 = this.d;
            this.d = i2 + 1;
            this.g = i2;
            if (!a(1, this.g, 2, (Object) null, (Bundle) null)) {
                return false;
            }
            try {
                this.f860a.getBinder().linkToDeath(this, 0);
                return true;
            } catch (RemoteException unused) {
                binderDied();
                return false;
            }
        }

        public void d(int i2) {
            int i3 = this.d;
            this.d = i3 + 1;
            a(5, i3, i2, (Object) null, (Bundle) null);
        }

        public void b(int i2, int i3) {
            Bundle bundle = new Bundle();
            bundle.putInt("unselectReason", i3);
            int i4 = this.d;
            this.d = i4 + 1;
            a(6, i4, i2, (Object) null, bundle);
        }

        public boolean a(int i2) {
            if (i2 == this.g) {
                this.g = 0;
                RegisteredMediaRouteProvider.this.a(this, "Registration failed");
            }
            MediaRouter.ControlRequestCallback controlRequestCallback = this.h.get(i2);
            if (controlRequestCallback == null) {
                return true;
            }
            this.h.remove(i2);
            controlRequestCallback.a((String) null, (Bundle) null);
            return true;
        }

        public void c(int i2) {
            int i3 = this.d;
            this.d = i3 + 1;
            a(4, i3, i2, (Object) null, (Bundle) null);
        }

        public void c(int i2, int i3) {
            Bundle bundle = new Bundle();
            bundle.putInt("volume", i3);
            int i4 = this.d;
            this.d = i4 + 1;
            a(8, i4, i2, (Object) null, bundle);
        }

        public boolean a(int i2, int i3, Bundle bundle) {
            if (this.f != 0 || i2 != this.g || i3 < 1) {
                return false;
            }
            this.g = 0;
            this.f = i3;
            RegisteredMediaRouteProvider.this.a(this, MediaRouteProviderDescriptor.a(bundle));
            RegisteredMediaRouteProvider.this.b(this);
            return true;
        }

        public boolean a(Bundle bundle) {
            if (this.f == 0) {
                return false;
            }
            RegisteredMediaRouteProvider.this.a(this, MediaRouteProviderDescriptor.a(bundle));
            return true;
        }

        public boolean a(int i2, Bundle bundle) {
            MediaRouter.ControlRequestCallback controlRequestCallback = this.h.get(i2);
            if (controlRequestCallback == null) {
                return false;
            }
            this.h.remove(i2);
            controlRequestCallback.a(bundle);
            return true;
        }

        public boolean a(int i2, String str, Bundle bundle) {
            MediaRouter.ControlRequestCallback controlRequestCallback = this.h.get(i2);
            if (controlRequestCallback == null) {
                return false;
            }
            this.h.remove(i2);
            controlRequestCallback.a(str, bundle);
            return true;
        }

        public int a(String str, String str2) {
            int i2 = this.e;
            this.e = i2 + 1;
            Bundle bundle = new Bundle();
            bundle.putString("routeId", str);
            bundle.putString("routeGroupId", str2);
            int i3 = this.d;
            this.d = i3 + 1;
            a(3, i3, i2, (Object) null, bundle);
            return i2;
        }

        public void a(int i2, int i3) {
            Bundle bundle = new Bundle();
            bundle.putInt("volume", i3);
            int i4 = this.d;
            this.d = i4 + 1;
            a(7, i4, i2, (Object) null, bundle);
        }

        public void a(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
            int i2 = this.d;
            this.d = i2 + 1;
            a(10, i2, 0, mediaRouteDiscoveryRequest != null ? mediaRouteDiscoveryRequest.a() : null, (Bundle) null);
        }

        private boolean a(int i2, int i3, int i4, Object obj, Bundle bundle) {
            Message obtain = Message.obtain();
            obtain.what = i2;
            obtain.arg1 = i3;
            obtain.arg2 = i4;
            obtain.obj = obj;
            obtain.setData(bundle);
            obtain.replyTo = this.c;
            try {
                this.f860a.send(obtain);
                return true;
            } catch (DeadObjectException unused) {
                return false;
            } catch (RemoteException e2) {
                if (i2 == 2) {
                    return false;
                }
                Log.e("MediaRouteProviderProxy", "Could not send message to service.", e2);
                return false;
            }
        }
    }

    public MediaRouteProvider.RouteController a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("routeId cannot be null");
        } else if (str2 != null) {
            return c(str, str2);
        } else {
            throw new IllegalArgumentException("routeGroupId cannot be null");
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Connection connection) {
        if (this.n == connection) {
            this.o = true;
            k();
            MediaRouteDiscoveryRequest e = e();
            if (e != null) {
                this.n.a(e);
            }
        }
    }

    public void a(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
        if (this.o) {
            this.n.a(mediaRouteDiscoveryRequest);
        }
        q();
    }

    /* access modifiers changed from: package-private */
    public void a(Connection connection) {
        if (this.n == connection) {
            if (p) {
                Log.d("MediaRouteProviderProxy", this + ": Service connection died");
            }
            n();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Connection connection, String str) {
        if (this.n == connection) {
            if (p) {
                Log.d("MediaRouteProviderProxy", this + ": Service connection error - " + str);
            }
            p();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Connection connection, MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
        if (this.n == connection) {
            if (p) {
                Log.d("MediaRouteProviderProxy", this + ": Descriptor changed, descriptor=" + mediaRouteProviderDescriptor);
            }
            a(mediaRouteProviderDescriptor);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Controller controller) {
        this.k.remove(controller);
        controller.d();
        q();
    }
}
