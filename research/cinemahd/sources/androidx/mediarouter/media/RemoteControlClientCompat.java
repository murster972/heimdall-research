package androidx.mediarouter.media;

import android.content.Context;
import android.os.Build;
import androidx.mediarouter.media.MediaRouterJellybean;
import java.lang.ref.WeakReference;

abstract class RemoteControlClientCompat {

    /* renamed from: a  reason: collision with root package name */
    protected final Object f868a;
    protected VolumeCallback b;

    static class JellybeanImpl extends RemoteControlClientCompat {
        private final Object c;
        private final Object d = MediaRouterJellybean.a(this.c, "", false);
        private final Object e = MediaRouterJellybean.b(this.c, this.d);
        private boolean f;

        private static final class VolumeCallbackWrapper implements MediaRouterJellybean.VolumeCallback {

            /* renamed from: a  reason: collision with root package name */
            private final WeakReference<JellybeanImpl> f869a;

            public VolumeCallbackWrapper(JellybeanImpl jellybeanImpl) {
                this.f869a = new WeakReference<>(jellybeanImpl);
            }

            public void a(Object obj, int i) {
                VolumeCallback volumeCallback;
                JellybeanImpl jellybeanImpl = (JellybeanImpl) this.f869a.get();
                if (jellybeanImpl != null && (volumeCallback = jellybeanImpl.b) != null) {
                    volumeCallback.b(i);
                }
            }

            public void b(Object obj, int i) {
                VolumeCallback volumeCallback;
                JellybeanImpl jellybeanImpl = (JellybeanImpl) this.f869a.get();
                if (jellybeanImpl != null && (volumeCallback = jellybeanImpl.b) != null) {
                    volumeCallback.a(i);
                }
            }
        }

        public JellybeanImpl(Context context, Object obj) {
            super(context, obj);
            this.c = MediaRouterJellybean.a(context);
        }

        public void a(PlaybackInfo playbackInfo) {
            MediaRouterJellybean.UserRouteInfo.c(this.e, playbackInfo.f870a);
            MediaRouterJellybean.UserRouteInfo.e(this.e, playbackInfo.b);
            MediaRouterJellybean.UserRouteInfo.d(this.e, playbackInfo.c);
            MediaRouterJellybean.UserRouteInfo.a(this.e, playbackInfo.d);
            MediaRouterJellybean.UserRouteInfo.b(this.e, playbackInfo.e);
            if (!this.f) {
                this.f = true;
                MediaRouterJellybean.UserRouteInfo.b(this.e, MediaRouterJellybean.a((MediaRouterJellybean.VolumeCallback) new VolumeCallbackWrapper(this)));
                MediaRouterJellybean.UserRouteInfo.a(this.e, this.f868a);
            }
        }
    }

    static class LegacyImpl extends RemoteControlClientCompat {
        public LegacyImpl(Context context, Object obj) {
            super(context, obj);
        }
    }

    public static final class PlaybackInfo {

        /* renamed from: a  reason: collision with root package name */
        public int f870a;
        public int b;
        public int c = 0;
        public int d = 3;
        public int e = 1;
    }

    public interface VolumeCallback {
        void a(int i);

        void b(int i);
    }

    protected RemoteControlClientCompat(Context context, Object obj) {
        this.f868a = obj;
    }

    public static RemoteControlClientCompat a(Context context, Object obj) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new JellybeanImpl(context, obj);
        }
        return new LegacyImpl(context, obj);
    }

    public void a(PlaybackInfo playbackInfo) {
    }

    public Object a() {
        return this.f868a;
    }

    public void a(VolumeCallback volumeCallback) {
        this.b = volumeCallback;
    }
}
