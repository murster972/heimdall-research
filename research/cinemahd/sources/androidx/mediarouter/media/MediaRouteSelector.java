package androidx.mediarouter.media;

import android.content.IntentFilter;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class MediaRouteSelector {
    public static final MediaRouteSelector c = new MediaRouteSelector(new Bundle(), (List<String>) null);

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f840a;
    List<String> b;

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private ArrayList<String> f841a;

        public Builder() {
        }

        public Builder a(String str) {
            if (str != null) {
                if (this.f841a == null) {
                    this.f841a = new ArrayList<>();
                }
                if (!this.f841a.contains(str)) {
                    this.f841a.add(str);
                }
                return this;
            }
            throw new IllegalArgumentException("category must not be null");
        }

        public Builder(MediaRouteSelector mediaRouteSelector) {
            if (mediaRouteSelector != null) {
                mediaRouteSelector.b();
                if (!mediaRouteSelector.b.isEmpty()) {
                    this.f841a = new ArrayList<>(mediaRouteSelector.b);
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("selector must not be null");
        }

        public Builder a(Collection<String> collection) {
            if (collection != null) {
                if (!collection.isEmpty()) {
                    for (String a2 : collection) {
                        a(a2);
                    }
                }
                return this;
            }
            throw new IllegalArgumentException("categories must not be null");
        }

        public Builder a(MediaRouteSelector mediaRouteSelector) {
            if (mediaRouteSelector != null) {
                a((Collection<String>) mediaRouteSelector.c());
                return this;
            }
            throw new IllegalArgumentException("selector must not be null");
        }

        public MediaRouteSelector a() {
            if (this.f841a == null) {
                return MediaRouteSelector.c;
            }
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("controlCategories", this.f841a);
            return new MediaRouteSelector(bundle, this.f841a);
        }
    }

    MediaRouteSelector(Bundle bundle, List<String> list) {
        this.f840a = bundle;
        this.b = list;
    }

    public boolean a(List<IntentFilter> list) {
        if (list != null) {
            b();
            int size = this.b.size();
            if (size != 0) {
                int size2 = list.size();
                for (int i = 0; i < size2; i++) {
                    IntentFilter intentFilter = list.get(i);
                    if (intentFilter != null) {
                        for (int i2 = 0; i2 < size; i2++) {
                            if (intentFilter.hasCategory(this.b.get(i2))) {
                                return true;
                            }
                        }
                        continue;
                    }
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.b == null) {
            this.b = this.f840a.getStringArrayList("controlCategories");
            List<String> list = this.b;
            if (list == null || list.isEmpty()) {
                this.b = Collections.emptyList();
            }
        }
    }

    public List<String> c() {
        b();
        return this.b;
    }

    public boolean d() {
        b();
        return this.b.isEmpty();
    }

    public boolean e() {
        b();
        return !this.b.contains((Object) null);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof MediaRouteSelector)) {
            return false;
        }
        MediaRouteSelector mediaRouteSelector = (MediaRouteSelector) obj;
        b();
        mediaRouteSelector.b();
        return this.b.equals(mediaRouteSelector.b);
    }

    public int hashCode() {
        b();
        return this.b.hashCode();
    }

    public String toString() {
        return "MediaRouteSelector{ " + "controlCategories=" + Arrays.toString(c().toArray()) + " }";
    }

    public boolean a(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            return false;
        }
        b();
        mediaRouteSelector.b();
        return this.b.containsAll(mediaRouteSelector.b);
    }

    public Bundle a() {
        return this.f840a;
    }

    public static MediaRouteSelector a(Bundle bundle) {
        if (bundle != null) {
            return new MediaRouteSelector(bundle, (List<String>) null);
        }
        return null;
    }
}
