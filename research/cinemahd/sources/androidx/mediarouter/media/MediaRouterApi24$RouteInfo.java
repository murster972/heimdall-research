package androidx.mediarouter.media;

import android.media.MediaRouter;

public final class MediaRouterApi24$RouteInfo {
    private MediaRouterApi24$RouteInfo() {
    }

    public static int a(Object obj) {
        return ((MediaRouter.RouteInfo) obj).getDeviceType();
    }
}
