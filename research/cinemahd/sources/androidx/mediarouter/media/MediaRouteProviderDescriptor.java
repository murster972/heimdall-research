package androidx.mediarouter.media;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class MediaRouteProviderDescriptor {

    /* renamed from: a  reason: collision with root package name */
    final Bundle f838a;
    List<MediaRouteDescriptor> b;

    MediaRouteProviderDescriptor(Bundle bundle, List<MediaRouteDescriptor> list) {
        this.f838a = bundle;
        this.b = list;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.b == null) {
            ArrayList parcelableArrayList = this.f838a.getParcelableArrayList("routes");
            if (parcelableArrayList == null || parcelableArrayList.isEmpty()) {
                this.b = Collections.emptyList();
                return;
            }
            int size = parcelableArrayList.size();
            this.b = new ArrayList(size);
            for (int i = 0; i < size; i++) {
                this.b.add(MediaRouteDescriptor.a((Bundle) parcelableArrayList.get(i)));
            }
        }
    }

    public List<MediaRouteDescriptor> b() {
        a();
        return this.b;
    }

    public boolean c() {
        a();
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            MediaRouteDescriptor mediaRouteDescriptor = this.b.get(i);
            if (mediaRouteDescriptor == null || !mediaRouteDescriptor.x()) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        return "MediaRouteProviderDescriptor{ " + "routes=" + Arrays.toString(b().toArray()) + ", isValid=" + c() + " }";
    }

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final Bundle f839a = new Bundle();
        private ArrayList<MediaRouteDescriptor> b;

        public Builder a(MediaRouteDescriptor mediaRouteDescriptor) {
            if (mediaRouteDescriptor != null) {
                ArrayList<MediaRouteDescriptor> arrayList = this.b;
                if (arrayList == null) {
                    this.b = new ArrayList<>();
                } else if (arrayList.contains(mediaRouteDescriptor)) {
                    throw new IllegalArgumentException("route descriptor already added");
                }
                this.b.add(mediaRouteDescriptor);
                return this;
            }
            throw new IllegalArgumentException("route must not be null");
        }

        public MediaRouteProviderDescriptor a() {
            ArrayList<MediaRouteDescriptor> arrayList = this.b;
            if (arrayList != null) {
                int size = arrayList.size();
                ArrayList arrayList2 = new ArrayList(size);
                for (int i = 0; i < size; i++) {
                    arrayList2.add(this.b.get(i).a());
                }
                this.f839a.putParcelableArrayList("routes", arrayList2);
            }
            return new MediaRouteProviderDescriptor(this.f839a, this.b);
        }
    }

    public static MediaRouteProviderDescriptor a(Bundle bundle) {
        if (bundle != null) {
            return new MediaRouteProviderDescriptor(bundle, (List<MediaRouteDescriptor>) null);
        }
        return null;
    }
}
