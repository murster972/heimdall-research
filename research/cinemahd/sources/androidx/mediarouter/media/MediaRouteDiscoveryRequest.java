package androidx.mediarouter.media;

import android.os.Bundle;

public final class MediaRouteDiscoveryRequest {

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f834a;
    private MediaRouteSelector b;

    public MediaRouteDiscoveryRequest(MediaRouteSelector mediaRouteSelector, boolean z) {
        if (mediaRouteSelector != null) {
            this.f834a = new Bundle();
            this.b = mediaRouteSelector;
            this.f834a.putBundle("selector", mediaRouteSelector.a());
            this.f834a.putBoolean("activeScan", z);
            return;
        }
        throw new IllegalArgumentException("selector must not be null");
    }

    private void e() {
        if (this.b == null) {
            this.b = MediaRouteSelector.a(this.f834a.getBundle("selector"));
            if (this.b == null) {
                this.b = MediaRouteSelector.c;
            }
        }
    }

    public Bundle a() {
        return this.f834a;
    }

    public MediaRouteSelector b() {
        e();
        return this.b;
    }

    public boolean c() {
        return this.f834a.getBoolean("activeScan");
    }

    public boolean d() {
        e();
        return this.b.e();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof MediaRouteDiscoveryRequest)) {
            return false;
        }
        MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest = (MediaRouteDiscoveryRequest) obj;
        if (!b().equals(mediaRouteDiscoveryRequest.b()) || c() != mediaRouteDiscoveryRequest.c()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return b().hashCode() ^ c() ? 1 : 0;
    }

    public String toString() {
        return "DiscoveryRequest{ selector=" + b() + ", activeScan=" + c() + ", isValid=" + d() + " }";
    }
}
