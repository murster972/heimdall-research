package androidx.mediarouter.media;

import android.content.ComponentName;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import androidx.core.util.ObjectsCompat;

public abstract class MediaRouteProvider {

    /* renamed from: a  reason: collision with root package name */
    private final Context f835a;
    private final ProviderMetadata b;
    private final ProviderHandler c = new ProviderHandler();
    private Callback d;
    private MediaRouteDiscoveryRequest e;
    private boolean f;
    private MediaRouteProviderDescriptor g;
    private boolean h;

    public static abstract class Callback {
        public void a(MediaRouteProvider mediaRouteProvider, MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
        }
    }

    private final class ProviderHandler extends Handler {
        ProviderHandler() {
        }

        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                MediaRouteProvider.this.a();
            } else if (i == 2) {
                MediaRouteProvider.this.b();
            }
        }
    }

    public static final class ProviderMetadata {

        /* renamed from: a  reason: collision with root package name */
        private final ComponentName f837a;

        ProviderMetadata(ComponentName componentName) {
            if (componentName != null) {
                this.f837a = componentName;
                return;
            }
            throw new IllegalArgumentException("componentName must not be null");
        }

        public ComponentName a() {
            return this.f837a;
        }

        public String b() {
            return this.f837a.getPackageName();
        }

        public String toString() {
            return "ProviderMetadata{ componentName=" + this.f837a.flattenToShortString() + " }";
        }
    }

    public static abstract class RouteController {
        public void a() {
        }

        public void a(int i) {
        }

        public void b() {
        }

        public void b(int i) {
            c();
        }

        public void c() {
        }

        public void c(int i) {
        }
    }

    MediaRouteProvider(Context context, ProviderMetadata providerMetadata) {
        if (context != null) {
            this.f835a = context;
            if (providerMetadata == null) {
                this.b = new ProviderMetadata(new ComponentName(context, getClass()));
            } else {
                this.b = providerMetadata;
            }
        } else {
            throw new IllegalArgumentException("context must not be null");
        }
    }

    public void a(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
    }

    public final void a(Callback callback) {
        MediaRouter.e();
        this.d = callback;
    }

    public final void b(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
        MediaRouter.e();
        if (!ObjectsCompat.a(this.e, mediaRouteDiscoveryRequest)) {
            this.e = mediaRouteDiscoveryRequest;
            if (!this.f) {
                this.f = true;
                this.c.sendEmptyMessage(2);
            }
        }
    }

    public final Context c() {
        return this.f835a;
    }

    public final MediaRouteProviderDescriptor d() {
        return this.g;
    }

    public final MediaRouteDiscoveryRequest e() {
        return this.e;
    }

    public final Handler f() {
        return this.c;
    }

    public final ProviderMetadata g() {
        return this.b;
    }

    public final void a(MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
        MediaRouter.e();
        if (this.g != mediaRouteProviderDescriptor) {
            this.g = mediaRouteProviderDescriptor;
            if (!this.h) {
                this.h = true;
                this.c.sendEmptyMessage(1);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f = false;
        a(this.e);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.h = false;
        Callback callback = this.d;
        if (callback != null) {
            callback.a(this, this.g);
        }
    }

    public RouteController a(String str) {
        if (str != null) {
            return null;
        }
        throw new IllegalArgumentException("routeId cannot be null");
    }

    public RouteController a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("routeId cannot be null");
        } else if (str2 != null) {
            return a(str);
        } else {
            throw new IllegalArgumentException("routeGroupId cannot be null");
        }
    }
}
