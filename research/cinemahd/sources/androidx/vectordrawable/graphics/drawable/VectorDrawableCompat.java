package androidx.vectordrawable.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import androidx.collection.ArrayMap;
import androidx.core.content.res.ComplexColorCompat;
import androidx.core.content.res.TypedArrayUtils;
import androidx.core.graphics.PathParser;
import androidx.core.graphics.drawable.DrawableCompat;
import com.facebook.imageutils.JfifUtil;
import com.facebook.react.uimanager.ViewProps;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class VectorDrawableCompat extends VectorDrawableCommon {
    static final PorterDuff.Mode j = PorterDuff.Mode.SRC_IN;
    private VectorDrawableCompatState b;
    private PorterDuffColorFilter c;
    private ColorFilter d;
    private boolean e;
    private boolean f;
    private final float[] g;
    private final Matrix h;
    private final Rect i;

    private static class VClipPath extends VPath {
        VClipPath() {
        }

        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            if (TypedArrayUtils.a(xmlPullParser, "pathData")) {
                TypedArray a2 = TypedArrayUtils.a(resources, theme, attributeSet, AndroidResources.d);
                a(a2, xmlPullParser);
                a2.recycle();
            }
        }

        public boolean b() {
            return true;
        }

        VClipPath(VClipPath vClipPath) {
            super(vClipPath);
        }

        private void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            String string = typedArray.getString(0);
            if (string != null) {
                this.b = string;
            }
            String string2 = typedArray.getString(1);
            if (string2 != null) {
                this.f1122a = PathParser.a(string2);
            }
            this.c = TypedArrayUtils.b(typedArray, xmlPullParser, "fillType", 2, 0);
        }
    }

    private static abstract class VObject {
        private VObject() {
        }

        public boolean a() {
            return false;
        }

        public boolean a(int[] iArr) {
            return false;
        }
    }

    private static class VectorDrawableCompatState extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        int f1124a;
        VPathRenderer b;
        ColorStateList c;
        PorterDuff.Mode d;
        boolean e;
        Bitmap f;
        ColorStateList g;
        PorterDuff.Mode h;
        int i;
        boolean j;
        boolean k;
        Paint l;

        public VectorDrawableCompatState(VectorDrawableCompatState vectorDrawableCompatState) {
            this.c = null;
            this.d = VectorDrawableCompat.j;
            if (vectorDrawableCompatState != null) {
                this.f1124a = vectorDrawableCompatState.f1124a;
                this.b = new VPathRenderer(vectorDrawableCompatState.b);
                Paint paint = vectorDrawableCompatState.b.e;
                if (paint != null) {
                    this.b.e = new Paint(paint);
                }
                Paint paint2 = vectorDrawableCompatState.b.d;
                if (paint2 != null) {
                    this.b.d = new Paint(paint2);
                }
                this.c = vectorDrawableCompatState.c;
                this.d = vectorDrawableCompatState.d;
                this.e = vectorDrawableCompatState.e;
            }
        }

        public void a(Canvas canvas, ColorFilter colorFilter, Rect rect) {
            canvas.drawBitmap(this.f, (Rect) null, rect, a(colorFilter));
        }

        public boolean b() {
            return this.b.getRootAlpha() < 255;
        }

        public void c(int i2, int i3) {
            this.f.eraseColor(0);
            this.b.a(new Canvas(this.f), i2, i3, (ColorFilter) null);
        }

        public void d() {
            this.g = this.c;
            this.h = this.d;
            this.i = this.b.getRootAlpha();
            this.j = this.e;
            this.k = false;
        }

        public int getChangingConfigurations() {
            return this.f1124a;
        }

        public Drawable newDrawable() {
            return new VectorDrawableCompat(this);
        }

        public void b(int i2, int i3) {
            if (this.f == null || !a(i2, i3)) {
                this.f = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
                this.k = true;
            }
        }

        public Drawable newDrawable(Resources resources) {
            return new VectorDrawableCompat(this);
        }

        public Paint a(ColorFilter colorFilter) {
            if (!b() && colorFilter == null) {
                return null;
            }
            if (this.l == null) {
                this.l = new Paint();
                this.l.setFilterBitmap(true);
            }
            this.l.setAlpha(this.b.getRootAlpha());
            this.l.setColorFilter(colorFilter);
            return this.l;
        }

        public boolean c() {
            return this.b.a();
        }

        public boolean a(int i2, int i3) {
            return i2 == this.f.getWidth() && i3 == this.f.getHeight();
        }

        public boolean a() {
            return !this.k && this.g == this.c && this.h == this.d && this.j == this.e && this.i == this.b.getRootAlpha();
        }

        public VectorDrawableCompatState() {
            this.c = null;
            this.d = VectorDrawableCompat.j;
            this.b = new VPathRenderer();
        }

        public boolean a(int[] iArr) {
            boolean a2 = this.b.a(iArr);
            this.k |= a2;
            return a2;
        }
    }

    VectorDrawableCompat() {
        this.f = true;
        this.g = new float[9];
        this.h = new Matrix();
        this.i = new Rect();
        this.b = new VectorDrawableCompatState();
    }

    public static VectorDrawableCompat createFromXmlInner(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
        vectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
        return vectorDrawableCompat;
    }

    /* access modifiers changed from: package-private */
    public Object a(String str) {
        return this.b.b.p.get(str);
    }

    public boolean canApplyTheme() {
        Drawable drawable = this.f1120a;
        if (drawable == null) {
            return false;
        }
        DrawableCompat.a(drawable);
        return false;
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        copyBounds(this.i);
        if (this.i.width() > 0 && this.i.height() > 0) {
            ColorFilter colorFilter = this.d;
            if (colorFilter == null) {
                colorFilter = this.c;
            }
            canvas.getMatrix(this.h);
            this.h.getValues(this.g);
            float abs = Math.abs(this.g[0]);
            float abs2 = Math.abs(this.g[4]);
            float abs3 = Math.abs(this.g[1]);
            float abs4 = Math.abs(this.g[3]);
            if (!(abs3 == 0.0f && abs4 == 0.0f)) {
                abs = 1.0f;
                abs2 = 1.0f;
            }
            int min = Math.min(2048, (int) (((float) this.i.width()) * abs));
            int min2 = Math.min(2048, (int) (((float) this.i.height()) * abs2));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                Rect rect = this.i;
                canvas.translate((float) rect.left, (float) rect.top);
                if (a()) {
                    canvas.translate((float) this.i.width(), 0.0f);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.i.offsetTo(0, 0);
                this.b.b(min, min2);
                if (!this.f) {
                    this.b.c(min, min2);
                } else if (!this.b.a()) {
                    this.b.c(min, min2);
                    this.b.d();
                }
                this.b.a(canvas, colorFilter, this.i);
                canvas.restoreToCount(save);
            }
        }
    }

    public int getAlpha() {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            return DrawableCompat.c(drawable);
        }
        return this.b.b.getRootAlpha();
    }

    public int getChangingConfigurations() {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.b.getChangingConfigurations();
    }

    public ColorFilter getColorFilter() {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            return DrawableCompat.d(drawable);
        }
        return this.d;
    }

    public Drawable.ConstantState getConstantState() {
        Drawable drawable = this.f1120a;
        if (drawable != null && Build.VERSION.SDK_INT >= 24) {
            return new VectorDrawableDelegateState(drawable.getConstantState());
        }
        this.b.f1124a = getChangingConfigurations();
        return this.b;
    }

    public int getIntrinsicHeight() {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return (int) this.b.b.j;
    }

    public int getIntrinsicWidth() {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return (int) this.b.b.i;
    }

    public int getOpacity() {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return -3;
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, (Resources.Theme) null);
        }
    }

    public void invalidateSelf() {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            drawable.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    public boolean isAutoMirrored() {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            return DrawableCompat.f(drawable);
        }
        return this.b.e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        r0 = r1.b.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000f, code lost:
        r0 = r1.b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isStateful() {
        /*
            r1 = this;
            android.graphics.drawable.Drawable r0 = r1.f1120a
            if (r0 == 0) goto L_0x0009
            boolean r0 = r0.isStateful()
            return r0
        L_0x0009:
            boolean r0 = super.isStateful()
            if (r0 != 0) goto L_0x0028
            androidx.vectordrawable.graphics.drawable.VectorDrawableCompat$VectorDrawableCompatState r0 = r1.b
            if (r0 == 0) goto L_0x0026
            boolean r0 = r0.c()
            if (r0 != 0) goto L_0x0028
            androidx.vectordrawable.graphics.drawable.VectorDrawableCompat$VectorDrawableCompatState r0 = r1.b
            android.content.res.ColorStateList r0 = r0.c
            if (r0 == 0) goto L_0x0026
            boolean r0 = r0.isStateful()
            if (r0 == 0) goto L_0x0026
            goto L_0x0028
        L_0x0026:
            r0 = 0
            goto L_0x0029
        L_0x0028:
            r0 = 1
        L_0x0029:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.vectordrawable.graphics.drawable.VectorDrawableCompat.isStateful():boolean");
    }

    public Drawable mutate() {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            drawable.mutate();
            return this;
        }
        if (!this.e && super.mutate() == this) {
            this.b = new VectorDrawableCompatState(this.b);
            this.e = true;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        PorterDuff.Mode mode;
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        boolean z = false;
        VectorDrawableCompatState vectorDrawableCompatState = this.b;
        ColorStateList colorStateList = vectorDrawableCompatState.c;
        if (!(colorStateList == null || (mode = vectorDrawableCompatState.d) == null)) {
            this.c = a(this.c, colorStateList, mode);
            invalidateSelf();
            z = true;
        }
        if (!vectorDrawableCompatState.c() || !vectorDrawableCompatState.a(iArr)) {
            return z;
        }
        invalidateSelf();
        return true;
    }

    public void scheduleSelf(Runnable runnable, long j2) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            drawable.scheduleSelf(runnable, j2);
        } else {
            super.scheduleSelf(runnable, j2);
        }
    }

    public void setAlpha(int i2) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            drawable.setAlpha(i2);
        } else if (this.b.b.getRootAlpha() != i2) {
            this.b.b.setRootAlpha(i2);
            invalidateSelf();
        }
    }

    public void setAutoMirrored(boolean z) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            DrawableCompat.a(drawable, z);
        } else {
            this.b.e = z;
        }
    }

    public void setTint(int i2) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            DrawableCompat.b(drawable, i2);
        } else {
            setTintList(ColorStateList.valueOf(i2));
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            DrawableCompat.a(drawable, colorStateList);
            return;
        }
        VectorDrawableCompatState vectorDrawableCompatState = this.b;
        if (vectorDrawableCompatState.c != colorStateList) {
            vectorDrawableCompatState.c = colorStateList;
            this.c = a(this.c, colorStateList, vectorDrawableCompatState.d);
            invalidateSelf();
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            DrawableCompat.a(drawable, mode);
            return;
        }
        VectorDrawableCompatState vectorDrawableCompatState = this.b;
        if (vectorDrawableCompatState.d != mode) {
            vectorDrawableCompatState.d = mode;
            this.c = a(this.c, vectorDrawableCompatState.c, mode);
            invalidateSelf();
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    public void unscheduleSelf(Runnable runnable) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            drawable.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }

    private static class VectorDrawableDelegateState extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        private final Drawable.ConstantState f1125a;

        public VectorDrawableDelegateState(Drawable.ConstantState constantState) {
            this.f1125a = constantState;
        }

        public boolean canApplyTheme() {
            return this.f1125a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f1125a.getChangingConfigurations();
        }

        public Drawable newDrawable() {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f1120a = (VectorDrawable) this.f1125a.newDrawable();
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f1120a = (VectorDrawable) this.f1125a.newDrawable(resources);
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f1120a = (VectorDrawable) this.f1125a.newDrawable(resources, theme);
            return vectorDrawableCompat;
        }
    }

    /* access modifiers changed from: package-private */
    public PorterDuffColorFilter a(PorterDuffColorFilter porterDuffColorFilter, ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
            return;
        }
        this.d = colorFilter;
        invalidateSelf();
    }

    private static class VFullPath extends VPath {
        private int[] e;
        ComplexColorCompat f;
        float g = 0.0f;
        ComplexColorCompat h;
        float i = 1.0f;
        float j = 1.0f;
        float k = 0.0f;
        float l = 1.0f;
        float m = 0.0f;
        Paint.Cap n = Paint.Cap.BUTT;
        Paint.Join o = Paint.Join.MITER;
        float p = 4.0f;

        VFullPath() {
        }

        private Paint.Cap a(int i2, Paint.Cap cap) {
            if (i2 == 0) {
                return Paint.Cap.BUTT;
            }
            if (i2 != 1) {
                return i2 != 2 ? cap : Paint.Cap.SQUARE;
            }
            return Paint.Cap.ROUND;
        }

        /* access modifiers changed from: package-private */
        public float getFillAlpha() {
            return this.j;
        }

        /* access modifiers changed from: package-private */
        public int getFillColor() {
            return this.h.a();
        }

        /* access modifiers changed from: package-private */
        public float getStrokeAlpha() {
            return this.i;
        }

        /* access modifiers changed from: package-private */
        public int getStrokeColor() {
            return this.f.a();
        }

        /* access modifiers changed from: package-private */
        public float getStrokeWidth() {
            return this.g;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathEnd() {
            return this.l;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathOffset() {
            return this.m;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathStart() {
            return this.k;
        }

        /* access modifiers changed from: package-private */
        public void setFillAlpha(float f2) {
            this.j = f2;
        }

        /* access modifiers changed from: package-private */
        public void setFillColor(int i2) {
            this.h.a(i2);
        }

        /* access modifiers changed from: package-private */
        public void setStrokeAlpha(float f2) {
            this.i = f2;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeColor(int i2) {
            this.f.a(i2);
        }

        /* access modifiers changed from: package-private */
        public void setStrokeWidth(float f2) {
            this.g = f2;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathEnd(float f2) {
            this.l = f2;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathOffset(float f2) {
            this.m = f2;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathStart(float f2) {
            this.k = f2;
        }

        private Paint.Join a(int i2, Paint.Join join) {
            if (i2 == 0) {
                return Paint.Join.MITER;
            }
            if (i2 != 1) {
                return i2 != 2 ? join : Paint.Join.BEVEL;
            }
            return Paint.Join.ROUND;
        }

        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a2 = TypedArrayUtils.a(resources, theme, attributeSet, AndroidResources.c);
            a(a2, xmlPullParser, theme);
            a2.recycle();
        }

        private void a(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme) {
            this.e = null;
            if (TypedArrayUtils.a(xmlPullParser, "pathData")) {
                String string = typedArray.getString(0);
                if (string != null) {
                    this.b = string;
                }
                String string2 = typedArray.getString(2);
                if (string2 != null) {
                    this.f1122a = PathParser.a(string2);
                }
                Resources.Theme theme2 = theme;
                this.h = TypedArrayUtils.a(typedArray, xmlPullParser, theme2, "fillColor", 1, 0);
                this.j = TypedArrayUtils.a(typedArray, xmlPullParser, "fillAlpha", 12, this.j);
                this.n = a(TypedArrayUtils.b(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.n);
                this.o = a(TypedArrayUtils.b(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.o);
                this.p = TypedArrayUtils.a(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.p);
                this.f = TypedArrayUtils.a(typedArray, xmlPullParser, theme2, "strokeColor", 3, 0);
                this.i = TypedArrayUtils.a(typedArray, xmlPullParser, "strokeAlpha", 11, this.i);
                this.g = TypedArrayUtils.a(typedArray, xmlPullParser, "strokeWidth", 4, this.g);
                this.l = TypedArrayUtils.a(typedArray, xmlPullParser, "trimPathEnd", 6, this.l);
                this.m = TypedArrayUtils.a(typedArray, xmlPullParser, "trimPathOffset", 7, this.m);
                this.k = TypedArrayUtils.a(typedArray, xmlPullParser, "trimPathStart", 5, this.k);
                this.c = TypedArrayUtils.b(typedArray, xmlPullParser, "fillType", 13, this.c);
            }
        }

        VFullPath(VFullPath vFullPath) {
            super(vFullPath);
            this.e = vFullPath.e;
            this.f = vFullPath.f;
            this.g = vFullPath.g;
            this.i = vFullPath.i;
            this.h = vFullPath.h;
            this.c = vFullPath.c;
            this.j = vFullPath.j;
            this.k = vFullPath.k;
            this.l = vFullPath.l;
            this.m = vFullPath.m;
            this.n = vFullPath.n;
            this.o = vFullPath.o;
            this.p = vFullPath.p;
        }

        public boolean a() {
            return this.h.d() || this.f.d();
        }

        public boolean a(int[] iArr) {
            return this.f.a(iArr) | this.h.a(iArr);
        }
    }

    private static class VGroup extends VObject {

        /* renamed from: a  reason: collision with root package name */
        final Matrix f1121a = new Matrix();
        final ArrayList<VObject> b = new ArrayList<>();
        float c = 0.0f;
        private float d = 0.0f;
        private float e = 0.0f;
        private float f = 1.0f;
        private float g = 1.0f;
        private float h = 0.0f;
        private float i = 0.0f;
        final Matrix j = new Matrix();
        int k;
        private int[] l;
        private String m = null;

        public VGroup(VGroup vGroup, ArrayMap<String, Object> arrayMap) {
            super();
            VPath vPath;
            this.c = vGroup.c;
            this.d = vGroup.d;
            this.e = vGroup.e;
            this.f = vGroup.f;
            this.g = vGroup.g;
            this.h = vGroup.h;
            this.i = vGroup.i;
            this.l = vGroup.l;
            this.m = vGroup.m;
            this.k = vGroup.k;
            String str = this.m;
            if (str != null) {
                arrayMap.put(str, this);
            }
            this.j.set(vGroup.j);
            ArrayList<VObject> arrayList = vGroup.b;
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                VObject vObject = arrayList.get(i2);
                if (vObject instanceof VGroup) {
                    this.b.add(new VGroup((VGroup) vObject, arrayMap));
                } else {
                    if (vObject instanceof VFullPath) {
                        vPath = new VFullPath((VFullPath) vObject);
                    } else if (vObject instanceof VClipPath) {
                        vPath = new VClipPath((VClipPath) vObject);
                    } else {
                        throw new IllegalStateException("Unknown object in the tree!");
                    }
                    this.b.add(vPath);
                    String str2 = vPath.b;
                    if (str2 != null) {
                        arrayMap.put(str2, vPath);
                    }
                }
            }
        }

        private void b() {
            this.j.reset();
            this.j.postTranslate(-this.d, -this.e);
            this.j.postScale(this.f, this.g);
            this.j.postRotate(this.c, 0.0f, 0.0f);
            this.j.postTranslate(this.h + this.d, this.i + this.e);
        }

        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a2 = TypedArrayUtils.a(resources, theme, attributeSet, AndroidResources.b);
            a(a2, xmlPullParser);
            a2.recycle();
        }

        public String getGroupName() {
            return this.m;
        }

        public Matrix getLocalMatrix() {
            return this.j;
        }

        public float getPivotX() {
            return this.d;
        }

        public float getPivotY() {
            return this.e;
        }

        public float getRotation() {
            return this.c;
        }

        public float getScaleX() {
            return this.f;
        }

        public float getScaleY() {
            return this.g;
        }

        public float getTranslateX() {
            return this.h;
        }

        public float getTranslateY() {
            return this.i;
        }

        public void setPivotX(float f2) {
            if (f2 != this.d) {
                this.d = f2;
                b();
            }
        }

        public void setPivotY(float f2) {
            if (f2 != this.e) {
                this.e = f2;
                b();
            }
        }

        public void setRotation(float f2) {
            if (f2 != this.c) {
                this.c = f2;
                b();
            }
        }

        public void setScaleX(float f2) {
            if (f2 != this.f) {
                this.f = f2;
                b();
            }
        }

        public void setScaleY(float f2) {
            if (f2 != this.g) {
                this.g = f2;
                b();
            }
        }

        public void setTranslateX(float f2) {
            if (f2 != this.h) {
                this.h = f2;
                b();
            }
        }

        public void setTranslateY(float f2) {
            if (f2 != this.i) {
                this.i = f2;
                b();
            }
        }

        private void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.l = null;
            this.c = TypedArrayUtils.a(typedArray, xmlPullParser, ViewProps.ROTATION, 5, this.c);
            this.d = typedArray.getFloat(1, this.d);
            this.e = typedArray.getFloat(2, this.e);
            this.f = TypedArrayUtils.a(typedArray, xmlPullParser, ViewProps.SCALE_X, 3, this.f);
            this.g = TypedArrayUtils.a(typedArray, xmlPullParser, ViewProps.SCALE_Y, 4, this.g);
            this.h = TypedArrayUtils.a(typedArray, xmlPullParser, ViewProps.TRANSLATE_X, 6, this.h);
            this.i = TypedArrayUtils.a(typedArray, xmlPullParser, ViewProps.TRANSLATE_Y, 7, this.i);
            String string = typedArray.getString(0);
            if (string != null) {
                this.m = string;
            }
            b();
        }

        public boolean a() {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                if (this.b.get(i2).a()) {
                    return true;
                }
            }
            return false;
        }

        public boolean a(int[] iArr) {
            boolean z = false;
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                z |= this.b.get(i2).a(iArr);
            }
            return z;
        }

        public VGroup() {
            super();
        }
    }

    private static abstract class VPath extends VObject {

        /* renamed from: a  reason: collision with root package name */
        protected PathParser.PathDataNode[] f1122a = null;
        String b;
        int c = 0;
        int d;

        public VPath() {
            super();
        }

        public void a(Path path) {
            path.reset();
            PathParser.PathDataNode[] pathDataNodeArr = this.f1122a;
            if (pathDataNodeArr != null) {
                PathParser.PathDataNode.a(pathDataNodeArr, path);
            }
        }

        public boolean b() {
            return false;
        }

        public PathParser.PathDataNode[] getPathData() {
            return this.f1122a;
        }

        public String getPathName() {
            return this.b;
        }

        public void setPathData(PathParser.PathDataNode[] pathDataNodeArr) {
            if (!PathParser.a(this.f1122a, pathDataNodeArr)) {
                this.f1122a = PathParser.a(pathDataNodeArr);
            } else {
                PathParser.b(this.f1122a, pathDataNodeArr);
            }
        }

        public VPath(VPath vPath) {
            super();
            this.b = vPath.b;
            this.d = vPath.d;
            this.f1122a = PathParser.a(vPath.f1122a);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036 A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static androidx.vectordrawable.graphics.drawable.VectorDrawableCompat a(android.content.res.Resources r6, int r7, android.content.res.Resources.Theme r8) {
        /*
            java.lang.String r0 = "parser error"
            java.lang.String r1 = "VectorDrawableCompat"
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 24
            if (r2 < r3) goto L_0x0021
            androidx.vectordrawable.graphics.drawable.VectorDrawableCompat r0 = new androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
            r0.<init>()
            android.graphics.drawable.Drawable r6 = androidx.core.content.res.ResourcesCompat.b(r6, r7, r8)
            r0.f1120a = r6
            androidx.vectordrawable.graphics.drawable.VectorDrawableCompat$VectorDrawableDelegateState r6 = new androidx.vectordrawable.graphics.drawable.VectorDrawableCompat$VectorDrawableDelegateState
            android.graphics.drawable.Drawable r7 = r0.f1120a
            android.graphics.drawable.Drawable$ConstantState r7 = r7.getConstantState()
            r6.<init>(r7)
            return r0
        L_0x0021:
            android.content.res.XmlResourceParser r7 = r6.getXml(r7)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            android.util.AttributeSet r2 = android.util.Xml.asAttributeSet(r7)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
        L_0x0029:
            int r3 = r7.next()     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            r4 = 2
            if (r3 == r4) goto L_0x0034
            r5 = 1
            if (r3 == r5) goto L_0x0034
            goto L_0x0029
        L_0x0034:
            if (r3 != r4) goto L_0x003b
            androidx.vectordrawable.graphics.drawable.VectorDrawableCompat r6 = createFromXmlInner(r6, r7, r2, r8)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            return r6
        L_0x003b:
            org.xmlpull.v1.XmlPullParserException r6 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            java.lang.String r7 = "No start tag found"
            r6.<init>(r7)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            throw r6     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
        L_0x0043:
            r6 = move-exception
            android.util.Log.e(r1, r0, r6)
            goto L_0x004c
        L_0x0048:
            r6 = move-exception
            android.util.Log.e(r1, r0, r6)
        L_0x004c:
            r6 = 0
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.vectordrawable.graphics.drawable.VectorDrawableCompat.a(android.content.res.Resources, int, android.content.res.Resources$Theme):androidx.vectordrawable.graphics.drawable.VectorDrawableCompat");
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = this.f1120a;
        if (drawable != null) {
            DrawableCompat.a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        VectorDrawableCompatState vectorDrawableCompatState = this.b;
        vectorDrawableCompatState.b = new VPathRenderer();
        TypedArray a2 = TypedArrayUtils.a(resources, theme, attributeSet, AndroidResources.f1113a);
        a(a2, xmlPullParser, theme);
        a2.recycle();
        vectorDrawableCompatState.f1124a = getChangingConfigurations();
        vectorDrawableCompatState.k = true;
        a(resources, xmlPullParser, attributeSet, theme);
        this.c = a(this.c, vectorDrawableCompatState.c, vectorDrawableCompatState.d);
    }

    VectorDrawableCompat(VectorDrawableCompatState vectorDrawableCompatState) {
        this.f = true;
        this.g = new float[9];
        this.h = new Matrix();
        this.i = new Rect();
        this.b = vectorDrawableCompatState;
        this.c = a(this.c, vectorDrawableCompatState.c, vectorDrawableCompatState.d);
    }

    private static class VPathRenderer {
        private static final Matrix q = new Matrix();

        /* renamed from: a  reason: collision with root package name */
        private final Path f1123a;
        private final Path b;
        private final Matrix c;
        Paint d;
        Paint e;
        private PathMeasure f;
        private int g;
        final VGroup h;
        float i;
        float j;
        float k;
        float l;
        int m;
        String n;
        Boolean o;
        final ArrayMap<String, Object> p;

        public VPathRenderer() {
            this.c = new Matrix();
            this.i = 0.0f;
            this.j = 0.0f;
            this.k = 0.0f;
            this.l = 0.0f;
            this.m = JfifUtil.MARKER_FIRST_BYTE;
            this.n = null;
            this.o = null;
            this.p = new ArrayMap<>();
            this.h = new VGroup();
            this.f1123a = new Path();
            this.b = new Path();
        }

        private static float a(float f2, float f3, float f4, float f5) {
            return (f2 * f5) - (f3 * f4);
        }

        private void a(VGroup vGroup, Matrix matrix, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            vGroup.f1121a.set(matrix);
            vGroup.f1121a.preConcat(vGroup.j);
            canvas.save();
            for (int i4 = 0; i4 < vGroup.b.size(); i4++) {
                VObject vObject = vGroup.b.get(i4);
                if (vObject instanceof VGroup) {
                    a((VGroup) vObject, vGroup.f1121a, canvas, i2, i3, colorFilter);
                } else if (vObject instanceof VPath) {
                    a(vGroup, (VPath) vObject, canvas, i2, i3, colorFilter);
                }
            }
            canvas.restore();
        }

        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        public int getRootAlpha() {
            return this.m;
        }

        public void setAlpha(float f2) {
            setRootAlpha((int) (f2 * 255.0f));
        }

        public void setRootAlpha(int i2) {
            this.m = i2;
        }

        public void a(Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            a(this.h, q, canvas, i2, i3, colorFilter);
        }

        public VPathRenderer(VPathRenderer vPathRenderer) {
            this.c = new Matrix();
            this.i = 0.0f;
            this.j = 0.0f;
            this.k = 0.0f;
            this.l = 0.0f;
            this.m = JfifUtil.MARKER_FIRST_BYTE;
            this.n = null;
            this.o = null;
            this.p = new ArrayMap<>();
            this.h = new VGroup(vPathRenderer.h, this.p);
            this.f1123a = new Path(vPathRenderer.f1123a);
            this.b = new Path(vPathRenderer.b);
            this.i = vPathRenderer.i;
            this.j = vPathRenderer.j;
            this.k = vPathRenderer.k;
            this.l = vPathRenderer.l;
            this.g = vPathRenderer.g;
            this.m = vPathRenderer.m;
            this.n = vPathRenderer.n;
            String str = vPathRenderer.n;
            if (str != null) {
                this.p.put(str, this);
            }
            this.o = vPathRenderer.o;
        }

        private void a(VGroup vGroup, VPath vPath, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            float f2 = ((float) i2) / this.k;
            float f3 = ((float) i3) / this.l;
            float min = Math.min(f2, f3);
            Matrix matrix = vGroup.f1121a;
            this.c.set(matrix);
            this.c.postScale(f2, f3);
            float a2 = a(matrix);
            if (a2 != 0.0f) {
                vPath.a(this.f1123a);
                Path path = this.f1123a;
                this.b.reset();
                if (vPath.b()) {
                    this.b.setFillType(vPath.c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    this.b.addPath(path, this.c);
                    canvas.clipPath(this.b);
                    return;
                }
                VFullPath vFullPath = (VFullPath) vPath;
                if (!(vFullPath.k == 0.0f && vFullPath.l == 1.0f)) {
                    float f4 = vFullPath.k;
                    float f5 = vFullPath.m;
                    float f6 = (f4 + f5) % 1.0f;
                    float f7 = (vFullPath.l + f5) % 1.0f;
                    if (this.f == null) {
                        this.f = new PathMeasure();
                    }
                    this.f.setPath(this.f1123a, false);
                    float length = this.f.getLength();
                    float f8 = f6 * length;
                    float f9 = f7 * length;
                    path.reset();
                    if (f8 > f9) {
                        this.f.getSegment(f8, length, path, true);
                        this.f.getSegment(0.0f, f9, path, true);
                    } else {
                        this.f.getSegment(f8, f9, path, true);
                    }
                    path.rLineTo(0.0f, 0.0f);
                }
                this.b.addPath(path, this.c);
                if (vFullPath.h.e()) {
                    ComplexColorCompat complexColorCompat = vFullPath.h;
                    if (this.e == null) {
                        this.e = new Paint(1);
                        this.e.setStyle(Paint.Style.FILL);
                    }
                    Paint paint = this.e;
                    if (complexColorCompat.c()) {
                        Shader b2 = complexColorCompat.b();
                        b2.setLocalMatrix(this.c);
                        paint.setShader(b2);
                        paint.setAlpha(Math.round(vFullPath.j * 255.0f));
                    } else {
                        paint.setShader((Shader) null);
                        paint.setAlpha(JfifUtil.MARKER_FIRST_BYTE);
                        paint.setColor(VectorDrawableCompat.a(complexColorCompat.a(), vFullPath.j));
                    }
                    paint.setColorFilter(colorFilter);
                    this.b.setFillType(vFullPath.c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    canvas.drawPath(this.b, paint);
                }
                if (vFullPath.f.e()) {
                    ComplexColorCompat complexColorCompat2 = vFullPath.f;
                    if (this.d == null) {
                        this.d = new Paint(1);
                        this.d.setStyle(Paint.Style.STROKE);
                    }
                    Paint paint2 = this.d;
                    Paint.Join join = vFullPath.o;
                    if (join != null) {
                        paint2.setStrokeJoin(join);
                    }
                    Paint.Cap cap = vFullPath.n;
                    if (cap != null) {
                        paint2.setStrokeCap(cap);
                    }
                    paint2.setStrokeMiter(vFullPath.p);
                    if (complexColorCompat2.c()) {
                        Shader b3 = complexColorCompat2.b();
                        b3.setLocalMatrix(this.c);
                        paint2.setShader(b3);
                        paint2.setAlpha(Math.round(vFullPath.i * 255.0f));
                    } else {
                        paint2.setShader((Shader) null);
                        paint2.setAlpha(JfifUtil.MARKER_FIRST_BYTE);
                        paint2.setColor(VectorDrawableCompat.a(complexColorCompat2.a(), vFullPath.i));
                    }
                    paint2.setColorFilter(colorFilter);
                    paint2.setStrokeWidth(vFullPath.g * min * a2);
                    canvas.drawPath(this.b, paint2);
                }
            }
        }

        private float a(Matrix matrix) {
            float[] fArr = {0.0f, 1.0f, 1.0f, 0.0f};
            matrix.mapVectors(fArr);
            float a2 = a(fArr[0], fArr[1], fArr[2], fArr[3]);
            float max = Math.max((float) Math.hypot((double) fArr[0], (double) fArr[1]), (float) Math.hypot((double) fArr[2], (double) fArr[3]));
            if (max > 0.0f) {
                return Math.abs(a2) / max;
            }
            return 0.0f;
        }

        public boolean a() {
            if (this.o == null) {
                this.o = Boolean.valueOf(this.h.a());
            }
            return this.o.booleanValue();
        }

        public boolean a(int[] iArr) {
            return this.h.a(iArr);
        }
    }

    static int a(int i2, float f2) {
        return (i2 & 16777215) | (((int) (((float) Color.alpha(i2)) * f2)) << 24);
    }

    private static PorterDuff.Mode a(int i2, PorterDuff.Mode mode) {
        if (i2 == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i2 == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i2 == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i2) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    private void a(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme) throws XmlPullParserException {
        VectorDrawableCompatState vectorDrawableCompatState = this.b;
        VPathRenderer vPathRenderer = vectorDrawableCompatState.b;
        vectorDrawableCompatState.d = a(TypedArrayUtils.b(typedArray, xmlPullParser, "tintMode", 6, -1), PorterDuff.Mode.SRC_IN);
        ColorStateList a2 = TypedArrayUtils.a(typedArray, xmlPullParser, theme, "tint", 1);
        if (a2 != null) {
            vectorDrawableCompatState.c = a2;
        }
        vectorDrawableCompatState.e = TypedArrayUtils.a(typedArray, xmlPullParser, "autoMirrored", 5, vectorDrawableCompatState.e);
        vPathRenderer.k = TypedArrayUtils.a(typedArray, xmlPullParser, "viewportWidth", 7, vPathRenderer.k);
        vPathRenderer.l = TypedArrayUtils.a(typedArray, xmlPullParser, "viewportHeight", 8, vPathRenderer.l);
        if (vPathRenderer.k <= 0.0f) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (vPathRenderer.l > 0.0f) {
            vPathRenderer.i = typedArray.getDimension(3, vPathRenderer.i);
            vPathRenderer.j = typedArray.getDimension(2, vPathRenderer.j);
            if (vPathRenderer.i <= 0.0f) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (vPathRenderer.j > 0.0f) {
                vPathRenderer.setAlpha(TypedArrayUtils.a(typedArray, xmlPullParser, "alpha", 4, vPathRenderer.getAlpha()));
                String string = typedArray.getString(0);
                if (string != null) {
                    vPathRenderer.n = string;
                    vPathRenderer.p.put(string, vPathRenderer);
                }
            } else {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            }
        } else {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        }
    }

    private void a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        VectorDrawableCompatState vectorDrawableCompatState = this.b;
        VPathRenderer vPathRenderer = vectorDrawableCompatState.b;
        ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.push(vPathRenderer.h);
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        boolean z = true;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                VGroup vGroup = (VGroup) arrayDeque.peek();
                if ("path".equals(name)) {
                    VFullPath vFullPath = new VFullPath();
                    vFullPath.a(resources, attributeSet, theme, xmlPullParser);
                    vGroup.b.add(vFullPath);
                    if (vFullPath.getPathName() != null) {
                        vPathRenderer.p.put(vFullPath.getPathName(), vFullPath);
                    }
                    z = false;
                    vectorDrawableCompatState.f1124a = vFullPath.d | vectorDrawableCompatState.f1124a;
                } else if ("clip-path".equals(name)) {
                    VClipPath vClipPath = new VClipPath();
                    vClipPath.a(resources, attributeSet, theme, xmlPullParser);
                    vGroup.b.add(vClipPath);
                    if (vClipPath.getPathName() != null) {
                        vPathRenderer.p.put(vClipPath.getPathName(), vClipPath);
                    }
                    vectorDrawableCompatState.f1124a = vClipPath.d | vectorDrawableCompatState.f1124a;
                } else if ("group".equals(name)) {
                    VGroup vGroup2 = new VGroup();
                    vGroup2.a(resources, attributeSet, theme, xmlPullParser);
                    vGroup.b.add(vGroup2);
                    arrayDeque.push(vGroup2);
                    if (vGroup2.getGroupName() != null) {
                        vPathRenderer.p.put(vGroup2.getGroupName(), vGroup2);
                    }
                    vectorDrawableCompatState.f1124a = vGroup2.k | vectorDrawableCompatState.f1124a;
                }
            } else if (eventType == 3 && "group".equals(xmlPullParser.getName())) {
                arrayDeque.pop();
            }
            eventType = xmlPullParser.next();
        }
        if (z) {
            throw new XmlPullParserException("no path defined");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f = z;
    }

    private boolean a() {
        if (Build.VERSION.SDK_INT < 17 || !isAutoMirrored() || DrawableCompat.e(this) != 1) {
            return false;
        }
        return true;
    }
}
