package androidx.arch.core.executor;

import java.util.concurrent.Executor;

public class ArchTaskExecutor extends TaskExecutor {
    private static volatile ArchTaskExecutor c;
    private static final Executor d = new Executor() {
        public void execute(Runnable runnable) {
            ArchTaskExecutor.c().a(runnable);
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private TaskExecutor f455a = this.b;
    private TaskExecutor b = new DefaultTaskExecutor();

    static {
        new Executor() {
            public void execute(Runnable runnable) {
                ArchTaskExecutor.c().b(runnable);
            }
        };
    }

    private ArchTaskExecutor() {
    }

    public static ArchTaskExecutor c() {
        if (c != null) {
            return c;
        }
        synchronized (ArchTaskExecutor.class) {
            if (c == null) {
                c = new ArchTaskExecutor();
            }
        }
        return c;
    }

    public void a(Runnable runnable) {
        this.f455a.a(runnable);
    }

    public void b(Runnable runnable) {
        this.f455a.b(runnable);
    }

    public static Executor b() {
        return d;
    }

    public boolean a() {
        return this.f455a.a();
    }
}
