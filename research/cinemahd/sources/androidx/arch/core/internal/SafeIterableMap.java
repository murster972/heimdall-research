package androidx.arch.core.internal;

import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

public class SafeIterableMap<K, V> implements Iterable<Map.Entry<K, V>> {

    /* renamed from: a  reason: collision with root package name */
    Entry<K, V> f458a;
    private Entry<K, V> b;
    private WeakHashMap<SupportRemove<K, V>, Boolean> c = new WeakHashMap<>();
    private int d = 0;

    static class AscendingIterator<K, V> extends ListIterator<K, V> {
        AscendingIterator(Entry<K, V> entry, Entry<K, V> entry2) {
            super(entry, entry2);
        }

        /* access modifiers changed from: package-private */
        public Entry<K, V> b(Entry<K, V> entry) {
            return entry.d;
        }

        /* access modifiers changed from: package-private */
        public Entry<K, V> c(Entry<K, V> entry) {
            return entry.c;
        }
    }

    private static class DescendingIterator<K, V> extends ListIterator<K, V> {
        DescendingIterator(Entry<K, V> entry, Entry<K, V> entry2) {
            super(entry, entry2);
        }

        /* access modifiers changed from: package-private */
        public Entry<K, V> b(Entry<K, V> entry) {
            return entry.c;
        }

        /* access modifiers changed from: package-private */
        public Entry<K, V> c(Entry<K, V> entry) {
            return entry.d;
        }
    }

    static class Entry<K, V> implements Map.Entry<K, V> {

        /* renamed from: a  reason: collision with root package name */
        final K f459a;
        final V b;
        Entry<K, V> c;
        Entry<K, V> d;

        Entry(K k, V v) {
            this.f459a = k;
            this.b = v;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Entry)) {
                return false;
            }
            Entry entry = (Entry) obj;
            if (!this.f459a.equals(entry.f459a) || !this.b.equals(entry.b)) {
                return false;
            }
            return true;
        }

        public K getKey() {
            return this.f459a;
        }

        public V getValue() {
            return this.b;
        }

        public int hashCode() {
            return this.f459a.hashCode() ^ this.b.hashCode();
        }

        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        public String toString() {
            return this.f459a + "=" + this.b;
        }
    }

    private class IteratorWithAdditions implements Iterator<Map.Entry<K, V>>, SupportRemove<K, V> {

        /* renamed from: a  reason: collision with root package name */
        private Entry<K, V> f460a;
        private boolean b = true;

        IteratorWithAdditions() {
        }

        public void a(Entry<K, V> entry) {
            Entry<K, V> entry2 = this.f460a;
            if (entry == entry2) {
                this.f460a = entry2.d;
                this.b = this.f460a == null;
            }
        }

        public boolean hasNext() {
            if (!this.b) {
                Entry<K, V> entry = this.f460a;
                if (entry == null || entry.c == null) {
                    return false;
                }
                return true;
            } else if (SafeIterableMap.this.f458a != null) {
                return true;
            } else {
                return false;
            }
        }

        public Map.Entry<K, V> next() {
            if (this.b) {
                this.b = false;
                this.f460a = SafeIterableMap.this.f458a;
            } else {
                Entry<K, V> entry = this.f460a;
                this.f460a = entry != null ? entry.c : null;
            }
            return this.f460a;
        }
    }

    private static abstract class ListIterator<K, V> implements Iterator<Map.Entry<K, V>>, SupportRemove<K, V> {

        /* renamed from: a  reason: collision with root package name */
        Entry<K, V> f461a;
        Entry<K, V> b;

        ListIterator(Entry<K, V> entry, Entry<K, V> entry2) {
            this.f461a = entry2;
            this.b = entry;
        }

        public void a(Entry<K, V> entry) {
            if (this.f461a == entry && entry == this.b) {
                this.b = null;
                this.f461a = null;
            }
            Entry<K, V> entry2 = this.f461a;
            if (entry2 == entry) {
                this.f461a = b(entry2);
            }
            if (this.b == entry) {
                this.b = a();
            }
        }

        /* access modifiers changed from: package-private */
        public abstract Entry<K, V> b(Entry<K, V> entry);

        /* access modifiers changed from: package-private */
        public abstract Entry<K, V> c(Entry<K, V> entry);

        public boolean hasNext() {
            return this.b != null;
        }

        public Map.Entry<K, V> next() {
            Entry<K, V> entry = this.b;
            this.b = a();
            return entry;
        }

        private Entry<K, V> a() {
            Entry<K, V> entry = this.b;
            Entry<K, V> entry2 = this.f461a;
            if (entry == entry2 || entry2 == null) {
                return null;
            }
            return c(entry);
        }
    }

    interface SupportRemove<K, V> {
        void a(Entry<K, V> entry);
    }

    /* access modifiers changed from: protected */
    public Entry<K, V> a(K k) {
        Entry<K, V> entry = this.f458a;
        while (entry != null && !entry.f459a.equals(k)) {
            entry = entry.c;
        }
        return entry;
    }

    public V b(K k, V v) {
        Entry a2 = a(k);
        if (a2 != null) {
            return a2.b;
        }
        a(k, v);
        return null;
    }

    public Map.Entry<K, V> c() {
        return this.b;
    }

    public Iterator<Map.Entry<K, V>> descendingIterator() {
        DescendingIterator descendingIterator = new DescendingIterator(this.b, this.f458a);
        this.c.put(descendingIterator, false);
        return descendingIterator;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SafeIterableMap)) {
            return false;
        }
        SafeIterableMap safeIterableMap = (SafeIterableMap) obj;
        if (size() != safeIterableMap.size()) {
            return false;
        }
        Iterator it2 = iterator();
        Iterator it3 = safeIterableMap.iterator();
        while (it2.hasNext() && it3.hasNext()) {
            Map.Entry entry = (Map.Entry) it2.next();
            Object next = it3.next();
            if ((entry == null && next != null) || (entry != null && !entry.equals(next))) {
                return false;
            }
        }
        if (it2.hasNext() || it3.hasNext()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        Iterator it2 = iterator();
        int i = 0;
        while (it2.hasNext()) {
            i += ((Map.Entry) it2.next()).hashCode();
        }
        return i;
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        AscendingIterator ascendingIterator = new AscendingIterator(this.f458a, this.b);
        this.c.put(ascendingIterator, false);
        return ascendingIterator;
    }

    public V remove(K k) {
        Entry a2 = a(k);
        if (a2 == null) {
            return null;
        }
        this.d--;
        if (!this.c.isEmpty()) {
            for (SupportRemove<K, V> a3 : this.c.keySet()) {
                a3.a(a2);
            }
        }
        Entry<K, V> entry = a2.d;
        if (entry != null) {
            entry.c = a2.c;
        } else {
            this.f458a = a2.c;
        }
        Entry<K, V> entry2 = a2.c;
        if (entry2 != null) {
            entry2.d = a2.d;
        } else {
            this.b = a2.d;
        }
        a2.c = null;
        a2.d = null;
        return a2.b;
    }

    public int size() {
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            sb.append(((Map.Entry) it2.next()).toString());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public Entry<K, V> a(K k, V v) {
        Entry<K, V> entry = new Entry<>(k, v);
        this.d++;
        Entry<K, V> entry2 = this.b;
        if (entry2 == null) {
            this.f458a = entry;
            this.b = this.f458a;
            return entry;
        }
        entry2.c = entry;
        entry.d = entry2;
        this.b = entry;
        return entry;
    }

    public SafeIterableMap<K, V>.IteratorWithAdditions b() {
        SafeIterableMap<K, V>.IteratorWithAdditions iteratorWithAdditions = new IteratorWithAdditions();
        this.c.put(iteratorWithAdditions, false);
        return iteratorWithAdditions;
    }

    public Map.Entry<K, V> a() {
        return this.f458a;
    }
}
