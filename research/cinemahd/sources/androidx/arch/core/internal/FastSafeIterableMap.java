package androidx.arch.core.internal;

import androidx.arch.core.internal.SafeIterableMap;
import java.util.HashMap;
import java.util.Map;

public class FastSafeIterableMap<K, V> extends SafeIterableMap<K, V> {
    private HashMap<K, SafeIterableMap.Entry<K, V>> e = new HashMap<>();

    /* access modifiers changed from: protected */
    public SafeIterableMap.Entry<K, V> a(K k) {
        return this.e.get(k);
    }

    public V b(K k, V v) {
        SafeIterableMap.Entry a2 = a(k);
        if (a2 != null) {
            return a2.b;
        }
        this.e.put(k, a(k, v));
        return null;
    }

    public boolean contains(K k) {
        return this.e.containsKey(k);
    }

    public V remove(K k) {
        V remove = super.remove(k);
        this.e.remove(k);
        return remove;
    }

    public Map.Entry<K, V> b(K k) {
        if (contains(k)) {
            return this.e.get(k).d;
        }
        return null;
    }
}
