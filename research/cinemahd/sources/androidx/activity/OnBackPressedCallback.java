package androidx.activity;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class OnBackPressedCallback {

    /* renamed from: a  reason: collision with root package name */
    private boolean f230a;
    private CopyOnWriteArrayList<Cancellable> b = new CopyOnWriteArrayList<>();

    public OnBackPressedCallback(boolean z) {
        this.f230a = z;
    }

    public abstract void a();

    public final void a(boolean z) {
        this.f230a = z;
    }

    public final boolean b() {
        return this.f230a;
    }

    public final void c() {
        Iterator<Cancellable> it2 = this.b.iterator();
        while (it2.hasNext()) {
            it2.next().cancel();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Cancellable cancellable) {
        this.b.add(cancellable);
    }

    /* access modifiers changed from: package-private */
    public void b(Cancellable cancellable) {
        this.b.remove(cancellable);
    }
}
