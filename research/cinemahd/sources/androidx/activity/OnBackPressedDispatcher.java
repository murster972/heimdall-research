package androidx.activity;

import android.annotation.SuppressLint;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;
import java.util.ArrayDeque;
import java.util.Iterator;

public final class OnBackPressedDispatcher {

    /* renamed from: a  reason: collision with root package name */
    private final Runnable f231a;
    final ArrayDeque<OnBackPressedCallback> b;

    private class LifecycleOnBackPressedCancellable implements LifecycleEventObserver, Cancellable {

        /* renamed from: a  reason: collision with root package name */
        private final Lifecycle f232a;
        private final OnBackPressedCallback b;
        private Cancellable c;

        LifecycleOnBackPressedCancellable(Lifecycle lifecycle, OnBackPressedCallback onBackPressedCallback) {
            this.f232a = lifecycle;
            this.b = onBackPressedCallback;
            lifecycle.a(this);
        }

        public void a(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
            if (event == Lifecycle.Event.ON_START) {
                this.c = OnBackPressedDispatcher.this.a(this.b);
            } else if (event == Lifecycle.Event.ON_STOP) {
                Cancellable cancellable = this.c;
                if (cancellable != null) {
                    cancellable.cancel();
                }
            } else if (event == Lifecycle.Event.ON_DESTROY) {
                cancel();
            }
        }

        public void cancel() {
            this.f232a.b(this);
            this.b.b(this);
            Cancellable cancellable = this.c;
            if (cancellable != null) {
                cancellable.cancel();
                this.c = null;
            }
        }
    }

    private class OnBackPressedCancellable implements Cancellable {

        /* renamed from: a  reason: collision with root package name */
        private final OnBackPressedCallback f233a;

        OnBackPressedCancellable(OnBackPressedCallback onBackPressedCallback) {
            this.f233a = onBackPressedCallback;
        }

        public void cancel() {
            OnBackPressedDispatcher.this.b.remove(this.f233a);
            this.f233a.b(this);
        }
    }

    public OnBackPressedDispatcher() {
        this((Runnable) null);
    }

    /* access modifiers changed from: package-private */
    public Cancellable a(OnBackPressedCallback onBackPressedCallback) {
        this.b.add(onBackPressedCallback);
        OnBackPressedCancellable onBackPressedCancellable = new OnBackPressedCancellable(onBackPressedCallback);
        onBackPressedCallback.a((Cancellable) onBackPressedCancellable);
        return onBackPressedCancellable;
    }

    public OnBackPressedDispatcher(Runnable runnable) {
        this.b = new ArrayDeque<>();
        this.f231a = runnable;
    }

    @SuppressLint({"LambdaLast"})
    public void a(LifecycleOwner lifecycleOwner, OnBackPressedCallback onBackPressedCallback) {
        Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        if (lifecycle.a() != Lifecycle.State.DESTROYED) {
            onBackPressedCallback.a((Cancellable) new LifecycleOnBackPressedCancellable(lifecycle, onBackPressedCallback));
        }
    }

    public void a() {
        Iterator<OnBackPressedCallback> descendingIterator = this.b.descendingIterator();
        while (descendingIterator.hasNext()) {
            OnBackPressedCallback next = descendingIterator.next();
            if (next.b()) {
                next.a();
                return;
            }
        }
        Runnable runnable = this.f231a;
        if (runnable != null) {
            runnable.run();
        }
    }
}
