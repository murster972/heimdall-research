package androidx.browser.customtabs;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import androidx.core.app.BundleCompat;
import java.util.ArrayList;

public final class CustomTabsIntent {

    /* renamed from: a  reason: collision with root package name */
    public final Intent f463a;

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final Intent f464a;
        private ArrayList<Bundle> b;
        private Bundle c;
        private ArrayList<Bundle> d;
        private boolean e;

        public Builder() {
            this((CustomTabsSession) null);
        }

        public CustomTabsIntent a() {
            ArrayList<Bundle> arrayList = this.b;
            if (arrayList != null) {
                this.f464a.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", arrayList);
            }
            ArrayList<Bundle> arrayList2 = this.d;
            if (arrayList2 != null) {
                this.f464a.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", arrayList2);
            }
            this.f464a.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.e);
            return new CustomTabsIntent(this.f464a, this.c);
        }

        public Builder(CustomTabsSession customTabsSession) {
            this.f464a = new Intent("android.intent.action.VIEW");
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = true;
            if (customTabsSession == null) {
                Bundle bundle = new Bundle();
                if (customTabsSession == null) {
                    BundleCompat.a(bundle, "android.support.customtabs.extra.SESSION", (IBinder) null);
                    this.f464a.putExtras(bundle);
                    return;
                }
                customTabsSession.a();
                throw null;
            }
            customTabsSession.b();
            throw null;
        }
    }

    CustomTabsIntent(Intent intent, Bundle bundle) {
        this.f463a = intent;
    }
}
