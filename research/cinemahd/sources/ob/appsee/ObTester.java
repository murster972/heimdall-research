package ob.appsee;

public class ObTester {
    public static String a() {
        String str;
        int i;
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace != null) {
            int i2 = 0;
            while (true) {
                if (i2 >= stackTrace.length) {
                    break;
                }
                StackTraceElement stackTraceElement = stackTrace[i2];
                if (!stackTraceElement.getClassName().equalsIgnoreCase(Thread.class.getName()) || !stackTraceElement.getMethodName().equalsIgnoreCase("getStackTrace") || (i = i2 + 1) >= stackTrace.length) {
                    i2++;
                } else {
                    StackTraceElement stackTraceElement2 = stackTrace[i];
                    if (stackTraceElement2 != null) {
                        str = stackTraceElement2.getMethodName();
                    }
                }
            }
        }
        str = null;
        if (str == null) {
            return null;
        }
        return ObTester.class.getName() + "." + str;
    }
}
