package io.reactivex;

import com.facebook.common.time.Clock;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.BiPredicate;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.functions.Function4;
import io.reactivex.functions.Function5;
import io.reactivex.functions.Function6;
import io.reactivex.functions.Function7;
import io.reactivex.functions.Function8;
import io.reactivex.functions.Function9;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.ScalarCallable;
import io.reactivex.internal.observers.BlockingFirstObserver;
import io.reactivex.internal.observers.BlockingLastObserver;
import io.reactivex.internal.observers.ForEachWhileObserver;
import io.reactivex.internal.observers.FutureObserver;
import io.reactivex.internal.observers.LambdaObserver;
import io.reactivex.internal.operators.flowable.FlowableFromObservable;
import io.reactivex.internal.operators.flowable.FlowableOnBackpressureError;
import io.reactivex.internal.operators.mixed.ObservableConcatMapCompletable;
import io.reactivex.internal.operators.mixed.ObservableConcatMapMaybe;
import io.reactivex.internal.operators.mixed.ObservableConcatMapSingle;
import io.reactivex.internal.operators.mixed.ObservableSwitchMapCompletable;
import io.reactivex.internal.operators.mixed.ObservableSwitchMapMaybe;
import io.reactivex.internal.operators.mixed.ObservableSwitchMapSingle;
import io.reactivex.internal.operators.observable.BlockingObservableIterable;
import io.reactivex.internal.operators.observable.BlockingObservableLatest;
import io.reactivex.internal.operators.observable.BlockingObservableMostRecent;
import io.reactivex.internal.operators.observable.BlockingObservableNext;
import io.reactivex.internal.operators.observable.ObservableAllSingle;
import io.reactivex.internal.operators.observable.ObservableAmb;
import io.reactivex.internal.operators.observable.ObservableAnySingle;
import io.reactivex.internal.operators.observable.ObservableBlockingSubscribe;
import io.reactivex.internal.operators.observable.ObservableBuffer;
import io.reactivex.internal.operators.observable.ObservableBufferBoundary;
import io.reactivex.internal.operators.observable.ObservableBufferBoundarySupplier;
import io.reactivex.internal.operators.observable.ObservableBufferExactBoundary;
import io.reactivex.internal.operators.observable.ObservableBufferTimed;
import io.reactivex.internal.operators.observable.ObservableCache;
import io.reactivex.internal.operators.observable.ObservableCollectSingle;
import io.reactivex.internal.operators.observable.ObservableCombineLatest;
import io.reactivex.internal.operators.observable.ObservableConcatMap;
import io.reactivex.internal.operators.observable.ObservableConcatMapEager;
import io.reactivex.internal.operators.observable.ObservableConcatWithCompletable;
import io.reactivex.internal.operators.observable.ObservableConcatWithMaybe;
import io.reactivex.internal.operators.observable.ObservableConcatWithSingle;
import io.reactivex.internal.operators.observable.ObservableCountSingle;
import io.reactivex.internal.operators.observable.ObservableCreate;
import io.reactivex.internal.operators.observable.ObservableDebounce;
import io.reactivex.internal.operators.observable.ObservableDebounceTimed;
import io.reactivex.internal.operators.observable.ObservableDefer;
import io.reactivex.internal.operators.observable.ObservableDelay;
import io.reactivex.internal.operators.observable.ObservableDelaySubscriptionOther;
import io.reactivex.internal.operators.observable.ObservableDematerialize;
import io.reactivex.internal.operators.observable.ObservableDetach;
import io.reactivex.internal.operators.observable.ObservableDistinct;
import io.reactivex.internal.operators.observable.ObservableDistinctUntilChanged;
import io.reactivex.internal.operators.observable.ObservableDoAfterNext;
import io.reactivex.internal.operators.observable.ObservableDoFinally;
import io.reactivex.internal.operators.observable.ObservableDoOnEach;
import io.reactivex.internal.operators.observable.ObservableDoOnLifecycle;
import io.reactivex.internal.operators.observable.ObservableElementAtMaybe;
import io.reactivex.internal.operators.observable.ObservableElementAtSingle;
import io.reactivex.internal.operators.observable.ObservableEmpty;
import io.reactivex.internal.operators.observable.ObservableError;
import io.reactivex.internal.operators.observable.ObservableFilter;
import io.reactivex.internal.operators.observable.ObservableFlatMap;
import io.reactivex.internal.operators.observable.ObservableFlatMapCompletableCompletable;
import io.reactivex.internal.operators.observable.ObservableFlatMapMaybe;
import io.reactivex.internal.operators.observable.ObservableFlatMapSingle;
import io.reactivex.internal.operators.observable.ObservableFlattenIterable;
import io.reactivex.internal.operators.observable.ObservableFromArray;
import io.reactivex.internal.operators.observable.ObservableFromCallable;
import io.reactivex.internal.operators.observable.ObservableFromFuture;
import io.reactivex.internal.operators.observable.ObservableFromIterable;
import io.reactivex.internal.operators.observable.ObservableFromPublisher;
import io.reactivex.internal.operators.observable.ObservableFromUnsafeSource;
import io.reactivex.internal.operators.observable.ObservableGenerate;
import io.reactivex.internal.operators.observable.ObservableGroupBy;
import io.reactivex.internal.operators.observable.ObservableGroupJoin;
import io.reactivex.internal.operators.observable.ObservableHide;
import io.reactivex.internal.operators.observable.ObservableIgnoreElements;
import io.reactivex.internal.operators.observable.ObservableIgnoreElementsCompletable;
import io.reactivex.internal.operators.observable.ObservableInternalHelper;
import io.reactivex.internal.operators.observable.ObservableInterval;
import io.reactivex.internal.operators.observable.ObservableIntervalRange;
import io.reactivex.internal.operators.observable.ObservableJoin;
import io.reactivex.internal.operators.observable.ObservableJust;
import io.reactivex.internal.operators.observable.ObservableLastMaybe;
import io.reactivex.internal.operators.observable.ObservableLastSingle;
import io.reactivex.internal.operators.observable.ObservableLift;
import io.reactivex.internal.operators.observable.ObservableMap;
import io.reactivex.internal.operators.observable.ObservableMapNotification;
import io.reactivex.internal.operators.observable.ObservableMaterialize;
import io.reactivex.internal.operators.observable.ObservableMergeWithCompletable;
import io.reactivex.internal.operators.observable.ObservableMergeWithMaybe;
import io.reactivex.internal.operators.observable.ObservableMergeWithSingle;
import io.reactivex.internal.operators.observable.ObservableNever;
import io.reactivex.internal.operators.observable.ObservableObserveOn;
import io.reactivex.internal.operators.observable.ObservableOnErrorNext;
import io.reactivex.internal.operators.observable.ObservableOnErrorReturn;
import io.reactivex.internal.operators.observable.ObservablePublish;
import io.reactivex.internal.operators.observable.ObservablePublishSelector;
import io.reactivex.internal.operators.observable.ObservableRange;
import io.reactivex.internal.operators.observable.ObservableRangeLong;
import io.reactivex.internal.operators.observable.ObservableReduceMaybe;
import io.reactivex.internal.operators.observable.ObservableReduceSeedSingle;
import io.reactivex.internal.operators.observable.ObservableReduceWithSingle;
import io.reactivex.internal.operators.observable.ObservableRepeat;
import io.reactivex.internal.operators.observable.ObservableRepeatUntil;
import io.reactivex.internal.operators.observable.ObservableRepeatWhen;
import io.reactivex.internal.operators.observable.ObservableReplay;
import io.reactivex.internal.operators.observable.ObservableRetryBiPredicate;
import io.reactivex.internal.operators.observable.ObservableRetryPredicate;
import io.reactivex.internal.operators.observable.ObservableRetryWhen;
import io.reactivex.internal.operators.observable.ObservableSampleTimed;
import io.reactivex.internal.operators.observable.ObservableSampleWithObservable;
import io.reactivex.internal.operators.observable.ObservableScalarXMap;
import io.reactivex.internal.operators.observable.ObservableScan;
import io.reactivex.internal.operators.observable.ObservableScanSeed;
import io.reactivex.internal.operators.observable.ObservableSequenceEqualSingle;
import io.reactivex.internal.operators.observable.ObservableSerialized;
import io.reactivex.internal.operators.observable.ObservableSingleMaybe;
import io.reactivex.internal.operators.observable.ObservableSingleSingle;
import io.reactivex.internal.operators.observable.ObservableSkip;
import io.reactivex.internal.operators.observable.ObservableSkipLast;
import io.reactivex.internal.operators.observable.ObservableSkipLastTimed;
import io.reactivex.internal.operators.observable.ObservableSkipUntil;
import io.reactivex.internal.operators.observable.ObservableSkipWhile;
import io.reactivex.internal.operators.observable.ObservableSubscribeOn;
import io.reactivex.internal.operators.observable.ObservableSwitchIfEmpty;
import io.reactivex.internal.operators.observable.ObservableSwitchMap;
import io.reactivex.internal.operators.observable.ObservableTake;
import io.reactivex.internal.operators.observable.ObservableTakeLast;
import io.reactivex.internal.operators.observable.ObservableTakeLastOne;
import io.reactivex.internal.operators.observable.ObservableTakeLastTimed;
import io.reactivex.internal.operators.observable.ObservableTakeUntil;
import io.reactivex.internal.operators.observable.ObservableTakeUntilPredicate;
import io.reactivex.internal.operators.observable.ObservableTakeWhile;
import io.reactivex.internal.operators.observable.ObservableThrottleFirstTimed;
import io.reactivex.internal.operators.observable.ObservableThrottleLatest;
import io.reactivex.internal.operators.observable.ObservableTimeInterval;
import io.reactivex.internal.operators.observable.ObservableTimeout;
import io.reactivex.internal.operators.observable.ObservableTimeoutTimed;
import io.reactivex.internal.operators.observable.ObservableTimer;
import io.reactivex.internal.operators.observable.ObservableToList;
import io.reactivex.internal.operators.observable.ObservableToListSingle;
import io.reactivex.internal.operators.observable.ObservableUnsubscribeOn;
import io.reactivex.internal.operators.observable.ObservableUsing;
import io.reactivex.internal.operators.observable.ObservableWindow;
import io.reactivex.internal.operators.observable.ObservableWindowBoundary;
import io.reactivex.internal.operators.observable.ObservableWindowBoundarySelector;
import io.reactivex.internal.operators.observable.ObservableWindowBoundarySupplier;
import io.reactivex.internal.operators.observable.ObservableWindowTimed;
import io.reactivex.internal.operators.observable.ObservableWithLatestFrom;
import io.reactivex.internal.operators.observable.ObservableWithLatestFromMany;
import io.reactivex.internal.operators.observable.ObservableZip;
import io.reactivex.internal.operators.observable.ObservableZipIterable;
import io.reactivex.internal.util.ArrayListSupplier;
import io.reactivex.internal.util.ErrorMode;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.HashMapSupplier;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.observables.GroupedObservable;
import io.reactivex.observers.SafeObserver;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.Timed;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.reactivestreams.Publisher;

public abstract class Observable<T> implements ObservableSource<T> {

    /* renamed from: io.reactivex.Observable$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6630a = new int[BackpressureStrategy.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                io.reactivex.BackpressureStrategy[] r0 = io.reactivex.BackpressureStrategy.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6630a = r0
                int[] r0 = f6630a     // Catch:{ NoSuchFieldError -> 0x0014 }
                io.reactivex.BackpressureStrategy r1 = io.reactivex.BackpressureStrategy.DROP     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6630a     // Catch:{ NoSuchFieldError -> 0x001f }
                io.reactivex.BackpressureStrategy r1 = io.reactivex.BackpressureStrategy.LATEST     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6630a     // Catch:{ NoSuchFieldError -> 0x002a }
                io.reactivex.BackpressureStrategy r1 = io.reactivex.BackpressureStrategy.MISSING     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6630a     // Catch:{ NoSuchFieldError -> 0x0035 }
                io.reactivex.BackpressureStrategy r1 = io.reactivex.BackpressureStrategy.ERROR     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: io.reactivex.Observable.AnonymousClass1.<clinit>():void");
        }
    }

    public static <T> Observable<T> amb(Iterable<? extends ObservableSource<? extends T>> iterable) {
        ObjectHelper.a(iterable, "sources is null");
        return RxJavaPlugins.a(new ObservableAmb((ObservableSource<? extends T>[]) null, iterable));
    }

    public static <T> Observable<T> ambArray(ObservableSource<? extends T>... observableSourceArr) {
        ObjectHelper.a(observableSourceArr, "sources is null");
        int length = observableSourceArr.length;
        if (length == 0) {
            return empty();
        }
        if (length == 1) {
            return wrap(observableSourceArr[0]);
        }
        return RxJavaPlugins.a(new ObservableAmb(observableSourceArr, (Iterable) null));
    }

    public static int bufferSize() {
        return Flowable.d();
    }

    public static <T, R> Observable<R> combineLatest(Function<? super Object[], ? extends R> function, int i, ObservableSource<? extends T>... observableSourceArr) {
        return combineLatest(observableSourceArr, function, i);
    }

    public static <T, R> Observable<R> combineLatestDelayError(ObservableSource<? extends T>[] observableSourceArr, Function<? super Object[], ? extends R> function) {
        return combineLatestDelayError(observableSourceArr, function, bufferSize());
    }

    public static <T> Observable<T> concat(Iterable<? extends ObservableSource<? extends T>> iterable) {
        ObjectHelper.a(iterable, "sources is null");
        return fromIterable(iterable).concatMapDelayError(Functions.e(), bufferSize(), false);
    }

    public static <T> Observable<T> concatArray(ObservableSource<? extends T>... observableSourceArr) {
        if (observableSourceArr.length == 0) {
            return empty();
        }
        if (observableSourceArr.length == 1) {
            return wrap(observableSourceArr[0]);
        }
        return RxJavaPlugins.a(new ObservableConcatMap(fromArray(observableSourceArr), Functions.e(), bufferSize(), ErrorMode.BOUNDARY));
    }

    public static <T> Observable<T> concatArrayDelayError(ObservableSource<? extends T>... observableSourceArr) {
        if (observableSourceArr.length == 0) {
            return empty();
        }
        if (observableSourceArr.length == 1) {
            return wrap(observableSourceArr[0]);
        }
        return concatDelayError(fromArray(observableSourceArr));
    }

    public static <T> Observable<T> concatArrayEager(ObservableSource<? extends T>... observableSourceArr) {
        return concatArrayEager(bufferSize(), bufferSize(), observableSourceArr);
    }

    public static <T> Observable<T> concatArrayEagerDelayError(ObservableSource<? extends T>... observableSourceArr) {
        return concatArrayEagerDelayError(bufferSize(), bufferSize(), observableSourceArr);
    }

    public static <T> Observable<T> concatDelayError(Iterable<? extends ObservableSource<? extends T>> iterable) {
        ObjectHelper.a(iterable, "sources is null");
        return concatDelayError(fromIterable(iterable));
    }

    public static <T> Observable<T> concatEager(ObservableSource<? extends ObservableSource<? extends T>> observableSource) {
        return concatEager(observableSource, bufferSize(), bufferSize());
    }

    public static <T> Observable<T> create(ObservableOnSubscribe<T> observableOnSubscribe) {
        ObjectHelper.a(observableOnSubscribe, "source is null");
        return RxJavaPlugins.a(new ObservableCreate(observableOnSubscribe));
    }

    public static <T> Observable<T> defer(Callable<? extends ObservableSource<? extends T>> callable) {
        ObjectHelper.a(callable, "supplier is null");
        return RxJavaPlugins.a(new ObservableDefer(callable));
    }

    private Observable<T> doOnEach(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, Action action, Action action2) {
        ObjectHelper.a(consumer, "onNext is null");
        ObjectHelper.a(consumer2, "onError is null");
        ObjectHelper.a(action, "onComplete is null");
        ObjectHelper.a(action2, "onAfterTerminate is null");
        return RxJavaPlugins.a(new ObservableDoOnEach(this, consumer, consumer2, action, action2));
    }

    public static <T> Observable<T> empty() {
        return RxJavaPlugins.a(ObservableEmpty.f6736a);
    }

    public static <T> Observable<T> error(Callable<? extends Throwable> callable) {
        ObjectHelper.a(callable, "errorSupplier is null");
        return RxJavaPlugins.a(new ObservableError(callable));
    }

    public static <T> Observable<T> fromArray(T... tArr) {
        ObjectHelper.a(tArr, "items is null");
        if (tArr.length == 0) {
            return empty();
        }
        if (tArr.length == 1) {
            return just(tArr[0]);
        }
        return RxJavaPlugins.a(new ObservableFromArray(tArr));
    }

    public static <T> Observable<T> fromCallable(Callable<? extends T> callable) {
        ObjectHelper.a(callable, "supplier is null");
        return RxJavaPlugins.a(new ObservableFromCallable(callable));
    }

    public static <T> Observable<T> fromFuture(Future<? extends T> future) {
        ObjectHelper.a(future, "future is null");
        return RxJavaPlugins.a(new ObservableFromFuture(future, 0, (TimeUnit) null));
    }

    public static <T> Observable<T> fromIterable(Iterable<? extends T> iterable) {
        ObjectHelper.a(iterable, "source is null");
        return RxJavaPlugins.a(new ObservableFromIterable(iterable));
    }

    public static <T> Observable<T> fromPublisher(Publisher<? extends T> publisher) {
        ObjectHelper.a(publisher, "publisher is null");
        return RxJavaPlugins.a(new ObservableFromPublisher(publisher));
    }

    public static <T> Observable<T> generate(Consumer<Emitter<T>> consumer) {
        ObjectHelper.a(consumer, "generator  is null");
        return generate(Functions.h(), ObservableInternalHelper.a(consumer), Functions.d());
    }

    public static Observable<Long> interval(long j, long j2, TimeUnit timeUnit) {
        return interval(j, j2, timeUnit, Schedulers.a());
    }

    public static Observable<Long> intervalRange(long j, long j2, long j3, long j4, TimeUnit timeUnit) {
        return intervalRange(j, j2, j3, j4, timeUnit, Schedulers.a());
    }

    public static <T> Observable<T> just(T t) {
        ObjectHelper.a(t, "The item is null");
        return RxJavaPlugins.a(new ObservableJust(t));
    }

    public static <T> Observable<T> merge(Iterable<? extends ObservableSource<? extends T>> iterable, int i, int i2) {
        return fromIterable(iterable).flatMap(Functions.e(), false, i, i2);
    }

    public static <T> Observable<T> mergeArray(int i, int i2, ObservableSource<? extends T>... observableSourceArr) {
        return fromArray(observableSourceArr).flatMap(Functions.e(), false, i, i2);
    }

    public static <T> Observable<T> mergeArrayDelayError(int i, int i2, ObservableSource<? extends T>... observableSourceArr) {
        return fromArray(observableSourceArr).flatMap(Functions.e(), true, i, i2);
    }

    public static <T> Observable<T> mergeDelayError(Iterable<? extends ObservableSource<? extends T>> iterable) {
        return fromIterable(iterable).flatMap(Functions.e(), true);
    }

    public static <T> Observable<T> never() {
        return RxJavaPlugins.a(ObservableNever.f6784a);
    }

    public static Observable<Integer> range(int i, int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("count >= 0 required but it was " + i2);
        } else if (i2 == 0) {
            return empty();
        } else {
            if (i2 == 1) {
                return just(Integer.valueOf(i));
            }
            if (((long) i) + ((long) (i2 - 1)) <= 2147483647L) {
                return RxJavaPlugins.a(new ObservableRange(i, i2));
            }
            throw new IllegalArgumentException("Integer overflow");
        }
    }

    public static Observable<Long> rangeLong(long j, long j2) {
        int i = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i < 0) {
            throw new IllegalArgumentException("count >= 0 required but it was " + j2);
        } else if (i == 0) {
            return empty();
        } else {
            if (j2 == 1) {
                return just(Long.valueOf(j));
            }
            long j3 = (j2 - 1) + j;
            if (j <= 0 || j3 >= 0) {
                return RxJavaPlugins.a(new ObservableRangeLong(j, j2));
            }
            throw new IllegalArgumentException("Overflow! start + count is bigger than Long.MAX_VALUE");
        }
    }

    public static <T> Single<Boolean> sequenceEqual(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2) {
        return sequenceEqual(observableSource, observableSource2, ObjectHelper.a(), bufferSize());
    }

    public static <T> Observable<T> switchOnNext(ObservableSource<? extends ObservableSource<? extends T>> observableSource, int i) {
        ObjectHelper.a(observableSource, "sources is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableSwitchMap(observableSource, Functions.e(), i, false));
    }

    public static <T> Observable<T> switchOnNextDelayError(ObservableSource<? extends ObservableSource<? extends T>> observableSource) {
        return switchOnNextDelayError(observableSource, bufferSize());
    }

    private Observable<T> timeout0(long j, TimeUnit timeUnit, ObservableSource<? extends T> observableSource, Scheduler scheduler) {
        ObjectHelper.a(timeUnit, "timeUnit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableTimeoutTimed(this, j, timeUnit, scheduler, observableSource));
    }

    public static Observable<Long> timer(long j, TimeUnit timeUnit) {
        return timer(j, timeUnit, Schedulers.a());
    }

    public static <T> Observable<T> unsafeCreate(ObservableSource<T> observableSource) {
        ObjectHelper.a(observableSource, "source is null");
        ObjectHelper.a(observableSource, "onSubscribe is null");
        if (!(observableSource instanceof Observable)) {
            return RxJavaPlugins.a(new ObservableFromUnsafeSource(observableSource));
        }
        throw new IllegalArgumentException("unsafeCreate(Observable) should be upgraded");
    }

    public static <T, D> Observable<T> using(Callable<? extends D> callable, Function<? super D, ? extends ObservableSource<? extends T>> function, Consumer<? super D> consumer) {
        return using(callable, function, consumer, true);
    }

    public static <T> Observable<T> wrap(ObservableSource<T> observableSource) {
        ObjectHelper.a(observableSource, "source is null");
        if (observableSource instanceof Observable) {
            return RxJavaPlugins.a((Observable) observableSource);
        }
        return RxJavaPlugins.a(new ObservableFromUnsafeSource(observableSource));
    }

    public static <T, R> Observable<R> zip(Iterable<? extends ObservableSource<? extends T>> iterable, Function<? super Object[], ? extends R> function) {
        ObjectHelper.a(function, "zipper is null");
        ObjectHelper.a(iterable, "sources is null");
        return RxJavaPlugins.a(new ObservableZip((ObservableSource<? extends T>[]) null, iterable, function, bufferSize(), false));
    }

    public static <T, R> Observable<R> zipArray(Function<? super Object[], ? extends R> function, boolean z, int i, ObservableSource<? extends T>... observableSourceArr) {
        if (observableSourceArr.length == 0) {
            return empty();
        }
        ObjectHelper.a(function, "zipper is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableZip(observableSourceArr, (Iterable) null, function, i, z));
    }

    public static <T, R> Observable<R> zipIterable(Iterable<? extends ObservableSource<? extends T>> iterable, Function<? super Object[], ? extends R> function, boolean z, int i) {
        ObjectHelper.a(function, "zipper is null");
        ObjectHelper.a(iterable, "sources is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableZip((ObservableSource<? extends T>[]) null, iterable, function, i, z));
    }

    public final Single<Boolean> all(Predicate<? super T> predicate) {
        ObjectHelper.a(predicate, "predicate is null");
        return RxJavaPlugins.a(new ObservableAllSingle(this, predicate));
    }

    public final Observable<T> ambWith(ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return ambArray(this, observableSource);
    }

    public final Single<Boolean> any(Predicate<? super T> predicate) {
        ObjectHelper.a(predicate, "predicate is null");
        return RxJavaPlugins.a(new ObservableAnySingle(this, predicate));
    }

    public final <R> R as(ObservableConverter<T, ? extends R> observableConverter) {
        ObjectHelper.a(observableConverter, "converter is null");
        return observableConverter.a(this);
    }

    public final T blockingFirst() {
        BlockingFirstObserver blockingFirstObserver = new BlockingFirstObserver();
        subscribe(blockingFirstObserver);
        T a2 = blockingFirstObserver.a();
        if (a2 != null) {
            return a2;
        }
        throw new NoSuchElementException();
    }

    public final void blockingForEach(Consumer<? super T> consumer) {
        Iterator it2 = blockingIterable().iterator();
        while (it2.hasNext()) {
            try {
                consumer.accept(it2.next());
            } catch (Throwable th) {
                Exceptions.b(th);
                ((Disposable) it2).dispose();
                throw ExceptionHelper.a(th);
            }
        }
    }

    public final Iterable<T> blockingIterable() {
        return blockingIterable(bufferSize());
    }

    public final T blockingLast() {
        BlockingLastObserver blockingLastObserver = new BlockingLastObserver();
        subscribe(blockingLastObserver);
        T a2 = blockingLastObserver.a();
        if (a2 != null) {
            return a2;
        }
        throw new NoSuchElementException();
    }

    public final Iterable<T> blockingLatest() {
        return new BlockingObservableLatest(this);
    }

    public final Iterable<T> blockingMostRecent(T t) {
        return new BlockingObservableMostRecent(this, t);
    }

    public final Iterable<T> blockingNext() {
        return new BlockingObservableNext(this);
    }

    public final T blockingSingle() {
        T b = singleElement().b();
        if (b != null) {
            return b;
        }
        throw new NoSuchElementException();
    }

    public final void blockingSubscribe() {
        ObservableBlockingSubscribe.a(this);
    }

    public final Observable<List<T>> buffer(int i) {
        return buffer(i, i);
    }

    public final Observable<T> cache() {
        return cacheWithInitialCapacity(16);
    }

    public final Observable<T> cacheWithInitialCapacity(int i) {
        ObjectHelper.a(i, "initialCapacity");
        return RxJavaPlugins.a(new ObservableCache(this, i));
    }

    public final <U> Observable<U> cast(Class<U> cls) {
        ObjectHelper.a(cls, "clazz is null");
        return map(Functions.a(cls));
    }

    public final <U> Single<U> collect(Callable<? extends U> callable, BiConsumer<? super U, ? super T> biConsumer) {
        ObjectHelper.a(callable, "initialValueSupplier is null");
        ObjectHelper.a(biConsumer, "collector is null");
        return RxJavaPlugins.a(new ObservableCollectSingle(this, callable, biConsumer));
    }

    public final <U> Single<U> collectInto(U u, BiConsumer<? super U, ? super T> biConsumer) {
        ObjectHelper.a(u, "initialValue is null");
        return collect(Functions.b(u), biConsumer);
    }

    public final <R> Observable<R> compose(ObservableTransformer<? super T, ? extends R> observableTransformer) {
        ObjectHelper.a(observableTransformer, "composer is null");
        return wrap(observableTransformer.a(this));
    }

    public final <R> Observable<R> concatMap(Function<? super T, ? extends ObservableSource<? extends R>> function) {
        return concatMap(function, 2);
    }

    public final Completable concatMapCompletable(Function<? super T, ? extends CompletableSource> function) {
        return concatMapCompletable(function, 2);
    }

    public final Completable concatMapCompletableDelayError(Function<? super T, ? extends CompletableSource> function) {
        return concatMapCompletableDelayError(function, true, 2);
    }

    public final <R> Observable<R> concatMapDelayError(Function<? super T, ? extends ObservableSource<? extends R>> function) {
        return concatMapDelayError(function, bufferSize(), true);
    }

    public final <R> Observable<R> concatMapEager(Function<? super T, ? extends ObservableSource<? extends R>> function) {
        return concatMapEager(function, Integer.MAX_VALUE, bufferSize());
    }

    public final <R> Observable<R> concatMapEagerDelayError(Function<? super T, ? extends ObservableSource<? extends R>> function, boolean z) {
        return concatMapEagerDelayError(function, Integer.MAX_VALUE, bufferSize(), z);
    }

    public final <U> Observable<U> concatMapIterable(Function<? super T, ? extends Iterable<? extends U>> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new ObservableFlattenIterable(this, function));
    }

    public final <R> Observable<R> concatMapMaybe(Function<? super T, ? extends MaybeSource<? extends R>> function) {
        return concatMapMaybe(function, 2);
    }

    public final <R> Observable<R> concatMapMaybeDelayError(Function<? super T, ? extends MaybeSource<? extends R>> function) {
        return concatMapMaybeDelayError(function, true, 2);
    }

    public final <R> Observable<R> concatMapSingle(Function<? super T, ? extends SingleSource<? extends R>> function) {
        return concatMapSingle(function, 2);
    }

    public final <R> Observable<R> concatMapSingleDelayError(Function<? super T, ? extends SingleSource<? extends R>> function) {
        return concatMapSingleDelayError(function, true, 2);
    }

    public final Observable<T> concatWith(ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return concat(this, (Observable) observableSource);
    }

    public final Single<Boolean> contains(Object obj) {
        ObjectHelper.a(obj, "element is null");
        return any(Functions.a(obj));
    }

    public final Single<Long> count() {
        return RxJavaPlugins.a(new ObservableCountSingle(this));
    }

    public final <U> Observable<T> debounce(Function<? super T, ? extends ObservableSource<U>> function) {
        ObjectHelper.a(function, "debounceSelector is null");
        return RxJavaPlugins.a(new ObservableDebounce(this, function));
    }

    public final Observable<T> defaultIfEmpty(T t) {
        ObjectHelper.a(t, "defaultItem is null");
        return switchIfEmpty(just(t));
    }

    public final <U> Observable<T> delay(Function<? super T, ? extends ObservableSource<U>> function) {
        ObjectHelper.a(function, "itemDelay is null");
        return flatMap(ObservableInternalHelper.b(function));
    }

    public final <U> Observable<T> delaySubscription(ObservableSource<U> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return RxJavaPlugins.a(new ObservableDelaySubscriptionOther(this, observableSource));
    }

    @Deprecated
    public final <T2> Observable<T2> dematerialize() {
        return RxJavaPlugins.a(new ObservableDematerialize(this, Functions.e()));
    }

    public final Observable<T> distinct() {
        return distinct(Functions.e(), Functions.c());
    }

    public final Observable<T> distinctUntilChanged() {
        return distinctUntilChanged(Functions.e());
    }

    public final Observable<T> doAfterNext(Consumer<? super T> consumer) {
        ObjectHelper.a(consumer, "onAfterNext is null");
        return RxJavaPlugins.a(new ObservableDoAfterNext(this, consumer));
    }

    public final Observable<T> doAfterTerminate(Action action) {
        ObjectHelper.a(action, "onFinally is null");
        return doOnEach(Functions.d(), Functions.d(), Functions.c, action);
    }

    public final Observable<T> doFinally(Action action) {
        ObjectHelper.a(action, "onFinally is null");
        return RxJavaPlugins.a(new ObservableDoFinally(this, action));
    }

    public final Observable<T> doOnComplete(Action action) {
        return doOnEach(Functions.d(), Functions.d(), action, Functions.c);
    }

    public final Observable<T> doOnDispose(Action action) {
        return doOnLifecycle(Functions.d(), action);
    }

    public final Observable<T> doOnError(Consumer<? super Throwable> consumer) {
        Consumer d = Functions.d();
        Action action = Functions.c;
        return doOnEach(d, consumer, action, action);
    }

    public final Observable<T> doOnLifecycle(Consumer<? super Disposable> consumer, Action action) {
        ObjectHelper.a(consumer, "onSubscribe is null");
        ObjectHelper.a(action, "onDispose is null");
        return RxJavaPlugins.a(new ObservableDoOnLifecycle(this, consumer, action));
    }

    public final Observable<T> doOnNext(Consumer<? super T> consumer) {
        Consumer d = Functions.d();
        Action action = Functions.c;
        return doOnEach(consumer, d, action, action);
    }

    public final Observable<T> doOnSubscribe(Consumer<? super Disposable> consumer) {
        return doOnLifecycle(consumer, Functions.c);
    }

    public final Observable<T> doOnTerminate(Action action) {
        ObjectHelper.a(action, "onTerminate is null");
        return doOnEach(Functions.d(), Functions.a(action), action, Functions.c);
    }

    public final Maybe<T> elementAt(long j) {
        if (j >= 0) {
            return RxJavaPlugins.a(new ObservableElementAtMaybe(this, j));
        }
        throw new IndexOutOfBoundsException("index >= 0 required but it was " + j);
    }

    public final Single<T> elementAtOrError(long j) {
        if (j >= 0) {
            return RxJavaPlugins.a(new ObservableElementAtSingle(this, j, null));
        }
        throw new IndexOutOfBoundsException("index >= 0 required but it was " + j);
    }

    public final Observable<T> filter(Predicate<? super T> predicate) {
        ObjectHelper.a(predicate, "predicate is null");
        return RxJavaPlugins.a(new ObservableFilter(this, predicate));
    }

    public final Single<T> first(T t) {
        return elementAt(0, t);
    }

    public final Maybe<T> firstElement() {
        return elementAt(0);
    }

    public final Single<T> firstOrError() {
        return elementAtOrError(0);
    }

    public final <R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends R>> function) {
        return flatMap(function, false);
    }

    public final Completable flatMapCompletable(Function<? super T, ? extends CompletableSource> function) {
        return flatMapCompletable(function, false);
    }

    public final <U> Observable<U> flatMapIterable(Function<? super T, ? extends Iterable<? extends U>> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new ObservableFlattenIterable(this, function));
    }

    public final <R> Observable<R> flatMapMaybe(Function<? super T, ? extends MaybeSource<? extends R>> function) {
        return flatMapMaybe(function, false);
    }

    public final <R> Observable<R> flatMapSingle(Function<? super T, ? extends SingleSource<? extends R>> function) {
        return flatMapSingle(function, false);
    }

    public final Disposable forEach(Consumer<? super T> consumer) {
        return subscribe(consumer);
    }

    public final Disposable forEachWhile(Predicate<? super T> predicate) {
        return forEachWhile(predicate, Functions.e, Functions.c);
    }

    public final <K> Observable<GroupedObservable<K, T>> groupBy(Function<? super T, ? extends K> function) {
        return groupBy(function, Functions.e(), false, bufferSize());
    }

    public final <TRight, TLeftEnd, TRightEnd, R> Observable<R> groupJoin(ObservableSource<? extends TRight> observableSource, Function<? super T, ? extends ObservableSource<TLeftEnd>> function, Function<? super TRight, ? extends ObservableSource<TRightEnd>> function2, BiFunction<? super T, ? super Observable<TRight>, ? extends R> biFunction) {
        ObjectHelper.a(observableSource, "other is null");
        ObjectHelper.a(function, "leftEnd is null");
        ObjectHelper.a(function2, "rightEnd is null");
        ObjectHelper.a(biFunction, "resultSelector is null");
        return RxJavaPlugins.a(new ObservableGroupJoin(this, observableSource, function, function2, biFunction));
    }

    public final Observable<T> hide() {
        return RxJavaPlugins.a(new ObservableHide(this));
    }

    public final Completable ignoreElements() {
        return RxJavaPlugins.a((Completable) new ObservableIgnoreElementsCompletable(this));
    }

    public final Single<Boolean> isEmpty() {
        return all(Functions.a());
    }

    public final <TRight, TLeftEnd, TRightEnd, R> Observable<R> join(ObservableSource<? extends TRight> observableSource, Function<? super T, ? extends ObservableSource<TLeftEnd>> function, Function<? super TRight, ? extends ObservableSource<TRightEnd>> function2, BiFunction<? super T, ? super TRight, ? extends R> biFunction) {
        ObjectHelper.a(observableSource, "other is null");
        ObjectHelper.a(function, "leftEnd is null");
        ObjectHelper.a(function2, "rightEnd is null");
        ObjectHelper.a(biFunction, "resultSelector is null");
        return RxJavaPlugins.a(new ObservableJoin(this, observableSource, function, function2, biFunction));
    }

    public final Single<T> last(T t) {
        ObjectHelper.a(t, "defaultItem is null");
        return RxJavaPlugins.a(new ObservableLastSingle(this, t));
    }

    public final Maybe<T> lastElement() {
        return RxJavaPlugins.a(new ObservableLastMaybe(this));
    }

    public final Single<T> lastOrError() {
        return RxJavaPlugins.a(new ObservableLastSingle(this, null));
    }

    public final <R> Observable<R> lift(ObservableOperator<? extends R, ? super T> observableOperator) {
        ObjectHelper.a(observableOperator, "onLift is null");
        return RxJavaPlugins.a(new ObservableLift(this, observableOperator));
    }

    public final <R> Observable<R> map(Function<? super T, ? extends R> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new ObservableMap(this, function));
    }

    public final Observable<Notification<T>> materialize() {
        return RxJavaPlugins.a(new ObservableMaterialize(this));
    }

    public final Observable<T> mergeWith(ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return merge(this, (Observable) observableSource);
    }

    public final Observable<T> observeOn(Scheduler scheduler) {
        return observeOn(scheduler, false, bufferSize());
    }

    public final <U> Observable<U> ofType(Class<U> cls) {
        ObjectHelper.a(cls, "clazz is null");
        return filter(Functions.b(cls)).cast(cls);
    }

    public final Observable<T> onErrorResumeNext(Function<? super Throwable, ? extends ObservableSource<? extends T>> function) {
        ObjectHelper.a(function, "resumeFunction is null");
        return RxJavaPlugins.a(new ObservableOnErrorNext(this, function, false));
    }

    public final Observable<T> onErrorReturn(Function<? super Throwable, ? extends T> function) {
        ObjectHelper.a(function, "valueSupplier is null");
        return RxJavaPlugins.a(new ObservableOnErrorReturn(this, function));
    }

    public final Observable<T> onErrorReturnItem(T t) {
        ObjectHelper.a(t, "item is null");
        return onErrorReturn(Functions.c(t));
    }

    public final Observable<T> onExceptionResumeNext(ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "next is null");
        return RxJavaPlugins.a(new ObservableOnErrorNext(this, Functions.c(observableSource), true));
    }

    public final Observable<T> onTerminateDetach() {
        return RxJavaPlugins.a(new ObservableDetach(this));
    }

    public final ConnectableObservable<T> publish() {
        return ObservablePublish.a(this);
    }

    public final Maybe<T> reduce(BiFunction<T, T, T> biFunction) {
        ObjectHelper.a(biFunction, "reducer is null");
        return RxJavaPlugins.a(new ObservableReduceMaybe(this, biFunction));
    }

    public final <R> Single<R> reduceWith(Callable<R> callable, BiFunction<R, ? super T, R> biFunction) {
        ObjectHelper.a(callable, "seedSupplier is null");
        ObjectHelper.a(biFunction, "reducer is null");
        return RxJavaPlugins.a(new ObservableReduceWithSingle(this, callable, biFunction));
    }

    public final Observable<T> repeat() {
        return repeat(Clock.MAX_TIME);
    }

    public final Observable<T> repeatUntil(BooleanSupplier booleanSupplier) {
        ObjectHelper.a(booleanSupplier, "stop is null");
        return RxJavaPlugins.a(new ObservableRepeatUntil(this, booleanSupplier));
    }

    public final Observable<T> repeatWhen(Function<? super Observable<Object>, ? extends ObservableSource<?>> function) {
        ObjectHelper.a(function, "handler is null");
        return RxJavaPlugins.a(new ObservableRepeatWhen(this, function));
    }

    public final ConnectableObservable<T> replay() {
        return ObservableReplay.a(this);
    }

    public final Observable<T> retry() {
        return retry(Clock.MAX_TIME, Functions.b());
    }

    public final Observable<T> retryUntil(BooleanSupplier booleanSupplier) {
        ObjectHelper.a(booleanSupplier, "stop is null");
        return retry(Clock.MAX_TIME, Functions.a(booleanSupplier));
    }

    public final Observable<T> retryWhen(Function<? super Observable<Throwable>, ? extends ObservableSource<?>> function) {
        ObjectHelper.a(function, "handler is null");
        return RxJavaPlugins.a(new ObservableRetryWhen(this, function));
    }

    public final void safeSubscribe(Observer<? super T> observer) {
        ObjectHelper.a(observer, "s is null");
        if (observer instanceof SafeObserver) {
            subscribe(observer);
        } else {
            subscribe(new SafeObserver(observer));
        }
    }

    public final Observable<T> sample(long j, TimeUnit timeUnit) {
        return sample(j, timeUnit, Schedulers.a());
    }

    public final Observable<T> scan(BiFunction<T, T, T> biFunction) {
        ObjectHelper.a(biFunction, "accumulator is null");
        return RxJavaPlugins.a(new ObservableScan(this, biFunction));
    }

    public final <R> Observable<R> scanWith(Callable<R> callable, BiFunction<R, ? super T, R> biFunction) {
        ObjectHelper.a(callable, "seedSupplier is null");
        ObjectHelper.a(biFunction, "accumulator is null");
        return RxJavaPlugins.a(new ObservableScanSeed(this, callable, biFunction));
    }

    public final Observable<T> serialize() {
        return RxJavaPlugins.a(new ObservableSerialized(this));
    }

    public final Observable<T> share() {
        return publish().a();
    }

    public final Single<T> single(T t) {
        ObjectHelper.a(t, "defaultItem is null");
        return RxJavaPlugins.a(new ObservableSingleSingle(this, t));
    }

    public final Maybe<T> singleElement() {
        return RxJavaPlugins.a(new ObservableSingleMaybe(this));
    }

    public final Single<T> singleOrError() {
        return RxJavaPlugins.a(new ObservableSingleSingle(this, null));
    }

    public final Observable<T> skip(long j) {
        if (j <= 0) {
            return RxJavaPlugins.a(this);
        }
        return RxJavaPlugins.a(new ObservableSkip(this, j));
    }

    public final Observable<T> skipLast(int i) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("count >= 0 required but it was " + i);
        } else if (i == 0) {
            return RxJavaPlugins.a(this);
        } else {
            return RxJavaPlugins.a(new ObservableSkipLast(this, i));
        }
    }

    public final <U> Observable<T> skipUntil(ObservableSource<U> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return RxJavaPlugins.a(new ObservableSkipUntil(this, observableSource));
    }

    public final Observable<T> skipWhile(Predicate<? super T> predicate) {
        ObjectHelper.a(predicate, "predicate is null");
        return RxJavaPlugins.a(new ObservableSkipWhile(this, predicate));
    }

    public final Observable<T> sorted() {
        return toList().c().map(Functions.a(Functions.f())).flatMapIterable(Functions.e());
    }

    public final Observable<T> startWith(Iterable<? extends T> iterable) {
        return concatArray(fromIterable(iterable), this);
    }

    public final Observable<T> startWithArray(T... tArr) {
        Observable fromArray = fromArray(tArr);
        if (fromArray == empty()) {
            return RxJavaPlugins.a(this);
        }
        return concatArray(fromArray, this);
    }

    public final Disposable subscribe() {
        return subscribe(Functions.d(), Functions.e, Functions.c, Functions.d());
    }

    /* access modifiers changed from: protected */
    public abstract void subscribeActual(Observer<? super T> observer);

    public final Observable<T> subscribeOn(Scheduler scheduler) {
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableSubscribeOn(this, scheduler));
    }

    public final <E extends Observer<? super T>> E subscribeWith(E e) {
        subscribe(e);
        return e;
    }

    public final Observable<T> switchIfEmpty(ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return RxJavaPlugins.a(new ObservableSwitchIfEmpty(this, observableSource));
    }

    public final <R> Observable<R> switchMap(Function<? super T, ? extends ObservableSource<? extends R>> function) {
        return switchMap(function, bufferSize());
    }

    public final Completable switchMapCompletable(Function<? super T, ? extends CompletableSource> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a((Completable) new ObservableSwitchMapCompletable(this, function, false));
    }

    public final Completable switchMapCompletableDelayError(Function<? super T, ? extends CompletableSource> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a((Completable) new ObservableSwitchMapCompletable(this, function, true));
    }

    public final <R> Observable<R> switchMapDelayError(Function<? super T, ? extends ObservableSource<? extends R>> function) {
        return switchMapDelayError(function, bufferSize());
    }

    public final <R> Observable<R> switchMapMaybe(Function<? super T, ? extends MaybeSource<? extends R>> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new ObservableSwitchMapMaybe(this, function, false));
    }

    public final <R> Observable<R> switchMapMaybeDelayError(Function<? super T, ? extends MaybeSource<? extends R>> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new ObservableSwitchMapMaybe(this, function, true));
    }

    public final <R> Observable<R> switchMapSingle(Function<? super T, ? extends SingleSource<? extends R>> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new ObservableSwitchMapSingle(this, function, false));
    }

    public final <R> Observable<R> switchMapSingleDelayError(Function<? super T, ? extends SingleSource<? extends R>> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new ObservableSwitchMapSingle(this, function, true));
    }

    public final Observable<T> take(long j) {
        if (j >= 0) {
            return RxJavaPlugins.a(new ObservableTake(this, j));
        }
        throw new IllegalArgumentException("count >= 0 required but it was " + j);
    }

    public final Observable<T> takeLast(int i) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("count >= 0 required but it was " + i);
        } else if (i == 0) {
            return RxJavaPlugins.a(new ObservableIgnoreElements(this));
        } else {
            if (i == 1) {
                return RxJavaPlugins.a(new ObservableTakeLastOne(this));
            }
            return RxJavaPlugins.a(new ObservableTakeLast(this, i));
        }
    }

    public final <U> Observable<T> takeUntil(ObservableSource<U> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return RxJavaPlugins.a(new ObservableTakeUntil(this, observableSource));
    }

    public final Observable<T> takeWhile(Predicate<? super T> predicate) {
        ObjectHelper.a(predicate, "predicate is null");
        return RxJavaPlugins.a(new ObservableTakeWhile(this, predicate));
    }

    public final TestObserver<T> test() {
        TestObserver<T> testObserver = new TestObserver<>();
        subscribe(testObserver);
        return testObserver;
    }

    public final Observable<T> throttleFirst(long j, TimeUnit timeUnit) {
        return throttleFirst(j, timeUnit, Schedulers.a());
    }

    public final Observable<T> throttleLast(long j, TimeUnit timeUnit) {
        return sample(j, timeUnit);
    }

    public final Observable<T> throttleLatest(long j, TimeUnit timeUnit) {
        return throttleLatest(j, timeUnit, Schedulers.a(), false);
    }

    public final Observable<T> throttleWithTimeout(long j, TimeUnit timeUnit) {
        return debounce(j, timeUnit);
    }

    public final Observable<Timed<T>> timeInterval() {
        return timeInterval(TimeUnit.MILLISECONDS, Schedulers.a());
    }

    public final <V> Observable<T> timeout(Function<? super T, ? extends ObservableSource<V>> function) {
        return timeout0((ObservableSource) null, function, (ObservableSource) null);
    }

    public final Observable<Timed<T>> timestamp() {
        return timestamp(TimeUnit.MILLISECONDS, Schedulers.a());
    }

    public final <R> R to(Function<? super Observable<T>, R> function) {
        try {
            ObjectHelper.a(function, "converter is null");
            return function.apply(this);
        } catch (Throwable th) {
            Exceptions.b(th);
            throw ExceptionHelper.a(th);
        }
    }

    public final Flowable<T> toFlowable(BackpressureStrategy backpressureStrategy) {
        FlowableFromObservable flowableFromObservable = new FlowableFromObservable(this);
        int i = AnonymousClass1.f6630a[backpressureStrategy.ordinal()];
        if (i == 1) {
            return flowableFromObservable.b();
        }
        if (i == 2) {
            return flowableFromObservable.c();
        }
        if (i == 3) {
            return flowableFromObservable;
        }
        if (i != 4) {
            return flowableFromObservable.a();
        }
        return RxJavaPlugins.a(new FlowableOnBackpressureError(flowableFromObservable));
    }

    public final Future<T> toFuture() {
        return (Future) subscribeWith(new FutureObserver());
    }

    public final Single<List<T>> toList() {
        return toList(16);
    }

    public final <K> Single<Map<K, T>> toMap(Function<? super T, ? extends K> function) {
        ObjectHelper.a(function, "keySelector is null");
        return collect(HashMapSupplier.a(), Functions.a(function));
    }

    public final <K> Single<Map<K, Collection<T>>> toMultimap(Function<? super T, ? extends K> function) {
        return toMultimap(function, Functions.e(), HashMapSupplier.a(), ArrayListSupplier.b());
    }

    public final Single<List<T>> toSortedList() {
        return toSortedList(Functions.g());
    }

    public final Observable<T> unsubscribeOn(Scheduler scheduler) {
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableUnsubscribeOn(this, scheduler));
    }

    public final Observable<Observable<T>> window(long j) {
        return window(j, j, bufferSize());
    }

    public final <U, R> Observable<R> withLatestFrom(ObservableSource<? extends U> observableSource, BiFunction<? super T, ? super U, ? extends R> biFunction) {
        ObjectHelper.a(observableSource, "other is null");
        ObjectHelper.a(biFunction, "combiner is null");
        return RxJavaPlugins.a(new ObservableWithLatestFrom(this, biFunction, observableSource));
    }

    public final <U, R> Observable<R> zipWith(Iterable<U> iterable, BiFunction<? super T, ? super U, ? extends R> biFunction) {
        ObjectHelper.a(iterable, "other is null");
        ObjectHelper.a(biFunction, "zipper is null");
        return RxJavaPlugins.a(new ObservableZipIterable(this, iterable, biFunction));
    }

    public static <T, R> Observable<R> combineLatest(Iterable<? extends ObservableSource<? extends T>> iterable, Function<? super Object[], ? extends R> function) {
        return combineLatest(iterable, function, bufferSize());
    }

    public static <T, R> Observable<R> combineLatestDelayError(Function<? super Object[], ? extends R> function, int i, ObservableSource<? extends T>... observableSourceArr) {
        return combineLatestDelayError(observableSourceArr, function, i);
    }

    public static <T> Observable<T> concatArrayEager(int i, int i2, ObservableSource<? extends T>... observableSourceArr) {
        return fromArray(observableSourceArr).concatMapEagerDelayError(Functions.e(), i, i2, false);
    }

    public static <T> Observable<T> concatArrayEagerDelayError(int i, int i2, ObservableSource<? extends T>... observableSourceArr) {
        return fromArray(observableSourceArr).concatMapEagerDelayError(Functions.e(), i, i2, true);
    }

    public static <T> Observable<T> concatEager(ObservableSource<? extends ObservableSource<? extends T>> observableSource, int i, int i2) {
        return wrap(observableSource).concatMapEager(Functions.e(), i, i2);
    }

    public static Observable<Long> interval(long j, long j2, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableInterval(Math.max(0, j), Math.max(0, j2), timeUnit, scheduler));
    }

    public static Observable<Long> intervalRange(long j, long j2, long j3, long j4, TimeUnit timeUnit, Scheduler scheduler) {
        long j5 = j2;
        long j6 = j3;
        TimeUnit timeUnit2 = timeUnit;
        Scheduler scheduler2 = scheduler;
        int i = (j5 > 0 ? 1 : (j5 == 0 ? 0 : -1));
        if (i < 0) {
            throw new IllegalArgumentException("count >= 0 required but it was " + j5);
        } else if (i == 0) {
            return empty().delay(j6, timeUnit2, scheduler2);
        } else {
            long j7 = j + (j5 - 1);
            if (j <= 0 || j7 >= 0) {
                ObjectHelper.a(timeUnit2, "unit is null");
                ObjectHelper.a(scheduler2, "scheduler is null");
                return RxJavaPlugins.a(new ObservableIntervalRange(j, j7, Math.max(0, j6), Math.max(0, j4), timeUnit, scheduler));
            }
            throw new IllegalArgumentException("Overflow! start + count is bigger than Long.MAX_VALUE");
        }
    }

    public static <T> Observable<T> merge(Iterable<? extends ObservableSource<? extends T>> iterable) {
        return fromIterable(iterable).flatMap(Functions.e());
    }

    public static <T> Observable<T> mergeArray(ObservableSource<? extends T>... observableSourceArr) {
        return fromArray(observableSourceArr).flatMap(Functions.e(), observableSourceArr.length);
    }

    public static <T> Observable<T> mergeArrayDelayError(ObservableSource<? extends T>... observableSourceArr) {
        return fromArray(observableSourceArr).flatMap(Functions.e(), true, observableSourceArr.length);
    }

    public static <T> Observable<T> mergeDelayError(Iterable<? extends ObservableSource<? extends T>> iterable, int i, int i2) {
        return fromIterable(iterable).flatMap(Functions.e(), true, i, i2);
    }

    public static <T> Single<Boolean> sequenceEqual(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2, BiPredicate<? super T, ? super T> biPredicate) {
        return sequenceEqual(observableSource, observableSource2, biPredicate, bufferSize());
    }

    public static <T> Observable<T> switchOnNextDelayError(ObservableSource<? extends ObservableSource<? extends T>> observableSource, int i) {
        ObjectHelper.a(observableSource, "sources is null");
        ObjectHelper.a(i, "prefetch");
        return RxJavaPlugins.a(new ObservableSwitchMap(observableSource, Functions.e(), i, true));
    }

    public static Observable<Long> timer(long j, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableTimer(Math.max(j, 0), timeUnit, scheduler));
    }

    public static <T, D> Observable<T> using(Callable<? extends D> callable, Function<? super D, ? extends ObservableSource<? extends T>> function, Consumer<? super D> consumer, boolean z) {
        ObjectHelper.a(callable, "resourceSupplier is null");
        ObjectHelper.a(function, "sourceSupplier is null");
        ObjectHelper.a(consumer, "disposer is null");
        return RxJavaPlugins.a(new ObservableUsing(callable, function, consumer, z));
    }

    public final Iterable<T> blockingIterable(int i) {
        ObjectHelper.a(i, "bufferSize");
        return new BlockingObservableIterable(this, i);
    }

    public final void blockingSubscribe(Consumer<? super T> consumer) {
        ObservableBlockingSubscribe.a(this, consumer, Functions.e, Functions.c);
    }

    public final Observable<List<T>> buffer(int i, int i2) {
        return buffer(i, i2, ArrayListSupplier.a());
    }

    public final <R> Observable<R> concatMap(Function<? super T, ? extends ObservableSource<? extends R>> function, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "prefetch");
        if (!(this instanceof ScalarCallable)) {
            return RxJavaPlugins.a(new ObservableConcatMap(this, function, i, ErrorMode.IMMEDIATE));
        }
        Object call = ((ScalarCallable) this).call();
        if (call == null) {
            return empty();
        }
        return ObservableScalarXMap.a(call, function);
    }

    public final Completable concatMapCompletable(Function<? super T, ? extends CompletableSource> function, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "capacityHint");
        return RxJavaPlugins.a((Completable) new ObservableConcatMapCompletable(this, function, ErrorMode.IMMEDIATE, i));
    }

    public final Completable concatMapCompletableDelayError(Function<? super T, ? extends CompletableSource> function, boolean z) {
        return concatMapCompletableDelayError(function, z, 2);
    }

    public final <R> Observable<R> concatMapDelayError(Function<? super T, ? extends ObservableSource<? extends R>> function, int i, boolean z) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "prefetch");
        if (this instanceof ScalarCallable) {
            Object call = ((ScalarCallable) this).call();
            if (call == null) {
                return empty();
            }
            return ObservableScalarXMap.a(call, function);
        }
        return RxJavaPlugins.a(new ObservableConcatMap(this, function, i, z ? ErrorMode.END : ErrorMode.BOUNDARY));
    }

    public final <R> Observable<R> concatMapEager(Function<? super T, ? extends ObservableSource<? extends R>> function, int i, int i2) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "maxConcurrency");
        ObjectHelper.a(i2, "prefetch");
        return RxJavaPlugins.a(new ObservableConcatMapEager(this, function, ErrorMode.IMMEDIATE, i, i2));
    }

    public final <R> Observable<R> concatMapEagerDelayError(Function<? super T, ? extends ObservableSource<? extends R>> function, int i, int i2, boolean z) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "maxConcurrency");
        ObjectHelper.a(i2, "prefetch");
        return RxJavaPlugins.a(new ObservableConcatMapEager(this, function, z ? ErrorMode.END : ErrorMode.BOUNDARY, i, i2));
    }

    public final <R> Observable<R> concatMapMaybe(Function<? super T, ? extends MaybeSource<? extends R>> function, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "prefetch");
        return RxJavaPlugins.a(new ObservableConcatMapMaybe(this, function, ErrorMode.IMMEDIATE, i));
    }

    public final <R> Observable<R> concatMapMaybeDelayError(Function<? super T, ? extends MaybeSource<? extends R>> function, boolean z) {
        return concatMapMaybeDelayError(function, z, 2);
    }

    public final <R> Observable<R> concatMapSingle(Function<? super T, ? extends SingleSource<? extends R>> function, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "prefetch");
        return RxJavaPlugins.a(new ObservableConcatMapSingle(this, function, ErrorMode.IMMEDIATE, i));
    }

    public final <R> Observable<R> concatMapSingleDelayError(Function<? super T, ? extends SingleSource<? extends R>> function, boolean z) {
        return concatMapSingleDelayError(function, z, 2);
    }

    public final <R> Observable<R> dematerialize(Function<? super T, Notification<R>> function) {
        ObjectHelper.a(function, "selector is null");
        return RxJavaPlugins.a(new ObservableDematerialize(this, function));
    }

    public final <K> Observable<T> distinct(Function<? super T, K> function) {
        return distinct(function, Functions.c());
    }

    public final <K> Observable<T> distinctUntilChanged(Function<? super T, K> function) {
        ObjectHelper.a(function, "keySelector is null");
        return RxJavaPlugins.a(new ObservableDistinctUntilChanged(this, function, ObjectHelper.a()));
    }

    public final <R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends R>> function, boolean z) {
        return flatMap(function, z, Integer.MAX_VALUE);
    }

    public final Completable flatMapCompletable(Function<? super T, ? extends CompletableSource> function, boolean z) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a((Completable) new ObservableFlatMapCompletableCompletable(this, function, z));
    }

    public final <R> Observable<R> flatMapMaybe(Function<? super T, ? extends MaybeSource<? extends R>> function, boolean z) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new ObservableFlatMapMaybe(this, function, z));
    }

    public final <R> Observable<R> flatMapSingle(Function<? super T, ? extends SingleSource<? extends R>> function, boolean z) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new ObservableFlatMapSingle(this, function, z));
    }

    public final Disposable forEachWhile(Predicate<? super T> predicate, Consumer<? super Throwable> consumer) {
        return forEachWhile(predicate, consumer, Functions.c);
    }

    public final <K> Observable<GroupedObservable<K, T>> groupBy(Function<? super T, ? extends K> function, boolean z) {
        return groupBy(function, Functions.e(), z, bufferSize());
    }

    public final Observable<T> observeOn(Scheduler scheduler, boolean z) {
        return observeOn(scheduler, z, bufferSize());
    }

    public final <R> Observable<R> publish(Function<? super Observable<T>, ? extends ObservableSource<R>> function) {
        ObjectHelper.a(function, "selector is null");
        return RxJavaPlugins.a(new ObservablePublishSelector(this, function));
    }

    public final Observable<T> repeat(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i < 0) {
            throw new IllegalArgumentException("times >= 0 required but it was " + j);
        } else if (i == 0) {
            return empty();
        } else {
            return RxJavaPlugins.a(new ObservableRepeat(this, j));
        }
    }

    public final <R> Observable<R> replay(Function<? super Observable<T>, ? extends ObservableSource<R>> function) {
        ObjectHelper.a(function, "selector is null");
        return ObservableReplay.a(ObservableInternalHelper.a(this), function);
    }

    public final Observable<T> retry(BiPredicate<? super Integer, ? super Throwable> biPredicate) {
        ObjectHelper.a(biPredicate, "predicate is null");
        return RxJavaPlugins.a(new ObservableRetryBiPredicate(this, biPredicate));
    }

    public final Observable<T> sample(long j, TimeUnit timeUnit, boolean z) {
        return sample(j, timeUnit, Schedulers.a(), z);
    }

    public final Observable<T> sorted(Comparator<? super T> comparator) {
        ObjectHelper.a(comparator, "sortFunction is null");
        return toList().c().map(Functions.a(comparator)).flatMapIterable(Functions.e());
    }

    public final Observable<T> startWith(ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return concatArray(observableSource, this);
    }

    public final Disposable subscribe(Consumer<? super T> consumer) {
        return subscribe(consumer, Functions.e, Functions.c, Functions.d());
    }

    public final <R> Observable<R> switchMap(Function<? super T, ? extends ObservableSource<? extends R>> function, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "bufferSize");
        if (!(this instanceof ScalarCallable)) {
            return RxJavaPlugins.a(new ObservableSwitchMap(this, function, i, false));
        }
        Object call = ((ScalarCallable) this).call();
        if (call == null) {
            return empty();
        }
        return ObservableScalarXMap.a(call, function);
    }

    public final <R> Observable<R> switchMapDelayError(Function<? super T, ? extends ObservableSource<? extends R>> function, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "bufferSize");
        if (!(this instanceof ScalarCallable)) {
            return RxJavaPlugins.a(new ObservableSwitchMap(this, function, i, true));
        }
        Object call = ((ScalarCallable) this).call();
        if (call == null) {
            return empty();
        }
        return ObservableScalarXMap.a(call, function);
    }

    public final Observable<T> throttleFirst(long j, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableThrottleFirstTimed(this, j, timeUnit, scheduler));
    }

    public final Observable<T> throttleLast(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return sample(j, timeUnit, scheduler);
    }

    public final Observable<T> throttleLatest(long j, TimeUnit timeUnit, boolean z) {
        return throttleLatest(j, timeUnit, Schedulers.a(), z);
    }

    public final Observable<T> throttleWithTimeout(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return debounce(j, timeUnit, scheduler);
    }

    public final Observable<Timed<T>> timeInterval(Scheduler scheduler) {
        return timeInterval(TimeUnit.MILLISECONDS, scheduler);
    }

    public final <V> Observable<T> timeout(Function<? super T, ? extends ObservableSource<V>> function, ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return timeout0((ObservableSource) null, function, observableSource);
    }

    public final Observable<Timed<T>> timestamp(Scheduler scheduler) {
        return timestamp(TimeUnit.MILLISECONDS, scheduler);
    }

    public final Single<List<T>> toList(int i) {
        ObjectHelper.a(i, "capacityHint");
        return RxJavaPlugins.a(new ObservableToListSingle(this, i));
    }

    public final Single<List<T>> toSortedList(Comparator<? super T> comparator) {
        ObjectHelper.a(comparator, "comparator is null");
        return toList().a(Functions.a(comparator));
    }

    public final Observable<Observable<T>> window(long j, long j2) {
        return window(j, j2, bufferSize());
    }

    public static <T, R> Observable<R> combineLatest(Iterable<? extends ObservableSource<? extends T>> iterable, Function<? super Object[], ? extends R> function, int i) {
        ObjectHelper.a(iterable, "sources is null");
        ObjectHelper.a(function, "combiner is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableCombineLatest((ObservableSource<? extends T>[]) null, iterable, function, i << 1, false));
    }

    public static <T, R> Observable<R> combineLatestDelayError(ObservableSource<? extends T>[] observableSourceArr, Function<? super Object[], ? extends R> function, int i) {
        ObjectHelper.a(i, "bufferSize");
        ObjectHelper.a(function, "combiner is null");
        if (observableSourceArr.length == 0) {
            return empty();
        }
        return RxJavaPlugins.a(new ObservableCombineLatest(observableSourceArr, (Iterable) null, function, i << 1, true));
    }

    public static <T> Observable<T> concat(ObservableSource<? extends ObservableSource<? extends T>> observableSource) {
        return concat(observableSource, bufferSize());
    }

    public static <T> Observable<T> concatDelayError(ObservableSource<? extends ObservableSource<? extends T>> observableSource) {
        return concatDelayError(observableSource, bufferSize(), true);
    }

    public static <T> Observable<T> concatEager(Iterable<? extends ObservableSource<? extends T>> iterable) {
        return concatEager(iterable, bufferSize(), bufferSize());
    }

    public static <T> Observable<T> error(Throwable th) {
        ObjectHelper.a(th, "e is null");
        return error((Callable<? extends Throwable>) Functions.b(th));
    }

    public static <T> Observable<T> fromFuture(Future<? extends T> future, long j, TimeUnit timeUnit) {
        ObjectHelper.a(future, "future is null");
        ObjectHelper.a(timeUnit, "unit is null");
        return RxJavaPlugins.a(new ObservableFromFuture(future, j, timeUnit));
    }

    public static <T> Observable<T> just(T t, T t2) {
        ObjectHelper.a(t, "The first item is null");
        ObjectHelper.a(t2, "The second item is null");
        return fromArray(t, t2);
    }

    public static <T> Observable<T> merge(Iterable<? extends ObservableSource<? extends T>> iterable, int i) {
        return fromIterable(iterable).flatMap(Functions.e(), i);
    }

    public static <T> Observable<T> mergeDelayError(Iterable<? extends ObservableSource<? extends T>> iterable, int i) {
        return fromIterable(iterable).flatMap(Functions.e(), true, i);
    }

    public static <T> Single<Boolean> sequenceEqual(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2, BiPredicate<? super T, ? super T> biPredicate, int i) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(biPredicate, "isEqual is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableSequenceEqualSingle(observableSource, observableSource2, biPredicate, i));
    }

    public final T blockingSingle(T t) {
        return single(t).b();
    }

    public final void blockingSubscribe(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2) {
        ObservableBlockingSubscribe.a(this, consumer, consumer2, Functions.c);
    }

    public final <U extends Collection<? super T>> Observable<U> buffer(int i, int i2, Callable<U> callable) {
        ObjectHelper.a(i, "count");
        ObjectHelper.a(i2, "skip");
        ObjectHelper.a(callable, "bufferSupplier is null");
        return RxJavaPlugins.a(new ObservableBuffer(this, i, i2, callable));
    }

    public final Completable concatMapCompletableDelayError(Function<? super T, ? extends CompletableSource> function, boolean z, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "prefetch");
        return RxJavaPlugins.a((Completable) new ObservableConcatMapCompletable(this, function, z ? ErrorMode.END : ErrorMode.BOUNDARY, i));
    }

    public final <U> Observable<U> concatMapIterable(Function<? super T, ? extends Iterable<? extends U>> function, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "prefetch");
        return concatMap(ObservableInternalHelper.a(function), i);
    }

    public final <R> Observable<R> concatMapMaybeDelayError(Function<? super T, ? extends MaybeSource<? extends R>> function, boolean z, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "prefetch");
        return RxJavaPlugins.a(new ObservableConcatMapMaybe(this, function, z ? ErrorMode.END : ErrorMode.BOUNDARY, i));
    }

    public final <R> Observable<R> concatMapSingleDelayError(Function<? super T, ? extends SingleSource<? extends R>> function, boolean z, int i) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "prefetch");
        return RxJavaPlugins.a(new ObservableConcatMapSingle(this, function, z ? ErrorMode.END : ErrorMode.BOUNDARY, i));
    }

    public final Observable<T> concatWith(SingleSource<? extends T> singleSource) {
        ObjectHelper.a(singleSource, "other is null");
        return RxJavaPlugins.a(new ObservableConcatWithSingle(this, singleSource));
    }

    public final Observable<T> debounce(long j, TimeUnit timeUnit) {
        return debounce(j, timeUnit, Schedulers.a());
    }

    public final Observable<T> delay(long j, TimeUnit timeUnit) {
        return delay(j, timeUnit, Schedulers.a(), false);
    }

    public final Observable<T> delaySubscription(long j, TimeUnit timeUnit) {
        return delaySubscription(j, timeUnit, Schedulers.a());
    }

    public final <K> Observable<T> distinct(Function<? super T, K> function, Callable<? extends Collection<? super K>> callable) {
        ObjectHelper.a(function, "keySelector is null");
        ObjectHelper.a(callable, "collectionSupplier is null");
        return RxJavaPlugins.a(new ObservableDistinct(this, function, callable));
    }

    public final Single<T> elementAt(long j, T t) {
        if (j >= 0) {
            ObjectHelper.a(t, "defaultItem is null");
            return RxJavaPlugins.a(new ObservableElementAtSingle(this, j, t));
        }
        throw new IndexOutOfBoundsException("index >= 0 required but it was " + j);
    }

    public final <R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends R>> function, boolean z, int i) {
        return flatMap(function, z, i, bufferSize());
    }

    public final <U, V> Observable<V> flatMapIterable(Function<? super T, ? extends Iterable<? extends U>> function, BiFunction<? super T, ? super U, ? extends V> biFunction) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(biFunction, "resultSelector is null");
        return flatMap(ObservableInternalHelper.a(function), biFunction, false, bufferSize(), bufferSize());
    }

    public final Disposable forEachWhile(Predicate<? super T> predicate, Consumer<? super Throwable> consumer, Action action) {
        ObjectHelper.a(predicate, "onNext is null");
        ObjectHelper.a(consumer, "onError is null");
        ObjectHelper.a(action, "onComplete is null");
        ForEachWhileObserver forEachWhileObserver = new ForEachWhileObserver(predicate, consumer, action);
        subscribe(forEachWhileObserver);
        return forEachWhileObserver;
    }

    public final <K, V> Observable<GroupedObservable<K, V>> groupBy(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2) {
        return groupBy(function, function2, false, bufferSize());
    }

    public final Observable<T> mergeWith(SingleSource<? extends T> singleSource) {
        ObjectHelper.a(singleSource, "other is null");
        return RxJavaPlugins.a(new ObservableMergeWithSingle(this, singleSource));
    }

    public final Observable<T> observeOn(Scheduler scheduler, boolean z, int i) {
        ObjectHelper.a(scheduler, "scheduler is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableObserveOn(this, scheduler, z, i));
    }

    public final Observable<T> onErrorResumeNext(ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "next is null");
        return onErrorResumeNext(Functions.c(observableSource));
    }

    public final <R> Single<R> reduce(R r, BiFunction<R, ? super T, R> biFunction) {
        ObjectHelper.a(r, "seed is null");
        ObjectHelper.a(biFunction, "reducer is null");
        return RxJavaPlugins.a(new ObservableReduceSeedSingle(this, r, biFunction));
    }

    public final Observable<T> sample(long j, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableSampleTimed(this, j, timeUnit, scheduler, false));
    }

    public final <R> Observable<R> scan(R r, BiFunction<R, ? super T, R> biFunction) {
        ObjectHelper.a(r, "seed is null");
        return scanWith(Functions.b(r), biFunction);
    }

    public final Observable<T> skip(long j, TimeUnit timeUnit) {
        return skipUntil(timer(j, timeUnit));
    }

    public final Disposable subscribe(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2) {
        return subscribe(consumer, consumer2, Functions.c, Functions.d());
    }

    public final Observable<T> take(long j, TimeUnit timeUnit) {
        return takeUntil(timer(j, timeUnit));
    }

    public final Observable<T> takeUntil(Predicate<? super T> predicate) {
        ObjectHelper.a(predicate, "predicate is null");
        return RxJavaPlugins.a(new ObservableTakeUntilPredicate(this, predicate));
    }

    public final TestObserver<T> test(boolean z) {
        TestObserver<T> testObserver = new TestObserver<>();
        if (z) {
            testObserver.dispose();
        }
        subscribe(testObserver);
        return testObserver;
    }

    public final Observable<T> throttleLatest(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return throttleLatest(j, timeUnit, scheduler, false);
    }

    public final Observable<Timed<T>> timeInterval(TimeUnit timeUnit) {
        return timeInterval(timeUnit, Schedulers.a());
    }

    public final Observable<Timed<T>> timestamp(TimeUnit timeUnit) {
        return timestamp(timeUnit, Schedulers.a());
    }

    public final <K, V> Single<Map<K, V>> toMap(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2) {
        ObjectHelper.a(function, "keySelector is null");
        ObjectHelper.a(function2, "valueSelector is null");
        return collect(HashMapSupplier.a(), Functions.a(function, function2));
    }

    public final Observable<Observable<T>> window(long j, long j2, int i) {
        ObjectHelper.a(j, "count");
        ObjectHelper.a(j2, "skip");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableWindow(this, j, j2, i));
    }

    public static <T> Observable<T> concat(ObservableSource<? extends ObservableSource<? extends T>> observableSource, int i) {
        ObjectHelper.a(observableSource, "sources is null");
        ObjectHelper.a(i, "prefetch");
        return RxJavaPlugins.a(new ObservableConcatMap(observableSource, Functions.e(), i, ErrorMode.IMMEDIATE));
    }

    public static <T> Observable<T> concatDelayError(ObservableSource<? extends ObservableSource<? extends T>> observableSource, int i, boolean z) {
        ObjectHelper.a(observableSource, "sources is null");
        ObjectHelper.a(i, "prefetch is null");
        return RxJavaPlugins.a(new ObservableConcatMap(observableSource, Functions.e(), i, z ? ErrorMode.END : ErrorMode.BOUNDARY));
    }

    public static <T> Observable<T> concatEager(Iterable<? extends ObservableSource<? extends T>> iterable, int i, int i2) {
        return fromIterable(iterable).concatMapEagerDelayError(Functions.e(), i, i2, false);
    }

    public static <T> Observable<T> merge(ObservableSource<? extends ObservableSource<? extends T>> observableSource) {
        ObjectHelper.a(observableSource, "sources is null");
        return RxJavaPlugins.a(new ObservableFlatMap(observableSource, Functions.e(), false, Integer.MAX_VALUE, bufferSize()));
    }

    public static <T> Observable<T> mergeDelayError(ObservableSource<? extends ObservableSource<? extends T>> observableSource) {
        ObjectHelper.a(observableSource, "sources is null");
        return RxJavaPlugins.a(new ObservableFlatMap(observableSource, Functions.e(), true, Integer.MAX_VALUE, bufferSize()));
    }

    public static <T> Observable<T> switchOnNext(ObservableSource<? extends ObservableSource<? extends T>> observableSource) {
        return switchOnNext(observableSource, bufferSize());
    }

    private <U, V> Observable<T> timeout0(ObservableSource<U> observableSource, Function<? super T, ? extends ObservableSource<V>> function, ObservableSource<? extends T> observableSource2) {
        ObjectHelper.a(function, "itemTimeoutIndicator is null");
        return RxJavaPlugins.a(new ObservableTimeout(this, observableSource, function, observableSource2));
    }

    public static <T, R> Observable<R> zip(ObservableSource<? extends ObservableSource<? extends T>> observableSource, Function<? super Object[], ? extends R> function) {
        ObjectHelper.a(function, "zipper is null");
        ObjectHelper.a(observableSource, "sources is null");
        return RxJavaPlugins.a(new ObservableToList(observableSource, 16).flatMap(ObservableInternalHelper.c(function)));
    }

    public final void blockingSubscribe(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, Action action) {
        ObservableBlockingSubscribe.a(this, consumer, consumer2, action);
    }

    public final Observable<T> debounce(long j, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableDebounceTimed(this, j, timeUnit, scheduler));
    }

    public final Observable<T> delay(long j, TimeUnit timeUnit, boolean z) {
        return delay(j, timeUnit, Schedulers.a(), z);
    }

    public final Observable<T> delaySubscription(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return delaySubscription(timer(j, timeUnit, scheduler));
    }

    public final Observable<T> distinctUntilChanged(BiPredicate<? super T, ? super T> biPredicate) {
        ObjectHelper.a(biPredicate, "comparer is null");
        return RxJavaPlugins.a(new ObservableDistinctUntilChanged(this, Functions.e(), biPredicate));
    }

    public final <R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends R>> function, boolean z, int i, int i2) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(i, "maxConcurrency");
        ObjectHelper.a(i2, "bufferSize");
        if (!(this instanceof ScalarCallable)) {
            return RxJavaPlugins.a(new ObservableFlatMap(this, function, z, i, i2));
        }
        Object call = ((ScalarCallable) this).call();
        if (call == null) {
            return empty();
        }
        return ObservableScalarXMap.a(call, function);
    }

    public final <K, V> Observable<GroupedObservable<K, V>> groupBy(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2, boolean z) {
        return groupBy(function, function2, z, bufferSize());
    }

    public final <R> Observable<R> replay(Function<? super Observable<T>, ? extends ObservableSource<R>> function, int i) {
        ObjectHelper.a(function, "selector is null");
        ObjectHelper.a(i, "bufferSize");
        return ObservableReplay.a(ObservableInternalHelper.a(this, i), function);
    }

    public final Observable<T> retry(long j) {
        return retry(j, Functions.b());
    }

    public final Observable<T> skip(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return skipUntil(timer(j, timeUnit, scheduler));
    }

    public final Observable<T> skipLast(long j, TimeUnit timeUnit) {
        return skipLast(j, timeUnit, Schedulers.c(), false, bufferSize());
    }

    public final Observable<T> startWith(T t) {
        ObjectHelper.a(t, "item is null");
        return concatArray(just(t), this);
    }

    public final Disposable subscribe(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, Action action) {
        return subscribe(consumer, consumer2, action, Functions.d());
    }

    public final Observable<T> take(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return takeUntil(timer(j, timeUnit, scheduler));
    }

    public final Observable<T> throttleLatest(long j, TimeUnit timeUnit, Scheduler scheduler, boolean z) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableThrottleLatest(this, j, timeUnit, scheduler, z));
    }

    public final Observable<Timed<T>> timeInterval(TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableTimeInterval(this, timeUnit, scheduler));
    }

    public final Observable<T> timeout(long j, TimeUnit timeUnit) {
        return timeout0(j, timeUnit, (ObservableSource) null, Schedulers.a());
    }

    public final Observable<Timed<T>> timestamp(TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return map(Functions.a(timeUnit, scheduler));
    }

    public final <U extends Collection<? super T>> Single<U> toList(Callable<U> callable) {
        ObjectHelper.a(callable, "collectionSupplier is null");
        return RxJavaPlugins.a(new ObservableToListSingle(this, callable));
    }

    public final Single<List<T>> toSortedList(Comparator<? super T> comparator, int i) {
        ObjectHelper.a(comparator, "comparator is null");
        return toList(i).a(Functions.a(comparator));
    }

    public final <T1, T2, R> Observable<R> withLatestFrom(ObservableSource<T1> observableSource, ObservableSource<T2> observableSource2, Function3<? super T, ? super T1, ? super T2, R> function3) {
        ObjectHelper.a(observableSource, "o1 is null");
        ObjectHelper.a(observableSource2, "o2 is null");
        ObjectHelper.a(function3, "combiner is null");
        return withLatestFrom((ObservableSource<?>[]) new ObservableSource[]{observableSource, observableSource2}, Functions.a(function3));
    }

    public final <U, R> Observable<R> zipWith(ObservableSource<? extends U> observableSource, BiFunction<? super T, ? super U, ? extends R> biFunction) {
        ObjectHelper.a(observableSource, "other is null");
        return zip(this, observableSource, biFunction);
    }

    public static <T, S> Observable<T> generate(Callable<S> callable, BiConsumer<S, Emitter<T>> biConsumer) {
        ObjectHelper.a(biConsumer, "generator  is null");
        return generate(callable, ObservableInternalHelper.a(biConsumer), Functions.d());
    }

    public static Observable<Long> interval(long j, TimeUnit timeUnit) {
        return interval(j, j, timeUnit, Schedulers.a());
    }

    public final T blockingFirst(T t) {
        BlockingFirstObserver blockingFirstObserver = new BlockingFirstObserver();
        subscribe(blockingFirstObserver);
        T a2 = blockingFirstObserver.a();
        return a2 != null ? a2 : t;
    }

    public final T blockingLast(T t) {
        BlockingLastObserver blockingLastObserver = new BlockingLastObserver();
        subscribe(blockingLastObserver);
        T a2 = blockingLastObserver.a();
        return a2 != null ? a2 : t;
    }

    public final void blockingSubscribe(Observer<? super T> observer) {
        ObservableBlockingSubscribe.a(this, observer);
    }

    public final Observable<T> concatWith(MaybeSource<? extends T> maybeSource) {
        ObjectHelper.a(maybeSource, "other is null");
        return RxJavaPlugins.a(new ObservableConcatWithMaybe(this, maybeSource));
    }

    public final Observable<T> delay(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return delay(j, timeUnit, scheduler, false);
    }

    public final <K, V> Observable<GroupedObservable<K, V>> groupBy(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2, boolean z, int i) {
        ObjectHelper.a(function, "keySelector is null");
        ObjectHelper.a(function2, "valueSelector is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableGroupBy(this, function, function2, i, z));
    }

    public final Observable<T> mergeWith(MaybeSource<? extends T> maybeSource) {
        ObjectHelper.a(maybeSource, "other is null");
        return RxJavaPlugins.a(new ObservableMergeWithMaybe(this, maybeSource));
    }

    public final Observable<T> retry(long j, Predicate<? super Throwable> predicate) {
        if (j >= 0) {
            ObjectHelper.a(predicate, "predicate is null");
            return RxJavaPlugins.a(new ObservableRetryPredicate(this, j, predicate));
        }
        throw new IllegalArgumentException("times >= 0 required but it was " + j);
    }

    public final Observable<T> skipLast(long j, TimeUnit timeUnit, boolean z) {
        return skipLast(j, timeUnit, Schedulers.c(), z, bufferSize());
    }

    public final Disposable subscribe(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, Action action, Consumer<? super Disposable> consumer3) {
        ObjectHelper.a(consumer, "onNext is null");
        ObjectHelper.a(consumer2, "onError is null");
        ObjectHelper.a(action, "onComplete is null");
        ObjectHelper.a(consumer3, "onSubscribe is null");
        LambdaObserver lambdaObserver = new LambdaObserver(consumer, consumer2, action, consumer3);
        subscribe(lambdaObserver);
        return lambdaObserver;
    }

    public final Observable<T> takeLast(long j, long j2, TimeUnit timeUnit) {
        return takeLast(j, j2, timeUnit, Schedulers.c(), false, bufferSize());
    }

    public final Observable<T> timeout(long j, TimeUnit timeUnit, ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return timeout0(j, timeUnit, observableSource, Schedulers.a());
    }

    public final <K, V> Single<Map<K, Collection<V>>> toMultimap(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2) {
        return toMultimap(function, function2, HashMapSupplier.a(), ArrayListSupplier.b());
    }

    public static <T> Observable<T> fromFuture(Future<? extends T> future, long j, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(scheduler, "scheduler is null");
        return fromFuture(future, j, timeUnit).subscribeOn(scheduler);
    }

    public static Observable<Long> interval(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return interval(j, j, timeUnit, scheduler);
    }

    public static <T> Observable<T> just(T t, T t2, T t3) {
        ObjectHelper.a(t, "The first item is null");
        ObjectHelper.a(t2, "The second item is null");
        ObjectHelper.a(t3, "The third item is null");
        return fromArray(t, t2, t3);
    }

    public static <T> Observable<T> merge(ObservableSource<? extends ObservableSource<? extends T>> observableSource, int i) {
        ObjectHelper.a(observableSource, "sources is null");
        ObjectHelper.a(i, "maxConcurrency");
        return RxJavaPlugins.a(new ObservableFlatMap(observableSource, Functions.e(), false, i, bufferSize()));
    }

    public static <T> Observable<T> mergeDelayError(ObservableSource<? extends ObservableSource<? extends T>> observableSource, int i) {
        ObjectHelper.a(observableSource, "sources is null");
        ObjectHelper.a(i, "maxConcurrency");
        return RxJavaPlugins.a(new ObservableFlatMap(observableSource, Functions.e(), true, i, bufferSize()));
    }

    public final Observable<T> delay(long j, TimeUnit timeUnit, Scheduler scheduler, boolean z) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableDelay(this, j, timeUnit, scheduler, z));
    }

    public final Observable<T> doOnEach(Consumer<? super Notification<T>> consumer) {
        ObjectHelper.a(consumer, "consumer is null");
        return doOnEach(Functions.c(consumer), Functions.b(consumer), Functions.a(consumer), Functions.c);
    }

    public final Observable<T> sample(long j, TimeUnit timeUnit, Scheduler scheduler, boolean z) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new ObservableSampleTimed(this, j, timeUnit, scheduler, z));
    }

    public final Observable<T> skipLast(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return skipLast(j, timeUnit, scheduler, false, bufferSize());
    }

    public final Observable<T> takeLast(long j, long j2, TimeUnit timeUnit, Scheduler scheduler) {
        return takeLast(j, j2, timeUnit, scheduler, false, bufferSize());
    }

    public final <K, V> Single<Map<K, V>> toMap(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2, Callable<? extends Map<K, V>> callable) {
        ObjectHelper.a(function, "keySelector is null");
        ObjectHelper.a(function2, "valueSelector is null");
        ObjectHelper.a(callable, "mapSupplier is null");
        return collect(callable, Functions.a(function, function2));
    }

    public final Single<List<T>> toSortedList(int i) {
        return toSortedList(Functions.g(), i);
    }

    public final <U, R> Observable<R> zipWith(ObservableSource<? extends U> observableSource, BiFunction<? super T, ? super U, ? extends R> biFunction, boolean z) {
        return zip(this, observableSource, biFunction, z);
    }

    public static <T, R> Observable<R> combineLatest(ObservableSource<? extends T>[] observableSourceArr, Function<? super Object[], ? extends R> function) {
        return combineLatest(observableSourceArr, function, bufferSize());
    }

    public static <T> Observable<T> concat(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        return concatArray(observableSource, observableSource2);
    }

    public static <T, S> Observable<T> generate(Callable<S> callable, BiConsumer<S, Emitter<T>> biConsumer, Consumer<? super S> consumer) {
        ObjectHelper.a(biConsumer, "generator  is null");
        return generate(callable, ObservableInternalHelper.a(biConsumer), consumer);
    }

    public final <U extends Collection<? super T>> Observable<U> buffer(int i, Callable<U> callable) {
        return buffer(i, i, callable);
    }

    public final Observable<T> concatWith(CompletableSource completableSource) {
        ObjectHelper.a(completableSource, "other is null");
        return RxJavaPlugins.a(new ObservableConcatWithCompletable(this, completableSource));
    }

    public final Observable<T> mergeWith(CompletableSource completableSource) {
        ObjectHelper.a(completableSource, "other is null");
        return RxJavaPlugins.a(new ObservableMergeWithCompletable(this, completableSource));
    }

    public final <R> Observable<R> replay(Function<? super Observable<T>, ? extends ObservableSource<R>> function, int i, long j, TimeUnit timeUnit) {
        return replay(function, i, j, timeUnit, Schedulers.a());
    }

    public final Observable<T> skipLast(long j, TimeUnit timeUnit, Scheduler scheduler, boolean z) {
        return skipLast(j, timeUnit, scheduler, z, bufferSize());
    }

    public final Observable<T> takeLast(long j, long j2, TimeUnit timeUnit, Scheduler scheduler, boolean z, int i) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        ObjectHelper.a(i, "bufferSize");
        if (j >= 0) {
            return RxJavaPlugins.a(new ObservableTakeLastTimed(this, j, j2, timeUnit, scheduler, i, z));
        }
        throw new IndexOutOfBoundsException("count >= 0 required but it was " + j);
    }

    public final Observable<T> timeout(long j, TimeUnit timeUnit, Scheduler scheduler, ObservableSource<? extends T> observableSource) {
        ObjectHelper.a(observableSource, "other is null");
        return timeout0(j, timeUnit, observableSource, scheduler);
    }

    public final Observable<Observable<T>> window(long j, long j2, TimeUnit timeUnit) {
        return window(j, j2, timeUnit, Schedulers.a(), bufferSize());
    }

    public final <U, R> Observable<R> zipWith(ObservableSource<? extends U> observableSource, BiFunction<? super T, ? super U, ? extends R> biFunction, boolean z, int i) {
        return zip(this, observableSource, biFunction, z, i);
    }

    public static <T, R> Observable<R> combineLatest(ObservableSource<? extends T>[] observableSourceArr, Function<? super Object[], ? extends R> function, int i) {
        ObjectHelper.a(observableSourceArr, "sources is null");
        if (observableSourceArr.length == 0) {
            return empty();
        }
        ObjectHelper.a(function, "combiner is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableCombineLatest(observableSourceArr, (Iterable) null, function, i << 1, false));
    }

    public static <T, R> Observable<R> combineLatestDelayError(Iterable<? extends ObservableSource<? extends T>> iterable, Function<? super Object[], ? extends R> function) {
        return combineLatestDelayError(iterable, function, bufferSize());
    }

    public static <T> Single<Boolean> sequenceEqual(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2, int i) {
        return sequenceEqual(observableSource, observableSource2, ObjectHelper.a(), i);
    }

    public final Observable<List<T>> buffer(long j, long j2, TimeUnit timeUnit) {
        return buffer(j, j2, timeUnit, Schedulers.a(), ArrayListSupplier.a());
    }

    public final <R> Observable<R> replay(Function<? super Observable<T>, ? extends ObservableSource<R>> function, int i, long j, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(function, "selector is null");
        ObjectHelper.a(i, "bufferSize");
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return ObservableReplay.a(ObservableInternalHelper.a(this, i, j, timeUnit, scheduler), function);
    }

    public final Observable<T> retry(Predicate<? super Throwable> predicate) {
        return retry(Clock.MAX_TIME, predicate);
    }

    public final Observable<T> skipLast(long j, TimeUnit timeUnit, Scheduler scheduler, boolean z, int i) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableSkipLastTimed(this, j, timeUnit, scheduler, i << 1, z));
    }

    public final <K, V> Single<Map<K, Collection<V>>> toMultimap(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2, Callable<? extends Map<K, Collection<V>>> callable, Function<? super K, ? extends Collection<? super V>> function3) {
        ObjectHelper.a(function, "keySelector is null");
        ObjectHelper.a(function2, "valueSelector is null");
        ObjectHelper.a(callable, "mapSupplier is null");
        ObjectHelper.a(function3, "collectionFactory is null");
        return collect(callable, Functions.a(function, function2, function3));
    }

    public final Observable<Observable<T>> window(long j, long j2, TimeUnit timeUnit, Scheduler scheduler) {
        return window(j, j2, timeUnit, scheduler, bufferSize());
    }

    public static <T, R> Observable<R> combineLatestDelayError(Iterable<? extends ObservableSource<? extends T>> iterable, Function<? super Object[], ? extends R> function, int i) {
        ObjectHelper.a(iterable, "sources is null");
        ObjectHelper.a(function, "combiner is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableCombineLatest((ObservableSource<? extends T>[]) null, iterable, function, i << 1, true));
    }

    public static <T> Observable<T> fromFuture(Future<? extends T> future, Scheduler scheduler) {
        ObjectHelper.a(scheduler, "scheduler is null");
        return fromFuture(future).subscribeOn(scheduler);
    }

    public static <T, S> Observable<T> generate(Callable<S> callable, BiFunction<S, Emitter<T>, S> biFunction) {
        return generate(callable, biFunction, Functions.d());
    }

    public static <T> Observable<T> merge(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        return fromArray(observableSource, observableSource2).flatMap(Functions.e(), false, 2);
    }

    public static <T> Observable<T> mergeDelayError(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        return fromArray(observableSource, observableSource2).flatMap(Functions.e(), true, 2);
    }

    public static <T1, T2, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, BiFunction<? super T1, ? super T2, ? extends R> biFunction) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        return zipArray(Functions.a(biFunction), false, bufferSize(), observableSource, observableSource2);
    }

    public final Observable<List<T>> buffer(long j, long j2, TimeUnit timeUnit, Scheduler scheduler) {
        return buffer(j, j2, timeUnit, scheduler, ArrayListSupplier.a());
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [io.reactivex.functions.Function, io.reactivex.functions.Function<? super T, ? extends io.reactivex.ObservableSource<V>>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <U, V> io.reactivex.Observable<T> delay(io.reactivex.ObservableSource<U> r1, io.reactivex.functions.Function<? super T, ? extends io.reactivex.ObservableSource<V>> r2) {
        /*
            r0 = this;
            io.reactivex.Observable r1 = r0.delaySubscription(r1)
            io.reactivex.Observable r1 = r1.delay(r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: io.reactivex.Observable.delay(io.reactivex.ObservableSource, io.reactivex.functions.Function):io.reactivex.Observable");
    }

    public final <U> Observable<T> sample(ObservableSource<U> observableSource) {
        ObjectHelper.a(observableSource, "sampler is null");
        return RxJavaPlugins.a(new ObservableSampleWithObservable(this, observableSource, false));
    }

    public final Observable<T> timeout(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return timeout0(j, timeUnit, (ObservableSource) null, scheduler);
    }

    public final Observable<Observable<T>> window(long j, long j2, TimeUnit timeUnit, Scheduler scheduler, int i) {
        ObjectHelper.a(j, "timespan");
        long j3 = j2;
        ObjectHelper.a(j3, "timeskip");
        int i2 = i;
        ObjectHelper.a(i2, "bufferSize");
        Scheduler scheduler2 = scheduler;
        ObjectHelper.a(scheduler2, "scheduler is null");
        TimeUnit timeUnit2 = timeUnit;
        ObjectHelper.a(timeUnit2, "unit is null");
        return RxJavaPlugins.a(new ObservableWindowTimed(this, j, j3, timeUnit2, scheduler2, Clock.MAX_TIME, i2, false));
    }

    public final <T1, T2, T3, R> Observable<R> withLatestFrom(ObservableSource<T1> observableSource, ObservableSource<T2> observableSource2, ObservableSource<T3> observableSource3, Function4<? super T, ? super T1, ? super T2, ? super T3, R> function4) {
        ObjectHelper.a(observableSource, "o1 is null");
        ObjectHelper.a(observableSource2, "o2 is null");
        ObjectHelper.a(observableSource3, "o3 is null");
        ObjectHelper.a(function4, "combiner is null");
        return withLatestFrom((ObservableSource<?>[]) new ObservableSource[]{observableSource, observableSource2, observableSource3}, Functions.a(function4));
    }

    public static <T> Observable<T> concat(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2, ObservableSource<? extends T> observableSource3) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        return concatArray(observableSource, observableSource2, observableSource3);
    }

    public static <T, S> Observable<T> generate(Callable<S> callable, BiFunction<S, Emitter<T>, S> biFunction, Consumer<? super S> consumer) {
        ObjectHelper.a(callable, "initialState is null");
        ObjectHelper.a(biFunction, "generator  is null");
        ObjectHelper.a(consumer, "disposeState is null");
        return RxJavaPlugins.a(new ObservableGenerate(callable, biFunction, consumer));
    }

    public static <T> Observable<T> just(T t, T t2, T t3, T t4) {
        ObjectHelper.a(t, "The first item is null");
        ObjectHelper.a(t2, "The second item is null");
        ObjectHelper.a(t3, "The third item is null");
        ObjectHelper.a(t4, "The fourth item is null");
        return fromArray(t, t2, t3, t4);
    }

    public final <U extends Collection<? super T>> Observable<U> buffer(long j, long j2, TimeUnit timeUnit, Scheduler scheduler, Callable<U> callable) {
        TimeUnit timeUnit2 = timeUnit;
        ObjectHelper.a(timeUnit2, "unit is null");
        Scheduler scheduler2 = scheduler;
        ObjectHelper.a(scheduler2, "scheduler is null");
        Callable<U> callable2 = callable;
        ObjectHelper.a(callable2, "bufferSupplier is null");
        return RxJavaPlugins.a(new ObservableBufferTimed(this, j, j2, timeUnit2, scheduler2, callable2, Integer.MAX_VALUE, false));
    }

    public final <U, V> Observable<T> timeout(ObservableSource<U> observableSource, Function<? super T, ? extends ObservableSource<V>> function) {
        ObjectHelper.a(observableSource, "firstTimeoutIndicator is null");
        return timeout0(observableSource, function, (ObservableSource) null);
    }

    public final Observable<T> doOnEach(Observer<? super T> observer) {
        ObjectHelper.a(observer, "observer is null");
        return doOnEach(ObservableInternalHelper.c(observer), ObservableInternalHelper.b(observer), ObservableInternalHelper.a(observer), Functions.c);
    }

    public final <U> Observable<T> sample(ObservableSource<U> observableSource, boolean z) {
        ObjectHelper.a(observableSource, "sampler is null");
        return RxJavaPlugins.a(new ObservableSampleWithObservable(this, observableSource, z));
    }

    public final void subscribe(Observer<? super T> observer) {
        ObjectHelper.a(observer, "observer is null");
        try {
            Observer<? super Object> a2 = RxJavaPlugins.a(this, observer);
            ObjectHelper.a(a2, "The RxJavaPlugins.onSubscribe hook returned a null Observer. Please change the handler provided to RxJavaPlugins.setOnObservableSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins");
            subscribeActual(a2);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable th) {
            Exceptions.b(th);
            RxJavaPlugins.b(th);
            NullPointerException nullPointerException = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }

    public static <T> Observable<T> merge(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2, ObservableSource<? extends T> observableSource3) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        return fromArray(observableSource, observableSource2, observableSource3).flatMap(Functions.e(), false, 3);
    }

    public static <T> Observable<T> mergeDelayError(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2, ObservableSource<? extends T> observableSource3) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        return fromArray(observableSource, observableSource2, observableSource3).flatMap(Functions.e(), true, 3);
    }

    public static <T1, T2, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, BiFunction<? super T1, ? super T2, ? extends R> biFunction, boolean z) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        return zipArray(Functions.a(biFunction), z, bufferSize(), observableSource, observableSource2);
    }

    public final <R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends R>> function, Function<? super Throwable, ? extends ObservableSource<? extends R>> function2, Callable<? extends ObservableSource<? extends R>> callable) {
        ObjectHelper.a(function, "onNextMapper is null");
        ObjectHelper.a(function2, "onErrorMapper is null");
        ObjectHelper.a(callable, "onCompleteSupplier is null");
        return merge(new ObservableMapNotification(this, function, function2, callable));
    }

    public final Observable<T> takeLast(long j, TimeUnit timeUnit) {
        return takeLast(j, timeUnit, Schedulers.c(), false, bufferSize());
    }

    public final <U, V> Observable<T> timeout(ObservableSource<U> observableSource, Function<? super T, ? extends ObservableSource<V>> function, ObservableSource<? extends T> observableSource2) {
        ObjectHelper.a(observableSource, "firstTimeoutIndicator is null");
        ObjectHelper.a(observableSource2, "other is null");
        return timeout0(observableSource, function, observableSource2);
    }

    public final Observable<T> takeLast(long j, TimeUnit timeUnit, boolean z) {
        return takeLast(j, timeUnit, Schedulers.c(), z, bufferSize());
    }

    public final <K, V> Single<Map<K, Collection<V>>> toMultimap(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2, Callable<Map<K, Collection<V>>> callable) {
        return toMultimap(function, function2, callable, ArrayListSupplier.b());
    }

    public static <T1, T2, R> Observable<R> combineLatest(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, BiFunction<? super T1, ? super T2, ? extends R> biFunction) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        return combineLatest(Functions.a(biFunction), bufferSize(), (ObservableSource<? extends T>[]) new ObservableSource[]{observableSource, observableSource2});
    }

    public static <T> Observable<T> concat(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2, ObservableSource<? extends T> observableSource3, ObservableSource<? extends T> observableSource4) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        return concatArray(observableSource, observableSource2, observableSource3, observableSource4);
    }

    public final Observable<List<T>> buffer(long j, TimeUnit timeUnit) {
        return buffer(j, timeUnit, Schedulers.a(), Integer.MAX_VALUE);
    }

    public final <R> Observable<R> replay(Function<? super Observable<T>, ? extends ObservableSource<R>> function, int i, Scheduler scheduler) {
        ObjectHelper.a(function, "selector is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        ObjectHelper.a(i, "bufferSize");
        return ObservableReplay.a(ObservableInternalHelper.a(this, i), ObservableInternalHelper.a(function, scheduler));
    }

    public final Observable<T> takeLast(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return takeLast(j, timeUnit, scheduler, false, bufferSize());
    }

    public static <T> Observable<T> just(T t, T t2, T t3, T t4, T t5) {
        ObjectHelper.a(t, "The first item is null");
        ObjectHelper.a(t2, "The second item is null");
        ObjectHelper.a(t3, "The third item is null");
        ObjectHelper.a(t4, "The fourth item is null");
        ObjectHelper.a(t5, "The fifth item is null");
        return fromArray(t, t2, t3, t4, t5);
    }

    public static <T1, T2, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, BiFunction<? super T1, ? super T2, ? extends R> biFunction, boolean z, int i) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        return zipArray(Functions.a(biFunction), z, i, observableSource, observableSource2);
    }

    public final Observable<List<T>> buffer(long j, TimeUnit timeUnit, int i) {
        return buffer(j, timeUnit, Schedulers.a(), i);
    }

    public final Observable<T> takeLast(long j, TimeUnit timeUnit, Scheduler scheduler, boolean z) {
        return takeLast(j, timeUnit, scheduler, z, bufferSize());
    }

    public final Observable<Observable<T>> window(long j, TimeUnit timeUnit) {
        return window(j, timeUnit, Schedulers.a(), (long) Clock.MAX_TIME, false);
    }

    public final <T1, T2, T3, T4, R> Observable<R> withLatestFrom(ObservableSource<T1> observableSource, ObservableSource<T2> observableSource2, ObservableSource<T3> observableSource3, ObservableSource<T4> observableSource4, Function5<? super T, ? super T1, ? super T2, ? super T3, ? super T4, R> function5) {
        ObjectHelper.a(observableSource, "o1 is null");
        ObjectHelper.a(observableSource2, "o2 is null");
        ObjectHelper.a(observableSource3, "o3 is null");
        ObjectHelper.a(observableSource4, "o4 is null");
        ObjectHelper.a(function5, "combiner is null");
        return withLatestFrom((ObservableSource<?>[]) new ObservableSource[]{observableSource, observableSource2, observableSource3, observableSource4}, Functions.a(function5));
    }

    public static <T> Observable<T> merge(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2, ObservableSource<? extends T> observableSource3, ObservableSource<? extends T> observableSource4) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        return fromArray(observableSource, observableSource2, observableSource3, observableSource4).flatMap(Functions.e(), false, 4);
    }

    public static <T> Observable<T> mergeDelayError(ObservableSource<? extends T> observableSource, ObservableSource<? extends T> observableSource2, ObservableSource<? extends T> observableSource3, ObservableSource<? extends T> observableSource4) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        return fromArray(observableSource, observableSource2, observableSource3, observableSource4).flatMap(Functions.e(), true, 4);
    }

    public final Observable<List<T>> buffer(long j, TimeUnit timeUnit, Scheduler scheduler, int i) {
        return buffer(j, timeUnit, scheduler, i, ArrayListSupplier.a(), false);
    }

    public final <R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends R>> function, Function<Throwable, ? extends ObservableSource<? extends R>> function2, Callable<? extends ObservableSource<? extends R>> callable, int i) {
        ObjectHelper.a(function, "onNextMapper is null");
        ObjectHelper.a(function2, "onErrorMapper is null");
        ObjectHelper.a(callable, "onCompleteSupplier is null");
        return merge(new ObservableMapNotification(this, function, function2, callable), i);
    }

    public final Observable<T> takeLast(long j, TimeUnit timeUnit, Scheduler scheduler, boolean z, int i) {
        return takeLast(Clock.MAX_TIME, j, timeUnit, scheduler, z, i);
    }

    public final Observable<Observable<T>> window(long j, TimeUnit timeUnit, long j2) {
        return window(j, timeUnit, Schedulers.a(), j2, false);
    }

    public static <T1, T2, T3, R> Observable<R> combineLatest(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, Function3<? super T1, ? super T2, ? super T3, ? extends R> function3) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        return combineLatest(Functions.a(function3), bufferSize(), (ObservableSource<? extends T>[]) new ObservableSource[]{observableSource, observableSource2, observableSource3});
    }

    public final <U extends Collection<? super T>> Observable<U> buffer(long j, TimeUnit timeUnit, Scheduler scheduler, int i, Callable<U> callable, boolean z) {
        ObjectHelper.a(timeUnit, "unit is null");
        Scheduler scheduler2 = scheduler;
        ObjectHelper.a(scheduler2, "scheduler is null");
        Callable<U> callable2 = callable;
        ObjectHelper.a(callable2, "bufferSupplier is null");
        int i2 = i;
        ObjectHelper.a(i2, "count");
        return RxJavaPlugins.a(new ObservableBufferTimed(this, j, j, timeUnit, scheduler2, callable2, i2, z));
    }

    public final Observable<Observable<T>> window(long j, TimeUnit timeUnit, long j2, boolean z) {
        return window(j, timeUnit, Schedulers.a(), j2, z);
    }

    public static <T1, T2, T3, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, Function3<? super T1, ? super T2, ? super T3, ? extends R> function3) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        return zipArray(Functions.a(function3), false, bufferSize(), observableSource, observableSource2, observableSource3);
    }

    public final Observable<Observable<T>> window(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return window(j, timeUnit, scheduler, (long) Clock.MAX_TIME, false);
    }

    public final Observable<Observable<T>> window(long j, TimeUnit timeUnit, Scheduler scheduler, long j2) {
        return window(j, timeUnit, scheduler, j2, false);
    }

    public final <R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends R>> function, int i) {
        return flatMap(function, false, i, bufferSize());
    }

    public final <R> Observable<R> replay(Function<? super Observable<T>, ? extends ObservableSource<R>> function, long j, TimeUnit timeUnit) {
        return replay(function, j, timeUnit, Schedulers.a());
    }

    public final Observable<Observable<T>> window(long j, TimeUnit timeUnit, Scheduler scheduler, long j2, boolean z) {
        return window(j, timeUnit, scheduler, j2, z, bufferSize());
    }

    public static <T1, T2, T3, T4, R> Observable<R> combineLatest(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, Function4<? super T1, ? super T2, ? super T3, ? super T4, ? extends R> function4) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        return combineLatest(Functions.a(function4), bufferSize(), (ObservableSource<? extends T>[]) new ObservableSource[]{observableSource, observableSource2, observableSource3, observableSource4});
    }

    public static <T> Observable<T> just(T t, T t2, T t3, T t4, T t5, T t6) {
        ObjectHelper.a(t, "The first item is null");
        ObjectHelper.a(t2, "The second item is null");
        ObjectHelper.a(t3, "The third item is null");
        ObjectHelper.a(t4, "The fourth item is null");
        ObjectHelper.a(t5, "The fifth item is null");
        ObjectHelper.a(t6, "The sixth item is null");
        return fromArray(t, t2, t3, t4, t5, t6);
    }

    public final <U, R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends U>> function, BiFunction<? super T, ? super U, ? extends R> biFunction) {
        return flatMap(function, biFunction, false, bufferSize(), bufferSize());
    }

    public final <R> Observable<R> replay(Function<? super Observable<T>, ? extends ObservableSource<R>> function, long j, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(function, "selector is null");
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return ObservableReplay.a(ObservableInternalHelper.a(this, j, timeUnit, scheduler), function);
    }

    public final Observable<Observable<T>> window(long j, TimeUnit timeUnit, Scheduler scheduler, long j2, boolean z, int i) {
        int i2 = i;
        ObjectHelper.a(i2, "bufferSize");
        Scheduler scheduler2 = scheduler;
        ObjectHelper.a(scheduler2, "scheduler is null");
        TimeUnit timeUnit2 = timeUnit;
        ObjectHelper.a(timeUnit2, "unit is null");
        long j3 = j2;
        ObjectHelper.a(j3, "count");
        return RxJavaPlugins.a(new ObservableWindowTimed(this, j, j, timeUnit2, scheduler2, j3, i2, z));
    }

    public static <T1, T2, T3, T4, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, Function4<? super T1, ? super T2, ? super T3, ? super T4, ? extends R> function4) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        return zipArray(Functions.a(function4), false, bufferSize(), observableSource, observableSource2, observableSource3, observableSource4);
    }

    public final Observable<List<T>> buffer(long j, TimeUnit timeUnit, Scheduler scheduler) {
        return buffer(j, timeUnit, scheduler, Integer.MAX_VALUE, ArrayListSupplier.a(), false);
    }

    public final <U, R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends U>> function, BiFunction<? super T, ? super U, ? extends R> biFunction, boolean z) {
        return flatMap(function, biFunction, z, bufferSize(), bufferSize());
    }

    public final <R> Observable<R> withLatestFrom(ObservableSource<?>[] observableSourceArr, Function<? super Object[], R> function) {
        ObjectHelper.a(observableSourceArr, "others is null");
        ObjectHelper.a(function, "combiner is null");
        return RxJavaPlugins.a(new ObservableWithLatestFromMany(this, observableSourceArr, function));
    }

    public final <TOpening, TClosing> Observable<List<T>> buffer(ObservableSource<? extends TOpening> observableSource, Function<? super TOpening, ? extends ObservableSource<? extends TClosing>> function) {
        return buffer(observableSource, function, ArrayListSupplier.a());
    }

    public final <U, R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends U>> function, BiFunction<? super T, ? super U, ? extends R> biFunction, boolean z, int i) {
        return flatMap(function, biFunction, z, i, bufferSize());
    }

    public final <TOpening, TClosing, U extends Collection<? super T>> Observable<U> buffer(ObservableSource<? extends TOpening> observableSource, Function<? super TOpening, ? extends ObservableSource<? extends TClosing>> function, Callable<U> callable) {
        ObjectHelper.a(observableSource, "openingIndicator is null");
        ObjectHelper.a(function, "closingIndicator is null");
        ObjectHelper.a(callable, "bufferSupplier is null");
        return RxJavaPlugins.a(new ObservableBufferBoundary(this, observableSource, function, callable));
    }

    public final <U, R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends U>> function, BiFunction<? super T, ? super U, ? extends R> biFunction, boolean z, int i, int i2) {
        ObjectHelper.a(function, "mapper is null");
        ObjectHelper.a(biFunction, "combiner is null");
        return flatMap(ObservableInternalHelper.a(function, biFunction), z, i, i2);
    }

    public final <R> Observable<R> replay(Function<? super Observable<T>, ? extends ObservableSource<R>> function, Scheduler scheduler) {
        ObjectHelper.a(function, "selector is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return ObservableReplay.a(ObservableInternalHelper.a(this), ObservableInternalHelper.a(function, scheduler));
    }

    public final <R> Observable<R> withLatestFrom(Iterable<? extends ObservableSource<?>> iterable, Function<? super Object[], R> function) {
        ObjectHelper.a(iterable, "others is null");
        ObjectHelper.a(function, "combiner is null");
        return RxJavaPlugins.a(new ObservableWithLatestFromMany(this, iterable, function));
    }

    public static <T1, T2, T3, T4, T5, R> Observable<R> combineLatest(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, Function5<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? extends R> function5) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        return combineLatest(Functions.a(function5), bufferSize(), (ObservableSource<? extends T>[]) new ObservableSource[]{observableSource, observableSource2, observableSource3, observableSource4, observableSource5});
    }

    public final <B> Observable<Observable<T>> window(ObservableSource<B> observableSource) {
        return window(observableSource, bufferSize());
    }

    public static <T1, T2, T3, T4, T5, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, Function5<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? extends R> function5) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        return zipArray(Functions.a(function5), false, bufferSize(), observableSource, observableSource2, observableSource3, observableSource4, observableSource5);
    }

    public final <U, R> Observable<R> flatMap(Function<? super T, ? extends ObservableSource<? extends U>> function, BiFunction<? super T, ? super U, ? extends R> biFunction, int i) {
        return flatMap(function, biFunction, false, i, bufferSize());
    }

    public final <B> Observable<Observable<T>> window(ObservableSource<B> observableSource, int i) {
        ObjectHelper.a(observableSource, "boundary is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableWindowBoundary(this, observableSource, i));
    }

    public static <T> Observable<T> just(T t, T t2, T t3, T t4, T t5, T t6, T t7) {
        ObjectHelper.a(t, "The first item is null");
        ObjectHelper.a(t2, "The second item is null");
        ObjectHelper.a(t3, "The third item is null");
        ObjectHelper.a(t4, "The fourth item is null");
        ObjectHelper.a(t5, "The fifth item is null");
        ObjectHelper.a(t6, "The sixth item is null");
        ObjectHelper.a(t7, "The seventh item is null");
        return fromArray(t, t2, t3, t4, t5, t6, t7);
    }

    public final <B> Observable<List<T>> buffer(ObservableSource<B> observableSource) {
        return buffer(observableSource, ArrayListSupplier.a());
    }

    public final <B> Observable<List<T>> buffer(ObservableSource<B> observableSource, int i) {
        ObjectHelper.a(i, "initialCapacity");
        return buffer(observableSource, Functions.a(i));
    }

    public final ConnectableObservable<T> replay(int i) {
        ObjectHelper.a(i, "bufferSize");
        return ObservableReplay.a(this, i);
    }

    public final <U, V> Observable<Observable<T>> window(ObservableSource<U> observableSource, Function<? super U, ? extends ObservableSource<V>> function) {
        return window(observableSource, function, bufferSize());
    }

    public final <B, U extends Collection<? super T>> Observable<U> buffer(ObservableSource<B> observableSource, Callable<U> callable) {
        ObjectHelper.a(observableSource, "boundary is null");
        ObjectHelper.a(callable, "bufferSupplier is null");
        return RxJavaPlugins.a(new ObservableBufferExactBoundary(this, observableSource, callable));
    }

    public final <U, V> Observable<Observable<T>> window(ObservableSource<U> observableSource, Function<? super U, ? extends ObservableSource<V>> function, int i) {
        ObjectHelper.a(observableSource, "openingIndicator is null");
        ObjectHelper.a(function, "closingIndicator is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableWindowBoundarySelector(this, observableSource, function, i));
    }

    public static <T1, T2, T3, T4, T5, T6, R> Observable<R> combineLatest(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, ObservableSource<? extends T6> observableSource6, Function6<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? extends R> function6) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        ObjectHelper.a(observableSource6, "source6 is null");
        return combineLatest(Functions.a(function6), bufferSize(), (ObservableSource<? extends T>[]) new ObservableSource[]{observableSource, observableSource2, observableSource3, observableSource4, observableSource5, observableSource6});
    }

    public final ConnectableObservable<T> replay(int i, long j, TimeUnit timeUnit) {
        return replay(i, j, timeUnit, Schedulers.a());
    }

    public static <T1, T2, T3, T4, T5, T6, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, ObservableSource<? extends T6> observableSource6, Function6<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? extends R> function6) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        ObjectHelper.a(observableSource6, "source6 is null");
        return zipArray(Functions.a(function6), false, bufferSize(), observableSource, observableSource2, observableSource3, observableSource4, observableSource5, observableSource6);
    }

    public final ConnectableObservable<T> replay(int i, long j, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(i, "bufferSize");
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return ObservableReplay.a(this, j, timeUnit, scheduler, i);
    }

    public final <B> Observable<List<T>> buffer(Callable<? extends ObservableSource<B>> callable) {
        return buffer(callable, ArrayListSupplier.a());
    }

    public final <B, U extends Collection<? super T>> Observable<U> buffer(Callable<? extends ObservableSource<B>> callable, Callable<U> callable2) {
        ObjectHelper.a(callable, "boundarySupplier is null");
        ObjectHelper.a(callable2, "bufferSupplier is null");
        return RxJavaPlugins.a(new ObservableBufferBoundarySupplier(this, callable, callable2));
    }

    public final <B> Observable<Observable<T>> window(Callable<? extends ObservableSource<B>> callable) {
        return window(callable, bufferSize());
    }

    public static <T> Observable<T> just(T t, T t2, T t3, T t4, T t5, T t6, T t7, T t8) {
        ObjectHelper.a(t, "The first item is null");
        ObjectHelper.a(t2, "The second item is null");
        ObjectHelper.a(t3, "The third item is null");
        ObjectHelper.a(t4, "The fourth item is null");
        ObjectHelper.a(t5, "The fifth item is null");
        ObjectHelper.a(t6, "The sixth item is null");
        ObjectHelper.a(t7, "The seventh item is null");
        ObjectHelper.a(t8, "The eighth item is null");
        return fromArray(t, t2, t3, t4, t5, t6, t7, t8);
    }

    public final <B> Observable<Observable<T>> window(Callable<? extends ObservableSource<B>> callable, int i) {
        ObjectHelper.a(callable, "boundary is null");
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new ObservableWindowBoundarySupplier(this, callable, i));
    }

    public final ConnectableObservable<T> replay(int i, Scheduler scheduler) {
        ObjectHelper.a(i, "bufferSize");
        return ObservableReplay.a(replay(i), scheduler);
    }

    public static <T1, T2, T3, T4, T5, T6, T7, R> Observable<R> combineLatest(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, ObservableSource<? extends T6> observableSource6, ObservableSource<? extends T7> observableSource7, Function7<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? extends R> function7) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        ObjectHelper.a(observableSource6, "source6 is null");
        ObjectHelper.a(observableSource7, "source7 is null");
        return combineLatest(Functions.a(function7), bufferSize(), (ObservableSource<? extends T>[]) new ObservableSource[]{observableSource, observableSource2, observableSource3, observableSource4, observableSource5, observableSource6, observableSource7});
    }

    public final ConnectableObservable<T> replay(long j, TimeUnit timeUnit) {
        return replay(j, timeUnit, Schedulers.a());
    }

    public static <T1, T2, T3, T4, T5, T6, T7, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, ObservableSource<? extends T6> observableSource6, ObservableSource<? extends T7> observableSource7, Function7<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? extends R> function7) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        ObjectHelper.a(observableSource6, "source6 is null");
        ObjectHelper.a(observableSource7, "source7 is null");
        return zipArray(Functions.a(function7), false, bufferSize(), observableSource, observableSource2, observableSource3, observableSource4, observableSource5, observableSource6, observableSource7);
    }

    public final ConnectableObservable<T> replay(long j, TimeUnit timeUnit, Scheduler scheduler) {
        ObjectHelper.a(timeUnit, "unit is null");
        ObjectHelper.a(scheduler, "scheduler is null");
        return ObservableReplay.a(this, j, timeUnit, scheduler);
    }

    public final ConnectableObservable<T> replay(Scheduler scheduler) {
        ObjectHelper.a(scheduler, "scheduler is null");
        return ObservableReplay.a(replay(), scheduler);
    }

    public static <T> Observable<T> just(T t, T t2, T t3, T t4, T t5, T t6, T t7, T t8, T t9) {
        ObjectHelper.a(t, "The first item is null");
        ObjectHelper.a(t2, "The second item is null");
        ObjectHelper.a(t3, "The third item is null");
        ObjectHelper.a(t4, "The fourth item is null");
        ObjectHelper.a(t5, "The fifth item is null");
        ObjectHelper.a(t6, "The sixth item is null");
        ObjectHelper.a(t7, "The seventh item is null");
        ObjectHelper.a(t8, "The eighth item is null");
        ObjectHelper.a(t9, "The ninth item is null");
        return fromArray(t, t2, t3, t4, t5, t6, t7, t8, t9);
    }

    public static <T1, T2, T3, T4, T5, T6, T7, T8, R> Observable<R> combineLatest(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, ObservableSource<? extends T6> observableSource6, ObservableSource<? extends T7> observableSource7, ObservableSource<? extends T8> observableSource8, Function8<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? extends R> function8) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        ObjectHelper.a(observableSource6, "source6 is null");
        ObjectHelper.a(observableSource7, "source7 is null");
        ObjectHelper.a(observableSource8, "source8 is null");
        return combineLatest(Functions.a(function8), bufferSize(), (ObservableSource<? extends T>[]) new ObservableSource[]{observableSource, observableSource2, observableSource3, observableSource4, observableSource5, observableSource6, observableSource7, observableSource8});
    }

    public static <T1, T2, T3, T4, T5, T6, T7, T8, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, ObservableSource<? extends T6> observableSource6, ObservableSource<? extends T7> observableSource7, ObservableSource<? extends T8> observableSource8, Function8<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? extends R> function8) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        ObjectHelper.a(observableSource6, "source6 is null");
        ObjectHelper.a(observableSource7, "source7 is null");
        ObjectHelper.a(observableSource8, "source8 is null");
        return zipArray(Functions.a(function8), false, bufferSize(), observableSource, observableSource2, observableSource3, observableSource4, observableSource5, observableSource6, observableSource7, observableSource8);
    }

    public static <T> Observable<T> just(T t, T t2, T t3, T t4, T t5, T t6, T t7, T t8, T t9, T t10) {
        ObjectHelper.a(t, "The first item is null");
        ObjectHelper.a(t2, "The second item is null");
        ObjectHelper.a(t3, "The third item is null");
        ObjectHelper.a(t4, "The fourth item is null");
        ObjectHelper.a(t5, "The fifth item is null");
        ObjectHelper.a(t6, "The sixth item is null");
        ObjectHelper.a(t7, "The seventh item is null");
        ObjectHelper.a(t8, "The eighth item is null");
        ObjectHelper.a(t9, "The ninth item is null");
        ObjectHelper.a(t10, "The tenth item is null");
        return fromArray(t, t2, t3, t4, t5, t6, t7, t8, t9, t10);
    }

    public static <T1, T2, T3, T4, T5, T6, T7, T8, T9, R> Observable<R> combineLatest(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, ObservableSource<? extends T6> observableSource6, ObservableSource<? extends T7> observableSource7, ObservableSource<? extends T8> observableSource8, ObservableSource<? extends T9> observableSource9, Function9<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? extends R> function9) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        ObjectHelper.a(observableSource6, "source6 is null");
        ObjectHelper.a(observableSource7, "source7 is null");
        ObjectHelper.a(observableSource8, "source8 is null");
        ObjectHelper.a(observableSource9, "source9 is null");
        return combineLatest(Functions.a(function9), bufferSize(), (ObservableSource<? extends T>[]) new ObservableSource[]{observableSource, observableSource2, observableSource3, observableSource4, observableSource5, observableSource6, observableSource7, observableSource8, observableSource9});
    }

    public static <T1, T2, T3, T4, T5, T6, T7, T8, T9, R> Observable<R> zip(ObservableSource<? extends T1> observableSource, ObservableSource<? extends T2> observableSource2, ObservableSource<? extends T3> observableSource3, ObservableSource<? extends T4> observableSource4, ObservableSource<? extends T5> observableSource5, ObservableSource<? extends T6> observableSource6, ObservableSource<? extends T7> observableSource7, ObservableSource<? extends T8> observableSource8, ObservableSource<? extends T9> observableSource9, Function9<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? extends R> function9) {
        ObjectHelper.a(observableSource, "source1 is null");
        ObjectHelper.a(observableSource2, "source2 is null");
        ObjectHelper.a(observableSource3, "source3 is null");
        ObjectHelper.a(observableSource4, "source4 is null");
        ObjectHelper.a(observableSource5, "source5 is null");
        ObjectHelper.a(observableSource6, "source6 is null");
        ObjectHelper.a(observableSource7, "source7 is null");
        ObjectHelper.a(observableSource8, "source8 is null");
        ObjectHelper.a(observableSource9, "source9 is null");
        return zipArray(Functions.a(function9), false, bufferSize(), observableSource, observableSource2, observableSource3, observableSource4, observableSource5, observableSource6, observableSource7, observableSource8, observableSource9);
    }
}
