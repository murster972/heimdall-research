package io.reactivex;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.BlockingMultiObserver;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class Maybe<T> implements MaybeSource<T> {
    public final void a(MaybeObserver<? super T> maybeObserver) {
        ObjectHelper.a(maybeObserver, "observer is null");
        MaybeObserver<? super Object> a2 = RxJavaPlugins.a(this, maybeObserver);
        ObjectHelper.a(a2, "The RxJavaPlugins.onSubscribe hook returned a null MaybeObserver. Please check the handler provided to RxJavaPlugins.setOnMaybeSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins");
        try {
            b(a2);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable th) {
            Exceptions.b(th);
            NullPointerException nullPointerException = new NullPointerException("subscribeActual failed");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }

    public final T b() {
        BlockingMultiObserver blockingMultiObserver = new BlockingMultiObserver();
        a(blockingMultiObserver);
        return blockingMultiObserver.a();
    }

    /* access modifiers changed from: protected */
    public abstract void b(MaybeObserver<? super T> maybeObserver);
}
