package io.reactivex;

import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.schedulers.NewThreadWorker;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.SchedulerRunnableIntrospection;
import java.util.concurrent.TimeUnit;

public abstract class Scheduler {

    /* renamed from: a  reason: collision with root package name */
    static final long f6631a = TimeUnit.MINUTES.toNanos(Long.getLong("rx2.scheduler.drift-tolerance", 15).longValue());

    static final class DisposeTask implements Disposable, Runnable, SchedulerRunnableIntrospection {

        /* renamed from: a  reason: collision with root package name */
        final Runnable f6632a;
        final Worker b;
        Thread c;

        DisposeTask(Runnable runnable, Worker worker) {
            this.f6632a = runnable;
            this.b = worker;
        }

        public void dispose() {
            if (this.c == Thread.currentThread()) {
                Worker worker = this.b;
                if (worker instanceof NewThreadWorker) {
                    ((NewThreadWorker) worker).a();
                    return;
                }
            }
            this.b.dispose();
        }

        public boolean isDisposed() {
            return this.b.isDisposed();
        }

        public void run() {
            this.c = Thread.currentThread();
            try {
                this.f6632a.run();
            } finally {
                dispose();
                this.c = null;
            }
        }
    }

    static final class PeriodicDirectTask implements Disposable, Runnable, SchedulerRunnableIntrospection {

        /* renamed from: a  reason: collision with root package name */
        final Runnable f6633a;
        final Worker b;
        volatile boolean c;

        PeriodicDirectTask(Runnable runnable, Worker worker) {
            this.f6633a = runnable;
            this.b = worker;
        }

        public void dispose() {
            this.c = true;
            this.b.dispose();
        }

        public boolean isDisposed() {
            return this.c;
        }

        public void run() {
            if (!this.c) {
                try {
                    this.f6633a.run();
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.b.dispose();
                    throw ExceptionHelper.a(th);
                }
            }
        }
    }

    public static abstract class Worker implements Disposable {

        final class PeriodicTask implements Runnable, SchedulerRunnableIntrospection {

            /* renamed from: a  reason: collision with root package name */
            final Runnable f6634a;
            final SequentialDisposable b;
            final long c;
            long d;
            long e;
            long f;

            PeriodicTask(long j, Runnable runnable, long j2, SequentialDisposable sequentialDisposable, long j3) {
                this.f6634a = runnable;
                this.b = sequentialDisposable;
                this.c = j3;
                this.e = j2;
                this.f = j;
            }

            public void run() {
                long j;
                this.f6634a.run();
                if (!this.b.isDisposed()) {
                    long a2 = Worker.this.a(TimeUnit.NANOSECONDS);
                    long j2 = Scheduler.f6631a;
                    long j3 = this.e;
                    if (a2 + j2 >= j3) {
                        long j4 = this.c;
                        if (a2 < j3 + j4 + j2) {
                            long j5 = this.f;
                            long j6 = this.d + 1;
                            this.d = j6;
                            j = j5 + (j6 * j4);
                            this.e = a2;
                            this.b.a(Worker.this.a(this, j - a2, TimeUnit.NANOSECONDS));
                        }
                    }
                    long j7 = this.c;
                    long j8 = a2 + j7;
                    long j9 = this.d + 1;
                    this.d = j9;
                    this.f = j8 - (j7 * j9);
                    j = j8;
                    this.e = a2;
                    this.b.a(Worker.this.a(this, j - a2, TimeUnit.NANOSECONDS));
                }
            }
        }

        public Disposable a(Runnable runnable) {
            return a(runnable, 0, TimeUnit.NANOSECONDS);
        }

        public abstract Disposable a(Runnable runnable, long j, TimeUnit timeUnit);

        public Disposable a(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
            long j3 = j;
            TimeUnit timeUnit2 = timeUnit;
            SequentialDisposable sequentialDisposable = new SequentialDisposable();
            SequentialDisposable sequentialDisposable2 = new SequentialDisposable(sequentialDisposable);
            Runnable a2 = RxJavaPlugins.a(runnable);
            long nanos = timeUnit2.toNanos(j2);
            long a3 = a(TimeUnit.NANOSECONDS);
            SequentialDisposable sequentialDisposable3 = sequentialDisposable;
            PeriodicTask periodicTask = r0;
            PeriodicTask periodicTask2 = new PeriodicTask(a3 + timeUnit2.toNanos(j3), a2, a3, sequentialDisposable2, nanos);
            Disposable a4 = a(periodicTask, j3, timeUnit2);
            if (a4 == EmptyDisposable.INSTANCE) {
                return a4;
            }
            sequentialDisposable3.a(a4);
            return sequentialDisposable2;
        }

        public long a(TimeUnit timeUnit) {
            return timeUnit.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        }
    }

    public long a(TimeUnit timeUnit) {
        return timeUnit.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    public abstract Worker a();

    public Disposable a(Runnable runnable) {
        return a(runnable, 0, TimeUnit.NANOSECONDS);
    }

    public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
        Worker a2 = a();
        DisposeTask disposeTask = new DisposeTask(RxJavaPlugins.a(runnable), a2);
        a2.a(disposeTask, j, timeUnit);
        return disposeTask;
    }

    public Disposable a(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        Worker a2 = a();
        PeriodicDirectTask periodicDirectTask = new PeriodicDirectTask(RxJavaPlugins.a(runnable), a2);
        Disposable a3 = a2.a(periodicDirectTask, j, j2, timeUnit);
        return a3 == EmptyDisposable.INSTANCE ? a3 : periodicDirectTask;
    }
}
