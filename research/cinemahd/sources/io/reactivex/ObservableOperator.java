package io.reactivex;

public interface ObservableOperator<Downstream, Upstream> {
    Observer<? super Upstream> a(Observer<? super Downstream> observer) throws Exception;
}
