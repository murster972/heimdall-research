package io.reactivex;

import org.reactivestreams.Subscriber;

public interface FlowableSubscriber<T> extends Subscriber<T> {
}
