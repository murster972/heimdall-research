package io.reactivex.observables;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.operators.observable.ObservableRefCount;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class ConnectableObservable<T> extends Observable<T> {
    public Observable<T> a() {
        return RxJavaPlugins.a(new ObservableRefCount(this));
    }

    public abstract void a(Consumer<? super Disposable> consumer);
}
