package io.reactivex.subjects;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.BasicIntQueueDisposable;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class UnicastSubject<T> extends Subject<T> {

    /* renamed from: a  reason: collision with root package name */
    final SpscLinkedArrayQueue<T> f6900a;
    final AtomicReference<Observer<? super T>> b;
    final AtomicReference<Runnable> c;
    final boolean d;
    volatile boolean e;
    volatile boolean f;
    Throwable g;
    final AtomicBoolean h;
    final BasicIntQueueDisposable<T> i;
    boolean j;

    final class UnicastQueueDisposable extends BasicIntQueueDisposable<T> {
        private static final long serialVersionUID = 7926949470189395511L;

        UnicastQueueDisposable() {
        }

        public int a(int i) {
            if ((i & 2) == 0) {
                return 0;
            }
            UnicastSubject.this.j = true;
            return 2;
        }

        public void clear() {
            UnicastSubject.this.f6900a.clear();
        }

        public void dispose() {
            if (!UnicastSubject.this.e) {
                UnicastSubject unicastSubject = UnicastSubject.this;
                unicastSubject.e = true;
                unicastSubject.b();
                UnicastSubject.this.b.lazySet((Object) null);
                if (UnicastSubject.this.i.getAndIncrement() == 0) {
                    UnicastSubject.this.b.lazySet((Object) null);
                    UnicastSubject.this.f6900a.clear();
                }
            }
        }

        public boolean isDisposed() {
            return UnicastSubject.this.e;
        }

        public boolean isEmpty() {
            return UnicastSubject.this.f6900a.isEmpty();
        }

        public T poll() throws Exception {
            return UnicastSubject.this.f6900a.poll();
        }
    }

    UnicastSubject(int i2, boolean z) {
        ObjectHelper.a(i2, "capacityHint");
        this.f6900a = new SpscLinkedArrayQueue<>(i2);
        this.c = new AtomicReference<>();
        this.d = z;
        this.b = new AtomicReference<>();
        this.h = new AtomicBoolean();
        this.i = new UnicastQueueDisposable();
    }

    public static <T> UnicastSubject<T> a(int i2) {
        return new UnicastSubject<>(i2, true);
    }

    public static <T> UnicastSubject<T> d() {
        return new UnicastSubject<>(Observable.bufferSize(), true);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        Runnable runnable = this.c.get();
        if (runnable != null && this.c.compareAndSet(runnable, (Object) null)) {
            runnable.run();
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Observer<? super T> observer) {
        this.b.lazySet((Object) null);
        Throwable th = this.g;
        if (th != null) {
            observer.onError(th);
        } else {
            observer.onComplete();
        }
    }

    public void onComplete() {
        if (!this.f && !this.e) {
            this.f = true;
            b();
            c();
        }
    }

    public void onError(Throwable th) {
        ObjectHelper.a(th, "onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        if (this.f || this.e) {
            RxJavaPlugins.b(th);
            return;
        }
        this.g = th;
        this.f = true;
        b();
        c();
    }

    public void onNext(T t) {
        ObjectHelper.a(t, "onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
        if (!this.f && !this.e) {
            this.f6900a.offer(t);
            c();
        }
    }

    public void onSubscribe(Disposable disposable) {
        if (this.f || this.e) {
            disposable.dispose();
        }
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        if (this.h.get() || !this.h.compareAndSet(false, true)) {
            EmptyDisposable.a((Throwable) new IllegalStateException("Only a single observer allowed."), (Observer<?>) observer);
            return;
        }
        observer.onSubscribe(this.i);
        this.b.lazySet(observer);
        if (this.e) {
            this.b.lazySet((Object) null);
        } else {
            c();
        }
    }

    public static <T> UnicastSubject<T> a(int i2, Runnable runnable) {
        return new UnicastSubject<>(i2, runnable, true);
    }

    /* access modifiers changed from: package-private */
    public void a(Observer<? super T> observer) {
        SpscLinkedArrayQueue<T> spscLinkedArrayQueue = this.f6900a;
        int i2 = 1;
        boolean z = !this.d;
        while (!this.e) {
            boolean z2 = this.f;
            if (!z || !z2 || !a(spscLinkedArrayQueue, observer)) {
                observer.onNext(null);
                if (z2) {
                    c(observer);
                    return;
                }
                i2 = this.i.addAndGet(-i2);
                if (i2 == 0) {
                    return;
                }
            } else {
                return;
            }
        }
        this.b.lazySet((Object) null);
        spscLinkedArrayQueue.clear();
    }

    /* access modifiers changed from: package-private */
    public void b(Observer<? super T> observer) {
        SpscLinkedArrayQueue<T> spscLinkedArrayQueue = this.f6900a;
        boolean z = !this.d;
        boolean z2 = true;
        int i2 = 1;
        while (!this.e) {
            boolean z3 = this.f;
            T poll = this.f6900a.poll();
            boolean z4 = poll == null;
            if (z3) {
                if (z && z2) {
                    if (!a(spscLinkedArrayQueue, observer)) {
                        z2 = false;
                    } else {
                        return;
                    }
                }
                if (z4) {
                    c(observer);
                    return;
                }
            }
            if (z4) {
                i2 = this.i.addAndGet(-i2);
                if (i2 == 0) {
                    return;
                }
            } else {
                observer.onNext(poll);
            }
        }
        this.b.lazySet((Object) null);
        spscLinkedArrayQueue.clear();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.i.getAndIncrement() == 0) {
            Observer observer = this.b.get();
            int i2 = 1;
            while (observer == null) {
                i2 = this.i.addAndGet(-i2);
                if (i2 != 0) {
                    observer = this.b.get();
                } else {
                    return;
                }
            }
            if (this.j) {
                a(observer);
            } else {
                b(observer);
            }
        }
    }

    UnicastSubject(int i2, Runnable runnable, boolean z) {
        ObjectHelper.a(i2, "capacityHint");
        this.f6900a = new SpscLinkedArrayQueue<>(i2);
        ObjectHelper.a(runnable, "onTerminate");
        this.c = new AtomicReference<>(runnable);
        this.d = z;
        this.b = new AtomicReference<>();
        this.h = new AtomicBoolean();
        this.i = new UnicastQueueDisposable();
    }

    /* access modifiers changed from: package-private */
    public boolean a(SimpleQueue<T> simpleQueue, Observer<? super T> observer) {
        Throwable th = this.g;
        if (th == null) {
            return false;
        }
        this.b.lazySet((Object) null);
        simpleQueue.clear();
        observer.onError(th);
        return true;
    }
}
