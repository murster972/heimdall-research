package io.reactivex.subjects;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class PublishSubject<T> extends Subject<T> {
    static final PublishDisposable[] c = new PublishDisposable[0];
    static final PublishDisposable[] d = new PublishDisposable[0];

    /* renamed from: a  reason: collision with root package name */
    final AtomicReference<PublishDisposable<T>[]> f6898a = new AtomicReference<>(d);
    Throwable b;

    PublishSubject() {
    }

    public static <T> PublishSubject<T> b() {
        return new PublishSubject<>();
    }

    /* access modifiers changed from: package-private */
    public boolean a(PublishDisposable<T> publishDisposable) {
        PublishDisposable[] publishDisposableArr;
        PublishDisposable[] publishDisposableArr2;
        do {
            publishDisposableArr = (PublishDisposable[]) this.f6898a.get();
            if (publishDisposableArr == c) {
                return false;
            }
            int length = publishDisposableArr.length;
            publishDisposableArr2 = new PublishDisposable[(length + 1)];
            System.arraycopy(publishDisposableArr, 0, publishDisposableArr2, 0, length);
            publishDisposableArr2[length] = publishDisposable;
        } while (!this.f6898a.compareAndSet(publishDisposableArr, publishDisposableArr2));
        return true;
    }

    public void onComplete() {
        PublishDisposable<T>[] publishDisposableArr = this.f6898a.get();
        PublishDisposable<T>[] publishDisposableArr2 = c;
        if (publishDisposableArr != publishDisposableArr2) {
            for (PublishDisposable a2 : (PublishDisposable[]) this.f6898a.getAndSet(publishDisposableArr2)) {
                a2.a();
            }
        }
    }

    public void onError(Throwable th) {
        ObjectHelper.a(th, "onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        PublishDisposable<T>[] publishDisposableArr = this.f6898a.get();
        PublishDisposable<T>[] publishDisposableArr2 = c;
        if (publishDisposableArr == publishDisposableArr2) {
            RxJavaPlugins.b(th);
            return;
        }
        this.b = th;
        for (PublishDisposable a2 : (PublishDisposable[]) this.f6898a.getAndSet(publishDisposableArr2)) {
            a2.a(th);
        }
    }

    public void onNext(T t) {
        ObjectHelper.a(t, "onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
        for (PublishDisposable a2 : (PublishDisposable[]) this.f6898a.get()) {
            a2.a(t);
        }
    }

    public void onSubscribe(Disposable disposable) {
        if (this.f6898a.get() == c) {
            disposable.dispose();
        }
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        PublishDisposable publishDisposable = new PublishDisposable(observer, this);
        observer.onSubscribe(publishDisposable);
        if (!a(publishDisposable)) {
            Throwable th = this.b;
            if (th != null) {
                observer.onError(th);
            } else {
                observer.onComplete();
            }
        } else if (publishDisposable.isDisposed()) {
            b(publishDisposable);
        }
    }

    static final class PublishDisposable<T> extends AtomicBoolean implements Disposable {
        private static final long serialVersionUID = 3562861878281475070L;
        final Observer<? super T> downstream;
        final PublishSubject<T> parent;

        PublishDisposable(Observer<? super T> observer, PublishSubject<T> publishSubject) {
            this.downstream = observer;
            this.parent = publishSubject;
        }

        public void a(T t) {
            if (!get()) {
                this.downstream.onNext(t);
            }
        }

        public void dispose() {
            if (compareAndSet(false, true)) {
                this.parent.b(this);
            }
        }

        public boolean isDisposed() {
            return get();
        }

        public void a(Throwable th) {
            if (get()) {
                RxJavaPlugins.b(th);
            } else {
                this.downstream.onError(th);
            }
        }

        public void a() {
            if (!get()) {
                this.downstream.onComplete();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(PublishDisposable<T> publishDisposable) {
        PublishDisposable<T>[] publishDisposableArr;
        PublishDisposable[] publishDisposableArr2;
        do {
            publishDisposableArr = (PublishDisposable[]) this.f6898a.get();
            if (publishDisposableArr != c && publishDisposableArr != d) {
                int length = publishDisposableArr.length;
                int i = -1;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    } else if (publishDisposableArr[i2] == publishDisposable) {
                        i = i2;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (i >= 0) {
                    if (length == 1) {
                        publishDisposableArr2 = d;
                    } else {
                        PublishDisposable[] publishDisposableArr3 = new PublishDisposable[(length - 1)];
                        System.arraycopy(publishDisposableArr, 0, publishDisposableArr3, 0, i);
                        System.arraycopy(publishDisposableArr, i + 1, publishDisposableArr3, i, (length - i) - 1);
                        publishDisposableArr2 = publishDisposableArr3;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } while (!this.f6898a.compareAndSet(publishDisposableArr, publishDisposableArr2));
    }
}
