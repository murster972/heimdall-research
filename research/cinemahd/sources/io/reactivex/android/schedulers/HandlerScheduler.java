package io.reactivex.android.schedulers;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeUnit;

final class HandlerScheduler extends Scheduler {
    private final Handler b;
    private final boolean c;

    private static final class HandlerWorker extends Scheduler.Worker {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f6638a;
        private final boolean b;
        private volatile boolean c;

        HandlerWorker(Handler handler, boolean z) {
            this.f6638a = handler;
            this.b = z;
        }

        @SuppressLint({"NewApi"})
        public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
            if (runnable == null) {
                throw new NullPointerException("run == null");
            } else if (timeUnit == null) {
                throw new NullPointerException("unit == null");
            } else if (this.c) {
                return Disposables.a();
            } else {
                ScheduledRunnable scheduledRunnable = new ScheduledRunnable(this.f6638a, RxJavaPlugins.a(runnable));
                Message obtain = Message.obtain(this.f6638a, scheduledRunnable);
                obtain.obj = this;
                if (this.b) {
                    obtain.setAsynchronous(true);
                }
                this.f6638a.sendMessageDelayed(obtain, timeUnit.toMillis(j));
                if (!this.c) {
                    return scheduledRunnable;
                }
                this.f6638a.removeCallbacks(scheduledRunnable);
                return Disposables.a();
            }
        }

        public void dispose() {
            this.c = true;
            this.f6638a.removeCallbacksAndMessages(this);
        }

        public boolean isDisposed() {
            return this.c;
        }
    }

    private static final class ScheduledRunnable implements Runnable, Disposable {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f6639a;
        private final Runnable b;
        private volatile boolean c;

        ScheduledRunnable(Handler handler, Runnable runnable) {
            this.f6639a = handler;
            this.b = runnable;
        }

        public void dispose() {
            this.f6639a.removeCallbacks(this);
            this.c = true;
        }

        public boolean isDisposed() {
            return this.c;
        }

        public void run() {
            try {
                this.b.run();
            } catch (Throwable th) {
                RxJavaPlugins.b(th);
            }
        }
    }

    HandlerScheduler(Handler handler, boolean z) {
        this.b = handler;
        this.c = z;
    }

    @SuppressLint({"NewApi"})
    public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
        if (runnable == null) {
            throw new NullPointerException("run == null");
        } else if (timeUnit != null) {
            ScheduledRunnable scheduledRunnable = new ScheduledRunnable(this.b, RxJavaPlugins.a(runnable));
            Message obtain = Message.obtain(this.b, scheduledRunnable);
            if (this.c) {
                obtain.setAsynchronous(true);
            }
            this.b.sendMessageDelayed(obtain, timeUnit.toMillis(j));
            return scheduledRunnable;
        } else {
            throw new NullPointerException("unit == null");
        }
    }

    public Scheduler.Worker a() {
        return new HandlerWorker(this.b, this.c);
    }
}
