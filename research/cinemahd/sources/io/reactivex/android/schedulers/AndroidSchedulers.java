package io.reactivex.android.schedulers;

import android.os.Handler;
import android.os.Looper;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import java.util.concurrent.Callable;

public final class AndroidSchedulers {

    /* renamed from: a  reason: collision with root package name */
    private static final Scheduler f6636a = RxAndroidPlugins.b(new Callable<Scheduler>() {
        public Scheduler call() throws Exception {
            return MainHolder.f6637a;
        }
    });

    private static final class MainHolder {

        /* renamed from: a  reason: collision with root package name */
        static final Scheduler f6637a = new HandlerScheduler(new Handler(Looper.getMainLooper()), false);

        private MainHolder() {
        }
    }

    private AndroidSchedulers() {
        throw new AssertionError("No instances.");
    }

    public static Scheduler a() {
        return RxAndroidPlugins.a(f6636a);
    }
}
