package io.reactivex.android.plugins;

import io.reactivex.Scheduler;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import java.util.concurrent.Callable;

public final class RxAndroidPlugins {

    /* renamed from: a  reason: collision with root package name */
    private static volatile Function<Callable<Scheduler>, Scheduler> f6635a;
    private static volatile Function<Scheduler, Scheduler> b;

    private RxAndroidPlugins() {
        throw new AssertionError("No instances.");
    }

    public static Scheduler a(Scheduler scheduler) {
        if (scheduler != null) {
            Function function = b;
            if (function == null) {
                return scheduler;
            }
            return (Scheduler) a(function, scheduler);
        }
        throw new NullPointerException("scheduler == null");
    }

    public static Scheduler b(Callable<Scheduler> callable) {
        if (callable != null) {
            Function<Callable<Scheduler>, Scheduler> function = f6635a;
            if (function == null) {
                return a(callable);
            }
            return a(function, callable);
        }
        throw new NullPointerException("scheduler == null");
    }

    static Scheduler a(Callable<Scheduler> callable) {
        try {
            Scheduler call = callable.call();
            if (call != null) {
                return call;
            }
            throw new NullPointerException("Scheduler Callable returned null");
        } catch (Throwable th) {
            Exceptions.a(th);
            throw null;
        }
    }

    /* JADX WARNING: type inference failed for: r0v0, types: [io.reactivex.functions.Function, io.reactivex.functions.Function<java.util.concurrent.Callable<io.reactivex.Scheduler>, io.reactivex.Scheduler>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static io.reactivex.Scheduler a(io.reactivex.functions.Function<java.util.concurrent.Callable<io.reactivex.Scheduler>, io.reactivex.Scheduler> r0, java.util.concurrent.Callable<io.reactivex.Scheduler> r1) {
        /*
            java.lang.Object r0 = a(r0, r1)
            io.reactivex.Scheduler r0 = (io.reactivex.Scheduler) r0
            if (r0 == 0) goto L_0x0009
            return r0
        L_0x0009:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "Scheduler Callable returned null"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: io.reactivex.android.plugins.RxAndroidPlugins.a(io.reactivex.functions.Function, java.util.concurrent.Callable):io.reactivex.Scheduler");
    }

    static <T, R> R a(Function<T, R> function, T t) {
        try {
            return function.apply(t);
        } catch (Throwable th) {
            Exceptions.a(th);
            throw null;
        }
    }
}
