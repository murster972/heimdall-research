package io.reactivex.disposables;

import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;

public final class Disposables {
    private Disposables() {
        throw new IllegalStateException("No instances!");
    }

    public static Disposable a(Runnable runnable) {
        ObjectHelper.a(runnable, "run is null");
        return new RunnableDisposable(runnable);
    }

    public static Disposable a() {
        return EmptyDisposable.INSTANCE;
    }
}
