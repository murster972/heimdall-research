package io.reactivex.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.util.EndConsumerHelper;

public abstract class DefaultObserver<T> implements Observer<T> {

    /* renamed from: a  reason: collision with root package name */
    private Disposable f6884a;

    /* access modifiers changed from: protected */
    public void a() {
    }

    public final void onSubscribe(Disposable disposable) {
        if (EndConsumerHelper.a(this.f6884a, disposable, getClass())) {
            this.f6884a = disposable;
            a();
        }
    }
}
