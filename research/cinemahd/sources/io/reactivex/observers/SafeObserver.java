package io.reactivex.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class SafeObserver<T> implements Observer<T>, Disposable {

    /* renamed from: a  reason: collision with root package name */
    final Observer<? super T> f6886a;
    Disposable b;
    boolean c;

    public SafeObserver(Observer<? super T> observer) {
        this.f6886a = observer;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        NullPointerException nullPointerException = new NullPointerException("Subscription not set!");
        try {
            this.f6886a.onSubscribe(EmptyDisposable.INSTANCE);
            try {
                this.f6886a.onError(nullPointerException);
            } catch (Throwable th) {
                Exceptions.b(th);
                RxJavaPlugins.b((Throwable) new CompositeException(nullPointerException, th));
            }
        } catch (Throwable th2) {
            Exceptions.b(th2);
            RxJavaPlugins.b((Throwable) new CompositeException(nullPointerException, th2));
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.c = true;
        NullPointerException nullPointerException = new NullPointerException("Subscription not set!");
        try {
            this.f6886a.onSubscribe(EmptyDisposable.INSTANCE);
            try {
                this.f6886a.onError(nullPointerException);
            } catch (Throwable th) {
                Exceptions.b(th);
                RxJavaPlugins.b((Throwable) new CompositeException(nullPointerException, th));
            }
        } catch (Throwable th2) {
            Exceptions.b(th2);
            RxJavaPlugins.b((Throwable) new CompositeException(nullPointerException, th2));
        }
    }

    public void dispose() {
        this.b.dispose();
    }

    public boolean isDisposed() {
        return this.b.isDisposed();
    }

    public void onComplete() {
        if (!this.c) {
            this.c = true;
            if (this.b == null) {
                a();
                return;
            }
            try {
                this.f6886a.onComplete();
            } catch (Throwable th) {
                Exceptions.b(th);
                RxJavaPlugins.b(th);
            }
        }
    }

    public void onError(Throwable th) {
        if (this.c) {
            RxJavaPlugins.b(th);
            return;
        }
        this.c = true;
        if (this.b == null) {
            NullPointerException nullPointerException = new NullPointerException("Subscription not set!");
            try {
                this.f6886a.onSubscribe(EmptyDisposable.INSTANCE);
                try {
                    this.f6886a.onError(new CompositeException(th, nullPointerException));
                } catch (Throwable th2) {
                    Exceptions.b(th2);
                    RxJavaPlugins.b((Throwable) new CompositeException(th, nullPointerException, th2));
                }
            } catch (Throwable th3) {
                Exceptions.b(th3);
                RxJavaPlugins.b((Throwable) new CompositeException(th, nullPointerException, th3));
            }
        } else {
            if (th == null) {
                th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
            }
            try {
                this.f6886a.onError(th);
            } catch (Throwable th4) {
                Exceptions.b(th4);
                RxJavaPlugins.b((Throwable) new CompositeException(th, th4));
            }
        }
    }

    public void onNext(T t) {
        if (!this.c) {
            if (this.b == null) {
                b();
            } else if (t == null) {
                NullPointerException nullPointerException = new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
                try {
                    this.b.dispose();
                    onError(nullPointerException);
                } catch (Throwable th) {
                    Exceptions.b(th);
                    onError(new CompositeException(nullPointerException, th));
                }
            } else {
                try {
                    this.f6886a.onNext(t);
                } catch (Throwable th2) {
                    Exceptions.b(th2);
                    onError(new CompositeException(th, th2));
                }
            }
        }
    }

    public void onSubscribe(Disposable disposable) {
        if (DisposableHelper.a(this.b, disposable)) {
            this.b = disposable;
            try {
                this.f6886a.onSubscribe(this);
            } catch (Throwable th) {
                Exceptions.b(th);
                RxJavaPlugins.b((Throwable) new CompositeException(th, th));
            }
        }
    }
}
