package io.reactivex.observers;

import io.reactivex.disposables.Disposable;
import io.reactivex.internal.util.VolatileSizeArrayList;
import io.reactivex.observers.BaseTestConsumer;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public abstract class BaseTestConsumer<T, U extends BaseTestConsumer<T, U>> implements Disposable {

    /* renamed from: a  reason: collision with root package name */
    protected final CountDownLatch f6883a = new CountDownLatch(1);
    protected final List<T> b = new VolatileSizeArrayList();
    protected final List<Throwable> c = new VolatileSizeArrayList();
    protected long d;
    protected boolean e;
    protected int f;
    protected int g;
}
