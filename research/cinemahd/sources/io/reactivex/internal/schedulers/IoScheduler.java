package io.reactivex.internal.schedulers;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.EmptyDisposable;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class IoScheduler extends Scheduler {
    static final RxThreadFactory d;
    static final RxThreadFactory e;
    private static final long f = Long.getLong("rx2.io-keep-alive-time", 60).longValue();
    private static final TimeUnit g = TimeUnit.SECONDS;
    static final ThreadWorker h = new ThreadWorker(new RxThreadFactory("RxCachedThreadSchedulerShutdown"));
    static final CachedWorkerPool i = new CachedWorkerPool(0, (TimeUnit) null, d);
    final ThreadFactory b;
    final AtomicReference<CachedWorkerPool> c;

    static final class EventLoopWorker extends Scheduler.Worker {

        /* renamed from: a  reason: collision with root package name */
        private final CompositeDisposable f6863a;
        private final CachedWorkerPool b;
        private final ThreadWorker c;
        final AtomicBoolean d = new AtomicBoolean();

        EventLoopWorker(CachedWorkerPool cachedWorkerPool) {
            this.b = cachedWorkerPool;
            this.f6863a = new CompositeDisposable();
            this.c = cachedWorkerPool.b();
        }

        public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
            if (this.f6863a.isDisposed()) {
                return EmptyDisposable.INSTANCE;
            }
            return this.c.a(runnable, j, timeUnit, this.f6863a);
        }

        public void dispose() {
            if (this.d.compareAndSet(false, true)) {
                this.f6863a.dispose();
                this.b.a(this.c);
            }
        }

        public boolean isDisposed() {
            return this.d.get();
        }
    }

    static final class ThreadWorker extends NewThreadWorker {
        private long c = 0;

        ThreadWorker(ThreadFactory threadFactory) {
            super(threadFactory);
        }

        public void a(long j) {
            this.c = j;
        }

        public long b() {
            return this.c;
        }
    }

    static {
        h.dispose();
        int max = Math.max(1, Math.min(10, Integer.getInteger("rx2.io-priority", 5).intValue()));
        d = new RxThreadFactory("RxCachedThreadScheduler", max);
        e = new RxThreadFactory("RxCachedWorkerPoolEvictor", max);
        i.d();
    }

    public IoScheduler() {
        this(d);
    }

    public Scheduler.Worker a() {
        return new EventLoopWorker(this.c.get());
    }

    public void b() {
        CachedWorkerPool cachedWorkerPool = new CachedWorkerPool(f, g, this.b);
        if (!this.c.compareAndSet(i, cachedWorkerPool)) {
            cachedWorkerPool.d();
        }
    }

    static final class CachedWorkerPool implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final long f6862a;
        private final ConcurrentLinkedQueue<ThreadWorker> b;
        final CompositeDisposable c;
        private final ScheduledExecutorService d;
        private final Future<?> e;
        private final ThreadFactory f;

        CachedWorkerPool(long j, TimeUnit timeUnit, ThreadFactory threadFactory) {
            ScheduledFuture<?> scheduledFuture;
            this.f6862a = timeUnit != null ? timeUnit.toNanos(j) : 0;
            this.b = new ConcurrentLinkedQueue<>();
            this.c = new CompositeDisposable();
            this.f = threadFactory;
            ScheduledExecutorService scheduledExecutorService = null;
            if (timeUnit != null) {
                scheduledExecutorService = Executors.newScheduledThreadPool(1, IoScheduler.e);
                long j2 = this.f6862a;
                scheduledFuture = scheduledExecutorService.scheduleWithFixedDelay(this, j2, j2, TimeUnit.NANOSECONDS);
            } else {
                scheduledFuture = null;
            }
            this.d = scheduledExecutorService;
            this.e = scheduledFuture;
        }

        /* access modifiers changed from: package-private */
        public void a(ThreadWorker threadWorker) {
            threadWorker.a(c() + this.f6862a);
            this.b.offer(threadWorker);
        }

        /* access modifiers changed from: package-private */
        public ThreadWorker b() {
            if (this.c.isDisposed()) {
                return IoScheduler.h;
            }
            while (!this.b.isEmpty()) {
                ThreadWorker poll = this.b.poll();
                if (poll != null) {
                    return poll;
                }
            }
            ThreadWorker threadWorker = new ThreadWorker(this.f);
            this.c.b(threadWorker);
            return threadWorker;
        }

        /* access modifiers changed from: package-private */
        public long c() {
            return System.nanoTime();
        }

        /* access modifiers changed from: package-private */
        public void d() {
            this.c.dispose();
            Future<?> future = this.e;
            if (future != null) {
                future.cancel(true);
            }
            ScheduledExecutorService scheduledExecutorService = this.d;
            if (scheduledExecutorService != null) {
                scheduledExecutorService.shutdownNow();
            }
        }

        public void run() {
            a();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (!this.b.isEmpty()) {
                long c2 = c();
                Iterator<ThreadWorker> it2 = this.b.iterator();
                while (it2.hasNext()) {
                    ThreadWorker next = it2.next();
                    if (next.b() > c2) {
                        return;
                    }
                    if (this.b.remove(next)) {
                        this.c.a((Disposable) next);
                    }
                }
            }
        }
    }

    public IoScheduler(ThreadFactory threadFactory) {
        this.b = threadFactory;
        this.c = new AtomicReference<>(i);
        b();
    }
}
