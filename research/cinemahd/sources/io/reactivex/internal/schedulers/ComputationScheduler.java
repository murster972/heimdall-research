package io.reactivex.internal.schedulers;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.disposables.ListCompositeDisposable;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class ComputationScheduler extends Scheduler implements SchedulerMultiWorkerSupport {
    static final FixedSchedulerPool d = new FixedSchedulerPool(0, e);
    static final RxThreadFactory e = new RxThreadFactory("RxComputationThreadPool", Math.max(1, Math.min(10, Integer.getInteger("rx2.computation-priority", 5).intValue())), true);
    static final int f = a(Runtime.getRuntime().availableProcessors(), Integer.getInteger("rx2.computation-threads", 0).intValue());
    static final PoolWorker g = new PoolWorker(new RxThreadFactory("RxComputationShutdown"));
    final ThreadFactory b;
    final AtomicReference<FixedSchedulerPool> c;

    static final class FixedSchedulerPool implements SchedulerMultiWorkerSupport {

        /* renamed from: a  reason: collision with root package name */
        final int f6860a;
        final PoolWorker[] b;
        long c;

        FixedSchedulerPool(int i, ThreadFactory threadFactory) {
            this.f6860a = i;
            this.b = new PoolWorker[i];
            for (int i2 = 0; i2 < i; i2++) {
                this.b[i2] = new PoolWorker(threadFactory);
            }
        }

        public PoolWorker a() {
            int i = this.f6860a;
            if (i == 0) {
                return ComputationScheduler.g;
            }
            PoolWorker[] poolWorkerArr = this.b;
            long j = this.c;
            this.c = 1 + j;
            return poolWorkerArr[(int) (j % ((long) i))];
        }

        public void b() {
            for (PoolWorker dispose : this.b) {
                dispose.dispose();
            }
        }
    }

    static final class PoolWorker extends NewThreadWorker {
        PoolWorker(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }

    static {
        g.dispose();
        d.b();
    }

    public ComputationScheduler() {
        this(e);
    }

    static int a(int i, int i2) {
        return (i2 <= 0 || i2 > i) ? i : i2;
    }

    public Scheduler.Worker a() {
        return new EventLoopWorker(this.c.get().a());
    }

    public void b() {
        FixedSchedulerPool fixedSchedulerPool = new FixedSchedulerPool(f, this.b);
        if (!this.c.compareAndSet(d, fixedSchedulerPool)) {
            fixedSchedulerPool.b();
        }
    }

    public ComputationScheduler(ThreadFactory threadFactory) {
        this.b = threadFactory;
        this.c = new AtomicReference<>(d);
        b();
    }

    public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
        return this.c.get().a().b(runnable, j, timeUnit);
    }

    static final class EventLoopWorker extends Scheduler.Worker {

        /* renamed from: a  reason: collision with root package name */
        private final ListCompositeDisposable f6859a = new ListCompositeDisposable();
        private final CompositeDisposable b = new CompositeDisposable();
        private final ListCompositeDisposable c = new ListCompositeDisposable();
        private final PoolWorker d;
        volatile boolean e;

        EventLoopWorker(PoolWorker poolWorker) {
            this.d = poolWorker;
            this.c.b(this.f6859a);
            this.c.b(this.b);
        }

        public Disposable a(Runnable runnable) {
            if (this.e) {
                return EmptyDisposable.INSTANCE;
            }
            return this.d.a(runnable, 0, TimeUnit.MILLISECONDS, this.f6859a);
        }

        public void dispose() {
            if (!this.e) {
                this.e = true;
                this.c.dispose();
            }
        }

        public boolean isDisposed() {
            return this.e;
        }

        public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
            if (this.e) {
                return EmptyDisposable.INSTANCE;
            }
            return this.d.a(runnable, j, timeUnit, this.b);
        }
    }

    public Disposable a(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        return this.c.get().a().b(runnable, j, j2, timeUnit);
    }
}
