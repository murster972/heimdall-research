package io.reactivex.internal.schedulers;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class SingleScheduler extends Scheduler {
    static final RxThreadFactory c = new RxThreadFactory("RxSingleScheduler", Math.max(1, Math.min(10, Integer.getInteger("rx2.single-priority", 5).intValue())), true);
    static final ScheduledExecutorService d = Executors.newScheduledThreadPool(0);
    final AtomicReference<ScheduledExecutorService> b;

    static final class ScheduledWorker extends Scheduler.Worker {

        /* renamed from: a  reason: collision with root package name */
        final ScheduledExecutorService f6868a;
        final CompositeDisposable b = new CompositeDisposable();
        volatile boolean c;

        ScheduledWorker(ScheduledExecutorService scheduledExecutorService) {
            this.f6868a = scheduledExecutorService;
        }

        public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
            Future future;
            if (this.c) {
                return EmptyDisposable.INSTANCE;
            }
            ScheduledRunnable scheduledRunnable = new ScheduledRunnable(RxJavaPlugins.a(runnable), this.b);
            this.b.b(scheduledRunnable);
            if (j <= 0) {
                try {
                    future = this.f6868a.submit(scheduledRunnable);
                } catch (RejectedExecutionException e) {
                    dispose();
                    RxJavaPlugins.b((Throwable) e);
                    return EmptyDisposable.INSTANCE;
                }
            } else {
                future = this.f6868a.schedule(scheduledRunnable, j, timeUnit);
            }
            scheduledRunnable.a(future);
            return scheduledRunnable;
        }

        public void dispose() {
            if (!this.c) {
                this.c = true;
                this.b.dispose();
            }
        }

        public boolean isDisposed() {
            return this.c;
        }
    }

    static {
        d.shutdown();
    }

    public SingleScheduler() {
        this(c);
    }

    static ScheduledExecutorService a(ThreadFactory threadFactory) {
        return SchedulerPoolFactory.a(threadFactory);
    }

    public SingleScheduler(ThreadFactory threadFactory) {
        this.b = new AtomicReference<>();
        this.b.lazySet(a(threadFactory));
    }

    public Scheduler.Worker a() {
        return new ScheduledWorker(this.b.get());
    }

    public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
        Future future;
        ScheduledDirectTask scheduledDirectTask = new ScheduledDirectTask(RxJavaPlugins.a(runnable));
        if (j <= 0) {
            try {
                future = this.b.get().submit(scheduledDirectTask);
            } catch (RejectedExecutionException e) {
                RxJavaPlugins.b((Throwable) e);
                return EmptyDisposable.INSTANCE;
            }
        } else {
            future = this.b.get().schedule(scheduledDirectTask, j, timeUnit);
        }
        scheduledDirectTask.a(future);
        return scheduledDirectTask;
    }

    public Disposable a(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        Future future;
        Runnable a2 = RxJavaPlugins.a(runnable);
        if (j2 <= 0) {
            ScheduledExecutorService scheduledExecutorService = this.b.get();
            InstantPeriodicTask instantPeriodicTask = new InstantPeriodicTask(a2, scheduledExecutorService);
            if (j <= 0) {
                try {
                    future = scheduledExecutorService.submit(instantPeriodicTask);
                } catch (RejectedExecutionException e) {
                    RxJavaPlugins.b((Throwable) e);
                    return EmptyDisposable.INSTANCE;
                }
            } else {
                future = scheduledExecutorService.schedule(instantPeriodicTask, j, timeUnit);
            }
            instantPeriodicTask.a(future);
            return instantPeriodicTask;
        }
        ScheduledDirectPeriodicTask scheduledDirectPeriodicTask = new ScheduledDirectPeriodicTask(a2);
        try {
            scheduledDirectPeriodicTask.a(this.b.get().scheduleAtFixedRate(scheduledDirectPeriodicTask, j, j2, timeUnit));
            return scheduledDirectPeriodicTask;
        } catch (RejectedExecutionException e2) {
            RxJavaPlugins.b((Throwable) e2);
            return EmptyDisposable.INSTANCE;
        }
    }
}
