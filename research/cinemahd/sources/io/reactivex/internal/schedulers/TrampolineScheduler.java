package io.reactivex.internal.schedulers;

import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class TrampolineScheduler extends Scheduler {
    private static final TrampolineScheduler b = new TrampolineScheduler();

    static final class SleepingRunnable implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Runnable f6869a;
        private final TrampolineWorker b;
        private final long c;

        SleepingRunnable(Runnable runnable, TrampolineWorker trampolineWorker, long j) {
            this.f6869a = runnable;
            this.b = trampolineWorker;
            this.c = j;
        }

        public void run() {
            if (!this.b.d) {
                long a2 = this.b.a(TimeUnit.MILLISECONDS);
                long j = this.c;
                if (j > a2) {
                    try {
                        Thread.sleep(j - a2);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        RxJavaPlugins.b((Throwable) e);
                        return;
                    }
                }
                if (!this.b.d) {
                    this.f6869a.run();
                }
            }
        }
    }

    static final class TimedRunnable implements Comparable<TimedRunnable> {

        /* renamed from: a  reason: collision with root package name */
        final Runnable f6870a;
        final long b;
        final int c;
        volatile boolean d;

        TimedRunnable(Runnable runnable, Long l, int i) {
            this.f6870a = runnable;
            this.b = l.longValue();
            this.c = i;
        }

        /* renamed from: a */
        public int compareTo(TimedRunnable timedRunnable) {
            int a2 = ObjectHelper.a(this.b, timedRunnable.b);
            return a2 == 0 ? ObjectHelper.a(this.c, timedRunnable.c) : a2;
        }
    }

    static final class TrampolineWorker extends Scheduler.Worker implements Disposable {

        /* renamed from: a  reason: collision with root package name */
        final PriorityBlockingQueue<TimedRunnable> f6871a = new PriorityBlockingQueue<>();
        private final AtomicInteger b = new AtomicInteger();
        final AtomicInteger c = new AtomicInteger();
        volatile boolean d;

        final class AppendToQueueTask implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final TimedRunnable f6872a;

            AppendToQueueTask(TimedRunnable timedRunnable) {
                this.f6872a = timedRunnable;
            }

            public void run() {
                TimedRunnable timedRunnable = this.f6872a;
                timedRunnable.d = true;
                TrampolineWorker.this.f6871a.remove(timedRunnable);
            }
        }

        TrampolineWorker() {
        }

        public Disposable a(Runnable runnable) {
            return a(runnable, a(TimeUnit.MILLISECONDS));
        }

        public void dispose() {
            this.d = true;
        }

        public boolean isDisposed() {
            return this.d;
        }

        public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
            long a2 = a(TimeUnit.MILLISECONDS) + timeUnit.toMillis(j);
            return a(new SleepingRunnable(runnable, this, a2), a2);
        }

        /* access modifiers changed from: package-private */
        public Disposable a(Runnable runnable, long j) {
            if (this.d) {
                return EmptyDisposable.INSTANCE;
            }
            TimedRunnable timedRunnable = new TimedRunnable(runnable, Long.valueOf(j), this.c.incrementAndGet());
            this.f6871a.add(timedRunnable);
            if (this.b.getAndIncrement() != 0) {
                return Disposables.a(new AppendToQueueTask(timedRunnable));
            }
            int i = 1;
            while (!this.d) {
                TimedRunnable poll = this.f6871a.poll();
                if (poll == null) {
                    i = this.b.addAndGet(-i);
                    if (i == 0) {
                        return EmptyDisposable.INSTANCE;
                    }
                } else if (!poll.d) {
                    poll.f6870a.run();
                }
            }
            this.f6871a.clear();
            return EmptyDisposable.INSTANCE;
        }
    }

    TrampolineScheduler() {
    }

    public static TrampolineScheduler b() {
        return b;
    }

    public Scheduler.Worker a() {
        return new TrampolineWorker();
    }

    public Disposable a(Runnable runnable) {
        RxJavaPlugins.a(runnable).run();
        return EmptyDisposable.INSTANCE;
    }

    public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
        try {
            timeUnit.sleep(j);
            RxJavaPlugins.a(runnable).run();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            RxJavaPlugins.b((Throwable) e);
        }
        return EmptyDisposable.INSTANCE;
    }
}
