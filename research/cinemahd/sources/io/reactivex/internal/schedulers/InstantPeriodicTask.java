package io.reactivex.internal.schedulers;

import io.reactivex.disposables.Disposable;
import io.reactivex.internal.functions.Functions;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;

final class InstantPeriodicTask implements Callable<Void>, Disposable {
    static final FutureTask<Void> f = new FutureTask<>(Functions.b, (Object) null);

    /* renamed from: a  reason: collision with root package name */
    final Runnable f6861a;
    final AtomicReference<Future<?>> b = new AtomicReference<>();
    final AtomicReference<Future<?>> c = new AtomicReference<>();
    final ExecutorService d;
    Thread e;

    InstantPeriodicTask(Runnable runnable, ExecutorService executorService) {
        this.f6861a = runnable;
        this.d = executorService;
    }

    /* access modifiers changed from: package-private */
    public void a(Future<?> future) {
        Future future2;
        do {
            future2 = this.c.get();
            if (future2 == f) {
                future.cancel(this.e != Thread.currentThread());
                return;
            }
        } while (!this.c.compareAndSet(future2, future));
    }

    /* access modifiers changed from: package-private */
    public void b(Future<?> future) {
        Future future2;
        do {
            future2 = this.b.get();
            if (future2 == f) {
                future.cancel(this.e != Thread.currentThread());
                return;
            }
        } while (!this.b.compareAndSet(future2, future));
    }

    public void dispose() {
        Future andSet = this.c.getAndSet(f);
        boolean z = true;
        if (!(andSet == null || andSet == f)) {
            andSet.cancel(this.e != Thread.currentThread());
        }
        Future andSet2 = this.b.getAndSet(f);
        if (andSet2 != null && andSet2 != f) {
            if (this.e == Thread.currentThread()) {
                z = false;
            }
            andSet2.cancel(z);
        }
    }

    public boolean isDisposed() {
        return this.c.get() == f;
    }

    public Void call() throws Exception {
        this.e = Thread.currentThread();
        try {
            this.f6861a.run();
            b(this.d.submit(this));
            this.e = null;
        } catch (Throwable th) {
            this.e = null;
            RxJavaPlugins.b(th);
        }
        return null;
    }
}
