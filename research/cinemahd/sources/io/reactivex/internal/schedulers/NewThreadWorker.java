package io.reactivex.internal.schedulers;

import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableContainer;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class NewThreadWorker extends Scheduler.Worker implements Disposable {

    /* renamed from: a  reason: collision with root package name */
    private final ScheduledExecutorService f6864a;
    volatile boolean b;

    public NewThreadWorker(ThreadFactory threadFactory) {
        this.f6864a = SchedulerPoolFactory.a(threadFactory);
    }

    public Disposable a(Runnable runnable) {
        return a(runnable, 0, (TimeUnit) null);
    }

    public Disposable b(Runnable runnable, long j, TimeUnit timeUnit) {
        Future future;
        ScheduledDirectTask scheduledDirectTask = new ScheduledDirectTask(RxJavaPlugins.a(runnable));
        if (j <= 0) {
            try {
                future = this.f6864a.submit(scheduledDirectTask);
            } catch (RejectedExecutionException e) {
                RxJavaPlugins.b((Throwable) e);
                return EmptyDisposable.INSTANCE;
            }
        } else {
            future = this.f6864a.schedule(scheduledDirectTask, j, timeUnit);
        }
        scheduledDirectTask.a(future);
        return scheduledDirectTask;
    }

    public void dispose() {
        if (!this.b) {
            this.b = true;
            this.f6864a.shutdownNow();
        }
    }

    public boolean isDisposed() {
        return this.b;
    }

    public Disposable a(Runnable runnable, long j, TimeUnit timeUnit) {
        if (this.b) {
            return EmptyDisposable.INSTANCE;
        }
        return a(runnable, j, timeUnit, (DisposableContainer) null);
    }

    public ScheduledRunnable a(Runnable runnable, long j, TimeUnit timeUnit, DisposableContainer disposableContainer) {
        Future future;
        ScheduledRunnable scheduledRunnable = new ScheduledRunnable(RxJavaPlugins.a(runnable), disposableContainer);
        if (disposableContainer != null && !disposableContainer.b(scheduledRunnable)) {
            return scheduledRunnable;
        }
        if (j <= 0) {
            try {
                future = this.f6864a.submit(scheduledRunnable);
            } catch (RejectedExecutionException e) {
                if (disposableContainer != null) {
                    disposableContainer.a(scheduledRunnable);
                }
                RxJavaPlugins.b((Throwable) e);
            }
        } else {
            future = this.f6864a.schedule(scheduledRunnable, j, timeUnit);
        }
        scheduledRunnable.a(future);
        return scheduledRunnable;
    }

    public Disposable b(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        Future future;
        Runnable a2 = RxJavaPlugins.a(runnable);
        if (j2 <= 0) {
            InstantPeriodicTask instantPeriodicTask = new InstantPeriodicTask(a2, this.f6864a);
            if (j <= 0) {
                try {
                    future = this.f6864a.submit(instantPeriodicTask);
                } catch (RejectedExecutionException e) {
                    RxJavaPlugins.b((Throwable) e);
                    return EmptyDisposable.INSTANCE;
                }
            } else {
                future = this.f6864a.schedule(instantPeriodicTask, j, timeUnit);
            }
            instantPeriodicTask.a(future);
            return instantPeriodicTask;
        }
        ScheduledDirectPeriodicTask scheduledDirectPeriodicTask = new ScheduledDirectPeriodicTask(a2);
        try {
            scheduledDirectPeriodicTask.a(this.f6864a.scheduleAtFixedRate(scheduledDirectPeriodicTask, j, j2, timeUnit));
            return scheduledDirectPeriodicTask;
        } catch (RejectedExecutionException e2) {
            RxJavaPlugins.b((Throwable) e2);
            return EmptyDisposable.INSTANCE;
        }
    }

    public void a() {
        if (!this.b) {
            this.b = true;
            this.f6864a.shutdown();
        }
    }
}
