package io.reactivex.internal.util;

import io.reactivex.Observer;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Subscriber;

public final class HalfSerializer {
    private HalfSerializer() {
        throw new IllegalStateException("No instances!");
    }

    public static <T> void a(Subscriber<? super T> subscriber, T t, AtomicInteger atomicInteger, AtomicThrowable atomicThrowable) {
        if (atomicInteger.get() == 0 && atomicInteger.compareAndSet(0, 1)) {
            subscriber.onNext(t);
            if (atomicInteger.decrementAndGet() != 0) {
                Throwable a2 = atomicThrowable.a();
                if (a2 != null) {
                    subscriber.onError(a2);
                } else {
                    subscriber.onComplete();
                }
            }
        }
    }

    public static void a(Subscriber<?> subscriber, Throwable th, AtomicInteger atomicInteger, AtomicThrowable atomicThrowable) {
        if (!atomicThrowable.a(th)) {
            RxJavaPlugins.b(th);
        } else if (atomicInteger.getAndIncrement() == 0) {
            subscriber.onError(atomicThrowable.a());
        }
    }

    public static void a(Subscriber<?> subscriber, AtomicInteger atomicInteger, AtomicThrowable atomicThrowable) {
        if (atomicInteger.getAndIncrement() == 0) {
            Throwable a2 = atomicThrowable.a();
            if (a2 != null) {
                subscriber.onError(a2);
            } else {
                subscriber.onComplete();
            }
        }
    }

    public static <T> void a(Observer<? super T> observer, T t, AtomicInteger atomicInteger, AtomicThrowable atomicThrowable) {
        if (atomicInteger.get() == 0 && atomicInteger.compareAndSet(0, 1)) {
            observer.onNext(t);
            if (atomicInteger.decrementAndGet() != 0) {
                Throwable a2 = atomicThrowable.a();
                if (a2 != null) {
                    observer.onError(a2);
                } else {
                    observer.onComplete();
                }
            }
        }
    }

    public static void a(Observer<?> observer, Throwable th, AtomicInteger atomicInteger, AtomicThrowable atomicThrowable) {
        if (!atomicThrowable.a(th)) {
            RxJavaPlugins.b(th);
        } else if (atomicInteger.getAndIncrement() == 0) {
            observer.onError(atomicThrowable.a());
        }
    }

    public static void a(Observer<?> observer, AtomicInteger atomicInteger, AtomicThrowable atomicThrowable) {
        if (atomicInteger.getAndIncrement() == 0) {
            Throwable a2 = atomicThrowable.a();
            if (a2 != null) {
                observer.onError(a2);
            } else {
                observer.onComplete();
            }
        }
    }
}
