package io.reactivex.internal.util;

import io.reactivex.Observer;
import io.reactivex.functions.Predicate;

public class AppendOnlyLinkedArrayList<T> {

    /* renamed from: a  reason: collision with root package name */
    final int f6874a;
    final Object[] b;
    Object[] c = this.b;
    int d;

    public interface NonThrowingPredicate<T> extends Predicate<T> {
        boolean a(T t);
    }

    public AppendOnlyLinkedArrayList(int i) {
        this.f6874a = i;
        this.b = new Object[(i + 1)];
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: java.lang.Object[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(T r4) {
        /*
            r3 = this;
            int r0 = r3.f6874a
            int r1 = r3.d
            if (r1 != r0) goto L_0x0011
            int r1 = r0 + 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.Object[] r2 = r3.c
            r2[r0] = r1
            r3.c = r1
            r1 = 0
        L_0x0011:
            java.lang.Object[] r0 = r3.c
            r0[r1] = r4
            int r1 = r1 + 1
            r3.d = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: io.reactivex.internal.util.AppendOnlyLinkedArrayList.a(java.lang.Object):void");
    }

    public void b(T t) {
        this.b[0] = t;
    }

    public void a(NonThrowingPredicate<? super T> nonThrowingPredicate) {
        int i = this.f6874a;
        for (Object[] objArr = this.b; objArr != null; objArr = objArr[i]) {
            int i2 = 0;
            while (i2 < i) {
                Object[] objArr2 = objArr[i2];
                if (objArr2 == null) {
                    continue;
                    break;
                } else if (!nonThrowingPredicate.a(objArr2)) {
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    public <U> boolean a(Observer<? super U> observer) {
        Object[] objArr = this.b;
        int i = this.f6874a;
        while (true) {
            int i2 = 0;
            if (objArr == null) {
                return false;
            }
            while (i2 < i) {
                Object[] objArr2 = objArr[i2];
                if (objArr2 == null) {
                    continue;
                    break;
                } else if (NotificationLite.b(objArr2, observer)) {
                    return true;
                } else {
                    i2++;
                }
            }
            objArr = objArr[i];
        }
    }
}
