package io.reactivex.internal.util;

import com.facebook.common.time.Clock;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicLong;

public final class BackpressureHelper {
    private BackpressureHelper() {
        throw new IllegalStateException("No instances!");
    }

    public static long a(long j, long j2) {
        long j3 = j + j2;
        return j3 < 0 ? Clock.MAX_TIME : j3;
    }

    public static long a(AtomicLong atomicLong, long j) {
        long j2;
        do {
            j2 = atomicLong.get();
            if (j2 == Clock.MAX_TIME) {
                return Clock.MAX_TIME;
            }
        } while (!atomicLong.compareAndSet(j2, a(j2, j)));
        return j2;
    }

    public static long b(AtomicLong atomicLong, long j) {
        long j2;
        long j3;
        do {
            j2 = atomicLong.get();
            if (j2 == Clock.MAX_TIME) {
                return Clock.MAX_TIME;
            }
            j3 = j2 - j;
            if (j3 < 0) {
                RxJavaPlugins.b((Throwable) new IllegalStateException("More produced than requested: " + j3));
                j3 = 0;
            }
        } while (!atomicLong.compareAndSet(j2, j3));
        return j3;
    }
}
