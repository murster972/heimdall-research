package io.reactivex.internal.queue;

import io.reactivex.internal.fuseable.SimplePlainQueue;
import java.util.concurrent.atomic.AtomicReference;

public final class MpscLinkedQueue<T> implements SimplePlainQueue<T> {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicReference<LinkedQueueNode<T>> f6855a = new AtomicReference<>();
    private final AtomicReference<LinkedQueueNode<T>> b = new AtomicReference<>();

    static final class LinkedQueueNode<E> extends AtomicReference<LinkedQueueNode<E>> {
        private static final long serialVersionUID = 2404266111789071508L;
        private E value;

        LinkedQueueNode() {
        }

        public E a() {
            E b = b();
            a((Object) null);
            return b;
        }

        public E b() {
            return this.value;
        }

        public LinkedQueueNode<E> c() {
            return (LinkedQueueNode) get();
        }

        LinkedQueueNode(E e) {
            a(e);
        }

        public void a(E e) {
            this.value = e;
        }

        public void a(LinkedQueueNode<E> linkedQueueNode) {
            lazySet(linkedQueueNode);
        }
    }

    public MpscLinkedQueue() {
        LinkedQueueNode linkedQueueNode = new LinkedQueueNode();
        a(linkedQueueNode);
        b(linkedQueueNode);
    }

    /* access modifiers changed from: package-private */
    public LinkedQueueNode<T> a() {
        return this.b.get();
    }

    /* access modifiers changed from: package-private */
    public LinkedQueueNode<T> b(LinkedQueueNode<T> linkedQueueNode) {
        return this.f6855a.getAndSet(linkedQueueNode);
    }

    /* access modifiers changed from: package-private */
    public LinkedQueueNode<T> c() {
        return this.f6855a.get();
    }

    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP:0: B:0:0x0000->B:3:0x000a, LOOP_START, MTH_ENTER_BLOCK] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void clear() {
        /*
            r1 = this;
        L_0x0000:
            java.lang.Object r0 = r1.poll()
            if (r0 == 0) goto L_0x000d
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x000d
            goto L_0x0000
        L_0x000d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: io.reactivex.internal.queue.MpscLinkedQueue.clear():void");
    }

    public boolean isEmpty() {
        return b() == c();
    }

    public boolean offer(T t) {
        if (t != null) {
            LinkedQueueNode linkedQueueNode = new LinkedQueueNode(t);
            b(linkedQueueNode).a(linkedQueueNode);
            return true;
        }
        throw new NullPointerException("Null is not a valid element");
    }

    public T poll() {
        LinkedQueueNode c;
        LinkedQueueNode a2 = a();
        LinkedQueueNode c2 = a2.c();
        if (c2 != null) {
            T a3 = c2.a();
            a(c2);
            return a3;
        } else if (a2 == c()) {
            return null;
        } else {
            do {
                c = a2.c();
            } while (c == null);
            T a4 = c.a();
            a(c);
            return a4;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(LinkedQueueNode<T> linkedQueueNode) {
        this.b.lazySet(linkedQueueNode);
    }

    /* access modifiers changed from: package-private */
    public LinkedQueueNode<T> b() {
        return this.b.get();
    }
}
