package io.reactivex.internal.queue;

import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.util.Pow2;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class SpscLinkedArrayQueue<T> implements SimplePlainQueue<T> {
    static final int i = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096).intValue();
    private static final Object j = new Object();

    /* renamed from: a  reason: collision with root package name */
    final AtomicLong f6857a = new AtomicLong();
    int b;
    long c;
    final int d;
    AtomicReferenceArray<Object> e;
    final int f;
    AtomicReferenceArray<Object> g;
    final AtomicLong h = new AtomicLong();

    public SpscLinkedArrayQueue(int i2) {
        int a2 = Pow2.a(Math.max(8, i2));
        int i3 = a2 - 1;
        AtomicReferenceArray<Object> atomicReferenceArray = new AtomicReferenceArray<>(a2 + 1);
        this.e = atomicReferenceArray;
        this.d = i3;
        a(a2);
        this.g = atomicReferenceArray;
        this.f = i3;
        this.c = (long) (i3 - 1);
        b(0);
    }

    private boolean a(AtomicReferenceArray<Object> atomicReferenceArray, T t, long j2, int i2) {
        a(atomicReferenceArray, i2, (Object) t);
        b(j2 + 1);
        return true;
    }

    private static int b(int i2) {
        return i2;
    }

    private AtomicReferenceArray<Object> b(AtomicReferenceArray<Object> atomicReferenceArray, int i2) {
        b(i2);
        AtomicReferenceArray<Object> atomicReferenceArray2 = (AtomicReferenceArray) a(atomicReferenceArray, i2);
        a(atomicReferenceArray, i2, (Object) null);
        return atomicReferenceArray2;
    }

    private long c() {
        return this.h.get();
    }

    private long d() {
        return this.f6857a.get();
    }

    private long e() {
        return this.h.get();
    }

    private long f() {
        return this.f6857a.get();
    }

    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }

    public boolean isEmpty() {
        return f() == e();
    }

    public boolean offer(T t) {
        if (t != null) {
            AtomicReferenceArray<Object> atomicReferenceArray = this.e;
            long d2 = d();
            int i2 = this.d;
            int a2 = a(d2, i2);
            if (d2 < this.c) {
                return a(atomicReferenceArray, t, d2, a2);
            }
            long j2 = ((long) this.b) + d2;
            if (a(atomicReferenceArray, a(j2, i2)) == null) {
                this.c = j2 - 1;
                return a(atomicReferenceArray, t, d2, a2);
            } else if (a(atomicReferenceArray, a(1 + d2, i2)) == null) {
                return a(atomicReferenceArray, t, d2, a2);
            } else {
                a(atomicReferenceArray, d2, a2, t, (long) i2);
                return true;
            }
        } else {
            throw new NullPointerException("Null is not a valid element");
        }
    }

    public T poll() {
        AtomicReferenceArray<Object> atomicReferenceArray = this.g;
        long c2 = c();
        int i2 = this.f;
        int a2 = a(c2, i2);
        T a3 = a(atomicReferenceArray, a2);
        boolean z = a3 == j;
        if (a3 != null && !z) {
            a(atomicReferenceArray, a2, (Object) null);
            a(c2 + 1);
            return a3;
        } else if (z) {
            return b(b(atomicReferenceArray, i2 + 1), c2, i2);
        } else {
            return null;
        }
    }

    private void a(AtomicReferenceArray<Object> atomicReferenceArray, long j2, int i2, T t, long j3) {
        AtomicReferenceArray<Object> atomicReferenceArray2 = new AtomicReferenceArray<>(atomicReferenceArray.length());
        this.e = atomicReferenceArray2;
        this.c = (j3 + j2) - 1;
        a(atomicReferenceArray2, i2, (Object) t);
        a(atomicReferenceArray, atomicReferenceArray2);
        a(atomicReferenceArray, i2, j);
        b(j2 + 1);
    }

    private T b(AtomicReferenceArray<Object> atomicReferenceArray, long j2, int i2) {
        this.g = atomicReferenceArray;
        int a2 = a(j2, i2);
        T a3 = a(atomicReferenceArray, a2);
        if (a3 != null) {
            a(atomicReferenceArray, a2, (Object) null);
            a(j2 + 1);
        }
        return a3;
    }

    public int b() {
        long e2 = e();
        while (true) {
            long f2 = f();
            long e3 = e();
            if (e2 == e3) {
                return (int) (f2 - e3);
            }
            e2 = e3;
        }
    }

    private void a(AtomicReferenceArray<Object> atomicReferenceArray, AtomicReferenceArray<Object> atomicReferenceArray2) {
        int length = atomicReferenceArray.length() - 1;
        b(length);
        a(atomicReferenceArray, length, (Object) atomicReferenceArray2);
    }

    private void b(long j2) {
        this.f6857a.lazySet(j2);
    }

    public T a() {
        AtomicReferenceArray<Object> atomicReferenceArray = this.g;
        long c2 = c();
        int i2 = this.f;
        T a2 = a(atomicReferenceArray, a(c2, i2));
        return a2 == j ? a(b(atomicReferenceArray, i2 + 1), c2, i2) : a2;
    }

    private T a(AtomicReferenceArray<Object> atomicReferenceArray, long j2, int i2) {
        this.g = atomicReferenceArray;
        return a(atomicReferenceArray, a(j2, i2));
    }

    private void a(int i2) {
        this.b = Math.min(i2 / 4, i);
    }

    private void a(long j2) {
        this.h.lazySet(j2);
    }

    private static int a(long j2, int i2) {
        int i3 = ((int) j2) & i2;
        b(i3);
        return i3;
    }

    private static void a(AtomicReferenceArray<Object> atomicReferenceArray, int i2, Object obj) {
        atomicReferenceArray.lazySet(i2, obj);
    }

    private static <E> Object a(AtomicReferenceArray<Object> atomicReferenceArray, int i2) {
        return atomicReferenceArray.get(i2);
    }

    public boolean a(T t, T t2) {
        AtomicReferenceArray<Object> atomicReferenceArray = this.e;
        long f2 = f();
        int i2 = this.d;
        long j2 = 2 + f2;
        if (a(atomicReferenceArray, a(j2, i2)) == null) {
            int a2 = a(f2, i2);
            a(atomicReferenceArray, a2 + 1, (Object) t2);
            a(atomicReferenceArray, a2, (Object) t);
            b(j2);
            return true;
        }
        AtomicReferenceArray<Object> atomicReferenceArray2 = new AtomicReferenceArray<>(atomicReferenceArray.length());
        this.e = atomicReferenceArray2;
        int a3 = a(f2, i2);
        a(atomicReferenceArray2, a3 + 1, (Object) t2);
        a(atomicReferenceArray2, a3, (Object) t);
        a(atomicReferenceArray, atomicReferenceArray2);
        a(atomicReferenceArray, a3, j);
        b(j2);
        return true;
    }
}
