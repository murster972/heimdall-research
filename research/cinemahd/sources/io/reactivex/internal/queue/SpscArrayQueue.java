package io.reactivex.internal.queue;

import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.util.Pow2;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class SpscArrayQueue<E> extends AtomicReferenceArray<E> implements SimplePlainQueue<E> {

    /* renamed from: a  reason: collision with root package name */
    private static final Integer f6856a = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096);
    private static final long serialVersionUID = -1296597691183856449L;
    final AtomicLong consumerIndex = new AtomicLong();
    final int lookAheadStep;
    final int mask = (length() - 1);
    final AtomicLong producerIndex = new AtomicLong();
    long producerLookAhead;

    public SpscArrayQueue(int i) {
        super(Pow2.a(i));
        this.lookAheadStep = Math.min(i / 4, f6856a.intValue());
    }

    /* access modifiers changed from: package-private */
    public int a(long j) {
        return this.mask & ((int) j);
    }

    /* access modifiers changed from: package-private */
    public int a(long j, int i) {
        return ((int) j) & i;
    }

    /* access modifiers changed from: package-private */
    public void b(long j) {
        this.consumerIndex.lazySet(j);
    }

    /* access modifiers changed from: package-private */
    public void c(long j) {
        this.producerIndex.lazySet(j);
    }

    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }

    public boolean isEmpty() {
        return this.producerIndex.get() == this.consumerIndex.get();
    }

    public boolean offer(E e) {
        if (e != null) {
            int i = this.mask;
            long j = this.producerIndex.get();
            int a2 = a(j, i);
            if (j >= this.producerLookAhead) {
                long j2 = ((long) this.lookAheadStep) + j;
                if (a(a(j2, i)) == null) {
                    this.producerLookAhead = j2;
                } else if (a(a2) != null) {
                    return false;
                }
            }
            a(a2, e);
            c(j + 1);
            return true;
        }
        throw new NullPointerException("Null is not a valid element");
    }

    public E poll() {
        long j = this.consumerIndex.get();
        int a2 = a(j);
        E a3 = a(a2);
        if (a3 == null) {
            return null;
        }
        b(j + 1);
        a(a2, (Object) null);
        return a3;
    }

    /* access modifiers changed from: package-private */
    public void a(int i, E e) {
        lazySet(i, e);
    }

    /* access modifiers changed from: package-private */
    public E a(int i) {
        return get(i);
    }
}
