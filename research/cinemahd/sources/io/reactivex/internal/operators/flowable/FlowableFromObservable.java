package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableFromObservable<T> extends Flowable<T> {
    private final Observable<T> b;

    static final class SubscriberObserver<T> implements Observer<T>, Subscription {

        /* renamed from: a  reason: collision with root package name */
        final Subscriber<? super T> f6680a;
        Disposable b;

        SubscriberObserver(Subscriber<? super T> subscriber) {
            this.f6680a = subscriber;
        }

        public void cancel() {
            this.b.dispose();
        }

        public void onComplete() {
            this.f6680a.onComplete();
        }

        public void onError(Throwable th) {
            this.f6680a.onError(th);
        }

        public void onNext(T t) {
            this.f6680a.onNext(t);
        }

        public void onSubscribe(Disposable disposable) {
            this.b = disposable;
            this.f6680a.a(this);
        }

        public void request(long j) {
        }
    }

    public FlowableFromObservable(Observable<T> observable) {
        this.b = observable;
    }

    /* access modifiers changed from: protected */
    public void b(Subscriber<? super T> subscriber) {
        this.b.subscribe(new SubscriberObserver(subscriber));
    }
}
