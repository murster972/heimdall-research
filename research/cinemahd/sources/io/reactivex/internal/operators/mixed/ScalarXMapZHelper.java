package io.reactivex.internal.operators.mixed;

import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.MaybeSource;
import io.reactivex.Observer;
import io.reactivex.SingleSource;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.operators.maybe.MaybeToObservable;
import io.reactivex.internal.operators.single.SingleToObservable;
import java.util.concurrent.Callable;

final class ScalarXMapZHelper {
    private ScalarXMapZHelper() {
        throw new IllegalStateException("No instances!");
    }

    static <T> boolean a(Object obj, Function<? super T, ? extends CompletableSource> function, CompletableObserver completableObserver) {
        if (!(obj instanceof Callable)) {
            return false;
        }
        CompletableSource completableSource = null;
        try {
            Object call = ((Callable) obj).call();
            if (call != null) {
                Object apply = function.apply(call);
                ObjectHelper.a(apply, "The mapper returned a null CompletableSource");
                completableSource = (CompletableSource) apply;
            }
            if (completableSource == null) {
                EmptyDisposable.a(completableObserver);
            } else {
                completableSource.a(completableObserver);
            }
            return true;
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, completableObserver);
            return true;
        }
    }

    static <T, R> boolean b(Object obj, Function<? super T, ? extends SingleSource<? extends R>> function, Observer<? super R> observer) {
        if (!(obj instanceof Callable)) {
            return false;
        }
        SingleSource singleSource = null;
        try {
            Object call = ((Callable) obj).call();
            if (call != null) {
                Object apply = function.apply(call);
                ObjectHelper.a(apply, "The mapper returned a null SingleSource");
                singleSource = (SingleSource) apply;
            }
            if (singleSource == null) {
                EmptyDisposable.a((Observer<?>) observer);
            } else {
                singleSource.a(SingleToObservable.a(observer));
            }
            return true;
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (Observer<?>) observer);
            return true;
        }
    }

    static <T, R> boolean a(Object obj, Function<? super T, ? extends MaybeSource<? extends R>> function, Observer<? super R> observer) {
        if (!(obj instanceof Callable)) {
            return false;
        }
        MaybeSource maybeSource = null;
        try {
            Object call = ((Callable) obj).call();
            if (call != null) {
                Object apply = function.apply(call);
                ObjectHelper.a(apply, "The mapper returned a null MaybeSource");
                maybeSource = (MaybeSource) apply;
            }
            if (maybeSource == null) {
                EmptyDisposable.a((Observer<?>) observer);
            } else {
                maybeSource.a(MaybeToObservable.a(observer));
            }
            return true;
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (Observer<?>) observer);
            return true;
        }
    }
}
