package io.reactivex.internal.operators.mixed;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableSwitchMapCompletable<T> extends Completable {

    /* renamed from: a  reason: collision with root package name */
    final Observable<T> f6685a;
    final Function<? super T, ? extends CompletableSource> b;
    final boolean c;

    public ObservableSwitchMapCompletable(Observable<T> observable, Function<? super T, ? extends CompletableSource> function, boolean z) {
        this.f6685a = observable;
        this.b = function;
        this.c = z;
    }

    /* access modifiers changed from: protected */
    public void b(CompletableObserver completableObserver) {
        if (!ScalarXMapZHelper.a((Object) this.f6685a, this.b, completableObserver)) {
            this.f6685a.subscribe(new SwitchMapCompletableObserver(completableObserver, this.b, this.c));
        }
    }

    static final class SwitchMapCompletableObserver<T> implements Observer<T>, Disposable {
        static final SwitchMapInnerObserver h = new SwitchMapInnerObserver((SwitchMapCompletableObserver<?>) null);

        /* renamed from: a  reason: collision with root package name */
        final CompletableObserver f6686a;
        final Function<? super T, ? extends CompletableSource> b;
        final boolean c;
        final AtomicThrowable d = new AtomicThrowable();
        final AtomicReference<SwitchMapInnerObserver> e = new AtomicReference<>();
        volatile boolean f;
        Disposable g;

        static final class SwitchMapInnerObserver extends AtomicReference<Disposable> implements CompletableObserver {
            private static final long serialVersionUID = -8003404460084760287L;
            final SwitchMapCompletableObserver<?> parent;

            SwitchMapInnerObserver(SwitchMapCompletableObserver<?> switchMapCompletableObserver) {
                this.parent = switchMapCompletableObserver;
            }

            /* access modifiers changed from: package-private */
            public void a() {
                DisposableHelper.a((AtomicReference<Disposable>) this);
            }

            public void onComplete() {
                this.parent.a(this);
            }

            public void onError(Throwable th) {
                this.parent.a(this, th);
            }

            public void onSubscribe(Disposable disposable) {
                DisposableHelper.c(this, disposable);
            }
        }

        SwitchMapCompletableObserver(CompletableObserver completableObserver, Function<? super T, ? extends CompletableSource> function, boolean z) {
            this.f6686a = completableObserver;
            this.b = function;
            this.c = z;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            SwitchMapInnerObserver andSet = this.e.getAndSet(h);
            if (andSet != null && andSet != h) {
                andSet.a();
            }
        }

        public void dispose() {
            this.g.dispose();
            a();
        }

        public boolean isDisposed() {
            return this.e.get() == h;
        }

        public void onComplete() {
            this.f = true;
            if (this.e.get() == null) {
                Throwable a2 = this.d.a();
                if (a2 == null) {
                    this.f6686a.onComplete();
                } else {
                    this.f6686a.onError(a2);
                }
            }
        }

        public void onError(Throwable th) {
            if (!this.d.a(th)) {
                RxJavaPlugins.b(th);
            } else if (this.c) {
                onComplete();
            } else {
                a();
                Throwable a2 = this.d.a();
                if (a2 != ExceptionHelper.f6879a) {
                    this.f6686a.onError(a2);
                }
            }
        }

        public void onNext(T t) {
            SwitchMapInnerObserver switchMapInnerObserver;
            try {
                Object apply = this.b.apply(t);
                ObjectHelper.a(apply, "The mapper returned a null CompletableSource");
                CompletableSource completableSource = (CompletableSource) apply;
                SwitchMapInnerObserver switchMapInnerObserver2 = new SwitchMapInnerObserver(this);
                do {
                    switchMapInnerObserver = this.e.get();
                    if (switchMapInnerObserver == h) {
                        return;
                    }
                } while (!this.e.compareAndSet(switchMapInnerObserver, switchMapInnerObserver2));
                if (switchMapInnerObserver != null) {
                    switchMapInnerObserver.a();
                }
                completableSource.a(switchMapInnerObserver2);
            } catch (Throwable th) {
                Exceptions.b(th);
                this.g.dispose();
                onError(th);
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.g, disposable)) {
                this.g = disposable;
                this.f6686a.onSubscribe(this);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(SwitchMapInnerObserver switchMapInnerObserver, Throwable th) {
            if (!this.e.compareAndSet(switchMapInnerObserver, (Object) null) || !this.d.a(th)) {
                RxJavaPlugins.b(th);
            } else if (!this.c) {
                dispose();
                Throwable a2 = this.d.a();
                if (a2 != ExceptionHelper.f6879a) {
                    this.f6686a.onError(a2);
                }
            } else if (this.f) {
                this.f6686a.onError(this.d.a());
            }
        }

        /* access modifiers changed from: package-private */
        public void a(SwitchMapInnerObserver switchMapInnerObserver) {
            if (this.e.compareAndSet(switchMapInnerObserver, (Object) null) && this.f) {
                Throwable a2 = this.d.a();
                if (a2 == null) {
                    this.f6686a.onComplete();
                } else {
                    this.f6686a.onError(a2);
                }
            }
        }
    }
}
