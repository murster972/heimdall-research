package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableReduceSeedSingle<T, R> extends Single<R> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6795a;
    final R b;
    final BiFunction<R, ? super T, R> c;

    static final class ReduceSeedObserver<T, R> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final SingleObserver<? super R> f6796a;
        final BiFunction<R, ? super T, R> b;
        R c;
        Disposable d;

        ReduceSeedObserver(SingleObserver<? super R> singleObserver, BiFunction<R, ? super T, R> biFunction, R r) {
            this.f6796a = singleObserver;
            this.c = r;
            this.b = biFunction;
        }

        public void dispose() {
            this.d.dispose();
        }

        public boolean isDisposed() {
            return this.d.isDisposed();
        }

        public void onComplete() {
            R r = this.c;
            if (r != null) {
                this.c = null;
                this.f6796a.onSuccess(r);
            }
        }

        public void onError(Throwable th) {
            if (this.c != null) {
                this.c = null;
                this.f6796a.onError(th);
                return;
            }
            RxJavaPlugins.b(th);
        }

        public void onNext(T t) {
            R r = this.c;
            if (r != null) {
                try {
                    R a2 = this.b.a(r, t);
                    ObjectHelper.a(a2, "The reducer returned a null value");
                    this.c = a2;
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.d.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.f6796a.onSubscribe(this);
            }
        }
    }

    public ObservableReduceSeedSingle(ObservableSource<T> observableSource, R r, BiFunction<R, ? super T, R> biFunction) {
        this.f6795a = observableSource;
        this.b = r;
        this.c = biFunction;
    }

    /* access modifiers changed from: protected */
    public void b(SingleObserver<? super R> singleObserver) {
        this.f6795a.subscribe(new ReduceSeedObserver(singleObserver, this.c, this.b));
    }
}
