package io.reactivex.internal.operators.observable;

import io.reactivex.Emitter;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class ObservableGenerate<T, S> extends Observable<T> {

    /* renamed from: a  reason: collision with root package name */
    final Callable<S> f6750a;
    final BiFunction<S, Emitter<T>, S> b;
    final Consumer<? super S> c;

    public ObservableGenerate(Callable<S> callable, BiFunction<S, Emitter<T>, S> biFunction, Consumer<? super S> consumer) {
        this.f6750a = callable;
        this.b = biFunction;
        this.c = consumer;
    }

    public void subscribeActual(Observer<? super T> observer) {
        try {
            GeneratorDisposable generatorDisposable = new GeneratorDisposable(observer, this.b, this.c, this.f6750a.call());
            observer.onSubscribe(generatorDisposable);
            generatorDisposable.a();
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (Observer<?>) observer);
        }
    }

    static final class GeneratorDisposable<T, S> implements Emitter<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6751a;
        final BiFunction<S, ? super Emitter<T>, S> b;
        final Consumer<? super S> c;
        S d;
        volatile boolean e;
        boolean f;

        GeneratorDisposable(Observer<? super T> observer, BiFunction<S, ? super Emitter<T>, S> biFunction, Consumer<? super S> consumer, S s) {
            this.f6751a = observer;
            this.b = biFunction;
            this.c = consumer;
            this.d = s;
        }

        public void a() {
            S s = this.d;
            if (this.e) {
                this.d = null;
                a(s);
                return;
            }
            BiFunction<S, ? super Emitter<T>, S> biFunction = this.b;
            while (!this.e) {
                try {
                    s = biFunction.a(s, this);
                    if (this.f) {
                        this.e = true;
                        this.d = null;
                        a(s);
                        return;
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.d = null;
                    this.e = true;
                    a(th);
                    a(s);
                    return;
                }
            }
            this.d = null;
            a(s);
        }

        public void dispose() {
            this.e = true;
        }

        public boolean isDisposed() {
            return this.e;
        }

        private void a(S s) {
            try {
                this.c.accept(s);
            } catch (Throwable th) {
                Exceptions.b(th);
                RxJavaPlugins.b(th);
            }
        }

        public void a(Throwable th) {
            if (this.f) {
                RxJavaPlugins.b(th);
                return;
            }
            if (th == null) {
                th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
            }
            this.f = true;
            this.f6751a.onError(th);
        }
    }
}
