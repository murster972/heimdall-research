package io.reactivex.internal.operators.observable;

import com.facebook.common.time.Clock;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableTimeoutTimed<T> extends AbstractObservableWithUpstream<T, T> {
    final long b;
    final TimeUnit c;
    final Scheduler d;
    final ObservableSource<? extends T> e;

    static final class FallbackObserver<T> implements Observer<T> {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6831a;
        final AtomicReference<Disposable> b;

        FallbackObserver(Observer<? super T> observer, AtomicReference<Disposable> atomicReference) {
            this.f6831a = observer;
            this.b = atomicReference;
        }

        public void onComplete() {
            this.f6831a.onComplete();
        }

        public void onError(Throwable th) {
            this.f6831a.onError(th);
        }

        public void onNext(T t) {
            this.f6831a.onNext(t);
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.a(this.b, disposable);
        }
    }

    static final class TimeoutFallbackObserver<T> extends AtomicReference<Disposable> implements Observer<T>, Disposable, TimeoutSupport {
        private static final long serialVersionUID = 3764492702657003550L;
        final Observer<? super T> downstream;
        ObservableSource<? extends T> fallback;
        final AtomicLong index = new AtomicLong();
        final SequentialDisposable task = new SequentialDisposable();
        final long timeout;
        final TimeUnit unit;
        final AtomicReference<Disposable> upstream = new AtomicReference<>();
        final Scheduler.Worker worker;

        TimeoutFallbackObserver(Observer<? super T> observer, long j, TimeUnit timeUnit, Scheduler.Worker worker2, ObservableSource<? extends T> observableSource) {
            this.downstream = observer;
            this.timeout = j;
            this.unit = timeUnit;
            this.worker = worker2;
            this.fallback = observableSource;
        }

        public void a(long j) {
            if (this.index.compareAndSet(j, Clock.MAX_TIME)) {
                DisposableHelper.a(this.upstream);
                ObservableSource<? extends T> observableSource = this.fallback;
                this.fallback = null;
                observableSource.subscribe(new FallbackObserver(this.downstream, this));
                this.worker.dispose();
            }
        }

        /* access modifiers changed from: package-private */
        public void b(long j) {
            this.task.a(this.worker.a(new TimeoutTask(j, this), this.timeout, this.unit));
        }

        public void dispose() {
            DisposableHelper.a(this.upstream);
            DisposableHelper.a((AtomicReference<Disposable>) this);
            this.worker.dispose();
        }

        public boolean isDisposed() {
            return DisposableHelper.a((Disposable) get());
        }

        public void onComplete() {
            if (this.index.getAndSet(Clock.MAX_TIME) != Clock.MAX_TIME) {
                this.task.dispose();
                this.downstream.onComplete();
                this.worker.dispose();
            }
        }

        public void onError(Throwable th) {
            if (this.index.getAndSet(Clock.MAX_TIME) != Clock.MAX_TIME) {
                this.task.dispose();
                this.downstream.onError(th);
                this.worker.dispose();
                return;
            }
            RxJavaPlugins.b(th);
        }

        public void onNext(T t) {
            long j = this.index.get();
            if (j != Clock.MAX_TIME) {
                long j2 = 1 + j;
                if (this.index.compareAndSet(j, j2)) {
                    ((Disposable) this.task.get()).dispose();
                    this.downstream.onNext(t);
                    b(j2);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.upstream, disposable);
        }
    }

    static final class TimeoutObserver<T> extends AtomicLong implements Observer<T>, Disposable, TimeoutSupport {
        private static final long serialVersionUID = 3764492702657003550L;
        final Observer<? super T> downstream;
        final SequentialDisposable task = new SequentialDisposable();
        final long timeout;
        final TimeUnit unit;
        final AtomicReference<Disposable> upstream = new AtomicReference<>();
        final Scheduler.Worker worker;

        TimeoutObserver(Observer<? super T> observer, long j, TimeUnit timeUnit, Scheduler.Worker worker2) {
            this.downstream = observer;
            this.timeout = j;
            this.unit = timeUnit;
            this.worker = worker2;
        }

        public void a(long j) {
            if (compareAndSet(j, Clock.MAX_TIME)) {
                DisposableHelper.a(this.upstream);
                this.downstream.onError(new TimeoutException(ExceptionHelper.a(this.timeout, this.unit)));
                this.worker.dispose();
            }
        }

        /* access modifiers changed from: package-private */
        public void b(long j) {
            this.task.a(this.worker.a(new TimeoutTask(j, this), this.timeout, this.unit));
        }

        public void dispose() {
            DisposableHelper.a(this.upstream);
            this.worker.dispose();
        }

        public boolean isDisposed() {
            return DisposableHelper.a(this.upstream.get());
        }

        public void onComplete() {
            if (getAndSet(Clock.MAX_TIME) != Clock.MAX_TIME) {
                this.task.dispose();
                this.downstream.onComplete();
                this.worker.dispose();
            }
        }

        public void onError(Throwable th) {
            if (getAndSet(Clock.MAX_TIME) != Clock.MAX_TIME) {
                this.task.dispose();
                this.downstream.onError(th);
                this.worker.dispose();
                return;
            }
            RxJavaPlugins.b(th);
        }

        public void onNext(T t) {
            long j = get();
            if (j != Clock.MAX_TIME) {
                long j2 = 1 + j;
                if (compareAndSet(j, j2)) {
                    ((Disposable) this.task.get()).dispose();
                    this.downstream.onNext(t);
                    b(j2);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.upstream, disposable);
        }
    }

    interface TimeoutSupport {
        void a(long j);
    }

    static final class TimeoutTask implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final TimeoutSupport f6832a;
        final long b;

        TimeoutTask(long j, TimeoutSupport timeoutSupport) {
            this.b = j;
            this.f6832a = timeoutSupport;
        }

        public void run() {
            this.f6832a.a(this.b);
        }
    }

    public ObservableTimeoutTimed(Observable<T> observable, long j, TimeUnit timeUnit, Scheduler scheduler, ObservableSource<? extends T> observableSource) {
        super(observable);
        this.b = j;
        this.c = timeUnit;
        this.d = scheduler;
        this.e = observableSource;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        if (this.e == null) {
            TimeoutObserver timeoutObserver = new TimeoutObserver(observer, this.b, this.c, this.d.a());
            observer.onSubscribe(timeoutObserver);
            timeoutObserver.b(0);
            this.f6691a.subscribe(timeoutObserver);
            return;
        }
        TimeoutFallbackObserver timeoutFallbackObserver = new TimeoutFallbackObserver(observer, this.b, this.c, this.d.a(), this.e);
        observer.onSubscribe(timeoutFallbackObserver);
        timeoutFallbackObserver.b(0);
        this.f6691a.subscribe(timeoutFallbackObserver);
    }
}
