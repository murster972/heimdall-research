package io.reactivex.internal.operators.observable;

import io.reactivex.Notification;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableMaterialize<T> extends AbstractObservableWithUpstream<T, Notification<T>> {

    static final class MaterializeObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super Notification<T>> f6783a;
        Disposable b;

        MaterializeObserver(Observer<? super Notification<T>> observer) {
            this.f6783a = observer;
        }

        public void dispose() {
            this.b.dispose();
        }

        public boolean isDisposed() {
            return this.b.isDisposed();
        }

        public void onComplete() {
            this.f6783a.onNext(Notification.f());
            this.f6783a.onComplete();
        }

        public void onError(Throwable th) {
            this.f6783a.onNext(Notification.a(th));
            this.f6783a.onComplete();
        }

        public void onNext(T t) {
            this.f6783a.onNext(Notification.a(t));
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.b, disposable)) {
                this.b = disposable;
                this.f6783a.onSubscribe(this);
            }
        }
    }

    public ObservableMaterialize(ObservableSource<T> observableSource) {
        super(observableSource);
    }

    public void subscribeActual(Observer<? super Notification<T>> observer) {
        this.f6691a.subscribe(new MaterializeObserver(observer));
    }
}
