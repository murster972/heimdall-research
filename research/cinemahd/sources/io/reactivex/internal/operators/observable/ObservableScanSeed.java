package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class ObservableScanSeed<T, R> extends AbstractObservableWithUpstream<T, R> {
    final BiFunction<R, ? super T, R> b;
    final Callable<R> c;

    static final class ScanSeedObserver<T, R> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super R> f6810a;
        final BiFunction<R, ? super T, R> b;
        R c;
        Disposable d;
        boolean e;

        ScanSeedObserver(Observer<? super R> observer, BiFunction<R, ? super T, R> biFunction, R r) {
            this.f6810a = observer;
            this.b = biFunction;
            this.c = r;
        }

        public void dispose() {
            this.d.dispose();
        }

        public boolean isDisposed() {
            return this.d.isDisposed();
        }

        public void onComplete() {
            if (!this.e) {
                this.e = true;
                this.f6810a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.e) {
                RxJavaPlugins.b(th);
                return;
            }
            this.e = true;
            this.f6810a.onError(th);
        }

        public void onNext(T t) {
            if (!this.e) {
                try {
                    R a2 = this.b.a(this.c, t);
                    ObjectHelper.a(a2, "The accumulator returned a null value");
                    this.c = a2;
                    this.f6810a.onNext(a2);
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.d.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.f6810a.onSubscribe(this);
                this.f6810a.onNext(this.c);
            }
        }
    }

    public ObservableScanSeed(ObservableSource<T> observableSource, Callable<R> callable, BiFunction<R, ? super T, R> biFunction) {
        super(observableSource);
        this.b = biFunction;
        this.c = callable;
    }

    public void subscribeActual(Observer<? super R> observer) {
        try {
            R call = this.c.call();
            ObjectHelper.a(call, "The seed supplied is null");
            this.f6691a.subscribe(new ScanSeedObserver(observer, this.b, call));
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (Observer<?>) observer);
        }
    }
}
