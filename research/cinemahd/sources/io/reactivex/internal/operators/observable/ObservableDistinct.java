package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.BasicFuseableObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.concurrent.Callable;

public final class ObservableDistinct<T, K> extends AbstractObservableWithUpstream<T, T> {
    final Function<? super T, K> b;
    final Callable<? extends Collection<? super K>> c;

    static final class DistinctObserver<T, K> extends BasicFuseableObserver<T, T> {
        final Collection<? super K> f;
        final Function<? super T, K> g;

        DistinctObserver(Observer<? super T> observer, Function<? super T, K> function, Collection<? super K> collection) {
            super(observer);
            this.g = function;
            this.f = collection;
        }

        public int a(int i) {
            return b(i);
        }

        public void clear() {
            this.f.clear();
            super.clear();
        }

        public void onComplete() {
            if (!this.d) {
                this.d = true;
                this.f.clear();
                this.f6673a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.d) {
                RxJavaPlugins.b(th);
                return;
            }
            this.d = true;
            this.f.clear();
            this.f6673a.onError(th);
        }

        public void onNext(T t) {
            if (!this.d) {
                if (this.e == 0) {
                    try {
                        K apply = this.g.apply(t);
                        ObjectHelper.a(apply, "The keySelector returned a null key");
                        if (this.f.add(apply)) {
                            this.f6673a.onNext(t);
                        }
                    } catch (Throwable th) {
                        a(th);
                    }
                } else {
                    this.f6673a.onNext(null);
                }
            }
        }

        public T poll() throws Exception {
            T poll;
            Collection<? super K> collection;
            K apply;
            do {
                poll = this.c.poll();
                if (poll == null) {
                    break;
                }
                collection = this.f;
                apply = this.g.apply(poll);
                ObjectHelper.a(apply, "The keySelector returned a null key");
            } while (!collection.add(apply));
            return poll;
        }
    }

    public ObservableDistinct(ObservableSource<T> observableSource, Function<? super T, K> function, Callable<? extends Collection<? super K>> callable) {
        super(observableSource);
        this.b = function;
        this.c = callable;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        try {
            Object call = this.c.call();
            ObjectHelper.a(call, "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
            this.f6691a.subscribe(new DistinctObserver(observer, this.b, (Collection) call));
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (Observer<?>) observer);
        }
    }
}
