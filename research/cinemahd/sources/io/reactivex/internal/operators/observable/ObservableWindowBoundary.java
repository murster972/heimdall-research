package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.UnicastSubject;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableWindowBoundary<T, B> extends AbstractObservableWithUpstream<T, Observable<T>> {
    final ObservableSource<B> b;
    final int c;

    static final class WindowBoundaryInnerObserver<T, B> extends DisposableObserver<B> {
        final WindowBoundaryMainObserver<T, B> b;
        boolean c;

        WindowBoundaryInnerObserver(WindowBoundaryMainObserver<T, B> windowBoundaryMainObserver) {
            this.b = windowBoundaryMainObserver;
        }

        public void onComplete() {
            if (!this.c) {
                this.c = true;
                this.b.b();
            }
        }

        public void onError(Throwable th) {
            if (this.c) {
                RxJavaPlugins.b(th);
                return;
            }
            this.c = true;
            this.b.a(th);
        }

        public void onNext(B b2) {
            if (!this.c) {
                this.b.c();
            }
        }
    }

    public ObservableWindowBoundary(ObservableSource<T> observableSource, ObservableSource<B> observableSource2, int i) {
        super(observableSource);
        this.b = observableSource2;
        this.c = i;
    }

    public void subscribeActual(Observer<? super Observable<T>> observer) {
        WindowBoundaryMainObserver windowBoundaryMainObserver = new WindowBoundaryMainObserver(observer, this.c);
        observer.onSubscribe(windowBoundaryMainObserver);
        this.b.subscribe(windowBoundaryMainObserver.boundaryObserver);
        this.f6691a.subscribe(windowBoundaryMainObserver);
    }

    static final class WindowBoundaryMainObserver<T, B> extends AtomicInteger implements Observer<T>, Disposable, Runnable {

        /* renamed from: a  reason: collision with root package name */
        static final Object f6839a = new Object();
        private static final long serialVersionUID = 2233020065421370272L;
        final WindowBoundaryInnerObserver<T, B> boundaryObserver = new WindowBoundaryInnerObserver<>(this);
        final int capacityHint;
        volatile boolean done;
        final Observer<? super Observable<T>> downstream;
        final AtomicThrowable errors = new AtomicThrowable();
        final MpscLinkedQueue<Object> queue = new MpscLinkedQueue<>();
        final AtomicBoolean stopWindows = new AtomicBoolean();
        final AtomicReference<Disposable> upstream = new AtomicReference<>();
        UnicastSubject<T> window;
        final AtomicInteger windows = new AtomicInteger(1);

        WindowBoundaryMainObserver(Observer<? super Observable<T>> observer, int i) {
            this.downstream = observer;
            this.capacityHint = i;
        }

        /* access modifiers changed from: package-private */
        public void a(Throwable th) {
            DisposableHelper.a(this.upstream);
            if (this.errors.a(th)) {
                this.done = true;
                a();
                return;
            }
            RxJavaPlugins.b(th);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            DisposableHelper.a(this.upstream);
            this.done = true;
            a();
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.queue.offer(f6839a);
            a();
        }

        public void dispose() {
            if (this.stopWindows.compareAndSet(false, true)) {
                this.boundaryObserver.dispose();
                if (this.windows.decrementAndGet() == 0) {
                    DisposableHelper.a(this.upstream);
                }
            }
        }

        public boolean isDisposed() {
            return this.stopWindows.get();
        }

        public void onComplete() {
            this.boundaryObserver.dispose();
            this.done = true;
            a();
        }

        public void onError(Throwable th) {
            this.boundaryObserver.dispose();
            if (this.errors.a(th)) {
                this.done = true;
                a();
                return;
            }
            RxJavaPlugins.b(th);
        }

        public void onNext(T t) {
            this.queue.offer(t);
            a();
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.c(this.upstream, disposable)) {
                c();
            }
        }

        public void run() {
            if (this.windows.decrementAndGet() == 0) {
                DisposableHelper.a(this.upstream);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (getAndIncrement() == 0) {
                Observer<? super Observable<T>> observer = this.downstream;
                MpscLinkedQueue<Object> mpscLinkedQueue = this.queue;
                AtomicThrowable atomicThrowable = this.errors;
                int i = 1;
                while (this.windows.get() != 0) {
                    UnicastSubject<T> unicastSubject = this.window;
                    boolean z = this.done;
                    if (!z || atomicThrowable.get() == null) {
                        Object poll = mpscLinkedQueue.poll();
                        boolean z2 = poll == null;
                        if (z && z2) {
                            Throwable a2 = atomicThrowable.a();
                            if (a2 == null) {
                                if (unicastSubject != null) {
                                    this.window = null;
                                    unicastSubject.onComplete();
                                }
                                observer.onComplete();
                                return;
                            }
                            if (unicastSubject != null) {
                                this.window = null;
                                unicastSubject.onError(a2);
                            }
                            observer.onError(a2);
                            return;
                        } else if (z2) {
                            i = addAndGet(-i);
                            if (i == 0) {
                                return;
                            }
                        } else if (poll != f6839a) {
                            unicastSubject.onNext(poll);
                        } else {
                            if (unicastSubject != null) {
                                this.window = null;
                                unicastSubject.onComplete();
                            }
                            if (!this.stopWindows.get()) {
                                UnicastSubject<T> a3 = UnicastSubject.a(this.capacityHint, (Runnable) this);
                                this.window = a3;
                                this.windows.getAndIncrement();
                                observer.onNext(a3);
                            }
                        }
                    } else {
                        mpscLinkedQueue.clear();
                        Throwable a4 = atomicThrowable.a();
                        if (unicastSubject != null) {
                            this.window = null;
                            unicastSubject.onError(a4);
                        }
                        observer.onError(a4);
                        return;
                    }
                }
                mpscLinkedQueue.clear();
                this.window = null;
            }
        }
    }
}
