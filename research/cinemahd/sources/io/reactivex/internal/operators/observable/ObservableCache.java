package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableCache<T> extends AbstractObservableWithUpstream<T, T> implements Observer<T> {
    static final CacheDisposable[] k = new CacheDisposable[0];
    static final CacheDisposable[] l = new CacheDisposable[0];
    final AtomicBoolean b = new AtomicBoolean();
    final int c;
    final AtomicReference<CacheDisposable<T>[]> d;
    volatile long e;
    final Node<T> f;
    Node<T> g;
    int h;
    Throwable i;
    volatile boolean j;

    static final class CacheDisposable<T> extends AtomicInteger implements Disposable {
        private static final long serialVersionUID = 6770240836423125754L;
        volatile boolean disposed;
        final Observer<? super T> downstream;
        long index;
        Node<T> node;
        int offset;
        final ObservableCache<T> parent;

        CacheDisposable(Observer<? super T> observer, ObservableCache<T> observableCache) {
            this.downstream = observer;
            this.parent = observableCache;
            this.node = observableCache.f;
        }

        public void dispose() {
            if (!this.disposed) {
                this.disposed = true;
                this.parent.b(this);
            }
        }

        public boolean isDisposed() {
            return this.disposed;
        }
    }

    static final class Node<T> {

        /* renamed from: a  reason: collision with root package name */
        final T[] f6709a;
        volatile Node<T> b;

        Node(int i) {
            this.f6709a = new Object[i];
        }
    }

    public ObservableCache(Observable<T> observable, int i2) {
        super(observable);
        this.c = i2;
        Node<T> node = new Node<>(i2);
        this.f = node;
        this.g = node;
        this.d = new AtomicReference<>(k);
    }

    /* access modifiers changed from: package-private */
    public void a(CacheDisposable<T> cacheDisposable) {
        CacheDisposable[] cacheDisposableArr;
        CacheDisposable[] cacheDisposableArr2;
        do {
            cacheDisposableArr = (CacheDisposable[]) this.d.get();
            if (cacheDisposableArr != l) {
                int length = cacheDisposableArr.length;
                cacheDisposableArr2 = new CacheDisposable[(length + 1)];
                System.arraycopy(cacheDisposableArr, 0, cacheDisposableArr2, 0, length);
                cacheDisposableArr2[length] = cacheDisposable;
            } else {
                return;
            }
        } while (!this.d.compareAndSet(cacheDisposableArr, cacheDisposableArr2));
    }

    /* access modifiers changed from: package-private */
    public void b(CacheDisposable<T> cacheDisposable) {
        CacheDisposable<T>[] cacheDisposableArr;
        CacheDisposable[] cacheDisposableArr2;
        do {
            cacheDisposableArr = (CacheDisposable[]) this.d.get();
            int length = cacheDisposableArr.length;
            if (length != 0) {
                int i2 = -1;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    } else if (cacheDisposableArr[i3] == cacheDisposable) {
                        i2 = i3;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (i2 >= 0) {
                    if (length == 1) {
                        cacheDisposableArr2 = k;
                    } else {
                        CacheDisposable[] cacheDisposableArr3 = new CacheDisposable[(length - 1)];
                        System.arraycopy(cacheDisposableArr, 0, cacheDisposableArr3, 0, i2);
                        System.arraycopy(cacheDisposableArr, i2 + 1, cacheDisposableArr3, i2, (length - i2) - 1);
                        cacheDisposableArr2 = cacheDisposableArr3;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } while (!this.d.compareAndSet(cacheDisposableArr, cacheDisposableArr2));
    }

    /* access modifiers changed from: package-private */
    public void c(CacheDisposable<T> cacheDisposable) {
        if (cacheDisposable.getAndIncrement() == 0) {
            long j2 = cacheDisposable.index;
            int i2 = cacheDisposable.offset;
            Node<T> node = cacheDisposable.node;
            Observer<? super T> observer = cacheDisposable.downstream;
            int i3 = this.c;
            int i4 = 1;
            while (!cacheDisposable.disposed) {
                boolean z = this.j;
                boolean z2 = this.e == j2;
                if (z && z2) {
                    cacheDisposable.node = null;
                    Throwable th = this.i;
                    if (th != null) {
                        observer.onError(th);
                        return;
                    } else {
                        observer.onComplete();
                        return;
                    }
                } else if (!z2) {
                    if (i2 == i3) {
                        node = node.b;
                        i2 = 0;
                    }
                    observer.onNext(node.f6709a[i2]);
                    i2++;
                    j2++;
                } else {
                    cacheDisposable.index = j2;
                    cacheDisposable.offset = i2;
                    cacheDisposable.node = node;
                    i4 = cacheDisposable.addAndGet(-i4);
                    if (i4 == 0) {
                        return;
                    }
                }
            }
            cacheDisposable.node = null;
        }
    }

    public void onComplete() {
        this.j = true;
        for (CacheDisposable c2 : (CacheDisposable[]) this.d.getAndSet(l)) {
            c(c2);
        }
    }

    public void onError(Throwable th) {
        this.i = th;
        this.j = true;
        for (CacheDisposable c2 : (CacheDisposable[]) this.d.getAndSet(l)) {
            c(c2);
        }
    }

    public void onNext(T t) {
        int i2 = this.h;
        if (i2 == this.c) {
            Node<T> node = new Node<>(i2);
            node.f6709a[0] = t;
            this.h = 1;
            this.g.b = node;
            this.g = node;
        } else {
            this.g.f6709a[i2] = t;
            this.h = i2 + 1;
        }
        this.e++;
        for (CacheDisposable c2 : (CacheDisposable[]) this.d.get()) {
            c(c2);
        }
    }

    public void onSubscribe(Disposable disposable) {
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        CacheDisposable cacheDisposable = new CacheDisposable(observer, this);
        observer.onSubscribe(cacheDisposable);
        a(cacheDisposable);
        if (this.b.get() || !this.b.compareAndSet(false, true)) {
            c(cacheDisposable);
        } else {
            this.f6691a.subscribe(this);
        }
    }
}
