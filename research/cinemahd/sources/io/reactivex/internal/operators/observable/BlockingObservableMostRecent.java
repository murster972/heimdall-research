package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.observers.DefaultObserver;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class BlockingObservableMostRecent<T> implements Iterable<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6694a;
    final T b;

    static final class MostRecentObserver<T> extends DefaultObserver<T> {
        volatile Object b;

        final class Iterator implements java.util.Iterator<T> {

            /* renamed from: a  reason: collision with root package name */
            private Object f6695a;

            Iterator() {
            }

            public boolean hasNext() {
                this.f6695a = MostRecentObserver.this.b;
                return !NotificationLite.c(this.f6695a);
            }

            public T next() {
                Object obj = null;
                try {
                    if (this.f6695a == null) {
                        obj = MostRecentObserver.this.b;
                    }
                    if (NotificationLite.c(this.f6695a)) {
                        throw new NoSuchElementException();
                    } else if (!NotificationLite.d(this.f6695a)) {
                        T t = this.f6695a;
                        NotificationLite.b(t);
                        this.f6695a = obj;
                        return t;
                    } else {
                        throw ExceptionHelper.a(NotificationLite.a(this.f6695a));
                    }
                } finally {
                    this.f6695a = obj;
                }
            }

            public void remove() {
                throw new UnsupportedOperationException("Read only iterator");
            }
        }

        MostRecentObserver(T t) {
            NotificationLite.e(t);
            this.b = t;
        }

        public MostRecentObserver<T>.Iterator b() {
            return new Iterator();
        }

        public void onComplete() {
            this.b = NotificationLite.a();
        }

        public void onError(Throwable th) {
            this.b = NotificationLite.a(th);
        }

        public void onNext(T t) {
            NotificationLite.e(t);
            this.b = t;
        }
    }

    public BlockingObservableMostRecent(ObservableSource<T> observableSource, T t) {
        this.f6694a = observableSource;
        this.b = t;
    }

    public Iterator<T> iterator() {
        MostRecentObserver mostRecentObserver = new MostRecentObserver(this.b);
        this.f6694a.subscribe(mostRecentObserver);
        return mostRecentObserver.b();
    }
}
