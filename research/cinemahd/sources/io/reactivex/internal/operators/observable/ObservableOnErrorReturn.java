package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableOnErrorReturn<T> extends AbstractObservableWithUpstream<T, T> {
    final Function<? super Throwable, ? extends T> b;

    static final class OnErrorReturnObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6786a;
        final Function<? super Throwable, ? extends T> b;
        Disposable c;

        OnErrorReturnObserver(Observer<? super T> observer, Function<? super Throwable, ? extends T> function) {
            this.f6786a = observer;
            this.b = function;
        }

        public void dispose() {
            this.c.dispose();
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            this.f6786a.onComplete();
        }

        public void onError(Throwable th) {
            try {
                Object apply = this.b.apply(th);
                if (apply == null) {
                    NullPointerException nullPointerException = new NullPointerException("The supplied value is null");
                    nullPointerException.initCause(th);
                    this.f6786a.onError(nullPointerException);
                    return;
                }
                this.f6786a.onNext(apply);
                this.f6786a.onComplete();
            } catch (Throwable th2) {
                Exceptions.b(th2);
                this.f6786a.onError(new CompositeException(th, th2));
            }
        }

        public void onNext(T t) {
            this.f6786a.onNext(t);
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6786a.onSubscribe(this);
            }
        }
    }

    public ObservableOnErrorReturn(ObservableSource<T> observableSource, Function<? super Throwable, ? extends T> function) {
        super(observableSource);
        this.b = function;
    }

    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new OnErrorReturnObserver(observer, this.b));
    }
}
