package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableCountSingle<T> extends Single<Long> implements FuseToObservable<Long> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6715a;

    static final class CountObserver implements Observer<Object>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final SingleObserver<? super Long> f6716a;
        Disposable b;
        long c;

        CountObserver(SingleObserver<? super Long> singleObserver) {
            this.f6716a = singleObserver;
        }

        public void dispose() {
            this.b.dispose();
            this.b = DisposableHelper.DISPOSED;
        }

        public boolean isDisposed() {
            return this.b.isDisposed();
        }

        public void onComplete() {
            this.b = DisposableHelper.DISPOSED;
            this.f6716a.onSuccess(Long.valueOf(this.c));
        }

        public void onError(Throwable th) {
            this.b = DisposableHelper.DISPOSED;
            this.f6716a.onError(th);
        }

        public void onNext(Object obj) {
            this.c++;
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.b, disposable)) {
                this.b = disposable;
                this.f6716a.onSubscribe(this);
            }
        }
    }

    public ObservableCountSingle(ObservableSource<T> observableSource) {
        this.f6715a = observableSource;
    }

    public Observable<Long> a() {
        return RxJavaPlugins.a(new ObservableCount(this.f6715a));
    }

    public void b(SingleObserver<? super Long> singleObserver) {
        this.f6715a.subscribe(new CountObserver(singleObserver));
    }
}
