package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableTakeUntilPredicate<T> extends AbstractObservableWithUpstream<T, T> {
    final Predicate<? super T> b;

    static final class TakeUntilPredicateObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6828a;
        final Predicate<? super T> b;
        Disposable c;
        boolean d;

        TakeUntilPredicateObserver(Observer<? super T> observer, Predicate<? super T> predicate) {
            this.f6828a = observer;
            this.b = predicate;
        }

        public void dispose() {
            this.c.dispose();
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            if (!this.d) {
                this.d = true;
                this.f6828a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (!this.d) {
                this.d = true;
                this.f6828a.onError(th);
                return;
            }
            RxJavaPlugins.b(th);
        }

        public void onNext(T t) {
            if (!this.d) {
                this.f6828a.onNext(t);
                try {
                    if (this.b.a(t)) {
                        this.d = true;
                        this.c.dispose();
                        this.f6828a.onComplete();
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.c.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6828a.onSubscribe(this);
            }
        }
    }

    public ObservableTakeUntilPredicate(ObservableSource<T> observableSource, Predicate<? super T> predicate) {
        super(observableSource);
        this.b = predicate;
    }

    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new TakeUntilPredicateObserver(observer, this.b));
    }
}
