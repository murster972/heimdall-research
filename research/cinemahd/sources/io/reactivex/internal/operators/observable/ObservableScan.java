package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableScan<T> extends AbstractObservableWithUpstream<T, T> {
    final BiFunction<T, T, T> b;

    static final class ScanObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6809a;
        final BiFunction<T, T, T> b;
        Disposable c;
        T d;
        boolean e;

        ScanObserver(Observer<? super T> observer, BiFunction<T, T, T> biFunction) {
            this.f6809a = observer;
            this.b = biFunction;
        }

        public void dispose() {
            this.c.dispose();
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            if (!this.e) {
                this.e = true;
                this.f6809a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.e) {
                RxJavaPlugins.b(th);
                return;
            }
            this.e = true;
            this.f6809a.onError(th);
        }

        public void onNext(T t) {
            if (!this.e) {
                Observer<? super T> observer = this.f6809a;
                T t2 = this.d;
                if (t2 == null) {
                    this.d = t;
                    observer.onNext(t);
                    return;
                }
                try {
                    T a2 = this.b.a(t2, t);
                    ObjectHelper.a(a2, "The value returned by the accumulator is null");
                    this.d = a2;
                    observer.onNext(a2);
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.c.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6809a.onSubscribe(this);
            }
        }
    }

    public ObservableScan(ObservableSource<T> observableSource, BiFunction<T, T, T> biFunction) {
        super(observableSource);
        this.b = biFunction;
    }

    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new ScanObserver(observer, this.b));
    }
}
