package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableTakeLastOne<T> extends AbstractObservableWithUpstream<T, T> {

    static final class TakeLastOneObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6827a;
        Disposable b;
        T c;

        TakeLastOneObserver(Observer<? super T> observer) {
            this.f6827a = observer;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            T t = this.c;
            if (t != null) {
                this.c = null;
                this.f6827a.onNext(t);
            }
            this.f6827a.onComplete();
        }

        public void dispose() {
            this.c = null;
            this.b.dispose();
        }

        public boolean isDisposed() {
            return this.b.isDisposed();
        }

        public void onComplete() {
            a();
        }

        public void onError(Throwable th) {
            this.c = null;
            this.f6827a.onError(th);
        }

        public void onNext(T t) {
            this.c = t;
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.b, disposable)) {
                this.b = disposable;
                this.f6827a.onSubscribe(this);
            }
        }
    }

    public ObservableTakeLastOne(ObservableSource<T> observableSource) {
        super(observableSource);
    }

    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new TakeLastOneObserver(observer));
    }
}
