package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableOnErrorNext<T> extends AbstractObservableWithUpstream<T, T> {
    final Function<? super Throwable, ? extends ObservableSource<? extends T>> b;
    final boolean c;

    static final class OnErrorNextObserver<T> implements Observer<T> {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6785a;
        final Function<? super Throwable, ? extends ObservableSource<? extends T>> b;
        final boolean c;
        final SequentialDisposable d = new SequentialDisposable();
        boolean e;
        boolean f;

        OnErrorNextObserver(Observer<? super T> observer, Function<? super Throwable, ? extends ObservableSource<? extends T>> function, boolean z) {
            this.f6785a = observer;
            this.b = function;
            this.c = z;
        }

        public void onComplete() {
            if (!this.f) {
                this.f = true;
                this.e = true;
                this.f6785a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (!this.e) {
                this.e = true;
                if (!this.c || (th instanceof Exception)) {
                    try {
                        ObservableSource observableSource = (ObservableSource) this.b.apply(th);
                        if (observableSource == null) {
                            NullPointerException nullPointerException = new NullPointerException("Observable is null");
                            nullPointerException.initCause(th);
                            this.f6785a.onError(nullPointerException);
                            return;
                        }
                        observableSource.subscribe(this);
                    } catch (Throwable th2) {
                        Exceptions.b(th2);
                        this.f6785a.onError(new CompositeException(th, th2));
                    }
                } else {
                    this.f6785a.onError(th);
                }
            } else if (this.f) {
                RxJavaPlugins.b(th);
            } else {
                this.f6785a.onError(th);
            }
        }

        public void onNext(T t) {
            if (!this.f) {
                this.f6785a.onNext(t);
            }
        }

        public void onSubscribe(Disposable disposable) {
            this.d.a(disposable);
        }
    }

    public ObservableOnErrorNext(ObservableSource<T> observableSource, Function<? super Throwable, ? extends ObservableSource<? extends T>> function, boolean z) {
        super(observableSource);
        this.b = function;
        this.c = z;
    }

    public void subscribeActual(Observer<? super T> observer) {
        OnErrorNextObserver onErrorNextObserver = new OnErrorNextObserver(observer, this.b, this.c);
        observer.onSubscribe(onErrorNextObserver.d);
        this.f6691a.subscribe(onErrorNextObserver);
    }
}
