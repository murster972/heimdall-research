package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.Collection;
import java.util.concurrent.Callable;

public final class ObservableToList<T, U extends Collection<? super T>> extends AbstractObservableWithUpstream<T, U> {
    final Callable<U> b;

    static final class ToListObserver<T, U extends Collection<? super T>> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super U> f6834a;
        Disposable b;
        U c;

        ToListObserver(Observer<? super U> observer, U u) {
            this.f6834a = observer;
            this.c = u;
        }

        public void dispose() {
            this.b.dispose();
        }

        public boolean isDisposed() {
            return this.b.isDisposed();
        }

        public void onComplete() {
            U u = this.c;
            this.c = null;
            this.f6834a.onNext(u);
            this.f6834a.onComplete();
        }

        public void onError(Throwable th) {
            this.c = null;
            this.f6834a.onError(th);
        }

        public void onNext(T t) {
            this.c.add(t);
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.b, disposable)) {
                this.b = disposable;
                this.f6834a.onSubscribe(this);
            }
        }
    }

    public ObservableToList(ObservableSource<T> observableSource, int i) {
        super(observableSource);
        this.b = Functions.a(i);
    }

    public void subscribeActual(Observer<? super U> observer) {
        try {
            U call = this.b.call();
            ObjectHelper.a(call, "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
            this.f6691a.subscribe(new ToListObserver(observer, (Collection) call));
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (Observer<?>) observer);
        }
    }

    public ObservableToList(ObservableSource<T> observableSource, Callable<U> callable) {
        super(observableSource);
        this.b = callable;
    }
}
