package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableDebounce<T, U> extends AbstractObservableWithUpstream<T, T> {
    final Function<? super T, ? extends ObservableSource<U>> b;

    static final class DebounceObserver<T, U> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6718a;
        final Function<? super T, ? extends ObservableSource<U>> b;
        Disposable c;
        final AtomicReference<Disposable> d = new AtomicReference<>();
        volatile long e;
        boolean f;

        static final class DebounceInnerObserver<T, U> extends DisposableObserver<U> {
            final DebounceObserver<T, U> b;
            final long c;
            final T d;
            boolean e;
            final AtomicBoolean f = new AtomicBoolean();

            DebounceInnerObserver(DebounceObserver<T, U> debounceObserver, long j, T t) {
                this.b = debounceObserver;
                this.c = j;
                this.d = t;
            }

            /* access modifiers changed from: package-private */
            public void b() {
                if (this.f.compareAndSet(false, true)) {
                    this.b.a(this.c, this.d);
                }
            }

            public void onComplete() {
                if (!this.e) {
                    this.e = true;
                    b();
                }
            }

            public void onError(Throwable th) {
                if (this.e) {
                    RxJavaPlugins.b(th);
                    return;
                }
                this.e = true;
                this.b.onError(th);
            }

            public void onNext(U u) {
                if (!this.e) {
                    this.e = true;
                    dispose();
                    b();
                }
            }
        }

        DebounceObserver(Observer<? super T> observer, Function<? super T, ? extends ObservableSource<U>> function) {
            this.f6718a = observer;
            this.b = function;
        }

        /* access modifiers changed from: package-private */
        public void a(long j, T t) {
            if (j == this.e) {
                this.f6718a.onNext(t);
            }
        }

        public void dispose() {
            this.c.dispose();
            DisposableHelper.a(this.d);
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            if (!this.f) {
                this.f = true;
                Disposable disposable = this.d.get();
                if (disposable != DisposableHelper.DISPOSED) {
                    ((DebounceInnerObserver) disposable).b();
                    DisposableHelper.a(this.d);
                    this.f6718a.onComplete();
                }
            }
        }

        public void onError(Throwable th) {
            DisposableHelper.a(this.d);
            this.f6718a.onError(th);
        }

        public void onNext(T t) {
            if (!this.f) {
                long j = this.e + 1;
                this.e = j;
                Disposable disposable = this.d.get();
                if (disposable != null) {
                    disposable.dispose();
                }
                try {
                    Object apply = this.b.apply(t);
                    ObjectHelper.a(apply, "The ObservableSource supplied is null");
                    ObservableSource observableSource = (ObservableSource) apply;
                    DebounceInnerObserver debounceInnerObserver = new DebounceInnerObserver(this, j, t);
                    if (this.d.compareAndSet(disposable, debounceInnerObserver)) {
                        observableSource.subscribe(debounceInnerObserver);
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    dispose();
                    this.f6718a.onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6718a.onSubscribe(this);
            }
        }
    }

    public ObservableDebounce(ObservableSource<T> observableSource, Function<? super T, ? extends ObservableSource<U>> function) {
        super(observableSource);
        this.b = function;
    }

    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new DebounceObserver(new SerializedObserver(observer), this.b));
    }
}
