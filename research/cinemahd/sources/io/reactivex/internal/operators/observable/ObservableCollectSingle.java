package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class ObservableCollectSingle<T, U> extends Single<U> implements FuseToObservable<U> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6711a;
    final Callable<? extends U> b;
    final BiConsumer<? super U, ? super T> c;

    static final class CollectObserver<T, U> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final SingleObserver<? super U> f6712a;
        final BiConsumer<? super U, ? super T> b;
        final U c;
        Disposable d;
        boolean e;

        CollectObserver(SingleObserver<? super U> singleObserver, U u, BiConsumer<? super U, ? super T> biConsumer) {
            this.f6712a = singleObserver;
            this.b = biConsumer;
            this.c = u;
        }

        public void dispose() {
            this.d.dispose();
        }

        public boolean isDisposed() {
            return this.d.isDisposed();
        }

        public void onComplete() {
            if (!this.e) {
                this.e = true;
                this.f6712a.onSuccess(this.c);
            }
        }

        public void onError(Throwable th) {
            if (this.e) {
                RxJavaPlugins.b(th);
                return;
            }
            this.e = true;
            this.f6712a.onError(th);
        }

        public void onNext(T t) {
            if (!this.e) {
                try {
                    this.b.a(this.c, t);
                } catch (Throwable th) {
                    this.d.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.f6712a.onSubscribe(this);
            }
        }
    }

    public ObservableCollectSingle(ObservableSource<T> observableSource, Callable<? extends U> callable, BiConsumer<? super U, ? super T> biConsumer) {
        this.f6711a = observableSource;
        this.b = callable;
        this.c = biConsumer;
    }

    public Observable<U> a() {
        return RxJavaPlugins.a(new ObservableCollect(this.f6711a, this.b, this.c));
    }

    /* access modifiers changed from: protected */
    public void b(SingleObserver<? super U> singleObserver) {
        try {
            Object call = this.b.call();
            ObjectHelper.a(call, "The initialSupplier returned a null value");
            this.f6711a.subscribe(new CollectObserver(singleObserver, call, this.c));
        } catch (Throwable th) {
            EmptyDisposable.a(th, (SingleObserver<?>) singleObserver);
        }
    }
}
