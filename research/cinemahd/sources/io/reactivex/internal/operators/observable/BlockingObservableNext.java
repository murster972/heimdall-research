package io.reactivex.internal.operators.observable;

import io.reactivex.Notification;
import io.reactivex.ObservableSource;
import io.reactivex.internal.util.BlockingHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public final class BlockingObservableNext<T> implements Iterable<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6696a;

    static final class NextIterator<T> implements Iterator<T> {

        /* renamed from: a  reason: collision with root package name */
        private final NextObserver<T> f6697a;
        private final ObservableSource<T> b;
        private T c;
        private boolean d = true;
        private boolean e = true;
        private Throwable f;
        private boolean g;

        NextIterator(ObservableSource<T> observableSource, NextObserver<T> nextObserver) {
            this.b = observableSource;
            this.f6697a = nextObserver;
        }

        private boolean a() {
            if (!this.g) {
                this.g = true;
                this.f6697a.b();
                new ObservableMaterialize(this.b).subscribe(this.f6697a);
            }
            try {
                Notification<T> c2 = this.f6697a.c();
                if (c2.e()) {
                    this.e = false;
                    this.c = c2.b();
                    return true;
                }
                this.d = false;
                if (c2.c()) {
                    return false;
                }
                this.f = c2.a();
                throw ExceptionHelper.a(this.f);
            } catch (InterruptedException e2) {
                this.f6697a.dispose();
                this.f = e2;
                throw ExceptionHelper.a((Throwable) e2);
            }
        }

        public boolean hasNext() {
            Throwable th = this.f;
            if (th != null) {
                throw ExceptionHelper.a(th);
            } else if (!this.d) {
                return false;
            } else {
                if (!this.e || a()) {
                    return true;
                }
                return false;
            }
        }

        public T next() {
            Throwable th = this.f;
            if (th != null) {
                throw ExceptionHelper.a(th);
            } else if (hasNext()) {
                this.e = true;
                return this.c;
            } else {
                throw new NoSuchElementException("No more elements");
            }
        }

        public void remove() {
            throw new UnsupportedOperationException("Read only iterator");
        }
    }

    static final class NextObserver<T> extends DisposableObserver<Notification<T>> {
        private final BlockingQueue<Notification<T>> b = new ArrayBlockingQueue(1);
        final AtomicInteger c = new AtomicInteger();

        NextObserver() {
        }

        /* renamed from: a */
        public void onNext(Notification<T> notification) {
            if (this.c.getAndSet(0) == 1 || !notification.e()) {
                while (!this.b.offer(notification)) {
                    Notification<T> notification2 = (Notification) this.b.poll();
                    if (notification2 != null && !notification2.e()) {
                        notification = notification2;
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.c.set(1);
        }

        public Notification<T> c() throws InterruptedException {
            b();
            BlockingHelper.a();
            return this.b.take();
        }

        public void onComplete() {
        }

        public void onError(Throwable th) {
            RxJavaPlugins.b(th);
        }
    }

    public BlockingObservableNext(ObservableSource<T> observableSource) {
        this.f6696a = observableSource;
    }

    public Iterator<T> iterator() {
        return new NextIterator(this.f6696a, new NextObserver());
    }
}
