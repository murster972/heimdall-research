package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.ArrayCompositeDisposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observers.SerializedObserver;

public final class ObservableSkipUntil<T, U> extends AbstractObservableWithUpstream<T, T> {
    final ObservableSource<U> b;

    final class SkipUntil implements Observer<U> {

        /* renamed from: a  reason: collision with root package name */
        final ArrayCompositeDisposable f6820a;
        final SkipUntilObserver<T> b;
        final SerializedObserver<T> c;
        Disposable d;

        SkipUntil(ObservableSkipUntil observableSkipUntil, ArrayCompositeDisposable arrayCompositeDisposable, SkipUntilObserver<T> skipUntilObserver, SerializedObserver<T> serializedObserver) {
            this.f6820a = arrayCompositeDisposable;
            this.b = skipUntilObserver;
            this.c = serializedObserver;
        }

        public void onComplete() {
            this.b.d = true;
        }

        public void onError(Throwable th) {
            this.f6820a.dispose();
            this.c.onError(th);
        }

        public void onNext(U u) {
            this.d.dispose();
            this.b.d = true;
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.f6820a.a(1, disposable);
            }
        }
    }

    static final class SkipUntilObserver<T> implements Observer<T> {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6821a;
        final ArrayCompositeDisposable b;
        Disposable c;
        volatile boolean d;
        boolean e;

        SkipUntilObserver(Observer<? super T> observer, ArrayCompositeDisposable arrayCompositeDisposable) {
            this.f6821a = observer;
            this.b = arrayCompositeDisposable;
        }

        public void onComplete() {
            this.b.dispose();
            this.f6821a.onComplete();
        }

        public void onError(Throwable th) {
            this.b.dispose();
            this.f6821a.onError(th);
        }

        public void onNext(T t) {
            if (this.e) {
                this.f6821a.onNext(t);
            } else if (this.d) {
                this.e = true;
                this.f6821a.onNext(t);
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.b.a(0, disposable);
            }
        }
    }

    public ObservableSkipUntil(ObservableSource<T> observableSource, ObservableSource<U> observableSource2) {
        super(observableSource);
        this.b = observableSource2;
    }

    public void subscribeActual(Observer<? super T> observer) {
        SerializedObserver serializedObserver = new SerializedObserver(observer);
        ArrayCompositeDisposable arrayCompositeDisposable = new ArrayCompositeDisposable(2);
        serializedObserver.onSubscribe(arrayCompositeDisposable);
        SkipUntilObserver skipUntilObserver = new SkipUntilObserver(serializedObserver, arrayCompositeDisposable);
        this.b.subscribe(new SkipUntil(this, arrayCompositeDisposable, skipUntilObserver, serializedObserver));
        this.f6691a.subscribe(skipUntilObserver);
    }
}
