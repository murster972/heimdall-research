package io.reactivex.internal.operators.observable;

import com.facebook.common.time.Clock;
import io.reactivex.FlowableSubscriber;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;

public final class ObservableFromPublisher<T> extends Observable<T> {

    /* renamed from: a  reason: collision with root package name */
    final Publisher<? extends T> f6747a;

    static final class PublisherSubscriber<T> implements FlowableSubscriber<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6748a;
        Subscription b;

        PublisherSubscriber(Observer<? super T> observer) {
            this.f6748a = observer;
        }

        public void a(Subscription subscription) {
            if (SubscriptionHelper.a(this.b, subscription)) {
                this.b = subscription;
                this.f6748a.onSubscribe(this);
                subscription.request(Clock.MAX_TIME);
            }
        }

        public void dispose() {
            this.b.cancel();
            this.b = SubscriptionHelper.CANCELLED;
        }

        public boolean isDisposed() {
            return this.b == SubscriptionHelper.CANCELLED;
        }

        public void onComplete() {
            this.f6748a.onComplete();
        }

        public void onError(Throwable th) {
            this.f6748a.onError(th);
        }

        public void onNext(T t) {
            this.f6748a.onNext(t);
        }
    }

    public ObservableFromPublisher(Publisher<? extends T> publisher) {
        this.f6747a = publisher;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        this.f6747a.a(new PublisherSubscriber(observer));
    }
}
