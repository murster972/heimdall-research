package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.util.EmptyComponent;

public final class ObservableDetach<T> extends AbstractObservableWithUpstream<T, T> {

    static final class DetachObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        Observer<? super T> f6729a;
        Disposable b;

        DetachObserver(Observer<? super T> observer) {
            this.f6729a = observer;
        }

        public void dispose() {
            Disposable disposable = this.b;
            this.b = EmptyComponent.INSTANCE;
            this.f6729a = EmptyComponent.a();
            disposable.dispose();
        }

        public boolean isDisposed() {
            return this.b.isDisposed();
        }

        public void onComplete() {
            Observer<? super T> observer = this.f6729a;
            this.b = EmptyComponent.INSTANCE;
            this.f6729a = EmptyComponent.a();
            observer.onComplete();
        }

        public void onError(Throwable th) {
            Observer<? super T> observer = this.f6729a;
            this.b = EmptyComponent.INSTANCE;
            this.f6729a = EmptyComponent.a();
            observer.onError(th);
        }

        public void onNext(T t) {
            this.f6729a.onNext(t);
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.b, disposable)) {
                this.b = disposable;
                this.f6729a.onSubscribe(this);
            }
        }
    }

    public ObservableDetach(ObservableSource<T> observableSource) {
        super(observableSource);
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new DetachObserver(observer));
    }
}
