package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableAll<T> extends AbstractObservableWithUpstream<T, Boolean> {
    final Predicate<? super T> b;

    static final class AllObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super Boolean> f6698a;
        final Predicate<? super T> b;
        Disposable c;
        boolean d;

        AllObserver(Observer<? super Boolean> observer, Predicate<? super T> predicate) {
            this.f6698a = observer;
            this.b = predicate;
        }

        public void dispose() {
            this.c.dispose();
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            if (!this.d) {
                this.d = true;
                this.f6698a.onNext(true);
                this.f6698a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.d) {
                RxJavaPlugins.b(th);
                return;
            }
            this.d = true;
            this.f6698a.onError(th);
        }

        public void onNext(T t) {
            if (!this.d) {
                try {
                    if (!this.b.a(t)) {
                        this.d = true;
                        this.c.dispose();
                        this.f6698a.onNext(false);
                        this.f6698a.onComplete();
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.c.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6698a.onSubscribe(this);
            }
        }
    }

    public ObservableAll(ObservableSource<T> observableSource, Predicate<? super T> predicate) {
        super(observableSource);
        this.b = predicate;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super Boolean> observer) {
        this.f6691a.subscribe(new AllObserver(observer, this.b));
    }
}
