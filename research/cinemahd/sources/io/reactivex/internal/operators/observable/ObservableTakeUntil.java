package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.HalfSerializer;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableTakeUntil<T, U> extends AbstractObservableWithUpstream<T, T> {
    final ObservableSource<? extends U> b;

    public ObservableTakeUntil(ObservableSource<T> observableSource, ObservableSource<? extends U> observableSource2) {
        super(observableSource);
        this.b = observableSource2;
    }

    public void subscribeActual(Observer<? super T> observer) {
        TakeUntilMainObserver takeUntilMainObserver = new TakeUntilMainObserver(observer);
        observer.onSubscribe(takeUntilMainObserver);
        this.b.subscribe(takeUntilMainObserver.otherObserver);
        this.f6691a.subscribe(takeUntilMainObserver);
    }

    static final class TakeUntilMainObserver<T, U> extends AtomicInteger implements Observer<T>, Disposable {
        private static final long serialVersionUID = 1418547743690811973L;
        final Observer<? super T> downstream;
        final AtomicThrowable error = new AtomicThrowable();
        final TakeUntilMainObserver<T, U>.OtherObserver otherObserver = new OtherObserver();
        final AtomicReference<Disposable> upstream = new AtomicReference<>();

        final class OtherObserver extends AtomicReference<Disposable> implements Observer<U> {
            private static final long serialVersionUID = -8693423678067375039L;

            OtherObserver() {
            }

            public void onComplete() {
                TakeUntilMainObserver.this.a();
            }

            public void onError(Throwable th) {
                TakeUntilMainObserver.this.a(th);
            }

            public void onNext(U u) {
                DisposableHelper.a((AtomicReference<Disposable>) this);
                TakeUntilMainObserver.this.a();
            }

            public void onSubscribe(Disposable disposable) {
                DisposableHelper.c(this, disposable);
            }
        }

        TakeUntilMainObserver(Observer<? super T> observer) {
            this.downstream = observer;
        }

        /* access modifiers changed from: package-private */
        public void a(Throwable th) {
            DisposableHelper.a(this.upstream);
            HalfSerializer.a((Observer<?>) this.downstream, th, (AtomicInteger) this, this.error);
        }

        public void dispose() {
            DisposableHelper.a(this.upstream);
            DisposableHelper.a((AtomicReference<Disposable>) this.otherObserver);
        }

        public boolean isDisposed() {
            return DisposableHelper.a(this.upstream.get());
        }

        public void onComplete() {
            DisposableHelper.a((AtomicReference<Disposable>) this.otherObserver);
            HalfSerializer.a((Observer<?>) this.downstream, (AtomicInteger) this, this.error);
        }

        public void onError(Throwable th) {
            DisposableHelper.a((AtomicReference<Disposable>) this.otherObserver);
            HalfSerializer.a((Observer<?>) this.downstream, th, (AtomicInteger) this, this.error);
        }

        public void onNext(T t) {
            HalfSerializer.a(this.downstream, t, (AtomicInteger) this, this.error);
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.upstream, disposable);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            DisposableHelper.a(this.upstream);
            HalfSerializer.a((Observer<?>) this.downstream, (AtomicInteger) this, this.error);
        }
    }
}
