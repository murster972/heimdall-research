package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableDelaySubscriptionOther<T, U> extends Observable<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<? extends T> f6725a;
    final ObservableSource<U> b;

    final class DelayObserver implements Observer<U> {

        /* renamed from: a  reason: collision with root package name */
        final SequentialDisposable f6726a;
        final Observer<? super T> b;
        boolean c;

        final class OnComplete implements Observer<T> {
            OnComplete() {
            }

            public void onComplete() {
                DelayObserver.this.b.onComplete();
            }

            public void onError(Throwable th) {
                DelayObserver.this.b.onError(th);
            }

            public void onNext(T t) {
                DelayObserver.this.b.onNext(t);
            }

            public void onSubscribe(Disposable disposable) {
                DelayObserver.this.f6726a.b(disposable);
            }
        }

        DelayObserver(SequentialDisposable sequentialDisposable, Observer<? super T> observer) {
            this.f6726a = sequentialDisposable;
            this.b = observer;
        }

        public void onComplete() {
            if (!this.c) {
                this.c = true;
                ObservableDelaySubscriptionOther.this.f6725a.subscribe(new OnComplete());
            }
        }

        public void onError(Throwable th) {
            if (this.c) {
                RxJavaPlugins.b(th);
                return;
            }
            this.c = true;
            this.b.onError(th);
        }

        public void onNext(U u) {
            onComplete();
        }

        public void onSubscribe(Disposable disposable) {
            this.f6726a.b(disposable);
        }
    }

    public ObservableDelaySubscriptionOther(ObservableSource<? extends T> observableSource, ObservableSource<U> observableSource2) {
        this.f6725a = observableSource;
        this.b = observableSource2;
    }

    public void subscribeActual(Observer<? super T> observer) {
        SequentialDisposable sequentialDisposable = new SequentialDisposable();
        observer.onSubscribe(sequentialDisposable);
        this.b.subscribe(new DelayObserver(sequentialDisposable, observer));
    }
}
