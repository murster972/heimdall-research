package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableTakeWhile<T> extends AbstractObservableWithUpstream<T, T> {
    final Predicate<? super T> b;

    static final class TakeWhileObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6829a;
        final Predicate<? super T> b;
        Disposable c;
        boolean d;

        TakeWhileObserver(Observer<? super T> observer, Predicate<? super T> predicate) {
            this.f6829a = observer;
            this.b = predicate;
        }

        public void dispose() {
            this.c.dispose();
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            if (!this.d) {
                this.d = true;
                this.f6829a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.d) {
                RxJavaPlugins.b(th);
                return;
            }
            this.d = true;
            this.f6829a.onError(th);
        }

        public void onNext(T t) {
            if (!this.d) {
                try {
                    if (!this.b.a(t)) {
                        this.d = true;
                        this.c.dispose();
                        this.f6829a.onComplete();
                        return;
                    }
                    this.f6829a.onNext(t);
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.c.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6829a.onSubscribe(this);
            }
        }
    }

    public ObservableTakeWhile(ObservableSource<T> observableSource, Predicate<? super T> predicate) {
        super(observableSource);
        this.b = predicate;
    }

    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new TakeWhileObserver(observer, this.b));
    }
}
