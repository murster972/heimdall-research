package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.BasicQueueDisposable;
import java.util.Iterator;

public final class ObservableFromIterable<T> extends Observable<T> {

    /* renamed from: a  reason: collision with root package name */
    final Iterable<? extends T> f6745a;

    public ObservableFromIterable(Iterable<? extends T> iterable) {
        this.f6745a = iterable;
    }

    public void subscribeActual(Observer<? super T> observer) {
        try {
            Iterator<? extends T> it2 = this.f6745a.iterator();
            try {
                if (!it2.hasNext()) {
                    EmptyDisposable.a((Observer<?>) observer);
                    return;
                }
                FromIterableDisposable fromIterableDisposable = new FromIterableDisposable(observer, it2);
                observer.onSubscribe(fromIterableDisposable);
                if (!fromIterableDisposable.d) {
                    fromIterableDisposable.a();
                }
            } catch (Throwable th) {
                Exceptions.b(th);
                EmptyDisposable.a(th, (Observer<?>) observer);
            }
        } catch (Throwable th2) {
            Exceptions.b(th2);
            EmptyDisposable.a(th2, (Observer<?>) observer);
        }
    }

    static final class FromIterableDisposable<T> extends BasicQueueDisposable<T> {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6746a;
        final Iterator<? extends T> b;
        volatile boolean c;
        boolean d;
        boolean e;
        boolean f;

        FromIterableDisposable(Observer<? super T> observer, Iterator<? extends T> it2) {
            this.f6746a = observer;
            this.b = it2;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            while (!isDisposed()) {
                try {
                    Object next = this.b.next();
                    ObjectHelper.a(next, "The iterator returned a null value");
                    this.f6746a.onNext(next);
                    if (!isDisposed()) {
                        try {
                            if (!this.b.hasNext()) {
                                if (!isDisposed()) {
                                    this.f6746a.onComplete();
                                    return;
                                }
                                return;
                            }
                        } catch (Throwable th) {
                            Exceptions.b(th);
                            this.f6746a.onError(th);
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable th2) {
                    Exceptions.b(th2);
                    this.f6746a.onError(th2);
                    return;
                }
            }
        }

        public void clear() {
            this.e = true;
        }

        public void dispose() {
            this.c = true;
        }

        public boolean isDisposed() {
            return this.c;
        }

        public boolean isEmpty() {
            return this.e;
        }

        public T poll() {
            if (this.e) {
                return null;
            }
            if (!this.f) {
                this.f = true;
            } else if (!this.b.hasNext()) {
                this.e = true;
                return null;
            }
            T next = this.b.next();
            ObjectHelper.a(next, "The iterator returned a null value");
            return next;
        }

        public int a(int i) {
            if ((i & 1) == 0) {
                return 0;
            }
            this.d = true;
            return 1;
        }
    }
}
