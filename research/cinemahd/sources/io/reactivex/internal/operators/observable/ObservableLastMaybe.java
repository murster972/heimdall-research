package io.reactivex.internal.operators.observable;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableLastMaybe<T> extends Maybe<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6778a;

    static final class LastObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final MaybeObserver<? super T> f6779a;
        Disposable b;
        T c;

        LastObserver(MaybeObserver<? super T> maybeObserver) {
            this.f6779a = maybeObserver;
        }

        public void dispose() {
            this.b.dispose();
            this.b = DisposableHelper.DISPOSED;
        }

        public boolean isDisposed() {
            return this.b == DisposableHelper.DISPOSED;
        }

        public void onComplete() {
            this.b = DisposableHelper.DISPOSED;
            T t = this.c;
            if (t != null) {
                this.c = null;
                this.f6779a.onSuccess(t);
                return;
            }
            this.f6779a.onComplete();
        }

        public void onError(Throwable th) {
            this.b = DisposableHelper.DISPOSED;
            this.c = null;
            this.f6779a.onError(th);
        }

        public void onNext(T t) {
            this.c = t;
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.b, disposable)) {
                this.b = disposable;
                this.f6779a.onSubscribe(this);
            }
        }
    }

    public ObservableLastMaybe(ObservableSource<T> observableSource) {
        this.f6778a = observableSource;
    }

    /* access modifiers changed from: protected */
    public void b(MaybeObserver<? super T> maybeObserver) {
        this.f6778a.subscribe(new LastObserver(maybeObserver));
    }
}
