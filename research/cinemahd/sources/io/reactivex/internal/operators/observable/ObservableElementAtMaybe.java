package io.reactivex.internal.operators.observable;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableElementAtMaybe<T> extends Maybe<T> implements FuseToObservable<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6732a;
    final long b;

    static final class ElementAtObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final MaybeObserver<? super T> f6733a;
        final long b;
        Disposable c;
        long d;
        boolean e;

        ElementAtObserver(MaybeObserver<? super T> maybeObserver, long j) {
            this.f6733a = maybeObserver;
            this.b = j;
        }

        public void dispose() {
            this.c.dispose();
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            if (!this.e) {
                this.e = true;
                this.f6733a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.e) {
                RxJavaPlugins.b(th);
                return;
            }
            this.e = true;
            this.f6733a.onError(th);
        }

        public void onNext(T t) {
            if (!this.e) {
                long j = this.d;
                if (j == this.b) {
                    this.e = true;
                    this.c.dispose();
                    this.f6733a.onSuccess(t);
                    return;
                }
                this.d = j + 1;
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6733a.onSubscribe(this);
            }
        }
    }

    public ObservableElementAtMaybe(ObservableSource<T> observableSource, long j) {
        this.f6732a = observableSource;
        this.b = j;
    }

    public Observable<T> a() {
        return RxJavaPlugins.a(new ObservableElementAt(this.f6732a, this.b, null, false));
    }

    public void b(MaybeObserver<? super T> maybeObserver) {
        this.f6732a.subscribe(new ElementAtObserver(maybeObserver, this.b));
    }
}
