package io.reactivex.internal.operators.observable;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableSingleMaybe<T> extends Maybe<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6815a;

    static final class SingleElementObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final MaybeObserver<? super T> f6816a;
        Disposable b;
        T c;
        boolean d;

        SingleElementObserver(MaybeObserver<? super T> maybeObserver) {
            this.f6816a = maybeObserver;
        }

        public void dispose() {
            this.b.dispose();
        }

        public boolean isDisposed() {
            return this.b.isDisposed();
        }

        public void onComplete() {
            if (!this.d) {
                this.d = true;
                T t = this.c;
                this.c = null;
                if (t == null) {
                    this.f6816a.onComplete();
                } else {
                    this.f6816a.onSuccess(t);
                }
            }
        }

        public void onError(Throwable th) {
            if (this.d) {
                RxJavaPlugins.b(th);
                return;
            }
            this.d = true;
            this.f6816a.onError(th);
        }

        public void onNext(T t) {
            if (!this.d) {
                if (this.c != null) {
                    this.d = true;
                    this.b.dispose();
                    this.f6816a.onError(new IllegalArgumentException("Sequence contains more than one element!"));
                    return;
                }
                this.c = t;
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.b, disposable)) {
                this.b = disposable;
                this.f6816a.onSubscribe(this);
            }
        }
    }

    public ObservableSingleMaybe(ObservableSource<T> observableSource) {
        this.f6815a = observableSource;
    }

    public void b(MaybeObserver<? super T> maybeObserver) {
        this.f6815a.subscribe(new SingleElementObserver(maybeObserver));
    }
}
