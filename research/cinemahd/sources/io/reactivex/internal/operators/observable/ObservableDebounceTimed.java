package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableDebounceTimed<T> extends AbstractObservableWithUpstream<T, T> {
    final long b;
    final TimeUnit c;
    final Scheduler d;

    static final class DebounceEmitter<T> extends AtomicReference<Disposable> implements Runnable, Disposable {
        private static final long serialVersionUID = 6812032969491025141L;
        final long idx;
        final AtomicBoolean once = new AtomicBoolean();
        final DebounceTimedObserver<T> parent;
        final T value;

        DebounceEmitter(T t, long j, DebounceTimedObserver<T> debounceTimedObserver) {
            this.value = t;
            this.idx = j;
            this.parent = debounceTimedObserver;
        }

        public void a(Disposable disposable) {
            DisposableHelper.a((AtomicReference<Disposable>) this, disposable);
        }

        public void dispose() {
            DisposableHelper.a((AtomicReference<Disposable>) this);
        }

        public boolean isDisposed() {
            return get() == DisposableHelper.DISPOSED;
        }

        public void run() {
            if (this.once.compareAndSet(false, true)) {
                this.parent.a(this.idx, this.value, this);
            }
        }
    }

    static final class DebounceTimedObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6719a;
        final long b;
        final TimeUnit c;
        final Scheduler.Worker d;
        Disposable e;
        Disposable f;
        volatile long g;
        boolean h;

        DebounceTimedObserver(Observer<? super T> observer, long j, TimeUnit timeUnit, Scheduler.Worker worker) {
            this.f6719a = observer;
            this.b = j;
            this.c = timeUnit;
            this.d = worker;
        }

        /* access modifiers changed from: package-private */
        public void a(long j, T t, DebounceEmitter<T> debounceEmitter) {
            if (j == this.g) {
                this.f6719a.onNext(t);
                debounceEmitter.dispose();
            }
        }

        public void dispose() {
            this.e.dispose();
            this.d.dispose();
        }

        public boolean isDisposed() {
            return this.d.isDisposed();
        }

        public void onComplete() {
            if (!this.h) {
                this.h = true;
                Disposable disposable = this.f;
                if (disposable != null) {
                    disposable.dispose();
                }
                DebounceEmitter debounceEmitter = (DebounceEmitter) disposable;
                if (debounceEmitter != null) {
                    debounceEmitter.run();
                }
                this.f6719a.onComplete();
                this.d.dispose();
            }
        }

        public void onError(Throwable th) {
            if (this.h) {
                RxJavaPlugins.b(th);
                return;
            }
            Disposable disposable = this.f;
            if (disposable != null) {
                disposable.dispose();
            }
            this.h = true;
            this.f6719a.onError(th);
            this.d.dispose();
        }

        public void onNext(T t) {
            if (!this.h) {
                long j = this.g + 1;
                this.g = j;
                Disposable disposable = this.f;
                if (disposable != null) {
                    disposable.dispose();
                }
                DebounceEmitter debounceEmitter = new DebounceEmitter(t, j, this);
                this.f = debounceEmitter;
                debounceEmitter.a(this.d.a(debounceEmitter, this.b, this.c));
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.e, disposable)) {
                this.e = disposable;
                this.f6719a.onSubscribe(this);
            }
        }
    }

    public ObservableDebounceTimed(ObservableSource<T> observableSource, long j, TimeUnit timeUnit, Scheduler scheduler) {
        super(observableSource);
        this.b = j;
        this.c = timeUnit;
        this.d = scheduler;
    }

    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new DebounceTimedObserver(new SerializedObserver(observer), this.b, this.c, this.d.a()));
    }
}
