package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.concurrent.Callable;

public final class ObservableToListSingle<T, U extends Collection<? super T>> extends Single<U> implements FuseToObservable<U> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6835a;
    final Callable<U> b;

    static final class ToListObserver<T, U extends Collection<? super T>> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final SingleObserver<? super U> f6836a;
        U b;
        Disposable c;

        ToListObserver(SingleObserver<? super U> singleObserver, U u) {
            this.f6836a = singleObserver;
            this.b = u;
        }

        public void dispose() {
            this.c.dispose();
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            U u = this.b;
            this.b = null;
            this.f6836a.onSuccess(u);
        }

        public void onError(Throwable th) {
            this.b = null;
            this.f6836a.onError(th);
        }

        public void onNext(T t) {
            this.b.add(t);
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6836a.onSubscribe(this);
            }
        }
    }

    public ObservableToListSingle(ObservableSource<T> observableSource, int i) {
        this.f6835a = observableSource;
        this.b = Functions.a(i);
    }

    public Observable<U> a() {
        return RxJavaPlugins.a(new ObservableToList(this.f6835a, this.b));
    }

    public void b(SingleObserver<? super U> singleObserver) {
        try {
            U call = this.b.call();
            ObjectHelper.a(call, "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
            this.f6835a.subscribe(new ToListObserver(singleObserver, (Collection) call));
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (SingleObserver<?>) singleObserver);
        }
    }

    public ObservableToListSingle(ObservableSource<T> observableSource, Callable<U> callable) {
        this.f6835a = observableSource;
        this.b = callable;
    }
}
