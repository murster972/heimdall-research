package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.NoSuchElementException;

public final class ObservableSingleSingle<T> extends Single<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<? extends T> f6817a;
    final T b;

    static final class SingleElementObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final SingleObserver<? super T> f6818a;
        final T b;
        Disposable c;
        T d;
        boolean e;

        SingleElementObserver(SingleObserver<? super T> singleObserver, T t) {
            this.f6818a = singleObserver;
            this.b = t;
        }

        public void dispose() {
            this.c.dispose();
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            if (!this.e) {
                this.e = true;
                T t = this.d;
                this.d = null;
                if (t == null) {
                    t = this.b;
                }
                if (t != null) {
                    this.f6818a.onSuccess(t);
                } else {
                    this.f6818a.onError(new NoSuchElementException());
                }
            }
        }

        public void onError(Throwable th) {
            if (this.e) {
                RxJavaPlugins.b(th);
                return;
            }
            this.e = true;
            this.f6818a.onError(th);
        }

        public void onNext(T t) {
            if (!this.e) {
                if (this.d != null) {
                    this.e = true;
                    this.c.dispose();
                    this.f6818a.onError(new IllegalArgumentException("Sequence contains more than one element!"));
                    return;
                }
                this.d = t;
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6818a.onSubscribe(this);
            }
        }
    }

    public ObservableSingleSingle(ObservableSource<? extends T> observableSource, T t) {
        this.f6817a = observableSource;
        this.b = t;
    }

    public void b(SingleObserver<? super T> singleObserver) {
        this.f6817a.subscribe(new SingleElementObserver(singleObserver, this.b));
    }
}
