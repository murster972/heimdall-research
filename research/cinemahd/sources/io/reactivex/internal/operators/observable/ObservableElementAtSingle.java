package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.NoSuchElementException;

public final class ObservableElementAtSingle<T> extends Single<T> implements FuseToObservable<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6734a;
    final long b;
    final T c;

    static final class ElementAtObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final SingleObserver<? super T> f6735a;
        final long b;
        final T c;
        Disposable d;
        long e;
        boolean f;

        ElementAtObserver(SingleObserver<? super T> singleObserver, long j, T t) {
            this.f6735a = singleObserver;
            this.b = j;
            this.c = t;
        }

        public void dispose() {
            this.d.dispose();
        }

        public boolean isDisposed() {
            return this.d.isDisposed();
        }

        public void onComplete() {
            if (!this.f) {
                this.f = true;
                T t = this.c;
                if (t != null) {
                    this.f6735a.onSuccess(t);
                } else {
                    this.f6735a.onError(new NoSuchElementException());
                }
            }
        }

        public void onError(Throwable th) {
            if (this.f) {
                RxJavaPlugins.b(th);
                return;
            }
            this.f = true;
            this.f6735a.onError(th);
        }

        public void onNext(T t) {
            if (!this.f) {
                long j = this.e;
                if (j == this.b) {
                    this.f = true;
                    this.d.dispose();
                    this.f6735a.onSuccess(t);
                    return;
                }
                this.e = j + 1;
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.f6735a.onSubscribe(this);
            }
        }
    }

    public ObservableElementAtSingle(ObservableSource<T> observableSource, long j, T t) {
        this.f6734a = observableSource;
        this.b = j;
        this.c = t;
    }

    public Observable<T> a() {
        return RxJavaPlugins.a(new ObservableElementAt(this.f6734a, this.b, this.c, true));
    }

    public void b(SingleObserver<? super T> singleObserver) {
        this.f6734a.subscribe(new ElementAtObserver(singleObserver, this.b, this.c));
    }
}
