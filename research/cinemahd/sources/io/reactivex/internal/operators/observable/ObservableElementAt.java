package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.NoSuchElementException;

public final class ObservableElementAt<T> extends AbstractObservableWithUpstream<T, T> {
    final long b;
    final T c;
    final boolean d;

    static final class ElementAtObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6731a;
        final long b;
        final T c;
        final boolean d;
        Disposable e;
        long f;
        boolean g;

        ElementAtObserver(Observer<? super T> observer, long j, T t, boolean z) {
            this.f6731a = observer;
            this.b = j;
            this.c = t;
            this.d = z;
        }

        public void dispose() {
            this.e.dispose();
        }

        public boolean isDisposed() {
            return this.e.isDisposed();
        }

        public void onComplete() {
            if (!this.g) {
                this.g = true;
                T t = this.c;
                if (t != null || !this.d) {
                    if (t != null) {
                        this.f6731a.onNext(t);
                    }
                    this.f6731a.onComplete();
                    return;
                }
                this.f6731a.onError(new NoSuchElementException());
            }
        }

        public void onError(Throwable th) {
            if (this.g) {
                RxJavaPlugins.b(th);
                return;
            }
            this.g = true;
            this.f6731a.onError(th);
        }

        public void onNext(T t) {
            if (!this.g) {
                long j = this.f;
                if (j == this.b) {
                    this.g = true;
                    this.e.dispose();
                    this.f6731a.onNext(t);
                    this.f6731a.onComplete();
                    return;
                }
                this.f = j + 1;
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.e, disposable)) {
                this.e = disposable;
                this.f6731a.onSubscribe(this);
            }
        }
    }

    public ObservableElementAt(ObservableSource<T> observableSource, long j, T t, boolean z) {
        super(observableSource);
        this.b = j;
        this.c = t;
        this.d = z;
    }

    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new ElementAtObserver(observer, this.b, this.c, this.d));
    }
}
