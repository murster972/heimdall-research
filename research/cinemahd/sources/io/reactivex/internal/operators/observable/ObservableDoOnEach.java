package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableDoOnEach<T> extends AbstractObservableWithUpstream<T, T> {
    final Consumer<? super T> b;
    final Consumer<? super Throwable> c;
    final Action d;
    final Action e;

    static final class DoOnEachObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6730a;
        final Consumer<? super T> b;
        final Consumer<? super Throwable> c;
        final Action d;
        final Action e;
        Disposable f;
        boolean g;

        DoOnEachObserver(Observer<? super T> observer, Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, Action action, Action action2) {
            this.f6730a = observer;
            this.b = consumer;
            this.c = consumer2;
            this.d = action;
            this.e = action2;
        }

        public void dispose() {
            this.f.dispose();
        }

        public boolean isDisposed() {
            return this.f.isDisposed();
        }

        public void onComplete() {
            if (!this.g) {
                try {
                    this.d.run();
                    this.g = true;
                    this.f6730a.onComplete();
                    try {
                        this.e.run();
                    } catch (Throwable th) {
                        Exceptions.b(th);
                        RxJavaPlugins.b(th);
                    }
                } catch (Throwable th2) {
                    Exceptions.b(th2);
                    onError(th2);
                }
            }
        }

        public void onError(Throwable th) {
            if (this.g) {
                RxJavaPlugins.b(th);
                return;
            }
            this.g = true;
            try {
                this.c.accept(th);
            } catch (Throwable th2) {
                Exceptions.b(th2);
                th = new CompositeException(th, th2);
            }
            this.f6730a.onError(th);
            try {
                this.e.run();
            } catch (Throwable th3) {
                Exceptions.b(th3);
                RxJavaPlugins.b(th3);
            }
        }

        public void onNext(T t) {
            if (!this.g) {
                try {
                    this.b.accept(t);
                    this.f6730a.onNext(t);
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.f.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.f, disposable)) {
                this.f = disposable;
                this.f6730a.onSubscribe(this);
            }
        }
    }

    public ObservableDoOnEach(ObservableSource<T> observableSource, Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, Action action, Action action2) {
        super(observableSource);
        this.b = consumer;
        this.c = consumer2;
        this.d = action;
        this.e = action2;
    }

    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new DoOnEachObserver(observer, this.b, this.c, this.d, this.e));
    }
}
