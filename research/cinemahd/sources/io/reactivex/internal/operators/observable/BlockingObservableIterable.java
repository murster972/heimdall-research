package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.BlockingHelper;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class BlockingObservableIterable<T> implements Iterable<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<? extends T> f6692a;
    final int b;

    static final class BlockingObservableIterator<T> extends AtomicReference<Disposable> implements Observer<T>, Iterator<T>, Disposable {
        private static final long serialVersionUID = 6695226475494099826L;
        final Condition condition = this.lock.newCondition();
        volatile boolean done;
        Throwable error;
        final Lock lock = new ReentrantLock();
        final SpscLinkedArrayQueue<T> queue;

        BlockingObservableIterator(int i) {
            this.queue = new SpscLinkedArrayQueue<>(i);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.lock.lock();
            try {
                this.condition.signalAll();
            } finally {
                this.lock.unlock();
            }
        }

        public void dispose() {
            DisposableHelper.a((AtomicReference<Disposable>) this);
        }

        public boolean hasNext() {
            while (true) {
                boolean z = this.done;
                boolean isEmpty = this.queue.isEmpty();
                if (z) {
                    Throwable th = this.error;
                    if (th != null) {
                        throw ExceptionHelper.a(th);
                    } else if (isEmpty) {
                        return false;
                    }
                }
                if (!isEmpty) {
                    return true;
                }
                BlockingHelper.a();
                this.lock.lock();
                while (!this.done && this.queue.isEmpty()) {
                    try {
                        this.condition.await();
                    } catch (InterruptedException e) {
                        DisposableHelper.a((AtomicReference<Disposable>) this);
                        a();
                        throw ExceptionHelper.a((Throwable) e);
                    } catch (Throwable th2) {
                        this.lock.unlock();
                        throw th2;
                    }
                }
                this.lock.unlock();
            }
        }

        public boolean isDisposed() {
            return DisposableHelper.a((Disposable) get());
        }

        public T next() {
            if (hasNext()) {
                return this.queue.poll();
            }
            throw new NoSuchElementException();
        }

        public void onComplete() {
            this.done = true;
            a();
        }

        public void onError(Throwable th) {
            this.error = th;
            this.done = true;
            a();
        }

        public void onNext(T t) {
            this.queue.offer(t);
            a();
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this, disposable);
        }

        public void remove() {
            throw new UnsupportedOperationException("remove");
        }
    }

    public BlockingObservableIterable(ObservableSource<? extends T> observableSource, int i) {
        this.f6692a = observableSource;
        this.b = i;
    }

    public Iterator<T> iterator() {
        BlockingObservableIterator blockingObservableIterator = new BlockingObservableIterator(this.b);
        this.f6692a.subscribe(blockingObservableIterator);
        return blockingObservableIterator;
    }
}
