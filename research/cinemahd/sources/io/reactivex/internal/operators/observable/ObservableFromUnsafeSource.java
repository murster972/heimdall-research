package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;

public final class ObservableFromUnsafeSource<T> extends Observable<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6749a;

    public ObservableFromUnsafeSource(ObservableSource<T> observableSource) {
        this.f6749a = observableSource;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        this.f6749a.subscribe(observer);
    }
}
