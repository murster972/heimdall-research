package io.reactivex.internal.operators.observable;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableReduceMaybe<T> extends Maybe<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6793a;
    final BiFunction<T, T, T> b;

    static final class ReduceObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final MaybeObserver<? super T> f6794a;
        final BiFunction<T, T, T> b;
        boolean c;
        T d;
        Disposable e;

        ReduceObserver(MaybeObserver<? super T> maybeObserver, BiFunction<T, T, T> biFunction) {
            this.f6794a = maybeObserver;
            this.b = biFunction;
        }

        public void dispose() {
            this.e.dispose();
        }

        public boolean isDisposed() {
            return this.e.isDisposed();
        }

        public void onComplete() {
            if (!this.c) {
                this.c = true;
                T t = this.d;
                this.d = null;
                if (t != null) {
                    this.f6794a.onSuccess(t);
                } else {
                    this.f6794a.onComplete();
                }
            }
        }

        public void onError(Throwable th) {
            if (this.c) {
                RxJavaPlugins.b(th);
                return;
            }
            this.c = true;
            this.d = null;
            this.f6794a.onError(th);
        }

        public void onNext(T t) {
            if (!this.c) {
                T t2 = this.d;
                if (t2 == null) {
                    this.d = t;
                    return;
                }
                try {
                    T a2 = this.b.a(t2, t);
                    ObjectHelper.a(a2, "The reducer returned a null value");
                    this.d = a2;
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.e.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.e, disposable)) {
                this.e = disposable;
                this.f6794a.onSubscribe(this);
            }
        }
    }

    public ObservableReduceMaybe(ObservableSource<T> observableSource, BiFunction<T, T, T> biFunction) {
        this.f6793a = observableSource;
        this.b = biFunction;
    }

    /* access modifiers changed from: protected */
    public void b(MaybeObserver<? super T> maybeObserver) {
        this.f6793a.subscribe(new ReduceObserver(maybeObserver, this.b));
    }
}
