package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.Callable;

public final class ObservableError<T> extends Observable<T> {

    /* renamed from: a  reason: collision with root package name */
    final Callable<? extends Throwable> f6737a;

    public ObservableError(Callable<? extends Throwable> callable) {
        this.f6737a = callable;
    }

    public void subscribeActual(Observer<? super T> observer) {
        try {
            Object call = this.f6737a.call();
            ObjectHelper.a(call, "Callable returned null throwable. Null values are generally not allowed in 2.x operators and sources.");
            th = (Throwable) call;
        } catch (Throwable th) {
            th = th;
            Exceptions.b(th);
        }
        EmptyDisposable.a(th, (Observer<?>) observer);
    }
}
