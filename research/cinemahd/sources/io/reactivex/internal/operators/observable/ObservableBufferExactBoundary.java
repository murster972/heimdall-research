package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import java.util.Collection;
import java.util.concurrent.Callable;

public final class ObservableBufferExactBoundary<T, U extends Collection<? super T>, B> extends AbstractObservableWithUpstream<T, U> {
    final ObservableSource<B> b;
    final Callable<U> c;

    static final class BufferBoundaryObserver<T, U extends Collection<? super T>, B> extends DisposableObserver<B> {
        final BufferExactBoundaryObserver<T, U, B> b;

        BufferBoundaryObserver(BufferExactBoundaryObserver<T, U, B> bufferExactBoundaryObserver) {
            this.b = bufferExactBoundaryObserver;
        }

        public void onComplete() {
            this.b.onComplete();
        }

        public void onError(Throwable th) {
            this.b.onError(th);
        }

        public void onNext(B b2) {
            this.b.f();
        }
    }

    static final class BufferExactBoundaryObserver<T, U extends Collection<? super T>, B> extends QueueDrainObserver<T, U, U> implements Observer<T>, Disposable {
        final Callable<U> g;
        final ObservableSource<B> h;
        Disposable i;
        Disposable j;
        U k;

        BufferExactBoundaryObserver(Observer<? super U> observer, Callable<U> callable, ObservableSource<B> observableSource) {
            super(observer, new MpscLinkedQueue());
            this.g = callable;
            this.h = observableSource;
        }

        public void dispose() {
            if (!this.d) {
                this.d = true;
                this.j.dispose();
                this.i.dispose();
                if (d()) {
                    this.c.clear();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
            try {
                U call = this.g.call();
                ObjectHelper.a(call, "The buffer supplied is null");
                U u = (Collection) call;
                synchronized (this) {
                    U u2 = this.k;
                    if (u2 != null) {
                        this.k = u;
                        a(u2, false, this);
                    }
                }
            } catch (Throwable th) {
                Exceptions.b(th);
                dispose();
                this.b.onError(th);
            }
        }

        public boolean isDisposed() {
            return this.d;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
            io.reactivex.internal.util.QueueDrainHelper.a(r3.c, r3.b, false, r3, r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x000b, code lost:
            r3.c.offer(r0);
            r3.e = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
            if (d() == false) goto L_?;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onComplete() {
            /*
                r3 = this;
                monitor-enter(r3)
                U r0 = r3.k     // Catch:{ all -> 0x0022 }
                if (r0 != 0) goto L_0x0007
                monitor-exit(r3)     // Catch:{ all -> 0x0022 }
                return
            L_0x0007:
                r1 = 0
                r3.k = r1     // Catch:{ all -> 0x0022 }
                monitor-exit(r3)     // Catch:{ all -> 0x0022 }
                io.reactivex.internal.fuseable.SimplePlainQueue<U> r1 = r3.c
                r1.offer(r0)
                r0 = 1
                r3.e = r0
                boolean r0 = r3.d()
                if (r0 == 0) goto L_0x0021
                io.reactivex.internal.fuseable.SimplePlainQueue<U> r0 = r3.c
                io.reactivex.Observer<? super V> r1 = r3.b
                r2 = 0
                io.reactivex.internal.util.QueueDrainHelper.a(r0, r1, r2, r3, r3)
            L_0x0021:
                return
            L_0x0022:
                r0 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x0022 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: io.reactivex.internal.operators.observable.ObservableBufferExactBoundary.BufferExactBoundaryObserver.onComplete():void");
        }

        public void onError(Throwable th) {
            dispose();
            this.b.onError(th);
        }

        public void onNext(T t) {
            synchronized (this) {
                U u = this.k;
                if (u != null) {
                    u.add(t);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.i, disposable)) {
                this.i = disposable;
                try {
                    U call = this.g.call();
                    ObjectHelper.a(call, "The buffer supplied is null");
                    this.k = (Collection) call;
                    BufferBoundaryObserver bufferBoundaryObserver = new BufferBoundaryObserver(this);
                    this.j = bufferBoundaryObserver;
                    this.b.onSubscribe(this);
                    if (!this.d) {
                        this.h.subscribe(bufferBoundaryObserver);
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.d = true;
                    disposable.dispose();
                    EmptyDisposable.a(th, (Observer<?>) this.b);
                }
            }
        }

        public void a(Observer<? super U> observer, U u) {
            this.b.onNext(u);
        }
    }

    public ObservableBufferExactBoundary(ObservableSource<T> observableSource, ObservableSource<B> observableSource2, Callable<U> callable) {
        super(observableSource);
        this.b = observableSource2;
        this.c = callable;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super U> observer) {
        this.f6691a.subscribe(new BufferExactBoundaryObserver(new SerializedObserver(observer), this.c, this.b));
    }
}
