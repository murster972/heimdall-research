package io.reactivex.internal.operators.observable;

import io.reactivex.Notification;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableDematerialize<T, R> extends AbstractObservableWithUpstream<T, R> {
    final Function<? super T, ? extends Notification<R>> b;

    static final class DematerializeObserver<T, R> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super R> f6728a;
        final Function<? super T, ? extends Notification<R>> b;
        boolean c;
        Disposable d;

        DematerializeObserver(Observer<? super R> observer, Function<? super T, ? extends Notification<R>> function) {
            this.f6728a = observer;
            this.b = function;
        }

        public void dispose() {
            this.d.dispose();
        }

        public boolean isDisposed() {
            return this.d.isDisposed();
        }

        public void onComplete() {
            if (!this.c) {
                this.c = true;
                this.f6728a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.c) {
                RxJavaPlugins.b(th);
                return;
            }
            this.c = true;
            this.f6728a.onError(th);
        }

        public void onNext(T t) {
            if (!this.c) {
                try {
                    Object apply = this.b.apply(t);
                    ObjectHelper.a(apply, "The selector returned a null Notification");
                    Notification notification = (Notification) apply;
                    if (notification.d()) {
                        this.d.dispose();
                        onError(notification.a());
                    } else if (notification.c()) {
                        this.d.dispose();
                        onComplete();
                    } else {
                        this.f6728a.onNext(notification.b());
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.d.dispose();
                    onError(th);
                }
            } else if (t instanceof Notification) {
                Notification notification2 = (Notification) t;
                if (notification2.d()) {
                    RxJavaPlugins.b(notification2.a());
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.f6728a.onSubscribe(this);
            }
        }
    }

    public ObservableDematerialize(ObservableSource<T> observableSource, Function<? super T, ? extends Notification<R>> function) {
        super(observableSource);
        this.b = function;
    }

    public void subscribeActual(Observer<? super R> observer) {
        this.f6691a.subscribe(new DematerializeObserver(observer, this.b));
    }
}
