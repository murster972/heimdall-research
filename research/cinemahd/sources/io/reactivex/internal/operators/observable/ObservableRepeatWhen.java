package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.HalfSerializer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableRepeatWhen<T> extends AbstractObservableWithUpstream<T, T> {
    final Function<? super Observable<Object>, ? extends ObservableSource<?>> b;

    public ObservableRepeatWhen(ObservableSource<T> observableSource, Function<? super Observable<Object>, ? extends ObservableSource<?>> function) {
        super(observableSource);
        this.b = function;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        Subject a2 = PublishSubject.b().a();
        try {
            Object apply = this.b.apply(a2);
            ObjectHelper.a(apply, "The handler returned a null ObservableSource");
            ObservableSource observableSource = (ObservableSource) apply;
            RepeatWhenObserver repeatWhenObserver = new RepeatWhenObserver(observer, a2, this.f6691a);
            observer.onSubscribe(repeatWhenObserver);
            observableSource.subscribe(repeatWhenObserver.inner);
            repeatWhenObserver.c();
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (Observer<?>) observer);
        }
    }

    static final class RepeatWhenObserver<T> extends AtomicInteger implements Observer<T>, Disposable {
        private static final long serialVersionUID = 802743776666017014L;
        volatile boolean active;
        final Observer<? super T> downstream;
        final AtomicThrowable error = new AtomicThrowable();
        final RepeatWhenObserver<T>.InnerRepeatObserver inner = new InnerRepeatObserver();
        final Subject<Object> signaller;
        final ObservableSource<T> source;
        final AtomicReference<Disposable> upstream = new AtomicReference<>();
        final AtomicInteger wip = new AtomicInteger();

        final class InnerRepeatObserver extends AtomicReference<Disposable> implements Observer<Object> {
            private static final long serialVersionUID = 3254781284376480842L;

            InnerRepeatObserver() {
            }

            public void onComplete() {
                RepeatWhenObserver.this.a();
            }

            public void onError(Throwable th) {
                RepeatWhenObserver.this.a(th);
            }

            public void onNext(Object obj) {
                RepeatWhenObserver.this.b();
            }

            public void onSubscribe(Disposable disposable) {
                DisposableHelper.c(this, disposable);
            }
        }

        RepeatWhenObserver(Observer<? super T> observer, Subject<Object> subject, ObservableSource<T> observableSource) {
            this.downstream = observer;
            this.signaller = subject;
            this.source = observableSource;
        }

        /* access modifiers changed from: package-private */
        public void a(Throwable th) {
            DisposableHelper.a(this.upstream);
            HalfSerializer.a((Observer<?>) this.downstream, th, (AtomicInteger) this, this.error);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            c();
        }

        /* access modifiers changed from: package-private */
        public void c() {
            if (this.wip.getAndIncrement() == 0) {
                while (!isDisposed()) {
                    if (!this.active) {
                        this.active = true;
                        this.source.subscribe(this);
                    }
                    if (this.wip.decrementAndGet() == 0) {
                        return;
                    }
                }
            }
        }

        public void dispose() {
            DisposableHelper.a(this.upstream);
            DisposableHelper.a((AtomicReference<Disposable>) this.inner);
        }

        public boolean isDisposed() {
            return DisposableHelper.a(this.upstream.get());
        }

        public void onComplete() {
            DisposableHelper.a(this.upstream, (Disposable) null);
            this.active = false;
            this.signaller.onNext(0);
        }

        public void onError(Throwable th) {
            DisposableHelper.a((AtomicReference<Disposable>) this.inner);
            HalfSerializer.a((Observer<?>) this.downstream, th, (AtomicInteger) this, this.error);
        }

        public void onNext(T t) {
            HalfSerializer.a(this.downstream, t, (AtomicInteger) this, this.error);
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.upstream, disposable);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            DisposableHelper.a(this.upstream);
            HalfSerializer.a((Observer<?>) this.downstream, (AtomicInteger) this, this.error);
        }
    }
}
