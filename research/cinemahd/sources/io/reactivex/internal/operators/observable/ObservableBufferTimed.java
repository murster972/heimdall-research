package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.observers.SerializedObserver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableBufferTimed<T, U extends Collection<? super T>> extends AbstractObservableWithUpstream<T, U> {
    final long b;
    final long c;
    final TimeUnit d;
    final Scheduler e;
    final Callable<U> f;
    final int g;
    final boolean h;

    static final class BufferExactBoundedObserver<T, U extends Collection<? super T>> extends QueueDrainObserver<T, U, U> implements Runnable, Disposable {
        final Callable<U> g;
        final long h;
        final TimeUnit i;
        final int j;
        final boolean k;
        final Scheduler.Worker l;
        U m;
        Disposable n;
        Disposable o;
        long p;
        long q;

        BufferExactBoundedObserver(Observer<? super U> observer, Callable<U> callable, long j2, TimeUnit timeUnit, int i2, boolean z, Scheduler.Worker worker) {
            super(observer, new MpscLinkedQueue());
            this.g = callable;
            this.h = j2;
            this.i = timeUnit;
            this.j = i2;
            this.k = z;
            this.l = worker;
        }

        public void dispose() {
            if (!this.d) {
                this.d = true;
                this.o.dispose();
                this.l.dispose();
                synchronized (this) {
                    this.m = null;
                }
            }
        }

        public boolean isDisposed() {
            return this.d;
        }

        public void onComplete() {
            U u;
            this.l.dispose();
            synchronized (this) {
                u = this.m;
                this.m = null;
            }
            this.c.offer(u);
            this.e = true;
            if (d()) {
                QueueDrainHelper.a(this.c, this.b, false, this, this);
            }
        }

        public void onError(Throwable th) {
            synchronized (this) {
                this.m = null;
            }
            this.b.onError(th);
            this.l.dispose();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
            if (r7.k == false) goto L_0x0028;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0023, code lost:
            r7.n.dispose();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0028, code lost:
            b(r0, false, r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            r8 = r7.g.call();
            io.reactivex.internal.functions.ObjectHelper.a(r8, "The buffer supplied is null");
            r8 = (java.util.Collection) r8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
            monitor-enter(r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            r7.m = r8;
            r7.q++;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
            monitor-exit(r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0044, code lost:
            if (r7.k == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0046, code lost:
            r0 = r7.l;
            r4 = r7.h;
            r7.n = r0.a(r7, r4, r4, r7.i);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0058, code lost:
            r8 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0059, code lost:
            io.reactivex.exceptions.Exceptions.b(r8);
            r7.b.onError(r8);
            dispose();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0064, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onNext(T r8) {
            /*
                r7 = this;
                monitor-enter(r7)
                U r0 = r7.m     // Catch:{ all -> 0x0065 }
                if (r0 != 0) goto L_0x0007
                monitor-exit(r7)     // Catch:{ all -> 0x0065 }
                return
            L_0x0007:
                r0.add(r8)     // Catch:{ all -> 0x0065 }
                int r8 = r0.size()     // Catch:{ all -> 0x0065 }
                int r1 = r7.j     // Catch:{ all -> 0x0065 }
                if (r8 >= r1) goto L_0x0014
                monitor-exit(r7)     // Catch:{ all -> 0x0065 }
                return
            L_0x0014:
                r8 = 0
                r7.m = r8     // Catch:{ all -> 0x0065 }
                long r1 = r7.p     // Catch:{ all -> 0x0065 }
                r3 = 1
                long r1 = r1 + r3
                r7.p = r1     // Catch:{ all -> 0x0065 }
                monitor-exit(r7)     // Catch:{ all -> 0x0065 }
                boolean r8 = r7.k
                if (r8 == 0) goto L_0x0028
                io.reactivex.disposables.Disposable r8 = r7.n
                r8.dispose()
            L_0x0028:
                r8 = 0
                r7.b(r0, r8, r7)
                java.util.concurrent.Callable<U> r8 = r7.g     // Catch:{ all -> 0x0058 }
                java.lang.Object r8 = r8.call()     // Catch:{ all -> 0x0058 }
                java.lang.String r0 = "The buffer supplied is null"
                io.reactivex.internal.functions.ObjectHelper.a(r8, (java.lang.String) r0)     // Catch:{ all -> 0x0058 }
                java.util.Collection r8 = (java.util.Collection) r8     // Catch:{ all -> 0x0058 }
                monitor-enter(r7)
                r7.m = r8     // Catch:{ all -> 0x0055 }
                long r0 = r7.q     // Catch:{ all -> 0x0055 }
                long r0 = r0 + r3
                r7.q = r0     // Catch:{ all -> 0x0055 }
                monitor-exit(r7)     // Catch:{ all -> 0x0055 }
                boolean r8 = r7.k
                if (r8 == 0) goto L_0x0054
                io.reactivex.Scheduler$Worker r0 = r7.l
                long r4 = r7.h
                java.util.concurrent.TimeUnit r6 = r7.i
                r1 = r7
                r2 = r4
                io.reactivex.disposables.Disposable r8 = r0.a(r1, r2, r4, r6)
                r7.n = r8
            L_0x0054:
                return
            L_0x0055:
                r8 = move-exception
                monitor-exit(r7)     // Catch:{ all -> 0x0055 }
                throw r8
            L_0x0058:
                r8 = move-exception
                io.reactivex.exceptions.Exceptions.b(r8)
                io.reactivex.Observer<? super V> r0 = r7.b
                r0.onError(r8)
                r7.dispose()
                return
            L_0x0065:
                r8 = move-exception
                monitor-exit(r7)     // Catch:{ all -> 0x0065 }
                throw r8
            */
            throw new UnsupportedOperationException("Method not decompiled: io.reactivex.internal.operators.observable.ObservableBufferTimed.BufferExactBoundedObserver.onNext(java.lang.Object):void");
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.o, disposable)) {
                this.o = disposable;
                try {
                    U call = this.g.call();
                    ObjectHelper.a(call, "The buffer supplied is null");
                    this.m = (Collection) call;
                    this.b.onSubscribe(this);
                    Scheduler.Worker worker = this.l;
                    long j2 = this.h;
                    this.n = worker.a(this, j2, j2, this.i);
                } catch (Throwable th) {
                    Exceptions.b(th);
                    disposable.dispose();
                    EmptyDisposable.a(th, (Observer<?>) this.b);
                    this.l.dispose();
                }
            }
        }

        public void run() {
            try {
                U call = this.g.call();
                ObjectHelper.a(call, "The bufferSupplier returned a null buffer");
                U u = (Collection) call;
                synchronized (this) {
                    U u2 = this.m;
                    if (u2 != null) {
                        if (this.p == this.q) {
                            this.m = u;
                            b(u2, false, this);
                        }
                    }
                }
            } catch (Throwable th) {
                Exceptions.b(th);
                dispose();
                this.b.onError(th);
            }
        }

        public void a(Observer<? super U> observer, U u) {
            observer.onNext(u);
        }
    }

    static final class BufferExactUnboundedObserver<T, U extends Collection<? super T>> extends QueueDrainObserver<T, U, U> implements Runnable, Disposable {
        final Callable<U> g;
        final long h;
        final TimeUnit i;
        final Scheduler j;
        Disposable k;
        U l;
        final AtomicReference<Disposable> m = new AtomicReference<>();

        BufferExactUnboundedObserver(Observer<? super U> observer, Callable<U> callable, long j2, TimeUnit timeUnit, Scheduler scheduler) {
            super(observer, new MpscLinkedQueue());
            this.g = callable;
            this.h = j2;
            this.i = timeUnit;
            this.j = scheduler;
        }

        public void dispose() {
            DisposableHelper.a(this.m);
            this.k.dispose();
        }

        public boolean isDisposed() {
            return this.m.get() == DisposableHelper.DISPOSED;
        }

        public void onComplete() {
            U u;
            synchronized (this) {
                u = this.l;
                this.l = null;
            }
            if (u != null) {
                this.c.offer(u);
                this.e = true;
                if (d()) {
                    QueueDrainHelper.a(this.c, this.b, false, (Disposable) null, this);
                }
            }
            DisposableHelper.a(this.m);
        }

        public void onError(Throwable th) {
            synchronized (this) {
                this.l = null;
            }
            this.b.onError(th);
            DisposableHelper.a(this.m);
        }

        public void onNext(T t) {
            synchronized (this) {
                U u = this.l;
                if (u != null) {
                    u.add(t);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.k, disposable)) {
                this.k = disposable;
                try {
                    U call = this.g.call();
                    ObjectHelper.a(call, "The buffer supplied is null");
                    this.l = (Collection) call;
                    this.b.onSubscribe(this);
                    if (!this.d) {
                        Scheduler scheduler = this.j;
                        long j2 = this.h;
                        Disposable a2 = scheduler.a(this, j2, j2, this.i);
                        if (!this.m.compareAndSet((Object) null, a2)) {
                            a2.dispose();
                        }
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    dispose();
                    EmptyDisposable.a(th, (Observer<?>) this.b);
                }
            }
        }

        public void run() {
            U u;
            try {
                U call = this.g.call();
                ObjectHelper.a(call, "The bufferSupplier returned a null buffer");
                U u2 = (Collection) call;
                synchronized (this) {
                    u = this.l;
                    if (u != null) {
                        this.l = u2;
                    }
                }
                if (u == null) {
                    DisposableHelper.a(this.m);
                } else {
                    a(u, false, this);
                }
            } catch (Throwable th) {
                Exceptions.b(th);
                this.b.onError(th);
                dispose();
            }
        }

        public void a(Observer<? super U> observer, U u) {
            this.b.onNext(u);
        }
    }

    public ObservableBufferTimed(ObservableSource<T> observableSource, long j, long j2, TimeUnit timeUnit, Scheduler scheduler, Callable<U> callable, int i, boolean z) {
        super(observableSource);
        this.b = j;
        this.c = j2;
        this.d = timeUnit;
        this.e = scheduler;
        this.f = callable;
        this.g = i;
        this.h = z;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super U> observer) {
        if (this.b == this.c && this.g == Integer.MAX_VALUE) {
            this.f6691a.subscribe(new BufferExactUnboundedObserver(new SerializedObserver(observer), this.f, this.b, this.d, this.e));
            return;
        }
        Scheduler.Worker a2 = this.e.a();
        if (this.b == this.c) {
            this.f6691a.subscribe(new BufferExactBoundedObserver(new SerializedObserver(observer), this.f, this.b, this.d, this.g, this.h, a2));
        } else {
            this.f6691a.subscribe(new BufferSkipBoundedObserver(new SerializedObserver(observer), this.f, this.b, this.c, this.d, a2));
        }
    }

    static final class BufferSkipBoundedObserver<T, U extends Collection<? super T>> extends QueueDrainObserver<T, U, U> implements Runnable, Disposable {
        final Callable<U> g;
        final long h;
        final long i;
        final TimeUnit j;
        final Scheduler.Worker k;
        final List<U> l = new LinkedList();
        Disposable m;

        final class RemoveFromBuffer implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            private final U f6707a;

            RemoveFromBuffer(U u) {
                this.f6707a = u;
            }

            public void run() {
                synchronized (BufferSkipBoundedObserver.this) {
                    BufferSkipBoundedObserver.this.l.remove(this.f6707a);
                }
                BufferSkipBoundedObserver bufferSkipBoundedObserver = BufferSkipBoundedObserver.this;
                bufferSkipBoundedObserver.b(this.f6707a, false, bufferSkipBoundedObserver.k);
            }
        }

        final class RemoveFromBufferEmit implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            private final U f6708a;

            RemoveFromBufferEmit(U u) {
                this.f6708a = u;
            }

            public void run() {
                synchronized (BufferSkipBoundedObserver.this) {
                    BufferSkipBoundedObserver.this.l.remove(this.f6708a);
                }
                BufferSkipBoundedObserver bufferSkipBoundedObserver = BufferSkipBoundedObserver.this;
                bufferSkipBoundedObserver.b(this.f6708a, false, bufferSkipBoundedObserver.k);
            }
        }

        BufferSkipBoundedObserver(Observer<? super U> observer, Callable<U> callable, long j2, long j3, TimeUnit timeUnit, Scheduler.Worker worker) {
            super(observer, new MpscLinkedQueue());
            this.g = callable;
            this.h = j2;
            this.i = j3;
            this.j = timeUnit;
            this.k = worker;
        }

        public void dispose() {
            if (!this.d) {
                this.d = true;
                f();
                this.m.dispose();
                this.k.dispose();
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
            synchronized (this) {
                this.l.clear();
            }
        }

        public boolean isDisposed() {
            return this.d;
        }

        public void onComplete() {
            ArrayList<Collection> arrayList;
            synchronized (this) {
                arrayList = new ArrayList<>(this.l);
                this.l.clear();
            }
            for (Collection offer : arrayList) {
                this.c.offer(offer);
            }
            this.e = true;
            if (d()) {
                QueueDrainHelper.a(this.c, this.b, false, this.k, this);
            }
        }

        public void onError(Throwable th) {
            this.e = true;
            f();
            this.b.onError(th);
            this.k.dispose();
        }

        public void onNext(T t) {
            synchronized (this) {
                for (U add : this.l) {
                    add.add(t);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.m, disposable)) {
                this.m = disposable;
                try {
                    U call = this.g.call();
                    ObjectHelper.a(call, "The buffer supplied is null");
                    Collection collection = (Collection) call;
                    this.l.add(collection);
                    this.b.onSubscribe(this);
                    Scheduler.Worker worker = this.k;
                    long j2 = this.i;
                    worker.a(this, j2, j2, this.j);
                    this.k.a(new RemoveFromBufferEmit(collection), this.h, this.j);
                } catch (Throwable th) {
                    Exceptions.b(th);
                    disposable.dispose();
                    EmptyDisposable.a(th, (Observer<?>) this.b);
                    this.k.dispose();
                }
            }
        }

        public void run() {
            if (!this.d) {
                try {
                    U call = this.g.call();
                    ObjectHelper.a(call, "The bufferSupplier returned a null buffer");
                    Collection collection = (Collection) call;
                    synchronized (this) {
                        if (!this.d) {
                            this.l.add(collection);
                            this.k.a(new RemoveFromBuffer(collection), this.h, this.j);
                        }
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.b.onError(th);
                    dispose();
                }
            }
        }

        public void a(Observer<? super U> observer, U u) {
            observer.onNext(u);
        }
    }
}
