package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.Callable;

public final class ObservableMapNotification<T, R> extends AbstractObservableWithUpstream<T, ObservableSource<? extends R>> {
    final Function<? super T, ? extends ObservableSource<? extends R>> b;
    final Function<? super Throwable, ? extends ObservableSource<? extends R>> c;
    final Callable<? extends ObservableSource<? extends R>> d;

    static final class MapNotificationObserver<T, R> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super ObservableSource<? extends R>> f6782a;
        final Function<? super T, ? extends ObservableSource<? extends R>> b;
        final Function<? super Throwable, ? extends ObservableSource<? extends R>> c;
        final Callable<? extends ObservableSource<? extends R>> d;
        Disposable e;

        MapNotificationObserver(Observer<? super ObservableSource<? extends R>> observer, Function<? super T, ? extends ObservableSource<? extends R>> function, Function<? super Throwable, ? extends ObservableSource<? extends R>> function2, Callable<? extends ObservableSource<? extends R>> callable) {
            this.f6782a = observer;
            this.b = function;
            this.c = function2;
            this.d = callable;
        }

        public void dispose() {
            this.e.dispose();
        }

        public boolean isDisposed() {
            return this.e.isDisposed();
        }

        public void onComplete() {
            try {
                Object call = this.d.call();
                ObjectHelper.a(call, "The onComplete ObservableSource returned is null");
                this.f6782a.onNext((ObservableSource) call);
                this.f6782a.onComplete();
            } catch (Throwable th) {
                Exceptions.b(th);
                this.f6782a.onError(th);
            }
        }

        public void onError(Throwable th) {
            try {
                Object apply = this.c.apply(th);
                ObjectHelper.a(apply, "The onError ObservableSource returned is null");
                this.f6782a.onNext((ObservableSource) apply);
                this.f6782a.onComplete();
            } catch (Throwable th2) {
                Exceptions.b(th2);
                this.f6782a.onError(new CompositeException(th, th2));
            }
        }

        public void onNext(T t) {
            try {
                Object apply = this.b.apply(t);
                ObjectHelper.a(apply, "The onNext ObservableSource returned is null");
                this.f6782a.onNext((ObservableSource) apply);
            } catch (Throwable th) {
                Exceptions.b(th);
                this.f6782a.onError(th);
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.e, disposable)) {
                this.e = disposable;
                this.f6782a.onSubscribe(this);
            }
        }
    }

    public ObservableMapNotification(ObservableSource<T> observableSource, Function<? super T, ? extends ObservableSource<? extends R>> function, Function<? super Throwable, ? extends ObservableSource<? extends R>> function2, Callable<? extends ObservableSource<? extends R>> callable) {
        super(observableSource);
        this.b = function;
        this.c = function2;
        this.d = callable;
    }

    public void subscribeActual(Observer<? super ObservableSource<? extends R>> observer) {
        this.f6691a.subscribe(new MapNotificationObserver(observer, this.b, this.c, this.d));
    }
}
