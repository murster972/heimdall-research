package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observers.SerializedObserver;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableSampleWithObservable<T> extends AbstractObservableWithUpstream<T, T> {
    final ObservableSource<?> b;
    final boolean c;

    static final class SampleMainEmitLast<T> extends SampleMainObserver<T> {
        private static final long serialVersionUID = -3029755663834015785L;
        volatile boolean done;
        final AtomicInteger wip = new AtomicInteger();

        SampleMainEmitLast(Observer<? super T> observer, ObservableSource<?> observableSource) {
            super(observer, observableSource);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.done = true;
            if (this.wip.getAndIncrement() == 0) {
                d();
                this.downstream.onComplete();
            }
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.done = true;
            if (this.wip.getAndIncrement() == 0) {
                d();
                this.downstream.onComplete();
            }
        }

        /* access modifiers changed from: package-private */
        public void e() {
            if (this.wip.getAndIncrement() == 0) {
                do {
                    boolean z = this.done;
                    d();
                    if (z) {
                        this.downstream.onComplete();
                        return;
                    }
                } while (this.wip.decrementAndGet() != 0);
            }
        }
    }

    static final class SampleMainNoLast<T> extends SampleMainObserver<T> {
        private static final long serialVersionUID = -3029755663834015785L;

        SampleMainNoLast(Observer<? super T> observer, ObservableSource<?> observableSource) {
            super(observer, observableSource);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.downstream.onComplete();
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.downstream.onComplete();
        }

        /* access modifiers changed from: package-private */
        public void e() {
            d();
        }
    }

    static abstract class SampleMainObserver<T> extends AtomicReference<T> implements Observer<T>, Disposable {
        private static final long serialVersionUID = -3517602651313910099L;
        final Observer<? super T> downstream;
        final AtomicReference<Disposable> other = new AtomicReference<>();
        final ObservableSource<?> sampler;
        Disposable upstream;

        SampleMainObserver(Observer<? super T> observer, ObservableSource<?> observableSource) {
            this.downstream = observer;
            this.sampler = observableSource;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Disposable disposable) {
            return DisposableHelper.c(this.other, disposable);
        }

        /* access modifiers changed from: package-private */
        public abstract void b();

        /* access modifiers changed from: package-private */
        public abstract void c();

        /* access modifiers changed from: package-private */
        public void d() {
            Object andSet = getAndSet((Object) null);
            if (andSet != null) {
                this.downstream.onNext(andSet);
            }
        }

        public void dispose() {
            DisposableHelper.a(this.other);
            this.upstream.dispose();
        }

        /* access modifiers changed from: package-private */
        public abstract void e();

        public boolean isDisposed() {
            return this.other.get() == DisposableHelper.DISPOSED;
        }

        public void onComplete() {
            DisposableHelper.a(this.other);
            b();
        }

        public void onError(Throwable th) {
            DisposableHelper.a(this.other);
            this.downstream.onError(th);
        }

        public void onNext(T t) {
            lazySet(t);
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.upstream, disposable)) {
                this.upstream = disposable;
                this.downstream.onSubscribe(this);
                if (this.other.get() == null) {
                    this.sampler.subscribe(new SamplerObserver(this));
                }
            }
        }

        public void a(Throwable th) {
            this.upstream.dispose();
            this.downstream.onError(th);
        }

        public void a() {
            this.upstream.dispose();
            c();
        }
    }

    static final class SamplerObserver<T> implements Observer<Object> {

        /* renamed from: a  reason: collision with root package name */
        final SampleMainObserver<T> f6807a;

        SamplerObserver(SampleMainObserver<T> sampleMainObserver) {
            this.f6807a = sampleMainObserver;
        }

        public void onComplete() {
            this.f6807a.a();
        }

        public void onError(Throwable th) {
            this.f6807a.a(th);
        }

        public void onNext(Object obj) {
            this.f6807a.e();
        }

        public void onSubscribe(Disposable disposable) {
            this.f6807a.a(disposable);
        }
    }

    public ObservableSampleWithObservable(ObservableSource<T> observableSource, ObservableSource<?> observableSource2, boolean z) {
        super(observableSource);
        this.b = observableSource2;
        this.c = z;
    }

    public void subscribeActual(Observer<? super T> observer) {
        SerializedObserver serializedObserver = new SerializedObserver(observer);
        if (this.c) {
            this.f6691a.subscribe(new SampleMainEmitLast(serializedObserver, this.b));
        } else {
            this.f6691a.subscribe(new SampleMainNoLast(serializedObserver, this.b));
        }
    }
}
