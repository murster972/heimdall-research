package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.UnicastSubject;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableWindowBoundarySupplier<T, B> extends AbstractObservableWithUpstream<T, Observable<T>> {
    final Callable<? extends ObservableSource<B>> b;
    final int c;

    static final class WindowBoundaryInnerObserver<T, B> extends DisposableObserver<B> {
        final WindowBoundaryMainObserver<T, B> b;
        boolean c;

        WindowBoundaryInnerObserver(WindowBoundaryMainObserver<T, B> windowBoundaryMainObserver) {
            this.b = windowBoundaryMainObserver;
        }

        public void onComplete() {
            if (!this.c) {
                this.c = true;
                this.b.c();
            }
        }

        public void onError(Throwable th) {
            if (this.c) {
                RxJavaPlugins.b(th);
                return;
            }
            this.c = true;
            this.b.a(th);
        }

        public void onNext(B b2) {
            if (!this.c) {
                this.c = true;
                dispose();
                this.b.a(this);
            }
        }
    }

    public ObservableWindowBoundarySupplier(ObservableSource<T> observableSource, Callable<? extends ObservableSource<B>> callable, int i) {
        super(observableSource);
        this.b = callable;
        this.c = i;
    }

    public void subscribeActual(Observer<? super Observable<T>> observer) {
        this.f6691a.subscribe(new WindowBoundaryMainObserver(observer, this.c, this.b));
    }

    static final class WindowBoundaryMainObserver<T, B> extends AtomicInteger implements Observer<T>, Disposable, Runnable {

        /* renamed from: a  reason: collision with root package name */
        static final WindowBoundaryInnerObserver<Object, Object> f6841a = new WindowBoundaryInnerObserver<>((WindowBoundaryMainObserver) null);
        static final Object b = new Object();
        private static final long serialVersionUID = 2233020065421370272L;
        final AtomicReference<WindowBoundaryInnerObserver<T, B>> boundaryObserver = new AtomicReference<>();
        final int capacityHint;
        volatile boolean done;
        final Observer<? super Observable<T>> downstream;
        final AtomicThrowable errors = new AtomicThrowable();
        final Callable<? extends ObservableSource<B>> other;
        final MpscLinkedQueue<Object> queue = new MpscLinkedQueue<>();
        final AtomicBoolean stopWindows = new AtomicBoolean();
        Disposable upstream;
        UnicastSubject<T> window;
        final AtomicInteger windows = new AtomicInteger(1);

        WindowBoundaryMainObserver(Observer<? super Observable<T>> observer, int i, Callable<? extends ObservableSource<B>> callable) {
            this.downstream = observer;
            this.capacityHint = i;
            this.other = callable;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            Disposable andSet = this.boundaryObserver.getAndSet(f6841a);
            if (andSet != null && andSet != f6841a) {
                andSet.dispose();
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (getAndIncrement() == 0) {
                Observer<? super Observable<T>> observer = this.downstream;
                MpscLinkedQueue<Object> mpscLinkedQueue = this.queue;
                AtomicThrowable atomicThrowable = this.errors;
                int i = 1;
                while (this.windows.get() != 0) {
                    UnicastSubject<T> unicastSubject = this.window;
                    boolean z = this.done;
                    if (!z || atomicThrowable.get() == null) {
                        Object poll = mpscLinkedQueue.poll();
                        boolean z2 = poll == null;
                        if (z && z2) {
                            Throwable a2 = atomicThrowable.a();
                            if (a2 == null) {
                                if (unicastSubject != null) {
                                    this.window = null;
                                    unicastSubject.onComplete();
                                }
                                observer.onComplete();
                                return;
                            }
                            if (unicastSubject != null) {
                                this.window = null;
                                unicastSubject.onError(a2);
                            }
                            observer.onError(a2);
                            return;
                        } else if (z2) {
                            i = addAndGet(-i);
                            if (i == 0) {
                                return;
                            }
                        } else if (poll != b) {
                            unicastSubject.onNext(poll);
                        } else {
                            if (unicastSubject != null) {
                                this.window = null;
                                unicastSubject.onComplete();
                            }
                            if (!this.stopWindows.get()) {
                                UnicastSubject<T> a3 = UnicastSubject.a(this.capacityHint, (Runnable) this);
                                this.window = a3;
                                this.windows.getAndIncrement();
                                try {
                                    Object call = this.other.call();
                                    ObjectHelper.a(call, "The other Callable returned a null ObservableSource");
                                    ObservableSource observableSource = (ObservableSource) call;
                                    WindowBoundaryInnerObserver windowBoundaryInnerObserver = new WindowBoundaryInnerObserver(this);
                                    if (this.boundaryObserver.compareAndSet((Object) null, windowBoundaryInnerObserver)) {
                                        observableSource.subscribe(windowBoundaryInnerObserver);
                                        observer.onNext(a3);
                                    }
                                } catch (Throwable th) {
                                    Exceptions.b(th);
                                    atomicThrowable.a(th);
                                    this.done = true;
                                }
                            }
                        }
                    } else {
                        mpscLinkedQueue.clear();
                        Throwable a4 = atomicThrowable.a();
                        if (unicastSubject != null) {
                            this.window = null;
                            unicastSubject.onError(a4);
                        }
                        observer.onError(a4);
                        return;
                    }
                }
                mpscLinkedQueue.clear();
                this.window = null;
            }
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.upstream.dispose();
            this.done = true;
            b();
        }

        public void dispose() {
            if (this.stopWindows.compareAndSet(false, true)) {
                a();
                if (this.windows.decrementAndGet() == 0) {
                    this.upstream.dispose();
                }
            }
        }

        public boolean isDisposed() {
            return this.stopWindows.get();
        }

        public void onComplete() {
            a();
            this.done = true;
            b();
        }

        public void onError(Throwable th) {
            a();
            if (this.errors.a(th)) {
                this.done = true;
                b();
                return;
            }
            RxJavaPlugins.b(th);
        }

        public void onNext(T t) {
            this.queue.offer(t);
            b();
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.upstream, disposable)) {
                this.upstream = disposable;
                this.downstream.onSubscribe(this);
                this.queue.offer(b);
                b();
            }
        }

        public void run() {
            if (this.windows.decrementAndGet() == 0) {
                this.upstream.dispose();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(WindowBoundaryInnerObserver<T, B> windowBoundaryInnerObserver) {
            this.boundaryObserver.compareAndSet(windowBoundaryInnerObserver, (Object) null);
            this.queue.offer(b);
            b();
        }

        /* access modifiers changed from: package-private */
        public void a(Throwable th) {
            this.upstream.dispose();
            if (this.errors.a(th)) {
                this.done = true;
                b();
                return;
            }
            RxJavaPlugins.b(th);
        }
    }
}
