package io.reactivex.internal.operators.observable;

import com.facebook.common.time.Clock;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.operators.observable.ObservableTimeoutTimed;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableTimeout<T, U, V> extends AbstractObservableWithUpstream<T, T> {
    final ObservableSource<U> b;
    final Function<? super T, ? extends ObservableSource<V>> c;
    final ObservableSource<? extends T> d;

    static final class TimeoutConsumer extends AtomicReference<Disposable> implements Observer<Object>, Disposable {
        private static final long serialVersionUID = 8708641127342403073L;
        final long idx;
        final TimeoutSelectorSupport parent;

        TimeoutConsumer(long j, TimeoutSelectorSupport timeoutSelectorSupport) {
            this.idx = j;
            this.parent = timeoutSelectorSupport;
        }

        public void dispose() {
            DisposableHelper.a((AtomicReference<Disposable>) this);
        }

        public boolean isDisposed() {
            return DisposableHelper.a((Disposable) get());
        }

        public void onComplete() {
            Object obj = get();
            DisposableHelper disposableHelper = DisposableHelper.DISPOSED;
            if (obj != disposableHelper) {
                lazySet(disposableHelper);
                this.parent.a(this.idx);
            }
        }

        public void onError(Throwable th) {
            Object obj = get();
            DisposableHelper disposableHelper = DisposableHelper.DISPOSED;
            if (obj != disposableHelper) {
                lazySet(disposableHelper);
                this.parent.a(this.idx, th);
                return;
            }
            RxJavaPlugins.b(th);
        }

        public void onNext(Object obj) {
            Disposable disposable = (Disposable) get();
            if (disposable != DisposableHelper.DISPOSED) {
                disposable.dispose();
                lazySet(DisposableHelper.DISPOSED);
                this.parent.a(this.idx);
            }
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this, disposable);
        }
    }

    interface TimeoutSelectorSupport extends ObservableTimeoutTimed.TimeoutSupport {
        void a(long j, Throwable th);
    }

    public ObservableTimeout(Observable<T> observable, ObservableSource<U> observableSource, Function<? super T, ? extends ObservableSource<V>> function, ObservableSource<? extends T> observableSource2) {
        super(observable);
        this.b = observableSource;
        this.c = function;
        this.d = observableSource2;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        ObservableSource<? extends T> observableSource = this.d;
        if (observableSource == null) {
            TimeoutObserver timeoutObserver = new TimeoutObserver(observer, this.c);
            observer.onSubscribe(timeoutObserver);
            timeoutObserver.a((ObservableSource<?>) this.b);
            this.f6691a.subscribe(timeoutObserver);
            return;
        }
        TimeoutFallbackObserver timeoutFallbackObserver = new TimeoutFallbackObserver(observer, this.c, observableSource);
        observer.onSubscribe(timeoutFallbackObserver);
        timeoutFallbackObserver.a((ObservableSource<?>) this.b);
        this.f6691a.subscribe(timeoutFallbackObserver);
    }

    static final class TimeoutFallbackObserver<T> extends AtomicReference<Disposable> implements Observer<T>, Disposable, TimeoutSelectorSupport {
        private static final long serialVersionUID = -7508389464265974549L;
        final Observer<? super T> downstream;
        ObservableSource<? extends T> fallback;
        final AtomicLong index;
        final Function<? super T, ? extends ObservableSource<?>> itemTimeoutIndicator;
        final SequentialDisposable task = new SequentialDisposable();
        final AtomicReference<Disposable> upstream;

        TimeoutFallbackObserver(Observer<? super T> observer, Function<? super T, ? extends ObservableSource<?>> function, ObservableSource<? extends T> observableSource) {
            this.downstream = observer;
            this.itemTimeoutIndicator = function;
            this.fallback = observableSource;
            this.index = new AtomicLong();
            this.upstream = new AtomicReference<>();
        }

        /* access modifiers changed from: package-private */
        public void a(ObservableSource<?> observableSource) {
            if (observableSource != null) {
                TimeoutConsumer timeoutConsumer = new TimeoutConsumer(0, this);
                if (this.task.a(timeoutConsumer)) {
                    observableSource.subscribe(timeoutConsumer);
                }
            }
        }

        public void dispose() {
            DisposableHelper.a(this.upstream);
            DisposableHelper.a((AtomicReference<Disposable>) this);
            this.task.dispose();
        }

        public boolean isDisposed() {
            return DisposableHelper.a((Disposable) get());
        }

        public void onComplete() {
            if (this.index.getAndSet(Clock.MAX_TIME) != Clock.MAX_TIME) {
                this.task.dispose();
                this.downstream.onComplete();
                this.task.dispose();
            }
        }

        public void onError(Throwable th) {
            if (this.index.getAndSet(Clock.MAX_TIME) != Clock.MAX_TIME) {
                this.task.dispose();
                this.downstream.onError(th);
                this.task.dispose();
                return;
            }
            RxJavaPlugins.b(th);
        }

        public void onNext(T t) {
            long j = this.index.get();
            if (j != Clock.MAX_TIME) {
                long j2 = 1 + j;
                if (this.index.compareAndSet(j, j2)) {
                    Disposable disposable = (Disposable) this.task.get();
                    if (disposable != null) {
                        disposable.dispose();
                    }
                    this.downstream.onNext(t);
                    try {
                        Object apply = this.itemTimeoutIndicator.apply(t);
                        ObjectHelper.a(apply, "The itemTimeoutIndicator returned a null ObservableSource.");
                        ObservableSource observableSource = (ObservableSource) apply;
                        TimeoutConsumer timeoutConsumer = new TimeoutConsumer(j2, this);
                        if (this.task.a(timeoutConsumer)) {
                            observableSource.subscribe(timeoutConsumer);
                        }
                    } catch (Throwable th) {
                        Exceptions.b(th);
                        this.upstream.get().dispose();
                        this.index.getAndSet(Clock.MAX_TIME);
                        this.downstream.onError(th);
                    }
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.upstream, disposable);
        }

        public void a(long j) {
            if (this.index.compareAndSet(j, Clock.MAX_TIME)) {
                DisposableHelper.a(this.upstream);
                ObservableSource<? extends T> observableSource = this.fallback;
                this.fallback = null;
                observableSource.subscribe(new ObservableTimeoutTimed.FallbackObserver(this.downstream, this));
            }
        }

        public void a(long j, Throwable th) {
            if (this.index.compareAndSet(j, Clock.MAX_TIME)) {
                DisposableHelper.a((AtomicReference<Disposable>) this);
                this.downstream.onError(th);
                return;
            }
            RxJavaPlugins.b(th);
        }
    }

    static final class TimeoutObserver<T> extends AtomicLong implements Observer<T>, Disposable, TimeoutSelectorSupport {
        private static final long serialVersionUID = 3764492702657003550L;
        final Observer<? super T> downstream;
        final Function<? super T, ? extends ObservableSource<?>> itemTimeoutIndicator;
        final SequentialDisposable task = new SequentialDisposable();
        final AtomicReference<Disposable> upstream = new AtomicReference<>();

        TimeoutObserver(Observer<? super T> observer, Function<? super T, ? extends ObservableSource<?>> function) {
            this.downstream = observer;
            this.itemTimeoutIndicator = function;
        }

        /* access modifiers changed from: package-private */
        public void a(ObservableSource<?> observableSource) {
            if (observableSource != null) {
                TimeoutConsumer timeoutConsumer = new TimeoutConsumer(0, this);
                if (this.task.a(timeoutConsumer)) {
                    observableSource.subscribe(timeoutConsumer);
                }
            }
        }

        public void dispose() {
            DisposableHelper.a(this.upstream);
            this.task.dispose();
        }

        public boolean isDisposed() {
            return DisposableHelper.a(this.upstream.get());
        }

        public void onComplete() {
            if (getAndSet(Clock.MAX_TIME) != Clock.MAX_TIME) {
                this.task.dispose();
                this.downstream.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (getAndSet(Clock.MAX_TIME) != Clock.MAX_TIME) {
                this.task.dispose();
                this.downstream.onError(th);
                return;
            }
            RxJavaPlugins.b(th);
        }

        public void onNext(T t) {
            long j = get();
            if (j != Clock.MAX_TIME) {
                long j2 = 1 + j;
                if (compareAndSet(j, j2)) {
                    Disposable disposable = (Disposable) this.task.get();
                    if (disposable != null) {
                        disposable.dispose();
                    }
                    this.downstream.onNext(t);
                    try {
                        Object apply = this.itemTimeoutIndicator.apply(t);
                        ObjectHelper.a(apply, "The itemTimeoutIndicator returned a null ObservableSource.");
                        ObservableSource observableSource = (ObservableSource) apply;
                        TimeoutConsumer timeoutConsumer = new TimeoutConsumer(j2, this);
                        if (this.task.a(timeoutConsumer)) {
                            observableSource.subscribe(timeoutConsumer);
                        }
                    } catch (Throwable th) {
                        Exceptions.b(th);
                        this.upstream.get().dispose();
                        getAndSet(Clock.MAX_TIME);
                        this.downstream.onError(th);
                    }
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.upstream, disposable);
        }

        public void a(long j) {
            if (compareAndSet(j, Clock.MAX_TIME)) {
                DisposableHelper.a(this.upstream);
                this.downstream.onError(new TimeoutException());
            }
        }

        public void a(long j, Throwable th) {
            if (compareAndSet(j, Clock.MAX_TIME)) {
                DisposableHelper.a(this.upstream);
                this.downstream.onError(th);
                return;
            }
            RxJavaPlugins.b(th);
        }
    }
}
