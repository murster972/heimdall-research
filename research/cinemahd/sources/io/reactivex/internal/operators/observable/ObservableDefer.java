package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.Callable;

public final class ObservableDefer<T> extends Observable<T> {

    /* renamed from: a  reason: collision with root package name */
    final Callable<? extends ObservableSource<? extends T>> f6720a;

    public ObservableDefer(Callable<? extends ObservableSource<? extends T>> callable) {
        this.f6720a = callable;
    }

    public void subscribeActual(Observer<? super T> observer) {
        try {
            Object call = this.f6720a.call();
            ObjectHelper.a(call, "null ObservableSource supplied");
            ((ObservableSource) call).subscribe(observer);
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (Observer<?>) observer);
        }
    }
}
