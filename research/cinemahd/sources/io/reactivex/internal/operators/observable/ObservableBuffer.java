package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ObservableBuffer<T, U extends Collection<? super T>> extends AbstractObservableWithUpstream<T, U> {
    final int b;
    final int c;
    final Callable<U> d;

    static final class BufferExactObserver<T, U extends Collection<? super T>> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super U> f6706a;
        final int b;
        final Callable<U> c;
        U d;
        int e;
        Disposable f;

        BufferExactObserver(Observer<? super U> observer, int i, Callable<U> callable) {
            this.f6706a = observer;
            this.b = i;
            this.c = callable;
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            try {
                U call = this.c.call();
                ObjectHelper.a(call, "Empty buffer supplied");
                this.d = (Collection) call;
                return true;
            } catch (Throwable th) {
                Exceptions.b(th);
                this.d = null;
                Disposable disposable = this.f;
                if (disposable == null) {
                    EmptyDisposable.a(th, (Observer<?>) this.f6706a);
                    return false;
                }
                disposable.dispose();
                this.f6706a.onError(th);
                return false;
            }
        }

        public void dispose() {
            this.f.dispose();
        }

        public boolean isDisposed() {
            return this.f.isDisposed();
        }

        public void onComplete() {
            U u = this.d;
            if (u != null) {
                this.d = null;
                if (!u.isEmpty()) {
                    this.f6706a.onNext(u);
                }
                this.f6706a.onComplete();
            }
        }

        public void onError(Throwable th) {
            this.d = null;
            this.f6706a.onError(th);
        }

        public void onNext(T t) {
            U u = this.d;
            if (u != null) {
                u.add(t);
                int i = this.e + 1;
                this.e = i;
                if (i >= this.b) {
                    this.f6706a.onNext(u);
                    this.e = 0;
                    a();
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.f, disposable)) {
                this.f = disposable;
                this.f6706a.onSubscribe(this);
            }
        }
    }

    static final class BufferSkipObserver<T, U extends Collection<? super T>> extends AtomicBoolean implements Observer<T>, Disposable {
        private static final long serialVersionUID = -8223395059921494546L;
        final Callable<U> bufferSupplier;
        final ArrayDeque<U> buffers = new ArrayDeque<>();
        final int count;
        final Observer<? super U> downstream;
        long index;
        final int skip;
        Disposable upstream;

        BufferSkipObserver(Observer<? super U> observer, int i, int i2, Callable<U> callable) {
            this.downstream = observer;
            this.count = i;
            this.skip = i2;
            this.bufferSupplier = callable;
        }

        public void dispose() {
            this.upstream.dispose();
        }

        public boolean isDisposed() {
            return this.upstream.isDisposed();
        }

        public void onComplete() {
            while (!this.buffers.isEmpty()) {
                this.downstream.onNext(this.buffers.poll());
            }
            this.downstream.onComplete();
        }

        public void onError(Throwable th) {
            this.buffers.clear();
            this.downstream.onError(th);
        }

        public void onNext(T t) {
            long j = this.index;
            this.index = 1 + j;
            if (j % ((long) this.skip) == 0) {
                try {
                    U call = this.bufferSupplier.call();
                    ObjectHelper.a(call, "The bufferSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
                    this.buffers.offer((Collection) call);
                } catch (Throwable th) {
                    this.buffers.clear();
                    this.upstream.dispose();
                    this.downstream.onError(th);
                    return;
                }
            }
            Iterator<U> it2 = this.buffers.iterator();
            while (it2.hasNext()) {
                Collection collection = (Collection) it2.next();
                collection.add(t);
                if (this.count <= collection.size()) {
                    it2.remove();
                    this.downstream.onNext(collection);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.upstream, disposable)) {
                this.upstream = disposable;
                this.downstream.onSubscribe(this);
            }
        }
    }

    public ObservableBuffer(ObservableSource<T> observableSource, int i, int i2, Callable<U> callable) {
        super(observableSource);
        this.b = i;
        this.c = i2;
        this.d = callable;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super U> observer) {
        int i = this.c;
        int i2 = this.b;
        if (i == i2) {
            BufferExactObserver bufferExactObserver = new BufferExactObserver(observer, i2, this.d);
            if (bufferExactObserver.a()) {
                this.f6691a.subscribe(bufferExactObserver);
                return;
            }
            return;
        }
        this.f6691a.subscribe(new BufferSkipObserver(observer, i2, i, this.d));
    }
}
