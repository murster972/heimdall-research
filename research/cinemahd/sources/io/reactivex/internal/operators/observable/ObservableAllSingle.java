package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableAllSingle<T> extends Single<Boolean> implements FuseToObservable<Boolean> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6699a;
    final Predicate<? super T> b;

    static final class AllObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final SingleObserver<? super Boolean> f6700a;
        final Predicate<? super T> b;
        Disposable c;
        boolean d;

        AllObserver(SingleObserver<? super Boolean> singleObserver, Predicate<? super T> predicate) {
            this.f6700a = singleObserver;
            this.b = predicate;
        }

        public void dispose() {
            this.c.dispose();
        }

        public boolean isDisposed() {
            return this.c.isDisposed();
        }

        public void onComplete() {
            if (!this.d) {
                this.d = true;
                this.f6700a.onSuccess(true);
            }
        }

        public void onError(Throwable th) {
            if (this.d) {
                RxJavaPlugins.b(th);
                return;
            }
            this.d = true;
            this.f6700a.onError(th);
        }

        public void onNext(T t) {
            if (!this.d) {
                try {
                    if (!this.b.a(t)) {
                        this.d = true;
                        this.c.dispose();
                        this.f6700a.onSuccess(false);
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.c.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6700a.onSubscribe(this);
            }
        }
    }

    public ObservableAllSingle(ObservableSource<T> observableSource, Predicate<? super T> predicate) {
        this.f6699a = observableSource;
        this.b = predicate;
    }

    public Observable<Boolean> a() {
        return RxJavaPlugins.a(new ObservableAll(this.f6699a, this.b));
    }

    /* access modifiers changed from: protected */
    public void b(SingleObserver<? super Boolean> singleObserver) {
        this.f6699a.subscribe(new AllObserver(singleObserver, this.b));
    }
}
