package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.NoSuchElementException;

public final class ObservableLastSingle<T> extends Single<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6780a;
    final T b;

    static final class LastObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final SingleObserver<? super T> f6781a;
        final T b;
        Disposable c;
        T d;

        LastObserver(SingleObserver<? super T> singleObserver, T t) {
            this.f6781a = singleObserver;
            this.b = t;
        }

        public void dispose() {
            this.c.dispose();
            this.c = DisposableHelper.DISPOSED;
        }

        public boolean isDisposed() {
            return this.c == DisposableHelper.DISPOSED;
        }

        public void onComplete() {
            this.c = DisposableHelper.DISPOSED;
            T t = this.d;
            if (t != null) {
                this.d = null;
                this.f6781a.onSuccess(t);
                return;
            }
            T t2 = this.b;
            if (t2 != null) {
                this.f6781a.onSuccess(t2);
            } else {
                this.f6781a.onError(new NoSuchElementException());
            }
        }

        public void onError(Throwable th) {
            this.c = DisposableHelper.DISPOSED;
            this.d = null;
            this.f6781a.onError(th);
        }

        public void onNext(T t) {
            this.d = t;
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.f6781a.onSubscribe(this);
            }
        }
    }

    public ObservableLastSingle(ObservableSource<T> observableSource, T t) {
        this.f6780a = observableSource;
        this.b = t;
    }

    /* access modifiers changed from: protected */
    public void b(SingleObserver<? super T> singleObserver) {
        this.f6780a.subscribe(new LastObserver(singleObserver, this.b));
    }
}
