package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.functions.BiPredicate;
import io.reactivex.functions.Function;
import io.reactivex.internal.observers.BasicFuseableObserver;

public final class ObservableDistinctUntilChanged<T, K> extends AbstractObservableWithUpstream<T, T> {
    final Function<? super T, K> b;
    final BiPredicate<? super K, ? super K> c;

    static final class DistinctUntilChangedObserver<T, K> extends BasicFuseableObserver<T, T> {
        final Function<? super T, K> f;
        final BiPredicate<? super K, ? super K> g;
        K h;
        boolean i;

        DistinctUntilChangedObserver(Observer<? super T> observer, Function<? super T, K> function, BiPredicate<? super K, ? super K> biPredicate) {
            super(observer);
            this.f = function;
            this.g = biPredicate;
        }

        public int a(int i2) {
            return b(i2);
        }

        public void onNext(T t) {
            if (!this.d) {
                if (this.e != 0) {
                    this.f6673a.onNext(t);
                    return;
                }
                try {
                    K apply = this.f.apply(t);
                    if (this.i) {
                        boolean a2 = this.g.a(this.h, apply);
                        this.h = apply;
                        if (a2) {
                            return;
                        }
                    } else {
                        this.i = true;
                        this.h = apply;
                    }
                    this.f6673a.onNext(t);
                } catch (Throwable th) {
                    a(th);
                }
            }
        }

        public T poll() throws Exception {
            while (true) {
                T poll = this.c.poll();
                if (poll == null) {
                    return null;
                }
                K apply = this.f.apply(poll);
                if (!this.i) {
                    this.i = true;
                    this.h = apply;
                    return poll;
                } else if (!this.g.a(this.h, apply)) {
                    this.h = apply;
                    return poll;
                } else {
                    this.h = apply;
                }
            }
        }
    }

    public ObservableDistinctUntilChanged(ObservableSource<T> observableSource, Function<? super T, K> function, BiPredicate<? super K, ? super K> biPredicate) {
        super(observableSource);
        this.b = function;
        this.c = biPredicate;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        this.f6691a.subscribe(new DistinctUntilChangedObserver(observer, this.b, this.c));
    }
}
