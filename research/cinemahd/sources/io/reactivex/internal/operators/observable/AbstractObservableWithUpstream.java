package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.internal.fuseable.HasUpstreamObservableSource;

abstract class AbstractObservableWithUpstream<T, U> extends Observable<U> implements HasUpstreamObservableSource<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final ObservableSource<T> f6691a;

    AbstractObservableWithUpstream(ObservableSource<T> observableSource) {
        this.f6691a = observableSource;
    }
}
