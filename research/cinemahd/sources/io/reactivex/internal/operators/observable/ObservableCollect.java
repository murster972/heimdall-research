package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class ObservableCollect<T, U> extends AbstractObservableWithUpstream<T, U> {
    final Callable<? extends U> b;
    final BiConsumer<? super U, ? super T> c;

    static final class CollectObserver<T, U> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super U> f6710a;
        final BiConsumer<? super U, ? super T> b;
        final U c;
        Disposable d;
        boolean e;

        CollectObserver(Observer<? super U> observer, U u, BiConsumer<? super U, ? super T> biConsumer) {
            this.f6710a = observer;
            this.b = biConsumer;
            this.c = u;
        }

        public void dispose() {
            this.d.dispose();
        }

        public boolean isDisposed() {
            return this.d.isDisposed();
        }

        public void onComplete() {
            if (!this.e) {
                this.e = true;
                this.f6710a.onNext(this.c);
                this.f6710a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.e) {
                RxJavaPlugins.b(th);
                return;
            }
            this.e = true;
            this.f6710a.onError(th);
        }

        public void onNext(T t) {
            if (!this.e) {
                try {
                    this.b.a(this.c, t);
                } catch (Throwable th) {
                    this.d.dispose();
                    onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.f6710a.onSubscribe(this);
            }
        }
    }

    public ObservableCollect(ObservableSource<T> observableSource, Callable<? extends U> callable, BiConsumer<? super U, ? super T> biConsumer) {
        super(observableSource);
        this.b = callable;
        this.c = biConsumer;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super U> observer) {
        try {
            Object call = this.b.call();
            ObjectHelper.a(call, "The initialSupplier returned a null value");
            this.f6691a.subscribe(new CollectObserver(observer, call, this.c));
        } catch (Throwable th) {
            EmptyDisposable.a(th, (Observer<?>) observer);
        }
    }
}
