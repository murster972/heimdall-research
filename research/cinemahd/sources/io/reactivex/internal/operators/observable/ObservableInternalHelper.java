package io.reactivex.internal.operators.observable;

import io.reactivex.Emitter;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.observables.ConnectableObservable;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public final class ObservableInternalHelper {

    static final class BufferedReplayCallable<T> implements Callable<ConnectableObservable<T>> {

        /* renamed from: a  reason: collision with root package name */
        private final Observable<T> f6759a;
        private final int b;

        BufferedReplayCallable(Observable<T> observable, int i) {
            this.f6759a = observable;
            this.b = i;
        }

        public ConnectableObservable<T> call() {
            return this.f6759a.replay(this.b);
        }
    }

    static final class BufferedTimedReplayCallable<T> implements Callable<ConnectableObservable<T>> {

        /* renamed from: a  reason: collision with root package name */
        private final Observable<T> f6760a;
        private final int b;
        private final long c;
        private final TimeUnit d;
        private final Scheduler e;

        BufferedTimedReplayCallable(Observable<T> observable, int i, long j, TimeUnit timeUnit, Scheduler scheduler) {
            this.f6760a = observable;
            this.b = i;
            this.c = j;
            this.d = timeUnit;
            this.e = scheduler;
        }

        public ConnectableObservable<T> call() {
            return this.f6760a.replay(this.b, this.c, this.d, this.e);
        }
    }

    static final class FlatMapIntoIterable<T, U> implements Function<T, ObservableSource<U>> {

        /* renamed from: a  reason: collision with root package name */
        private final Function<? super T, ? extends Iterable<? extends U>> f6761a;

        FlatMapIntoIterable(Function<? super T, ? extends Iterable<? extends U>> function) {
            this.f6761a = function;
        }

        public ObservableSource<U> apply(T t) throws Exception {
            Object apply = this.f6761a.apply(t);
            ObjectHelper.a(apply, "The mapper returned a null Iterable");
            return new ObservableFromIterable((Iterable) apply);
        }
    }

    static final class FlatMapWithCombinerInner<U, R, T> implements Function<U, R> {

        /* renamed from: a  reason: collision with root package name */
        private final BiFunction<? super T, ? super U, ? extends R> f6762a;
        private final T b;

        FlatMapWithCombinerInner(BiFunction<? super T, ? super U, ? extends R> biFunction, T t) {
            this.f6762a = biFunction;
            this.b = t;
        }

        public R apply(U u) throws Exception {
            return this.f6762a.a(this.b, u);
        }
    }

    static final class FlatMapWithCombinerOuter<T, R, U> implements Function<T, ObservableSource<R>> {

        /* renamed from: a  reason: collision with root package name */
        private final BiFunction<? super T, ? super U, ? extends R> f6763a;
        private final Function<? super T, ? extends ObservableSource<? extends U>> b;

        FlatMapWithCombinerOuter(BiFunction<? super T, ? super U, ? extends R> biFunction, Function<? super T, ? extends ObservableSource<? extends U>> function) {
            this.f6763a = biFunction;
            this.b = function;
        }

        public ObservableSource<R> apply(T t) throws Exception {
            Object apply = this.b.apply(t);
            ObjectHelper.a(apply, "The mapper returned a null ObservableSource");
            return new ObservableMap((ObservableSource) apply, new FlatMapWithCombinerInner(this.f6763a, t));
        }
    }

    static final class ItemDelayFunction<T, U> implements Function<T, ObservableSource<T>> {

        /* renamed from: a  reason: collision with root package name */
        final Function<? super T, ? extends ObservableSource<U>> f6764a;

        ItemDelayFunction(Function<? super T, ? extends ObservableSource<U>> function) {
            this.f6764a = function;
        }

        public ObservableSource<T> apply(T t) throws Exception {
            Object apply = this.f6764a.apply(t);
            ObjectHelper.a(apply, "The itemDelay returned a null ObservableSource");
            return new ObservableTake((ObservableSource) apply, 1).map(Functions.c(t)).defaultIfEmpty(t);
        }
    }

    static final class ObserverOnComplete<T> implements Action {

        /* renamed from: a  reason: collision with root package name */
        final Observer<T> f6765a;

        ObserverOnComplete(Observer<T> observer) {
            this.f6765a = observer;
        }

        public void run() throws Exception {
            this.f6765a.onComplete();
        }
    }

    static final class ObserverOnError<T> implements Consumer<Throwable> {

        /* renamed from: a  reason: collision with root package name */
        final Observer<T> f6766a;

        ObserverOnError(Observer<T> observer) {
            this.f6766a = observer;
        }

        /* renamed from: a */
        public void accept(Throwable th) throws Exception {
            this.f6766a.onError(th);
        }
    }

    static final class ObserverOnNext<T> implements Consumer<T> {

        /* renamed from: a  reason: collision with root package name */
        final Observer<T> f6767a;

        ObserverOnNext(Observer<T> observer) {
            this.f6767a = observer;
        }

        public void accept(T t) throws Exception {
            this.f6767a.onNext(t);
        }
    }

    static final class ReplayCallable<T> implements Callable<ConnectableObservable<T>> {

        /* renamed from: a  reason: collision with root package name */
        private final Observable<T> f6768a;

        ReplayCallable(Observable<T> observable) {
            this.f6768a = observable;
        }

        public ConnectableObservable<T> call() {
            return this.f6768a.replay();
        }
    }

    static final class ReplayFunction<T, R> implements Function<Observable<T>, ObservableSource<R>> {

        /* renamed from: a  reason: collision with root package name */
        private final Function<? super Observable<T>, ? extends ObservableSource<R>> f6769a;
        private final Scheduler b;

        ReplayFunction(Function<? super Observable<T>, ? extends ObservableSource<R>> function, Scheduler scheduler) {
            this.f6769a = function;
            this.b = scheduler;
        }

        /* renamed from: a */
        public ObservableSource<R> apply(Observable<T> observable) throws Exception {
            Object apply = this.f6769a.apply(observable);
            ObjectHelper.a(apply, "The selector returned a null ObservableSource");
            return Observable.wrap((ObservableSource) apply).observeOn(this.b);
        }
    }

    static final class SimpleBiGenerator<T, S> implements BiFunction<S, Emitter<T>, S> {

        /* renamed from: a  reason: collision with root package name */
        final BiConsumer<S, Emitter<T>> f6770a;

        SimpleBiGenerator(BiConsumer<S, Emitter<T>> biConsumer) {
            this.f6770a = biConsumer;
        }

        public /* bridge */ /* synthetic */ Object a(Object obj, Object obj2) throws Exception {
            a(obj, (Emitter) obj2);
            return obj;
        }

        public S a(S s, Emitter<T> emitter) throws Exception {
            this.f6770a.a(s, emitter);
            return s;
        }
    }

    static final class SimpleGenerator<T, S> implements BiFunction<S, Emitter<T>, S> {

        /* renamed from: a  reason: collision with root package name */
        final Consumer<Emitter<T>> f6771a;

        SimpleGenerator(Consumer<Emitter<T>> consumer) {
            this.f6771a = consumer;
        }

        public /* bridge */ /* synthetic */ Object a(Object obj, Object obj2) throws Exception {
            a(obj, (Emitter) obj2);
            return obj;
        }

        public S a(S s, Emitter<T> emitter) throws Exception {
            this.f6771a.accept(emitter);
            return s;
        }
    }

    static final class TimedReplayCallable<T> implements Callable<ConnectableObservable<T>> {

        /* renamed from: a  reason: collision with root package name */
        private final Observable<T> f6772a;
        private final long b;
        private final TimeUnit c;
        private final Scheduler d;

        TimedReplayCallable(Observable<T> observable, long j, TimeUnit timeUnit, Scheduler scheduler) {
            this.f6772a = observable;
            this.b = j;
            this.c = timeUnit;
            this.d = scheduler;
        }

        public ConnectableObservable<T> call() {
            return this.f6772a.replay(this.b, this.c, this.d);
        }
    }

    static final class ZipIterableFunction<T, R> implements Function<List<ObservableSource<? extends T>>, ObservableSource<? extends R>> {

        /* renamed from: a  reason: collision with root package name */
        private final Function<? super Object[], ? extends R> f6773a;

        ZipIterableFunction(Function<? super Object[], ? extends R> function) {
            this.f6773a = function;
        }

        /* renamed from: a */
        public ObservableSource<? extends R> apply(List<ObservableSource<? extends T>> list) {
            return Observable.zipIterable(list, this.f6773a, false, Observable.bufferSize());
        }
    }

    private ObservableInternalHelper() {
        throw new IllegalStateException("No instances!");
    }

    public static <T, S> BiFunction<S, Emitter<T>, S> a(Consumer<Emitter<T>> consumer) {
        return new SimpleGenerator(consumer);
    }

    public static <T, U> Function<T, ObservableSource<T>> b(Function<? super T, ? extends ObservableSource<U>> function) {
        return new ItemDelayFunction(function);
    }

    public static <T> Consumer<T> c(Observer<T> observer) {
        return new ObserverOnNext(observer);
    }

    public static <T, S> BiFunction<S, Emitter<T>, S> a(BiConsumer<S, Emitter<T>> biConsumer) {
        return new SimpleBiGenerator(biConsumer);
    }

    public static <T> Consumer<Throwable> b(Observer<T> observer) {
        return new ObserverOnError(observer);
    }

    public static <T, R> Function<List<ObservableSource<? extends T>>, ObservableSource<? extends R>> c(Function<? super Object[], ? extends R> function) {
        return new ZipIterableFunction(function);
    }

    public static <T> Action a(Observer<T> observer) {
        return new ObserverOnComplete(observer);
    }

    public static <T, U, R> Function<T, ObservableSource<R>> a(Function<? super T, ? extends ObservableSource<? extends U>> function, BiFunction<? super T, ? super U, ? extends R> biFunction) {
        return new FlatMapWithCombinerOuter(biFunction, function);
    }

    public static <T, U> Function<T, ObservableSource<U>> a(Function<? super T, ? extends Iterable<? extends U>> function) {
        return new FlatMapIntoIterable(function);
    }

    public static <T> Callable<ConnectableObservable<T>> a(Observable<T> observable) {
        return new ReplayCallable(observable);
    }

    public static <T> Callable<ConnectableObservable<T>> a(Observable<T> observable, int i) {
        return new BufferedReplayCallable(observable, i);
    }

    public static <T> Callable<ConnectableObservable<T>> a(Observable<T> observable, int i, long j, TimeUnit timeUnit, Scheduler scheduler) {
        return new BufferedTimedReplayCallable(observable, i, j, timeUnit, scheduler);
    }

    public static <T> Callable<ConnectableObservable<T>> a(Observable<T> observable, long j, TimeUnit timeUnit, Scheduler scheduler) {
        return new TimedReplayCallable(observable, j, timeUnit, scheduler);
    }

    public static <T, R> Function<Observable<T>, ObservableSource<R>> a(Function<? super Observable<T>, ? extends ObservableSource<R>> function, Scheduler scheduler) {
        return new ReplayFunction(function, scheduler);
    }
}
