package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.subjects.PublishSubject;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservablePublishSelector<T, R> extends AbstractObservableWithUpstream<T, R> {
    final Function<? super Observable<T>, ? extends ObservableSource<R>> b;

    static final class SourceObserver<T, R> implements Observer<T> {

        /* renamed from: a  reason: collision with root package name */
        final PublishSubject<T> f6790a;
        final AtomicReference<Disposable> b;

        SourceObserver(PublishSubject<T> publishSubject, AtomicReference<Disposable> atomicReference) {
            this.f6790a = publishSubject;
            this.b = atomicReference;
        }

        public void onComplete() {
            this.f6790a.onComplete();
        }

        public void onError(Throwable th) {
            this.f6790a.onError(th);
        }

        public void onNext(T t) {
            this.f6790a.onNext(t);
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.b, disposable);
        }
    }

    static final class TargetObserver<T, R> extends AtomicReference<Disposable> implements Observer<R>, Disposable {
        private static final long serialVersionUID = 854110278590336484L;
        final Observer<? super R> downstream;
        Disposable upstream;

        TargetObserver(Observer<? super R> observer) {
            this.downstream = observer;
        }

        public void dispose() {
            this.upstream.dispose();
            DisposableHelper.a((AtomicReference<Disposable>) this);
        }

        public boolean isDisposed() {
            return this.upstream.isDisposed();
        }

        public void onComplete() {
            DisposableHelper.a((AtomicReference<Disposable>) this);
            this.downstream.onComplete();
        }

        public void onError(Throwable th) {
            DisposableHelper.a((AtomicReference<Disposable>) this);
            this.downstream.onError(th);
        }

        public void onNext(R r) {
            this.downstream.onNext(r);
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.upstream, disposable)) {
                this.upstream = disposable;
                this.downstream.onSubscribe(this);
            }
        }
    }

    public ObservablePublishSelector(ObservableSource<T> observableSource, Function<? super Observable<T>, ? extends ObservableSource<R>> function) {
        super(observableSource);
        this.b = function;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super R> observer) {
        PublishSubject b2 = PublishSubject.b();
        try {
            Object apply = this.b.apply(b2);
            ObjectHelper.a(apply, "The selector returned a null ObservableSource");
            ObservableSource observableSource = (ObservableSource) apply;
            TargetObserver targetObserver = new TargetObserver(observer);
            observableSource.subscribe(targetObserver);
            this.f6691a.subscribe(new SourceObserver(b2, targetObserver));
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (Observer<?>) observer);
        }
    }
}
