package io.reactivex.internal.operators.observable;

import com.facebook.common.time.Clock;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.subjects.UnicastSubject;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableWindowTimed<T> extends AbstractObservableWithUpstream<T, Observable<T>> {
    final long b;
    final long c;
    final TimeUnit d;
    final Scheduler e;
    final long f;
    final int g;
    final boolean h;

    static final class WindowExactBoundedObserver<T> extends QueueDrainObserver<T, Object, Observable<T>> implements Disposable {
        final long g;
        final TimeUnit h;
        final Scheduler i;
        final int j;
        final boolean k;
        final long l;
        final Scheduler.Worker m;
        long n;
        long o;
        Disposable p;
        UnicastSubject<T> q;
        volatile boolean r;
        final AtomicReference<Disposable> s = new AtomicReference<>();

        static final class ConsumerIndexHolder implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final long f6842a;
            final WindowExactBoundedObserver<?> b;

            ConsumerIndexHolder(long j, WindowExactBoundedObserver<?> windowExactBoundedObserver) {
                this.f6842a = j;
                this.b = windowExactBoundedObserver;
            }

            public void run() {
                WindowExactBoundedObserver<?> windowExactBoundedObserver = this.b;
                if (!windowExactBoundedObserver.d) {
                    windowExactBoundedObserver.c.offer(this);
                } else {
                    windowExactBoundedObserver.r = true;
                    windowExactBoundedObserver.f();
                }
                if (windowExactBoundedObserver.d()) {
                    windowExactBoundedObserver.g();
                }
            }
        }

        WindowExactBoundedObserver(Observer<? super Observable<T>> observer, long j2, TimeUnit timeUnit, Scheduler scheduler, int i2, long j3, boolean z) {
            super(observer, new MpscLinkedQueue());
            this.g = j2;
            this.h = timeUnit;
            this.i = scheduler;
            this.j = i2;
            this.l = j3;
            this.k = z;
            if (z) {
                this.m = scheduler.a();
            } else {
                this.m = null;
            }
        }

        public void dispose() {
            this.d = true;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            DisposableHelper.a(this.s);
            Scheduler.Worker worker = this.m;
            if (worker != null) {
                worker.dispose();
            }
        }

        /* access modifiers changed from: package-private */
        public void g() {
            MpscLinkedQueue mpscLinkedQueue = (MpscLinkedQueue) this.c;
            Observer<? super V> observer = this.b;
            UnicastSubject<T> unicastSubject = this.q;
            int i2 = 1;
            while (!this.r) {
                boolean z = this.e;
                Object poll = mpscLinkedQueue.poll();
                boolean z2 = poll == null;
                boolean z3 = poll instanceof ConsumerIndexHolder;
                if (z && (z2 || z3)) {
                    this.q = null;
                    mpscLinkedQueue.clear();
                    f();
                    Throwable th = this.f;
                    if (th != null) {
                        unicastSubject.onError(th);
                        return;
                    } else {
                        unicastSubject.onComplete();
                        return;
                    }
                } else if (z2) {
                    i2 = a(-i2);
                    if (i2 == 0) {
                        return;
                    }
                } else if (z3) {
                    ConsumerIndexHolder consumerIndexHolder = (ConsumerIndexHolder) poll;
                    if (this.k || this.o == consumerIndexHolder.f6842a) {
                        unicastSubject.onComplete();
                        this.n = 0;
                        unicastSubject = UnicastSubject.a(this.j);
                        this.q = unicastSubject;
                        observer.onNext(unicastSubject);
                    }
                } else {
                    NotificationLite.b(poll);
                    unicastSubject.onNext(poll);
                    long j2 = this.n + 1;
                    if (j2 >= this.l) {
                        this.o++;
                        this.n = 0;
                        unicastSubject.onComplete();
                        unicastSubject = UnicastSubject.a(this.j);
                        this.q = unicastSubject;
                        this.b.onNext(unicastSubject);
                        if (this.k) {
                            Disposable disposable = this.s.get();
                            disposable.dispose();
                            Scheduler.Worker worker = this.m;
                            ConsumerIndexHolder consumerIndexHolder2 = new ConsumerIndexHolder(this.o, this);
                            long j3 = this.g;
                            Disposable a2 = worker.a(consumerIndexHolder2, j3, j3, this.h);
                            if (!this.s.compareAndSet(disposable, a2)) {
                                a2.dispose();
                            }
                        }
                    } else {
                        this.n = j2;
                    }
                }
            }
            this.p.dispose();
            mpscLinkedQueue.clear();
            f();
        }

        public boolean isDisposed() {
            return this.d;
        }

        public void onComplete() {
            this.e = true;
            if (d()) {
                g();
            }
            this.b.onComplete();
            f();
        }

        public void onError(Throwable th) {
            this.f = th;
            this.e = true;
            if (d()) {
                g();
            }
            this.b.onError(th);
            f();
        }

        public void onNext(T t) {
            if (!this.r) {
                if (e()) {
                    UnicastSubject<T> unicastSubject = this.q;
                    unicastSubject.onNext(t);
                    long j2 = this.n + 1;
                    if (j2 >= this.l) {
                        this.o++;
                        this.n = 0;
                        unicastSubject.onComplete();
                        UnicastSubject<T> a2 = UnicastSubject.a(this.j);
                        this.q = a2;
                        this.b.onNext(a2);
                        if (this.k) {
                            this.s.get().dispose();
                            Scheduler.Worker worker = this.m;
                            ConsumerIndexHolder consumerIndexHolder = new ConsumerIndexHolder(this.o, this);
                            long j3 = this.g;
                            DisposableHelper.a(this.s, worker.a(consumerIndexHolder, j3, j3, this.h));
                        }
                    } else {
                        this.n = j2;
                    }
                    if (a(-1) == 0) {
                        return;
                    }
                } else {
                    SimplePlainQueue<U> simplePlainQueue = this.c;
                    NotificationLite.e(t);
                    simplePlainQueue.offer(t);
                    if (!d()) {
                        return;
                    }
                }
                g();
            }
        }

        public void onSubscribe(Disposable disposable) {
            Disposable disposable2;
            if (DisposableHelper.a(this.p, disposable)) {
                this.p = disposable;
                Observer<? super V> observer = this.b;
                observer.onSubscribe(this);
                if (!this.d) {
                    UnicastSubject<T> a2 = UnicastSubject.a(this.j);
                    this.q = a2;
                    observer.onNext(a2);
                    ConsumerIndexHolder consumerIndexHolder = new ConsumerIndexHolder(this.o, this);
                    if (this.k) {
                        Scheduler.Worker worker = this.m;
                        long j2 = this.g;
                        disposable2 = worker.a(consumerIndexHolder, j2, j2, this.h);
                    } else {
                        Scheduler scheduler = this.i;
                        long j3 = this.g;
                        disposable2 = scheduler.a(consumerIndexHolder, j3, j3, this.h);
                    }
                    DisposableHelper.a(this.s, disposable2);
                }
            }
        }
    }

    static final class WindowExactUnboundedObserver<T> extends QueueDrainObserver<T, Object, Observable<T>> implements Observer<T>, Disposable, Runnable {
        static final Object o = new Object();
        final long g;
        final TimeUnit h;
        final Scheduler i;
        final int j;
        Disposable k;
        UnicastSubject<T> l;
        final AtomicReference<Disposable> m = new AtomicReference<>();
        volatile boolean n;

        WindowExactUnboundedObserver(Observer<? super Observable<T>> observer, long j2, TimeUnit timeUnit, Scheduler scheduler, int i2) {
            super(observer, new MpscLinkedQueue());
            this.g = j2;
            this.h = timeUnit;
            this.i = scheduler;
            this.j = i2;
        }

        public void dispose() {
            this.d = true;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            DisposableHelper.a(this.m);
        }

        /* access modifiers changed from: package-private */
        public void g() {
            MpscLinkedQueue mpscLinkedQueue = (MpscLinkedQueue) this.c;
            Observer<? super V> observer = this.b;
            UnicastSubject<T> unicastSubject = this.l;
            int i2 = 1;
            while (true) {
                boolean z = this.n;
                boolean z2 = this.e;
                Object poll = mpscLinkedQueue.poll();
                if (!z2 || !(poll == null || poll == o)) {
                    if (poll == null) {
                        i2 = a(-i2);
                        if (i2 == 0) {
                            return;
                        }
                    } else if (poll == o) {
                        unicastSubject.onComplete();
                        if (!z) {
                            unicastSubject = UnicastSubject.a(this.j);
                            this.l = unicastSubject;
                            observer.onNext(unicastSubject);
                        } else {
                            this.k.dispose();
                        }
                    } else {
                        NotificationLite.b(poll);
                        unicastSubject.onNext(poll);
                    }
                }
            }
            this.l = null;
            mpscLinkedQueue.clear();
            f();
            Throwable th = this.f;
            if (th != null) {
                unicastSubject.onError(th);
            } else {
                unicastSubject.onComplete();
            }
        }

        public boolean isDisposed() {
            return this.d;
        }

        public void onComplete() {
            this.e = true;
            if (d()) {
                g();
            }
            f();
            this.b.onComplete();
        }

        public void onError(Throwable th) {
            this.f = th;
            this.e = true;
            if (d()) {
                g();
            }
            f();
            this.b.onError(th);
        }

        public void onNext(T t) {
            if (!this.n) {
                if (e()) {
                    this.l.onNext(t);
                    if (a(-1) == 0) {
                        return;
                    }
                } else {
                    SimplePlainQueue<U> simplePlainQueue = this.c;
                    NotificationLite.e(t);
                    simplePlainQueue.offer(t);
                    if (!d()) {
                        return;
                    }
                }
                g();
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.k, disposable)) {
                this.k = disposable;
                this.l = UnicastSubject.a(this.j);
                Observer<? super V> observer = this.b;
                observer.onSubscribe(this);
                observer.onNext(this.l);
                if (!this.d) {
                    Scheduler scheduler = this.i;
                    long j2 = this.g;
                    DisposableHelper.a(this.m, scheduler.a(this, j2, j2, this.h));
                }
            }
        }

        public void run() {
            if (this.d) {
                this.n = true;
                f();
            }
            this.c.offer(o);
            if (d()) {
                g();
            }
        }
    }

    static final class WindowSkipObserver<T> extends QueueDrainObserver<T, Object, Observable<T>> implements Disposable, Runnable {
        final long g;
        final long h;
        final TimeUnit i;
        final Scheduler.Worker j;
        final int k;
        final List<UnicastSubject<T>> l = new LinkedList();
        Disposable m;
        volatile boolean n;

        final class CompletionTask implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            private final UnicastSubject<T> f6843a;

            CompletionTask(UnicastSubject<T> unicastSubject) {
                this.f6843a = unicastSubject;
            }

            public void run() {
                WindowSkipObserver.this.a(this.f6843a);
            }
        }

        static final class SubjectWork<T> {

            /* renamed from: a  reason: collision with root package name */
            final UnicastSubject<T> f6844a;
            final boolean b;

            SubjectWork(UnicastSubject<T> unicastSubject, boolean z) {
                this.f6844a = unicastSubject;
                this.b = z;
            }
        }

        WindowSkipObserver(Observer<? super Observable<T>> observer, long j2, long j3, TimeUnit timeUnit, Scheduler.Worker worker, int i2) {
            super(observer, new MpscLinkedQueue());
            this.g = j2;
            this.h = j3;
            this.i = timeUnit;
            this.j = worker;
            this.k = i2;
        }

        /* access modifiers changed from: package-private */
        public void a(UnicastSubject<T> unicastSubject) {
            this.c.offer(new SubjectWork(unicastSubject, false));
            if (d()) {
                g();
            }
        }

        public void dispose() {
            this.d = true;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.j.dispose();
        }

        /* access modifiers changed from: package-private */
        public void g() {
            MpscLinkedQueue mpscLinkedQueue = (MpscLinkedQueue) this.c;
            Observer<? super V> observer = this.b;
            List<UnicastSubject<T>> list = this.l;
            int i2 = 1;
            while (!this.n) {
                boolean z = this.e;
                Object poll = mpscLinkedQueue.poll();
                boolean z2 = poll == null;
                boolean z3 = poll instanceof SubjectWork;
                if (z && (z2 || z3)) {
                    mpscLinkedQueue.clear();
                    Throwable th = this.f;
                    if (th != null) {
                        for (UnicastSubject<T> onError : list) {
                            onError.onError(th);
                        }
                    } else {
                        for (UnicastSubject<T> onComplete : list) {
                            onComplete.onComplete();
                        }
                    }
                    f();
                    list.clear();
                    return;
                } else if (z2) {
                    i2 = a(-i2);
                    if (i2 == 0) {
                        return;
                    }
                } else if (z3) {
                    SubjectWork subjectWork = (SubjectWork) poll;
                    if (!subjectWork.b) {
                        list.remove(subjectWork.f6844a);
                        subjectWork.f6844a.onComplete();
                        if (list.isEmpty() && this.d) {
                            this.n = true;
                        }
                    } else if (!this.d) {
                        UnicastSubject a2 = UnicastSubject.a(this.k);
                        list.add(a2);
                        observer.onNext(a2);
                        this.j.a(new CompletionTask(a2), this.g, this.i);
                    }
                } else {
                    for (UnicastSubject<T> onNext : list) {
                        onNext.onNext(poll);
                    }
                }
            }
            this.m.dispose();
            f();
            mpscLinkedQueue.clear();
            list.clear();
        }

        public boolean isDisposed() {
            return this.d;
        }

        public void onComplete() {
            this.e = true;
            if (d()) {
                g();
            }
            this.b.onComplete();
            f();
        }

        public void onError(Throwable th) {
            this.f = th;
            this.e = true;
            if (d()) {
                g();
            }
            this.b.onError(th);
            f();
        }

        public void onNext(T t) {
            if (e()) {
                for (UnicastSubject<T> onNext : this.l) {
                    onNext.onNext(t);
                }
                if (a(-1) == 0) {
                    return;
                }
            } else {
                this.c.offer(t);
                if (!d()) {
                    return;
                }
            }
            g();
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.m, disposable)) {
                this.m = disposable;
                this.b.onSubscribe(this);
                if (!this.d) {
                    UnicastSubject a2 = UnicastSubject.a(this.k);
                    this.l.add(a2);
                    this.b.onNext(a2);
                    this.j.a(new CompletionTask(a2), this.g, this.i);
                    Scheduler.Worker worker = this.j;
                    long j2 = this.h;
                    worker.a(this, j2, j2, this.i);
                }
            }
        }

        public void run() {
            SubjectWork subjectWork = new SubjectWork(UnicastSubject.a(this.k), true);
            if (!this.d) {
                this.c.offer(subjectWork);
            }
            if (d()) {
                g();
            }
        }
    }

    public ObservableWindowTimed(ObservableSource<T> observableSource, long j, long j2, TimeUnit timeUnit, Scheduler scheduler, long j3, int i, boolean z) {
        super(observableSource);
        this.b = j;
        this.c = j2;
        this.d = timeUnit;
        this.e = scheduler;
        this.f = j3;
        this.g = i;
        this.h = z;
    }

    public void subscribeActual(Observer<? super Observable<T>> observer) {
        SerializedObserver serializedObserver = new SerializedObserver(observer);
        long j = this.b;
        long j2 = this.c;
        if (j == j2) {
            long j3 = this.f;
            if (j3 == Clock.MAX_TIME) {
                this.f6691a.subscribe(new WindowExactUnboundedObserver(serializedObserver, j, this.d, this.e, this.g));
            } else {
                this.f6691a.subscribe(new WindowExactBoundedObserver(serializedObserver, j, this.d, this.e, this.g, j3, this.h));
            }
        } else {
            this.f6691a.subscribe(new WindowSkipObserver(serializedObserver, j, j2, this.d, this.e.a(), this.g));
        }
    }
}
