package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.UnicastSubject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableWindowBoundarySelector<T, B, V> extends AbstractObservableWithUpstream<T, Observable<T>> {
    final ObservableSource<B> b;
    final Function<? super B, ? extends ObservableSource<V>> c;
    final int d;

    static final class OperatorWindowBoundaryCloseObserver<T, V> extends DisposableObserver<V> {
        final WindowBoundaryMainObserver<T, ?, V> b;
        final UnicastSubject<T> c;
        boolean d;

        OperatorWindowBoundaryCloseObserver(WindowBoundaryMainObserver<T, ?, V> windowBoundaryMainObserver, UnicastSubject<T> unicastSubject) {
            this.b = windowBoundaryMainObserver;
            this.c = unicastSubject;
        }

        public void onComplete() {
            if (!this.d) {
                this.d = true;
                this.b.a(this);
            }
        }

        public void onError(Throwable th) {
            if (this.d) {
                RxJavaPlugins.b(th);
                return;
            }
            this.d = true;
            this.b.a(th);
        }

        public void onNext(V v) {
            dispose();
            onComplete();
        }
    }

    static final class OperatorWindowBoundaryOpenObserver<T, B> extends DisposableObserver<B> {
        final WindowBoundaryMainObserver<T, B, ?> b;

        OperatorWindowBoundaryOpenObserver(WindowBoundaryMainObserver<T, B, ?> windowBoundaryMainObserver) {
            this.b = windowBoundaryMainObserver;
        }

        public void onComplete() {
            this.b.onComplete();
        }

        public void onError(Throwable th) {
            this.b.a(th);
        }

        public void onNext(B b2) {
            this.b.a(b2);
        }
    }

    static final class WindowOperation<T, B> {

        /* renamed from: a  reason: collision with root package name */
        final UnicastSubject<T> f6840a;
        final B b;

        WindowOperation(UnicastSubject<T> unicastSubject, B b2) {
            this.f6840a = unicastSubject;
            this.b = b2;
        }
    }

    public ObservableWindowBoundarySelector(ObservableSource<T> observableSource, ObservableSource<B> observableSource2, Function<? super B, ? extends ObservableSource<V>> function, int i) {
        super(observableSource);
        this.b = observableSource2;
        this.c = function;
        this.d = i;
    }

    public void subscribeActual(Observer<? super Observable<T>> observer) {
        this.f6691a.subscribe(new WindowBoundaryMainObserver(new SerializedObserver(observer), this.b, this.c, this.d));
    }

    static final class WindowBoundaryMainObserver<T, B, V> extends QueueDrainObserver<T, Object, Observable<T>> implements Disposable {
        final ObservableSource<B> g;
        final Function<? super B, ? extends ObservableSource<V>> h;
        final int i;
        final CompositeDisposable j;
        Disposable k;
        final AtomicReference<Disposable> l = new AtomicReference<>();
        final List<UnicastSubject<T>> m;
        final AtomicLong n = new AtomicLong();

        WindowBoundaryMainObserver(Observer<? super Observable<T>> observer, ObservableSource<B> observableSource, Function<? super B, ? extends ObservableSource<V>> function, int i2) {
            super(observer, new MpscLinkedQueue());
            this.g = observableSource;
            this.h = function;
            this.i = i2;
            this.j = new CompositeDisposable();
            this.m = new ArrayList();
            this.n.lazySet(1);
        }

        public void a(Observer<? super Observable<T>> observer, Object obj) {
        }

        /* access modifiers changed from: package-private */
        public void a(Throwable th) {
            this.k.dispose();
            this.j.dispose();
            onError(th);
        }

        public void dispose() {
            this.d = true;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.j.dispose();
            DisposableHelper.a(this.l);
        }

        /* access modifiers changed from: package-private */
        public void g() {
            MpscLinkedQueue mpscLinkedQueue = (MpscLinkedQueue) this.c;
            Observer<? super V> observer = this.b;
            List<UnicastSubject<T>> list = this.m;
            int i2 = 1;
            while (true) {
                boolean z = this.e;
                Object poll = mpscLinkedQueue.poll();
                boolean z2 = poll == null;
                if (z && z2) {
                    f();
                    Throwable th = this.f;
                    if (th != null) {
                        for (UnicastSubject<T> onError : list) {
                            onError.onError(th);
                        }
                    } else {
                        for (UnicastSubject<T> onComplete : list) {
                            onComplete.onComplete();
                        }
                    }
                    list.clear();
                    return;
                } else if (z2) {
                    i2 = a(-i2);
                    if (i2 == 0) {
                        return;
                    }
                } else if (poll instanceof WindowOperation) {
                    WindowOperation windowOperation = (WindowOperation) poll;
                    UnicastSubject<T> unicastSubject = windowOperation.f6840a;
                    if (unicastSubject != null) {
                        if (list.remove(unicastSubject)) {
                            windowOperation.f6840a.onComplete();
                            if (this.n.decrementAndGet() == 0) {
                                f();
                                return;
                            }
                        } else {
                            continue;
                        }
                    } else if (!this.d) {
                        UnicastSubject a2 = UnicastSubject.a(this.i);
                        list.add(a2);
                        observer.onNext(a2);
                        try {
                            Object apply = this.h.apply(windowOperation.b);
                            ObjectHelper.a(apply, "The ObservableSource supplied is null");
                            ObservableSource observableSource = (ObservableSource) apply;
                            OperatorWindowBoundaryCloseObserver operatorWindowBoundaryCloseObserver = new OperatorWindowBoundaryCloseObserver(this, a2);
                            if (this.j.b(operatorWindowBoundaryCloseObserver)) {
                                this.n.getAndIncrement();
                                observableSource.subscribe(operatorWindowBoundaryCloseObserver);
                            }
                        } catch (Throwable th2) {
                            Exceptions.b(th2);
                            this.d = true;
                            observer.onError(th2);
                        }
                    }
                } else {
                    for (UnicastSubject<T> onNext : list) {
                        NotificationLite.b(poll);
                        onNext.onNext(poll);
                    }
                }
            }
        }

        public boolean isDisposed() {
            return this.d;
        }

        public void onComplete() {
            if (!this.e) {
                this.e = true;
                if (d()) {
                    g();
                }
                if (this.n.decrementAndGet() == 0) {
                    this.j.dispose();
                }
                this.b.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.e) {
                RxJavaPlugins.b(th);
                return;
            }
            this.f = th;
            this.e = true;
            if (d()) {
                g();
            }
            if (this.n.decrementAndGet() == 0) {
                this.j.dispose();
            }
            this.b.onError(th);
        }

        public void onNext(T t) {
            if (e()) {
                for (UnicastSubject<T> onNext : this.m) {
                    onNext.onNext(t);
                }
                if (a(-1) == 0) {
                    return;
                }
            } else {
                SimplePlainQueue<U> simplePlainQueue = this.c;
                NotificationLite.e(t);
                simplePlainQueue.offer(t);
                if (!d()) {
                    return;
                }
            }
            g();
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.k, disposable)) {
                this.k = disposable;
                this.b.onSubscribe(this);
                if (!this.d) {
                    OperatorWindowBoundaryOpenObserver operatorWindowBoundaryOpenObserver = new OperatorWindowBoundaryOpenObserver(this);
                    if (this.l.compareAndSet((Object) null, operatorWindowBoundaryOpenObserver)) {
                        this.n.getAndIncrement();
                        this.g.subscribe(operatorWindowBoundaryOpenObserver);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(B b) {
            this.c.offer(new WindowOperation((UnicastSubject) null, b));
            if (d()) {
                g();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(OperatorWindowBoundaryCloseObserver<T, V> operatorWindowBoundaryCloseObserver) {
            this.j.c(operatorWindowBoundaryCloseObserver);
            this.c.offer(new WindowOperation(operatorWindowBoundaryCloseObserver.c, null));
            if (d()) {
                g();
            }
        }
    }
}
