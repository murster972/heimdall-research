package io.reactivex.internal.operators.observable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.plugins.RxJavaPlugins;

public final class ObservableIgnoreElementsCompletable<T> extends Completable implements FuseToObservable<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6757a;

    static final class IgnoreObservable<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final CompletableObserver f6758a;
        Disposable b;

        IgnoreObservable(CompletableObserver completableObserver) {
            this.f6758a = completableObserver;
        }

        public void dispose() {
            this.b.dispose();
        }

        public boolean isDisposed() {
            return this.b.isDisposed();
        }

        public void onComplete() {
            this.f6758a.onComplete();
        }

        public void onError(Throwable th) {
            this.f6758a.onError(th);
        }

        public void onNext(T t) {
        }

        public void onSubscribe(Disposable disposable) {
            this.b = disposable;
            this.f6758a.onSubscribe(this);
        }
    }

    public ObservableIgnoreElementsCompletable(ObservableSource<T> observableSource) {
        this.f6757a = observableSource;
    }

    public Observable<T> a() {
        return RxJavaPlugins.a(new ObservableIgnoreElements(this.f6757a));
    }

    public void b(CompletableObserver completableObserver) {
        this.f6757a.subscribe(new IgnoreObservable(completableObserver));
    }
}
