package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.BasicQueueDisposable;

public final class ObservableFromArray<T> extends Observable<T> {

    /* renamed from: a  reason: collision with root package name */
    final T[] f6741a;

    static final class FromArrayDisposable<T> extends BasicQueueDisposable<T> {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6742a;
        final T[] b;
        int c;
        boolean d;
        volatile boolean e;

        FromArrayDisposable(Observer<? super T> observer, T[] tArr) {
            this.f6742a = observer;
            this.b = tArr;
        }

        public int a(int i) {
            if ((i & 1) == 0) {
                return 0;
            }
            this.d = true;
            return 1;
        }

        public void clear() {
            this.c = this.b.length;
        }

        public void dispose() {
            this.e = true;
        }

        public boolean isDisposed() {
            return this.e;
        }

        public boolean isEmpty() {
            return this.c == this.b.length;
        }

        public T poll() {
            int i = this.c;
            T[] tArr = this.b;
            if (i == tArr.length) {
                return null;
            }
            this.c = i + 1;
            T t = tArr[i];
            ObjectHelper.a(t, "The array element is null");
            return t;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            T[] tArr = this.b;
            int length = tArr.length;
            for (int i = 0; i < length && !isDisposed(); i++) {
                T t = tArr[i];
                if (t == null) {
                    Observer<? super T> observer = this.f6742a;
                    observer.onError(new NullPointerException("The " + i + "th element is null"));
                    return;
                }
                this.f6742a.onNext(t);
            }
            if (!isDisposed()) {
                this.f6742a.onComplete();
            }
        }
    }

    public ObservableFromArray(T[] tArr) {
        this.f6741a = tArr;
    }

    public void subscribeActual(Observer<? super T> observer) {
        FromArrayDisposable fromArrayDisposable = new FromArrayDisposable(observer, this.f6741a);
        observer.onSubscribe(fromArrayDisposable);
        if (!fromArrayDisposable.d) {
            fromArrayDisposable.a();
        }
    }
}
