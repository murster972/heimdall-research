package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public final class ObservableCount<T> extends AbstractObservableWithUpstream<T, Long> {

    static final class CountObserver implements Observer<Object>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super Long> f6714a;
        Disposable b;
        long c;

        CountObserver(Observer<? super Long> observer) {
            this.f6714a = observer;
        }

        public void dispose() {
            this.b.dispose();
        }

        public boolean isDisposed() {
            return this.b.isDisposed();
        }

        public void onComplete() {
            this.f6714a.onNext(Long.valueOf(this.c));
            this.f6714a.onComplete();
        }

        public void onError(Throwable th) {
            this.f6714a.onError(th);
        }

        public void onNext(Object obj) {
            this.c++;
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.b, disposable)) {
                this.b = disposable;
                this.f6714a.onSubscribe(this);
            }
        }
    }

    public ObservableCount(ObservableSource<T> observableSource) {
        super(observableSource);
    }

    public void subscribeActual(Observer<? super Long> observer) {
        this.f6691a.subscribe(new CountObserver(observer));
    }
}
