package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.operators.observable.ObservableReduceSeedSingle;
import java.util.concurrent.Callable;

public final class ObservableReduceWithSingle<T, R> extends Single<R> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6797a;
    final Callable<R> b;
    final BiFunction<R, ? super T, R> c;

    public ObservableReduceWithSingle(ObservableSource<T> observableSource, Callable<R> callable, BiFunction<R, ? super T, R> biFunction) {
        this.f6797a = observableSource;
        this.b = callable;
        this.c = biFunction;
    }

    /* access modifiers changed from: protected */
    public void b(SingleObserver<? super R> singleObserver) {
        try {
            R call = this.b.call();
            ObjectHelper.a(call, "The seedSupplier returned a null value");
            this.f6797a.subscribe(new ObservableReduceSeedSingle.ReduceSeedObserver(singleObserver, this.c, call));
        } catch (Throwable th) {
            Exceptions.b(th);
            EmptyDisposable.a(th, (SingleObserver<?>) singleObserver);
        }
    }
}
