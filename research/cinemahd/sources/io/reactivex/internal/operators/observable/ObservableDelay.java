package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.observers.SerializedObserver;
import java.util.concurrent.TimeUnit;

public final class ObservableDelay<T> extends AbstractObservableWithUpstream<T, T> {
    final long b;
    final TimeUnit c;
    final Scheduler d;
    final boolean e;

    static final class DelayObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6721a;
        final long b;
        final TimeUnit c;
        final Scheduler.Worker d;
        final boolean e;
        Disposable f;

        final class OnComplete implements Runnable {
            OnComplete() {
            }

            public void run() {
                try {
                    DelayObserver.this.f6721a.onComplete();
                } finally {
                    DelayObserver.this.d.dispose();
                }
            }
        }

        final class OnError implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            private final Throwable f6723a;

            OnError(Throwable th) {
                this.f6723a = th;
            }

            public void run() {
                try {
                    DelayObserver.this.f6721a.onError(this.f6723a);
                } finally {
                    DelayObserver.this.d.dispose();
                }
            }
        }

        final class OnNext implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            private final T f6724a;

            OnNext(T t) {
                this.f6724a = t;
            }

            public void run() {
                DelayObserver.this.f6721a.onNext(this.f6724a);
            }
        }

        DelayObserver(Observer<? super T> observer, long j, TimeUnit timeUnit, Scheduler.Worker worker, boolean z) {
            this.f6721a = observer;
            this.b = j;
            this.c = timeUnit;
            this.d = worker;
            this.e = z;
        }

        public void dispose() {
            this.f.dispose();
            this.d.dispose();
        }

        public boolean isDisposed() {
            return this.d.isDisposed();
        }

        public void onComplete() {
            this.d.a(new OnComplete(), this.b, this.c);
        }

        public void onError(Throwable th) {
            this.d.a(new OnError(th), this.e ? this.b : 0, this.c);
        }

        public void onNext(T t) {
            this.d.a(new OnNext(t), this.b, this.c);
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.f, disposable)) {
                this.f = disposable;
                this.f6721a.onSubscribe(this);
            }
        }
    }

    public ObservableDelay(ObservableSource<T> observableSource, long j, TimeUnit timeUnit, Scheduler scheduler, boolean z) {
        super(observableSource);
        this.b = j;
        this.c = timeUnit;
        this.d = scheduler;
        this.e = z;
    }

    public void subscribeActual(Observer<? super T> observer) {
        SerializedObserver serializedObserver;
        if (this.e) {
            serializedObserver = observer;
        } else {
            serializedObserver = new SerializedObserver(observer);
        }
        this.f6691a.subscribe(new DelayObserver(serializedObserver, this.b, this.c, this.d.a(), this.e));
    }
}
