package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;

public final class ObservableZipIterable<T, U, V> extends Observable<V> {

    /* renamed from: a  reason: collision with root package name */
    final Observable<? extends T> f6849a;
    final Iterable<U> b;
    final BiFunction<? super T, ? super U, ? extends V> c;

    static final class ZipIterableObserver<T, U, V> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super V> f6850a;
        final Iterator<U> b;
        final BiFunction<? super T, ? super U, ? extends V> c;
        Disposable d;
        boolean e;

        ZipIterableObserver(Observer<? super V> observer, Iterator<U> it2, BiFunction<? super T, ? super U, ? extends V> biFunction) {
            this.f6850a = observer;
            this.b = it2;
            this.c = biFunction;
        }

        /* access modifiers changed from: package-private */
        public void a(Throwable th) {
            this.e = true;
            this.d.dispose();
            this.f6850a.onError(th);
        }

        public void dispose() {
            this.d.dispose();
        }

        public boolean isDisposed() {
            return this.d.isDisposed();
        }

        public void onComplete() {
            if (!this.e) {
                this.e = true;
                this.f6850a.onComplete();
            }
        }

        public void onError(Throwable th) {
            if (this.e) {
                RxJavaPlugins.b(th);
                return;
            }
            this.e = true;
            this.f6850a.onError(th);
        }

        public void onNext(T t) {
            if (!this.e) {
                try {
                    U next = this.b.next();
                    ObjectHelper.a(next, "The iterator returned a null value");
                    try {
                        Object a2 = this.c.a(t, next);
                        ObjectHelper.a(a2, "The zipper function returned a null value");
                        this.f6850a.onNext(a2);
                        try {
                            if (!this.b.hasNext()) {
                                this.e = true;
                                this.d.dispose();
                                this.f6850a.onComplete();
                            }
                        } catch (Throwable th) {
                            Exceptions.b(th);
                            a(th);
                        }
                    } catch (Throwable th2) {
                        Exceptions.b(th2);
                        a(th2);
                    }
                } catch (Throwable th3) {
                    Exceptions.b(th3);
                    a(th3);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.f6850a.onSubscribe(this);
            }
        }
    }

    public ObservableZipIterable(Observable<? extends T> observable, Iterable<U> iterable, BiFunction<? super T, ? super U, ? extends V> biFunction) {
        this.f6849a = observable;
        this.b = iterable;
        this.c = biFunction;
    }

    public void subscribeActual(Observer<? super V> observer) {
        try {
            Iterator<U> it2 = this.b.iterator();
            ObjectHelper.a(it2, "The iterator returned by other is null");
            Iterator it3 = it2;
            try {
                if (!it3.hasNext()) {
                    EmptyDisposable.a((Observer<?>) observer);
                } else {
                    this.f6849a.subscribe(new ZipIterableObserver(observer, it3, this.c));
                }
            } catch (Throwable th) {
                Exceptions.b(th);
                EmptyDisposable.a(th, (Observer<?>) observer);
            }
        } catch (Throwable th2) {
            Exceptions.b(th2);
            EmptyDisposable.a(th2, (Observer<?>) observer);
        }
    }
}
