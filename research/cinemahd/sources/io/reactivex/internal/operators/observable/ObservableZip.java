package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableZip<T, R> extends Observable<R> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<? extends T>[] f6847a;
    final Iterable<? extends ObservableSource<? extends T>> b;
    final Function<? super Object[], ? extends R> c;
    final int d;
    final boolean e;

    static final class ZipObserver<T, R> implements Observer<T> {

        /* renamed from: a  reason: collision with root package name */
        final ZipCoordinator<T, R> f6848a;
        final SpscLinkedArrayQueue<T> b;
        volatile boolean c;
        Throwable d;
        final AtomicReference<Disposable> e = new AtomicReference<>();

        ZipObserver(ZipCoordinator<T, R> zipCoordinator, int i) {
            this.f6848a = zipCoordinator;
            this.b = new SpscLinkedArrayQueue<>(i);
        }

        public void a() {
            DisposableHelper.a(this.e);
        }

        public void onComplete() {
            this.c = true;
            this.f6848a.d();
        }

        public void onError(Throwable th) {
            this.d = th;
            this.c = true;
            this.f6848a.d();
        }

        public void onNext(T t) {
            this.b.offer(t);
            this.f6848a.d();
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.e, disposable);
        }
    }

    public ObservableZip(ObservableSource<? extends T>[] observableSourceArr, Iterable<? extends ObservableSource<? extends T>> iterable, Function<? super Object[], ? extends R> function, int i, boolean z) {
        this.f6847a = observableSourceArr;
        this.b = iterable;
        this.c = function;
        this.d = i;
        this.e = z;
    }

    public void subscribeActual(Observer<? super R> observer) {
        int i;
        ObservableSource<? extends T>[] observableSourceArr = this.f6847a;
        if (observableSourceArr == null) {
            observableSourceArr = new Observable[8];
            i = 0;
            for (ObservableSource<? extends T> observableSource : this.b) {
                if (i == observableSourceArr.length) {
                    ObservableSource<? extends T>[] observableSourceArr2 = new ObservableSource[((i >> 2) + i)];
                    System.arraycopy(observableSourceArr, 0, observableSourceArr2, 0, i);
                    observableSourceArr = observableSourceArr2;
                }
                observableSourceArr[i] = observableSource;
                i++;
            }
        } else {
            i = observableSourceArr.length;
        }
        if (i == 0) {
            EmptyDisposable.a((Observer<?>) observer);
        } else {
            new ZipCoordinator(observer, this.c, i, this.e).a(observableSourceArr, this.d);
        }
    }

    static final class ZipCoordinator<T, R> extends AtomicInteger implements Disposable {
        private static final long serialVersionUID = 2983708048395377667L;
        volatile boolean cancelled;
        final boolean delayError;
        final Observer<? super R> downstream;
        final ZipObserver<T, R>[] observers;
        final T[] row;
        final Function<? super Object[], ? extends R> zipper;

        ZipCoordinator(Observer<? super R> observer, Function<? super Object[], ? extends R> function, int i, boolean z) {
            this.downstream = observer;
            this.zipper = function;
            this.observers = new ZipObserver[i];
            this.row = new Object[i];
            this.delayError = z;
        }

        public void a(ObservableSource<? extends T>[] observableSourceArr, int i) {
            ZipObserver<T, R>[] zipObserverArr = this.observers;
            int length = zipObserverArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                zipObserverArr[i2] = new ZipObserver<>(this, i);
            }
            lazySet(0);
            this.downstream.onSubscribe(this);
            for (int i3 = 0; i3 < length && !this.cancelled; i3++) {
                observableSourceArr[i3].subscribe(zipObserverArr[i3]);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            for (ZipObserver<T, R> a2 : this.observers) {
                a2.a();
            }
        }

        /* access modifiers changed from: package-private */
        public void c() {
            for (ZipObserver<T, R> zipObserver : this.observers) {
                zipObserver.b.clear();
            }
        }

        public void d() {
            Throwable th;
            if (getAndIncrement() == 0) {
                ZipObserver<T, R>[] zipObserverArr = this.observers;
                Observer<? super R> observer = this.downstream;
                T[] tArr = this.row;
                boolean z = this.delayError;
                int i = 1;
                while (true) {
                    int i2 = 0;
                    int i3 = 0;
                    for (ZipObserver<T, R> zipObserver : zipObserverArr) {
                        if (tArr[i3] == null) {
                            boolean z2 = zipObserver.c;
                            T poll = zipObserver.b.poll();
                            boolean z3 = poll == null;
                            if (!a(z2, z3, observer, z, zipObserver)) {
                                if (!z3) {
                                    tArr[i3] = poll;
                                } else {
                                    i2++;
                                }
                            } else {
                                return;
                            }
                        } else if (zipObserver.c && !z && (th = zipObserver.d) != null) {
                            a();
                            observer.onError(th);
                            return;
                        }
                        i3++;
                    }
                    if (i2 != 0) {
                        i = addAndGet(-i);
                        if (i == 0) {
                            return;
                        }
                    } else {
                        try {
                            Object apply = this.zipper.apply(tArr.clone());
                            ObjectHelper.a(apply, "The zipper returned a null value");
                            observer.onNext(apply);
                            Arrays.fill(tArr, (Object) null);
                        } catch (Throwable th2) {
                            Exceptions.b(th2);
                            a();
                            observer.onError(th2);
                            return;
                        }
                    }
                }
            }
        }

        public void dispose() {
            if (!this.cancelled) {
                this.cancelled = true;
                b();
                if (getAndIncrement() == 0) {
                    c();
                }
            }
        }

        public boolean isDisposed() {
            return this.cancelled;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            c();
            b();
        }

        /* access modifiers changed from: package-private */
        public boolean a(boolean z, boolean z2, Observer<? super R> observer, boolean z3, ZipObserver<?, ?> zipObserver) {
            if (this.cancelled) {
                a();
                return true;
            } else if (!z) {
                return false;
            } else {
                if (!z3) {
                    Throwable th = zipObserver.d;
                    if (th != null) {
                        a();
                        observer.onError(th);
                        return true;
                    } else if (!z2) {
                        return false;
                    } else {
                        a();
                        observer.onComplete();
                        return true;
                    }
                } else if (!z2) {
                    return false;
                } else {
                    Throwable th2 = zipObserver.d;
                    a();
                    if (th2 != null) {
                        observer.onError(th2);
                    } else {
                        observer.onComplete();
                    }
                    return true;
                }
            }
        }
    }
}
