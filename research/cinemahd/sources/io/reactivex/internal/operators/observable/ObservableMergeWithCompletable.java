package io.reactivex.internal.operators.observable;

import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.HalfSerializer;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableMergeWithCompletable<T> extends AbstractObservableWithUpstream<T, T> {
    final CompletableSource b;

    public ObservableMergeWithCompletable(Observable<T> observable, CompletableSource completableSource) {
        super(observable);
        this.b = completableSource;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        MergeWithObserver mergeWithObserver = new MergeWithObserver(observer);
        observer.onSubscribe(mergeWithObserver);
        this.f6691a.subscribe(mergeWithObserver);
        this.b.a(mergeWithObserver.otherObserver);
    }

    static final class MergeWithObserver<T> extends AtomicInteger implements Observer<T>, Disposable {
        private static final long serialVersionUID = -4592979584110982903L;
        final Observer<? super T> downstream;
        final AtomicThrowable error = new AtomicThrowable();
        final AtomicReference<Disposable> mainDisposable = new AtomicReference<>();
        volatile boolean mainDone;
        volatile boolean otherDone;
        final OtherObserver otherObserver = new OtherObserver(this);

        static final class OtherObserver extends AtomicReference<Disposable> implements CompletableObserver {
            private static final long serialVersionUID = -2935427570954647017L;
            final MergeWithObserver<?> parent;

            OtherObserver(MergeWithObserver<?> mergeWithObserver) {
                this.parent = mergeWithObserver;
            }

            public void onComplete() {
                this.parent.a();
            }

            public void onError(Throwable th) {
                this.parent.a(th);
            }

            public void onSubscribe(Disposable disposable) {
                DisposableHelper.c(this, disposable);
            }
        }

        MergeWithObserver(Observer<? super T> observer) {
            this.downstream = observer;
        }

        /* access modifiers changed from: package-private */
        public void a(Throwable th) {
            DisposableHelper.a(this.mainDisposable);
            HalfSerializer.a((Observer<?>) this.downstream, th, (AtomicInteger) this, this.error);
        }

        public void dispose() {
            DisposableHelper.a(this.mainDisposable);
            DisposableHelper.a((AtomicReference<Disposable>) this.otherObserver);
        }

        public boolean isDisposed() {
            return DisposableHelper.a(this.mainDisposable.get());
        }

        public void onComplete() {
            this.mainDone = true;
            if (this.otherDone) {
                HalfSerializer.a((Observer<?>) this.downstream, (AtomicInteger) this, this.error);
            }
        }

        public void onError(Throwable th) {
            DisposableHelper.a(this.mainDisposable);
            HalfSerializer.a((Observer<?>) this.downstream, th, (AtomicInteger) this, this.error);
        }

        public void onNext(T t) {
            HalfSerializer.a(this.downstream, t, (AtomicInteger) this, this.error);
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.mainDisposable, disposable);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.otherDone = true;
            if (this.mainDone) {
                HalfSerializer.a((Observer<?>) this.downstream, (AtomicInteger) this, this.error);
            }
        }
    }
}
