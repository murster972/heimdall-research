package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.DeferredScalarDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class ObservableFromCallable<T> extends Observable<T> implements Callable<T> {

    /* renamed from: a  reason: collision with root package name */
    final Callable<? extends T> f6743a;

    public ObservableFromCallable(Callable<? extends T> callable) {
        this.f6743a = callable;
    }

    public T call() throws Exception {
        T call = this.f6743a.call();
        ObjectHelper.a(call, "The callable returned a null value");
        return call;
    }

    public void subscribeActual(Observer<? super T> observer) {
        DeferredScalarDisposable deferredScalarDisposable = new DeferredScalarDisposable(observer);
        observer.onSubscribe(deferredScalarDisposable);
        if (!deferredScalarDisposable.isDisposed()) {
            try {
                Object call = this.f6743a.call();
                ObjectHelper.a(call, "Callable returned null");
                deferredScalarDisposable.a(call);
            } catch (Throwable th) {
                Exceptions.b(th);
                if (!deferredScalarDisposable.isDisposed()) {
                    observer.onError(th);
                } else {
                    RxJavaPlugins.b(th);
                }
            }
        }
    }
}
