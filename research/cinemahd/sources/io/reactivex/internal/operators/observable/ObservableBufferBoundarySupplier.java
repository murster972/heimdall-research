package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.QueueDrainObserver;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableBufferBoundarySupplier<T, U extends Collection<? super T>, B> extends AbstractObservableWithUpstream<T, U> {
    final Callable<? extends ObservableSource<B>> b;
    final Callable<U> c;

    static final class BufferBoundaryObserver<T, U extends Collection<? super T>, B> extends DisposableObserver<B> {
        final BufferBoundarySupplierObserver<T, U, B> b;
        boolean c;

        BufferBoundaryObserver(BufferBoundarySupplierObserver<T, U, B> bufferBoundarySupplierObserver) {
            this.b = bufferBoundarySupplierObserver;
        }

        public void onComplete() {
            if (!this.c) {
                this.c = true;
                this.b.g();
            }
        }

        public void onError(Throwable th) {
            if (this.c) {
                RxJavaPlugins.b(th);
                return;
            }
            this.c = true;
            this.b.onError(th);
        }

        public void onNext(B b2) {
            if (!this.c) {
                this.c = true;
                dispose();
                this.b.g();
            }
        }
    }

    static final class BufferBoundarySupplierObserver<T, U extends Collection<? super T>, B> extends QueueDrainObserver<T, U, U> implements Observer<T>, Disposable {
        final Callable<U> g;
        final Callable<? extends ObservableSource<B>> h;
        Disposable i;
        final AtomicReference<Disposable> j = new AtomicReference<>();
        U k;

        BufferBoundarySupplierObserver(Observer<? super U> observer, Callable<U> callable, Callable<? extends ObservableSource<B>> callable2) {
            super(observer, new MpscLinkedQueue());
            this.g = callable;
            this.h = callable2;
        }

        public void dispose() {
            if (!this.d) {
                this.d = true;
                this.i.dispose();
                f();
                if (d()) {
                    this.c.clear();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
            DisposableHelper.a(this.j);
        }

        /* access modifiers changed from: package-private */
        public void g() {
            try {
                U call = this.g.call();
                ObjectHelper.a(call, "The buffer supplied is null");
                U u = (Collection) call;
                try {
                    Object call2 = this.h.call();
                    ObjectHelper.a(call2, "The boundary ObservableSource supplied is null");
                    ObservableSource observableSource = (ObservableSource) call2;
                    BufferBoundaryObserver bufferBoundaryObserver = new BufferBoundaryObserver(this);
                    if (DisposableHelper.a(this.j, (Disposable) bufferBoundaryObserver)) {
                        synchronized (this) {
                            U u2 = this.k;
                            if (u2 != null) {
                                this.k = u;
                                observableSource.subscribe(bufferBoundaryObserver);
                                a(u2, false, this);
                            }
                        }
                    }
                } catch (Throwable th) {
                    Exceptions.b(th);
                    this.d = true;
                    this.i.dispose();
                    this.b.onError(th);
                }
            } catch (Throwable th2) {
                Exceptions.b(th2);
                dispose();
                this.b.onError(th2);
            }
        }

        public boolean isDisposed() {
            return this.d;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
            io.reactivex.internal.util.QueueDrainHelper.a(r3.c, r3.b, false, r3, r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x000b, code lost:
            r3.c.offer(r0);
            r3.e = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
            if (d() == false) goto L_?;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onComplete() {
            /*
                r3 = this;
                monitor-enter(r3)
                U r0 = r3.k     // Catch:{ all -> 0x0022 }
                if (r0 != 0) goto L_0x0007
                monitor-exit(r3)     // Catch:{ all -> 0x0022 }
                return
            L_0x0007:
                r1 = 0
                r3.k = r1     // Catch:{ all -> 0x0022 }
                monitor-exit(r3)     // Catch:{ all -> 0x0022 }
                io.reactivex.internal.fuseable.SimplePlainQueue<U> r1 = r3.c
                r1.offer(r0)
                r0 = 1
                r3.e = r0
                boolean r0 = r3.d()
                if (r0 == 0) goto L_0x0021
                io.reactivex.internal.fuseable.SimplePlainQueue<U> r0 = r3.c
                io.reactivex.Observer<? super V> r1 = r3.b
                r2 = 0
                io.reactivex.internal.util.QueueDrainHelper.a(r0, r1, r2, r3, r3)
            L_0x0021:
                return
            L_0x0022:
                r0 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x0022 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: io.reactivex.internal.operators.observable.ObservableBufferBoundarySupplier.BufferBoundarySupplierObserver.onComplete():void");
        }

        public void onError(Throwable th) {
            dispose();
            this.b.onError(th);
        }

        public void onNext(T t) {
            synchronized (this) {
                U u = this.k;
                if (u != null) {
                    u.add(t);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.i, disposable)) {
                this.i = disposable;
                Observer<? super V> observer = this.b;
                try {
                    U call = this.g.call();
                    ObjectHelper.a(call, "The buffer supplied is null");
                    this.k = (Collection) call;
                    try {
                        Object call2 = this.h.call();
                        ObjectHelper.a(call2, "The boundary ObservableSource supplied is null");
                        ObservableSource observableSource = (ObservableSource) call2;
                        BufferBoundaryObserver bufferBoundaryObserver = new BufferBoundaryObserver(this);
                        this.j.set(bufferBoundaryObserver);
                        observer.onSubscribe(this);
                        if (!this.d) {
                            observableSource.subscribe(bufferBoundaryObserver);
                        }
                    } catch (Throwable th) {
                        Exceptions.b(th);
                        this.d = true;
                        disposable.dispose();
                        EmptyDisposable.a(th, (Observer<?>) observer);
                    }
                } catch (Throwable th2) {
                    Exceptions.b(th2);
                    this.d = true;
                    disposable.dispose();
                    EmptyDisposable.a(th2, (Observer<?>) observer);
                }
            }
        }

        public void a(Observer<? super U> observer, U u) {
            this.b.onNext(u);
        }
    }

    public ObservableBufferBoundarySupplier(ObservableSource<T> observableSource, Callable<? extends ObservableSource<B>> callable, Callable<U> callable2) {
        super(observableSource);
        this.b = callable;
        this.c = callable2;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super U> observer) {
        this.f6691a.subscribe(new BufferBoundarySupplierObserver(new SerializedObserver(observer), this.c, this.b));
    }
}
