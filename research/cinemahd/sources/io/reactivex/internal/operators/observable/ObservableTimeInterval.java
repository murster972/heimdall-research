package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.schedulers.Timed;
import java.util.concurrent.TimeUnit;

public final class ObservableTimeInterval<T> extends AbstractObservableWithUpstream<T, Timed<T>> {
    final Scheduler b;
    final TimeUnit c;

    static final class TimeIntervalObserver<T> implements Observer<T>, Disposable {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super Timed<T>> f6830a;
        final TimeUnit b;
        final Scheduler c;
        long d;
        Disposable e;

        TimeIntervalObserver(Observer<? super Timed<T>> observer, TimeUnit timeUnit, Scheduler scheduler) {
            this.f6830a = observer;
            this.c = scheduler;
            this.b = timeUnit;
        }

        public void dispose() {
            this.e.dispose();
        }

        public boolean isDisposed() {
            return this.e.isDisposed();
        }

        public void onComplete() {
            this.f6830a.onComplete();
        }

        public void onError(Throwable th) {
            this.f6830a.onError(th);
        }

        public void onNext(T t) {
            long a2 = this.c.a(this.b);
            long j = this.d;
            this.d = a2;
            this.f6830a.onNext(new Timed(t, a2 - j, this.b));
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.a(this.e, disposable)) {
                this.e = disposable;
                this.d = this.c.a(this.b);
                this.f6830a.onSubscribe(this);
            }
        }
    }

    public ObservableTimeInterval(ObservableSource<T> observableSource, TimeUnit timeUnit, Scheduler scheduler) {
        super(observableSource);
        this.b = scheduler;
        this.c = timeUnit;
    }

    public void subscribeActual(Observer<? super Timed<T>> observer) {
        this.f6691a.subscribe(new TimeIntervalObserver(observer, this.c, this.b));
    }
}
