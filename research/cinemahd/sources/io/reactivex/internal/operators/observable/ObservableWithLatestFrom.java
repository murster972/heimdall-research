package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.observers.SerializedObserver;
import java.util.concurrent.atomic.AtomicReference;

public final class ObservableWithLatestFrom<T, U, R> extends AbstractObservableWithUpstream<T, R> {
    final BiFunction<? super T, ? super U, ? extends R> b;
    final ObservableSource<? extends U> c;

    static final class WithLatestFromObserver<T, U, R> extends AtomicReference<U> implements Observer<T>, Disposable {
        private static final long serialVersionUID = -312246233408980075L;
        final BiFunction<? super T, ? super U, ? extends R> combiner;
        final Observer<? super R> downstream;
        final AtomicReference<Disposable> other = new AtomicReference<>();
        final AtomicReference<Disposable> upstream = new AtomicReference<>();

        WithLatestFromObserver(Observer<? super R> observer, BiFunction<? super T, ? super U, ? extends R> biFunction) {
            this.downstream = observer;
            this.combiner = biFunction;
        }

        public boolean a(Disposable disposable) {
            return DisposableHelper.c(this.other, disposable);
        }

        public void dispose() {
            DisposableHelper.a(this.upstream);
            DisposableHelper.a(this.other);
        }

        public boolean isDisposed() {
            return DisposableHelper.a(this.upstream.get());
        }

        public void onComplete() {
            DisposableHelper.a(this.other);
            this.downstream.onComplete();
        }

        public void onError(Throwable th) {
            DisposableHelper.a(this.other);
            this.downstream.onError(th);
        }

        public void onNext(T t) {
            Object obj = get();
            if (obj != null) {
                try {
                    Object a2 = this.combiner.a(t, obj);
                    ObjectHelper.a(a2, "The combiner returned a null value");
                    this.downstream.onNext(a2);
                } catch (Throwable th) {
                    Exceptions.b(th);
                    dispose();
                    this.downstream.onError(th);
                }
            }
        }

        public void onSubscribe(Disposable disposable) {
            DisposableHelper.c(this.upstream, disposable);
        }

        public void a(Throwable th) {
            DisposableHelper.a(this.upstream);
            this.downstream.onError(th);
        }
    }

    final class WithLatestFromOtherObserver implements Observer<U> {

        /* renamed from: a  reason: collision with root package name */
        private final WithLatestFromObserver<T, U, R> f6845a;

        WithLatestFromOtherObserver(ObservableWithLatestFrom observableWithLatestFrom, WithLatestFromObserver<T, U, R> withLatestFromObserver) {
            this.f6845a = withLatestFromObserver;
        }

        public void onComplete() {
        }

        public void onError(Throwable th) {
            this.f6845a.a(th);
        }

        public void onNext(U u) {
            this.f6845a.lazySet(u);
        }

        public void onSubscribe(Disposable disposable) {
            this.f6845a.a(disposable);
        }
    }

    public ObservableWithLatestFrom(ObservableSource<T> observableSource, BiFunction<? super T, ? super U, ? extends R> biFunction, ObservableSource<? extends U> observableSource2) {
        super(observableSource);
        this.b = biFunction;
        this.c = observableSource2;
    }

    public void subscribeActual(Observer<? super R> observer) {
        SerializedObserver serializedObserver = new SerializedObserver(observer);
        WithLatestFromObserver withLatestFromObserver = new WithLatestFromObserver(serializedObserver, this.b);
        serializedObserver.onSubscribe(withLatestFromObserver);
        this.c.subscribe(new WithLatestFromOtherObserver(this, withLatestFromObserver));
        this.f6691a.subscribe(withLatestFromObserver);
    }
}
