package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.observers.BasicFuseableObserver;

public final class ObservableMap<T, U> extends AbstractObservableWithUpstream<T, U> {
    final Function<? super T, ? extends U> b;

    static final class MapObserver<T, U> extends BasicFuseableObserver<T, U> {
        final Function<? super T, ? extends U> f;

        MapObserver(Observer<? super U> observer, Function<? super T, ? extends U> function) {
            super(observer);
            this.f = function;
        }

        public int a(int i) {
            return b(i);
        }

        public void onNext(T t) {
            if (!this.d) {
                if (this.e != 0) {
                    this.f6673a.onNext(null);
                    return;
                }
                try {
                    Object apply = this.f.apply(t);
                    ObjectHelper.a(apply, "The mapper function returned a null value.");
                    this.f6673a.onNext(apply);
                } catch (Throwable th) {
                    a(th);
                }
            }
        }

        public U poll() throws Exception {
            T poll = this.c.poll();
            if (poll == null) {
                return null;
            }
            U apply = this.f.apply(poll);
            ObjectHelper.a(apply, "The mapper function returned a null value.");
            return apply;
        }
    }

    public ObservableMap(ObservableSource<T> observableSource, Function<? super T, ? extends U> function) {
        super(observableSource);
        this.b = function;
    }

    public void subscribeActual(Observer<? super U> observer) {
        this.f6691a.subscribe(new MapObserver(observer, this.b));
    }
}
