package io.reactivex.internal.operators.observable;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.SequentialDisposable;

public final class ObservableSwitchIfEmpty<T> extends AbstractObservableWithUpstream<T, T> {
    final ObservableSource<? extends T> b;

    static final class SwitchIfEmptyObserver<T> implements Observer<T> {

        /* renamed from: a  reason: collision with root package name */
        final Observer<? super T> f6824a;
        final ObservableSource<? extends T> b;
        final SequentialDisposable c = new SequentialDisposable();
        boolean d = true;

        SwitchIfEmptyObserver(Observer<? super T> observer, ObservableSource<? extends T> observableSource) {
            this.f6824a = observer;
            this.b = observableSource;
        }

        public void onComplete() {
            if (this.d) {
                this.d = false;
                this.b.subscribe(this);
                return;
            }
            this.f6824a.onComplete();
        }

        public void onError(Throwable th) {
            this.f6824a.onError(th);
        }

        public void onNext(T t) {
            if (this.d) {
                this.d = false;
            }
            this.f6824a.onNext(t);
        }

        public void onSubscribe(Disposable disposable) {
            this.c.b(disposable);
        }
    }

    public ObservableSwitchIfEmpty(ObservableSource<T> observableSource, ObservableSource<? extends T> observableSource2) {
        super(observableSource);
        this.b = observableSource2;
    }

    public void subscribeActual(Observer<? super T> observer) {
        SwitchIfEmptyObserver switchIfEmptyObserver = new SwitchIfEmptyObserver(observer, this.b);
        observer.onSubscribe(switchIfEmptyObserver.c);
        this.f6691a.subscribe(switchIfEmptyObserver);
    }
}
