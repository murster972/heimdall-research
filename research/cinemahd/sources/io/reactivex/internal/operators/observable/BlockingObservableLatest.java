package io.reactivex.internal.operators.observable;

import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.internal.util.BlockingHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;

public final class BlockingObservableLatest<T> implements Iterable<T> {

    /* renamed from: a  reason: collision with root package name */
    final ObservableSource<T> f6693a;

    static final class BlockingObservableLatestIterator<T> extends DisposableObserver<Notification<T>> implements Iterator<T> {
        Notification<T> b;
        final Semaphore c = new Semaphore(0);
        final AtomicReference<Notification<T>> d = new AtomicReference<>();

        BlockingObservableLatestIterator() {
        }

        /* renamed from: a */
        public void onNext(Notification<T> notification) {
            if (this.d.getAndSet(notification) == null) {
                this.c.release();
            }
        }

        public boolean hasNext() {
            Notification<T> notification = this.b;
            if (notification == null || !notification.d()) {
                if (this.b == null) {
                    try {
                        BlockingHelper.a();
                        this.c.acquire();
                        Notification<T> andSet = this.d.getAndSet((Object) null);
                        this.b = andSet;
                        if (andSet.d()) {
                            throw ExceptionHelper.a(andSet.a());
                        }
                    } catch (InterruptedException e) {
                        dispose();
                        this.b = Notification.a((Throwable) e);
                        throw ExceptionHelper.a((Throwable) e);
                    }
                }
                return this.b.e();
            }
            throw ExceptionHelper.a(this.b.a());
        }

        public T next() {
            if (hasNext()) {
                T b2 = this.b.b();
                this.b = null;
                return b2;
            }
            throw new NoSuchElementException();
        }

        public void onComplete() {
        }

        public void onError(Throwable th) {
            RxJavaPlugins.b(th);
        }

        public void remove() {
            throw new UnsupportedOperationException("Read-only iterator.");
        }
    }

    public BlockingObservableLatest(ObservableSource<T> observableSource) {
        this.f6693a = observableSource;
    }

    public Iterator<T> iterator() {
        BlockingObservableLatestIterator blockingObservableLatestIterator = new BlockingObservableLatestIterator();
        Observable.wrap(this.f6693a).materialize().subscribe(blockingObservableLatestIterator);
        return blockingObservableLatestIterator;
    }
}
