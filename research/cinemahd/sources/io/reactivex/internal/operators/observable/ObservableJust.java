package io.reactivex.internal.operators.observable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.internal.fuseable.ScalarCallable;
import io.reactivex.internal.operators.observable.ObservableScalarXMap;

public final class ObservableJust<T> extends Observable<T> implements ScalarCallable<T> {

    /* renamed from: a  reason: collision with root package name */
    private final T f6777a;

    public ObservableJust(T t) {
        this.f6777a = t;
    }

    public T call() {
        return this.f6777a;
    }

    /* access modifiers changed from: protected */
    public void subscribeActual(Observer<? super T> observer) {
        ObservableScalarXMap.ScalarDisposable scalarDisposable = new ObservableScalarXMap.ScalarDisposable(observer, this.f6777a);
        observer.onSubscribe(scalarDisposable);
        scalarDisposable.run();
    }
}
