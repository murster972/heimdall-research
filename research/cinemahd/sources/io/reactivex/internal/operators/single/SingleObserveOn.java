package io.reactivex.internal.operators.single;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class SingleObserveOn<T> extends Single<T> {

    /* renamed from: a  reason: collision with root package name */
    final SingleSource<T> f6853a;
    final Scheduler b;

    static final class ObserveOnSingleObserver<T> extends AtomicReference<Disposable> implements SingleObserver<T>, Disposable, Runnable {
        private static final long serialVersionUID = 3528003840217436037L;
        final SingleObserver<? super T> downstream;
        Throwable error;
        final Scheduler scheduler;
        T value;

        ObserveOnSingleObserver(SingleObserver<? super T> singleObserver, Scheduler scheduler2) {
            this.downstream = singleObserver;
            this.scheduler = scheduler2;
        }

        public void dispose() {
            DisposableHelper.a((AtomicReference<Disposable>) this);
        }

        public boolean isDisposed() {
            return DisposableHelper.a((Disposable) get());
        }

        public void onError(Throwable th) {
            this.error = th;
            DisposableHelper.a((AtomicReference<Disposable>) this, this.scheduler.a((Runnable) this));
        }

        public void onSubscribe(Disposable disposable) {
            if (DisposableHelper.c(this, disposable)) {
                this.downstream.onSubscribe(this);
            }
        }

        public void onSuccess(T t) {
            this.value = t;
            DisposableHelper.a((AtomicReference<Disposable>) this, this.scheduler.a((Runnable) this));
        }

        public void run() {
            Throwable th = this.error;
            if (th != null) {
                this.downstream.onError(th);
            } else {
                this.downstream.onSuccess(this.value);
            }
        }
    }

    public SingleObserveOn(SingleSource<T> singleSource, Scheduler scheduler) {
        this.f6853a = singleSource;
        this.b = scheduler;
    }

    /* access modifiers changed from: protected */
    public void b(SingleObserver<? super T> singleObserver) {
        this.f6853a.a(new ObserveOnSingleObserver(singleObserver, this.b));
    }
}
