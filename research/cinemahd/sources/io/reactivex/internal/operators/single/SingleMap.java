package io.reactivex.internal.operators.single;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;

public final class SingleMap<T, R> extends Single<R> {

    /* renamed from: a  reason: collision with root package name */
    final SingleSource<? extends T> f6851a;
    final Function<? super T, ? extends R> b;

    static final class MapSingleObserver<T, R> implements SingleObserver<T> {

        /* renamed from: a  reason: collision with root package name */
        final SingleObserver<? super R> f6852a;
        final Function<? super T, ? extends R> b;

        MapSingleObserver(SingleObserver<? super R> singleObserver, Function<? super T, ? extends R> function) {
            this.f6852a = singleObserver;
            this.b = function;
        }

        public void onError(Throwable th) {
            this.f6852a.onError(th);
        }

        public void onSubscribe(Disposable disposable) {
            this.f6852a.onSubscribe(disposable);
        }

        public void onSuccess(T t) {
            try {
                Object apply = this.b.apply(t);
                ObjectHelper.a(apply, "The mapper function returned a null value.");
                this.f6852a.onSuccess(apply);
            } catch (Throwable th) {
                Exceptions.b(th);
                onError(th);
            }
        }
    }

    public SingleMap(SingleSource<? extends T> singleSource, Function<? super T, ? extends R> function) {
        this.f6851a = singleSource;
        this.b = function;
    }

    /* access modifiers changed from: protected */
    public void b(SingleObserver<? super R> singleObserver) {
        this.f6851a.a(new MapSingleObserver(singleObserver, this.b));
    }
}
