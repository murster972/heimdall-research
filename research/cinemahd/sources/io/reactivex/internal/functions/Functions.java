package io.reactivex.internal.functions;

import com.facebook.common.time.Clock;
import io.reactivex.Notification;
import io.reactivex.Scheduler;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.functions.Function4;
import io.reactivex.functions.Function5;
import io.reactivex.functions.Function6;
import io.reactivex.functions.Function7;
import io.reactivex.functions.Function8;
import io.reactivex.functions.Function9;
import io.reactivex.functions.LongConsumer;
import io.reactivex.functions.Predicate;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Timed;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import org.reactivestreams.Subscription;

public final class Functions {

    /* renamed from: a  reason: collision with root package name */
    static final Function<Object, Object> f6646a = new Identity();
    public static final Runnable b = new EmptyRunnable();
    public static final Action c = new EmptyAction();
    static final Consumer<Object> d = new EmptyConsumer();
    public static final Consumer<Throwable> e = new OnErrorMissingConsumer();
    static final Predicate<Object> f = new TruePredicate();
    static final Predicate<Object> g = new FalsePredicate();
    static final Callable<Object> h = new NullCallable();
    static final Comparator<Object> i = new NaturalObjectComparator();

    static final class ActionConsumer<T> implements Consumer<T> {

        /* renamed from: a  reason: collision with root package name */
        final Action f6647a;

        ActionConsumer(Action action) {
            this.f6647a = action;
        }

        public void accept(T t) throws Exception {
            this.f6647a.run();
        }
    }

    static final class Array2Func<T1, T2, R> implements Function<Object[], R> {

        /* renamed from: a  reason: collision with root package name */
        final BiFunction<? super T1, ? super T2, ? extends R> f6648a;

        Array2Func(BiFunction<? super T1, ? super T2, ? extends R> biFunction) {
            this.f6648a = biFunction;
        }

        /* renamed from: a */
        public R apply(Object[] objArr) throws Exception {
            if (objArr.length == 2) {
                return this.f6648a.a(objArr[0], objArr[1]);
            }
            throw new IllegalArgumentException("Array of size 2 expected but got " + objArr.length);
        }
    }

    static final class Array3Func<T1, T2, T3, R> implements Function<Object[], R> {

        /* renamed from: a  reason: collision with root package name */
        final Function3<T1, T2, T3, R> f6649a;

        Array3Func(Function3<T1, T2, T3, R> function3) {
            this.f6649a = function3;
        }

        /* renamed from: a */
        public R apply(Object[] objArr) throws Exception {
            if (objArr.length == 3) {
                return this.f6649a.a(objArr[0], objArr[1], objArr[2]);
            }
            throw new IllegalArgumentException("Array of size 3 expected but got " + objArr.length);
        }
    }

    static final class Array4Func<T1, T2, T3, T4, R> implements Function<Object[], R> {

        /* renamed from: a  reason: collision with root package name */
        final Function4<T1, T2, T3, T4, R> f6650a;

        Array4Func(Function4<T1, T2, T3, T4, R> function4) {
            this.f6650a = function4;
        }

        /* renamed from: a */
        public R apply(Object[] objArr) throws Exception {
            if (objArr.length == 4) {
                return this.f6650a.a(objArr[0], objArr[1], objArr[2], objArr[3]);
            }
            throw new IllegalArgumentException("Array of size 4 expected but got " + objArr.length);
        }
    }

    static final class Array5Func<T1, T2, T3, T4, T5, R> implements Function<Object[], R> {

        /* renamed from: a  reason: collision with root package name */
        private final Function5<T1, T2, T3, T4, T5, R> f6651a;

        Array5Func(Function5<T1, T2, T3, T4, T5, R> function5) {
            this.f6651a = function5;
        }

        /* renamed from: a */
        public R apply(Object[] objArr) throws Exception {
            if (objArr.length == 5) {
                return this.f6651a.a(objArr[0], objArr[1], objArr[2], objArr[3], objArr[4]);
            }
            throw new IllegalArgumentException("Array of size 5 expected but got " + objArr.length);
        }
    }

    static final class Array6Func<T1, T2, T3, T4, T5, T6, R> implements Function<Object[], R> {

        /* renamed from: a  reason: collision with root package name */
        final Function6<T1, T2, T3, T4, T5, T6, R> f6652a;

        Array6Func(Function6<T1, T2, T3, T4, T5, T6, R> function6) {
            this.f6652a = function6;
        }

        /* renamed from: a */
        public R apply(Object[] objArr) throws Exception {
            if (objArr.length == 6) {
                return this.f6652a.a(objArr[0], objArr[1], objArr[2], objArr[3], objArr[4], objArr[5]);
            }
            throw new IllegalArgumentException("Array of size 6 expected but got " + objArr.length);
        }
    }

    static final class Array7Func<T1, T2, T3, T4, T5, T6, T7, R> implements Function<Object[], R> {

        /* renamed from: a  reason: collision with root package name */
        final Function7<T1, T2, T3, T4, T5, T6, T7, R> f6653a;

        Array7Func(Function7<T1, T2, T3, T4, T5, T6, T7, R> function7) {
            this.f6653a = function7;
        }

        /* renamed from: a */
        public R apply(Object[] objArr) throws Exception {
            if (objArr.length == 7) {
                return this.f6653a.a(objArr[0], objArr[1], objArr[2], objArr[3], objArr[4], objArr[5], objArr[6]);
            }
            throw new IllegalArgumentException("Array of size 7 expected but got " + objArr.length);
        }
    }

    static final class Array8Func<T1, T2, T3, T4, T5, T6, T7, T8, R> implements Function<Object[], R> {

        /* renamed from: a  reason: collision with root package name */
        final Function8<T1, T2, T3, T4, T5, T6, T7, T8, R> f6654a;

        Array8Func(Function8<T1, T2, T3, T4, T5, T6, T7, T8, R> function8) {
            this.f6654a = function8;
        }

        /* renamed from: a */
        public R apply(Object[] objArr) throws Exception {
            if (objArr.length == 8) {
                return this.f6654a.a(objArr[0], objArr[1], objArr[2], objArr[3], objArr[4], objArr[5], objArr[6], objArr[7]);
            }
            throw new IllegalArgumentException("Array of size 8 expected but got " + objArr.length);
        }
    }

    static final class Array9Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, R> implements Function<Object[], R> {

        /* renamed from: a  reason: collision with root package name */
        final Function9<T1, T2, T3, T4, T5, T6, T7, T8, T9, R> f6655a;

        Array9Func(Function9<T1, T2, T3, T4, T5, T6, T7, T8, T9, R> function9) {
            this.f6655a = function9;
        }

        /* renamed from: a */
        public R apply(Object[] objArr) throws Exception {
            if (objArr.length == 9) {
                return this.f6655a.a(objArr[0], objArr[1], objArr[2], objArr[3], objArr[4], objArr[5], objArr[6], objArr[7], objArr[8]);
            }
            throw new IllegalArgumentException("Array of size 9 expected but got " + objArr.length);
        }
    }

    static final class ArrayListCapacityCallable<T> implements Callable<List<T>> {

        /* renamed from: a  reason: collision with root package name */
        final int f6656a;

        ArrayListCapacityCallable(int i) {
            this.f6656a = i;
        }

        public List<T> call() throws Exception {
            return new ArrayList(this.f6656a);
        }
    }

    static final class BooleanSupplierPredicateReverse<T> implements Predicate<T> {

        /* renamed from: a  reason: collision with root package name */
        final BooleanSupplier f6657a;

        BooleanSupplierPredicateReverse(BooleanSupplier booleanSupplier) {
            this.f6657a = booleanSupplier;
        }

        public boolean a(T t) throws Exception {
            return !this.f6657a.a();
        }
    }

    static final class CastToClass<T, U> implements Function<T, U> {

        /* renamed from: a  reason: collision with root package name */
        final Class<U> f6658a;

        CastToClass(Class<U> cls) {
            this.f6658a = cls;
        }

        public U apply(T t) throws Exception {
            return this.f6658a.cast(t);
        }
    }

    static final class ClassFilter<T, U> implements Predicate<T> {

        /* renamed from: a  reason: collision with root package name */
        final Class<U> f6659a;

        ClassFilter(Class<U> cls) {
            this.f6659a = cls;
        }

        public boolean a(T t) throws Exception {
            return this.f6659a.isInstance(t);
        }
    }

    static final class EmptyAction implements Action {
        EmptyAction() {
        }

        public void run() {
        }

        public String toString() {
            return "EmptyAction";
        }
    }

    static final class EmptyConsumer implements Consumer<Object> {
        EmptyConsumer() {
        }

        public void accept(Object obj) {
        }

        public String toString() {
            return "EmptyConsumer";
        }
    }

    static final class EmptyLongConsumer implements LongConsumer {
        EmptyLongConsumer() {
        }
    }

    static final class EmptyRunnable implements Runnable {
        EmptyRunnable() {
        }

        public void run() {
        }

        public String toString() {
            return "EmptyRunnable";
        }
    }

    static final class EqualsPredicate<T> implements Predicate<T> {

        /* renamed from: a  reason: collision with root package name */
        final T f6660a;

        EqualsPredicate(T t) {
            this.f6660a = t;
        }

        public boolean a(T t) throws Exception {
            return ObjectHelper.a((Object) t, (Object) this.f6660a);
        }
    }

    static final class ErrorConsumer implements Consumer<Throwable> {
        ErrorConsumer() {
        }

        /* renamed from: a */
        public void accept(Throwable th) {
            RxJavaPlugins.b(th);
        }
    }

    static final class FalsePredicate implements Predicate<Object> {
        FalsePredicate() {
        }

        public boolean a(Object obj) {
            return false;
        }
    }

    enum HashSetCallable implements Callable<Set<Object>> {
        INSTANCE;

        public Set<Object> call() throws Exception {
            return new HashSet();
        }
    }

    static final class Identity implements Function<Object, Object> {
        Identity() {
        }

        public Object apply(Object obj) {
            return obj;
        }

        public String toString() {
            return "IdentityFunction";
        }
    }

    static final class JustValue<T, U> implements Callable<U>, Function<T, U> {

        /* renamed from: a  reason: collision with root package name */
        final U f6662a;

        JustValue(U u) {
            this.f6662a = u;
        }

        public U apply(T t) throws Exception {
            return this.f6662a;
        }

        public U call() throws Exception {
            return this.f6662a;
        }
    }

    static final class ListSorter<T> implements Function<List<T>, List<T>> {

        /* renamed from: a  reason: collision with root package name */
        final Comparator<? super T> f6663a;

        ListSorter(Comparator<? super T> comparator) {
            this.f6663a = comparator;
        }

        public List<T> a(List<T> list) {
            Collections.sort(list, this.f6663a);
            return list;
        }

        public /* bridge */ /* synthetic */ Object apply(Object obj) throws Exception {
            List list = (List) obj;
            a(list);
            return list;
        }
    }

    static final class MaxRequestSubscription implements Consumer<Subscription> {
        MaxRequestSubscription() {
        }

        /* renamed from: a */
        public void accept(Subscription subscription) throws Exception {
            subscription.request(Clock.MAX_TIME);
        }
    }

    enum NaturalComparator implements Comparator<Object> {
        INSTANCE;

        public int compare(Object obj, Object obj2) {
            return ((Comparable) obj).compareTo(obj2);
        }
    }

    static final class NaturalObjectComparator implements Comparator<Object> {
        NaturalObjectComparator() {
        }

        public int compare(Object obj, Object obj2) {
            return ((Comparable) obj).compareTo(obj2);
        }
    }

    static final class NotificationOnComplete<T> implements Action {

        /* renamed from: a  reason: collision with root package name */
        final Consumer<? super Notification<T>> f6665a;

        NotificationOnComplete(Consumer<? super Notification<T>> consumer) {
            this.f6665a = consumer;
        }

        public void run() throws Exception {
            this.f6665a.accept(Notification.f());
        }
    }

    static final class NotificationOnError<T> implements Consumer<Throwable> {

        /* renamed from: a  reason: collision with root package name */
        final Consumer<? super Notification<T>> f6666a;

        NotificationOnError(Consumer<? super Notification<T>> consumer) {
            this.f6666a = consumer;
        }

        /* renamed from: a */
        public void accept(Throwable th) throws Exception {
            this.f6666a.accept(Notification.a(th));
        }
    }

    static final class NotificationOnNext<T> implements Consumer<T> {

        /* renamed from: a  reason: collision with root package name */
        final Consumer<? super Notification<T>> f6667a;

        NotificationOnNext(Consumer<? super Notification<T>> consumer) {
            this.f6667a = consumer;
        }

        public void accept(T t) throws Exception {
            this.f6667a.accept(Notification.a(t));
        }
    }

    static final class NullCallable implements Callable<Object> {
        NullCallable() {
        }

        public Object call() {
            return null;
        }
    }

    static final class OnErrorMissingConsumer implements Consumer<Throwable> {
        OnErrorMissingConsumer() {
        }

        /* renamed from: a */
        public void accept(Throwable th) {
            RxJavaPlugins.b((Throwable) new OnErrorNotImplementedException(th));
        }
    }

    static final class TimestampFunction<T> implements Function<T, Timed<T>> {

        /* renamed from: a  reason: collision with root package name */
        final TimeUnit f6668a;
        final Scheduler b;

        TimestampFunction(TimeUnit timeUnit, Scheduler scheduler) {
            this.f6668a = timeUnit;
            this.b = scheduler;
        }

        public Timed<T> apply(T t) throws Exception {
            return new Timed<>(t, this.b.a(this.f6668a), this.f6668a);
        }
    }

    static final class ToMapKeySelector<K, T> implements BiConsumer<Map<K, T>, T> {

        /* renamed from: a  reason: collision with root package name */
        private final Function<? super T, ? extends K> f6669a;

        ToMapKeySelector(Function<? super T, ? extends K> function) {
            this.f6669a = function;
        }

        public void a(Map<K, T> map, T t) throws Exception {
            map.put(this.f6669a.apply(t), t);
        }
    }

    static final class ToMapKeyValueSelector<K, V, T> implements BiConsumer<Map<K, V>, T> {

        /* renamed from: a  reason: collision with root package name */
        private final Function<? super T, ? extends V> f6670a;
        private final Function<? super T, ? extends K> b;

        ToMapKeyValueSelector(Function<? super T, ? extends V> function, Function<? super T, ? extends K> function2) {
            this.f6670a = function;
            this.b = function2;
        }

        public void a(Map<K, V> map, T t) throws Exception {
            map.put(this.b.apply(t), this.f6670a.apply(t));
        }
    }

    static final class ToMultimapKeyValueSelector<K, V, T> implements BiConsumer<Map<K, Collection<V>>, T> {

        /* renamed from: a  reason: collision with root package name */
        private final Function<? super K, ? extends Collection<? super V>> f6671a;
        private final Function<? super T, ? extends V> b;
        private final Function<? super T, ? extends K> c;

        ToMultimapKeyValueSelector(Function<? super K, ? extends Collection<? super V>> function, Function<? super T, ? extends V> function2, Function<? super T, ? extends K> function3) {
            this.f6671a = function;
            this.b = function2;
            this.c = function3;
        }

        public void a(Map<K, Collection<V>> map, T t) throws Exception {
            Object apply = this.c.apply(t);
            Collection collection = map.get(apply);
            if (collection == null) {
                collection = (Collection) this.f6671a.apply(apply);
                map.put(apply, collection);
            }
            collection.add(this.b.apply(t));
        }
    }

    static final class TruePredicate implements Predicate<Object> {
        TruePredicate() {
        }

        public boolean a(Object obj) {
            return true;
        }
    }

    static {
        new ErrorConsumer();
        new EmptyLongConsumer();
        new MaxRequestSubscription();
    }

    private Functions() {
        throw new IllegalStateException("No instances!");
    }

    public static <T1, T2, R> Function<Object[], R> a(BiFunction<? super T1, ? super T2, ? extends R> biFunction) {
        ObjectHelper.a(biFunction, "f is null");
        return new Array2Func(biFunction);
    }

    public static <T> Predicate<T> b() {
        return f;
    }

    public static <T, U> Function<T, U> c(U u) {
        return new JustValue(u);
    }

    public static <T> Consumer<T> d() {
        return d;
    }

    public static <T> Function<T, T> e() {
        return f6646a;
    }

    public static <T> Comparator<T> f() {
        return NaturalComparator.INSTANCE;
    }

    public static <T> Comparator<T> g() {
        return i;
    }

    public static <T> Callable<T> h() {
        return h;
    }

    public static <T> Callable<T> b(T t) {
        return new JustValue(t);
    }

    public static <T> Callable<Set<T>> c() {
        return HashSetCallable.INSTANCE;
    }

    public static <T1, T2, T3, R> Function<Object[], R> a(Function3<T1, T2, T3, R> function3) {
        ObjectHelper.a(function3, "f is null");
        return new Array3Func(function3);
    }

    public static <T> Consumer<Throwable> b(Consumer<? super Notification<T>> consumer) {
        return new NotificationOnError(consumer);
    }

    public static <T> Consumer<T> c(Consumer<? super Notification<T>> consumer) {
        return new NotificationOnNext(consumer);
    }

    public static <T, U> Predicate<T> b(Class<U> cls) {
        return new ClassFilter(cls);
    }

    public static <T1, T2, T3, T4, R> Function<Object[], R> a(Function4<T1, T2, T3, T4, R> function4) {
        ObjectHelper.a(function4, "f is null");
        return new Array4Func(function4);
    }

    public static <T1, T2, T3, T4, T5, R> Function<Object[], R> a(Function5<T1, T2, T3, T4, T5, R> function5) {
        ObjectHelper.a(function5, "f is null");
        return new Array5Func(function5);
    }

    public static <T1, T2, T3, T4, T5, T6, R> Function<Object[], R> a(Function6<T1, T2, T3, T4, T5, T6, R> function6) {
        ObjectHelper.a(function6, "f is null");
        return new Array6Func(function6);
    }

    public static <T1, T2, T3, T4, T5, T6, T7, R> Function<Object[], R> a(Function7<T1, T2, T3, T4, T5, T6, T7, R> function7) {
        ObjectHelper.a(function7, "f is null");
        return new Array7Func(function7);
    }

    public static <T1, T2, T3, T4, T5, T6, T7, T8, R> Function<Object[], R> a(Function8<T1, T2, T3, T4, T5, T6, T7, T8, R> function8) {
        ObjectHelper.a(function8, "f is null");
        return new Array8Func(function8);
    }

    public static <T1, T2, T3, T4, T5, T6, T7, T8, T9, R> Function<Object[], R> a(Function9<T1, T2, T3, T4, T5, T6, T7, T8, T9, R> function9) {
        ObjectHelper.a(function9, "f is null");
        return new Array9Func(function9);
    }

    public static <T> Predicate<T> a() {
        return g;
    }

    public static <T, U> Function<T, U> a(Class<U> cls) {
        return new CastToClass(cls);
    }

    public static <T> Callable<List<T>> a(int i2) {
        return new ArrayListCapacityCallable(i2);
    }

    public static <T> Predicate<T> a(T t) {
        return new EqualsPredicate(t);
    }

    public static <T> Action a(Consumer<? super Notification<T>> consumer) {
        return new NotificationOnComplete(consumer);
    }

    public static <T> Consumer<T> a(Action action) {
        return new ActionConsumer(action);
    }

    public static <T> Predicate<T> a(BooleanSupplier booleanSupplier) {
        return new BooleanSupplierPredicateReverse(booleanSupplier);
    }

    public static <T> Function<T, Timed<T>> a(TimeUnit timeUnit, Scheduler scheduler) {
        return new TimestampFunction(timeUnit, scheduler);
    }

    public static <T, K> BiConsumer<Map<K, T>, T> a(Function<? super T, ? extends K> function) {
        return new ToMapKeySelector(function);
    }

    public static <T, K, V> BiConsumer<Map<K, V>, T> a(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2) {
        return new ToMapKeyValueSelector(function2, function);
    }

    public static <T, K, V> BiConsumer<Map<K, Collection<V>>, T> a(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2, Function<? super K, ? extends Collection<? super V>> function3) {
        return new ToMultimapKeyValueSelector(function3, function2, function);
    }

    public static <T> Function<List<T>, List<T>> a(Comparator<? super T> comparator) {
        return new ListSorter(comparator);
    }
}
