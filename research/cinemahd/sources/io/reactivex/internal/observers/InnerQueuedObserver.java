package io.reactivex.internal.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.util.QueueDrainHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class InnerQueuedObserver<T> extends AtomicReference<Disposable> implements Observer<T>, Disposable {
    private static final long serialVersionUID = -5417183359794346637L;
    volatile boolean done;
    int fusionMode;
    final InnerQueuedObserverSupport<T> parent;
    final int prefetch;
    SimpleQueue<T> queue;

    public InnerQueuedObserver(InnerQueuedObserverSupport<T> innerQueuedObserverSupport, int i) {
        this.parent = innerQueuedObserverSupport;
        this.prefetch = i;
    }

    public boolean a() {
        return this.done;
    }

    public SimpleQueue<T> b() {
        return this.queue;
    }

    public void c() {
        this.done = true;
    }

    public void dispose() {
        DisposableHelper.a((AtomicReference<Disposable>) this);
    }

    public boolean isDisposed() {
        return DisposableHelper.a((Disposable) get());
    }

    public void onComplete() {
        this.parent.a(this);
    }

    public void onError(Throwable th) {
        this.parent.a(this, th);
    }

    public void onNext(T t) {
        if (this.fusionMode == 0) {
            this.parent.a(this, t);
        } else {
            this.parent.a();
        }
    }

    public void onSubscribe(Disposable disposable) {
        if (DisposableHelper.c(this, disposable)) {
            if (disposable instanceof QueueDisposable) {
                QueueDisposable queueDisposable = (QueueDisposable) disposable;
                int a2 = queueDisposable.a(3);
                if (a2 == 1) {
                    this.fusionMode = a2;
                    this.queue = queueDisposable;
                    this.done = true;
                    this.parent.a(this);
                    return;
                } else if (a2 == 2) {
                    this.fusionMode = a2;
                    this.queue = queueDisposable;
                    return;
                }
            }
            this.queue = QueueDrainHelper.a(-this.prefetch);
        }
    }
}
