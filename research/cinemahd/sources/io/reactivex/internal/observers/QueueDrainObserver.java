package io.reactivex.internal.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.util.ObservableQueueDrain;
import io.reactivex.internal.util.QueueDrainHelper;

public abstract class QueueDrainObserver<T, U, V> extends QueueDrainSubscriberPad2 implements Observer<T>, ObservableQueueDrain<U, V> {
    protected final Observer<? super V> b;
    protected final SimplePlainQueue<U> c;
    protected volatile boolean d;
    protected volatile boolean e;
    protected Throwable f;

    public QueueDrainObserver(Observer<? super V> observer, SimplePlainQueue<U> simplePlainQueue) {
        this.b = observer;
        this.c = simplePlainQueue;
    }

    public void a(Observer<? super V> observer, U u) {
    }

    public final boolean a() {
        return this.e;
    }

    public final boolean b() {
        return this.d;
    }

    public final Throwable c() {
        return this.f;
    }

    public final boolean d() {
        return this.f6679a.getAndIncrement() == 0;
    }

    public final boolean e() {
        return this.f6679a.get() == 0 && this.f6679a.compareAndSet(0, 1);
    }

    /* access modifiers changed from: protected */
    public final void a(U u, boolean z, Disposable disposable) {
        Observer<? super V> observer = this.b;
        SimplePlainQueue<U> simplePlainQueue = this.c;
        if (this.f6679a.get() != 0 || !this.f6679a.compareAndSet(0, 1)) {
            simplePlainQueue.offer(u);
            if (!d()) {
                return;
            }
        } else {
            a(observer, u);
            if (a(-1) == 0) {
                return;
            }
        }
        QueueDrainHelper.a(simplePlainQueue, observer, z, disposable, this);
    }

    /* access modifiers changed from: protected */
    public final void b(U u, boolean z, Disposable disposable) {
        Observer<? super V> observer = this.b;
        SimplePlainQueue<U> simplePlainQueue = this.c;
        if (this.f6679a.get() != 0 || !this.f6679a.compareAndSet(0, 1)) {
            simplePlainQueue.offer(u);
            if (!d()) {
                return;
            }
        } else if (simplePlainQueue.isEmpty()) {
            a(observer, u);
            if (a(-1) == 0) {
                return;
            }
        } else {
            simplePlainQueue.offer(u);
        }
        QueueDrainHelper.a(simplePlainQueue, observer, z, disposable, this);
    }

    public final int a(int i) {
        return this.f6679a.addAndGet(i);
    }
}
