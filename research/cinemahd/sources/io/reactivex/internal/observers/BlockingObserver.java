package io.reactivex.internal.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.util.NotificationLite;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicReference;

public final class BlockingObserver<T> extends AtomicReference<Disposable> implements Observer<T>, Disposable {

    /* renamed from: a  reason: collision with root package name */
    public static final Object f6676a = new Object();
    private static final long serialVersionUID = -4875965440900746268L;
    final Queue<Object> queue;

    public BlockingObserver(Queue<Object> queue2) {
        this.queue = queue2;
    }

    public void dispose() {
        if (DisposableHelper.a((AtomicReference<Disposable>) this)) {
            this.queue.offer(f6676a);
        }
    }

    public boolean isDisposed() {
        return get() == DisposableHelper.DISPOSED;
    }

    public void onComplete() {
        this.queue.offer(NotificationLite.a());
    }

    public void onError(Throwable th) {
        this.queue.offer(NotificationLite.a(th));
    }

    public void onNext(T t) {
        Queue<Object> queue2 = this.queue;
        NotificationLite.e(t);
        queue2.offer(t);
    }

    public void onSubscribe(Disposable disposable) {
        DisposableHelper.c(this, disposable);
    }
}
