package io.reactivex.internal.observers;

public final class BlockingLastObserver<T> extends BlockingBaseObserver<T> {
    public void onError(Throwable th) {
        this.f6674a = null;
        this.b = th;
        countDown();
    }

    public void onNext(T t) {
        this.f6674a = t;
    }
}
