package io.reactivex.internal.observers;

public interface InnerQueuedObserverSupport<T> {
    void a();

    void a(InnerQueuedObserver<T> innerQueuedObserver);

    void a(InnerQueuedObserver<T> innerQueuedObserver, T t);

    void a(InnerQueuedObserver<T> innerQueuedObserver, Throwable th);
}
