package io.reactivex.internal.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class DisposableLambdaObserver<T> implements Observer<T>, Disposable {

    /* renamed from: a  reason: collision with root package name */
    final Observer<? super T> f6677a;
    final Consumer<? super Disposable> b;
    final Action c;
    Disposable d;

    public DisposableLambdaObserver(Observer<? super T> observer, Consumer<? super Disposable> consumer, Action action) {
        this.f6677a = observer;
        this.b = consumer;
        this.c = action;
    }

    public void dispose() {
        Disposable disposable = this.d;
        DisposableHelper disposableHelper = DisposableHelper.DISPOSED;
        if (disposable != disposableHelper) {
            this.d = disposableHelper;
            try {
                this.c.run();
            } catch (Throwable th) {
                Exceptions.b(th);
                RxJavaPlugins.b(th);
            }
            disposable.dispose();
        }
    }

    public boolean isDisposed() {
        return this.d.isDisposed();
    }

    public void onComplete() {
        Disposable disposable = this.d;
        DisposableHelper disposableHelper = DisposableHelper.DISPOSED;
        if (disposable != disposableHelper) {
            this.d = disposableHelper;
            this.f6677a.onComplete();
        }
    }

    public void onError(Throwable th) {
        Disposable disposable = this.d;
        DisposableHelper disposableHelper = DisposableHelper.DISPOSED;
        if (disposable != disposableHelper) {
            this.d = disposableHelper;
            this.f6677a.onError(th);
            return;
        }
        RxJavaPlugins.b(th);
    }

    public void onNext(T t) {
        this.f6677a.onNext(t);
    }

    public void onSubscribe(Disposable disposable) {
        try {
            this.b.accept(disposable);
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.f6677a.onSubscribe(this);
            }
        } catch (Throwable th) {
            Exceptions.b(th);
            disposable.dispose();
            this.d = DisposableHelper.DISPOSED;
            EmptyDisposable.a(th, (Observer<?>) this.f6677a);
        }
    }
}
