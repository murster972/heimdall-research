package io.reactivex.internal.observers;

public final class BlockingFirstObserver<T> extends BlockingBaseObserver<T> {
    public void onError(Throwable th) {
        if (this.f6674a == null) {
            this.b = th;
        }
        countDown();
    }

    public void onNext(T t) {
        if (this.f6674a == null) {
            this.f6674a = t;
            this.c.dispose();
            countDown();
        }
    }
}
