package io.reactivex.internal.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class BasicFuseableObserver<T, R> implements Observer<T>, QueueDisposable<R> {

    /* renamed from: a  reason: collision with root package name */
    protected final Observer<? super R> f6673a;
    protected Disposable b;
    protected QueueDisposable<T> c;
    protected boolean d;
    protected int e;

    public BasicFuseableObserver(Observer<? super R> observer) {
        this.f6673a = observer;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Throwable th) {
        Exceptions.b(th);
        this.b.dispose();
        onError(th);
    }

    /* access modifiers changed from: protected */
    public final int b(int i) {
        QueueDisposable<T> queueDisposable = this.c;
        if (queueDisposable == null || (i & 4) != 0) {
            return 0;
        }
        int a2 = queueDisposable.a(i);
        if (a2 != 0) {
            this.e = a2;
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return true;
    }

    public void clear() {
        this.c.clear();
    }

    public void dispose() {
        this.b.dispose();
    }

    public boolean isDisposed() {
        return this.b.isDisposed();
    }

    public boolean isEmpty() {
        return this.c.isEmpty();
    }

    public final boolean offer(R r) {
        throw new UnsupportedOperationException("Should not be called!");
    }

    public void onComplete() {
        if (!this.d) {
            this.d = true;
            this.f6673a.onComplete();
        }
    }

    public void onError(Throwable th) {
        if (this.d) {
            RxJavaPlugins.b(th);
            return;
        }
        this.d = true;
        this.f6673a.onError(th);
    }

    public final void onSubscribe(Disposable disposable) {
        if (DisposableHelper.a(this.b, disposable)) {
            this.b = disposable;
            if (disposable instanceof QueueDisposable) {
                this.c = (QueueDisposable) disposable;
            }
            if (b()) {
                this.f6673a.onSubscribe(this);
                a();
            }
        }
    }
}
