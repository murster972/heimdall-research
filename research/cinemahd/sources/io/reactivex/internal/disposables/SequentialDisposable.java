package io.reactivex.internal.disposables;

import io.reactivex.disposables.Disposable;
import java.util.concurrent.atomic.AtomicReference;

public final class SequentialDisposable extends AtomicReference<Disposable> implements Disposable {
    private static final long serialVersionUID = -754898800686245608L;

    public SequentialDisposable() {
    }

    public boolean a(Disposable disposable) {
        return DisposableHelper.a((AtomicReference<Disposable>) this, disposable);
    }

    public boolean b(Disposable disposable) {
        return DisposableHelper.b(this, disposable);
    }

    public void dispose() {
        DisposableHelper.a((AtomicReference<Disposable>) this);
    }

    public boolean isDisposed() {
        return DisposableHelper.a((Disposable) get());
    }

    public SequentialDisposable(Disposable disposable) {
        lazySet(disposable);
    }
}
