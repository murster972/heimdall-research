package io.reactivex;

import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.NotificationLite;

public final class Notification<T> {
    static final Notification<Object> b = new Notification<>((Object) null);

    /* renamed from: a  reason: collision with root package name */
    final Object f6629a;

    private Notification(Object obj) {
        this.f6629a = obj;
    }

    public static <T> Notification<T> f() {
        return b;
    }

    public Throwable a() {
        Object obj = this.f6629a;
        if (NotificationLite.d(obj)) {
            return NotificationLite.a(obj);
        }
        return null;
    }

    public T b() {
        Object obj = this.f6629a;
        if (obj == null || NotificationLite.d(obj)) {
            return null;
        }
        return this.f6629a;
    }

    public boolean c() {
        return this.f6629a == null;
    }

    public boolean d() {
        return NotificationLite.d(this.f6629a);
    }

    public boolean e() {
        Object obj = this.f6629a;
        return obj != null && !NotificationLite.d(obj);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Notification) {
            return ObjectHelper.a(this.f6629a, ((Notification) obj).f6629a);
        }
        return false;
    }

    public int hashCode() {
        Object obj = this.f6629a;
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    public String toString() {
        Object obj = this.f6629a;
        if (obj == null) {
            return "OnCompleteNotification";
        }
        if (NotificationLite.d(obj)) {
            return "OnErrorNotification[" + NotificationLite.a(obj) + "]";
        }
        return "OnNextNotification[" + this.f6629a + "]";
    }

    public static <T> Notification<T> a(T t) {
        ObjectHelper.a(t, "value is null");
        return new Notification<>(t);
    }

    public static <T> Notification<T> a(Throwable th) {
        ObjectHelper.a(th, "error is null");
        return new Notification<>(NotificationLite.a(th));
    }
}
