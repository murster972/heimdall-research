package io.reactivex;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.operators.flowable.FlowableOnBackpressureBuffer;
import io.reactivex.internal.operators.flowable.FlowableOnBackpressureDrop;
import io.reactivex.internal.operators.flowable.FlowableOnBackpressureLatest;
import io.reactivex.internal.subscribers.StrictSubscriber;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

public abstract class Flowable<T> implements Publisher<T> {

    /* renamed from: a  reason: collision with root package name */
    static final int f6628a = Math.max(1, Integer.getInteger("rx2.buffer-size", 128).intValue());

    public static int d() {
        return f6628a;
    }

    public final Flowable<T> a() {
        return a(d(), false, true);
    }

    public final Flowable<T> b() {
        return RxJavaPlugins.a(new FlowableOnBackpressureDrop(this));
    }

    /* access modifiers changed from: protected */
    public abstract void b(Subscriber<? super T> subscriber);

    public final Flowable<T> c() {
        return RxJavaPlugins.a(new FlowableOnBackpressureLatest(this));
    }

    public final Flowable<T> a(int i, boolean z, boolean z2) {
        ObjectHelper.a(i, "bufferSize");
        return RxJavaPlugins.a(new FlowableOnBackpressureBuffer(this, i, z2, z, Functions.c));
    }

    public final void a(Subscriber<? super T> subscriber) {
        if (subscriber instanceof FlowableSubscriber) {
            a((FlowableSubscriber) subscriber);
            return;
        }
        ObjectHelper.a(subscriber, "s is null");
        a(new StrictSubscriber(subscriber));
    }

    public final void a(FlowableSubscriber<? super T> flowableSubscriber) {
        ObjectHelper.a(flowableSubscriber, "s is null");
        try {
            Subscriber<? super Object> a2 = RxJavaPlugins.a(this, flowableSubscriber);
            ObjectHelper.a(a2, "The RxJavaPlugins.onSubscribe hook returned a null FlowableSubscriber. Please check the handler provided to RxJavaPlugins.setOnFlowableSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins");
            b(a2);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable th) {
            Exceptions.b(th);
            RxJavaPlugins.b(th);
            NullPointerException nullPointerException = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }
}
