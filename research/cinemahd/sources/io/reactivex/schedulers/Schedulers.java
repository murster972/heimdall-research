package io.reactivex.schedulers;

import io.reactivex.Scheduler;
import io.reactivex.internal.schedulers.ComputationScheduler;
import io.reactivex.internal.schedulers.IoScheduler;
import io.reactivex.internal.schedulers.NewThreadScheduler;
import io.reactivex.internal.schedulers.SingleScheduler;
import io.reactivex.internal.schedulers.TrampolineScheduler;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.Callable;

public final class Schedulers {

    /* renamed from: a  reason: collision with root package name */
    static final Scheduler f6890a = RxJavaPlugins.b((Callable<Scheduler>) new ComputationTask());
    static final Scheduler b = RxJavaPlugins.c((Callable<Scheduler>) new IOTask());
    static final Scheduler c = TrampolineScheduler.b();

    static final class ComputationHolder {

        /* renamed from: a  reason: collision with root package name */
        static final Scheduler f6891a = new ComputationScheduler();

        ComputationHolder() {
        }
    }

    static final class ComputationTask implements Callable<Scheduler> {
        ComputationTask() {
        }

        public Scheduler call() throws Exception {
            return ComputationHolder.f6891a;
        }
    }

    static final class IOTask implements Callable<Scheduler> {
        IOTask() {
        }

        public Scheduler call() throws Exception {
            return IoHolder.f6892a;
        }
    }

    static final class IoHolder {

        /* renamed from: a  reason: collision with root package name */
        static final Scheduler f6892a = new IoScheduler();

        IoHolder() {
        }
    }

    static final class NewThreadHolder {

        /* renamed from: a  reason: collision with root package name */
        static final Scheduler f6893a = new NewThreadScheduler();

        NewThreadHolder() {
        }
    }

    static final class NewThreadTask implements Callable<Scheduler> {
        NewThreadTask() {
        }

        public Scheduler call() throws Exception {
            return NewThreadHolder.f6893a;
        }
    }

    static final class SingleHolder {

        /* renamed from: a  reason: collision with root package name */
        static final Scheduler f6894a = new SingleScheduler();

        SingleHolder() {
        }
    }

    static final class SingleTask implements Callable<Scheduler> {
        SingleTask() {
        }

        public Scheduler call() throws Exception {
            return SingleHolder.f6894a;
        }
    }

    static {
        RxJavaPlugins.e(new SingleTask());
        RxJavaPlugins.d(new NewThreadTask());
    }

    private Schedulers() {
        throw new IllegalStateException("No instances!");
    }

    public static Scheduler a() {
        return RxJavaPlugins.a(f6890a);
    }

    public static Scheduler b() {
        return RxJavaPlugins.b(b);
    }

    public static Scheduler c() {
        return c;
    }
}
