package io.reactivex.schedulers;

import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.TimeUnit;

public final class Timed<T> {

    /* renamed from: a  reason: collision with root package name */
    final T f6895a;
    final long b;
    final TimeUnit c;

    public Timed(T t, long j, TimeUnit timeUnit) {
        this.f6895a = t;
        this.b = j;
        ObjectHelper.a(timeUnit, "unit is null");
        this.c = timeUnit;
    }

    public long a() {
        return this.b;
    }

    public T b() {
        return this.f6895a;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Timed)) {
            return false;
        }
        Timed timed = (Timed) obj;
        if (!ObjectHelper.a((Object) this.f6895a, (Object) timed.f6895a) || this.b != timed.b || !ObjectHelper.a((Object) this.c, (Object) timed.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        T t = this.f6895a;
        int hashCode = t != null ? t.hashCode() : 0;
        long j = this.b;
        return (((hashCode * 31) + ((int) (j ^ (j >>> 31)))) * 31) + this.c.hashCode();
    }

    public String toString() {
        return "Timed[time=" + this.b + ", unit=" + this.c + ", value=" + this.f6895a + "]";
    }
}
