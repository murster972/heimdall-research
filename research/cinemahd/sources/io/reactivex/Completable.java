package io.reactivex;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class Completable implements CompletableSource {
    private static NullPointerException a(Throwable th) {
        NullPointerException nullPointerException = new NullPointerException("Actually not, but can't pass out an exception otherwise...");
        nullPointerException.initCause(th);
        return nullPointerException;
    }

    /* access modifiers changed from: protected */
    public abstract void b(CompletableObserver completableObserver);

    public final void a(CompletableObserver completableObserver) {
        ObjectHelper.a(completableObserver, "s is null");
        try {
            CompletableObserver a2 = RxJavaPlugins.a(this, completableObserver);
            ObjectHelper.a(a2, "The RxJavaPlugins.onSubscribe hook returned a null CompletableObserver. Please check the handler provided to RxJavaPlugins.setOnCompletableSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins");
            b(a2);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable th) {
            Exceptions.b(th);
            RxJavaPlugins.b(th);
            throw a(th);
        }
    }
}
