package io.reactivex.plugins;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.observables.ConnectableObservable;
import java.util.concurrent.Callable;
import org.reactivestreams.Subscriber;

public final class RxJavaPlugins {

    /* renamed from: a  reason: collision with root package name */
    static volatile Consumer<? super Throwable> f6889a;
    static volatile Function<? super Runnable, ? extends Runnable> b;
    static volatile Function<? super Callable<Scheduler>, ? extends Scheduler> c;
    static volatile Function<? super Callable<Scheduler>, ? extends Scheduler> d;
    static volatile Function<? super Callable<Scheduler>, ? extends Scheduler> e;
    static volatile Function<? super Callable<Scheduler>, ? extends Scheduler> f;
    static volatile Function<? super Scheduler, ? extends Scheduler> g;
    static volatile Function<? super Scheduler, ? extends Scheduler> h;
    static volatile Function<? super Flowable, ? extends Flowable> i;
    static volatile Function<? super Observable, ? extends Observable> j;
    static volatile Function<? super ConnectableObservable, ? extends ConnectableObservable> k;
    static volatile Function<? super Maybe, ? extends Maybe> l;
    static volatile Function<? super Single, ? extends Single> m;
    static volatile Function<? super Completable, ? extends Completable> n;
    static volatile BiFunction<? super Flowable, ? super Subscriber, ? extends Subscriber> o;
    static volatile BiFunction<? super Maybe, ? super MaybeObserver, ? extends MaybeObserver> p;
    static volatile BiFunction<? super Observable, ? super Observer, ? extends Observer> q;
    static volatile BiFunction<? super Single, ? super SingleObserver, ? extends SingleObserver> r;
    static volatile BiFunction<? super Completable, ? super CompletableObserver, ? extends CompletableObserver> s;
    static volatile BooleanSupplier t;
    static volatile boolean u;
    static volatile boolean v;

    private RxJavaPlugins() {
        throw new IllegalStateException("No instances!");
    }

    public static boolean a() {
        return v;
    }

    public static Scheduler b(Callable<Scheduler> callable) {
        ObjectHelper.a(callable, "Scheduler Callable can't be null");
        Function<? super Callable<Scheduler>, ? extends Scheduler> function = c;
        if (function == null) {
            return a(callable);
        }
        return a(function, callable);
    }

    public static Scheduler c(Callable<Scheduler> callable) {
        ObjectHelper.a(callable, "Scheduler Callable can't be null");
        Function<? super Callable<Scheduler>, ? extends Scheduler> function = e;
        if (function == null) {
            return a(callable);
        }
        return a(function, callable);
    }

    public static Scheduler d(Callable<Scheduler> callable) {
        ObjectHelper.a(callable, "Scheduler Callable can't be null");
        Function<? super Callable<Scheduler>, ? extends Scheduler> function = f;
        if (function == null) {
            return a(callable);
        }
        return a(function, callable);
    }

    public static Scheduler e(Callable<Scheduler> callable) {
        ObjectHelper.a(callable, "Scheduler Callable can't be null");
        Function<? super Callable<Scheduler>, ? extends Scheduler> function = d;
        if (function == null) {
            return a(callable);
        }
        return a(function, callable);
    }

    public static Scheduler a(Scheduler scheduler) {
        Function<? super Scheduler, ? extends Scheduler> function = g;
        if (function == null) {
            return scheduler;
        }
        return (Scheduler) a(function, scheduler);
    }

    static boolean a(Throwable th) {
        if (!(th instanceof OnErrorNotImplementedException) && !(th instanceof MissingBackpressureException) && !(th instanceof IllegalStateException) && !(th instanceof NullPointerException) && !(th instanceof IllegalArgumentException) && !(th instanceof CompositeException)) {
            return false;
        }
        return true;
    }

    public static void b(Throwable th) {
        Consumer<? super Throwable> consumer = f6889a;
        if (th == null) {
            th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        } else if (!a(th)) {
            th = new UndeliverableException(th);
        }
        if (consumer != null) {
            try {
                consumer.accept(th);
                return;
            } catch (Throwable th2) {
                th2.printStackTrace();
                c(th2);
            }
        }
        th.printStackTrace();
        c(th);
    }

    static void c(Throwable th) {
        Thread currentThread = Thread.currentThread();
        currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, th);
    }

    public static Runnable a(Runnable runnable) {
        ObjectHelper.a(runnable, "run is null");
        Function<? super Runnable, ? extends Runnable> function = b;
        if (function == null) {
            return runnable;
        }
        return (Runnable) a(function, runnable);
    }

    public static void a(Consumer<? super Throwable> consumer) {
        if (!u) {
            f6889a = consumer;
            return;
        }
        throw new IllegalStateException("Plugins can't be changed anymore");
    }

    public static Scheduler b(Scheduler scheduler) {
        Function<? super Scheduler, ? extends Scheduler> function = h;
        if (function == null) {
            return scheduler;
        }
        return (Scheduler) a(function, scheduler);
    }

    public static <T> Subscriber<? super T> a(Flowable<T> flowable, Subscriber<? super T> subscriber) {
        BiFunction<? super Flowable, ? super Subscriber, ? extends Subscriber> biFunction = o;
        return biFunction != null ? (Subscriber) a(biFunction, flowable, subscriber) : subscriber;
    }

    public static boolean b() {
        BooleanSupplier booleanSupplier = t;
        if (booleanSupplier == null) {
            return false;
        }
        try {
            return booleanSupplier.a();
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    public static <T> Observer<? super T> a(Observable<T> observable, Observer<? super T> observer) {
        BiFunction<? super Observable, ? super Observer, ? extends Observer> biFunction = q;
        return biFunction != null ? (Observer) a(biFunction, observable, observer) : observer;
    }

    public static <T> SingleObserver<? super T> a(Single<T> single, SingleObserver<? super T> singleObserver) {
        BiFunction<? super Single, ? super SingleObserver, ? extends SingleObserver> biFunction = r;
        return biFunction != null ? (SingleObserver) a(biFunction, single, singleObserver) : singleObserver;
    }

    public static CompletableObserver a(Completable completable, CompletableObserver completableObserver) {
        BiFunction<? super Completable, ? super CompletableObserver, ? extends CompletableObserver> biFunction = s;
        return biFunction != null ? (CompletableObserver) a(biFunction, completable, completableObserver) : completableObserver;
    }

    public static <T> MaybeObserver<? super T> a(Maybe<T> maybe, MaybeObserver<? super T> maybeObserver) {
        BiFunction<? super Maybe, ? super MaybeObserver, ? extends MaybeObserver> biFunction = p;
        return biFunction != null ? (MaybeObserver) a(biFunction, maybe, maybeObserver) : maybeObserver;
    }

    public static <T> Maybe<T> a(Maybe<T> maybe) {
        Function<? super Maybe, ? extends Maybe> function = l;
        return function != null ? (Maybe) a(function, maybe) : maybe;
    }

    public static <T> Flowable<T> a(Flowable<T> flowable) {
        Function<? super Flowable, ? extends Flowable> function = i;
        return function != null ? (Flowable) a(function, flowable) : flowable;
    }

    public static <T> Observable<T> a(Observable<T> observable) {
        Function<? super Observable, ? extends Observable> function = j;
        return function != null ? (Observable) a(function, observable) : observable;
    }

    public static <T> ConnectableObservable<T> a(ConnectableObservable<T> connectableObservable) {
        Function<? super ConnectableObservable, ? extends ConnectableObservable> function = k;
        return function != null ? (ConnectableObservable) a(function, connectableObservable) : connectableObservable;
    }

    public static <T> Single<T> a(Single<T> single) {
        Function<? super Single, ? extends Single> function = m;
        return function != null ? (Single) a(function, single) : single;
    }

    public static Completable a(Completable completable) {
        Function<? super Completable, ? extends Completable> function = n;
        return function != null ? (Completable) a(function, completable) : completable;
    }

    static <T, R> R a(Function<T, R> function, T t2) {
        try {
            return function.apply(t2);
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    static <T, U, R> R a(BiFunction<T, U, R> biFunction, T t2, U u2) {
        try {
            return biFunction.a(t2, u2);
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    static Scheduler a(Callable<Scheduler> callable) {
        try {
            Scheduler call = callable.call();
            ObjectHelper.a(call, "Scheduler Callable result can't be null");
            return call;
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    static Scheduler a(Function<? super Callable<Scheduler>, ? extends Scheduler> function, Callable<Scheduler> callable) {
        Object a2 = a(function, callable);
        ObjectHelper.a(a2, "Scheduler Callable result can't be null");
        return (Scheduler) a2;
    }
}
