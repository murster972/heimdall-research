package io.reactivex;

import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.FuseToObservable;
import io.reactivex.internal.observers.BlockingMultiObserver;
import io.reactivex.internal.observers.ConsumerSingleObserver;
import io.reactivex.internal.operators.single.SingleMap;
import io.reactivex.internal.operators.single.SingleObserveOn;
import io.reactivex.internal.operators.single.SingleToObservable;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class Single<T> implements SingleSource<T> {
    public final <R> Single<R> a(Function<? super T, ? extends R> function) {
        ObjectHelper.a(function, "mapper is null");
        return RxJavaPlugins.a(new SingleMap(this, function));
    }

    public final T b() {
        BlockingMultiObserver blockingMultiObserver = new BlockingMultiObserver();
        a(blockingMultiObserver);
        return blockingMultiObserver.a();
    }

    /* access modifiers changed from: protected */
    public abstract void b(SingleObserver<? super T> singleObserver);

    public final Observable<T> c() {
        if (this instanceof FuseToObservable) {
            return ((FuseToObservable) this).a();
        }
        return RxJavaPlugins.a(new SingleToObservable(this));
    }

    public final Single<T> a(Scheduler scheduler) {
        ObjectHelper.a(scheduler, "scheduler is null");
        return RxJavaPlugins.a(new SingleObserveOn(this, scheduler));
    }

    public final Disposable a(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2) {
        ObjectHelper.a(consumer, "onSuccess is null");
        ObjectHelper.a(consumer2, "onError is null");
        ConsumerSingleObserver consumerSingleObserver = new ConsumerSingleObserver(consumer, consumer2);
        a(consumerSingleObserver);
        return consumerSingleObserver;
    }

    public final void a(SingleObserver<? super T> singleObserver) {
        ObjectHelper.a(singleObserver, "subscriber is null");
        SingleObserver<? super Object> a2 = RxJavaPlugins.a(this, singleObserver);
        ObjectHelper.a(a2, "The RxJavaPlugins.onSubscribe hook returned a null SingleObserver. Please check the handler provided to RxJavaPlugins.setOnSingleSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins");
        try {
            b(a2);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable th) {
            Exceptions.b(th);
            NullPointerException nullPointerException = new NullPointerException("subscribeActual failed");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }
}
