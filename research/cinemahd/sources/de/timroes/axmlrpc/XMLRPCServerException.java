package de.timroes.axmlrpc;

public class XMLRPCServerException extends XMLRPCException {
    private final int errornr;

    public String getMessage() {
        return super.getMessage() + " [" + this.errornr + "]";
    }
}
