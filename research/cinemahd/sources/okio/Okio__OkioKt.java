package okio;

import kotlin.jvm.internal.Intrinsics;

final /* synthetic */ class Okio__OkioKt {
    public static final BufferedSource a(Source source) {
        Intrinsics.b(source, "$this$buffer");
        return new RealBufferedSource(source);
    }

    public static final BufferedSink a(Sink sink) {
        Intrinsics.b(sink, "$this$buffer");
        return new RealBufferedSink(sink);
    }

    public static final Sink a() {
        return new BlackholeSink();
    }
}
