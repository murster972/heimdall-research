package okio;

import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Deflater;
import kotlin.jvm.internal.Intrinsics;

public final class GzipSink implements Sink {

    /* renamed from: a  reason: collision with root package name */
    private final RealBufferedSink f4073a;
    private final Deflater b = new Deflater(-1, true);
    private final DeflaterSink c = new DeflaterSink((BufferedSink) this.f4073a, this.b);
    private boolean d;
    private final CRC32 e = new CRC32();

    public GzipSink(Sink sink) {
        Intrinsics.b(sink, "sink");
        this.f4073a = new RealBufferedSink(sink);
        Buffer buffer = this.f4073a.f4079a;
        buffer.writeShort(8075);
        buffer.writeByte(8);
        buffer.writeByte(0);
        buffer.writeInt(0);
        buffer.writeByte(0);
        buffer.writeByte(0);
    }

    private final void a(Buffer buffer, long j) {
        Segment segment = buffer.f4066a;
        if (segment != null) {
            while (j > 0) {
                int min = (int) Math.min(j, (long) (segment.c - segment.b));
                this.e.update(segment.f4083a, segment.b, min);
                j -= (long) min;
                segment = segment.f;
                if (segment == null) {
                    Intrinsics.a();
                    throw null;
                }
            }
            return;
        }
        Intrinsics.a();
        throw null;
    }

    private final void b() {
        this.f4073a.a((int) this.e.getValue());
        this.f4073a.a((int) this.b.getBytesRead());
    }

    public void close() throws IOException {
        if (!this.d) {
            Throwable th = null;
            try {
                this.c.b();
                b();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.b.end();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            try {
                this.f4073a.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.d = true;
            if (th != null) {
                throw th;
            }
        }
    }

    public void flush() throws IOException {
        this.c.flush();
    }

    public Timeout timeout() {
        return this.f4073a.timeout();
    }

    public void write(Buffer buffer, long j) throws IOException {
        Intrinsics.b(buffer, "source");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (i != 0) {
            a(buffer, j);
            this.c.write(buffer, j);
        }
    }
}
