package okio;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import kotlin.jvm.internal.Intrinsics;

public final class RealBufferedSink implements BufferedSink {

    /* renamed from: a  reason: collision with root package name */
    public final Buffer f4079a = new Buffer();
    public boolean b;
    public final Sink c;

    public RealBufferedSink(Sink sink) {
        Intrinsics.b(sink, "sink");
        this.c = sink;
    }

    public Buffer a() {
        return this.f4079a;
    }

    public void close() {
        if (!this.b) {
            Throwable th = null;
            try {
                if (this.f4079a.u() > 0) {
                    this.c.write(this.f4079a, this.f4079a.u());
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.c.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.b = true;
            if (th != null) {
                throw th;
            }
        }
    }

    public BufferedSink e(long j) {
        if (!this.b) {
            this.f4079a.e(j);
            l();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink f(long j) {
        if (!this.b) {
            this.f4079a.f(j);
            return l();
        }
        throw new IllegalStateException("closed".toString());
    }

    public void flush() {
        if (!this.b) {
            if (this.f4079a.u() > 0) {
                Sink sink = this.c;
                Buffer buffer = this.f4079a;
                sink.write(buffer, buffer.u());
            }
            this.c.flush();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    public boolean isOpen() {
        return !this.b;
    }

    public Buffer j() {
        return this.f4079a;
    }

    public BufferedSink k() {
        if (!this.b) {
            long u = this.f4079a.u();
            if (u > 0) {
                this.c.write(this.f4079a, u);
            }
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink l() {
        if (!this.b) {
            long p = this.f4079a.p();
            if (p > 0) {
                this.c.write(this.f4079a, p);
            }
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public OutputStream m() {
        return new RealBufferedSink$outputStream$1(this);
    }

    public Timeout timeout() {
        return this.c.timeout();
    }

    public String toString() {
        return "buffer(" + this.c + ')';
    }

    public int write(ByteBuffer byteBuffer) {
        Intrinsics.b(byteBuffer, "source");
        if (!this.b) {
            int write = this.f4079a.write(byteBuffer);
            l();
            return write;
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink writeByte(int i) {
        if (!this.b) {
            this.f4079a.writeByte(i);
            l();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink writeInt(int i) {
        if (!this.b) {
            this.f4079a.writeInt(i);
            return l();
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink writeShort(int i) {
        if (!this.b) {
            this.f4079a.writeShort(i);
            l();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink a(ByteString byteString) {
        Intrinsics.b(byteString, "byteString");
        if (!this.b) {
            this.f4079a.a(byteString);
            l();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public void write(Buffer buffer, long j) {
        Intrinsics.b(buffer, "source");
        if (!this.b) {
            this.f4079a.write(buffer, j);
            l();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink a(String str) {
        Intrinsics.b(str, "string");
        if (!this.b) {
            this.f4079a.a(str);
            return l();
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink write(byte[] bArr) {
        Intrinsics.b(bArr, "source");
        if (!this.b) {
            this.f4079a.write(bArr);
            l();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public long a(Source source) {
        Intrinsics.b(source, "source");
        long j = 0;
        while (true) {
            long read = source.read(this.f4079a, (long) 8192);
            if (read == -1) {
                return j;
            }
            j += read;
            l();
        }
    }

    public BufferedSink a(int i) {
        if (!this.b) {
            this.f4079a.c(i);
            l();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink write(byte[] bArr, int i, int i2) {
        Intrinsics.b(bArr, "source");
        if (!this.b) {
            this.f4079a.write(bArr, i, i2);
            l();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }
}
