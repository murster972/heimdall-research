package okio;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;

public class AsyncTimeout extends Timeout {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static final long IDLE_TIMEOUT_MILLIS = TimeUnit.SECONDS.toMillis(60);
    /* access modifiers changed from: private */
    public static final long IDLE_TIMEOUT_NANOS = TimeUnit.MILLISECONDS.toNanos(IDLE_TIMEOUT_MILLIS);
    private static final int TIMEOUT_WRITE_SIZE = 65536;
    /* access modifiers changed from: private */
    public static AsyncTimeout head;
    private boolean inQueue;
    /* access modifiers changed from: private */
    public AsyncTimeout next;
    /* access modifiers changed from: private */
    public long timeoutAt;

    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final void a(AsyncTimeout asyncTimeout, long j, boolean z) {
            Class<AsyncTimeout> cls = AsyncTimeout.class;
            synchronized (cls) {
                if (AsyncTimeout.head == null) {
                    AsyncTimeout.head = new AsyncTimeout();
                    new Watchdog().start();
                }
                long nanoTime = System.nanoTime();
                int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
                if (i != 0 && z) {
                    asyncTimeout.timeoutAt = Math.min(j, asyncTimeout.deadlineNanoTime() - nanoTime) + nanoTime;
                } else if (i != 0) {
                    asyncTimeout.timeoutAt = j + nanoTime;
                } else if (z) {
                    asyncTimeout.timeoutAt = asyncTimeout.deadlineNanoTime();
                } else {
                    throw new AssertionError();
                }
                long access$remainingNanos = asyncTimeout.remainingNanos(nanoTime);
                AsyncTimeout access$getHead$cp = AsyncTimeout.head;
                if (access$getHead$cp != null) {
                    while (true) {
                        if (access$getHead$cp.next != null) {
                            AsyncTimeout access$getNext$p = access$getHead$cp.next;
                            if (access$getNext$p == null) {
                                Intrinsics.a();
                                throw null;
                            } else if (access$remainingNanos < access$getNext$p.remainingNanos(nanoTime)) {
                                break;
                            } else {
                                access$getHead$cp = access$getHead$cp.next;
                                if (access$getHead$cp == null) {
                                    Intrinsics.a();
                                    throw null;
                                }
                            }
                        }
                    }
                    asyncTimeout.next = access$getHead$cp.next;
                    access$getHead$cp.next = asyncTimeout;
                    if (access$getHead$cp == AsyncTimeout.head) {
                        cls.notify();
                    }
                    Unit unit = Unit.f6917a;
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
        }

        /* access modifiers changed from: private */
        public final boolean a(AsyncTimeout asyncTimeout) {
            synchronized (AsyncTimeout.class) {
                for (AsyncTimeout access$getHead$cp = AsyncTimeout.head; access$getHead$cp != null; access$getHead$cp = access$getHead$cp.next) {
                    if (access$getHead$cp.next == asyncTimeout) {
                        access$getHead$cp.next = asyncTimeout.next;
                        asyncTimeout.next = null;
                        return false;
                    }
                }
                return true;
            }
        }

        public final AsyncTimeout a() throws InterruptedException {
            Class<AsyncTimeout> cls = AsyncTimeout.class;
            AsyncTimeout access$getHead$cp = AsyncTimeout.head;
            if (access$getHead$cp != null) {
                AsyncTimeout access$getNext$p = access$getHead$cp.next;
                if (access$getNext$p == null) {
                    long nanoTime = System.nanoTime();
                    cls.wait(AsyncTimeout.IDLE_TIMEOUT_MILLIS);
                    AsyncTimeout access$getHead$cp2 = AsyncTimeout.head;
                    if (access$getHead$cp2 == null) {
                        Intrinsics.a();
                        throw null;
                    } else if (access$getHead$cp2.next != null || System.nanoTime() - nanoTime < AsyncTimeout.IDLE_TIMEOUT_NANOS) {
                        return null;
                    } else {
                        return AsyncTimeout.head;
                    }
                } else {
                    long access$remainingNanos = access$getNext$p.remainingNanos(System.nanoTime());
                    if (access$remainingNanos > 0) {
                        long j = access$remainingNanos / 1000000;
                        cls.wait(j, (int) (access$remainingNanos - (1000000 * j)));
                        return null;
                    }
                    AsyncTimeout access$getHead$cp3 = AsyncTimeout.head;
                    if (access$getHead$cp3 != null) {
                        access$getHead$cp3.next = access$getNext$p.next;
                        access$getNext$p.next = null;
                        return access$getNext$p;
                    }
                    Intrinsics.a();
                    throw null;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        }
    }

    private static final class Watchdog extends Thread {
        public Watchdog() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0018, code lost:
            if (r1 == null) goto L_0x0000;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x001a, code lost:
            r1.timedOut();
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r3 = this;
            L_0x0000:
                java.lang.Class<okio.AsyncTimeout> r0 = okio.AsyncTimeout.class
                monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0000 }
                okio.AsyncTimeout$Companion r1 = okio.AsyncTimeout.Companion     // Catch:{ all -> 0x001e }
                okio.AsyncTimeout r1 = r1.a()     // Catch:{ all -> 0x001e }
                okio.AsyncTimeout r2 = okio.AsyncTimeout.head     // Catch:{ all -> 0x001e }
                if (r1 != r2) goto L_0x0015
                r1 = 0
                okio.AsyncTimeout.head = r1     // Catch:{ all -> 0x001e }
                monitor-exit(r0)     // Catch:{ InterruptedException -> 0x0000 }
                return
            L_0x0015:
                kotlin.Unit r2 = kotlin.Unit.f6917a     // Catch:{ all -> 0x001e }
                monitor-exit(r0)     // Catch:{ InterruptedException -> 0x0000 }
                if (r1 == 0) goto L_0x0000
                r1.timedOut()     // Catch:{ InterruptedException -> 0x0000 }
                goto L_0x0000
            L_0x001e:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ InterruptedException -> 0x0000 }
                throw r1     // Catch:{ InterruptedException -> 0x0000 }
            */
            throw new UnsupportedOperationException("Method not decompiled: okio.AsyncTimeout.Watchdog.run():void");
        }
    }

    /* access modifiers changed from: private */
    public final long remainingNanos(long j) {
        return this.timeoutAt - j;
    }

    public final IOException access$newTimeoutException(IOException iOException) {
        return newTimeoutException(iOException);
    }

    public final void enter() {
        if (!this.inQueue) {
            long timeoutNanos = timeoutNanos();
            boolean hasDeadline = hasDeadline();
            if (timeoutNanos != 0 || hasDeadline) {
                this.inQueue = true;
                Companion.a(this, timeoutNanos, hasDeadline);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit".toString());
    }

    public final boolean exit() {
        if (!this.inQueue) {
            return false;
        }
        this.inQueue = false;
        return Companion.a(this);
    }

    /* access modifiers changed from: protected */
    public IOException newTimeoutException(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    public final Sink sink(Sink sink) {
        Intrinsics.b(sink, "sink");
        return new AsyncTimeout$sink$1(this, sink);
    }

    public final Source source(Source source) {
        Intrinsics.b(source, "source");
        return new AsyncTimeout$source$1(this, source);
    }

    /* access modifiers changed from: protected */
    public void timedOut() {
    }

    public final <T> T withTimeout(Function0<? extends T> function0) {
        Intrinsics.b(function0, "block");
        enter();
        try {
            T invoke = function0.invoke();
            InlineMarker.b(1);
            if (!exit()) {
                InlineMarker.a(1);
                return invoke;
            }
            throw access$newTimeoutException((IOException) null);
        } catch (IOException e) {
            e = e;
            if (exit()) {
                e = access$newTimeoutException(e);
            }
            throw e;
        } catch (Throwable th) {
            InlineMarker.b(1);
            boolean exit = exit();
            InlineMarker.a(1);
            throw th;
        }
    }
}
