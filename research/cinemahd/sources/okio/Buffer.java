package okio;

import com.facebook.common.time.Clock;
import com.facebook.imageutils.JfifUtil;
import com.google.ar.core.ImageMetadata;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import java.util.Arrays;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import kotlin.text.Charsets;
import okhttp3.internal.connection.RealConnection;
import okio.internal.BufferKt;

public final class Buffer implements BufferedSource, BufferedSink, Cloneable, ByteChannel {

    /* renamed from: a  reason: collision with root package name */
    public Segment f4066a;
    private long b;

    public static final class UnsafeCursor implements Closeable {

        /* renamed from: a  reason: collision with root package name */
        public Buffer f4067a;
        public boolean b;
        private Segment c;
        public long d = -1;
        public byte[] e;
        public int f = -1;
        public int g = -1;

        public void close() {
            if (this.f4067a != null) {
                this.f4067a = null;
                this.c = null;
                this.d = -1;
                this.e = null;
                this.f = -1;
                this.g = -1;
                return;
            }
            throw new IllegalStateException("not attached to a buffer".toString());
        }

        public final long h(long j) {
            long j2 = j;
            Buffer buffer = this.f4067a;
            if (buffer == null) {
                throw new IllegalStateException("not attached to a buffer".toString());
            } else if (this.b) {
                long u = buffer.u();
                int i = 1;
                int i2 = (j2 > u ? 1 : (j2 == u ? 0 : -1));
                if (i2 <= 0) {
                    if (j2 >= 0) {
                        long j3 = u - j2;
                        while (true) {
                            if (j3 <= 0) {
                                break;
                            }
                            Segment segment = buffer.f4066a;
                            if (segment != null) {
                                Segment segment2 = segment.g;
                                if (segment2 != null) {
                                    int i3 = segment2.c;
                                    long j4 = (long) (i3 - segment2.b);
                                    if (j4 > j3) {
                                        segment2.c = i3 - ((int) j3);
                                        break;
                                    }
                                    buffer.f4066a = segment2.b();
                                    SegmentPool.c.a(segment2);
                                    j3 -= j4;
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                        this.c = null;
                        this.d = j2;
                        this.e = null;
                        this.f = -1;
                        this.g = -1;
                    } else {
                        throw new IllegalArgumentException(("newSize < 0: " + j2).toString());
                    }
                } else if (i2 > 0) {
                    long j5 = j2 - u;
                    boolean z = true;
                    while (j5 > 0) {
                        Segment b2 = buffer.b(i);
                        int min = (int) Math.min(j5, (long) (8192 - b2.c));
                        b2.c += min;
                        j5 -= (long) min;
                        if (z) {
                            this.c = b2;
                            this.d = u;
                            this.e = b2.f4083a;
                            int i4 = b2.c;
                            this.f = i4 - min;
                            this.g = i4;
                            z = false;
                        }
                        i = 1;
                    }
                }
                buffer.j(j2);
                return u;
            } else {
                throw new IllegalStateException("resizeBuffer() only permitted for read/write buffers".toString());
            }
        }

        public final int i(long j) {
            Segment segment;
            long j2;
            long j3 = j;
            Buffer buffer = this.f4067a;
            if (buffer == null) {
                throw new IllegalStateException("not attached to a buffer".toString());
            } else if (j3 < ((long) -1) || j3 > buffer.u()) {
                StringCompanionObject stringCompanionObject = StringCompanionObject.f6941a;
                Object[] objArr = {Long.valueOf(j), Long.valueOf(buffer.u())};
                String format = String.format("offset=%s > size=%s", Arrays.copyOf(objArr, objArr.length));
                Intrinsics.a((Object) format, "java.lang.String.format(format, *args)");
                throw new ArrayIndexOutOfBoundsException(format);
            } else if (j3 == -1 || j3 == buffer.u()) {
                this.c = null;
                this.d = j3;
                this.e = null;
                this.f = -1;
                this.g = -1;
                return -1;
            } else {
                long j4 = 0;
                long u = buffer.u();
                Segment segment2 = buffer.f4066a;
                Segment segment3 = this.c;
                if (segment3 != null) {
                    long j5 = this.d;
                    int i = this.f;
                    if (segment3 != null) {
                        long j6 = j5 - ((long) (i - segment3.b));
                        if (j6 > j3) {
                            u = j6;
                            Segment segment4 = segment3;
                            segment3 = segment2;
                            segment2 = segment4;
                        } else {
                            j4 = j6;
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                } else {
                    segment3 = segment2;
                }
                if (u - j3 > j3 - j4) {
                    while (segment != null) {
                        int i2 = segment.c;
                        int i3 = segment.b;
                        if (j3 >= ((long) (i2 - i3)) + j2) {
                            j4 = j2 + ((long) (i2 - i3));
                            segment3 = segment.f;
                        }
                    }
                    Intrinsics.a();
                    throw null;
                }
                segment = segment2;
                j2 = u;
                while (j2 > j3) {
                    if (segment != null) {
                        segment = segment.g;
                        if (segment != null) {
                            j2 -= (long) (segment.c - segment.b);
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                if (this.b) {
                    if (segment == null) {
                        Intrinsics.a();
                        throw null;
                    } else if (segment.d) {
                        Segment d2 = segment.d();
                        if (buffer.f4066a == segment) {
                            buffer.f4066a = d2;
                        }
                        segment = segment.a(d2);
                        Segment segment5 = segment.g;
                        if (segment5 != null) {
                            segment5.b();
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                }
                this.c = segment;
                this.d = j3;
                if (segment != null) {
                    this.e = segment.f4083a;
                    this.f = segment.b + ((int) (j3 - j2));
                    this.g = segment.c;
                    return this.g - this.f;
                }
                Intrinsics.a();
                throw null;
            }
        }

        public final int s() {
            long j = this.d;
            Buffer buffer = this.f4067a;
            if (buffer != null) {
                if (j != buffer.u()) {
                    long j2 = this.d;
                    return i(j2 == -1 ? 0 : j2 + ((long) (this.g - this.f)));
                }
                throw new IllegalStateException("no more bytes".toString());
            }
            Intrinsics.a();
            throw null;
        }
    }

    public Buffer a() {
        return this;
    }

    public InputStream b() {
        return new Buffer$inputStream$1(this);
    }

    public void c(long j) throws EOFException {
        if (this.b < j) {
            throw new EOFException();
        }
    }

    public void close() {
    }

    public ByteString d(long j) throws EOFException {
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (u() < j) {
            throw new EOFException();
        } else if (j < ((long) 4096)) {
            return new ByteString(b(j));
        } else {
            ByteString a2 = a((int) j);
            skip(j);
            return a2;
        }
    }

    /* JADX WARNING: type inference failed for: r20v0, types: [java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r20) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r3 = 1
            if (r0 != r1) goto L_0x0009
            goto L_0x008f
        L_0x0009:
            boolean r4 = r1 instanceof okio.Buffer
            if (r4 != 0) goto L_0x0010
        L_0x000d:
            r3 = 0
            goto L_0x008f
        L_0x0010:
            long r4 = r19.u()
            okio.Buffer r1 = (okio.Buffer) r1
            long r6 = r1.u()
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x001f
            goto L_0x000d
        L_0x001f:
            long r4 = r19.u()
            r6 = 0
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 != 0) goto L_0x002b
            goto L_0x008f
        L_0x002b:
            okio.Segment r4 = r0.f4066a
            r5 = 0
            if (r4 == 0) goto L_0x0094
            okio.Segment r1 = r1.f4066a
            if (r1 == 0) goto L_0x0090
            int r8 = r4.b
            int r9 = r1.b
            r10 = r1
            r1 = r8
            r11 = r9
            r8 = r6
        L_0x003c:
            long r12 = r19.u()
            int r14 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r14 >= 0) goto L_0x008f
            int r12 = r4.c
            int r12 = r12 - r1
            int r13 = r10.c
            int r13 = r13 - r11
            int r12 = java.lang.Math.min(r12, r13)
            long r12 = (long) r12
            r14 = r6
        L_0x0050:
            int r16 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r16 >= 0) goto L_0x006b
            byte[] r2 = r4.f4083a
            int r17 = r1 + 1
            byte r1 = r2[r1]
            byte[] r2 = r10.f4083a
            int r18 = r11 + 1
            byte r2 = r2[r11]
            if (r1 == r2) goto L_0x0063
            goto L_0x000d
        L_0x0063:
            r1 = 1
            long r14 = r14 + r1
            r1 = r17
            r11 = r18
            goto L_0x0050
        L_0x006b:
            int r2 = r4.c
            if (r1 != r2) goto L_0x007c
            okio.Segment r1 = r4.f
            if (r1 == 0) goto L_0x0078
            int r2 = r1.b
            r4 = r1
            r1 = r2
            goto L_0x007c
        L_0x0078:
            kotlin.jvm.internal.Intrinsics.a()
            throw r5
        L_0x007c:
            int r2 = r10.c
            if (r11 != r2) goto L_0x008d
            okio.Segment r2 = r10.f
            if (r2 == 0) goto L_0x0089
            int r10 = r2.b
            r11 = r10
            r10 = r2
            goto L_0x008d
        L_0x0089:
            kotlin.jvm.internal.Intrinsics.a()
            throw r5
        L_0x008d:
            long r8 = r8 + r12
            goto L_0x003c
        L_0x008f:
            return r3
        L_0x0090:
            kotlin.jvm.internal.Intrinsics.a()
            throw r5
        L_0x0094:
            kotlin.jvm.internal.Intrinsics.a()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.equals(java.lang.Object):boolean");
    }

    public void flush() {
    }

    public ByteString g() {
        return d(u());
    }

    public String h() {
        return a(this.b, Charsets.f6950a);
    }

    public int hashCode() {
        Segment segment = this.f4066a;
        if (segment == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = segment.c;
            for (int i3 = segment.b; i3 < i2; i3++) {
                i = (i * 31) + segment.f4083a[i3];
            }
            segment = segment.f;
            if (segment == null) {
                Intrinsics.a();
                throw null;
            }
        } while (segment != this.f4066a);
        return i;
    }

    public String i(long j) throws EOFException {
        return a(j, Charsets.f6950a);
    }

    public boolean isOpen() {
        return true;
    }

    public Buffer j() {
        return this;
    }

    public final void j(long j) {
        this.b = j;
    }

    public Buffer k() {
        return this;
    }

    public Buffer l() {
        return this;
    }

    public OutputStream m() {
        return new Buffer$outputStream$1(this);
    }

    public final void o() {
        skip(u());
    }

    public final long p() {
        long u = u();
        if (u == 0) {
            return 0;
        }
        Segment segment = this.f4066a;
        if (segment != null) {
            Segment segment2 = segment.g;
            if (segment2 != null) {
                int i = segment2.c;
                if (i < 8192 && segment2.e) {
                    u -= (long) (i - segment2.b);
                }
                return u;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a();
        throw null;
    }

    public BufferedSource peek() {
        return Okio.a((Source) new PeekSource(this));
    }

    public final Buffer q() {
        Buffer buffer = new Buffer();
        if (u() != 0) {
            Segment segment = this.f4066a;
            if (segment != null) {
                Segment c = segment.c();
                buffer.f4066a = c;
                c.g = buffer.f4066a;
                c.f = c.g;
                Segment segment2 = segment.f;
                while (segment2 != segment) {
                    Segment segment3 = c.g;
                    if (segment3 == null) {
                        Intrinsics.a();
                        throw null;
                    } else if (segment2 != null) {
                        segment3.a(segment2.c());
                        segment2 = segment2.f;
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                buffer.j(u());
            } else {
                Intrinsics.a();
                throw null;
            }
        }
        return buffer;
    }

    public int r() throws EOFException {
        return Util.a(readInt());
    }

    public int read(ByteBuffer byteBuffer) throws IOException {
        Intrinsics.b(byteBuffer, "sink");
        Segment segment = this.f4066a;
        if (segment == null) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), segment.c - segment.b);
        byteBuffer.put(segment.f4083a, segment.b, min);
        segment.b += min;
        this.b -= (long) min;
        if (segment.b == segment.c) {
            this.f4066a = segment.b();
            SegmentPool.c.a(segment);
        }
        return min;
    }

    public byte readByte() throws EOFException {
        if (u() != 0) {
            Segment segment = this.f4066a;
            if (segment != null) {
                int i = segment.b;
                int i2 = segment.c;
                int i3 = i + 1;
                byte b2 = segment.f4083a[i];
                j(u() - 1);
                if (i3 == i2) {
                    this.f4066a = segment.b();
                    SegmentPool.c.a(segment);
                } else {
                    segment.b = i3;
                }
                return b2;
            }
            Intrinsics.a();
            throw null;
        }
        throw new EOFException();
    }

    public void readFully(byte[] bArr) throws EOFException {
        Intrinsics.b(bArr, "sink");
        int i = 0;
        while (i < bArr.length) {
            int a2 = a(bArr, i, bArr.length - i);
            if (a2 != -1) {
                i += a2;
            } else {
                throw new EOFException();
            }
        }
    }

    public int readInt() throws EOFException {
        if (u() >= 4) {
            Segment segment = this.f4066a;
            if (segment != null) {
                int i = segment.b;
                int i2 = segment.c;
                if (((long) (i2 - i)) < 4) {
                    return ((readByte() & 255) << 24) | ((readByte() & 255) << 16) | ((readByte() & 255) << 8) | (readByte() & 255);
                }
                byte[] bArr = segment.f4083a;
                int i3 = i + 1;
                int i4 = i3 + 1;
                byte b2 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
                int i5 = i4 + 1;
                byte b3 = b2 | ((bArr[i4] & 255) << 8);
                int i6 = i5 + 1;
                byte b4 = b3 | (bArr[i5] & 255);
                j(u() - 4);
                if (i6 == i2) {
                    this.f4066a = segment.b();
                    SegmentPool.c.a(segment);
                } else {
                    segment.b = i6;
                }
                return b4;
            }
            Intrinsics.a();
            throw null;
        }
        throw new EOFException();
    }

    public long readLong() throws EOFException {
        if (u() >= 8) {
            Segment segment = this.f4066a;
            if (segment != null) {
                int i = segment.b;
                int i2 = segment.c;
                if (((long) (i2 - i)) < 8) {
                    return ((((long) readInt()) & 4294967295L) << 32) | (4294967295L & ((long) readInt()));
                }
                byte[] bArr = segment.f4083a;
                int i3 = i + 1;
                int i4 = i3 + 1;
                int i5 = i4 + 1;
                long j = ((((long) bArr[i]) & 255) << 56) | ((((long) bArr[i3]) & 255) << 48) | ((((long) bArr[i4]) & 255) << 40);
                int i6 = i5 + 1;
                long j2 = ((((long) bArr[i5]) & 255) << 32) | j;
                int i7 = i6 + 1;
                int i8 = i7 + 1;
                long j3 = j2 | ((((long) bArr[i6]) & 255) << 24) | ((((long) bArr[i7]) & 255) << 16);
                int i9 = i8 + 1;
                int i10 = i9 + 1;
                long j4 = j3 | ((((long) bArr[i8]) & 255) << 8) | (((long) bArr[i9]) & 255);
                j(u() - 8);
                if (i10 == i2) {
                    this.f4066a = segment.b();
                    SegmentPool.c.a(segment);
                } else {
                    segment.b = i10;
                }
                return j4;
            }
            Intrinsics.a();
            throw null;
        }
        throw new EOFException();
    }

    public short readShort() throws EOFException {
        if (u() >= 2) {
            Segment segment = this.f4066a;
            if (segment != null) {
                int i = segment.b;
                int i2 = segment.c;
                if (i2 - i < 2) {
                    return (short) (((readByte() & 255) << 8) | (readByte() & 255));
                }
                byte[] bArr = segment.f4083a;
                int i3 = i + 1;
                int i4 = i3 + 1;
                byte b2 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
                j(u() - 2);
                if (i4 == i2) {
                    this.f4066a = segment.b();
                    SegmentPool.c.a(segment);
                } else {
                    segment.b = i4;
                }
                return (short) b2;
            }
            Intrinsics.a();
            throw null;
        }
        throw new EOFException();
    }

    public boolean request(long j) {
        return this.b >= j;
    }

    public short s() throws EOFException {
        return Util.a(readShort());
    }

    public void skip(long j) throws EOFException {
        while (j > 0) {
            Segment segment = this.f4066a;
            if (segment != null) {
                int min = (int) Math.min(j, (long) (segment.c - segment.b));
                long j2 = (long) min;
                j(u() - j2);
                j -= j2;
                segment.b += min;
                if (segment.b == segment.c) {
                    this.f4066a = segment.b();
                    SegmentPool.c.a(segment);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    public int t() throws EOFException {
        byte b2;
        int i;
        byte b3;
        if (u() != 0) {
            byte h = h(0);
            int i2 = 1;
            if ((h & 128) == 0) {
                b3 = h & Byte.MAX_VALUE;
                i = 1;
                b2 = 0;
            } else if ((h & 224) == 192) {
                b3 = h & 31;
                i = 2;
                b2 = 128;
            } else if ((h & 240) == 224) {
                b3 = h & 15;
                i = 3;
                b2 = 2048;
            } else if ((h & 248) == 240) {
                b3 = h & 7;
                i = 4;
                b2 = 65536;
            } else {
                skip(1);
                return 65533;
            }
            long j = (long) i;
            if (u() >= j) {
                while (i2 < i) {
                    long j2 = (long) i2;
                    byte h2 = h(j2);
                    if ((h2 & 192) == 128) {
                        b3 = (b3 << 6) | (h2 & 63);
                        i2++;
                    } else {
                        skip(j2);
                        return 65533;
                    }
                }
                skip(j);
                if (b3 > 1114111) {
                    return 65533;
                }
                if ((55296 <= b3 && 57343 >= b3) || b3 < b2) {
                    return 65533;
                }
                return b3;
            }
            throw new EOFException("size < " + i + ": " + u() + " (to read code point prefixed 0x" + Util.a(h) + ')');
        }
        throw new EOFException();
    }

    public Timeout timeout() {
        return Timeout.NONE;
    }

    public String toString() {
        return v().toString();
    }

    public final long u() {
        return this.b;
    }

    public final ByteString v() {
        if (u() <= ((long) Integer.MAX_VALUE)) {
            return a((int) u());
        }
        throw new IllegalStateException(("size > Int.MAX_VALUE: " + u()).toString());
    }

    public long b(ByteString byteString) throws IOException {
        Intrinsics.b(byteString, "bytes");
        return a(byteString, 0);
    }

    public String c() throws EOFException {
        return a((long) Clock.MAX_TIME);
    }

    public Buffer clone() {
        return q();
    }

    public boolean e() {
        return this.b == 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00aa, code lost:
        if (r10 != r11) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ac, code lost:
        r0.f4066a = r15.b();
        okio.SegmentPool.c.a(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b9, code lost:
        r15.b = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00bc, code lost:
        if (r2 != false) goto L_0x00c2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long f() throws java.io.EOFException {
        /*
            r17 = this;
            r0 = r17
            long r1 = r17.u()
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x00d5
            r1 = -7
            r5 = 0
            r6 = r1
            r1 = 0
            r2 = 0
        L_0x0012:
            okio.Segment r8 = r0.f4066a
            if (r8 == 0) goto L_0x00d0
            byte[] r9 = r8.f4083a
            int r10 = r8.b
            int r11 = r8.c
        L_0x001c:
            r12 = 1
            if (r10 >= r11) goto L_0x00a9
            byte r13 = r9[r10]
            r14 = 48
            byte r14 = (byte) r14
            if (r13 < r14) goto L_0x0074
            r15 = 57
            byte r15 = (byte) r15
            if (r13 > r15) goto L_0x0074
            int r14 = r14 - r13
            r15 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            int r12 = (r3 > r15 ? 1 : (r3 == r15 ? 0 : -1))
            if (r12 < 0) goto L_0x0047
            r15 = r8
            r16 = r9
            if (r12 != 0) goto L_0x0040
            long r8 = (long) r14
            int r12 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r12 >= 0) goto L_0x0040
            goto L_0x0047
        L_0x0040:
            r8 = 10
            long r3 = r3 * r8
            long r8 = (long) r14
            long r3 = r3 + r8
            goto L_0x0082
        L_0x0047:
            okio.Buffer r2 = new okio.Buffer
            r2.<init>()
            okio.Buffer r2 = r2.e((long) r3)
            okio.Buffer r2 = r2.writeByte((int) r13)
            if (r1 != 0) goto L_0x0059
            r2.readByte()
        L_0x0059:
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Number too large: "
            r3.append(r4)
            java.lang.String r2 = r2.h()
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            r1.<init>(r2)
            throw r1
        L_0x0074:
            r15 = r8
            r16 = r9
            r8 = 45
            byte r8 = (byte) r8
            if (r13 != r8) goto L_0x008a
            if (r5 != 0) goto L_0x008a
            r8 = 1
            long r6 = r6 - r8
            r1 = 1
        L_0x0082:
            int r10 = r10 + 1
            int r5 = r5 + 1
            r8 = r15
            r9 = r16
            goto L_0x001c
        L_0x008a:
            if (r5 == 0) goto L_0x008e
            r2 = 1
            goto L_0x00aa
        L_0x008e:
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Expected leading [0-9] or '-' character but was 0x"
            r2.append(r3)
            java.lang.String r3 = okio.Util.a((byte) r13)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x00a9:
            r15 = r8
        L_0x00aa:
            if (r10 != r11) goto L_0x00b9
            okio.Segment r8 = r15.b()
            r0.f4066a = r8
            okio.SegmentPool r8 = okio.SegmentPool.c
            r9 = r15
            r8.a(r9)
            goto L_0x00bc
        L_0x00b9:
            r9 = r15
            r9.b = r10
        L_0x00bc:
            if (r2 != 0) goto L_0x00c2
            okio.Segment r8 = r0.f4066a
            if (r8 != 0) goto L_0x0012
        L_0x00c2:
            long r6 = r17.u()
            long r8 = (long) r5
            long r6 = r6 - r8
            r0.j(r6)
            if (r1 == 0) goto L_0x00ce
            goto L_0x00cf
        L_0x00ce:
            long r3 = -r3
        L_0x00cf:
            return r3
        L_0x00d0:
            kotlin.jvm.internal.Intrinsics.a()
            r1 = 0
            throw r1
        L_0x00d5:
            java.io.EOFException r1 = new java.io.EOFException
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.f():long");
    }

    public final byte h(long j) {
        Util.a(u(), j, 1);
        Segment segment = this.f4066a;
        if (segment == null) {
            Intrinsics.a();
            throw null;
        } else if (u() - j < j) {
            long u = u();
            while (u > j) {
                segment = segment.g;
                if (segment != null) {
                    u -= (long) (segment.c - segment.b);
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
            if (segment != null) {
                return segment.f4083a[(int) ((((long) segment.b) + j) - u)];
            }
            Intrinsics.a();
            throw null;
        } else {
            long j2 = 0;
            while (true) {
                int i = segment.c;
                int i2 = segment.b;
                long j3 = ((long) (i - i2)) + j2;
                if (j3 <= j) {
                    segment = segment.f;
                    if (segment != null) {
                        j2 = j3;
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                } else if (segment != null) {
                    return segment.f4083a[(int) ((((long) i2) + j) - j2)];
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0099, code lost:
        if (r8 != r9) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009b, code lost:
        r15.f4066a = r6.b();
        okio.SegmentPool.c.a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a7, code lost:
        r6.b = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a9, code lost:
        if (r1 != false) goto L_0x00af;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x007e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long i() throws java.io.EOFException {
        /*
            r15 = this;
            long r0 = r15.u()
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x00be
            r0 = 0
            r4 = r2
            r1 = 0
        L_0x000d:
            okio.Segment r6 = r15.f4066a
            if (r6 == 0) goto L_0x00b9
            byte[] r7 = r6.f4083a
            int r8 = r6.b
            int r9 = r6.c
        L_0x0017:
            if (r8 >= r9) goto L_0x0099
            byte r10 = r7[r8]
            r11 = 48
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0028
            r12 = 57
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0028
            int r11 = r10 - r11
            goto L_0x0042
        L_0x0028:
            r11 = 97
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0037
            r12 = 102(0x66, float:1.43E-43)
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0037
        L_0x0032:
            int r11 = r10 - r11
            int r11 = r11 + 10
            goto L_0x0042
        L_0x0037:
            r11 = 65
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x007a
            r12 = 70
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x007a
            goto L_0x0032
        L_0x0042:
            r12 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r12 = r12 & r4
            int r14 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r14 != 0) goto L_0x0052
            r10 = 4
            long r4 = r4 << r10
            long r10 = (long) r11
            long r4 = r4 | r10
            int r8 = r8 + 1
            int r0 = r0 + 1
            goto L_0x0017
        L_0x0052:
            okio.Buffer r0 = new okio.Buffer
            r0.<init>()
            okio.Buffer r0 = r0.f((long) r4)
            okio.Buffer r0 = r0.writeByte((int) r10)
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Number too large: "
            r2.append(r3)
            java.lang.String r0 = r0.h()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L_0x007a:
            if (r0 == 0) goto L_0x007e
            r1 = 1
            goto L_0x0099
        L_0x007e:
            java.lang.NumberFormatException r0 = new java.lang.NumberFormatException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Expected leading [0-9a-fA-F] character but was 0x"
            r1.append(r2)
            java.lang.String r2 = okio.Util.a((byte) r10)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0099:
            if (r8 != r9) goto L_0x00a7
            okio.Segment r7 = r6.b()
            r15.f4066a = r7
            okio.SegmentPool r7 = okio.SegmentPool.c
            r7.a(r6)
            goto L_0x00a9
        L_0x00a7:
            r6.b = r8
        L_0x00a9:
            if (r1 != 0) goto L_0x00af
            okio.Segment r6 = r15.f4066a
            if (r6 != 0) goto L_0x000d
        L_0x00af:
            long r1 = r15.u()
            long r6 = (long) r0
            long r1 = r1 - r6
            r15.j(r1)
            return r4
        L_0x00b9:
            kotlin.jvm.internal.Intrinsics.a()
            r0 = 0
            throw r0
        L_0x00be:
            java.io.EOFException r0 = new java.io.EOFException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.i():long");
    }

    public Buffer k(long j) {
        Segment b2 = b(8);
        byte[] bArr = b2.f4083a;
        int i = b2.c;
        int i2 = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 56) & 255));
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((int) ((j >>> 48) & 255));
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((int) ((j >>> 40) & 255));
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((int) ((j >>> 32) & 255));
        int i6 = i5 + 1;
        bArr[i5] = (byte) ((int) ((j >>> 24) & 255));
        int i7 = i6 + 1;
        bArr[i6] = (byte) ((int) ((j >>> 16) & 255));
        int i8 = i7 + 1;
        bArr[i7] = (byte) ((int) ((j >>> 8) & 255));
        bArr[i8] = (byte) ((int) (j & 255));
        b2.c = i8 + 1;
        j(u() + 8);
        return this;
    }

    public Buffer writeByte(int i) {
        Segment b2 = b(1);
        byte[] bArr = b2.f4083a;
        int i2 = b2.c;
        b2.c = i2 + 1;
        bArr[i2] = (byte) i;
        j(u() + 1);
        return this;
    }

    public Buffer writeInt(int i) {
        Segment b2 = b(4);
        byte[] bArr = b2.f4083a;
        int i2 = b2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & JfifUtil.MARKER_FIRST_BYTE);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & JfifUtil.MARKER_FIRST_BYTE);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & JfifUtil.MARKER_FIRST_BYTE);
        bArr[i5] = (byte) (i & JfifUtil.MARKER_FIRST_BYTE);
        b2.c = i5 + 1;
        j(u() + 4);
        return this;
    }

    public Buffer writeShort(int i) {
        Segment b2 = b(2);
        byte[] bArr = b2.f4083a;
        int i2 = b2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & JfifUtil.MARKER_FIRST_BYTE);
        bArr[i3] = (byte) (i & JfifUtil.MARKER_FIRST_BYTE);
        b2.c = i3 + 1;
        j(u() + 2);
        return this;
    }

    public String a(Charset charset) {
        Intrinsics.b(charset, "charset");
        return a(this.b, charset);
    }

    public byte[] b(long j) throws EOFException {
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (u() >= j) {
            byte[] bArr = new byte[((int) j)];
            readFully(bArr);
            return bArr;
        } else {
            throw new EOFException();
        }
    }

    public Buffer c(int i) {
        return writeInt(Util.a(i));
    }

    public Buffer e(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i == 0) {
            return writeByte(48);
        }
        boolean z = false;
        int i2 = 1;
        if (i < 0) {
            j = -j;
            if (j < 0) {
                return a("-9223372036854775808");
            }
            z = true;
        }
        if (j >= 100000000) {
            i2 = j < 1000000000000L ? j < RealConnection.IDLE_CONNECTION_HEALTHY_NS ? j < 1000000000 ? 9 : 10 : j < 100000000000L ? 11 : 12 : j < 1000000000000000L ? j < 10000000000000L ? 13 : j < 100000000000000L ? 14 : 15 : j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
        } else if (j >= 10000) {
            i2 = j < 1000000 ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8;
        } else if (j >= 100) {
            i2 = j < 1000 ? 3 : 4;
        } else if (j >= 10) {
            i2 = 2;
        }
        if (z) {
            i2++;
        }
        Segment b2 = b(i2);
        byte[] bArr = b2.f4083a;
        int i3 = b2.c + i2;
        while (j != 0) {
            long j2 = (long) 10;
            i3--;
            bArr[i3] = BufferKt.a()[(int) (j % j2)];
            j /= j2;
        }
        if (z) {
            bArr[i3 - 1] = (byte) 45;
        }
        b2.c += i2;
        j(u() + ((long) i2));
        return this;
    }

    public int write(ByteBuffer byteBuffer) throws IOException {
        Intrinsics.b(byteBuffer, "source");
        int remaining = byteBuffer.remaining();
        int i = remaining;
        while (i > 0) {
            Segment b2 = b(1);
            int min = Math.min(i, 8192 - b2.c);
            byteBuffer.get(b2.f4083a, b2.c, min);
            i -= min;
            b2.c += min;
        }
        this.b += (long) remaining;
        return remaining;
    }

    public String a(long j, Charset charset) throws EOFException {
        Intrinsics.b(charset, "charset");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (!(i >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (this.b < j) {
            throw new EOFException();
        } else if (i == 0) {
            return "";
        } else {
            Segment segment = this.f4066a;
            if (segment != null) {
                int i2 = segment.b;
                if (((long) i2) + j > ((long) segment.c)) {
                    return new String(b(j), charset);
                }
                int i3 = (int) j;
                String str = new String(segment.f4083a, i2, i3, charset);
                segment.b += i3;
                this.b -= j;
                if (segment.b == segment.c) {
                    this.f4066a = segment.b();
                    SegmentPool.c.a(segment);
                }
                return str;
            }
            Intrinsics.a();
            throw null;
        }
    }

    public long c(ByteString byteString) {
        Intrinsics.b(byteString, "targetBytes");
        return b(byteString, 0);
    }

    public byte[] d() {
        return b(u());
    }

    public Buffer d(int i) {
        if (i < 128) {
            writeByte(i);
        } else if (i < 2048) {
            Segment b2 = b(2);
            byte[] bArr = b2.f4083a;
            int i2 = b2.c;
            bArr[i2] = (byte) ((i >> 6) | JfifUtil.MARKER_SOFn);
            bArr[i2 + 1] = (byte) ((i & 63) | 128);
            b2.c = i2 + 2;
            j(u() + 2);
        } else if (55296 <= i && 57343 >= i) {
            writeByte(63);
        } else if (i < 65536) {
            Segment b3 = b(3);
            byte[] bArr2 = b3.f4083a;
            int i3 = b3.c;
            bArr2[i3] = (byte) ((i >> 12) | 224);
            bArr2[i3 + 1] = (byte) (((i >> 6) & 63) | 128);
            bArr2[i3 + 2] = (byte) ((i & 63) | 128);
            b3.c = i3 + 3;
            j(u() + 3);
        } else if (i <= 1114111) {
            Segment b4 = b(4);
            byte[] bArr3 = b4.f4083a;
            int i4 = b4.c;
            bArr3[i4] = (byte) ((i >> 18) | 240);
            bArr3[i4 + 1] = (byte) (((i >> 12) & 63) | 128);
            bArr3[i4 + 2] = (byte) (((i >> 6) & 63) | 128);
            bArr3[i4 + 3] = (byte) ((i & 63) | 128);
            b4.c = i4 + 4;
            j(u() + 4);
        } else {
            throw new IllegalArgumentException("Unexpected code point: 0x" + Util.b(i));
        }
        return this;
    }

    public final Segment b(int i) {
        boolean z = true;
        if (i < 1 || i > 8192) {
            z = false;
        }
        if (z) {
            Segment segment = this.f4066a;
            if (segment == null) {
                Segment a2 = SegmentPool.c.a();
                this.f4066a = a2;
                a2.g = a2;
                a2.f = a2;
                return a2;
            } else if (segment != null) {
                Segment segment2 = segment.g;
                if (segment2 == null) {
                    Intrinsics.a();
                    throw null;
                } else if (segment2.c + i > 8192 || !segment2.e) {
                    return segment2.a(SegmentPool.c.a());
                } else {
                    return segment2;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        } else {
            throw new IllegalArgumentException("unexpected capacity".toString());
        }
    }

    public long read(Buffer buffer, long j) {
        Intrinsics.b(buffer, "sink");
        if (!(j >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (u() == 0) {
            return -1;
        } else {
            if (j > u()) {
                j = u();
            }
            buffer.write(this, j);
            return j;
        }
    }

    public Buffer write(byte[] bArr) {
        Intrinsics.b(bArr, "source");
        return write(bArr, 0, bArr.length);
    }

    public Buffer write(byte[] bArr, int i, int i2) {
        Intrinsics.b(bArr, "source");
        long j = (long) i2;
        Util.a((long) bArr.length, (long) i, j);
        int i3 = i2 + i;
        while (i < i3) {
            Segment b2 = b(1);
            int min = Math.min(i3 - i, 8192 - b2.c);
            int i4 = i + min;
            byte[] unused = ArraysKt___ArraysJvmKt.a(bArr, b2.f4083a, b2.c, i, i4);
            b2.c += min;
            i = i4;
        }
        j(u() + j);
        return this;
    }

    public Buffer a(String str) {
        Intrinsics.b(str, "string");
        return a(str, 0, str.length());
    }

    public Buffer a(String str, Charset charset) {
        Intrinsics.b(str, "string");
        Intrinsics.b(charset, "charset");
        return a(str, 0, str.length(), charset);
    }

    public void write(Buffer buffer, long j) {
        Segment segment;
        Intrinsics.b(buffer, "source");
        if (buffer != this) {
            Util.a(buffer.u(), 0, j);
            while (j > 0) {
                Segment segment2 = buffer.f4066a;
                if (segment2 != null) {
                    int i = segment2.c;
                    if (segment2 != null) {
                        if (j < ((long) (i - segment2.b))) {
                            Segment segment3 = this.f4066a;
                            if (segment3 == null) {
                                segment = null;
                            } else if (segment3 != null) {
                                segment = segment3.g;
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                            if (segment != null && segment.e) {
                                if ((((long) segment.c) + j) - ((long) (segment.d ? 0 : segment.b)) <= ((long) 8192)) {
                                    Segment segment4 = buffer.f4066a;
                                    if (segment4 != null) {
                                        segment4.a(segment, (int) j);
                                        buffer.j(buffer.u() - j);
                                        j(u() + j);
                                        return;
                                    }
                                    Intrinsics.a();
                                    throw null;
                                }
                            }
                            Segment segment5 = buffer.f4066a;
                            if (segment5 != null) {
                                buffer.f4066a = segment5.a((int) j);
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                        Segment segment6 = buffer.f4066a;
                        if (segment6 != null) {
                            long j2 = (long) (segment6.c - segment6.b);
                            buffer.f4066a = segment6.b();
                            Segment segment7 = this.f4066a;
                            if (segment7 == null) {
                                this.f4066a = segment6;
                                segment6.g = segment6;
                                segment6.f = segment6.g;
                            } else if (segment7 != null) {
                                Segment segment8 = segment7.g;
                                if (segment8 != null) {
                                    segment8.a(segment6).a();
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                            buffer.j(buffer.u() - j2);
                            j(u() + j2);
                            j -= j2;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
            return;
        }
        throw new IllegalArgumentException("source == this".toString());
    }

    public Buffer a(String str, int i, int i2, Charset charset) {
        Intrinsics.b(str, "string");
        Intrinsics.b(charset, "charset");
        boolean z = true;
        if (i >= 0) {
            if (i2 >= i) {
                if (i2 > str.length()) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
                } else if (Intrinsics.a((Object) charset, (Object) Charsets.f6950a)) {
                    return a(str, i, i2);
                } else {
                    String substring = str.substring(i, i2);
                    Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    if (substring != null) {
                        byte[] bytes = substring.getBytes(charset);
                        Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                        return write(bytes, 0, bytes.length);
                    }
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
            }
        } else {
            throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
        }
    }

    public long b(ByteString byteString, long j) {
        long j2;
        int i;
        int i2;
        int i3;
        Intrinsics.b(byteString, "targetBytes");
        long j3 = 0;
        if (j >= 0) {
            Segment segment = this.f4066a;
            if (segment == null) {
                return -1;
            }
            if (u() - j < j) {
                j2 = u();
                while (j2 > j) {
                    segment = segment.g;
                    if (segment != null) {
                        j2 -= (long) (segment.c - segment.b);
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                if (segment == null) {
                    return -1;
                }
                if (byteString.size() == 2) {
                    byte a2 = byteString.a(0);
                    byte a3 = byteString.a(1);
                    while (j2 < u()) {
                        byte[] bArr = segment.f4083a;
                        i2 = (int) ((((long) segment.b) + j) - j2);
                        int i4 = segment.c;
                        while (i2 < i4) {
                            byte b2 = bArr[i2];
                            if (!(b2 == a2 || b2 == a3)) {
                                i2++;
                            }
                        }
                        j = ((long) (segment.c - segment.b)) + j2;
                        segment = segment.f;
                        if (segment != null) {
                            j2 = j;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                    return -1;
                }
                byte[] g = byteString.g();
                while (j2 < u()) {
                    byte[] bArr2 = segment.f4083a;
                    i = (int) ((((long) segment.b) + j) - j2);
                    int i5 = segment.c;
                    while (i < i5) {
                        byte b3 = bArr2[i];
                        for (byte b4 : g) {
                            if (b3 == b4) {
                                i3 = segment.b;
                                return ((long) (i2 - i3)) + j2;
                            }
                        }
                        i++;
                    }
                    j = ((long) (segment.c - segment.b)) + j2;
                    segment = segment.f;
                    if (segment != null) {
                        j2 = j;
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                return -1;
            }
            while (true) {
                long j4 = ((long) (segment.c - segment.b)) + j3;
                if (j4 <= j) {
                    segment = segment.f;
                    if (segment != null) {
                        j3 = j4;
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                } else if (segment == null) {
                    return -1;
                } else {
                    if (byteString.size() == 2) {
                        byte a4 = byteString.a(0);
                        byte a5 = byteString.a(1);
                        while (j2 < u()) {
                            byte[] bArr3 = segment.f4083a;
                            int i6 = (int) ((((long) segment.b) + j) - j2);
                            int i7 = segment.c;
                            while (i2 < i7) {
                                byte b5 = bArr3[i2];
                                if (!(b5 == a4 || b5 == a5)) {
                                    i6 = i2 + 1;
                                }
                            }
                            j = ((long) (segment.c - segment.b)) + j2;
                            segment = segment.f;
                            if (segment != null) {
                                j3 = j;
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                        return -1;
                    }
                    byte[] g2 = byteString.g();
                    while (j2 < u()) {
                        byte[] bArr4 = segment.f4083a;
                        int i8 = (int) ((((long) segment.b) + j) - j2);
                        int i9 = segment.c;
                        while (i < i9) {
                            byte b6 = bArr4[i];
                            int length = g2.length;
                            int i10 = 0;
                            while (i10 < length) {
                                if (b6 != g2[i10]) {
                                    i10++;
                                }
                            }
                            i8 = i + 1;
                        }
                        j = ((long) (segment.c - segment.b)) + j2;
                        segment = segment.f;
                        if (segment != null) {
                            j3 = j;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                    return -1;
                }
            }
            i3 = segment.b;
            return ((long) (i2 - i3)) + j2;
            i3 = segment.b;
            return ((long) (i2 - i3)) + j2;
        }
        throw new IllegalArgumentException(("fromIndex < 0: " + j).toString());
    }

    public Buffer f(long j) {
        if (j == 0) {
            return writeByte(48);
        }
        long j2 = (j >>> 1) | j;
        long j3 = j2 | (j2 >>> 2);
        long j4 = j3 | (j3 >>> 4);
        long j5 = j4 | (j4 >>> 8);
        long j6 = j5 | (j5 >>> 16);
        long j7 = j6 | (j6 >>> 32);
        long j8 = j7 - ((j7 >>> 1) & 6148914691236517205L);
        long j9 = ((j8 >>> 2) & 3689348814741910323L) + (j8 & 3689348814741910323L);
        long j10 = ((j9 >>> 4) + j9) & 1085102592571150095L;
        long j11 = j10 + (j10 >>> 8);
        long j12 = j11 + (j11 >>> 16);
        int i = (int) ((((j12 & 63) + ((j12 >>> 32) & 63)) + ((long) 3)) / ((long) 4));
        Segment b2 = b(i);
        byte[] bArr = b2.f4083a;
        int i2 = b2.c;
        for (int i3 = (i2 + i) - 1; i3 >= i2; i3--) {
            bArr[i3] = BufferKt.a()[(int) (15 & j)];
            j >>>= 4;
        }
        b2.c += i;
        j(u() + ((long) i));
        return this;
    }

    public boolean a(long j, ByteString byteString) {
        Intrinsics.b(byteString, "bytes");
        return a(j, byteString, 0, byteString.size());
    }

    public static /* synthetic */ UnsafeCursor a(Buffer buffer, UnsafeCursor unsafeCursor, int i, Object obj) {
        if ((i & 1) != 0) {
            unsafeCursor = new UnsafeCursor();
        }
        return buffer.a(unsafeCursor);
    }

    public final UnsafeCursor a(UnsafeCursor unsafeCursor) {
        Intrinsics.b(unsafeCursor, "unsafeCursor");
        if (unsafeCursor.f4067a == null) {
            unsafeCursor.f4067a = this;
            unsafeCursor.b = true;
            return unsafeCursor;
        }
        throw new IllegalStateException("already attached to a buffer".toString());
    }

    public final Buffer a(Buffer buffer, long j, long j2) {
        Intrinsics.b(buffer, "out");
        Util.a(u(), j, j2);
        if (j2 != 0) {
            buffer.j(buffer.u() + j2);
            Segment segment = this.f4066a;
            while (segment != null) {
                int i = segment.c;
                int i2 = segment.b;
                if (j >= ((long) (i - i2))) {
                    j -= (long) (i - i2);
                    segment = segment.f;
                } else {
                    while (j2 > 0) {
                        if (segment != null) {
                            Segment c = segment.c();
                            c.b += (int) j;
                            c.c = Math.min(c.b + ((int) j2), c.c);
                            Segment segment2 = buffer.f4066a;
                            if (segment2 == null) {
                                c.g = c;
                                c.f = c.g;
                                buffer.f4066a = c.f;
                            } else if (segment2 != null) {
                                Segment segment3 = segment2.g;
                                if (segment3 != null) {
                                    segment3.a(c);
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                            j2 -= (long) (c.c - c.b);
                            segment = segment.f;
                            j = 0;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                }
            }
            Intrinsics.a();
            throw null;
        }
        return this;
    }

    public int a(Options options) {
        Intrinsics.b(options, "options");
        int a2 = BufferKt.a(this, options, false, 2, (Object) null);
        if (a2 == -1) {
            return -1;
        }
        skip((long) options.b()[a2].size());
        return a2;
    }

    public void a(Buffer buffer, long j) throws EOFException {
        Intrinsics.b(buffer, "sink");
        if (u() >= j) {
            buffer.write(this, j);
        } else {
            buffer.write(this, u());
            throw new EOFException();
        }
    }

    public long a(Sink sink) throws IOException {
        Intrinsics.b(sink, "sink");
        long u = u();
        if (u > 0) {
            sink.write(this, u);
        }
        return u;
    }

    public String a(long j) throws EOFException {
        if (j >= 0) {
            long j2 = Clock.MAX_TIME;
            if (j != Clock.MAX_TIME) {
                j2 = j + 1;
            }
            byte b2 = (byte) 10;
            long a2 = a(b2, 0, j2);
            if (a2 != -1) {
                return BufferKt.a(this, a2);
            }
            if (j2 < u() && h(j2 - 1) == ((byte) 13) && h(j2) == b2) {
                return BufferKt.a(this, j2);
            }
            Buffer buffer = new Buffer();
            a(buffer, 0, Math.min((long) 32, u()));
            throw new EOFException("\\n not found: limit=" + Math.min(u(), j) + " content=" + buffer.g().f() + 8230);
        }
        throw new IllegalArgumentException(("limit < 0: " + j).toString());
    }

    public int a(byte[] bArr, int i, int i2) {
        Intrinsics.b(bArr, "sink");
        Util.a((long) bArr.length, (long) i, (long) i2);
        Segment segment = this.f4066a;
        if (segment == null) {
            return -1;
        }
        int min = Math.min(i2, segment.c - segment.b);
        byte[] bArr2 = segment.f4083a;
        int i3 = segment.b;
        byte[] unused = ArraysKt___ArraysJvmKt.a(bArr2, bArr, i, i3, i3 + min);
        segment.b += min;
        j(u() - ((long) min));
        if (segment.b != segment.c) {
            return min;
        }
        this.f4066a = segment.b();
        SegmentPool.c.a(segment);
        return min;
    }

    public Buffer a(ByteString byteString) {
        Intrinsics.b(byteString, "byteString");
        byteString.a(this, 0, byteString.size());
        return this;
    }

    public Buffer a(String str, int i, int i2) {
        Intrinsics.b(str, "string");
        if (i >= 0) {
            if (i2 >= i) {
                if (i2 <= str.length()) {
                    while (i < i2) {
                        char charAt = str.charAt(i);
                        if (charAt < 128) {
                            Segment b2 = b(1);
                            byte[] bArr = b2.f4083a;
                            int i3 = b2.c - i;
                            int min = Math.min(i2, 8192 - i3);
                            int i4 = i + 1;
                            bArr[i + i3] = (byte) charAt;
                            while (i4 < min) {
                                char charAt2 = str.charAt(i4);
                                if (charAt2 >= 128) {
                                    break;
                                }
                                bArr[i4 + i3] = (byte) charAt2;
                                i4++;
                            }
                            int i5 = b2.c;
                            int i6 = (i3 + i4) - i5;
                            b2.c = i5 + i6;
                            j(u() + ((long) i6));
                            i = i4;
                        } else {
                            if (charAt < 2048) {
                                Segment b3 = b(2);
                                byte[] bArr2 = b3.f4083a;
                                int i7 = b3.c;
                                bArr2[i7] = (byte) ((charAt >> 6) | JfifUtil.MARKER_SOFn);
                                bArr2[i7 + 1] = (byte) ((charAt & '?') | 128);
                                b3.c = i7 + 2;
                                j(u() + 2);
                            } else if (charAt < 55296 || charAt > 57343) {
                                Segment b4 = b(3);
                                byte[] bArr3 = b4.f4083a;
                                int i8 = b4.c;
                                bArr3[i8] = (byte) ((charAt >> 12) | 224);
                                bArr3[i8 + 1] = (byte) ((63 & (charAt >> 6)) | 128);
                                bArr3[i8 + 2] = (byte) ((charAt & '?') | 128);
                                b4.c = i8 + 3;
                                j(u() + 3);
                            } else {
                                int i9 = i + 1;
                                char charAt3 = i9 < i2 ? str.charAt(i9) : 0;
                                if (charAt > 56319 || 56320 > charAt3 || 57343 < charAt3) {
                                    writeByte(63);
                                    i = i9;
                                } else {
                                    int i10 = (((charAt & 1023) << 10) | (charAt3 & 1023)) + ImageMetadata.CONTROL_AE_ANTIBANDING_MODE;
                                    Segment b5 = b(4);
                                    byte[] bArr4 = b5.f4083a;
                                    int i11 = b5.c;
                                    bArr4[i11] = (byte) ((i10 >> 18) | 240);
                                    bArr4[i11 + 1] = (byte) (((i10 >> 12) & 63) | 128);
                                    bArr4[i11 + 2] = (byte) (((i10 >> 6) & 63) | 128);
                                    bArr4[i11 + 3] = (byte) ((i10 & 63) | 128);
                                    b5.c = i11 + 4;
                                    j(u() + 4);
                                    i += 2;
                                }
                            }
                            i++;
                        }
                    }
                    return this;
                }
                throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
            }
            throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
        }
        throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
    }

    public long a(Source source) throws IOException {
        Intrinsics.b(source, "source");
        long j = 0;
        while (true) {
            long read = source.read(this, (long) 8192);
            if (read == -1) {
                return j;
            }
            j += read;
        }
    }

    public Buffer a(Source source, long j) throws IOException {
        Intrinsics.b(source, "source");
        while (j > 0) {
            long read = source.read(this, j);
            if (read != -1) {
                j -= read;
            } else {
                throw new EOFException();
            }
        }
        return this;
    }

    public long a(byte b2, long j, long j2) {
        long j3;
        int i;
        byte b3 = b2;
        long j4 = j;
        long j5 = j2;
        long j6 = 0;
        if (0 <= j4 && j5 >= j4) {
            if (j5 > u()) {
                j5 = u();
            }
            if (j4 == j5) {
                return -1;
            }
            Segment segment = this.f4066a;
            if (segment != null) {
                if (u() - j4 < j4) {
                    j3 = u();
                    while (j3 > j4) {
                        segment = segment.g;
                        if (segment != null) {
                            j3 -= (long) (segment.c - segment.b);
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                    if (segment != null) {
                        while (j3 < j5) {
                            byte[] bArr = segment.f4083a;
                            int min = (int) Math.min((long) segment.c, (((long) segment.b) + j5) - j3);
                            i = (int) ((((long) segment.b) + j4) - j3);
                            while (i < min) {
                                if (bArr[i] != b3) {
                                    i++;
                                }
                            }
                            j4 = ((long) (segment.c - segment.b)) + j3;
                            segment = segment.f;
                            if (segment != null) {
                                j3 = j4;
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                    }
                } else {
                    while (true) {
                        long j7 = ((long) (segment.c - segment.b)) + j6;
                        if (j7 <= j4) {
                            segment = segment.f;
                            if (segment != null) {
                                j6 = j7;
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        } else if (segment != null) {
                            while (j3 < j5) {
                                byte[] bArr2 = segment.f4083a;
                                int min2 = (int) Math.min((long) segment.c, (((long) segment.b) + j5) - j3);
                                int i2 = (int) ((((long) segment.b) + j4) - j3);
                                while (i < min2) {
                                    if (bArr2[i] != b3) {
                                        i2 = i + 1;
                                    }
                                }
                                j4 = ((long) (segment.c - segment.b)) + j3;
                                segment = segment.f;
                                if (segment != null) {
                                    j6 = j4;
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            }
                        }
                    }
                }
                return ((long) (i - segment.b)) + j3;
            }
            return -1;
        }
        throw new IllegalArgumentException(("size=" + u() + " fromIndex=" + j4 + " toIndex=" + j5).toString());
    }

    public long a(ByteString byteString, long j) throws IOException {
        long j2;
        long j3 = j;
        Intrinsics.b(byteString, "bytes");
        if (byteString.size() > 0) {
            long j4 = 0;
            if (j3 >= 0) {
                Segment segment = this.f4066a;
                if (segment != null) {
                    if (u() - j3 < j3) {
                        long u = u();
                        while (u > j3) {
                            segment = segment.g;
                            if (segment != null) {
                                u -= (long) (segment.c - segment.b);
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                        if (segment != null) {
                            byte[] g = byteString.g();
                            byte b2 = g[0];
                            int size = byteString.size();
                            long u2 = (u() - ((long) size)) + 1;
                            while (u < u2) {
                                byte[] bArr = segment.f4083a;
                                long j5 = u;
                                int min = (int) Math.min((long) segment.c, (((long) segment.b) + u2) - u);
                                int i = (int) ((((long) segment.b) + j3) - j5);
                                while (i < min) {
                                    if (bArr[i] != b2 || !BufferKt.a(segment, i + 1, g, 1, size)) {
                                        i++;
                                    } else {
                                        j2 = ((long) (i - segment.b)) + j5;
                                    }
                                }
                                j3 = j5 + ((long) (segment.c - segment.b));
                                segment = segment.f;
                                if (segment != null) {
                                    u = j3;
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            }
                        }
                    } else {
                        while (true) {
                            long j6 = ((long) (segment.c - segment.b)) + j4;
                            if (j6 <= j3) {
                                segment = segment.f;
                                if (segment != null) {
                                    j4 = j6;
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            } else if (segment != null) {
                                byte[] g2 = byteString.g();
                                byte b3 = g2[0];
                                int size2 = byteString.size();
                                long u3 = (u() - ((long) size2)) + 1;
                                while (j4 < u3) {
                                    byte[] bArr2 = segment.f4083a;
                                    long j7 = u3;
                                    int min2 = (int) Math.min((long) segment.c, (((long) segment.b) + u3) - j4);
                                    for (int i2 = (int) ((((long) segment.b) + j3) - j4); i2 < min2; i2++) {
                                        if (bArr2[i2] == b3) {
                                            if (BufferKt.a(segment, i2 + 1, g2, 1, size2)) {
                                                j2 = ((long) (i2 - segment.b)) + j4;
                                            }
                                        }
                                    }
                                    j3 = ((long) (segment.c - segment.b)) + j4;
                                    segment = segment.f;
                                    if (segment != null) {
                                        j4 = j3;
                                        u3 = j7;
                                    } else {
                                        Intrinsics.a();
                                        throw null;
                                    }
                                }
                            }
                        }
                    }
                    return j2;
                }
                return -1;
            }
            throw new IllegalArgumentException(("fromIndex < 0: " + j3).toString());
        }
        throw new IllegalArgumentException("bytes is empty".toString());
    }

    public boolean a(long j, ByteString byteString, int i, int i2) {
        Intrinsics.b(byteString, "bytes");
        if (j < 0 || i < 0 || i2 < 0 || u() - j < ((long) i2) || byteString.size() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (h(((long) i3) + j) != byteString.a(i + i3)) {
                return false;
            }
        }
        return true;
    }

    public final ByteString a(int i) {
        if (i == 0) {
            return ByteString.c;
        }
        Util.a(u(), 0, (long) i);
        int i2 = 0;
        Segment segment = this.f4066a;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            if (segment != null) {
                int i5 = segment.c;
                int i6 = segment.b;
                if (i5 != i6) {
                    i3 += i5 - i6;
                    i4++;
                    segment = segment.f;
                } else {
                    throw new AssertionError("s.limit == s.pos");
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        }
        byte[][] bArr = new byte[i4][];
        int[] iArr = new int[(i4 * 2)];
        Segment segment2 = this.f4066a;
        int i7 = 0;
        while (i2 < i) {
            if (segment2 != null) {
                bArr[i7] = segment2.f4083a;
                i2 += segment2.c - segment2.b;
                iArr[i7] = Math.min(i2, i);
                iArr[bArr.length + i7] = segment2.b;
                segment2.d = true;
                i7++;
                segment2 = segment2.f;
            } else {
                Intrinsics.a();
                throw null;
            }
        }
        return new SegmentedByteString(bArr, iArr);
    }
}
