package okio;

import com.facebook.common.util.UriUtil;
import java.io.OutputStream;
import kotlin.jvm.internal.Intrinsics;

public final class Buffer$outputStream$1 extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Buffer f4069a;

    Buffer$outputStream$1(Buffer buffer) {
        this.f4069a = buffer;
    }

    public void close() {
    }

    public void flush() {
    }

    public String toString() {
        return this.f4069a + ".outputStream()";
    }

    public void write(int i) {
        this.f4069a.writeByte(i);
    }

    public void write(byte[] bArr, int i, int i2) {
        Intrinsics.b(bArr, UriUtil.DATA_SCHEME);
        this.f4069a.write(bArr, i, i2);
    }
}
