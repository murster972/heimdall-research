package okio;

import com.facebook.common.util.UriUtil;
import java.util.Arrays;
import kotlin.jvm.internal.Intrinsics;

public final class Segment {

    /* renamed from: a  reason: collision with root package name */
    public final byte[] f4083a;
    public int b;
    public int c;
    public boolean d;
    public boolean e;
    public Segment f;
    public Segment g;

    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    static {
        new Companion((DefaultConstructorMarker) null);
    }

    public Segment() {
        this.f4083a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    public final Segment a(Segment segment) {
        Intrinsics.b(segment, "segment");
        segment.g = this;
        segment.f = this.f;
        Segment segment2 = this.f;
        if (segment2 != null) {
            segment2.g = segment;
            this.f = segment;
            return segment;
        }
        Intrinsics.a();
        throw null;
    }

    public final Segment b() {
        Segment segment = this.f;
        if (segment == this) {
            segment = null;
        }
        Segment segment2 = this.g;
        if (segment2 != null) {
            segment2.f = this.f;
            Segment segment3 = this.f;
            if (segment3 != null) {
                segment3.g = segment2;
                this.f = null;
                this.g = null;
                return segment;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a();
        throw null;
    }

    public final Segment c() {
        this.d = true;
        return new Segment(this.f4083a, this.b, this.c, true, false);
    }

    public final Segment d() {
        byte[] bArr = this.f4083a;
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
        Intrinsics.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        return new Segment(copyOf, this.b, this.c, false, true);
    }

    public Segment(byte[] bArr, int i, int i2, boolean z, boolean z2) {
        Intrinsics.b(bArr, UriUtil.DATA_SCHEME);
        this.f4083a = bArr;
        this.b = i;
        this.c = i2;
        this.d = z;
        this.e = z2;
    }

    public final Segment a(int i) {
        Segment segment;
        if (i > 0 && i <= this.c - this.b) {
            if (i >= 1024) {
                segment = c();
            } else {
                segment = SegmentPool.c.a();
                byte[] bArr = this.f4083a;
                byte[] bArr2 = segment.f4083a;
                int i2 = this.b;
                byte[] unused = ArraysKt___ArraysJvmKt.a(bArr, bArr2, 0, i2, i2 + i, 2, (Object) null);
            }
            segment.c = segment.b + i;
            this.b += i;
            Segment segment2 = this.g;
            if (segment2 != null) {
                segment2.a(segment);
                return segment;
            }
            Intrinsics.a();
            throw null;
        }
        throw new IllegalArgumentException("byteCount out of range".toString());
    }

    public final void a() {
        int i = 0;
        if (this.g != this) {
            Segment segment = this.g;
            if (segment == null) {
                Intrinsics.a();
                throw null;
            } else if (segment.e) {
                int i2 = this.c - this.b;
                if (segment != null) {
                    int i3 = 8192 - segment.c;
                    if (segment != null) {
                        if (!segment.d) {
                            if (segment != null) {
                                i = segment.b;
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                        if (i2 <= i3 + i) {
                            Segment segment2 = this.g;
                            if (segment2 != null) {
                                a(segment2, i2);
                                b();
                                SegmentPool.c.a(this);
                                return;
                            }
                            Intrinsics.a();
                            throw null;
                        }
                        return;
                    }
                    Intrinsics.a();
                    throw null;
                }
                Intrinsics.a();
                throw null;
            }
        } else {
            throw new IllegalStateException("cannot compact".toString());
        }
    }

    public final void a(Segment segment, int i) {
        Intrinsics.b(segment, "sink");
        if (segment.e) {
            int i2 = segment.c;
            if (i2 + i > 8192) {
                if (!segment.d) {
                    int i3 = segment.b;
                    if ((i2 + i) - i3 <= 8192) {
                        byte[] bArr = segment.f4083a;
                        byte[] unused = ArraysKt___ArraysJvmKt.a(bArr, bArr, 0, i3, i2, 2, (Object) null);
                        segment.c -= segment.b;
                        segment.b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            byte[] bArr2 = this.f4083a;
            byte[] bArr3 = segment.f4083a;
            int i4 = segment.c;
            int i5 = this.b;
            byte[] unused2 = ArraysKt___ArraysJvmKt.a(bArr2, bArr3, i4, i5, i5 + i);
            segment.c += i;
            this.b += i;
            return;
        }
        throw new IllegalStateException("only owner can write".toString());
    }
}
