package okio;

import java.io.IOException;
import java.io.InputStream;
import kotlin.jvm.internal.Intrinsics;

final class InputStreamSource implements Source {

    /* renamed from: a  reason: collision with root package name */
    private final InputStream f4076a;
    private final Timeout b;

    public InputStreamSource(InputStream inputStream, Timeout timeout) {
        Intrinsics.b(inputStream, "input");
        Intrinsics.b(timeout, "timeout");
        this.f4076a = inputStream;
        this.b = timeout;
    }

    public void close() {
        this.f4076a.close();
    }

    public long read(Buffer buffer, long j) {
        Intrinsics.b(buffer, "sink");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i == 0) {
            return 0;
        }
        if (i >= 0) {
            try {
                this.b.throwIfReached();
                Segment b2 = buffer.b(1);
                int read = this.f4076a.read(b2.f4083a, b2.c, (int) Math.min(j, (long) (8192 - b2.c)));
                if (read != -1) {
                    b2.c += read;
                    long j2 = (long) read;
                    buffer.j(buffer.u() + j2);
                    return j2;
                } else if (b2.b != b2.c) {
                    return -1;
                } else {
                    buffer.f4066a = b2.b();
                    SegmentPool.c.a(b2);
                    return -1;
                }
            } catch (AssertionError e) {
                if (Okio.a(e)) {
                    throw new IOException(e);
                }
                throw e;
            }
        } else {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        }
    }

    public Timeout timeout() {
        return this.b;
    }

    public String toString() {
        return "source(" + this.f4076a + ')';
    }
}
