package okio;

import com.facebook.common.time.Clock;
import java.io.EOFException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import kotlin.jvm.internal.Intrinsics;
import okio.internal.BufferKt;

public final class RealBufferedSource implements BufferedSource {

    /* renamed from: a  reason: collision with root package name */
    public final Buffer f4081a = new Buffer();
    public boolean b;
    public final Source c;

    public RealBufferedSource(Source source) {
        Intrinsics.b(source, "source");
        this.c = source;
    }

    public Buffer a() {
        return this.f4081a;
    }

    public InputStream b() {
        return new RealBufferedSource$inputStream$1(this);
    }

    public String c() {
        return a((long) Clock.MAX_TIME);
    }

    public void close() {
        if (!this.b) {
            this.b = true;
            this.c.close();
            this.f4081a.o();
        }
    }

    public ByteString d(long j) {
        c(j);
        return this.f4081a.d(j);
    }

    public boolean e() {
        if (!(!this.b)) {
            throw new IllegalStateException("closed".toString());
        } else if (!this.f4081a.e() || this.c.read(this.f4081a, (long) 8192) != -1) {
            return false;
        } else {
            return true;
        }
    }

    public long f() {
        int i;
        c(1);
        long j = 0;
        while (true) {
            long j2 = j + 1;
            if (!request(j2)) {
                break;
            }
            byte h = this.f4081a.h(j);
            if ((h >= ((byte) 48) && h <= ((byte) 57)) || (j == 0 && h == ((byte) 45))) {
                j = j2;
            } else if (i == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("Expected leading [0-9] or '-' character but was 0x");
                int unused = CharsKt__CharJVMKt.a(16);
                int unused2 = CharsKt__CharJVMKt.a(16);
                String num = Integer.toString(h, 16);
                Intrinsics.a((Object) num, "java.lang.Integer.toStri…(this, checkRadix(radix))");
                sb.append(num);
                throw new NumberFormatException(sb.toString());
            }
        }
        return this.f4081a.f();
    }

    public ByteString g() {
        this.f4081a.a(this.c);
        return this.f4081a.g();
    }

    public String h() {
        this.f4081a.a(this.c);
        return this.f4081a.h();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long i() {
        /*
            r5 = this;
            r0 = 1
            r5.c(r0)
            r0 = 0
        L_0x0006:
            int r1 = r0 + 1
            long r2 = (long) r1
            boolean r2 = r5.request(r2)
            if (r2 == 0) goto L_0x0062
            okio.Buffer r2 = r5.f4081a
            long r3 = (long) r0
            byte r2 = r2.h(r3)
            r3 = 48
            byte r3 = (byte) r3
            if (r2 < r3) goto L_0x0020
            r3 = 57
            byte r3 = (byte) r3
            if (r2 <= r3) goto L_0x0035
        L_0x0020:
            r3 = 97
            byte r3 = (byte) r3
            if (r2 < r3) goto L_0x002a
            r3 = 102(0x66, float:1.43E-43)
            byte r3 = (byte) r3
            if (r2 <= r3) goto L_0x0035
        L_0x002a:
            r3 = 65
            byte r3 = (byte) r3
            if (r2 < r3) goto L_0x0037
            r3 = 70
            byte r3 = (byte) r3
            if (r2 <= r3) goto L_0x0035
            goto L_0x0037
        L_0x0035:
            r0 = r1
            goto L_0x0006
        L_0x0037:
            if (r0 == 0) goto L_0x003a
            goto L_0x0062
        L_0x003a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Expected leading [0-9a-fA-F] character but was 0x"
            r0.append(r1)
            r1 = 16
            int unused = kotlin.text.CharsKt__CharJVMKt.a((int) r1)
            int unused = kotlin.text.CharsKt__CharJVMKt.a((int) r1)
            java.lang.String r1 = java.lang.Integer.toString(r2, r1)
            java.lang.String r2 = "java.lang.Integer.toStri…(this, checkRadix(radix))"
            kotlin.jvm.internal.Intrinsics.a((java.lang.Object) r1, (java.lang.String) r2)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            r1.<init>(r0)
            throw r1
        L_0x0062:
            okio.Buffer r0 = r5.f4081a
            long r0 = r0.i()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.RealBufferedSource.i():long");
    }

    public boolean isOpen() {
        return !this.b;
    }

    public int j() {
        c(4);
        return this.f4081a.r();
    }

    public short k() {
        c(2);
        return this.f4081a.s();
    }

    public BufferedSource peek() {
        return Okio.a((Source) new PeekSource(this));
    }

    public long read(Buffer buffer, long j) {
        Intrinsics.b(buffer, "sink");
        if (!(j >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (!(true ^ this.b)) {
            throw new IllegalStateException("closed".toString());
        } else if (this.f4081a.u() == 0 && this.c.read(this.f4081a, (long) 8192) == -1) {
            return -1;
        } else {
            return this.f4081a.read(buffer, Math.min(j, this.f4081a.u()));
        }
    }

    public byte readByte() {
        c(1);
        return this.f4081a.readByte();
    }

    public void readFully(byte[] bArr) {
        Intrinsics.b(bArr, "sink");
        try {
            c((long) bArr.length);
            this.f4081a.readFully(bArr);
        } catch (EOFException e) {
            int i = 0;
            while (this.f4081a.u() > 0) {
                Buffer buffer = this.f4081a;
                int a2 = buffer.a(bArr, i, (int) buffer.u());
                if (a2 != -1) {
                    i += a2;
                } else {
                    throw new AssertionError();
                }
            }
            throw e;
        }
    }

    public int readInt() {
        c(4);
        return this.f4081a.readInt();
    }

    public long readLong() {
        c(8);
        return this.f4081a.readLong();
    }

    public short readShort() {
        c(2);
        return this.f4081a.readShort();
    }

    public boolean request(long j) {
        if (!(j >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (!this.b) {
            while (this.f4081a.u() < j) {
                if (this.c.read(this.f4081a, (long) 8192) == -1) {
                    return false;
                }
            }
            return true;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    public void skip(long j) {
        if (!this.b) {
            while (j > 0) {
                if (this.f4081a.u() == 0 && this.c.read(this.f4081a, (long) 8192) == -1) {
                    throw new EOFException();
                }
                long min = Math.min(j, this.f4081a.u());
                this.f4081a.skip(min);
                j -= min;
            }
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    public Timeout timeout() {
        return this.c.timeout();
    }

    public String toString() {
        return "buffer(" + this.c + ')';
    }

    public long a(byte b2) {
        return a(b2, 0, Clock.MAX_TIME);
    }

    public byte[] b(long j) {
        c(j);
        return this.f4081a.b(j);
    }

    public void c(long j) {
        if (!request(j)) {
            throw new EOFException();
        }
    }

    public boolean a(long j, ByteString byteString) {
        Intrinsics.b(byteString, "bytes");
        return a(j, byteString, 0, byteString.size());
    }

    public byte[] d() {
        this.f4081a.a(this.c);
        return this.f4081a.d();
    }

    public int a(Options options) {
        Intrinsics.b(options, "options");
        if (!this.b) {
            do {
                int a2 = BufferKt.a(this.f4081a, options, true);
                if (a2 != -2) {
                    if (a2 == -1) {
                        return -1;
                    }
                    this.f4081a.skip((long) options.b()[a2].size());
                    return a2;
                }
            } while (this.c.read(this.f4081a, (long) 8192) != -1);
            return -1;
        }
        throw new IllegalStateException("closed".toString());
    }

    public int read(ByteBuffer byteBuffer) {
        Intrinsics.b(byteBuffer, "sink");
        if (this.f4081a.u() == 0 && this.c.read(this.f4081a, (long) 8192) == -1) {
            return -1;
        }
        return this.f4081a.read(byteBuffer);
    }

    public void a(Buffer buffer, long j) {
        Intrinsics.b(buffer, "sink");
        try {
            c(j);
            this.f4081a.a(buffer, j);
        } catch (EOFException e) {
            buffer.a((Source) this.f4081a);
            throw e;
        }
    }

    public long a(Sink sink) {
        Intrinsics.b(sink, "sink");
        long j = 0;
        while (this.c.read(this.f4081a, (long) 8192) != -1) {
            long p = this.f4081a.p();
            if (p > 0) {
                j += p;
                sink.write(this.f4081a, p);
            }
        }
        if (this.f4081a.u() <= 0) {
            return j;
        }
        long u = j + this.f4081a.u();
        Buffer buffer = this.f4081a;
        sink.write(buffer, buffer.u());
        return u;
    }

    public String a(Charset charset) {
        Intrinsics.b(charset, "charset");
        this.f4081a.a(this.c);
        return this.f4081a.a(charset);
    }

    public String a(long j) {
        if (j >= 0) {
            long j2 = j == Clock.MAX_TIME ? Long.MAX_VALUE : j + 1;
            byte b2 = (byte) 10;
            long a2 = a(b2, 0, j2);
            if (a2 != -1) {
                return BufferKt.a(this.f4081a, a2);
            }
            if (j2 < Clock.MAX_TIME && request(j2) && this.f4081a.h(j2 - 1) == ((byte) 13) && request(1 + j2) && this.f4081a.h(j2) == b2) {
                return BufferKt.a(this.f4081a, j2);
            }
            Buffer buffer = new Buffer();
            Buffer buffer2 = this.f4081a;
            buffer2.a(buffer, 0, Math.min((long) 32, buffer2.u()));
            throw new EOFException("\\n not found: limit=" + Math.min(this.f4081a.u(), j) + " content=" + buffer.g().f() + "…");
        }
        throw new IllegalArgumentException(("limit < 0: " + j).toString());
    }

    public long a(byte b2, long j, long j2) {
        boolean z = true;
        if (!this.b) {
            if (0 > j || j2 < j) {
                z = false;
            }
            if (z) {
                while (j < j2) {
                    long a2 = this.f4081a.a(b2, j, j2);
                    if (a2 != -1) {
                        return a2;
                    }
                    long u = this.f4081a.u();
                    if (u >= j2 || this.c.read(this.f4081a, (long) 8192) == -1) {
                        return -1;
                    }
                    j = Math.max(j, u);
                }
                return -1;
            }
            throw new IllegalArgumentException(("fromIndex=" + j + " toIndex=" + j2).toString());
        }
        throw new IllegalStateException("closed".toString());
    }

    public boolean a(long j, ByteString byteString, int i, int i2) {
        Intrinsics.b(byteString, "bytes");
        if (!(!this.b)) {
            throw new IllegalStateException("closed".toString());
        } else if (j < 0 || i < 0 || i2 < 0 || byteString.size() - i < i2) {
            return false;
        } else {
            for (int i3 = 0; i3 < i2; i3++) {
                long j2 = ((long) i3) + j;
                if (!request(1 + j2) || this.f4081a.h(j2) != byteString.a(i + i3)) {
                    return false;
                }
            }
            return true;
        }
    }
}
