package okio;

import java.security.MessageDigest;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import okio.internal.SegmentedByteStringKt;

public final class SegmentedByteString extends ByteString {
    private final transient byte[][] e;
    private final transient int[] f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SegmentedByteString(byte[][] bArr, int[] iArr) {
        super(ByteString.c.b());
        Intrinsics.b(bArr, "segments");
        Intrinsics.b(iArr, "directory");
        this.e = bArr;
        this.f = iArr;
    }

    private final ByteString p() {
        return new ByteString(l());
    }

    private final Object writeReplace() {
        ByteString p = p();
        if (p != null) {
            return p;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.Object");
    }

    public String a() {
        return p().a();
    }

    public byte b(int i) {
        int i2;
        Util.a((long) n()[o().length - 1], (long) i, 1);
        int a2 = SegmentedByteStringKt.a(this, i);
        if (a2 == 0) {
            i2 = 0;
        } else {
            i2 = n()[a2 - 1];
        }
        return o()[a2][(i - i2) + n()[o().length + a2]];
    }

    public int d() {
        return n()[o().length - 1];
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ByteString) {
            ByteString byteString = (ByteString) obj;
            return byteString.size() == size() && a(0, byteString, 0, size());
        }
    }

    public String f() {
        return p().f();
    }

    public byte[] g() {
        return l();
    }

    public int hashCode() {
        int c = c();
        if (c != 0) {
            return c;
        }
        int length = o().length;
        int i = 0;
        int i2 = 1;
        int i3 = 0;
        while (i < length) {
            int i4 = n()[length + i];
            int i5 = n()[i];
            byte[] bArr = o()[i];
            int i6 = (i5 - i3) + i4;
            while (i4 < i6) {
                i2 = (i2 * 31) + bArr[i4];
                i4++;
            }
            i++;
            i3 = i5;
        }
        c(i2);
        return i2;
    }

    public ByteString k() {
        return p().k();
    }

    public byte[] l() {
        byte[] bArr = new byte[size()];
        int length = o().length;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i < length) {
            int i4 = n()[length + i];
            int i5 = n()[i];
            int i6 = i5 - i2;
            byte[] unused = ArraysKt___ArraysJvmKt.a(o()[i], bArr, i3, i4, i4 + i6);
            i3 += i6;
            i++;
            i2 = i5;
        }
        return bArr;
    }

    public final int[] n() {
        return this.f;
    }

    public final byte[][] o() {
        return this.e;
    }

    public String toString() {
        return p().toString();
    }

    public ByteString a(String str) {
        Intrinsics.b(str, "algorithm");
        MessageDigest instance = MessageDigest.getInstance(str);
        int length = o().length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = n()[length + i];
            int i4 = n()[i];
            instance.update(o()[i], i3, i4 - i2);
            i++;
            i2 = i4;
        }
        byte[] digest = instance.digest();
        Intrinsics.a((Object) digest, "digest.digest()");
        return new ByteString(digest);
    }

    public void a(Buffer buffer, int i, int i2) {
        int i3;
        Intrinsics.b(buffer, "buffer");
        int i4 = i2 + i;
        int a2 = SegmentedByteStringKt.a(this, i);
        while (i < i4) {
            if (a2 == 0) {
                i3 = 0;
            } else {
                i3 = n()[a2 - 1];
            }
            int i5 = n()[o().length + a2];
            int min = Math.min(i4, (n()[a2] - i3) + i3) - i;
            int i6 = i5 + (i - i3);
            Segment segment = new Segment(o()[a2], i6, i6 + min, true, false);
            Segment segment2 = buffer.f4066a;
            if (segment2 == null) {
                segment.g = segment;
                segment.f = segment.g;
                buffer.f4066a = segment.f;
            } else if (segment2 != null) {
                Segment segment3 = segment2.g;
                if (segment3 != null) {
                    segment3.a(segment);
                } else {
                    Intrinsics.a();
                    throw null;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
            i += min;
            a2++;
        }
        buffer.j(buffer.u() + ((long) size()));
    }

    public boolean a(int i, ByteString byteString, int i2, int i3) {
        int i4;
        Intrinsics.b(byteString, "other");
        if (i < 0 || i > size() - i3) {
            return false;
        }
        int i5 = i3 + i;
        int a2 = SegmentedByteStringKt.a(this, i);
        while (i < i5) {
            if (a2 == 0) {
                i4 = 0;
            } else {
                i4 = n()[a2 - 1];
            }
            int i6 = n()[o().length + a2];
            int min = Math.min(i5, (n()[a2] - i4) + i4) - i;
            if (!byteString.a(i2, o()[a2], i6 + (i - i4), min)) {
                return false;
            }
            i2 += min;
            i += min;
            a2++;
        }
        return true;
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        int i4;
        Intrinsics.b(bArr, "other");
        if (i < 0 || i > size() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int i5 = i3 + i;
        int a2 = SegmentedByteStringKt.a(this, i);
        while (i < i5) {
            if (a2 == 0) {
                i4 = 0;
            } else {
                i4 = n()[a2 - 1];
            }
            int i6 = n()[o().length + a2];
            int min = Math.min(i5, (n()[a2] - i4) + i4) - i;
            if (!Util.a(o()[a2], i6 + (i - i4), bArr, i2, min)) {
                return false;
            }
            i2 += min;
            i += min;
            a2++;
        }
        return true;
    }
}
