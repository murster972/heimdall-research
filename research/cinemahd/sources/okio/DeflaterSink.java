package okio;

import java.io.IOException;
import java.util.zip.Deflater;
import kotlin.jvm.internal.Intrinsics;

public final class DeflaterSink implements Sink {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4071a;
    private final BufferedSink b;
    private final Deflater c;

    public DeflaterSink(BufferedSink bufferedSink, Deflater deflater) {
        Intrinsics.b(bufferedSink, "sink");
        Intrinsics.b(deflater, "deflater");
        this.b = bufferedSink;
        this.c = deflater;
    }

    private final void a(boolean z) {
        Segment b2;
        int i;
        Buffer a2 = this.b.a();
        while (true) {
            b2 = a2.b(1);
            if (z) {
                Deflater deflater = this.c;
                byte[] bArr = b2.f4083a;
                int i2 = b2.c;
                i = deflater.deflate(bArr, i2, 8192 - i2, 2);
            } else {
                Deflater deflater2 = this.c;
                byte[] bArr2 = b2.f4083a;
                int i3 = b2.c;
                i = deflater2.deflate(bArr2, i3, 8192 - i3);
            }
            if (i > 0) {
                b2.c += i;
                a2.j(a2.u() + ((long) i));
                this.b.l();
            } else if (this.c.needsInput()) {
                break;
            }
        }
        if (b2.b == b2.c) {
            a2.f4066a = b2.b();
            SegmentPool.c.a(b2);
        }
    }

    public final void b() {
        this.c.finish();
        a(false);
    }

    public void close() throws IOException {
        if (!this.f4071a) {
            Throwable th = null;
            try {
                b();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.c.end();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            try {
                this.b.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.f4071a = true;
            if (th != null) {
                throw th;
            }
        }
    }

    public void flush() throws IOException {
        a(true);
        this.b.flush();
    }

    public Timeout timeout() {
        return this.b.timeout();
    }

    public String toString() {
        return "DeflaterSink(" + this.b + ')';
    }

    public void write(Buffer buffer, long j) throws IOException {
        Intrinsics.b(buffer, "source");
        Util.a(buffer.u(), 0, j);
        while (j > 0) {
            Segment segment = buffer.f4066a;
            if (segment != null) {
                int min = (int) Math.min(j, (long) (segment.c - segment.b));
                this.c.setInput(segment.f4083a, segment.b, min);
                a(false);
                long j2 = (long) min;
                buffer.j(buffer.u() - j2);
                segment.b += min;
                if (segment.b == segment.c) {
                    buffer.f4066a = segment.b();
                    SegmentPool.c.a(segment);
                }
                j -= j2;
            } else {
                Intrinsics.a();
                throw null;
            }
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DeflaterSink(Sink sink, Deflater deflater) {
        this(Okio.a(sink), deflater);
        Intrinsics.b(sink, "sink");
        Intrinsics.b(deflater, "deflater");
    }
}
