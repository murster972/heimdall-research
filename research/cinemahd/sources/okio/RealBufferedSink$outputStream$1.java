package okio;

import com.facebook.common.util.UriUtil;
import java.io.IOException;
import java.io.OutputStream;
import kotlin.jvm.internal.Intrinsics;

public final class RealBufferedSink$outputStream$1 extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RealBufferedSink f4080a;

    RealBufferedSink$outputStream$1(RealBufferedSink realBufferedSink) {
        this.f4080a = realBufferedSink;
    }

    public void close() {
        this.f4080a.close();
    }

    public void flush() {
        RealBufferedSink realBufferedSink = this.f4080a;
        if (!realBufferedSink.b) {
            realBufferedSink.flush();
        }
    }

    public String toString() {
        return this.f4080a + ".outputStream()";
    }

    public void write(int i) {
        RealBufferedSink realBufferedSink = this.f4080a;
        if (!realBufferedSink.b) {
            realBufferedSink.f4079a.writeByte((int) (byte) i);
            this.f4080a.l();
            return;
        }
        throw new IOException("closed");
    }

    public void write(byte[] bArr, int i, int i2) {
        Intrinsics.b(bArr, UriUtil.DATA_SCHEME);
        RealBufferedSink realBufferedSink = this.f4080a;
        if (!realBufferedSink.b) {
            realBufferedSink.f4079a.write(bArr, i, i2);
            this.f4080a.l();
            return;
        }
        throw new IOException("closed");
    }
}
