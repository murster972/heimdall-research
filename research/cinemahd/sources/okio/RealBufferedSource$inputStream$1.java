package okio;

import com.facebook.common.util.UriUtil;
import java.io.IOException;
import java.io.InputStream;
import kotlin.jvm.internal.Intrinsics;

public final class RealBufferedSource$inputStream$1 extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RealBufferedSource f4082a;

    RealBufferedSource$inputStream$1(RealBufferedSource realBufferedSource) {
        this.f4082a = realBufferedSource;
    }

    public int available() {
        RealBufferedSource realBufferedSource = this.f4082a;
        if (!realBufferedSource.b) {
            return (int) Math.min(realBufferedSource.f4081a.u(), (long) Integer.MAX_VALUE);
        }
        throw new IOException("closed");
    }

    public void close() {
        this.f4082a.close();
    }

    public int read() {
        RealBufferedSource realBufferedSource = this.f4082a;
        if (!realBufferedSource.b) {
            if (realBufferedSource.f4081a.u() == 0) {
                RealBufferedSource realBufferedSource2 = this.f4082a;
                if (realBufferedSource2.c.read(realBufferedSource2.f4081a, (long) 8192) == -1) {
                    return -1;
                }
            }
            return this.f4082a.f4081a.readByte() & 255;
        }
        throw new IOException("closed");
    }

    public String toString() {
        return this.f4082a + ".inputStream()";
    }

    public int read(byte[] bArr, int i, int i2) {
        Intrinsics.b(bArr, UriUtil.DATA_SCHEME);
        if (!this.f4082a.b) {
            Util.a((long) bArr.length, (long) i, (long) i2);
            if (this.f4082a.f4081a.u() == 0) {
                RealBufferedSource realBufferedSource = this.f4082a;
                if (realBufferedSource.c.read(realBufferedSource.f4081a, (long) 8192) == -1) {
                    return -1;
                }
            }
            return this.f4082a.f4081a.a(bArr, i, i2);
        }
        throw new IOException("closed");
    }
}
