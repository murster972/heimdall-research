package okio.internal;

import kotlin.jvm.internal.Intrinsics;
import okio.Buffer;
import okio.ByteString;

public final class ByteStringKt {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f4087a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* access modifiers changed from: private */
    public static final int b(char c) {
        if ('0' <= c && '9' >= c) {
            return c - '0';
        }
        char c2 = 'a';
        if ('a' > c || 'f' < c) {
            c2 = 'A';
            if ('A' > c || 'F' < c) {
                throw new IllegalArgumentException("Unexpected hex digit: " + c);
            }
        }
        return (c - c2) + 10;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0069, code lost:
        return -1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final int b(byte[] r20, int r21) {
        /*
            r0 = r20
            r1 = r21
            int r2 = r0.length
            r4 = 0
            r5 = 0
            r6 = 0
        L_0x0008:
            if (r4 >= r2) goto L_0x01df
            byte r7 = r0[r4]
            r8 = 127(0x7f, float:1.78E-43)
            r9 = 159(0x9f, float:2.23E-43)
            r10 = 31
            r11 = 13
            r12 = 65533(0xfffd, float:9.1831E-41)
            r13 = 10
            r14 = 65536(0x10000, float:9.18355E-41)
            r16 = -1
            r17 = 1
            if (r7 < 0) goto L_0x0078
            int r18 = r6 + 1
            if (r6 != r1) goto L_0x0026
            return r5
        L_0x0026:
            if (r7 == r13) goto L_0x0038
            if (r7 == r11) goto L_0x0038
            if (r7 < 0) goto L_0x002e
            if (r10 >= r7) goto L_0x0033
        L_0x002e:
            if (r8 <= r7) goto L_0x0031
            goto L_0x0035
        L_0x0031:
            if (r9 < r7) goto L_0x0035
        L_0x0033:
            r6 = 1
            goto L_0x0036
        L_0x0035:
            r6 = 0
        L_0x0036:
            if (r6 != 0) goto L_0x003a
        L_0x0038:
            if (r7 != r12) goto L_0x003b
        L_0x003a:
            return r16
        L_0x003b:
            if (r7 >= r14) goto L_0x003f
            r6 = 1
            goto L_0x0040
        L_0x003f:
            r6 = 2
        L_0x0040:
            int r5 = r5 + r6
            int r4 = r4 + 1
            r6 = r5
        L_0x0044:
            r5 = r18
            if (r4 >= r2) goto L_0x0072
            byte r7 = r0[r4]
            if (r7 < 0) goto L_0x0072
            int r7 = r4 + 1
            byte r4 = r0[r4]
            int r18 = r5 + 1
            if (r5 != r1) goto L_0x0055
            return r6
        L_0x0055:
            if (r4 == r13) goto L_0x0067
            if (r4 == r11) goto L_0x0067
            if (r4 < 0) goto L_0x005d
            if (r10 >= r4) goto L_0x0062
        L_0x005d:
            if (r8 <= r4) goto L_0x0060
            goto L_0x0064
        L_0x0060:
            if (r9 < r4) goto L_0x0064
        L_0x0062:
            r5 = 1
            goto L_0x0065
        L_0x0064:
            r5 = 0
        L_0x0065:
            if (r5 != 0) goto L_0x0069
        L_0x0067:
            if (r4 != r12) goto L_0x006a
        L_0x0069:
            return r16
        L_0x006a:
            if (r4 >= r14) goto L_0x006e
            r4 = 1
            goto L_0x006f
        L_0x006e:
            r4 = 2
        L_0x006f:
            int r6 = r6 + r4
            r4 = r7
            goto L_0x0044
        L_0x0072:
            r19 = r6
            r6 = r5
            r5 = r19
            goto L_0x0008
        L_0x0078:
            int r3 = r7 >> 5
            r15 = -2
            r14 = 128(0x80, float:1.794E-43)
            if (r3 != r15) goto L_0x00cb
            int r3 = r4 + 1
            if (r2 > r3) goto L_0x0087
            if (r6 != r1) goto L_0x0086
            return r5
        L_0x0086:
            return r16
        L_0x0087:
            byte r7 = r0[r4]
            byte r3 = r0[r3]
            r15 = r3 & 192(0xc0, float:2.69E-43)
            if (r15 != r14) goto L_0x0091
            r15 = 1
            goto L_0x0092
        L_0x0091:
            r15 = 0
        L_0x0092:
            if (r15 != 0) goto L_0x0098
            if (r6 != r1) goto L_0x0097
            return r5
        L_0x0097:
            return r16
        L_0x0098:
            r3 = r3 ^ 3968(0xf80, float:5.56E-42)
            int r7 = r7 << 6
            r3 = r3 ^ r7
            if (r3 >= r14) goto L_0x00a3
            if (r6 != r1) goto L_0x00a2
            return r5
        L_0x00a2:
            return r16
        L_0x00a3:
            int r7 = r6 + 1
            if (r6 != r1) goto L_0x00a8
            return r5
        L_0x00a8:
            if (r3 == r13) goto L_0x00ba
            if (r3 == r11) goto L_0x00ba
            if (r3 < 0) goto L_0x00b0
            if (r10 >= r3) goto L_0x00b5
        L_0x00b0:
            if (r8 <= r3) goto L_0x00b3
            goto L_0x00b7
        L_0x00b3:
            if (r9 < r3) goto L_0x00b7
        L_0x00b5:
            r6 = 1
            goto L_0x00b8
        L_0x00b7:
            r6 = 0
        L_0x00b8:
            if (r6 != 0) goto L_0x00bc
        L_0x00ba:
            if (r3 != r12) goto L_0x00bd
        L_0x00bc:
            return r16
        L_0x00bd:
            r6 = 65536(0x10000, float:9.18355E-41)
            if (r3 >= r6) goto L_0x00c2
            goto L_0x00c4
        L_0x00c2:
            r17 = 2
        L_0x00c4:
            int r5 = r5 + r17
            int r4 = r4 + 2
        L_0x00c8:
            r6 = r7
            goto L_0x0008
        L_0x00cb:
            int r3 = r7 >> 4
            if (r3 != r15) goto L_0x0143
            int r3 = r4 + 2
            if (r2 > r3) goto L_0x00d7
            if (r6 != r1) goto L_0x00d6
            return r5
        L_0x00d6:
            return r16
        L_0x00d7:
            byte r7 = r0[r4]
            int r15 = r4 + 1
            byte r15 = r0[r15]
            r12 = r15 & 192(0xc0, float:2.69E-43)
            if (r12 != r14) goto L_0x00e3
            r12 = 1
            goto L_0x00e4
        L_0x00e3:
            r12 = 0
        L_0x00e4:
            if (r12 != 0) goto L_0x00ea
            if (r6 != r1) goto L_0x00e9
            return r5
        L_0x00e9:
            return r16
        L_0x00ea:
            byte r3 = r0[r3]
            r12 = r3 & 192(0xc0, float:2.69E-43)
            if (r12 != r14) goto L_0x00f2
            r12 = 1
            goto L_0x00f3
        L_0x00f2:
            r12 = 0
        L_0x00f3:
            if (r12 != 0) goto L_0x00f9
            if (r6 != r1) goto L_0x00f8
            return r5
        L_0x00f8:
            return r16
        L_0x00f9:
            r12 = -123008(0xfffffffffffe1f80, float:NaN)
            r3 = r3 ^ r12
            int r12 = r15 << 6
            r3 = r3 ^ r12
            int r7 = r7 << 12
            r3 = r3 ^ r7
            r7 = 2048(0x800, float:2.87E-42)
            if (r3 >= r7) goto L_0x010b
            if (r6 != r1) goto L_0x010a
            return r5
        L_0x010a:
            return r16
        L_0x010b:
            r7 = 57343(0xdfff, float:8.0355E-41)
            r12 = 55296(0xd800, float:7.7486E-41)
            if (r12 <= r3) goto L_0x0114
            goto L_0x011a
        L_0x0114:
            if (r7 < r3) goto L_0x011a
            if (r6 != r1) goto L_0x0119
            return r5
        L_0x0119:
            return r16
        L_0x011a:
            int r7 = r6 + 1
            if (r6 != r1) goto L_0x011f
            return r5
        L_0x011f:
            if (r3 == r13) goto L_0x0131
            if (r3 == r11) goto L_0x0131
            if (r3 < 0) goto L_0x0127
            if (r10 >= r3) goto L_0x012c
        L_0x0127:
            if (r8 <= r3) goto L_0x012a
            goto L_0x012e
        L_0x012a:
            if (r9 < r3) goto L_0x012e
        L_0x012c:
            r6 = 1
            goto L_0x012f
        L_0x012e:
            r6 = 0
        L_0x012f:
            if (r6 != 0) goto L_0x0136
        L_0x0131:
            r6 = 65533(0xfffd, float:9.1831E-41)
            if (r3 != r6) goto L_0x0137
        L_0x0136:
            return r16
        L_0x0137:
            r6 = 65536(0x10000, float:9.18355E-41)
            if (r3 >= r6) goto L_0x013c
            goto L_0x013e
        L_0x013c:
            r17 = 2
        L_0x013e:
            int r5 = r5 + r17
            int r4 = r4 + 3
            goto L_0x00c8
        L_0x0143:
            int r3 = r7 >> 3
            if (r3 != r15) goto L_0x01db
            int r3 = r4 + 3
            if (r2 > r3) goto L_0x014f
            if (r6 != r1) goto L_0x014e
            return r5
        L_0x014e:
            return r16
        L_0x014f:
            byte r7 = r0[r4]
            int r12 = r4 + 1
            byte r12 = r0[r12]
            r15 = r12 & 192(0xc0, float:2.69E-43)
            if (r15 != r14) goto L_0x015b
            r15 = 1
            goto L_0x015c
        L_0x015b:
            r15 = 0
        L_0x015c:
            if (r15 != 0) goto L_0x0162
            if (r6 != r1) goto L_0x0161
            return r5
        L_0x0161:
            return r16
        L_0x0162:
            int r15 = r4 + 2
            byte r15 = r0[r15]
            r9 = r15 & 192(0xc0, float:2.69E-43)
            if (r9 != r14) goto L_0x016c
            r9 = 1
            goto L_0x016d
        L_0x016c:
            r9 = 0
        L_0x016d:
            if (r9 != 0) goto L_0x0173
            if (r6 != r1) goto L_0x0172
            return r5
        L_0x0172:
            return r16
        L_0x0173:
            byte r3 = r0[r3]
            r9 = r3 & 192(0xc0, float:2.69E-43)
            if (r9 != r14) goto L_0x017b
            r9 = 1
            goto L_0x017c
        L_0x017b:
            r9 = 0
        L_0x017c:
            if (r9 != 0) goto L_0x0182
            if (r6 != r1) goto L_0x0181
            return r5
        L_0x0181:
            return r16
        L_0x0182:
            r9 = 3678080(0x381f80, float:5.154088E-39)
            r3 = r3 ^ r9
            int r9 = r15 << 6
            r3 = r3 ^ r9
            int r9 = r12 << 12
            r3 = r3 ^ r9
            int r7 = r7 << 18
            r3 = r3 ^ r7
            r7 = 1114111(0x10ffff, float:1.561202E-39)
            if (r3 <= r7) goto L_0x0198
            if (r6 != r1) goto L_0x0197
            return r5
        L_0x0197:
            return r16
        L_0x0198:
            r7 = 57343(0xdfff, float:8.0355E-41)
            r9 = 55296(0xd800, float:7.7486E-41)
            if (r9 <= r3) goto L_0x01a1
            goto L_0x01a7
        L_0x01a1:
            if (r7 < r3) goto L_0x01a7
            if (r6 != r1) goto L_0x01a6
            return r5
        L_0x01a6:
            return r16
        L_0x01a7:
            r7 = 65536(0x10000, float:9.18355E-41)
            if (r3 >= r7) goto L_0x01af
            if (r6 != r1) goto L_0x01ae
            return r5
        L_0x01ae:
            return r16
        L_0x01af:
            int r7 = r6 + 1
            if (r6 != r1) goto L_0x01b4
            return r5
        L_0x01b4:
            if (r3 == r13) goto L_0x01c8
            if (r3 == r11) goto L_0x01c8
            if (r3 < 0) goto L_0x01bc
            if (r10 >= r3) goto L_0x01c3
        L_0x01bc:
            if (r8 <= r3) goto L_0x01bf
            goto L_0x01c5
        L_0x01bf:
            r6 = 159(0x9f, float:2.23E-43)
            if (r6 < r3) goto L_0x01c5
        L_0x01c3:
            r6 = 1
            goto L_0x01c6
        L_0x01c5:
            r6 = 0
        L_0x01c6:
            if (r6 != 0) goto L_0x01cd
        L_0x01c8:
            r6 = 65533(0xfffd, float:9.1831E-41)
            if (r3 != r6) goto L_0x01ce
        L_0x01cd:
            return r16
        L_0x01ce:
            r6 = 65536(0x10000, float:9.18355E-41)
            if (r3 >= r6) goto L_0x01d3
            goto L_0x01d5
        L_0x01d3:
            r17 = 2
        L_0x01d5:
            int r5 = r5 + r17
            int r4 = r4 + 4
            goto L_0x00c8
        L_0x01db:
            if (r6 != r1) goto L_0x01de
            return r5
        L_0x01de:
            return r16
        L_0x01df:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.internal.ByteStringKt.b(byte[], int):int");
    }

    public static final char[] a() {
        return f4087a;
    }

    public static final void a(ByteString byteString, Buffer buffer, int i, int i2) {
        Intrinsics.b(byteString, "$this$commonWrite");
        Intrinsics.b(buffer, "buffer");
        buffer.write(byteString.b(), i, i2);
    }
}
