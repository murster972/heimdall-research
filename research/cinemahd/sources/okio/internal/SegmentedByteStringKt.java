package okio.internal;

import kotlin.jvm.internal.Intrinsics;
import okio.SegmentedByteString;

public final class SegmentedByteStringKt {
    public static final int a(int[] iArr, int i, int i2, int i3) {
        Intrinsics.b(iArr, "$this$binarySearch");
        int i4 = i3 - 1;
        while (i2 <= i4) {
            int i5 = (i2 + i4) >>> 1;
            int i6 = iArr[i5];
            if (i6 < i) {
                i2 = i5 + 1;
            } else if (i6 <= i) {
                return i5;
            } else {
                i4 = i5 - 1;
            }
        }
        return (-i2) - 1;
    }

    public static final int a(SegmentedByteString segmentedByteString, int i) {
        Intrinsics.b(segmentedByteString, "$this$segment");
        int a2 = a(segmentedByteString.n(), i + 1, 0, segmentedByteString.o().length);
        return a2 >= 0 ? a2 : ~a2;
    }
}
