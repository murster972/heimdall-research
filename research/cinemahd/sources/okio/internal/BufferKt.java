package okio.internal;

import kotlin.jvm.internal.Intrinsics;
import okio.Buffer;
import okio.Options;
import okio.Platform;
import okio.Segment;

public final class BufferKt {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f4086a = Platform.a("0123456789abcdef");

    public static final byte[] a() {
        return f4086a;
    }

    public static final boolean a(Segment segment, int i, byte[] bArr, int i2, int i3) {
        Intrinsics.b(segment, "segment");
        Intrinsics.b(bArr, "bytes");
        int i4 = segment.c;
        byte[] bArr2 = segment.f4083a;
        while (i2 < i3) {
            if (i == i4) {
                segment = segment.f;
                if (segment != null) {
                    byte[] bArr3 = segment.f4083a;
                    int i5 = segment.b;
                    bArr2 = bArr3;
                    i = i5;
                    i4 = segment.c;
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
            if (bArr2[i] != bArr[i2]) {
                return false;
            }
            i++;
            i2++;
        }
        return true;
    }

    public static final String a(Buffer buffer, long j) {
        Intrinsics.b(buffer, "$this$readUtf8Line");
        if (j > 0) {
            long j2 = j - 1;
            if (buffer.h(j2) == ((byte) 13)) {
                String i = buffer.i(j2);
                buffer.skip(2);
                return i;
            }
        }
        String i2 = buffer.i(j);
        buffer.skip(1);
        return i2;
    }

    public static /* synthetic */ int a(Buffer buffer, Options options, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return a(buffer, options, z);
    }

    public static final int a(Buffer buffer, Options options, boolean z) {
        int i;
        int i2;
        Buffer buffer2 = buffer;
        Intrinsics.b(buffer2, "$this$selectPrefix");
        Intrinsics.b(options, "options");
        Segment segment = buffer2.f4066a;
        if (segment != null) {
            byte[] bArr = segment.f4083a;
            int i3 = segment.b;
            int i4 = segment.c;
            int[] c = options.c();
            int i5 = i3;
            int i6 = i4;
            int i7 = -1;
            Segment segment2 = segment;
            byte[] bArr2 = bArr;
            int i8 = 0;
            loop0:
            while (true) {
                int i9 = i8 + 1;
                int i10 = c[i8];
                int i11 = i9 + 1;
                int i12 = c[i9];
                if (i12 != -1) {
                    i7 = i12;
                }
                if (segment2 == null) {
                    break;
                }
                if (i10 < 0) {
                    int i13 = i11 + (i10 * -1);
                    while (true) {
                        int i14 = i5 + 1;
                        int i15 = i11 + 1;
                        if ((bArr2[i5] & 255) != c[i11]) {
                            return i7;
                        }
                        boolean z2 = i15 == i13;
                        if (i14 == i6) {
                            if (segment2 != null) {
                                Segment segment3 = segment2.f;
                                if (segment3 != null) {
                                    int i16 = segment3.b;
                                    bArr2 = segment3.f4083a;
                                    i6 = segment3.c;
                                    if (segment3 != segment) {
                                        int i17 = i16;
                                        segment2 = segment3;
                                        i14 = i17;
                                    } else if (!z2) {
                                        break loop0;
                                    } else {
                                        i14 = i16;
                                        segment2 = null;
                                    }
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                        if (z2) {
                            i2 = c[i15];
                            i = i14;
                            break;
                        }
                        i5 = i14;
                        i11 = i15;
                    }
                } else {
                    i = i5 + 1;
                    byte b = bArr2[i5] & 255;
                    int i18 = i11 + i10;
                    while (i11 != i18) {
                        if (b == c[i11]) {
                            i2 = c[i11 + i10];
                            if (i == i6) {
                                Segment segment4 = segment2.f;
                                if (segment4 != null) {
                                    int i19 = segment4.b;
                                    bArr2 = segment4.f4083a;
                                    i6 = segment4.c;
                                    i = i19;
                                    segment2 = segment4 == segment ? null : segment4;
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            }
                        } else {
                            i11++;
                        }
                    }
                    return i7;
                }
                if (i2 >= 0) {
                    return i2;
                }
                i8 = -i2;
                i5 = i;
            }
            if (z) {
                return -2;
            }
            return i7;
        } else if (z) {
            return -2;
        } else {
            return -1;
        }
    }
}
