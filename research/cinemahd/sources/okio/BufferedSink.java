package okio;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;

public interface BufferedSink extends Sink, WritableByteChannel {
    long a(Source source) throws IOException;

    Buffer a();

    BufferedSink a(String str) throws IOException;

    BufferedSink a(ByteString byteString) throws IOException;

    BufferedSink e(long j) throws IOException;

    BufferedSink f(long j) throws IOException;

    void flush() throws IOException;

    Buffer j();

    BufferedSink k() throws IOException;

    BufferedSink l() throws IOException;

    OutputStream m();

    BufferedSink write(byte[] bArr) throws IOException;

    BufferedSink write(byte[] bArr, int i, int i2) throws IOException;

    BufferedSink writeByte(int i) throws IOException;

    BufferedSink writeInt(int i) throws IOException;

    BufferedSink writeShort(int i) throws IOException;
}
