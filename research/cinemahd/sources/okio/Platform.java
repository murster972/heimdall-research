package okio;

import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;

/* renamed from: okio.-Platform  reason: invalid class name */
public final class Platform {
    public static final String a(byte[] bArr) {
        Intrinsics.b(bArr, "$this$toUtf8String");
        return new String(bArr, Charsets.f6950a);
    }

    public static final byte[] a(String str) {
        Intrinsics.b(str, "$this$asUtf8ToByteArray");
        byte[] bytes = str.getBytes(Charsets.f6950a);
        Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        return bytes;
    }
}
