package okio;

import java.io.IOException;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;

public final class AsyncTimeout$source$1 implements Source {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AsyncTimeout f4065a;
    final /* synthetic */ Source b;

    AsyncTimeout$source$1(AsyncTimeout asyncTimeout, Source source) {
        this.f4065a = asyncTimeout;
        this.b = source;
    }

    public void close() {
        AsyncTimeout asyncTimeout = this.f4065a;
        asyncTimeout.enter();
        try {
            this.b.close();
            Unit unit = Unit.f6917a;
            if (asyncTimeout.exit()) {
                throw asyncTimeout.access$newTimeoutException((IOException) null);
            }
        } catch (IOException e) {
            e = e;
            if (asyncTimeout.exit()) {
                e = asyncTimeout.access$newTimeoutException(e);
            }
            throw e;
        } finally {
            boolean exit = asyncTimeout.exit();
        }
    }

    public long read(Buffer buffer, long j) {
        Intrinsics.b(buffer, "sink");
        AsyncTimeout asyncTimeout = this.f4065a;
        asyncTimeout.enter();
        try {
            long read = this.b.read(buffer, j);
            if (!asyncTimeout.exit()) {
                return read;
            }
            throw asyncTimeout.access$newTimeoutException((IOException) null);
        } catch (IOException e) {
            e = e;
            if (asyncTimeout.exit()) {
                e = asyncTimeout.access$newTimeoutException(e);
            }
            throw e;
        } finally {
            boolean exit = asyncTimeout.exit();
        }
    }

    public String toString() {
        return "AsyncTimeout.source(" + this.b + ')';
    }

    public AsyncTimeout timeout() {
        return this.f4065a;
    }
}
