package okio;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import kotlin.jvm.internal.Intrinsics;

final class SocketAsyncTimeout extends AsyncTimeout {

    /* renamed from: a  reason: collision with root package name */
    private final Logger f4085a = Logger.getLogger("okio.Okio");
    private final Socket b;

    public SocketAsyncTimeout(Socket socket) {
        Intrinsics.b(socket, "socket");
        this.b = socket;
    }

    /* access modifiers changed from: protected */
    public IOException newTimeoutException(IOException iOException) {
        SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
        if (iOException != null) {
            socketTimeoutException.initCause(iOException);
        }
        return socketTimeoutException;
    }

    /* access modifiers changed from: protected */
    public void timedOut() {
        try {
            this.b.close();
        } catch (Exception e) {
            Logger logger = this.f4085a;
            Level level = Level.WARNING;
            logger.log(level, "Failed to close timed out socket " + this.b, e);
        } catch (AssertionError e2) {
            if (Okio.a(e2)) {
                Logger logger2 = this.f4085a;
                Level level2 = Level.WARNING;
                logger2.log(level2, "Failed to close timed out socket " + this.b, e2);
                return;
            }
            throw e2;
        }
    }
}
