package okio;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.Inflater;
import kotlin.jvm.internal.Intrinsics;

public final class GzipSource implements Source {

    /* renamed from: a  reason: collision with root package name */
    private byte f4074a;
    private final RealBufferedSource b;
    private final Inflater c = new Inflater(true);
    private final InflaterSource d = new InflaterSource((BufferedSource) this.b, this.c);
    private final CRC32 e = new CRC32();

    public GzipSource(Source source) {
        Intrinsics.b(source, "source");
        this.b = new RealBufferedSource(source);
    }

    private final void a(Buffer buffer, long j, long j2) {
        Segment segment = buffer.f4066a;
        if (segment != null) {
            do {
                int i = segment.c;
                int i2 = segment.b;
                if (j >= ((long) (i - i2))) {
                    j -= (long) (i - i2);
                    segment = segment.f;
                } else {
                    while (j2 > 0) {
                        int i3 = (int) (((long) segment.b) + j);
                        int min = (int) Math.min((long) (segment.c - i3), j2);
                        this.e.update(segment.f4083a, i3, min);
                        j2 -= (long) min;
                        segment = segment.f;
                        if (segment != null) {
                            j = 0;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                    return;
                }
            } while (segment != null);
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a();
        throw null;
    }

    private final void j() throws IOException {
        this.b.c(10);
        byte h = this.b.f4081a.h(3);
        boolean z = true;
        boolean z2 = ((h >> 1) & 1) == 1;
        if (z2) {
            a(this.b.f4081a, 0, 10);
        }
        a("ID1ID2", 8075, (int) this.b.readShort());
        this.b.skip(8);
        if (((h >> 2) & 1) == 1) {
            this.b.c(2);
            if (z2) {
                a(this.b.f4081a, 0, 2);
            }
            long s = (long) this.b.f4081a.s();
            this.b.c(s);
            if (z2) {
                a(this.b.f4081a, 0, s);
            }
            this.b.skip(s);
        }
        if (((h >> 3) & 1) == 1) {
            long a2 = this.b.a((byte) 0);
            if (a2 != -1) {
                if (z2) {
                    a(this.b.f4081a, 0, a2 + 1);
                }
                this.b.skip(a2 + 1);
            } else {
                throw new EOFException();
            }
        }
        if (((h >> 4) & 1) != 1) {
            z = false;
        }
        if (z) {
            long a3 = this.b.a((byte) 0);
            if (a3 != -1) {
                if (z2) {
                    a(this.b.f4081a, 0, a3 + 1);
                }
                this.b.skip(a3 + 1);
            } else {
                throw new EOFException();
            }
        }
        if (z2) {
            a("FHCRC", (int) this.b.k(), (int) (short) ((int) this.e.getValue()));
            this.e.reset();
        }
    }

    private final void k() throws IOException {
        a("CRC", this.b.j(), (int) this.e.getValue());
        a("ISIZE", this.b.j(), (int) this.c.getBytesWritten());
    }

    public void close() throws IOException {
        this.d.close();
    }

    public long read(Buffer buffer, long j) throws IOException {
        Intrinsics.b(buffer, "sink");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (i == 0) {
            return 0;
        } else {
            if (this.f4074a == 0) {
                j();
                this.f4074a = 1;
            }
            if (this.f4074a == 1) {
                long u = buffer.u();
                long read = this.d.read(buffer, j);
                if (read != -1) {
                    a(buffer, u, read);
                    return read;
                }
                this.f4074a = 2;
            }
            if (this.f4074a == 2) {
                k();
                this.f4074a = 3;
                if (!this.b.e()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    public Timeout timeout() {
        return this.b.timeout();
    }

    private final void a(String str, int i, int i2) {
        if (i2 != i) {
            Object[] objArr = {str, Integer.valueOf(i2), Integer.valueOf(i)};
            String format = String.format("%s: actual 0x%08x != expected 0x%08x", Arrays.copyOf(objArr, objArr.length));
            Intrinsics.a((Object) format, "java.lang.String.format(this, *args)");
            throw new IOException(format);
        }
    }
}
