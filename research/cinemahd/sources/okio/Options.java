package okio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.RandomAccess;
import kotlin.TypeCastException;
import kotlin.collections.AbstractList;
import kotlin.jvm.internal.Intrinsics;

public final class Options extends AbstractList<ByteString> implements RandomAccess {
    public static final Companion d = new Companion((DefaultConstructorMarker) null);
    private final ByteString[] b;
    private final int[] c;

    public static final class Companion {
        private Companion() {
        }

        public final Options a(ByteString... byteStringArr) {
            ByteString[] byteStringArr2 = byteStringArr;
            Intrinsics.b(byteStringArr2, "byteStrings");
            int i = 0;
            if (byteStringArr2.length == 0) {
                return new Options(new ByteString[0], new int[]{0, -1}, (DefaultConstructorMarker) null);
            }
            List g = ArraysKt___ArraysKt.g(byteStringArr);
            CollectionsKt__MutableCollectionsJVMKt.c(g);
            ArrayList arrayList = new ArrayList(byteStringArr2.length);
            for (ByteString byteString : byteStringArr2) {
                arrayList.add(-1);
            }
            Object[] array = arrayList.toArray(new Integer[0]);
            if (array != null) {
                Integer[] numArr = (Integer[]) array;
                List d = CollectionsKt__CollectionsKt.d((Integer[]) Arrays.copyOf(numArr, numArr.length));
                int length = byteStringArr2.length;
                int i2 = 0;
                int i3 = 0;
                while (i2 < length) {
                    d.set(CollectionsKt__CollectionsKt.a(g, byteStringArr2[i2], 0, 0, 6, (Object) null), Integer.valueOf(i3));
                    i2++;
                    i3++;
                }
                if (((ByteString) g.get(0)).size() > 0) {
                    int i4 = 0;
                    while (i4 < g.size()) {
                        ByteString byteString2 = (ByteString) g.get(i4);
                        int i5 = i4 + 1;
                        int i6 = i5;
                        while (i6 < g.size()) {
                            ByteString byteString3 = (ByteString) g.get(i6);
                            if (!byteString3.b(byteString2)) {
                                continue;
                                break;
                            }
                            if (!(byteString3.size() != byteString2.size())) {
                                throw new IllegalArgumentException(("duplicate option: " + byteString3).toString());
                            } else if (((Number) d.get(i6)).intValue() > ((Number) d.get(i4)).intValue()) {
                                g.remove(i6);
                                d.remove(i6);
                            } else {
                                i6++;
                            }
                        }
                        i4 = i5;
                    }
                    Buffer buffer = new Buffer();
                    a(this, 0, buffer, 0, g, 0, 0, d, 53, (Object) null);
                    int[] iArr = new int[((int) a(buffer))];
                    while (!buffer.e()) {
                        iArr[i] = buffer.readInt();
                        i++;
                    }
                    Object[] copyOf = Arrays.copyOf(byteStringArr2, byteStringArr2.length);
                    Intrinsics.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
                    return new Options((ByteString[]) copyOf, iArr, (DefaultConstructorMarker) null);
                }
                throw new IllegalArgumentException("the empty byte string is not a supported option".toString());
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        static /* synthetic */ void a(Companion companion, long j, Buffer buffer, int i, List list, int i2, int i3, List list2, int i4, Object obj) {
            companion.a((i4 & 1) != 0 ? 0 : j, buffer, (i4 & 4) != 0 ? 0 : i, list, (i4 & 16) != 0 ? 0 : i2, (i4 & 32) != 0 ? list.size() : i3, list2);
        }

        private final void a(long j, Buffer buffer, int i, List<? extends ByteString> list, int i2, int i3, List<Integer> list2) {
            int i4;
            int i5;
            int i6;
            int i7;
            Buffer buffer2;
            Buffer buffer3 = buffer;
            int i8 = i;
            List<? extends ByteString> list3 = list;
            int i9 = i2;
            int i10 = i3;
            List<Integer> list4 = list2;
            if (i9 < i10) {
                int i11 = i9;
                while (i11 < i10) {
                    if (((ByteString) list3.get(i11)).size() >= i8) {
                        i11++;
                    } else {
                        throw new IllegalArgumentException("Failed requirement.".toString());
                    }
                }
                ByteString byteString = (ByteString) list.get(i2);
                ByteString byteString2 = (ByteString) list3.get(i10 - 1);
                if (i8 == byteString.size()) {
                    int intValue = list4.get(i9).intValue();
                    int i12 = i9 + 1;
                    i4 = i12;
                    i5 = intValue;
                    byteString = (ByteString) list3.get(i12);
                } else {
                    i4 = i9;
                    i5 = -1;
                }
                if (byteString.a(i8) != byteString2.a(i8)) {
                    int i13 = 1;
                    for (int i14 = i4 + 1; i14 < i10; i14++) {
                        if (((ByteString) list3.get(i14 - 1)).a(i8) != ((ByteString) list3.get(i14)).a(i8)) {
                            i13++;
                        }
                    }
                    long a2 = j + a(buffer3) + ((long) 2) + ((long) (i13 * 2));
                    buffer3.writeInt(i13);
                    buffer3.writeInt(i5);
                    for (int i15 = i4; i15 < i10; i15++) {
                        byte a3 = ((ByteString) list3.get(i15)).a(i8);
                        if (i15 == i4 || a3 != ((ByteString) list3.get(i15 - 1)).a(i8)) {
                            buffer3.writeInt((int) a3 & 255);
                        }
                    }
                    Buffer buffer4 = new Buffer();
                    while (i4 < i10) {
                        byte a4 = ((ByteString) list3.get(i4)).a(i8);
                        int i16 = i4 + 1;
                        int i17 = i16;
                        while (true) {
                            if (i17 >= i10) {
                                i6 = i10;
                                break;
                            } else if (a4 != ((ByteString) list3.get(i17)).a(i8)) {
                                i6 = i17;
                                break;
                            } else {
                                i17++;
                            }
                        }
                        if (i16 == i6 && i8 + 1 == ((ByteString) list3.get(i4)).size()) {
                            buffer3.writeInt(list4.get(i4).intValue());
                            i7 = i6;
                            buffer2 = buffer4;
                        } else {
                            buffer3.writeInt(((int) (a2 + a(buffer4))) * -1);
                            i7 = i6;
                            buffer2 = buffer4;
                            a(a2, buffer4, i8 + 1, list, i4, i6, list2);
                        }
                        buffer4 = buffer2;
                        i4 = i7;
                    }
                    buffer3.a((Source) buffer4);
                    return;
                }
                int min = Math.min(byteString.size(), byteString2.size());
                int i18 = i8;
                int i19 = 0;
                while (i18 < min && byteString.a(i18) == byteString2.a(i18)) {
                    i19++;
                    i18++;
                }
                long a5 = j + a(buffer3) + ((long) 2) + ((long) i19) + 1;
                buffer3.writeInt(-i19);
                buffer3.writeInt(i5);
                int i20 = i8 + i19;
                while (i8 < i20) {
                    buffer3.writeInt((int) byteString.a(i8) & 255);
                    i8++;
                }
                if (i4 + 1 == i10) {
                    if (i20 == ((ByteString) list3.get(i4)).size()) {
                        buffer3.writeInt(list4.get(i4).intValue());
                        return;
                    }
                    throw new IllegalStateException("Check failed.".toString());
                }
                Buffer buffer5 = new Buffer();
                buffer3.writeInt(((int) (a(buffer5) + a5)) * -1);
                a(a5, buffer5, i20, list, i4, i3, list2);
                buffer3.a((Source) buffer5);
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        private final long a(Buffer buffer) {
            return buffer.u() / ((long) 4);
        }
    }

    public /* synthetic */ Options(ByteString[] byteStringArr, int[] iArr, DefaultConstructorMarker defaultConstructorMarker) {
        this(byteStringArr, iArr);
    }

    public /* bridge */ boolean a(ByteString byteString) {
        return super.contains(byteString);
    }

    public /* bridge */ int b(ByteString byteString) {
        return super.indexOf(byteString);
    }

    public /* bridge */ int c(ByteString byteString) {
        return super.lastIndexOf(byteString);
    }

    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof ByteString) {
            return a((ByteString) obj);
        }
        return false;
    }

    public final /* bridge */ int indexOf(Object obj) {
        if (obj instanceof ByteString) {
            return b((ByteString) obj);
        }
        return -1;
    }

    public final /* bridge */ int lastIndexOf(Object obj) {
        if (obj instanceof ByteString) {
            return c((ByteString) obj);
        }
        return -1;
    }

    private Options(ByteString[] byteStringArr, int[] iArr) {
        this.b = byteStringArr;
        this.c = iArr;
    }

    public int a() {
        return this.b.length;
    }

    public final ByteString[] b() {
        return this.b;
    }

    public final int[] c() {
        return this.c;
    }

    public ByteString get(int i) {
        return this.b[i];
    }
}
