package okio;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;

public interface BufferedSource extends Source, ReadableByteChannel {
    int a(Options options) throws IOException;

    long a(Sink sink) throws IOException;

    String a(long j) throws IOException;

    String a(Charset charset) throws IOException;

    Buffer a();

    void a(Buffer buffer, long j) throws IOException;

    boolean a(long j, ByteString byteString) throws IOException;

    InputStream b();

    byte[] b(long j) throws IOException;

    String c() throws IOException;

    void c(long j) throws IOException;

    ByteString d(long j) throws IOException;

    byte[] d() throws IOException;

    boolean e() throws IOException;

    long f() throws IOException;

    ByteString g() throws IOException;

    String h() throws IOException;

    long i() throws IOException;

    BufferedSource peek();

    byte readByte() throws IOException;

    void readFully(byte[] bArr) throws IOException;

    int readInt() throws IOException;

    long readLong() throws IOException;

    short readShort() throws IOException;

    boolean request(long j) throws IOException;

    void skip(long j) throws IOException;
}
