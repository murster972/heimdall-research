package okio;

import kotlin.jvm.internal.Intrinsics;

public final class PeekSource implements Source {

    /* renamed from: a  reason: collision with root package name */
    private final Buffer f4078a = this.f.a();
    private Segment b;
    private int c;
    private boolean d;
    private long e;
    private final BufferedSource f;

    public PeekSource(BufferedSource bufferedSource) {
        Intrinsics.b(bufferedSource, "upstream");
        this.f = bufferedSource;
        Segment segment = this.f4078a.f4066a;
        this.b = segment;
        this.c = segment != null ? segment.b : -1;
    }

    public void close() {
        this.d = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        if (r5 == r7.b) goto L_0x002f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long read(okio.Buffer r9, long r10) {
        /*
            r8 = this;
            java.lang.String r0 = "sink"
            kotlin.jvm.internal.Intrinsics.b(r9, r0)
            r0 = 0
            r1 = 0
            r3 = 1
            int r4 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r4 < 0) goto L_0x000f
            r5 = 1
            goto L_0x0010
        L_0x000f:
            r5 = 0
        L_0x0010:
            if (r5 == 0) goto L_0x0090
            boolean r5 = r8.d
            r5 = r5 ^ r3
            if (r5 == 0) goto L_0x0084
            okio.Segment r5 = r8.b
            r6 = 0
            if (r5 == 0) goto L_0x002f
            okio.Buffer r7 = r8.f4078a
            okio.Segment r7 = r7.f4066a
            if (r5 != r7) goto L_0x0030
            int r5 = r8.c
            if (r7 == 0) goto L_0x002b
            int r7 = r7.b
            if (r5 != r7) goto L_0x0030
            goto L_0x002f
        L_0x002b:
            kotlin.jvm.internal.Intrinsics.a()
            throw r6
        L_0x002f:
            r0 = 1
        L_0x0030:
            if (r0 == 0) goto L_0x0078
            if (r4 != 0) goto L_0x0035
            return r1
        L_0x0035:
            okio.BufferedSource r0 = r8.f
            long r1 = r8.e
            r3 = 1
            long r1 = r1 + r3
            boolean r0 = r0.request(r1)
            if (r0 != 0) goto L_0x0045
            r9 = -1
            return r9
        L_0x0045:
            okio.Segment r0 = r8.b
            if (r0 != 0) goto L_0x005c
            okio.Buffer r0 = r8.f4078a
            okio.Segment r0 = r0.f4066a
            if (r0 == 0) goto L_0x005c
            r8.b = r0
            if (r0 == 0) goto L_0x0058
            int r0 = r0.b
            r8.c = r0
            goto L_0x005c
        L_0x0058:
            kotlin.jvm.internal.Intrinsics.a()
            throw r6
        L_0x005c:
            okio.Buffer r0 = r8.f4078a
            long r0 = r0.u()
            long r2 = r8.e
            long r0 = r0 - r2
            long r10 = java.lang.Math.min(r10, r0)
            okio.Buffer r2 = r8.f4078a
            long r4 = r8.e
            r3 = r9
            r6 = r10
            r2.a((okio.Buffer) r3, (long) r4, (long) r6)
            long r0 = r8.e
            long r0 = r0 + r10
            r8.e = r0
            return r10
        L_0x0078:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "Peek source is invalid because upstream source was used"
            java.lang.String r10 = r10.toString()
            r9.<init>(r10)
            throw r9
        L_0x0084:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "closed"
            java.lang.String r10 = r10.toString()
            r9.<init>(r10)
            throw r9
        L_0x0090:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r0 = "byteCount < 0: "
            r9.append(r0)
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            java.lang.String r9 = r9.toString()
            r10.<init>(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.PeekSource.read(okio.Buffer, long):long");
    }

    public Timeout timeout() {
        return this.f.timeout();
    }
}
