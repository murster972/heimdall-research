package okio;

import com.facebook.common.util.UriUtil;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import okio.internal.ByteStringKt;

public class ByteString implements Serializable, Comparable<ByteString> {
    public static final ByteString c = new ByteString(new byte[0]);
    public static final Companion d = new Companion((DefaultConstructorMarker) null);
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    private transient int f4070a;
    private transient String b;
    private final byte[] data;

    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ ByteString a(Companion companion, byte[] bArr, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = 0;
            }
            if ((i3 & 2) != 0) {
                i2 = bArr.length;
            }
            return companion.a(bArr, i, i2);
        }

        public final ByteString a(String str, Charset charset) {
            Intrinsics.b(str, "$this$encode");
            Intrinsics.b(charset, "charset");
            byte[] bytes = str.getBytes(charset);
            Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return new ByteString(bytes);
        }

        public final ByteString b(String str) {
            Intrinsics.b(str, "$this$decodeHex");
            if (str.length() % 2 == 0) {
                byte[] bArr = new byte[(str.length() / 2)];
                int length = bArr.length;
                for (int i = 0; i < length; i++) {
                    int i2 = i * 2;
                    bArr[i] = (byte) ((ByteStringKt.b(str.charAt(i2)) << 4) + ByteStringKt.b(str.charAt(i2 + 1)));
                }
                return new ByteString(bArr);
            }
            throw new IllegalArgumentException(("Unexpected hex string: " + str).toString());
        }

        public final ByteString c(String str) {
            Intrinsics.b(str, "$this$encodeUtf8");
            ByteString byteString = new ByteString(Platform.a(str));
            byteString.b(str);
            return byteString;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final ByteString a(InputStream inputStream, int i) throws IOException {
            Intrinsics.b(inputStream, "$this$readByteString");
            int i2 = 0;
            if (i >= 0) {
                byte[] bArr = new byte[i];
                while (i2 < i) {
                    int read = inputStream.read(bArr, i2, i - i2);
                    if (read != -1) {
                        i2 += read;
                    } else {
                        throw new EOFException();
                    }
                }
                return new ByteString(bArr);
            }
            throw new IllegalArgumentException(("byteCount < 0: " + i).toString());
        }

        public final ByteString a(byte... bArr) {
            Intrinsics.b(bArr, UriUtil.DATA_SCHEME);
            byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
            Intrinsics.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
            return new ByteString(copyOf);
        }

        public final ByteString a(byte[] bArr, int i, int i2) {
            Intrinsics.b(bArr, "$this$toByteString");
            Util.a((long) bArr.length, (long) i, (long) i2);
            return new ByteString(ArraysKt___ArraysJvmKt.a(bArr, i, i2 + i));
        }

        public final ByteString a(String str) {
            Intrinsics.b(str, "$this$decodeBase64");
            byte[] a2 = Base64.a(str);
            if (a2 != null) {
                return new ByteString(a2);
            }
            return null;
        }
    }

    public ByteString(byte[] bArr) {
        Intrinsics.b(bArr, UriUtil.DATA_SCHEME);
        this.data = bArr;
    }

    public static final ByteString a(byte... bArr) {
        return d.a(bArr);
    }

    public static final ByteString c(String str) {
        return d.a(str);
    }

    public static final ByteString d(String str) {
        return d.c(str);
    }

    private final void readObject(ObjectInputStream objectInputStream) throws IOException {
        ByteString a2 = d.a((InputStream) objectInputStream, objectInputStream.readInt());
        Field declaredField = ByteString.class.getDeclaredField(UriUtil.DATA_SCHEME);
        Intrinsics.a((Object) declaredField, "field");
        declaredField.setAccessible(true);
        declaredField.set(this, a2.data);
    }

    private final void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(this.data.length);
        objectOutputStream.write(this.data);
    }

    public ByteString a(String str) {
        Intrinsics.b(str, "algorithm");
        byte[] digest = MessageDigest.getInstance(str).digest(this.data);
        Intrinsics.a((Object) digest, "MessageDigest.getInstance(algorithm).digest(data)");
        return new ByteString(digest);
    }

    public final byte[] b() {
        return this.data;
    }

    public final int c() {
        return this.f4070a;
    }

    public int d() {
        return b().length;
    }

    public final String e() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ByteString) {
            ByteString byteString = (ByteString) obj;
            return byteString.size() == b().length && byteString.a(0, b(), 0, b().length);
        }
    }

    public String f() {
        char[] cArr = new char[(b().length * 2)];
        int i = 0;
        for (byte b2 : b()) {
            int i2 = i + 1;
            cArr[i] = ByteStringKt.a()[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = ByteStringKt.a()[b2 & 15];
        }
        return new String(cArr);
    }

    public byte[] g() {
        return b();
    }

    public ByteString h() {
        return a("MD5");
    }

    public int hashCode() {
        int c2 = c();
        if (c2 != 0) {
            return c2;
        }
        int hashCode = Arrays.hashCode(b());
        c(hashCode);
        return hashCode;
    }

    public ByteString i() {
        return a("SHA-1");
    }

    public ByteString j() {
        return a("SHA-256");
    }

    public ByteString k() {
        byte b2;
        int i = 0;
        while (i < b().length) {
            byte b3 = b()[i];
            byte b4 = (byte) 65;
            if (b3 < b4 || b3 > (b2 = (byte) 90)) {
                i++;
            } else {
                byte[] b5 = b();
                byte[] copyOf = Arrays.copyOf(b5, b5.length);
                Intrinsics.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
                copyOf[i] = (byte) (b3 + 32);
                for (int i2 = i + 1; i2 < copyOf.length; i2++) {
                    byte b6 = copyOf[i2];
                    if (b6 >= b4 && b6 <= b2) {
                        copyOf[i2] = (byte) (b6 + 32);
                    }
                }
                return new ByteString(copyOf);
            }
        }
        return this;
    }

    public byte[] l() {
        byte[] b2 = b();
        byte[] copyOf = Arrays.copyOf(b2, b2.length);
        Intrinsics.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        return copyOf;
    }

    public String m() {
        String e = e();
        if (e != null) {
            return e;
        }
        String a2 = Platform.a(g());
        b(a2);
        return a2;
    }

    public final int size() {
        return d();
    }

    public String toString() {
        ByteString byteString;
        boolean z = true;
        if (b().length == 0) {
            return "[size=0]";
        }
        int a2 = ByteStringKt.b(b(), 64);
        if (a2 != -1) {
            String m = m();
            if (m != null) {
                String substring = m.substring(0, a2);
                Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                String a3 = StringsKt__StringsJVMKt.a(StringsKt__StringsJVMKt.a(StringsKt__StringsJVMKt.a(substring, "\\", "\\\\", false, 4, (Object) null), ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, "\\n", false, 4, (Object) null), "\r", "\\r", false, 4, (Object) null);
                if (a2 < m.length()) {
                    return "[size=" + b().length + " text=" + a3 + "…]";
                }
                return "[text=" + a3 + ']';
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        } else if (b().length <= 64) {
            return "[hex=" + f() + ']';
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("[size=");
            sb.append(b().length);
            sb.append(" hex=");
            if (64 > b().length) {
                z = false;
            }
            if (z) {
                if (64 == b().length) {
                    byteString = this;
                } else {
                    byteString = new ByteString(ArraysKt___ArraysJvmKt.a(b(), 0, 64));
                }
                sb.append(byteString.f());
                sb.append("…]");
                return sb.toString();
            }
            throw new IllegalArgumentException(("endIndex > length(" + b().length + ')').toString());
        }
    }

    public final byte a(int i) {
        return b(i);
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void c(int i) {
        this.f4070a = i;
    }

    public void a(Buffer buffer, int i, int i2) {
        Intrinsics.b(buffer, "buffer");
        ByteStringKt.a(this, buffer, i, i2);
    }

    public byte b(int i) {
        return b()[i];
    }

    public String a() {
        return Base64.a(b(), (byte[]) null, 1, (Object) null);
    }

    public final boolean b(ByteString byteString) {
        Intrinsics.b(byteString, "prefix");
        return a(0, byteString, 0, byteString.size());
    }

    public boolean a(int i, ByteString byteString, int i2, int i3) {
        Intrinsics.b(byteString, "other");
        return byteString.a(i2, b(), i, i3);
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        Intrinsics.b(bArr, "other");
        return i >= 0 && i <= b().length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && Util.a(b(), i, bArr, i2, i3);
    }

    /* renamed from: a */
    public int compareTo(ByteString byteString) {
        Intrinsics.b(byteString, "other");
        int size = size();
        int size2 = byteString.size();
        int min = Math.min(size, size2);
        int i = 0;
        while (true) {
            if (i < min) {
                byte a2 = a(i) & 255;
                byte a3 = byteString.a(i) & 255;
                if (a2 == a3) {
                    i++;
                } else if (a2 < a3) {
                    return -1;
                }
            } else if (size == size2) {
                return 0;
            } else {
                if (size < size2) {
                    return -1;
                }
            }
        }
        return 1;
    }
}
