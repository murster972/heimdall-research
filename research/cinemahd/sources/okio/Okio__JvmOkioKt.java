package okio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import kotlin.jvm.internal.Intrinsics;

final /* synthetic */ class Okio__JvmOkioKt {
    public static final Sink a(OutputStream outputStream) {
        Intrinsics.b(outputStream, "$this$sink");
        return new OutputStreamSink(outputStream, new Timeout());
    }

    public static final Source b(Socket socket) throws IOException {
        Intrinsics.b(socket, "$this$source");
        SocketAsyncTimeout socketAsyncTimeout = new SocketAsyncTimeout(socket);
        InputStream inputStream = socket.getInputStream();
        Intrinsics.a((Object) inputStream, "getInputStream()");
        return socketAsyncTimeout.source(new InputStreamSource(inputStream, socketAsyncTimeout));
    }

    public static final Source a(InputStream inputStream) {
        Intrinsics.b(inputStream, "$this$source");
        return new InputStreamSource(inputStream, new Timeout());
    }

    public static final Sink a(Socket socket) throws IOException {
        Intrinsics.b(socket, "$this$sink");
        SocketAsyncTimeout socketAsyncTimeout = new SocketAsyncTimeout(socket);
        OutputStream outputStream = socket.getOutputStream();
        Intrinsics.a((Object) outputStream, "getOutputStream()");
        return socketAsyncTimeout.sink(new OutputStreamSink(outputStream, socketAsyncTimeout));
    }

    public static final Source b(File file) throws FileNotFoundException {
        Intrinsics.b(file, "$this$source");
        return Okio.a((InputStream) new FileInputStream(file));
    }

    public static final Sink a(File file, boolean z) throws FileNotFoundException {
        Intrinsics.b(file, "$this$sink");
        return Okio.a((OutputStream) new FileOutputStream(file, z));
    }

    public static /* synthetic */ Sink a(File file, boolean z, int i, Object obj) throws FileNotFoundException {
        if ((i & 1) != 0) {
            z = false;
        }
        return Okio.a(file, z);
    }

    public static final Sink a(File file) throws FileNotFoundException {
        Intrinsics.b(file, "$this$appendingSink");
        return Okio.a((OutputStream) new FileOutputStream(file, true));
    }

    public static final boolean a(AssertionError assertionError) {
        Intrinsics.b(assertionError, "$this$isAndroidGetsocknameError");
        if (assertionError.getCause() == null) {
            return false;
        }
        String message = assertionError.getMessage();
        return message != null ? StringsKt__StringsKt.a((CharSequence) message, (CharSequence) "getsockname failed", false, 2, (Object) null) : false;
    }
}
