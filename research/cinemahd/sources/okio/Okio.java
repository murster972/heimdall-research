package okio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public final class Okio {
    public static final Sink a(File file) throws FileNotFoundException {
        return Okio__JvmOkioKt.a(file);
    }

    public static final Sink b(File file) throws FileNotFoundException {
        return Okio__JvmOkioKt.a(file, false, 1, (Object) null);
    }

    public static final Source b(Socket socket) throws IOException {
        return Okio__JvmOkioKt.b(socket);
    }

    public static final Source c(File file) throws FileNotFoundException {
        return Okio__JvmOkioKt.b(file);
    }

    public static final Sink a() {
        return Okio__OkioKt.a();
    }

    public static final BufferedSink a(Sink sink) {
        return Okio__OkioKt.a(sink);
    }

    public static final BufferedSource a(Source source) {
        return Okio__OkioKt.a(source);
    }

    public static final boolean a(AssertionError assertionError) {
        return Okio__JvmOkioKt.a(assertionError);
    }

    public static final Sink a(File file, boolean z) throws FileNotFoundException {
        return Okio__JvmOkioKt.a(file, z);
    }

    public static final Sink a(OutputStream outputStream) {
        return Okio__JvmOkioKt.a(outputStream);
    }

    public static final Sink a(Socket socket) throws IOException {
        return Okio__JvmOkioKt.a(socket);
    }

    public static final Source a(InputStream inputStream) {
        return Okio__JvmOkioKt.a(inputStream);
    }
}
