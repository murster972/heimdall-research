package okio;

import com.facebook.imageutils.JfifUtil;
import kotlin.jvm.internal.Intrinsics;
import okio.internal.ByteStringKt;

/* renamed from: okio.-Util  reason: invalid class name */
public final class Util {
    public static final int a(int i) {
        return ((i & JfifUtil.MARKER_FIRST_BYTE) << 24) | ((-16777216 & i) >>> 24) | ((16711680 & i) >>> 8) | ((65280 & i) << 8);
    }

    public static final short a(short s) {
        short s2 = s & 65535;
        return (short) (((s2 & 255) << 8) | ((65280 & s2) >>> 8));
    }

    public static final void a(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException("size=" + j + " offset=" + j2 + " byteCount=" + j3);
        }
    }

    public static final String b(int i) {
        if (i == 0) {
            return "0";
        }
        int i2 = 0;
        char[] cArr = {ByteStringKt.a()[(i >> 28) & 15], ByteStringKt.a()[(i >> 24) & 15], ByteStringKt.a()[(i >> 20) & 15], ByteStringKt.a()[(i >> 16) & 15], ByteStringKt.a()[(i >> 12) & 15], ByteStringKt.a()[(i >> 8) & 15], ByteStringKt.a()[(i >> 4) & 15], ByteStringKt.a()[i & 15]};
        while (i2 < cArr.length && cArr[i2] == '0') {
            i2++;
        }
        return new String(cArr, i2, cArr.length - i2);
    }

    public static final boolean a(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        Intrinsics.b(bArr, "a");
        Intrinsics.b(bArr2, "b");
        for (int i4 = 0; i4 < i3; i4++) {
            if (bArr[i4 + i] != bArr2[i4 + i2]) {
                return false;
            }
        }
        return true;
    }

    public static final String a(byte b) {
        return new String(new char[]{ByteStringKt.a()[(b >> 4) & 15], ByteStringKt.a()[b & 15]});
    }
}
