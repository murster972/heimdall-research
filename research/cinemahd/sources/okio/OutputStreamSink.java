package okio;

import java.io.OutputStream;
import kotlin.jvm.internal.Intrinsics;

final class OutputStreamSink implements Sink {

    /* renamed from: a  reason: collision with root package name */
    private final OutputStream f4077a;
    private final Timeout b;

    public OutputStreamSink(OutputStream outputStream, Timeout timeout) {
        Intrinsics.b(outputStream, "out");
        Intrinsics.b(timeout, "timeout");
        this.f4077a = outputStream;
        this.b = timeout;
    }

    public void close() {
        this.f4077a.close();
    }

    public void flush() {
        this.f4077a.flush();
    }

    public Timeout timeout() {
        return this.b;
    }

    public String toString() {
        return "sink(" + this.f4077a + ')';
    }

    public void write(Buffer buffer, long j) {
        Intrinsics.b(buffer, "source");
        Util.a(buffer.u(), 0, j);
        while (j > 0) {
            this.b.throwIfReached();
            Segment segment = buffer.f4066a;
            if (segment != null) {
                int min = (int) Math.min(j, (long) (segment.c - segment.b));
                this.f4077a.write(segment.f4083a, segment.b, min);
                segment.b += min;
                long j2 = (long) min;
                j -= j2;
                buffer.j(buffer.u() - j2);
                if (segment.b == segment.c) {
                    buffer.f4066a = segment.b();
                    SegmentPool.c.a(segment);
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        }
    }
}
