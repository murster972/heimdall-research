package okio;

import com.google.ar.core.ImageMetadata;
import java.io.IOException;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;

public final class AsyncTimeout$sink$1 implements Sink {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AsyncTimeout f4064a;
    final /* synthetic */ Sink b;

    AsyncTimeout$sink$1(AsyncTimeout asyncTimeout, Sink sink) {
        this.f4064a = asyncTimeout;
        this.b = sink;
    }

    public void close() {
        AsyncTimeout asyncTimeout = this.f4064a;
        asyncTimeout.enter();
        try {
            this.b.close();
            Unit unit = Unit.f6917a;
            if (asyncTimeout.exit()) {
                throw asyncTimeout.access$newTimeoutException((IOException) null);
            }
        } catch (IOException e) {
            e = e;
            if (asyncTimeout.exit()) {
                e = asyncTimeout.access$newTimeoutException(e);
            }
            throw e;
        } finally {
            boolean exit = asyncTimeout.exit();
        }
    }

    public void flush() {
        AsyncTimeout asyncTimeout = this.f4064a;
        asyncTimeout.enter();
        try {
            this.b.flush();
            Unit unit = Unit.f6917a;
            if (asyncTimeout.exit()) {
                throw asyncTimeout.access$newTimeoutException((IOException) null);
            }
        } catch (IOException e) {
            e = e;
            if (asyncTimeout.exit()) {
                e = asyncTimeout.access$newTimeoutException(e);
            }
            throw e;
        } finally {
            boolean exit = asyncTimeout.exit();
        }
    }

    public String toString() {
        return "AsyncTimeout.sink(" + this.b + ')';
    }

    public void write(Buffer buffer, long j) {
        Intrinsics.b(buffer, "source");
        Util.a(buffer.u(), 0, j);
        while (true) {
            long j2 = 0;
            if (j > 0) {
                Segment segment = buffer.f4066a;
                if (segment != null) {
                    while (true) {
                        if (j2 >= ((long) ImageMetadata.CONTROL_AE_ANTIBANDING_MODE)) {
                            break;
                        }
                        j2 += (long) (segment.c - segment.b);
                        if (j2 >= j) {
                            j2 = j;
                            break;
                        }
                        segment = segment.f;
                        if (segment == null) {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                    AsyncTimeout asyncTimeout = this.f4064a;
                    asyncTimeout.enter();
                    try {
                        this.b.write(buffer, j2);
                        Unit unit = Unit.f6917a;
                        if (!asyncTimeout.exit()) {
                            j -= j2;
                        } else {
                            throw asyncTimeout.access$newTimeoutException((IOException) null);
                        }
                    } catch (IOException e) {
                        e = e;
                        if (asyncTimeout.exit()) {
                            e = asyncTimeout.access$newTimeoutException(e);
                        }
                        throw e;
                    } finally {
                        boolean exit = asyncTimeout.exit();
                    }
                } else {
                    Intrinsics.a();
                    throw null;
                }
            } else {
                return;
            }
        }
    }

    public AsyncTimeout timeout() {
        return this.f4064a;
    }
}
