package okio;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import kotlin.jvm.internal.Intrinsics;

public final class InflaterSource implements Source {

    /* renamed from: a  reason: collision with root package name */
    private int f4075a;
    private boolean b;
    private final BufferedSource c;
    private final Inflater d;

    public InflaterSource(BufferedSource bufferedSource, Inflater inflater) {
        Intrinsics.b(bufferedSource, "source");
        Intrinsics.b(inflater, "inflater");
        this.c = bufferedSource;
        this.d = inflater;
    }

    private final void k() {
        int i = this.f4075a;
        if (i != 0) {
            int remaining = i - this.d.getRemaining();
            this.f4075a -= remaining;
            this.c.skip((long) remaining);
        }
    }

    public final long b(Buffer buffer, long j) throws IOException {
        Intrinsics.b(buffer, "sink");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (!(!this.b)) {
            throw new IllegalStateException("closed".toString());
        } else if (i == 0) {
            return 0;
        } else {
            try {
                Segment b2 = buffer.b(1);
                int min = (int) Math.min(j, (long) (8192 - b2.c));
                j();
                int inflate = this.d.inflate(b2.f4083a, b2.c, min);
                k();
                if (inflate > 0) {
                    b2.c += inflate;
                    long j2 = (long) inflate;
                    buffer.j(buffer.u() + j2);
                    return j2;
                }
                if (b2.b == b2.c) {
                    buffer.f4066a = b2.b();
                    SegmentPool.c.a(b2);
                }
                return 0;
            } catch (DataFormatException e) {
                throw new IOException(e);
            }
        }
    }

    public void close() throws IOException {
        if (!this.b) {
            this.d.end();
            this.b = true;
            this.c.close();
        }
    }

    public final boolean j() throws IOException {
        if (!this.d.needsInput()) {
            return false;
        }
        if (this.c.e()) {
            return true;
        }
        Segment segment = this.c.a().f4066a;
        if (segment != null) {
            int i = segment.c;
            int i2 = segment.b;
            this.f4075a = i - i2;
            this.d.setInput(segment.f4083a, i2, this.f4075a);
            return false;
        }
        Intrinsics.a();
        throw null;
    }

    public long read(Buffer buffer, long j) throws IOException {
        Intrinsics.b(buffer, "sink");
        do {
            long b2 = b(buffer, j);
            if (b2 > 0) {
                return b2;
            }
            if (this.d.finished() || this.d.needsDictionary()) {
                return -1;
            }
        } while (!this.c.e());
        throw new EOFException("source exhausted prematurely");
    }

    public Timeout timeout() {
        return this.c.timeout();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public InflaterSource(Source source, Inflater inflater) {
        this(Okio.a(source), inflater);
        Intrinsics.b(source, "source");
        Intrinsics.b(inflater, "inflater");
    }
}
