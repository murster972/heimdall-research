package okio;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.Intrinsics;

public class ForwardingTimeout extends Timeout {

    /* renamed from: a  reason: collision with root package name */
    private Timeout f4072a;

    public ForwardingTimeout(Timeout timeout) {
        Intrinsics.b(timeout, "delegate");
        this.f4072a = timeout;
    }

    public final Timeout a() {
        return this.f4072a;
    }

    public Timeout clearDeadline() {
        return this.f4072a.clearDeadline();
    }

    public Timeout clearTimeout() {
        return this.f4072a.clearTimeout();
    }

    public long deadlineNanoTime() {
        return this.f4072a.deadlineNanoTime();
    }

    public boolean hasDeadline() {
        return this.f4072a.hasDeadline();
    }

    public void throwIfReached() throws IOException {
        this.f4072a.throwIfReached();
    }

    public Timeout timeout(long j, TimeUnit timeUnit) {
        Intrinsics.b(timeUnit, "unit");
        return this.f4072a.timeout(j, timeUnit);
    }

    public long timeoutNanos() {
        return this.f4072a.timeoutNanos();
    }

    public final ForwardingTimeout a(Timeout timeout) {
        Intrinsics.b(timeout, "delegate");
        this.f4072a = timeout;
        return this;
    }

    public Timeout deadlineNanoTime(long j) {
        return this.f4072a.deadlineNanoTime(j);
    }
}
