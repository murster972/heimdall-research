package okio;

import java.io.InputStream;
import kotlin.jvm.internal.Intrinsics;

public final class Buffer$inputStream$1 extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Buffer f4068a;

    Buffer$inputStream$1(Buffer buffer) {
        this.f4068a = buffer;
    }

    public int available() {
        return (int) Math.min(this.f4068a.u(), (long) Integer.MAX_VALUE);
    }

    public void close() {
    }

    public int read() {
        if (this.f4068a.u() > 0) {
            return this.f4068a.readByte() & 255;
        }
        return -1;
    }

    public String toString() {
        return this.f4068a + ".inputStream()";
    }

    public int read(byte[] bArr, int i, int i2) {
        Intrinsics.b(bArr, "sink");
        return this.f4068a.a(bArr, i, i2);
    }
}
