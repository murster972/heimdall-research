package okio;

import android.support.v4.media.session.PlaybackStateCompat;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;

public final class SegmentPool {

    /* renamed from: a  reason: collision with root package name */
    private static Segment f4084a;
    private static long b;
    public static final SegmentPool c = new SegmentPool();

    private SegmentPool() {
    }

    public final void a(Segment segment) {
        Intrinsics.b(segment, "segment");
        if (!(segment.f == null && segment.g == null)) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        } else if (!segment.d) {
            synchronized (this) {
                long j = (long) 8192;
                if (b + j <= PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                    b += j;
                    segment.f = f4084a;
                    segment.c = 0;
                    segment.b = segment.c;
                    f4084a = segment;
                    Unit unit = Unit.f6917a;
                }
            }
        }
    }

    public final Segment a() {
        synchronized (this) {
            Segment segment = f4084a;
            if (segment == null) {
                return new Segment();
            }
            f4084a = segment.f;
            segment.f = null;
            b -= (long) 8192;
            return segment;
        }
    }
}
