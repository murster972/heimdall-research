package dagger.internal;

import dagger.Lazy;
import javax.inject.Provider;

public final class DoubleCheck<T> implements Provider<T>, Lazy<T> {
    private static final Object c = new Object();

    /* renamed from: a  reason: collision with root package name */
    private volatile Provider<T> f6612a;
    private volatile Object b = c;

    private DoubleCheck(Provider<T> provider) {
        this.f6612a = provider;
    }

    public static Object a(Object obj, Object obj2) {
        if (!(obj != c && !(obj instanceof MemoizedSentinel)) || obj == obj2) {
            return obj2;
        }
        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + obj + " & " + obj2 + ". This is likely due to a circular dependency.");
    }

    public T get() {
        T t = this.b;
        if (t == c) {
            synchronized (this) {
                t = this.b;
                if (t == c) {
                    t = this.f6612a.get();
                    a(this.b, t);
                    this.b = t;
                    this.f6612a = null;
                }
            }
        }
        return t;
    }

    public static <P extends Provider<T>, T> Provider<T> a(P p) {
        Preconditions.a(p);
        if (p instanceof DoubleCheck) {
            return p;
        }
        return new DoubleCheck(p);
    }
}
