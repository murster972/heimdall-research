package fi.iki.elonen;

import com.facebook.common.statfs.StatFsHelper;
import com.facebook.common.util.ByteConstants;
import com.google.android.gms.ads.AdRequest;
import com.original.tase.model.socket.UserResponces;
import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.CookieDBAdapter;
import com.vungle.warren.ui.JavascriptBridge;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

public abstract class NanoHTTPD {
    /* access modifiers changed from: private */
    public static final Pattern h = Pattern.compile("[ |\t]*(charset)[ |\t]*=[ |\t]*['|\"]?([^\"^'^;]*)['|\"]?", 2);
    /* access modifiers changed from: private */
    public static final Pattern i = Pattern.compile("[ |\t]*(boundary)[ |\t]*=[ |\t]*['|\"]?([^\"^'^;]*)['|\"]?", 2);
    /* access modifiers changed from: private */
    public static final Pattern j = Pattern.compile("([ |\t]*Content-Disposition[ |\t]*:)(.*)", 2);
    /* access modifiers changed from: private */
    public static final Pattern k = Pattern.compile("([ |\t]*content-type[ |\t]*:)(.*)", 2);
    /* access modifiers changed from: private */
    public static final Pattern l = Pattern.compile("[ |\t]*([a-zA-Z]*)[ |\t]*=[ |\t]*['|\"]([^\"^']*)['|\"]");
    /* access modifiers changed from: private */
    public static final Logger m = Logger.getLogger(NanoHTTPD.class.getName());
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final String f6613a;
    /* access modifiers changed from: private */
    public final int b;
    /* access modifiers changed from: private */
    public volatile ServerSocket c;
    private ServerSocketFactory d;
    private Thread e;
    protected AsyncRunner f;
    /* access modifiers changed from: private */
    public TempFileManagerFactory g;

    public interface AsyncRunner {
        void a();

        void a(ClientHandler clientHandler);

        void b(ClientHandler clientHandler);
    }

    public class ClientHandler implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final InputStream f6614a;
        private final Socket b;

        public void a() {
            NanoHTTPD.b((Object) this.f6614a);
            NanoHTTPD.b((Object) this.b);
        }

        public void run() {
            OutputStream outputStream = null;
            try {
                outputStream = this.b.getOutputStream();
                HTTPSession hTTPSession = new HTTPSession(NanoHTTPD.this.g.create(), this.f6614a, outputStream, this.b.getInetAddress());
                while (!this.b.isClosed()) {
                    hTTPSession.d();
                }
            } catch (Exception e) {
                if ((!(e instanceof SocketException) || !"NanoHttpd Shutdown".equals(e.getMessage())) && !(e instanceof SocketTimeoutException)) {
                    NanoHTTPD.m.log(Level.FINE, "Communication with the client broken", e);
                }
            } catch (Throwable th) {
                NanoHTTPD.b((Object) null);
                NanoHTTPD.b((Object) this.f6614a);
                NanoHTTPD.b((Object) this.b);
                NanoHTTPD.this.f.a(this);
                throw th;
            }
            NanoHTTPD.b((Object) outputStream);
            NanoHTTPD.b((Object) this.f6614a);
            NanoHTTPD.b((Object) this.b);
            NanoHTTPD.this.f.a(this);
        }

        private ClientHandler(InputStream inputStream, Socket socket) {
            this.f6614a = inputStream;
            this.b = socket;
        }
    }

    public static class Cookie {

        /* renamed from: a  reason: collision with root package name */
        private final String f6615a;
        private final String b;
        private final String c;

        public String a() {
            return String.format("%s=%s; expires=%s", new Object[]{this.f6615a, this.b, this.c});
        }
    }

    public class CookieHandler implements Iterable<String> {

        /* renamed from: a  reason: collision with root package name */
        private final HashMap<String, String> f6616a = new HashMap<>();
        private final ArrayList<Cookie> b = new ArrayList<>();

        public CookieHandler(NanoHTTPD nanoHTTPD, Map<String, String> map) {
            String str = map.get(CookieDBAdapter.CookieColumns.TABLE_NAME);
            if (str != null) {
                for (String trim : str.split(";")) {
                    String[] split = trim.trim().split("=");
                    if (split.length == 2) {
                        this.f6616a.put(split[0], split[1]);
                    }
                }
            }
        }

        public void a(Response response) {
            Iterator<Cookie> it2 = this.b.iterator();
            while (it2.hasNext()) {
                response.a("Set-Cookie", it2.next().a());
            }
        }

        public Iterator<String> iterator() {
            return this.f6616a.keySet().iterator();
        }
    }

    public static class DefaultServerSocketFactory implements ServerSocketFactory {
        public ServerSocket create() throws IOException {
            return new ServerSocket();
        }
    }

    public static class DefaultTempFile implements TempFile {

        /* renamed from: a  reason: collision with root package name */
        private final File f6618a;
        private final OutputStream b = new FileOutputStream(this.f6618a);

        public DefaultTempFile(File file) throws IOException {
            this.f6618a = File.createTempFile("NanoHTTPD-", "", file);
        }

        public void a() throws Exception {
            NanoHTTPD.b((Object) this.b);
            if (!this.f6618a.delete()) {
                throw new Exception("could not delete temporary file");
            }
        }

        public String getName() {
            return this.f6618a.getAbsolutePath();
        }
    }

    public static class DefaultTempFileManager implements TempFileManager {

        /* renamed from: a  reason: collision with root package name */
        private final File f6619a = new File(System.getProperty("java.io.tmpdir"));
        private final List<TempFile> b;

        public DefaultTempFileManager() {
            if (!this.f6619a.exists()) {
                this.f6619a.mkdirs();
            }
            this.b = new ArrayList();
        }

        public TempFile a(String str) throws Exception {
            DefaultTempFile defaultTempFile = new DefaultTempFile(this.f6619a);
            this.b.add(defaultTempFile);
            return defaultTempFile;
        }

        public void clear() {
            for (TempFile a2 : this.b) {
                try {
                    a2.a();
                } catch (Exception e) {
                    NanoHTTPD.m.log(Level.WARNING, "could not delete file ", e);
                }
            }
            this.b.clear();
        }
    }

    private class DefaultTempFileManagerFactory implements TempFileManagerFactory {
        private DefaultTempFileManagerFactory(NanoHTTPD nanoHTTPD) {
        }

        public TempFileManager create() {
            return new DefaultTempFileManager();
        }
    }

    protected class HTTPSession implements IHTTPSession {

        /* renamed from: a  reason: collision with root package name */
        private final TempFileManager f6620a;
        private final OutputStream b;
        private final BufferedInputStream c;
        private int d;
        private int e;
        private String f;
        private Method g;
        private Map<String, String> h;
        private Map<String, String> i;
        private CookieHandler j;
        private String k;
        private String l;
        private String m;

        public HTTPSession(TempFileManager tempFileManager, InputStream inputStream, OutputStream outputStream, InetAddress inetAddress) {
            this.f6620a = tempFileManager;
            this.c = new BufferedInputStream(inputStream, 8192);
            this.b = outputStream;
            this.l = (inetAddress.isLoopbackAddress() || inetAddress.isAnyLocalAddress()) ? "127.0.0.1" : inetAddress.getHostAddress().toString();
            this.i = new HashMap();
        }

        private void a(BufferedReader bufferedReader, Map<String, String> map, Map<String, String> map2, Map<String, String> map3) throws ResponseException {
            String str;
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    StringTokenizer stringTokenizer = new StringTokenizer(readLine);
                    if (stringTokenizer.hasMoreTokens()) {
                        map.put("method", stringTokenizer.nextToken());
                        if (stringTokenizer.hasMoreTokens()) {
                            String nextToken = stringTokenizer.nextToken();
                            int indexOf = nextToken.indexOf(63);
                            if (indexOf >= 0) {
                                a(nextToken.substring(indexOf + 1), map2);
                                str = NanoHTTPD.a(nextToken.substring(0, indexOf));
                            } else {
                                str = NanoHTTPD.a(nextToken);
                            }
                            if (stringTokenizer.hasMoreTokens()) {
                                this.m = stringTokenizer.nextToken();
                            } else {
                                this.m = "HTTP/1.1";
                                NanoHTTPD.m.log(Level.FINE, "no protocol version specified, strange. Assuming HTTP/1.1.");
                            }
                            String readLine2 = bufferedReader.readLine();
                            while (readLine2 != null && readLine2.trim().length() > 0) {
                                int indexOf2 = readLine2.indexOf(58);
                                if (indexOf2 >= 0) {
                                    map3.put(readLine2.substring(0, indexOf2).trim().toLowerCase(Locale.US), readLine2.substring(indexOf2 + 1).trim());
                                }
                                readLine2 = bufferedReader.readLine();
                            }
                            map.put("uri", str);
                            return;
                        }
                        throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Missing URI. Usage: GET /example/file.html");
                    }
                    throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Syntax error. Usage: GET /example/file.html");
                }
            } catch (IOException e2) {
                Response.Status status = Response.Status.INTERNAL_ERROR;
                throw new ResponseException(status, "SERVER INTERNAL ERROR: IOException: " + e2.getMessage(), e2);
            }
        }

        private int b(byte[] bArr, int i2) {
            while (bArr[i2] != 10) {
                i2++;
            }
            return i2 + 1;
        }

        private RandomAccessFile f() {
            try {
                return new RandomAccessFile(this.f6620a.a((String) null).getName(), "rw");
            } catch (Exception e2) {
                throw new Error(e2);
            }
        }

        public String c() {
            return this.k;
        }

        public void d() throws IOException {
            Response response = null;
            try {
                byte[] bArr = new byte[8192];
                boolean z = false;
                this.d = 0;
                this.e = 0;
                this.c.mark(8192);
                int read = this.c.read(bArr, 0, 8192);
                if (read != -1) {
                    while (true) {
                        if (read <= 0) {
                            break;
                        }
                        this.e += read;
                        this.d = a(bArr, this.e);
                        if (this.d > 0) {
                            break;
                        }
                        read = this.c.read(bArr, this.e, 8192 - this.e);
                    }
                    if (this.d < this.e) {
                        this.c.reset();
                        this.c.skip((long) this.d);
                    }
                    this.h = new HashMap();
                    if (this.i == null) {
                        this.i = new HashMap();
                    } else {
                        this.i.clear();
                    }
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bArr, 0, this.e)));
                    HashMap hashMap = new HashMap();
                    a(bufferedReader, (Map<String, String>) hashMap, this.h, this.i);
                    if (this.l != null) {
                        this.i.put("remote-addr", this.l);
                        this.i.put("http-client-ip", this.l);
                    }
                    this.g = Method.a((String) hashMap.get("method"));
                    if (this.g != null) {
                        this.f = (String) hashMap.get("uri");
                        this.j = new CookieHandler(NanoHTTPD.this, this.i);
                        String str = this.i.get("connection");
                        boolean z2 = this.m.equals("HTTP/1.1") && (str == null || !str.matches("(?i).*close.*"));
                        response = NanoHTTPD.this.a((IHTTPSession) this);
                        if (response != null) {
                            String str2 = this.i.get("accept-encoding");
                            this.j.a(response);
                            response.a(this.g);
                            if (NanoHTTPD.this.a(response) && str2 != null && str2.contains("gzip")) {
                                z = true;
                            }
                            response.b(z);
                            response.c(z2);
                            response.a(this.b);
                            if (!z2 || JavascriptBridge.MraidHandler.CLOSE_ACTION.equalsIgnoreCase(response.e("connection"))) {
                                throw new SocketException("NanoHttpd Shutdown");
                            }
                            NanoHTTPD.b((Object) response);
                            this.f6620a.clear();
                            return;
                        }
                        throw new ResponseException(Response.Status.INTERNAL_ERROR, "SERVER INTERNAL ERROR: Serve() returned a null response.");
                    }
                    throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Syntax error.");
                }
                NanoHTTPD.b((Object) this.c);
                NanoHTTPD.b((Object) this.b);
                throw new SocketException("NanoHttpd Shutdown");
            } catch (Exception unused) {
                NanoHTTPD.b((Object) this.c);
                NanoHTTPD.b((Object) this.b);
                throw new SocketException("NanoHttpd Shutdown");
            } catch (SocketException e2) {
                throw e2;
            } catch (SocketTimeoutException e3) {
                throw e3;
            } catch (IOException e4) {
                NanoHTTPD.a(Response.Status.INTERNAL_ERROR, "text/plain", "SERVER INTERNAL ERROR: IOException: " + e4.getMessage()).a(this.b);
                NanoHTTPD.b((Object) this.b);
            } catch (ResponseException e5) {
                try {
                    NanoHTTPD.a(e5.a(), "text/plain", e5.getMessage()).a(this.b);
                    NanoHTTPD.b((Object) this.b);
                } catch (Throwable th) {
                    NanoHTTPD.b((Object) null);
                    this.f6620a.clear();
                    throw th;
                }
            }
        }

        public long e() {
            if (this.i.containsKey("content-length")) {
                return Long.parseLong(this.i.get("content-length"));
            }
            int i2 = this.d;
            int i3 = this.e;
            if (i2 < i3) {
                return (long) (i3 - i2);
            }
            return 0;
        }

        public final Method getMethod() {
            return this.g;
        }

        public final String getUri() {
            return this.f;
        }

        public final Map<String, String> b() {
            return this.h;
        }

        private void a(String str, String str2, ByteBuffer byteBuffer, Map<String, String> map, Map<String, String> map2) throws ResponseException {
            ByteBuffer byteBuffer2 = byteBuffer;
            Map<String, String> map3 = map;
            Map<String, String> map4 = map2;
            try {
                int[] a2 = a(byteBuffer2, str.getBytes());
                int i2 = 2;
                if (a2.length >= 2) {
                    int i3 = ByteConstants.KB;
                    byte[] bArr = new byte[ByteConstants.KB];
                    int i4 = 0;
                    int i5 = 0;
                    while (i5 < a2.length - 1) {
                        byteBuffer2.position(a2[i5]);
                        int remaining = byteBuffer.remaining() < i3 ? byteBuffer.remaining() : ByteConstants.KB;
                        byteBuffer2.get(bArr, i4, remaining);
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bArr, i4, remaining), Charset.forName(str2)), remaining);
                        if (bufferedReader.readLine().contains(str)) {
                            String readLine = bufferedReader.readLine();
                            String str3 = null;
                            String str4 = null;
                            String str5 = null;
                            int i6 = 2;
                            while (readLine != null && readLine.trim().length() > 0) {
                                Matcher matcher = NanoHTTPD.j.matcher(readLine);
                                if (matcher.matches()) {
                                    Matcher matcher2 = NanoHTTPD.l.matcher(matcher.group(i2));
                                    while (matcher2.find()) {
                                        String str6 = str3;
                                        String group = matcher2.group(1);
                                        if (group.equalsIgnoreCase(MediationMetaData.KEY_NAME)) {
                                            str4 = matcher2.group(2);
                                        } else if (group.equalsIgnoreCase("filename")) {
                                            str3 = matcher2.group(2);
                                        }
                                        str3 = str6;
                                    }
                                    String str7 = str3;
                                }
                                Matcher matcher3 = NanoHTTPD.k.matcher(readLine);
                                if (matcher3.matches()) {
                                    str5 = matcher3.group(2).trim();
                                }
                                readLine = bufferedReader.readLine();
                                i6++;
                                i2 = 2;
                            }
                            int i7 = 0;
                            while (true) {
                                int i8 = i6 - 1;
                                if (i6 <= 0) {
                                    break;
                                }
                                i7 = b(bArr, i7);
                                i6 = i8;
                            }
                            if (i7 < remaining - 4) {
                                int i9 = a2[i5] + i7;
                                i5++;
                                int i10 = a2[i5] - 4;
                                byteBuffer2.position(i9);
                                if (str5 == null) {
                                    byte[] bArr2 = new byte[(i10 - i9)];
                                    byteBuffer2.get(bArr2);
                                    map3.put(str4, new String(bArr2, str2));
                                } else {
                                    String str8 = str2;
                                    String a3 = a(byteBuffer2, i9, i10 - i9, str3);
                                    if (!map4.containsKey(str4)) {
                                        map4.put(str4, a3);
                                    } else {
                                        int i11 = 2;
                                        while (true) {
                                            if (!map4.containsKey(str4 + i11)) {
                                                break;
                                            }
                                            i11++;
                                        }
                                        map4.put(str4 + i11, a3);
                                    }
                                    map3.put(str4, str3);
                                }
                                i3 = ByteConstants.KB;
                                i2 = 2;
                                i4 = 0;
                            } else {
                                throw new ResponseException(Response.Status.INTERNAL_ERROR, "Multipart header size exceeds MAX_HEADER_SIZE.");
                            }
                        } else {
                            throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Content type is multipart/form-data but chunk does not start with boundary.");
                        }
                    }
                    return;
                }
                throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Content type is multipart/form-data but contains less than two boundary strings.");
            } catch (ResponseException e2) {
                throw e2;
            } catch (Exception e3) {
                throw new ResponseException(Response.Status.INTERNAL_ERROR, e3.toString());
            }
        }

        private void a(String str, Map<String, String> map) {
            if (str == null) {
                this.k = "";
                return;
            }
            this.k = str;
            StringTokenizer stringTokenizer = new StringTokenizer(str, "&");
            while (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                int indexOf = nextToken.indexOf(61);
                if (indexOf >= 0) {
                    map.put(NanoHTTPD.a(nextToken.substring(0, indexOf)).trim(), NanoHTTPD.a(nextToken.substring(indexOf + 1)));
                } else {
                    map.put(NanoHTTPD.a(nextToken).trim(), "");
                }
            }
        }

        private int a(byte[] bArr, int i2) {
            int i3;
            int i4 = 0;
            while (true) {
                int i5 = i4 + 1;
                if (i5 >= i2) {
                    return 0;
                }
                if (bArr[i4] == 13 && bArr[i5] == 10 && (i3 = i4 + 3) < i2 && bArr[i4 + 2] == 13 && bArr[i3] == 10) {
                    return i4 + 4;
                }
                if (bArr[i4] == 10 && bArr[i5] == 10) {
                    return i4 + 2;
                }
                i4 = i5;
            }
        }

        private int[] a(ByteBuffer byteBuffer, byte[] bArr) {
            int[] iArr = new int[0];
            if (byteBuffer.remaining() < bArr.length) {
                return iArr;
            }
            byte[] bArr2 = new byte[(bArr.length + 4096)];
            int remaining = byteBuffer.remaining() < bArr2.length ? byteBuffer.remaining() : bArr2.length;
            byteBuffer.get(bArr2, 0, remaining);
            int length = remaining - bArr.length;
            int[] iArr2 = iArr;
            int i2 = 0;
            while (true) {
                int[] iArr3 = iArr2;
                int i3 = 0;
                while (i3 < length) {
                    int[] iArr4 = iArr3;
                    int i4 = 0;
                    while (i4 < bArr.length && bArr2[i3 + i4] == bArr[i4]) {
                        if (i4 == bArr.length - 1) {
                            int[] iArr5 = new int[(iArr4.length + 1)];
                            System.arraycopy(iArr4, 0, iArr5, 0, iArr4.length);
                            iArr5[iArr4.length] = i2 + i3;
                            iArr4 = iArr5;
                        }
                        i4++;
                    }
                    i3++;
                    iArr3 = iArr4;
                }
                i2 += length;
                System.arraycopy(bArr2, bArr2.length - bArr.length, bArr2, 0, bArr.length);
                length = bArr2.length - bArr.length;
                if (byteBuffer.remaining() < length) {
                    length = byteBuffer.remaining();
                }
                byteBuffer.get(bArr2, bArr.length, length);
                if (length <= 0) {
                    return iArr3;
                }
                iArr2 = iArr3;
            }
        }

        public final Map<String, String> a() {
            return this.i;
        }

        public void a(Map<String, String> map) throws IOException, ResponseException {
            RandomAccessFile randomAccessFile;
            DataOutput dataOutput;
            ByteArrayOutputStream byteArrayOutputStream;
            ByteBuffer map2;
            StringTokenizer stringTokenizer;
            Map<String, String> map3 = map;
            try {
                long e2 = e();
                if (e2 < 1024) {
                    byteArrayOutputStream = new ByteArrayOutputStream();
                    randomAccessFile = null;
                    dataOutput = new DataOutputStream(byteArrayOutputStream);
                } else {
                    DataOutput f2 = f();
                    randomAccessFile = f2;
                    byteArrayOutputStream = null;
                    dataOutput = f2;
                }
                try {
                    byte[] bArr = new byte[AdRequest.MAX_CONTENT_URL_LENGTH];
                    while (this.e >= 0 && e2 > 0) {
                        this.e = this.c.read(bArr, 0, (int) Math.min(e2, 512));
                        e2 -= (long) this.e;
                        if (this.e > 0) {
                            dataOutput.write(bArr, 0, this.e);
                        }
                    }
                    if (byteArrayOutputStream != null) {
                        map2 = ByteBuffer.wrap(byteArrayOutputStream.toByteArray(), 0, byteArrayOutputStream.size());
                    } else {
                        map2 = randomAccessFile.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, randomAccessFile.length());
                        randomAccessFile.seek(0);
                    }
                    ByteBuffer byteBuffer = map2;
                    if (Method.POST.equals(this.g)) {
                        String str = "";
                        String str2 = this.i.get("content-type");
                        if (str2 != null) {
                            stringTokenizer = new StringTokenizer(str2, ",; ");
                            if (stringTokenizer.hasMoreTokens()) {
                                str = stringTokenizer.nextToken();
                            }
                        } else {
                            stringTokenizer = null;
                        }
                        if (!"multipart/form-data".equalsIgnoreCase(str)) {
                            byte[] bArr2 = new byte[byteBuffer.remaining()];
                            byteBuffer.get(bArr2);
                            String trim = new String(bArr2).trim();
                            if ("application/x-www-form-urlencoded".equalsIgnoreCase(str)) {
                                a(trim, this.h);
                            } else if (trim.length() != 0) {
                                map3.put("postData", trim);
                            }
                        } else if (stringTokenizer.hasMoreTokens()) {
                            a(a(str2, NanoHTTPD.i, (String) null), a(str2, NanoHTTPD.h, "US-ASCII"), byteBuffer, this.h, map);
                        } else {
                            throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Content type is multipart/form-data but boundary missing. Usage: GET /example/file.html");
                        }
                    } else if (Method.PUT.equals(this.g)) {
                        map3.put("content", a(byteBuffer, 0, byteBuffer.limit(), (String) null));
                    }
                    NanoHTTPD.b((Object) randomAccessFile);
                } catch (Throwable th) {
                    th = th;
                    NanoHTTPD.b((Object) randomAccessFile);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                randomAccessFile = null;
                NanoHTTPD.b((Object) randomAccessFile);
                throw th;
            }
        }

        private String a(String str, Pattern pattern, String str2) {
            Matcher matcher = pattern.matcher(str);
            return matcher.find() ? matcher.group(2) : str2;
        }

        private String a(ByteBuffer byteBuffer, int i2, int i3, String str) {
            if (i3 <= 0) {
                return "";
            }
            FileOutputStream fileOutputStream = null;
            try {
                TempFile a2 = this.f6620a.a(str);
                ByteBuffer duplicate = byteBuffer.duplicate();
                FileOutputStream fileOutputStream2 = new FileOutputStream(a2.getName());
                try {
                    FileChannel channel = fileOutputStream2.getChannel();
                    duplicate.position(i2).limit(i2 + i3);
                    channel.write(duplicate.slice());
                    String name = a2.getName();
                    NanoHTTPD.b((Object) fileOutputStream2);
                    return name;
                } catch (Exception e2) {
                    e = e2;
                    fileOutputStream = fileOutputStream2;
                    try {
                        throw new Error(e);
                    } catch (Throwable th) {
                        th = th;
                        NanoHTTPD.b((Object) fileOutputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = fileOutputStream2;
                    NanoHTTPD.b((Object) fileOutputStream);
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                throw new Error(e);
            }
        }
    }

    public interface IHTTPSession {
        Map<String, String> a();

        void a(Map<String, String> map) throws IOException, ResponseException;

        Map<String, String> b();

        String c();

        Method getMethod();

        String getUri();
    }

    public enum Method {
        GET,
        PUT,
        POST,
        DELETE,
        HEAD,
        OPTIONS,
        TRACE,
        CONNECT,
        PATCH;

        static Method a(String str) {
            for (Method method : values()) {
                if (method.toString().equalsIgnoreCase(str)) {
                    return method;
                }
            }
            return null;
        }
    }

    public static class Response implements Closeable {

        /* renamed from: a  reason: collision with root package name */
        private IStatus f6622a;
        private String b;
        private InputStream c;
        private long d;
        private final Map<String, String> e = new HashMap();
        private Method f;
        private boolean g;
        private boolean h;
        private boolean i;

        private static class ChunkedOutputStream extends FilterOutputStream {
            public ChunkedOutputStream(OutputStream outputStream) {
                super(outputStream);
            }

            public void s() throws IOException {
                this.out.write("0\r\n\r\n".getBytes());
            }

            public void write(int i) throws IOException {
                write(new byte[]{(byte) i}, 0, 1);
            }

            public void write(byte[] bArr) throws IOException {
                write(bArr, 0, bArr.length);
            }

            public void write(byte[] bArr, int i, int i2) throws IOException {
                if (i2 != 0) {
                    this.out.write(String.format("%x\r\n", new Object[]{Integer.valueOf(i2)}).getBytes());
                    this.out.write(bArr, i, i2);
                    this.out.write("\r\n".getBytes());
                }
            }
        }

        public interface IStatus {
            String getDescription();
        }

        public enum Status implements IStatus {
            SWITCH_PROTOCOL(101, "Switching Protocols"),
            OK(UserResponces.USER_RESPONCE_SUCCSES, "OK"),
            CREATED(201, "Created"),
            ACCEPTED(202, "Accepted"),
            NO_CONTENT(204, "No Content"),
            PARTIAL_CONTENT(206, "Partial Content"),
            REDIRECT(301, "Moved Permanently"),
            NOT_MODIFIED(304, "Not Modified"),
            BAD_REQUEST(StatFsHelper.DEFAULT_DISK_YELLOW_LEVEL_IN_MB, "Bad Request"),
            UNAUTHORIZED(401, "Unauthorized"),
            FORBIDDEN(403, "Forbidden"),
            NOT_FOUND(UserResponces.USER_RESPONCE_FAIL, "Not Found"),
            METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
            NOT_ACCEPTABLE(406, "Not Acceptable"),
            REQUEST_TIMEOUT(408, "Request Timeout"),
            CONFLICT(409, "Conflict"),
            RANGE_NOT_SATISFIABLE(416, "Requested Range Not Satisfiable"),
            INTERNAL_ERROR(500, "Internal Server Error"),
            NOT_IMPLEMENTED(501, "Not Implemented"),
            UNSUPPORTED_HTTP_VERSION(505, "HTTP Version Not Supported");
            
            private final String description;
            private final int requestStatus;

            private Status(int i, String str) {
                this.requestStatus = i;
                this.description = str;
            }

            public String getDescription() {
                return "" + this.requestStatus + " " + this.description;
            }
        }

        protected Response(IStatus iStatus, String str, InputStream inputStream, long j) {
            this.f6622a = iStatus;
            this.b = str;
            boolean z = false;
            if (inputStream == null) {
                this.c = new ByteArrayInputStream(new byte[0]);
                this.d = 0;
            } else {
                this.c = inputStream;
                this.d = j;
            }
            this.g = this.d < 0 ? true : z;
            this.i = true;
        }

        public void a(String str, String str2) {
            this.e.put(str, str2);
        }

        public void b(boolean z) {
            this.h = z;
        }

        public void c(boolean z) {
            this.i = z;
        }

        public void close() throws IOException {
            InputStream inputStream = this.c;
            if (inputStream != null) {
                inputStream.close();
            }
        }

        public String e(String str) {
            for (String next : this.e.keySet()) {
                if (next.equalsIgnoreCase(str)) {
                    return this.e.get(next);
                }
            }
            return null;
        }

        public String s() {
            return this.b;
        }

        private static boolean a(Map<String, String> map, String str) {
            boolean z = false;
            for (String equalsIgnoreCase : map.keySet()) {
                z |= equalsIgnoreCase.equalsIgnoreCase(str);
            }
            return z;
        }

        private void b(OutputStream outputStream, long j) throws IOException {
            if (this.h) {
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
                a((OutputStream) gZIPOutputStream, -1);
                gZIPOutputStream.finish();
                return;
            }
            a(outputStream, j);
        }

        private void c(OutputStream outputStream, long j) throws IOException {
            if (this.f == Method.HEAD || !this.g) {
                b(outputStream, j);
                return;
            }
            ChunkedOutputStream chunkedOutputStream = new ChunkedOutputStream(outputStream);
            b(chunkedOutputStream, -1);
            chunkedOutputStream.s();
        }

        /* access modifiers changed from: protected */
        public void a(OutputStream outputStream) {
            String str = this.b;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            try {
                if (this.f6622a != null) {
                    PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8")), false);
                    printWriter.print("HTTP/1.1 " + this.f6622a.getDescription() + " \r\n");
                    if (str != null) {
                        printWriter.print("Content-Type: " + str + "\r\n");
                    }
                    if (this.e == null || this.e.get("Date") == null) {
                        printWriter.print("Date: " + simpleDateFormat.format(new Date()) + "\r\n");
                    }
                    if (this.e != null) {
                        for (String next : this.e.keySet()) {
                            printWriter.print(next + ": " + this.e.get(next) + "\r\n");
                        }
                    }
                    if (!a(this.e, "connection")) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Connection: ");
                        sb.append(this.i ? "keep-alive" : JavascriptBridge.MraidHandler.CLOSE_ACTION);
                        sb.append("\r\n");
                        printWriter.print(sb.toString());
                    }
                    if (a(this.e, "content-length")) {
                        this.h = false;
                    }
                    if (this.h) {
                        printWriter.print("Content-Encoding: gzip\r\n");
                        a(true);
                    }
                    long j = this.c != null ? this.d : 0;
                    if (this.f != Method.HEAD && this.g) {
                        printWriter.print("Transfer-Encoding: chunked\r\n");
                    } else if (!this.h) {
                        j = a(printWriter, this.e, j);
                    }
                    printWriter.print("\r\n");
                    printWriter.flush();
                    c(outputStream, j);
                    outputStream.flush();
                    NanoHTTPD.b((Object) this.c);
                    return;
                }
                throw new Error("sendResponse(): Status can't be null.");
            } catch (IOException e2) {
                NanoHTTPD.m.log(Level.SEVERE, "Could not send response to the client", e2);
            }
        }

        private void a(OutputStream outputStream, long j) throws IOException {
            long j2;
            byte[] bArr = new byte[((int) 16384)];
            boolean z = j == -1;
            while (true) {
                if (j > 0 || z) {
                    if (z) {
                        j2 = 16384;
                    } else {
                        j2 = Math.min(j, 16384);
                    }
                    int read = this.c.read(bArr, 0, (int) j2);
                    if (read > 0) {
                        outputStream.write(bArr, 0, read);
                        if (!z) {
                            j -= (long) read;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
        }

        protected static long a(PrintWriter printWriter, Map<String, String> map, long j) {
            for (String next : map.keySet()) {
                if (next.equalsIgnoreCase("content-length")) {
                    try {
                        return Long.parseLong(map.get(next));
                    } catch (NumberFormatException unused) {
                        return j;
                    }
                }
            }
            printWriter.print("Content-Length: " + j + "\r\n");
            return j;
        }

        public void a(boolean z) {
            this.g = z;
        }

        public void a(Method method) {
            this.f = method;
        }
    }

    public class ServerRunnable implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final int f6624a;
        /* access modifiers changed from: private */
        public IOException b;
        /* access modifiers changed from: private */
        public boolean c;

        public void run() {
            try {
                NanoHTTPD.this.c.bind(NanoHTTPD.this.f6613a != null ? new InetSocketAddress(NanoHTTPD.this.f6613a, NanoHTTPD.this.b) : new InetSocketAddress(NanoHTTPD.this.b));
                this.c = true;
                do {
                    try {
                        Socket accept = NanoHTTPD.this.c.accept();
                        if (this.f6624a > 0) {
                            accept.setSoTimeout(this.f6624a);
                        }
                        NanoHTTPD.this.f.b(NanoHTTPD.this.a(accept, accept.getInputStream()));
                    } catch (IOException e) {
                        NanoHTTPD.m.log(Level.FINE, "Communication with the client broken", e);
                    }
                } while (!NanoHTTPD.this.c.isClosed());
            } catch (IOException e2) {
                this.b = e2;
            }
        }

        private ServerRunnable(int i) {
            this.c = false;
            this.f6624a = i;
        }
    }

    public interface ServerSocketFactory {
        ServerSocket create() throws IOException;
    }

    public interface TempFile {
        void a() throws Exception;

        String getName();
    }

    public interface TempFileManager {
        TempFile a(String str) throws Exception;

        void clear();
    }

    public interface TempFileManagerFactory {
        TempFileManager create();
    }

    public NanoHTTPD(int i2) {
        this((String) null, i2);
    }

    public final boolean e() {
        return (this.c == null || this.e == null) ? false : true;
    }

    public static class DefaultAsyncRunner implements AsyncRunner {

        /* renamed from: a  reason: collision with root package name */
        private long f6617a;
        private final List<ClientHandler> b = Collections.synchronizedList(new ArrayList());

        public void a() {
            Iterator it2 = new ArrayList(this.b).iterator();
            while (it2.hasNext()) {
                ((ClientHandler) it2.next()).a();
            }
        }

        public void b(ClientHandler clientHandler) {
            this.f6617a++;
            Thread thread = new Thread(clientHandler);
            thread.setDaemon(true);
            thread.setName("NanoHttpd Request Processor (#" + this.f6617a + ")");
            this.b.add(clientHandler);
            thread.start();
        }

        public void a(ClientHandler clientHandler) {
            this.b.remove(clientHandler);
        }
    }

    public static final class ResponseException extends Exception {
        private static final long serialVersionUID = 6569838532917408380L;
        private final Response.Status status;

        public ResponseException(Response.Status status2, String str) {
            super(str);
            this.status = status2;
        }

        public Response.Status a() {
            return this.status;
        }

        public ResponseException(Response.Status status2, String str, Exception exc) {
            super(str, exc);
            this.status = status2;
        }
    }

    public NanoHTTPD(String str, int i2) {
        this.d = new DefaultServerSocketFactory();
        this.f6613a = str;
        this.b = i2;
        a((TempFileManagerFactory) new DefaultTempFileManagerFactory());
        a((AsyncRunner) new DefaultAsyncRunner());
    }

    /* access modifiers changed from: private */
    public static final void b(Object obj) {
        if (obj != null) {
            try {
                if (obj instanceof Closeable) {
                    ((Closeable) obj).close();
                } else if (obj instanceof Socket) {
                    ((Socket) obj).close();
                } else if (obj instanceof ServerSocket) {
                    ((ServerSocket) obj).close();
                } else {
                    throw new IllegalArgumentException("Unknown object to close");
                }
            } catch (IOException e2) {
                m.log(Level.SEVERE, "Could not close", e2);
            }
        }
    }

    public final boolean c() {
        return e() && !this.c.isClosed() && this.e.isAlive();
    }

    public void d() {
        try {
            b((Object) this.c);
            this.f.a();
            if (this.e != null) {
                this.e.join();
            }
        } catch (Exception e2) {
            m.log(Level.SEVERE, "Could not stop all connections", e2);
        }
    }

    public synchronized void a() {
        d();
    }

    /* access modifiers changed from: protected */
    public ClientHandler a(Socket socket, InputStream inputStream) {
        return new ClientHandler(inputStream, socket);
    }

    /* access modifiers changed from: protected */
    public ServerRunnable a(int i2) {
        return new ServerRunnable(i2);
    }

    protected static String a(String str) {
        try {
            return URLDecoder.decode(str, "UTF8");
        } catch (UnsupportedEncodingException e2) {
            m.log(Level.WARNING, "Encoding not supported, ignored", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(Response response) {
        return response.s() != null && response.s().toLowerCase().contains("text/");
    }

    public static Response a(Response.IStatus iStatus, String str, InputStream inputStream, long j2) {
        return new Response(iStatus, str, inputStream, j2);
    }

    public ServerSocketFactory b() {
        return this.d;
    }

    public static Response a(Response.IStatus iStatus, String str, String str2) {
        byte[] bArr;
        if (str2 == null) {
            return a(iStatus, str, new ByteArrayInputStream(new byte[0]), 0);
        }
        try {
            bArr = str2.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e2) {
            m.log(Level.SEVERE, "encoding problem, responding nothing", e2);
            bArr = new byte[0];
        }
        return a(iStatus, str, new ByteArrayInputStream(bArr), (long) bArr.length);
    }

    public Response a(IHTTPSession iHTTPSession) {
        HashMap hashMap = new HashMap();
        Method method = iHTTPSession.getMethod();
        if (Method.PUT.equals(method) || Method.POST.equals(method)) {
            try {
                iHTTPSession.a(hashMap);
            } catch (IOException e2) {
                Response.Status status = Response.Status.INTERNAL_ERROR;
                return a(status, "text/plain", "SERVER INTERNAL ERROR: IOException: " + e2.getMessage());
            } catch (ResponseException e3) {
                return a(e3.a(), "text/plain", e3.getMessage());
            }
        }
        Map<String, String> b2 = iHTTPSession.b();
        b2.put("NanoHttpd.QUERY_STRING", iHTTPSession.c());
        return a(iHTTPSession.getUri(), method, iHTTPSession.a(), b2, hashMap);
    }

    @Deprecated
    public Response a(String str, Method method, Map<String, String> map, Map<String, String> map2, Map<String, String> map3) {
        return a(Response.Status.NOT_FOUND, "text/plain", "Not Found");
    }

    public void a(AsyncRunner asyncRunner) {
        this.f = asyncRunner;
    }

    public void a(TempFileManagerFactory tempFileManagerFactory) {
        this.g = tempFileManagerFactory;
    }

    public void a(int i2, boolean z) throws IOException {
        this.c = b().create();
        this.c.setReuseAddress(true);
        ServerRunnable a2 = a(i2);
        this.e = new Thread(a2);
        this.e.setDaemon(z);
        this.e.setName("NanoHttpd Main Listener");
        this.e.start();
        while (!a2.c && a2.b == null) {
            try {
                Thread.sleep(10);
            } catch (Throwable unused) {
            }
        }
        if (a2.b != null) {
            throw a2.b;
        }
    }
}
