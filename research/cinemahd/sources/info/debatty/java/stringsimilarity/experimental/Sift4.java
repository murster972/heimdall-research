package info.debatty.java.stringsimilarity.experimental;

import info.debatty.java.stringsimilarity.interfaces.StringDistance;
import java.util.LinkedList;

public class Sift4 implements StringDistance {
    private int max_offset = 10;

    public final void a(int i) {
        this.max_offset = i;
    }

    public final double a(String str, String str2) {
        int length;
        int i;
        boolean z;
        AnonymousClass1Offset r13;
        String str3 = str;
        String str4 = str2;
        if (str3 == null || str.isEmpty()) {
            if (str4 == null) {
                return 0.0d;
            }
            length = str2.length();
        } else if (str4 == null || str2.isEmpty()) {
            length = str.length();
        } else {
            int length2 = str.length();
            int length3 = str2.length();
            LinkedList linkedList = new LinkedList();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (i2 < length2 && i3 < length3) {
                if (str3.charAt(i2) == str4.charAt(i3)) {
                    i5++;
                    int i7 = 0;
                    while (true) {
                        if (i7 >= linkedList.size()) {
                            z = false;
                            break;
                        }
                        r13 = (AnonymousClass1Offset) linkedList.get(i7);
                        if (i2 > r13.f6625a && i3 > r13.b) {
                            if (i2 <= r13.b || i3 <= r13.f6625a) {
                                i7++;
                            } else {
                                linkedList.remove(i7);
                            }
                        }
                    }
                    z = Math.abs(i3 - i2) >= Math.abs(r13.b - r13.f6625a);
                    if (!z) {
                        if (!r13.c) {
                            boolean unused = r13.c = true;
                        }
                        linkedList.add(new Object(this, i2, i3, z) {
                            /* access modifiers changed from: private */

                            /* renamed from: a  reason: collision with root package name */
                            public final int f6625a;
                            /* access modifiers changed from: private */
                            public final int b;
                            /* access modifiers changed from: private */
                            public boolean c;

                            {
                                this.f6625a = r2;
                                this.b = r3;
                                this.c = r4;
                            }
                        });
                    }
                    i6++;
                    linkedList.add(new Object(this, i2, i3, z) {
                        /* access modifiers changed from: private */

                        /* renamed from: a  reason: collision with root package name */
                        public final int f6625a;
                        /* access modifiers changed from: private */
                        public final int b;
                        /* access modifiers changed from: private */
                        public boolean c;

                        {
                            this.f6625a = r2;
                            this.b = r3;
                            this.c = r4;
                        }
                    });
                } else {
                    i4 += i5;
                    if (i2 != i3) {
                        i2 = Math.min(i2, i3);
                        i3 = i2;
                    }
                    int i8 = 0;
                    while (true) {
                        if (i8 < this.max_offset && ((i = i2 + i8) < length2 || i3 + i8 < length3)) {
                            if (i < length2 && str3.charAt(i) == str4.charAt(i3)) {
                                i2 += i8 - 1;
                                i3--;
                                break;
                            }
                            int i9 = i3 + i8;
                            if (i9 < length3 && str3.charAt(i2) == str4.charAt(i9)) {
                                i2--;
                                i3 += i8 - 1;
                                break;
                            }
                            i8++;
                        } else {
                            break;
                        }
                    }
                    i5 = 0;
                }
                i2++;
                i3++;
                if (i2 >= length2 || i3 >= length3) {
                    i4 += i5;
                    i3 = Math.min(i2, i3);
                    i2 = i3;
                    i5 = 0;
                }
            }
            length = Math.round((float) ((Math.max(length2, length3) - (i4 + i5)) + i6));
        }
        return (double) length;
    }
}
