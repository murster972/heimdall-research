package timber.log;

import android.util.Log;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Timber {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final List<Tree> f7070a = new CopyOnWriteArrayList();
    private static final Tree b = new Tree() {
        public void a(String str, Object... objArr) {
            List a2 = Timber.f7070a;
            int size = a2.size();
            for (int i = 0; i < size; i++) {
                ((Tree) a2.get(i)).a(str, objArr);
            }
        }

        public void b(String str, Object... objArr) {
            List a2 = Timber.f7070a;
            int size = a2.size();
            for (int i = 0; i < size; i++) {
                ((Tree) a2.get(i)).b(str, objArr);
            }
        }

        public void a(Throwable th, String str, Object... objArr) {
            List a2 = Timber.f7070a;
            int size = a2.size();
            for (int i = 0; i < size; i++) {
                ((Tree) a2.get(i)).a(th, str, objArr);
            }
        }

        /* access modifiers changed from: protected */
        public void a(int i, String str, String str2, Throwable th) {
            throw new AssertionError("Missing override for log method.");
        }
    };

    private Timber() {
        throw new AssertionError("No instances.");
    }

    public static void b(String str, Object... objArr) {
        b.b(str, objArr);
    }

    public static abstract class Tree {

        /* renamed from: a  reason: collision with root package name */
        private final ThreadLocal<String> f7071a = new ThreadLocal<>();

        /* access modifiers changed from: package-private */
        public String a() {
            String str = this.f7071a.get();
            if (str != null) {
                this.f7071a.remove();
            }
            return str;
        }

        /* access modifiers changed from: protected */
        public abstract void a(int i, String str, String str2, Throwable th);

        /* access modifiers changed from: protected */
        public boolean a(int i) {
            return true;
        }

        public void b(String str, Object... objArr) {
            a(5, (Throwable) null, str, objArr);
        }

        public void a(String str, Object... objArr) {
            a(3, (Throwable) null, str, objArr);
        }

        public void a(Throwable th, String str, Object... objArr) {
            a(6, th, str, objArr);
        }

        private void a(int i, Throwable th, String str, Object... objArr) {
            if (a(i)) {
                if (str != null && str.length() == 0) {
                    str = null;
                }
                if (str != null) {
                    if (objArr.length > 0) {
                        str = String.format(str, objArr);
                    }
                    if (th != null) {
                        str = str + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE + Log.getStackTraceString(th);
                    }
                } else if (th != null) {
                    str = Log.getStackTraceString(th);
                } else {
                    return;
                }
                a(i, a(), str, th);
            }
        }
    }

    public static void a(String str, Object... objArr) {
        b.a(str, objArr);
    }

    public static void a(Throwable th, String str, Object... objArr) {
        b.a(th, str, objArr);
    }

    public static void a(Tree tree) {
        if (tree == null) {
            throw new NullPointerException("tree == null");
        } else if (tree != b) {
            f7070a.add(tree);
        } else {
            throw new IllegalArgumentException("Cannot plant Timber into itself.");
        }
    }

    public static class DebugTree extends Tree {
        private static final Pattern b = Pattern.compile("(\\$\\d+)+$");

        /* access modifiers changed from: protected */
        public String a(StackTraceElement stackTraceElement) {
            String className = stackTraceElement.getClassName();
            Matcher matcher = b.matcher(className);
            if (matcher.find()) {
                className = matcher.replaceAll("");
            }
            return className.substring(className.lastIndexOf(46) + 1);
        }

        /* access modifiers changed from: package-private */
        public final String a() {
            String a2 = super.a();
            if (a2 != null) {
                return a2;
            }
            StackTraceElement[] stackTrace = new Throwable().getStackTrace();
            if (stackTrace.length > 5) {
                return a(stackTrace[5]);
            }
            throw new IllegalStateException("Synthetic stacktrace didn't have enough elements: are you using proguard?");
        }

        /* access modifiers changed from: protected */
        public void a(int i, String str, String str2, Throwable th) {
            int min;
            if (str2.length() >= 4000) {
                int i2 = 0;
                int length = str2.length();
                while (i2 < length) {
                    int indexOf = str2.indexOf(10, i2);
                    if (indexOf == -1) {
                        indexOf = length;
                    }
                    while (true) {
                        min = Math.min(indexOf, i2 + 4000);
                        String substring = str2.substring(i2, min);
                        if (i == 7) {
                            Log.wtf(str, substring);
                        } else {
                            Log.println(i, str, substring);
                        }
                        if (min >= indexOf) {
                            break;
                        }
                        i2 = min;
                    }
                    i2 = min + 1;
                }
            } else if (i == 7) {
                Log.wtf(str, str2);
            } else {
                Log.println(i, str, str2);
            }
        }
    }
}
