package bolts;

public class TaskCompletionSource<TResult> {

    /* renamed from: a  reason: collision with root package name */
    private final Task<TResult> f1144a = new Task<>();

    public Task<TResult> a() {
        return this.f1144a;
    }

    public boolean b(TResult tresult) {
        return this.f1144a.a(tresult);
    }

    public boolean c() {
        return this.f1144a.f();
    }

    public void a(TResult tresult) {
        if (!b(tresult)) {
            throw new IllegalStateException("Cannot set the result of a completed task.");
        }
    }

    public boolean b(Exception exc) {
        return this.f1144a.a(exc);
    }

    public void b() {
        if (!c()) {
            throw new IllegalStateException("Cannot cancel a completed task.");
        }
    }

    public void a(Exception exc) {
        if (!b(exc)) {
            throw new IllegalStateException("Cannot set the error on a completed task.");
        }
    }
}
