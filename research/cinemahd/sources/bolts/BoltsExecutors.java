package bolts;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

final class BoltsExecutors {
    private static final BoltsExecutors c = new BoltsExecutors();

    /* renamed from: a  reason: collision with root package name */
    private final ExecutorService f1135a;
    private final Executor b;

    private static class ImmediateExecutor implements Executor {

        /* renamed from: a  reason: collision with root package name */
        private ThreadLocal<Integer> f1136a;

        private ImmediateExecutor() {
            this.f1136a = new ThreadLocal<>();
        }

        private int a() {
            Integer num = this.f1136a.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.f1136a.remove();
            } else {
                this.f1136a.set(Integer.valueOf(intValue));
            }
            return intValue;
        }

        private int b() {
            Integer num = this.f1136a.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() + 1;
            this.f1136a.set(Integer.valueOf(intValue));
            return intValue;
        }

        public void execute(Runnable runnable) {
            if (b() <= 15) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    a();
                    throw th;
                }
            } else {
                BoltsExecutors.a().execute(runnable);
            }
            a();
        }
    }

    private BoltsExecutors() {
        this.f1135a = !c() ? Executors.newCachedThreadPool() : AndroidExecutors.a();
        Executors.newSingleThreadScheduledExecutor();
        this.b = new ImmediateExecutor();
    }

    public static ExecutorService a() {
        return c.f1135a;
    }

    static Executor b() {
        return c.b;
    }

    private static boolean c() {
        String property = System.getProperty("java.runtime.name");
        if (property == null) {
            return false;
        }
        return property.toLowerCase(Locale.US).contains("android");
    }
}
