package bolts;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;

public class Task<TResult> {
    private static final Executor i = BoltsExecutors.b();
    private static volatile UnobservedExceptionHandler j;
    private static Task<?> k = new Task<>((Object) null);
    private static Task<Boolean> l = new Task<>(true);
    private static Task<Boolean> m = new Task<>(false);

    /* renamed from: a  reason: collision with root package name */
    private final Object f1137a = new Object();
    private boolean b;
    private boolean c;
    private TResult d;
    private Exception e;
    private boolean f;
    private UnobservedErrorNotifier g;
    private List<Continuation<TResult, Void>> h = new ArrayList();

    public interface UnobservedExceptionHandler {
        void a(Task<?> task, UnobservedTaskException unobservedTaskException);
    }

    static {
        BoltsExecutors.a();
        AndroidExecutors.b();
        new Task(true);
    }

    Task() {
    }

    public static UnobservedExceptionHandler g() {
        return j;
    }

    private void h() {
        synchronized (this.f1137a) {
            for (Continuation then : this.h) {
                try {
                    then.then(this);
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeException(e3);
                }
            }
            this.h = null;
        }
    }

    public boolean c() {
        boolean z;
        synchronized (this.f1137a) {
            z = this.c;
        }
        return z;
    }

    public boolean d() {
        boolean z;
        synchronized (this.f1137a) {
            z = this.b;
        }
        return z;
    }

    public boolean e() {
        boolean z;
        synchronized (this.f1137a) {
            z = a() != null;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        synchronized (this.f1137a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.c = true;
            this.f1137a.notifyAll();
            h();
            return true;
        }
    }

    public Exception a() {
        Exception exc;
        synchronized (this.f1137a) {
            if (this.e != null) {
                this.f = true;
                if (this.g != null) {
                    this.g.a();
                    this.g = null;
                }
            }
            exc = this.e;
        }
        return exc;
    }

    public TResult b() {
        TResult tresult;
        synchronized (this.f1137a) {
            tresult = this.d;
        }
        return tresult;
    }

    private Task(TResult tresult) {
        a(tresult);
    }

    /* access modifiers changed from: private */
    public static <TContinuationResult, TResult> void c(final TaskCompletionSource<TContinuationResult> taskCompletionSource, final Continuation<TResult, Task<TContinuationResult>> continuation, final Task<TResult> task, Executor executor, final CancellationToken cancellationToken) {
        try {
            executor.execute(new Runnable() {
                public void run() {
                    CancellationToken cancellationToken = cancellationToken;
                    if (cancellationToken == null) {
                        try {
                            Task task = (Task) continuation.then(task);
                            if (task == null) {
                                taskCompletionSource.a(null);
                            } else {
                                task.a(new Continuation<TContinuationResult, Void>() {
                                    public Void then(Task<TContinuationResult> task) {
                                        CancellationToken cancellationToken = cancellationToken;
                                        if (cancellationToken == null) {
                                            if (task.c()) {
                                                taskCompletionSource.b();
                                            } else if (task.e()) {
                                                taskCompletionSource.a(task.a());
                                            } else {
                                                taskCompletionSource.a(task.b());
                                            }
                                            return null;
                                        }
                                        cancellationToken.a();
                                        throw null;
                                    }
                                });
                            }
                        } catch (CancellationException unused) {
                            taskCompletionSource.b();
                        } catch (Exception e) {
                            taskCompletionSource.a(e);
                        }
                    } else {
                        cancellationToken.a();
                        throw null;
                    }
                }
            });
        } catch (Exception e2) {
            taskCompletionSource.a((Exception) new ExecutorException(e2));
        }
    }

    /* access modifiers changed from: private */
    public static <TContinuationResult, TResult> void d(final TaskCompletionSource<TContinuationResult> taskCompletionSource, final Continuation<TResult, TContinuationResult> continuation, final Task<TResult> task, Executor executor, final CancellationToken cancellationToken) {
        try {
            executor.execute(new Runnable() {
                public void run() {
                    CancellationToken cancellationToken = cancellationToken;
                    if (cancellationToken == null) {
                        try {
                            taskCompletionSource.a(continuation.then(task));
                        } catch (CancellationException unused) {
                            taskCompletionSource.b();
                        } catch (Exception e) {
                            taskCompletionSource.a(e);
                        }
                    } else {
                        cancellationToken.a();
                        throw null;
                    }
                }
            });
        } catch (Exception e2) {
            taskCompletionSource.a((Exception) new ExecutorException(e2));
        }
    }

    public static <TResult> Task<TResult> b(TResult tresult) {
        if (tresult == null) {
            return k;
        }
        if (tresult instanceof Boolean) {
            return ((Boolean) tresult).booleanValue() ? l : m;
        }
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        taskCompletionSource.a(tresult);
        return taskCompletionSource.a();
    }

    private Task(boolean z) {
        if (z) {
            f();
        } else {
            a((Object) null);
        }
    }

    public static <TResult> Task<TResult> a(Callable<TResult> callable, Executor executor) {
        return a(callable, executor, (CancellationToken) null);
    }

    public static <TResult> Task<TResult> a(final Callable<TResult> callable, Executor executor, final CancellationToken cancellationToken) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        try {
            executor.execute(new Runnable() {
                public void run() {
                    CancellationToken cancellationToken = cancellationToken;
                    if (cancellationToken == null) {
                        try {
                            taskCompletionSource.a(callable.call());
                        } catch (CancellationException unused) {
                            taskCompletionSource.b();
                        } catch (Exception e) {
                            taskCompletionSource.a(e);
                        }
                    } else {
                        cancellationToken.a();
                        throw null;
                    }
                }
            });
        } catch (Exception e2) {
            taskCompletionSource.a((Exception) new ExecutorException(e2));
        }
        return taskCompletionSource.a();
    }

    public static <TResult> Task<TResult> b(Exception exc) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        taskCompletionSource.a(exc);
        return taskCompletionSource.a();
    }

    public <TContinuationResult> Task<TContinuationResult> b(Continuation<TResult, Task<TContinuationResult>> continuation, Executor executor, CancellationToken cancellationToken) {
        boolean d2;
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        synchronized (this.f1137a) {
            d2 = d();
            if (!d2) {
                final TaskCompletionSource taskCompletionSource2 = taskCompletionSource;
                final Continuation<TResult, Task<TContinuationResult>> continuation2 = continuation;
                final Executor executor2 = executor;
                final CancellationToken cancellationToken2 = cancellationToken;
                this.h.add(new Continuation<TResult, Void>(this) {
                    public Void then(Task<TResult> task) {
                        Task.c(taskCompletionSource2, continuation2, task, executor2, cancellationToken2);
                        return null;
                    }
                });
            }
        }
        if (d2) {
            c(taskCompletionSource, continuation, this, executor, cancellationToken);
        }
        return taskCompletionSource.a();
    }

    public <TContinuationResult> Task<TContinuationResult> a(Continuation<TResult, TContinuationResult> continuation, Executor executor, CancellationToken cancellationToken) {
        boolean d2;
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        synchronized (this.f1137a) {
            d2 = d();
            if (!d2) {
                final TaskCompletionSource taskCompletionSource2 = taskCompletionSource;
                final Continuation<TResult, TContinuationResult> continuation2 = continuation;
                final Executor executor2 = executor;
                final CancellationToken cancellationToken2 = cancellationToken;
                this.h.add(new Continuation<TResult, Void>(this) {
                    public Void then(Task<TResult> task) {
                        Task.d(taskCompletionSource2, continuation2, task, executor2, cancellationToken2);
                        return null;
                    }
                });
            }
        }
        if (d2) {
            d(taskCompletionSource, continuation, this, executor, cancellationToken);
        }
        return taskCompletionSource.a();
    }

    public <TContinuationResult> Task<TContinuationResult> b(Continuation<TResult, Task<TContinuationResult>> continuation) {
        return b(continuation, i, (CancellationToken) null);
    }

    public <TContinuationResult> Task<TContinuationResult> a(Continuation<TResult, TContinuationResult> continuation) {
        return a(continuation, i, (CancellationToken) null);
    }

    /* access modifiers changed from: package-private */
    public boolean a(TResult tresult) {
        synchronized (this.f1137a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.d = tresult;
            this.f1137a.notifyAll();
            h();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.Exception r4) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.f1137a
            monitor-enter(r0)
            boolean r1 = r3.b     // Catch:{ all -> 0x002c }
            r2 = 0
            if (r1 == 0) goto L_0x000a
            monitor-exit(r0)     // Catch:{ all -> 0x002c }
            return r2
        L_0x000a:
            r1 = 1
            r3.b = r1     // Catch:{ all -> 0x002c }
            r3.e = r4     // Catch:{ all -> 0x002c }
            r3.f = r2     // Catch:{ all -> 0x002c }
            java.lang.Object r4 = r3.f1137a     // Catch:{ all -> 0x002c }
            r4.notifyAll()     // Catch:{ all -> 0x002c }
            r3.h()     // Catch:{ all -> 0x002c }
            boolean r4 = r3.f     // Catch:{ all -> 0x002c }
            if (r4 != 0) goto L_0x002a
            bolts.Task$UnobservedExceptionHandler r4 = g()     // Catch:{ all -> 0x002c }
            if (r4 == 0) goto L_0x002a
            bolts.UnobservedErrorNotifier r4 = new bolts.UnobservedErrorNotifier     // Catch:{ all -> 0x002c }
            r4.<init>(r3)     // Catch:{ all -> 0x002c }
            r3.g = r4     // Catch:{ all -> 0x002c }
        L_0x002a:
            monitor-exit(r0)     // Catch:{ all -> 0x002c }
            return r1
        L_0x002c:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002c }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: bolts.Task.a(java.lang.Exception):boolean");
    }
}
