package bolts;

import bolts.Task;

class UnobservedErrorNotifier {

    /* renamed from: a  reason: collision with root package name */
    private Task<?> f1145a;

    public UnobservedErrorNotifier(Task<?> task) {
        this.f1145a = task;
    }

    public void a() {
        this.f1145a = null;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        Task.UnobservedExceptionHandler g;
        try {
            Task<?> task = this.f1145a;
            if (!(task == null || (g = Task.g()) == null)) {
                g.a(task, new UnobservedTaskException(task.a()));
            }
        } finally {
            super.finalize();
        }
    }
}
