package bolts;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;

public class AggregateException extends Exception {
    private static final long serialVersionUID = 1;
    private List<Throwable> innerThrowables;

    public void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        int i = -1;
        for (Throwable printStackTrace : this.innerThrowables) {
            printStream.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            printStream.append("  Inner throwable #");
            i++;
            printStream.append(Integer.toString(i));
            printStream.append(": ");
            printStackTrace.printStackTrace(printStream);
            printStream.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        int i = -1;
        for (Throwable printStackTrace : this.innerThrowables) {
            printWriter.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            printWriter.append("  Inner throwable #");
            i++;
            printWriter.append(Integer.toString(i));
            printWriter.append(": ");
            printStackTrace.printStackTrace(printWriter);
            printWriter.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }
    }
}
