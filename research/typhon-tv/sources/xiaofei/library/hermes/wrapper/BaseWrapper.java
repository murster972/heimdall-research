package xiaofei.library.hermes.wrapper;

import android.os.Parcel;

public class BaseWrapper {
    private boolean mIsName;
    private String mName;

    public String getName() {
        return this.mName;
    }

    public boolean isName() {
        return this.mIsName;
    }

    public void readFromParcel(Parcel parcel) {
        boolean z = true;
        if (parcel.readInt() != 1) {
            z = false;
        }
        this.mIsName = z;
        this.mName = parcel.readString();
    }

    /* access modifiers changed from: protected */
    public void setName(boolean z, String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        }
        this.mIsName = z;
        this.mName = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mIsName ? 1 : 0);
        parcel.writeString(this.mName);
    }
}
