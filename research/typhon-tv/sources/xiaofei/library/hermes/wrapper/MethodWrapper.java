package xiaofei.library.hermes.wrapper;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.reflect.Method;
import xiaofei.library.hermes.annotation.MethodId;
import xiaofei.library.hermes.util.TypeUtils;

public class MethodWrapper extends BaseWrapper implements Parcelable {
    public static final Parcelable.Creator<MethodWrapper> CREATOR = new Parcelable.Creator<MethodWrapper>() {
        public MethodWrapper createFromParcel(Parcel parcel) {
            MethodWrapper methodWrapper = new MethodWrapper();
            methodWrapper.readFromParcel(parcel);
            return methodWrapper;
        }

        public MethodWrapper[] newArray(int i) {
            return new MethodWrapper[i];
        }
    };
    private TypeWrapper[] mParameterTypes;
    private TypeWrapper mReturnType;

    private MethodWrapper() {
    }

    public MethodWrapper(String str, Class<?>[] clsArr) {
        setName(true, str);
        clsArr = clsArr == null ? new Class[0] : clsArr;
        int length = clsArr.length;
        this.mParameterTypes = new TypeWrapper[length];
        for (int i = 0; i < length; i++) {
            this.mParameterTypes[i] = new TypeWrapper(clsArr[i]);
        }
        this.mReturnType = null;
    }

    public MethodWrapper(Method method) {
        setName(!method.isAnnotationPresent(MethodId.class), TypeUtils.getMethodId(method));
        Class[] parameterTypes = method.getParameterTypes();
        parameterTypes = parameterTypes == null ? new Class[0] : parameterTypes;
        int length = parameterTypes.length;
        this.mParameterTypes = new TypeWrapper[length];
        for (int i = 0; i < length; i++) {
            this.mParameterTypes[i] = new TypeWrapper((Class<?>) parameterTypes[i]);
        }
        this.mReturnType = new TypeWrapper(method.getReturnType());
    }

    public MethodWrapper(Class<?>[] clsArr) {
        setName(false, "");
        clsArr = clsArr == null ? new Class[0] : clsArr;
        int length = clsArr.length;
        this.mParameterTypes = new TypeWrapper[length];
        for (int i = 0; i < length; i++) {
            this.mParameterTypes[i] = clsArr[i] == null ? null : new TypeWrapper(clsArr[i]);
        }
        this.mReturnType = null;
    }

    public int describeContents() {
        return 0;
    }

    public TypeWrapper[] getParameterTypes() {
        return this.mParameterTypes;
    }

    public TypeWrapper getReturnType() {
        return this.mReturnType;
    }

    public void readFromParcel(Parcel parcel) {
        super.readFromParcel(parcel);
        ClassLoader classLoader = MethodWrapper.class.getClassLoader();
        Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
        if (readParcelableArray == null) {
            this.mParameterTypes = null;
        } else {
            int length = readParcelableArray.length;
            this.mParameterTypes = new TypeWrapper[length];
            for (int i = 0; i < length; i++) {
                this.mParameterTypes[i] = (TypeWrapper) readParcelableArray[i];
            }
        }
        this.mReturnType = (TypeWrapper) parcel.readParcelable(classLoader);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelableArray(this.mParameterTypes, i);
        parcel.writeParcelable(this.mReturnType, i);
    }
}
