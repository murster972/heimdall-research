package xiaofei.library.hermes.wrapper;

import android.os.Parcel;
import android.os.Parcelable;
import xiaofei.library.hermes.annotation.ClassId;
import xiaofei.library.hermes.util.TypeUtils;

public class TypeWrapper extends BaseWrapper implements Parcelable {
    public static final Parcelable.Creator<TypeWrapper> CREATOR = new Parcelable.Creator<TypeWrapper>() {
        public TypeWrapper createFromParcel(Parcel parcel) {
            TypeWrapper typeWrapper = new TypeWrapper();
            typeWrapper.readFromParcel(parcel);
            return typeWrapper;
        }

        public TypeWrapper[] newArray(int i) {
            return new TypeWrapper[i];
        }
    };

    private TypeWrapper() {
    }

    public TypeWrapper(Class<?> cls) {
        setName(!cls.isAnnotationPresent(ClassId.class), TypeUtils.getClassId(cls));
    }

    public int describeContents() {
        return 0;
    }

    public void readFromParcel(Parcel parcel) {
        super.readFromParcel(parcel);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
    }
}
