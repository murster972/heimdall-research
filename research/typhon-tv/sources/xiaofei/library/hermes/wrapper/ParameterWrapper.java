package xiaofei.library.hermes.wrapper;

import android.os.Parcel;
import android.os.Parcelable;
import xiaofei.library.hermes.annotation.ClassId;
import xiaofei.library.hermes.util.CodeUtils;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TypeUtils;

public class ParameterWrapper extends BaseWrapper implements Parcelable {
    public static final Parcelable.Creator<ParameterWrapper> CREATOR = new Parcelable.Creator<ParameterWrapper>() {
        public ParameterWrapper createFromParcel(Parcel parcel) {
            ParameterWrapper parameterWrapper = new ParameterWrapper();
            parameterWrapper.readFromParcel(parcel);
            return parameterWrapper;
        }

        public ParameterWrapper[] newArray(int i) {
            return new ParameterWrapper[i];
        }
    };
    private Class<?> mClass;
    private String mData;

    private ParameterWrapper() {
    }

    public ParameterWrapper(Class<?> cls, Object obj) throws HermesException {
        this.mClass = cls;
        setName(!cls.isAnnotationPresent(ClassId.class), TypeUtils.getClassId(cls));
        this.mData = CodeUtils.encode(obj);
    }

    public ParameterWrapper(Object obj) throws HermesException {
        boolean z = false;
        if (obj == null) {
            setName(false, "");
            this.mData = null;
            this.mClass = null;
            return;
        }
        Class<?> cls = obj.getClass();
        this.mClass = cls;
        setName(!cls.isAnnotationPresent(ClassId.class) ? true : z, TypeUtils.getClassId(cls));
        this.mData = CodeUtils.encode(obj);
    }

    public int describeContents() {
        return 0;
    }

    public Class<?> getClassType() {
        return this.mClass;
    }

    public String getData() {
        return this.mData;
    }

    public boolean isNull() {
        return this.mData == null;
    }

    public void readFromParcel(Parcel parcel) {
        super.readFromParcel(parcel);
        this.mData = parcel.readString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.mData);
    }
}
