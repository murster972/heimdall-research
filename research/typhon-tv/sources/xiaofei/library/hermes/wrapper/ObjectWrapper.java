package xiaofei.library.hermes.wrapper;

import android.os.Parcel;
import android.os.Parcelable;
import xiaofei.library.hermes.annotation.ClassId;
import xiaofei.library.hermes.util.TimeStampGenerator;
import xiaofei.library.hermes.util.TypeUtils;

public class ObjectWrapper extends BaseWrapper implements Parcelable {
    public static final Parcelable.Creator<ObjectWrapper> CREATOR = new Parcelable.Creator<ObjectWrapper>() {
        public ObjectWrapper createFromParcel(Parcel parcel) {
            ObjectWrapper objectWrapper = new ObjectWrapper();
            objectWrapper.readFromParcel(parcel);
            return objectWrapper;
        }

        public ObjectWrapper[] newArray(int i) {
            return new ObjectWrapper[i];
        }
    };
    public static final int TYPE_CLASS = 4;
    public static final int TYPE_CLASS_TO_GET = 5;
    public static final int TYPE_OBJECT = 3;
    public static final int TYPE_OBJECT_TO_GET = 1;
    public static final int TYPE_OBJECT_TO_NEW = 0;
    private Class<?> mClass;
    private long mTimeStamp;
    private int mType;

    private ObjectWrapper() {
    }

    public ObjectWrapper(Class<?> cls, int i) {
        setName(!cls.isAnnotationPresent(ClassId.class), TypeUtils.getClassId(cls));
        this.mClass = cls;
        this.mTimeStamp = TimeStampGenerator.getTimeStamp();
        this.mType = i;
    }

    public int describeContents() {
        return 0;
    }

    public Class<?> getObjectClass() {
        return this.mClass;
    }

    public long getTimeStamp() {
        return this.mTimeStamp;
    }

    public int getType() {
        return this.mType;
    }

    public void readFromParcel(Parcel parcel) {
        super.readFromParcel(parcel);
        this.mTimeStamp = parcel.readLong();
        this.mType = parcel.readInt();
    }

    public void setType(int i) {
        this.mType = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeLong(this.mTimeStamp);
        parcel.writeInt(this.mType);
    }
}
