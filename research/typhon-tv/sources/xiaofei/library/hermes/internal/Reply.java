package xiaofei.library.hermes.internal;

import android.os.Parcel;
import android.os.Parcelable;
import xiaofei.library.hermes.util.CodeUtils;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TypeCenter;
import xiaofei.library.hermes.wrapper.ParameterWrapper;
import xiaofei.library.hermes.wrapper.TypeWrapper;

public class Reply implements Parcelable {
    public static final Parcelable.Creator<Reply> CREATOR = new Parcelable.Creator<Reply>() {
        public Reply createFromParcel(Parcel parcel) {
            Reply reply = new Reply();
            reply.readFromParcel(parcel);
            return reply;
        }

        public Reply[] newArray(int i) {
            return new Reply[i];
        }
    };
    private static final TypeCenter TYPE_CENTER = TypeCenter.getInstance();
    private TypeWrapper mClass;
    private int mErrorCode;
    private String mErrorMessage;
    private Object mResult;

    private Reply() {
    }

    public Reply(int i, String str) {
        this.mErrorCode = i;
        this.mErrorMessage = str;
        this.mResult = null;
        this.mClass = null;
    }

    public Reply(ParameterWrapper parameterWrapper) {
        try {
            Class<?> classType = TYPE_CENTER.getClassType(parameterWrapper);
            this.mResult = CodeUtils.decode(parameterWrapper.getData(), classType);
            this.mErrorCode = 0;
            this.mErrorMessage = null;
            this.mClass = new TypeWrapper(classType);
        } catch (HermesException e) {
            e.printStackTrace();
            this.mErrorCode = e.getErrorCode();
            this.mErrorMessage = e.getMessage();
            this.mResult = null;
            this.mClass = null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public int getErrorCode() {
        return this.mErrorCode;
    }

    public String getMessage() {
        return this.mErrorMessage;
    }

    public Object getResult() {
        return this.mResult;
    }

    public void readFromParcel(Parcel parcel) {
        this.mErrorCode = parcel.readInt();
        ClassLoader classLoader = Reply.class.getClassLoader();
        this.mErrorMessage = parcel.readString();
        this.mClass = (TypeWrapper) parcel.readParcelable(classLoader);
        try {
            this.mResult = CodeUtils.decode(parcel.readString(), TYPE_CENTER.getClassType(this.mClass));
        } catch (Exception e) {
        }
    }

    public boolean success() {
        return this.mErrorCode == 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mErrorCode);
        parcel.writeString(this.mErrorMessage);
        parcel.writeParcelable(this.mClass, i);
        try {
            parcel.writeString(CodeUtils.encode(this.mResult));
        } catch (HermesException e) {
            e.printStackTrace();
        }
    }
}
