package xiaofei.library.hermes.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import android.support.v4.util.Pair;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import xiaofei.library.hermes.HermesListener;
import xiaofei.library.hermes.HermesService;
import xiaofei.library.hermes.internal.IHermesService;
import xiaofei.library.hermes.internal.IHermesServiceCallback;
import xiaofei.library.hermes.util.CallbackManager;
import xiaofei.library.hermes.util.CodeUtils;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TypeCenter;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class Channel {
    /* access modifiers changed from: private */
    public static final CallbackManager CALLBACK_MANAGER = CallbackManager.getInstance();
    private static final String TAG = "CHANNEL";
    /* access modifiers changed from: private */
    public static final TypeCenter TYPE_CENTER = TypeCenter.getInstance();
    private static volatile Channel sInstance = null;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<Class<? extends HermesService>, Boolean> mBindings = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<Class<? extends HermesService>, Boolean> mBounds = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public IHermesServiceCallback mHermesServiceCallback = new IHermesServiceCallback.Stub() {
        private Object[] getParameters(ParameterWrapper[] parameterWrapperArr) throws HermesException {
            if (parameterWrapperArr == null) {
                parameterWrapperArr = new ParameterWrapper[0];
            }
            int length = parameterWrapperArr.length;
            Object[] objArr = new Object[length];
            for (int i = 0; i < length; i++) {
                ParameterWrapper parameterWrapper = parameterWrapperArr[i];
                if (parameterWrapper == null) {
                    objArr[i] = null;
                } else {
                    Class<?> classType = Channel.TYPE_CENTER.getClassType(parameterWrapper);
                    String data = parameterWrapper.getData();
                    if (data == null) {
                        objArr[i] = null;
                    } else {
                        objArr[i] = CodeUtils.decode(data, classType);
                    }
                }
            }
            return objArr;
        }

        public Reply callback(CallbackMail callbackMail) {
            Pair<Boolean, Object> callback = Channel.CALLBACK_MANAGER.getCallback(callbackMail.getTimeStamp(), callbackMail.getIndex());
            if (callback == null) {
                return null;
            }
            final S s = callback.second;
            if (s == null) {
                return new Reply(22, "");
            }
            boolean booleanValue = ((Boolean) callback.first).booleanValue();
            try {
                final Method method = Channel.TYPE_CENTER.getMethod(s.getClass(), callbackMail.getMethod());
                final Object[] parameters = getParameters(callbackMail.getParameters());
                Object obj = null;
                IllegalAccessException illegalAccessException = null;
                if (booleanValue) {
                    if (Looper.getMainLooper() == Looper.myLooper()) {
                        try {
                            obj = method.invoke(s, parameters);
                        } catch (IllegalAccessException e) {
                            illegalAccessException = e;
                        } catch (InvocationTargetException e2) {
                            illegalAccessException = e2;
                        }
                    } else {
                        Channel.this.mUiHandler.post(new Runnable() {
                            public void run() {
                                try {
                                    method.invoke(s, parameters);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        return null;
                    }
                } else {
                    try {
                        obj = method.invoke(s, parameters);
                    } catch (IllegalAccessException e3) {
                        illegalAccessException = e3;
                    } catch (InvocationTargetException e4) {
                        illegalAccessException = e4;
                    }
                }
                if (illegalAccessException != null) {
                    illegalAccessException.printStackTrace();
                    throw new HermesException(18, "Error occurs when invoking method " + method + " on " + s, illegalAccessException);
                } else if (obj == null) {
                    return null;
                } else {
                    return new Reply(new ParameterWrapper(obj));
                }
            } catch (HermesException e5) {
                e5.printStackTrace();
                return new Reply(e5.getErrorCode(), e5.getErrorMessage());
            }
        }

        public void gc(List<Long> list, List<Integer> list2) throws RemoteException {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                Channel.CALLBACK_MANAGER.removeCallback(list.get(i).longValue(), list2.get(i).intValue());
            }
        }
    };
    private final ConcurrentHashMap<Class<? extends HermesService>, HermesServiceConnection> mHermesServiceConnections = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<Class<? extends HermesService>, IHermesService> mHermesServices = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public HermesListener mListener = null;
    /* access modifiers changed from: private */
    public Handler mUiHandler = new Handler(Looper.getMainLooper());

    private class HermesServiceConnection implements ServiceConnection {
        private Class<? extends HermesService> mClass;

        HermesServiceConnection(Class<? extends HermesService> cls) {
            this.mClass = cls;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            synchronized (Channel.this) {
                Channel.this.mBounds.put(this.mClass, true);
                Channel.this.mBindings.put(this.mClass, false);
                IHermesService asInterface = IHermesService.Stub.asInterface(iBinder);
                Channel.this.mHermesServices.put(this.mClass, asInterface);
                try {
                    asInterface.register(Channel.this.mHermesServiceCallback, Process.myPid());
                } catch (RemoteException e) {
                    e.printStackTrace();
                    Log.e(Channel.TAG, "Remote Exception: Check whether the process you are communicating with is still alive.");
                    return;
                }
            }
            if (Channel.this.mListener != null) {
                Channel.this.mListener.onHermesConnected(this.mClass);
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            synchronized (Channel.this) {
                Channel.this.mHermesServices.remove(this.mClass);
                Channel.this.mBounds.put(this.mClass, false);
                Channel.this.mBindings.put(this.mClass, false);
            }
            if (Channel.this.mListener != null) {
                Channel.this.mListener.onHermesDisconnected(this.mClass);
            }
        }
    }

    private Channel() {
    }

    public static Channel getInstance() {
        if (sInstance == null) {
            synchronized (Channel.class) {
                if (sInstance == null) {
                    sInstance = new Channel();
                }
            }
        }
        return sInstance;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0038, code lost:
        if (android.text.TextUtils.isEmpty(r8) == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003a, code lost:
        r2 = new android.content.Intent(r7, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003f, code lost:
        r7.bindService(r2, r1, 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0043, code lost:
        r2 = new android.content.Intent();
        r2.setClassName(r8, r9.getName());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void bind(android.content.Context r7, java.lang.String r8, java.lang.Class<? extends xiaofei.library.hermes.HermesService> r9) {
        /*
            r6 = this;
            r5 = 1
            monitor-enter(r6)
            boolean r3 = r6.getBound(r9)     // Catch:{ all -> 0x001c }
            if (r3 == 0) goto L_0x000a
            monitor-exit(r6)     // Catch:{ all -> 0x001c }
        L_0x0009:
            return
        L_0x000a:
            java.util.concurrent.ConcurrentHashMap<java.lang.Class<? extends xiaofei.library.hermes.HermesService>, java.lang.Boolean> r3 = r6.mBindings     // Catch:{ all -> 0x001c }
            java.lang.Object r0 = r3.get(r9)     // Catch:{ all -> 0x001c }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x001f
            boolean r3 = r0.booleanValue()     // Catch:{ all -> 0x001c }
            if (r3 == 0) goto L_0x001f
            monitor-exit(r6)     // Catch:{ all -> 0x001c }
            goto L_0x0009
        L_0x001c:
            r3 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x001c }
            throw r3
        L_0x001f:
            java.util.concurrent.ConcurrentHashMap<java.lang.Class<? extends xiaofei.library.hermes.HermesService>, java.lang.Boolean> r3 = r6.mBindings     // Catch:{ all -> 0x001c }
            r4 = 1
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x001c }
            r3.put(r9, r4)     // Catch:{ all -> 0x001c }
            xiaofei.library.hermes.internal.Channel$HermesServiceConnection r1 = new xiaofei.library.hermes.internal.Channel$HermesServiceConnection     // Catch:{ all -> 0x001c }
            r1.<init>(r9)     // Catch:{ all -> 0x001c }
            java.util.concurrent.ConcurrentHashMap<java.lang.Class<? extends xiaofei.library.hermes.HermesService>, xiaofei.library.hermes.internal.Channel$HermesServiceConnection> r3 = r6.mHermesServiceConnections     // Catch:{ all -> 0x001c }
            r3.put(r9, r1)     // Catch:{ all -> 0x001c }
            monitor-exit(r6)     // Catch:{ all -> 0x001c }
            boolean r3 = android.text.TextUtils.isEmpty(r8)
            if (r3 == 0) goto L_0x0043
            android.content.Intent r2 = new android.content.Intent
            r2.<init>(r7, r9)
        L_0x003f:
            r7.bindService(r2, r1, r5)
            goto L_0x0009
        L_0x0043:
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r3 = r9.getName()
            r2.setClassName(r8, r3)
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: xiaofei.library.hermes.internal.Channel.bind(android.content.Context, java.lang.String, java.lang.Class):void");
    }

    public void gc(Class<? extends HermesService> cls, List<Long> list) {
        IHermesService iHermesService = this.mHermesServices.get(cls);
        if (iHermesService == null) {
            Log.e(TAG, "Service Unavailable: Check whether you have disconnected the service before a process dies.");
            return;
        }
        try {
            iHermesService.gc(list);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public boolean getBound(Class<? extends HermesService> cls) {
        Boolean bool = this.mBounds.get(cls);
        return bool != null && bool.booleanValue();
    }

    public boolean isConnected(Class<? extends HermesService> cls) {
        IHermesService iHermesService = this.mHermesServices.get(cls);
        return iHermesService != null && iHermesService.asBinder().pingBinder();
    }

    public Reply send(Class<? extends HermesService> cls, Mail mail) {
        IHermesService iHermesService = this.mHermesServices.get(cls);
        if (iHermesService != null) {
            return iHermesService.send(mail);
        }
        try {
            return new Reply(2, "Service Unavailable: Check whether you have connected Hermes.");
        } catch (RemoteException e) {
            return new Reply(1, "Remote Exception: Check whether the process you are communicating with is still alive.");
        }
    }

    public void setHermesListener(HermesListener hermesListener) {
        this.mListener = hermesListener;
    }

    public void unbind(Context context, Class<? extends HermesService> cls) {
        synchronized (this) {
            Boolean bool = this.mBounds.get(cls);
            if (bool != null && bool.booleanValue()) {
                HermesServiceConnection hermesServiceConnection = this.mHermesServiceConnections.get(cls);
                if (hermesServiceConnection != null) {
                    context.unbindService(hermesServiceConnection);
                }
                this.mBounds.put(cls, false);
            }
        }
    }
}
