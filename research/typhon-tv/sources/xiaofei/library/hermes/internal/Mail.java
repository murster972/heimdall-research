package xiaofei.library.hermes.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class Mail implements Parcelable {
    public static final Parcelable.Creator<Mail> CREATOR = new Parcelable.Creator<Mail>() {
        public Mail createFromParcel(Parcel parcel) {
            Mail mail = new Mail();
            mail.readFromParcel(parcel);
            return mail;
        }

        public Mail[] newArray(int i) {
            return new Mail[i];
        }
    };
    private MethodWrapper mMethod;
    private ObjectWrapper mObject;
    private ParameterWrapper[] mParameters;
    private int mPid;
    private long mTimeStamp;

    private Mail() {
    }

    public Mail(long j, ObjectWrapper objectWrapper, MethodWrapper methodWrapper, ParameterWrapper[] parameterWrapperArr) {
        this.mTimeStamp = j;
        this.mPid = Process.myPid();
        this.mObject = objectWrapper;
        this.mMethod = methodWrapper;
        this.mParameters = parameterWrapperArr;
    }

    public int describeContents() {
        return 0;
    }

    public MethodWrapper getMethod() {
        return this.mMethod;
    }

    public ObjectWrapper getObject() {
        return this.mObject;
    }

    public ParameterWrapper[] getParameters() {
        return this.mParameters;
    }

    public int getPid() {
        return this.mPid;
    }

    public long getTimeStamp() {
        return this.mTimeStamp;
    }

    public void readFromParcel(Parcel parcel) {
        this.mTimeStamp = parcel.readLong();
        this.mPid = parcel.readInt();
        ClassLoader classLoader = Mail.class.getClassLoader();
        this.mObject = (ObjectWrapper) parcel.readParcelable(classLoader);
        this.mMethod = (MethodWrapper) parcel.readParcelable(classLoader);
        Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
        if (readParcelableArray == null) {
            this.mParameters = null;
            return;
        }
        int length = readParcelableArray.length;
        this.mParameters = new ParameterWrapper[length];
        for (int i = 0; i < length; i++) {
            this.mParameters[i] = (ParameterWrapper) readParcelableArray[i];
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.mTimeStamp);
        parcel.writeInt(this.mPid);
        parcel.writeParcelable(this.mObject, i);
        parcel.writeParcelable(this.mMethod, i);
        parcel.writeParcelableArray(this.mParameters, i);
    }
}
