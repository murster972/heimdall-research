package xiaofei.library.hermes.internal;

import android.os.Parcel;
import android.os.Parcelable;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class CallbackMail implements Parcelable {
    public static final Parcelable.Creator<CallbackMail> CREATOR = new Parcelable.Creator<CallbackMail>() {
        public CallbackMail createFromParcel(Parcel parcel) {
            CallbackMail callbackMail = new CallbackMail();
            callbackMail.readFromParcel(parcel);
            return callbackMail;
        }

        public CallbackMail[] newArray(int i) {
            return new CallbackMail[i];
        }
    };
    private int mIndex;
    private MethodWrapper mMethod;
    private ParameterWrapper[] mParameters;
    private long mTimeStamp;

    private CallbackMail() {
    }

    public CallbackMail(long j, int i, MethodWrapper methodWrapper, ParameterWrapper[] parameterWrapperArr) {
        this.mTimeStamp = j;
        this.mIndex = i;
        this.mMethod = methodWrapper;
        this.mParameters = parameterWrapperArr;
    }

    public int describeContents() {
        return 0;
    }

    public int getIndex() {
        return this.mIndex;
    }

    public MethodWrapper getMethod() {
        return this.mMethod;
    }

    public ParameterWrapper[] getParameters() {
        return this.mParameters;
    }

    public long getTimeStamp() {
        return this.mTimeStamp;
    }

    public void readFromParcel(Parcel parcel) {
        this.mTimeStamp = parcel.readLong();
        this.mIndex = parcel.readInt();
        ClassLoader classLoader = CallbackMail.class.getClassLoader();
        this.mMethod = (MethodWrapper) parcel.readParcelable(classLoader);
        Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
        if (readParcelableArray == null) {
            this.mParameters = null;
            return;
        }
        int length = readParcelableArray.length;
        this.mParameters = new ParameterWrapper[length];
        for (int i = 0; i < length; i++) {
            this.mParameters[i] = (ParameterWrapper) readParcelableArray[i];
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.mTimeStamp);
        parcel.writeInt(this.mIndex);
        parcel.writeParcelable(this.mMethod, i);
        parcel.writeParcelableArray(this.mParameters, i);
    }
}
