package xiaofei.library.hermes.internal;

import android.util.Log;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import xiaofei.library.hermes.HermesService;
import xiaofei.library.hermes.sender.Sender;
import xiaofei.library.hermes.sender.SenderDesignator;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.wrapper.ObjectWrapper;

public class HermesInvocationHandler implements InvocationHandler {
    private static final String TAG = "HERMES_INVOCATION";
    private Sender mSender;

    public HermesInvocationHandler(Class<? extends HermesService> cls, ObjectWrapper objectWrapper) {
        this.mSender = SenderDesignator.getPostOffice(cls, 3, objectWrapper);
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        try {
            Reply send = this.mSender.send(method, objArr);
            if (send == null) {
                return null;
            }
            if (send.success()) {
                return send.getResult();
            }
            Log.e(TAG, "Error occurs. Error " + send.getErrorCode() + ": " + send.getMessage());
            return null;
        } catch (HermesException e) {
            e.printStackTrace();
            Log.e(TAG, "Error occurs. Error " + e.getErrorCode() + ": " + e.getErrorMessage());
            return null;
        }
    }
}
