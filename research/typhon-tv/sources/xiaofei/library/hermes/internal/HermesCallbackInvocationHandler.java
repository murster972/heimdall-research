package xiaofei.library.hermes.internal;

import android.os.RemoteException;
import android.util.Log;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TypeUtils;
import xiaofei.library.hermes.wrapper.MethodWrapper;

public class HermesCallbackInvocationHandler implements InvocationHandler {
    private static final String TAG = "HERMES_CALLBACK";
    private IHermesServiceCallback mCallback;
    private int mIndex;
    private long mTimeStamp;

    public HermesCallbackInvocationHandler(long j, int i, IHermesServiceCallback iHermesServiceCallback) {
        this.mTimeStamp = j;
        this.mIndex = i;
        this.mCallback = iHermesServiceCallback;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        try {
            Reply callback = this.mCallback.callback(new CallbackMail(this.mTimeStamp, this.mIndex, new MethodWrapper(method), TypeUtils.objectToWrapper(objArr)));
            if (callback == null) {
                return null;
            }
            if (callback.success()) {
                return callback.getResult();
            }
            Log.e(TAG, "Error occurs: " + callback.getMessage());
            return null;
        } catch (HermesException e) {
            Log.e(TAG, "Error occurs but does not crash the app.", e);
            return null;
        } catch (RemoteException e2) {
            Log.e(TAG, "Error occurs but does not crash the app.", e2);
            return null;
        }
    }
}
