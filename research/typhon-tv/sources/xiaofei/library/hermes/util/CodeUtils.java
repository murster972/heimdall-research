package xiaofei.library.hermes.util;

import com.google.gson.Gson;

public class CodeUtils {
    private static final Gson GSON = new Gson();

    private CodeUtils() {
    }

    public static <T> T decode(String str, Class<T> cls) throws HermesException {
        try {
            return GSON.fromJson(str, cls);
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        throw new HermesException(7, "Error occurs when Gson decodes data of the Class " + cls.getName());
    }

    public static String encode(Object obj) throws HermesException {
        if (obj == null) {
            return null;
        }
        try {
            return GSON.toJson(obj);
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        throw new HermesException(6, "Error occurs when Gson encodes Object " + obj + " to Json.");
    }
}
