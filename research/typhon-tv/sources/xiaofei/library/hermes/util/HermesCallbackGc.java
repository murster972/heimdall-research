package xiaofei.library.hermes.util;

import android.os.RemoteException;
import android.support.v4.util.Pair;
import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import xiaofei.library.hermes.internal.IHermesServiceCallback;

public class HermesCallbackGc {
    private static volatile HermesCallbackGc sInstance = null;
    private final ReferenceQueue<Object> mReferenceQueue = new ReferenceQueue<>();
    private final ConcurrentHashMap<PhantomReference<Object>, Triple<IHermesServiceCallback, Long, Integer>> mTimeStamps = new ConcurrentHashMap<>();

    private HermesCallbackGc() {
    }

    private void gc() {
        synchronized (this.mReferenceQueue) {
            HashMap hashMap = new HashMap();
            while (true) {
                PhantomReference phantomReference = (PhantomReference) this.mReferenceQueue.poll();
                if (phantomReference == null) {
                    break;
                }
                Triple remove = this.mTimeStamps.remove(phantomReference);
                if (remove != null) {
                    Pair pair = (Pair) hashMap.get(remove.first);
                    if (pair == null) {
                        pair = new Pair(new ArrayList(), new ArrayList());
                        hashMap.put(remove.first, pair);
                    }
                    ((ArrayList) pair.first).add(remove.second);
                    ((ArrayList) pair.second).add(remove.third);
                }
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                Pair pair2 = (Pair) entry.getValue();
                if (!((ArrayList) pair2.first).isEmpty()) {
                    try {
                        ((IHermesServiceCallback) entry.getKey()).gc((List) pair2.first, (List) pair2.second);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static HermesCallbackGc getInstance() {
        if (sInstance == null) {
            synchronized (HermesCallbackGc.class) {
                if (sInstance == null) {
                    sInstance = new HermesCallbackGc();
                }
            }
        }
        return sInstance;
    }

    public void register(IHermesServiceCallback iHermesServiceCallback, Object obj, long j, int i) {
        gc();
        this.mTimeStamps.put(new PhantomReference(obj, this.mReferenceQueue), Triple.create(iHermesServiceCallback, Long.valueOf(j), Integer.valueOf(i)));
    }
}
