package xiaofei.library.hermes.util;

import android.app.Activity;
import android.app.Application;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import net.pubnative.library.request.PubnativeRequest;
import xiaofei.library.hermes.annotation.ClassId;
import xiaofei.library.hermes.annotation.GetInstance;
import xiaofei.library.hermes.annotation.MethodId;
import xiaofei.library.hermes.annotation.WithinProcess;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class TypeUtils {
    private static final HashSet<Class<?>> CONTEXT_CLASSES = new HashSet<Class<?>>() {
        {
            add(Context.class);
            add(ActionBarActivity.class);
            add(Activity.class);
            add(AppCompatActivity.class);
            add(Application.class);
            add(FragmentActivity.class);
            add(IntentService.class);
            add(Service.class);
        }
    };

    public static boolean arrayContainsAnnotation(Annotation[] annotationArr, Class<? extends Annotation> cls) {
        if (annotationArr == null || cls == null) {
            return false;
        }
        for (Annotation isInstance : annotationArr) {
            if (cls.isInstance(isInstance)) {
                return true;
            }
        }
        return false;
    }

    public static boolean classAssignable(Class<?>[] clsArr, Class<?>[] clsArr2) {
        if (clsArr.length != clsArr2.length) {
            return false;
        }
        int length = clsArr2.length;
        for (int i = 0; i < length; i++) {
            if (clsArr2[i] != null && !primitiveMatch(clsArr[i], clsArr2[i]) && !clsArr[i].isAssignableFrom(clsArr2[i])) {
                return false;
            }
        }
        return true;
    }

    public static String getClassId(Class<?> cls) {
        ClassId classId = (ClassId) cls.getAnnotation(ClassId.class);
        return classId != null ? classId.value() : cls.getName();
    }

    private static String getClassName(Class<?> cls) {
        return cls == Boolean.class ? "boolean" : cls == Byte.class ? "byte" : cls == Character.class ? "char" : cls == Short.class ? "short" : cls == Integer.class ? "int" : cls == Long.class ? PubnativeRequest.Parameters.LONG : cls == Float.class ? "float" : cls == Double.class ? "double" : cls == Void.class ? "void" : cls.getName();
    }

    public static Constructor<?> getConstructor(Class<?> cls, Class<?>[] clsArr) throws HermesException {
        Constructor<?> constructor = null;
        for (Constructor<?> constructor2 : cls.getConstructors()) {
            if (classAssignable(constructor2.getParameterTypes(), clsArr)) {
                if (constructor != null) {
                    throw new HermesException(14, "The class " + cls.getName() + " has too many constructors whose " + " parameter types match the required types.");
                }
                constructor = constructor2;
            }
        }
        if (constructor != null) {
            return constructor;
        }
        throw new HermesException(15, "The class " + cls.getName() + " do not have a constructor whose " + " parameter types match the required types.");
    }

    public static Class<?> getContextClass(Class<?> cls) {
        for (Class<?> cls2 = cls; cls2 != Object.class; cls2 = cls2.getSuperclass()) {
            if (CONTEXT_CLASSES.contains(cls2)) {
                return cls2;
            }
        }
        throw new IllegalArgumentException();
    }

    public static Method getMethod(Class<?> cls, String str, Class<?>[] clsArr, Class<?> cls2) throws HermesException {
        Method method = null;
        for (Method method2 : cls.getMethods()) {
            if (method2.getName().equals(str) && classAssignable(method2.getParameterTypes(), clsArr)) {
                if (method == null) {
                    method = method2;
                } else {
                    throw new HermesException(8, "There are more than one method named " + str + " of the class " + cls.getName() + " matching the parameters!");
                }
            }
        }
        if (method == null || method.getReturnType() == cls2) {
            return method;
        }
        throw new HermesException(10, "The method named " + str + " of the class " + cls.getName() + " matches the parameter types but not the return type. The return type is " + method.getReturnType().getName() + " but the required type is " + cls2.getName() + ". The method in the local interface must exactly " + "match the method in the remote class.");
    }

    public static Method getMethodForGettingInstance(Class<?> cls, String str, Class<?>[] clsArr) throws HermesException {
        Method method = null;
        for (Method method2 : cls.getMethods()) {
            String name = method2.getName();
            if (((str.equals("") && (name.equals("getInstance") || method2.isAnnotationPresent(GetInstance.class))) || (!str.equals("") && name.equals(str))) && classAssignable(method2.getParameterTypes(), clsArr)) {
                if (method == null) {
                    method = method2;
                } else {
                    throw new HermesException(11, "When getting instance, there are more than one method named " + str + " of the class " + cls.getName() + " matching the parameters!");
                }
            }
        }
        if (method == null) {
            throw new HermesException(13, "When getting instance, the method named " + str + " of the class " + cls.getName() + " is not found. The class must have a method for getting instance.");
        } else if (method.getReturnType() == cls) {
            return method;
        } else {
            throw new HermesException(12, "When getting instance, the method named " + str + " of the class " + cls.getName() + " matches the parameter types but not the return type. The return type is " + method.getReturnType().getName() + " but the required type is " + cls.getName() + ".");
        }
    }

    public static String getMethodId(Method method) {
        MethodId methodId = (MethodId) method.getAnnotation(MethodId.class);
        if (methodId != null) {
            return methodId.value();
        }
        StringBuilder sb = new StringBuilder(method.getName());
        sb.append('(').append(getMethodParameters(method.getParameterTypes())).append(')');
        return sb.toString();
    }

    public static String getMethodParameters(Class<?>[] clsArr) {
        StringBuilder sb = new StringBuilder();
        int length = clsArr.length;
        if (length == 0) {
            return sb.toString();
        }
        sb.append(getClassName(clsArr[0]));
        for (int i = 1; i < length; i++) {
            sb.append(",").append(getClassName(clsArr[i]));
        }
        return sb.toString();
    }

    public static void methodMatch(Method method, MethodWrapper methodWrapper) throws HermesException {
        methodParameterTypeMatch(method, methodWrapper);
        methodReturnTypeMatch(method, methodWrapper);
    }

    public static void methodParameterTypeMatch(Method method, MethodWrapper methodWrapper) throws HermesException {
        Class[] classTypes = TypeCenter.getInstance().getClassTypes(methodWrapper.getParameterTypes());
        Class[] parameterTypes = method.getParameterTypes();
        if (classTypes.length != parameterTypes.length) {
            throw new HermesException(9, "The number of method parameters do not match. Method " + method + " has " + parameterTypes.length + " parameters. " + "The required method has " + classTypes.length + " parameters.");
        }
        int length = classTypes.length;
        for (int i = 0; i < length; i++) {
            if (classTypes[i].isPrimitive() || parameterTypes[i].isPrimitive()) {
                if (!primitiveMatch(classTypes[i], parameterTypes[i])) {
                    throw new HermesException(9, "The parameter type of method " + method + " do not match at index " + i + ".");
                }
            } else if (classTypes[i] != parameterTypes[i] && !primitiveMatch(classTypes[i], parameterTypes[i])) {
                throw new HermesException(9, "The parameter type of method " + method + " do not match at index " + i + ".");
            }
        }
    }

    public static void methodReturnTypeMatch(Method method, MethodWrapper methodWrapper) throws HermesException {
        Class<?> returnType = method.getReturnType();
        Class<?> classType = TypeCenter.getInstance().getClassType(methodWrapper.getReturnType());
        if (returnType.isPrimitive() || classType.isPrimitive()) {
            if (!primitiveMatch(returnType, classType)) {
                throw new HermesException(10, "The return type of methods do not match. Method " + method + " return type: " + returnType.getName() + ". The required is " + classType.getName());
            }
        } else if (classType != returnType && !primitiveMatch(returnType, classType)) {
            throw new HermesException(10, "The return type of methods do not match. Method " + method + " return type: " + returnType.getName() + ". The required is " + classType.getName());
        }
    }

    public static ParameterWrapper[] objectToWrapper(Object[] objArr) throws HermesException {
        if (objArr == null) {
            objArr = new Object[0];
        }
        int length = objArr.length;
        ParameterWrapper[] parameterWrapperArr = new ParameterWrapper[length];
        int i = 0;
        while (i < length) {
            try {
                parameterWrapperArr[i] = new ParameterWrapper(objArr[i]);
                i++;
            } catch (HermesException e) {
                e.printStackTrace();
                throw new HermesException(e.getErrorCode(), "Error happens at parameter encoding, and parameter index is " + i + ". See the stack trace for more information.", e);
            }
        }
        return parameterWrapperArr;
    }

    public static boolean primitiveMatch(Class<?> cls, Class<?> cls2) {
        if (!cls.isPrimitive() && !cls2.isPrimitive()) {
            return false;
        }
        if (cls == cls2) {
            return true;
        }
        if (cls.isPrimitive()) {
            return primitiveMatch(cls2, cls);
        }
        if (cls == Boolean.class && cls2 == Boolean.TYPE) {
            return true;
        }
        if (cls == Byte.class && cls2 == Byte.TYPE) {
            return true;
        }
        if (cls == Character.class && cls2 == Character.TYPE) {
            return true;
        }
        if (cls == Short.class && cls2 == Short.TYPE) {
            return true;
        }
        if (cls == Integer.class && cls2 == Integer.TYPE) {
            return true;
        }
        if (cls == Long.class && cls2 == Long.TYPE) {
            return true;
        }
        if (cls == Float.class && cls2 == Float.TYPE) {
            return true;
        }
        if (cls == Double.class && cls2 == Double.TYPE) {
            return true;
        }
        return cls == Void.class && cls2 == Void.TYPE;
    }

    public static void validateAccessible(Class<?> cls) throws HermesException {
        if (cls.isAnnotationPresent(WithinProcess.class)) {
            throw new HermesException(19, "Class " + cls.getName() + " has a WithProcess annotation on it, " + "so it cannot be accessed from outside the process.");
        }
    }

    public static void validateAccessible(Constructor<?> constructor) throws HermesException {
        if (constructor.isAnnotationPresent(WithinProcess.class)) {
            throw new HermesException(20, "Constructor " + constructor.getName() + " of class " + constructor.getDeclaringClass().getName() + " has a WithProcess annotation on it, so it cannot be accessed from " + "outside the process.");
        }
    }

    public static void validateAccessible(Method method) throws HermesException {
        if (method.isAnnotationPresent(WithinProcess.class)) {
            throw new HermesException(20, "Method " + method.getName() + " of class " + method.getDeclaringClass().getName() + " has a WithProcess annotation on it, so it cannot be accessed from " + "outside the process.");
        }
    }

    public static void validateClass(Class<?> cls) {
        if (cls == null) {
            throw new IllegalArgumentException("Class object is null.");
        } else if (!cls.isPrimitive() && !cls.isInterface()) {
            if (cls.isAnnotationPresent(WithinProcess.class)) {
                throw new IllegalArgumentException("Error occurs when registering class " + cls.getName() + ". Class with a WithinProcess annotation presented on it cannot be accessed" + " from outside the process.");
            } else if (cls.isAnonymousClass()) {
                throw new IllegalArgumentException("Error occurs when registering class " + cls.getName() + ". Anonymous class cannot be accessed from outside the process.");
            } else if (cls.isLocalClass()) {
                throw new IllegalArgumentException("Error occurs when registering class " + cls.getName() + ". Local class cannot be accessed from outside the process.");
            } else if (!Context.class.isAssignableFrom(cls) && Modifier.isAbstract(cls.getModifiers())) {
                throw new IllegalArgumentException("Error occurs when registering class " + cls.getName() + ". Abstract class cannot be accessed from outside the process.");
            }
        }
    }

    public static void validateServiceInterface(Class<?> cls) {
        if (cls == null) {
            throw new IllegalArgumentException("Class object is null.");
        } else if (!cls.isInterface()) {
            throw new IllegalArgumentException("Only interfaces can be passed as the parameters.");
        }
    }
}
