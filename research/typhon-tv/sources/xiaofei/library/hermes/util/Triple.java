package xiaofei.library.hermes.util;

public class Triple<T1, T2, T3> {
    public final T1 first;
    public final T2 second;
    public final T3 third;

    @Deprecated
    public Triple(T1 t1, T2 t2, T3 t3) {
        this.first = t1;
        this.second = t2;
        this.third = t3;
    }

    public static <T1, T2, T3> Triple<T1, T2, T3> create(T1 t1, T2 t2, T3 t3) {
        return new Triple<>(t1, t2, t3);
    }
}
