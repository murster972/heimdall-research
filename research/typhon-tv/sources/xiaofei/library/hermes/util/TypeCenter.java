package xiaofei.library.hermes.util;

import android.text.TextUtils;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import net.pubnative.library.request.PubnativeRequest;
import xiaofei.library.hermes.annotation.ClassId;
import xiaofei.library.hermes.annotation.MethodId;
import xiaofei.library.hermes.wrapper.BaseWrapper;
import xiaofei.library.hermes.wrapper.MethodWrapper;

public class TypeCenter {
    private static volatile TypeCenter sInstance = null;
    private final ConcurrentHashMap<String, Class<?>> mAnnotatedClasses = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Class<?>, ConcurrentHashMap<String, Method>> mAnnotatedMethods = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Class<?>> mRawClasses = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Class<?>, ConcurrentHashMap<String, Method>> mRawMethods = new ConcurrentHashMap<>();

    private TypeCenter() {
    }

    public static TypeCenter getInstance() {
        if (sInstance == null) {
            synchronized (TypeCenter.class) {
                if (sInstance == null) {
                    sInstance = new TypeCenter();
                }
            }
        }
        return sInstance;
    }

    private void registerClass(Class<?> cls) {
        ClassId classId = (ClassId) cls.getAnnotation(ClassId.class);
        if (classId == null) {
            this.mRawClasses.putIfAbsent(cls.getName(), cls);
            return;
        }
        this.mAnnotatedClasses.putIfAbsent(classId.value(), cls);
    }

    private void registerMethod(Class<?> cls) {
        for (Method method : cls.getMethods()) {
            if (((MethodId) method.getAnnotation(MethodId.class)) == null) {
                this.mRawMethods.putIfAbsent(cls, new ConcurrentHashMap());
                this.mRawMethods.get(cls).putIfAbsent(TypeUtils.getMethodId(method), method);
            } else {
                this.mAnnotatedMethods.putIfAbsent(cls, new ConcurrentHashMap());
                this.mAnnotatedMethods.get(cls).putIfAbsent(TypeUtils.getMethodId(method), method);
            }
        }
    }

    public Class<?> getClassType(BaseWrapper baseWrapper) throws HermesException {
        Class<?> cls;
        String name = baseWrapper.getName();
        if (TextUtils.isEmpty(name)) {
            return null;
        }
        if (baseWrapper.isName()) {
            Class<?> cls2 = this.mRawClasses.get(name);
            if (cls2 != null) {
                return cls2;
            }
            if (name.equals("boolean")) {
                cls = Boolean.TYPE;
            } else if (name.equals("byte")) {
                cls = Byte.TYPE;
            } else if (name.equals("char")) {
                cls = Character.TYPE;
            } else if (name.equals("short")) {
                cls = Short.TYPE;
            } else if (name.equals("int")) {
                cls = Integer.TYPE;
            } else if (name.equals(PubnativeRequest.Parameters.LONG)) {
                cls = Long.TYPE;
            } else if (name.equals("float")) {
                cls = Float.TYPE;
            } else if (name.equals("double")) {
                cls = Double.TYPE;
            } else if (name.equals("void")) {
                cls = Void.TYPE;
            } else {
                try {
                    cls = Class.forName(name);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    throw new HermesException(16, "Cannot find class " + name + ". Classes without ClassId annotation on it " + "should be located at the same package and have the same name, " + "EVEN IF the source code has been obfuscated by Proguard.");
                }
            }
            this.mRawClasses.putIfAbsent(name, cls);
            return cls;
        }
        Class<?> cls3 = this.mAnnotatedClasses.get(name);
        if (cls3 != null) {
            return cls3;
        }
        throw new HermesException(16, "Cannot find class with ClassId annotation on it. ClassId = " + name + ". Please add the same annotation on the corresponding class in the remote process" + " and register it. Have you forgotten to register the class?");
    }

    public Class<?>[] getClassTypes(BaseWrapper[] baseWrapperArr) throws HermesException {
        Class<?>[] clsArr = new Class[baseWrapperArr.length];
        for (int i = 0; i < baseWrapperArr.length; i++) {
            clsArr[i] = getClassType(baseWrapperArr[i]);
        }
        return clsArr;
    }

    public Method getMethod(Class<?> cls, MethodWrapper methodWrapper) throws HermesException {
        String name = methodWrapper.getName();
        if (methodWrapper.isName()) {
            this.mRawMethods.putIfAbsent(cls, new ConcurrentHashMap());
            ConcurrentHashMap concurrentHashMap = this.mRawMethods.get(cls);
            Method method = (Method) concurrentHashMap.get(name);
            if (method != null) {
                TypeUtils.methodReturnTypeMatch(method, methodWrapper);
                Method method2 = method;
                return method;
            }
            Method method3 = TypeUtils.getMethod(cls, name.substring(0, name.indexOf(40)), getClassTypes(methodWrapper.getParameterTypes()), getClassType(methodWrapper.getReturnType()));
            if (method3 == null) {
                throw new HermesException(17, "Method not found: " + name + " in class " + cls.getName());
            }
            concurrentHashMap.put(name, method3);
            Method method4 = method3;
            return method3;
        }
        Method method5 = (Method) this.mAnnotatedMethods.get(cls).get(name);
        if (method5 != null) {
            TypeUtils.methodMatch(method5, methodWrapper);
            Method method6 = method5;
            return method5;
        }
        throw new HermesException(17, "Method not found in class " + cls.getName() + ". Method id = " + name + ". " + "Please add the same annotation on the corresponding method in the remote process.");
    }

    public void register(Class<?> cls) {
        TypeUtils.validateClass(cls);
        registerClass(cls);
        registerMethod(cls);
    }
}
