package xiaofei.library.hermes.util;

public class HermesException extends Exception {
    private int mErrorCode;
    private String mErrorMessage;

    public HermesException(int i, String str) {
        this.mErrorCode = i;
        this.mErrorMessage = str;
    }

    public HermesException(int i, String str, Throwable th) {
        super(th);
        this.mErrorCode = i;
        this.mErrorMessage = str;
    }

    public int getErrorCode() {
        return this.mErrorCode;
    }

    public String getErrorMessage() {
        return this.mErrorMessage;
    }
}
