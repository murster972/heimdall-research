package xiaofei.library.hermes.util;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import xiaofei.library.hermes.HermesService;
import xiaofei.library.hermes.internal.Channel;

public class HermesGc {
    private static final Channel CHANNEL = Channel.getInstance();
    private static volatile HermesGc sInstance = null;
    private final ReferenceQueue<Object> mReferenceQueue = new ReferenceQueue<>();
    private final ConcurrentHashMap<Long, Class<? extends HermesService>> mServices = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<PhantomReference<Object>, Long> mTimeStamps = new ConcurrentHashMap<>();

    private HermesGc() {
    }

    private void gc() {
        Class remove;
        synchronized (this.mReferenceQueue) {
            HashMap hashMap = new HashMap();
            while (true) {
                PhantomReference phantomReference = (PhantomReference) this.mReferenceQueue.poll();
                if (phantomReference == null) {
                    break;
                }
                Long remove2 = this.mTimeStamps.remove(phantomReference);
                if (!(remove2 == null || (remove = this.mServices.remove(remove2)) == null)) {
                    ArrayList arrayList = (ArrayList) hashMap.get(remove);
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                        hashMap.put(remove, arrayList);
                    }
                    arrayList.add(remove2);
                }
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                ArrayList arrayList2 = (ArrayList) entry.getValue();
                if (!arrayList2.isEmpty()) {
                    CHANNEL.gc((Class) entry.getKey(), arrayList2);
                }
            }
        }
    }

    public static HermesGc getInstance() {
        if (sInstance == null) {
            synchronized (HermesGc.class) {
                if (sInstance == null) {
                    sInstance = new HermesGc();
                }
            }
        }
        return sInstance;
    }

    public void register(Class<? extends HermesService> cls, Object obj, Long l) {
        gc();
        this.mTimeStamps.put(new PhantomReference(obj, this.mReferenceQueue), l);
        this.mServices.put(l, cls);
    }
}
