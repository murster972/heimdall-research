package xiaofei.library.hermes.util;

import android.support.v4.util.Pair;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;

public class CallbackManager {
    private static final int MAX_INDEX = 10;
    private static final String TAG = "CallbackManager";
    private static volatile CallbackManager sInstance = null;
    private final ConcurrentHashMap<Long, CallbackWrapper> mCallbackWrappers = new ConcurrentHashMap<>();

    private static class CallbackWrapper {
        private Object mCallback;
        private boolean mUiThread;

        CallbackWrapper(boolean z, Object obj, boolean z2) {
            if (z) {
                this.mCallback = new WeakReference(obj);
            } else {
                this.mCallback = obj;
            }
            this.mUiThread = z2;
        }

        public Pair<Boolean, Object> get() {
            return new Pair<>(Boolean.valueOf(this.mUiThread), this.mCallback instanceof WeakReference ? ((WeakReference) this.mCallback).get() : this.mCallback);
        }
    }

    private CallbackManager() {
    }

    public static CallbackManager getInstance() {
        if (sInstance == null) {
            synchronized (CallbackManager.class) {
                if (sInstance == null) {
                    sInstance = new CallbackManager();
                }
            }
        }
        return sInstance;
    }

    private static long getKey(long j, int i) {
        if (i < 10) {
            return (10 * j) + ((long) i);
        }
        throw new IllegalArgumentException("Index should be less than 10");
    }

    public void addCallback(long j, int i, Object obj, boolean z, boolean z2) {
        this.mCallbackWrappers.put(Long.valueOf(getKey(j, i)), new CallbackWrapper(z, obj, z2));
    }

    public Pair<Boolean, Object> getCallback(long j, int i) {
        long key = getKey(j, i);
        CallbackWrapper callbackWrapper = this.mCallbackWrappers.get(Long.valueOf(key));
        if (callbackWrapper == null) {
            return null;
        }
        Pair<Boolean, Object> pair = callbackWrapper.get();
        if (pair.second != null) {
            return pair;
        }
        this.mCallbackWrappers.remove(Long.valueOf(key));
        return pair;
    }

    public void removeCallback(long j, int i) {
        if (this.mCallbackWrappers.remove(Long.valueOf(getKey(j, i))) == null) {
            Log.e(TAG, "An error occurs in the callback GC.");
        }
    }
}
