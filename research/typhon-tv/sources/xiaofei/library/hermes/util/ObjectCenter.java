package xiaofei.library.hermes.util;

import android.util.Log;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ObjectCenter {
    private static final String TAG = "ObjectCenter";
    private static volatile ObjectCenter sInstance = null;
    private final ConcurrentHashMap<Long, Object> mObjects = new ConcurrentHashMap<>();

    private ObjectCenter() {
    }

    public static ObjectCenter getInstance() {
        if (sInstance == null) {
            synchronized (ObjectCenter.class) {
                if (sInstance == null) {
                    sInstance = new ObjectCenter();
                }
            }
        }
        return sInstance;
    }

    public void deleteObjects(List<Long> list) {
        for (Long remove : list) {
            if (this.mObjects.remove(remove) == null) {
                Log.e(TAG, "An error occurs in the GC.");
            }
        }
    }

    public Object getObject(Long l) {
        return this.mObjects.get(l);
    }

    public void putObject(long j, Object obj) {
        this.mObjects.put(Long.valueOf(j), obj);
    }
}
