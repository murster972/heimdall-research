package xiaofei.library.hermes.sender;

import xiaofei.library.hermes.HermesService;
import xiaofei.library.hermes.wrapper.ObjectWrapper;

public class SenderDesignator {
    public static final int TYPE_GET_INSTANCE = 1;
    public static final int TYPE_GET_UTILITY_CLASS = 2;
    public static final int TYPE_INVOKE_METHOD = 3;
    public static final int TYPE_NEW_INSTANCE = 0;

    public static Sender getPostOffice(Class<? extends HermesService> cls, int i, ObjectWrapper objectWrapper) {
        switch (i) {
            case 0:
                return new InstanceCreatingSender(cls, objectWrapper);
            case 1:
                return new InstanceGettingSender(cls, objectWrapper);
            case 2:
                return new UtilityGettingSender(cls, objectWrapper);
            case 3:
                return new ObjectSender(cls, objectWrapper);
            default:
                throw new IllegalArgumentException("Type " + i + " is not supported.");
        }
    }
}
