package xiaofei.library.hermes.sender;

import android.content.Context;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import xiaofei.library.hermes.HermesService;
import xiaofei.library.hermes.annotation.Background;
import xiaofei.library.hermes.annotation.WeakRef;
import xiaofei.library.hermes.internal.Channel;
import xiaofei.library.hermes.internal.Mail;
import xiaofei.library.hermes.internal.Reply;
import xiaofei.library.hermes.util.CallbackManager;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TimeStampGenerator;
import xiaofei.library.hermes.util.TypeCenter;
import xiaofei.library.hermes.util.TypeUtils;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public abstract class Sender {
    private static final CallbackManager CALLBACK_MANAGER = CallbackManager.getInstance();
    private static final Channel CHANNEL = Channel.getInstance();
    protected static final TypeCenter TYPE_CENTER = TypeCenter.getInstance();
    private MethodWrapper mMethod;
    private ObjectWrapper mObject;
    private ParameterWrapper[] mParameters;
    private Class<? extends HermesService> mService;
    private long mTimeStamp;

    public Sender(Class<? extends HermesService> cls, ObjectWrapper objectWrapper) {
        this.mService = cls;
        this.mObject = objectWrapper;
    }

    private final ParameterWrapper[] getParameterWrappers(Method method, Object[] objArr) throws HermesException {
        int length = objArr.length;
        ParameterWrapper[] parameterWrapperArr = new ParameterWrapper[length];
        if (method != null) {
            Class[] parameterTypes = method.getParameterTypes();
            Annotation[][] parameterAnnotations = method.getParameterAnnotations();
            for (int i = 0; i < length; i++) {
                if (parameterTypes[i].isInterface()) {
                    Object obj = objArr[i];
                    if (obj != null) {
                        parameterWrapperArr[i] = new ParameterWrapper(parameterTypes[i], (Object) null);
                    } else {
                        parameterWrapperArr[i] = new ParameterWrapper((Object) null);
                    }
                    if (!(parameterAnnotations[i] == null || obj == null)) {
                        CALLBACK_MANAGER.addCallback(this.mTimeStamp, i, obj, TypeUtils.arrayContainsAnnotation(parameterAnnotations[i], WeakRef.class), !TypeUtils.arrayContainsAnnotation(parameterAnnotations[i], Background.class));
                    }
                } else if (Context.class.isAssignableFrom(parameterTypes[i])) {
                    parameterWrapperArr[i] = new ParameterWrapper(TypeUtils.getContextClass(parameterTypes[i]), (Object) null);
                } else {
                    parameterWrapperArr[i] = new ParameterWrapper(objArr[i]);
                }
            }
        } else {
            for (int i2 = 0; i2 < length; i2++) {
                parameterWrapperArr[i2] = new ParameterWrapper(objArr[i2]);
            }
        }
        return parameterWrapperArr;
    }

    private void registerCallbackMethodParameterTypes(Class<?> cls) {
        for (Method parameterTypes : cls.getMethods()) {
            for (Class register : parameterTypes.getParameterTypes()) {
                TYPE_CENTER.register(register);
            }
        }
    }

    private void registerClass(Method method) throws HermesException {
        if (method != null) {
            for (Class cls : method.getParameterTypes()) {
                if (cls.isInterface()) {
                    TYPE_CENTER.register(cls);
                    registerCallbackMethodParameterTypes(cls);
                }
            }
            TYPE_CENTER.register(method.getReturnType());
        }
    }

    /* access modifiers changed from: protected */
    public abstract MethodWrapper getMethodWrapper(Method method, ParameterWrapper[] parameterWrapperArr) throws HermesException;

    public ObjectWrapper getObject() {
        return this.mObject;
    }

    public final Reply send(Method method, Object[] objArr) throws HermesException {
        this.mTimeStamp = TimeStampGenerator.getTimeStamp();
        if (objArr == null) {
            objArr = new Object[0];
        }
        ParameterWrapper[] parameterWrappers = getParameterWrappers(method, objArr);
        this.mMethod = getMethodWrapper(method, parameterWrappers);
        registerClass(method);
        setParameterWrappers(parameterWrappers);
        return CHANNEL.send(this.mService, new Mail(this.mTimeStamp, this.mObject, this.mMethod, this.mParameters));
    }

    /* access modifiers changed from: protected */
    public void setParameterWrappers(ParameterWrapper[] parameterWrapperArr) {
        this.mParameters = parameterWrapperArr;
    }
}
