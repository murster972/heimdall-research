package xiaofei.library.hermes.sender;

import java.lang.reflect.Method;
import xiaofei.library.hermes.HermesService;
import xiaofei.library.hermes.util.CodeUtils;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class InstanceGettingSender extends Sender {
    public InstanceGettingSender(Class<? extends HermesService> cls, ObjectWrapper objectWrapper) {
        super(cls, objectWrapper);
    }

    /* access modifiers changed from: protected */
    public MethodWrapper getMethodWrapper(Method method, ParameterWrapper[] parameterWrapperArr) throws HermesException {
        try {
            String str = (String) CodeUtils.decode(parameterWrapperArr[0].getData(), String.class);
            int length = parameterWrapperArr.length;
            Class[] clsArr = new Class[(length - 1)];
            for (int i = 1; i < length; i++) {
                ParameterWrapper parameterWrapper = parameterWrapperArr[i];
                clsArr[i - 1] = parameterWrapper == null ? null : parameterWrapper.getClassType();
            }
            return new MethodWrapper(str, clsArr);
        } catch (HermesException e) {
            e.printStackTrace();
            throw new HermesException(7, "Error occurs when decoding the method name.");
        }
    }

    /* access modifiers changed from: protected */
    public void setParameterWrappers(ParameterWrapper[] parameterWrapperArr) {
        int length = parameterWrapperArr.length;
        ParameterWrapper[] parameterWrapperArr2 = new ParameterWrapper[(length - 1)];
        for (int i = 1; i < length; i++) {
            parameterWrapperArr2[i - 1] = parameterWrapperArr[i];
        }
        super.setParameterWrappers(parameterWrapperArr2);
    }
}
