package xiaofei.library.hermes.sender;

import java.lang.reflect.Method;
import xiaofei.library.hermes.HermesService;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class InstanceCreatingSender extends Sender {
    private Class<?>[] mConstructorParameterTypes;

    public InstanceCreatingSender(Class<? extends HermesService> cls, ObjectWrapper objectWrapper) {
        super(cls, objectWrapper);
    }

    /* access modifiers changed from: protected */
    public MethodWrapper getMethodWrapper(Method method, ParameterWrapper[] parameterWrapperArr) {
        int length = parameterWrapperArr == null ? 0 : parameterWrapperArr.length;
        this.mConstructorParameterTypes = new Class[length];
        for (int i = 0; i < length; i++) {
            try {
                ParameterWrapper parameterWrapper = parameterWrapperArr[i];
                this.mConstructorParameterTypes[i] = parameterWrapper == null ? null : parameterWrapper.getClassType();
            } catch (Exception e) {
            }
        }
        return new MethodWrapper(this.mConstructorParameterTypes);
    }
}
