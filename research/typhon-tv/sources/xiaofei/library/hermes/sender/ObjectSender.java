package xiaofei.library.hermes.sender;

import java.lang.reflect.Method;
import xiaofei.library.hermes.HermesService;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class ObjectSender extends Sender {
    public ObjectSender(Class<? extends HermesService> cls, ObjectWrapper objectWrapper) {
        super(cls, objectWrapper);
    }

    /* access modifiers changed from: protected */
    public MethodWrapper getMethodWrapper(Method method, ParameterWrapper[] parameterWrapperArr) {
        return new MethodWrapper(method);
    }
}
