package xiaofei.library.hermes.receiver;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TypeUtils;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class ObjectReceiver extends Receiver {
    private Method mMethod;
    private Object mObject = OBJECT_CENTER.getObject(Long.valueOf(getObjectTimeStamp()));

    public ObjectReceiver(ObjectWrapper objectWrapper) {
        super(objectWrapper);
    }

    /* access modifiers changed from: protected */
    public Object invokeMethod() throws HermesException {
        ReflectiveOperationException reflectiveOperationException;
        try {
            return this.mMethod.invoke(this.mObject, getParameters());
        } catch (IllegalAccessException e) {
            reflectiveOperationException = e;
        } catch (InvocationTargetException e2) {
            reflectiveOperationException = e2;
        }
        reflectiveOperationException.printStackTrace();
        throw new HermesException(18, "Error occurs when invoking method " + this.mMethod + " on " + this.mObject, reflectiveOperationException);
    }

    public void setMethod(MethodWrapper methodWrapper, ParameterWrapper[] parameterWrapperArr) throws HermesException {
        Method method = TYPE_CENTER.getMethod(this.mObject.getClass(), methodWrapper);
        TypeUtils.validateAccessible(method);
        this.mMethod = method;
    }
}
