package xiaofei.library.hermes.receiver;

import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.wrapper.ObjectWrapper;

public class ReceiverDesignator {
    public static Receiver getReceiver(ObjectWrapper objectWrapper) throws HermesException {
        int type = objectWrapper.getType();
        switch (type) {
            case 0:
                return new InstanceCreatingReceiver(objectWrapper);
            case 1:
                return new InstanceGettingReceiver(objectWrapper);
            case 3:
                return new ObjectReceiver(objectWrapper);
            case 4:
                return new UtilityReceiver(objectWrapper);
            case 5:
                return new UtilityGettingReceiver(objectWrapper);
            default:
                throw new HermesException(4, "Type " + type + " is not supported.");
        }
    }
}
