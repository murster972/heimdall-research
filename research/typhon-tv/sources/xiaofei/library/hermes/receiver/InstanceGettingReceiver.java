package xiaofei.library.hermes.receiver;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TypeUtils;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class InstanceGettingReceiver extends Receiver {
    private Method mMethod;
    private Class<?> mObjectClass;

    public InstanceGettingReceiver(ObjectWrapper objectWrapper) throws HermesException {
        super(objectWrapper);
        Class<?> classType = TYPE_CENTER.getClassType(objectWrapper);
        TypeUtils.validateAccessible(classType);
        this.mObjectClass = classType;
    }

    /* access modifiers changed from: protected */
    public Object invokeMethod() throws HermesException {
        ReflectiveOperationException reflectiveOperationException;
        try {
            OBJECT_CENTER.putObject(getObjectTimeStamp(), this.mMethod.invoke((Object) null, getParameters()));
            return null;
        } catch (IllegalAccessException e) {
            reflectiveOperationException = e;
            reflectiveOperationException.printStackTrace();
            throw new HermesException(18, "Error occurs when invoking method " + this.mMethod + " to get an instance of " + this.mObjectClass.getName(), reflectiveOperationException);
        } catch (InvocationTargetException e2) {
            reflectiveOperationException = e2;
            reflectiveOperationException.printStackTrace();
            throw new HermesException(18, "Error occurs when invoking method " + this.mMethod + " to get an instance of " + this.mObjectClass.getName(), reflectiveOperationException);
        }
    }

    public void setMethod(MethodWrapper methodWrapper, ParameterWrapper[] parameterWrapperArr) throws HermesException {
        int length = parameterWrapperArr.length;
        Class[] clsArr = new Class[length];
        for (int i = 0; i < length; i++) {
            clsArr[i] = TYPE_CENTER.getClassType(parameterWrapperArr[i]);
        }
        Method methodForGettingInstance = TypeUtils.getMethodForGettingInstance(this.mObjectClass, methodWrapper.getName(), clsArr);
        if (!Modifier.isStatic(methodForGettingInstance.getModifiers())) {
            throw new HermesException(21, "Method " + methodForGettingInstance.getName() + " of class " + this.mObjectClass.getName() + " is not static. " + "Only the static method can be invoked to get an instance.");
        }
        TypeUtils.validateAccessible(methodForGettingInstance);
        this.mMethod = methodForGettingInstance;
    }
}
