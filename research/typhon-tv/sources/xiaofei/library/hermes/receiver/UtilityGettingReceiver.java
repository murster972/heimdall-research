package xiaofei.library.hermes.receiver;

import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TypeUtils;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class UtilityGettingReceiver extends Receiver {
    public UtilityGettingReceiver(ObjectWrapper objectWrapper) throws HermesException {
        super(objectWrapper);
        TypeUtils.validateAccessible(TYPE_CENTER.getClassType(objectWrapper));
    }

    /* access modifiers changed from: protected */
    public Object invokeMethod() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void setMethod(MethodWrapper methodWrapper, ParameterWrapper[] parameterWrapperArr) {
    }
}
