package xiaofei.library.hermes.receiver;

import android.content.Context;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import xiaofei.library.hermes.Hermes;
import xiaofei.library.hermes.internal.HermesCallbackInvocationHandler;
import xiaofei.library.hermes.internal.IHermesServiceCallback;
import xiaofei.library.hermes.internal.Reply;
import xiaofei.library.hermes.util.CodeUtils;
import xiaofei.library.hermes.util.HermesCallbackGc;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.ObjectCenter;
import xiaofei.library.hermes.util.TypeCenter;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public abstract class Receiver {
    protected static final HermesCallbackGc HERMES_CALLBACK_GC = HermesCallbackGc.getInstance();
    protected static final ObjectCenter OBJECT_CENTER = ObjectCenter.getInstance();
    protected static final TypeCenter TYPE_CENTER = TypeCenter.getInstance();
    private IHermesServiceCallback mCallback;
    private long mObjectTimeStamp;
    private Object[] mParameters;

    public Receiver(ObjectWrapper objectWrapper) {
        this.mObjectTimeStamp = objectWrapper.getTimeStamp();
    }

    private Object getProxy(Class<?> cls, int i, long j) {
        return Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new HermesCallbackInvocationHandler(j, i, this.mCallback));
    }

    private static void registerCallbackReturnTypes(Class<?> cls) {
        for (Method returnType : cls.getMethods()) {
            TYPE_CENTER.register(returnType.getReturnType());
        }
    }

    private void setParameters(long j, ParameterWrapper[] parameterWrapperArr) throws HermesException {
        if (parameterWrapperArr == null) {
            this.mParameters = null;
            return;
        }
        int length = parameterWrapperArr.length;
        this.mParameters = new Object[length];
        for (int i = 0; i < length; i++) {
            ParameterWrapper parameterWrapper = parameterWrapperArr[i];
            if (parameterWrapper == null) {
                this.mParameters[i] = null;
            } else {
                Class<?> classType = TYPE_CENTER.getClassType(parameterWrapper);
                if (classType != null && classType.isInterface()) {
                    registerCallbackReturnTypes(classType);
                    this.mParameters[i] = getProxy(classType, i, j);
                    HERMES_CALLBACK_GC.register(this.mCallback, this.mParameters[i], j, i);
                } else if (classType == null || !Context.class.isAssignableFrom(classType)) {
                    String data = parameterWrapper.getData();
                    if (data == null) {
                        this.mParameters[i] = null;
                    } else {
                        this.mParameters[i] = CodeUtils.decode(data, classType);
                    }
                } else {
                    this.mParameters[i] = Hermes.getContext();
                }
            }
        }
    }

    public final Reply action(long j, MethodWrapper methodWrapper, ParameterWrapper[] parameterWrapperArr) throws HermesException {
        setMethod(methodWrapper, parameterWrapperArr);
        setParameters(j, parameterWrapperArr);
        Object invokeMethod = invokeMethod();
        if (invokeMethod == null) {
            return null;
        }
        return new Reply(new ParameterWrapper(invokeMethod));
    }

    /* access modifiers changed from: protected */
    public long getObjectTimeStamp() {
        return this.mObjectTimeStamp;
    }

    /* access modifiers changed from: protected */
    public Object[] getParameters() {
        return this.mParameters;
    }

    /* access modifiers changed from: protected */
    public abstract Object invokeMethod() throws HermesException;

    public void setHermesServiceCallback(IHermesServiceCallback iHermesServiceCallback) {
        this.mCallback = iHermesServiceCallback;
    }

    /* access modifiers changed from: protected */
    public abstract void setMethod(MethodWrapper methodWrapper, ParameterWrapper[] parameterWrapperArr) throws HermesException;
}
