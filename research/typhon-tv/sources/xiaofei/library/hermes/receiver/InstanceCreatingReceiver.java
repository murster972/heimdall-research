package xiaofei.library.hermes.receiver;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TypeUtils;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class InstanceCreatingReceiver extends Receiver {
    private Constructor<?> mConstructor;
    private Class<?> mObjectClass;

    public InstanceCreatingReceiver(ObjectWrapper objectWrapper) throws HermesException {
        super(objectWrapper);
        Class<?> classType = TYPE_CENTER.getClassType(objectWrapper);
        TypeUtils.validateAccessible(classType);
        this.mObjectClass = classType;
    }

    /* access modifiers changed from: protected */
    public Object invokeMethod() throws HermesException {
        ReflectiveOperationException reflectiveOperationException;
        try {
            Object[] parameters = getParameters();
            OBJECT_CENTER.putObject(getObjectTimeStamp(), parameters == null ? this.mConstructor.newInstance(new Object[0]) : this.mConstructor.newInstance(parameters));
            return null;
        } catch (InstantiationException e) {
            reflectiveOperationException = e;
            reflectiveOperationException.printStackTrace();
            throw new HermesException(18, "Error occurs when invoking constructor to create an instance of " + this.mObjectClass.getName(), reflectiveOperationException);
        } catch (IllegalAccessException e2) {
            reflectiveOperationException = e2;
            reflectiveOperationException.printStackTrace();
            throw new HermesException(18, "Error occurs when invoking constructor to create an instance of " + this.mObjectClass.getName(), reflectiveOperationException);
        } catch (InvocationTargetException e3) {
            reflectiveOperationException = e3;
            reflectiveOperationException.printStackTrace();
            throw new HermesException(18, "Error occurs when invoking constructor to create an instance of " + this.mObjectClass.getName(), reflectiveOperationException);
        }
    }

    public void setMethod(MethodWrapper methodWrapper, ParameterWrapper[] parameterWrapperArr) throws HermesException {
        Constructor<?> constructor = TypeUtils.getConstructor(this.mObjectClass, TYPE_CENTER.getClassTypes(parameterWrapperArr));
        TypeUtils.validateAccessible(constructor);
        this.mConstructor = constructor;
    }
}
