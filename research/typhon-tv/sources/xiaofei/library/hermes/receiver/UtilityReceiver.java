package xiaofei.library.hermes.receiver;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.TypeUtils;
import xiaofei.library.hermes.wrapper.MethodWrapper;
import xiaofei.library.hermes.wrapper.ObjectWrapper;
import xiaofei.library.hermes.wrapper.ParameterWrapper;

public class UtilityReceiver extends Receiver {
    private Class<?> mClass;
    private Method mMethod;

    public UtilityReceiver(ObjectWrapper objectWrapper) throws HermesException {
        super(objectWrapper);
        Class<?> classType = TYPE_CENTER.getClassType(objectWrapper);
        TypeUtils.validateAccessible(classType);
        this.mClass = classType;
    }

    /* access modifiers changed from: protected */
    public Object invokeMethod() throws HermesException {
        ReflectiveOperationException reflectiveOperationException;
        try {
            return this.mMethod.invoke((Object) null, getParameters());
        } catch (IllegalAccessException e) {
            reflectiveOperationException = e;
        } catch (InvocationTargetException e2) {
            reflectiveOperationException = e2;
        }
        reflectiveOperationException.printStackTrace();
        throw new HermesException(18, "Error occurs when invoking method " + this.mMethod + ".", reflectiveOperationException);
    }

    /* access modifiers changed from: protected */
    public void setMethod(MethodWrapper methodWrapper, ParameterWrapper[] parameterWrapperArr) throws HermesException {
        Method method = TYPE_CENTER.getMethod(this.mClass, methodWrapper);
        if (!Modifier.isStatic(method.getModifiers())) {
            throw new HermesException(5, "Only static methods can be invoked on the utility class " + this.mClass.getName() + ". Please modify the method: " + this.mMethod);
        }
        TypeUtils.validateAccessible(method);
        this.mMethod = method;
    }
}
