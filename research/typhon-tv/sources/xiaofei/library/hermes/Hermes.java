package xiaofei.library.hermes;

import android.content.Context;
import android.util.Log;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import xiaofei.library.hermes.HermesService;
import xiaofei.library.hermes.internal.Channel;
import xiaofei.library.hermes.internal.HermesInvocationHandler;
import xiaofei.library.hermes.internal.Reply;
import xiaofei.library.hermes.sender.Sender;
import xiaofei.library.hermes.sender.SenderDesignator;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.HermesGc;
import xiaofei.library.hermes.util.TypeCenter;
import xiaofei.library.hermes.util.TypeUtils;
import xiaofei.library.hermes.wrapper.ObjectWrapper;

public class Hermes {
    private static final Channel CHANNEL = Channel.getInstance();
    private static final HermesGc HERMES_GC = HermesGc.getInstance();
    private static final String TAG = "HERMES";
    private static final TypeCenter TYPE_CENTER = TypeCenter.getInstance();
    private static Context sContext = null;

    private static void checkBound(Class<? extends HermesService> cls) {
        if (!CHANNEL.getBound(cls)) {
            throw new IllegalStateException("Service Unavailable: You have not connected the service or the connection is not completed. You can set HermesListener to receive a callback when the connection is completed.");
        }
    }

    private static void checkInit() {
        if (sContext == null) {
            throw new IllegalStateException("Hermes has not been initialized.");
        }
    }

    public static void connect(Context context) {
        connectApp(context, (String) null, HermesService.HermesService0.class);
    }

    public static void connect(Context context, Class<? extends HermesService> cls) {
        connectApp(context, (String) null, cls);
    }

    public static void connectApp(Context context, String str) {
        connectApp(context, str, HermesService.HermesService0.class);
    }

    public static void connectApp(Context context, String str, Class<? extends HermesService> cls) {
        init(context);
        CHANNEL.bind(context.getApplicationContext(), str, cls);
    }

    public static void disconnect(Context context) {
        disconnect(context, HermesService.HermesService0.class);
    }

    public static void disconnect(Context context, Class<? extends HermesService> cls) {
        CHANNEL.unbind(context.getApplicationContext(), cls);
    }

    public static Context getContext() {
        return sContext;
    }

    public static <T> T getInstance(Class<T> cls, Object... objArr) {
        return getInstanceInService(HermesService.HermesService0.class, cls, objArr);
    }

    public static <T> T getInstanceInService(Class<? extends HermesService> cls, Class<T> cls2, Object... objArr) {
        return getInstanceWithMethodNameInService(cls, cls2, "", objArr);
    }

    public static <T> T getInstanceWithMethodName(Class<T> cls, String str, Object... objArr) {
        return getInstanceWithMethodNameInService(HermesService.HermesService0.class, cls, str, objArr);
    }

    public static <T> T getInstanceWithMethodNameInService(Class<? extends HermesService> cls, Class<T> cls2, String str, Object... objArr) {
        TypeUtils.validateServiceInterface(cls2);
        checkBound(cls);
        ObjectWrapper objectWrapper = new ObjectWrapper(cls2, 1);
        Sender postOffice = SenderDesignator.getPostOffice(cls, 1, objectWrapper);
        if (objArr == null) {
            objArr = new Object[0];
        }
        int length = objArr.length;
        Object[] objArr2 = new Object[(length + 1)];
        objArr2[0] = str;
        for (int i = 0; i < length; i++) {
            objArr2[i + 1] = objArr[i];
        }
        try {
            Reply send = postOffice.send((Method) null, objArr2);
            if (send == null || send.success()) {
                objectWrapper.setType(3);
                return getProxy(cls, objectWrapper);
            }
            Log.e(TAG, "Error occurs during getting instance. Error code: " + send.getErrorCode());
            Log.e(TAG, "Error message: " + send.getMessage());
            return null;
        } catch (HermesException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static <T> T getProxy(Class<? extends HermesService> cls, ObjectWrapper objectWrapper) {
        Class<?> objectClass = objectWrapper.getObjectClass();
        T newProxyInstance = Proxy.newProxyInstance(objectClass.getClassLoader(), new Class[]{objectClass}, new HermesInvocationHandler(cls, objectWrapper));
        HERMES_GC.register(cls, newProxyInstance, Long.valueOf(objectWrapper.getTimeStamp()));
        return newProxyInstance;
    }

    public static <T> T getUtilityClass(Class<T> cls) {
        return getUtilityClassInService(HermesService.HermesService0.class, cls);
    }

    public static <T> T getUtilityClassInService(Class<? extends HermesService> cls, Class<T> cls2) {
        TypeUtils.validateServiceInterface(cls2);
        checkBound(cls);
        ObjectWrapper objectWrapper = new ObjectWrapper(cls2, 5);
        try {
            Reply send = SenderDesignator.getPostOffice(cls, 2, objectWrapper).send((Method) null, (Object[]) null);
            if (send == null || send.success()) {
                objectWrapper.setType(4);
                return getProxy(cls, objectWrapper);
            }
            Log.e(TAG, "Error occurs during getting utility class. Error code: " + send.getErrorCode());
            Log.e(TAG, "Error message: " + send.getMessage());
            return null;
        } catch (HermesException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getVersion() {
        return "0.5.2-alpha2";
    }

    public static void init(Context context) {
        if (sContext == null) {
            sContext = context.getApplicationContext();
        }
    }

    public static boolean isConnected() {
        return isConnected(HermesService.HermesService0.class);
    }

    public static boolean isConnected(Class<? extends HermesService> cls) {
        return CHANNEL.isConnected(cls);
    }

    public static <T> T newInstance(Class<T> cls, Object... objArr) {
        return newInstanceInService(HermesService.HermesService0.class, cls, objArr);
    }

    public static <T> T newInstanceInService(Class<? extends HermesService> cls, Class<T> cls2, Object... objArr) {
        TypeUtils.validateServiceInterface(cls2);
        checkBound(cls);
        ObjectWrapper objectWrapper = new ObjectWrapper(cls2, 0);
        try {
            Reply send = SenderDesignator.getPostOffice(cls, 0, objectWrapper).send((Method) null, objArr);
            if (send == null || send.success()) {
                objectWrapper.setType(3);
                return getProxy(cls, objectWrapper);
            }
            Log.e(TAG, "Error occurs during creating instance. Error code: " + send.getErrorCode());
            Log.e(TAG, "Error message: " + send.getMessage());
            return null;
        } catch (HermesException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void register(Class<?> cls) {
        checkInit();
        TYPE_CENTER.register(cls);
    }

    public static void register(Object obj) {
        register(obj.getClass());
    }

    public static void setHermesListener(HermesListener hermesListener) {
        CHANNEL.setHermesListener(hermesListener);
    }
}
