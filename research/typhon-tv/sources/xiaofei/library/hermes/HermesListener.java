package xiaofei.library.hermes;

public abstract class HermesListener {
    public abstract void onHermesConnected(Class<? extends HermesService> cls);

    public void onHermesDisconnected(Class<? extends HermesService> cls) {
    }
}
