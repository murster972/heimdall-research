package xiaofei.library.hermes;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import xiaofei.library.hermes.internal.IHermesService;
import xiaofei.library.hermes.internal.IHermesServiceCallback;
import xiaofei.library.hermes.internal.Mail;
import xiaofei.library.hermes.internal.Reply;
import xiaofei.library.hermes.receiver.Receiver;
import xiaofei.library.hermes.receiver.ReceiverDesignator;
import xiaofei.library.hermes.util.HermesException;
import xiaofei.library.hermes.util.ObjectCenter;

public abstract class HermesService extends Service {
    /* access modifiers changed from: private */
    public static final ObjectCenter OBJECT_CENTER = ObjectCenter.getInstance();
    private final IHermesService.Stub mBinder = new IHermesService.Stub() {
        public void gc(List<Long> list) throws RemoteException {
            HermesService.OBJECT_CENTER.deleteObjects(list);
        }

        public void register(IHermesServiceCallback iHermesServiceCallback, int i) throws RemoteException {
            HermesService.this.mCallbacks.put(Integer.valueOf(i), iHermesServiceCallback);
        }

        public Reply send(Mail mail) {
            try {
                Receiver receiver = ReceiverDesignator.getReceiver(mail.getObject());
                IHermesServiceCallback iHermesServiceCallback = (IHermesServiceCallback) HermesService.this.mCallbacks.get(Integer.valueOf(mail.getPid()));
                if (iHermesServiceCallback != null) {
                    receiver.setHermesServiceCallback(iHermesServiceCallback);
                }
                return receiver.action(mail.getTimeStamp(), mail.getMethod(), mail.getParameters());
            } catch (HermesException e) {
                e.printStackTrace();
                return new Reply(e.getErrorCode(), e.getErrorMessage());
            }
        }
    };
    /* access modifiers changed from: private */
    public ConcurrentHashMap<Integer, IHermesServiceCallback> mCallbacks = new ConcurrentHashMap<>();

    public static class HermesService0 extends HermesService {
    }

    public static class HermesService1 extends HermesService {
    }

    public static class HermesService2 extends HermesService {
    }

    public static class HermesService3 extends HermesService {
    }

    public static class HermesService4 extends HermesService {
    }

    public static class HermesService5 extends HermesService {
    }

    public static class HermesService6 extends HermesService {
    }

    public static class HermesService7 extends HermesService {
    }

    public static class HermesService8 extends HermesService {
    }

    public static class HermesService9 extends HermesService {
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }
}
