package xiaofei.library.hermes;

public final class BuildConfig {
    public static final String APPLICATION_ID = "xiaofei.library.hermes";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 8;
    public static final String VERSION_NAME = "0.7.0";
}
