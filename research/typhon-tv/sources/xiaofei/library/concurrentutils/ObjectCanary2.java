package xiaofei.library.concurrentutils;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import xiaofei.library.concurrentutils.util.Action;
import xiaofei.library.concurrentutils.util.Function;

public class ObjectCanary2<T> {

    /* renamed from: 连任  reason: contains not printable characters */
    private final ExecutorService f19467 = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final AtomicInteger f19468 = new AtomicInteger(0);
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final Condition f19469 = this.f19470.newCondition();
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Lock f19470 = new ReentrantLock();
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public volatile T f19471 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public <R> R m25095(final Function<? super T, ? extends R> function) {
        if (this.f19471 != null && this.f19468.get() <= 0) {
            return function.call(this.f19471);
        }
        this.f19468.incrementAndGet();
        try {
            return this.f19467.submit(new Callable<R>() {
                public R call() throws Exception {
                    R r = null;
                    if (ObjectCanary2.this.f19471 == null) {
                        try {
                            ObjectCanary2.this.f19470.lock();
                            while (ObjectCanary2.this.f19471 == null) {
                                ObjectCanary2.this.f19469.await();
                            }
                            r = function.call(ObjectCanary2.this.f19471);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            ObjectCanary2.this.f19470.unlock();
                        }
                    } else {
                        r = function.call(ObjectCanary2.this.f19471);
                    }
                    ObjectCanary2.this.f19468.decrementAndGet();
                    return r;
                }
            }).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25096(T t) {
        if (t == null) {
            throw new IllegalArgumentException("You cannot assign null to this object.");
        }
        this.f19470.lock();
        this.f19471 = t;
        this.f19469.signalAll();
        this.f19470.unlock();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25097(final Action<? super T> action) {
        if (this.f19471 == null || this.f19468.get() > 0) {
            this.f19468.incrementAndGet();
            this.f19467.execute(new Runnable() {
                public void run() {
                    if (ObjectCanary2.this.f19471 == null) {
                        try {
                            ObjectCanary2.this.f19470.lock();
                            while (ObjectCanary2.this.f19471 == null) {
                                ObjectCanary2.this.f19469.await();
                            }
                            action.call(ObjectCanary2.this.f19471);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            ObjectCanary2.this.f19470.unlock();
                        }
                    } else {
                        action.call(ObjectCanary2.this.f19471);
                    }
                    ObjectCanary2.this.f19468.decrementAndGet();
                }
            });
            return;
        }
        action.call(this.f19471);
    }
}
