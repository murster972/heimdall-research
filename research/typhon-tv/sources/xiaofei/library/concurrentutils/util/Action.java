package xiaofei.library.concurrentutils.util;

public interface Action<T> {
    void call(T t);
}
