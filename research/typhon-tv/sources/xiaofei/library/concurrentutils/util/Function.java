package xiaofei.library.concurrentutils.util;

public interface Function<T, R> {
    R call(T t);
}
