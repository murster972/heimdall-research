package xiaofei.library.hermeseventbus;

import java.util.concurrent.ConcurrentHashMap;
import org.greenrobot.eventbus.EventBus;
import xiaofei.library.hermes.annotation.ClassId;
import xiaofei.library.hermes.annotation.GetInstance;
import xiaofei.library.hermes.annotation.MethodId;

@ClassId("MainService")
public class MainService implements IMainService {
    private static volatile MainService sInstance = null;
    private EventBus mEventBus = EventBus.m20814();
    private ConcurrentHashMap<Integer, ISubService> mSubServices = new ConcurrentHashMap<>();

    private MainService() {
    }

    @GetInstance
    public static MainService getInstance() {
        if (sInstance == null) {
            synchronized (MainService.class) {
                if (sInstance == null) {
                    sInstance = new MainService();
                }
            }
        }
        return sInstance;
    }

    @MethodId("cancelEventDelivery")
    public void cancelEventDelivery(Object obj) {
        this.mEventBus.m20824(obj);
        for (ISubService cancelEventDelivery : this.mSubServices.values()) {
            cancelEventDelivery.cancelEventDelivery(obj);
        }
    }

    @MethodId("getStickyEvent")
    public Object getStickyEvent(String str) {
        try {
            return this.mEventBus.m20832(Class.forName(str));
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    @MethodId("post")
    public void post(Object obj) {
        this.mEventBus.m20828(obj);
        for (ISubService post : this.mSubServices.values()) {
            post.post(obj);
        }
    }

    @MethodId("postSticky")
    public void postSticky(Object obj) {
        this.mEventBus.m20822(obj);
        for (ISubService post : this.mSubServices.values()) {
            post.post(obj);
        }
    }

    @MethodId("register")
    public void register(int i, ISubService iSubService) {
        this.mSubServices.put(Integer.valueOf(i), iSubService);
    }

    @MethodId("removeAllStickyEvents")
    public void removeAllStickyEvents() {
        this.mEventBus.m20826();
    }

    @MethodId("removeStickyEvent(String)")
    public Object removeStickyEvent(String str) {
        try {
            return this.mEventBus.m20825(Class.forName(str));
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    @MethodId("removeStickyEvent(Object)")
    public boolean removeStickyEvent(Object obj) {
        return this.mEventBus.m20823(obj);
    }

    @MethodId("unregister")
    public void unregister(int i) {
        this.mSubServices.remove(Integer.valueOf(i));
    }
}
