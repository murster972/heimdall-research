package xiaofei.library.hermeseventbus;

import xiaofei.library.hermes.annotation.ClassId;
import xiaofei.library.hermes.annotation.MethodId;

@ClassId("MainService")
public interface IMainService {
    @MethodId("cancelEventDelivery")
    void cancelEventDelivery(Object obj);

    @MethodId("getStickyEvent")
    Object getStickyEvent(String str);

    @MethodId("post")
    void post(Object obj);

    @MethodId("postSticky")
    void postSticky(Object obj);

    @MethodId("register")
    void register(int i, ISubService iSubService);

    @MethodId("removeAllStickyEvents")
    void removeAllStickyEvents();

    @MethodId("removeStickyEvent(String)")
    Object removeStickyEvent(String str);

    @MethodId("removeStickyEvent(Object)")
    boolean removeStickyEvent(Object obj);

    @MethodId("unregister")
    void unregister(int i);
}
