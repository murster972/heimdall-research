package xiaofei.library.hermeseventbus;

import org.greenrobot.eventbus.EventBus;
import xiaofei.library.hermes.annotation.MethodId;

public class SubService implements ISubService {
    private static volatile SubService sInstance = null;
    private EventBus mEventBus = EventBus.m20814();

    private SubService() {
    }

    public static SubService getInstance() {
        if (sInstance == null) {
            synchronized (SubService.class) {
                if (sInstance == null) {
                    sInstance = new SubService();
                }
            }
        }
        return sInstance;
    }

    @MethodId("cancelEventDelivery")
    public void cancelEventDelivery(Object obj) {
        this.mEventBus.m20824(obj);
    }

    @MethodId("post")
    public void post(Object obj) {
        this.mEventBus.m20828(obj);
    }
}
