package xiaofei.library.hermeseventbus;

import xiaofei.library.hermes.annotation.MethodId;

public interface ISubService {
    @MethodId("cancelEventDelivery")
    void cancelEventDelivery(Object obj);

    @MethodId("post")
    void post(Object obj);
}
