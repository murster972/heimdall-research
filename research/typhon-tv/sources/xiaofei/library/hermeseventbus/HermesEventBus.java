package xiaofei.library.hermeseventbus;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Process;
import android.util.Log;
import org.greenrobot.eventbus.EventBus;
import xiaofei.library.concurrentutils.ObjectCanary2;
import xiaofei.library.concurrentutils.util.Action;
import xiaofei.library.concurrentutils.util.Function;
import xiaofei.library.hermes.Hermes;
import xiaofei.library.hermes.HermesService;

public class HermesEventBus {
    private static final String HERMES_SERVICE_DISCONNECTED = "Hermes service disconnected!";
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_DISCONNECTED = 0;
    private static final String TAG = "HermesEventBus";
    private static volatile HermesEventBus sInstance = null;
    private volatile Context mContext;
    private final EventBus mEventBus = EventBus.m20814();
    private volatile MainService mMainApis;
    private volatile boolean mMainProcess;
    /* access modifiers changed from: private */
    public volatile ObjectCanary2<IMainService> mRemoteApis = new ObjectCanary2<>();
    /* access modifiers changed from: private */
    public volatile int mState = 0;

    public class HermesListener extends xiaofei.library.hermes.HermesListener {
        public HermesListener() {
        }

        public void onHermesConnected(Class<? extends HermesService> cls) {
            IMainService iMainService = (IMainService) Hermes.getInstanceInService(cls, IMainService.class, new Object[0]);
            if (iMainService != null) {
                try {
                    iMainService.register(Process.myPid(), SubService.getInstance());
                    HermesEventBus.this.mRemoteApis.m25096(iMainService);
                    int unused = HermesEventBus.this.mState = 2;
                } catch (Throwable th) {
                    Log.e(HermesEventBus.TAG, "onHermesConnected", th);
                    int unused2 = HermesEventBus.this.mState = 0;
                }
            }
        }

        public void onHermesDisconnected(Class<? extends HermesService> cls) {
            int unused = HermesEventBus.this.mState = 0;
            HermesEventBus.this.mRemoteApis.m25097(new Action<IMainService>() {
                public void call(IMainService iMainService) {
                    if (iMainService != null) {
                        try {
                            iMainService.unregister(Process.myPid());
                        } catch (Throwable th) {
                            Log.e(HermesEventBus.TAG, "onHermesDisconnected", th);
                        }
                    }
                }
            });
        }
    }

    public static class Service extends HermesService {
    }

    private HermesEventBus() {
    }

    private void actionInternal(final Action<IMainService> action) {
        if (this.mMainProcess) {
            action.call(this.mMainApis);
        } else if (this.mState == 0) {
            Log.w(TAG, HERMES_SERVICE_DISCONNECTED);
        } else {
            this.mRemoteApis.m25097(new Action<IMainService>() {
                public void call(IMainService iMainService) {
                    action.call(iMainService);
                }
            });
        }
    }

    private <T> T calculateInternal(final Function<IMainService, ? extends T> function) {
        if (this.mMainProcess) {
            return function.call(this.mMainApis);
        }
        if (this.mState != 0) {
            return this.mRemoteApis.m25095(new Function<IMainService, T>() {
                public T call(IMainService iMainService) {
                    return function.call(iMainService);
                }
            });
        }
        Log.w(TAG, HERMES_SERVICE_DISCONNECTED);
        return null;
    }

    private static String getCurrentProcessName(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo next : activityManager.getRunningAppProcesses()) {
            if (next.pid == Process.myPid()) {
                return next.processName;
            }
        }
        return null;
    }

    public static HermesEventBus getDefault() {
        if (sInstance == null) {
            synchronized (HermesEventBus.class) {
                if (sInstance == null) {
                    sInstance = new HermesEventBus();
                }
            }
        }
        return sInstance;
    }

    private static boolean isMainProcess(Context context) {
        return context.getPackageName().equals(getCurrentProcessName(context));
    }

    public void cancelEventDelivery(final Object obj) {
        actionInternal(new Action<IMainService>() {
            public void call(IMainService iMainService) {
                iMainService.cancelEventDelivery(obj);
            }
        });
    }

    public void connectApp(Context context, String str) {
        this.mContext = context.getApplicationContext();
        this.mMainProcess = false;
        this.mState = 1;
        Hermes.setHermesListener(new HermesListener());
        Hermes.connectApp(context, str);
        Hermes.register((Class<?>) SubService.class);
    }

    public void destroy() {
        if (!this.mMainProcess) {
            Hermes.disconnect(this.mContext);
        }
    }

    public <T> T getStickyEvent(final Class<T> cls) {
        return calculateInternal(new Function<IMainService, T>() {
            public T call(IMainService iMainService) {
                return cls.cast(iMainService.getStickyEvent(cls.getName()));
            }
        });
    }

    public boolean hasSubscriberForEvent(Class<?> cls) {
        return this.mEventBus.m20831(cls);
    }

    public void init(Context context) {
        this.mContext = context.getApplicationContext();
        this.mMainProcess = isMainProcess(context.getApplicationContext());
        if (this.mMainProcess) {
            Hermes.init(context);
            Hermes.register((Class<?>) MainService.class);
            this.mMainApis = MainService.getInstance();
            return;
        }
        this.mState = 1;
        Hermes.setHermesListener(new HermesListener());
        Hermes.connect(context, Service.class);
        Hermes.register((Class<?>) SubService.class);
    }

    public boolean isRegistered(Object obj) {
        return this.mEventBus.m20827(obj);
    }

    public void post(final Object obj) {
        actionInternal(new Action<IMainService>() {
            public void call(IMainService iMainService) {
                iMainService.post(obj);
            }
        });
    }

    public void postSticky(final Object obj) {
        actionInternal(new Action<IMainService>() {
            public void call(IMainService iMainService) {
                iMainService.postSticky(obj);
            }
        });
    }

    public void register(Object obj) {
        this.mEventBus.m20833(obj);
    }

    public void removeAllStickyEvents() {
        actionInternal(new Action<IMainService>() {
            public void call(IMainService iMainService) {
                iMainService.removeAllStickyEvents();
            }
        });
    }

    public Boolean removeStickyEvent(final Object obj) {
        return (Boolean) calculateInternal(new Function<IMainService, Boolean>() {
            public Boolean call(IMainService iMainService) {
                return Boolean.valueOf(iMainService.removeStickyEvent(obj));
            }
        });
    }

    public <T> T removeStickyEvent(final Class<T> cls) {
        return calculateInternal(new Function<IMainService, T>() {
            public T call(IMainService iMainService) {
                return cls.cast(iMainService.removeStickyEvent(cls.getName()));
            }
        });
    }

    public void unregister(Object obj) {
        this.mEventBus.m20830(obj);
    }
}
