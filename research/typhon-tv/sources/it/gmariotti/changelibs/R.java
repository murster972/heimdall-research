package it.gmariotti.changelibs;

public final class R {

    public static final class attr {
        public static final int changeLogFileResourceId = 2130968737;
        public static final int changeLogFileResourceUrl = 2130968738;
        public static final int layoutManager = 2130968861;
        public static final int reverseLayout = 2130969013;
        public static final int rowHeaderLayoutId = 2130969016;
        public static final int rowLayoutId = 2130969017;
        public static final int spanCount = 2130969039;
        public static final int stackFromEnd = 2130969045;
    }

    public static final class color {
        public static final int chglib_background_default_divider_color = 2131099711;
        public static final int chglib_material_background_default_divider_color = 2131099712;
        public static final int chglib_material_color_text_1 = 2131099713;
        public static final int chglib_material_color_text_2 = 2131099714;
        public static final int chglib_material_color_text_3 = 2131099715;
    }

    public static final class dimen {
        public static final int chglib_material_keyline1 = 2131165302;
        public static final int chglib_material_list_fontsize = 2131165303;
        public static final int chglib_material_list_suheader_fontsize = 2131165304;
        public static final int chglib_material_minHeight = 2131165305;
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131165390;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131165391;
        public static final int item_touch_helper_swipe_escape_velocity = 2131165392;
        public static final int listPreferredItemHeightSmall = 2131165396;
    }

    public static final class id {
        public static final int chg_headerDate = 2131296405;
        public static final int chg_headerVersion = 2131296406;
        public static final int chg_row = 2131296407;
        public static final int chg_rowheader = 2131296408;
        public static final int chg_text = 2131296409;
        public static final int chg_textbullet = 2131296410;
        public static final int item_touch_helper_previous_elevation = 2131296523;
    }

    public static final class integer {
        public static final int font_textStyle_material_item = 2131361801;
    }

    public static final class layout {
        public static final int changelogrow_layout = 2131492929;
        public static final int changelogrow_material_layout = 2131492930;
        public static final int changelogrowheader_layout = 2131492931;
        public static final int changelogrowheader_material_layout = 2131492932;
    }

    public static final class raw {
        public static final int changelog = 2131755008;
    }

    public static final class string {
        public static final int changelog_header_version = 2131820681;
        public static final int changelog_internal_error_internet_connection = 2131820682;
        public static final int changelog_internal_error_parsing = 2131820683;
        public static final int changelog_row_bulletpoint = 2131820684;
        public static final int changelog_row_prefix_bug = 2131820685;
        public static final int changelog_row_prefix_improvement = 2131820686;
        public static final int font_fontFamily_material_item = 2131820797;
    }

    public static final class styleable {
        public static final int[] ChangeLogListView = {com.typhoon.tv.R.attr.changeLogFileResourceId, com.typhoon.tv.R.attr.changeLogFileResourceUrl, com.typhoon.tv.R.attr.rowHeaderLayoutId, com.typhoon.tv.R.attr.rowLayoutId};
        public static final int ChangeLogListView_changeLogFileResourceId = 0;
        public static final int ChangeLogListView_changeLogFileResourceUrl = 1;
        public static final int ChangeLogListView_rowHeaderLayoutId = 2;
        public static final int ChangeLogListView_rowLayoutId = 3;
        public static final int[] RecyclerView = {16842948, 16842993, com.typhoon.tv.R.attr.fastScrollEnabled, com.typhoon.tv.R.attr.fastScrollHorizontalThumbDrawable, com.typhoon.tv.R.attr.fastScrollHorizontalTrackDrawable, com.typhoon.tv.R.attr.fastScrollVerticalThumbDrawable, com.typhoon.tv.R.attr.fastScrollVerticalTrackDrawable, com.typhoon.tv.R.attr.layoutManager, com.typhoon.tv.R.attr.reverseLayout, com.typhoon.tv.R.attr.spanCount, com.typhoon.tv.R.attr.stackFromEnd};
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_fastScrollEnabled = 2;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 3;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 4;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 5;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 6;
        public static final int RecyclerView_layoutManager = 7;
        public static final int RecyclerView_reverseLayout = 8;
        public static final int RecyclerView_spanCount = 9;
        public static final int RecyclerView_stackFromEnd = 10;
    }
}
