package it.gmariotti.changelibs.library;

public class Util {
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000f, code lost:
        r0 = (r1 = (android.net.ConnectivityManager) r4.getSystemService("connectivity")).getActiveNetworkInfo();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isConnected(android.content.Context r4) {
        /*
            r2 = 0
            if (r4 != 0) goto L_0x0004
        L_0x0003:
            return r2
        L_0x0004:
            java.lang.String r3 = "connectivity"
            java.lang.Object r1 = r4.getSystemService(r3)
            android.net.ConnectivityManager r1 = (android.net.ConnectivityManager) r1
            if (r1 == 0) goto L_0x0003
            android.net.NetworkInfo r0 = r1.getActiveNetworkInfo()
            if (r0 == 0) goto L_0x0003
            if (r0 == 0) goto L_0x0003
            boolean r3 = r0.isConnectedOrConnecting()
            if (r3 == 0) goto L_0x0003
            r2 = 1
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: it.gmariotti.changelibs.library.Util.isConnected(android.content.Context):boolean");
    }
}
