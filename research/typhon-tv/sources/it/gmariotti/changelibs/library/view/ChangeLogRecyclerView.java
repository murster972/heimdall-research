package it.gmariotti.changelibs.library.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;
import it.gmariotti.changelibs.R;
import it.gmariotti.changelibs.library.TyphoonApp;
import it.gmariotti.changelibs.library.Util;
import it.gmariotti.changelibs.library.internal.ChangeLog;
import it.gmariotti.changelibs.library.internal.ChangeLogRecyclerViewAdapter;
import it.gmariotti.changelibs.library.parser.XmlParser;

public class ChangeLogRecyclerView extends RecyclerView {
    protected static String TAG = "ChangeLogRecyclerView";
    protected ChangeLogRecyclerViewAdapter mAdapter;
    protected int mChangeLogFileResourceId;
    protected String mChangeLogFileResourceUrl;
    protected int mRowHeaderLayoutId;
    protected int mRowLayoutId;

    protected class ParseAsyncTask extends AsyncTask<Void, Void, ChangeLog> {
        private ChangeLogRecyclerViewAdapter mAdapter;
        private XmlParser mParse;

        public ParseAsyncTask(ChangeLogRecyclerViewAdapter changeLogRecyclerViewAdapter, XmlParser xmlParser) {
            this.mAdapter = changeLogRecyclerViewAdapter;
            this.mParse = xmlParser;
        }

        /* access modifiers changed from: protected */
        public ChangeLog doInBackground(Void... voidArr) {
            try {
                if (this.mParse != null) {
                    return this.mParse.readChangeLogFile();
                }
            } catch (Exception e) {
                Log.e(ChangeLogRecyclerView.TAG, ChangeLogRecyclerView.this.getResources().getString(R.string.changelog_internal_error_parsing), e);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(ChangeLog changeLog) {
            if (changeLog != null) {
                this.mAdapter.add(changeLog.getRows());
            }
        }
    }

    public ChangeLogRecyclerView(Context context) {
        this(context, (AttributeSet) null);
    }

    public ChangeLogRecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ChangeLogRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mRowLayoutId = TyphoonApp.mRowLayoutId;
        this.mRowHeaderLayoutId = TyphoonApp.mRowHeaderLayoutId;
        this.mChangeLogFileResourceId = TyphoonApp.mChangeLogFileResourceId;
        this.mChangeLogFileResourceUrl = null;
        init(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public void init(AttributeSet attributeSet, int i) {
        initAttrs(attributeSet, i);
        initAdapter();
        initLayoutManager();
    }

    /* access modifiers changed from: protected */
    public void initAdapter() {
        try {
            XmlParser xmlParser = this.mChangeLogFileResourceUrl != null ? new XmlParser(getContext(), this.mChangeLogFileResourceUrl) : new XmlParser(getContext(), this.mChangeLogFileResourceId);
            this.mAdapter = new ChangeLogRecyclerViewAdapter(getContext(), new ChangeLog().getRows());
            this.mAdapter.setRowLayoutId(this.mRowLayoutId);
            this.mAdapter.setRowHeaderLayoutId(this.mRowHeaderLayoutId);
            if (this.mChangeLogFileResourceUrl == null || (this.mChangeLogFileResourceUrl != null && Util.isConnected(getContext()))) {
                new ParseAsyncTask(this.mAdapter, xmlParser).execute(new Void[0]);
            } else {
                Toast.makeText(getContext(), R.string.changelog_internal_error_internet_connection, 1).show();
            }
            setAdapter(this.mAdapter);
        } catch (Exception e) {
            Log.e(TAG, getResources().getString(R.string.changelog_internal_error_parsing), e);
        }
    }

    /* access modifiers changed from: protected */
    public void initAttrs(AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.ChangeLogListView, i, i);
        try {
            this.mRowLayoutId = obtainStyledAttributes.getResourceId(R.styleable.ChangeLogListView_rowLayoutId, this.mRowLayoutId);
            this.mRowHeaderLayoutId = obtainStyledAttributes.getResourceId(R.styleable.ChangeLogListView_rowHeaderLayoutId, this.mRowHeaderLayoutId);
            this.mChangeLogFileResourceId = obtainStyledAttributes.getResourceId(R.styleable.ChangeLogListView_changeLogFileResourceId, this.mChangeLogFileResourceId);
            this.mChangeLogFileResourceUrl = obtainStyledAttributes.getString(R.styleable.ChangeLogListView_changeLogFileResourceUrl);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void initLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(1);
        setLayoutManager(linearLayoutManager);
    }
}
