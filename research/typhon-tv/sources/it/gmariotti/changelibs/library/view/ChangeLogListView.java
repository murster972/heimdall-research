package it.gmariotti.changelibs.library.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import it.gmariotti.changelibs.R;
import it.gmariotti.changelibs.library.TyphoonApp;
import it.gmariotti.changelibs.library.Util;
import it.gmariotti.changelibs.library.internal.ChangeLog;
import it.gmariotti.changelibs.library.internal.ChangeLogAdapter;
import it.gmariotti.changelibs.library.internal.ChangeLogRow;
import it.gmariotti.changelibs.library.parser.XmlParser;
import java.util.Iterator;

public class ChangeLogListView extends ListView implements AdapterView.OnItemClickListener {
    protected static String TAG = "ChangeLogListView";
    protected ChangeLogAdapter mAdapter;
    protected int mChangeLogFileResourceId;
    protected String mChangeLogFileResourceUrl;
    protected int mRowHeaderLayoutId;
    protected int mRowLayoutId;

    protected class ParseAsyncTask extends AsyncTask<Void, Void, ChangeLog> {
        private ChangeLogAdapter mAdapter;
        private XmlParser mParse;

        public ParseAsyncTask(ChangeLogAdapter changeLogAdapter, XmlParser xmlParser) {
            this.mAdapter = changeLogAdapter;
            this.mParse = xmlParser;
        }

        /* access modifiers changed from: protected */
        public ChangeLog doInBackground(Void... voidArr) {
            try {
                if (this.mParse != null) {
                    return this.mParse.readChangeLogFile();
                }
            } catch (Exception e) {
                Log.e(ChangeLogListView.TAG, ChangeLogListView.this.getResources().getString(R.string.changelog_internal_error_parsing), e);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(ChangeLog changeLog) {
            if (changeLog != null) {
                if (Build.VERSION.SDK_INT >= 11) {
                    this.mAdapter.addAll(changeLog.getRows());
                } else if (changeLog.getRows() != null) {
                    Iterator it2 = changeLog.getRows().iterator();
                    while (it2.hasNext()) {
                        this.mAdapter.add((ChangeLogRow) it2.next());
                    }
                }
                this.mAdapter.notifyDataSetChanged();
            }
        }
    }

    public ChangeLogListView(Context context) {
        this(context, (AttributeSet) null);
    }

    public ChangeLogListView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ChangeLogListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mRowLayoutId = TyphoonApp.mRowLayoutId;
        this.mRowHeaderLayoutId = TyphoonApp.mRowHeaderLayoutId;
        this.mChangeLogFileResourceId = TyphoonApp.mChangeLogFileResourceId;
        this.mChangeLogFileResourceUrl = null;
        if (Build.VERSION.SDK_INT >= 21) {
            setNestedScrollingEnabled(true);
        }
        init(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void init(AttributeSet attributeSet, int i) {
        initAttrs(attributeSet, i);
        initAdapter();
        setDividerHeight(0);
    }

    /* access modifiers changed from: protected */
    public void initAdapter() {
        try {
            XmlParser xmlParser = this.mChangeLogFileResourceUrl != null ? new XmlParser(getContext(), this.mChangeLogFileResourceUrl) : new XmlParser(getContext(), this.mChangeLogFileResourceId);
            this.mAdapter = new ChangeLogAdapter(getContext(), new ChangeLog().getRows());
            this.mAdapter.setmRowLayoutId(this.mRowLayoutId);
            this.mAdapter.setmRowHeaderLayoutId(this.mRowHeaderLayoutId);
            if (this.mChangeLogFileResourceUrl == null || (this.mChangeLogFileResourceUrl != null && Util.isConnected(getContext()))) {
                new ParseAsyncTask(this.mAdapter, xmlParser).execute(new Void[0]);
            } else {
                Toast.makeText(getContext(), R.string.changelog_internal_error_internet_connection, 1).show();
            }
            setAdapter(this.mAdapter);
        } catch (Exception e) {
            Log.e(TAG, getResources().getString(R.string.changelog_internal_error_parsing), e);
        }
    }

    /* access modifiers changed from: protected */
    public void initAttrs(AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.ChangeLogListView, i, i);
        try {
            this.mRowLayoutId = obtainStyledAttributes.getResourceId(R.styleable.ChangeLogListView_rowLayoutId, this.mRowLayoutId);
            this.mRowHeaderLayoutId = obtainStyledAttributes.getResourceId(R.styleable.ChangeLogListView_rowHeaderLayoutId, this.mRowHeaderLayoutId);
            this.mChangeLogFileResourceId = obtainStyledAttributes.getResourceId(R.styleable.ChangeLogListView_changeLogFileResourceId, this.mChangeLogFileResourceId);
            this.mChangeLogFileResourceUrl = obtainStyledAttributes.getString(R.styleable.ChangeLogListView_changeLogFileResourceUrl);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
    }

    public void setAdapter(ChangeLogAdapter changeLogAdapter) {
        super.setAdapter(changeLogAdapter);
    }
}
