package it.gmariotti.changelibs.library.parser;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import it.gmariotti.changelibs.library.TyphoonApp;
import it.gmariotti.changelibs.library.Util;
import it.gmariotti.changelibs.library.internal.ChangeLog;
import it.gmariotti.changelibs.library.internal.ChangeLogAdapter;
import it.gmariotti.changelibs.library.internal.ChangeLogException;
import it.gmariotti.changelibs.library.internal.ChangeLogRow;
import it.gmariotti.changelibs.library.internal.ChangeLogRowHeader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class XmlParser extends BaseParser {
    private static final String ATTRIBUTE_BULLETEDLIST = "bulletedList";
    private static final String ATTRIBUTE_CHANGEDATE = "changeDate";
    private static final String ATTRIBUTE_CHANGETEXTTITLE = "changeTextTitle";
    private static final String ATTRIBUTE_VERSIONCODE = "versionCode";
    private static final String ATTRIBUTE_VERSIONNAME = "versionName";
    private static String TAG = "XmlParser";
    private static final String TAG_CHANGELOG = "changelog";
    private static final String TAG_CHANGELOGBUG = "changelogbug";
    private static final String TAG_CHANGELOGIMPROVEMENT = "changelogimprovement";
    private static final String TAG_CHANGELOGTEXT = "changelogtext";
    private static final String TAG_CHANGELOGVERSION = "changelogversion";
    private static List<String> mChangeLogTags = new ArrayList<String>() {
        {
            add(XmlParser.TAG_CHANGELOGBUG);
            add(XmlParser.TAG_CHANGELOGIMPROVEMENT);
            add(XmlParser.TAG_CHANGELOGTEXT);
        }
    };
    protected ChangeLogAdapter mChangeLogAdapter;
    private int mChangeLogFileResourceId = TyphoonApp.mChangeLogFileResourceId;
    private String mChangeLogFileResourceUrl = null;

    public XmlParser(Context context) {
        super(context);
    }

    public XmlParser(Context context, int i) {
        super(context);
        this.mChangeLogFileResourceId = i;
    }

    public XmlParser(Context context, String str) {
        super(context);
        this.mChangeLogFileResourceUrl = str;
    }

    private void readChangeLogRowNode(XmlPullParser xmlPullParser, ChangeLog changeLog, String str, int i) throws Exception {
        int i2 = 1;
        if (xmlPullParser != null) {
            String name = xmlPullParser.getName();
            ChangeLogRow changeLogRow = new ChangeLogRow();
            changeLogRow.setVersionName(str);
            changeLogRow.setVersionCode(i);
            String attributeValue = xmlPullParser.getAttributeValue((String) null, ATTRIBUTE_CHANGETEXTTITLE);
            if (attributeValue != null) {
                changeLogRow.setChangeTextTitle(attributeValue);
            }
            String attributeValue2 = xmlPullParser.getAttributeValue((String) null, ATTRIBUTE_BULLETEDLIST);
            if (attributeValue2 == null) {
                changeLogRow.setBulletedList(this.bulletedList);
            } else if (attributeValue2.equals("true")) {
                changeLogRow.setBulletedList(true);
            } else {
                changeLogRow.setBulletedList(false);
            }
            if (xmlPullParser.next() == 4) {
                String text = xmlPullParser.getText();
                if (text == null) {
                    throw new ChangeLogException("ChangeLogText required in changeLogText node");
                }
                changeLogRow.parseChangeText(text);
                if (!name.equalsIgnoreCase(TAG_CHANGELOGBUG)) {
                    i2 = name.equalsIgnoreCase(TAG_CHANGELOGIMPROVEMENT) ? 2 : 0;
                }
                changeLogRow.setType(i2);
                xmlPullParser.nextTag();
            }
            changeLog.addRow(changeLogRow);
        }
    }

    public ChangeLog readChangeLogFile() throws Exception {
        InputStream inputStream = null;
        try {
            if (this.mChangeLogFileResourceUrl == null) {
                inputStream = this.mContext.getResources().openRawResource(this.mChangeLogFileResourceId);
            } else if (Util.isConnected(this.mContext)) {
                inputStream = new URL(this.mChangeLogFileResourceUrl).openStream();
            }
            if (inputStream != null) {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
                newPullParser.setInput(inputStream, (String) null);
                newPullParser.nextTag();
                ChangeLog changeLog = new ChangeLog();
                try {
                    readChangeLogNode(newPullParser, changeLog);
                    inputStream.close();
                    return changeLog;
                } catch (XmlPullParserException e) {
                    e = e;
                    ChangeLog changeLog2 = changeLog;
                    Log.d(TAG, "XmlPullParseException while parsing changelog file", e);
                    throw e;
                } catch (IOException e2) {
                    e = e2;
                    ChangeLog changeLog3 = changeLog;
                    Log.d(TAG, "Error i/o with changelog.xml", e);
                    throw e;
                }
            } else {
                Log.d(TAG, "Changelog.xml not found");
                throw new ChangeLogException("Changelog.xml not found");
            }
        } catch (XmlPullParserException e3) {
            e = e3;
            Log.d(TAG, "XmlPullParseException while parsing changelog file", e);
            throw e;
        } catch (IOException e4) {
            e = e4;
            Log.d(TAG, "Error i/o with changelog.xml", e);
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    public void readChangeLogNode(XmlPullParser xmlPullParser, ChangeLog changeLog) throws Exception {
        if (xmlPullParser != null && changeLog != null) {
            xmlPullParser.require(2, (String) null, TAG_CHANGELOG);
            String attributeValue = xmlPullParser.getAttributeValue((String) null, ATTRIBUTE_BULLETEDLIST);
            if (attributeValue == null || attributeValue.equals("true")) {
                changeLog.setBulletedList(true);
                this.bulletedList = true;
            } else {
                changeLog.setBulletedList(false);
                this.bulletedList = false;
            }
            while (xmlPullParser.next() != 3) {
                if (xmlPullParser.getEventType() == 2 && xmlPullParser.getName().equals(TAG_CHANGELOGVERSION)) {
                    readChangeLogVersionNode(xmlPullParser, changeLog);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void readChangeLogVersionNode(XmlPullParser xmlPullParser, ChangeLog changeLog) throws Exception {
        if (xmlPullParser != null) {
            xmlPullParser.require(2, (String) null, TAG_CHANGELOGVERSION);
            String attributeValue = xmlPullParser.getAttributeValue((String) null, ATTRIBUTE_VERSIONNAME);
            String attributeValue2 = xmlPullParser.getAttributeValue((String) null, ATTRIBUTE_VERSIONCODE);
            int i = 0;
            if (attributeValue2 != null) {
                try {
                    i = Integer.parseInt(attributeValue2);
                } catch (NumberFormatException e) {
                    Log.w(TAG, "Error while parsing versionCode.It must be a numeric value. Check you file.");
                }
            }
            String attributeValue3 = xmlPullParser.getAttributeValue((String) null, ATTRIBUTE_CHANGEDATE);
            if (attributeValue == null) {
                throw new ChangeLogException("VersionName required in changeLogVersion node");
            }
            ChangeLogRowHeader changeLogRowHeader = new ChangeLogRowHeader();
            changeLogRowHeader.setVersionName(attributeValue);
            changeLogRowHeader.setChangeDate(attributeValue3);
            changeLog.addRow(changeLogRowHeader);
            while (xmlPullParser.next() != 3) {
                if (xmlPullParser.getEventType() == 2) {
                    if (mChangeLogTags.contains(xmlPullParser.getName())) {
                        readChangeLogRowNode(xmlPullParser, changeLog, attributeValue, i);
                    }
                }
            }
        }
    }

    public void setChangeLogAdapter(ChangeLogAdapter changeLogAdapter) {
        this.mChangeLogAdapter = changeLogAdapter;
    }
}
