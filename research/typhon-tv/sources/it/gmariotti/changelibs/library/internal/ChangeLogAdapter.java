package it.gmariotti.changelibs.library.internal;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import it.gmariotti.changelibs.library.TyphoonApp;
import java.util.List;

public class ChangeLogAdapter extends ArrayAdapter<ChangeLogRow> {
    protected static final int TYPE_HEADER = 1;
    protected static final int TYPE_ROW = 0;
    private final Context mContext;
    private int mRowHeaderLayoutId = TyphoonApp.mRowHeaderLayoutId;
    private int mRowLayoutId = TyphoonApp.mRowLayoutId;
    private int mStringVersionHeader = TyphoonApp.mStringVersionHeader;

    static class ViewHolderHeader {
        TextView date;
        TextView version;

        public ViewHolderHeader(TextView textView, TextView textView2) {
            this.version = textView;
            this.date = textView2;
        }
    }

    static class ViewHolderRow {
        TextView bulletText;
        TextView text;

        public ViewHolderRow(TextView textView, TextView textView2) {
            this.text = textView;
            this.bulletText = textView2;
        }
    }

    public ChangeLogAdapter(Context context, List<ChangeLogRow> list) {
        super(context, 0, list);
        this.mContext = context;
    }

    public int getItemViewType(int i) {
        return ((ChangeLogRow) getItem(i)).isHeader() ? 1 : 0;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v5, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v5, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r18, android.view.View r19, android.view.ViewGroup r20) {
        /*
            r17 = this;
            java.lang.Object r2 = r17.getItem(r18)
            it.gmariotti.changelibs.library.internal.ChangeLogRow r2 = (it.gmariotti.changelibs.library.internal.ChangeLogRow) r2
            r11 = r19
            int r14 = r17.getItemViewType(r18)
            r0 = r17
            android.content.Context r15 = r0.mContext
            java.lang.String r16 = "layout_inflater"
            java.lang.Object r4 = r15.getSystemService(r16)
            android.view.LayoutInflater r4 = (android.view.LayoutInflater) r4
            switch(r14) {
                case 0: goto L_0x00ac;
                case 1: goto L_0x001d;
                default: goto L_0x001c;
            }
        L_0x001c:
            return r11
        L_0x001d:
            r13 = 0
            if (r11 == 0) goto L_0x002b
            java.lang.Object r5 = r11.getTag()
            boolean r15 = r5 instanceof it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderHeader
            if (r15 == 0) goto L_0x0099
            r13 = r5
            it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader r13 = (it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderHeader) r13
        L_0x002b:
            if (r11 == 0) goto L_0x002f
            if (r13 != 0) goto L_0x0052
        L_0x002f:
            r0 = r17
            int r3 = r0.mRowHeaderLayoutId
            r15 = 0
            r0 = r20
            android.view.View r11 = r4.inflate(r3, r0, r15)
            int r15 = it.gmariotti.changelibs.R.id.chg_headerVersion
            android.view.View r8 = r11.findViewById(r15)
            android.widget.TextView r8 = (android.widget.TextView) r8
            int r15 = it.gmariotti.changelibs.R.id.chg_headerDate
            android.view.View r7 = r11.findViewById(r15)
            android.widget.TextView r7 = (android.widget.TextView) r7
            it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader r13 = new it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader
            r13.<init>(r8, r7)
            r11.setTag(r13)
        L_0x0052:
            if (r2 == 0) goto L_0x001c
            if (r13 == 0) goto L_0x001c
            android.widget.TextView r15 = r13.version
            if (r15 == 0) goto L_0x0080
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            android.content.Context r15 = r17.getContext()
            r0 = r17
            int r0 = r0.mStringVersionHeader
            r16 = r0
            java.lang.String r10 = r15.getString(r16)
            if (r10 == 0) goto L_0x0072
            r6.append(r10)
        L_0x0072:
            java.lang.String r15 = r2.versionName
            r6.append(r15)
            android.widget.TextView r15 = r13.version
            java.lang.String r16 = r6.toString()
            r15.setText(r16)
        L_0x0080:
            android.widget.TextView r15 = r13.date
            if (r15 == 0) goto L_0x001c
            java.lang.String r15 = r2.changeDate
            if (r15 == 0) goto L_0x009b
            android.widget.TextView r15 = r13.date
            java.lang.String r0 = r2.changeDate
            r16 = r0
            r15.setText(r16)
            android.widget.TextView r15 = r13.date
            r16 = 0
            r15.setVisibility(r16)
            goto L_0x001c
        L_0x0099:
            r13 = 0
            goto L_0x002b
        L_0x009b:
            android.widget.TextView r15 = r13.date
            java.lang.String r16 = ""
            r15.setText(r16)
            android.widget.TextView r15 = r13.date
            r16 = 8
            r15.setVisibility(r16)
            goto L_0x001c
        L_0x00ac:
            r12 = 0
            if (r11 == 0) goto L_0x00ba
            java.lang.Object r5 = r11.getTag()
            boolean r15 = r5 instanceof it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderRow
            if (r15 == 0) goto L_0x011a
            r12 = r5
            it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow r12 = (it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderRow) r12
        L_0x00ba:
            if (r11 == 0) goto L_0x00be
            if (r12 != 0) goto L_0x00e1
        L_0x00be:
            r0 = r17
            int r3 = r0.mRowLayoutId
            r15 = 0
            r0 = r20
            android.view.View r11 = r4.inflate(r3, r0, r15)
            int r15 = it.gmariotti.changelibs.R.id.chg_text
            android.view.View r9 = r11.findViewById(r15)
            android.widget.TextView r9 = (android.widget.TextView) r9
            int r15 = it.gmariotti.changelibs.R.id.chg_textbullet
            android.view.View r1 = r11.findViewById(r15)
            android.widget.TextView r1 = (android.widget.TextView) r1
            it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow r12 = new it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow
            r12.<init>(r9, r1)
            r11.setTag(r12)
        L_0x00e1:
            if (r2 == 0) goto L_0x001c
            if (r12 == 0) goto L_0x001c
            android.widget.TextView r15 = r12.text
            if (r15 == 0) goto L_0x0107
            android.widget.TextView r15 = r12.text
            r0 = r17
            android.content.Context r0 = r0.mContext
            r16 = r0
            r0 = r16
            java.lang.String r16 = r2.getChangeText(r0)
            android.text.Spanned r16 = android.text.Html.fromHtml(r16)
            r15.setText(r16)
            android.widget.TextView r15 = r12.text
            android.text.method.MovementMethod r16 = android.text.method.LinkMovementMethod.getInstance()
            r15.setMovementMethod(r16)
        L_0x0107:
            android.widget.TextView r15 = r12.bulletText
            if (r15 == 0) goto L_0x001c
            boolean r15 = r2.isBulletedList()
            if (r15 == 0) goto L_0x011c
            android.widget.TextView r15 = r12.bulletText
            r16 = 0
            r15.setVisibility(r16)
            goto L_0x001c
        L_0x011a:
            r12 = 0
            goto L_0x00ba
        L_0x011c:
            android.widget.TextView r15 = r12.bulletText
            r16 = 8
            r15.setVisibility(r16)
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: it.gmariotti.changelibs.library.internal.ChangeLogAdapter.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getmRowHeaderLayoutId() {
        return this.mRowHeaderLayoutId;
    }

    public int getmRowLayoutId() {
        return this.mRowLayoutId;
    }

    public int getmStringVersionHeader() {
        return this.mStringVersionHeader;
    }

    public boolean isEnabled(int i) {
        return false;
    }

    public void setmRowHeaderLayoutId(int i) {
        this.mRowHeaderLayoutId = i;
    }

    public void setmRowLayoutId(int i) {
        this.mRowLayoutId = i;
    }

    public void setmStringVersionHeader(int i) {
        this.mStringVersionHeader = i;
    }
}
