package it.gmariotti.changelibs.library.internal;

import java.util.Iterator;
import java.util.LinkedList;
import org.apache.commons.lang3.StringUtils;

public class ChangeLog {
    private boolean bulletedList;
    private LinkedList<ChangeLogRow> rows = new LinkedList<>();

    public void addRow(ChangeLogRow changeLogRow) {
        if (changeLogRow != null) {
            if (this.rows == null) {
                this.rows = new LinkedList<>();
            }
            this.rows.add(changeLogRow);
        }
    }

    public void clearAllRows() {
        this.rows = new LinkedList<>();
    }

    public LinkedList<ChangeLogRow> getRows() {
        return this.rows;
    }

    public boolean isBulletedList() {
        return this.bulletedList;
    }

    public void setBulletedList(boolean z) {
        this.bulletedList = z;
    }

    public void setRows(LinkedList<ChangeLogRow> linkedList) {
        this.rows = linkedList;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("bulletedList=" + this.bulletedList);
        sb.append(StringUtils.LF);
        if (this.rows != null) {
            Iterator it2 = this.rows.iterator();
            while (it2.hasNext()) {
                sb.append("row=[");
                sb.append(((ChangeLogRow) it2.next()).toString());
                sb.append("]\n");
            }
        } else {
            sb.append("rows:none");
        }
        return sb.toString();
    }
}
