package it.gmariotti.changelibs.library.internal;

import android.content.Context;
import it.gmariotti.changelibs.R;
import org.apache.commons.lang3.StringUtils;

public class ChangeLogRow {
    public static final int BUGFIX = 1;
    public static final int DEFAULT = 0;
    public static final int IMPROVEMENT = 2;
    private boolean bulletedList;
    protected String changeDate;
    private String changeText;
    private String changeTextTitle;
    protected boolean header;
    private int type;
    protected int versionCode;
    protected String versionName;

    public String getChangeDate() {
        return this.changeDate;
    }

    public String getChangeText() {
        return this.changeText;
    }

    public String getChangeText(Context context) {
        if (context == null) {
            return getChangeText();
        }
        String str = "";
        switch (this.type) {
            case 1:
                str = context.getResources().getString(R.string.changelog_row_prefix_bug).replaceAll("\\[", "<").replaceAll("\\]", ">");
                break;
            case 2:
                str = context.getResources().getString(R.string.changelog_row_prefix_improvement).replaceAll("\\[", "<").replaceAll("\\]", ">");
                break;
        }
        return str + StringUtils.SPACE + this.changeText;
    }

    public String getChangeTextTitle() {
        return this.changeTextTitle;
    }

    public int getVersionCode() {
        return this.versionCode;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public boolean isBulletedList() {
        return this.bulletedList;
    }

    public boolean isHeader() {
        return this.header;
    }

    public void parseChangeText(String str) {
        if (str != null) {
            str = str.replaceAll("\\[", "<").replaceAll("\\]", ">");
        }
        setChangeText(str);
    }

    public void setBulletedList(boolean z) {
        this.bulletedList = z;
    }

    public void setChangeDate(String str) {
        this.changeDate = str;
    }

    public void setChangeText(String str) {
        this.changeText = str;
    }

    public void setChangeTextTitle(String str) {
        this.changeTextTitle = str;
    }

    public void setHeader(boolean z) {
        this.header = z;
    }

    public void setType(int i) {
        this.type = i;
    }

    public void setVersionCode(int i) {
        this.versionCode = i;
    }

    public void setVersionName(String str) {
        this.versionName = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("header=" + this.header);
        sb.append(",");
        sb.append("versionName=" + this.versionName);
        sb.append(",");
        sb.append("versionCode=" + this.versionCode);
        sb.append(",");
        sb.append("bulletedList=" + this.bulletedList);
        sb.append(",");
        sb.append("changeText=" + this.changeText);
        return sb.toString();
    }
}
