package okhttp3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import okhttp3.internal.Util;
import okhttp3.internal.http.HttpDate;
import okhttp3.internal.publicsuffix.PublicSuffixDatabase;

public final class Cookie {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Pattern f6256 = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Pattern f6257 = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Pattern f6258 = Pattern.compile("(\\d{1,2})[^\\d]*");

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f6259 = Pattern.compile("(\\d{2,4})[^\\d]*");

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f6260;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final long f6261;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String f6262;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final boolean f6263;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final boolean f6264;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final String f6265;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean f6266;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final boolean f6267;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f6268;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f16020;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f16021;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f16022;

        /* renamed from: ˑ  reason: contains not printable characters */
        boolean f16023;

        /* renamed from: 连任  reason: contains not printable characters */
        String f16024 = InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;

        /* renamed from: 靐  reason: contains not printable characters */
        String f16025;

        /* renamed from: 麤  reason: contains not printable characters */
        String f16026;

        /* renamed from: 齉  reason: contains not printable characters */
        long f16027 = 253402300799999L;

        /* renamed from: 龘  reason: contains not printable characters */
        String f16028;

        /* renamed from: 龘  reason: contains not printable characters */
        private Builder m19899(String str, boolean z) {
            if (str == null) {
                throw new NullPointerException("domain == null");
            }
            String r0 = Util.m7115(str);
            if (r0 == null) {
                throw new IllegalArgumentException("unexpected domain: " + str);
            }
            this.f16026 = r0;
            this.f16023 = z;
            return this;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public Builder m19900(String str) {
            if (!str.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                throw new IllegalArgumentException("path must start with '/'");
            }
            this.f16024 = str;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19901() {
            this.f16021 = true;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19902(String str) {
            if (str == null) {
                throw new NullPointerException("value == null");
            } else if (!str.trim().equals(str)) {
                throw new IllegalArgumentException("value is not trimmed");
            } else {
                this.f16025 = str;
                return this;
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Builder m19903(String str) {
            return m19899(str, true);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m19904(String str) {
            return m19899(str, false);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Cookie m19905() {
            return new Cookie(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19906() {
            this.f16020 = true;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19907(long j) {
            if (j <= 0) {
                j = Long.MIN_VALUE;
            }
            if (j > 253402300799999L) {
                j = 253402300799999L;
            }
            this.f16027 = j;
            this.f16022 = true;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19908(String str) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (!str.trim().equals(str)) {
                throw new IllegalArgumentException("name is not trimmed");
            } else {
                this.f16028 = str;
                return this;
            }
        }
    }

    private Cookie(String str, String str2, long j, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4) {
        this.f6268 = str;
        this.f6260 = str2;
        this.f6261 = j;
        this.f6262 = str3;
        this.f6265 = str4;
        this.f6266 = z;
        this.f6267 = z2;
        this.f6263 = z3;
        this.f6264 = z4;
    }

    Cookie(Builder builder) {
        if (builder.f16028 == null) {
            throw new NullPointerException("builder.name == null");
        } else if (builder.f16025 == null) {
            throw new NullPointerException("builder.value == null");
        } else if (builder.f16026 == null) {
            throw new NullPointerException("builder.domain == null");
        } else {
            this.f6268 = builder.f16028;
            this.f6260 = builder.f16025;
            this.f6261 = builder.f16027;
            this.f6262 = builder.f16026;
            this.f6265 = builder.f16024;
            this.f6266 = builder.f16020;
            this.f6267 = builder.f16021;
            this.f6264 = builder.f16022;
            this.f6263 = builder.f16023;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m6896(String str) {
        if (str.endsWith(".")) {
            throw new IllegalArgumentException();
        }
        if (str.startsWith(".")) {
            str = str.substring(1);
        }
        String r0 = Util.m7115(str);
        if (r0 != null) {
            return r0;
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m6897(HttpUrl httpUrl, String str) {
        String r0 = httpUrl.m6953();
        if (r0.equals(str)) {
            return true;
        }
        return r0.startsWith(str) && (str.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) || r0.charAt(str.length()) == '/');
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m6898(String str, int i, int i2, boolean z) {
        for (int i3 = i; i3 < i2; i3++) {
            char charAt = str.charAt(i3);
            if (((charAt < ' ' && charAt != 9) || charAt >= 127 || (charAt >= '0' && charAt <= '9') || ((charAt >= 'a' && charAt <= 'z') || ((charAt >= 'A' && charAt <= 'Z') || charAt == ':'))) == (!z)) {
                return i3;
            }
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m6899(String str) {
        long j = Long.MIN_VALUE;
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong <= 0) {
                return Long.MIN_VALUE;
            }
            return parseLong;
        } catch (NumberFormatException e) {
            if (str.matches("-?\\d+")) {
                if (!str.startsWith("-")) {
                    j = Long.MAX_VALUE;
                }
                return j;
            }
            throw e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m6900(String str, int i, int i2) {
        int r13 = m6898(str, i, i2, false);
        int i3 = -1;
        int i4 = -1;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        int i8 = -1;
        Matcher matcher = f6257.matcher(str);
        while (r13 < i2) {
            int r2 = m6898(str, r13 + 1, i2, true);
            matcher.region(r13, r2);
            if (i3 == -1 && matcher.usePattern(f6257).matches()) {
                i3 = Integer.parseInt(matcher.group(1));
                i4 = Integer.parseInt(matcher.group(2));
                i5 = Integer.parseInt(matcher.group(3));
            } else if (i6 == -1 && matcher.usePattern(f6258).matches()) {
                i6 = Integer.parseInt(matcher.group(1));
            } else if (i7 == -1 && matcher.usePattern(f6256).matches()) {
                i7 = f6256.pattern().indexOf(matcher.group(1).toLowerCase(Locale.US)) / 4;
            } else if (i8 == -1 && matcher.usePattern(f6259).matches()) {
                i8 = Integer.parseInt(matcher.group(1));
            }
            r13 = m6898(str, r2 + 1, i2, false);
        }
        if (i8 >= 70 && i8 <= 99) {
            i8 += 1900;
        }
        if (i8 >= 0 && i8 <= 69) {
            i8 += 2000;
        }
        if (i8 < 1601) {
            throw new IllegalArgumentException();
        } else if (i7 == -1) {
            throw new IllegalArgumentException();
        } else if (i6 < 1 || i6 > 31) {
            throw new IllegalArgumentException();
        } else if (i3 < 0 || i3 > 23) {
            throw new IllegalArgumentException();
        } else if (i4 < 0 || i4 > 59) {
            throw new IllegalArgumentException();
        } else if (i5 < 0 || i5 > 59) {
            throw new IllegalArgumentException();
        } else {
            GregorianCalendar gregorianCalendar = new GregorianCalendar(Util.f6403);
            gregorianCalendar.setLenient(false);
            gregorianCalendar.set(1, i8);
            gregorianCalendar.set(2, i7 - 1);
            gregorianCalendar.set(5, i6);
            gregorianCalendar.set(11, i3);
            gregorianCalendar.set(12, i4);
            gregorianCalendar.set(13, i5);
            gregorianCalendar.set(14, 0);
            return gregorianCalendar.getTimeInMillis();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<Cookie> m6901(HttpUrl httpUrl, Headers headers) {
        List<String> r1 = headers.m6931("Set-Cookie");
        ArrayList arrayList = null;
        int size = r1.size();
        for (int i = 0; i < size; i++) {
            Cookie r0 = m6903(httpUrl, r1.get(i));
            if (r0 != null) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(r0);
            }
        }
        return arrayList != null ? Collections.unmodifiableList(arrayList) : Collections.emptyList();
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    static Cookie m6902(long j, HttpUrl httpUrl, String str) {
        int length = str.length();
        int r19 = Util.m7110(str, 0, length, ';');
        int r27 = Util.m7110(str, 0, r19, '=');
        if (r27 == r19) {
            return null;
        }
        String r6 = Util.m7106(str, 0, r27);
        if (r6.isEmpty() || Util.m7100(r6) != -1) {
            return null;
        }
        String r7 = Util.m7106(str, r27 + 1, r19);
        if (Util.m7100(r7) != -1) {
            return null;
        }
        long j2 = 253402300799999L;
        long j3 = -1;
        String str2 = null;
        String str3 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = true;
        boolean z4 = false;
        int i = r19 + 1;
        while (i < length) {
            int r17 = Util.m7110(str, i, length, ';');
            int r4 = Util.m7110(str, i, r17, '=');
            String r16 = Util.m7106(str, i, r4);
            String r18 = r4 < r17 ? Util.m7106(str, r4 + 1, r17) : "";
            if (r16.equalsIgnoreCase("expires")) {
                try {
                    j2 = m6900(r18, 0, r18.length());
                    z4 = true;
                } catch (IllegalArgumentException e) {
                }
            } else if (r16.equalsIgnoreCase("max-age")) {
                try {
                    j3 = m6899(r18);
                    z4 = true;
                } catch (NumberFormatException e2) {
                }
            } else if (r16.equalsIgnoreCase("domain")) {
                try {
                    str2 = m6896(r18);
                    z3 = false;
                } catch (IllegalArgumentException e3) {
                }
            } else if (r16.equalsIgnoreCase("path")) {
                str3 = r18;
            } else if (r16.equalsIgnoreCase("secure")) {
                z = true;
            } else if (r16.equalsIgnoreCase("httponly")) {
                z2 = true;
            }
            i = r17 + 1;
        }
        if (j3 == Long.MIN_VALUE) {
            j2 = Long.MIN_VALUE;
        } else if (j3 != -1) {
            j2 = j + (j3 <= 9223372036854775L ? j3 * 1000 : Long.MAX_VALUE);
            if (j2 < j || j2 > 253402300799999L) {
                j2 = 253402300799999L;
            }
        }
        String r29 = httpUrl.m6951();
        if (str2 == null) {
            str2 = r29;
        } else if (!m6904(r29, str2)) {
            return null;
        }
        if (r29.length() != str2.length() && PublicSuffixDatabase.m20412().m20414(str2) == null) {
            return null;
        }
        if (str3 == null || !str3.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
            String r24 = httpUrl.m6953();
            int lastIndexOf = r24.lastIndexOf(47);
            str3 = lastIndexOf != 0 ? r24.substring(0, lastIndexOf) : InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
        }
        return new Cookie(r6, r7, j2, str2, str3, z, z2, z3, z4);
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public static Cookie m6903(HttpUrl httpUrl, String str) {
        return m6902(System.currentTimeMillis(), httpUrl, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m6904(String str, String str2) {
        if (str.equals(str2)) {
            return true;
        }
        return str.endsWith(str2) && str.charAt((str.length() - str2.length()) + -1) == '.' && !Util.m7107(str);
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof Cookie)) {
            return false;
        }
        Cookie cookie = (Cookie) obj;
        return cookie.f6268.equals(this.f6268) && cookie.f6260.equals(this.f6260) && cookie.f6262.equals(this.f6262) && cookie.f6265.equals(this.f6265) && cookie.f6261 == this.f6261 && cookie.f6266 == this.f6266 && cookie.f6267 == this.f6267 && cookie.f6264 == this.f6264 && cookie.f6263 == this.f6263;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((((this.f6268.hashCode() + 527) * 31) + this.f6260.hashCode()) * 31) + this.f6262.hashCode()) * 31) + this.f6265.hashCode()) * 31) + ((int) (this.f6261 ^ (this.f6261 >>> 32)))) * 31) + (this.f6266 ? 0 : 1)) * 31) + (this.f6267 ? 0 : 1)) * 31) + (this.f6264 ? 0 : 1)) * 31;
        if (!this.f6263) {
            i = 1;
        }
        return hashCode + i;
    }

    public String toString() {
        return m6914(false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m6905() {
        return this.f6262;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m6906() {
        return this.f6265;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m6907() {
        return this.f6267;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m6908() {
        return this.f6266;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m6909() {
        return this.f6263;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m6910() {
        return this.f6260;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public long m6911() {
        return this.f6261;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m6912() {
        return this.f6264;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6913() {
        return this.f6268;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m6914(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f6268);
        sb.append('=');
        sb.append(this.f6260);
        if (this.f6264) {
            if (this.f6261 == Long.MIN_VALUE) {
                sb.append("; max-age=0");
            } else {
                sb.append("; expires=").append(HttpDate.m20113(new Date(this.f6261)));
            }
        }
        if (!this.f6263) {
            sb.append("; domain=");
            if (z) {
                sb.append(".");
            }
            sb.append(this.f6262);
        }
        sb.append("; path=").append(this.f6265);
        if (this.f6266) {
            sb.append("; secure");
        }
        if (this.f6267) {
            sb.append("; httponly");
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6915(HttpUrl httpUrl) {
        if ((this.f6263 ? httpUrl.m6951().equals(this.f6262) : m6904(httpUrl.m6951(), this.f6262)) && m6897(httpUrl, this.f6265)) {
            return !this.f6266 || httpUrl.m6965();
        }
        return false;
    }
}
