package okhttp3.internal.platform;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class OptionalMethod<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f16406;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Class[] f16407;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Class<?> f16408;

    OptionalMethod(Class<?> cls, String str, Class... clsArr) {
        this.f16408 = cls;
        this.f16406 = str;
        this.f16407 = clsArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Method m20402(Class<?> cls) {
        if (this.f16406 == null) {
            return null;
        }
        Method r0 = m20403(cls, this.f16406, this.f16407);
        if (r0 == null || this.f16408 == null || this.f16408.isAssignableFrom(r0.getReturnType())) {
            return r0;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Method m20403(Class<?> cls, String str, Class[] clsArr) {
        Method method = null;
        try {
            method = cls.getMethod(str, clsArr);
            if ((method.getModifiers() & 1) == 0) {
                return null;
            }
            return method;
        } catch (NoSuchMethodException e) {
            return method;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Object m20404(T t, Object... objArr) {
        try {
            return m20407(t, objArr);
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Object m20405(T t, Object... objArr) {
        try {
            return m20406(t, objArr);
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Object m20406(T t, Object... objArr) throws InvocationTargetException {
        Method r2 = m20402(t.getClass());
        if (r2 == null) {
            throw new AssertionError("Method " + this.f16406 + " not supported for object " + t);
        }
        try {
            return r2.invoke(t, objArr);
        } catch (IllegalAccessException e) {
            AssertionError assertionError = new AssertionError("Unexpectedly could not call: " + r2);
            assertionError.initCause(e);
            throw assertionError;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m20407(T t, Object... objArr) throws InvocationTargetException {
        Method r1 = m20402(t.getClass());
        if (r1 == null) {
            return null;
        }
        try {
            return r1.invoke(t, objArr);
        } catch (IllegalAccessException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20408(T t) {
        return m20402(t.getClass()) != null;
    }
}
