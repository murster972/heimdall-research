package okhttp3.internal.platform;

import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import okhttp3.Protocol;
import org.conscrypt.Conscrypt;
import org.conscrypt.OpenSSLProvider;

public class ConscryptPlatform extends Platform {
    private ConscryptPlatform() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private Provider m7170() {
        return new OpenSSLProvider();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Platform m7171() {
        try {
            Class.forName("org.conscrypt.ConscryptEngineSocket");
            if (!Conscrypt.isAvailable()) {
                return null;
            }
            Conscrypt.setUseEngineSocketByDefault(true);
            return new ConscryptPlatform();
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public SSLContext D_() {
        try {
            return SSLContext.getInstance("TLS", m7170());
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No TLS provider", e);
        }
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m7172(SSLSocket sSLSocket) {
        return Conscrypt.isConscrypt(sSLSocket) ? Conscrypt.getApplicationProtocol(sSLSocket) : super.m7191(sSLSocket);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7173(SSLSocket sSLSocket, String str, List<Protocol> list) {
        if (Conscrypt.isConscrypt(sSLSocket)) {
            if (str != null) {
                Conscrypt.setUseSessionTickets(sSLSocket, true);
                Conscrypt.setHostname(sSLSocket, str);
            }
            Conscrypt.setApplicationProtocols(sSLSocket, (String[]) Platform.m7185(list).toArray(new String[0]));
            return;
        }
        super.m7196(sSLSocket, str, list);
    }
}
