package okhttp3.internal.platform;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nullable;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.internal.tls.BasicCertificateChainCleaner;
import okhttp3.internal.tls.BasicTrustRootIndex;
import okhttp3.internal.tls.CertificateChainCleaner;
import okhttp3.internal.tls.TrustRootIndex;
import okio.Buffer;

public class Platform {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Logger f6465 = Logger.getLogger(OkHttpClient.class.getName());

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Platform f6466 = m7182();

    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean m7181() {
        if ("conscrypt".equals(System.getProperty("okhttp.platform"))) {
            return true;
        }
        return "Conscrypt".equals(Security.getProviders()[0].getName());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static Platform m7182() {
        Platform r1;
        Platform r0 = AndroidPlatform.m7159();
        if (r0 != null) {
            return r0;
        }
        if (m7181() && (r1 = ConscryptPlatform.m7171()) != null) {
            return r1;
        }
        Jdk9Platform r2 = Jdk9Platform.m7174();
        if (r2 != null) {
            return r2;
        }
        Platform r3 = JdkWithJettyBootPlatform.m7177();
        return r3 != null ? r3 : new Platform();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static byte[] m7183(List<Protocol> list) {
        Buffer buffer = new Buffer();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Protocol protocol = list.get(i);
            if (protocol != Protocol.HTTP_1_0) {
                buffer.m7238(protocol.toString().length());
                buffer.m7246(protocol.toString());
            }
        }
        return buffer.m7220();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Platform m7184() {
        return f6466;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<String> m7185(List<Protocol> list) {
        ArrayList arrayList = new ArrayList(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Protocol protocol = list.get(i);
            if (protocol != Protocol.HTTP_1_0) {
                arrayList.add(protocol.toString());
            }
        }
        return arrayList;
    }

    public SSLContext D_() {
        try {
            return SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No TLS provider", e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public TrustRootIndex m7186(X509TrustManager x509TrustManager) {
        return new BasicTrustRootIndex(x509TrustManager.getAcceptedIssuers());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m7187(SSLSocket sSLSocket) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m7188(String str) {
        return true;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m7189() {
        return "OkHttp";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m7190(String str) {
        if (f6465.isLoggable(Level.FINE)) {
            return new Throwable(str);
        }
        return null;
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m7191(SSLSocket sSLSocket) {
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CertificateChainCleaner m7192(X509TrustManager x509TrustManager) {
        return new BasicCertificateChainCleaner(m7186(x509TrustManager));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7193(int i, String str, Throwable th) {
        f6465.log(i == 5 ? Level.WARNING : Level.INFO, str, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7194(String str, Object obj) {
        if (obj == null) {
            str = str + " To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);";
        }
        m7193(5, str, (Throwable) obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7195(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        socket.connect(inetSocketAddress, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7196(SSLSocket sSLSocket, String str, List<Protocol> list) {
    }
}
