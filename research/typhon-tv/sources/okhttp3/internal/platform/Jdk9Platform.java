package okhttp3.internal.platform;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import okhttp3.Protocol;
import okhttp3.internal.Util;

final class Jdk9Platform extends Platform {

    /* renamed from: 靐  reason: contains not printable characters */
    final Method f6458;

    /* renamed from: 龘  reason: contains not printable characters */
    final Method f6459;

    Jdk9Platform(Method method, Method method2) {
        this.f6459 = method;
        this.f6458 = method2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Jdk9Platform m7174() {
        try {
            return new Jdk9Platform(SSLParameters.class.getMethod("setApplicationProtocols", new Class[]{String[].class}), SSLSocket.class.getMethod("getApplicationProtocol", new Class[0]));
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m7175(SSLSocket sSLSocket) {
        try {
            String str = (String) this.f6458.invoke(sSLSocket, new Object[0]);
            if (str == null || str.equals("")) {
                return null;
            }
            return str;
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw Util.m7114("unable to get selected protocols", (Exception) e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7176(SSLSocket sSLSocket, String str, List<Protocol> list) {
        try {
            SSLParameters sSLParameters = sSLSocket.getSSLParameters();
            List<String> r1 = m7185(list);
            this.f6459.invoke(sSLParameters, new Object[]{r1.toArray(new String[r1.size()])});
            sSLSocket.setSSLParameters(sSLParameters);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw Util.m7114("unable to set ssl parameters", (Exception) e);
        }
    }
}
