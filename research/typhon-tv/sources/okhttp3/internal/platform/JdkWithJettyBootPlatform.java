package okhttp3.internal.platform;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLSocket;
import okhttp3.Protocol;
import okhttp3.internal.Util;

class JdkWithJettyBootPlatform extends Platform {

    /* renamed from: 连任  reason: contains not printable characters */
    private final Class<?> f6460;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Method f6461;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Class<?> f6462;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Method f6463;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Method f6464;

    private static class JettyNegoProvider implements InvocationHandler {

        /* renamed from: 靐  reason: contains not printable characters */
        String f16403;

        /* renamed from: 齉  reason: contains not printable characters */
        private final List<String> f16404;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f16405;

        JettyNegoProvider(List<String> list) {
            this.f16404 = list;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String name = method.getName();
            Class<?> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = Util.f6414;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return true;
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.f16405 = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.f16404;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.f16404.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.f16403 = str;
                            return str;
                        }
                    }
                    String str2 = this.f16404.get(0);
                    this.f16403 = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.f16403 = (String) objArr[0];
                    return null;
                }
            }
        }
    }

    JdkWithJettyBootPlatform(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
        this.f6464 = method;
        this.f6461 = method2;
        this.f6463 = method3;
        this.f6462 = cls;
        this.f6460 = cls2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Platform m7177() {
        try {
            Class<?> cls = Class.forName("org.eclipse.jetty.alpn.ALPN");
            Class<?> cls2 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$Provider");
            Class<?> cls3 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ClientProvider");
            Class<?> cls4 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ServerProvider");
            return new JdkWithJettyBootPlatform(cls.getMethod("put", new Class[]{SSLSocket.class, cls2}), cls.getMethod("get", new Class[]{SSLSocket.class}), cls.getMethod("remove", new Class[]{SSLSocket.class}), cls3, cls4);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m7178(SSLSocket sSLSocket) {
        try {
            this.f6463.invoke((Object) null, new Object[]{sSLSocket});
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw Util.m7114("unable to remove alpn", (Exception) e);
        }
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m7179(SSLSocket sSLSocket) {
        try {
            JettyNegoProvider jettyNegoProvider = (JettyNegoProvider) Proxy.getInvocationHandler(this.f6461.invoke((Object) null, new Object[]{sSLSocket}));
            if (!jettyNegoProvider.f16405 && jettyNegoProvider.f16403 == null) {
                Platform.m7184().m7193(4, "ALPN callback dropped: HTTP/2 is disabled. Is alpn-boot on the boot class path?", (Throwable) null);
                return null;
            } else if (!jettyNegoProvider.f16405) {
                return jettyNegoProvider.f16403;
            } else {
                return null;
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw Util.m7114("unable to get selected protocol", (Exception) e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7180(SSLSocket sSLSocket, String str, List<Protocol> list) {
        List<String> r1 = m7185(list);
        try {
            Object newProxyInstance = Proxy.newProxyInstance(Platform.class.getClassLoader(), new Class[]{this.f6462, this.f6460}, new JettyNegoProvider(r1));
            this.f6464.invoke((Object) null, new Object[]{sSLSocket, newProxyInstance});
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw Util.m7114("unable to set alpn", (Exception) e);
        }
    }
}
