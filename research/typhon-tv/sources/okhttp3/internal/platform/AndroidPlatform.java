package okhttp3.internal.platform;

import android.os.Build;
import android.util.Log;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import okhttp3.Protocol;
import okhttp3.internal.Util;
import okhttp3.internal.tls.CertificateChainCleaner;
import okhttp3.internal.tls.TrustRootIndex;

class AndroidPlatform extends Platform {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final CloseGuard f6452 = CloseGuard.m20399();

    /* renamed from: 连任  reason: contains not printable characters */
    private final OptionalMethod<Socket> f6453;

    /* renamed from: 靐  reason: contains not printable characters */
    private final OptionalMethod<Socket> f6454;

    /* renamed from: 麤  reason: contains not printable characters */
    private final OptionalMethod<Socket> f6455;

    /* renamed from: 齉  reason: contains not printable characters */
    private final OptionalMethod<Socket> f6456;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Class<?> f6457;

    static final class AndroidCertificateChainCleaner extends CertificateChainCleaner {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Method f16396;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object f16397;

        AndroidCertificateChainCleaner(Object obj, Method method) {
            this.f16397 = obj;
            this.f16396 = method;
        }

        public boolean equals(Object obj) {
            return obj instanceof AndroidCertificateChainCleaner;
        }

        public int hashCode() {
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public List<Certificate> m20397(List<Certificate> list, String str) throws SSLPeerUnverifiedException {
            try {
                return (List) this.f16396.invoke(this.f16397, new Object[]{(X509Certificate[]) list.toArray(new X509Certificate[list.size()]), "RSA", str});
            } catch (InvocationTargetException e) {
                SSLPeerUnverifiedException sSLPeerUnverifiedException = new SSLPeerUnverifiedException(e.getMessage());
                sSLPeerUnverifiedException.initCause(e);
                throw sSLPeerUnverifiedException;
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }
    }

    static final class AndroidTrustRootIndex implements TrustRootIndex {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Method f16398;

        /* renamed from: 龘  reason: contains not printable characters */
        private final X509TrustManager f16399;

        AndroidTrustRootIndex(X509TrustManager x509TrustManager, Method method) {
            this.f16398 = method;
            this.f16399 = x509TrustManager;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof AndroidTrustRootIndex)) {
                return false;
            }
            AndroidTrustRootIndex androidTrustRootIndex = (AndroidTrustRootIndex) obj;
            return this.f16399.equals(androidTrustRootIndex.f16399) && this.f16398.equals(androidTrustRootIndex.f16398);
        }

        public int hashCode() {
            return this.f16399.hashCode() + (this.f16398.hashCode() * 31);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public X509Certificate m20398(X509Certificate x509Certificate) {
            try {
                TrustAnchor trustAnchor = (TrustAnchor) this.f16398.invoke(this.f16399, new Object[]{x509Certificate});
                if (trustAnchor != null) {
                    return trustAnchor.getTrustedCert();
                }
                return null;
            } catch (IllegalAccessException e) {
                throw Util.m7114("unable to get issues and signature", (Exception) e);
            } catch (InvocationTargetException e2) {
                return null;
            }
        }
    }

    static final class CloseGuard {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Method f16400;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Method f16401;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Method f16402;

        CloseGuard(Method method, Method method2, Method method3) {
            this.f16402 = method;
            this.f16400 = method2;
            this.f16401 = method3;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static CloseGuard m20399() {
            Method method;
            Method method2;
            Method method3;
            try {
                Class<?> cls = Class.forName("dalvik.system.CloseGuard");
                method = cls.getMethod("get", new Class[0]);
                method2 = cls.getMethod("open", new Class[]{String.class});
                method3 = cls.getMethod("warnIfOpen", new Class[0]);
            } catch (Exception e) {
                method = null;
                method2 = null;
                method3 = null;
            }
            return new CloseGuard(method, method2, method3);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Object m20400(String str) {
            if (this.f16402 != null) {
                try {
                    Object invoke = this.f16402.invoke((Object) null, new Object[0]);
                    this.f16400.invoke(invoke, new Object[]{str});
                    return invoke;
                } catch (Exception e) {
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20401(Object obj) {
            if (obj == null) {
                return false;
            }
            try {
                this.f16401.invoke(obj, new Object[0]);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    }

    AndroidPlatform(Class<?> cls, OptionalMethod<Socket> optionalMethod, OptionalMethod<Socket> optionalMethod2, OptionalMethod<Socket> optionalMethod3, OptionalMethod<Socket> optionalMethod4) {
        this.f6457 = cls;
        this.f6454 = optionalMethod;
        this.f6456 = optionalMethod2;
        this.f6455 = optionalMethod3;
        this.f6453 = optionalMethod4;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m7157() {
        if (Security.getProvider("GMSCore_OpenSSL") != null) {
            return true;
        }
        try {
            Class.forName("android.net.Network");
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m7158(String str, Class<?> cls, Object obj) throws InvocationTargetException, IllegalAccessException {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", new Class[0]).invoke(obj, new Object[0])).booleanValue();
        } catch (NoSuchMethodException e) {
            return super.m7188(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Platform m7159() {
        Class<?> cls;
        try {
            cls = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
        } catch (ClassNotFoundException e) {
            cls = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
        }
        try {
            OptionalMethod optionalMethod = new OptionalMethod((Class<?>) null, "setUseSessionTickets", Boolean.TYPE);
            OptionalMethod optionalMethod2 = new OptionalMethod((Class<?>) null, "setHostname", String.class);
            OptionalMethod optionalMethod3 = null;
            OptionalMethod optionalMethod4 = null;
            if (m7157()) {
                optionalMethod3 = new OptionalMethod(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
                optionalMethod4 = new OptionalMethod((Class<?>) null, "setAlpnProtocols", byte[].class);
            }
            return new AndroidPlatform(cls, optionalMethod, optionalMethod2, optionalMethod3, optionalMethod4);
        } catch (ClassNotFoundException e2) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m7160(String str, Class<?> cls, Object obj) throws InvocationTargetException, IllegalAccessException {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", new Class[]{String.class}).invoke(obj, new Object[]{str})).booleanValue();
        } catch (NoSuchMethodException e) {
            return m7158(str, cls, obj);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public TrustRootIndex m7161(X509TrustManager x509TrustManager) {
        try {
            Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", new Class[]{X509Certificate.class});
            declaredMethod.setAccessible(true);
            return new AndroidTrustRootIndex(x509TrustManager, declaredMethod);
        } catch (NoSuchMethodException e) {
            return super.m7186(x509TrustManager);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m7162(String str) {
        try {
            Class<?> cls = Class.forName("android.security.NetworkSecurityPolicy");
            return m7160(str, cls, cls.getMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]));
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            return super.m7188(str);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
            throw Util.m7114("unable to determine cleartext support", e2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m7163(String str) {
        return this.f6452.m20400(str);
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m7164(SSLSocket sSLSocket) {
        byte[] bArr;
        if (this.f6455 == null || !this.f6455.m20408(sSLSocket) || (bArr = (byte[]) this.f6455.m20405(sSLSocket, new Object[0])) == null) {
            return null;
        }
        return new String(bArr, Util.f6413);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CertificateChainCleaner m7165(X509TrustManager x509TrustManager) {
        try {
            Class<?> cls = Class.forName("android.net.http.X509TrustManagerExtensions");
            return new AndroidCertificateChainCleaner(cls.getConstructor(new Class[]{X509TrustManager.class}).newInstance(new Object[]{x509TrustManager}), cls.getMethod("checkServerTrusted", new Class[]{X509Certificate[].class, String.class, String.class}));
        } catch (Exception e) {
            return super.m7192(x509TrustManager);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7166(int i, String str, Throwable th) {
        int i2 = 5;
        if (i != 5) {
            i2 = 3;
        }
        if (th != null) {
            str = str + 10 + Log.getStackTraceString(th);
        }
        int i3 = 0;
        int length = str.length();
        while (i3 < length) {
            int indexOf = str.indexOf(10, i3);
            if (indexOf == -1) {
                indexOf = length;
            }
            do {
                int min = Math.min(indexOf, i3 + 4000);
                Log.println(i2, "OkHttp", str.substring(i3, min));
                i3 = min;
            } while (i3 < indexOf);
            i3++;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7167(String str, Object obj) {
        if (!this.f6452.m20401(obj)) {
            m7166(5, str, (Throwable) null);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7168(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        try {
            socket.connect(inetSocketAddress, i);
        } catch (AssertionError e) {
            if (Util.m7126(e)) {
                throw new IOException(e);
            }
            throw e;
        } catch (SecurityException e2) {
            IOException iOException = new IOException("Exception in connect");
            iOException.initCause(e2);
            throw iOException;
        } catch (ClassCastException e3) {
            if (Build.VERSION.SDK_INT == 26) {
                IOException iOException2 = new IOException("Exception in connect");
                iOException2.initCause(e3);
                throw iOException2;
            }
            throw e3;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7169(SSLSocket sSLSocket, String str, List<Protocol> list) {
        if (str != null) {
            this.f6454.m20404(sSLSocket, true);
            this.f6456.m20404(sSLSocket, str);
        }
        if (this.f6453 != null && this.f6453.m20408(sSLSocket)) {
            this.f6453.m20405(sSLSocket, m7183(list));
        }
    }
}
