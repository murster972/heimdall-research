package okhttp3.internal.http;

import java.io.IOException;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Sink;

public interface HttpCodec {
    /* renamed from: 靐  reason: contains not printable characters */
    void m20106() throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    void m20107();

    /* renamed from: 龘  reason: contains not printable characters */
    Response.Builder m20108(boolean z) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    ResponseBody m20109(Response response) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    Sink m20110(Request request, long j);

    /* renamed from: 龘  reason: contains not printable characters */
    void m20111() throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m20112(Request request) throws IOException;
}
