package okhttp3.internal.http;

import java.io.IOException;
import java.net.ProtocolException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Util;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.StreamAllocation;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

public final class CallServerInterceptor implements Interceptor {

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f16175;

    static final class CountingSink extends ForwardingSink {

        /* renamed from: 龘  reason: contains not printable characters */
        long f16176;

        CountingSink(Sink sink) {
            super(sink);
        }

        public void a_(Buffer buffer, long j) throws IOException {
            super.a_(buffer, j);
            this.f16176 += j;
        }
    }

    public CallServerInterceptor(boolean z) {
        this.f16175 = z;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        RealInterceptorChain realInterceptorChain = (RealInterceptorChain) chain;
        HttpCodec r7 = realInterceptorChain.m20137();
        StreamAllocation r15 = realInterceptorChain.m20136();
        RealConnection realConnection = (RealConnection) realInterceptorChain.m20141();
        Request r11 = realInterceptorChain.m20144();
        long currentTimeMillis = System.currentTimeMillis();
        realInterceptorChain.m20139().m19923(realInterceptorChain.m20138());
        r7.m20112(r11);
        realInterceptorChain.m20139().m19934(realInterceptorChain.m20138(), r11);
        Response.Builder builder = null;
        if (HttpMethod.m20134(r11.m7048()) && r11.m7050() != null) {
            if ("100-continue".equalsIgnoreCase(r11.m7052("Expect"))) {
                r7.m20111();
                realInterceptorChain.m20139().m19918(realInterceptorChain.m20138());
                builder = r7.m20108(true);
            }
            if (builder == null) {
                realInterceptorChain.m20139().m19922(realInterceptorChain.m20138());
                CountingSink countingSink = new CountingSink(r7.m20110(r11, r11.m7050().contentLength()));
                BufferedSink r4 = Okio.m20506((Sink) countingSink);
                r11.m7050().writeTo(r4);
                r4.close();
                realInterceptorChain.m20139().m19925(realInterceptorChain.m20138(), countingSink.f16176);
            } else if (!realConnection.m20059()) {
                r15.m20095();
            }
        }
        r7.m20106();
        if (builder == null) {
            realInterceptorChain.m20139().m19918(realInterceptorChain.m20138());
            builder = r7.m20108(false);
        }
        Response r13 = builder.m7084(r11).m7081(r15.m20098().m20060()).m7078(currentTimeMillis).m7072(System.currentTimeMillis()).m7087();
        int r5 = r13.m7066();
        if (r5 == 100) {
            r13 = r7.m20108(false).m7084(r11).m7081(r15.m20098().m20060()).m7078(currentTimeMillis).m7072(System.currentTimeMillis()).m7087();
            r5 = r13.m7066();
        }
        realInterceptorChain.m20139().m19935(realInterceptorChain.m20138(), r13);
        Response r132 = (!this.f16175 || r5 != 101) ? r13.m7060().m7086(r7.m20109(r13)).m7087() : r13.m7060().m7086(Util.f6416).m7087();
        if ("close".equalsIgnoreCase(r132.m7069().m7052("Connection")) || "close".equalsIgnoreCase(r132.m7067("Connection"))) {
            r15.m20095();
        }
        if ((r5 != 204 && r5 != 205) || r132.m7056().m7093() <= 0) {
            return r132;
        }
        throw new ProtocolException("HTTP " + r5 + " had non-zero Content-Length: " + r132.m7056().m7093());
    }
}
