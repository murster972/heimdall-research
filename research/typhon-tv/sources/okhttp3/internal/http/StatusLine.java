package okhttp3.internal.http;

import java.io.IOException;
import java.net.ProtocolException;
import okhttp3.Protocol;

public final class StatusLine {

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f16198;

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f16199;

    /* renamed from: 龘  reason: contains not printable characters */
    public final Protocol f16200;

    public StatusLine(Protocol protocol, int i, String str) {
        this.f16200 = protocol;
        this.f16198 = i;
        this.f16199 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static StatusLine m20159(String str) throws IOException {
        Protocol protocol;
        int i;
        if (str.startsWith("HTTP/1.")) {
            if (str.length() < 9 || str.charAt(8) != ' ') {
                throw new ProtocolException("Unexpected status line: " + str);
            }
            int charAt = str.charAt(7) - '0';
            i = 9;
            if (charAt == 0) {
                protocol = Protocol.HTTP_1_0;
            } else if (charAt == 1) {
                protocol = Protocol.HTTP_1_1;
            } else {
                throw new ProtocolException("Unexpected status line: " + str);
            }
        } else if (str.startsWith("ICY ")) {
            protocol = Protocol.HTTP_1_0;
            i = 4;
        } else {
            throw new ProtocolException("Unexpected status line: " + str);
        }
        if (str.length() < i + 3) {
            throw new ProtocolException("Unexpected status line: " + str);
        }
        try {
            int parseInt = Integer.parseInt(str.substring(i, i + 3));
            String str2 = "";
            if (str.length() > i + 3) {
                if (str.charAt(i + 3) != ' ') {
                    throw new ProtocolException("Unexpected status line: " + str);
                }
                str2 = str.substring(i + 4);
            }
            return new StatusLine(protocol, parseInt, str2);
        } catch (NumberFormatException e) {
            throw new ProtocolException("Unexpected status line: " + str);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f16200 == Protocol.HTTP_1_0 ? "HTTP/1.0" : "HTTP/1.1");
        sb.append(' ').append(this.f16198);
        if (this.f16199 != null) {
            sb.append(' ').append(this.f16199);
        }
        return sb.toString();
    }
}
