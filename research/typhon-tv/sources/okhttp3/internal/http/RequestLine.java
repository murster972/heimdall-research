package okhttp3.internal.http;

import java.net.Proxy;
import okhttp3.HttpUrl;
import okhttp3.Request;

public final class RequestLine {
    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m20147(Request request, Proxy.Type type) {
        return !request.m7046() && type == Proxy.Type.HTTP;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m20148(HttpUrl httpUrl) {
        String r0 = httpUrl.m6953();
        String r1 = httpUrl.m6959();
        return r1 != null ? r0 + '?' + r1 : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m20149(Request request, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(request.m7048());
        sb.append(' ');
        if (m20147(request, type)) {
            sb.append(request.m7053());
        } else {
            sb.append(m20148(request.m7053()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }
}
