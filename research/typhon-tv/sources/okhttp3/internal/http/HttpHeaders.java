package okhttp3.internal.http;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Util;

public final class HttpHeaders {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f16180 = Pattern.compile(" +([^ \"=]*)=(:?\"([^\"]*)\"|([^ \"=]*)) *(:?,|$)");

    /* renamed from: 连任  reason: contains not printable characters */
    private static Set<String> m20116(Response response) {
        return m20121(response.m7055());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m20117(String str, int i) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 2147483647L) {
                return MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
            }
            if (parseLong < 0) {
                return 0;
            }
            return (int) parseLong;
        } catch (NumberFormatException e) {
            return i;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m20118(Headers headers) {
        return m20121(headers).contains("*");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m20119(Response response) {
        return m20118(response.m7055());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m20120(Response response) {
        if (response.m7069().m7048().equals("HEAD")) {
            return false;
        }
        int r0 = response.m7066();
        if ((r0 >= 100 && r0 < 200) || r0 == 204 || r0 == 304) {
            return m20127(response) != -1 || "chunked".equalsIgnoreCase(response.m7067("Transfer-Encoding"));
        }
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Set<String> m20121(Headers headers) {
        Set<String> emptySet = Collections.emptySet();
        int r2 = headers.m6934();
        for (int i = 0; i < r2; i++) {
            if ("Vary".equalsIgnoreCase(headers.m6935(i))) {
                String r3 = headers.m6930(i);
                if (emptySet.isEmpty()) {
                    emptySet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                for (String trim : r3.split(",")) {
                    emptySet.add(trim.trim());
                }
            }
        }
        return emptySet;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Headers m20122(Response response) {
        return m20128(response.m7061().m7069().m7051(), response.m7055());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m20123(String str, int i) {
        while (i < str.length() && ((r0 = str.charAt(i)) == ' ' || r0 == 9)) {
            i++;
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m20124(String str, int i, String str2) {
        while (i < str.length() && str2.indexOf(str.charAt(i)) == -1) {
            i++;
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m20125(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m20126(Headers headers) {
        return m20125(headers.m6936("Content-Length"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m20127(Response response) {
        return m20126(response.m7055());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Headers m20128(Headers headers, Headers headers2) {
        Set<String> r4 = m20121(headers2);
        if (r4.isEmpty()) {
            return new Headers.Builder().m19955();
        }
        Headers.Builder builder = new Headers.Builder();
        int r3 = headers.m6934();
        for (int i = 0; i < r3; i++) {
            String r0 = headers.m6935(i);
            if (r4.contains(r0)) {
                builder.m19954(r0, headers.m6930(i));
            }
        }
        return builder.m19955();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m20129(CookieJar cookieJar, HttpUrl httpUrl, Headers headers) {
        if (cookieJar != CookieJar.f16029) {
            List<Cookie> r0 = Cookie.m6901(httpUrl, headers);
            if (!r0.isEmpty()) {
                cookieJar.m19910(httpUrl, r0);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m20130(Response response, Headers headers, Request request) {
        for (String next : m20116(response)) {
            if (!Util.m7127((Object) headers.m6931(next), (Object) request.m7049(next))) {
                return false;
            }
        }
        return true;
    }
}
