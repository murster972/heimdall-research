package okhttp3.internal.http;

import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.IOException;
import java.util.List;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Util;
import okhttp3.internal.Version;
import okio.GzipSource;
import okio.Okio;
import okio.Source;
import org.apache.oltu.oauth2.common.OAuth;

public final class BridgeInterceptor implements Interceptor {

    /* renamed from: 龘  reason: contains not printable characters */
    private final CookieJar f16174;

    public BridgeInterceptor(CookieJar cookieJar) {
        this.f16174 = cookieJar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m20105(List<Cookie> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            Cookie cookie = list.get(i);
            sb.append(cookie.m6913()).append('=').append(cookie.m6910());
        }
        return sb.toString();
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request r13 = chain.m6994();
        Request.Builder r8 = r13.m7044();
        RequestBody r2 = r13.m7050();
        if (r2 != null) {
            MediaType contentType = r2.contentType();
            if (contentType != null) {
                r8.m19993(OAuth.HeaderType.CONTENT_TYPE, contentType.toString());
            }
            long contentLength = r2.contentLength();
            if (contentLength != -1) {
                r8.m19993("Content-Length", Long.toString(contentLength));
                r8.m19987("Transfer-Encoding");
            } else {
                r8.m19993("Transfer-Encoding", "chunked");
                r8.m19987("Content-Length");
            }
        }
        if (r13.m7052("Host") == null) {
            r8.m19993("Host", Util.m7117(r13.m7053(), false));
        }
        if (r13.m7052("Connection") == null) {
            r8.m19993("Connection", "Keep-Alive");
        }
        boolean z = false;
        if (r13.m7052("Accept-Encoding") == null && r13.m7052("Range") == null) {
            z = true;
            r8.m19993("Accept-Encoding", "gzip");
        }
        List<Cookie> r6 = this.f16174.m19909(r13.m7053());
        if (!r6.isEmpty()) {
            r8.m19993("Cookie", m20105(r6));
        }
        if (r13.m7052(AbstractSpiCall.HEADER_USER_AGENT) == null) {
            r8.m19993(AbstractSpiCall.HEADER_USER_AGENT, Version.m20016());
        }
        Response r7 = chain.m6995(r8.m19989());
        HttpHeaders.m20129(this.f16174, r13.m7053(), r7.m7055());
        Response.Builder r10 = r7.m7060().m7084(r13);
        if (z && "gzip".equalsIgnoreCase(r7.m7067("Content-Encoding")) && HttpHeaders.m20120(r7)) {
            GzipSource gzipSource = new GzipSource(r7.m7056().m7095());
            r10.m7082(r7.m7055().m6932().m19949("Content-Encoding").m19949("Content-Length").m19955());
            r10.m7086((ResponseBody) new RealResponseBody(r7.m7067(OAuth.HeaderType.CONTENT_TYPE), -1, Okio.m20507((Source) gzipSource)));
        }
        return r10.m7087();
    }
}
