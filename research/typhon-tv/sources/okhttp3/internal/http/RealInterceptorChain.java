package okhttp3.internal.http;

import java.io.IOException;
import java.util.List;
import okhttp3.Call;
import okhttp3.Connection;
import okhttp3.EventListener;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.StreamAllocation;

public final class RealInterceptorChain implements Interceptor.Chain {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Request f16181;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Call f16182;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final EventListener f16183;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f16184;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f16185;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final int f16186;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f16187;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f16188;

    /* renamed from: 靐  reason: contains not printable characters */
    private final StreamAllocation f16189;

    /* renamed from: 麤  reason: contains not printable characters */
    private final RealConnection f16190;

    /* renamed from: 齉  reason: contains not printable characters */
    private final HttpCodec f16191;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<Interceptor> f16192;

    public RealInterceptorChain(List<Interceptor> list, StreamAllocation streamAllocation, HttpCodec httpCodec, RealConnection realConnection, int i, Request request, Call call, EventListener eventListener, int i2, int i3, int i4) {
        this.f16192 = list;
        this.f16190 = realConnection;
        this.f16189 = streamAllocation;
        this.f16191 = httpCodec;
        this.f16188 = i;
        this.f16181 = request;
        this.f16182 = call;
        this.f16183 = eventListener;
        this.f16185 = i2;
        this.f16186 = i3;
        this.f16187 = i4;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public StreamAllocation m20136() {
        return this.f16189;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public HttpCodec m20137() {
        return this.f16191;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Call m20138() {
        return this.f16182;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public EventListener m20139() {
        return this.f16183;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m20140() {
        return this.f16187;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Connection m20141() {
        return this.f16190;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m20142() {
        return this.f16186;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m20143() {
        return this.f16185;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Request m20144() {
        return this.f16181;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Response m20145(Request request) throws IOException {
        return m20146(request, this.f16189, this.f16191, this.f16190);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Response m20146(Request request, StreamAllocation streamAllocation, HttpCodec httpCodec, RealConnection realConnection) throws IOException {
        if (this.f16188 >= this.f16192.size()) {
            throw new AssertionError();
        }
        this.f16184++;
        if (this.f16191 != null && !this.f16190.m20070(request.m7053())) {
            throw new IllegalStateException("network interceptor " + this.f16192.get(this.f16188 - 1) + " must retain the same host and port");
        } else if (this.f16191 == null || this.f16184 <= 1) {
            RealInterceptorChain realInterceptorChain = new RealInterceptorChain(this.f16192, streamAllocation, httpCodec, realConnection, this.f16188 + 1, request, this.f16182, this.f16183, this.f16185, this.f16186, this.f16187);
            Interceptor interceptor = this.f16192.get(this.f16188);
            Response intercept = interceptor.intercept(realInterceptorChain);
            if (httpCodec != null && this.f16188 + 1 < this.f16192.size() && realInterceptorChain.f16184 != 1) {
                throw new IllegalStateException("network interceptor " + interceptor + " must call proceed() exactly once");
            } else if (intercept == null) {
                throw new NullPointerException("interceptor " + interceptor + " returned null");
            } else if (intercept.m7056() != null) {
                return intercept;
            } else {
                throw new IllegalStateException("interceptor " + interceptor + " returned a response with no body");
            }
        } else {
            throw new IllegalStateException("network interceptor " + this.f16192.get(this.f16188 - 1) + " must call proceed() exactly once");
        }
    }
}
