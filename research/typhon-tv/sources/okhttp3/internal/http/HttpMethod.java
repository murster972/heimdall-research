package okhttp3.internal.http;

import com.mopub.volley.toolbox.HttpClientStack;
import org.apache.oltu.oauth2.common.OAuth;

public final class HttpMethod {
    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean m20131(String str) {
        return !str.equals("PROPFIND");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m20132(String str) {
        return str.equals(OAuth.HttpMethod.POST) || str.equals(OAuth.HttpMethod.PUT) || str.equals(HttpClientStack.HttpPatch.METHOD_NAME) || str.equals("PROPPATCH") || str.equals("REPORT");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m20133(String str) {
        return str.equals("PROPFIND");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m20134(String str) {
        return !str.equals(OAuth.HttpMethod.GET) && !str.equals("HEAD");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m20135(String str) {
        return str.equals(OAuth.HttpMethod.POST) || str.equals(HttpClientStack.HttpPatch.METHOD_NAME) || str.equals(OAuth.HttpMethod.PUT) || str.equals(OAuth.HttpMethod.DELETE) || str.equals("MOVE");
    }
}
