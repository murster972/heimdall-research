package okhttp3.internal.http;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import okhttp3.internal.Util;

public final class HttpDate {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f16177 = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z", "EEE MMM d yyyy HH:mm:ss z"};

    /* renamed from: 齉  reason: contains not printable characters */
    private static final DateFormat[] f16178 = new DateFormat[f16177.length];

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ThreadLocal<DateFormat> f16179 = new ThreadLocal<DateFormat>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public DateFormat initialValue() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
            simpleDateFormat.setLenient(false);
            simpleDateFormat.setTimeZone(Util.f6403);
            return simpleDateFormat;
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m20113(Date date) {
        return f16179.get().format(date);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Date m20114(String str) {
        if (str.length() == 0) {
            return null;
        }
        ParsePosition parsePosition = new ParsePosition(0);
        Date parse = f16179.get().parse(str, parsePosition);
        if (parsePosition.getIndex() == str.length()) {
            return parse;
        }
        synchronized (f16177) {
            int length = f16177.length;
            for (int i = 0; i < length; i++) {
                DateFormat dateFormat = f16178[i];
                if (dateFormat == null) {
                    dateFormat = new SimpleDateFormat(f16177[i], Locale.US);
                    dateFormat.setTimeZone(Util.f6403);
                    f16178[i] = dateFormat;
                }
                parsePosition.setIndex(0);
                Date parse2 = dateFormat.parse(str, parsePosition);
                if (parsePosition.getIndex() != 0) {
                    return parse2;
                }
            }
            return null;
        }
    }
}
