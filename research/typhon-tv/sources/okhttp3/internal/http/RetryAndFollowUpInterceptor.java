package okhttp3.internal.http;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.HttpRetryException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.Address;
import okhttp3.Call;
import okhttp3.CertificatePinner;
import okhttp3.EventListener;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.Route;
import okhttp3.internal.Util;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.RouteException;
import okhttp3.internal.connection.StreamAllocation;
import okhttp3.internal.http2.ConnectionShutdownException;
import org.apache.oltu.oauth2.common.OAuth;

public final class RetryAndFollowUpInterceptor implements Interceptor {

    /* renamed from: 连任  reason: contains not printable characters */
    private volatile boolean f16193;

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f16194;

    /* renamed from: 麤  reason: contains not printable characters */
    private Object f16195;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile StreamAllocation f16196;

    /* renamed from: 龘  reason: contains not printable characters */
    private final OkHttpClient f16197;

    public RetryAndFollowUpInterceptor(OkHttpClient okHttpClient, boolean z) {
        this.f16197 = okHttpClient;
        this.f16194 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m20150(Response response, int i) {
        String r0 = response.m7067("Retry-After");
        return r0 == null ? i : r0.matches("\\d+") ? Integer.valueOf(r0).intValue() : MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Address m20151(HttpUrl httpUrl) {
        SSLSocketFactory sSLSocketFactory = null;
        HostnameVerifier hostnameVerifier = null;
        CertificatePinner certificatePinner = null;
        if (httpUrl.m6965()) {
            sSLSocketFactory = this.f16197.m7019();
            hostnameVerifier = this.f16197.m7009();
            certificatePinner = this.f16197.m7006();
        }
        return new Address(httpUrl.m6951(), httpUrl.m6952(), this.f16197.m7015(), this.f16197.m7018(), sSLSocketFactory, hostnameVerifier, certificatePinner, this.f16197.m7028(), this.f16197.m7022(), this.f16197.m7010(), this.f16197.m7014(), this.f16197.m7003());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Request m20152(Response response, Route route) throws IOException {
        String r0;
        HttpUrl r8;
        RequestBody requestBody = null;
        if (response == null) {
            throw new IllegalStateException();
        }
        int r5 = response.m7066();
        String r2 = response.m7069().m7048();
        switch (r5) {
            case 300:
            case 301:
            case 302:
            case 303:
                break;
            case 307:
            case 308:
                if (!r2.equals(OAuth.HttpMethod.GET) && !r2.equals("HEAD")) {
                    return null;
                }
            case 401:
                return this.f16197.m7007().m6840(route, response);
            case 407:
                if ((route != null ? route.m19999() : this.f16197.m7022()).type() == Proxy.Type.HTTP) {
                    return this.f16197.m7028().m6840(route, response);
                }
                throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
            case 408:
                if (!this.f16197.m7013() || (response.m7069().m7050() instanceof UnrepeatableRequestBody)) {
                    return null;
                }
                if ((response.m7062() == null || response.m7062().m7066() != 408) && m20150(response, 0) <= 0) {
                    return response.m7069();
                }
                return null;
            case 503:
                if ((response.m7062() == null || response.m7062().m7066() != 503) && m20150(response, (int) MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT) == 0) {
                    return response.m7069();
                }
                return null;
            default:
                return null;
        }
        if (!this.f16197.m7012() || (r0 = response.m7067("Location")) == null || (r8 = response.m7069().m7053().m6964(r0)) == null) {
            return null;
        }
        if (!r8.m6961().equals(response.m7069().m7053().m6961()) && !this.f16197.m7011()) {
            return null;
        }
        Request.Builder r4 = response.m7069().m7044();
        if (HttpMethod.m20134(r2)) {
            boolean r1 = HttpMethod.m20133(r2);
            if (HttpMethod.m20131(r2)) {
                r4.m19994(OAuth.HttpMethod.GET, (RequestBody) null);
            } else {
                if (r1) {
                    requestBody = response.m7069().m7050();
                }
                r4.m19994(r2, requestBody);
            }
            if (!r1) {
                r4.m19987("Transfer-Encoding");
                r4.m19987("Content-Length");
                r4.m19987(OAuth.HeaderType.CONTENT_TYPE);
            }
        }
        if (!m20155(response, r8)) {
            r4.m19987(OAuth.HeaderType.AUTHORIZATION);
        }
        return r4.m19997(r8).m19989();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m20153(IOException iOException, StreamAllocation streamAllocation, boolean z, Request request) {
        streamAllocation.m20102(iOException);
        if (!this.f16197.m7013()) {
            return false;
        }
        return (!z || !(request.m7050() instanceof UnrepeatableRequestBody)) && m20154(iOException, z) && streamAllocation.m20094();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m20154(IOException iOException, boolean z) {
        boolean z2 = true;
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (!(iOException instanceof InterruptedIOException)) {
            return (!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException);
        }
        if (!(iOException instanceof SocketTimeoutException) || z) {
            z2 = false;
        }
        return z2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m20155(Response response, HttpUrl httpUrl) {
        HttpUrl r0 = response.m7069().m7053();
        return r0.m6951().equals(httpUrl.m6951()) && r0.m6952() == httpUrl.m6952() && r0.m6961().equals(httpUrl.m6961());
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request r12 = chain.m6994();
        RealInterceptorChain realInterceptorChain = (RealInterceptorChain) chain;
        Call r3 = realInterceptorChain.m20138();
        EventListener r4 = realInterceptorChain.m20139();
        StreamAllocation streamAllocation = new StreamAllocation(this.f16197.m7029(), m20151(r12.m7053()), r3, r4, this.f16195);
        this.f16196 = streamAllocation;
        int i = 0;
        Response response = null;
        while (!this.f16193) {
            try {
                Response r14 = realInterceptorChain.m20146(r12, streamAllocation, (HttpCodec) null, (RealConnection) null);
                if (0 != 0) {
                    streamAllocation.m20102((IOException) null);
                    streamAllocation.m20097();
                }
                if (response != null) {
                    r14 = r14.m7060().m7076(response.m7060().m7086((ResponseBody) null).m7087()).m7087();
                }
                Request r7 = m20152(r14, streamAllocation.m20096());
                if (r7 == null) {
                    if (!this.f16194) {
                        streamAllocation.m20097();
                    }
                    return r14;
                }
                Util.m7124((Closeable) r14.m7056());
                i++;
                if (i > 20) {
                    streamAllocation.m20097();
                    throw new ProtocolException("Too many follow-up requests: " + i);
                } else if (r7.m7050() instanceof UnrepeatableRequestBody) {
                    streamAllocation.m20097();
                    throw new HttpRetryException("Cannot retry streamed HTTP body", r14.m7066());
                } else {
                    if (!m20155(r14, r7.m7053())) {
                        streamAllocation.m20097();
                        streamAllocation = new StreamAllocation(this.f16197.m7029(), m20151(r7.m7053()), r3, r4, this.f16195);
                        this.f16196 = streamAllocation;
                    } else if (streamAllocation.m20100() != null) {
                        throw new IllegalStateException("Closing the body of " + r14 + " didn't close its backing stream. Bad interceptor?");
                    }
                    r12 = r7;
                    response = r14;
                }
            } catch (RouteException e) {
                if (!m20153(e.getLastConnectException(), streamAllocation, false, r12)) {
                    throw e.getLastConnectException();
                } else if (0 != 0) {
                    streamAllocation.m20102((IOException) null);
                    streamAllocation.m20097();
                }
            } catch (IOException e2) {
                if (!m20153(e2, streamAllocation, !(e2 instanceof ConnectionShutdownException), r12)) {
                    throw e2;
                } else if (0 != 0) {
                    streamAllocation.m20102((IOException) null);
                    streamAllocation.m20097();
                }
            } catch (Throwable th) {
                if (1 != 0) {
                    streamAllocation.m20102((IOException) null);
                    streamAllocation.m20097();
                }
                throw th;
            }
        }
        streamAllocation.m20097();
        throw new IOException("Canceled");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m20156() {
        return this.f16193;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20157() {
        this.f16193 = true;
        StreamAllocation streamAllocation = this.f16196;
        if (streamAllocation != null) {
            streamAllocation.m20093();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20158(Object obj) {
        this.f16195 = obj;
    }
}
