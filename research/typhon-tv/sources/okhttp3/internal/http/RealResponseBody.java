package okhttp3.internal.http;

import javax.annotation.Nullable;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;

public final class RealResponseBody extends ResponseBody {

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f6449;

    /* renamed from: 齉  reason: contains not printable characters */
    private final BufferedSource f6450;
    @Nullable

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f6451;

    public RealResponseBody(@Nullable String str, long j, BufferedSource bufferedSource) {
        this.f6451 = str;
        this.f6449 = j;
        this.f6450 = bufferedSource;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m7154() {
        return this.f6449;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public BufferedSource m7155() {
        return this.f6450;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaType m7156() {
        if (this.f6451 != null) {
            return MediaType.m6996(this.f6451);
        }
        return null;
    }
}
