package okhttp3.internal.http1;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.StreamAllocation;
import okhttp3.internal.http.HttpCodec;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.RealResponseBody;
import okhttp3.internal.http.RequestLine;
import okhttp3.internal.http.StatusLine;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.ForwardingTimeout;
import okio.Okio;
import okio.Sink;
import okio.Source;
import okio.Timeout;
import org.apache.oltu.oauth2.common.OAuth;

public final class Http1Codec implements HttpCodec {

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f16201 = PlaybackStateCompat.ACTION_SET_REPEAT_MODE;

    /* renamed from: 连任  reason: contains not printable characters */
    int f16202 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    final StreamAllocation f16203;

    /* renamed from: 麤  reason: contains not printable characters */
    final BufferedSink f16204;

    /* renamed from: 齉  reason: contains not printable characters */
    final BufferedSource f16205;

    /* renamed from: 龘  reason: contains not printable characters */
    final OkHttpClient f16206;

    private abstract class AbstractSource implements Source {

        /* renamed from: 靐  reason: contains not printable characters */
        protected boolean f16207;

        /* renamed from: 齉  reason: contains not printable characters */
        protected long f16209;

        /* renamed from: 龘  reason: contains not printable characters */
        protected final ForwardingTimeout f16210;

        private AbstractSource() {
            this.f16210 = new ForwardingTimeout(Http1Codec.this.f16205.m20569());
            this.f16209 = 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m20176(Buffer buffer, long j) throws IOException {
            try {
                long r2 = Http1Codec.this.f16205.m20568(buffer, j);
                if (r2 > 0) {
                    this.f16209 += r2;
                }
                return r2;
            } catch (IOException e) {
                m20178(false, e);
                throw e;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Timeout m20177() {
            return this.f16210;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m20178(boolean z, IOException iOException) throws IOException {
            if (Http1Codec.this.f16202 != 6) {
                if (Http1Codec.this.f16202 != 5) {
                    throw new IllegalStateException("state: " + Http1Codec.this.f16202);
                }
                Http1Codec.this.m20175(this.f16210);
                Http1Codec.this.f16202 = 6;
                if (Http1Codec.this.f16203 != null) {
                    Http1Codec.this.f16203.m20104(!z, Http1Codec.this, this.f16209, iOException);
                }
            }
        }
    }

    private final class ChunkedSink implements Sink {

        /* renamed from: 靐  reason: contains not printable characters */
        private final ForwardingTimeout f16211 = new ForwardingTimeout(Http1Codec.this.f16204.m20567());

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f16212;

        ChunkedSink() {
        }

        public void a_(Buffer buffer, long j) throws IOException {
            if (this.f16212) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                Http1Codec.this.f16204.m20440(j);
                Http1Codec.this.f16204.m20445("\r\n");
                Http1Codec.this.f16204.a_(buffer, j);
                Http1Codec.this.f16204.m20445("\r\n");
            }
        }

        public synchronized void close() throws IOException {
            if (!this.f16212) {
                this.f16212 = true;
                Http1Codec.this.f16204.m20445("0\r\n\r\n");
                Http1Codec.this.m20175(this.f16211);
                Http1Codec.this.f16202 = 3;
            }
        }

        public synchronized void flush() throws IOException {
            if (!this.f16212) {
                Http1Codec.this.f16204.flush();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Timeout m20179() {
            return this.f16211;
        }
    }

    private class ChunkedSource extends AbstractSource {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final HttpUrl f16214;

        /* renamed from: ʼ  reason: contains not printable characters */
        private long f16215 = -1;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f16216 = true;

        ChunkedSource(HttpUrl httpUrl) {
            super();
            this.f16214 = httpUrl;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m20180() throws IOException {
            if (this.f16215 != -1) {
                Http1Codec.this.f16205.m20459();
            }
            try {
                this.f16215 = Http1Codec.this.f16205.m20473();
                String trim = Http1Codec.this.f16205.m20459().trim();
                if (this.f16215 < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.f16215 + trim + "\"");
                } else if (this.f16215 == 0) {
                    this.f16216 = false;
                    HttpHeaders.m20129(Http1Codec.this.f16206.m7004(), this.f16214, Http1Codec.this.m20165());
                    m20178(true, (IOException) null);
                }
            } catch (NumberFormatException e) {
                throw new ProtocolException(e.getMessage());
            }
        }

        public void close() throws IOException {
            if (!this.f16207) {
                if (this.f16216 && !Util.m7129((Source) this, 100, TimeUnit.MILLISECONDS)) {
                    m20178(false, (IOException) null);
                }
                this.f16207 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m20181(Buffer buffer, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f16207) {
                throw new IllegalStateException("closed");
            } else if (!this.f16216) {
                return -1;
            } else {
                if (this.f16215 == 0 || this.f16215 == -1) {
                    m20180();
                    if (!this.f16216) {
                        return -1;
                    }
                }
                long r2 = super.m20176(buffer, Math.min(j, this.f16215));
                if (r2 == -1) {
                    ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                    m20178(false, (IOException) protocolException);
                    throw protocolException;
                }
                this.f16215 -= r2;
                return r2;
            }
        }
    }

    private final class FixedLengthSink implements Sink {

        /* renamed from: 靐  reason: contains not printable characters */
        private final ForwardingTimeout f16218 = new ForwardingTimeout(Http1Codec.this.f16204.m20567());

        /* renamed from: 麤  reason: contains not printable characters */
        private long f16219;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f16220;

        FixedLengthSink(long j) {
            this.f16219 = j;
        }

        public void a_(Buffer buffer, long j) throws IOException {
            if (this.f16220) {
                throw new IllegalStateException("closed");
            }
            Util.m7123(buffer.m7242(), 0, j);
            if (j > this.f16219) {
                throw new ProtocolException("expected " + this.f16219 + " bytes but received " + j);
            }
            Http1Codec.this.f16204.a_(buffer, j);
            this.f16219 -= j;
        }

        public void close() throws IOException {
            if (!this.f16220) {
                this.f16220 = true;
                if (this.f16219 > 0) {
                    throw new ProtocolException("unexpected end of stream");
                }
                Http1Codec.this.m20175(this.f16218);
                Http1Codec.this.f16202 = 3;
            }
        }

        public void flush() throws IOException {
            if (!this.f16220) {
                Http1Codec.this.f16204.flush();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Timeout m20182() {
            return this.f16218;
        }
    }

    private class FixedLengthSource extends AbstractSource {

        /* renamed from: ʻ  reason: contains not printable characters */
        private long f16222;

        FixedLengthSource(long j) throws IOException {
            super();
            this.f16222 = j;
            if (this.f16222 == 0) {
                m20178(true, (IOException) null);
            }
        }

        public void close() throws IOException {
            if (!this.f16207) {
                if (this.f16222 != 0 && !Util.m7129((Source) this, 100, TimeUnit.MILLISECONDS)) {
                    m20178(false, (IOException) null);
                }
                this.f16207 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m20183(Buffer buffer, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f16207) {
                throw new IllegalStateException("closed");
            } else if (this.f16222 == 0) {
                return -1;
            } else {
                long r2 = super.m20176(buffer, Math.min(this.f16222, j));
                if (r2 == -1) {
                    ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                    m20178(false, (IOException) protocolException);
                    throw protocolException;
                }
                this.f16222 -= r2;
                if (this.f16222 != 0) {
                    return r2;
                }
                m20178(true, (IOException) null);
                return r2;
            }
        }
    }

    private class UnknownLengthSource extends AbstractSource {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f16224;

        UnknownLengthSource() {
            super();
        }

        public void close() throws IOException {
            if (!this.f16207) {
                if (!this.f16224) {
                    m20178(false, (IOException) null);
                }
                this.f16207 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m20184(Buffer buffer, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f16207) {
                throw new IllegalStateException("closed");
            } else if (this.f16224) {
                return -1;
            } else {
                long r0 = super.m20176(buffer, j);
                if (r0 != -1) {
                    return r0;
                }
                this.f16224 = true;
                m20178(true, (IOException) null);
                return -1;
            }
        }
    }

    public Http1Codec(OkHttpClient okHttpClient, StreamAllocation streamAllocation, BufferedSource bufferedSource, BufferedSink bufferedSink) {
        this.f16206 = okHttpClient;
        this.f16203 = streamAllocation;
        this.f16205 = bufferedSource;
        this.f16204 = bufferedSink;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private String m20160() throws IOException {
        String r0 = this.f16205.m20451(this.f16201);
        this.f16201 -= (long) r0.length();
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Source m20161() throws IOException {
        if (this.f16202 != 4) {
            throw new IllegalStateException("state: " + this.f16202);
        } else if (this.f16203 == null) {
            throw new IllegalStateException("streamAllocation == null");
        } else {
            this.f16202 = 5;
            this.f16203.m20095();
            return new UnknownLengthSource();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Sink m20162() {
        if (this.f16202 != 1) {
            throw new IllegalStateException("state: " + this.f16202);
        }
        this.f16202 = 2;
        return new ChunkedSink();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Source m20163(long j) throws IOException {
        if (this.f16202 != 4) {
            throw new IllegalStateException("state: " + this.f16202);
        }
        this.f16202 = 5;
        return new FixedLengthSource(j);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m20164() throws IOException {
        this.f16204.flush();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Headers m20165() throws IOException {
        Headers.Builder builder = new Headers.Builder();
        while (true) {
            String r1 = m20160();
            if (r1.length() == 0) {
                return builder.m19955();
            }
            Internal.f16088.m20010(builder, r1);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m20166() {
        RealConnection r0 = this.f16203.m20098();
        if (r0 != null) {
            r0.m20063();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Response.Builder m20167(boolean z) throws IOException {
        if (this.f16202 == 1 || this.f16202 == 3) {
            try {
                StatusLine r3 = StatusLine.m20159(m20160());
                Response.Builder r2 = new Response.Builder().m7083(r3.f16200).m7077(r3.f16198).m7079(r3.f16199).m7082(m20165());
                if (z && r3.f16198 == 100) {
                    return null;
                }
                if (r3.f16198 == 100) {
                    this.f16202 = 3;
                    return r2;
                }
                this.f16202 = 4;
                return r2;
            } catch (EOFException e) {
                IOException iOException = new IOException("unexpected end of stream on " + this.f16203);
                iOException.initCause(e);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.f16202);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResponseBody m20168(Response response) throws IOException {
        this.f16203.f16170.m19916(this.f16203.f16169);
        String r2 = response.m7067(OAuth.HeaderType.CONTENT_TYPE);
        if (!HttpHeaders.m20120(response)) {
            return new RealResponseBody(r2, 0, Okio.m20507(m20163(0)));
        }
        if ("chunked".equalsIgnoreCase(response.m7067("Transfer-Encoding"))) {
            return new RealResponseBody(r2, -1, Okio.m20507(m20171(response.m7069().m7053())));
        }
        long r0 = HttpHeaders.m20127(response);
        return r0 != -1 ? new RealResponseBody(r2, r0, Okio.m20507(m20163(r0))) : new RealResponseBody(r2, -1, Okio.m20507(m20161()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Sink m20169(long j) {
        if (this.f16202 != 1) {
            throw new IllegalStateException("state: " + this.f16202);
        }
        this.f16202 = 2;
        return new FixedLengthSink(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Sink m20170(Request request, long j) {
        if ("chunked".equalsIgnoreCase(request.m7052("Transfer-Encoding"))) {
            return m20162();
        }
        if (j != -1) {
            return m20169(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Source m20171(HttpUrl httpUrl) throws IOException {
        if (this.f16202 != 4) {
            throw new IllegalStateException("state: " + this.f16202);
        }
        this.f16202 = 5;
        return new ChunkedSource(httpUrl);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20172() throws IOException {
        this.f16204.flush();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20173(Headers headers, String str) throws IOException {
        if (this.f16202 != 0) {
            throw new IllegalStateException("state: " + this.f16202);
        }
        this.f16204.m20445(str).m20445("\r\n");
        int r1 = headers.m6934();
        for (int i = 0; i < r1; i++) {
            this.f16204.m20445(headers.m6935(i)).m20445(": ").m20445(headers.m6930(i)).m20445("\r\n");
        }
        this.f16204.m20445("\r\n");
        this.f16202 = 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20174(Request request) throws IOException {
        m20173(request.m7051(), RequestLine.m20149(request, this.f16203.m20098().m20061().m19999().type()));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20175(ForwardingTimeout forwardingTimeout) {
        Timeout r0 = forwardingTimeout.m20485();
        forwardingTimeout.m20484(Timeout.f16468);
        r0.m20570();
        r0.C_();
    }
}
