package okhttp3.internal.publicsuffix;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.IDN;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import okhttp3.internal.Util;
import okhttp3.internal.platform.Platform;
import okio.BufferedSource;
import okio.GzipSource;
import okio.Okio;
import okio.Source;

public final class PublicSuffixDatabase {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f16409 = new String[0];

    /* renamed from: 麤  reason: contains not printable characters */
    private static final PublicSuffixDatabase f16410 = new PublicSuffixDatabase();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final String[] f16411 = {"*"};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final byte[] f16412 = {42};

    /* renamed from: ʻ  reason: contains not printable characters */
    private final CountDownLatch f16413 = new CountDownLatch(1);

    /* renamed from: ʼ  reason: contains not printable characters */
    private byte[] f16414;

    /* renamed from: ʽ  reason: contains not printable characters */
    private byte[] f16415;

    /* renamed from: 连任  reason: contains not printable characters */
    private final AtomicBoolean f16416 = new AtomicBoolean(false);

    /* renamed from: 靐  reason: contains not printable characters */
    private void m20409() {
        boolean z = false;
        while (true) {
            try {
                m20410();
                break;
            } catch (InterruptedIOException e) {
                z = true;
            } catch (IOException e2) {
                Platform.m7184().m7193(5, "Failed to read public suffix list", (Throwable) e2);
                if (z) {
                    Thread.currentThread().interrupt();
                    return;
                }
                return;
            } catch (Throwable th) {
                if (z) {
                    Thread.currentThread().interrupt();
                }
                throw th;
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m20410() throws IOException {
        InputStream resourceAsStream = PublicSuffixDatabase.class.getResourceAsStream("publicsuffixes.gz");
        if (resourceAsStream != null) {
            BufferedSource r0 = Okio.m20507((Source) new GzipSource(Okio.m20513(resourceAsStream)));
            try {
                byte[] bArr = new byte[r0.m20463()];
                r0.m20471(bArr);
                byte[] bArr2 = new byte[r0.m20463()];
                r0.m20471(bArr2);
                synchronized (this) {
                    this.f16414 = bArr;
                    this.f16415 = bArr2;
                }
                this.f16413.countDown();
            } finally {
                Util.m7124((Closeable) r0);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m20411(byte[] bArr, byte[][] bArr2, int i) {
        byte b;
        int i2;
        int i3 = 0;
        int length = bArr.length;
        while (i3 < length) {
            int i4 = (i3 + length) / 2;
            while (i4 > -1 && bArr[i4] != 10) {
                i4--;
            }
            int i5 = i4 + 1;
            int i6 = 1;
            while (bArr[i5 + i6] != 10) {
                i6++;
            }
            int i7 = (i5 + i6) - i5;
            int i8 = i;
            int i9 = 0;
            int i10 = 0;
            boolean z = false;
            while (true) {
                if (z) {
                    b = 46;
                    z = false;
                } else {
                    b = bArr2[i8][i9] & 255;
                }
                i2 = b - (bArr[i5 + i10] & 255);
                if (i2 == 0) {
                    i10++;
                    i9++;
                    if (i10 != i7) {
                        if (bArr2[i8].length == i9) {
                            if (i8 == bArr2.length - 1) {
                                break;
                            }
                            i8++;
                            i9 = -1;
                            z = true;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            if (i2 < 0) {
                length = i5 - 1;
            } else if (i2 > 0) {
                i3 = i5 + i6 + 1;
            } else {
                int i11 = i7 - i10;
                int length2 = bArr2[i8].length - i9;
                for (int i12 = i8 + 1; i12 < bArr2.length; i12++) {
                    length2 += bArr2[i12].length;
                }
                if (length2 < i11) {
                    length = i5 - 1;
                } else if (length2 <= i11) {
                    return new String(bArr, i5, i7, Util.f6413);
                } else {
                    i3 = i5 + i6 + 1;
                }
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static PublicSuffixDatabase m20412() {
        return f16410;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String[] m20413(String[] strArr) {
        if (this.f16416.get() || !this.f16416.compareAndSet(false, true)) {
            try {
                this.f16413.await();
            } catch (InterruptedException e) {
            }
        } else {
            m20409();
        }
        synchronized (this) {
            if (this.f16414 == null) {
                throw new IllegalStateException("Unable to load publicsuffixes.gz resource from the classpath.");
            }
        }
        byte[][] bArr = new byte[strArr.length][];
        for (int i = 0; i < strArr.length; i++) {
            bArr[i] = strArr[i].getBytes(Util.f6413);
        }
        String str = null;
        int i2 = 0;
        while (true) {
            if (i2 >= bArr.length) {
                break;
            }
            String r7 = m20411(this.f16414, bArr, i2);
            if (r7 != null) {
                str = r7;
                break;
            }
            i2++;
        }
        String str2 = null;
        if (bArr.length > 1) {
            byte[][] bArr2 = (byte[][]) bArr.clone();
            int i3 = 0;
            while (true) {
                if (i3 >= bArr2.length - 1) {
                    break;
                }
                bArr2[i3] = f16412;
                String r72 = m20411(this.f16414, bArr2, i3);
                if (r72 != null) {
                    str2 = r72;
                    break;
                }
                i3++;
            }
        }
        String str3 = null;
        if (str2 != null) {
            int i4 = 0;
            while (true) {
                if (i4 >= bArr.length - 1) {
                    break;
                }
                String r73 = m20411(this.f16415, bArr, i4);
                if (r73 != null) {
                    str3 = r73;
                    break;
                }
                i4++;
            }
        }
        if (str3 != null) {
            return ("!" + str3).split("\\.");
        }
        if (str == null && str2 == null) {
            return f16411;
        }
        String[] split = str != null ? str.split("\\.") : f16409;
        String[] split2 = str2 != null ? str2.split("\\.") : f16409;
        return split.length <= split2.length ? split2 : split;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m20414(String str) {
        if (str == null) {
            throw new NullPointerException("domain == null");
        }
        String[] split = IDN.toUnicode(str).split("\\.");
        String[] r5 = m20413(split);
        if (split.length == r5.length && r5[0].charAt(0) != '!') {
            return null;
        }
        int length = r5[0].charAt(0) == '!' ? split.length - r5.length : split.length - (r5.length + 1);
        StringBuilder sb = new StringBuilder();
        String[] split2 = str.split("\\.");
        for (int i = length; i < split2.length; i++) {
            sb.append(split2[i]).append('.');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
