package okhttp3.internal;

public abstract class NamedRunnable implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    protected final String f16089;

    public NamedRunnable(String str, Object... objArr) {
        this.f16089 = Util.m7116(str, objArr);
    }

    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.f16089);
        try {
            m20014();
        } finally {
            Thread.currentThread().setName(name);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m20014();
}
