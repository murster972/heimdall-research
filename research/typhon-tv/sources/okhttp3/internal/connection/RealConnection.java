package okhttp3.internal.connection;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import okhttp3.Address;
import okhttp3.Call;
import okhttp3.Connection;
import okhttp3.ConnectionPool;
import okhttp3.EventListener;
import okhttp3.Handshake;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.Version;
import okhttp3.internal.http.HttpCodec;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http1.Http1Codec;
import okhttp3.internal.http2.ErrorCode;
import okhttp3.internal.http2.Http2Codec;
import okhttp3.internal.http2.Http2Connection;
import okhttp3.internal.http2.Http2Stream;
import okhttp3.internal.platform.Platform;
import okhttp3.internal.tls.OkHostnameVerifier;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import okio.Source;

public final class RealConnection extends Http2Connection.Listener implements Connection {

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ConnectionPool f16132;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Route f16133;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Http2Connection f16134;

    /* renamed from: ʿ  reason: contains not printable characters */
    private BufferedSource f16135;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Protocol f16136;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Socket f16137;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Socket f16138;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Handshake f16139;

    /* renamed from: 连任  reason: contains not printable characters */
    public long f16140 = Long.MAX_VALUE;

    /* renamed from: 靐  reason: contains not printable characters */
    public int f16141;

    /* renamed from: 麤  reason: contains not printable characters */
    public final List<Reference<StreamAllocation>> f16142 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    public int f16143 = 1;

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean f16144;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private BufferedSink f16145;

    public RealConnection(ConnectionPool connectionPool, Route route) {
        this.f16132 = connectionPool;
        this.f16133 = route;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private Request m20052() {
        return new Request.Builder().m19997(this.f16133.m20002().m6838()).m19993("Host", Util.m7117(this.f16133.m20002().m6838(), true)).m19993("Proxy-Connection", "Keep-Alive").m19993(AbstractSpiCall.HEADER_USER_AGENT, Version.m20016()).m19989();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Request m20053(int i, int i2, Request request, HttpUrl httpUrl) throws IOException {
        Response r4;
        String str = "CONNECT " + Util.m7117(httpUrl, true) + " HTTP/1.1";
        do {
            Http1Codec http1Codec = new Http1Codec((OkHttpClient) null, (StreamAllocation) null, this.f16135, this.f16145);
            this.f16135.m20569().m20574((long) i, TimeUnit.MILLISECONDS);
            this.f16145.m20567().m20574((long) i2, TimeUnit.MILLISECONDS);
            http1Codec.m20173(request.m7051(), str);
            http1Codec.m20164();
            r4 = http1Codec.m20167(false).m7084(request).m7087();
            long r2 = HttpHeaders.m20127(r4);
            if (r2 == -1) {
                r2 = 0;
            }
            Source r0 = http1Codec.m20163(r2);
            Util.m7103(r0, (int) MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, TimeUnit.MILLISECONDS);
            r0.close();
            switch (r4.m7066()) {
                case 200:
                    if (!this.f16135.m20466().m7210() || !this.f16145.m20447().m7210()) {
                        throw new IOException("TLS tunnel buffered too many bytes!");
                    }
                    Request request2 = request;
                    return null;
                case 407:
                    request = this.f16133.m20002().m6836().m6840(this.f16133, r4);
                    if (request != null) {
                        break;
                    } else {
                        throw new IOException("Failed to authenticate with proxy");
                    }
                default:
                    throw new IOException("Unexpected response code for CONNECT: " + r4.m7066());
            }
        } while (!"close".equalsIgnoreCase(r4.m7067("Connection")));
        Request request3 = request;
        return request;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20054(int i, int i2, int i3, Call call, EventListener eventListener) throws IOException {
        Request r1 = m20052();
        HttpUrl r2 = r1.m7053();
        int i4 = 0;
        while (i4 < 21) {
            m20055(i, i2, call, eventListener);
            r1 = m20053(i2, i3, r1, r2);
            if (r1 != null) {
                Util.m7125(this.f16137);
                this.f16137 = null;
                this.f16145 = null;
                this.f16135 = null;
                eventListener.m19930(call, this.f16133.m20001(), this.f16133.m19999(), (Protocol) null);
                i4++;
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20055(int i, int i2, Call call, EventListener eventListener) throws IOException {
        Proxy r4 = this.f16133.m19999();
        this.f16137 = (r4.type() == Proxy.Type.DIRECT || r4.type() == Proxy.Type.HTTP) ? this.f16133.m20002().m6837().createSocket() : new Socket(r4);
        eventListener.m19929(call, this.f16133.m20001(), r4);
        this.f16137.setSoTimeout(i2);
        try {
            Platform.m7184().m7195(this.f16137, this.f16133.m20001(), i);
            try {
                this.f16135 = Okio.m20507(Okio.m20503(this.f16137));
                this.f16145 = Okio.m20506(Okio.m20511(this.f16137));
            } catch (NullPointerException e) {
                if ("throw with null exception".equals(e.getMessage())) {
                    throw new IOException(e);
                }
            }
        } catch (ConnectException e2) {
            ConnectException connectException = new ConnectException("Failed to connect to " + this.f16133.m20001());
            connectException.initCause(e2);
            throw connectException;
        }
    }

    /* JADX WARNING: type inference failed for: r11v5, types: [java.net.Socket] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m20056(okhttp3.internal.connection.ConnectionSpecSelector r16) throws java.io.IOException {
        /*
            r15 = this;
            okhttp3.Route r11 = r15.f16133
            okhttp3.Address r1 = r11.m20002()
            javax.net.ssl.SSLSocketFactory r7 = r1.m6831()
            r9 = 0
            r6 = 0
            java.net.Socket r11 = r15.f16137     // Catch:{ AssertionError -> 0x005c }
            okhttp3.HttpUrl r12 = r1.m6838()     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r12 = r12.m6951()     // Catch:{ AssertionError -> 0x005c }
            okhttp3.HttpUrl r13 = r1.m6838()     // Catch:{ AssertionError -> 0x005c }
            int r13 = r13.m6952()     // Catch:{ AssertionError -> 0x005c }
            r14 = 1
            java.net.Socket r11 = r7.createSocket(r11, r12, r13, r14)     // Catch:{ AssertionError -> 0x005c }
            r0 = r11
            javax.net.ssl.SSLSocket r0 = (javax.net.ssl.SSLSocket) r0     // Catch:{ AssertionError -> 0x005c }
            r6 = r0
            r0 = r16
            okhttp3.ConnectionSpec r3 = r0.m20050((javax.net.ssl.SSLSocket) r6)     // Catch:{ AssertionError -> 0x005c }
            boolean r11 = r3.m6885()     // Catch:{ AssertionError -> 0x005c }
            if (r11 == 0) goto L_0x0046
            okhttp3.internal.platform.Platform r11 = okhttp3.internal.platform.Platform.m7184()     // Catch:{ AssertionError -> 0x005c }
            okhttp3.HttpUrl r12 = r1.m6838()     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r12 = r12.m6951()     // Catch:{ AssertionError -> 0x005c }
            java.util.List r13 = r1.m6834()     // Catch:{ AssertionError -> 0x005c }
            r11.m7196((javax.net.ssl.SSLSocket) r6, (java.lang.String) r12, (java.util.List<okhttp3.Protocol>) r13)     // Catch:{ AssertionError -> 0x005c }
        L_0x0046:
            r6.startHandshake()     // Catch:{ AssertionError -> 0x005c }
            javax.net.ssl.SSLSession r8 = r6.getSession()     // Catch:{ AssertionError -> 0x005c }
            boolean r11 = r15.m20058((javax.net.ssl.SSLSession) r8)     // Catch:{ AssertionError -> 0x005c }
            if (r11 != 0) goto L_0x0079
            java.io.IOException r11 = new java.io.IOException     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r12 = "a valid ssl session was not established"
            r11.<init>(r12)     // Catch:{ AssertionError -> 0x005c }
            throw r11     // Catch:{ AssertionError -> 0x005c }
        L_0x005c:
            r4 = move-exception
            boolean r11 = okhttp3.internal.Util.m7126((java.lang.AssertionError) r4)     // Catch:{ all -> 0x0069 }
            if (r11 == 0) goto L_0x0147
            java.io.IOException r11 = new java.io.IOException     // Catch:{ all -> 0x0069 }
            r11.<init>(r4)     // Catch:{ all -> 0x0069 }
            throw r11     // Catch:{ all -> 0x0069 }
        L_0x0069:
            r11 = move-exception
            if (r6 == 0) goto L_0x0073
            okhttp3.internal.platform.Platform r12 = okhttp3.internal.platform.Platform.m7184()
            r12.m7187((javax.net.ssl.SSLSocket) r6)
        L_0x0073:
            if (r9 != 0) goto L_0x0078
            okhttp3.internal.Util.m7125((java.net.Socket) r6)
        L_0x0078:
            throw r11
        L_0x0079:
            okhttp3.Handshake r10 = okhttp3.Handshake.m19942(r8)     // Catch:{ AssertionError -> 0x005c }
            javax.net.ssl.HostnameVerifier r11 = r1.m6832()     // Catch:{ AssertionError -> 0x005c }
            okhttp3.HttpUrl r12 = r1.m6838()     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r12 = r12.m6951()     // Catch:{ AssertionError -> 0x005c }
            boolean r11 = r11.verify(r12, r8)     // Catch:{ AssertionError -> 0x005c }
            if (r11 != 0) goto L_0x00ed
            java.util.List r11 = r10.m19946()     // Catch:{ AssertionError -> 0x005c }
            r12 = 0
            java.lang.Object r2 = r11.get(r12)     // Catch:{ AssertionError -> 0x005c }
            java.security.cert.X509Certificate r2 = (java.security.cert.X509Certificate) r2     // Catch:{ AssertionError -> 0x005c }
            javax.net.ssl.SSLPeerUnverifiedException r11 = new javax.net.ssl.SSLPeerUnverifiedException     // Catch:{ AssertionError -> 0x005c }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ AssertionError -> 0x005c }
            r12.<init>()     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r13 = "Hostname "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ AssertionError -> 0x005c }
            okhttp3.HttpUrl r13 = r1.m6838()     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r13 = r13.m6951()     // Catch:{ AssertionError -> 0x005c }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r13 = " not verified:\n    certificate: "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r13 = okhttp3.CertificatePinner.m6872((java.security.cert.Certificate) r2)     // Catch:{ AssertionError -> 0x005c }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r13 = "\n    DN: "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ AssertionError -> 0x005c }
            java.security.Principal r13 = r2.getSubjectDN()     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r13 = r13.getName()     // Catch:{ AssertionError -> 0x005c }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r13 = "\n    subjectAltNames: "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ AssertionError -> 0x005c }
            java.util.List r13 = okhttp3.internal.tls.OkHostnameVerifier.m20422(r2)     // Catch:{ AssertionError -> 0x005c }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r12 = r12.toString()     // Catch:{ AssertionError -> 0x005c }
            r11.<init>(r12)     // Catch:{ AssertionError -> 0x005c }
            throw r11     // Catch:{ AssertionError -> 0x005c }
        L_0x00ed:
            okhttp3.CertificatePinner r11 = r1.m6833()     // Catch:{ AssertionError -> 0x005c }
            okhttp3.HttpUrl r12 = r1.m6838()     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r12 = r12.m6951()     // Catch:{ AssertionError -> 0x005c }
            java.util.List r13 = r10.m19946()     // Catch:{ AssertionError -> 0x005c }
            r11.m6876(r12, r13)     // Catch:{ AssertionError -> 0x005c }
            boolean r11 = r3.m6885()     // Catch:{ AssertionError -> 0x005c }
            if (r11 == 0) goto L_0x0142
            okhttp3.internal.platform.Platform r11 = okhttp3.internal.platform.Platform.m7184()     // Catch:{ AssertionError -> 0x005c }
            java.lang.String r5 = r11.m7191((javax.net.ssl.SSLSocket) r6)     // Catch:{ AssertionError -> 0x005c }
        L_0x010e:
            r15.f16138 = r6     // Catch:{ AssertionError -> 0x005c }
            java.net.Socket r11 = r15.f16138     // Catch:{ AssertionError -> 0x005c }
            okio.Source r11 = okio.Okio.m20503((java.net.Socket) r11)     // Catch:{ AssertionError -> 0x005c }
            okio.BufferedSource r11 = okio.Okio.m20507((okio.Source) r11)     // Catch:{ AssertionError -> 0x005c }
            r15.f16135 = r11     // Catch:{ AssertionError -> 0x005c }
            java.net.Socket r11 = r15.f16138     // Catch:{ AssertionError -> 0x005c }
            okio.Sink r11 = okio.Okio.m20511((java.net.Socket) r11)     // Catch:{ AssertionError -> 0x005c }
            okio.BufferedSink r11 = okio.Okio.m20506((okio.Sink) r11)     // Catch:{ AssertionError -> 0x005c }
            r15.f16145 = r11     // Catch:{ AssertionError -> 0x005c }
            r15.f16139 = r10     // Catch:{ AssertionError -> 0x005c }
            if (r5 == 0) goto L_0x0144
            okhttp3.Protocol r11 = okhttp3.Protocol.get(r5)     // Catch:{ AssertionError -> 0x005c }
        L_0x0130:
            r15.f16136 = r11     // Catch:{ AssertionError -> 0x005c }
            r9 = 1
            if (r6 == 0) goto L_0x013c
            okhttp3.internal.platform.Platform r11 = okhttp3.internal.platform.Platform.m7184()
            r11.m7187((javax.net.ssl.SSLSocket) r6)
        L_0x013c:
            if (r9 != 0) goto L_0x0141
            okhttp3.internal.Util.m7125((java.net.Socket) r6)
        L_0x0141:
            return
        L_0x0142:
            r5 = 0
            goto L_0x010e
        L_0x0144:
            okhttp3.Protocol r11 = okhttp3.Protocol.HTTP_1_1     // Catch:{ AssertionError -> 0x005c }
            goto L_0x0130
        L_0x0147:
            throw r4     // Catch:{ all -> 0x0069 }
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.RealConnection.m20056(okhttp3.internal.connection.ConnectionSpecSelector):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20057(ConnectionSpecSelector connectionSpecSelector, int i, Call call, EventListener eventListener) throws IOException {
        if (this.f16133.m20002().m6831() == null) {
            this.f16136 = Protocol.HTTP_1_1;
            this.f16138 = this.f16137;
            return;
        }
        eventListener.m19919(call);
        m20056(connectionSpecSelector);
        eventListener.m19933(call, this.f16139);
        if (this.f16136 == Protocol.HTTP_2) {
            this.f16138.setSoTimeout(0);
            this.f16134 = new Http2Connection.Builder(true).m20261(this.f16138, this.f16133.m20002().m6838().m6951(), this.f16135, this.f16145).m20262((Http2Connection.Listener) this).m20260(i).m20263();
            this.f16134.m20237();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m20058(SSLSession sSLSession) {
        return !"NONE".equals(sSLSession.getProtocol()) && !"SSL_NULL_WITH_NULL_NULL".equals(sSLSession.getCipherSuite());
    }

    public String toString() {
        return "Connection{" + this.f16133.m20002().m6838().m6951() + ":" + this.f16133.m20002().m6838().m6952() + ", proxy=" + this.f16133.m19999() + " hostAddress=" + this.f16133.m20001() + " cipherSuite=" + (this.f16139 != null ? this.f16139.m19944() : "none") + " protocol=" + this.f16136 + '}';
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m20059() {
        return this.f16134 != null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Handshake m20060() {
        return this.f16139;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Route m20061() {
        return this.f16133;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Socket m20062() {
        return this.f16138;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m20063() {
        Util.m7125(this.f16137);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Protocol m20064() {
        return this.f16136;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpCodec m20065(OkHttpClient okHttpClient, Interceptor.Chain chain, StreamAllocation streamAllocation) throws SocketException {
        if (this.f16134 != null) {
            return new Http2Codec(okHttpClient, chain, streamAllocation, this.f16134);
        }
        this.f16138.setSoTimeout(chain.m6992());
        this.f16135.m20569().m20574((long) chain.m6992(), TimeUnit.MILLISECONDS);
        this.f16145.m20567().m20574((long) chain.m6990(), TimeUnit.MILLISECONDS);
        return new Http1Codec(okHttpClient, streamAllocation, this.f16135, this.f16145);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0094, code lost:
        if (r14.f16137 == null) goto L_0x0096;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0085 A[SYNTHETIC] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m20066(int r15, int r16, int r17, int r18, boolean r19, okhttp3.Call r20, okhttp3.EventListener r21) {
        /*
            r14 = this;
            okhttp3.Protocol r3 = r14.f16136
            if (r3 == 0) goto L_0x000d
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r4 = "already connected"
            r3.<init>(r4)
            throw r3
        L_0x000d:
            r13 = 0
            okhttp3.Route r3 = r14.f16133
            okhttp3.Address r3 = r3.m20002()
            java.util.List r10 = r3.m6828()
            okhttp3.internal.connection.ConnectionSpecSelector r9 = new okhttp3.internal.connection.ConnectionSpecSelector
            r9.<init>(r10)
            okhttp3.Route r3 = r14.f16133
            okhttp3.Address r3 = r3.m20002()
            javax.net.ssl.SSLSocketFactory r3 = r3.m6831()
            if (r3 != 0) goto L_0x007d
            okhttp3.ConnectionSpec r3 = okhttp3.ConnectionSpec.f6246
            boolean r3 = r10.contains(r3)
            if (r3 != 0) goto L_0x003f
            okhttp3.internal.connection.RouteException r3 = new okhttp3.internal.connection.RouteException
            java.net.UnknownServiceException r4 = new java.net.UnknownServiceException
            java.lang.String r5 = "CLEARTEXT communication not enabled for client"
            r4.<init>(r5)
            r3.<init>(r4)
            throw r3
        L_0x003f:
            okhttp3.Route r3 = r14.f16133
            okhttp3.Address r3 = r3.m20002()
            okhttp3.HttpUrl r3 = r3.m6838()
            java.lang.String r12 = r3.m6951()
            okhttp3.internal.platform.Platform r3 = okhttp3.internal.platform.Platform.m7184()
            boolean r3 = r3.m7188((java.lang.String) r12)
            if (r3 != 0) goto L_0x007d
            okhttp3.internal.connection.RouteException r3 = new okhttp3.internal.connection.RouteException
            java.net.UnknownServiceException r4 = new java.net.UnknownServiceException
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "CLEARTEXT communication to "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r12)
            java.lang.String r6 = " not permitted by network security policy"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.<init>(r5)
            r3.<init>(r4)
            throw r3
        L_0x007d:
            okhttp3.Route r3 = r14.f16133     // Catch:{ IOException -> 0x00d8 }
            boolean r3 = r3.m20000()     // Catch:{ IOException -> 0x00d8 }
            if (r3 == 0) goto L_0x00b0
            r3 = r14
            r4 = r15
            r5 = r16
            r6 = r17
            r7 = r20
            r8 = r21
            r3.m20054(r4, r5, r6, r7, r8)     // Catch:{ IOException -> 0x00d8 }
            java.net.Socket r3 = r14.f16137     // Catch:{ IOException -> 0x00d8 }
            if (r3 != 0) goto L_0x00b9
        L_0x0096:
            okhttp3.Route r3 = r14.f16133
            boolean r3 = r3.m20000()
            if (r3 == 0) goto L_0x0120
            java.net.Socket r3 = r14.f16137
            if (r3 != 0) goto L_0x0120
            java.net.ProtocolException r11 = new java.net.ProtocolException
            java.lang.String r3 = "Too many tunnel connections attempted: 21"
            r11.<init>(r3)
            okhttp3.internal.connection.RouteException r3 = new okhttp3.internal.connection.RouteException
            r3.<init>(r11)
            throw r3
        L_0x00b0:
            r0 = r16
            r1 = r20
            r2 = r21
            r14.m20055((int) r15, (int) r0, (okhttp3.Call) r1, (okhttp3.EventListener) r2)     // Catch:{ IOException -> 0x00d8 }
        L_0x00b9:
            r0 = r18
            r1 = r20
            r2 = r21
            r14.m20057((okhttp3.internal.connection.ConnectionSpecSelector) r9, (int) r0, (okhttp3.Call) r1, (okhttp3.EventListener) r2)     // Catch:{ IOException -> 0x00d8 }
            okhttp3.Route r3 = r14.f16133     // Catch:{ IOException -> 0x00d8 }
            java.net.InetSocketAddress r3 = r3.m20001()     // Catch:{ IOException -> 0x00d8 }
            okhttp3.Route r4 = r14.f16133     // Catch:{ IOException -> 0x00d8 }
            java.net.Proxy r4 = r4.m19999()     // Catch:{ IOException -> 0x00d8 }
            okhttp3.Protocol r5 = r14.f16136     // Catch:{ IOException -> 0x00d8 }
            r0 = r21
            r1 = r20
            r0.m19930(r1, r3, r4, r5)     // Catch:{ IOException -> 0x00d8 }
            goto L_0x0096
        L_0x00d8:
            r8 = move-exception
            java.net.Socket r3 = r14.f16138
            okhttp3.internal.Util.m7125((java.net.Socket) r3)
            java.net.Socket r3 = r14.f16137
            okhttp3.internal.Util.m7125((java.net.Socket) r3)
            r3 = 0
            r14.f16138 = r3
            r3 = 0
            r14.f16137 = r3
            r3 = 0
            r14.f16135 = r3
            r3 = 0
            r14.f16145 = r3
            r3 = 0
            r14.f16139 = r3
            r3 = 0
            r14.f16136 = r3
            r3 = 0
            r14.f16134 = r3
            okhttp3.Route r3 = r14.f16133
            java.net.InetSocketAddress r5 = r3.m20001()
            okhttp3.Route r3 = r14.f16133
            java.net.Proxy r6 = r3.m19999()
            r7 = 0
            r3 = r21
            r4 = r20
            r3.m19931(r4, r5, r6, r7, r8)
            if (r13 != 0) goto L_0x011c
            okhttp3.internal.connection.RouteException r13 = new okhttp3.internal.connection.RouteException
            r13.<init>(r8)
        L_0x0113:
            if (r19 == 0) goto L_0x011b
            boolean r3 = r9.m20051((java.io.IOException) r8)
            if (r3 != 0) goto L_0x007d
        L_0x011b:
            throw r13
        L_0x011c:
            r13.addConnectException(r8)
            goto L_0x0113
        L_0x0120:
            okhttp3.internal.http2.Http2Connection r3 = r14.f16134
            if (r3 == 0) goto L_0x0130
            okhttp3.ConnectionPool r4 = r14.f16132
            monitor-enter(r4)
            okhttp3.internal.http2.Http2Connection r3 = r14.f16134     // Catch:{ all -> 0x0131 }
            int r3 = r3.m20240()     // Catch:{ all -> 0x0131 }
            r14.f16143 = r3     // Catch:{ all -> 0x0131 }
            monitor-exit(r4)     // Catch:{ all -> 0x0131 }
        L_0x0130:
            return
        L_0x0131:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0131 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.RealConnection.m20066(int, int, int, int, boolean, okhttp3.Call, okhttp3.EventListener):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20067(Http2Connection http2Connection) {
        synchronized (this.f16132) {
            this.f16143 = http2Connection.m20240();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20068(Http2Stream http2Stream) throws IOException {
        http2Stream.m20328(ErrorCode.REFUSED_STREAM);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20069(Address address, @Nullable Route route) {
        if (this.f16142.size() >= this.f16143 || this.f16144 || !Internal.f16088.m20012(this.f16133.m20002(), address)) {
            return false;
        }
        if (address.m6838().m6951().equals(m20061().m20002().m6838().m6951())) {
            return true;
        }
        if (this.f16134 == null || route == null || route.m19999().type() != Proxy.Type.DIRECT || this.f16133.m19999().type() != Proxy.Type.DIRECT || !this.f16133.m20001().equals(route.m20001()) || route.m20002().m6832() != OkHostnameVerifier.f16419 || !m20070(address.m6838())) {
            return false;
        }
        try {
            address.m6833().m6876(address.m6838().m6951(), m20060().m19946());
            return true;
        } catch (SSLPeerUnverifiedException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20070(HttpUrl httpUrl) {
        if (httpUrl.m6952() != this.f16133.m20002().m6838().m6952()) {
            return false;
        }
        if (httpUrl.m6951().equals(this.f16133.m20002().m6838().m6951())) {
            return true;
        }
        return this.f16139 != null && OkHostnameVerifier.f16419.m20425(httpUrl.m6951(), (X509Certificate) this.f16139.m19946().get(0));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20071(boolean z) {
        int soTimeout;
        if (this.f16138.isClosed() || this.f16138.isInputShutdown() || this.f16138.isOutputShutdown()) {
            return false;
        }
        if (this.f16134 != null) {
            return !this.f16134.m20236();
        }
        if (!z) {
            return true;
        }
        try {
            soTimeout = this.f16138.getSoTimeout();
            this.f16138.setSoTimeout(1);
            if (this.f16135.m20452()) {
                this.f16138.setSoTimeout(soTimeout);
                return false;
            }
            this.f16138.setSoTimeout(soTimeout);
            return true;
        } catch (SocketTimeoutException e) {
            return true;
        } catch (IOException e2) {
            return false;
        } catch (Throwable th) {
            this.f16138.setSoTimeout(soTimeout);
            throw th;
        }
    }
}
