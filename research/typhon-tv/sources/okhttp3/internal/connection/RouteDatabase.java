package okhttp3.internal.connection;

import java.util.LinkedHashSet;
import java.util.Set;
import okhttp3.Route;

public final class RouteDatabase {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Set<Route> f16146 = new LinkedHashSet();

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m20072(Route route) {
        this.f16146.remove(route);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized boolean m20073(Route route) {
        return this.f16146.contains(route);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20074(Route route) {
        this.f16146.add(route);
    }
}
