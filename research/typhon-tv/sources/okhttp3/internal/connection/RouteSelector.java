package okhttp3.internal.connection;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import okhttp3.Address;
import okhttp3.Call;
import okhttp3.EventListener;
import okhttp3.HttpUrl;
import okhttp3.Route;
import okhttp3.internal.Util;

public final class RouteSelector {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f16148;

    /* renamed from: ʼ  reason: contains not printable characters */
    private List<InetSocketAddress> f16149 = Collections.emptyList();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final List<Route> f16150 = new ArrayList();

    /* renamed from: 连任  reason: contains not printable characters */
    private List<Proxy> f16151 = Collections.emptyList();

    /* renamed from: 靐  reason: contains not printable characters */
    private final RouteDatabase f16152;

    /* renamed from: 麤  reason: contains not printable characters */
    private final EventListener f16153;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Call f16154;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Address f16155;

    public static final class Selection {

        /* renamed from: 靐  reason: contains not printable characters */
        private int f16156 = 0;

        /* renamed from: 龘  reason: contains not printable characters */
        private final List<Route> f16157;

        Selection(List<Route> list) {
            this.f16157 = list;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Route m20084() {
            if (!m20086()) {
                throw new NoSuchElementException();
            }
            List<Route> list = this.f16157;
            int i = this.f16156;
            this.f16156 = i + 1;
            return list.get(i);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public List<Route> m20085() {
            return new ArrayList(this.f16157);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20086() {
            return this.f16156 < this.f16157.size();
        }
    }

    public RouteSelector(Address address, RouteDatabase routeDatabase, Call call, EventListener eventListener) {
        this.f16155 = address;
        this.f16152 = routeDatabase;
        this.f16154 = call;
        this.f16153 = eventListener;
        m20080(address.m6838(), address.m6830());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private Proxy m20076() throws IOException {
        if (!m20077()) {
            throw new SocketException("No route to " + this.f16155.m6838().m6951() + "; exhausted proxy configurations: " + this.f16151);
        }
        List<Proxy> list = this.f16151;
        int i = this.f16148;
        this.f16148 = i + 1;
        Proxy proxy = list.get(i);
        m20079(proxy);
        return proxy;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m20077() {
        return this.f16148 < this.f16151.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m20078(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        return address == null ? inetSocketAddress.getHostName() : address.getHostAddress();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20079(Proxy proxy) throws IOException {
        int i;
        String str;
        this.f16149 = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            str = this.f16155.m6838().m6951();
            i = this.f16155.m6838().m6952();
        } else {
            SocketAddress address = proxy.address();
            if (!(address instanceof InetSocketAddress)) {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
            InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
            str = m20078(inetSocketAddress);
            i = inetSocketAddress.getPort();
        }
        if (i < 1 || i > 65535) {
            throw new SocketException("No route to " + str + ":" + i + "; port is out of range");
        } else if (proxy.type() == Proxy.Type.SOCKS) {
            this.f16149.add(InetSocketAddress.createUnresolved(str, i));
        } else {
            this.f16153.m19927(this.f16154, str);
            List<InetAddress> r0 = this.f16155.m6835().m19913(str);
            if (r0.isEmpty()) {
                throw new UnknownHostException(this.f16155.m6835() + " returned no addresses for " + str);
            }
            this.f16153.m19928(this.f16154, str, r0);
            int size = r0.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f16149.add(new InetSocketAddress(r0.get(i2), i));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20080(HttpUrl httpUrl, Proxy proxy) {
        List<Proxy> r1;
        if (proxy != null) {
            this.f16151 = Collections.singletonList(proxy);
        } else {
            List<Proxy> select = this.f16155.m6829().select(httpUrl.m6966());
            if (select == null || select.isEmpty()) {
                r1 = Util.m7121((T[]) new Proxy[]{Proxy.NO_PROXY});
            } else {
                r1 = Util.m7120(select);
            }
            this.f16151 = r1;
        }
        this.f16148 = 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Selection m20081() throws IOException {
        if (!m20083()) {
            throw new NoSuchElementException();
        }
        ArrayList arrayList = new ArrayList();
        while (m20077()) {
            Proxy r1 = m20076();
            int size = this.f16149.size();
            for (int i = 0; i < size; i++) {
                Route route = new Route(this.f16155, r1, this.f16149.get(i));
                if (this.f16152.m20073(route)) {
                    this.f16150.add(route);
                } else {
                    arrayList.add(route);
                }
            }
            if (!arrayList.isEmpty()) {
                break;
            }
        }
        if (arrayList.isEmpty()) {
            arrayList.addAll(this.f16150);
            this.f16150.clear();
        }
        return new Selection(arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20082(Route route, IOException iOException) {
        if (!(route.m19999().type() == Proxy.Type.DIRECT || this.f16155.m6829() == null)) {
            this.f16155.m6829().connectFailed(this.f16155.m6838().m6966(), route.m19999().address(), iOException);
        }
        this.f16152.m20074(route);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20083() {
        return m20077() || !this.f16150.isEmpty();
    }
}
