package okhttp3.internal.connection;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.http.RealInterceptorChain;
import org.apache.oltu.oauth2.common.OAuth;

public final class ConnectInterceptor implements Interceptor {

    /* renamed from: 龘  reason: contains not printable characters */
    public final OkHttpClient f16127;

    public ConnectInterceptor(OkHttpClient okHttpClient) {
        this.f16127 = okHttpClient;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        RealInterceptorChain realInterceptorChain = (RealInterceptorChain) chain;
        Request r4 = realInterceptorChain.m20144();
        StreamAllocation r5 = realInterceptorChain.m20136();
        return realInterceptorChain.m20146(r4, r5, r5.m20101(this.f16127, chain, !r4.m7048().equals(OAuth.HttpMethod.GET)), r5.m20098());
    }
}
