package okhttp3.internal.connection;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import okhttp3.Address;
import okhttp3.Call;
import okhttp3.Connection;
import okhttp3.ConnectionPool;
import okhttp3.EventListener;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Route;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.connection.RouteSelector;
import okhttp3.internal.http.HttpCodec;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.internal.http2.ErrorCode;
import okhttp3.internal.http2.StreamResetException;

public final class StreamAllocation {

    /* renamed from: 麤  reason: contains not printable characters */
    static final /* synthetic */ boolean f16158 = (!StreamAllocation.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    private Route f16159;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ConnectionPool f16160;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Object f16161;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f16162;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f16163;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f16164;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final RouteSelector f16165;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f16166;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private RealConnection f16167;

    /* renamed from: 连任  reason: contains not printable characters */
    private RouteSelector.Selection f16168;

    /* renamed from: 靐  reason: contains not printable characters */
    public final Call f16169;

    /* renamed from: 齉  reason: contains not printable characters */
    public final EventListener f16170;

    /* renamed from: 龘  reason: contains not printable characters */
    public final Address f16171;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private HttpCodec f16172;

    public static final class StreamAllocationReference extends WeakReference<StreamAllocation> {

        /* renamed from: 龘  reason: contains not printable characters */
        public final Object f16173;

        StreamAllocationReference(StreamAllocation streamAllocation, Object obj) {
            super(streamAllocation);
            this.f16173 = obj;
        }
    }

    public StreamAllocation(ConnectionPool connectionPool, Address address, Call call, EventListener eventListener, Object obj) {
        this.f16160 = connectionPool;
        this.f16171 = address;
        this.f16169 = call;
        this.f16170 = eventListener;
        this.f16165 = new RouteSelector(address, m20088(), call, eventListener);
        this.f16161 = obj;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private Socket m20087() {
        if (f16158 || Thread.holdsLock(this.f16160)) {
            RealConnection realConnection = this.f16167;
            if (realConnection == null || !realConnection.f16144) {
                return null;
            }
            return m20090(false, false, true);
        }
        throw new AssertionError();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private RouteDatabase m20088() {
        return Internal.f16088.m20008(this.f16160);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m20089(RealConnection realConnection) {
        int size = realConnection.f16142.size();
        for (int i = 0; i < size; i++) {
            if (realConnection.f16142.get(i).get() == this) {
                realConnection.f16142.remove(i);
                return;
            }
        }
        throw new IllegalStateException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Socket m20090(boolean z, boolean z2, boolean z3) {
        if (f16158 || Thread.holdsLock(this.f16160)) {
            if (z3) {
                this.f16172 = null;
            }
            if (z2) {
                this.f16162 = true;
            }
            Socket socket = null;
            if (this.f16167 != null) {
                if (z) {
                    this.f16167.f16144 = true;
                }
                if (this.f16172 == null && (this.f16162 || this.f16167.f16144)) {
                    m20089(this.f16167);
                    if (this.f16167.f16142.isEmpty()) {
                        this.f16167.f16140 = System.nanoTime();
                        if (Internal.f16088.m20013(this.f16160, this.f16167)) {
                            socket = this.f16167.m20062();
                        }
                    }
                    this.f16167 = null;
                }
            }
            return socket;
        }
        throw new AssertionError();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0138, code lost:
        if (r10 == false) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x013a, code lost:
        r22.f16170.m19932(r22.f16169, (okhttp3.Connection) r2);
        r14 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x014c, code lost:
        r2.m20066(r23, r24, r25, r26, r27, r22.f16169, r22.f16170);
        m20088().m20072(r2.m20061());
        r20 = null;
        r4 = r22.f16160;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0172, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r22.f16164 = true;
        okhttp3.internal.Internal.f16088.m20004(r22.f16160, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0185, code lost:
        if (r2.m20059() == false) goto L_0x019b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0187, code lost:
        r20 = okhttp3.internal.Internal.f16088.m20006(r22.f16160, r22.f16171, r22);
        r2 = r22.f16167;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x019b, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x019c, code lost:
        okhttp3.internal.Util.m7125(r20);
        r22.f16170.m19932(r22.f16169, (okhttp3.Connection) r2);
        r14 = r2;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private okhttp3.internal.connection.RealConnection m20091(int r23, int r24, int r25, int r26, boolean r27) throws java.io.IOException {
        /*
            r22 = this;
            r10 = 0
            r2 = 0
            r18 = 0
            r0 = r22
            okhttp3.ConnectionPool r4 = r0.f16160
            monitor-enter(r4)
            r0 = r22
            boolean r3 = r0.f16162     // Catch:{ all -> 0x0018 }
            if (r3 == 0) goto L_0x001b
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0018 }
            java.lang.String r5 = "released"
            r3.<init>(r5)     // Catch:{ all -> 0x0018 }
            throw r3     // Catch:{ all -> 0x0018 }
        L_0x0018:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0018 }
            throw r3
        L_0x001b:
            r0 = r22
            okhttp3.internal.http.HttpCodec r3 = r0.f16172     // Catch:{ all -> 0x0018 }
            if (r3 == 0) goto L_0x002a
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0018 }
            java.lang.String r5 = "codec != null"
            r3.<init>(r5)     // Catch:{ all -> 0x0018 }
            throw r3     // Catch:{ all -> 0x0018 }
        L_0x002a:
            r0 = r22
            boolean r3 = r0.f16163     // Catch:{ all -> 0x0018 }
            if (r3 == 0) goto L_0x0039
            java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x0018 }
            java.lang.String r5 = "Canceled"
            r3.<init>(r5)     // Catch:{ all -> 0x0018 }
            throw r3     // Catch:{ all -> 0x0018 }
        L_0x0039:
            r0 = r22
            okhttp3.internal.connection.RealConnection r13 = r0.f16167     // Catch:{ all -> 0x0018 }
            java.net.Socket r21 = r22.m20087()     // Catch:{ all -> 0x0018 }
            r0 = r22
            okhttp3.internal.connection.RealConnection r3 = r0.f16167     // Catch:{ all -> 0x0018 }
            if (r3 == 0) goto L_0x004c
            r0 = r22
            okhttp3.internal.connection.RealConnection r2 = r0.f16167     // Catch:{ all -> 0x0018 }
            r13 = 0
        L_0x004c:
            r0 = r22
            boolean r3 = r0.f16164     // Catch:{ all -> 0x0018 }
            if (r3 != 0) goto L_0x0053
            r13 = 0
        L_0x0053:
            if (r2 != 0) goto L_0x0070
            okhttp3.internal.Internal r3 = okhttp3.internal.Internal.f16088     // Catch:{ all -> 0x0018 }
            r0 = r22
            okhttp3.ConnectionPool r5 = r0.f16160     // Catch:{ all -> 0x0018 }
            r0 = r22
            okhttp3.Address r6 = r0.f16171     // Catch:{ all -> 0x0018 }
            r7 = 0
            r0 = r22
            r3.m20007(r5, r6, r0, r7)     // Catch:{ all -> 0x0018 }
            r0 = r22
            okhttp3.internal.connection.RealConnection r3 = r0.f16167     // Catch:{ all -> 0x0018 }
            if (r3 == 0) goto L_0x0093
            r10 = 1
            r0 = r22
            okhttp3.internal.connection.RealConnection r2 = r0.f16167     // Catch:{ all -> 0x0018 }
        L_0x0070:
            monitor-exit(r4)     // Catch:{ all -> 0x0018 }
            okhttp3.internal.Util.m7125((java.net.Socket) r21)
            if (r13 == 0) goto L_0x0081
            r0 = r22
            okhttp3.EventListener r3 = r0.f16170
            r0 = r22
            okhttp3.Call r4 = r0.f16169
            r3.m19921((okhttp3.Call) r4, (okhttp3.Connection) r13)
        L_0x0081:
            if (r10 == 0) goto L_0x008e
            r0 = r22
            okhttp3.EventListener r3 = r0.f16170
            r0 = r22
            okhttp3.Call r4 = r0.f16169
            r3.m19932((okhttp3.Call) r4, (okhttp3.Connection) r2)
        L_0x008e:
            if (r2 == 0) goto L_0x009a
            r14 = r2
            r15 = r2
        L_0x0092:
            return r15
        L_0x0093:
            r0 = r22
            okhttp3.Route r0 = r0.f16159     // Catch:{ all -> 0x0018 }
            r18 = r0
            goto L_0x0070
        L_0x009a:
            r12 = 0
            if (r18 != 0) goto L_0x00ba
            r0 = r22
            okhttp3.internal.connection.RouteSelector$Selection r3 = r0.f16168
            if (r3 == 0) goto L_0x00ad
            r0 = r22
            okhttp3.internal.connection.RouteSelector$Selection r3 = r0.f16168
            boolean r3 = r3.m20086()
            if (r3 != 0) goto L_0x00ba
        L_0x00ad:
            r12 = 1
            r0 = r22
            okhttp3.internal.connection.RouteSelector r3 = r0.f16165
            okhttp3.internal.connection.RouteSelector$Selection r3 = r3.m20081()
            r0 = r22
            r0.f16168 = r3
        L_0x00ba:
            r0 = r22
            okhttp3.ConnectionPool r4 = r0.f16160
            monitor-enter(r4)
            r0 = r22
            boolean r3 = r0.f16163     // Catch:{ all -> 0x00ce }
            if (r3 == 0) goto L_0x00d1
            java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x00ce }
            java.lang.String r5 = "Canceled"
            r3.<init>(r5)     // Catch:{ all -> 0x00ce }
            throw r3     // Catch:{ all -> 0x00ce }
        L_0x00ce:
            r3 = move-exception
        L_0x00cf:
            monitor-exit(r4)     // Catch:{ all -> 0x00ce }
            throw r3
        L_0x00d1:
            if (r12 == 0) goto L_0x01b7
            r0 = r22
            okhttp3.internal.connection.RouteSelector$Selection r3 = r0.f16168     // Catch:{ all -> 0x00ce }
            java.util.List r17 = r3.m20085()     // Catch:{ all -> 0x00ce }
            r11 = 0
            int r19 = r17.size()     // Catch:{ all -> 0x00ce }
        L_0x00e0:
            r0 = r19
            if (r11 >= r0) goto L_0x01b7
            r0 = r17
            java.lang.Object r16 = r0.get(r11)     // Catch:{ all -> 0x00ce }
            okhttp3.Route r16 = (okhttp3.Route) r16     // Catch:{ all -> 0x00ce }
            okhttp3.internal.Internal r3 = okhttp3.internal.Internal.f16088     // Catch:{ all -> 0x00ce }
            r0 = r22
            okhttp3.ConnectionPool r5 = r0.f16160     // Catch:{ all -> 0x00ce }
            r0 = r22
            okhttp3.Address r6 = r0.f16171     // Catch:{ all -> 0x00ce }
            r0 = r22
            r1 = r16
            r3.m20007(r5, r6, r0, r1)     // Catch:{ all -> 0x00ce }
            r0 = r22
            okhttp3.internal.connection.RealConnection r3 = r0.f16167     // Catch:{ all -> 0x00ce }
            if (r3 == 0) goto L_0x0149
            r10 = 1
            r0 = r22
            okhttp3.internal.connection.RealConnection r2 = r0.f16167     // Catch:{ all -> 0x00ce }
            r0 = r16
            r1 = r22
            r1.f16159 = r0     // Catch:{ all -> 0x00ce }
            r14 = r2
        L_0x010f:
            if (r10 != 0) goto L_0x01b5
            if (r18 != 0) goto L_0x011b
            r0 = r22
            okhttp3.internal.connection.RouteSelector$Selection r3 = r0.f16168     // Catch:{ all -> 0x01b1 }
            okhttp3.Route r18 = r3.m20084()     // Catch:{ all -> 0x01b1 }
        L_0x011b:
            r0 = r18
            r1 = r22
            r1.f16159 = r0     // Catch:{ all -> 0x01b1 }
            r3 = 0
            r0 = r22
            r0.f16166 = r3     // Catch:{ all -> 0x01b1 }
            okhttp3.internal.connection.RealConnection r2 = new okhttp3.internal.connection.RealConnection     // Catch:{ all -> 0x01b1 }
            r0 = r22
            okhttp3.ConnectionPool r3 = r0.f16160     // Catch:{ all -> 0x01b1 }
            r0 = r18
            r2.<init>(r3, r0)     // Catch:{ all -> 0x01b1 }
            r3 = 0
            r0 = r22
            r0.m20103(r2, r3)     // Catch:{ all -> 0x00ce }
        L_0x0137:
            monitor-exit(r4)     // Catch:{ all -> 0x00ce }
            if (r10 == 0) goto L_0x014c
            r0 = r22
            okhttp3.EventListener r3 = r0.f16170
            r0 = r22
            okhttp3.Call r4 = r0.f16169
            r3.m19932((okhttp3.Call) r4, (okhttp3.Connection) r2)
            r14 = r2
            r15 = r2
            goto L_0x0092
        L_0x0149:
            int r11 = r11 + 1
            goto L_0x00e0
        L_0x014c:
            r0 = r22
            okhttp3.Call r8 = r0.f16169
            r0 = r22
            okhttp3.EventListener r9 = r0.f16170
            r3 = r23
            r4 = r24
            r5 = r25
            r6 = r26
            r7 = r27
            r2.m20066(r3, r4, r5, r6, r7, r8, r9)
            okhttp3.internal.connection.RouteDatabase r3 = r22.m20088()
            okhttp3.Route r4 = r2.m20061()
            r3.m20072(r4)
            r20 = 0
            r0 = r22
            okhttp3.ConnectionPool r4 = r0.f16160
            monitor-enter(r4)
            r3 = 1
            r0 = r22
            r0.f16164 = r3     // Catch:{ all -> 0x01ae }
            okhttp3.internal.Internal r3 = okhttp3.internal.Internal.f16088     // Catch:{ all -> 0x01ae }
            r0 = r22
            okhttp3.ConnectionPool r5 = r0.f16160     // Catch:{ all -> 0x01ae }
            r3.m20004(r5, r2)     // Catch:{ all -> 0x01ae }
            boolean r3 = r2.m20059()     // Catch:{ all -> 0x01ae }
            if (r3 == 0) goto L_0x019b
            okhttp3.internal.Internal r3 = okhttp3.internal.Internal.f16088     // Catch:{ all -> 0x01ae }
            r0 = r22
            okhttp3.ConnectionPool r5 = r0.f16160     // Catch:{ all -> 0x01ae }
            r0 = r22
            okhttp3.Address r6 = r0.f16171     // Catch:{ all -> 0x01ae }
            r0 = r22
            java.net.Socket r20 = r3.m20006((okhttp3.ConnectionPool) r5, (okhttp3.Address) r6, (okhttp3.internal.connection.StreamAllocation) r0)     // Catch:{ all -> 0x01ae }
            r0 = r22
            okhttp3.internal.connection.RealConnection r2 = r0.f16167     // Catch:{ all -> 0x01ae }
        L_0x019b:
            monitor-exit(r4)     // Catch:{ all -> 0x01ae }
            okhttp3.internal.Util.m7125((java.net.Socket) r20)
            r0 = r22
            okhttp3.EventListener r3 = r0.f16170
            r0 = r22
            okhttp3.Call r4 = r0.f16169
            r3.m19932((okhttp3.Call) r4, (okhttp3.Connection) r2)
            r14 = r2
            r15 = r2
            goto L_0x0092
        L_0x01ae:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x01ae }
            throw r3
        L_0x01b1:
            r3 = move-exception
            r2 = r14
            goto L_0x00cf
        L_0x01b5:
            r2 = r14
            goto L_0x0137
        L_0x01b7:
            r14 = r2
            goto L_0x010f
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.StreamAllocation.m20091(int, int, int, int, boolean):okhttp3.internal.connection.RealConnection");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private RealConnection m20092(int i, int i2, int i3, int i4, boolean z, boolean z2) throws IOException {
        RealConnection r0;
        while (true) {
            r0 = m20091(i, i2, i3, i4, z);
            synchronized (this.f16160) {
                if (r0.f16141 != 0) {
                    if (r0.m20071(z2)) {
                        break;
                    }
                    m20095();
                } else {
                    break;
                }
            }
        }
        return r0;
    }

    public String toString() {
        RealConnection r0 = m20098();
        return r0 != null ? r0.toString() : this.f16171.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m20093() {
        HttpCodec httpCodec;
        RealConnection realConnection;
        synchronized (this.f16160) {
            this.f16163 = true;
            httpCodec = this.f16172;
            realConnection = this.f16167;
        }
        if (httpCodec != null) {
            httpCodec.m20107();
        } else if (realConnection != null) {
            realConnection.m20063();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m20094() {
        return this.f16159 != null || (this.f16168 != null && this.f16168.m20086()) || this.f16165.m20083();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m20095() {
        RealConnection realConnection;
        Socket r1;
        synchronized (this.f16160) {
            realConnection = this.f16167;
            r1 = m20090(true, false, false);
            if (this.f16167 != null) {
                realConnection = null;
            }
        }
        Util.m7125(r1);
        if (realConnection != null) {
            this.f16170.m19921(this.f16169, (Connection) realConnection);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Route m20096() {
        return this.f16159;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m20097() {
        RealConnection realConnection;
        Socket r1;
        synchronized (this.f16160) {
            realConnection = this.f16167;
            r1 = m20090(false, true, false);
            if (this.f16167 != null) {
                realConnection = null;
            }
        }
        Util.m7125(r1);
        if (realConnection != null) {
            this.f16170.m19921(this.f16169, (Connection) realConnection);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized RealConnection m20098() {
        return this.f16167;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Socket m20099(RealConnection realConnection) {
        if (!f16158 && !Thread.holdsLock(this.f16160)) {
            throw new AssertionError();
        } else if (this.f16172 == null && this.f16167.f16142.size() == 1) {
            Socket r1 = m20090(true, false, false);
            this.f16167 = realConnection;
            realConnection.f16142.add(this.f16167.f16142.get(0));
            return r1;
        } else {
            throw new IllegalStateException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpCodec m20100() {
        HttpCodec httpCodec;
        synchronized (this.f16160) {
            httpCodec = this.f16172;
        }
        return httpCodec;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpCodec m20101(OkHttpClient okHttpClient, Interceptor.Chain chain, boolean z) {
        try {
            HttpCodec r8 = m20092(chain.m6993(), chain.m6992(), chain.m6990(), okHttpClient.m7024(), okHttpClient.m7013(), z).m20065(okHttpClient, chain, this);
            synchronized (this.f16160) {
                this.f16172 = r8;
            }
            return r8;
        } catch (IOException e) {
            throw new RouteException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20102(IOException iOException) {
        RealConnection realConnection;
        Socket r3;
        boolean z = false;
        synchronized (this.f16160) {
            if (iOException instanceof StreamResetException) {
                StreamResetException streamResetException = (StreamResetException) iOException;
                if (streamResetException.errorCode == ErrorCode.REFUSED_STREAM) {
                    this.f16166++;
                }
                if (streamResetException.errorCode != ErrorCode.REFUSED_STREAM || this.f16166 > 1) {
                    z = true;
                    this.f16159 = null;
                }
            } else if (this.f16167 != null && (!this.f16167.m20059() || (iOException instanceof ConnectionShutdownException))) {
                z = true;
                if (this.f16167.f16141 == 0) {
                    if (!(this.f16159 == null || iOException == null)) {
                        this.f16165.m20082(this.f16159, iOException);
                    }
                    this.f16159 = null;
                }
            }
            realConnection = this.f16167;
            r3 = m20090(z, false, true);
            if (this.f16167 != null || !this.f16164) {
                realConnection = null;
            }
        }
        Util.m7125(r3);
        if (realConnection != null) {
            this.f16170.m19921(this.f16169, (Connection) realConnection);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20103(RealConnection realConnection, boolean z) {
        if (!f16158 && !Thread.holdsLock(this.f16160)) {
            throw new AssertionError();
        } else if (this.f16167 != null) {
            throw new IllegalStateException();
        } else {
            this.f16167 = realConnection;
            this.f16164 = z;
            realConnection.f16142.add(new StreamAllocationReference(this, this.f16161));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20104(boolean z, HttpCodec httpCodec, long j, IOException iOException) {
        RealConnection realConnection;
        Socket r2;
        boolean z2;
        this.f16170.m19920(this.f16169, j);
        synchronized (this.f16160) {
            if (httpCodec != null) {
                if (httpCodec == this.f16172) {
                    if (!z) {
                        this.f16167.f16141++;
                    }
                    realConnection = this.f16167;
                    r2 = m20090(z, false, true);
                    if (this.f16167 != null) {
                        realConnection = null;
                    }
                    z2 = this.f16162;
                }
            }
            throw new IllegalStateException("expected " + this.f16172 + " but was " + httpCodec);
        }
        Util.m7125(r2);
        if (realConnection != null) {
            this.f16170.m19921(this.f16169, (Connection) realConnection);
        }
        if (iOException != null) {
            this.f16170.m19926(this.f16169, iOException);
        } else if (z2) {
            this.f16170.m19917(this.f16169);
        }
    }
}
