package okhttp3.internal.connection;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;
import okhttp3.ConnectionSpec;
import okhttp3.internal.Internal;

public final class ConnectionSpecSelector {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f16128 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f16129;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f16130;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<ConnectionSpec> f16131;

    public ConnectionSpecSelector(List<ConnectionSpec> list) {
        this.f16131 = list;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m20049(SSLSocket sSLSocket) {
        for (int i = this.f16128; i < this.f16131.size(); i++) {
            if (this.f16131.get(i).m6889(sSLSocket)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ConnectionSpec m20050(SSLSocket sSLSocket) throws IOException {
        ConnectionSpec connectionSpec = null;
        int i = this.f16128;
        int size = this.f16131.size();
        while (true) {
            if (i >= size) {
                break;
            }
            ConnectionSpec connectionSpec2 = this.f16131.get(i);
            if (connectionSpec2.m6889(sSLSocket)) {
                connectionSpec = connectionSpec2;
                this.f16128 = i + 1;
                break;
            }
            i++;
        }
        if (connectionSpec == null) {
            throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.f16129 + ", modes=" + this.f16131 + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
        }
        this.f16130 = m20049(sSLSocket);
        Internal.f16088.m20009(connectionSpec, sSLSocket, this.f16129);
        return connectionSpec;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20051(IOException iOException) {
        this.f16129 = true;
        if (!this.f16130 || (iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return (iOException instanceof SSLHandshakeException) || (iOException instanceof SSLProtocolException);
        }
        return false;
    }
}
