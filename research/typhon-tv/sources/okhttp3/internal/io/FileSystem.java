package okhttp3.internal.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import okio.Okio;
import okio.Sink;
import okio.Source;

public interface FileSystem {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final FileSystem f16395 = new FileSystem() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public long m20389(File file) {
            return file.length();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m20390(File file) throws IOException {
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                throw new IOException("not a readable directory: " + file);
            }
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    m20390(file2);
                }
                if (!file2.delete()) {
                    throw new IOException("failed to delete " + file2);
                }
            }
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public boolean m20391(File file) {
            return file.exists();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Sink m20392(File file) throws FileNotFoundException {
            try {
                return Okio.m20502(file);
            } catch (FileNotFoundException e) {
                file.getParentFile().mkdirs();
                return Okio.m20502(file);
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m20393(File file) throws IOException {
            if (!file.delete() && file.exists()) {
                throw new IOException("failed to delete " + file);
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Sink m20394(File file) throws FileNotFoundException {
            try {
                return Okio.m20505(file);
            } catch (FileNotFoundException e) {
                file.getParentFile().mkdirs();
                return Okio.m20505(file);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Source m20395(File file) throws FileNotFoundException {
            return Okio.m20512(file);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20396(File file, File file2) throws IOException {
            m20393(file2);
            if (!file.renameTo(file2)) {
                throw new IOException("failed to rename " + file + " to " + file2);
            }
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    long m20381(File file);

    /* renamed from: ʼ  reason: contains not printable characters */
    void m20382(File file) throws IOException;

    /* renamed from: 连任  reason: contains not printable characters */
    boolean m20383(File file);

    /* renamed from: 靐  reason: contains not printable characters */
    Sink m20384(File file) throws FileNotFoundException;

    /* renamed from: 麤  reason: contains not printable characters */
    void m20385(File file) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    Sink m20386(File file) throws FileNotFoundException;

    /* renamed from: 龘  reason: contains not printable characters */
    Source m20387(File file) throws FileNotFoundException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m20388(File file, File file2) throws IOException;
}
