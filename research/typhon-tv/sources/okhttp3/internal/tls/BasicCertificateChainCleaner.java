package okhttp3.internal.tls;

import java.security.GeneralSecurityException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class BasicCertificateChainCleaner extends CertificateChainCleaner {

    /* renamed from: 龘  reason: contains not printable characters */
    private final TrustRootIndex f16417;

    public BasicCertificateChainCleaner(TrustRootIndex trustRootIndex) {
        this.f16417 = trustRootIndex;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m20415(X509Certificate x509Certificate, X509Certificate x509Certificate2) {
        if (!x509Certificate.getIssuerDN().equals(x509Certificate2.getSubjectDN())) {
            return false;
        }
        try {
            x509Certificate.verify(x509Certificate2.getPublicKey());
            return true;
        } catch (GeneralSecurityException e) {
            return false;
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof BasicCertificateChainCleaner) && ((BasicCertificateChainCleaner) obj).f16417.equals(this.f16417);
    }

    public int hashCode() {
        return this.f16417.hashCode();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<Certificate> m20416(List<Certificate> list, String str) throws SSLPeerUnverifiedException {
        ArrayDeque arrayDeque = new ArrayDeque(list);
        ArrayList arrayList = new ArrayList();
        arrayList.add(arrayDeque.removeFirst());
        boolean z = false;
        int i = 0;
        while (i < 9) {
            X509Certificate x509Certificate = (X509Certificate) arrayList.get(arrayList.size() - 1);
            X509Certificate r7 = this.f16417.m20426(x509Certificate);
            if (r7 != null) {
                if (arrayList.size() > 1 || !x509Certificate.equals(r7)) {
                    arrayList.add(r7);
                }
                if (!m20415(r7, r7)) {
                    z = true;
                    i++;
                }
            } else {
                Iterator it2 = arrayDeque.iterator();
                while (it2.hasNext()) {
                    X509Certificate x509Certificate2 = (X509Certificate) it2.next();
                    if (m20415(x509Certificate, x509Certificate2)) {
                        it2.remove();
                        arrayList.add(x509Certificate2);
                        i++;
                    }
                }
                if (!z) {
                    throw new SSLPeerUnverifiedException("Failed to find a trusted cert that signed " + x509Certificate);
                }
            }
            return arrayList;
        }
        throw new SSLPeerUnverifiedException("Certificate chain too long: " + arrayList);
    }
}
