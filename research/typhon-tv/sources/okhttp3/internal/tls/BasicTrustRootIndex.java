package okhttp3.internal.tls;

import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

public final class BasicTrustRootIndex implements TrustRootIndex {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<X500Principal, Set<X509Certificate>> f16418 = new LinkedHashMap();

    public BasicTrustRootIndex(X509Certificate... x509CertificateArr) {
        for (X509Certificate x509Certificate : x509CertificateArr) {
            X500Principal subjectX500Principal = x509Certificate.getSubjectX500Principal();
            Set set = this.f16418.get(subjectX500Principal);
            if (set == null) {
                set = new LinkedHashSet(1);
                this.f16418.put(subjectX500Principal, set);
            }
            set.add(x509Certificate);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof BasicTrustRootIndex) && ((BasicTrustRootIndex) obj).f16418.equals(this.f16418);
    }

    public int hashCode() {
        return this.f16418.hashCode();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public X509Certificate m20417(X509Certificate x509Certificate) {
        Set<X509Certificate> set = this.f16418.get(x509Certificate.getIssuerX500Principal());
        if (set == null) {
            return null;
        }
        for (X509Certificate x509Certificate2 : set) {
            try {
                x509Certificate.verify(x509Certificate2.getPublicKey());
                return x509Certificate2;
            } catch (Exception e) {
            }
        }
        return null;
    }
}
