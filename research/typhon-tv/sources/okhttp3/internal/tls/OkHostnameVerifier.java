package okhttp3.internal.tls;

import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import okhttp3.internal.Util;

public final class OkHostnameVerifier implements HostnameVerifier {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final OkHostnameVerifier f16419 = new OkHostnameVerifier();

    private OkHostnameVerifier() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m20420(String str, X509Certificate x509Certificate) {
        List<String> r0 = m20423(x509Certificate, 7);
        int size = r0.size();
        for (int i = 0; i < size; i++) {
            if (str.equalsIgnoreCase(r0.get(i))) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m20421(String str, X509Certificate x509Certificate) {
        String lowerCase = str.toLowerCase(Locale.US);
        for (String r0 : m20423(x509Certificate, 2)) {
            if (m20424(lowerCase, r0)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<String> m20422(X509Certificate x509Certificate) {
        List<String> r1 = m20423(x509Certificate, 7);
        List<String> r0 = m20423(x509Certificate, 2);
        ArrayList arrayList = new ArrayList(r1.size() + r0.size());
        arrayList.addAll(r1);
        arrayList.addAll(r0);
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<String> m20423(X509Certificate x509Certificate, int i) {
        Integer num;
        String str;
        ArrayList arrayList = new ArrayList();
        try {
            Collection<List<?>> subjectAlternativeNames = x509Certificate.getSubjectAlternativeNames();
            if (subjectAlternativeNames == null) {
                return Collections.emptyList();
            }
            for (List next : subjectAlternativeNames) {
                if (!(next == null || next.size() < 2 || (num = (Integer) next.get(0)) == null || num.intValue() != i || (str = (String) next.get(1)) == null)) {
                    arrayList.add(str);
                }
            }
            return arrayList;
        } catch (CertificateParsingException e) {
            return Collections.emptyList();
        }
    }

    public boolean verify(String str, SSLSession sSLSession) {
        try {
            return m20425(str, (X509Certificate) sSLSession.getPeerCertificates()[0]);
        } catch (SSLException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20424(String str, String str2) {
        if (str == null || str.length() == 0 || str.startsWith(".") || str.endsWith("..") || str2 == null || str2.length() == 0 || str2.startsWith(".") || str2.endsWith("..")) {
            return false;
        }
        if (!str.endsWith(".")) {
            str = str + '.';
        }
        if (!str2.endsWith(".")) {
            str2 = str2 + '.';
        }
        String lowerCase = str2.toLowerCase(Locale.US);
        if (!lowerCase.contains("*")) {
            return str.equals(lowerCase);
        }
        if (!lowerCase.startsWith("*.") || lowerCase.indexOf(42, 1) != -1 || str.length() < lowerCase.length() || "*.".equals(lowerCase)) {
            return false;
        }
        String substring = lowerCase.substring(1);
        if (!str.endsWith(substring)) {
            return false;
        }
        int length = str.length() - substring.length();
        return length <= 0 || str.lastIndexOf(46, length + -1) == -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20425(String str, X509Certificate x509Certificate) {
        return Util.m7107(str) ? m20420(str, x509Certificate) : m20421(str, x509Certificate);
    }
}
