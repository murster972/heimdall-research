package okhttp3.internal.tls;

import java.security.cert.Certificate;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.X509TrustManager;
import okhttp3.internal.platform.Platform;

public abstract class CertificateChainCleaner {
    /* renamed from: 龘  reason: contains not printable characters */
    public static CertificateChainCleaner m20418(X509TrustManager x509TrustManager) {
        return Platform.m7184().m7192(x509TrustManager);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract List<Certificate> m20419(List<Certificate> list, String str) throws SSLPeerUnverifiedException;
}
