package okhttp3.internal.http2;

import com.mopub.common.TyphoonApp;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeRequest;
import okhttp3.internal.Util;
import okio.Buffer;
import okio.BufferedSource;
import okio.ByteString;
import okio.Okio;
import okio.Source;
import org.apache.oltu.oauth2.common.OAuth;

final class Hpack {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Map<ByteString, Integer> f16236 = m20185();

    /* renamed from: 龘  reason: contains not printable characters */
    static final Header[] f16237 = {new Header(Header.f16227, ""), new Header(Header.f16231, (String) OAuth.HttpMethod.GET), new Header(Header.f16231, (String) OAuth.HttpMethod.POST), new Header(Header.f16230, (String) InternalZipTyphoonApp.ZIP_FILE_SEPARATOR), new Header(Header.f16230, "/index.html"), new Header(Header.f16228, (String) TyphoonApp.HTTP), new Header(Header.f16228, (String) TyphoonApp.HTTPS), new Header(Header.f16229, "200"), new Header(Header.f16229, "204"), new Header(Header.f16229, "206"), new Header(Header.f16229, "304"), new Header(Header.f16229, "400"), new Header(Header.f16229, "404"), new Header(Header.f16229, "500"), new Header("accept-charset", ""), new Header("accept-encoding", "gzip, deflate"), new Header("accept-language", ""), new Header("accept-ranges", ""), new Header("accept", ""), new Header("access-control-allow-origin", ""), new Header((String) PubnativeRequest.Parameters.AGE, ""), new Header("allow", ""), new Header("authorization", ""), new Header("cache-control", ""), new Header("content-disposition", ""), new Header("content-encoding", ""), new Header("content-language", ""), new Header("content-length", ""), new Header("content-location", ""), new Header("content-range", ""), new Header("content-type", ""), new Header("cookie", ""), new Header("date", ""), new Header("etag", ""), new Header("expect", ""), new Header("expires", ""), new Header("from", ""), new Header("host", ""), new Header("if-match", ""), new Header("if-modified-since", ""), new Header("if-none-match", ""), new Header("if-range", ""), new Header("if-unmodified-since", ""), new Header("last-modified", ""), new Header("link", ""), new Header("location", ""), new Header("max-forwards", ""), new Header("proxy-authenticate", ""), new Header("proxy-authorization", ""), new Header("range", ""), new Header("referer", ""), new Header("refresh", ""), new Header("retry-after", ""), new Header("server", ""), new Header("set-cookie", ""), new Header("strict-transport-security", ""), new Header("transfer-encoding", ""), new Header("user-agent", ""), new Header("vary", ""), new Header("via", ""), new Header("www-authenticate", "")};

    static final class Reader {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final BufferedSource f16238;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f16239;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f16240;

        /* renamed from: 连任  reason: contains not printable characters */
        private final List<Header> f16241;

        /* renamed from: 靐  reason: contains not printable characters */
        int f16242;

        /* renamed from: 麤  reason: contains not printable characters */
        int f16243;

        /* renamed from: 齉  reason: contains not printable characters */
        int f16244;

        /* renamed from: 龘  reason: contains not printable characters */
        Header[] f16245;

        Reader(int i, int i2, Source source) {
            this.f16241 = new ArrayList();
            this.f16245 = new Header[8];
            this.f16242 = this.f16245.length - 1;
            this.f16244 = 0;
            this.f16243 = 0;
            this.f16239 = i;
            this.f16240 = i2;
            this.f16238 = Okio.m20507(source);
        }

        Reader(int i, Source source) {
            this(i, i, source);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private ByteString m20187(int i) throws IOException {
            if (m20190(i)) {
                return Hpack.f16237[i].f16233;
            }
            int r0 = m20197(i - Hpack.f16237.length);
            if (r0 >= 0 && r0 < this.f16245.length) {
                return this.f16245[r0].f16233;
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m20188() throws IOException {
            this.f16241.add(new Header(Hpack.m20186(m20201()), m20201()));
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private void m20189() throws IOException {
            m20199(-1, new Header(Hpack.m20186(m20201()), m20201()));
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean m20190(int i) {
            return i >= 0 && i <= Hpack.f16237.length + -1;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        private int m20191() throws IOException {
            return this.f16238.m20460() & 255;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private void m20192() {
            Arrays.fill(this.f16245, (Object) null);
            this.f16242 = this.f16245.length - 1;
            this.f16244 = 0;
            this.f16243 = 0;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private void m20193(int i) throws IOException {
            m20199(-1, new Header(m20187(i), m20201()));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m20194(int i) throws IOException {
            if (m20190(i)) {
                this.f16241.add(Hpack.f16237[i]);
                return;
            }
            int r0 = m20197(i - Hpack.f16237.length);
            if (r0 < 0 || r0 >= this.f16245.length) {
                throw new IOException("Header index too large " + (i + 1));
            }
            this.f16241.add(this.f16245[r0]);
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m20195() {
            if (this.f16240 >= this.f16243) {
                return;
            }
            if (this.f16240 == 0) {
                m20192();
            } else {
                m20198(this.f16243 - this.f16240);
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m20196(int i) throws IOException {
            this.f16241.add(new Header(m20187(i), m20201()));
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private int m20197(int i) {
            return this.f16242 + 1 + i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m20198(int i) {
            int i2 = 0;
            if (i > 0) {
                for (int length = this.f16245.length - 1; length >= this.f16242 && i > 0; length--) {
                    i -= this.f16245[length].f16235;
                    this.f16243 -= this.f16245[length].f16235;
                    this.f16244--;
                    i2++;
                }
                System.arraycopy(this.f16245, this.f16242 + 1, this.f16245, this.f16242 + 1 + i2, this.f16244);
                this.f16242 += i2;
            }
            return i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m20199(int i, Header header) {
            this.f16241.add(header);
            int i2 = header.f16235;
            if (i != -1) {
                i2 -= this.f16245[m20197(i)].f16235;
            }
            if (i2 > this.f16240) {
                m20192();
                return;
            }
            int r3 = m20198((this.f16243 + i2) - this.f16240);
            if (i == -1) {
                if (this.f16244 + 1 > this.f16245.length) {
                    Header[] headerArr = new Header[(this.f16245.length * 2)];
                    System.arraycopy(this.f16245, 0, headerArr, this.f16245.length, this.f16245.length);
                    this.f16242 = this.f16245.length - 1;
                    this.f16245 = headerArr;
                }
                int i3 = this.f16242;
                this.f16242 = i3 - 1;
                this.f16245[i3] = header;
                this.f16244++;
            } else {
                this.f16245[i + m20197(i) + r3] = header;
            }
            this.f16243 += i2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public List<Header> m20200() {
            ArrayList arrayList = new ArrayList(this.f16241);
            this.f16241.clear();
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public ByteString m20201() throws IOException {
            int r0 = m20191();
            boolean z = (r0 & 128) == 128;
            int r2 = m20202(r0, 127);
            return z ? ByteString.of(Huffman.m20358().m20362(this.f16238.m20454((long) r2))) : this.f16238.m20465((long) r2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m20202(int i, int i2) throws IOException {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = i2;
            int i5 = 0;
            while (true) {
                int r0 = m20191();
                if ((r0 & 128) == 0) {
                    return i4 + (r0 << i5);
                }
                i4 += (r0 & 127) << i5;
                i5 += 7;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20203() throws IOException {
            while (!this.f16238.m20452()) {
                byte r0 = this.f16238.m20460() & 255;
                if (r0 == 128) {
                    throw new IOException("index == 0");
                } else if ((r0 & 128) == 128) {
                    m20194(m20202((int) r0, 127) - 1);
                } else if (r0 == 64) {
                    m20189();
                } else if ((r0 & 64) == 64) {
                    m20193(m20202((int) r0, 63) - 1);
                } else if ((r0 & 32) == 32) {
                    this.f16240 = m20202((int) r0, 31);
                    if (this.f16240 < 0 || this.f16240 > this.f16239) {
                        throw new IOException("Invalid dynamic table size update " + this.f16240);
                    }
                    m20195();
                } else if (r0 == 16 || r0 == 0) {
                    m20188();
                } else {
                    m20196(m20202((int) r0, 15) - 1);
                }
            }
        }
    }

    static final class Writer {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f16246;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final Buffer f16247;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final boolean f16248;

        /* renamed from: ˑ  reason: contains not printable characters */
        private int f16249;

        /* renamed from: ٴ  reason: contains not printable characters */
        private boolean f16250;

        /* renamed from: 连任  reason: contains not printable characters */
        int f16251;

        /* renamed from: 靐  reason: contains not printable characters */
        int f16252;

        /* renamed from: 麤  reason: contains not printable characters */
        int f16253;

        /* renamed from: 齉  reason: contains not printable characters */
        Header[] f16254;

        /* renamed from: 龘  reason: contains not printable characters */
        int f16255;

        Writer(int i, boolean z, Buffer buffer) {
            this.f16249 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
            this.f16254 = new Header[8];
            this.f16253 = this.f16254.length - 1;
            this.f16251 = 0;
            this.f16246 = 0;
            this.f16255 = i;
            this.f16252 = i;
            this.f16248 = z;
            this.f16247 = buffer;
        }

        Writer(Buffer buffer) {
            this(4096, true, buffer);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private int m20204(int i) {
            int i2 = 0;
            if (i > 0) {
                for (int length = this.f16254.length - 1; length >= this.f16253 && i > 0; length--) {
                    i -= this.f16254[length].f16235;
                    this.f16246 -= this.f16254[length].f16235;
                    this.f16251--;
                    i2++;
                }
                System.arraycopy(this.f16254, this.f16253 + 1, this.f16254, this.f16253 + 1 + i2, this.f16251);
                Arrays.fill(this.f16254, this.f16253 + 1, this.f16253 + 1 + i2, (Object) null);
                this.f16253 += i2;
            }
            return i2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m20205() {
            if (this.f16252 >= this.f16246) {
                return;
            }
            if (this.f16252 == 0) {
                m20206();
            } else {
                m20204(this.f16246 - this.f16252);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m20206() {
            Arrays.fill(this.f16254, (Object) null);
            this.f16253 = this.f16254.length - 1;
            this.f16251 = 0;
            this.f16246 = 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m20207(Header header) {
            int i = header.f16235;
            if (i > this.f16252) {
                m20206();
                return;
            }
            m20204((this.f16246 + i) - this.f16252);
            if (this.f16251 + 1 > this.f16254.length) {
                Header[] headerArr = new Header[(this.f16254.length * 2)];
                System.arraycopy(this.f16254, 0, headerArr, this.f16254.length, this.f16254.length);
                this.f16253 = this.f16254.length - 1;
                this.f16254 = headerArr;
            }
            int i2 = this.f16253;
            this.f16253 = i2 - 1;
            this.f16254[i2] = header;
            this.f16251++;
            this.f16246 += i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20208(int i) {
            this.f16255 = i;
            int min = Math.min(i, 16384);
            if (this.f16252 != min) {
                if (min < this.f16252) {
                    this.f16249 = Math.min(this.f16249, min);
                }
                this.f16250 = true;
                this.f16252 = min;
                m20205();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20209(int i, int i2, int i3) {
            if (i < i2) {
                this.f16247.m7238(i3 | i);
                return;
            }
            this.f16247.m7238(i3 | i2);
            int i4 = i - i2;
            while (i4 >= 128) {
                this.f16247.m7238((i4 & 127) | 128);
                i4 >>>= 7;
            }
            this.f16247.m7238(i4);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20210(List<Header> list) throws IOException {
            if (this.f16250) {
                if (this.f16249 < this.f16252) {
                    m20209(this.f16249, 31, 32);
                }
                this.f16250 = false;
                this.f16249 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
                m20209(this.f16252, 31, 32);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                Header header = list.get(i);
                ByteString asciiLowercase = header.f16233.toAsciiLowercase();
                ByteString byteString = header.f16234;
                int i2 = -1;
                int i3 = -1;
                Integer num = Hpack.f16236.get(asciiLowercase);
                if (num != null && (i3 = num.intValue() + 1) > 1 && i3 < 8) {
                    if (Util.m7127((Object) Hpack.f16237[i3 - 1].f16234, (Object) byteString)) {
                        i2 = i3;
                    } else if (Util.m7127((Object) Hpack.f16237[i3].f16234, (Object) byteString)) {
                        i2 = i3 + 1;
                    }
                }
                if (i2 == -1) {
                    int i4 = this.f16253 + 1;
                    int length = this.f16254.length;
                    while (true) {
                        if (i4 >= length) {
                            break;
                        }
                        if (Util.m7127((Object) this.f16254[i4].f16233, (Object) asciiLowercase)) {
                            if (Util.m7127((Object) this.f16254[i4].f16234, (Object) byteString)) {
                                i2 = (i4 - this.f16253) + Hpack.f16237.length;
                                break;
                            } else if (i3 == -1) {
                                i3 = (i4 - this.f16253) + Hpack.f16237.length;
                            }
                        }
                        i4++;
                    }
                }
                if (i2 != -1) {
                    m20209(i2, 127, 128);
                } else if (i3 == -1) {
                    this.f16247.m7238(64);
                    m20211(asciiLowercase);
                    m20211(byteString);
                    m20207(header);
                } else if (!asciiLowercase.startsWith(Header.f16232) || Header.f16227.equals(asciiLowercase)) {
                    m20209(i3, 63, 64);
                    m20211(byteString);
                    m20207(header);
                } else {
                    m20209(i3, 15, 0);
                    m20211(byteString);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20211(ByteString byteString) throws IOException {
            if (!this.f16248 || Huffman.m20358().m20360(byteString) >= byteString.size()) {
                m20209(byteString.size(), 127, 0);
                this.f16247.m7247(byteString);
                return;
            }
            Buffer buffer = new Buffer();
            Huffman.m20358().m20361(byteString, buffer);
            ByteString r1 = buffer.m7277();
            m20209(r1.size(), 127, 128);
            this.f16247.m7247(r1);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Map<ByteString, Integer> m20185() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(f16237.length);
        for (int i = 0; i < f16237.length; i++) {
            if (!linkedHashMap.containsKey(f16237[i].f16233)) {
                linkedHashMap.put(f16237[i].f16233, Integer.valueOf(i));
            }
        }
        return Collections.unmodifiableMap(linkedHashMap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ByteString m20186(ByteString byteString) throws IOException {
        int i = 0;
        int size = byteString.size();
        while (i < size) {
            byte b = byteString.getByte(i);
            if (b < 65 || b > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + byteString.utf8());
            }
        }
        return byteString;
    }
}
