package okhttp3.internal.http2;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.NamedRunnable;
import okhttp3.internal.Util;
import okhttp3.internal.http2.Http2Reader;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.ByteString;

public final class Http2Connection implements Closeable {

    /* renamed from: ˋ  reason: contains not printable characters */
    static final /* synthetic */ boolean f16278 = (!Http2Connection.class.desiredAssertionStatus());
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public static final ExecutorService f16279 = new ThreadPoolExecutor(0, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, 60, TimeUnit.SECONDS, new SynchronousQueue(), Util.m7122("OkHttp Http2Connection", true));

    /* renamed from: ʻ  reason: contains not printable characters */
    int f16280;

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean f16281;

    /* renamed from: ʽ  reason: contains not printable characters */
    final PushObserver f16282;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f16283 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    final Socket f16284;
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public final ScheduledExecutorService f16285;

    /* renamed from: ˈ  reason: contains not printable characters */
    final Settings f16286 = new Settings();

    /* renamed from: ˉ  reason: contains not printable characters */
    private final ExecutorService f16287;

    /* renamed from: ˊ  reason: contains not printable characters */
    final Set<Integer> f16288 = new LinkedHashSet();
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean f16289;

    /* renamed from: ˑ  reason: contains not printable characters */
    long f16290 = 0;

    /* renamed from: ٴ  reason: contains not printable characters */
    long f16291;

    /* renamed from: ᐧ  reason: contains not printable characters */
    Settings f16292 = new Settings();

    /* renamed from: 连任  reason: contains not printable characters */
    int f16293;

    /* renamed from: 靐  reason: contains not printable characters */
    final Listener f16294;

    /* renamed from: 麤  reason: contains not printable characters */
    final String f16295;

    /* renamed from: 齉  reason: contains not printable characters */
    final Map<Integer, Http2Stream> f16296 = new LinkedHashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    final boolean f16297;

    /* renamed from: ﹶ  reason: contains not printable characters */
    final Http2Writer f16298;

    /* renamed from: ﾞ  reason: contains not printable characters */
    final ReaderRunnable f16299;

    public static class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        PushObserver f16321 = PushObserver.f16392;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f16322;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f16323;

        /* renamed from: 连任  reason: contains not printable characters */
        Listener f16324 = Listener.f16329;

        /* renamed from: 靐  reason: contains not printable characters */
        String f16325;

        /* renamed from: 麤  reason: contains not printable characters */
        BufferedSink f16326;

        /* renamed from: 齉  reason: contains not printable characters */
        BufferedSource f16327;

        /* renamed from: 龘  reason: contains not printable characters */
        Socket f16328;

        public Builder(boolean z) {
            this.f16322 = z;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m20260(int i) {
            this.f16323 = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m20261(Socket socket, String str, BufferedSource bufferedSource, BufferedSink bufferedSink) {
            this.f16328 = socket;
            this.f16325 = str;
            this.f16327 = bufferedSource;
            this.f16326 = bufferedSink;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m20262(Listener listener) {
            this.f16324 = listener;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Http2Connection m20263() {
            return new Http2Connection(this);
        }
    }

    public static abstract class Listener {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final Listener f16329 = new Listener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m20266(Http2Stream http2Stream) throws IOException {
                http2Stream.m20328(ErrorCode.REFUSED_STREAM);
            }
        };

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20264(Http2Connection http2Connection) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract void m20265(Http2Stream http2Stream) throws IOException;
    }

    final class PingRunnable extends NamedRunnable {

        /* renamed from: 麤  reason: contains not printable characters */
        final int f16331;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f16332;

        /* renamed from: 龘  reason: contains not printable characters */
        final boolean f16333;

        PingRunnable(boolean z, int i, int i2) {
            super("OkHttp %s ping %08x%08x", Http2Connection.this.f16295, Integer.valueOf(i), Integer.valueOf(i2));
            this.f16333 = z;
            this.f16332 = i;
            this.f16331 = i2;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m20267() {
            Http2Connection.this.m20253(this.f16333, this.f16332, this.f16331);
        }
    }

    class ReaderRunnable extends NamedRunnable implements Http2Reader.Handler {

        /* renamed from: 龘  reason: contains not printable characters */
        final Http2Reader f16335;

        ReaderRunnable(Http2Reader http2Reader) {
            super("OkHttp %s", Http2Connection.this.f16295);
            this.f16335 = http2Reader;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m20268(final Settings settings) {
            try {
                Http2Connection.this.f16285.execute(new NamedRunnable("OkHttp %s ACK Settings", new Object[]{Http2Connection.this.f16295}) {
                    /* renamed from: 齉  reason: contains not printable characters */
                    public void m20282() {
                        try {
                            Http2Connection.this.f16298.m20352(settings);
                        } catch (IOException e) {
                            Http2Connection.this.m20227();
                        }
                    }
                });
            } catch (RejectedExecutionException e) {
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m20269() {
            ErrorCode errorCode = ErrorCode.INTERNAL_ERROR;
            ErrorCode errorCode2 = ErrorCode.INTERNAL_ERROR;
            try {
                this.f16335.m20296((Http2Reader.Handler) this);
                do {
                } while (this.f16335.m20297(false, (Http2Reader.Handler) this));
                try {
                    Http2Connection.this.m20251(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
                } catch (IOException e) {
                }
                Util.m7124((Closeable) this.f16335);
            } catch (IOException e2) {
                errorCode = ErrorCode.PROTOCOL_ERROR;
                try {
                    Http2Connection.this.m20251(errorCode, ErrorCode.PROTOCOL_ERROR);
                } catch (IOException e3) {
                }
                Util.m7124((Closeable) this.f16335);
            } catch (Throwable th) {
                try {
                    Http2Connection.this.m20251(errorCode, errorCode2);
                } catch (IOException e4) {
                }
                Util.m7124((Closeable) this.f16335);
                throw th;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20270() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20271(int i, int i2, int i3, boolean z) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20272(int i, int i2, List<Header> list) {
            Http2Connection.this.m20244(i2, list);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20273(int i, long j) {
            if (i == 0) {
                synchronized (Http2Connection.this) {
                    Http2Connection.this.f16291 += j;
                    Http2Connection.this.notifyAll();
                }
                return;
            }
            Http2Stream r0 = Http2Connection.this.m20241(i);
            if (r0 != null) {
                synchronized (r0) {
                    r0.m20326(j);
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20274(int i, ErrorCode errorCode) {
            if (Http2Connection.this.m20239(i)) {
                Http2Connection.this.m20238(i, errorCode);
                return;
            }
            Http2Stream r0 = Http2Connection.this.m20233(i);
            if (r0 != null) {
                r0.m20323(errorCode);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20275(int i, ErrorCode errorCode, ByteString byteString) {
            Http2Stream[] http2StreamArr;
            if (byteString.size() > 0) {
            }
            synchronized (Http2Connection.this) {
                http2StreamArr = (Http2Stream[]) Http2Connection.this.f16296.values().toArray(new Http2Stream[Http2Connection.this.f16296.size()]);
                Http2Connection.this.f16281 = true;
            }
            for (Http2Stream http2Stream : http2StreamArr) {
                if (http2Stream.m20325() > i && http2Stream.m20324()) {
                    http2Stream.m20323(ErrorCode.REFUSED_STREAM);
                    Http2Connection.this.m20233(http2Stream.m20325());
                }
            }
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m20276(boolean r5, int r6, int r7) {
            /*
                r4 = this;
                if (r5 == 0) goto L_0x0015
                okhttp3.internal.http2.Http2Connection r1 = okhttp3.internal.http2.Http2Connection.this
                monitor-enter(r1)
                okhttp3.internal.http2.Http2Connection r0 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0012 }
                r2 = 0
                boolean unused = r0.f16289 = r2     // Catch:{ all -> 0x0012 }
                okhttp3.internal.http2.Http2Connection r0 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0012 }
                r0.notifyAll()     // Catch:{ all -> 0x0012 }
                monitor-exit(r1)     // Catch:{ all -> 0x0012 }
            L_0x0011:
                return
            L_0x0012:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0012 }
                throw r0
            L_0x0015:
                okhttp3.internal.http2.Http2Connection r0 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ RejectedExecutionException -> 0x0027 }
                java.util.concurrent.ScheduledExecutorService r0 = r0.f16285     // Catch:{ RejectedExecutionException -> 0x0027 }
                okhttp3.internal.http2.Http2Connection$PingRunnable r1 = new okhttp3.internal.http2.Http2Connection$PingRunnable     // Catch:{ RejectedExecutionException -> 0x0027 }
                okhttp3.internal.http2.Http2Connection r2 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ RejectedExecutionException -> 0x0027 }
                r3 = 1
                r1.<init>(r3, r6, r7)     // Catch:{ RejectedExecutionException -> 0x0027 }
                r0.execute(r1)     // Catch:{ RejectedExecutionException -> 0x0027 }
                goto L_0x0011
            L_0x0027:
                r0 = move-exception
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.http2.Http2Connection.ReaderRunnable.m20276(boolean, int, int):void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0075, code lost:
            r6.m20327(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0078, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x007a, code lost:
            r6.m20316();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m20277(boolean r10, int r11, int r12, java.util.List<okhttp3.internal.http2.Header> r13) {
            /*
                r9 = this;
                okhttp3.internal.http2.Http2Connection r1 = okhttp3.internal.http2.Http2Connection.this
                boolean r1 = r1.m20239(r11)
                if (r1 == 0) goto L_0x000e
                okhttp3.internal.http2.Http2Connection r1 = okhttp3.internal.http2.Http2Connection.this
                r1.m20245((int) r11, (java.util.List<okhttp3.internal.http2.Header>) r13, (boolean) r10)
            L_0x000d:
                return
            L_0x000e:
                okhttp3.internal.http2.Http2Connection r7 = okhttp3.internal.http2.Http2Connection.this
                monitor-enter(r7)
                okhttp3.internal.http2.Http2Connection r1 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0021 }
                okhttp3.internal.http2.Http2Stream r6 = r1.m20241((int) r11)     // Catch:{ all -> 0x0021 }
                if (r6 != 0) goto L_0x0074
                okhttp3.internal.http2.Http2Connection r1 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0021 }
                boolean r1 = r1.f16281     // Catch:{ all -> 0x0021 }
                if (r1 == 0) goto L_0x0024
                monitor-exit(r7)     // Catch:{ all -> 0x0021 }
                goto L_0x000d
            L_0x0021:
                r1 = move-exception
                monitor-exit(r7)     // Catch:{ all -> 0x0021 }
                throw r1
            L_0x0024:
                okhttp3.internal.http2.Http2Connection r1 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0021 }
                int r1 = r1.f16293     // Catch:{ all -> 0x0021 }
                if (r11 > r1) goto L_0x002c
                monitor-exit(r7)     // Catch:{ all -> 0x0021 }
                goto L_0x000d
            L_0x002c:
                int r1 = r11 % 2
                okhttp3.internal.http2.Http2Connection r2 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0021 }
                int r2 = r2.f16280     // Catch:{ all -> 0x0021 }
                int r2 = r2 % 2
                if (r1 != r2) goto L_0x0038
                monitor-exit(r7)     // Catch:{ all -> 0x0021 }
                goto L_0x000d
            L_0x0038:
                okhttp3.internal.http2.Http2Stream r0 = new okhttp3.internal.http2.Http2Stream     // Catch:{ all -> 0x0021 }
                okhttp3.internal.http2.Http2Connection r2 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0021 }
                r3 = 0
                r1 = r11
                r4 = r10
                r5 = r13
                r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0021 }
                okhttp3.internal.http2.Http2Connection r1 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0021 }
                r1.f16293 = r11     // Catch:{ all -> 0x0021 }
                okhttp3.internal.http2.Http2Connection r1 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0021 }
                java.util.Map<java.lang.Integer, okhttp3.internal.http2.Http2Stream> r1 = r1.f16296     // Catch:{ all -> 0x0021 }
                java.lang.Integer r2 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x0021 }
                r1.put(r2, r0)     // Catch:{ all -> 0x0021 }
                java.util.concurrent.ExecutorService r1 = okhttp3.internal.http2.Http2Connection.f16279     // Catch:{ all -> 0x0021 }
                okhttp3.internal.http2.Http2Connection$ReaderRunnable$1 r2 = new okhttp3.internal.http2.Http2Connection$ReaderRunnable$1     // Catch:{ all -> 0x0021 }
                java.lang.String r3 = "OkHttp %s stream %d"
                r4 = 2
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0021 }
                r5 = 0
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0021 }
                java.lang.String r8 = r8.f16295     // Catch:{ all -> 0x0021 }
                r4[r5] = r8     // Catch:{ all -> 0x0021 }
                r5 = 1
                java.lang.Integer r8 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x0021 }
                r4[r5] = r8     // Catch:{ all -> 0x0021 }
                r2.<init>(r3, r4, r0)     // Catch:{ all -> 0x0021 }
                r1.execute(r2)     // Catch:{ all -> 0x0021 }
                monitor-exit(r7)     // Catch:{ all -> 0x0021 }
                goto L_0x000d
            L_0x0074:
                monitor-exit(r7)     // Catch:{ all -> 0x0021 }
                r6.m20327((java.util.List<okhttp3.internal.http2.Header>) r13)
                if (r10 == 0) goto L_0x000d
                r6.m20316()
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.http2.Http2Connection.ReaderRunnable.m20277(boolean, int, int, java.util.List):void");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20278(boolean z, int i, BufferedSource bufferedSource, int i2) throws IOException {
            if (Http2Connection.this.m20239(i)) {
                Http2Connection.this.m20247(i, bufferedSource, i2, z);
                return;
            }
            Http2Stream r0 = Http2Connection.this.m20241(i);
            if (r0 == null) {
                Http2Connection.this.m20246(i, ErrorCode.PROTOCOL_ERROR);
                bufferedSource.m20461((long) i2);
                return;
            }
            r0.m20329(bufferedSource, i2);
            if (z) {
                r0.m20316();
            }
        }

        /* JADX WARNING: type inference failed for: r8v24, types: [java.lang.Object[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m20279(boolean r16, okhttp3.internal.http2.Settings r17) {
            /*
                r15 = this;
                r2 = 0
                r7 = 0
                okhttp3.internal.http2.Http2Connection r9 = okhttp3.internal.http2.Http2Connection.this
                monitor-enter(r9)
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Settings r8 = r8.f16286     // Catch:{ all -> 0x0099 }
                int r5 = r8.m20373()     // Catch:{ all -> 0x0099 }
                if (r16 == 0) goto L_0x0017
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Settings r8 = r8.f16286     // Catch:{ all -> 0x0099 }
                r8.m20378()     // Catch:{ all -> 0x0099 }
            L_0x0017:
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Settings r8 = r8.f16286     // Catch:{ all -> 0x0099 }
                r0 = r17
                r8.m20379((okhttp3.internal.http2.Settings) r0)     // Catch:{ all -> 0x0099 }
                r0 = r17
                r15.m20268(r0)     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Settings r8 = r8.f16286     // Catch:{ all -> 0x0099 }
                int r4 = r8.m20373()     // Catch:{ all -> 0x0099 }
                r8 = -1
                if (r4 == r8) goto L_0x0069
                if (r4 == r5) goto L_0x0069
                int r8 = r4 - r5
                long r2 = (long) r8     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                boolean r8 = r8.f16283     // Catch:{ all -> 0x0099 }
                if (r8 != 0) goto L_0x0045
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                r8.m20249((long) r2)     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                r10 = 1
                r8.f16283 = r10     // Catch:{ all -> 0x0099 }
            L_0x0045:
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                java.util.Map<java.lang.Integer, okhttp3.internal.http2.Http2Stream> r8 = r8.f16296     // Catch:{ all -> 0x0099 }
                boolean r8 = r8.isEmpty()     // Catch:{ all -> 0x0099 }
                if (r8 != 0) goto L_0x0069
                okhttp3.internal.http2.Http2Connection r8 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                java.util.Map<java.lang.Integer, okhttp3.internal.http2.Http2Stream> r8 = r8.f16296     // Catch:{ all -> 0x0099 }
                java.util.Collection r8 = r8.values()     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Http2Connection r10 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                java.util.Map<java.lang.Integer, okhttp3.internal.http2.Http2Stream> r10 = r10.f16296     // Catch:{ all -> 0x0099 }
                int r10 = r10.size()     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Http2Stream[] r10 = new okhttp3.internal.http2.Http2Stream[r10]     // Catch:{ all -> 0x0099 }
                java.lang.Object[] r8 = r8.toArray(r10)     // Catch:{ all -> 0x0099 }
                r0 = r8
                okhttp3.internal.http2.Http2Stream[] r0 = (okhttp3.internal.http2.Http2Stream[]) r0     // Catch:{ all -> 0x0099 }
                r7 = r0
            L_0x0069:
                java.util.concurrent.ExecutorService r8 = okhttp3.internal.http2.Http2Connection.f16279     // Catch:{ all -> 0x0099 }
                okhttp3.internal.http2.Http2Connection$ReaderRunnable$2 r10 = new okhttp3.internal.http2.Http2Connection$ReaderRunnable$2     // Catch:{ all -> 0x0099 }
                java.lang.String r11 = "OkHttp %s settings"
                r12 = 1
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ all -> 0x0099 }
                r13 = 0
                okhttp3.internal.http2.Http2Connection r14 = okhttp3.internal.http2.Http2Connection.this     // Catch:{ all -> 0x0099 }
                java.lang.String r14 = r14.f16295     // Catch:{ all -> 0x0099 }
                r12[r13] = r14     // Catch:{ all -> 0x0099 }
                r10.<init>(r11, r12)     // Catch:{ all -> 0x0099 }
                r8.execute(r10)     // Catch:{ all -> 0x0099 }
                monitor-exit(r9)     // Catch:{ all -> 0x0099 }
                if (r7 == 0) goto L_0x009f
                r8 = 0
                int r8 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
                if (r8 == 0) goto L_0x009f
                int r9 = r7.length
                r8 = 0
            L_0x008d:
                if (r8 >= r9) goto L_0x009f
                r6 = r7[r8]
                monitor-enter(r6)
                r6.m20326((long) r2)     // Catch:{ all -> 0x009c }
                monitor-exit(r6)     // Catch:{ all -> 0x009c }
                int r8 = r8 + 1
                goto L_0x008d
            L_0x0099:
                r8 = move-exception
                monitor-exit(r9)     // Catch:{ all -> 0x0099 }
                throw r8
            L_0x009c:
                r8 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x009c }
                throw r8
            L_0x009f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.http2.Http2Connection.ReaderRunnable.m20279(boolean, okhttp3.internal.http2.Settings):void");
        }
    }

    Http2Connection(Builder builder) {
        this.f16282 = builder.f16321;
        this.f16297 = builder.f16322;
        this.f16294 = builder.f16324;
        this.f16280 = builder.f16322 ? 1 : 2;
        if (builder.f16322) {
            this.f16280 += 2;
        }
        if (builder.f16322) {
            this.f16292.m20377(7, 16777216);
        }
        this.f16295 = builder.f16325;
        this.f16285 = new ScheduledThreadPoolExecutor(1, Util.m7122(Util.m7116("OkHttp %s Writer", this.f16295), false));
        if (builder.f16323 != 0) {
            this.f16285.scheduleAtFixedRate(new PingRunnable(false, 0, 0), (long) builder.f16323, (long) builder.f16323, TimeUnit.MILLISECONDS);
        }
        this.f16287 = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), Util.m7122(Util.m7116("OkHttp %s Push Observer", this.f16295), true));
        this.f16286.m20377(7, 65535);
        this.f16286.m20377(5, 16384);
        this.f16291 = (long) this.f16286.m20373();
        this.f16284 = builder.f16328;
        this.f16298 = new Http2Writer(builder.f16326, this.f16297);
        this.f16299 = new ReaderRunnable(new Http2Reader(builder.f16327, this.f16297));
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m20227() {
        try {
            m20251(ErrorCode.PROTOCOL_ERROR, ErrorCode.PROTOCOL_ERROR);
        } catch (IOException e) {
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Http2Stream m20230(int i, List<Header> list, boolean z) throws IOException {
        int i2;
        Http2Stream http2Stream;
        boolean z2;
        boolean z3 = !z;
        synchronized (this.f16298) {
            synchronized (this) {
                if (this.f16280 > 1073741823) {
                    m20250(ErrorCode.REFUSED_STREAM);
                }
                if (this.f16281) {
                    throw new ConnectionShutdownException();
                }
                i2 = this.f16280;
                this.f16280 += 2;
                http2Stream = new Http2Stream(i2, this, z3, false, list);
                z2 = !z || this.f16291 == 0 || http2Stream.f16361 == 0;
                if (http2Stream.m20321()) {
                    this.f16296.put(Integer.valueOf(i2), http2Stream);
                }
            }
            if (i == 0) {
                this.f16298.m20354(z3, i2, i, list);
            } else if (this.f16297) {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            } else {
                this.f16298.m20348(i, i2, list);
            }
        }
        if (z2) {
            this.f16298.m20342();
        }
        return http2Stream;
    }

    public void close() throws IOException {
        m20251(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized Http2Stream m20233(int i) {
        Http2Stream remove;
        remove = this.f16296.remove(Integer.valueOf(i));
        notifyAll();
        return remove;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m20234() throws IOException {
        this.f16298.m20342();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m20235(int i, ErrorCode errorCode) throws IOException {
        this.f16298.m20350(i, errorCode);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized boolean m20236() {
        return this.f16281;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m20237() throws IOException {
        m20252(true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m20238(int i, ErrorCode errorCode) {
        final int i2 = i;
        final ErrorCode errorCode2 = errorCode;
        this.f16287.execute(new NamedRunnable("OkHttp %s Push Reset[%s]", new Object[]{this.f16295, Integer.valueOf(i)}) {
            /* renamed from: 齉  reason: contains not printable characters */
            public void m20259() {
                Http2Connection.this.f16282.m20363(i2, errorCode2);
                synchronized (Http2Connection.this) {
                    Http2Connection.this.f16288.remove(Integer.valueOf(i2));
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m20239(int i) {
        return i != 0 && (i & 1) == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized int m20240() {
        return this.f16286.m20376(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Http2Stream m20241(int i) {
        return this.f16296.get(Integer.valueOf(i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Http2Stream m20242(List<Header> list, boolean z) throws IOException {
        return m20230(0, list, z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20243(int i, long j) {
        try {
            final int i2 = i;
            final long j2 = j;
            this.f16285.execute(new NamedRunnable("OkHttp Window Update %s stream %d", new Object[]{this.f16295, Integer.valueOf(i)}) {
                /* renamed from: 齉  reason: contains not printable characters */
                public void m20255() {
                    try {
                        Http2Connection.this.f16298.m20349(i2, j2);
                    } catch (IOException e) {
                        Http2Connection.this.m20227();
                    }
                }
            });
        } catch (RejectedExecutionException e) {
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m20244(int r8, java.util.List<okhttp3.internal.http2.Header> r9) {
        /*
            r7 = this;
            monitor-enter(r7)
            java.util.Set<java.lang.Integer> r0 = r7.f16288     // Catch:{ all -> 0x0040 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0040 }
            boolean r0 = r0.contains(r1)     // Catch:{ all -> 0x0040 }
            if (r0 == 0) goto L_0x0014
            okhttp3.internal.http2.ErrorCode r0 = okhttp3.internal.http2.ErrorCode.PROTOCOL_ERROR     // Catch:{ all -> 0x0040 }
            r7.m20246((int) r8, (okhttp3.internal.http2.ErrorCode) r0)     // Catch:{ all -> 0x0040 }
            monitor-exit(r7)     // Catch:{ all -> 0x0040 }
        L_0x0013:
            return
        L_0x0014:
            java.util.Set<java.lang.Integer> r0 = r7.f16288     // Catch:{ all -> 0x0040 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0040 }
            r0.add(r1)     // Catch:{ all -> 0x0040 }
            monitor-exit(r7)     // Catch:{ all -> 0x0040 }
            java.util.concurrent.ExecutorService r6 = r7.f16287     // Catch:{ RejectedExecutionException -> 0x003e }
            okhttp3.internal.http2.Http2Connection$3 r0 = new okhttp3.internal.http2.Http2Connection$3     // Catch:{ RejectedExecutionException -> 0x003e }
            java.lang.String r2 = "OkHttp %s Push Request[%s]"
            r1 = 2
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ RejectedExecutionException -> 0x003e }
            r1 = 0
            java.lang.String r4 = r7.f16295     // Catch:{ RejectedExecutionException -> 0x003e }
            r3[r1] = r4     // Catch:{ RejectedExecutionException -> 0x003e }
            r1 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r8)     // Catch:{ RejectedExecutionException -> 0x003e }
            r3[r1] = r4     // Catch:{ RejectedExecutionException -> 0x003e }
            r1 = r7
            r4 = r8
            r5 = r9
            r0.<init>(r2, r3, r4, r5)     // Catch:{ RejectedExecutionException -> 0x003e }
            r6.execute(r0)     // Catch:{ RejectedExecutionException -> 0x003e }
            goto L_0x0013
        L_0x003e:
            r0 = move-exception
            goto L_0x0013
        L_0x0040:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0040 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.http2.Http2Connection.m20244(int, java.util.List):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20245(int i, List<Header> list, boolean z) {
        try {
            final int i2 = i;
            final List<Header> list2 = list;
            final boolean z2 = z;
            this.f16287.execute(new NamedRunnable("OkHttp %s Push Headers[%s]", new Object[]{this.f16295, Integer.valueOf(i)}) {
                /* renamed from: 齉  reason: contains not printable characters */
                public void m20257() {
                    boolean r0 = Http2Connection.this.f16282.m20365(i2, list2, z2);
                    if (r0) {
                        try {
                            Http2Connection.this.f16298.m20350(i2, ErrorCode.CANCEL);
                        } catch (IOException e) {
                            return;
                        }
                    }
                    if (r0 || z2) {
                        synchronized (Http2Connection.this) {
                            Http2Connection.this.f16288.remove(Integer.valueOf(i2));
                        }
                    }
                }
            });
        } catch (RejectedExecutionException e) {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20246(int i, ErrorCode errorCode) {
        try {
            final int i2 = i;
            final ErrorCode errorCode2 = errorCode;
            this.f16285.execute(new NamedRunnable("OkHttp %s stream %d", new Object[]{this.f16295, Integer.valueOf(i)}) {
                /* renamed from: 齉  reason: contains not printable characters */
                public void m20254() {
                    try {
                        Http2Connection.this.m20235(i2, errorCode2);
                    } catch (IOException e) {
                        Http2Connection.this.m20227();
                    }
                }
            });
        } catch (RejectedExecutionException e) {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20247(int i, BufferedSource bufferedSource, int i2, boolean z) throws IOException {
        final Buffer buffer = new Buffer();
        bufferedSource.m20470((long) i2);
        bufferedSource.m20568(buffer, (long) i2);
        if (buffer.m7242() != ((long) i2)) {
            throw new IOException(buffer.m7242() + " != " + i2);
        }
        final int i3 = i;
        final int i4 = i2;
        final boolean z2 = z;
        this.f16287.execute(new NamedRunnable("OkHttp %s Push Data[%s]", new Object[]{this.f16295, Integer.valueOf(i)}) {
            /* renamed from: 齉  reason: contains not printable characters */
            public void m20258() {
                try {
                    boolean r0 = Http2Connection.this.f16282.m20366(i3, buffer, i4, z2);
                    if (r0) {
                        Http2Connection.this.f16298.m20350(i3, ErrorCode.CANCEL);
                    }
                    if (r0 || z2) {
                        synchronized (Http2Connection.this) {
                            Http2Connection.this.f16288.remove(Integer.valueOf(i3));
                        }
                    }
                } catch (IOException e) {
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20248(int i, boolean z, Buffer buffer, long j) throws IOException {
        int min;
        if (j == 0) {
            this.f16298.m20356(z, i, buffer, 0);
            return;
        }
        while (j > 0) {
            synchronized (this) {
                while (this.f16291 <= 0) {
                    try {
                        if (!this.f16296.containsKey(Integer.valueOf(i))) {
                            throw new IOException("stream closed");
                        }
                        wait();
                    } catch (InterruptedException e) {
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j, this.f16291), this.f16298.m20344());
                this.f16291 -= (long) min;
            }
            j -= (long) min;
            this.f16298.m20356(z && j == 0, i, buffer, min);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20249(long j) {
        this.f16291 += j;
        if (j > 0) {
            notifyAll();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20250(ErrorCode errorCode) throws IOException {
        synchronized (this.f16298) {
            synchronized (this) {
                if (!this.f16281) {
                    this.f16281 = true;
                    int i = this.f16293;
                    this.f16298.m20351(i, errorCode, Util.f6417);
                }
            }
        }
    }

    /* JADX WARNING: type inference failed for: r5v13, types: [java.lang.Object[]] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m20251(okhttp3.internal.http2.ErrorCode r8, okhttp3.internal.http2.ErrorCode r9) throws java.io.IOException {
        /*
            r7 = this;
            boolean r5 = f16278
            if (r5 != 0) goto L_0x0010
            boolean r5 = java.lang.Thread.holdsLock(r7)
            if (r5 == 0) goto L_0x0010
            java.lang.AssertionError r5 = new java.lang.AssertionError
            r5.<init>()
            throw r5
        L_0x0010:
            r4 = 0
            r7.m20250((okhttp3.internal.http2.ErrorCode) r8)     // Catch:{ IOException -> 0x0048 }
        L_0x0014:
            r3 = 0
            monitor-enter(r7)
            java.util.Map<java.lang.Integer, okhttp3.internal.http2.Http2Stream> r5 = r7.f16296     // Catch:{ all -> 0x004b }
            boolean r5 = r5.isEmpty()     // Catch:{ all -> 0x004b }
            if (r5 != 0) goto L_0x0039
            java.util.Map<java.lang.Integer, okhttp3.internal.http2.Http2Stream> r5 = r7.f16296     // Catch:{ all -> 0x004b }
            java.util.Collection r5 = r5.values()     // Catch:{ all -> 0x004b }
            java.util.Map<java.lang.Integer, okhttp3.internal.http2.Http2Stream> r6 = r7.f16296     // Catch:{ all -> 0x004b }
            int r6 = r6.size()     // Catch:{ all -> 0x004b }
            okhttp3.internal.http2.Http2Stream[] r6 = new okhttp3.internal.http2.Http2Stream[r6]     // Catch:{ all -> 0x004b }
            java.lang.Object[] r5 = r5.toArray(r6)     // Catch:{ all -> 0x004b }
            r0 = r5
            okhttp3.internal.http2.Http2Stream[] r0 = (okhttp3.internal.http2.Http2Stream[]) r0     // Catch:{ all -> 0x004b }
            r3 = r0
            java.util.Map<java.lang.Integer, okhttp3.internal.http2.Http2Stream> r5 = r7.f16296     // Catch:{ all -> 0x004b }
            r5.clear()     // Catch:{ all -> 0x004b }
        L_0x0039:
            monitor-exit(r7)     // Catch:{ all -> 0x004b }
            if (r3 == 0) goto L_0x0053
            int r6 = r3.length
            r5 = 0
        L_0x003e:
            if (r5 >= r6) goto L_0x0053
            r2 = r3[r5]
            r2.m20328((okhttp3.internal.http2.ErrorCode) r9)     // Catch:{ IOException -> 0x004e }
        L_0x0045:
            int r5 = r5 + 1
            goto L_0x003e
        L_0x0048:
            r1 = move-exception
            r4 = r1
            goto L_0x0014
        L_0x004b:
            r5 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x004b }
            throw r5
        L_0x004e:
            r1 = move-exception
            if (r4 == 0) goto L_0x0045
            r4 = r1
            goto L_0x0045
        L_0x0053:
            okhttp3.internal.http2.Http2Writer r5 = r7.f16298     // Catch:{ IOException -> 0x006a }
            r5.close()     // Catch:{ IOException -> 0x006a }
        L_0x0058:
            java.net.Socket r5 = r7.f16284     // Catch:{ IOException -> 0x006f }
            r5.close()     // Catch:{ IOException -> 0x006f }
        L_0x005d:
            java.util.concurrent.ScheduledExecutorService r5 = r7.f16285
            r5.shutdown()
            java.util.concurrent.ExecutorService r5 = r7.f16287
            r5.shutdown()
            if (r4 == 0) goto L_0x0072
            throw r4
        L_0x006a:
            r1 = move-exception
            if (r4 != 0) goto L_0x0058
            r4 = r1
            goto L_0x0058
        L_0x006f:
            r1 = move-exception
            r4 = r1
            goto L_0x005d
        L_0x0072:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.http2.Http2Connection.m20251(okhttp3.internal.http2.ErrorCode, okhttp3.internal.http2.ErrorCode):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20252(boolean z) throws IOException {
        if (z) {
            this.f16298.m20345();
            this.f16298.m20343(this.f16292);
            int r0 = this.f16292.m20373();
            if (r0 != 65535) {
                this.f16298.m20349(0, (long) (r0 - 65535));
            }
        }
        new Thread(this.f16299).start();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20253(boolean z, int i, int i2) {
        boolean z2;
        if (!z) {
            synchronized (this) {
                z2 = this.f16289;
                this.f16289 = true;
            }
            if (z2) {
                m20227();
                return;
            }
        }
        try {
            this.f16298.m20353(z, i, i2);
        } catch (IOException e) {
            m20227();
        }
    }
}
