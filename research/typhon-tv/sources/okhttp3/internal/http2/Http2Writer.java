package okhttp3.internal.http2;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.internal.Util;
import okhttp3.internal.http2.Hpack;
import okio.Buffer;
import okio.BufferedSink;

final class Http2Writer implements Closeable {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Logger f16378 = Logger.getLogger(Http2.class.getName());

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f16379 = 16384;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f16380;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Buffer f16381 = new Buffer();

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f16382;

    /* renamed from: 齉  reason: contains not printable characters */
    private final BufferedSink f16383;

    /* renamed from: 龘  reason: contains not printable characters */
    final Hpack.Writer f16384 = new Hpack.Writer(this.f16381);

    Http2Writer(BufferedSink bufferedSink, boolean z) {
        this.f16383 = bufferedSink;
        this.f16382 = z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m20340(int i, long j) throws IOException {
        while (j > 0) {
            int min = (int) Math.min((long) this.f16379, j);
            j -= (long) min;
            m20347(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
            this.f16383.a_(this.f16381, (long) min);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m20341(BufferedSink bufferedSink, int i) throws IOException {
        bufferedSink.m20444((i >>> 16) & 255);
        bufferedSink.m20444((i >>> 8) & 255);
        bufferedSink.m20444(i & 255);
    }

    public synchronized void close() throws IOException {
        this.f16380 = true;
        this.f16383.close();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m20342() throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        }
        this.f16383.flush();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m20343(Settings settings) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        }
        m20347(0, settings.m20371() * 6, (byte) 4, (byte) 0);
        for (int i = 0; i < 10; i++) {
            if (settings.m20380(i)) {
                int i2 = i;
                if (i2 == 4) {
                    i2 = 3;
                } else if (i2 == 7) {
                    i2 = 4;
                }
                this.f16383.m20443(i2);
                this.f16383.m20441(settings.m20372(i));
            }
        }
        this.f16383.flush();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m20344() {
        return this.f16379;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20345() throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        } else if (this.f16382) {
            if (f16378.isLoggable(Level.FINE)) {
                f16378.fine(Util.m7116(">> CONNECTION %s", Http2.f16259.hex()));
            }
            this.f16383.m20448(Http2.f16259.toByteArray());
            this.f16383.flush();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20346(int i, byte b, Buffer buffer, int i2) throws IOException {
        m20347(i, i2, (byte) 0, b);
        if (i2 > 0) {
            this.f16383.a_(buffer, (long) i2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20347(int i, int i2, byte b, byte b2) throws IOException {
        if (f16378.isLoggable(Level.FINE)) {
            f16378.fine(Http2.m20215(false, i, i2, b, b2));
        }
        if (i2 > this.f16379) {
            throw Http2.m20213("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(this.f16379), Integer.valueOf(i2));
        } else if ((Integer.MIN_VALUE & i) != 0) {
            throw Http2.m20213("reserved bit set: %s", Integer.valueOf(i));
        } else {
            m20341(this.f16383, i2);
            this.f16383.m20444(b & 255);
            this.f16383.m20444(b2 & 255);
            this.f16383.m20441(Integer.MAX_VALUE & i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20348(int i, int i2, List<Header> list) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        }
        this.f16384.m20210(list);
        long r0 = this.f16381.m7242();
        int min = (int) Math.min((long) (this.f16379 - 4), r0);
        m20347(i, min + 4, (byte) 5, r0 == ((long) min) ? (byte) 4 : 0);
        this.f16383.m20441(Integer.MAX_VALUE & i2);
        this.f16383.a_(this.f16381, (long) min);
        if (r0 > ((long) min)) {
            m20340(i, r0 - ((long) min));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20349(int i, long j) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        } else if (j == 0 || j > 2147483647L) {
            throw Http2.m20213("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
        } else {
            m20347(i, 4, (byte) 8, (byte) 0);
            this.f16383.m20441((int) j);
            this.f16383.flush();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20350(int i, ErrorCode errorCode) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        } else if (errorCode.httpCode == -1) {
            throw new IllegalArgumentException();
        } else {
            m20347(i, 4, (byte) 3, (byte) 0);
            this.f16383.m20441(errorCode.httpCode);
            this.f16383.flush();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20351(int i, ErrorCode errorCode, byte[] bArr) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        } else if (errorCode.httpCode == -1) {
            throw Http2.m20213("errorCode.httpCode == -1", new Object[0]);
        } else {
            m20347(0, bArr.length + 8, (byte) 7, (byte) 0);
            this.f16383.m20441(i);
            this.f16383.m20441(errorCode.httpCode);
            if (bArr.length > 0) {
                this.f16383.m20448(bArr);
            }
            this.f16383.flush();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20352(Settings settings) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        }
        this.f16379 = settings.m20374(this.f16379);
        if (settings.m20375() != -1) {
            this.f16384.m20208(settings.m20375());
        }
        m20347(0, 0, (byte) 4, (byte) 1);
        this.f16383.flush();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20353(boolean z, int i, int i2) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        }
        m20347(0, 8, (byte) 6, z ? (byte) 1 : 0);
        this.f16383.m20441(i);
        this.f16383.m20441(i2);
        this.f16383.flush();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20354(boolean z, int i, int i2, List<Header> list) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        }
        m20355(z, i, list);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20355(boolean z, int i, List<Header> list) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        }
        this.f16384.m20210(list);
        long r0 = this.f16381.m7242();
        int min = (int) Math.min((long) this.f16379, r0);
        byte b = r0 == ((long) min) ? (byte) 4 : 0;
        if (z) {
            b = (byte) (b | 1);
        }
        m20347(i, min, (byte) 1, b);
        this.f16383.a_(this.f16381, (long) min);
        if (r0 > ((long) min)) {
            m20340(i, r0 - ((long) min));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20356(boolean z, int i, Buffer buffer, int i2) throws IOException {
        if (this.f16380) {
            throw new IOException("closed");
        }
        byte b = 0;
        if (z) {
            b = (byte) 1;
        }
        m20346(i, b, buffer, i2);
    }
}
