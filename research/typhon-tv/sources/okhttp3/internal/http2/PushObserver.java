package okhttp3.internal.http2;

import java.io.IOException;
import java.util.List;
import okio.BufferedSource;

public interface PushObserver {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final PushObserver f16392 = new PushObserver() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20367(int i, ErrorCode errorCode) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20368(int i, List<Header> list) {
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20369(int i, List<Header> list, boolean z) {
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20370(int i, BufferedSource bufferedSource, int i2, boolean z) throws IOException {
            bufferedSource.m20461((long) i2);
            return true;
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    void m20363(int i, ErrorCode errorCode);

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m20364(int i, List<Header> list);

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m20365(int i, List<Header> list, boolean z);

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m20366(int i, BufferedSource bufferedSource, int i2, boolean z) throws IOException;
}
