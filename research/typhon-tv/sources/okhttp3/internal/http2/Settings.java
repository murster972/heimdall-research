package okhttp3.internal.http2;

import java.util.Arrays;

public final class Settings {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int[] f16393 = new int[10];

    /* renamed from: 龘  reason: contains not printable characters */
    private int f16394;

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m20371() {
        return Integer.bitCount(this.f16394);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m20372(int i) {
        return this.f16393[i];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m20373() {
        if ((this.f16394 & 128) != 0) {
            return this.f16393[7];
        }
        return 65535;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m20374(int i) {
        return (this.f16394 & 32) != 0 ? this.f16393[5] : i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m20375() {
        if ((this.f16394 & 2) != 0) {
            return this.f16393[1];
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m20376(int i) {
        return (this.f16394 & 16) != 0 ? this.f16393[4] : i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Settings m20377(int i, int i2) {
        if (i >= 0 && i < this.f16393.length) {
            this.f16394 |= 1 << i;
            this.f16393[i] = i2;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20378() {
        this.f16394 = 0;
        Arrays.fill(this.f16393, 0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20379(Settings settings) {
        for (int i = 0; i < 10; i++) {
            if (settings.m20380(i)) {
                m20377(i, settings.m20372(i));
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20380(int i) {
        return (this.f16394 & (1 << i)) != 0;
    }
}
