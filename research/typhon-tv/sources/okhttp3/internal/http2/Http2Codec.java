package okhttp3.internal.http2;

import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.connection.StreamAllocation;
import okhttp3.internal.http.HttpCodec;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.RealResponseBody;
import okhttp3.internal.http.RequestLine;
import okhttp3.internal.http.StatusLine;
import okio.Buffer;
import okio.ByteString;
import okio.ForwardingSource;
import okio.Okio;
import okio.Sink;
import okio.Source;
import org.apache.oltu.oauth2.common.OAuth;

public final class Http2Codec implements HttpCodec {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final ByteString f16260 = ByteString.encodeUtf8("transfer-encoding");

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final ByteString f16261 = ByteString.encodeUtf8("te");

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final ByteString f16262 = ByteString.encodeUtf8("encoding");

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final ByteString f16263 = ByteString.encodeUtf8("upgrade");

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final List<ByteString> f16264 = Util.m7121((T[]) new ByteString[]{f16267, f16269, f16268, f16266, f16261, f16260, f16262, f16263, Header.f16231, Header.f16230, Header.f16228, Header.f16227});

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final List<ByteString> f16265 = Util.m7121((T[]) new ByteString[]{f16267, f16269, f16268, f16266, f16261, f16260, f16262, f16263});

    /* renamed from: 连任  reason: contains not printable characters */
    private static final ByteString f16266 = ByteString.encodeUtf8("proxy-connection");

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ByteString f16267 = ByteString.encodeUtf8("connection");

    /* renamed from: 麤  reason: contains not printable characters */
    private static final ByteString f16268 = ByteString.encodeUtf8("keep-alive");

    /* renamed from: 齉  reason: contains not printable characters */
    private static final ByteString f16269 = ByteString.encodeUtf8("host");

    /* renamed from: ʾ  reason: contains not printable characters */
    private final Interceptor.Chain f16270;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Http2Connection f16271;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final OkHttpClient f16272;

    /* renamed from: 龘  reason: contains not printable characters */
    final StreamAllocation f16273;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Http2Stream f16274;

    class StreamFinishingSource extends ForwardingSource {

        /* renamed from: 靐  reason: contains not printable characters */
        long f16275 = 0;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f16277 = false;

        StreamFinishingSource(Source source) {
            super(source);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m20225(IOException iOException) {
            if (!this.f16277) {
                this.f16277 = true;
                Http2Codec.this.f16273.m20104(false, Http2Codec.this, this.f16275, iOException);
            }
        }

        public void close() throws IOException {
            super.close();
            m20225((IOException) null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m20226(Buffer buffer, long j) throws IOException {
            try {
                long r2 = m20478().m20568(buffer, j);
                if (r2 > 0) {
                    this.f16275 += r2;
                }
                return r2;
            } catch (IOException e) {
                m20225(e);
                throw e;
            }
        }
    }

    public Http2Codec(OkHttpClient okHttpClient, Interceptor.Chain chain, StreamAllocation streamAllocation, Http2Connection http2Connection) {
        this.f16272 = okHttpClient;
        this.f16270 = chain;
        this.f16273 = streamAllocation;
        this.f16271 = http2Connection;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static List<Header> m20216(Request request) {
        Headers r0 = request.m7051();
        ArrayList arrayList = new ArrayList(r0.m6934() + 4);
        arrayList.add(new Header(Header.f16231, request.m7048()));
        arrayList.add(new Header(Header.f16230, RequestLine.m20148(request.m7053())));
        String r1 = request.m7052("Host");
        if (r1 != null) {
            arrayList.add(new Header(Header.f16227, r1));
        }
        arrayList.add(new Header(Header.f16228, request.m7053().m6961()));
        int r5 = r0.m6934();
        for (int i = 0; i < r5; i++) {
            ByteString encodeUtf8 = ByteString.encodeUtf8(r0.m6935(i).toLowerCase(Locale.US));
            if (!f16264.contains(encodeUtf8)) {
                arrayList.add(new Header(encodeUtf8, r0.m6930(i)));
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Response.Builder m20217(List<Header> list) throws IOException {
        StatusLine statusLine = null;
        Headers.Builder builder = new Headers.Builder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Header header = list.get(i);
            if (header != null) {
                ByteString byteString = header.f16233;
                String utf8 = header.f16234.utf8();
                if (byteString.equals(Header.f16229)) {
                    statusLine = StatusLine.m20159("HTTP/1.1 " + utf8);
                } else if (!f16265.contains(byteString)) {
                    Internal.f16088.m20011(builder, byteString.utf8(), utf8);
                }
            } else if (statusLine != null && statusLine.f16198 == 100) {
                statusLine = null;
                builder = new Headers.Builder();
            }
        }
        if (statusLine != null) {
            return new Response.Builder().m7083(Protocol.HTTP_2).m7077(statusLine.f16198).m7079(statusLine.f16199).m7082(builder.m19955());
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m20218() throws IOException {
        this.f16274.m20314().close();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m20219() {
        if (this.f16274 != null) {
            this.f16274.m20320(ErrorCode.CANCEL);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Response.Builder m20220(boolean z) throws IOException {
        Response.Builder r1 = m20217(this.f16274.m20322());
        if (!z || Internal.f16088.m20005(r1) != 100) {
            return r1;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResponseBody m20221(Response response) throws IOException {
        this.f16273.f16170.m19916(this.f16273.f16169);
        return new RealResponseBody(response.m7067(OAuth.HeaderType.CONTENT_TYPE), HttpHeaders.m20127(response), Okio.m20507((Source) new StreamFinishingSource(this.f16274.m20313())));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Sink m20222(Request request, long j) {
        return this.f16274.m20314();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20223() throws IOException {
        this.f16271.m20234();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20224(Request request) throws IOException {
        if (this.f16274 == null) {
            this.f16274 = this.f16271.m20242(m20216(request), request.m7050() != null);
            this.f16274.m20319().m20574((long) this.f16270.m6992(), TimeUnit.MILLISECONDS);
            this.f16274.m20312().m20574((long) this.f16270.m6990(), TimeUnit.MILLISECONDS);
        }
    }
}
