package okhttp3.internal.http2;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.internal.Util;
import okhttp3.internal.http2.Hpack;
import okio.Buffer;
import okio.BufferedSource;
import okio.ByteString;
import okio.Source;
import okio.Timeout;

final class Http2Reader implements Closeable {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Logger f16341 = Logger.getLogger(Http2.class.getName());

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f16342;

    /* renamed from: 靐  reason: contains not printable characters */
    final Hpack.Reader f16343 = new Hpack.Reader(4096, this.f16344);

    /* renamed from: 麤  reason: contains not printable characters */
    private final ContinuationSource f16344 = new ContinuationSource(this.f16345);

    /* renamed from: 齉  reason: contains not printable characters */
    private final BufferedSource f16345;

    static final class ContinuationSource implements Source {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final BufferedSource f16346;

        /* renamed from: 连任  reason: contains not printable characters */
        short f16347;

        /* renamed from: 靐  reason: contains not printable characters */
        byte f16348;

        /* renamed from: 麤  reason: contains not printable characters */
        int f16349;

        /* renamed from: 齉  reason: contains not printable characters */
        int f16350;

        /* renamed from: 龘  reason: contains not printable characters */
        int f16351;

        ContinuationSource(BufferedSource bufferedSource) {
            this.f16346 = bufferedSource;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m20298() throws IOException {
            int i = this.f16350;
            int r2 = Http2Reader.m20292(this.f16346);
            this.f16349 = r2;
            this.f16351 = r2;
            byte r1 = (byte) (this.f16346.m20460() & 255);
            this.f16348 = (byte) (this.f16346.m20460() & 255);
            if (Http2Reader.f16341.isLoggable(Level.FINE)) {
                Http2Reader.f16341.fine(Http2.m20215(true, this.f16350, this.f16351, r1, this.f16348));
            }
            this.f16350 = this.f16346.m20463() & MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
            if (r1 != 9) {
                throw Http2.m20212("%s != TYPE_CONTINUATION", Byte.valueOf(r1));
            } else if (this.f16350 != i) {
                throw Http2.m20212("TYPE_CONTINUATION streamId changed", new Object[0]);
            }
        }

        public void close() throws IOException {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m20299(Buffer buffer, long j) throws IOException {
            while (this.f16349 == 0) {
                this.f16346.m20461((long) this.f16347);
                this.f16347 = 0;
                if ((this.f16348 & 4) != 0) {
                    return -1;
                }
                m20298();
            }
            long r0 = this.f16346.m20568(buffer, Math.min(j, (long) this.f16349));
            if (r0 == -1) {
                return -1;
            }
            this.f16349 = (int) (((long) this.f16349) - r0);
            return r0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Timeout m20300() {
            return this.f16346.m20569();
        }
    }

    interface Handler {
        /* renamed from: 龘  reason: contains not printable characters */
        void m20301();

        /* renamed from: 龘  reason: contains not printable characters */
        void m20302(int i, int i2, int i3, boolean z);

        /* renamed from: 龘  reason: contains not printable characters */
        void m20303(int i, int i2, List<Header> list) throws IOException;

        /* renamed from: 龘  reason: contains not printable characters */
        void m20304(int i, long j);

        /* renamed from: 龘  reason: contains not printable characters */
        void m20305(int i, ErrorCode errorCode);

        /* renamed from: 龘  reason: contains not printable characters */
        void m20306(int i, ErrorCode errorCode, ByteString byteString);

        /* renamed from: 龘  reason: contains not printable characters */
        void m20307(boolean z, int i, int i2);

        /* renamed from: 龘  reason: contains not printable characters */
        void m20308(boolean z, int i, int i2, List<Header> list);

        /* renamed from: 龘  reason: contains not printable characters */
        void m20309(boolean z, int i, BufferedSource bufferedSource, int i2) throws IOException;

        /* renamed from: 龘  reason: contains not printable characters */
        void m20310(boolean z, Settings settings);
    }

    Http2Reader(BufferedSource bufferedSource, boolean z) {
        this.f16345 = bufferedSource;
        this.f16342 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m20283(Handler handler, int i, byte b, int i2) throws IOException {
        short s = 0;
        if (i2 == 0) {
            throw Http2.m20212("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        }
        if ((b & 8) != 0) {
            s = (short) (this.f16345.m20460() & 255);
        }
        handler.m20303(i2, this.f16345.m20463() & MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, m20293(m20291(i - 4, b, s), s, b, i2));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m20284(Handler handler, int i, byte b, int i2) throws IOException {
        boolean z = true;
        if (i != 8) {
            throw Http2.m20212("TYPE_PING length != 8: %s", Integer.valueOf(i));
        } else if (i2 != 0) {
            throw Http2.m20212("TYPE_PING streamId != 0", new Object[0]);
        } else {
            int r1 = this.f16345.m20463();
            int r2 = this.f16345.m20463();
            if ((b & 1) == 0) {
                z = false;
            }
            handler.m20307(z, r1, r2);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m20285(Handler handler, int i, byte b, int i2) throws IOException {
        if (i < 8) {
            throw Http2.m20212("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
        } else if (i2 != 0) {
            throw Http2.m20212("TYPE_GOAWAY streamId != 0", new Object[0]);
        } else {
            int r3 = this.f16345.m20463();
            int r2 = this.f16345.m20463();
            int i3 = i - 8;
            ErrorCode fromHttp2 = ErrorCode.fromHttp2(r2);
            if (fromHttp2 == null) {
                throw Http2.m20212("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(r2));
            }
            ByteString byteString = ByteString.EMPTY;
            if (i3 > 0) {
                byteString = this.f16345.m20465((long) i3);
            }
            handler.m20306(r3, fromHttp2, byteString);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m20286(Handler handler, int i, byte b, int i2) throws IOException {
        if (i != 4) {
            throw Http2.m20212("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
        }
        long r0 = ((long) this.f16345.m20463()) & 2147483647L;
        if (r0 == 0) {
            throw Http2.m20212("windowSizeIncrement was 0", Long.valueOf(r0));
        } else {
            handler.m20304(i2, r0);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m20287(Handler handler, int i, byte b, int i2) throws IOException {
        if (i2 != 0) {
            throw Http2.m20212("TYPE_SETTINGS streamId != 0", new Object[0]);
        } else if ((b & 1) != 0) {
            if (i != 0) {
                throw Http2.m20212("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
            }
            handler.m20301();
        } else if (i % 6 != 0) {
            throw Http2.m20212("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
        } else {
            Settings settings = new Settings();
            for (int i3 = 0; i3 < i; i3 += 6) {
                short r1 = this.f16345.m20462() & 65535;
                int r3 = this.f16345.m20463();
                switch (r1) {
                    case 2:
                        if (!(r3 == 0 || r3 == 1)) {
                            throw Http2.m20212("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                        }
                    case 3:
                        r1 = 4;
                        break;
                    case 4:
                        r1 = 7;
                        if (r3 >= 0) {
                            break;
                        } else {
                            throw Http2.m20212("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                        }
                    case 5:
                        if (r3 >= 16384 && r3 <= 16777215) {
                            break;
                        } else {
                            throw Http2.m20212("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(r3));
                        }
                }
                settings.m20377(r1, r3);
            }
            handler.m20310(false, settings);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m20288(Handler handler, int i, byte b, int i2) throws IOException {
        boolean z = true;
        short s = 0;
        if (i2 == 0) {
            throw Http2.m20212("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
        }
        boolean z2 = (b & 1) != 0;
        if ((b & 32) == 0) {
            z = false;
        }
        if (z) {
            throw Http2.m20212("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
        }
        if ((b & 8) != 0) {
            s = (short) (this.f16345.m20460() & 255);
        }
        handler.m20309(z2, i2, this.f16345, m20291(i, b, s));
        this.f16345.m20461((long) s);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m20289(Handler handler, int i, byte b, int i2) throws IOException {
        if (i != 4) {
            throw Http2.m20212("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
        } else if (i2 == 0) {
            throw Http2.m20212("TYPE_RST_STREAM streamId == 0", new Object[0]);
        } else {
            int r1 = this.f16345.m20463();
            ErrorCode fromHttp2 = ErrorCode.fromHttp2(r1);
            if (fromHttp2 == null) {
                throw Http2.m20212("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(r1));
            } else {
                handler.m20305(i2, fromHttp2);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m20290(Handler handler, int i, byte b, int i2) throws IOException {
        if (i != 5) {
            throw Http2.m20212("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
        } else if (i2 == 0) {
            throw Http2.m20212("TYPE_PRIORITY streamId == 0", new Object[0]);
        } else {
            m20294(handler, i2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m20291(int i, byte b, short s) throws IOException {
        if ((b & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        throw Http2.m20212("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m20292(BufferedSource bufferedSource) throws IOException {
        return ((bufferedSource.m20460() & 255) << 16) | ((bufferedSource.m20460() & 255) << 8) | (bufferedSource.m20460() & 255);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<Header> m20293(int i, short s, byte b, int i2) throws IOException {
        ContinuationSource continuationSource = this.f16344;
        this.f16344.f16349 = i;
        continuationSource.f16351 = i;
        this.f16344.f16347 = s;
        this.f16344.f16348 = b;
        this.f16344.f16350 = i2;
        this.f16343.m20203();
        return this.f16343.m20200();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20294(Handler handler, int i) throws IOException {
        int r2 = this.f16345.m20463();
        handler.m20302(i, r2 & MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, (this.f16345.m20460() & 255) + 1, (Integer.MIN_VALUE & r2) != 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20295(Handler handler, int i, byte b, int i2) throws IOException {
        short s = 0;
        if (i2 == 0) {
            throw Http2.m20212("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        }
        boolean z = (b & 1) != 0;
        if ((b & 8) != 0) {
            s = (short) (this.f16345.m20460() & 255);
        }
        if ((b & 32) != 0) {
            m20294(handler, i2);
            i -= 5;
        }
        handler.m20308(z, i2, -1, m20293(m20291(i, b, s), s, b, i2));
    }

    public void close() throws IOException {
        this.f16345.close();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20296(Handler handler) throws IOException {
        if (!this.f16342) {
            ByteString r0 = this.f16345.m20465((long) Http2.f16259.size());
            if (f16341.isLoggable(Level.FINE)) {
                f16341.fine(Util.m7116("<< CONNECTION %s", r0.hex()));
            }
            if (!Http2.f16259.equals(r0)) {
                throw Http2.m20212("Expected a connection header but was %s", r0.utf8());
            }
        } else if (!m20297(true, handler)) {
            throw Http2.m20212("Required SETTINGS preface not received", new Object[0]);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20297(boolean z, Handler handler) throws IOException {
        try {
            this.f16345.m20470(9);
            int r2 = m20292(this.f16345);
            if (r2 < 0 || r2 > 16384) {
                throw Http2.m20212("FRAME_SIZE_ERROR: %s", Integer.valueOf(r2));
            }
            byte r4 = (byte) (this.f16345.m20460() & 255);
            if (!z || r4 == 4) {
                byte r1 = (byte) (this.f16345.m20460() & 255);
                int r3 = this.f16345.m20463() & MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
                if (f16341.isLoggable(Level.FINE)) {
                    f16341.fine(Http2.m20215(true, r3, r2, r4, r1));
                }
                switch (r4) {
                    case 0:
                        m20288(handler, r2, r1, r3);
                        return true;
                    case 1:
                        m20295(handler, r2, r1, r3);
                        return true;
                    case 2:
                        m20290(handler, r2, r1, r3);
                        return true;
                    case 3:
                        m20289(handler, r2, r1, r3);
                        return true;
                    case 4:
                        m20287(handler, r2, r1, r3);
                        return true;
                    case 5:
                        m20283(handler, r2, r1, r3);
                        return true;
                    case 6:
                        m20284(handler, r2, r1, r3);
                        return true;
                    case 7:
                        m20285(handler, r2, r1, r3);
                        return true;
                    case 8:
                        m20286(handler, r2, r1, r3);
                        return true;
                    default:
                        this.f16345.m20461((long) r2);
                        return true;
                }
            } else {
                throw Http2.m20212("Expected a SETTINGS frame but was %s", Byte.valueOf(r4));
            }
        } catch (IOException e) {
            return false;
        }
    }
}
