package okhttp3.internal.http2;

import java.io.IOException;
import okhttp3.internal.Util;
import okio.ByteString;

public final class Http2 {

    /* renamed from: 靐  reason: contains not printable characters */
    static final String[] f16256 = new String[64];

    /* renamed from: 麤  reason: contains not printable characters */
    private static final String[] f16257 = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};

    /* renamed from: 齉  reason: contains not printable characters */
    static final String[] f16258 = new String[256];

    /* renamed from: 龘  reason: contains not printable characters */
    static final ByteString f16259 = ByteString.encodeUtf8("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");

    static {
        for (int i = 0; i < f16258.length; i++) {
            f16258[i] = Util.m7116("%8s", Integer.toBinaryString(i)).replace(' ', '0');
        }
        f16256[0] = "";
        f16256[1] = "END_STREAM";
        int[] iArr = {1};
        f16256[8] = "PADDED";
        for (int i2 : iArr) {
            f16256[i2 | 8] = f16256[i2] + "|PADDED";
        }
        f16256[4] = "END_HEADERS";
        f16256[32] = "PRIORITY";
        f16256[36] = "END_HEADERS|PRIORITY";
        for (int i3 : new int[]{4, 32, 36}) {
            for (int i4 : iArr) {
                f16256[i4 | i3] = f16256[i4] + '|' + f16256[i3];
                f16256[i4 | i3 | 8] = f16256[i4] + '|' + f16256[i3] + "|PADDED";
            }
        }
        for (int i5 = 0; i5 < f16256.length; i5++) {
            if (f16256[i5] == null) {
                f16256[i5] = f16258[i5];
            }
        }
    }

    private Http2() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static IOException m20212(String str, Object... objArr) throws IOException {
        throw new IOException(Util.m7116(str, objArr));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static IllegalArgumentException m20213(String str, Object... objArr) {
        throw new IllegalArgumentException(Util.m7116(str, objArr));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m20214(byte b, byte b2) {
        if (b2 == 0) {
            return "";
        }
        switch (b) {
            case 2:
            case 3:
            case 7:
            case 8:
                return f16258[b2];
            case 4:
            case 6:
                return b2 == 1 ? "ACK" : f16258[b2];
            default:
                String str = b2 < f16256.length ? f16256[b2] : f16258[b2];
                return (b != 5 || (b2 & 4) == 0) ? (b != 0 || (b2 & 32) == 0) ? str : str.replace("PRIORITY", "COMPRESSED") : str.replace("HEADERS", "PUSH_PROMISE");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m20215(boolean z, int i, int i2, byte b, byte b2) {
        String r1 = b < f16257.length ? f16257[b] : Util.m7116("0x%02x", Byte.valueOf(b));
        String r0 = m20214(b, b2);
        Object[] objArr = new Object[5];
        objArr[0] = z ? "<<" : ">>";
        objArr[1] = Integer.valueOf(i);
        objArr[2] = Integer.valueOf(i2);
        objArr[3] = r1;
        objArr[4] = r0;
        return Util.m7116("%s 0x%08x %5d %-13s %s", objArr);
    }
}
