package okhttp3.internal.http2;

import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import okio.AsyncTimeout;
import okio.Buffer;
import okio.BufferedSource;
import okio.Sink;
import okio.Source;
import okio.Timeout;

public final class Http2Stream {

    /* renamed from: ˑ  reason: contains not printable characters */
    static final /* synthetic */ boolean f16352 = (!Http2Stream.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    final StreamTimeout f16353 = new StreamTimeout();

    /* renamed from: ʼ  reason: contains not printable characters */
    final StreamTimeout f16354 = new StreamTimeout();

    /* renamed from: ʽ  reason: contains not printable characters */
    ErrorCode f16355 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final FramingSource f16356;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f16357;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final List<Header> f16358;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private List<Header> f16359;

    /* renamed from: 连任  reason: contains not printable characters */
    final FramingSink f16360;

    /* renamed from: 靐  reason: contains not printable characters */
    long f16361;

    /* renamed from: 麤  reason: contains not printable characters */
    final Http2Connection f16362;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f16363;

    /* renamed from: 龘  reason: contains not printable characters */
    long f16364 = 0;

    final class FramingSink implements Sink {

        /* renamed from: 齉  reason: contains not printable characters */
        static final /* synthetic */ boolean f16365 = (!Http2Stream.class.desiredAssertionStatus());

        /* renamed from: 连任  reason: contains not printable characters */
        private final Buffer f16366 = new Buffer();

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f16367;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f16369;

        FramingSink() {
        }

        /* JADX INFO: finally extract failed */
        /* renamed from: 龘  reason: contains not printable characters */
        private void m20330(boolean z) throws IOException {
            long min;
            synchronized (Http2Stream.this) {
                Http2Stream.this.f16354.m7202();
                while (Http2Stream.this.f16361 <= 0 && !this.f16367 && !this.f16369 && Http2Stream.this.f16355 == null) {
                    try {
                        Http2Stream.this.m20315();
                    } catch (Throwable th) {
                        Http2Stream.this.f16354.m20337();
                        throw th;
                    }
                }
                Http2Stream.this.f16354.m20337();
                Http2Stream.this.m20318();
                min = Math.min(Http2Stream.this.f16361, this.f16366.m7242());
                Http2Stream.this.f16361 -= min;
            }
            Http2Stream.this.f16354.m7202();
            try {
                Http2Stream.this.f16362.m20248(Http2Stream.this.f16363, z && min == this.f16366.m7242(), this.f16366, min);
            } finally {
                Http2Stream.this.f16354.m20337();
            }
        }

        public void a_(Buffer buffer, long j) throws IOException {
            if (f16365 || !Thread.holdsLock(Http2Stream.this)) {
                this.f16366.a_(buffer, j);
                while (this.f16366.m7242() >= 16384) {
                    m20330(false);
                }
                return;
            }
            throw new AssertionError();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
            if (r6.f16368.f16360.f16367 != false) goto L_0x004e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
            if (r6.f16366.m7242() <= 0) goto L_0x0042;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
            if (r6.f16366.m7242() <= 0) goto L_0x004e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
            m20330(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0042, code lost:
            r6.f16368.f16362.m20248(r6.f16368.f16363, true, (okio.Buffer) null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x004e, code lost:
            r1 = r6.f16368;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0050, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            r6.f16369 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0054, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0055, code lost:
            r6.f16368.f16362.m20234();
            r6.f16368.m20317();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() throws java.io.IOException {
            /*
                r6 = this;
                r4 = 0
                r2 = 1
                boolean r0 = f16365
                if (r0 != 0) goto L_0x0015
                okhttp3.internal.http2.Http2Stream r0 = okhttp3.internal.http2.Http2Stream.this
                boolean r0 = java.lang.Thread.holdsLock(r0)
                if (r0 == 0) goto L_0x0015
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x0015:
                okhttp3.internal.http2.Http2Stream r1 = okhttp3.internal.http2.Http2Stream.this
                monitor-enter(r1)
                boolean r0 = r6.f16369     // Catch:{ all -> 0x003f }
                if (r0 == 0) goto L_0x001e
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
            L_0x001d:
                return
            L_0x001e:
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                okhttp3.internal.http2.Http2Stream r0 = okhttp3.internal.http2.Http2Stream.this
                okhttp3.internal.http2.Http2Stream$FramingSink r0 = r0.f16360
                boolean r0 = r0.f16367
                if (r0 != 0) goto L_0x004e
                okio.Buffer r0 = r6.f16366
                long r0 = r0.m7242()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0042
            L_0x0031:
                okio.Buffer r0 = r6.f16366
                long r0 = r0.m7242()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x004e
                r6.m20330(r2)
                goto L_0x0031
            L_0x003f:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                throw r0
            L_0x0042:
                okhttp3.internal.http2.Http2Stream r0 = okhttp3.internal.http2.Http2Stream.this
                okhttp3.internal.http2.Http2Connection r0 = r0.f16362
                okhttp3.internal.http2.Http2Stream r1 = okhttp3.internal.http2.Http2Stream.this
                int r1 = r1.f16363
                r3 = 0
                r0.m20248((int) r1, (boolean) r2, (okio.Buffer) r3, (long) r4)
            L_0x004e:
                okhttp3.internal.http2.Http2Stream r1 = okhttp3.internal.http2.Http2Stream.this
                monitor-enter(r1)
                r0 = 1
                r6.f16369 = r0     // Catch:{ all -> 0x0062 }
                monitor-exit(r1)     // Catch:{ all -> 0x0062 }
                okhttp3.internal.http2.Http2Stream r0 = okhttp3.internal.http2.Http2Stream.this
                okhttp3.internal.http2.Http2Connection r0 = r0.f16362
                r0.m20234()
                okhttp3.internal.http2.Http2Stream r0 = okhttp3.internal.http2.Http2Stream.this
                r0.m20317()
                goto L_0x001d
            L_0x0062:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0062 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.http2.Http2Stream.FramingSink.close():void");
        }

        public void flush() throws IOException {
            if (f16365 || !Thread.holdsLock(Http2Stream.this)) {
                synchronized (Http2Stream.this) {
                    Http2Stream.this.m20318();
                }
                while (this.f16366.m7242() > 0) {
                    m20330(false);
                    Http2Stream.this.f16362.m20234();
                }
                return;
            }
            throw new AssertionError();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Timeout m20331() {
            return Http2Stream.this.f16354;
        }
    }

    private final class FramingSource implements Source {

        /* renamed from: 齉  reason: contains not printable characters */
        static final /* synthetic */ boolean f16370 = (!Http2Stream.class.desiredAssertionStatus());

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Buffer f16371 = new Buffer();

        /* renamed from: ʼ  reason: contains not printable characters */
        private final long f16372;

        /* renamed from: 连任  reason: contains not printable characters */
        private final Buffer f16373 = new Buffer();

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f16374;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f16376;

        FramingSource(long j) {
            this.f16372 = j;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m20332() throws IOException {
            Http2Stream.this.f16353.m7202();
            while (this.f16371.m7242() == 0 && !this.f16374 && !this.f16376 && Http2Stream.this.f16355 == null) {
                try {
                    Http2Stream.this.m20315();
                } finally {
                    Http2Stream.this.f16353.m20337();
                }
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private void m20333() throws IOException {
            if (this.f16376) {
                throw new IOException("stream closed");
            } else if (Http2Stream.this.f16355 != null) {
                throw new StreamResetException(Http2Stream.this.f16355);
            }
        }

        public void close() throws IOException {
            synchronized (Http2Stream.this) {
                this.f16376 = true;
                this.f16371.m7223();
                Http2Stream.this.notifyAll();
            }
            Http2Stream.this.m20317();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m20334(Buffer buffer, long j) throws IOException {
            long r0;
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            }
            synchronized (Http2Stream.this) {
                m20332();
                m20333();
                if (this.f16371.m7242() == 0) {
                    r0 = -1;
                } else {
                    r0 = this.f16371.m7260(buffer, Math.min(j, this.f16371.m7242()));
                    Http2Stream.this.f16364 += r0;
                    if (Http2Stream.this.f16364 >= ((long) (Http2Stream.this.f16362.f16292.m20373() / 2))) {
                        Http2Stream.this.f16362.m20243(Http2Stream.this.f16363, Http2Stream.this.f16364);
                        Http2Stream.this.f16364 = 0;
                    }
                    synchronized (Http2Stream.this.f16362) {
                        Http2Stream.this.f16362.f16290 += r0;
                        if (Http2Stream.this.f16362.f16290 >= ((long) (Http2Stream.this.f16362.f16292.m20373() / 2))) {
                            Http2Stream.this.f16362.m20243(0, Http2Stream.this.f16362.f16290);
                            Http2Stream.this.f16362.f16290 = 0;
                        }
                    }
                }
            }
            return r0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Timeout m20335() {
            return Http2Stream.this.f16353;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20336(BufferedSource bufferedSource, long j) throws IOException {
            boolean z;
            boolean z2;
            if (f16370 || !Thread.holdsLock(Http2Stream.this)) {
                while (j > 0) {
                    synchronized (Http2Stream.this) {
                        z = this.f16374;
                        z2 = this.f16371.m7242() + j > this.f16372;
                    }
                    if (z2) {
                        bufferedSource.m20461(j);
                        Http2Stream.this.m20320(ErrorCode.FLOW_CONTROL_ERROR);
                        return;
                    } else if (z) {
                        bufferedSource.m20461(j);
                        return;
                    } else {
                        long r2 = bufferedSource.m20568(this.f16373, j);
                        if (r2 == -1) {
                            throw new EOFException();
                        }
                        j -= r2;
                        synchronized (Http2Stream.this) {
                            boolean z3 = this.f16371.m7242() == 0;
                            this.f16371.m7262((Source) this.f16373);
                            if (z3) {
                                Http2Stream.this.notifyAll();
                            }
                        }
                    }
                }
                return;
            }
            throw new AssertionError();
        }
    }

    class StreamTimeout extends AsyncTimeout {
        StreamTimeout() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m20337() throws IOException {
            if (z_()) {
                throw m20338((IOException) null);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public IOException m20338(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20339() {
            Http2Stream.this.m20320(ErrorCode.CANCEL);
        }
    }

    Http2Stream(int i, Http2Connection http2Connection, boolean z, boolean z2, List<Header> list) {
        if (http2Connection == null) {
            throw new NullPointerException("connection == null");
        } else if (list == null) {
            throw new NullPointerException("requestHeaders == null");
        } else {
            this.f16363 = i;
            this.f16362 = http2Connection;
            this.f16361 = (long) http2Connection.f16286.m20373();
            this.f16356 = new FramingSource((long) http2Connection.f16292.m20373());
            this.f16360 = new FramingSink();
            this.f16356.f16374 = z2;
            this.f16360.f16367 = z;
            this.f16358 = list;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m20311(ErrorCode errorCode) {
        if (f16352 || !Thread.holdsLock(this)) {
            synchronized (this) {
                if (this.f16355 != null) {
                    return false;
                }
                if (this.f16356.f16374 && this.f16360.f16367) {
                    return false;
                }
                this.f16355 = errorCode;
                notifyAll();
                this.f16362.m20233(this.f16363);
                return true;
            }
        }
        throw new AssertionError();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Timeout m20312() {
        return this.f16354;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Source m20313() {
        return this.f16356;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Sink m20314() {
        synchronized (this) {
            if (!this.f16357 && !m20324()) {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.f16360;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m20315() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException e) {
            throw new InterruptedIOException();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m20316() {
        boolean r0;
        if (f16352 || !Thread.holdsLock(this)) {
            synchronized (this) {
                this.f16356.f16374 = true;
                r0 = m20321();
                notifyAll();
            }
            if (!r0) {
                this.f16362.m20233(this.f16363);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m20317() throws IOException {
        boolean z;
        boolean r1;
        if (f16352 || !Thread.holdsLock(this)) {
            synchronized (this) {
                z = !this.f16356.f16374 && this.f16356.f16376 && (this.f16360.f16367 || this.f16360.f16369);
                r1 = m20321();
            }
            if (z) {
                m20328(ErrorCode.CANCEL);
            } else if (!r1) {
                this.f16362.m20233(this.f16363);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m20318() throws IOException {
        if (this.f16360.f16369) {
            throw new IOException("stream closed");
        } else if (this.f16360.f16367) {
            throw new IOException("stream finished");
        } else if (this.f16355 != null) {
            throw new StreamResetException(this.f16355);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Timeout m20319() {
        return this.f16353;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m20320(ErrorCode errorCode) {
        if (m20311(errorCode)) {
            this.f16362.m20246(this.f16363, errorCode);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized boolean m20321() {
        boolean z = false;
        synchronized (this) {
            if (this.f16355 == null) {
                if ((!this.f16356.f16374 && !this.f16356.f16376) || ((!this.f16360.f16367 && !this.f16360.f16369) || !this.f16357)) {
                    z = true;
                }
            }
        }
        return z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized List<Header> m20322() throws IOException {
        List<Header> list;
        if (!m20324()) {
            throw new IllegalStateException("servers cannot read response headers");
        }
        this.f16353.m7202();
        while (this.f16359 == null && this.f16355 == null) {
            try {
                m20315();
            } catch (Throwable th) {
                this.f16353.m20337();
                throw th;
            }
        }
        this.f16353.m20337();
        list = this.f16359;
        if (list != null) {
            this.f16359 = null;
        } else {
            throw new StreamResetException(this.f16355);
        }
        return list;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized void m20323(ErrorCode errorCode) {
        if (this.f16355 == null) {
            this.f16355 = errorCode;
            notifyAll();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m20324() {
        return this.f16362.f16297 == ((this.f16363 & 1) == 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m20325() {
        return this.f16363;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20326(long j) {
        this.f16361 += j;
        if (j > 0) {
            notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20327(List<Header> list) {
        if (f16352 || !Thread.holdsLock(this)) {
            boolean z = true;
            synchronized (this) {
                this.f16357 = true;
                if (this.f16359 == null) {
                    this.f16359 = list;
                    z = m20321();
                    notifyAll();
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(this.f16359);
                    arrayList.add((Object) null);
                    arrayList.addAll(list);
                    this.f16359 = arrayList;
                }
            }
            if (!z) {
                this.f16362.m20233(this.f16363);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20328(ErrorCode errorCode) throws IOException {
        if (m20311(errorCode)) {
            this.f16362.m20235(this.f16363, errorCode);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20329(BufferedSource bufferedSource, int i) throws IOException {
        if (f16352 || !Thread.holdsLock(this)) {
            this.f16356.m20336(bufferedSource, (long) i);
            return;
        }
        throw new AssertionError();
    }
}
