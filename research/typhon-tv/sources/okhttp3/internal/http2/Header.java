package okhttp3.internal.http2;

import okhttp3.internal.Util;
import okio.ByteString;

public final class Header {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final ByteString f16227 = ByteString.encodeUtf8(":authority");

    /* renamed from: 连任  reason: contains not printable characters */
    public static final ByteString f16228 = ByteString.encodeUtf8(":scheme");

    /* renamed from: 靐  reason: contains not printable characters */
    public static final ByteString f16229 = ByteString.encodeUtf8(":status");

    /* renamed from: 麤  reason: contains not printable characters */
    public static final ByteString f16230 = ByteString.encodeUtf8(":path");

    /* renamed from: 齉  reason: contains not printable characters */
    public static final ByteString f16231 = ByteString.encodeUtf8(":method");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ByteString f16232 = ByteString.encodeUtf8(":");

    /* renamed from: ʼ  reason: contains not printable characters */
    public final ByteString f16233;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final ByteString f16234;

    /* renamed from: ˑ  reason: contains not printable characters */
    final int f16235;

    public Header(String str, String str2) {
        this(ByteString.encodeUtf8(str), ByteString.encodeUtf8(str2));
    }

    public Header(ByteString byteString, String str) {
        this(byteString, ByteString.encodeUtf8(str));
    }

    public Header(ByteString byteString, ByteString byteString2) {
        this.f16233 = byteString;
        this.f16234 = byteString2;
        this.f16235 = byteString.size() + 32 + byteString2.size();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Header)) {
            return false;
        }
        Header header = (Header) obj;
        return this.f16233.equals(header.f16233) && this.f16234.equals(header.f16234);
    }

    public int hashCode() {
        return ((this.f16233.hashCode() + 527) * 31) + this.f16234.hashCode();
    }

    public String toString() {
        return Util.m7116("%s: %s", this.f16233.utf8(), this.f16234.utf8());
    }
}
