package okhttp3.internal.cache;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Flushable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import net.pubnative.library.request.PubnativeRequest;
import okhttp3.internal.Util;
import okhttp3.internal.io.FileSystem;
import okhttp3.internal.platform.Platform;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import okio.Sink;
import okio.Source;
import org.apache.commons.lang3.StringUtils;

public final class DiskLruCache implements Closeable, Flushable {

    /* renamed from: ʾ  reason: contains not printable characters */
    static final /* synthetic */ boolean f6422 = (!DiskLruCache.class.desiredAssertionStatus());

    /* renamed from: 龘  reason: contains not printable characters */
    static final Pattern f6423 = Pattern.compile("[a-z0-9_-]{1,120}");

    /* renamed from: ʻ  reason: contains not printable characters */
    final LinkedHashMap<String, Entry> f6424 = new LinkedHashMap<>(0, 0.75f, true);

    /* renamed from: ʼ  reason: contains not printable characters */
    int f6425;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f6426;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final File f6427;

    /* renamed from: ˆ  reason: contains not printable characters */
    private long f6428 = 0;

    /* renamed from: ˈ  reason: contains not printable characters */
    boolean f6429;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final Executor f6430;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int f6431;

    /* renamed from: ˋ  reason: contains not printable characters */
    private long f6432;

    /* renamed from: ˎ  reason: contains not printable characters */
    private long f6433 = 0;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final Runnable f6434 = new Runnable() {
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x003a, code lost:
            r6.f16110.f6429 = true;
            r6.f16110.f6438 = okio.Okio.m20506(okio.Okio.m20508());
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r6 = this;
                r3 = 0
                r2 = 1
                okhttp3.internal.cache.DiskLruCache r4 = okhttp3.internal.cache.DiskLruCache.this
                monitor-enter(r4)
                okhttp3.internal.cache.DiskLruCache r5 = okhttp3.internal.cache.DiskLruCache.this     // Catch:{ all -> 0x002f }
                boolean r5 = r5.f6435     // Catch:{ all -> 0x002f }
                if (r5 != 0) goto L_0x0014
            L_0x000b:
                okhttp3.internal.cache.DiskLruCache r3 = okhttp3.internal.cache.DiskLruCache.this     // Catch:{ all -> 0x002f }
                boolean r3 = r3.f6436     // Catch:{ all -> 0x002f }
                r2 = r2 | r3
                if (r2 == 0) goto L_0x0016
                monitor-exit(r4)     // Catch:{ all -> 0x002f }
            L_0x0013:
                return
            L_0x0014:
                r2 = r3
                goto L_0x000b
            L_0x0016:
                okhttp3.internal.cache.DiskLruCache r2 = okhttp3.internal.cache.DiskLruCache.this     // Catch:{ IOException -> 0x0032 }
                r2.m7141()     // Catch:{ IOException -> 0x0032 }
            L_0x001b:
                okhttp3.internal.cache.DiskLruCache r2 = okhttp3.internal.cache.DiskLruCache.this     // Catch:{ IOException -> 0x0039 }
                boolean r2 = r2.m7145()     // Catch:{ IOException -> 0x0039 }
                if (r2 == 0) goto L_0x002d
                okhttp3.internal.cache.DiskLruCache r2 = okhttp3.internal.cache.DiskLruCache.this     // Catch:{ IOException -> 0x0039 }
                r2.m7143()     // Catch:{ IOException -> 0x0039 }
                okhttp3.internal.cache.DiskLruCache r2 = okhttp3.internal.cache.DiskLruCache.this     // Catch:{ IOException -> 0x0039 }
                r3 = 0
                r2.f6425 = r3     // Catch:{ IOException -> 0x0039 }
            L_0x002d:
                monitor-exit(r4)     // Catch:{ all -> 0x002f }
                goto L_0x0013
            L_0x002f:
                r2 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x002f }
                throw r2
            L_0x0032:
                r1 = move-exception
                okhttp3.internal.cache.DiskLruCache r2 = okhttp3.internal.cache.DiskLruCache.this     // Catch:{ all -> 0x002f }
                r3 = 1
                r2.f6437 = r3     // Catch:{ all -> 0x002f }
                goto L_0x001b
            L_0x0039:
                r0 = move-exception
                okhttp3.internal.cache.DiskLruCache r2 = okhttp3.internal.cache.DiskLruCache.this     // Catch:{ all -> 0x002f }
                r3 = 1
                r2.f6429 = r3     // Catch:{ all -> 0x002f }
                okhttp3.internal.cache.DiskLruCache r2 = okhttp3.internal.cache.DiskLruCache.this     // Catch:{ all -> 0x002f }
                okio.Sink r3 = okio.Okio.m20508()     // Catch:{ all -> 0x002f }
                okio.BufferedSink r3 = okio.Okio.m20506((okio.Sink) r3)     // Catch:{ all -> 0x002f }
                r2.f6438 = r3     // Catch:{ all -> 0x002f }
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.AnonymousClass1.run():void");
        }
    };

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean f6435;

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean f6436;

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean f6437;

    /* renamed from: 连任  reason: contains not printable characters */
    BufferedSink f6438;

    /* renamed from: 靐  reason: contains not printable characters */
    final FileSystem f6439;

    /* renamed from: 麤  reason: contains not printable characters */
    final int f6440;

    /* renamed from: 齉  reason: contains not printable characters */
    final File f6441;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final File f6442;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final File f6443;

    public final class Snapshot implements Closeable {

        /* renamed from: 连任  reason: contains not printable characters */
        private final long[] f6444;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f6445;

        /* renamed from: 麤  reason: contains not printable characters */
        private final Source[] f6446;

        /* renamed from: 齉  reason: contains not printable characters */
        private final long f6447;

        Snapshot(String str, long j, Source[] sourceArr, long[] jArr) {
            this.f6445 = str;
            this.f6447 = j;
            this.f6446 = sourceArr;
            this.f6444 = jArr;
        }

        public void close() {
            for (Source r0 : this.f6446) {
                Util.m7124((Closeable) r0);
            }
        }

        @Nullable
        /* renamed from: 龘  reason: contains not printable characters */
        public Editor m7152() throws IOException {
            return DiskLruCache.this.m7147(this.f6445, this.f6447);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Source m7153(int i) {
            return this.f6446[i];
        }
    }

    public final class Editor {

        /* renamed from: 靐  reason: contains not printable characters */
        final boolean[] f16113;

        /* renamed from: 麤  reason: contains not printable characters */
        private boolean f16114;

        /* renamed from: 龘  reason: contains not printable characters */
        final Entry f16116;

        Editor(Entry entry) {
            this.f16116 = entry;
            this.f16113 = entry.f16121 ? null : new boolean[DiskLruCache.this.f6440];
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m20033() throws IOException {
            synchronized (DiskLruCache.this) {
                if (this.f16114) {
                    throw new IllegalStateException();
                }
                if (this.f16116.f16118 == this) {
                    DiskLruCache.this.m7150(this, true);
                }
                this.f16114 = true;
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m20034() throws IOException {
            synchronized (DiskLruCache.this) {
                if (this.f16114) {
                    throw new IllegalStateException();
                }
                if (this.f16116.f16118 == this) {
                    DiskLruCache.this.m7150(this, false);
                }
                this.f16114 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Sink m20035(int i) {
            Sink r3;
            synchronized (DiskLruCache.this) {
                if (this.f16114) {
                    throw new IllegalStateException();
                } else if (this.f16116.f16118 != this) {
                    r3 = Okio.m20508();
                } else {
                    if (!this.f16116.f16121) {
                        this.f16113[i] = true;
                    }
                    try {
                        r3 = new FaultHidingSink(DiskLruCache.this.f6439.m20384(this.f16116.f16123[i])) {
                            /* access modifiers changed from: protected */
                            /* renamed from: 龘  reason: contains not printable characters */
                            public void m20037(IOException iOException) {
                                synchronized (DiskLruCache.this) {
                                    Editor.this.m20036();
                                }
                            }
                        };
                    } catch (FileNotFoundException e) {
                        r3 = Okio.m20508();
                    }
                }
            }
            return r3;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20036() {
            if (this.f16116.f16118 == this) {
                for (int i = 0; i < DiskLruCache.this.f6440; i++) {
                    try {
                        DiskLruCache.this.f6439.m20385(this.f16116.f16123[i]);
                    } catch (IOException e) {
                    }
                }
                this.f16116.f16118 = null;
            }
        }
    }

    private final class Entry {

        /* renamed from: ʻ  reason: contains not printable characters */
        Editor f16118;

        /* renamed from: ʼ  reason: contains not printable characters */
        long f16119;

        /* renamed from: 连任  reason: contains not printable characters */
        boolean f16121;

        /* renamed from: 靐  reason: contains not printable characters */
        final long[] f16122;

        /* renamed from: 麤  reason: contains not printable characters */
        final File[] f16123;

        /* renamed from: 齉  reason: contains not printable characters */
        final File[] f16124;

        /* renamed from: 龘  reason: contains not printable characters */
        final String f16125;

        Entry(String str) {
            this.f16125 = str;
            this.f16122 = new long[DiskLruCache.this.f6440];
            this.f16124 = new File[DiskLruCache.this.f6440];
            this.f16123 = new File[DiskLruCache.this.f6440];
            StringBuilder append = new StringBuilder(str).append('.');
            int length = append.length();
            for (int i = 0; i < DiskLruCache.this.f6440; i++) {
                append.append(i);
                this.f16124[i] = new File(DiskLruCache.this.f6441, append.toString());
                append.append(".tmp");
                this.f16123[i] = new File(DiskLruCache.this.f6441, append.toString());
                append.setLength(length);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private IOException m20038(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Snapshot m20039() {
            if (!Thread.holdsLock(DiskLruCache.this)) {
                throw new AssertionError();
            }
            Source[] sourceArr = new Source[DiskLruCache.this.f6440];
            long[] jArr = (long[]) this.f16122.clone();
            int i = 0;
            while (i < DiskLruCache.this.f6440) {
                try {
                    sourceArr[i] = DiskLruCache.this.f6439.m20387(this.f16124[i]);
                    i++;
                } catch (FileNotFoundException e) {
                    int i2 = 0;
                    while (i2 < DiskLruCache.this.f6440 && sourceArr[i2] != null) {
                        Util.m7124((Closeable) sourceArr[i2]);
                        i2++;
                    }
                    try {
                        DiskLruCache.this.m7151(this);
                    } catch (IOException e2) {
                    }
                    return null;
                }
            }
            return new Snapshot(this.f16125, this.f16119, sourceArr, jArr);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20040(BufferedSink bufferedSink) throws IOException {
            for (long r0 : this.f16122) {
                bufferedSink.m20444(32).m20439(r0);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20041(String[] strArr) throws IOException {
            if (strArr.length != DiskLruCache.this.f6440) {
                throw m20038(strArr);
            }
            int i = 0;
            while (i < strArr.length) {
                try {
                    this.f16122[i] = Long.parseLong(strArr[i]);
                    i++;
                } catch (NumberFormatException e) {
                    throw m20038(strArr);
                }
            }
        }
    }

    DiskLruCache(FileSystem fileSystem, File file, int i, int i2, long j, Executor executor) {
        this.f6439 = fileSystem;
        this.f6441 = file;
        this.f6431 = i;
        this.f6427 = new File(file, "journal");
        this.f6442 = new File(file, "journal.tmp");
        this.f6443 = new File(file, "journal.bkp");
        this.f6440 = i2;
        this.f6432 = j;
        this.f6430 = executor;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m7133() throws IOException {
        int i;
        BufferedSource r5 = Okio.m20507(this.f6439.m20387(this.f6427));
        try {
            String r4 = r5.m20459();
            String r7 = r5.m20459();
            String r0 = r5.m20459();
            String r6 = r5.m20459();
            String r1 = r5.m20459();
            if (!"libcore.io.DiskLruCache".equals(r4) || !PubnativeRequest.LEGACY_ZONE_ID.equals(r7) || !Integer.toString(this.f6431).equals(r0) || !Integer.toString(this.f6440).equals(r6) || !"".equals(r1)) {
                throw new IOException("unexpected journal header: [" + r4 + ", " + r7 + ", " + r6 + ", " + r1 + "]");
            }
            i = 0;
            while (true) {
                m7138(r5.m20459());
                i++;
            }
        } catch (EOFException e) {
            this.f6425 = i - this.f6424.size();
            if (!r5.m20452()) {
                m7143();
            } else {
                this.f6438 = m7134();
            }
            Util.m7124((Closeable) r5);
        } catch (Throwable th) {
            Util.m7124((Closeable) r5);
            throw th;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private BufferedSink m7134() throws FileNotFoundException {
        return Okio.m20506((Sink) new FaultHidingSink(this.f6439.m20386(this.f6427)) {

            /* renamed from: 龘  reason: contains not printable characters */
            static final /* synthetic */ boolean f16111 = (!DiskLruCache.class.desiredAssertionStatus());

            /* access modifiers changed from: protected */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m20032(IOException iOException) {
                if (f16111 || Thread.holdsLock(DiskLruCache.this)) {
                    DiskLruCache.this.f6426 = true;
                    return;
                }
                throw new AssertionError();
            }
        });
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m7135() throws IOException {
        this.f6439.m20385(this.f6442);
        Iterator<Entry> it2 = this.f6424.values().iterator();
        while (it2.hasNext()) {
            Entry next = it2.next();
            if (next.f16118 == null) {
                for (int i = 0; i < this.f6440; i++) {
                    this.f6433 += next.f16122[i];
                }
            } else {
                next.f16118 = null;
                for (int i2 = 0; i2 < this.f6440; i2++) {
                    this.f6439.m20385(next.f16124[i2]);
                    this.f6439.m20385(next.f16123[i2]);
                }
                it2.remove();
            }
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private synchronized void m7136() {
        if (m7144()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m7137(String str) {
        if (!f6423.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m7138(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf == -1) {
            throw new IOException("unexpected journal line: " + str);
        }
        int i = indexOf + 1;
        int indexOf2 = str.indexOf(32, i);
        if (indexOf2 == -1) {
            str2 = str.substring(i);
            if (indexOf == "REMOVE".length() && str.startsWith("REMOVE")) {
                this.f6424.remove(str2);
                return;
            }
        } else {
            str2 = str.substring(i, indexOf2);
        }
        Entry entry = this.f6424.get(str2);
        if (entry == null) {
            entry = new Entry(str2);
            this.f6424.put(str2, entry);
        }
        if (indexOf2 != -1 && indexOf == "CLEAN".length() && str.startsWith("CLEAN")) {
            String[] split = str.substring(indexOf2 + 1).split(StringUtils.SPACE);
            entry.f16121 = true;
            entry.f16118 = null;
            entry.m20041(split);
        } else if (indexOf2 == -1 && indexOf == "DIRTY".length() && str.startsWith("DIRTY")) {
            entry.f16118 = new Editor(entry);
        } else if (indexOf2 != -1 || indexOf != "READ".length() || !str.startsWith("READ")) {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DiskLruCache m7139(FileSystem fileSystem, File file, int i, int i2, long j) {
        if (j <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("valueCount <= 0");
        } else {
            return new DiskLruCache(fileSystem, file, i, i2, j, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), Util.m7122("OkHttp DiskLruCache", true)));
        }
    }

    public synchronized void close() throws IOException {
        if (!this.f6435 || this.f6436) {
            this.f6436 = true;
        } else {
            for (Entry entry : (Entry[]) this.f6424.values().toArray(new Entry[this.f6424.size()])) {
                if (entry.f16118 != null) {
                    entry.f16118.m20034();
                }
            }
            m7141();
            this.f6438.close();
            this.f6438 = null;
            this.f6436 = true;
        }
    }

    public synchronized void flush() throws IOException {
        if (this.f6435) {
            m7136();
            m7141();
            this.f6438.flush();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m7140() throws IOException {
        close();
        this.f6439.m20382(this.f6441);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m7141() throws IOException {
        while (this.f6433 > this.f6432) {
            m7151(this.f6424.values().iterator().next());
        }
        this.f6437 = false;
    }

    @Nullable
    /* renamed from: 靐  reason: contains not printable characters */
    public Editor m7142(String str) throws IOException {
        return m7147(str, -1);
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m7143() throws IOException {
        if (this.f6438 != null) {
            this.f6438.close();
        }
        BufferedSink r1 = Okio.m20506(this.f6439.m20384(this.f6442));
        try {
            r1.m20445("libcore.io.DiskLruCache").m20444(10);
            r1.m20445(PubnativeRequest.LEGACY_ZONE_ID).m20444(10);
            r1.m20439((long) this.f6431).m20444(10);
            r1.m20439((long) this.f6440).m20444(10);
            r1.m20444(10);
            for (Entry next : this.f6424.values()) {
                if (next.f16118 != null) {
                    r1.m20445("DIRTY").m20444(32);
                    r1.m20445(next.f16125);
                    r1.m20444(10);
                } else {
                    r1.m20445("CLEAN").m20444(32);
                    r1.m20445(next.f16125);
                    next.m20040(r1);
                    r1.m20444(10);
                }
            }
            r1.close();
            if (this.f6439.m20383(this.f6427)) {
                this.f6439.m20388(this.f6427, this.f6443);
            }
            this.f6439.m20388(this.f6442, this.f6427);
            this.f6439.m20385(this.f6443);
            this.f6438 = m7134();
            this.f6426 = false;
            this.f6429 = false;
        } catch (Throwable th) {
            r1.close();
            throw th;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized boolean m7144() {
        return this.f6436;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m7145() {
        return this.f6425 >= 2000 && this.f6425 >= this.f6424.size();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized boolean m7146(String str) throws IOException {
        boolean z = false;
        synchronized (this) {
            m7149();
            m7136();
            m7137(str);
            Entry entry = this.f6424.get(str);
            if (entry != null) {
                z = m7151(entry);
                if (z && this.f6433 <= this.f6432) {
                    this.f6437 = false;
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0027, code lost:
        if (r1.f16118 == null) goto L_0x0029;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized okhttp3.internal.cache.DiskLruCache.Editor m7147(java.lang.String r5, long r6) throws java.io.IOException {
        /*
            r4 = this;
            r0 = 0
            monitor-enter(r4)
            r4.m7149()     // Catch:{ all -> 0x0039 }
            r4.m7136()     // Catch:{ all -> 0x0039 }
            r4.m7137(r5)     // Catch:{ all -> 0x0039 }
            java.util.LinkedHashMap<java.lang.String, okhttp3.internal.cache.DiskLruCache$Entry> r2 = r4.f6424     // Catch:{ all -> 0x0039 }
            java.lang.Object r1 = r2.get(r5)     // Catch:{ all -> 0x0039 }
            okhttp3.internal.cache.DiskLruCache$Entry r1 = (okhttp3.internal.cache.DiskLruCache.Entry) r1     // Catch:{ all -> 0x0039 }
            r2 = -1
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x0023
            if (r1 == 0) goto L_0x0021
            long r2 = r1.f16119     // Catch:{ all -> 0x0039 }
            int r2 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x0023
        L_0x0021:
            monitor-exit(r4)
            return r0
        L_0x0023:
            if (r1 == 0) goto L_0x0029
            okhttp3.internal.cache.DiskLruCache$Editor r2 = r1.f16118     // Catch:{ all -> 0x0039 }
            if (r2 != 0) goto L_0x0021
        L_0x0029:
            boolean r2 = r4.f6437     // Catch:{ all -> 0x0039 }
            if (r2 != 0) goto L_0x0031
            boolean r2 = r4.f6429     // Catch:{ all -> 0x0039 }
            if (r2 == 0) goto L_0x003c
        L_0x0031:
            java.util.concurrent.Executor r2 = r4.f6430     // Catch:{ all -> 0x0039 }
            java.lang.Runnable r3 = r4.f6434     // Catch:{ all -> 0x0039 }
            r2.execute(r3)     // Catch:{ all -> 0x0039 }
            goto L_0x0021
        L_0x0039:
            r2 = move-exception
            monitor-exit(r4)
            throw r2
        L_0x003c:
            okio.BufferedSink r2 = r4.f6438     // Catch:{ all -> 0x0039 }
            java.lang.String r3 = "DIRTY"
            okio.BufferedSink r2 = r2.m20445((java.lang.String) r3)     // Catch:{ all -> 0x0039 }
            r3 = 32
            okio.BufferedSink r2 = r2.m20444(r3)     // Catch:{ all -> 0x0039 }
            okio.BufferedSink r2 = r2.m20445((java.lang.String) r5)     // Catch:{ all -> 0x0039 }
            r3 = 10
            r2.m20444(r3)     // Catch:{ all -> 0x0039 }
            okio.BufferedSink r2 = r4.f6438     // Catch:{ all -> 0x0039 }
            r2.flush()     // Catch:{ all -> 0x0039 }
            boolean r2 = r4.f6426     // Catch:{ all -> 0x0039 }
            if (r2 != 0) goto L_0x0021
            if (r1 != 0) goto L_0x0069
            okhttp3.internal.cache.DiskLruCache$Entry r1 = new okhttp3.internal.cache.DiskLruCache$Entry     // Catch:{ all -> 0x0039 }
            r1.<init>(r5)     // Catch:{ all -> 0x0039 }
            java.util.LinkedHashMap<java.lang.String, okhttp3.internal.cache.DiskLruCache$Entry> r2 = r4.f6424     // Catch:{ all -> 0x0039 }
            r2.put(r5, r1)     // Catch:{ all -> 0x0039 }
        L_0x0069:
            okhttp3.internal.cache.DiskLruCache$Editor r0 = new okhttp3.internal.cache.DiskLruCache$Editor     // Catch:{ all -> 0x0039 }
            r0.<init>(r1)     // Catch:{ all -> 0x0039 }
            r1.f16118 = r0     // Catch:{ all -> 0x0039 }
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.cache.DiskLruCache.m7147(java.lang.String, long):okhttp3.internal.cache.DiskLruCache$Editor");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Snapshot m7148(String str) throws IOException {
        Snapshot snapshot;
        m7149();
        m7136();
        m7137(str);
        Entry entry = this.f6424.get(str);
        if (entry == null || !entry.f16121) {
            snapshot = null;
        } else {
            snapshot = entry.m20039();
            if (snapshot == null) {
                snapshot = null;
            } else {
                this.f6425++;
                this.f6438.m20445("READ").m20444(32).m20445(str).m20444(10);
                if (m7145()) {
                    this.f6430.execute(this.f6434);
                }
            }
        }
        return snapshot;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m7149() throws IOException {
        if (!f6422 && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (!this.f6435) {
            if (this.f6439.m20383(this.f6443)) {
                if (this.f6439.m20383(this.f6427)) {
                    this.f6439.m20385(this.f6443);
                } else {
                    this.f6439.m20388(this.f6443, this.f6427);
                }
            }
            if (this.f6439.m20383(this.f6427)) {
                try {
                    m7133();
                    m7135();
                    this.f6435 = true;
                } catch (IOException e) {
                    Platform.m7184().m7193(5, "DiskLruCache " + this.f6441 + " is corrupt: " + e.getMessage() + ", removing", (Throwable) e);
                    m7140();
                    this.f6436 = false;
                } catch (Throwable th) {
                    this.f6436 = false;
                    throw th;
                }
            }
            m7143();
            this.f6435 = true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m7150(Editor editor, boolean z) throws IOException {
        Entry entry = editor.f16116;
        if (entry.f16118 != editor) {
            throw new IllegalStateException();
        }
        if (z) {
            if (!entry.f16121) {
                int i = 0;
                while (true) {
                    if (i >= this.f6440) {
                        break;
                    } else if (!editor.f16113[i]) {
                        editor.m20034();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i);
                    } else if (!this.f6439.m20383(entry.f16123[i])) {
                        editor.m20034();
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        for (int i2 = 0; i2 < this.f6440; i2++) {
            File file = entry.f16123[i2];
            if (!z) {
                this.f6439.m20385(file);
            } else if (this.f6439.m20383(file)) {
                File file2 = entry.f16124[i2];
                this.f6439.m20388(file, file2);
                long j = entry.f16122[i2];
                long r4 = this.f6439.m20381(file2);
                entry.f16122[i2] = r4;
                this.f6433 = (this.f6433 - j) + r4;
            }
        }
        this.f6425++;
        entry.f16118 = null;
        if (entry.f16121 || z) {
            entry.f16121 = true;
            this.f6438.m20445("CLEAN").m20444(32);
            this.f6438.m20445(entry.f16125);
            entry.m20040(this.f6438);
            this.f6438.m20444(10);
            if (z) {
                long j2 = this.f6428;
                this.f6428 = 1 + j2;
                entry.f16119 = j2;
            }
        } else {
            this.f6424.remove(entry.f16125);
            this.f6438.m20445("REMOVE").m20444(32);
            this.f6438.m20445(entry.f16125);
            this.f6438.m20444(10);
        }
        this.f6438.flush();
        if (this.f6433 > this.f6432 || m7145()) {
            this.f6430.execute(this.f6434);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m7151(Entry entry) throws IOException {
        if (entry.f16118 != null) {
            entry.f16118.m20036();
        }
        for (int i = 0; i < this.f6440; i++) {
            this.f6439.m20385(entry.f16124[i]);
            this.f6433 -= entry.f16122[i];
            entry.f16122[i] = 0;
        }
        this.f6425++;
        this.f6438.m20445("REMOVE").m20444(32).m20445(entry.f16125).m20444(10);
        this.f6424.remove(entry.f16125);
        if (!m7145()) {
            return true;
        }
        this.f6430.execute(this.f6434);
        return true;
    }
}
