package okhttp3.internal.cache;

import java.io.IOException;
import okio.Buffer;
import okio.ForwardingSink;
import okio.Sink;

class FaultHidingSink extends ForwardingSink {

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f16126;

    FaultHidingSink(Sink sink) {
        super(sink);
    }

    public void a_(Buffer buffer, long j) throws IOException {
        if (this.f16126) {
            buffer.m7230(j);
            return;
        }
        try {
            super.a_(buffer, j);
        } catch (IOException e) {
            this.f16126 = true;
            m20042(e);
        }
    }

    public void close() throws IOException {
        if (!this.f16126) {
            try {
                super.close();
            } catch (IOException e) {
                this.f16126 = true;
                m20042(e);
            }
        }
    }

    public void flush() throws IOException {
        if (!this.f16126) {
            try {
                super.flush();
            } catch (IOException e) {
                this.f16126 = true;
                m20042(e);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20042(IOException iOException) {
    }
}
