package okhttp3.internal.cache;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import net.pubnative.library.request.PubnativeRequest;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.cache.CacheStrategy;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.HttpMethod;
import okhttp3.internal.http.RealResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import okio.Sink;
import okio.Source;
import okio.Timeout;
import org.apache.oltu.oauth2.common.OAuth;

public final class CacheInterceptor implements Interceptor {

    /* renamed from: 龘  reason: contains not printable characters */
    final InternalCache f16092;

    public CacheInterceptor(InternalCache internalCache) {
        this.f16092 = internalCache;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static boolean m20017(String str) {
        return "Content-Length".equalsIgnoreCase(str) || "Content-Encoding".equalsIgnoreCase(str) || OAuth.HeaderType.CONTENT_TYPE.equalsIgnoreCase(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Headers m20018(Headers headers, Headers headers2) {
        Headers.Builder builder = new Headers.Builder();
        int r3 = headers.m6934();
        for (int i = 0; i < r3; i++) {
            String r0 = headers.m6935(i);
            String r4 = headers.m6930(i);
            if ((!"Warning".equalsIgnoreCase(r0) || !r4.startsWith(PubnativeRequest.LEGACY_ZONE_ID)) && (m20017(r0) || !m20021(r0) || headers2.m6936(r0) == null)) {
                Internal.f16088.m20011(builder, r0, r4);
            }
        }
        int r32 = headers2.m6934();
        for (int i2 = 0; i2 < r32; i2++) {
            String r02 = headers2.m6935(i2);
            if (!m20017(r02) && m20021(r02)) {
                Internal.f16088.m20011(builder, r02, headers2.m6930(i2));
            }
        }
        return builder.m19955();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Response m20019(Response response) {
        return (response == null || response.m7056() == null) ? response : response.m7060().m7086((ResponseBody) null).m7087();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Response m20020(final CacheRequest cacheRequest, Response response) throws IOException {
        Sink r1;
        if (cacheRequest == null || (r1 = cacheRequest.m20024()) == null) {
            return response;
        }
        final BufferedSource r6 = response.m7056().m7095();
        final BufferedSink r0 = Okio.m20506(r1);
        AnonymousClass1 r2 = new Source() {

            /* renamed from: 龘  reason: contains not printable characters */
            boolean f16097;

            public void close() throws IOException {
                if (!this.f16097 && !Util.m7129((Source) this, 100, TimeUnit.MILLISECONDS)) {
                    this.f16097 = true;
                    cacheRequest.m20025();
                }
                r6.close();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public long m20022(Buffer buffer, long j) throws IOException {
                try {
                    long r4 = r6.m20568(buffer, j);
                    if (r4 == -1) {
                        if (!this.f16097) {
                            this.f16097 = true;
                            r0.close();
                        }
                        return -1;
                    }
                    buffer.m7269(r0.m20447(), buffer.m7242() - r4, r4);
                    r0.m20442();
                    return r4;
                } catch (IOException e) {
                    if (!this.f16097) {
                        this.f16097 = true;
                        cacheRequest.m20025();
                    }
                    throw e;
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Timeout m20023() {
                return r6.m20569();
            }
        };
        return response.m7060().m7086((ResponseBody) new RealResponseBody(response.m7067(OAuth.HeaderType.CONTENT_TYPE), response.m7056().m7093(), Okio.m20507((Source) r2))).m7087();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m20021(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response r0 = this.f16092 != null ? this.f16092.m20044(chain.m6994()) : null;
        CacheStrategy r8 = new CacheStrategy.Factory(System.currentTimeMillis(), chain.m6994(), r0).m20031();
        Request request = r8.f6421;
        Response response = r8.f6420;
        if (this.f16092 != null) {
            this.f16092.m20048(r8);
        }
        if (r0 != null && response == null) {
            Util.m7124((Closeable) r0.m7056());
        }
        if (request == null && response == null) {
            return new Response.Builder().m7084(chain.m6994()).m7083(Protocol.HTTP_1_1).m7077(504).m7079("Unsatisfiable Request (only-if-cached)").m7086(Util.f6416).m7078(-1).m7072(System.currentTimeMillis()).m7087();
        }
        if (request == null) {
            return response.m7060().m7075(m20019(response)).m7087();
        }
        boolean z = false;
        try {
            z = chain.m6995(request);
            if (response != null) {
                if (z.m7066() == 304) {
                    Response r5 = response.m7060().m7082(m20018(response.m7055(), z.m7055())).m7078(z.m7057()).m7072(z.m7058()).m7075(m20019(response)).m7085(m20019(z)).m7087();
                    z.m7056().close();
                    this.f16092.m20046();
                    this.f16092.m20047(response, r5);
                    return r5;
                }
                Util.m7124((Closeable) response.m7056());
            }
            Response r52 = z.m7060().m7075(m20019(response)).m7085(m20019(z)).m7087();
            if (this.f16092 == null) {
                return r52;
            }
            if (HttpHeaders.m20120(r52) && CacheStrategy.m7132(r52, request)) {
                return m20020(this.f16092.m20045(r52), r52);
            }
            if (!HttpMethod.m20135(request.m7048())) {
                return r52;
            }
            try {
                this.f16092.m20043(request);
                return r52;
            } catch (IOException e) {
                return r52;
            }
        } finally {
            if (!z && r0 != null) {
                Util.m7124((Closeable) r0.m7056());
            }
        }
    }
}
