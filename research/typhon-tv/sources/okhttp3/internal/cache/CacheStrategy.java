package okhttp3.internal.cache;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import okhttp3.CacheControl;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Internal;
import okhttp3.internal.http.HttpDate;
import okhttp3.internal.http.HttpHeaders;

public final class CacheStrategy {
    @Nullable

    /* renamed from: 靐  reason: contains not printable characters */
    public final Response f6420;
    @Nullable

    /* renamed from: 龘  reason: contains not printable characters */
    public final Request f6421;

    public static class Factory {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Date f16098;

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f16099;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Date f16100;

        /* renamed from: ˈ  reason: contains not printable characters */
        private int f16101 = -1;

        /* renamed from: ˑ  reason: contains not printable characters */
        private long f16102;

        /* renamed from: ٴ  reason: contains not printable characters */
        private long f16103;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private String f16104;

        /* renamed from: 连任  reason: contains not printable characters */
        private String f16105;

        /* renamed from: 靐  reason: contains not printable characters */
        final Request f16106;

        /* renamed from: 麤  reason: contains not printable characters */
        private Date f16107;

        /* renamed from: 齉  reason: contains not printable characters */
        final Response f16108;

        /* renamed from: 龘  reason: contains not printable characters */
        final long f16109;

        public Factory(long j, Request request, Response response) {
            this.f16109 = j;
            this.f16106 = request;
            this.f16108 = response;
            if (response != null) {
                this.f16102 = response.m7057();
                this.f16103 = response.m7058();
                Headers r1 = response.m7055();
                int r3 = r1.m6934();
                for (int i = 0; i < r3; i++) {
                    String r0 = r1.m6935(i);
                    String r4 = r1.m6930(i);
                    if ("Date".equalsIgnoreCase(r0)) {
                        this.f16107 = HttpDate.m20114(r4);
                        this.f16105 = r4;
                    } else if ("Expires".equalsIgnoreCase(r0)) {
                        this.f16100 = HttpDate.m20114(r4);
                    } else if ("Last-Modified".equalsIgnoreCase(r0)) {
                        this.f16098 = HttpDate.m20114(r4);
                        this.f16099 = r4;
                    } else if ("ETag".equalsIgnoreCase(r0)) {
                        this.f16104 = r4;
                    } else if ("Age".equalsIgnoreCase(r0)) {
                        this.f16101 = HttpHeaders.m20117(r4, -1);
                    }
                }
            }
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean m20026() {
            return this.f16108.m7059().m6869() == -1 && this.f16100 == null;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private CacheStrategy m20027() {
            String str;
            String str2;
            if (this.f16108 == null) {
                return new CacheStrategy(this.f16106, (Response) null);
            }
            if (this.f16106.m7046() && this.f16108.m7054() == null) {
                return new CacheStrategy(this.f16106, (Response) null);
            }
            if (!CacheStrategy.m7132(this.f16108, this.f16106)) {
                return new CacheStrategy(this.f16106, (Response) null);
            }
            CacheControl r9 = this.f16106.m7045();
            if (r9.m6870() || m20030(this.f16106)) {
                return new CacheStrategy(this.f16106, (Response) null);
            }
            CacheControl r18 = this.f16108.m7059();
            if (r18.m6865()) {
                return new CacheStrategy((Request) null, this.f16108);
            }
            long r2 = m20028();
            long r10 = m20029();
            if (r9.m6869() != -1) {
                r10 = Math.min(r10, TimeUnit.SECONDS.toMillis((long) r9.m6869()));
            }
            long j = 0;
            if (r9.m6863() != -1) {
                j = TimeUnit.SECONDS.toMillis((long) r9.m6863());
            }
            long j2 = 0;
            if (!r18.m6861() && r9.m6862() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) r9.m6862());
            }
            if (r18.m6870() || r2 + j >= r10 + j2) {
                if (this.f16104 != null) {
                    str = "If-None-Match";
                    str2 = this.f16104;
                } else if (this.f16098 != null) {
                    str = "If-Modified-Since";
                    str2 = this.f16099;
                } else if (this.f16107 == null) {
                    return new CacheStrategy(this.f16106, (Response) null);
                } else {
                    str = "If-Modified-Since";
                    str2 = this.f16105;
                }
                Headers.Builder r8 = this.f16106.m7051().m6932();
                Internal.f16088.m20011(r8, str, str2);
                return new CacheStrategy(this.f16106.m7044().m19996(r8.m19955()).m19989(), this.f16108);
            }
            Response.Builder r4 = this.f16108.m7060();
            if (r2 + j >= r10) {
                r4.m7074("Warning", "110 HttpURLConnection \"Response is stale\"");
            }
            if (r2 > 86400000 && m20026()) {
                r4.m7074("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
            }
            return new CacheStrategy((Request) null, r4.m7087());
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private long m20028() {
            long j = 0;
            if (this.f16107 != null) {
                j = Math.max(0, this.f16103 - this.f16107.getTime());
            }
            return (this.f16101 != -1 ? Math.max(j, TimeUnit.SECONDS.toMillis((long) this.f16101)) : j) + (this.f16103 - this.f16102) + (this.f16109 - this.f16103);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private long m20029() {
            CacheControl r2 = this.f16108.m7059();
            if (r2.m6869() != -1) {
                return TimeUnit.SECONDS.toMillis((long) r2.m6869());
            }
            if (this.f16100 != null) {
                long time = this.f16100.getTime() - (this.f16107 != null ? this.f16107.getTime() : this.f16103);
                if (time <= 0) {
                    time = 0;
                }
                return time;
            } else if (this.f16098 == null || this.f16108.m7069().m7053().m6956() != null) {
                return 0;
            } else {
                long time2 = (this.f16107 != null ? this.f16107.getTime() : this.f16102) - this.f16098.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static boolean m20030(Request request) {
            return (request.m7052("If-Modified-Since") == null && request.m7052("If-None-Match") == null) ? false : true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public CacheStrategy m20031() {
            CacheStrategy r0 = m20027();
            return (r0.f6421 == null || !this.f16106.m7045().m6864()) ? r0 : new CacheStrategy((Request) null, (Response) null);
        }
    }

    CacheStrategy(Request request, Response response) {
        this.f6421 = request;
        this.f6420 = response;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m7132(Response response, Request request) {
        switch (response.m7066()) {
            case 200:
            case 203:
            case 204:
            case 300:
            case 301:
            case 308:
            case 404:
            case 405:
            case 410:
            case 414:
            case 501:
                break;
            case 302:
            case 307:
                if (response.m7067("Expires") == null && response.m7059().m6869() == -1 && !response.m7059().m6866() && !response.m7059().m6868()) {
                    return false;
                }
            default:
                return false;
        }
        return !response.m7059().m6867() && !request.m7045().m6867();
    }
}
