package okhttp3.internal.cache;

import java.io.IOException;
import okhttp3.Request;
import okhttp3.Response;

public interface InternalCache {
    /* renamed from: 靐  reason: contains not printable characters */
    void m20043(Request request) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    Response m20044(Request request) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    CacheRequest m20045(Response response) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m20046();

    /* renamed from: 龘  reason: contains not printable characters */
    void m20047(Response response, Response response2);

    /* renamed from: 龘  reason: contains not printable characters */
    void m20048(CacheStrategy cacheStrategy);
}
