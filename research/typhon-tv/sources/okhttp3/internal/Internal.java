package okhttp3.internal;

import java.net.Socket;
import javax.net.ssl.SSLSocket;
import okhttp3.Address;
import okhttp3.ConnectionPool;
import okhttp3.ConnectionSpec;
import okhttp3.Headers;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.RouteDatabase;
import okhttp3.internal.connection.StreamAllocation;

public abstract class Internal {

    /* renamed from: 龘  reason: contains not printable characters */
    public static Internal f16088;

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m20004(ConnectionPool connectionPool, RealConnection realConnection);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m20005(Response.Builder builder);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Socket m20006(ConnectionPool connectionPool, Address address, StreamAllocation streamAllocation);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract RealConnection m20007(ConnectionPool connectionPool, Address address, StreamAllocation streamAllocation, Route route);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract RouteDatabase m20008(ConnectionPool connectionPool);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m20009(ConnectionSpec connectionSpec, SSLSocket sSLSocket, boolean z);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m20010(Headers.Builder builder, String str);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m20011(Headers.Builder builder, String str, String str2);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m20012(Address address, Address address2);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m20013(ConnectionPool connectionPool, RealConnection realConnection);
}
