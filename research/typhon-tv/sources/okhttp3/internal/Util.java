package okhttp3.internal;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.IDN;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import okio.ByteString;
import okio.Source;
import org.apache.commons.lang3.time.TimeZones;

public final class Util {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final Charset f6402 = Charset.forName("ISO-8859-1");

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final TimeZone f6403 = TimeZone.getTimeZone(TimeZones.GMT_ID);

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final Comparator<String> f6404 = new Comparator<String>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(String str, String str2) {
            return str.compareTo(str2);
        }
    };

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final ByteString f6405 = ByteString.decodeHex("ffff0000");

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final Charset f6406 = Charset.forName("UTF-16BE");

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final ByteString f6407 = ByteString.decodeHex("0000ffff");

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final Charset f6408 = Charset.forName("UTF-32LE");

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final Pattern f6409 = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final ByteString f6410 = ByteString.decodeHex("efbbbf");

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final ByteString f6411 = ByteString.decodeHex("feff");

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final ByteString f6412 = ByteString.decodeHex("fffe");

    /* renamed from: 连任  reason: contains not printable characters */
    public static final Charset f6413 = Charset.forName("UTF-8");

    /* renamed from: 靐  reason: contains not printable characters */
    public static final String[] f6414 = new String[0];

    /* renamed from: 麤  reason: contains not printable characters */
    public static final RequestBody f6415 = RequestBody.create((MediaType) null, f6417);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final ResponseBody f6416 = ResponseBody.m7090((MediaType) null, f6417);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final byte[] f6417 = new byte[0];

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final Charset f6418 = Charset.forName("UTF-16LE");

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static final Charset f6419 = Charset.forName("UTF-32BE");

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m7100(String str) {
        int i = 0;
        int length = str.length();
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m7101(String str, int i, int i2) {
        int i3 = i2 - 1;
        while (i3 >= i) {
            switch (str.charAt(i3)) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    i3--;
                default:
                    return i3 + 1;
            }
        }
        return i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m7102(Comparator<String> comparator, String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length == 0 || strArr2.length == 0) {
            return false;
        }
        for (String str : strArr) {
            for (String compare : strArr2) {
                if (comparator.compare(str, compare) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m7103(Source source, int i, TimeUnit timeUnit) throws IOException {
        long nanoTime = System.nanoTime();
        long r4 = source.m20569().B_() ? source.m20569().m20572() - nanoTime : Long.MAX_VALUE;
        source.m20569().m20573(Math.min(r4, timeUnit.toNanos((long) i)) + nanoTime);
        try {
            Buffer buffer = new Buffer();
            while (source.m20568(buffer, PlaybackStateCompat.ACTION_PLAY_FROM_URI) != -1) {
                buffer.m7223();
            }
            if (r4 == Long.MAX_VALUE) {
                source.m20569().m20570();
            } else {
                source.m20569().m20573(nanoTime + r4);
            }
            return true;
        } catch (InterruptedIOException e) {
            if (r4 == Long.MAX_VALUE) {
                source.m20569().m20570();
            } else {
                source.m20569().m20573(nanoTime + r4);
            }
            return false;
        } catch (Throwable th) {
            if (r4 == Long.MAX_VALUE) {
                source.m20569().m20570();
            } else {
                source.m20569().m20573(nanoTime + r4);
            }
            throw th;
        }
    }

    @Nullable
    /* renamed from: 麤  reason: contains not printable characters */
    private static InetAddress m7104(String str, int i, int i2) {
        int r9;
        byte[] bArr = new byte[16];
        int i3 = 0;
        int i4 = -1;
        int i5 = -1;
        int i6 = i;
        while (true) {
            if (i6 >= i2) {
                break;
            } else if (i3 == bArr.length) {
                return null;
            } else {
                if (i6 + 2 <= i2 && str.regionMatches(i6, "::", 0, 2)) {
                    if (i4 == -1) {
                        i6 += 2;
                        i3 += 2;
                        i4 = i3;
                        if (i6 == i2) {
                            break;
                        }
                    } else {
                        return null;
                    }
                } else if (i3 != 0) {
                    if (str.regionMatches(i6, ":", 0, 1)) {
                        i6++;
                    } else if (!str.regionMatches(i6, ".", 0, 1)) {
                        return null;
                    } else {
                        if (!m7128(str, i5, i2, bArr, i3 - 2)) {
                            return null;
                        }
                        i3 += 2;
                    }
                }
                int i7 = 0;
                i5 = i6;
                while (i6 < i2 && (r9 = m7108(str.charAt(i6))) != -1) {
                    i7 = (i7 << 4) + r9;
                    i6++;
                }
                int i8 = i6 - i5;
                if (i8 == 0 || i8 > 4) {
                    return null;
                }
                int i9 = i3 + 1;
                bArr[i3] = (byte) ((i7 >>> 8) & 255);
                i3 = i9 + 1;
                bArr[i9] = (byte) (i7 & 255);
            }
        }
        if (i3 != bArr.length) {
            if (i4 == -1) {
                return null;
            }
            System.arraycopy(bArr, i4, bArr, bArr.length - (i3 - i4), i3 - i4);
            Arrays.fill(bArr, i4, (bArr.length - i3) + i4, (byte) 0);
        }
        try {
            return InetAddress.getByAddress(bArr);
        } catch (UnknownHostException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static boolean m7105(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127 || " #%/:?@[\\]".indexOf(charAt) != -1) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m7106(String str, int i, int i2) {
        int r1 = m7109(str, i, i2);
        return str.substring(r1, m7101(str, r1, i2));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m7107(String str) {
        return f6409.matcher(str).matches();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m7108(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'a' && c <= 'f') {
            return (c - 'a') + 10;
        }
        if (c < 'A' || c > 'F') {
            return -1;
        }
        return (c - 'A') + 10;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m7109(String str, int i, int i2) {
        int i3 = i;
        while (i3 < i2) {
            switch (str.charAt(i3)) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    i3++;
                default:
                    return i3;
            }
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m7110(String str, int i, int i2, char c) {
        for (int i3 = i; i3 < i2; i3++) {
            if (str.charAt(i3) == c) {
                return i3;
            }
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m7111(String str, int i, int i2, String str2) {
        for (int i3 = i; i3 < i2; i3++) {
            if (str2.indexOf(str.charAt(i3)) != -1) {
                return i3;
            }
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m7112(String str, long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException(str + " < 0");
        } else if (timeUnit == null) {
            throw new NullPointerException("unit == null");
        } else {
            long millis = timeUnit.toMillis(j);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException(str + " too large.");
            } else if (millis != 0 || j <= 0) {
                return (int) millis;
            } else {
                throw new IllegalArgumentException(str + " too small.");
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m7113(Comparator<String> comparator, String[] strArr, String str) {
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            if (comparator.compare(strArr[i], str) == 0) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static AssertionError m7114(String str, Exception exc) {
        AssertionError assertionError = new AssertionError(str);
        try {
            assertionError.initCause(exc);
        } catch (IllegalStateException e) {
        }
        return assertionError;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m7115(String str) {
        if (str.contains(":")) {
            InetAddress r2 = (!str.startsWith("[") || !str.endsWith("]")) ? m7104(str, 0, str.length()) : m7104(str, 1, str.length() - 1);
            if (r2 == null) {
                return null;
            }
            byte[] address = r2.getAddress();
            if (address.length == 16) {
                return m7118(address);
            }
            throw new AssertionError("Invalid IPv6 address: '" + str + "'");
        }
        try {
            String lowerCase = IDN.toASCII(str).toLowerCase(Locale.US);
            if (lowerCase.isEmpty()) {
                return null;
            }
            if (m7105(lowerCase)) {
                return null;
            }
            return lowerCase;
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m7116(String str, Object... objArr) {
        return String.format(Locale.US, str, objArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m7117(HttpUrl httpUrl, boolean z) {
        String r0 = httpUrl.m6951().contains(":") ? "[" + httpUrl.m6951() + "]" : httpUrl.m6951();
        return (z || httpUrl.m6952() != HttpUrl.m6940(httpUrl.m6961())) ? r0 + ":" + httpUrl.m6952() : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m7118(byte[] bArr) {
        int i = -1;
        int i2 = 0;
        int i3 = 0;
        while (i3 < bArr.length) {
            int i4 = i3;
            while (i3 < 16 && bArr[i3] == 0 && bArr[i3 + 1] == 0) {
                i3 += 2;
            }
            int i5 = i3 - i4;
            if (i5 > i2 && i5 >= 4) {
                i = i4;
                i2 = i5;
            }
            i3 += 2;
        }
        Buffer buffer = new Buffer();
        int i6 = 0;
        while (i6 < bArr.length) {
            if (i6 == i) {
                buffer.m7238(58);
                i6 += i2;
                if (i6 == 16) {
                    buffer.m7238(58);
                }
            } else {
                if (i6 > 0) {
                    buffer.m7238(58);
                }
                buffer.m7221((long) (((bArr[i6] & 255) << 8) | (bArr[i6 + 1] & 255)));
                i6 += 2;
            }
        }
        return buffer.m7224();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Charset m7119(BufferedSource bufferedSource, Charset charset) throws IOException {
        if (bufferedSource.m20472(0, f6410)) {
            bufferedSource.m20461((long) f6410.size());
            return f6413;
        } else if (bufferedSource.m20472(0, f6411)) {
            bufferedSource.m20461((long) f6411.size());
            return f6406;
        } else if (bufferedSource.m20472(0, f6412)) {
            bufferedSource.m20461((long) f6412.size());
            return f6418;
        } else if (bufferedSource.m20472(0, f6407)) {
            bufferedSource.m20461((long) f6407.size());
            return f6419;
        } else if (!bufferedSource.m20472(0, f6405)) {
            return charset;
        } else {
            bufferedSource.m20461((long) f6405.size());
            return f6408;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> List<T> m7120(List<T> list) {
        return Collections.unmodifiableList(new ArrayList(list));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> List<T> m7121(T... tArr) {
        return Collections.unmodifiableList(Arrays.asList((Object[]) tArr.clone()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ThreadFactory m7122(final String str, final boolean z) {
        return new ThreadFactory() {
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, str);
                thread.setDaemon(z);
                return thread;
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m7123(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m7124(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m7125(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (AssertionError e) {
                if (!m7126(e)) {
                    throw e;
                }
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m7126(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m7127(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m7128(String str, int i, int i2, byte[] bArr, int i3) {
        int i4 = i;
        int i5 = i3;
        while (i4 < i2) {
            if (i5 == bArr.length) {
                return false;
            }
            if (i5 != i3) {
                if (str.charAt(i4) != '.') {
                    return false;
                }
                i4++;
            }
            int i6 = 0;
            int i7 = i4;
            while (i4 < i2) {
                char charAt = str.charAt(i4);
                if (charAt < '0' || charAt > '9') {
                    break;
                } else if ((i6 == 0 && i7 != i4) || ((i6 * 10) + charAt) - 48 > 255) {
                    return false;
                } else {
                    i4++;
                }
            }
            if (i4 - i7 == 0) {
                return false;
            }
            bArr[i5] = (byte) i6;
            i5++;
        }
        return i5 == i3 + 4;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m7129(Source source, int i, TimeUnit timeUnit) {
        try {
            return m7103(source, i, timeUnit);
        } catch (IOException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String[] m7130(Comparator<? super String> comparator, String[] strArr, String[] strArr2) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            int length = strArr2.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (comparator.compare(str, strArr2[i]) == 0) {
                    arrayList.add(str);
                    break;
                } else {
                    i++;
                }
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String[] m7131(String[] strArr, String str) {
        String[] strArr2 = new String[(strArr.length + 1)];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
        strArr2[strArr2.length - 1] = str;
        return strArr2;
    }
}
