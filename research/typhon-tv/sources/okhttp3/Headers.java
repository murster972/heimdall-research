package okhttp3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.Nullable;
import okhttp3.internal.Util;
import org.apache.commons.lang3.StringUtils;

public final class Headers {

    /* renamed from: 龘  reason: contains not printable characters */
    private final String[] f6276;

    public static final class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        final List<String> f16043 = new ArrayList(20);

        /* renamed from: 麤  reason: contains not printable characters */
        private void m19948(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str.isEmpty()) {
                throw new IllegalArgumentException("name is empty");
            } else {
                int length = str.length();
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if (charAt <= ' ' || charAt >= 127) {
                        throw new IllegalArgumentException(Util.m7116("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), str));
                    }
                }
                if (str2 == null) {
                    throw new NullPointerException("value for name " + str + " == null");
                }
                int i2 = 0;
                int length2 = str2.length();
                while (i2 < length2) {
                    char charAt2 = str2.charAt(i2);
                    if ((charAt2 > 31 || charAt2 == 9) && charAt2 < 127) {
                        i2++;
                    } else {
                        throw new IllegalArgumentException(Util.m7116("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt2), Integer.valueOf(i2), str, str2));
                    }
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19949(String str) {
            int i = 0;
            while (i < this.f16043.size()) {
                if (str.equalsIgnoreCase(this.f16043.get(i))) {
                    this.f16043.remove(i);
                    this.f16043.remove(i);
                    i -= 2;
                }
                i += 2;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19950(String str, String str2) {
            this.f16043.add(str);
            this.f16043.add(str2.trim());
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public String m19951(String str) {
            for (int size = this.f16043.size() - 2; size >= 0; size -= 2) {
                if (str.equalsIgnoreCase(this.f16043.get(size))) {
                    return this.f16043.get(size + 1);
                }
            }
            return null;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m19952(String str, String str2) {
            m19948(str, str2);
            m19949(str);
            m19950(str, str2);
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19953(String str) {
            int indexOf = str.indexOf(":", 1);
            return indexOf != -1 ? m19950(str.substring(0, indexOf), str.substring(indexOf + 1)) : str.startsWith(":") ? m19950("", str.substring(1)) : m19950("", str);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19954(String str, String str2) {
            m19948(str, str2);
            return m19950(str, str2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Headers m19955() {
            return new Headers(this);
        }
    }

    Headers(Builder builder) {
        this.f6276 = (String[]) builder.f16043.toArray(new String[builder.f16043.size()]);
    }

    private Headers(String[] strArr) {
        this.f6276 = strArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m6928(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Headers m6929(String... strArr) {
        if (strArr == null) {
            throw new NullPointerException("namesAndValues == null");
        } else if (strArr.length % 2 != 0) {
            throw new IllegalArgumentException("Expected alternating header names and values");
        } else {
            String[] strArr2 = (String[]) strArr.clone();
            for (int i = 0; i < strArr2.length; i++) {
                if (strArr2[i] == null) {
                    throw new IllegalArgumentException("Headers cannot be null");
                }
                strArr2[i] = strArr2[i].trim();
            }
            int i2 = 0;
            while (i2 < strArr2.length) {
                String str = strArr2[i2];
                String str2 = strArr2[i2 + 1];
                if (str.length() != 0 && str.indexOf(0) == -1 && str2.indexOf(0) == -1) {
                    i2 += 2;
                } else {
                    throw new IllegalArgumentException("Unexpected header: " + str + ": " + str2);
                }
            }
            return new Headers(strArr2);
        }
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof Headers) && Arrays.equals(((Headers) obj).f6276, this.f6276);
    }

    public int hashCode() {
        return Arrays.hashCode(this.f6276);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int r2 = m6934();
        for (int i = 0; i < r2; i++) {
            sb.append(m6935(i)).append(": ").append(m6930(i)).append(StringUtils.LF);
        }
        return sb.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m6930(int i) {
        return this.f6276[(i * 2) + 1];
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public List<String> m6931(String str) {
        ArrayList arrayList = null;
        int r2 = m6934();
        for (int i = 0; i < r2; i++) {
            if (str.equalsIgnoreCase(m6935(i))) {
                if (arrayList == null) {
                    arrayList = new ArrayList(2);
                }
                arrayList.add(m6930(i));
            }
        }
        return arrayList != null ? Collections.unmodifiableList(arrayList) : Collections.emptyList();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Builder m6932() {
        Builder builder = new Builder();
        Collections.addAll(builder.f16043, this.f6276);
        return builder;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Map<String, List<String>> m6933() {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        int r3 = m6934();
        for (int i = 0; i < r3; i++) {
            String lowerCase = m6935(i).toLowerCase(Locale.US);
            List list = (List) treeMap.get(lowerCase);
            if (list == null) {
                list = new ArrayList(2);
                treeMap.put(lowerCase, list);
            }
            list.add(m6930(i));
        }
        return treeMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m6934() {
        return this.f6276.length / 2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6935(int i) {
        return this.f6276[i * 2];
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m6936(String str) {
        return m6928(this.f6276, str);
    }
}
