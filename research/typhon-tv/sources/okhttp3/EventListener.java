package okhttp3;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import javax.annotation.Nullable;

public abstract class EventListener {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final EventListener f16031 = new EventListener() {
    };

    public interface Factory {
        /* renamed from: 龘  reason: contains not printable characters */
        EventListener m19937(Call call);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Factory m19915(EventListener eventListener) {
        return new Factory() {
            /* renamed from: 龘  reason: contains not printable characters */
            public EventListener m19936(Call call) {
                return EventListener.this;
            }
        };
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m19916(Call call) {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m19917(Call call) {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m19918(Call call) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19919(Call call) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19920(Call call, long j) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19921(Call call, Connection connection) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19922(Call call) {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19923(Call call) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19924(Call call) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19925(Call call, long j) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19926(Call call, IOException iOException) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19927(Call call, String str) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19928(Call call, String str, List<InetAddress> list) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19929(Call call, InetSocketAddress inetSocketAddress, Proxy proxy) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19930(Call call, InetSocketAddress inetSocketAddress, Proxy proxy, @Nullable Protocol protocol) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19931(Call call, InetSocketAddress inetSocketAddress, Proxy proxy, @Nullable Protocol protocol, IOException iOException) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19932(Call call, Connection connection) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19933(Call call, @Nullable Handshake handshake) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19934(Call call, Request request) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19935(Call call, Response response) {
    }
}
