package okhttp3;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;

public final class MediaType {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Pattern f6295 = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f6296 = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");
    @Nullable

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f6297;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f6298;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f6299;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f6300;

    private MediaType(String str, String str2, String str3, @Nullable String str4) {
        this.f6300 = str;
        this.f6299 = str2;
        this.f6298 = str3;
        this.f6297 = str4;
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaType m6996(String str) {
        Matcher matcher = f6296.matcher(str);
        if (!matcher.lookingAt()) {
            return null;
        }
        String lowerCase = matcher.group(1).toLowerCase(Locale.US);
        String lowerCase2 = matcher.group(2).toLowerCase(Locale.US);
        String str2 = null;
        Matcher matcher2 = f6295.matcher(str);
        for (int end = matcher.end(); end < str.length(); end = matcher2.end()) {
            matcher2.region(end, str.length());
            if (!matcher2.lookingAt()) {
                return null;
            }
            String group = matcher2.group(1);
            if (group != null && group.equalsIgnoreCase("charset")) {
                String group2 = matcher2.group(2);
                String substring = group2 != null ? (!group2.startsWith("'") || !group2.endsWith("'") || group2.length() <= 2) ? group2 : group2.substring(1, group2.length() - 1) : matcher2.group(3);
                if (str2 != null && !substring.equalsIgnoreCase(str2)) {
                    return null;
                }
                str2 = substring;
            }
        }
        return new MediaType(str, lowerCase, lowerCase2, str2);
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof MediaType) && ((MediaType) obj).f6300.equals(this.f6300);
    }

    public int hashCode() {
        return this.f6300.hashCode();
    }

    public String toString() {
        return this.f6300;
    }

    @Nullable
    /* renamed from: 靐  reason: contains not printable characters */
    public Charset m6997() {
        return m6999((Charset) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6998() {
        return this.f6299;
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public Charset m6999(@Nullable Charset charset) {
        try {
            return this.f6297 != null ? Charset.forName(this.f6297) : charset;
        } catch (IllegalArgumentException e) {
            return charset;
        }
    }
}
