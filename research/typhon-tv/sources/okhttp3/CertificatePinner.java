package okhttp3;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import okhttp3.internal.Util;
import okhttp3.internal.tls.CertificateChainCleaner;
import okio.ByteString;

public final class CertificatePinner {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final CertificatePinner f6233 = new Builder().m19891();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Set<Pin> f6234;
    @Nullable

    /* renamed from: 齉  reason: contains not printable characters */
    private final CertificateChainCleaner f6235;

    public static final class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private final List<Pin> f15899 = new ArrayList();

        /* renamed from: 龘  reason: contains not printable characters */
        public CertificatePinner m19891() {
            return new CertificatePinner(new LinkedHashSet(this.f15899), (CertificateChainCleaner) null);
        }
    }

    static final class Pin {

        /* renamed from: 靐  reason: contains not printable characters */
        final String f15900;

        /* renamed from: 麤  reason: contains not printable characters */
        final ByteString f15901;

        /* renamed from: 齉  reason: contains not printable characters */
        final String f15902;

        /* renamed from: 龘  reason: contains not printable characters */
        final String f15903;

        public boolean equals(Object obj) {
            return (obj instanceof Pin) && this.f15903.equals(((Pin) obj).f15903) && this.f15902.equals(((Pin) obj).f15902) && this.f15901.equals(((Pin) obj).f15901);
        }

        public int hashCode() {
            return ((((this.f15903.hashCode() + 527) * 31) + this.f15902.hashCode()) * 31) + this.f15901.hashCode();
        }

        public String toString() {
            return this.f15902 + this.f15901.base64();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m19892(String str) {
            if (!this.f15903.startsWith("*.")) {
                return str.equals(this.f15900);
            }
            int indexOf = str.indexOf(46);
            if ((str.length() - indexOf) - 1 != this.f15900.length()) {
                return false;
            }
            return str.regionMatches(false, indexOf + 1, this.f15900, 0, this.f15900.length());
        }
    }

    CertificatePinner(Set<Pin> set, @Nullable CertificateChainCleaner certificateChainCleaner) {
        this.f6234 = set;
        this.f6235 = certificateChainCleaner;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static ByteString m6871(X509Certificate x509Certificate) {
        return ByteString.of(x509Certificate.getPublicKey().getEncoded()).sha256();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m6872(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha256/" + m6871((X509Certificate) certificate).base64();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ByteString m6873(X509Certificate x509Certificate) {
        return ByteString.of(x509Certificate.getPublicKey().getEncoded()).sha1();
    }

    public boolean equals(@Nullable Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof CertificatePinner) && Util.m7127((Object) this.f6235, (Object) ((CertificatePinner) obj).f6235) && this.f6234.equals(((CertificatePinner) obj).f6234);
    }

    public int hashCode() {
        return ((this.f6235 != null ? this.f6235.hashCode() : 0) * 31) + this.f6234.hashCode();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<Pin> m6874(String str) {
        List<Pin> emptyList = Collections.emptyList();
        for (Pin next : this.f6234) {
            if (next.m19892(str)) {
                if (emptyList.isEmpty()) {
                    emptyList = new ArrayList<>();
                }
                emptyList.add(next);
            }
        }
        return emptyList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public CertificatePinner m6875(@Nullable CertificateChainCleaner certificateChainCleaner) {
        return Util.m7127((Object) this.f6235, (Object) certificateChainCleaner) ? this : new CertificatePinner(this.f6234, certificateChainCleaner);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6876(String str, List<Certificate> list) throws SSLPeerUnverifiedException {
        List<Pin> r5 = m6874(str);
        if (!r5.isEmpty()) {
            if (this.f6235 != null) {
                list = this.f6235.m20419(list, str);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                ByteString byteString = null;
                ByteString byteString2 = null;
                int size2 = r5.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    Pin pin = r5.get(i2);
                    if (pin.f15902.equals("sha256/")) {
                        if (byteString2 == null) {
                            byteString2 = m6871(x509Certificate);
                        }
                        if (pin.f15901.equals(byteString2)) {
                            return;
                        }
                    } else if (pin.f15902.equals("sha1/")) {
                        if (byteString == null) {
                            byteString = m6873(x509Certificate);
                        }
                        if (pin.f15901.equals(byteString)) {
                            return;
                        }
                    } else {
                        throw new AssertionError("unsupported hashAlgorithm: " + pin.f15902);
                    }
                }
            }
            StringBuilder append = new StringBuilder().append("Certificate pinning failure!").append("\n  Peer certificate chain:");
            int size3 = list.size();
            for (int i3 = 0; i3 < size3; i3++) {
                X509Certificate x509Certificate2 = (X509Certificate) list.get(i3);
                append.append("\n    ").append(m6872((Certificate) x509Certificate2)).append(": ").append(x509Certificate2.getSubjectDN().getName());
            }
            append.append("\n  Pinned certificates for ").append(str).append(":");
            int size4 = r5.size();
            for (int i4 = 0; i4 < size4; i4++) {
                append.append("\n    ").append(r5.get(i4));
            }
            throw new SSLPeerUnverifiedException(append.toString());
        }
    }
}
