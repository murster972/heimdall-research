package okhttp3;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLSocket;
import okhttp3.internal.Util;

public final class ConnectionSpec {

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final CipherSuite[] f6244 = {CipherSuite.f15954, CipherSuite.f15960, CipherSuite.f15955, CipherSuite.f15961, CipherSuite.f15967, CipherSuite.f15966, CipherSuite.f15939, CipherSuite.f15940, CipherSuite.f15909, CipherSuite.f15910, CipherSuite.f16017, CipherSuite.f15959, CipherSuite.f15987};

    /* renamed from: 靐  reason: contains not printable characters */
    public static final ConnectionSpec f6245 = new Builder(f6247).m6894(TlsVersion.TLS_1_0).m6891(true).m6895();

    /* renamed from: 齉  reason: contains not printable characters */
    public static final ConnectionSpec f6246 = new Builder(false).m6895();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ConnectionSpec f6247 = new Builder(true).m6893(f6244).m6894(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0).m6891(true).m6895();
    @Nullable

    /* renamed from: ʻ  reason: contains not printable characters */
    final String[] f6248;
    @Nullable

    /* renamed from: ʼ  reason: contains not printable characters */
    final String[] f6249;

    /* renamed from: 连任  reason: contains not printable characters */
    final boolean f6250;

    /* renamed from: 麤  reason: contains not printable characters */
    final boolean f6251;

    public static final class Builder {
        @Nullable

        /* renamed from: 靐  reason: contains not printable characters */
        String[] f6252;

        /* renamed from: 麤  reason: contains not printable characters */
        boolean f6253;
        @Nullable

        /* renamed from: 齉  reason: contains not printable characters */
        String[] f6254;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f6255;

        public Builder(ConnectionSpec connectionSpec) {
            this.f6255 = connectionSpec.f6251;
            this.f6252 = connectionSpec.f6248;
            this.f6254 = connectionSpec.f6249;
            this.f6253 = connectionSpec.f6250;
        }

        Builder(boolean z) {
            this.f6255 = z;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m6890(String... strArr) {
            if (!this.f6255) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one TLS version is required");
            } else {
                this.f6254 = (String[]) strArr.clone();
                return this;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m6891(boolean z) {
            if (!this.f6255) {
                throw new IllegalStateException("no TLS extensions for cleartext connections");
            }
            this.f6253 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m6892(String... strArr) {
            if (!this.f6255) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one cipher suite is required");
            } else {
                this.f6252 = (String[]) strArr.clone();
                return this;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m6893(CipherSuite... cipherSuiteArr) {
            if (!this.f6255) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            }
            String[] strArr = new String[cipherSuiteArr.length];
            for (int i = 0; i < cipherSuiteArr.length; i++) {
                strArr[i] = cipherSuiteArr[i].f16018;
            }
            return m6892(strArr);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m6894(TlsVersion... tlsVersionArr) {
            if (!this.f6255) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            }
            String[] strArr = new String[tlsVersionArr.length];
            for (int i = 0; i < tlsVersionArr.length; i++) {
                strArr[i] = tlsVersionArr[i].javaName;
            }
            return m6890(strArr);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ConnectionSpec m6895() {
            return new ConnectionSpec(this);
        }
    }

    ConnectionSpec(Builder builder) {
        this.f6251 = builder.f6255;
        this.f6248 = builder.f6252;
        this.f6249 = builder.f6254;
        this.f6250 = builder.f6253;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private ConnectionSpec m6883(SSLSocket sSLSocket, boolean z) {
        String[] r0 = this.f6248 != null ? Util.m7130((Comparator<? super String>) CipherSuite.f16011, sSLSocket.getEnabledCipherSuites(), this.f6248) : sSLSocket.getEnabledCipherSuites();
        String[] r3 = this.f6249 != null ? Util.m7130((Comparator<? super String>) Util.f6404, sSLSocket.getEnabledProtocols(), this.f6249) : sSLSocket.getEnabledProtocols();
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        int r1 = Util.m7113(CipherSuite.f16011, supportedCipherSuites, "TLS_FALLBACK_SCSV");
        if (z && r1 != -1) {
            r0 = Util.m7131(r0, supportedCipherSuites[r1]);
        }
        return new Builder(this).m6892(r0).m6890(r3).m6895();
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof ConnectionSpec)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        ConnectionSpec connectionSpec = (ConnectionSpec) obj;
        if (this.f6251 == connectionSpec.f6251) {
            return !this.f6251 || (Arrays.equals(this.f6248, connectionSpec.f6248) && Arrays.equals(this.f6249, connectionSpec.f6249) && this.f6250 == connectionSpec.f6250);
        }
        return false;
    }

    public int hashCode() {
        if (!this.f6251) {
            return 17;
        }
        return ((((Arrays.hashCode(this.f6248) + 527) * 31) + Arrays.hashCode(this.f6249)) * 31) + (this.f6250 ? 0 : 1);
    }

    public String toString() {
        if (!this.f6251) {
            return "ConnectionSpec()";
        }
        return "ConnectionSpec(cipherSuites=" + (this.f6248 != null ? m6884().toString() : "[all enabled]") + ", tlsVersions=" + (this.f6249 != null ? m6886().toString() : "[all enabled]") + ", supportsTlsExtensions=" + this.f6250 + ")";
    }

    @Nullable
    /* renamed from: 靐  reason: contains not printable characters */
    public List<CipherSuite> m6884() {
        if (this.f6248 != null) {
            return CipherSuite.m19893(this.f6248);
        }
        return null;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m6885() {
        return this.f6250;
    }

    @Nullable
    /* renamed from: 齉  reason: contains not printable characters */
    public List<TlsVersion> m6886() {
        if (this.f6249 != null) {
            return TlsVersion.m20003(this.f6249);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6887(SSLSocket sSLSocket, boolean z) {
        ConnectionSpec r0 = m6883(sSLSocket, z);
        if (r0.f6249 != null) {
            sSLSocket.setEnabledProtocols(r0.f6249);
        }
        if (r0.f6248 != null) {
            sSLSocket.setEnabledCipherSuites(r0.f6248);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6888() {
        return this.f6251;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6889(SSLSocket sSLSocket) {
        if (!this.f6251) {
            return false;
        }
        if (this.f6249 == null || Util.m7102(Util.f6404, this.f6249, sSLSocket.getEnabledProtocols())) {
            return this.f6248 == null || Util.m7102(CipherSuite.f16011, this.f6248, sSLSocket.getEnabledCipherSuites());
        }
        return false;
    }
}
