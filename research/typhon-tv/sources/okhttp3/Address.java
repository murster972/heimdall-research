package okhttp3;

import com.mopub.common.TyphoonApp;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.HttpUrl;
import okhttp3.internal.Util;

public final class Address {

    /* renamed from: ʻ  reason: contains not printable characters */
    final List<ConnectionSpec> f6183;

    /* renamed from: ʼ  reason: contains not printable characters */
    final ProxySelector f6184;
    @Nullable

    /* renamed from: ʽ  reason: contains not printable characters */
    final Proxy f6185;
    @Nullable

    /* renamed from: ˑ  reason: contains not printable characters */
    final SSLSocketFactory f6186;
    @Nullable

    /* renamed from: ٴ  reason: contains not printable characters */
    final HostnameVerifier f6187;
    @Nullable

    /* renamed from: ᐧ  reason: contains not printable characters */
    final CertificatePinner f6188;

    /* renamed from: 连任  reason: contains not printable characters */
    final List<Protocol> f6189;

    /* renamed from: 靐  reason: contains not printable characters */
    final Dns f6190;

    /* renamed from: 麤  reason: contains not printable characters */
    final Authenticator f6191;

    /* renamed from: 齉  reason: contains not printable characters */
    final SocketFactory f6192;

    /* renamed from: 龘  reason: contains not printable characters */
    final HttpUrl f6193;

    public Address(String str, int i, Dns dns, SocketFactory socketFactory, @Nullable SSLSocketFactory sSLSocketFactory, @Nullable HostnameVerifier hostnameVerifier, @Nullable CertificatePinner certificatePinner, Authenticator authenticator, @Nullable Proxy proxy, List<Protocol> list, List<ConnectionSpec> list2, ProxySelector proxySelector) {
        this.f6193 = new HttpUrl.Builder().m6988(sSLSocketFactory != null ? TyphoonApp.HTTPS : TyphoonApp.HTTP).m6982(str).m6987(i).m6984();
        if (dns == null) {
            throw new NullPointerException("dns == null");
        }
        this.f6190 = dns;
        if (socketFactory == null) {
            throw new NullPointerException("socketFactory == null");
        }
        this.f6192 = socketFactory;
        if (authenticator == null) {
            throw new NullPointerException("proxyAuthenticator == null");
        }
        this.f6191 = authenticator;
        if (list == null) {
            throw new NullPointerException("protocols == null");
        }
        this.f6189 = Util.m7120(list);
        if (list2 == null) {
            throw new NullPointerException("connectionSpecs == null");
        }
        this.f6183 = Util.m7120(list2);
        if (proxySelector == null) {
            throw new NullPointerException("proxySelector == null");
        }
        this.f6184 = proxySelector;
        this.f6185 = proxy;
        this.f6186 = sSLSocketFactory;
        this.f6187 = hostnameVerifier;
        this.f6188 = certificatePinner;
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof Address) && this.f6193.equals(((Address) obj).f6193) && m6839((Address) obj);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((((((this.f6193.hashCode() + 527) * 31) + this.f6190.hashCode()) * 31) + this.f6191.hashCode()) * 31) + this.f6189.hashCode()) * 31) + this.f6183.hashCode()) * 31) + this.f6184.hashCode()) * 31) + (this.f6185 != null ? this.f6185.hashCode() : 0)) * 31) + (this.f6186 != null ? this.f6186.hashCode() : 0)) * 31) + (this.f6187 != null ? this.f6187.hashCode() : 0)) * 31;
        if (this.f6188 != null) {
            i = this.f6188.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder append = new StringBuilder().append("Address{").append(this.f6193.m6951()).append(":").append(this.f6193.m6952());
        if (this.f6185 != null) {
            append.append(", proxy=").append(this.f6185);
        } else {
            append.append(", proxySelector=").append(this.f6184);
        }
        append.append("}");
        return append.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public List<ConnectionSpec> m6828() {
        return this.f6183;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public ProxySelector m6829() {
        return this.f6184;
    }

    @Nullable
    /* renamed from: ʽ  reason: contains not printable characters */
    public Proxy m6830() {
        return this.f6185;
    }

    @Nullable
    /* renamed from: ˑ  reason: contains not printable characters */
    public SSLSocketFactory m6831() {
        return this.f6186;
    }

    @Nullable
    /* renamed from: ٴ  reason: contains not printable characters */
    public HostnameVerifier m6832() {
        return this.f6187;
    }

    @Nullable
    /* renamed from: ᐧ  reason: contains not printable characters */
    public CertificatePinner m6833() {
        return this.f6188;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public List<Protocol> m6834() {
        return this.f6189;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Dns m6835() {
        return this.f6190;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Authenticator m6836() {
        return this.f6191;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public SocketFactory m6837() {
        return this.f6192;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpUrl m6838() {
        return this.f6193;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6839(Address address) {
        return this.f6190.equals(address.f6190) && this.f6191.equals(address.f6191) && this.f6189.equals(address.f6189) && this.f6183.equals(address.f6183) && this.f6184.equals(address.f6184) && Util.m7127((Object) this.f6185, (Object) address.f6185) && Util.m7127((Object) this.f6186, (Object) address.f6186) && Util.m7127((Object) this.f6187, (Object) address.f6187) && Util.m7127((Object) this.f6188, (Object) address.f6188) && m6838().m6952() == address.m6838().m6952();
    }
}
