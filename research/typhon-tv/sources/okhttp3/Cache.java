package okhttp3;

import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Util;
import okhttp3.internal.cache.CacheRequest;
import okhttp3.internal.cache.CacheStrategy;
import okhttp3.internal.cache.DiskLruCache;
import okhttp3.internal.cache.InternalCache;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.HttpMethod;
import okhttp3.internal.http.StatusLine;
import okhttp3.internal.io.FileSystem;
import okhttp3.internal.platform.Platform;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.ByteString;
import okio.ForwardingSink;
import okio.ForwardingSource;
import okio.Okio;
import okio.Sink;
import okio.Source;
import org.apache.oltu.oauth2.common.OAuth;

public final class Cache implements Closeable, Flushable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f6195;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f6196;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f6197;

    /* renamed from: 靐  reason: contains not printable characters */
    final DiskLruCache f6198;

    /* renamed from: 麤  reason: contains not printable characters */
    int f6199;

    /* renamed from: 齉  reason: contains not printable characters */
    int f6200;

    /* renamed from: 龘  reason: contains not printable characters */
    final InternalCache f6201;

    private static class CacheResponseBody extends ResponseBody {

        /* renamed from: 靐  reason: contains not printable characters */
        private final BufferedSource f6202;
        @Nullable

        /* renamed from: 麤  reason: contains not printable characters */
        private final String f6203;
        @Nullable

        /* renamed from: 齉  reason: contains not printable characters */
        private final String f6204;

        /* renamed from: 龘  reason: contains not printable characters */
        final DiskLruCache.Snapshot f6205;

        CacheResponseBody(final DiskLruCache.Snapshot snapshot, String str, String str2) {
            this.f6205 = snapshot;
            this.f6204 = str;
            this.f6203 = str2;
            this.f6202 = Okio.m20507((Source) new ForwardingSource(snapshot.m7153(1)) {
                public void close() throws IOException {
                    snapshot.close();
                    super.close();
                }
            });
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m6850() {
            try {
                if (this.f6203 != null) {
                    return Long.parseLong(this.f6203);
                }
                return -1;
            } catch (NumberFormatException e) {
                return -1;
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public BufferedSource m6851() {
            return this.f6202;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaType m6852() {
            if (this.f6204 != null) {
                return MediaType.m6996(this.f6204);
            }
            return null;
        }
    }

    private static final class Entry {

        /* renamed from: 靐  reason: contains not printable characters */
        private static final String f6206 = (Platform.m7184().m7189() + "-Received-Millis");

        /* renamed from: 龘  reason: contains not printable characters */
        private static final String f6207 = (Platform.m7184().m7189() + "-Sent-Millis");

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Protocol f6208;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f6209;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final String f6210;

        /* renamed from: ˈ  reason: contains not printable characters */
        private final long f6211;

        /* renamed from: ˑ  reason: contains not printable characters */
        private final Headers f6212;
        @Nullable

        /* renamed from: ٴ  reason: contains not printable characters */
        private final Handshake f6213;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private final long f6214;

        /* renamed from: 连任  reason: contains not printable characters */
        private final String f6215;

        /* renamed from: 麤  reason: contains not printable characters */
        private final Headers f6216;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String f6217;

        Entry(Response response) {
            this.f6217 = response.m7069().m7053().toString();
            this.f6216 = HttpHeaders.m20122(response);
            this.f6215 = response.m7069().m7048();
            this.f6208 = response.m7064();
            this.f6209 = response.m7066();
            this.f6210 = response.m7063();
            this.f6212 = response.m7055();
            this.f6213 = response.m7054();
            this.f6214 = response.m7057();
            this.f6211 = response.m7058();
        }

        Entry(Source source) throws IOException {
            try {
                BufferedSource r14 = Okio.m20507(source);
                this.f6217 = r14.m20459();
                this.f6215 = r14.m20459();
                Headers.Builder builder = new Headers.Builder();
                int r18 = Cache.m6841(r14);
                for (int i = 0; i < r18; i++) {
                    builder.m19953(r14.m20459());
                }
                this.f6216 = builder.m19955();
                StatusLine r15 = StatusLine.m20159(r14.m20459());
                this.f6208 = r15.f16200;
                this.f6209 = r15.f16198;
                this.f6210 = r15.f16199;
                Headers.Builder builder2 = new Headers.Builder();
                int r11 = Cache.m6841(r14);
                for (int i2 = 0; i2 < r11; i2++) {
                    builder2.m19953(r14.m20459());
                }
                String r13 = builder2.m19951(f6207);
                String r10 = builder2.m19951(f6206);
                builder2.m19949(f6207);
                builder2.m19949(f6206);
                this.f6214 = r13 != null ? Long.parseLong(r13) : 0;
                this.f6211 = r10 != null ? Long.parseLong(r10) : 0;
                this.f6212 = builder2.m19955();
                if (m6855()) {
                    String r4 = r14.m20459();
                    if (r4.length() > 0) {
                        throw new IOException("expected \"\" but was \"" + r4 + "\"");
                    }
                    this.f6213 = Handshake.m19943(!r14.m20452() ? TlsVersion.forJavaName(r14.m20459()) : TlsVersion.SSL_3_0, CipherSuite.m19894(r14.m20459()), m6853(r14), m6853(r14));
                } else {
                    this.f6213 = null;
                }
            } finally {
                source.close();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private List<Certificate> m6853(BufferedSource bufferedSource) throws IOException {
            int r4 = Cache.m6841(bufferedSource);
            if (r4 == -1) {
                return Collections.emptyList();
            }
            try {
                CertificateFactory instance = CertificateFactory.getInstance("X.509");
                ArrayList arrayList = new ArrayList(r4);
                for (int i = 0; i < r4; i++) {
                    String r5 = bufferedSource.m20459();
                    Buffer buffer = new Buffer();
                    buffer.m7247(ByteString.decodeBase64(r5));
                    arrayList.add(instance.generateCertificate(buffer.m7211()));
                }
                return arrayList;
            } catch (CertificateException e) {
                throw new IOException(e.getMessage());
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m6854(BufferedSink bufferedSink, List<Certificate> list) throws IOException {
            try {
                bufferedSink.m20439((long) list.size()).m20444(10);
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    bufferedSink.m20445(ByteString.of(list.get(i).getEncoded()).base64()).m20444(10);
                }
            } catch (CertificateEncodingException e) {
                throw new IOException(e.getMessage());
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m6855() {
            return this.f6217.startsWith("https://");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Response m6856(DiskLruCache.Snapshot snapshot) {
            String r2 = this.f6212.m6936(OAuth.HeaderType.CONTENT_TYPE);
            String r1 = this.f6212.m6936("Content-Length");
            return new Response.Builder().m7084(new Request.Builder().m19992(this.f6217).m19994(this.f6215, (RequestBody) null).m19996(this.f6216).m19989()).m7083(this.f6208).m7077(this.f6209).m7079(this.f6210).m7082(this.f6212).m7086((ResponseBody) new CacheResponseBody(snapshot, r2, r1)).m7081(this.f6213).m7078(this.f6214).m7072(this.f6211).m7087();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m6857(DiskLruCache.Editor editor) throws IOException {
            BufferedSink r1 = Okio.m20506(editor.m20035(0));
            r1.m20445(this.f6217).m20444(10);
            r1.m20445(this.f6215).m20444(10);
            r1.m20439((long) this.f6216.m6934()).m20444(10);
            int r2 = this.f6216.m6934();
            for (int i = 0; i < r2; i++) {
                r1.m20445(this.f6216.m6935(i)).m20445(": ").m20445(this.f6216.m6930(i)).m20444(10);
            }
            r1.m20445(new StatusLine(this.f6208, this.f6209, this.f6210).toString()).m20444(10);
            r1.m20439((long) (this.f6212.m6934() + 2)).m20444(10);
            int r22 = this.f6212.m6934();
            for (int i2 = 0; i2 < r22; i2++) {
                r1.m20445(this.f6212.m6935(i2)).m20445(": ").m20445(this.f6212.m6930(i2)).m20444(10);
            }
            r1.m20445(f6207).m20445(": ").m20439(this.f6214).m20444(10);
            r1.m20445(f6206).m20445(": ").m20439(this.f6211).m20444(10);
            if (m6855()) {
                r1.m20444(10);
                r1.m20445(this.f6213.m19944().m19896()).m20444(10);
                m6854(r1, this.f6213.m19946());
                m6854(r1, this.f6213.m19945());
                r1.m20445(this.f6213.m19947().javaName()).m20444(10);
            }
            r1.close();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m6858(Request request, Response response) {
            return this.f6217.equals(request.m7053().toString()) && this.f6215.equals(request.m7048()) && HttpHeaders.m20130(response, this.f6216, request);
        }
    }

    private final class CacheRequestImpl implements CacheRequest {

        /* renamed from: 连任  reason: contains not printable characters */
        private Sink f15881;

        /* renamed from: 麤  reason: contains not printable characters */
        private Sink f15883;

        /* renamed from: 齉  reason: contains not printable characters */
        private final DiskLruCache.Editor f15884;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f15885;

        CacheRequestImpl(final DiskLruCache.Editor editor) {
            this.f15884 = editor;
            this.f15883 = editor.m20035(1);
            this.f15881 = new ForwardingSink(this.f15883, Cache.this) {
                public void close() throws IOException {
                    synchronized (Cache.this) {
                        if (!CacheRequestImpl.this.f15885) {
                            CacheRequestImpl.this.f15885 = true;
                            Cache.this.f6200++;
                            super.close();
                            editor.m20033();
                        }
                    }
                }
            };
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Sink m19876() {
            return this.f15881;
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m19877() {
            /*
                r3 = this;
                okhttp3.Cache r1 = okhttp3.Cache.this
                monitor-enter(r1)
                boolean r0 = r3.f15885     // Catch:{ all -> 0x0022 }
                if (r0 == 0) goto L_0x0009
                monitor-exit(r1)     // Catch:{ all -> 0x0022 }
            L_0x0008:
                return
            L_0x0009:
                r0 = 1
                r3.f15885 = r0     // Catch:{ all -> 0x0022 }
                okhttp3.Cache r0 = okhttp3.Cache.this     // Catch:{ all -> 0x0022 }
                int r2 = r0.f6199     // Catch:{ all -> 0x0022 }
                int r2 = r2 + 1
                r0.f6199 = r2     // Catch:{ all -> 0x0022 }
                monitor-exit(r1)     // Catch:{ all -> 0x0022 }
                okio.Sink r0 = r3.f15883
                okhttp3.internal.Util.m7124((java.io.Closeable) r0)
                okhttp3.internal.cache.DiskLruCache$Editor r0 = r3.f15884     // Catch:{ IOException -> 0x0020 }
                r0.m20034()     // Catch:{ IOException -> 0x0020 }
                goto L_0x0008
            L_0x0020:
                r0 = move-exception
                goto L_0x0008
            L_0x0022:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0022 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.Cache.CacheRequestImpl.m19877():void");
        }
    }

    public Cache(File file, long j) {
        this(file, j, FileSystem.f16395);
    }

    Cache(File file, long j, FileSystem fileSystem) {
        this.f6201 = new InternalCache() {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m19870(Request request) throws IOException {
                Cache.this.m6844(request);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Response m19871(Request request) throws IOException {
                return Cache.this.m6845(request);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public CacheRequest m19872(Response response) throws IOException {
                return Cache.this.m6846(response);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m19873() {
                Cache.this.m6847();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m19874(Response response, Response response2) {
                Cache.this.m6848(response, response2);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m19875(CacheStrategy cacheStrategy) {
                Cache.this.m6849(cacheStrategy);
            }
        };
        this.f6198 = DiskLruCache.m7139(fileSystem, file, 201105, 2, j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m6841(BufferedSource bufferedSource) throws IOException {
        try {
            long r2 = bufferedSource.m20456();
            String r1 = bufferedSource.m20459();
            if (r2 >= 0 && r2 <= 2147483647L && r1.isEmpty()) {
                return (int) r2;
            }
            throw new IOException("expected an int but was \"" + r2 + r1 + "\"");
        } catch (NumberFormatException e) {
            throw new IOException(e.getMessage());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m6842(HttpUrl httpUrl) {
        return ByteString.encodeUtf8(httpUrl.toString()).md5().hex();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m6843(@Nullable DiskLruCache.Editor editor) {
        if (editor != null) {
            try {
                editor.m20034();
            } catch (IOException e) {
            }
        }
    }

    public void close() throws IOException {
        this.f6198.close();
    }

    public void flush() throws IOException {
        this.f6198.flush();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6844(Request request) throws IOException {
        this.f6198.m7146(m6842(request.m7053()));
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public Response m6845(Request request) {
        try {
            DiskLruCache.Snapshot r4 = this.f6198.m7148(m6842(request.m7053()));
            if (r4 == null) {
                return null;
            }
            try {
                Entry entry = new Entry(r4.m7153(0));
                Response r3 = entry.m6856(r4);
                if (entry.m6858(request, r3)) {
                    return r3;
                }
                Util.m7124((Closeable) r3.m7056());
                return null;
            } catch (IOException e) {
                Util.m7124((Closeable) r4);
                return null;
            }
        } catch (IOException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public CacheRequest m6846(Response response) {
        String r3 = response.m7069().m7048();
        if (HttpMethod.m20135(response.m7069().m7048())) {
            try {
                m6844(response.m7069());
                return null;
            } catch (IOException e) {
                return null;
            }
        } else if (!r3.equals(OAuth.HttpMethod.GET) || HttpHeaders.m20119(response)) {
            return null;
        } else {
            Entry entry = new Entry(response);
            try {
                DiskLruCache.Editor r1 = this.f6198.m7142(m6842(response.m7069().m7053()));
                if (r1 == null) {
                    return null;
                }
                entry.m6857(r1);
                return new CacheRequestImpl(r1);
            } catch (IOException e2) {
                m6843((DiskLruCache.Editor) null);
                return null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m6847() {
        this.f6195++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6848(Response response, Response response2) {
        Entry entry = new Entry(response2);
        try {
            DiskLruCache.Editor r1 = ((CacheResponseBody) response.m7056()).f6205.m7152();
            if (r1 != null) {
                entry.m6857(r1);
                r1.m20033();
            }
        } catch (IOException e) {
            m6843((DiskLruCache.Editor) null);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m6849(CacheStrategy cacheStrategy) {
        this.f6196++;
        if (cacheStrategy.f6421 != null) {
            this.f6197++;
        } else if (cacheStrategy.f6420 != null) {
            this.f6195++;
        }
    }
}
