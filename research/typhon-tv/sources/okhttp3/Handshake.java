package okhttp3;

import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import okhttp3.internal.Util;

public final class Handshake {

    /* renamed from: 靐  reason: contains not printable characters */
    private final CipherSuite f16039;

    /* renamed from: 麤  reason: contains not printable characters */
    private final List<Certificate> f16040;

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<Certificate> f16041;

    /* renamed from: 龘  reason: contains not printable characters */
    private final TlsVersion f16042;

    private Handshake(TlsVersion tlsVersion, CipherSuite cipherSuite, List<Certificate> list, List<Certificate> list2) {
        this.f16042 = tlsVersion;
        this.f16039 = cipherSuite;
        this.f16041 = list;
        this.f16040 = list2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Handshake m19942(SSLSession sSLSession) {
        Certificate[] certificateArr;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        }
        CipherSuite r0 = CipherSuite.m19894(cipherSuite);
        String protocol = sSLSession.getProtocol();
        if (protocol == null) {
            throw new IllegalStateException("tlsVersion == null");
        }
        TlsVersion forJavaName = TlsVersion.forJavaName(protocol);
        try {
            certificateArr = sSLSession.getPeerCertificates();
        } catch (SSLPeerUnverifiedException e) {
            certificateArr = null;
        }
        List r6 = certificateArr != null ? Util.m7121((T[]) certificateArr) : Collections.emptyList();
        Certificate[] localCertificates = sSLSession.getLocalCertificates();
        return new Handshake(forJavaName, r0, r6, localCertificates != null ? Util.m7121((T[]) localCertificates) : Collections.emptyList());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Handshake m19943(TlsVersion tlsVersion, CipherSuite cipherSuite, List<Certificate> list, List<Certificate> list2) {
        if (tlsVersion == null) {
            throw new NullPointerException("tlsVersion == null");
        } else if (cipherSuite != null) {
            return new Handshake(tlsVersion, cipherSuite, Util.m7120(list), Util.m7120(list2));
        } else {
            throw new NullPointerException("cipherSuite == null");
        }
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof Handshake)) {
            return false;
        }
        Handshake handshake = (Handshake) obj;
        return this.f16042.equals(handshake.f16042) && this.f16039.equals(handshake.f16039) && this.f16041.equals(handshake.f16041) && this.f16040.equals(handshake.f16040);
    }

    public int hashCode() {
        return ((((((this.f16042.hashCode() + 527) * 31) + this.f16039.hashCode()) * 31) + this.f16041.hashCode()) * 31) + this.f16040.hashCode();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public CipherSuite m19944() {
        return this.f16039;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public List<Certificate> m19945() {
        return this.f16040;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public List<Certificate> m19946() {
        return this.f16041;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TlsVersion m19947() {
        return this.f16042;
    }
}
