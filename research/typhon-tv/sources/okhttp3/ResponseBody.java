package okhttp3;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import javax.annotation.Nullable;
import okhttp3.internal.Util;
import okio.Buffer;
import okio.BufferedSource;

public abstract class ResponseBody implements Closeable {

    /* renamed from: 龘  reason: contains not printable characters */
    private Reader f6398;

    static final class BomAwareReader extends Reader {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Charset f16080;

        /* renamed from: 麤  reason: contains not printable characters */
        private Reader f16081;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f16082;

        /* renamed from: 龘  reason: contains not printable characters */
        private final BufferedSource f16083;

        BomAwareReader(BufferedSource bufferedSource, Charset charset) {
            this.f16083 = bufferedSource;
            this.f16080 = charset;
        }

        public void close() throws IOException {
            this.f16082 = true;
            if (this.f16081 != null) {
                this.f16081.close();
            } else {
                this.f16083.close();
            }
        }

        public int read(char[] cArr, int i, int i2) throws IOException {
            if (this.f16082) {
                throw new IOException("Stream closed");
            }
            Reader reader = this.f16081;
            if (reader == null) {
                reader = new InputStreamReader(this.f16083.m20453(), Util.m7119(this.f16083, this.f16080));
                this.f16081 = reader;
            }
            return reader.read(cArr, i, i2);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private Charset m7088() {
        MediaType r0 = m7096();
        return r0 != null ? r0.m6999(Util.f6413) : Util.f6413;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ResponseBody m7089(@Nullable final MediaType mediaType, final long j, final BufferedSource bufferedSource) {
        if (bufferedSource != null) {
            return new ResponseBody() {
                /* renamed from: 靐  reason: contains not printable characters */
                public long m7097() {
                    return j;
                }

                /* renamed from: 齉  reason: contains not printable characters */
                public BufferedSource m7098() {
                    return bufferedSource;
                }

                @Nullable
                /* renamed from: 龘  reason: contains not printable characters */
                public MediaType m7099() {
                    return MediaType.this;
                }
            };
        }
        throw new NullPointerException("source == null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ResponseBody m7090(@Nullable MediaType mediaType, byte[] bArr) {
        return m7089(mediaType, (long) bArr.length, new Buffer().m7255(bArr));
    }

    public void close() {
        Util.m7124((Closeable) m7095());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m7091() throws IOException {
        BufferedSource r1 = m7095();
        try {
            return r1.m20469(Util.m7119(r1, m7088()));
        } finally {
            Util.m7124((Closeable) r1);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Reader m7092() {
        Reader reader = this.f6398;
        if (reader != null) {
            return reader;
        }
        BomAwareReader bomAwareReader = new BomAwareReader(m7095(), m7088());
        this.f6398 = bomAwareReader;
        return bomAwareReader;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract long m7093();

    /* renamed from: 麤  reason: contains not printable characters */
    public final InputStream m7094() {
        return m7095().m20453();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract BufferedSource m7095();

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract MediaType m7096();
}
