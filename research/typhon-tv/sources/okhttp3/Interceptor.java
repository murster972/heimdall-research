package okhttp3;

import java.io.IOException;
import javax.annotation.Nullable;

public interface Interceptor {

    public interface Chain {
        /* renamed from: 连任  reason: contains not printable characters */
        int m6990();

        @Nullable
        /* renamed from: 靐  reason: contains not printable characters */
        Connection m6991();

        /* renamed from: 麤  reason: contains not printable characters */
        int m6992();

        /* renamed from: 齉  reason: contains not printable characters */
        int m6993();

        /* renamed from: 龘  reason: contains not printable characters */
        Request m6994();

        /* renamed from: 龘  reason: contains not printable characters */
        Response m6995(Request request) throws IOException;
    }

    Response intercept(Chain chain) throws IOException;
}
