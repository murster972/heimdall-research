package okhttp3;

import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public final class CipherSuite {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final CipherSuite f15904 = m19895("SSL_RSA_WITH_RC4_128_SHA", 5);

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public static final CipherSuite f15905 = m19895("TLS_DH_anon_WITH_AES_128_CBC_SHA", 52);

    /* renamed from: ʻʼ  reason: contains not printable characters */
    public static final CipherSuite f15906 = m19895("TLS_PSK_WITH_AES_128_CBC_SHA", 140);

    /* renamed from: ʻʽ  reason: contains not printable characters */
    public static final CipherSuite f15907 = m19895("TLS_PSK_WITH_AES_256_CBC_SHA", 141);

    /* renamed from: ʻʾ  reason: contains not printable characters */
    public static final CipherSuite f15908 = m19895("TLS_RSA_WITH_SEED_CBC_SHA", 150);

    /* renamed from: ʻʿ  reason: contains not printable characters */
    public static final CipherSuite f15909 = m19895("TLS_RSA_WITH_AES_128_GCM_SHA256", 156);

    /* renamed from: ʻˆ  reason: contains not printable characters */
    public static final CipherSuite f15910 = m19895("TLS_RSA_WITH_AES_256_GCM_SHA384", 157);

    /* renamed from: ʻˈ  reason: contains not printable characters */
    public static final CipherSuite f15911 = m19895("TLS_DHE_RSA_WITH_AES_128_GCM_SHA256", 158);

    /* renamed from: ʻˉ  reason: contains not printable characters */
    public static final CipherSuite f15912 = m19895("TLS_DHE_RSA_WITH_AES_256_GCM_SHA384", 159);

    /* renamed from: ʻˊ  reason: contains not printable characters */
    public static final CipherSuite f15913 = m19895("TLS_DHE_DSS_WITH_AES_128_GCM_SHA256", 162);

    /* renamed from: ʻˋ  reason: contains not printable characters */
    public static final CipherSuite f15914 = m19895("TLS_DHE_DSS_WITH_AES_256_GCM_SHA384", 163);

    /* renamed from: ʻˎ  reason: contains not printable characters */
    public static final CipherSuite f15915 = m19895("TLS_DH_anon_WITH_AES_128_GCM_SHA256", 166);

    /* renamed from: ʻˏ  reason: contains not printable characters */
    public static final CipherSuite f15916 = m19895("TLS_DH_anon_WITH_AES_256_GCM_SHA384", 167);

    /* renamed from: ʻˑ  reason: contains not printable characters */
    public static final CipherSuite f15917 = m19895("TLS_EMPTY_RENEGOTIATION_INFO_SCSV", 255);

    /* renamed from: ʻי  reason: contains not printable characters */
    public static final CipherSuite f15918 = m19895("TLS_FALLBACK_SCSV", 22016);

    /* renamed from: ʻـ  reason: contains not printable characters */
    public static final CipherSuite f15919 = m19895("TLS_ECDH_ECDSA_WITH_NULL_SHA", 49153);

    /* renamed from: ʻٴ  reason: contains not printable characters */
    public static final CipherSuite f15920 = m19895("TLS_ECDH_ECDSA_WITH_RC4_128_SHA", 49154);

    /* renamed from: ʻᐧ  reason: contains not printable characters */
    public static final CipherSuite f15921 = m19895("TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA", 49155);

    /* renamed from: ʻᴵ  reason: contains not printable characters */
    public static final CipherSuite f15922 = m19895("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA", 49156);

    /* renamed from: ʻᵎ  reason: contains not printable characters */
    public static final CipherSuite f15923 = m19895("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA", 49157);

    /* renamed from: ʻᵔ  reason: contains not printable characters */
    public static final CipherSuite f15924 = m19895("TLS_ECDHE_ECDSA_WITH_NULL_SHA", 49158);

    /* renamed from: ʻᵢ  reason: contains not printable characters */
    public static final CipherSuite f15925 = m19895("TLS_ECDHE_ECDSA_WITH_RC4_128_SHA", 49159);

    /* renamed from: ʻⁱ  reason: contains not printable characters */
    public static final CipherSuite f15926 = m19895("TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA", 49160);

    /* renamed from: ʻﹳ  reason: contains not printable characters */
    public static final CipherSuite f15927 = m19895("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", 49161);

    /* renamed from: ʻﹶ  reason: contains not printable characters */
    public static final CipherSuite f15928 = m19895("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA", 49162);

    /* renamed from: ʻﾞ  reason: contains not printable characters */
    public static final CipherSuite f15929 = m19895("TLS_ECDH_RSA_WITH_NULL_SHA", 49163);

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final CipherSuite f15930 = m19895("SSL_RSA_EXPORT_WITH_DES40_CBC_SHA", 8);

    /* renamed from: ʼʻ  reason: contains not printable characters */
    public static final CipherSuite f15931 = m19895("TLS_ECDH_RSA_WITH_RC4_128_SHA", 49164);

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public static final CipherSuite f15932 = m19895("TLS_DHE_DSS_WITH_AES_256_CBC_SHA", 56);

    /* renamed from: ʼʽ  reason: contains not printable characters */
    public static final CipherSuite f15933 = m19895("TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA", 49165);

    /* renamed from: ʼʾ  reason: contains not printable characters */
    public static final CipherSuite f15934 = m19895("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA", 49166);

    /* renamed from: ʼʿ  reason: contains not printable characters */
    public static final CipherSuite f15935 = m19895("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA", 49167);

    /* renamed from: ʼˆ  reason: contains not printable characters */
    public static final CipherSuite f15936 = m19895("TLS_ECDHE_RSA_WITH_NULL_SHA", 49168);

    /* renamed from: ʼˈ  reason: contains not printable characters */
    public static final CipherSuite f15937 = m19895("TLS_ECDHE_RSA_WITH_RC4_128_SHA", 49169);

    /* renamed from: ʼˉ  reason: contains not printable characters */
    public static final CipherSuite f15938 = m19895("TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA", 49170);

    /* renamed from: ʼˊ  reason: contains not printable characters */
    public static final CipherSuite f15939 = m19895("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA", 49171);

    /* renamed from: ʼˋ  reason: contains not printable characters */
    public static final CipherSuite f15940 = m19895("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA", 49172);

    /* renamed from: ʼˎ  reason: contains not printable characters */
    public static final CipherSuite f15941 = m19895("TLS_ECDH_anon_WITH_NULL_SHA", 49173);

    /* renamed from: ʼˏ  reason: contains not printable characters */
    public static final CipherSuite f15942 = m19895("TLS_ECDH_anon_WITH_RC4_128_SHA", 49174);

    /* renamed from: ʼˑ  reason: contains not printable characters */
    public static final CipherSuite f15943 = m19895("TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA", 49175);

    /* renamed from: ʼי  reason: contains not printable characters */
    public static final CipherSuite f15944 = m19895("TLS_ECDH_anon_WITH_AES_128_CBC_SHA", 49176);

    /* renamed from: ʼـ  reason: contains not printable characters */
    public static final CipherSuite f15945 = m19895("TLS_ECDH_anon_WITH_AES_256_CBC_SHA", 49177);

    /* renamed from: ʼٴ  reason: contains not printable characters */
    public static final CipherSuite f15946 = m19895("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", 49187);

    /* renamed from: ʼᐧ  reason: contains not printable characters */
    public static final CipherSuite f15947 = m19895("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384", 49188);

    /* renamed from: ʼᴵ  reason: contains not printable characters */
    public static final CipherSuite f15948 = m19895("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256", 49189);

    /* renamed from: ʼᵎ  reason: contains not printable characters */
    public static final CipherSuite f15949 = m19895("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384", 49190);

    /* renamed from: ʼᵔ  reason: contains not printable characters */
    public static final CipherSuite f15950 = m19895("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", 49191);

    /* renamed from: ʼᵢ  reason: contains not printable characters */
    public static final CipherSuite f15951 = m19895("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384", 49192);

    /* renamed from: ʼⁱ  reason: contains not printable characters */
    public static final CipherSuite f15952 = m19895("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256", 49193);

    /* renamed from: ʼﹳ  reason: contains not printable characters */
    public static final CipherSuite f15953 = m19895("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384", 49194);

    /* renamed from: ʼﹶ  reason: contains not printable characters */
    public static final CipherSuite f15954 = m19895("TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", 49195);

    /* renamed from: ʼﾞ  reason: contains not printable characters */
    public static final CipherSuite f15955 = m19895("TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384", 49196);

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final CipherSuite f15956 = m19895("SSL_RSA_WITH_DES_CBC_SHA", 9);

    /* renamed from: ʽʻ  reason: contains not printable characters */
    public static final CipherSuite f15957 = m19895("TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256", 49197);

    /* renamed from: ʽʼ  reason: contains not printable characters */
    public static final CipherSuite f15958 = m19895("TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384", 49198);

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public static final CipherSuite f15959 = m19895("TLS_RSA_WITH_AES_256_CBC_SHA", 53);

    /* renamed from: ʽʾ  reason: contains not printable characters */
    public static final CipherSuite f15960 = m19895("TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", 49199);

    /* renamed from: ʽʿ  reason: contains not printable characters */
    public static final CipherSuite f15961 = m19895("TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", 49200);

    /* renamed from: ʽˆ  reason: contains not printable characters */
    public static final CipherSuite f15962 = m19895("TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256", 49201);

    /* renamed from: ʽˈ  reason: contains not printable characters */
    public static final CipherSuite f15963 = m19895("TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384", 49202);

    /* renamed from: ʽˉ  reason: contains not printable characters */
    public static final CipherSuite f15964 = m19895("TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA", 49205);

    /* renamed from: ʽˊ  reason: contains not printable characters */
    public static final CipherSuite f15965 = m19895("TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA", 49206);

    /* renamed from: ʽˋ  reason: contains not printable characters */
    public static final CipherSuite f15966 = m19895("TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256", 52392);

    /* renamed from: ʽˎ  reason: contains not printable characters */
    public static final CipherSuite f15967 = m19895("TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256", 52393);

    /* renamed from: ʽˑ  reason: contains not printable characters */
    private static final Map<String, CipherSuite> f15968 = new TreeMap(f16011);

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final CipherSuite f15969 = m19895("SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA", 20);

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public static final CipherSuite f15970 = m19895("TLS_DH_anon_WITH_AES_256_CBC_SHA", 58);

    /* renamed from: ʿ  reason: contains not printable characters */
    public static final CipherSuite f15971 = m19895("SSL_DHE_RSA_WITH_DES_CBC_SHA", 21);

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public static final CipherSuite f15972 = m19895("TLS_DHE_RSA_WITH_AES_256_CBC_SHA", 57);

    /* renamed from: ˆ  reason: contains not printable characters */
    public static final CipherSuite f15973 = m19895("SSL_DH_anon_WITH_3DES_EDE_CBC_SHA", 27);

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public static final CipherSuite f15974 = m19895("TLS_RSA_WITH_AES_128_CBC_SHA256", 60);

    /* renamed from: ˈ  reason: contains not printable characters */
    public static final CipherSuite f15975 = m19895("SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA", 19);

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public static final CipherSuite f15976 = m19895("TLS_DHE_DSS_WITH_AES_128_CBC_SHA256", 64);

    /* renamed from: ˉ  reason: contains not printable characters */
    public static final CipherSuite f15977 = m19895("TLS_KRB5_WITH_DES_CBC_SHA", 30);

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public static final CipherSuite f15978 = m19895("TLS_RSA_WITH_AES_256_CBC_SHA256", 61);

    /* renamed from: ˊ  reason: contains not printable characters */
    public static final CipherSuite f15979 = m19895("SSL_DH_anon_WITH_RC4_128_MD5", 24);

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public static final CipherSuite f15980 = m19895("TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA", 68);

    /* renamed from: ˋ  reason: contains not printable characters */
    public static final CipherSuite f15981 = m19895("SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA", 25);

    /* renamed from: ˋˋ  reason: contains not printable characters */
    public static final CipherSuite f15982 = m19895("TLS_RSA_WITH_CAMELLIA_128_CBC_SHA", 65);

    /* renamed from: ˎ  reason: contains not printable characters */
    public static final CipherSuite f15983 = m19895("SSL_DH_anon_WITH_DES_CBC_SHA", 26);

    /* renamed from: ˎˎ  reason: contains not printable characters */
    public static final CipherSuite f15984 = m19895("TLS_DHE_RSA_WITH_AES_128_CBC_SHA256", 103);

    /* renamed from: ˏ  reason: contains not printable characters */
    public static final CipherSuite f15985 = m19895("TLS_KRB5_WITH_3DES_EDE_CBC_SHA", 31);

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public static final CipherSuite f15986 = m19895("TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA", 69);

    /* renamed from: ˑ  reason: contains not printable characters */
    public static final CipherSuite f15987 = m19895("SSL_RSA_WITH_3DES_EDE_CBC_SHA", 10);

    /* renamed from: ˑˑ  reason: contains not printable characters */
    public static final CipherSuite f15988 = m19895("TLS_DHE_DSS_WITH_AES_256_CBC_SHA256", 106);

    /* renamed from: י  reason: contains not printable characters */
    public static final CipherSuite f15989 = m19895("TLS_KRB5_WITH_RC4_128_SHA", 32);

    /* renamed from: יי  reason: contains not printable characters */
    public static final CipherSuite f15990 = m19895("TLS_DH_anon_WITH_AES_128_CBC_SHA256", 108);

    /* renamed from: ـ  reason: contains not printable characters */
    public static final CipherSuite f15991 = m19895("TLS_KRB5_WITH_DES_CBC_MD5", 34);

    /* renamed from: ــ  reason: contains not printable characters */
    public static final CipherSuite f15992 = m19895("TLS_RSA_WITH_NULL_SHA256", 59);

    /* renamed from: ٴ  reason: contains not printable characters */
    public static final CipherSuite f15993 = m19895("SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA", 17);

    /* renamed from: ٴٴ  reason: contains not printable characters */
    public static final CipherSuite f15994 = m19895("TLS_PSK_WITH_RC4_128_SHA", TsExtractor.TS_STREAM_TYPE_DTS);

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static final CipherSuite f15995 = m19895("SSL_DHE_DSS_WITH_DES_CBC_SHA", 18);

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public static final CipherSuite f15996 = m19895("TLS_DHE_DSS_WITH_AES_128_CBC_SHA", 50);

    /* renamed from: ᴵ  reason: contains not printable characters */
    public static final CipherSuite f15997 = m19895("TLS_KRB5_WITH_3DES_EDE_CBC_MD5", 35);

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public static final CipherSuite f15998 = m19895("TLS_DHE_RSA_WITH_AES_128_CBC_SHA", 51);

    /* renamed from: ᵎ  reason: contains not printable characters */
    public static final CipherSuite f15999 = m19895("TLS_KRB5_WITH_RC4_128_MD5", 36);

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    public static final CipherSuite f16000 = m19895("TLS_DH_anon_WITH_AES_256_CBC_SHA256", 109);

    /* renamed from: ᵔ  reason: contains not printable characters */
    public static final CipherSuite f16001 = m19895("TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA", 38);

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    public static final CipherSuite f16002 = m19895("TLS_DHE_RSA_WITH_AES_256_CBC_SHA256", 107);

    /* renamed from: ᵢ  reason: contains not printable characters */
    public static final CipherSuite f16003 = m19895("TLS_KRB5_EXPORT_WITH_RC4_40_SHA", 40);

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    public static final CipherSuite f16004 = m19895("TLS_RSA_WITH_CAMELLIA_256_CBC_SHA", 132);

    /* renamed from: ⁱ  reason: contains not printable characters */
    public static final CipherSuite f16005 = m19895("TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5", 41);

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    public static final CipherSuite f16006 = m19895("TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA", TsExtractor.TS_STREAM_TYPE_E_AC3);

    /* renamed from: 连任  reason: contains not printable characters */
    public static final CipherSuite f16007 = m19895("SSL_RSA_WITH_RC4_128_MD5", 4);

    /* renamed from: 靐  reason: contains not printable characters */
    public static final CipherSuite f16008 = m19895("SSL_RSA_WITH_NULL_MD5", 1);

    /* renamed from: 麤  reason: contains not printable characters */
    public static final CipherSuite f16009 = m19895("SSL_RSA_EXPORT_WITH_RC4_40_MD5", 3);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final CipherSuite f16010 = m19895("SSL_RSA_WITH_NULL_SHA", 2);

    /* renamed from: 龘  reason: contains not printable characters */
    static final Comparator<String> f16011 = new Comparator<String>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(String str, String str2) {
            int min = Math.min(str.length(), str2.length());
            for (int i = 4; i < min; i++) {
                char charAt = str.charAt(i);
                char charAt2 = str2.charAt(i);
                if (charAt != charAt2) {
                    return charAt < charAt2 ? -1 : 1;
                }
            }
            int length = str.length();
            int length2 = str2.length();
            if (length != length2) {
                return length >= length2 ? 1 : -1;
            }
            return 0;
        }
    };

    /* renamed from: ﹳ  reason: contains not printable characters */
    public static final CipherSuite f16012 = m19895("TLS_KRB5_EXPORT_WITH_RC4_40_MD5", 43);

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    public static final CipherSuite f16013 = m19895("TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA", 136);

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static final CipherSuite f16014 = m19895("SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA", 22);

    /* renamed from: ﹶﹶ  reason: contains not printable characters */
    public static final CipherSuite f16015 = m19895("TLS_PSK_WITH_3DES_EDE_CBC_SHA", 139);

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static final CipherSuite f16016 = m19895("SSL_DH_anon_EXPORT_WITH_RC4_40_MD5", 23);

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public static final CipherSuite f16017 = m19895("TLS_RSA_WITH_AES_128_CBC_SHA", 47);

    /* renamed from: ʽˏ  reason: contains not printable characters */
    final String f16018;

    private CipherSuite(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f16018 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static List<CipherSuite> m19893(String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String r0 : strArr) {
            arrayList.add(m19894(r0));
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized CipherSuite m19894(String str) {
        CipherSuite cipherSuite;
        synchronized (CipherSuite.class) {
            cipherSuite = f15968.get(str);
            if (cipherSuite == null) {
                cipherSuite = new CipherSuite(str);
                f15968.put(str, cipherSuite);
            }
        }
        return cipherSuite;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static CipherSuite m19895(String str, int i) {
        return m19894(str);
    }

    public String toString() {
        return this.f16018;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m19896() {
        return this.f16018;
    }
}
