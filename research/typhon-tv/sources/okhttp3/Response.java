package okhttp3;

import java.io.Closeable;
import javax.annotation.Nullable;
import okhttp3.Headers;

public final class Response implements Closeable {

    /* renamed from: ʻ  reason: contains not printable characters */
    final Headers f6373;
    @Nullable

    /* renamed from: ʼ  reason: contains not printable characters */
    final ResponseBody f6374;
    @Nullable

    /* renamed from: ʽ  reason: contains not printable characters */
    final Response f6375;

    /* renamed from: ʾ  reason: contains not printable characters */
    private volatile CacheControl f6376;

    /* renamed from: ˈ  reason: contains not printable characters */
    final long f6377;
    @Nullable

    /* renamed from: ˑ  reason: contains not printable characters */
    final Response f6378;
    @Nullable

    /* renamed from: ٴ  reason: contains not printable characters */
    final Response f6379;

    /* renamed from: ᐧ  reason: contains not printable characters */
    final long f6380;
    @Nullable

    /* renamed from: 连任  reason: contains not printable characters */
    final Handshake f6381;

    /* renamed from: 靐  reason: contains not printable characters */
    final Protocol f6382;

    /* renamed from: 麤  reason: contains not printable characters */
    final String f6383;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f6384;

    /* renamed from: 龘  reason: contains not printable characters */
    final Request f6385;

    public static class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        Headers.Builder f6386;

        /* renamed from: ʼ  reason: contains not printable characters */
        ResponseBody f6387;

        /* renamed from: ʽ  reason: contains not printable characters */
        Response f6388;

        /* renamed from: ˈ  reason: contains not printable characters */
        long f6389;

        /* renamed from: ˑ  reason: contains not printable characters */
        Response f6390;

        /* renamed from: ٴ  reason: contains not printable characters */
        Response f6391;

        /* renamed from: ᐧ  reason: contains not printable characters */
        long f6392;
        @Nullable

        /* renamed from: 连任  reason: contains not printable characters */
        Handshake f6393;

        /* renamed from: 靐  reason: contains not printable characters */
        Protocol f6394;

        /* renamed from: 麤  reason: contains not printable characters */
        String f6395;

        /* renamed from: 齉  reason: contains not printable characters */
        int f6396;

        /* renamed from: 龘  reason: contains not printable characters */
        Request f6397;

        public Builder() {
            this.f6396 = -1;
            this.f6386 = new Headers.Builder();
        }

        Builder(Response response) {
            this.f6396 = -1;
            this.f6397 = response.f6385;
            this.f6394 = response.f6382;
            this.f6396 = response.f6384;
            this.f6395 = response.f6383;
            this.f6393 = response.f6381;
            this.f6386 = response.f6373.m6932();
            this.f6387 = response.f6374;
            this.f6388 = response.f6375;
            this.f6390 = response.f6378;
            this.f6391 = response.f6379;
            this.f6392 = response.f6380;
            this.f6389 = response.f6377;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m7070(Response response) {
            if (response.f6374 != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m7071(String str, Response response) {
            if (response.f6374 != null) {
                throw new IllegalArgumentException(str + ".body != null");
            } else if (response.f6375 != null) {
                throw new IllegalArgumentException(str + ".networkResponse != null");
            } else if (response.f6378 != null) {
                throw new IllegalArgumentException(str + ".cacheResponse != null");
            } else if (response.f6379 != null) {
                throw new IllegalArgumentException(str + ".priorResponse != null");
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m7072(long j) {
            this.f6389 = j;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m7073(String str) {
            this.f6386.m19949(str);
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m7074(String str, String str2) {
            this.f6386.m19954(str, str2);
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m7075(@Nullable Response response) {
            if (response != null) {
                m7071("cacheResponse", response);
            }
            this.f6390 = response;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m7076(@Nullable Response response) {
            if (response != null) {
                m7070(response);
            }
            this.f6391 = response;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7077(int i) {
            this.f6396 = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7078(long j) {
            this.f6392 = j;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7079(String str) {
            this.f6395 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7080(String str, String str2) {
            this.f6386.m19952(str, str2);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7081(@Nullable Handshake handshake) {
            this.f6393 = handshake;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7082(Headers headers) {
            this.f6386 = headers.m6932();
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7083(Protocol protocol) {
            this.f6394 = protocol;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7084(Request request) {
            this.f6397 = request;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7085(@Nullable Response response) {
            if (response != null) {
                m7071("networkResponse", response);
            }
            this.f6388 = response;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7086(@Nullable ResponseBody responseBody) {
            this.f6387 = responseBody;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Response m7087() {
            if (this.f6397 == null) {
                throw new IllegalStateException("request == null");
            } else if (this.f6394 == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.f6396 < 0) {
                throw new IllegalStateException("code < 0: " + this.f6396);
            } else if (this.f6395 != null) {
                return new Response(this);
            } else {
                throw new IllegalStateException("message == null");
            }
        }
    }

    Response(Builder builder) {
        this.f6385 = builder.f6397;
        this.f6382 = builder.f6394;
        this.f6384 = builder.f6396;
        this.f6383 = builder.f6395;
        this.f6381 = builder.f6393;
        this.f6373 = builder.f6386.m19955();
        this.f6374 = builder.f6387;
        this.f6375 = builder.f6388;
        this.f6378 = builder.f6390;
        this.f6379 = builder.f6391;
        this.f6380 = builder.f6392;
        this.f6377 = builder.f6389;
    }

    public void close() {
        if (this.f6374 == null) {
            throw new IllegalStateException("response is not eligible for a body and must not be closed");
        }
        this.f6374.close();
    }

    public String toString() {
        return "Response{protocol=" + this.f6382 + ", code=" + this.f6384 + ", message=" + this.f6383 + ", url=" + this.f6385.m7053() + '}';
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Handshake m7054() {
        return this.f6381;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Headers m7055() {
        return this.f6373;
    }

    @Nullable
    /* renamed from: ʽ  reason: contains not printable characters */
    public ResponseBody m7056() {
        return this.f6374;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public long m7057() {
        return this.f6380;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public long m7058() {
        return this.f6377;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public CacheControl m7059() {
        CacheControl cacheControl = this.f6376;
        if (cacheControl != null) {
            return cacheControl;
        }
        CacheControl r0 = CacheControl.m6860(this.f6373);
        this.f6376 = r0;
        return r0;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public Builder m7060() {
        return new Builder(this);
    }

    @Nullable
    /* renamed from: ٴ  reason: contains not printable characters */
    public Response m7061() {
        return this.f6375;
    }

    @Nullable
    /* renamed from: ᐧ  reason: contains not printable characters */
    public Response m7062() {
        return this.f6379;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m7063() {
        return this.f6383;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Protocol m7064() {
        return this.f6382;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m7065() {
        return this.f6384 >= 200 && this.f6384 < 300;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m7066() {
        return this.f6384;
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m7067(String str) {
        return m7068(str, (String) null);
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m7068(String str, @Nullable String str2) {
        String r0 = this.f6373.m6936(str);
        return r0 != null ? r0 : str2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Request m7069() {
        return this.f6385;
    }
}
