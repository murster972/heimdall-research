package okhttp3;

import java.util.List;
import javax.annotation.Nullable;
import okhttp3.Headers;
import okhttp3.internal.http.HttpMethod;
import org.apache.oltu.oauth2.common.OAuth;

public final class Request {

    /* renamed from: ʻ  reason: contains not printable characters */
    private volatile CacheControl f6359;

    /* renamed from: 连任  reason: contains not printable characters */
    final Object f6360;

    /* renamed from: 靐  reason: contains not printable characters */
    final String f6361;
    @Nullable

    /* renamed from: 麤  reason: contains not printable characters */
    final RequestBody f6362;

    /* renamed from: 齉  reason: contains not printable characters */
    final Headers f6363;

    /* renamed from: 龘  reason: contains not printable characters */
    final HttpUrl f6364;

    public static class Builder {

        /* renamed from: 连任  reason: contains not printable characters */
        Object f16075;

        /* renamed from: 靐  reason: contains not printable characters */
        String f16076;

        /* renamed from: 麤  reason: contains not printable characters */
        RequestBody f16077;

        /* renamed from: 齉  reason: contains not printable characters */
        Headers.Builder f16078;

        /* renamed from: 龘  reason: contains not printable characters */
        HttpUrl f16079;

        public Builder() {
            this.f16076 = OAuth.HttpMethod.GET;
            this.f16078 = new Headers.Builder();
        }

        Builder(Request request) {
            this.f16079 = request.f6364;
            this.f16076 = request.f6361;
            this.f16077 = request.f6362;
            this.f16075 = request.f6360;
            this.f16078 = request.f6363.m6932();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19986() {
            return m19994("HEAD", (RequestBody) null);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19987(String str) {
            this.f16078.m19949(str);
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19988(String str, String str2) {
            this.f16078.m19954(str, str2);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Request m19989() {
            if (this.f16079 != null) {
                return new Request(this);
            }
            throw new IllegalStateException("url == null");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19990() {
            return m19994(OAuth.HttpMethod.GET, (RequestBody) null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19991(Object obj) {
            this.f16075 = obj;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19992(String str) {
            if (str == null) {
                throw new NullPointerException("url == null");
            }
            if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                str = "http:" + str.substring(3);
            } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                str = "https:" + str.substring(4);
            }
            HttpUrl r6 = HttpUrl.m6937(str);
            if (r6 != null) {
                return m19997(r6);
            }
            throw new IllegalArgumentException("unexpected url: " + str);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19993(String str, String str2) {
            this.f16078.m19952(str, str2);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19994(String str, @Nullable RequestBody requestBody) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (requestBody != null && !HttpMethod.m20134(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (requestBody != null || !HttpMethod.m20132(str)) {
                this.f16076 = str;
                this.f16077 = requestBody;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19995(CacheControl cacheControl) {
            String cacheControl2 = cacheControl.toString();
            return cacheControl2.isEmpty() ? m19987("Cache-Control") : m19993("Cache-Control", cacheControl2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19996(Headers headers) {
            this.f16078 = headers.m6932();
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19997(HttpUrl httpUrl) {
            if (httpUrl == null) {
                throw new NullPointerException("url == null");
            }
            this.f16079 = httpUrl;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19998(RequestBody requestBody) {
            return m19994(OAuth.HttpMethod.POST, requestBody);
        }
    }

    Request(Builder builder) {
        this.f6364 = builder.f16079;
        this.f6361 = builder.f16076;
        this.f6363 = builder.f16078.m19955();
        this.f6362 = builder.f16077;
        this.f6360 = builder.f16075 != null ? builder.f16075 : this;
    }

    public String toString() {
        return "Request{method=" + this.f6361 + ", url=" + this.f6364 + ", tag=" + (this.f6360 != this ? this.f6360 : null) + '}';
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Builder m7044() {
        return new Builder(this);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public CacheControl m7045() {
        CacheControl cacheControl = this.f6359;
        if (cacheControl != null) {
            return cacheControl;
        }
        CacheControl r0 = CacheControl.m6860(this.f6363);
        this.f6359 = r0;
        return r0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m7046() {
        return this.f6364.m6965();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Object m7047() {
        return this.f6360;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m7048() {
        return this.f6361;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public List<String> m7049(String str) {
        return this.f6363.m6931(str);
    }

    @Nullable
    /* renamed from: 麤  reason: contains not printable characters */
    public RequestBody m7050() {
        return this.f6362;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Headers m7051() {
        return this.f6363;
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m7052(String str) {
        return this.f6363.m6936(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpUrl m7053() {
        return this.f6364;
    }
}
