package okhttp3;

import com.google.android.exoplayer2.C;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import okhttp3.internal.Util;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.RouteDatabase;
import okhttp3.internal.connection.StreamAllocation;
import okhttp3.internal.platform.Platform;

public final class ConnectionPool {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Executor f6236 = new ThreadPoolExecutor(0, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, 60, TimeUnit.SECONDS, new SynchronousQueue(), Util.m7122("OkHttp ConnectionPool", true));

    /* renamed from: 齉  reason: contains not printable characters */
    static final /* synthetic */ boolean f6237 = (!ConnectionPool.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    private final long f6238;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Runnable f6239;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Deque<RealConnection> f6240;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f6241;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean f6242;

    /* renamed from: 龘  reason: contains not printable characters */
    final RouteDatabase f6243;

    public ConnectionPool() {
        this(5, 5, TimeUnit.MINUTES);
    }

    public ConnectionPool(int i, long j, TimeUnit timeUnit) {
        this.f6239 = new Runnable() {
            public void run() {
                while (true) {
                    long r2 = ConnectionPool.this.m6879(System.nanoTime());
                    if (r2 != -1) {
                        if (r2 > 0) {
                            long j = r2 / C.MICROS_PER_SECOND;
                            long j2 = r2 - (j * C.MICROS_PER_SECOND);
                            synchronized (ConnectionPool.this) {
                                try {
                                    ConnectionPool.this.wait(j, (int) j2);
                                } catch (InterruptedException e) {
                                }
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        };
        this.f6240 = new ArrayDeque();
        this.f6243 = new RouteDatabase();
        this.f6241 = i;
        this.f6238 = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m6877(RealConnection realConnection, long j) {
        List<Reference<StreamAllocation>> list = realConnection.f16142;
        int i = 0;
        while (i < list.size()) {
            Reference reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                Platform.m7184().m7194("A connection to " + realConnection.m20061().m20002().m6838() + " was leaked. Did you forget to close a response body?", ((StreamAllocation.StreamAllocationReference) reference).f16173);
                list.remove(i);
                realConnection.f16144 = true;
                if (list.isEmpty()) {
                    realConnection.f16140 = j - this.f6238;
                    return 0;
                }
            }
        }
        return list.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m6878(RealConnection realConnection) {
        if (!f6237 && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (realConnection.f16144 || this.f6241 == 0) {
            this.f6240.remove(realConnection);
            return true;
        } else {
            notifyAll();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m6879(long j) {
        int i = 0;
        int i2 = 0;
        RealConnection realConnection = null;
        long j2 = Long.MIN_VALUE;
        synchronized (this) {
            for (RealConnection next : this.f6240) {
                if (m6877(next, j) > 0) {
                    i++;
                } else {
                    i2++;
                    long j3 = j - next.f16140;
                    if (j3 > j2) {
                        j2 = j3;
                        realConnection = next;
                    }
                }
            }
            if (j2 >= this.f6238 || i2 > this.f6241) {
                this.f6240.remove(realConnection);
                Util.m7125(realConnection.m20062());
                return 0;
            } else if (i2 > 0) {
                long j4 = this.f6238 - j2;
                return j4;
            } else if (i > 0) {
                long j5 = this.f6238;
                return j5;
            } else {
                this.f6242 = false;
                return -1;
            }
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public Socket m6880(Address address, StreamAllocation streamAllocation) {
        if (f6237 || Thread.holdsLock(this)) {
            for (RealConnection next : this.f6240) {
                if (next.m20069(address, (Route) null) && next.m20059() && next != streamAllocation.m20098()) {
                    return streamAllocation.m20099(next);
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public RealConnection m6881(Address address, StreamAllocation streamAllocation, Route route) {
        if (f6237 || Thread.holdsLock(this)) {
            for (RealConnection next : this.f6240) {
                if (next.m20069(address, route)) {
                    streamAllocation.m20103(next, true);
                    return next;
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6882(RealConnection realConnection) {
        if (f6237 || Thread.holdsLock(this)) {
            if (!this.f6242) {
                this.f6242 = true;
                f6236.execute(this.f6239);
            }
            this.f6240.add(realConnection);
            return;
        }
        throw new AssertionError();
    }
}
