package okhttp3;

import com.mopub.common.TyphoonApp;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import okhttp3.internal.Util;
import okio.Buffer;

public final class HttpUrl {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final char[] f6277 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f6278;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final List<String> f6279;
    @Nullable

    /* renamed from: ʽ  reason: contains not printable characters */
    private final List<String> f6280;
    @Nullable

    /* renamed from: ˑ  reason: contains not printable characters */
    private final String f6281;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final String f6282;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f6283;

    /* renamed from: 靐  reason: contains not printable characters */
    final String f6284;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f6285;

    /* renamed from: 龘  reason: contains not printable characters */
    final String f6286;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        final List<String> f6287 = new ArrayList();
        @Nullable

        /* renamed from: ʼ  reason: contains not printable characters */
        List<String> f6288;
        @Nullable

        /* renamed from: ʽ  reason: contains not printable characters */
        String f6289;

        /* renamed from: 连任  reason: contains not printable characters */
        int f6290 = -1;

        /* renamed from: 靐  reason: contains not printable characters */
        String f6291 = "";
        @Nullable

        /* renamed from: 麤  reason: contains not printable characters */
        String f6292;

        /* renamed from: 齉  reason: contains not printable characters */
        String f6293 = "";
        @Nullable

        /* renamed from: 龘  reason: contains not printable characters */
        String f6294;

        enum ParseResult {
            SUCCESS,
            MISSING_SCHEME,
            UNSUPPORTED_SCHEME,
            INVALID_PORT,
            INVALID_HOST
        }

        public Builder() {
            this.f6287.add("");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private static int m6968(String str, int i, int i2) {
            try {
                int parseInt = Integer.parseInt(HttpUrl.m6941(str, i, i2, "", false, false, false, true, (Charset) null));
                if (parseInt <= 0 || parseInt > 65535) {
                    return -1;
                }
                return parseInt;
            } catch (NumberFormatException e) {
                return -1;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean m6969(String str) {
            return str.equals(".") || str.equalsIgnoreCase("%2e");
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean m6970(String str) {
            return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private static String m6971(String str, int i, int i2) {
            return Util.m7115(HttpUrl.m6942(str, i, i2, false));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private static int m6972(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
                return -1;
            }
            int i3 = i + 1;
            while (i3 < i2) {
                char charAt2 = str.charAt(i3);
                if ((charAt2 >= 'a' && charAt2 <= 'z') || ((charAt2 >= 'A' && charAt2 <= 'Z') || ((charAt2 >= '0' && charAt2 <= '9') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.'))) {
                    i3++;
                } else if (charAt2 != ':') {
                    return -1;
                } else {
                    return i3;
                }
            }
            return -1;
        }

        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x000f, code lost:
            if (r0 >= r5) goto L_0x000a;
         */
        /* renamed from: 麤  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static int m6973(java.lang.String r3, int r4, int r5) {
            /*
                r0 = r4
            L_0x0001:
                if (r0 >= r5) goto L_0x001a
                char r1 = r3.charAt(r0)
                switch(r1) {
                    case 58: goto L_0x001b;
                    case 91: goto L_0x000d;
                    default: goto L_0x000a;
                }
            L_0x000a:
                int r0 = r0 + 1
                goto L_0x0001
            L_0x000d:
                int r0 = r0 + 1
                if (r0 >= r5) goto L_0x000a
                char r1 = r3.charAt(r0)
                r2 = 93
                if (r1 != r2) goto L_0x000d
                goto L_0x000a
            L_0x001a:
                r0 = r5
            L_0x001b:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.HttpUrl.Builder.m6973(java.lang.String, int, int):int");
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m6974() {
            if (!this.f6287.remove(this.f6287.size() - 1).isEmpty() || this.f6287.isEmpty()) {
                this.f6287.add("");
            } else {
                this.f6287.set(this.f6287.size() - 1, "");
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private static int m6975(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m6976(String str, int i, int i2) {
            if (i != i2) {
                char charAt = str.charAt(i);
                if (charAt == '/' || charAt == '\\') {
                    this.f6287.clear();
                    this.f6287.add("");
                    i++;
                } else {
                    this.f6287.set(this.f6287.size() - 1, "");
                }
                int i3 = i;
                while (i3 < i2) {
                    int r3 = Util.m7111(str, i3, i2, "/\\");
                    boolean z = r3 < i2;
                    m6977(str, i3, r3, z, true);
                    i3 = r3;
                    if (z) {
                        i3++;
                    }
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m6977(String str, int i, int i2, boolean z, boolean z2) {
            String r9 = HttpUrl.m6941(str, i, i2, " \"<>^`{}|/\\?#", z2, false, false, true, (Charset) null);
            if (!m6969(r9)) {
                if (m6970(r9)) {
                    m6974();
                    return;
                }
                if (this.f6287.get(this.f6287.size() - 1).isEmpty()) {
                    this.f6287.set(this.f6287.size() - 1, r9);
                } else {
                    this.f6287.add(r9);
                }
                if (z) {
                    this.f6287.add("");
                }
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f6294);
            sb.append("://");
            if (!this.f6291.isEmpty() || !this.f6293.isEmpty()) {
                sb.append(this.f6291);
                if (!this.f6293.isEmpty()) {
                    sb.append(':');
                    sb.append(this.f6293);
                }
                sb.append('@');
            }
            if (this.f6292.indexOf(58) != -1) {
                sb.append('[');
                sb.append(this.f6292);
                sb.append(']');
            } else {
                sb.append(this.f6292);
            }
            int r0 = m6985();
            if (r0 != HttpUrl.m6940(this.f6294)) {
                sb.append(':');
                sb.append(r0);
            }
            HttpUrl.m6947(sb, this.f6287);
            if (this.f6288 != null) {
                sb.append('?');
                HttpUrl.m6939(sb, this.f6288);
            }
            if (this.f6289 != null) {
                sb.append('#');
                sb.append(this.f6289);
            }
            return sb.toString();
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public Builder m6978(@Nullable String str) {
            this.f6288 = str != null ? HttpUrl.m6938(HttpUrl.m6943(str, " \"'<>#", true, false, true, true)) : null;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m6979() {
            int size = this.f6287.size();
            for (int i = 0; i < size; i++) {
                this.f6287.set(i, HttpUrl.m6943(this.f6287.get(i), "[]", true, true, false, true));
            }
            if (this.f6288 != null) {
                int size2 = this.f6288.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    String str = this.f6288.get(i2);
                    if (str != null) {
                        this.f6288.set(i2, HttpUrl.m6943(str, "\\^`{|}", true, true, true, true));
                    }
                }
            }
            if (this.f6289 != null) {
                this.f6289 = HttpUrl.m6943(this.f6289, " \"#<>\\^`{|}", true, true, false, false);
            }
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m6980(String str) {
            if (str == null) {
                throw new NullPointerException("username == null");
            }
            this.f6291 = HttpUrl.m6943(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m6981(String str, @Nullable String str2) {
            if (str == null) {
                throw new NullPointerException("encodedName == null");
            }
            if (this.f6288 == null) {
                this.f6288 = new ArrayList();
            }
            this.f6288.add(HttpUrl.m6943(str, " \"'<>#&=", true, false, true, true));
            this.f6288.add(str2 != null ? HttpUrl.m6943(str2, " \"'<>#&=", true, false, true, true) : null);
            return this;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Builder m6982(String str) {
            if (str == null) {
                throw new NullPointerException("host == null");
            }
            String r0 = m6971(str, 0, str.length());
            if (r0 == null) {
                throw new IllegalArgumentException("unexpected host: " + str);
            }
            this.f6292 = r0;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m6983(String str) {
            if (str == null) {
                throw new NullPointerException("password == null");
            }
            this.f6293 = HttpUrl.m6943(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public HttpUrl m6984() {
            if (this.f6294 == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.f6292 != null) {
                return new HttpUrl(this);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m6985() {
            return this.f6290 != -1 ? this.f6290 : HttpUrl.m6940(this.f6294);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public ParseResult m6986(@Nullable HttpUrl httpUrl, String str) {
            int r5 = Util.m7109(str, 0, str.length());
            int r24 = Util.m7101(str, r5, str.length());
            if (m6972(str, r5, r24) != -1) {
                if (str.regionMatches(true, r5, "https:", 0, 6)) {
                    this.f6294 = TyphoonApp.HTTPS;
                    r5 += "https:".length();
                } else if (!str.regionMatches(true, r5, "http:", 0, 5)) {
                    return ParseResult.UNSUPPORTED_SCHEME;
                } else {
                    this.f6294 = TyphoonApp.HTTP;
                    r5 += "http:".length();
                }
            } else if (httpUrl == null) {
                return ParseResult.MISSING_SCHEME;
            } else {
                this.f6294 = httpUrl.f6286;
            }
            boolean z = false;
            boolean z2 = false;
            int r28 = m6975(str, r5, r24);
            if (r28 >= 2 || httpUrl == null || !httpUrl.f6286.equals(this.f6294)) {
                int i = r5 + r28;
                while (true) {
                    int r21 = Util.m7111(str, i, r24, "@/\\?#");
                    switch (r21 != r24 ? str.charAt(r21) : 65535) {
                        case 65535:
                        case '#':
                        case '/':
                        case '?':
                        case '\\':
                            int r26 = m6973(str, i, r21);
                            if (r26 + 1 < r21) {
                                this.f6292 = m6971(str, i, r26);
                                this.f6290 = m6968(str, r26 + 1, r21);
                                if (this.f6290 == -1) {
                                    return ParseResult.INVALID_PORT;
                                }
                            } else {
                                this.f6292 = m6971(str, i, r26);
                                this.f6290 = HttpUrl.m6940(this.f6294);
                            }
                            if (this.f6292 != null) {
                                r5 = r21;
                                break;
                            } else {
                                return ParseResult.INVALID_HOST;
                            }
                        case '@':
                            if (!z2) {
                                int r6 = Util.m7110(str, i, r21, ':');
                                String r20 = HttpUrl.m6941(str, i, r6, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, (Charset) null);
                                if (z) {
                                    r20 = this.f6291 + "%40" + r20;
                                }
                                this.f6291 = r20;
                                if (r6 != r21) {
                                    z2 = true;
                                    this.f6293 = HttpUrl.m6941(str, r6 + 1, r21, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, (Charset) null);
                                }
                                z = true;
                            } else {
                                this.f6293 += "%40" + HttpUrl.m6941(str, i, r21, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, (Charset) null);
                            }
                            i = r21 + 1;
                            continue;
                    }
                }
            } else {
                this.f6291 = httpUrl.m6962();
                this.f6293 = httpUrl.m6960();
                this.f6292 = httpUrl.f6284;
                this.f6290 = httpUrl.f6285;
                this.f6287.clear();
                this.f6287.addAll(httpUrl.m6957());
                if (r5 == r24 || str.charAt(r5) == '#') {
                    m6978(httpUrl.m6959());
                }
            }
            int r25 = Util.m7111(str, r5, r24, "?#");
            m6976(str, r5, r25);
            int i2 = r25;
            if (i2 < r24 && str.charAt(i2) == '?') {
                int r9 = Util.m7110(str, i2, r24, '#');
                this.f6288 = HttpUrl.m6938(HttpUrl.m6941(str, i2 + 1, r9, " \"'<>#", true, false, true, true, (Charset) null));
                i2 = r9;
            }
            if (i2 < r24 && str.charAt(i2) == '#') {
                this.f6289 = HttpUrl.m6941(str, i2 + 1, r24, "", true, false, false, false, (Charset) null);
            }
            return ParseResult.SUCCESS;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m6987(int i) {
            if (i <= 0 || i > 65535) {
                throw new IllegalArgumentException("unexpected port: " + i);
            }
            this.f6290 = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m6988(String str) {
            if (str == null) {
                throw new NullPointerException("scheme == null");
            }
            if (str.equalsIgnoreCase(TyphoonApp.HTTP)) {
                this.f6294 = TyphoonApp.HTTP;
            } else if (str.equalsIgnoreCase(TyphoonApp.HTTPS)) {
                this.f6294 = TyphoonApp.HTTPS;
            } else {
                throw new IllegalArgumentException("unexpected scheme: " + str);
            }
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m6989(String str, @Nullable String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            }
            if (this.f6288 == null) {
                this.f6288 = new ArrayList();
            }
            this.f6288.add(HttpUrl.m6943(str, " !\"#$&'(),/:;<=>?@[]\\^`{|}~", false, false, true, true));
            this.f6288.add(str2 != null ? HttpUrl.m6943(str2, " !\"#$&'(),/:;<=>?@[]\\^`{|}~", false, false, true, true) : null);
            return this;
        }
    }

    HttpUrl(Builder builder) {
        String str = null;
        this.f6286 = builder.f6294;
        this.f6283 = m6945(builder.f6291, false);
        this.f6278 = m6945(builder.f6293, false);
        this.f6284 = builder.f6292;
        this.f6285 = builder.m6985();
        this.f6279 = m6946(builder.f6287, false);
        this.f6280 = builder.f6288 != null ? m6946(builder.f6288, true) : null;
        this.f6281 = builder.f6289 != null ? m6945(builder.f6289, false) : str;
        this.f6282 = builder.toString();
    }

    @Nullable
    /* renamed from: 连任  reason: contains not printable characters */
    public static HttpUrl m6937(String str) {
        Builder builder = new Builder();
        if (builder.m6986((HttpUrl) null, str) == Builder.ParseResult.SUCCESS) {
            return builder.m6984();
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static List<String> m6938(String str) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i <= str.length()) {
            int indexOf = str.indexOf(38, i);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i, indexOf));
                arrayList.add((Object) null);
            } else {
                arrayList.add(str.substring(i, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i = indexOf + 1;
        }
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static void m6939(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i += 2) {
            String str = list.get(i);
            String str2 = list.get(i + 1);
            if (i > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m6940(String str) {
        if (str.equals(TyphoonApp.HTTP)) {
            return 80;
        }
        return str.equals(TyphoonApp.HTTPS) ? 443 : -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6941(String str, int i, int i2, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        int i3 = i;
        while (i3 < i2) {
            int codePointAt = str.codePointAt(i3);
            if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || ((codePointAt == 37 && (!z || (z2 && !m6950(str, i3, i2)))) || (codePointAt == 43 && z3)))) {
                Buffer buffer = new Buffer();
                buffer.m7267(str, i, i3);
                m6948(buffer, str, i3, i2, str2, z, z2, z3, z4, charset);
                return buffer.m7224();
            }
            i3 += Character.charCount(codePointAt);
        }
        return str.substring(i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6942(String str, int i, int i2, boolean z) {
        for (int i3 = i; i3 < i2; i3++) {
            char charAt = str.charAt(i3);
            if (charAt == '%' || (charAt == '+' && z)) {
                Buffer buffer = new Buffer();
                buffer.m7267(str, i, i3);
                m6949(buffer, str, i3, i2, z);
                return buffer.m7224();
            }
        }
        return str.substring(i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6943(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        return m6941(str, 0, str.length(), str2, z, z2, z3, z4, (Charset) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6944(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        return m6941(str, 0, str.length(), str2, z, z2, z3, z4, charset);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6945(String str, boolean z) {
        return m6942(str, 0, str.length(), z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<String> m6946(List<String> list, boolean z) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            String str = list.get(i);
            arrayList.add(str != null ? m6945(str, z) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m6947(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            sb.append('/');
            sb.append(list.get(i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m6948(Buffer buffer, String str, int i, int i2, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        Buffer buffer2 = null;
        int i3 = i;
        while (i3 < i2) {
            int codePointAt = str.codePointAt(i3);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt == 43 && z3) {
                    buffer.m7246(z ? "+" : "%2B");
                } else if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && (!z || (z2 && !m6950(str, i3, i2)))))) {
                    if (buffer2 == null) {
                        buffer2 = new Buffer();
                    }
                    if (charset == null || charset.equals(Util.f6413)) {
                        buffer2.m7265(codePointAt);
                    } else {
                        buffer2.m7268(str, i3, Character.charCount(codePointAt) + i3, charset);
                    }
                    while (!buffer2.m7210()) {
                        byte r0 = buffer2.m7228() & 255;
                        buffer.m7238(37);
                        buffer.m7238((int) f6277[(r0 >> 4) & 15]);
                        buffer.m7238((int) f6277[r0 & 15]);
                    }
                } else {
                    buffer.m7265(codePointAt);
                }
            }
            i3 += Character.charCount(codePointAt);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m6949(Buffer buffer, String str, int i, int i2, boolean z) {
        int i3 = i;
        while (i3 < i2) {
            int codePointAt = str.codePointAt(i3);
            if (codePointAt != 37 || i3 + 2 >= i2) {
                if (codePointAt == 43 && z) {
                    buffer.m7238(32);
                }
                buffer.m7265(codePointAt);
            } else {
                int r1 = Util.m7108(str.charAt(i3 + 1));
                int r2 = Util.m7108(str.charAt(i3 + 2));
                if (!(r1 == -1 || r2 == -1)) {
                    buffer.m7238((r1 << 4) + r2);
                    i3 += 2;
                }
                buffer.m7265(codePointAt);
            }
            i3 += Character.charCount(codePointAt);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m6950(String str, int i, int i2) {
        return i + 2 < i2 && str.charAt(i) == '%' && Util.m7108(str.charAt(i + 1)) != -1 && Util.m7108(str.charAt(i + 2)) != -1;
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof HttpUrl) && ((HttpUrl) obj).f6282.equals(this.f6282);
    }

    public int hashCode() {
        return this.f6282.hashCode();
    }

    public String toString() {
        return this.f6282;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m6951() {
        return this.f6284;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6952() {
        return this.f6285;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m6953() {
        int indexOf = this.f6282.indexOf(47, this.f6286.length() + 3);
        return this.f6282.substring(indexOf, Util.m7111(this.f6282, indexOf, this.f6282.length(), "?#"));
    }

    @Nullable
    /* renamed from: ʾ  reason: contains not printable characters */
    public String m6954() {
        if (this.f6281 == null) {
            return null;
        }
        return this.f6282.substring(this.f6282.indexOf(35) + 1);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public String m6955() {
        return m6963("/...").m6980("").m6983("").m6984().toString();
    }

    @Nullable
    /* renamed from: ˈ  reason: contains not printable characters */
    public String m6956() {
        if (this.f6280 == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        m6939(sb, this.f6280);
        return sb.toString();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public List<String> m6957() {
        int indexOf = this.f6282.indexOf(47, this.f6286.length() + 3);
        int r1 = Util.m7111(this.f6282, indexOf, this.f6282.length(), "?#");
        ArrayList arrayList = new ArrayList();
        int i = indexOf;
        while (i < r1) {
            int i2 = i + 1;
            int r4 = Util.m7110(this.f6282, i2, r1, '/');
            arrayList.add(this.f6282.substring(i2, r4));
            i = r4;
        }
        return arrayList;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public List<String> m6958() {
        return this.f6279;
    }

    @Nullable
    /* renamed from: ᐧ  reason: contains not printable characters */
    public String m6959() {
        if (this.f6280 == null) {
            return null;
        }
        int indexOf = this.f6282.indexOf(63) + 1;
        return this.f6282.substring(indexOf, Util.m7110(this.f6282, indexOf, this.f6282.length(), '#'));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m6960() {
        if (this.f6278.isEmpty()) {
            return "";
        }
        return this.f6282.substring(this.f6282.indexOf(58, this.f6286.length() + 3) + 1, this.f6282.indexOf(64));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m6961() {
        return this.f6286;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m6962() {
        if (this.f6283.isEmpty()) {
            return "";
        }
        int length = this.f6286.length() + 3;
        return this.f6282.substring(length, Util.m7111(this.f6282, length, this.f6282.length(), ":@"));
    }

    @Nullable
    /* renamed from: 麤  reason: contains not printable characters */
    public Builder m6963(String str) {
        Builder builder = new Builder();
        if (builder.m6986(this, str) == Builder.ParseResult.SUCCESS) {
            return builder;
        }
        return null;
    }

    @Nullable
    /* renamed from: 齉  reason: contains not printable characters */
    public HttpUrl m6964(String str) {
        Builder r0 = m6963(str);
        if (r0 != null) {
            return r0.m6984();
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m6965() {
        return this.f6286.equals(TyphoonApp.HTTPS);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public URI m6966() {
        String builder = m6967().m6979().toString();
        try {
            return new URI(builder);
        } catch (URISyntaxException e) {
            try {
                return URI.create(builder.replaceAll("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]", ""));
            } catch (Exception e2) {
                throw new RuntimeException(e);
            }
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Builder m6967() {
        Builder builder = new Builder();
        builder.f6294 = this.f6286;
        builder.f6291 = m6962();
        builder.f6293 = m6960();
        builder.f6292 = this.f6284;
        builder.f6290 = this.f6285 != m6940(this.f6286) ? this.f6285 : -1;
        builder.f6287.clear();
        builder.f6287.addAll(m6957());
        builder.m6978(m6959());
        builder.f6289 = m6954();
        return builder;
    }
}
