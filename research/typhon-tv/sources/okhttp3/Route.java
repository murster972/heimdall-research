package okhttp3;

import java.net.InetSocketAddress;
import java.net.Proxy;
import javax.annotation.Nullable;

public final class Route {

    /* renamed from: 靐  reason: contains not printable characters */
    final Proxy f16084;

    /* renamed from: 齉  reason: contains not printable characters */
    final InetSocketAddress f16085;

    /* renamed from: 龘  reason: contains not printable characters */
    final Address f16086;

    public Route(Address address, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (address == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress == null) {
            throw new NullPointerException("inetSocketAddress == null");
        } else {
            this.f16086 = address;
            this.f16084 = proxy;
            this.f16085 = inetSocketAddress;
        }
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof Route) && ((Route) obj).f16086.equals(this.f16086) && ((Route) obj).f16084.equals(this.f16084) && ((Route) obj).f16085.equals(this.f16085);
    }

    public int hashCode() {
        return ((((this.f16086.hashCode() + 527) * 31) + this.f16084.hashCode()) * 31) + this.f16085.hashCode();
    }

    public String toString() {
        return "Route{" + this.f16085 + "}";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Proxy m19999() {
        return this.f16084;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m20000() {
        return this.f16086.f6186 != null && this.f16084.type() == Proxy.Type.HTTP;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public InetSocketAddress m20001() {
        return this.f16085;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Address m20002() {
        return this.f16086;
    }
}
