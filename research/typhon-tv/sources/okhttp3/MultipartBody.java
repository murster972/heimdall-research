package okhttp3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.Nullable;
import okhttp3.internal.Util;
import okio.Buffer;
import okio.BufferedSink;
import okio.ByteString;
import org.apache.oltu.oauth2.common.OAuth;

public final class MultipartBody extends RequestBody {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final byte[] f16050 = {58, 32};

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final byte[] f16051 = {13, 10};

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final byte[] f16052 = {45, 45};

    /* renamed from: 连任  reason: contains not printable characters */
    public static final MediaType f16053 = MediaType.m6996("multipart/form-data");

    /* renamed from: 靐  reason: contains not printable characters */
    public static final MediaType f16054 = MediaType.m6996("multipart/alternative");

    /* renamed from: 麤  reason: contains not printable characters */
    public static final MediaType f16055 = MediaType.m6996("multipart/parallel");

    /* renamed from: 齉  reason: contains not printable characters */
    public static final MediaType f16056 = MediaType.m6996("multipart/digest");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final MediaType f16057 = MediaType.m6996("multipart/mixed");

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f16058 = -1;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final List<Part> f16059;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final ByteString f16060;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final MediaType f16061;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final MediaType f16062;

    public static final class Part {

        /* renamed from: 靐  reason: contains not printable characters */
        final RequestBody f6301;
        @Nullable

        /* renamed from: 龘  reason: contains not printable characters */
        final Headers f6302;

        private Part(@Nullable Headers headers, RequestBody requestBody) {
            this.f6302 = headers;
            this.f6301 = requestBody;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static Part m7000(@Nullable Headers headers, RequestBody requestBody) {
            if (requestBody == null) {
                throw new NullPointerException("body == null");
            } else if (headers != null && headers.m6936(OAuth.HeaderType.CONTENT_TYPE) != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (headers == null || headers.m6936("Content-Length") == null) {
                return new Part(headers, requestBody);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }
    }

    public static final class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private MediaType f16063;

        /* renamed from: 齉  reason: contains not printable characters */
        private final List<Part> f16064;

        /* renamed from: 龘  reason: contains not printable characters */
        private final ByteString f16065;

        public Builder() {
            this(UUID.randomUUID().toString());
        }

        public Builder(String str) {
            this.f16063 = MultipartBody.f16057;
            this.f16064 = new ArrayList();
            this.f16065 = ByteString.encodeUtf8(str);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19957(@Nullable Headers headers, RequestBody requestBody) {
            return m19959(Part.m7000(headers, requestBody));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19958(MediaType mediaType) {
            if (mediaType == null) {
                throw new NullPointerException("type == null");
            } else if (!mediaType.m6998().equals("multipart")) {
                throw new IllegalArgumentException("multipart != " + mediaType);
            } else {
                this.f16063 = mediaType;
                return this;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19959(Part part) {
            if (part == null) {
                throw new NullPointerException("part == null");
            }
            this.f16064.add(part);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MultipartBody m19960() {
            if (!this.f16064.isEmpty()) {
                return new MultipartBody(this.f16065, this.f16063, this.f16064);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }
    }

    MultipartBody(ByteString byteString, MediaType mediaType, List<Part> list) {
        this.f16060 = byteString;
        this.f16061 = mediaType;
        this.f16062 = MediaType.m6996(mediaType + "; boundary=" + byteString.utf8());
        this.f16059 = Util.m7120(list);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m19956(@Nullable BufferedSink bufferedSink, boolean z) throws IOException {
        long j = 0;
        Buffer buffer = null;
        if (z) {
            buffer = new Buffer();
            bufferedSink = buffer;
        }
        int size = this.f16059.size();
        for (int i = 0; i < size; i++) {
            Part part = this.f16059.get(i);
            Headers headers = part.f6302;
            RequestBody requestBody = part.f6301;
            bufferedSink.m20448(f16052);
            bufferedSink.m20446(this.f16060);
            bufferedSink.m20448(f16051);
            if (headers != null) {
                int r12 = headers.m6934();
                for (int i2 = 0; i2 < r12; i2++) {
                    bufferedSink.m20445(headers.m6935(i2)).m20448(f16050).m20445(headers.m6930(i2)).m20448(f16051);
                }
            }
            MediaType contentType = requestBody.contentType();
            if (contentType != null) {
                bufferedSink.m20445("Content-Type: ").m20445(contentType.toString()).m20448(f16051);
            }
            long contentLength = requestBody.contentLength();
            if (contentLength != -1) {
                bufferedSink.m20445("Content-Length: ").m20439(contentLength).m20448(f16051);
            } else if (z) {
                buffer.m7223();
                long j2 = j;
                return -1;
            }
            bufferedSink.m20448(f16051);
            if (z) {
                j += contentLength;
            } else {
                requestBody.writeTo(bufferedSink);
            }
            bufferedSink.m20448(f16051);
        }
        bufferedSink.m20448(f16052);
        bufferedSink.m20446(this.f16060);
        bufferedSink.m20448(f16052);
        bufferedSink.m20448(f16051);
        if (z) {
            j += buffer.m7242();
            buffer.m7223();
        }
        long j3 = j;
        return j;
    }

    public long contentLength() throws IOException {
        long j = this.f16058;
        if (j != -1) {
            return j;
        }
        long r0 = m19956((BufferedSink) null, true);
        this.f16058 = r0;
        return r0;
    }

    public MediaType contentType() {
        return this.f16062;
    }

    public void writeTo(BufferedSink bufferedSink) throws IOException {
        m19956(bufferedSink, false);
    }
}
