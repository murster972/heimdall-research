package okhttp3;

import java.net.Proxy;
import java.net.ProxySelector;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.Call;
import okhttp3.EventListener;
import okhttp3.Headers;
import okhttp3.Response;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.cache.InternalCache;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.RouteDatabase;
import okhttp3.internal.connection.StreamAllocation;
import okhttp3.internal.platform.Platform;
import okhttp3.internal.tls.CertificateChainCleaner;
import okhttp3.internal.tls.OkHostnameVerifier;

public class OkHttpClient implements Cloneable, Call.Factory {

    /* renamed from: 靐  reason: contains not printable characters */
    static final List<ConnectionSpec> f6303 = Util.m7121((T[]) new ConnectionSpec[]{ConnectionSpec.f6247, ConnectionSpec.f6246});

    /* renamed from: 龘  reason: contains not printable characters */
    static final List<Protocol> f6304 = Util.m7121((T[]) new Protocol[]{Protocol.HTTP_2, Protocol.HTTP_1_1});

    /* renamed from: ʻ  reason: contains not printable characters */
    final List<ConnectionSpec> f6305;

    /* renamed from: ʼ  reason: contains not printable characters */
    final List<Interceptor> f6306;

    /* renamed from: ʽ  reason: contains not printable characters */
    final List<Interceptor> f6307;
    @Nullable

    /* renamed from: ʾ  reason: contains not printable characters */
    final InternalCache f6308;

    /* renamed from: ʿ  reason: contains not printable characters */
    final SocketFactory f6309;

    /* renamed from: ˆ  reason: contains not printable characters */
    final Authenticator f6310;
    @Nullable

    /* renamed from: ˈ  reason: contains not printable characters */
    final Cache f6311;

    /* renamed from: ˉ  reason: contains not printable characters */
    final ConnectionPool f6312;

    /* renamed from: ˊ  reason: contains not printable characters */
    final HostnameVerifier f6313;

    /* renamed from: ˋ  reason: contains not printable characters */
    final CertificatePinner f6314;

    /* renamed from: ˎ  reason: contains not printable characters */
    final Authenticator f6315;

    /* renamed from: ˏ  reason: contains not printable characters */
    final Dns f6316;

    /* renamed from: ˑ  reason: contains not printable characters */
    final EventListener.Factory f6317;

    /* renamed from: י  reason: contains not printable characters */
    final boolean f6318;

    /* renamed from: ـ  reason: contains not printable characters */
    final boolean f6319;

    /* renamed from: ٴ  reason: contains not printable characters */
    final ProxySelector f6320;

    /* renamed from: ᐧ  reason: contains not printable characters */
    final CookieJar f6321;

    /* renamed from: ᴵ  reason: contains not printable characters */
    final boolean f6322;

    /* renamed from: ᵎ  reason: contains not printable characters */
    final int f6323;

    /* renamed from: ᵔ  reason: contains not printable characters */
    final int f6324;

    /* renamed from: ᵢ  reason: contains not printable characters */
    final int f6325;

    /* renamed from: ⁱ  reason: contains not printable characters */
    final int f6326;

    /* renamed from: 连任  reason: contains not printable characters */
    final List<Protocol> f6327;
    @Nullable

    /* renamed from: 麤  reason: contains not printable characters */
    final Proxy f6328;

    /* renamed from: 齉  reason: contains not printable characters */
    final Dispatcher f6329;
    @Nullable

    /* renamed from: ﹶ  reason: contains not printable characters */
    final SSLSocketFactory f6330;
    @Nullable

    /* renamed from: ﾞ  reason: contains not printable characters */
    final CertificateChainCleaner f6331;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        final List<Interceptor> f6332;

        /* renamed from: ʼ  reason: contains not printable characters */
        EventListener.Factory f6333;

        /* renamed from: ʽ  reason: contains not printable characters */
        ProxySelector f6334;
        @Nullable

        /* renamed from: ʾ  reason: contains not printable characters */
        SSLSocketFactory f6335;
        @Nullable

        /* renamed from: ʿ  reason: contains not printable characters */
        CertificateChainCleaner f6336;

        /* renamed from: ˆ  reason: contains not printable characters */
        Dns f6337;

        /* renamed from: ˈ  reason: contains not printable characters */
        SocketFactory f6338;

        /* renamed from: ˉ  reason: contains not printable characters */
        boolean f6339;

        /* renamed from: ˊ  reason: contains not printable characters */
        Authenticator f6340;

        /* renamed from: ˋ  reason: contains not printable characters */
        Authenticator f6341;

        /* renamed from: ˎ  reason: contains not printable characters */
        ConnectionPool f6342;

        /* renamed from: ˏ  reason: contains not printable characters */
        boolean f6343;

        /* renamed from: ˑ  reason: contains not printable characters */
        CookieJar f6344;

        /* renamed from: י  reason: contains not printable characters */
        boolean f6345;

        /* renamed from: ـ  reason: contains not printable characters */
        int f6346;
        @Nullable

        /* renamed from: ٴ  reason: contains not printable characters */
        Cache f6347;
        @Nullable

        /* renamed from: ᐧ  reason: contains not printable characters */
        InternalCache f6348;

        /* renamed from: ᴵ  reason: contains not printable characters */
        int f6349;

        /* renamed from: ᵎ  reason: contains not printable characters */
        int f6350;

        /* renamed from: ᵔ  reason: contains not printable characters */
        int f6351;

        /* renamed from: 连任  reason: contains not printable characters */
        final List<Interceptor> f6352;
        @Nullable

        /* renamed from: 靐  reason: contains not printable characters */
        Proxy f6353;

        /* renamed from: 麤  reason: contains not printable characters */
        List<ConnectionSpec> f6354;

        /* renamed from: 齉  reason: contains not printable characters */
        List<Protocol> f6355;

        /* renamed from: 龘  reason: contains not printable characters */
        Dispatcher f6356;

        /* renamed from: ﹶ  reason: contains not printable characters */
        HostnameVerifier f6357;

        /* renamed from: ﾞ  reason: contains not printable characters */
        CertificatePinner f6358;

        public Builder() {
            this.f6352 = new ArrayList();
            this.f6332 = new ArrayList();
            this.f6356 = new Dispatcher();
            this.f6355 = OkHttpClient.f6304;
            this.f6354 = OkHttpClient.f6303;
            this.f6333 = EventListener.m19915(EventListener.f16031);
            this.f6334 = ProxySelector.getDefault();
            this.f6344 = CookieJar.f16029;
            this.f6338 = SocketFactory.getDefault();
            this.f6357 = OkHostnameVerifier.f16419;
            this.f6358 = CertificatePinner.f6233;
            this.f6340 = Authenticator.f6194;
            this.f6341 = Authenticator.f6194;
            this.f6342 = new ConnectionPool();
            this.f6337 = Dns.f16030;
            this.f6339 = true;
            this.f6343 = true;
            this.f6345 = true;
            this.f6346 = 10000;
            this.f6349 = 10000;
            this.f6350 = 10000;
            this.f6351 = 0;
        }

        Builder(OkHttpClient okHttpClient) {
            this.f6352 = new ArrayList();
            this.f6332 = new ArrayList();
            this.f6356 = okHttpClient.f6329;
            this.f6353 = okHttpClient.f6328;
            this.f6355 = okHttpClient.f6327;
            this.f6354 = okHttpClient.f6305;
            this.f6352.addAll(okHttpClient.f6306);
            this.f6332.addAll(okHttpClient.f6307);
            this.f6333 = okHttpClient.f6317;
            this.f6334 = okHttpClient.f6320;
            this.f6344 = okHttpClient.f6321;
            this.f6348 = okHttpClient.f6308;
            this.f6347 = okHttpClient.f6311;
            this.f6338 = okHttpClient.f6309;
            this.f6335 = okHttpClient.f6330;
            this.f6336 = okHttpClient.f6331;
            this.f6357 = okHttpClient.f6313;
            this.f6358 = okHttpClient.f6314;
            this.f6340 = okHttpClient.f6315;
            this.f6341 = okHttpClient.f6310;
            this.f6342 = okHttpClient.f6312;
            this.f6337 = okHttpClient.f6316;
            this.f6339 = okHttpClient.f6318;
            this.f6343 = okHttpClient.f6319;
            this.f6345 = okHttpClient.f6322;
            this.f6346 = okHttpClient.f6323;
            this.f6349 = okHttpClient.f6324;
            this.f6350 = okHttpClient.f6325;
            this.f6351 = okHttpClient.f6326;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m7030(long j, TimeUnit timeUnit) {
            this.f6349 = Util.m7112("timeout", j, timeUnit);
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m7031(Interceptor interceptor) {
            if (interceptor == null) {
                throw new IllegalArgumentException("interceptor == null");
            }
            this.f6332.add(interceptor);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m7032(long j, TimeUnit timeUnit) {
            this.f6350 = Util.m7112("timeout", j, timeUnit);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7033(long j, TimeUnit timeUnit) {
            this.f6346 = Util.m7112("timeout", j, timeUnit);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7034(List<ConnectionSpec> list) {
            this.f6354 = Util.m7120(list);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7035(HostnameVerifier hostnameVerifier) {
            if (hostnameVerifier == null) {
                throw new NullPointerException("hostnameVerifier == null");
            }
            this.f6357 = hostnameVerifier;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7036(SSLSocketFactory sSLSocketFactory, X509TrustManager x509TrustManager) {
            if (sSLSocketFactory == null) {
                throw new NullPointerException("sslSocketFactory == null");
            } else if (x509TrustManager == null) {
                throw new NullPointerException("trustManager == null");
            } else {
                this.f6335 = sSLSocketFactory;
                this.f6336 = CertificateChainCleaner.m20418(x509TrustManager);
                return this;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7037(Authenticator authenticator) {
            if (authenticator == null) {
                throw new NullPointerException("authenticator == null");
            }
            this.f6341 = authenticator;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7038(@Nullable Cache cache) {
            this.f6347 = cache;
            this.f6348 = null;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7039(ConnectionPool connectionPool) {
            if (connectionPool == null) {
                throw new NullPointerException("connectionPool == null");
            }
            this.f6342 = connectionPool;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7040(CookieJar cookieJar) {
            if (cookieJar == null) {
                throw new NullPointerException("cookieJar == null");
            }
            this.f6344 = cookieJar;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7041(Interceptor interceptor) {
            if (interceptor == null) {
                throw new IllegalArgumentException("interceptor == null");
            }
            this.f6352.add(interceptor);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m7042(boolean z) {
            this.f6345 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OkHttpClient m7043() {
            return new OkHttpClient(this);
        }
    }

    static {
        Internal.f16088 = new Internal() {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m19961(ConnectionPool connectionPool, RealConnection realConnection) {
                connectionPool.m6882(realConnection);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public int m19962(Response.Builder builder) {
                return builder.f6396;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Socket m19963(ConnectionPool connectionPool, Address address, StreamAllocation streamAllocation) {
                return connectionPool.m6880(address, streamAllocation);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public RealConnection m19964(ConnectionPool connectionPool, Address address, StreamAllocation streamAllocation, Route route) {
                return connectionPool.m6881(address, streamAllocation, route);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public RouteDatabase m19965(ConnectionPool connectionPool) {
                return connectionPool.f6243;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m19966(ConnectionSpec connectionSpec, SSLSocket sSLSocket, boolean z) {
                connectionSpec.m6887(sSLSocket, z);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m19967(Headers.Builder builder, String str) {
                builder.m19953(str);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m19968(Headers.Builder builder, String str, String str2) {
                builder.m19950(str, str2);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m19969(Address address, Address address2) {
                return address.m6839(address2);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m19970(ConnectionPool connectionPool, RealConnection realConnection) {
                return connectionPool.m6878(realConnection);
            }
        };
    }

    public OkHttpClient() {
        this(new Builder());
    }

    OkHttpClient(Builder builder) {
        this.f6329 = builder.f6356;
        this.f6328 = builder.f6353;
        this.f6327 = builder.f6355;
        this.f6305 = builder.f6354;
        this.f6306 = Util.m7120(builder.f6352);
        this.f6307 = Util.m7120(builder.f6332);
        this.f6317 = builder.f6333;
        this.f6320 = builder.f6334;
        this.f6321 = builder.f6344;
        this.f6311 = builder.f6347;
        this.f6308 = builder.f6348;
        this.f6309 = builder.f6338;
        boolean z = false;
        for (ConnectionSpec r1 : this.f6305) {
            z = z || r1.m6888();
        }
        if (builder.f6335 != null || !z) {
            this.f6330 = builder.f6335;
            this.f6331 = builder.f6336;
        } else {
            X509TrustManager r2 = m7001();
            this.f6330 = m7002(r2);
            this.f6331 = CertificateChainCleaner.m20418(r2);
        }
        this.f6313 = builder.f6357;
        this.f6314 = builder.f6358.m6875(this.f6331);
        this.f6315 = builder.f6340;
        this.f6310 = builder.f6341;
        this.f6312 = builder.f6342;
        this.f6316 = builder.f6337;
        this.f6318 = builder.f6339;
        this.f6319 = builder.f6343;
        this.f6322 = builder.f6345;
        this.f6323 = builder.f6346;
        this.f6324 = builder.f6349;
        this.f6325 = builder.f6350;
        this.f6326 = builder.f6351;
        if (this.f6306.contains((Object) null)) {
            throw new IllegalStateException("Null interceptor: " + this.f6306);
        } else if (this.f6307.contains((Object) null)) {
            throw new IllegalStateException("Null network interceptor: " + this.f6307);
        }
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    private X509TrustManager m7001() {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance.init((KeyStore) null);
            TrustManager[] trustManagers = instance.getTrustManagers();
            if (trustManagers.length == 1 && (trustManagers[0] instanceof X509TrustManager)) {
                return (X509TrustManager) trustManagers[0];
            }
            throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
        } catch (GeneralSecurityException e) {
            throw Util.m7114("No System TLS", (Exception) e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private SSLSocketFactory m7002(X509TrustManager x509TrustManager) {
        try {
            SSLContext D_ = Platform.m7184().D_();
            D_.init((KeyManager[]) null, new TrustManager[]{x509TrustManager}, (SecureRandom) null);
            return D_.getSocketFactory();
        } catch (GeneralSecurityException e) {
            throw Util.m7114("No System TLS", (Exception) e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public ProxySelector m7003() {
        return this.f6320;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public CookieJar m7004() {
        return this.f6321;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public InternalCache m7005() {
        return this.f6311 != null ? this.f6311.f6201 : this.f6308;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public CertificatePinner m7006() {
        return this.f6314;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Authenticator m7007() {
        return this.f6310;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Dispatcher m7008() {
        return this.f6329;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public HostnameVerifier m7009() {
        return this.f6313;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public List<Protocol> m7010() {
        return this.f6327;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m7011() {
        return this.f6318;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m7012() {
        return this.f6319;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m7013() {
        return this.f6322;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public List<ConnectionSpec> m7014() {
        return this.f6305;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public Dns m7015() {
        return this.f6316;
    }

    /* renamed from: י  reason: contains not printable characters */
    public List<Interceptor> m7016() {
        return this.f6306;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public List<Interceptor> m7017() {
        return this.f6307;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public SocketFactory m7018() {
        return this.f6309;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public SSLSocketFactory m7019() {
        return this.f6330;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public EventListener.Factory m7020() {
        return this.f6317;
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public Builder m7021() {
        return new Builder(this);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Proxy m7022() {
        return this.f6328;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m7023() {
        return this.f6324;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m7024() {
        return this.f6326;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m7025() {
        return this.f6325;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m7026() {
        return this.f6323;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Call m7027(Request request) {
        return RealCall.m19973(this, request, false);
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Authenticator m7028() {
        return this.f6315;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public ConnectionPool m7029() {
        return this.f6312;
    }
}
