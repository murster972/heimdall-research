package okhttp3;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import okhttp3.RealCall;
import okhttp3.internal.Util;

public final class Dispatcher {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Deque<RealCall.AsyncCall> f6269 = new ArrayDeque();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Deque<RealCall> f6270 = new ArrayDeque();

    /* renamed from: 连任  reason: contains not printable characters */
    private final Deque<RealCall.AsyncCall> f6271 = new ArrayDeque();

    /* renamed from: 靐  reason: contains not printable characters */
    private int f6272 = 5;
    @Nullable

    /* renamed from: 麤  reason: contains not printable characters */
    private ExecutorService f6273;
    @Nullable

    /* renamed from: 齉  reason: contains not printable characters */
    private Runnable f6274;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f6275 = 64;

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m6916() {
        if (this.f6269.size() < this.f6275 && !this.f6271.isEmpty()) {
            Iterator<RealCall.AsyncCall> it2 = this.f6271.iterator();
            while (it2.hasNext()) {
                RealCall.AsyncCall next = it2.next();
                if (m6917(next) < this.f6272) {
                    it2.remove();
                    this.f6269.add(next);
                    m6925().execute(next);
                }
                if (this.f6269.size() >= this.f6275) {
                    return;
                }
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private int m6917(RealCall.AsyncCall asyncCall) {
        int i = 0;
        for (RealCall.AsyncCall next : this.f6269) {
            if (!next.m19983().f16070 && next.m19985().equals(asyncCall.m19985())) {
                i++;
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private <T> void m6918(Deque<T> deque, T t, boolean z) {
        int r1;
        Runnable runnable;
        synchronized (this) {
            if (!deque.remove(t)) {
                throw new AssertionError("Call wasn't in-flight!");
            }
            if (z) {
                m6916();
            }
            r1 = m6919();
            runnable = this.f6274;
        }
        if (r1 == 0 && runnable != null) {
            runnable.run();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public synchronized int m6919() {
        return this.f6269.size() + this.f6270.size();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m6920() {
        for (RealCall.AsyncCall r0 : this.f6271) {
            r0.m19983().m19980();
        }
        for (RealCall.AsyncCall r02 : this.f6269) {
            r02.m19983().m19980();
        }
        for (RealCall r03 : this.f6270) {
            r03.m19980();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6921(RealCall.AsyncCall asyncCall) {
        m6918(this.f6269, asyncCall, true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6922(RealCall realCall) {
        m6918(this.f6270, realCall, false);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized List<Call> m6923() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        arrayList.addAll(this.f6270);
        for (RealCall.AsyncCall r0 : this.f6269) {
            arrayList.add(r0.m19983());
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized List<Call> m6924() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (RealCall.AsyncCall r0 : this.f6271) {
            arrayList.add(r0.m19983());
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized ExecutorService m6925() {
        if (this.f6273 == null) {
            this.f6273 = new ThreadPoolExecutor(0, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, 60, TimeUnit.SECONDS, new SynchronousQueue(), Util.m7122("OkHttp Dispatcher", false));
        }
        return this.f6273;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m6926(RealCall.AsyncCall asyncCall) {
        if (this.f6269.size() >= this.f6275 || m6917(asyncCall) >= this.f6272) {
            this.f6271.add(asyncCall);
        } else {
            this.f6269.add(asyncCall);
            m6925().execute(asyncCall);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m6927(RealCall realCall) {
        this.f6270.add(realCall);
    }
}
