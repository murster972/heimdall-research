package okhttp3;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

public interface Dns {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Dns f16030 = new Dns() {
        /* renamed from: 龘  reason: contains not printable characters */
        public List<InetAddress> m19914(String str) throws UnknownHostException {
            if (str == null) {
                throw new UnknownHostException("hostname == null");
            }
            try {
                return Arrays.asList(InetAddress.getAllByName(str));
            } catch (NullPointerException e) {
                UnknownHostException unknownHostException = new UnknownHostException("Broken system behaviour for dns lookup of " + str);
                unknownHostException.initCause(e);
                throw unknownHostException;
            }
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    List<InetAddress> m19913(String str) throws UnknownHostException;
}
