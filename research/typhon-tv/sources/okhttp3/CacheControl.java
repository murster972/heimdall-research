package okhttp3;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import okhttp3.internal.http.HttpHeaders;

public final class CacheControl {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final CacheControl f6218 = new Builder().m19878().m19879(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, TimeUnit.SECONDS).m19880();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final CacheControl f6219 = new Builder().m19881().m19880();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f6220;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f6221;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final boolean f6222;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final boolean f6223;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final boolean f6224;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f6225;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final boolean f6226;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean f6227;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f6228;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f6229;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f6230;
    @Nullable

    /* renamed from: 齉  reason: contains not printable characters */
    String f6231;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final boolean f6232;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f15891;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f15892;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f15893;

        /* renamed from: 连任  reason: contains not printable characters */
        int f15894 = -1;

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f15895;

        /* renamed from: 麤  reason: contains not printable characters */
        int f15896 = -1;

        /* renamed from: 齉  reason: contains not printable characters */
        int f15897 = -1;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f15898;

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19878() {
            this.f15891 = true;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19879(int i, TimeUnit timeUnit) {
            if (i < 0) {
                throw new IllegalArgumentException("maxStale < 0: " + i);
            }
            long seconds = timeUnit.toSeconds((long) i);
            this.f15896 = seconds > 2147483647L ? MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT : (int) seconds;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public CacheControl m19880() {
            return new CacheControl(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19881() {
            this.f15898 = true;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19882(int i, TimeUnit timeUnit) {
            if (i < 0) {
                throw new IllegalArgumentException("maxAge < 0: " + i);
            }
            long seconds = timeUnit.toSeconds((long) i);
            this.f15897 = seconds > 2147483647L ? MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT : (int) seconds;
            return this;
        }
    }

    CacheControl(Builder builder) {
        this.f6230 = builder.f15898;
        this.f6229 = builder.f15895;
        this.f6220 = builder.f15897;
        this.f6221 = -1;
        this.f6222 = false;
        this.f6226 = false;
        this.f6227 = false;
        this.f6228 = builder.f15896;
        this.f6225 = builder.f15894;
        this.f6223 = builder.f15891;
        this.f6224 = builder.f15892;
        this.f6232 = builder.f15893;
    }

    private CacheControl(boolean z, boolean z2, int i, int i2, boolean z3, boolean z4, boolean z5, int i3, int i4, boolean z6, boolean z7, boolean z8, @Nullable String str) {
        this.f6230 = z;
        this.f6229 = z2;
        this.f6220 = i;
        this.f6221 = i2;
        this.f6222 = z3;
        this.f6226 = z4;
        this.f6227 = z5;
        this.f6228 = i3;
        this.f6225 = i4;
        this.f6223 = z6;
        this.f6224 = z7;
        this.f6232 = z8;
        this.f6231 = str;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private String m6859() {
        StringBuilder sb = new StringBuilder();
        if (this.f6230) {
            sb.append("no-cache, ");
        }
        if (this.f6229) {
            sb.append("no-store, ");
        }
        if (this.f6220 != -1) {
            sb.append("max-age=").append(this.f6220).append(", ");
        }
        if (this.f6221 != -1) {
            sb.append("s-maxage=").append(this.f6221).append(", ");
        }
        if (this.f6222) {
            sb.append("private, ");
        }
        if (this.f6226) {
            sb.append("public, ");
        }
        if (this.f6227) {
            sb.append("must-revalidate, ");
        }
        if (this.f6228 != -1) {
            sb.append("max-stale=").append(this.f6228).append(", ");
        }
        if (this.f6225 != -1) {
            sb.append("min-fresh=").append(this.f6225).append(", ");
        }
        if (this.f6223) {
            sb.append("only-if-cached, ");
        }
        if (this.f6224) {
            sb.append("no-transform, ");
        }
        if (this.f6232) {
            sb.append("immutable, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static CacheControl m6860(Headers headers) {
        String str;
        boolean z = false;
        boolean z2 = false;
        int i = -1;
        int i2 = -1;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        int i3 = -1;
        int i4 = -1;
        boolean z6 = false;
        boolean z7 = false;
        boolean z8 = false;
        boolean z9 = true;
        String str2 = null;
        int r24 = headers.m6934();
        for (int i5 = 0; i5 < r24; i5++) {
            String r20 = headers.m6935(i5);
            String r26 = headers.m6930(i5);
            if (r20.equalsIgnoreCase("Cache-Control")) {
                if (str2 != null) {
                    z9 = false;
                } else {
                    str2 = r26;
                }
            } else if (r20.equalsIgnoreCase("Pragma")) {
                z9 = false;
            }
            int i6 = 0;
            while (i6 < r26.length()) {
                int i7 = i6;
                int r23 = HttpHeaders.m20124(r26, i6, "=,;");
                String trim = r26.substring(i7, r23).trim();
                if (r23 == r26.length() || r26.charAt(r23) == ',' || r26.charAt(r23) == ';') {
                    i6 = r23 + 1;
                    str = null;
                } else {
                    int r232 = HttpHeaders.m20123(r26, r23 + 1);
                    if (r232 >= r26.length() || r26.charAt(r232) != '\"') {
                        int i8 = r232;
                        i6 = HttpHeaders.m20124(r26, r232, ",;");
                        str = r26.substring(i8, i6).trim();
                    } else {
                        int i9 = r232 + 1;
                        int i10 = i9;
                        int r233 = HttpHeaders.m20124(r26, i9, "\"");
                        str = r26.substring(i10, r233);
                        i6 = r233 + 1;
                    }
                }
                if ("no-cache".equalsIgnoreCase(trim)) {
                    z = true;
                } else if ("no-store".equalsIgnoreCase(trim)) {
                    z2 = true;
                } else if ("max-age".equalsIgnoreCase(trim)) {
                    i = HttpHeaders.m20117(str, -1);
                } else if ("s-maxage".equalsIgnoreCase(trim)) {
                    i2 = HttpHeaders.m20117(str, -1);
                } else if ("private".equalsIgnoreCase(trim)) {
                    z3 = true;
                } else if ("public".equalsIgnoreCase(trim)) {
                    z4 = true;
                } else if ("must-revalidate".equalsIgnoreCase(trim)) {
                    z5 = true;
                } else if ("max-stale".equalsIgnoreCase(trim)) {
                    i3 = HttpHeaders.m20117(str, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                } else if ("min-fresh".equalsIgnoreCase(trim)) {
                    i4 = HttpHeaders.m20117(str, -1);
                } else if ("only-if-cached".equalsIgnoreCase(trim)) {
                    z6 = true;
                } else if ("no-transform".equalsIgnoreCase(trim)) {
                    z7 = true;
                } else if ("immutable".equalsIgnoreCase(trim)) {
                    z8 = true;
                }
            }
        }
        if (!z9) {
            str2 = null;
        }
        return new CacheControl(z, z2, i, i2, z3, z4, z5, i3, i4, z6, z7, z8, str2);
    }

    public String toString() {
        String str = this.f6231;
        if (str != null) {
            return str;
        }
        String r0 = m6859();
        this.f6231 = r0;
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m6861() {
        return this.f6227;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6862() {
        return this.f6228;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6863() {
        return this.f6225;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m6864() {
        return this.f6223;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m6865() {
        return this.f6232;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m6866() {
        return this.f6226;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m6867() {
        return this.f6229;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m6868() {
        return this.f6222;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m6869() {
        return this.f6220;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6870() {
        return this.f6230;
    }
}
