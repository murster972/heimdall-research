package okhttp3;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;
import okhttp3.internal.Util;
import okio.Buffer;
import okio.BufferedSink;
import org.apache.oltu.oauth2.common.OAuth;

public final class FormBody extends RequestBody {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final MediaType f16033 = MediaType.m6996(OAuth.ContentType.URL_ENCODED);

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<String> f16034;

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<String> f16035;

    public static final class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private final List<String> f16036;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Charset f16037;

        /* renamed from: 龘  reason: contains not printable characters */
        private final List<String> f16038;

        public Builder() {
            this((Charset) null);
        }

        public Builder(Charset charset) {
            this.f16038 = new ArrayList();
            this.f16036 = new ArrayList();
            this.f16037 = charset;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m19939(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 == null) {
                throw new NullPointerException("value == null");
            } else {
                this.f16038.add(HttpUrl.m6944(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.f16037));
                this.f16036.add(HttpUrl.m6944(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.f16037));
                return this;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19940(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 == null) {
                throw new NullPointerException("value == null");
            } else {
                this.f16038.add(HttpUrl.m6944(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.f16037));
                this.f16036.add(HttpUrl.m6944(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.f16037));
                return this;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public FormBody m19941() {
            return new FormBody(this.f16038, this.f16036);
        }
    }

    FormBody(List<String> list, List<String> list2) {
        this.f16034 = Util.m7120(list);
        this.f16035 = Util.m7120(list2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m19938(@Nullable BufferedSink bufferedSink, boolean z) {
        Buffer buffer = z ? new Buffer() : bufferedSink.m20447();
        int size = this.f16034.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                buffer.m7238(38);
            }
            buffer.m7246(this.f16034.get(i));
            buffer.m7238(61);
            buffer.m7246(this.f16035.get(i));
        }
        if (!z) {
            return 0;
        }
        long r2 = buffer.m7242();
        buffer.m7223();
        return r2;
    }

    public long contentLength() {
        return m19938((BufferedSink) null, true);
    }

    public MediaType contentType() {
        return f16033;
    }

    public void writeTo(BufferedSink bufferedSink) throws IOException {
        m19938(bufferedSink, false);
    }
}
