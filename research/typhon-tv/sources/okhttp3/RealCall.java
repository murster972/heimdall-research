package okhttp3;

import android.support.v4.app.NotificationCompat;
import java.io.IOException;
import java.util.ArrayList;
import okhttp3.internal.NamedRunnable;
import okhttp3.internal.cache.CacheInterceptor;
import okhttp3.internal.connection.ConnectInterceptor;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.StreamAllocation;
import okhttp3.internal.http.BridgeInterceptor;
import okhttp3.internal.http.CallServerInterceptor;
import okhttp3.internal.http.HttpCodec;
import okhttp3.internal.http.RealInterceptorChain;
import okhttp3.internal.http.RetryAndFollowUpInterceptor;
import okhttp3.internal.platform.Platform;

final class RealCall implements Call {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f16067;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public EventListener f16068;

    /* renamed from: 靐  reason: contains not printable characters */
    final RetryAndFollowUpInterceptor f16069;

    /* renamed from: 麤  reason: contains not printable characters */
    final boolean f16070;

    /* renamed from: 齉  reason: contains not printable characters */
    final Request f16071;

    /* renamed from: 龘  reason: contains not printable characters */
    final OkHttpClient f16072;

    final class AsyncCall extends NamedRunnable {

        /* renamed from: 齉  reason: contains not printable characters */
        private final Callback f16073;

        AsyncCall(Callback callback) {
            super("OkHttp %s", RealCall.this.m19975());
            this.f16073 = callback;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public RealCall m19983() {
            return RealCall.this;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m19984() {
            boolean z = false;
            try {
                Response r1 = RealCall.this.m19976();
                if (RealCall.this.f16069.m20156()) {
                    this.f16073.m19889((Call) RealCall.this, new IOException("Canceled"));
                } else {
                    z = true;
                    this.f16073.m19890((Call) RealCall.this, r1);
                }
            } catch (IOException e) {
                if (z) {
                    Platform.m7184().m7193(4, "Callback failure for " + RealCall.this.m19974(), (Throwable) e);
                } else {
                    RealCall.this.f16068.m19926((Call) RealCall.this, e);
                    this.f16073.m19889((Call) RealCall.this, e);
                }
            } finally {
                RealCall.this.f16072.m7008().m6921(this);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m19985() {
            return RealCall.this.f16071.m7053().m6951();
        }
    }

    private RealCall(OkHttpClient okHttpClient, Request request, boolean z) {
        this.f16072 = okHttpClient;
        this.f16071 = request;
        this.f16070 = z;
        this.f16069 = new RetryAndFollowUpInterceptor(okHttpClient, z);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m19971() {
        this.f16069.m20158(Platform.m7184().m7190("response.body().close()"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static RealCall m19973(OkHttpClient okHttpClient, Request request, boolean z) {
        RealCall realCall = new RealCall(okHttpClient, request, z);
        realCall.f16068 = okHttpClient.m7020().m19937(realCall);
        return realCall;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public String m19974() {
        return (m19979() ? "canceled " : "") + (this.f16070 ? "web socket" : NotificationCompat.CATEGORY_CALL) + " to " + m19975();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public String m19975() {
        return this.f16071.m7053().m6955();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public Response m19976() throws IOException {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.f16072.m7016());
        arrayList.add(this.f16069);
        arrayList.add(new BridgeInterceptor(this.f16072.m7004()));
        arrayList.add(new CacheInterceptor(this.f16072.m7005()));
        arrayList.add(new ConnectInterceptor(this.f16072));
        if (!this.f16070) {
            arrayList.addAll(this.f16072.m7017());
        }
        arrayList.add(new CallServerInterceptor(this.f16070));
        return new RealInterceptorChain(arrayList, (StreamAllocation) null, (HttpCodec) null, (RealConnection) null, 0, this.f16071, this, this.f16068, this.f16072.m7026(), this.f16072.m7023(), this.f16072.m7025()).m6995(this.f16071);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public RealCall clone() {
        return m19973(this.f16072, this.f16071, this.f16070);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Response m19978() throws IOException {
        synchronized (this) {
            if (this.f16067) {
                throw new IllegalStateException("Already Executed");
            }
            this.f16067 = true;
        }
        m19971();
        this.f16068.m19924((Call) this);
        try {
            this.f16072.m7008().m6927(this);
            Response r1 = m19976();
            if (r1 == null) {
                throw new IOException("Canceled");
            }
            this.f16072.m7008().m6922(this);
            return r1;
        } catch (IOException e) {
            this.f16068.m19926((Call) this, e);
            throw e;
        } catch (Throwable th) {
            this.f16072.m7008().m6922(this);
            throw th;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m19979() {
        return this.f16069.m20156();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19980() {
        this.f16069.m20157();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Request m19981() {
        return this.f16071;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19982(Callback callback) {
        synchronized (this) {
            if (this.f16067) {
                throw new IllegalStateException("Already Executed");
            }
            this.f16067 = true;
        }
        m19971();
        this.f16068.m19924((Call) this);
        this.f16072.m7008().m6926(new AsyncCall(callback));
    }
}
