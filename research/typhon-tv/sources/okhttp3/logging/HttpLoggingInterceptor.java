package okhttp3.logging;

import java.io.EOFException;
import java.nio.charset.Charset;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.internal.platform.Platform;
import okio.Buffer;

public final class HttpLoggingInterceptor implements Interceptor {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Charset f16420 = Charset.forName("UTF-8");

    /* renamed from: 靐  reason: contains not printable characters */
    private final Logger f16421;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile Level f16422;

    public enum Level {
        NONE,
        BASIC,
        HEADERS,
        BODY
    }

    public interface Logger {

        /* renamed from: 龘  reason: contains not printable characters */
        public static final Logger f16424 = new Logger() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m20430(String str) {
                Platform.m7184().m7193(4, str, (Throwable) null);
            }
        };

        /* renamed from: 龘  reason: contains not printable characters */
        void m20429(String str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m20427(Headers headers) {
        String r0 = headers.m6936("Content-Encoding");
        return r0 != null && !r0.equalsIgnoreCase("identity") && !r0.equalsIgnoreCase("gzip");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m20428(Buffer buffer) {
        long j = 64;
        try {
            Buffer buffer2 = new Buffer();
            if (buffer.m7242() < 64) {
                j = buffer.m7242();
            }
            buffer.m7269(buffer2, 0, j);
            for (int i = 0; i < 16 && !buffer2.m7210(); i++) {
                int r6 = buffer2.m7226();
                if (Character.isISOControl(r6) && !Character.isWhitespace(r6)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException e) {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x04db  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okhttp3.Response intercept(okhttp3.Interceptor.Chain r39) throws java.io.IOException {
        /*
            r38 = this;
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Level r0 = r0.f16422
            r20 = r0
            okhttp3.Request r24 = r39.m6994()
            okhttp3.logging.HttpLoggingInterceptor$Level r34 = okhttp3.logging.HttpLoggingInterceptor.Level.NONE
            r0 = r20
            r1 = r34
            if (r0 != r1) goto L_0x001b
            r0 = r39
            r1 = r24
            okhttp3.Response r27 = r0.m6995(r1)
        L_0x001a:
            return r27
        L_0x001b:
            okhttp3.logging.HttpLoggingInterceptor$Level r34 = okhttp3.logging.HttpLoggingInterceptor.Level.BODY
            r0 = r20
            r1 = r34
            if (r0 != r1) goto L_0x0177
            r21 = 1
        L_0x0025:
            if (r21 != 0) goto L_0x002f
            okhttp3.logging.HttpLoggingInterceptor$Level r34 = okhttp3.logging.HttpLoggingInterceptor.Level.HEADERS
            r0 = r20
            r1 = r34
            if (r0 != r1) goto L_0x017b
        L_0x002f:
            r22 = 1
        L_0x0031:
            okhttp3.RequestBody r25 = r24.m7050()
            if (r25 == 0) goto L_0x017f
            r17 = 1
        L_0x0039:
            okhttp3.Connection r8 = r39.m6991()
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            java.lang.String r35 = "--> "
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r35 = r24.m7048()
            java.lang.StringBuilder r34 = r34.append(r35)
            r35 = 32
            java.lang.StringBuilder r34 = r34.append(r35)
            okhttp3.HttpUrl r35 = r24.m7053()
            java.lang.StringBuilder r35 = r34.append(r35)
            if (r8 == 0) goto L_0x0183
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            java.lang.String r36 = " "
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r34 = r0.append(r1)
            okhttp3.Protocol r36 = r8.m19898()
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r34 = r34.toString()
        L_0x0081:
            r0 = r35
            r1 = r34
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r26 = r34.toString()
            if (r22 != 0) goto L_0x00bc
            if (r17 == 0) goto L_0x00bc
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            r0 = r34
            r1 = r26
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r35 = " ("
            java.lang.StringBuilder r34 = r34.append(r35)
            long r36 = r25.contentLength()
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r35 = "-byte body)"
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r26 = r34.toString()
        L_0x00bc:
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            r0 = r34
            r1 = r26
            r0.m20429(r1)
            if (r22 == 0) goto L_0x01ad
            if (r17 == 0) goto L_0x011f
            okhttp3.MediaType r34 = r25.contentType()
            if (r34 == 0) goto L_0x00f4
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "Content-Type: "
            java.lang.StringBuilder r35 = r35.append(r36)
            okhttp3.MediaType r36 = r25.contentType()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
        L_0x00f4:
            long r34 = r25.contentLength()
            r36 = -1
            int r34 = (r34 > r36 ? 1 : (r34 == r36 ? 0 : -1))
            if (r34 == 0) goto L_0x011f
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "Content-Length: "
            java.lang.StringBuilder r35 = r35.append(r36)
            long r36 = r25.contentLength()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
        L_0x011f:
            okhttp3.Headers r18 = r24.m7051()
            r19 = 0
            int r12 = r18.m6934()
        L_0x0129:
            r0 = r19
            if (r0 >= r12) goto L_0x0188
            java.lang.String r23 = r18.m6935((int) r19)
            java.lang.String r34 = "Content-Type"
            r0 = r34
            r1 = r23
            boolean r34 = r0.equalsIgnoreCase(r1)
            if (r34 != 0) goto L_0x0174
            java.lang.String r34 = "Content-Length"
            r0 = r34
            r1 = r23
            boolean r34 = r0.equalsIgnoreCase(r1)
            if (r34 != 0) goto L_0x0174
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            r0 = r35
            r1 = r23
            java.lang.StringBuilder r35 = r0.append(r1)
            java.lang.String r36 = ": "
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = r18.m6930((int) r19)
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
        L_0x0174:
            int r19 = r19 + 1
            goto L_0x0129
        L_0x0177:
            r21 = 0
            goto L_0x0025
        L_0x017b:
            r22 = 0
            goto L_0x0031
        L_0x017f:
            r17 = 0
            goto L_0x0039
        L_0x0183:
            java.lang.String r34 = ""
            goto L_0x0081
        L_0x0188:
            if (r21 == 0) goto L_0x018c
            if (r17 != 0) goto L_0x02dc
        L_0x018c:
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "--> END "
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = r24.m7048()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
        L_0x01ad:
            long r30 = java.lang.System.nanoTime()
            r0 = r39
            r1 = r24
            okhttp3.Response r27 = r0.m6995(r1)     // Catch:{ Exception -> 0x03bf }
            java.util.concurrent.TimeUnit r34 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r36 = java.lang.System.nanoTime()
            long r36 = r36 - r30
            r0 = r34
            r1 = r36
            long r32 = r0.toMillis(r1)
            okhttp3.ResponseBody r28 = r27.m7056()
            long r10 = r28.m7093()
            r34 = -1
            int r34 = (r10 > r34 ? 1 : (r10 == r34 ? 0 : -1))
            if (r34 == 0) goto L_0x03e0
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            r0 = r34
            java.lang.StringBuilder r34 = r0.append(r10)
            java.lang.String r35 = "-byte"
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r4 = r34.toString()
        L_0x01ed:
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r35 = r0
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            java.lang.String r36 = "<-- "
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r34 = r0.append(r1)
            int r36 = r27.m7066()
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r34 = r27.m7063()
            boolean r34 = r34.isEmpty()
            if (r34 == 0) goto L_0x03e5
            java.lang.String r34 = ""
        L_0x021c:
            r0 = r36
            r1 = r34
            java.lang.StringBuilder r34 = r0.append(r1)
            r36 = 32
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r34 = r0.append(r1)
            okhttp3.Request r36 = r27.m7069()
            okhttp3.HttpUrl r36 = r36.m7053()
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r36 = " ("
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r34 = r0.append(r1)
            r0 = r34
            r1 = r32
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r36 = "ms"
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r36 = r0.append(r1)
            if (r22 != 0) goto L_0x0406
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            java.lang.String r37 = ", "
            r0 = r34
            r1 = r37
            java.lang.StringBuilder r34 = r0.append(r1)
            r0 = r34
            java.lang.StringBuilder r34 = r0.append(r4)
            java.lang.String r37 = " body"
            r0 = r34
            r1 = r37
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r34 = r34.toString()
        L_0x0283:
            r0 = r36
            r1 = r34
            java.lang.StringBuilder r34 = r0.append(r1)
            r36 = 41
            r0 = r34
            r1 = r36
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r34 = r34.toString()
            r0 = r35
            r1 = r34
            r0.m20429(r1)
            if (r22 == 0) goto L_0x001a
            okhttp3.Headers r18 = r27.m7055()
            r19 = 0
            int r12 = r18.m6934()
        L_0x02ac:
            r0 = r19
            if (r0 >= r12) goto L_0x040b
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = r18.m6935((int) r19)
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = ": "
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = r18.m6930((int) r19)
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
            int r19 = r19 + 1
            goto L_0x02ac
        L_0x02dc:
            okhttp3.Headers r34 = r24.m7051()
            r0 = r38
            r1 = r34
            boolean r34 = r0.m20427((okhttp3.Headers) r1)
            if (r34 == 0) goto L_0x0314
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "--> END "
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = r24.m7048()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = " (encoded body omitted)"
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
            goto L_0x01ad
        L_0x0314:
            okio.Buffer r5 = new okio.Buffer
            r5.<init>()
            r0 = r25
            r0.writeTo(r5)
            java.nio.charset.Charset r7 = f16420
            okhttp3.MediaType r9 = r25.contentType()
            if (r9 == 0) goto L_0x032e
            java.nio.charset.Charset r34 = f16420
            r0 = r34
            java.nio.charset.Charset r7 = r9.m6999((java.nio.charset.Charset) r0)
        L_0x032e:
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.String r35 = ""
            r34.m20429(r35)
            boolean r34 = m20428((okio.Buffer) r5)
            if (r34 == 0) goto L_0x0386
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.String r35 = r5.m7264((java.nio.charset.Charset) r7)
            r34.m20429(r35)
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "--> END "
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = r24.m7048()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = " ("
            java.lang.StringBuilder r35 = r35.append(r36)
            long r36 = r25.contentLength()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = "-byte body)"
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
            goto L_0x01ad
        L_0x0386:
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "--> END "
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = r24.m7048()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = " (binary "
            java.lang.StringBuilder r35 = r35.append(r36)
            long r36 = r25.contentLength()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = "-byte body omitted)"
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
            goto L_0x01ad
        L_0x03bf:
            r13 = move-exception
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "<-- HTTP FAILED: "
            java.lang.StringBuilder r35 = r35.append(r36)
            r0 = r35
            java.lang.StringBuilder r35 = r0.append(r13)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
            throw r13
        L_0x03e0:
            java.lang.String r4 = "unknown-length"
            goto L_0x01ed
        L_0x03e5:
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            r37 = 32
            r0 = r34
            r1 = r37
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r37 = r27.m7063()
            r0 = r34
            r1 = r37
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r34 = r34.toString()
            goto L_0x021c
        L_0x0406:
            java.lang.String r34 = ""
            goto L_0x0283
        L_0x040b:
            if (r21 == 0) goto L_0x0413
            boolean r34 = okhttp3.internal.http.HttpHeaders.m20120(r27)
            if (r34 != 0) goto L_0x0421
        L_0x0413:
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.String r35 = "<-- END HTTP"
            r34.m20429(r35)
            goto L_0x001a
        L_0x0421:
            okhttp3.Headers r34 = r27.m7055()
            r0 = r38
            r1 = r34
            boolean r34 = r0.m20427((okhttp3.Headers) r1)
            if (r34 == 0) goto L_0x043d
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.String r35 = "<-- END HTTP (encoded body omitted)"
            r34.m20429(r35)
            goto L_0x001a
        L_0x043d:
            okio.BufferedSource r29 = r28.m7095()
            r34 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r0 = r29
            r1 = r34
            r0.m20464(r1)
            okio.Buffer r5 = r29.m20466()
            r14 = 0
            java.lang.String r34 = "gzip"
            java.lang.String r35 = "Content-Encoding"
            r0 = r18
            r1 = r35
            java.lang.String r35 = r0.m6936((java.lang.String) r1)
            boolean r34 = r34.equalsIgnoreCase(r35)
            if (r34 == 0) goto L_0x048c
            long r34 = r5.m7242()
            java.lang.Long r14 = java.lang.Long.valueOf(r34)
            r15 = 0
            okio.GzipSource r16 = new okio.GzipSource     // Catch:{ all -> 0x04d8 }
            okio.Buffer r34 = r5.clone()     // Catch:{ all -> 0x04d8 }
            r0 = r16
            r1 = r34
            r0.<init>(r1)     // Catch:{ all -> 0x04d8 }
            okio.Buffer r6 = new okio.Buffer     // Catch:{ all -> 0x0567 }
            r6.<init>()     // Catch:{ all -> 0x0567 }
            r0 = r16
            r6.m7262((okio.Source) r0)     // Catch:{ all -> 0x056c }
            if (r16 == 0) goto L_0x0572
            r16.close()
            r5 = r6
        L_0x048c:
            java.nio.charset.Charset r7 = f16420
            okhttp3.MediaType r9 = r28.m7096()
            if (r9 == 0) goto L_0x049c
            java.nio.charset.Charset r34 = f16420
            r0 = r34
            java.nio.charset.Charset r7 = r9.m6999((java.nio.charset.Charset) r0)
        L_0x049c:
            boolean r34 = m20428((okio.Buffer) r5)
            if (r34 != 0) goto L_0x04df
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.String r35 = ""
            r34.m20429(r35)
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "<-- END HTTP (binary "
            java.lang.StringBuilder r35 = r35.append(r36)
            long r36 = r5.m7242()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = "-byte body omitted)"
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
            goto L_0x001a
        L_0x04d8:
            r34 = move-exception
        L_0x04d9:
            if (r15 == 0) goto L_0x04de
            r15.close()
        L_0x04de:
            throw r34
        L_0x04df:
            r34 = 0
            int r34 = (r10 > r34 ? 1 : (r10 == r34 ? 0 : -1))
            if (r34 == 0) goto L_0x0504
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.String r35 = ""
            r34.m20429(r35)
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            okio.Buffer r35 = r5.clone()
            r0 = r35
            java.lang.String r35 = r0.m7264((java.nio.charset.Charset) r7)
            r34.m20429(r35)
        L_0x0504:
            if (r14 == 0) goto L_0x053d
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "<-- END HTTP ("
            java.lang.StringBuilder r35 = r35.append(r36)
            long r36 = r5.m7242()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = "-byte, "
            java.lang.StringBuilder r35 = r35.append(r36)
            r0 = r35
            java.lang.StringBuilder r35 = r0.append(r14)
            java.lang.String r36 = "-gzipped-byte body)"
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
            goto L_0x001a
        L_0x053d:
            r0 = r38
            okhttp3.logging.HttpLoggingInterceptor$Logger r0 = r0.f16421
            r34 = r0
            java.lang.StringBuilder r35 = new java.lang.StringBuilder
            r35.<init>()
            java.lang.String r36 = "<-- END HTTP ("
            java.lang.StringBuilder r35 = r35.append(r36)
            long r36 = r5.m7242()
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r36 = "-byte body)"
            java.lang.StringBuilder r35 = r35.append(r36)
            java.lang.String r35 = r35.toString()
            r34.m20429(r35)
            goto L_0x001a
        L_0x0567:
            r34 = move-exception
            r15 = r16
            goto L_0x04d9
        L_0x056c:
            r34 = move-exception
            r15 = r16
            r5 = r6
            goto L_0x04d9
        L_0x0572:
            r5 = r6
            goto L_0x048c
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.logging.HttpLoggingInterceptor.intercept(okhttp3.Interceptor$Chain):okhttp3.Response");
    }
}
