package retrofit2;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import retrofit2.CallAdapter;

final class DefaultCallAdapterFactory extends CallAdapter.Factory {

    /* renamed from: 龘  reason: contains not printable characters */
    static final CallAdapter.Factory f18586 = new DefaultCallAdapterFactory();

    DefaultCallAdapterFactory() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CallAdapter<?, ?> m24305(Type type, Annotation[] annotationArr, Retrofit retrofit) {
        if (m24298(type) != Call.class) {
            return null;
        }
        final Type r0 = Utils.m24428(type);
        return new CallAdapter<Object, Call<?>>() {
            /* renamed from: 靐  reason: contains not printable characters */
            public Call<Object> m24307(Call<Object> call) {
                return call;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Type m24308() {
                return r0;
            }
        };
    }
}
