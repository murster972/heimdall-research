package retrofit2.adapter.rxjava;

import java.lang.reflect.Type;
import retrofit2.Call;
import retrofit2.CallAdapter;
import rx.Observable;
import rx.Scheduler;

final class RxJavaCallAdapter<R> implements CallAdapter<R, Object> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean f18730;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f18731;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f18732;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Scheduler f18733;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f18734;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f18735;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Type f18736;

    private static final class CompletableHelper {
        /* renamed from: 龘  reason: contains not printable characters */
        static Object m24460(Observable<?> observable) {
            return observable.m7377();
        }
    }

    RxJavaCallAdapter(Type type, Scheduler scheduler, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.f18736 = type;
        this.f18733 = scheduler;
        this.f18735 = z;
        this.f18734 = z2;
        this.f18732 = z3;
        this.f18730 = z4;
        this.f18731 = z5;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m24458(Call<R> call) {
        Observable.OnSubscribe callEnqueueOnSubscribe = this.f18735 ? new CallEnqueueOnSubscribe(call) : new CallExecuteOnSubscribe(call);
        Observable r2 = Observable.m7359(this.f18734 ? new ResultOnSubscribe(callEnqueueOnSubscribe) : this.f18732 ? new BodyOnSubscribe(callEnqueueOnSubscribe) : callEnqueueOnSubscribe);
        if (this.f18733 != null) {
            r2 = r2.m7382(this.f18733);
        }
        return this.f18730 ? r2.m7414() : this.f18731 ? CompletableHelper.m24460(r2) : r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Type m24459() {
        return this.f18736;
    }
}
