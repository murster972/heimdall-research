package retrofit2.adapter.rxjava;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Scheduler;
import rx.Single;

public final class RxJavaCallAdapterFactory extends CallAdapter.Factory {

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f18737;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Scheduler f18738;

    private RxJavaCallAdapterFactory(Scheduler scheduler, boolean z) {
        this.f18738 = scheduler;
        this.f18737 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static RxJavaCallAdapterFactory m24461() {
        return new RxJavaCallAdapterFactory((Scheduler) null, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CallAdapter<?, ?> m24462(Type type, Annotation[] annotationArr, Retrofit retrofit) {
        Type type2;
        Class<?> r12 = m24298(type);
        boolean z = r12 == Single.class;
        boolean equals = "rx.Completable".equals(r12.getCanonicalName());
        if (r12 != Observable.class && !z && !equals) {
            return null;
        }
        if (equals) {
            return new RxJavaCallAdapter(Void.class, this.f18738, this.f18737, false, true, false, true);
        }
        boolean z2 = false;
        boolean z3 = false;
        if (!(type instanceof ParameterizedType)) {
            String str = z ? "Single" : "Observable";
            throw new IllegalStateException(str + " return type must be parameterized as " + str + "<Foo> or " + str + "<? extends Foo>");
        }
        Type r10 = m24299(0, (ParameterizedType) type);
        Class<?> r11 = m24298(r10);
        if (r11 == Response.class) {
            if (!(r10 instanceof ParameterizedType)) {
                throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
            }
            type2 = m24299(0, (ParameterizedType) r10);
        } else if (r11 != Result.class) {
            type2 = r10;
            z3 = true;
        } else if (!(r10 instanceof ParameterizedType)) {
            throw new IllegalStateException("Result must be parameterized as Result<Foo> or Result<? extends Foo>");
        } else {
            type2 = m24299(0, (ParameterizedType) r10);
            z2 = true;
        }
        return new RxJavaCallAdapter(type2, this.f18738, this.f18737, z2, z3, z, false);
    }
}
