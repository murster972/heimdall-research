package retrofit2.adapter.rxjava;

import java.util.concurrent.atomic.AtomicInteger;
import retrofit2.Call;
import retrofit2.Response;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.CompositeException;
import rx.exceptions.Exceptions;
import rx.plugins.RxJavaPlugins;

final class CallArbiter<T> extends AtomicInteger implements Producer, Subscription {
    private final Call<T> call;
    private volatile Response<T> response;
    private final Subscriber<? super Response<T>> subscriber;

    CallArbiter(Call<T> call2, Subscriber<? super Response<T>> subscriber2) {
        super(0);
        this.call = call2;
        this.subscriber = subscriber2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m24449(Response<T> response2) {
        try {
            if (!isUnsubscribed()) {
                this.subscriber.onNext(response2);
            }
            try {
                this.subscriber.onCompleted();
            } catch (Throwable th) {
                Exceptions.m24529(th);
                RxJavaPlugins.m7437().m7440().m7429(th);
            }
        } catch (Throwable th2) {
            Exceptions.m24529(th2);
            RxJavaPlugins.m7437().m7440().m7429((Throwable) new CompositeException(th, th2));
        }
    }

    public boolean isUnsubscribed() {
        return this.call.m24293();
    }

    public void request(long j) {
        if (j != 0) {
            while (true) {
                int i = get();
                switch (i) {
                    case 0:
                        if (!compareAndSet(0, 1)) {
                            break;
                        } else {
                            return;
                        }
                    case 1:
                    case 3:
                        return;
                    case 2:
                        if (!compareAndSet(2, 3)) {
                            break;
                        } else {
                            m24449(this.response);
                            return;
                        }
                    default:
                        throw new IllegalStateException("Unknown state: " + i);
                }
            }
        }
    }

    public void unsubscribe() {
        this.call.m24291();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24450(Throwable th) {
        set(3);
        if (!isUnsubscribed()) {
            try {
                this.subscriber.onError(th);
            } catch (Throwable th2) {
                Exceptions.m24529(th2);
                RxJavaPlugins.m7437().m7440().m7429((Throwable) new CompositeException(th, th2));
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24451(Response<T> response2) {
        while (true) {
            int i = get();
            switch (i) {
                case 0:
                    this.response = response2;
                    if (!compareAndSet(0, 2)) {
                        break;
                    } else {
                        return;
                    }
                case 1:
                    if (!compareAndSet(1, 3)) {
                        break;
                    } else {
                        m24449(response2);
                        return;
                    }
                case 2:
                case 3:
                    throw new AssertionError();
                default:
                    throw new IllegalStateException("Unknown state: " + i);
            }
        }
    }
}
