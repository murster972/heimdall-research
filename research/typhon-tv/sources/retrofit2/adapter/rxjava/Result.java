package retrofit2.adapter.rxjava;

import retrofit2.Response;

public final class Result<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Throwable f18726;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Response<T> f18727;

    private Result(Response<T> response, Throwable th) {
        this.f18727 = response;
        this.f18726 = th;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Result<T> m24454(Throwable th) {
        if (th != null) {
            return new Result<>((Response) null, th);
        }
        throw new NullPointerException("error == null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Result<T> m24455(Response<T> response) {
        if (response != null) {
            return new Result<>(response, (Throwable) null);
        }
        throw new NullPointerException("response == null");
    }

    public String toString() {
        return this.f18726 != null ? "Result{isError=true, error=\"" + this.f18726 + "\"}" : "Result{isError=false, response=" + this.f18727 + '}';
    }
}
