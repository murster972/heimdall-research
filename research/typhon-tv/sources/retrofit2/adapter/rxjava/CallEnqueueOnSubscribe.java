package retrofit2.adapter.rxjava;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;

final class CallEnqueueOnSubscribe<T> implements Observable.OnSubscribe<Response<T>> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Call<T> f18722;

    CallEnqueueOnSubscribe(Call<T> call) {
        this.f18722 = call;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super Response<T>> subscriber) {
        Call<T> r1 = this.f18722.m24292();
        final CallArbiter callArbiter = new CallArbiter(r1, subscriber);
        subscriber.m24513((Subscription) callArbiter);
        subscriber.m24512((Producer) callArbiter);
        r1.m24295(new Callback<T>() {
            public void onFailure(Call<T> call, Throwable th) {
                Exceptions.m24529(th);
                callArbiter.m24450(th);
            }

            public void onResponse(Call<T> call, Response<T> response) {
                callArbiter.m24451(response);
            }
        });
    }
}
