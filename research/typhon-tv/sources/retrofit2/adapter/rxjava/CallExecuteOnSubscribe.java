package retrofit2.adapter.rxjava;

import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;

final class CallExecuteOnSubscribe<T> implements Observable.OnSubscribe<Response<T>> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Call<T> f18725;

    CallExecuteOnSubscribe(Call<T> call) {
        this.f18725 = call;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super Response<T>> subscriber) {
        Call<T> r1 = this.f18725.m24292();
        CallArbiter callArbiter = new CallArbiter(r1, subscriber);
        subscriber.m24513((Subscription) callArbiter);
        subscriber.m24512((Producer) callArbiter);
        try {
            callArbiter.m24451(r1.m24294());
        } catch (Throwable th) {
            Exceptions.m24529(th);
            callArbiter.m24450(th);
        }
    }
}
