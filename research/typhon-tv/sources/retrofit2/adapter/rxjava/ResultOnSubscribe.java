package retrofit2.adapter.rxjava;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.exceptions.CompositeException;
import rx.exceptions.Exceptions;
import rx.plugins.RxJavaPlugins;

final class ResultOnSubscribe<T> implements Observable.OnSubscribe<Result<T>> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Observable.OnSubscribe<Response<T>> f18728;

    private static class ResultSubscriber<R> extends Subscriber<Response<R>> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Subscriber<? super Result<R>> f18729;

        ResultSubscriber(Subscriber<? super Result<R>> subscriber) {
            super(subscriber);
            this.f18729 = subscriber;
        }

        public void onCompleted() {
            this.f18729.onCompleted();
        }

        public void onError(Throwable th) {
            try {
                this.f18729.onNext(Result.m24454(th));
                this.f18729.onCompleted();
            } catch (Throwable th2) {
                Exceptions.m24529(th2);
                RxJavaPlugins.m7437().m7440().m7429((Throwable) new CompositeException(th, th2));
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void onNext(Response<R> response) {
            this.f18729.onNext(Result.m24455(response));
        }
    }

    ResultOnSubscribe(Observable.OnSubscribe<Response<T>> onSubscribe) {
        this.f18728 = onSubscribe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super Result<T>> subscriber) {
        this.f18728.call(new ResultSubscriber(subscriber));
    }
}
