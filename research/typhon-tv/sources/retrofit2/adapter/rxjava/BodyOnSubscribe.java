package retrofit2.adapter.rxjava;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.exceptions.CompositeException;
import rx.exceptions.Exceptions;
import rx.plugins.RxJavaPlugins;

final class BodyOnSubscribe<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Observable.OnSubscribe<Response<T>> f18719;

    private static class BodySubscriber<R> extends Subscriber<Response<R>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f18720;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Subscriber<? super R> f18721;

        BodySubscriber(Subscriber<? super R> subscriber) {
            super(subscriber);
            this.f18721 = subscriber;
        }

        public void onCompleted() {
            if (!this.f18720) {
                this.f18721.onCompleted();
            }
        }

        public void onError(Throwable th) {
            if (!this.f18720) {
                this.f18721.onError(th);
                return;
            }
            AssertionError assertionError = new AssertionError("This should never happen! Report as a Retrofit bug with the full stacktrace.");
            assertionError.initCause(th);
            RxJavaPlugins.m7437().m7440().m7429((Throwable) assertionError);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void onNext(Response<R> response) {
            if (response.m24390()) {
                this.f18721.onNext(response.m24389());
                return;
            }
            this.f18720 = true;
            HttpException httpException = new HttpException(response);
            try {
                this.f18721.onError(httpException);
            } catch (Throwable th) {
                Exceptions.m24529(th);
                RxJavaPlugins.m7437().m7440().m7429((Throwable) new CompositeException(httpException, th));
            }
        }
    }

    BodyOnSubscribe(Observable.OnSubscribe<Response<T>> onSubscribe) {
        this.f18719 = onSubscribe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> subscriber) {
        this.f18719.call(new BodySubscriber(subscriber));
    }
}
