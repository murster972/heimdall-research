package retrofit2;

import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import okio.ForwardingSource;
import okio.Okio;
import okio.Source;

final class OkHttpCall<T> implements Call<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f18601;

    /* renamed from: 连任  reason: contains not printable characters */
    private Throwable f18602;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object[] f18603;

    /* renamed from: 麤  reason: contains not printable characters */
    private Call f18604;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile boolean f18605;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ServiceMethod<T, ?> f18606;

    static final class ExceptionCatchingRequestBody extends ResponseBody {

        /* renamed from: 靐  reason: contains not printable characters */
        private final ResponseBody f18609;

        /* renamed from: 龘  reason: contains not printable characters */
        IOException f18610;

        ExceptionCatchingRequestBody(ResponseBody responseBody) {
            this.f18609 = responseBody;
        }

        public void close() {
            this.f18609.close();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m24333() throws IOException {
            if (this.f18610 != null) {
                throw this.f18610;
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m24334() {
            return this.f18609.m7093();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public BufferedSource m24335() {
            return Okio.m20507((Source) new ForwardingSource(this.f18609.m7095()) {
                /* renamed from: 龘  reason: contains not printable characters */
                public long m24337(Buffer buffer, long j) throws IOException {
                    try {
                        return super.m20479(buffer, j);
                    } catch (IOException e) {
                        ExceptionCatchingRequestBody.this.f18610 = e;
                        throw e;
                    }
                }
            });
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaType m24336() {
            return this.f18609.m7096();
        }
    }

    static final class NoContentResponseBody extends ResponseBody {

        /* renamed from: 靐  reason: contains not printable characters */
        private final long f18612;

        /* renamed from: 龘  reason: contains not printable characters */
        private final MediaType f18613;

        NoContentResponseBody(MediaType mediaType, long j) {
            this.f18613 = mediaType;
            this.f18612 = j;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m24338() {
            return this.f18612;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public BufferedSource m24339() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaType m24340() {
            return this.f18613;
        }
    }

    OkHttpCall(ServiceMethod<T, ?> serviceMethod, Object[] objArr) {
        this.f18606 = serviceMethod;
        this.f18603 = objArr;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private Call m24320() throws IOException {
        Call r0 = this.f18606.f18688.m19888(this.f18606.m24414(this.f18603));
        if (r0 != null) {
            return r0;
        }
        throw new NullPointerException("Call.Factory returned null.");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public OkHttpCall<T> m24324() {
        return new OkHttpCall<>(this.f18606, this.f18603);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public synchronized Request m24322() {
        Request r2;
        Call call = this.f18604;
        if (call != null) {
            r2 = call.m19886();
        } else if (this.f18602 == null) {
            try {
                Call r22 = m24320();
                this.f18604 = r22;
                r2 = r22.m19886();
            } catch (RuntimeException e) {
                this.f18602 = e;
                throw e;
            } catch (IOException e2) {
                this.f18602 = e2;
                throw new RuntimeException("Unable to create request.", e2);
            }
        } else if (this.f18602 instanceof IOException) {
            throw new RuntimeException("Unable to create request.", this.f18602);
        } else {
            throw ((RuntimeException) this.f18602);
        }
        return r2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m24323() {
        Call call;
        this.f18605 = true;
        synchronized (this) {
            call = this.f18604;
        }
        if (call != null) {
            call.m19885();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m24325() {
        boolean z = true;
        if (!this.f18605) {
            synchronized (this) {
                if (this.f18604 == null || !this.f18604.m19884()) {
                    z = false;
                }
            }
        }
        return z;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x0032=Splitter:B:23:0x0032, B:32:0x0045=Splitter:B:32:0x0045} */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public retrofit2.Response<T> m24326() throws java.io.IOException {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r2 = r4.f18601     // Catch:{ all -> 0x000e }
            if (r2 == 0) goto L_0x0011
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x000e }
            java.lang.String r3 = "Already executed."
            r2.<init>(r3)     // Catch:{ all -> 0x000e }
            throw r2     // Catch:{ all -> 0x000e }
        L_0x000e:
            r2 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x000e }
            throw r2
        L_0x0011:
            r2 = 1
            r4.f18601 = r2     // Catch:{ all -> 0x000e }
            java.lang.Throwable r2 = r4.f18602     // Catch:{ all -> 0x000e }
            if (r2 == 0) goto L_0x0028
            java.lang.Throwable r2 = r4.f18602     // Catch:{ all -> 0x000e }
            boolean r2 = r2 instanceof java.io.IOException     // Catch:{ all -> 0x000e }
            if (r2 == 0) goto L_0x0023
            java.lang.Throwable r2 = r4.f18602     // Catch:{ all -> 0x000e }
            java.io.IOException r2 = (java.io.IOException) r2     // Catch:{ all -> 0x000e }
            throw r2     // Catch:{ all -> 0x000e }
        L_0x0023:
            java.lang.Throwable r2 = r4.f18602     // Catch:{ all -> 0x000e }
            java.lang.RuntimeException r2 = (java.lang.RuntimeException) r2     // Catch:{ all -> 0x000e }
            throw r2     // Catch:{ all -> 0x000e }
        L_0x0028:
            okhttp3.Call r0 = r4.f18604     // Catch:{ all -> 0x000e }
            if (r0 != 0) goto L_0x0032
            okhttp3.Call r0 = r4.m24320()     // Catch:{ IOException -> 0x0043, RuntimeException -> 0x0048 }
            r4.f18604 = r0     // Catch:{ IOException -> 0x0043, RuntimeException -> 0x0048 }
        L_0x0032:
            monitor-exit(r4)     // Catch:{ all -> 0x000e }
            boolean r2 = r4.f18605
            if (r2 == 0) goto L_0x003a
            r0.m19885()
        L_0x003a:
            okhttp3.Response r2 = r0.m19883()
            retrofit2.Response r2 = r4.m24327((okhttp3.Response) r2)
            return r2
        L_0x0043:
            r2 = move-exception
            r1 = r2
        L_0x0045:
            r4.f18602 = r1     // Catch:{ all -> 0x000e }
            throw r1     // Catch:{ all -> 0x000e }
        L_0x0048:
            r2 = move-exception
            r1 = r2
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: retrofit2.OkHttpCall.m24326():retrofit2.Response");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Response<T> m24327(Response response) throws IOException {
        ResponseBody r5 = response.m7056();
        Response r13 = response.m7060().m7086((ResponseBody) new NoContentResponseBody(r5.m7096(), r5.m7093())).m7087();
        int r3 = r13.m7066();
        if (r3 < 200 || r3 >= 300) {
            try {
                return Response.m24387(Utils.m24442(r5), r13);
            } finally {
                r5.close();
            }
        } else if (r3 == 204 || r3 == 205) {
            r5.close();
            return Response.m24386(null, r13);
        } else {
            ExceptionCatchingRequestBody exceptionCatchingRequestBody = new ExceptionCatchingRequestBody(r5);
            try {
                return Response.m24386(this.f18606.m24413((ResponseBody) exceptionCatchingRequestBody), r13);
            } catch (RuntimeException e) {
                exceptionCatchingRequestBody.m24333();
                throw e;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m24328(final Callback<T> callback) {
        Call call;
        Throwable th;
        if (callback == null) {
            throw new NullPointerException("callback == null");
        }
        synchronized (this) {
            if (this.f18601) {
                throw new IllegalStateException("Already executed.");
            }
            this.f18601 = true;
            call = this.f18604;
            th = this.f18602;
            if (call == null && th == null) {
                try {
                    Call r1 = m24320();
                    this.f18604 = r1;
                    call = r1;
                } catch (Throwable th2) {
                    this.f18602 = th2;
                    th = th2;
                }
            }
        }
        if (th != null) {
            callback.onFailure(this, th);
            return;
        }
        if (this.f18605) {
            call.m19885();
        }
        call.m19887(new Callback() {
            /* renamed from: 龘  reason: contains not printable characters */
            private void m24329(Throwable th) {
                try {
                    callback.onFailure(OkHttpCall.this, th);
                } catch (Throwable th2) {
                    th2.printStackTrace();
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            private void m24330(Response<T> response) {
                try {
                    callback.onResponse(OkHttpCall.this, response);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m24331(Call call, IOException iOException) {
                try {
                    callback.onFailure(OkHttpCall.this, iOException);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m24332(Call call, Response response) throws IOException {
                try {
                    m24330(OkHttpCall.this.m24327(response));
                } catch (Throwable th) {
                    m24329(th);
                }
            }
        });
    }
}
