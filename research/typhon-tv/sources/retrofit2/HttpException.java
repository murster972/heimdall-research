package retrofit2;

import org.apache.commons.lang3.StringUtils;

public class HttpException extends Exception {
    private final int code;
    private final String message;

    /* renamed from: 龘  reason: contains not printable characters */
    private final transient Response<?> f18600;

    public HttpException(Response<?> response) {
        super(m24319(response));
        this.code = response.m24391();
        this.message = response.m24388();
        this.f18600 = response;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m24319(Response<?> response) {
        if (response != null) {
            return "HTTP " + response.m24391() + StringUtils.SPACE + response.m24388();
        }
        throw new NullPointerException("response == null");
    }

    public int code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

    public Response<?> response() {
        return this.f18600;
    }
}
