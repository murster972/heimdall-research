package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.NoSuchElementException;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.Sink;

final class Utils {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Type[] f18712 = new Type[0];

    private static final class GenericArrayTypeImpl implements GenericArrayType {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Type f18713;

        GenericArrayTypeImpl(Type type) {
            this.f18713 = type;
        }

        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && Utils.m24445((Type) this, (Type) (GenericArrayType) obj);
        }

        public Type getGenericComponentType() {
            return this.f18713;
        }

        public int hashCode() {
            return this.f18713.hashCode();
        }

        public String toString() {
            return Utils.m24429(this.f18713) + "[]";
        }
    }

    private static final class ParameterizedTypeImpl implements ParameterizedType {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Type f18714;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Type[] f18715;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Type f18716;

        ParameterizedTypeImpl(Type type, Type type2, Type... typeArr) {
            boolean z = true;
            if (type2 instanceof Class) {
                if ((type == null) != (((Class) type2).getEnclosingClass() != null ? false : z)) {
                    throw new IllegalArgumentException();
                }
            }
            this.f18716 = type;
            this.f18714 = type2;
            this.f18715 = (Type[]) typeArr.clone();
            for (Type type3 : this.f18715) {
                if (type3 == null) {
                    throw new NullPointerException();
                }
                Utils.m24432(type3);
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && Utils.m24445((Type) this, (Type) (ParameterizedType) obj);
        }

        public Type[] getActualTypeArguments() {
            return (Type[]) this.f18715.clone();
        }

        public Type getOwnerType() {
            return this.f18716;
        }

        public Type getRawType() {
            return this.f18714;
        }

        public int hashCode() {
            return (Arrays.hashCode(this.f18715) ^ this.f18714.hashCode()) ^ Utils.m24433((Object) this.f18716);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder((this.f18715.length + 1) * 30);
            sb.append(Utils.m24429(this.f18714));
            if (this.f18715.length == 0) {
                return sb.toString();
            }
            sb.append("<").append(Utils.m24429(this.f18715[0]));
            for (int i = 1; i < this.f18715.length; i++) {
                sb.append(", ").append(Utils.m24429(this.f18715[i]));
            }
            return sb.append(">").toString();
        }
    }

    private static final class WildcardTypeImpl implements WildcardType {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Type f18717;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Type f18718;

        WildcardTypeImpl(Type[] typeArr, Type[] typeArr2) {
            if (typeArr2.length > 1) {
                throw new IllegalArgumentException();
            } else if (typeArr.length != 1) {
                throw new IllegalArgumentException();
            } else if (typeArr2.length == 1) {
                if (typeArr2[0] == null) {
                    throw new NullPointerException();
                }
                Utils.m24432(typeArr2[0]);
                if (typeArr[0] != Object.class) {
                    throw new IllegalArgumentException();
                }
                this.f18717 = typeArr2[0];
                this.f18718 = Object.class;
            } else if (typeArr[0] == null) {
                throw new NullPointerException();
            } else {
                Utils.m24432(typeArr[0]);
                this.f18717 = null;
                this.f18718 = typeArr[0];
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && Utils.m24445((Type) this, (Type) (WildcardType) obj);
        }

        public Type[] getLowerBounds() {
            if (this.f18717 == null) {
                return Utils.f18712;
            }
            return new Type[]{this.f18717};
        }

        public Type[] getUpperBounds() {
            return new Type[]{this.f18718};
        }

        public int hashCode() {
            return (this.f18717 != null ? this.f18717.hashCode() + 31 : 1) ^ (this.f18718.hashCode() + 31);
        }

        public String toString() {
            return this.f18717 != null ? "? super " + Utils.m24429(this.f18717) : this.f18718 == Object.class ? "?" : "? extends " + Utils.m24429(this.f18718);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    static Type m24428(Type type) {
        if (type instanceof ParameterizedType) {
            return m24438(0, (ParameterizedType) type);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static String m24429(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static Type m24430(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2.isAssignableFrom(cls)) {
            return m24440(type, cls, m24439(type, cls, cls2));
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static boolean m24431(Type type) {
        if (type instanceof Class) {
            return false;
        }
        if (type instanceof ParameterizedType) {
            for (Type r2 : ((ParameterizedType) type).getActualTypeArguments()) {
                if (m24431(r2)) {
                    return true;
                }
            }
            return false;
        } else if (type instanceof GenericArrayType) {
            return m24431(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (type instanceof TypeVariable) {
                return true;
            }
            if (type instanceof WildcardType) {
                return true;
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + (type == null ? "null" : type.getClass().getName()));
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static void m24432(Type type) {
        if ((type instanceof Class) && ((Class) type).isPrimitive()) {
            throw new IllegalArgumentException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m24433(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m24434(Object[] objArr, Object obj) {
        for (int i = 0; i < objArr.length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Class<?> m24435(Type type) {
        if (type == null) {
            throw new NullPointerException("type == null");
        } else if (type instanceof Class) {
            return (Class) type;
        } else {
            if (type instanceof ParameterizedType) {
                Type rawType = ((ParameterizedType) type).getRawType();
                if (rawType instanceof Class) {
                    return (Class) rawType;
                }
                throw new IllegalArgumentException();
            } else if (type instanceof GenericArrayType) {
                return Array.newInstance(m24435(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
            } else {
                if (type instanceof TypeVariable) {
                    return Object.class;
                }
                if (type instanceof WildcardType) {
                    return m24435(((WildcardType) type).getUpperBounds()[0]);
                }
                throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + type.getClass().getName());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Class<?> m24436(TypeVariable<?> typeVariable) {
        Object genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> T m24437(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Type m24438(int i, ParameterizedType parameterizedType) {
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if (i < 0 || i >= actualTypeArguments.length) {
            throw new IllegalArgumentException("Index " + i + " not in range [0," + actualTypeArguments.length + ") for " + parameterizedType);
        }
        Type type = actualTypeArguments[i];
        return type instanceof WildcardType ? ((WildcardType) type).getUpperBounds()[0] : type;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Type m24439(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return m24439(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<? super Object> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return m24439(cls.getGenericSuperclass(), (Class<?>) superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Type m24440(Type type, Class<?> cls, Type type2) {
        Type r19;
        while (type2 instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) type2;
            type2 = m24441(type, cls, (TypeVariable<?>) typeVariable);
            if (type2 == typeVariable) {
                Type type3 = type2;
                return type2;
            }
        }
        if ((type2 instanceof Class) && ((Class) type2).isArray()) {
            Class cls2 = (Class) type2;
            Class<?> componentType = cls2.getComponentType();
            Type r8 = m24440(type, cls, (Type) componentType);
            Type type4 = cls2;
            if (componentType != r8) {
                type4 = new GenericArrayTypeImpl(r8);
            }
            Type type5 = type2;
            return type4;
        } else if (type2 instanceof GenericArrayType) {
            GenericArrayType genericArrayType = (GenericArrayType) type2;
            Type genericComponentType = genericArrayType.getGenericComponentType();
            Type r82 = m24440(type, cls, genericComponentType);
            if (genericComponentType != r82) {
                genericArrayType = new GenericArrayTypeImpl(r82);
            }
            Type type6 = type2;
            return genericArrayType;
        } else if (type2 instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type2;
            Type ownerType = parameterizedType.getOwnerType();
            Type r9 = m24440(type, cls, ownerType);
            boolean z = r9 != ownerType;
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            int length = actualTypeArguments.length;
            for (int i = 0; i < length; i++) {
                Type r15 = m24440(type, cls, actualTypeArguments[i]);
                if (r15 != actualTypeArguments[i]) {
                    if (!z) {
                        actualTypeArguments = (Type[]) actualTypeArguments.clone();
                        z = true;
                    }
                    actualTypeArguments[i] = r15;
                }
            }
            if (z) {
                parameterizedType = new ParameterizedTypeImpl(r9, parameterizedType.getRawType(), actualTypeArguments);
            }
            Type type7 = type2;
            return parameterizedType;
        } else if (type2 instanceof WildcardType) {
            WildcardType wildcardType = (WildcardType) type2;
            Type[] lowerBounds = wildcardType.getLowerBounds();
            Type[] upperBounds = wildcardType.getUpperBounds();
            if (lowerBounds.length == 1) {
                Type r7 = m24440(type, cls, lowerBounds[0]);
                if (r7 != lowerBounds[0]) {
                    Type type8 = type2;
                    return new WildcardTypeImpl(new Type[]{Object.class}, new Type[]{r7});
                }
            } else if (upperBounds.length == 1 && (r19 = m24440(type, cls, upperBounds[0])) != upperBounds[0]) {
                Type type9 = type2;
                return new WildcardTypeImpl(new Type[]{r19}, f18712);
            }
            Type type10 = type2;
            return wildcardType;
        } else {
            Type type11 = type2;
            return type2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Type m24441(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class<?> r1 = m24436(typeVariable);
        if (r1 == null) {
            return typeVariable;
        }
        Type r0 = m24439(type, cls, r1);
        if (!(r0 instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) r0).getActualTypeArguments()[m24434((Object[]) r1.getTypeParameters(), (Object) typeVariable)];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ResponseBody m24442(ResponseBody responseBody) throws IOException {
        Buffer buffer = new Buffer();
        responseBody.m7095().m20468((Sink) buffer);
        return ResponseBody.m7089(responseBody.m7096(), responseBody.m7093(), buffer);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> void m24443(Class<T> cls) {
        if (!cls.isInterface()) {
            throw new IllegalArgumentException("API declarations must be interfaces.");
        } else if (cls.getInterfaces().length > 0) {
            throw new IllegalArgumentException("API interfaces must not extend other interfaces.");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m24444(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m24445(Type type, Type type2) {
        boolean z = true;
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            if (!m24444((Object) parameterizedType.getOwnerType(), (Object) parameterizedType2.getOwnerType()) || !parameterizedType.getRawType().equals(parameterizedType2.getRawType()) || !Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments())) {
                z = false;
            }
            return z;
        } else if (type instanceof GenericArrayType) {
            if (type2 instanceof GenericArrayType) {
                return m24445(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
            }
            return false;
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            if (!Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) || !Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds())) {
                z = false;
            }
            return z;
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            if (typeVariable.getGenericDeclaration() != typeVariable2.getGenericDeclaration() || !typeVariable.getName().equals(typeVariable2.getName())) {
                z = false;
            }
            return z;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m24446(Annotation[] annotationArr, Class<? extends Annotation> cls) {
        for (Annotation isInstance : annotationArr) {
            if (cls.isInstance(isInstance)) {
                return true;
            }
        }
        return false;
    }
}
