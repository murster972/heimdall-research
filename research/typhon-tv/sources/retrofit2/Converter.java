package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public interface Converter<F, T> {

    public static abstract class Factory {
        /* renamed from: 靐  reason: contains not printable characters */
        public Converter<?, String> m24302(Type type, Annotation[] annotationArr, Retrofit retrofit) {
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Converter<ResponseBody, ?> m24303(Type type, Annotation[] annotationArr, Retrofit retrofit) {
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Converter<?, RequestBody> m24304(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    T m24301(F f) throws IOException;
}
