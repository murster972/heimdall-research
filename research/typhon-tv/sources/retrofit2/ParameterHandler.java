package retrofit2;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Map;
import okhttp3.Headers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

abstract class ParameterHandler<T> {

    static final class Body<T> extends ParameterHandler<T> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Converter<T, RequestBody> f18616;

        Body(Converter<T, RequestBody> converter) {
            this.f18616 = converter;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24347(RequestBuilder requestBuilder, T t) {
            if (t == null) {
                throw new IllegalArgumentException("Body parameter value must not be null.");
            }
            try {
                requestBuilder.m24385(this.f18616.m24301(t));
            } catch (IOException e) {
                throw new RuntimeException("Unable to convert " + t + " to RequestBody", e);
            }
        }
    }

    static final class Field<T> extends ParameterHandler<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Converter<T, String> f18617;

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f18618;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f18619;

        Field(String str, Converter<T, String> converter, boolean z) {
            this.f18619 = (String) Utils.m24437(str, "name == null");
            this.f18617 = converter;
            this.f18618 = z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24348(RequestBuilder requestBuilder, T t) throws IOException {
            if (t != null) {
                requestBuilder.m24378(this.f18619, this.f18617.m24301(t), this.f18618);
            }
        }
    }

    static final class FieldMap<T> extends ParameterHandler<Map<String, T>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final boolean f18620;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Converter<T, String> f18621;

        FieldMap(Converter<T, String> converter, boolean z) {
            this.f18621 = converter;
            this.f18620 = z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24349(RequestBuilder requestBuilder, Map<String, T> map) throws IOException {
            if (map == null) {
                throw new IllegalArgumentException("Field map was null.");
            }
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                if (str == null) {
                    throw new IllegalArgumentException("Field map contained null key.");
                }
                Object value = next.getValue();
                if (value == null) {
                    throw new IllegalArgumentException("Field map contained null value for key '" + str + "'.");
                }
                requestBuilder.m24378(str, this.f18621.m24301(value), this.f18620);
            }
        }
    }

    static final class Header<T> extends ParameterHandler<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Converter<T, String> f18622;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f18623;

        Header(String str, Converter<T, String> converter) {
            this.f18623 = (String) Utils.m24437(str, "name == null");
            this.f18622 = converter;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24351(RequestBuilder requestBuilder, T t) throws IOException {
            if (t != null) {
                requestBuilder.m24381(this.f18623, this.f18622.m24301(t));
            }
        }
    }

    static final class HeaderMap<T> extends ParameterHandler<Map<String, T>> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Converter<T, String> f18624;

        HeaderMap(Converter<T, String> converter) {
            this.f18624 = converter;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24352(RequestBuilder requestBuilder, Map<String, T> map) throws IOException {
            if (map == null) {
                throw new IllegalArgumentException("Header map was null.");
            }
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                if (str == null) {
                    throw new IllegalArgumentException("Header map contained null key.");
                }
                Object value = next.getValue();
                if (value == null) {
                    throw new IllegalArgumentException("Header map contained null value for key '" + str + "'.");
                }
                requestBuilder.m24381(str, this.f18624.m24301(value));
            }
        }
    }

    static final class Part<T> extends ParameterHandler<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Converter<T, RequestBody> f18625;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Headers f18626;

        Part(Headers headers, Converter<T, RequestBody> converter) {
            this.f18626 = headers;
            this.f18625 = converter;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24354(RequestBuilder requestBuilder, T t) {
            if (t != null) {
                try {
                    requestBuilder.m24383(this.f18626, this.f18625.m24301(t));
                } catch (IOException e) {
                    throw new RuntimeException("Unable to convert " + t + " to RequestBody", e);
                }
            }
        }
    }

    static final class PartMap<T> extends ParameterHandler<Map<String, T>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f18627;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Converter<T, RequestBody> f18628;

        PartMap(Converter<T, RequestBody> converter, String str) {
            this.f18628 = converter;
            this.f18627 = str;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24355(RequestBuilder requestBuilder, Map<String, T> map) throws IOException {
            if (map == null) {
                throw new IllegalArgumentException("Part map was null.");
            }
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                if (str == null) {
                    throw new IllegalArgumentException("Part map contained null key.");
                }
                Object value = next.getValue();
                if (value == null) {
                    throw new IllegalArgumentException("Part map contained null value for key '" + str + "'.");
                }
                requestBuilder.m24383(Headers.m6929("Content-Disposition", "form-data; name=\"" + str + "\"", "Content-Transfer-Encoding", this.f18627), this.f18628.m24301(value));
            }
        }
    }

    static final class Path<T> extends ParameterHandler<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Converter<T, String> f18629;

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f18630;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f18631;

        Path(String str, Converter<T, String> converter, boolean z) {
            this.f18631 = (String) Utils.m24437(str, "name == null");
            this.f18629 = converter;
            this.f18630 = z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24357(RequestBuilder requestBuilder, T t) throws IOException {
            if (t == null) {
                throw new IllegalArgumentException("Path parameter \"" + this.f18631 + "\" value must not be null.");
            }
            requestBuilder.m24382(this.f18631, this.f18629.m24301(t), this.f18630);
        }
    }

    static final class Query<T> extends ParameterHandler<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Converter<T, String> f18632;

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f18633;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f18634;

        Query(String str, Converter<T, String> converter, boolean z) {
            this.f18634 = (String) Utils.m24437(str, "name == null");
            this.f18632 = converter;
            this.f18633 = z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24358(RequestBuilder requestBuilder, T t) throws IOException {
            if (t != null) {
                requestBuilder.m24377(this.f18634, this.f18632.m24301(t), this.f18633);
            }
        }
    }

    static final class QueryMap<T> extends ParameterHandler<Map<String, T>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final boolean f18635;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Converter<T, String> f18636;

        QueryMap(Converter<T, String> converter, boolean z) {
            this.f18636 = converter;
            this.f18635 = z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24359(RequestBuilder requestBuilder, Map<String, T> map) throws IOException {
            if (map == null) {
                throw new IllegalArgumentException("Query map was null.");
            }
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                if (str == null) {
                    throw new IllegalArgumentException("Query map contained null key.");
                }
                Object value = next.getValue();
                if (value == null) {
                    throw new IllegalArgumentException("Query map contained null value for key '" + str + "'.");
                }
                requestBuilder.m24377(str, this.f18636.m24301(value), this.f18635);
            }
        }
    }

    static final class QueryName<T> extends ParameterHandler<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final boolean f18637;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Converter<T, String> f18638;

        QueryName(Converter<T, String> converter, boolean z) {
            this.f18638 = converter;
            this.f18637 = z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24361(RequestBuilder requestBuilder, T t) throws IOException {
            if (t != null) {
                requestBuilder.m24377(this.f18638.m24301(t), (String) null, this.f18637);
            }
        }
    }

    static final class RawPart extends ParameterHandler<MultipartBody.Part> {

        /* renamed from: 龘  reason: contains not printable characters */
        static final RawPart f18639 = new RawPart();

        private RawPart() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24362(RequestBuilder requestBuilder, MultipartBody.Part part) throws IOException {
            if (part != null) {
                requestBuilder.m24384(part);
            }
        }
    }

    static final class RelativeUrl extends ParameterHandler<Object> {
        RelativeUrl() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24364(RequestBuilder requestBuilder, Object obj) {
            requestBuilder.m24380(obj);
        }
    }

    ParameterHandler() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final ParameterHandler<Object> m24341() {
        return new ParameterHandler<Object>() {
            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m24346(RequestBuilder requestBuilder, Object obj) throws IOException {
                if (obj != null) {
                    int length = Array.getLength(obj);
                    for (int i = 0; i < length; i++) {
                        ParameterHandler.this.m24343(requestBuilder, Array.get(obj, i));
                    }
                }
            }
        };
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final ParameterHandler<Iterable<T>> m24342() {
        return new ParameterHandler<Iterable<T>>() {
            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m24345(RequestBuilder requestBuilder, Iterable<T> iterable) throws IOException {
                if (iterable != null) {
                    for (T r0 : iterable) {
                        ParameterHandler.this.m24343(requestBuilder, r0);
                    }
                }
            }
        };
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m24343(RequestBuilder requestBuilder, T t) throws IOException;
}
