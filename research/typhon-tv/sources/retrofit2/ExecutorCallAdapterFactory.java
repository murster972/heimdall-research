package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import okhttp3.Request;
import retrofit2.CallAdapter;

final class ExecutorCallAdapterFactory extends CallAdapter.Factory {

    /* renamed from: 龘  reason: contains not printable characters */
    final Executor f18589;

    static final class ExecutorCallbackCall<T> implements Call<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Call<T> f18592;

        /* renamed from: 龘  reason: contains not printable characters */
        final Executor f18593;

        ExecutorCallbackCall(Executor executor, Call<T> call) {
            this.f18593 = executor;
            this.f18592 = call;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public Request m24313() {
            return this.f18592.m24290();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m24314() {
            this.f18592.m24291();
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Call<T> clone() {
            return new ExecutorCallbackCall(this.f18593, this.f18592.m24292());
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m24316() {
            return this.f18592.m24293();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Response<T> m24317() throws IOException {
            return this.f18592.m24294();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24318(final Callback<T> callback) {
            if (callback == null) {
                throw new NullPointerException("callback == null");
            }
            this.f18592.m24295(new Callback<T>() {
                public void onFailure(Call<T> call, final Throwable th) {
                    ExecutorCallbackCall.this.f18593.execute(new Runnable() {
                        public void run() {
                            callback.onFailure(ExecutorCallbackCall.this, th);
                        }
                    });
                }

                public void onResponse(Call<T> call, final Response<T> response) {
                    ExecutorCallbackCall.this.f18593.execute(new Runnable() {
                        public void run() {
                            if (ExecutorCallbackCall.this.f18592.m24293()) {
                                callback.onFailure(ExecutorCallbackCall.this, new IOException("Canceled"));
                            } else {
                                callback.onResponse(ExecutorCallbackCall.this, response);
                            }
                        }
                    });
                }
            });
        }
    }

    ExecutorCallAdapterFactory(Executor executor) {
        this.f18589 = executor;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CallAdapter<?, ?> m24309(Type type, Annotation[] annotationArr, Retrofit retrofit) {
        if (m24298(type) != Call.class) {
            return null;
        }
        final Type r0 = Utils.m24428(type);
        return new CallAdapter<Object, Call<?>>() {
            /* renamed from: 靐  reason: contains not printable characters */
            public Call<Object> m24311(Call<Object> call) {
                return new ExecutorCallbackCall(ExecutorCallAdapterFactory.this.f18589, call);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Type m24312() {
                return r0;
            }
        };
    }
}
