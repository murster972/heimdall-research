package retrofit2.converter.gson;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public final class GsonConverterFactory extends Converter.Factory {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Gson f18739;

    private GsonConverterFactory(Gson gson) {
        if (gson == null) {
            throw new NullPointerException("gson == null");
        }
        this.f18739 = gson;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static GsonConverterFactory m24463() {
        return m24464(new Gson());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static GsonConverterFactory m24464(Gson gson) {
        return new GsonConverterFactory(gson);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Converter<ResponseBody, ?> m24465(Type type, Annotation[] annotationArr, Retrofit retrofit) {
        return new GsonResponseBodyConverter(this.f18739, this.f18739.getAdapter(TypeToken.get(type)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Converter<?, RequestBody> m24466(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit) {
        return new GsonRequestBodyConverter(this.f18739, this.f18739.getAdapter(TypeToken.get(type)));
    }
}
