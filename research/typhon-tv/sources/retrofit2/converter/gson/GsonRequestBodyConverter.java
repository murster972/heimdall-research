package retrofit2.converter.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Converter;

final class GsonRequestBodyConverter<T> implements Converter<T, RequestBody> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Charset f18740 = Charset.forName("UTF-8");

    /* renamed from: 龘  reason: contains not printable characters */
    private static final MediaType f18741 = MediaType.m6996("application/json; charset=UTF-8");

    /* renamed from: 麤  reason: contains not printable characters */
    private final TypeAdapter<T> f18742;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Gson f18743;

    GsonRequestBodyConverter(Gson gson, TypeAdapter<T> typeAdapter) {
        this.f18743 = gson;
        this.f18742 = typeAdapter;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public RequestBody m24468(T t) throws IOException {
        Buffer buffer = new Buffer();
        JsonWriter newJsonWriter = this.f18743.newJsonWriter(new OutputStreamWriter(buffer.m7249(), f18740));
        this.f18742.write(newJsonWriter, t);
        newJsonWriter.close();
        return RequestBody.create(f18741, buffer.m7277());
    }
}
