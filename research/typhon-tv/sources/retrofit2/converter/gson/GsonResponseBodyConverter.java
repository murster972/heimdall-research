package retrofit2.converter.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import java.io.IOException;
import okhttp3.ResponseBody;
import retrofit2.Converter;

final class GsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final TypeAdapter<T> f18744;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Gson f18745;

    GsonResponseBodyConverter(Gson gson, TypeAdapter<T> typeAdapter) {
        this.f18745 = gson;
        this.f18744 = typeAdapter;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public T m24469(ResponseBody responseBody) throws IOException {
        try {
            return this.f18744.read(this.f18745.newJsonReader(responseBody.m7092()));
        } finally {
            responseBody.close();
        }
    }
}
