package retrofit2;

import com.mopub.volley.toolbox.HttpClientStack;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.apache.oltu.oauth2.common.OAuth;
import retrofit2.ParameterHandler;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.OPTIONS;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.QueryName;
import retrofit2.http.Url;

final class ServiceMethod<R, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Pattern f18675 = Pattern.compile("[a-zA-Z][a-zA-Z0-9_-]*");

    /* renamed from: 龘  reason: contains not printable characters */
    static final Pattern f18676 = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Converter<ResponseBody, R> f18677;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f18678;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String f18679;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final boolean f18680;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final ParameterHandler<?>[] f18681;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final boolean f18682;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Headers f18683;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final MediaType f18684;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final boolean f18685;

    /* renamed from: 连任  reason: contains not printable characters */
    private final HttpUrl f18686;

    /* renamed from: 麤  reason: contains not printable characters */
    final CallAdapter<R, T> f18687;

    /* renamed from: 齉  reason: contains not printable characters */
    final Call.Factory f18688;

    static final class Builder<T, R> {

        /* renamed from: ʻ  reason: contains not printable characters */
        Type f18689;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f18690;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f18691;

        /* renamed from: ʾ  reason: contains not printable characters */
        String f18692;

        /* renamed from: ʿ  reason: contains not printable characters */
        boolean f18693;

        /* renamed from: ˆ  reason: contains not printable characters */
        Set<String> f18694;

        /* renamed from: ˈ  reason: contains not printable characters */
        boolean f18695;

        /* renamed from: ˉ  reason: contains not printable characters */
        ParameterHandler<?>[] f18696;

        /* renamed from: ˊ  reason: contains not printable characters */
        String f18697;

        /* renamed from: ˋ  reason: contains not printable characters */
        Headers f18698;

        /* renamed from: ˎ  reason: contains not printable characters */
        MediaType f18699;

        /* renamed from: ˏ  reason: contains not printable characters */
        Converter<ResponseBody, T> f18700;

        /* renamed from: ˑ  reason: contains not printable characters */
        boolean f18701;

        /* renamed from: י  reason: contains not printable characters */
        CallAdapter<T, R> f18702;

        /* renamed from: ٴ  reason: contains not printable characters */
        boolean f18703;

        /* renamed from: ᐧ  reason: contains not printable characters */
        boolean f18704;

        /* renamed from: 连任  reason: contains not printable characters */
        final Type[] f18705;

        /* renamed from: 靐  reason: contains not printable characters */
        final Method f18706;

        /* renamed from: 麤  reason: contains not printable characters */
        final Annotation[][] f18707;

        /* renamed from: 齉  reason: contains not printable characters */
        final Annotation[] f18708;

        /* renamed from: 龘  reason: contains not printable characters */
        final Retrofit f18709;

        /* renamed from: ﹶ  reason: contains not printable characters */
        boolean f18710;

        /* renamed from: ﾞ  reason: contains not printable characters */
        boolean f18711;

        Builder(Retrofit retrofit, Method method) {
            this.f18709 = retrofit;
            this.f18706 = method;
            this.f18708 = method.getAnnotations();
            this.f18705 = method.getGenericParameterTypes();
            this.f18707 = method.getParameterAnnotations();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private CallAdapter<T, R> m24415() {
            Type genericReturnType = this.f18706.getGenericReturnType();
            if (Utils.m24431(genericReturnType)) {
                throw m24418("Method return type must not include a type variable or wildcard: %s", genericReturnType);
            } else if (genericReturnType == Void.TYPE) {
                throw m24418("Service methods cannot return void.", new Object[0]);
            } else {
                try {
                    return this.f18709.m24398(genericReturnType, this.f18706.getAnnotations());
                } catch (RuntimeException e) {
                    throw m24420((Throwable) e, "Unable to create call adapter for %s", genericReturnType);
                }
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private Converter<ResponseBody, T> m24416() {
            try {
                return this.f18709.m24394(this.f18689, this.f18706.getAnnotations());
            } catch (RuntimeException e) {
                throw m24420((Throwable) e, "Unable to create converter for %s", this.f18689);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private RuntimeException m24417(int i, String str, Object... objArr) {
            return m24418(str + " (parameter #" + (i + 1) + ")", objArr);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private RuntimeException m24418(String str, Object... objArr) {
            return m24420((Throwable) null, str, objArr);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private RuntimeException m24419(Throwable th, int i, String str, Object... objArr) {
            return m24420(th, str + " (parameter #" + (i + 1) + ")", objArr);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private RuntimeException m24420(Throwable th, String str, Object... objArr) {
            return new IllegalArgumentException(String.format(str, objArr) + "\n    for method " + this.f18706.getDeclaringClass().getSimpleName() + "." + this.f18706.getName(), th);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Headers m24421(String[] strArr) {
            Headers.Builder builder = new Headers.Builder();
            for (String str : strArr) {
                int indexOf = str.indexOf(58);
                if (indexOf == -1 || indexOf == 0 || indexOf == str.length() - 1) {
                    throw m24418("@Headers value must be in the form \"Name: Value\". Found: \"%s\"", str);
                }
                String substring = str.substring(0, indexOf);
                String trim = str.substring(indexOf + 1).trim();
                if (OAuth.HeaderType.CONTENT_TYPE.equalsIgnoreCase(substring)) {
                    MediaType r5 = MediaType.m6996(trim);
                    if (r5 == null) {
                        throw m24418("Malformed content type: %s", trim);
                    }
                    this.f18699 = r5;
                } else {
                    builder.m19954(substring, trim);
                }
            }
            return builder.m19955();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private ParameterHandler<?> m24422(int i, Type type, Annotation[] annotationArr) {
            ParameterHandler<?> parameterHandler = null;
            for (Annotation r0 : annotationArr) {
                ParameterHandler<?> r1 = m24423(i, type, annotationArr, r0);
                if (r1 != null) {
                    if (parameterHandler != null) {
                        throw m24417(i, "Multiple Retrofit annotations found, only one allowed.", new Object[0]);
                    }
                    parameterHandler = r1;
                }
            }
            if (parameterHandler != null) {
                return parameterHandler;
            }
            throw m24417(i, "No Retrofit annotation found.", new Object[0]);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private ParameterHandler<?> m24423(int i, Type type, Annotation[] annotationArr, Annotation annotation) {
            if (annotation instanceof Url) {
                if (this.f18695) {
                    throw m24417(i, "Multiple @Url method annotations found.", new Object[0]);
                } else if (this.f18703) {
                    throw m24417(i, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.f18704) {
                    throw m24417(i, "A @Url parameter must not come after a @Query", new Object[0]);
                } else if (this.f18697 != null) {
                    throw m24417(i, "@Url cannot be used with @%s URL", this.f18692);
                } else {
                    this.f18695 = true;
                    if (type == HttpUrl.class || type == String.class || type == URI.class || ((type instanceof Class) && "android.net.Uri".equals(((Class) type).getName()))) {
                        return new ParameterHandler.RelativeUrl();
                    }
                    throw m24417(i, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type.", new Object[0]);
                }
            } else if (annotation instanceof Path) {
                if (this.f18704) {
                    throw m24417(i, "A @Path parameter must not come after a @Query.", new Object[0]);
                } else if (this.f18695) {
                    throw m24417(i, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.f18697 == null) {
                    throw m24417(i, "@Path can only be used with relative url on @%s", this.f18692);
                } else {
                    this.f18703 = true;
                    Path path = (Path) annotation;
                    String r15 = path.m7340();
                    m24424(i, r15);
                    return new ParameterHandler.Path(r15, this.f18709.m24395(type, annotationArr), path.m7339());
                }
            } else if (annotation instanceof Query) {
                Query query = (Query) annotation;
                String r152 = query.m7342();
                boolean r8 = query.m7341();
                Class<?> r22 = Utils.m24435(type);
                this.f18704 = true;
                if (Iterable.class.isAssignableFrom(r22)) {
                    if (!(type instanceof ParameterizedType)) {
                        throw m24417(i, r22.getSimpleName() + " must include generic type (e.g., " + r22.getSimpleName() + "<String>)", new Object[0]);
                    }
                    return new ParameterHandler.Query(r152, this.f18709.m24395(Utils.m24438(0, (ParameterizedType) type), annotationArr), r8).m24342();
                } else if (!r22.isArray()) {
                    return new ParameterHandler.Query(r152, this.f18709.m24395(type, annotationArr), r8);
                } else {
                    return new ParameterHandler.Query(r152, this.f18709.m24395(ServiceMethod.m24411(r22.getComponentType()), annotationArr), r8).m24341();
                }
            } else if (annotation instanceof QueryName) {
                boolean r82 = ((QueryName) annotation).m7344();
                Class<?> r222 = Utils.m24435(type);
                this.f18704 = true;
                if (Iterable.class.isAssignableFrom(r222)) {
                    if (!(type instanceof ParameterizedType)) {
                        throw m24417(i, r222.getSimpleName() + " must include generic type (e.g., " + r222.getSimpleName() + "<String>)", new Object[0]);
                    }
                    return new ParameterHandler.QueryName(this.f18709.m24395(Utils.m24438(0, (ParameterizedType) type), annotationArr), r82).m24342();
                } else if (!r222.isArray()) {
                    return new ParameterHandler.QueryName(this.f18709.m24395(type, annotationArr), r82);
                } else {
                    return new ParameterHandler.QueryName(this.f18709.m24395(ServiceMethod.m24411(r222.getComponentType()), annotationArr), r82).m24341();
                }
            } else if (annotation instanceof QueryMap) {
                Class<?> r223 = Utils.m24435(type);
                if (!Map.class.isAssignableFrom(r223)) {
                    throw m24417(i, "@QueryMap parameter type must be Map.", new Object[0]);
                }
                Type r14 = Utils.m24430(type, r223, Map.class);
                if (!(r14 instanceof ParameterizedType)) {
                    throw m24417(i, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                ParameterizedType parameterizedType = (ParameterizedType) r14;
                Type r13 = Utils.m24438(0, parameterizedType);
                if (String.class != r13) {
                    throw m24417(i, "@QueryMap keys must be of type String: " + r13, new Object[0]);
                }
                return new ParameterHandler.QueryMap(this.f18709.m24395(Utils.m24438(1, parameterizedType), annotationArr), ((QueryMap) annotation).m7343());
            } else if (annotation instanceof Header) {
                String r153 = ((Header) annotation).m7330();
                Class<?> r224 = Utils.m24435(type);
                if (Iterable.class.isAssignableFrom(r224)) {
                    if (!(type instanceof ParameterizedType)) {
                        throw m24417(i, r224.getSimpleName() + " must include generic type (e.g., " + r224.getSimpleName() + "<String>)", new Object[0]);
                    }
                    return new ParameterHandler.Header(r153, this.f18709.m24395(Utils.m24438(0, (ParameterizedType) type), annotationArr)).m24342();
                } else if (!r224.isArray()) {
                    return new ParameterHandler.Header(r153, this.f18709.m24395(type, annotationArr));
                } else {
                    return new ParameterHandler.Header(r153, this.f18709.m24395(ServiceMethod.m24411(r224.getComponentType()), annotationArr)).m24341();
                }
            } else if (annotation instanceof HeaderMap) {
                Class<?> r225 = Utils.m24435(type);
                if (!Map.class.isAssignableFrom(r225)) {
                    throw m24417(i, "@HeaderMap parameter type must be Map.", new Object[0]);
                }
                Type r142 = Utils.m24430(type, r225, Map.class);
                if (!(r142 instanceof ParameterizedType)) {
                    throw m24417(i, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                ParameterizedType parameterizedType2 = (ParameterizedType) r142;
                Type r132 = Utils.m24438(0, parameterizedType2);
                if (String.class != r132) {
                    throw m24417(i, "@HeaderMap keys must be of type String: " + r132, new Object[0]);
                }
                return new ParameterHandler.HeaderMap(this.f18709.m24395(Utils.m24438(1, parameterizedType2), annotationArr));
            } else if (annotation instanceof Field) {
                if (!this.f18710) {
                    throw m24417(i, "@Field parameters can only be used with form encoding.", new Object[0]);
                }
                Field field = (Field) annotation;
                String r154 = field.m7323();
                boolean r83 = field.m7322();
                this.f18690 = true;
                Class<?> r226 = Utils.m24435(type);
                if (Iterable.class.isAssignableFrom(r226)) {
                    if (!(type instanceof ParameterizedType)) {
                        throw m24417(i, r226.getSimpleName() + " must include generic type (e.g., " + r226.getSimpleName() + "<String>)", new Object[0]);
                    }
                    return new ParameterHandler.Field(r154, this.f18709.m24395(Utils.m24438(0, (ParameterizedType) type), annotationArr), r83).m24342();
                } else if (!r226.isArray()) {
                    return new ParameterHandler.Field(r154, this.f18709.m24395(type, annotationArr), r83);
                } else {
                    return new ParameterHandler.Field(r154, this.f18709.m24395(ServiceMethod.m24411(r226.getComponentType()), annotationArr), r83).m24341();
                }
            } else if (annotation instanceof FieldMap) {
                if (!this.f18710) {
                    throw m24417(i, "@FieldMap parameters can only be used with form encoding.", new Object[0]);
                }
                Class<?> r227 = Utils.m24435(type);
                if (!Map.class.isAssignableFrom(r227)) {
                    throw m24417(i, "@FieldMap parameter type must be Map.", new Object[0]);
                }
                Type r143 = Utils.m24430(type, r227, Map.class);
                if (!(r143 instanceof ParameterizedType)) {
                    throw m24417(i, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                ParameterizedType parameterizedType3 = (ParameterizedType) r143;
                Type r133 = Utils.m24438(0, parameterizedType3);
                if (String.class != r133) {
                    throw m24417(i, "@FieldMap keys must be of type String: " + r133, new Object[0]);
                }
                Converter r23 = this.f18709.m24395(Utils.m24438(1, parameterizedType3), annotationArr);
                this.f18690 = true;
                return new ParameterHandler.FieldMap(r23, ((FieldMap) annotation).m7324());
            } else if (annotation instanceof Part) {
                if (!this.f18711) {
                    throw m24417(i, "@Part parameters can only be used with multipart encoding.", new Object[0]);
                }
                Part part = (Part) annotation;
                this.f18691 = true;
                String r19 = part.m7337();
                Class<?> r228 = Utils.m24435(type);
                if (!r19.isEmpty()) {
                    Headers r11 = Headers.m6929("Content-Disposition", "form-data; name=\"" + r19 + "\"", "Content-Transfer-Encoding", part.m7336());
                    if (Iterable.class.isAssignableFrom(r228)) {
                        if (!(type instanceof ParameterizedType)) {
                            throw m24417(i, r228.getSimpleName() + " must include generic type (e.g., " + r228.getSimpleName() + "<String>)", new Object[0]);
                        }
                        Type r12 = Utils.m24438(0, (ParameterizedType) type);
                        if (MultipartBody.Part.class.isAssignableFrom(Utils.m24435(r12))) {
                            throw m24417(i, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        }
                        return new ParameterHandler.Part(r11, this.f18709.m24400(r12, annotationArr, this.f18708)).m24342();
                    } else if (r228.isArray()) {
                        Class<?> r4 = ServiceMethod.m24411(r228.getComponentType());
                        if (MultipartBody.Part.class.isAssignableFrom(r4)) {
                            throw m24417(i, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        }
                        return new ParameterHandler.Part(r11, this.f18709.m24400((Type) r4, annotationArr, this.f18708)).m24341();
                    } else if (MultipartBody.Part.class.isAssignableFrom(r228)) {
                        throw m24417(i, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                    } else {
                        return new ParameterHandler.Part(r11, this.f18709.m24400(type, annotationArr, this.f18708));
                    }
                } else if (Iterable.class.isAssignableFrom(r228)) {
                    if (!(type instanceof ParameterizedType)) {
                        throw m24417(i, r228.getSimpleName() + " must include generic type (e.g., " + r228.getSimpleName() + "<String>)", new Object[0]);
                    } else if (MultipartBody.Part.class.isAssignableFrom(Utils.m24435(Utils.m24438(0, (ParameterizedType) type)))) {
                        return ParameterHandler.RawPart.f18639.m24342();
                    } else {
                        throw m24417(i, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                    }
                } else if (r228.isArray()) {
                    if (MultipartBody.Part.class.isAssignableFrom(r228.getComponentType())) {
                        return ParameterHandler.RawPart.f18639.m24341();
                    }
                    throw m24417(i, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                } else if (MultipartBody.Part.class.isAssignableFrom(r228)) {
                    return ParameterHandler.RawPart.f18639;
                } else {
                    throw m24417(i, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                }
            } else if (annotation instanceof PartMap) {
                if (!this.f18711) {
                    throw m24417(i, "@PartMap parameters can only be used with multipart encoding.", new Object[0]);
                }
                this.f18691 = true;
                Class<?> r229 = Utils.m24435(type);
                if (!Map.class.isAssignableFrom(r229)) {
                    throw m24417(i, "@PartMap parameter type must be Map.", new Object[0]);
                }
                Type r144 = Utils.m24430(type, r229, Map.class);
                if (!(r144 instanceof ParameterizedType)) {
                    throw m24417(i, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                ParameterizedType parameterizedType4 = (ParameterizedType) r144;
                Type r134 = Utils.m24438(0, parameterizedType4);
                if (String.class != r134) {
                    throw m24417(i, "@PartMap keys must be of type String: " + r134, new Object[0]);
                }
                Type r25 = Utils.m24438(1, parameterizedType4);
                if (MultipartBody.Part.class.isAssignableFrom(Utils.m24435(r25))) {
                    throw m24417(i, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead.", new Object[0]);
                }
                return new ParameterHandler.PartMap(this.f18709.m24400(r25, annotationArr, this.f18708), ((PartMap) annotation).m7338());
            } else if (!(annotation instanceof Body)) {
                return null;
            } else {
                if (this.f18710 || this.f18711) {
                    throw m24417(i, "@Body parameters cannot be used with form or multi-part encoding.", new Object[0]);
                } else if (this.f18701) {
                    throw m24417(i, "Multiple @Body method annotations found.", new Object[0]);
                } else {
                    try {
                        Converter r6 = this.f18709.m24400(type, annotationArr, this.f18708);
                        this.f18701 = true;
                        return new ParameterHandler.Body(r6);
                    } catch (RuntimeException e) {
                        throw m24419((Throwable) e, i, "Unable to create @Body converter for %s", type);
                    }
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m24424(int i, String str) {
            if (!ServiceMethod.f18675.matcher(str).matches()) {
                throw m24417(i, "@Path parameter name must match %s. Found: %s", ServiceMethod.f18676.pattern(), str);
            } else if (!this.f18694.contains(str)) {
                throw m24417(i, "URL \"%s\" does not contain \"{%s}\".", this.f18697, str);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m24425(String str, String str2, boolean z) {
            if (this.f18692 != null) {
                throw m24418("Only one HTTP method is allowed. Found: %s and %s.", this.f18692, str);
            }
            this.f18692 = str;
            this.f18693 = z;
            if (!str2.isEmpty()) {
                int indexOf = str2.indexOf(63);
                if (indexOf != -1 && indexOf < str2.length() - 1) {
                    String substring = str2.substring(indexOf + 1);
                    if (ServiceMethod.f18676.matcher(substring).find()) {
                        throw m24418("URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", substring);
                    }
                }
                this.f18697 = str2;
                this.f18694 = ServiceMethod.m24412(str2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m24426(Annotation annotation) {
            if (annotation instanceof DELETE) {
                m24425(OAuth.HttpMethod.DELETE, ((DELETE) annotation).m7321(), false);
            } else if (annotation instanceof GET) {
                m24425(OAuth.HttpMethod.GET, ((GET) annotation).m7325(), false);
            } else if (annotation instanceof HEAD) {
                m24425("HEAD", ((HEAD) annotation).m7326(), false);
                if (!Void.class.equals(this.f18689)) {
                    throw m24418("HEAD method must use Void as response type.", new Object[0]);
                }
            } else if (annotation instanceof PATCH) {
                m24425(HttpClientStack.HttpPatch.METHOD_NAME, ((PATCH) annotation).m7333(), true);
            } else if (annotation instanceof POST) {
                m24425(OAuth.HttpMethod.POST, ((POST) annotation).m7334(), true);
            } else if (annotation instanceof PUT) {
                m24425(OAuth.HttpMethod.PUT, ((PUT) annotation).m7335(), true);
            } else if (annotation instanceof OPTIONS) {
                m24425("OPTIONS", ((OPTIONS) annotation).m7332(), false);
            } else if (annotation instanceof HTTP) {
                HTTP http = (HTTP) annotation;
                m24425(http.m7329(), http.m7327(), http.m7328());
            } else if (annotation instanceof retrofit2.http.Headers) {
                String[] r0 = ((retrofit2.http.Headers) annotation).m7331();
                if (r0.length == 0) {
                    throw m24418("@Headers annotation is empty.", new Object[0]);
                }
                this.f18698 = m24421(r0);
            } else if (annotation instanceof Multipart) {
                if (this.f18710) {
                    throw m24418("Only one encoding annotation is allowed.", new Object[0]);
                }
                this.f18711 = true;
            } else if (!(annotation instanceof FormUrlEncoded)) {
            } else {
                if (this.f18711) {
                    throw m24418("Only one encoding annotation is allowed.", new Object[0]);
                }
                this.f18710 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ServiceMethod m24427() {
            this.f18702 = m24415();
            this.f18689 = this.f18702.m24297();
            if (this.f18689 == Response.class || this.f18689 == Response.class) {
                throw m24418("'" + Utils.m24435(this.f18689).getName() + "' is not a valid response body type. Did you mean ResponseBody?", new Object[0]);
            }
            this.f18700 = m24416();
            for (Annotation r0 : this.f18708) {
                m24426(r0);
            }
            if (this.f18692 == null) {
                throw m24418("HTTP method annotation is required (e.g., @GET, @POST, etc.).", new Object[0]);
            }
            if (!this.f18693) {
                if (this.f18711) {
                    throw m24418("Multipart can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                } else if (this.f18710) {
                    throw m24418("FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                }
            }
            int length = this.f18707.length;
            this.f18696 = new ParameterHandler[length];
            for (int i = 0; i < length; i++) {
                Type type = this.f18705[i];
                if (Utils.m24431(type)) {
                    throw m24417(i, "Parameter type must not include a type variable or wildcard: %s", type);
                }
                Annotation[] annotationArr = this.f18707[i];
                if (annotationArr == null) {
                    throw m24417(i, "No Retrofit annotation found.", new Object[0]);
                }
                this.f18696[i] = m24422(i, type, annotationArr);
            }
            if (this.f18697 == null && !this.f18695) {
                throw m24418("Missing either @%s URL or @Url parameter.", this.f18692);
            } else if (!this.f18710 && !this.f18711 && !this.f18693 && this.f18701) {
                throw m24418("Non-body HTTP method cannot contain @Body.", new Object[0]);
            } else if (this.f18710 && !this.f18690) {
                throw m24418("Form-encoded method must contain at least one @Field.", new Object[0]);
            } else if (!this.f18711 || this.f18691) {
                return new ServiceMethod(this);
            } else {
                throw m24418("Multipart method must contain at least one @Part.", new Object[0]);
            }
        }
    }

    ServiceMethod(Builder<R, T> builder) {
        this.f18688 = builder.f18709.m24397();
        this.f18687 = builder.f18702;
        this.f18686 = builder.f18709.m24393();
        this.f18677 = builder.f18700;
        this.f18678 = builder.f18692;
        this.f18679 = builder.f18697;
        this.f18683 = builder.f18698;
        this.f18684 = builder.f18699;
        this.f18685 = builder.f18693;
        this.f18682 = builder.f18710;
        this.f18680 = builder.f18711;
        this.f18681 = builder.f18696;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Class<?> m24411(Class<?> cls) {
        return Boolean.TYPE == cls ? Boolean.class : Byte.TYPE == cls ? Byte.class : Character.TYPE == cls ? Character.class : Double.TYPE == cls ? Double.class : Float.TYPE == cls ? Float.class : Integer.TYPE == cls ? Integer.class : Long.TYPE == cls ? Long.class : Short.TYPE == cls ? Short.class : cls;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Set<String> m24412(String str) {
        Matcher matcher = f18676.matcher(str);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        while (matcher.find()) {
            linkedHashSet.add(matcher.group(1));
        }
        return linkedHashSet;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public R m24413(ResponseBody responseBody) throws IOException {
        return this.f18677.m24301(responseBody);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Request m24414(Object... objArr) throws IOException {
        RequestBuilder requestBuilder = new RequestBuilder(this.f18678, this.f18686, this.f18679, this.f18683, this.f18684, this.f18685, this.f18682, this.f18680);
        ParameterHandler<?>[] parameterHandlerArr = this.f18681;
        int length = objArr != null ? objArr.length : 0;
        if (length != parameterHandlerArr.length) {
            throw new IllegalArgumentException("Argument count (" + length + ") doesn't match expected count (" + parameterHandlerArr.length + ")");
        }
        for (int i = 0; i < length; i++) {
            parameterHandlerArr[i].m24343(requestBuilder, objArr[i]);
        }
        return requestBuilder.m24379();
    }
}
