package retrofit2;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public interface CallAdapter<R, T> {

    public static abstract class Factory {
        /* renamed from: 龘  reason: contains not printable characters */
        protected static Class<?> m24298(Type type) {
            return Utils.m24435(type);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        protected static Type m24299(int i, ParameterizedType parameterizedType) {
            return Utils.m24438(i, parameterizedType);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract CallAdapter<?, ?> m24300(Type type, Annotation[] annotationArr, Retrofit retrofit);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    T m24296(Call<R> call);

    /* renamed from: 龘  reason: contains not printable characters */
    Type m24297();
}
