package retrofit2;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.BuiltInConverters;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.ServiceMethod;

public final class Retrofit {

    /* renamed from: ʻ  reason: contains not printable characters */
    final boolean f18658;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Map<Method, ServiceMethod<?, ?>> f18659 = new ConcurrentHashMap();

    /* renamed from: 连任  reason: contains not printable characters */
    final Executor f18660;

    /* renamed from: 靐  reason: contains not printable characters */
    final HttpUrl f18661;

    /* renamed from: 麤  reason: contains not printable characters */
    final List<CallAdapter.Factory> f18662;

    /* renamed from: 齉  reason: contains not printable characters */
    final List<Converter.Factory> f18663;

    /* renamed from: 龘  reason: contains not printable characters */
    final Call.Factory f18664;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Executor f18668;

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f18669;

        /* renamed from: 连任  reason: contains not printable characters */
        private final List<CallAdapter.Factory> f18670;

        /* renamed from: 靐  reason: contains not printable characters */
        private Call.Factory f18671;

        /* renamed from: 麤  reason: contains not printable characters */
        private final List<Converter.Factory> f18672;

        /* renamed from: 齉  reason: contains not printable characters */
        private HttpUrl f18673;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Platform f18674;

        public Builder() {
            this(Platform.m24366());
        }

        Builder(Platform platform) {
            this.f18672 = new ArrayList();
            this.f18670 = new ArrayList();
            this.f18674 = platform;
            this.f18672.add(new BuiltInConverters());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m24404(String str) {
            Utils.m24437(str, "baseUrl == null");
            HttpUrl r0 = HttpUrl.m6937(str);
            if (r0 != null) {
                return m24406(r0);
            }
            throw new IllegalArgumentException("Illegal URL: " + str);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m24405(Call.Factory factory) {
            this.f18671 = (Call.Factory) Utils.m24437(factory, "factory == null");
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m24406(HttpUrl httpUrl) {
            Utils.m24437(httpUrl, "baseUrl == null");
            List<String> r0 = httpUrl.m6958();
            if (!"".equals(r0.get(r0.size() - 1))) {
                throw new IllegalArgumentException("baseUrl must end in /: " + httpUrl);
            }
            this.f18673 = httpUrl;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m24407(OkHttpClient okHttpClient) {
            return m24405((Call.Factory) Utils.m24437(okHttpClient, "client == null"));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m24408(CallAdapter.Factory factory) {
            this.f18670.add(Utils.m24437(factory, "factory == null"));
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m24409(Converter.Factory factory) {
            this.f18672.add(Utils.m24437(factory, "factory == null"));
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Retrofit m24410() {
            if (this.f18673 == null) {
                throw new IllegalStateException("Base URL required.");
            }
            Call.Factory factory = this.f18671;
            if (factory == null) {
                factory = new OkHttpClient();
            }
            Executor executor = this.f18668;
            if (executor == null) {
                executor = this.f18674.m24367();
            }
            ArrayList arrayList = new ArrayList(this.f18670);
            arrayList.add(this.f18674.m24369(executor));
            return new Retrofit(factory, this.f18673, new ArrayList(this.f18672), arrayList, executor, this.f18669);
        }
    }

    Retrofit(Call.Factory factory, HttpUrl httpUrl, List<Converter.Factory> list, List<CallAdapter.Factory> list2, Executor executor, boolean z) {
        this.f18664 = factory;
        this.f18661 = httpUrl;
        this.f18663 = Collections.unmodifiableList(list);
        this.f18662 = Collections.unmodifiableList(list2);
        this.f18660 = executor;
        this.f18658 = z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m24392(Class<?> cls) {
        Platform r1 = Platform.m24366();
        for (Method method : cls.getDeclaredMethods()) {
            if (!r1.m24370(method)) {
                m24403(method);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public HttpUrl m24393() {
        return this.f18661;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public <T> Converter<ResponseBody, T> m24394(Type type, Annotation[] annotationArr) {
        return m24401((Converter.Factory) null, type, annotationArr);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public <T> Converter<T, String> m24395(Type type, Annotation[] annotationArr) {
        Utils.m24437(type, "type == null");
        Utils.m24437(annotationArr, "annotations == null");
        int size = this.f18663.size();
        for (int i = 0; i < size; i++) {
            Converter<?, String> r0 = this.f18663.get(i).m24302(type, annotationArr, this);
            if (r0 != null) {
                return r0;
            }
        }
        return BuiltInConverters.ToStringConverter.f18584;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> T m24396(final Class<T> cls) {
        Utils.m24443(cls);
        if (this.f18658) {
            m24392(cls);
        }
        return Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new InvocationHandler() {

            /* renamed from: 齉  reason: contains not printable characters */
            private final Platform f18666 = Platform.m24366();

            public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
                if (method.getDeclaringClass() == Object.class) {
                    return method.invoke(this, objArr);
                }
                if (this.f18666.m24370(method)) {
                    return this.f18666.m24368(method, cls, obj, objArr);
                }
                ServiceMethod<?, ?> r1 = Retrofit.this.m24403(method);
                return r1.f18687.m24296(new OkHttpCall(r1, objArr));
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Call.Factory m24397() {
        return this.f18664;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CallAdapter<?, ?> m24398(Type type, Annotation[] annotationArr) {
        return m24399((CallAdapter.Factory) null, type, annotationArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CallAdapter<?, ?> m24399(CallAdapter.Factory factory, Type type, Annotation[] annotationArr) {
        Utils.m24437(type, "returnType == null");
        Utils.m24437(annotationArr, "annotations == null");
        int indexOf = this.f18662.indexOf(factory) + 1;
        int size = this.f18662.size();
        for (int i = indexOf; i < size; i++) {
            CallAdapter<?, ?> r0 = this.f18662.get(i).m24300(type, annotationArr, this);
            if (r0 != null) {
                return r0;
            }
        }
        StringBuilder append = new StringBuilder("Could not locate call adapter for ").append(type).append(".\n");
        if (factory != null) {
            append.append("  Skipped:");
            for (int i2 = 0; i2 < indexOf; i2++) {
                append.append("\n   * ").append(this.f18662.get(i2).getClass().getName());
            }
            append.append(10);
        }
        append.append("  Tried:");
        int size2 = this.f18662.size();
        for (int i3 = indexOf; i3 < size2; i3++) {
            append.append("\n   * ").append(this.f18662.get(i3).getClass().getName());
        }
        throw new IllegalArgumentException(append.toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Converter<T, RequestBody> m24400(Type type, Annotation[] annotationArr, Annotation[] annotationArr2) {
        return m24402((Converter.Factory) null, type, annotationArr, annotationArr2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Converter<ResponseBody, T> m24401(Converter.Factory factory, Type type, Annotation[] annotationArr) {
        Utils.m24437(type, "type == null");
        Utils.m24437(annotationArr, "annotations == null");
        int indexOf = this.f18663.indexOf(factory) + 1;
        int size = this.f18663.size();
        for (int i = indexOf; i < size; i++) {
            Converter<ResponseBody, ?> r1 = this.f18663.get(i).m24303(type, annotationArr, this);
            if (r1 != null) {
                return r1;
            }
        }
        StringBuilder append = new StringBuilder("Could not locate ResponseBody converter for ").append(type).append(".\n");
        if (factory != null) {
            append.append("  Skipped:");
            for (int i2 = 0; i2 < indexOf; i2++) {
                append.append("\n   * ").append(this.f18663.get(i2).getClass().getName());
            }
            append.append(10);
        }
        append.append("  Tried:");
        int size2 = this.f18663.size();
        for (int i3 = indexOf; i3 < size2; i3++) {
            append.append("\n   * ").append(this.f18663.get(i3).getClass().getName());
        }
        throw new IllegalArgumentException(append.toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Converter<T, RequestBody> m24402(Converter.Factory factory, Type type, Annotation[] annotationArr, Annotation[] annotationArr2) {
        Utils.m24437(type, "type == null");
        Utils.m24437(annotationArr, "parameterAnnotations == null");
        Utils.m24437(annotationArr2, "methodAnnotations == null");
        int indexOf = this.f18663.indexOf(factory) + 1;
        int size = this.f18663.size();
        for (int i = indexOf; i < size; i++) {
            Converter<?, RequestBody> r1 = this.f18663.get(i).m24304(type, annotationArr, annotationArr2, this);
            if (r1 != null) {
                return r1;
            }
        }
        StringBuilder append = new StringBuilder("Could not locate RequestBody converter for ").append(type).append(".\n");
        if (factory != null) {
            append.append("  Skipped:");
            for (int i2 = 0; i2 < indexOf; i2++) {
                append.append("\n   * ").append(this.f18663.get(i2).getClass().getName());
            }
            append.append(10);
        }
        append.append("  Tried:");
        int size2 = this.f18663.size();
        for (int i3 = indexOf; i3 < size2; i3++) {
            append.append("\n   * ").append(this.f18663.get(i3).getClass().getName());
        }
        throw new IllegalArgumentException(append.toString());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ServiceMethod<?, ?> m24403(Method method) {
        ServiceMethod<?, ?> serviceMethod;
        ServiceMethod<?, ?> serviceMethod2 = this.f18659.get(method);
        if (serviceMethod2 != null) {
            ServiceMethod<?, ?> serviceMethod3 = serviceMethod2;
            return serviceMethod2;
        }
        synchronized (this.f18659) {
            serviceMethod = this.f18659.get(method);
            if (serviceMethod == null) {
                serviceMethod = new ServiceMethod.Builder(this, method).m24427();
                this.f18659.put(method, serviceMethod);
            }
        }
        ServiceMethod<?, ?> serviceMethod4 = serviceMethod;
        return serviceMethod;
    }
}
