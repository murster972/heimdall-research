package retrofit2;

import okhttp3.ResponseBody;

public final class Response<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final T f18655;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ResponseBody f18656;

    /* renamed from: 龘  reason: contains not printable characters */
    private final okhttp3.Response f18657;

    private Response(okhttp3.Response response, T t, ResponseBody responseBody) {
        this.f18657 = response;
        this.f18655 = t;
        this.f18656 = responseBody;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Response<T> m24386(T t, okhttp3.Response response) {
        if (response == null) {
            throw new NullPointerException("rawResponse == null");
        } else if (response.m7065()) {
            return new Response<>(response, t, (ResponseBody) null);
        } else {
            throw new IllegalArgumentException("rawResponse must be successful response");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Response<T> m24387(ResponseBody responseBody, okhttp3.Response response) {
        if (responseBody == null) {
            throw new NullPointerException("body == null");
        } else if (response == null) {
            throw new NullPointerException("rawResponse == null");
        } else if (!response.m7065()) {
            return new Response<>(response, (Object) null, responseBody);
        } else {
            throw new IllegalArgumentException("rawResponse should not be successful response");
        }
    }

    public String toString() {
        return this.f18657.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m24388() {
        return this.f18657.m7063();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public T m24389() {
        return this.f18655;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m24390() {
        return this.f18657.m7065();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m24391() {
        return this.f18657.m7066();
    }
}
