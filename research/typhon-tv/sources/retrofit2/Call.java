package retrofit2;

import java.io.IOException;
import okhttp3.Request;

public interface Call<T> extends Cloneable {
    /* renamed from: 连任  reason: contains not printable characters */
    Request m24290();

    /* renamed from: 靐  reason: contains not printable characters */
    void m24291();

    /* renamed from: 麤  reason: contains not printable characters */
    Call<T> m24292();

    /* renamed from: 齉  reason: contains not printable characters */
    boolean m24293();

    /* renamed from: 龘  reason: contains not printable characters */
    Response<T> m24294() throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m24295(Callback<T> callback);
}
