package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.http.Streaming;

final class BuiltInConverters extends Converter.Factory {

    static final class BufferingResponseBodyConverter implements Converter<ResponseBody, ResponseBody> {

        /* renamed from: 龘  reason: contains not printable characters */
        static final BufferingResponseBodyConverter f18581 = new BufferingResponseBodyConverter();

        BufferingResponseBodyConverter() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ResponseBody m24280(ResponseBody responseBody) throws IOException {
            try {
                return Utils.m24442(responseBody);
            } finally {
                responseBody.close();
            }
        }
    }

    static final class RequestBodyConverter implements Converter<RequestBody, RequestBody> {

        /* renamed from: 龘  reason: contains not printable characters */
        static final RequestBodyConverter f18582 = new RequestBodyConverter();

        RequestBodyConverter() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public RequestBody m24282(RequestBody requestBody) throws IOException {
            return requestBody;
        }
    }

    static final class StreamingResponseBodyConverter implements Converter<ResponseBody, ResponseBody> {

        /* renamed from: 龘  reason: contains not printable characters */
        static final StreamingResponseBodyConverter f18583 = new StreamingResponseBodyConverter();

        StreamingResponseBodyConverter() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ResponseBody m24284(ResponseBody responseBody) throws IOException {
            return responseBody;
        }
    }

    static final class ToStringConverter implements Converter<Object, String> {

        /* renamed from: 龘  reason: contains not printable characters */
        static final ToStringConverter f18584 = new ToStringConverter();

        ToStringConverter() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m24287(Object obj) {
            return obj.toString();
        }
    }

    static final class VoidResponseBodyConverter implements Converter<ResponseBody, Void> {

        /* renamed from: 龘  reason: contains not printable characters */
        static final VoidResponseBodyConverter f18585 = new VoidResponseBodyConverter();

        VoidResponseBodyConverter() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Void m24288(ResponseBody responseBody) throws IOException {
            responseBody.close();
            return null;
        }
    }

    BuiltInConverters() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Converter<ResponseBody, ?> m24278(Type type, Annotation[] annotationArr, Retrofit retrofit) {
        if (type == ResponseBody.class) {
            return Utils.m24446(annotationArr, (Class<? extends Annotation>) Streaming.class) ? StreamingResponseBodyConverter.f18583 : BufferingResponseBodyConverter.f18581;
        }
        if (type == Void.class) {
            return VoidResponseBodyConverter.f18585;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Converter<?, RequestBody> m24279(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit) {
        if (RequestBody.class.isAssignableFrom(Utils.m24435(type))) {
            return RequestBodyConverter.f18582;
        }
        return null;
    }
}
