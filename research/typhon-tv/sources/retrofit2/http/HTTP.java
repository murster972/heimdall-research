package retrofit2.http;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface HTTP {
    /* renamed from: 靐  reason: contains not printable characters */
    String m7327() default "";

    /* renamed from: 齉  reason: contains not printable characters */
    boolean m7328() default false;

    /* renamed from: 龘  reason: contains not printable characters */
    String m7329();
}
