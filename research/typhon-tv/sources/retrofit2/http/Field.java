package retrofit2.http;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Field {
    /* renamed from: 靐  reason: contains not printable characters */
    boolean m7322() default false;

    /* renamed from: 龘  reason: contains not printable characters */
    String m7323();
}
