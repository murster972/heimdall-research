package retrofit2.http;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Part {
    /* renamed from: 靐  reason: contains not printable characters */
    String m7336() default "binary";

    /* renamed from: 龘  reason: contains not printable characters */
    String m7337() default "";
}
