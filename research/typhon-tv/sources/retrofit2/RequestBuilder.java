package retrofit2;

import java.io.IOException;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import org.apache.oltu.oauth2.common.OAuth;

final class RequestBuilder {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final char[] f18642 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Request.Builder f18643 = new Request.Builder();

    /* renamed from: ʼ  reason: contains not printable characters */
    private MediaType f18644;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final boolean f18645;

    /* renamed from: ˑ  reason: contains not printable characters */
    private MultipartBody.Builder f18646;

    /* renamed from: ٴ  reason: contains not printable characters */
    private FormBody.Builder f18647;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private RequestBody f18648;

    /* renamed from: 连任  reason: contains not printable characters */
    private HttpUrl.Builder f18649;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f18650;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f18651;

    /* renamed from: 齉  reason: contains not printable characters */
    private final HttpUrl f18652;

    private static class ContentTypeOverridingRequestBody extends RequestBody {

        /* renamed from: 靐  reason: contains not printable characters */
        private final MediaType f18653;

        /* renamed from: 龘  reason: contains not printable characters */
        private final RequestBody f18654;

        ContentTypeOverridingRequestBody(RequestBody requestBody, MediaType mediaType) {
            this.f18654 = requestBody;
            this.f18653 = mediaType;
        }

        public long contentLength() throws IOException {
            return this.f18654.contentLength();
        }

        public MediaType contentType() {
            return this.f18653;
        }

        public void writeTo(BufferedSink bufferedSink) throws IOException {
            this.f18654.writeTo(bufferedSink);
        }
    }

    RequestBuilder(String str, HttpUrl httpUrl, String str2, Headers headers, MediaType mediaType, boolean z, boolean z2, boolean z3) {
        this.f18650 = str;
        this.f18652 = httpUrl;
        this.f18651 = str2;
        this.f18644 = mediaType;
        this.f18645 = z;
        if (headers != null) {
            this.f18643.m19996(headers);
        }
        if (z2) {
            this.f18647 = new FormBody.Builder();
        } else if (z3) {
            this.f18646 = new MultipartBody.Builder();
            this.f18646.m19958(MultipartBody.f16053);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m24375(String str, boolean z) {
        int i = 0;
        int length = str.length();
        while (i < length) {
            int codePointAt = str.codePointAt(i);
            if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                Buffer buffer = new Buffer();
                buffer.m7267(str, 0, i);
                m24376(buffer, str, i, length, z);
                return buffer.m7224();
            }
            i += Character.charCount(codePointAt);
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m24376(Buffer buffer, String str, int i, int i2, boolean z) {
        Buffer buffer2 = null;
        int i3 = i;
        while (i3 < i2) {
            int codePointAt = str.codePointAt(i3);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                    if (buffer2 == null) {
                        buffer2 = new Buffer();
                    }
                    buffer2.m7265(codePointAt);
                    while (!buffer2.m7210()) {
                        byte r0 = buffer2.m7228() & 255;
                        buffer.m7238(37);
                        buffer.m7238((int) f18642[(r0 >> 4) & 15]);
                        buffer.m7238((int) f18642[r0 & 15]);
                    }
                } else {
                    buffer.m7265(codePointAt);
                }
            }
            i3 += Character.charCount(codePointAt);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m24377(String str, String str2, boolean z) {
        if (this.f18651 != null) {
            this.f18649 = this.f18652.m6963(this.f18651);
            if (this.f18649 == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.f18652 + ", Relative: " + this.f18651);
            }
            this.f18651 = null;
        }
        if (z) {
            this.f18649.m6981(str, str2);
        } else {
            this.f18649.m6989(str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m24378(String str, String str2, boolean z) {
        if (z) {
            this.f18647.m19939(str, str2);
        } else {
            this.f18647.m19940(str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Request m24379() {
        HttpUrl r3;
        HttpUrl.Builder builder = this.f18649;
        if (builder != null) {
            r3 = builder.m6984();
        } else {
            r3 = this.f18652.m6964(this.f18651);
            if (r3 == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.f18652 + ", Relative: " + this.f18651);
            }
        }
        ContentTypeOverridingRequestBody contentTypeOverridingRequestBody = this.f18648;
        if (contentTypeOverridingRequestBody == null) {
            if (this.f18647 != null) {
                contentTypeOverridingRequestBody = this.f18647.m19941();
            } else if (this.f18646 != null) {
                contentTypeOverridingRequestBody = this.f18646.m19960();
            } else if (this.f18645) {
                contentTypeOverridingRequestBody = RequestBody.create((MediaType) null, new byte[0]);
            }
        }
        MediaType mediaType = this.f18644;
        if (mediaType != null) {
            if (contentTypeOverridingRequestBody != null) {
                contentTypeOverridingRequestBody = new ContentTypeOverridingRequestBody(contentTypeOverridingRequestBody, mediaType);
            } else {
                this.f18643.m19988(OAuth.HeaderType.CONTENT_TYPE, mediaType.toString());
            }
        }
        return this.f18643.m19997(r3).m19994(this.f18650, contentTypeOverridingRequestBody).m19989();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24380(Object obj) {
        if (obj == null) {
            throw new NullPointerException("@Url parameter is null.");
        }
        this.f18651 = obj.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24381(String str, String str2) {
        if (OAuth.HeaderType.CONTENT_TYPE.equalsIgnoreCase(str)) {
            MediaType r0 = MediaType.m6996(str2);
            if (r0 == null) {
                throw new IllegalArgumentException("Malformed content type: " + str2);
            }
            this.f18644 = r0;
            return;
        }
        this.f18643.m19988(str, str2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24382(String str, String str2, boolean z) {
        if (this.f18651 == null) {
            throw new AssertionError();
        }
        this.f18651 = this.f18651.replace("{" + str + "}", m24375(str2, z));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24383(Headers headers, RequestBody requestBody) {
        this.f18646.m19957(headers, requestBody);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24384(MultipartBody.Part part) {
        this.f18646.m19959(part);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24385(RequestBody requestBody) {
        this.f18648 = requestBody;
    }
}
