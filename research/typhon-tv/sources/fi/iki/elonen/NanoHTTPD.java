package fi.iki.elonen;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.TimeZones;
import org.apache.oltu.oauth2.common.OAuth;

public abstract class NanoHTTPD {
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final Logger f6131 = Logger.getLogger(NanoHTTPD.class.getName());
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Pattern f6132 = Pattern.compile("([ |\t]*Content-Disposition[ |\t]*:)(.*)", 2);
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public static final Pattern f6133 = Pattern.compile("[ |\t]*([a-zA-Z]*)[ |\t]*=[ |\t]*['|\"]([^\"^']*)['|\"]");
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Pattern f6134 = Pattern.compile("([ |\t]*content-type[ |\t]*:)(.*)", 2);
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String f6135;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int f6136;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public volatile ServerSocket f6137;

    /* renamed from: ˑ  reason: contains not printable characters */
    private ServerSocketFactory f6138;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Thread f6139;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public TempFileManagerFactory f6140;

    /* renamed from: 龘  reason: contains not printable characters */
    protected AsyncRunner f6141;

    protected class HTTPSession implements IHTTPSession {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f6142;

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f6143;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Method f6144;

        /* renamed from: ʾ  reason: contains not printable characters */
        private String f6145;

        /* renamed from: ʿ  reason: contains not printable characters */
        private String f6146;

        /* renamed from: ˈ  reason: contains not printable characters */
        private String f6147;

        /* renamed from: ˑ  reason: contains not printable characters */
        private Map<String, List<String>> f6148;

        /* renamed from: ٴ  reason: contains not printable characters */
        private Map<String, String> f6149;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private CookieHandler f6150;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f6151;

        /* renamed from: 靐  reason: contains not printable characters */
        private final TempFileManager f6152;

        /* renamed from: 麤  reason: contains not printable characters */
        private final BufferedInputStream f6153;

        /* renamed from: 齉  reason: contains not printable characters */
        private final OutputStream f6154;

        /* renamed from: ﹶ  reason: contains not printable characters */
        private String f6156;

        public HTTPSession(TempFileManager tempFileManager, InputStream inputStream, OutputStream outputStream, InetAddress inetAddress) {
            this.f6152 = tempFileManager;
            this.f6153 = new BufferedInputStream(inputStream, 8192);
            this.f6154 = outputStream;
            this.f6145 = (inetAddress.isLoopbackAddress() || inetAddress.isAnyLocalAddress()) ? "127.0.0.1" : inetAddress.getHostAddress().toString();
            this.f6146 = (inetAddress.isLoopbackAddress() || inetAddress.isAnyLocalAddress()) ? "localhost" : inetAddress.getHostName().toString();
            this.f6149 = new HashMap();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        private RandomAccessFile m6673() {
            try {
                return new RandomAccessFile(this.f6152.m18899((String) null).m18897(), InternalZipTyphoonApp.WRITE_MODE);
            } catch (Exception e) {
                throw new Error(e);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private int m6674(byte[] bArr, int i) {
            for (int i2 = 0; i2 + 1 < i; i2++) {
                if (bArr[i2] == 13 && bArr[i2 + 1] == 10 && i2 + 3 < i && bArr[i2 + 2] == 13 && bArr[i2 + 3] == 10) {
                    return i2 + 4;
                }
                if (bArr[i2] == 10 && bArr[i2 + 1] == 10) {
                    return i2 + 2;
                }
            }
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m6675(byte[] bArr, int i) {
            while (bArr[i] != 10) {
                i++;
            }
            return i + 1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m6676(ByteBuffer byteBuffer, int i, int i2, String str) {
            String str2 = "";
            if (i2 > 0) {
                FileOutputStream fileOutputStream = null;
                try {
                    TempFile r6 = this.f6152.m18899(str);
                    ByteBuffer duplicate = byteBuffer.duplicate();
                    FileOutputStream fileOutputStream2 = new FileOutputStream(r6.m18897());
                    try {
                        FileChannel channel = fileOutputStream2.getChannel();
                        duplicate.position(i).limit(i + i2);
                        channel.write(duplicate.slice());
                        str2 = r6.m18897();
                        NanoHTTPD.m6653((Object) fileOutputStream2);
                    } catch (Exception e) {
                        e = e;
                        fileOutputStream = fileOutputStream2;
                        try {
                            throw new Error(e);
                        } catch (Throwable th) {
                            th = th;
                            NanoHTTPD.m6653((Object) fileOutputStream);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        fileOutputStream = fileOutputStream2;
                        NanoHTTPD.m6653((Object) fileOutputStream);
                        throw th;
                    }
                } catch (Exception e2) {
                    e = e2;
                    throw new Error(e);
                }
            }
            return str2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m6677(ContentType contentType, ByteBuffer byteBuffer, Map<String, List<String>> map, Map<String, String> map2) throws ResponseException {
            int i;
            int i2 = 0;
            try {
                int[] r6 = m6680(byteBuffer, contentType.m18862().getBytes());
                if (r6.length < 2) {
                    throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Content type is multipart/form-data but contains less than two boundary strings.");
                }
                byte[] bArr = new byte[1024];
                for (int i3 = 0; i3 < r6.length - 1; i3++) {
                    byteBuffer.position(r6[i3]);
                    int remaining = byteBuffer.remaining() < 1024 ? byteBuffer.remaining() : 1024;
                    byteBuffer.get(bArr, 0, remaining);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bArr, 0, remaining), Charset.forName(contentType.m18863())), remaining);
                    String readLine = bufferedReader.readLine();
                    int i4 = 0 + 1;
                    if (readLine == null || !readLine.contains(contentType.m18862())) {
                        throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Content type is multipart/form-data but chunk does not start with boundary.");
                    }
                    String str = null;
                    String str2 = null;
                    String str3 = null;
                    String readLine2 = bufferedReader.readLine();
                    int i5 = i4 + 1;
                    while (readLine2 != null && readLine2.trim().length() > 0) {
                        Matcher matcher = NanoHTTPD.f6132.matcher(readLine2);
                        if (matcher.matches()) {
                            Matcher matcher2 = NanoHTTPD.f6133.matcher(matcher.group(2));
                            int i6 = i2;
                            while (matcher2.find()) {
                                try {
                                    String group = matcher2.group(1);
                                    if ("name".equalsIgnoreCase(group)) {
                                        str = matcher2.group(2);
                                        i = i6;
                                    } else {
                                        if ("filename".equalsIgnoreCase(group)) {
                                            str2 = matcher2.group(2);
                                            if (!str2.isEmpty()) {
                                                if (i6 > 0) {
                                                    i = i6 + 1;
                                                    str = str + String.valueOf(i6);
                                                } else {
                                                    i = i6 + 1;
                                                }
                                            }
                                        }
                                        i = i6;
                                    }
                                    i6 = i;
                                } catch (ResponseException e) {
                                    e = e;
                                    int i7 = i6;
                                    throw e;
                                } catch (Exception e2) {
                                    e = e2;
                                    int i8 = i6;
                                    throw new ResponseException(Response.Status.INTERNAL_ERROR, e.toString());
                                }
                            }
                            i2 = i6;
                        }
                        Matcher matcher3 = NanoHTTPD.f6134.matcher(readLine2);
                        if (matcher3.matches()) {
                            str3 = matcher3.group(2).trim();
                        }
                        readLine2 = bufferedReader.readLine();
                        i5++;
                    }
                    int i9 = 0;
                    int i10 = i5;
                    while (true) {
                        int i11 = i10 - 1;
                        if (i10 <= 0) {
                            break;
                        }
                        i9 = m6675(bArr, i9);
                        i10 = i11;
                    }
                    if (i9 >= remaining - 4) {
                        throw new ResponseException(Response.Status.INTERNAL_ERROR, "Multipart header size exceeds MAX_HEADER_SIZE.");
                    }
                    int i12 = r6[i3] + i9;
                    int i13 = r6[i3 + 1] - 4;
                    byteBuffer.position(i12);
                    List list = map.get(str);
                    if (list == null) {
                        list = new ArrayList();
                        map.put(str, list);
                    }
                    if (str3 == null) {
                        byte[] bArr2 = new byte[(i13 - i12)];
                        byteBuffer.get(bArr2);
                        list.add(new String(bArr2, contentType.m18863()));
                    } else {
                        String r24 = m6676(byteBuffer, i12, i13 - i12, str2);
                        if (!map2.containsKey(str)) {
                            map2.put(str, r24);
                        } else {
                            int i14 = 2;
                            while (map2.containsKey(str + i14)) {
                                i14++;
                            }
                            map2.put(str + i14, r24);
                        }
                        list.add(str2);
                    }
                }
            } catch (ResponseException e3) {
                e = e3;
                throw e;
            } catch (Exception e4) {
                e = e4;
                throw new ResponseException(Response.Status.INTERNAL_ERROR, e.toString());
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m6678(BufferedReader bufferedReader, Map<String, String> map, Map<String, List<String>> map2, Map<String, String> map3) throws ResponseException {
            String r6;
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    StringTokenizer stringTokenizer = new StringTokenizer(readLine);
                    if (!stringTokenizer.hasMoreTokens()) {
                        throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Syntax error. Usage: GET /example/file.html");
                    }
                    map.put("method", stringTokenizer.nextToken());
                    if (!stringTokenizer.hasMoreTokens()) {
                        throw new ResponseException(Response.Status.BAD_REQUEST, "BAD REQUEST: Missing URI. Usage: GET /example/file.html");
                    }
                    String nextToken = stringTokenizer.nextToken();
                    int indexOf = nextToken.indexOf(63);
                    if (indexOf >= 0) {
                        m6679(nextToken.substring(indexOf + 1), map2);
                        r6 = NanoHTTPD.m6659(nextToken.substring(0, indexOf));
                    } else {
                        r6 = NanoHTTPD.m6659(nextToken);
                    }
                    if (stringTokenizer.hasMoreTokens()) {
                        this.f6156 = stringTokenizer.nextToken();
                    } else {
                        this.f6156 = "HTTP/1.1";
                        NanoHTTPD.f6131.log(Level.FINE, "no protocol version specified, strange. Assuming HTTP/1.1.");
                    }
                    String readLine2 = bufferedReader.readLine();
                    while (readLine2 != null && !readLine2.trim().isEmpty()) {
                        int indexOf2 = readLine2.indexOf(58);
                        if (indexOf2 >= 0) {
                            map3.put(readLine2.substring(0, indexOf2).trim().toLowerCase(Locale.US), readLine2.substring(indexOf2 + 1).trim());
                        }
                        readLine2 = bufferedReader.readLine();
                    }
                    map.put("uri", r6);
                }
            } catch (IOException e) {
                throw new ResponseException(Response.Status.INTERNAL_ERROR, "SERVER INTERNAL ERROR: IOException: " + e.getMessage(), e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m6679(String str, Map<String, List<String>> map) {
            String trim;
            String str2;
            if (str == null) {
                this.f6147 = "";
                return;
            }
            this.f6147 = str;
            StringTokenizer stringTokenizer = new StringTokenizer(str, "&");
            while (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                int indexOf = nextToken.indexOf(61);
                if (indexOf >= 0) {
                    trim = NanoHTTPD.m6659(nextToken.substring(0, indexOf)).trim();
                    str2 = NanoHTTPD.m6659(nextToken.substring(indexOf + 1));
                } else {
                    trim = NanoHTTPD.m6659(nextToken).trim();
                    str2 = "";
                }
                List list = map.get(trim);
                if (list == null) {
                    list = new ArrayList();
                    map.put(trim, list);
                }
                list.add(str2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int[] m6680(ByteBuffer byteBuffer, byte[] bArr) {
            int[] iArr = new int[0];
            if (byteBuffer.remaining() < bArr.length) {
                int[] iArr2 = iArr;
                return iArr;
            }
            int i = 0;
            byte[] bArr2 = new byte[(bArr.length + 4096)];
            int remaining = byteBuffer.remaining() < bArr2.length ? byteBuffer.remaining() : bArr2.length;
            byteBuffer.get(bArr2, 0, remaining);
            int length = remaining - bArr.length;
            do {
                int i2 = 0;
                while (i2 < length) {
                    int i3 = 0;
                    while (i3 < bArr.length && bArr2[i2 + i3] == bArr[i3]) {
                        if (i3 == bArr.length - 1) {
                            int[] iArr3 = new int[(iArr.length + 1)];
                            System.arraycopy(iArr, 0, iArr3, 0, iArr.length);
                            iArr3[iArr.length] = i + i2;
                            iArr = iArr3;
                        }
                        i3++;
                    }
                    i2++;
                }
                i += length;
                System.arraycopy(bArr2, bArr2.length - bArr.length, bArr2, 0, bArr.length);
                length = bArr2.length - bArr.length;
                if (byteBuffer.remaining() < length) {
                    length = byteBuffer.remaining();
                }
                byteBuffer.get(bArr2, bArr.length, length);
            } while (length > 0);
            int[] iArr4 = iArr;
            return iArr;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public final String m6681() {
            return this.f6143;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public long m6682() {
            if (this.f6149.containsKey("content-length")) {
                return Long.parseLong(this.f6149.get("content-length"));
            }
            if (this.f6151 < this.f6142) {
                return (long) (this.f6142 - this.f6151);
            }
            return 0;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public String m6683() {
            return this.f6147;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final Map<String, String> m6684() {
            return this.f6149;
        }

        @Deprecated
        /* renamed from: 麤  reason: contains not printable characters */
        public final Map<String, String> m6685() {
            HashMap hashMap = new HashMap();
            for (String next : this.f6148.keySet()) {
                hashMap.put(next, this.f6148.get(next).get(0));
            }
            return hashMap;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final Method m6686() {
            return this.f6144;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0058, code lost:
            r7 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            throw r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x006a, code lost:
            r17 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
            throw r17;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x027b, code lost:
            r13 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
            fi.iki.elonen.NanoHTTPD.m6657(r13.getStatus(), "text/plain", r13.getMessage()).m18887(r22.f6154);
            fi.iki.elonen.NanoHTTPD.m6660((java.lang.Object) r22.f6154);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:0x029f, code lost:
            fi.iki.elonen.NanoHTTPD.m6660((java.lang.Object) null);
            r22.f6152.m18900();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
            return;
         */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0058 A[ExcHandler: SocketException (r7v0 'e' java.net.SocketException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x006a A[ExcHandler: SocketTimeoutException (r17v0 'e' java.net.SocketTimeoutException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x027b A[ExcHandler: ResponseException (r13v0 'e' fi.iki.elonen.NanoHTTPD$ResponseException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m6687() throws java.io.IOException {
            /*
                r22 = this;
                r12 = 0
                r18 = 8192(0x2000, float:1.14794E-41)
                r0 = r18
                byte[] r5 = new byte[r0]     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = 0
                r0 = r18
                r1 = r22
                r1.f6151 = r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = 0
                r0 = r18
                r1 = r22
                r1.f6142 = r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r14 = -1
                r0 = r22
                java.io.BufferedInputStream r0 = r0.f6153     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r19 = 8192(0x2000, float:1.14794E-41)
                r18.mark(r19)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.io.BufferedInputStream r0 = r0.f6153     // Catch:{ SSLException -> 0x0068, IOException -> 0x006c, SocketException -> 0x0058, SocketTimeoutException -> 0x006a, ResponseException -> 0x027b }
                r18 = r0
                r19 = 0
                r20 = 8192(0x2000, float:1.14794E-41)
                r0 = r18
                r1 = r19
                r2 = r20
                int r14 = r0.read(r5, r1, r2)     // Catch:{ SSLException -> 0x0068, IOException -> 0x006c, SocketException -> 0x0058, SocketTimeoutException -> 0x006a, ResponseException -> 0x027b }
                r18 = -1
                r0 = r18
                if (r14 != r0) goto L_0x00ed
                r0 = r22
                java.io.BufferedInputStream r0 = r0.f6153     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r18)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.io.OutputStream r0 = r0.f6154     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r18)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.net.SocketException r18 = new java.net.SocketException     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r19 = "NanoHttpd Shutdown"
                r18.<init>(r19)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                throw r18     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x0058:
                r7 = move-exception
                throw r7     // Catch:{ all -> 0x005a }
            L_0x005a:
                r18 = move-exception
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r12)
                r0 = r22
                fi.iki.elonen.NanoHTTPD$TempFileManager r0 = r0.f6152
                r19 = r0
                r19.m18900()
                throw r18
            L_0x0068:
                r7 = move-exception
                throw r7     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x006a:
                r17 = move-exception
                throw r17     // Catch:{ all -> 0x005a }
            L_0x006c:
                r7 = move-exception
                r0 = r22
                java.io.BufferedInputStream r0 = r0.f6153     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r18)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.io.OutputStream r0 = r0.f6154     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r18)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.net.SocketException r18 = new java.net.SocketException     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r19 = "NanoHttpd Shutdown"
                r18.<init>(r19)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                throw r18     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x0088:
                r16 = move-exception
                fi.iki.elonen.NanoHTTPD$Response$Status r18 = fi.iki.elonen.NanoHTTPD.Response.Status.INTERNAL_ERROR     // Catch:{ all -> 0x005a }
                java.lang.String r19 = "text/plain"
                java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ all -> 0x005a }
                r20.<init>()     // Catch:{ all -> 0x005a }
                java.lang.String r21 = "SSL PROTOCOL FAILURE: "
                java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ all -> 0x005a }
                java.lang.String r21 = r16.getMessage()     // Catch:{ all -> 0x005a }
                java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ all -> 0x005a }
                java.lang.String r20 = r20.toString()     // Catch:{ all -> 0x005a }
                fi.iki.elonen.NanoHTTPD$Response r15 = fi.iki.elonen.NanoHTTPD.m6657(r18, r19, r20)     // Catch:{ all -> 0x005a }
                r0 = r22
                java.io.OutputStream r0 = r0.f6154     // Catch:{ all -> 0x005a }
                r18 = r0
                r0 = r18
                r15.m18887((java.io.OutputStream) r0)     // Catch:{ all -> 0x005a }
                r0 = r22
                java.io.OutputStream r0 = r0.f6154     // Catch:{ all -> 0x005a }
                r18 = r0
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r18)     // Catch:{ all -> 0x005a }
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r12)
                r0 = r22
                fi.iki.elonen.NanoHTTPD$TempFileManager r0 = r0.f6152
                r18 = r0
                r18.m18900()
            L_0x00ca:
                return
            L_0x00cb:
                r0 = r22
                java.io.BufferedInputStream r0 = r0.f6153     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r22
                int r0 = r0.f6142     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r19 = r0
                r0 = r22
                int r0 = r0.f6142     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r20 = r0
                r0 = r20
                int r0 = 8192 - r0
                r20 = r0
                r0 = r18
                r1 = r19
                r2 = r20
                int r14 = r0.read(r5, r1, r2)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x00ed:
                if (r14 <= 0) goto L_0x0119
                r0 = r22
                int r0 = r0.f6142     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                int r18 = r18 + r14
                r0 = r18
                r1 = r22
                r1.f6142 = r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                int r0 = r0.f6142     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r22
                r1 = r18
                int r18 = r0.m6674(r5, r1)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r18
                r1 = r22
                r1.f6151 = r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                int r0 = r0.f6151     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                if (r18 <= 0) goto L_0x00cb
            L_0x0119:
                r0 = r22
                int r0 = r0.f6151     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r22
                int r0 = r0.f6142     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r19 = r0
                r0 = r18
                r1 = r19
                if (r0 >= r1) goto L_0x014c
                r0 = r22
                java.io.BufferedInputStream r0 = r0.f6153     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r18.reset()     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.io.BufferedInputStream r0 = r0.f6153     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r22
                int r0 = r0.f6151     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r19 = r0
                r0 = r19
                long r0 = (long) r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r20 = r0
                r0 = r18
                r1 = r20
                r0.skip(r1)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x014c:
                java.util.HashMap r18 = new java.util.HashMap     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18.<init>()     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r18
                r1 = r22
                r1.f6148 = r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.util.Map<java.lang.String, java.lang.String> r0 = r0.f6149     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                if (r18 != 0) goto L_0x0270
                java.util.HashMap r18 = new java.util.HashMap     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18.<init>()     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r18
                r1 = r22
                r1.f6149 = r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x016a:
                java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.io.InputStreamReader r18 = new java.io.InputStreamReader     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.io.ByteArrayInputStream r19 = new java.io.ByteArrayInputStream     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r20 = 0
                r0 = r22
                int r0 = r0.f6142     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r21 = r0
                r0 = r19
                r1 = r20
                r2 = r21
                r0.<init>(r5, r1, r2)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18.<init>(r19)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r18
                r8.<init>(r0)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.util.HashMap r11 = new java.util.HashMap     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r11.<init>()     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.util.Map<java.lang.String, java.util.List<java.lang.String>> r0 = r0.f6148     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r22
                java.util.Map<java.lang.String, java.lang.String> r0 = r0.f6149     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r19 = r0
                r0 = r22
                r1 = r18
                r2 = r19
                r0.m6678((java.io.BufferedReader) r8, (java.util.Map<java.lang.String, java.lang.String>) r11, (java.util.Map<java.lang.String, java.util.List<java.lang.String>>) r1, (java.util.Map<java.lang.String, java.lang.String>) r2)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.lang.String r0 = r0.f6145     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                if (r18 == 0) goto L_0x01cf
                r0 = r22
                java.util.Map<java.lang.String, java.lang.String> r0 = r0.f6149     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                java.lang.String r19 = "remote-addr"
                r0 = r22
                java.lang.String r0 = r0.f6145     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r20 = r0
                r18.put(r19, r20)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.util.Map<java.lang.String, java.lang.String> r0 = r0.f6149     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                java.lang.String r19 = "http-client-ip"
                r0 = r22
                java.lang.String r0 = r0.f6145     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r20 = r0
                r18.put(r19, r20)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x01cf:
                java.lang.String r18 = "method"
                r0 = r18
                java.lang.Object r18 = r11.get(r0)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r18 = (java.lang.String) r18     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                fi.iki.elonen.NanoHTTPD$Method r18 = fi.iki.elonen.NanoHTTPD.Method.m18876(r18)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r18
                r1 = r22
                r1.f6144 = r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                fi.iki.elonen.NanoHTTPD$Method r0 = r0.f6144     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                if (r18 != 0) goto L_0x02ad
                fi.iki.elonen.NanoHTTPD$ResponseException r19 = new fi.iki.elonen.NanoHTTPD$ResponseException     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                fi.iki.elonen.NanoHTTPD$Response$Status r20 = fi.iki.elonen.NanoHTTPD.Response.Status.BAD_REQUEST     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18.<init>()     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r21 = "BAD REQUEST: Syntax error. HTTP verb "
                r0 = r18
                r1 = r21
                java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r18 = "method"
                r0 = r18
                java.lang.Object r18 = r11.get(r0)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r18 = (java.lang.String) r18     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r21
                r1 = r18
                java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r21 = " unhandled."
                r0 = r18
                r1 = r21
                java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r18 = r18.toString()     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r19
                r1 = r20
                r2 = r18
                r0.<init>(r1, r2)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                throw r19     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x022c:
                r9 = move-exception
                fi.iki.elonen.NanoHTTPD$Response$Status r18 = fi.iki.elonen.NanoHTTPD.Response.Status.INTERNAL_ERROR     // Catch:{ all -> 0x005a }
                java.lang.String r19 = "text/plain"
                java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ all -> 0x005a }
                r20.<init>()     // Catch:{ all -> 0x005a }
                java.lang.String r21 = "SERVER INTERNAL ERROR: IOException: "
                java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ all -> 0x005a }
                java.lang.String r21 = r9.getMessage()     // Catch:{ all -> 0x005a }
                java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ all -> 0x005a }
                java.lang.String r20 = r20.toString()     // Catch:{ all -> 0x005a }
                fi.iki.elonen.NanoHTTPD$Response r15 = fi.iki.elonen.NanoHTTPD.m6657(r18, r19, r20)     // Catch:{ all -> 0x005a }
                r0 = r22
                java.io.OutputStream r0 = r0.f6154     // Catch:{ all -> 0x005a }
                r18 = r0
                r0 = r18
                r15.m18887((java.io.OutputStream) r0)     // Catch:{ all -> 0x005a }
                r0 = r22
                java.io.OutputStream r0 = r0.f6154     // Catch:{ all -> 0x005a }
                r18 = r0
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r18)     // Catch:{ all -> 0x005a }
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r12)
                r0 = r22
                fi.iki.elonen.NanoHTTPD$TempFileManager r0 = r0.f6152
                r18 = r0
                r18.m18900()
                goto L_0x00ca
            L_0x0270:
                r0 = r22
                java.util.Map<java.lang.String, java.lang.String> r0 = r0.f6149     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r18.clear()     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                goto L_0x016a
            L_0x027b:
                r13 = move-exception
                fi.iki.elonen.NanoHTTPD$Response$Status r18 = r13.getStatus()     // Catch:{ all -> 0x005a }
                java.lang.String r19 = "text/plain"
                java.lang.String r20 = r13.getMessage()     // Catch:{ all -> 0x005a }
                fi.iki.elonen.NanoHTTPD$Response r15 = fi.iki.elonen.NanoHTTPD.m6657(r18, r19, r20)     // Catch:{ all -> 0x005a }
                r0 = r22
                java.io.OutputStream r0 = r0.f6154     // Catch:{ all -> 0x005a }
                r18 = r0
                r0 = r18
                r15.m18887((java.io.OutputStream) r0)     // Catch:{ all -> 0x005a }
                r0 = r22
                java.io.OutputStream r0 = r0.f6154     // Catch:{ all -> 0x005a }
                r18 = r0
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r18)     // Catch:{ all -> 0x005a }
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r12)
                r0 = r22
                fi.iki.elonen.NanoHTTPD$TempFileManager r0 = r0.f6152
                r18 = r0
                r18.m18900()
                goto L_0x00ca
            L_0x02ad:
                java.lang.String r18 = "uri"
                r0 = r18
                java.lang.Object r18 = r11.get(r0)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r18 = (java.lang.String) r18     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r18
                r1 = r22
                r1.f6143 = r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                fi.iki.elonen.NanoHTTPD$CookieHandler r18 = new fi.iki.elonen.NanoHTTPD$CookieHandler     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                fi.iki.elonen.NanoHTTPD r0 = fi.iki.elonen.NanoHTTPD.this     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r19 = r0
                r0 = r22
                java.util.Map<java.lang.String, java.lang.String> r0 = r0.f6149     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r20 = r0
                r18.<init>(r20)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r18
                r1 = r22
                r1.f6150 = r0     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.util.Map<java.lang.String, java.lang.String> r0 = r0.f6149     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                java.lang.String r19 = "connection"
                java.lang.Object r6 = r18.get(r19)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r6 = (java.lang.String) r6     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r18 = "HTTP/1.1"
                r0 = r22
                java.lang.String r0 = r0.f6156     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r19 = r0
                boolean r18 = r18.equals(r19)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                if (r18 == 0) goto L_0x031c
                if (r6 == 0) goto L_0x0300
                java.lang.String r18 = "(?i).*close.*"
                r0 = r18
                boolean r18 = r6.matches(r0)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                if (r18 != 0) goto L_0x031c
            L_0x0300:
                r10 = 1
            L_0x0301:
                r0 = r22
                fi.iki.elonen.NanoHTTPD r0 = fi.iki.elonen.NanoHTTPD.this     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r18
                r1 = r22
                fi.iki.elonen.NanoHTTPD$Response r12 = r0.m6666((fi.iki.elonen.NanoHTTPD.IHTTPSession) r1)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                if (r12 != 0) goto L_0x031e
                fi.iki.elonen.NanoHTTPD$ResponseException r18 = new fi.iki.elonen.NanoHTTPD$ResponseException     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                fi.iki.elonen.NanoHTTPD$Response$Status r19 = fi.iki.elonen.NanoHTTPD.Response.Status.INTERNAL_ERROR     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r20 = "SERVER INTERNAL ERROR: Serve() returned a null response."
                r18.<init>(r19, r20)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                throw r18     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x031c:
                r10 = 0
                goto L_0x0301
            L_0x031e:
                r0 = r22
                java.util.Map<java.lang.String, java.lang.String> r0 = r0.f6149     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                java.lang.String r19 = "accept-encoding"
                java.lang.Object r4 = r18.get(r19)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r4 = (java.lang.String) r4     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                fi.iki.elonen.NanoHTTPD$CookieHandler r0 = r0.f6150     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r18
                r0.m18866(r12)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                fi.iki.elonen.NanoHTTPD$Method r0 = r0.f6144     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r18
                r12.m18886((fi.iki.elonen.NanoHTTPD.Method) r0)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                fi.iki.elonen.NanoHTTPD r0 = fi.iki.elonen.NanoHTTPD.this     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r18
                boolean r18 = r0.m6672((fi.iki.elonen.NanoHTTPD.Response) r12)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                if (r18 == 0) goto L_0x0384
                if (r4 == 0) goto L_0x0384
                java.lang.String r18 = "gzip"
                r0 = r18
                boolean r18 = r4.contains(r0)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                if (r18 == 0) goto L_0x0384
                r18 = 1
            L_0x0360:
                r0 = r18
                r12.m18890((boolean) r0)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r12.m18882(r10)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r0 = r22
                java.io.OutputStream r0 = r0.f6154     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                r18 = r0
                r0 = r18
                r12.m18887((java.io.OutputStream) r0)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                if (r10 == 0) goto L_0x037b
                boolean r18 = r12.m18891()     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                if (r18 == 0) goto L_0x0387
            L_0x037b:
                java.net.SocketException r18 = new java.net.SocketException     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                java.lang.String r19 = "NanoHttpd Shutdown"
                r18.<init>(r19)     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
                throw r18     // Catch:{ SocketException -> 0x0058, SocketTimeoutException -> 0x006a, SSLException -> 0x0088, IOException -> 0x022c, ResponseException -> 0x027b }
            L_0x0384:
                r18 = 0
                goto L_0x0360
            L_0x0387:
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r12)
                r0 = r22
                fi.iki.elonen.NanoHTTPD$TempFileManager r0 = r0.f6152
                r18 = r0
                r18.m18900()
                goto L_0x00ca
            */
            throw new UnsupportedOperationException("Method not decompiled: fi.iki.elonen.NanoHTTPD.HTTPSession.m6687():void");
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v0, resolved type: java.io.RandomAccessFile} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v1, resolved type: java.io.RandomAccessFile} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v2, resolved type: java.io.RandomAccessFile} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v1, resolved type: java.io.DataOutputStream} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v3, resolved type: java.io.RandomAccessFile} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: java.io.DataOutputStream} */
        /* JADX WARNING: type inference failed for: r16v2 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m6688(java.util.Map<java.lang.String, java.lang.String> r21) throws java.io.IOException, fi.iki.elonen.NanoHTTPD.ResponseException {
            /*
                r20 = this;
                r15 = 0
                long r18 = r20.m6682()     // Catch:{ all -> 0x0058 }
                r8 = 0
                r16 = 0
                r2 = 1024(0x400, double:5.06E-321)
                int r2 = (r18 > r2 ? 1 : (r18 == r2 ? 0 : -1))
                if (r2 >= 0) goto L_0x005d
                java.io.ByteArrayOutputStream r8 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0058 }
                r8.<init>()     // Catch:{ all -> 0x0058 }
                java.io.DataOutputStream r16 = new java.io.DataOutputStream     // Catch:{ all -> 0x0058 }
                r0 = r16
                r0.<init>(r8)     // Catch:{ all -> 0x0058 }
            L_0x001a:
                r2 = 512(0x200, float:7.175E-43)
                byte[] r10 = new byte[r2]     // Catch:{ all -> 0x0058 }
            L_0x001e:
                r0 = r20
                int r2 = r0.f6142     // Catch:{ all -> 0x0058 }
                if (r2 < 0) goto L_0x0064
                r2 = 0
                int r2 = (r18 > r2 ? 1 : (r18 == r2 ? 0 : -1))
                if (r2 <= 0) goto L_0x0064
                r0 = r20
                java.io.BufferedInputStream r2 = r0.f6153     // Catch:{ all -> 0x0058 }
                r3 = 0
                r4 = 512(0x200, double:2.53E-321)
                r0 = r18
                long r4 = java.lang.Math.min(r0, r4)     // Catch:{ all -> 0x0058 }
                int r4 = (int) r4     // Catch:{ all -> 0x0058 }
                int r2 = r2.read(r10, r3, r4)     // Catch:{ all -> 0x0058 }
                r0 = r20
                r0.f6142 = r2     // Catch:{ all -> 0x0058 }
                r0 = r20
                int r2 = r0.f6142     // Catch:{ all -> 0x0058 }
                long r2 = (long) r2     // Catch:{ all -> 0x0058 }
                long r18 = r18 - r2
                r0 = r20
                int r2 = r0.f6142     // Catch:{ all -> 0x0058 }
                if (r2 <= 0) goto L_0x001e
                r2 = 0
                r0 = r20
                int r3 = r0.f6142     // Catch:{ all -> 0x0058 }
                r0 = r16
                r0.write(r10, r2, r3)     // Catch:{ all -> 0x0058 }
                goto L_0x001e
            L_0x0058:
                r2 = move-exception
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r15)
                throw r2
            L_0x005d:
                java.io.RandomAccessFile r15 = r20.m6673()     // Catch:{ all -> 0x0058 }
                r16 = r15
                goto L_0x001a
            L_0x0064:
                r12 = 0
                if (r8 == 0) goto L_0x00a9
                byte[] r2 = r8.toByteArray()     // Catch:{ all -> 0x0058 }
                r3 = 0
                int r4 = r8.size()     // Catch:{ all -> 0x0058 }
                java.nio.ByteBuffer r12 = java.nio.ByteBuffer.wrap(r2, r3, r4)     // Catch:{ all -> 0x0058 }
            L_0x0074:
                fi.iki.elonen.NanoHTTPD$Method r2 = fi.iki.elonen.NanoHTTPD.Method.POST     // Catch:{ all -> 0x0058 }
                r0 = r20
                fi.iki.elonen.NanoHTTPD$Method r3 = r0.f6144     // Catch:{ all -> 0x0058 }
                boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x0058 }
                if (r2 == 0) goto L_0x010a
                fi.iki.elonen.NanoHTTPD$ContentType r11 = new fi.iki.elonen.NanoHTTPD$ContentType     // Catch:{ all -> 0x0058 }
                r0 = r20
                java.util.Map<java.lang.String, java.lang.String> r2 = r0.f6149     // Catch:{ all -> 0x0058 }
                java.lang.String r3 = "content-type"
                java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x0058 }
                java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0058 }
                r11.<init>(r2)     // Catch:{ all -> 0x0058 }
                boolean r2 = r11.m18860()     // Catch:{ all -> 0x0058 }
                if (r2 == 0) goto L_0x00ce
                java.lang.String r9 = r11.m18862()     // Catch:{ all -> 0x0058 }
                if (r9 != 0) goto L_0x00bf
                fi.iki.elonen.NanoHTTPD$ResponseException r2 = new fi.iki.elonen.NanoHTTPD$ResponseException     // Catch:{ all -> 0x0058 }
                fi.iki.elonen.NanoHTTPD$Response$Status r3 = fi.iki.elonen.NanoHTTPD.Response.Status.BAD_REQUEST     // Catch:{ all -> 0x0058 }
                java.lang.String r4 = "BAD REQUEST: Content type is multipart/form-data but boundary missing. Usage: GET /example/file.html"
                r2.<init>(r3, r4)     // Catch:{ all -> 0x0058 }
                throw r2     // Catch:{ all -> 0x0058 }
            L_0x00a9:
                java.nio.channels.FileChannel r2 = r15.getChannel()     // Catch:{ all -> 0x0058 }
                java.nio.channels.FileChannel$MapMode r3 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ all -> 0x0058 }
                r4 = 0
                long r6 = r15.length()     // Catch:{ all -> 0x0058 }
                java.nio.MappedByteBuffer r12 = r2.map(r3, r4, r6)     // Catch:{ all -> 0x0058 }
                r2 = 0
                r15.seek(r2)     // Catch:{ all -> 0x0058 }
                goto L_0x0074
            L_0x00bf:
                r0 = r20
                java.util.Map<java.lang.String, java.util.List<java.lang.String>> r2 = r0.f6148     // Catch:{ all -> 0x0058 }
                r0 = r20
                r1 = r21
                r0.m6677((fi.iki.elonen.NanoHTTPD.ContentType) r11, (java.nio.ByteBuffer) r12, (java.util.Map<java.lang.String, java.util.List<java.lang.String>>) r2, (java.util.Map<java.lang.String, java.lang.String>) r1)     // Catch:{ all -> 0x0058 }
            L_0x00ca:
                fi.iki.elonen.NanoHTTPD.m6653((java.lang.Object) r15)
                return
            L_0x00ce:
                int r2 = r12.remaining()     // Catch:{ all -> 0x0058 }
                byte[] r13 = new byte[r2]     // Catch:{ all -> 0x0058 }
                r12.get(r13)     // Catch:{ all -> 0x0058 }
                java.lang.String r2 = new java.lang.String     // Catch:{ all -> 0x0058 }
                java.lang.String r3 = r11.m18863()     // Catch:{ all -> 0x0058 }
                r2.<init>(r13, r3)     // Catch:{ all -> 0x0058 }
                java.lang.String r14 = r2.trim()     // Catch:{ all -> 0x0058 }
                java.lang.String r2 = "application/x-www-form-urlencoded"
                java.lang.String r3 = r11.m18861()     // Catch:{ all -> 0x0058 }
                boolean r2 = r2.equalsIgnoreCase(r3)     // Catch:{ all -> 0x0058 }
                if (r2 == 0) goto L_0x00fb
                r0 = r20
                java.util.Map<java.lang.String, java.util.List<java.lang.String>> r2 = r0.f6148     // Catch:{ all -> 0x0058 }
                r0 = r20
                r0.m6679((java.lang.String) r14, (java.util.Map<java.lang.String, java.util.List<java.lang.String>>) r2)     // Catch:{ all -> 0x0058 }
                goto L_0x00ca
            L_0x00fb:
                int r2 = r14.length()     // Catch:{ all -> 0x0058 }
                if (r2 == 0) goto L_0x00ca
                java.lang.String r2 = "postData"
                r0 = r21
                r0.put(r2, r14)     // Catch:{ all -> 0x0058 }
                goto L_0x00ca
            L_0x010a:
                fi.iki.elonen.NanoHTTPD$Method r2 = fi.iki.elonen.NanoHTTPD.Method.PUT     // Catch:{ all -> 0x0058 }
                r0 = r20
                fi.iki.elonen.NanoHTTPD$Method r3 = r0.f6144     // Catch:{ all -> 0x0058 }
                boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x0058 }
                if (r2 == 0) goto L_0x00ca
                java.lang.String r2 = "content"
                r3 = 0
                int r4 = r12.limit()     // Catch:{ all -> 0x0058 }
                r5 = 0
                r0 = r20
                java.lang.String r3 = r0.m6676((java.nio.ByteBuffer) r12, (int) r3, (int) r4, (java.lang.String) r5)     // Catch:{ all -> 0x0058 }
                r0 = r21
                r0.put(r2, r3)     // Catch:{ all -> 0x0058 }
                goto L_0x00ca
            */
            throw new UnsupportedOperationException("Method not decompiled: fi.iki.elonen.NanoHTTPD.HTTPSession.m6688(java.util.Map):void");
        }
    }

    public interface IHTTPSession {
        /* renamed from: ʻ  reason: contains not printable characters */
        String m6689();

        /* renamed from: 连任  reason: contains not printable characters */
        String m6690();

        /* renamed from: 靐  reason: contains not printable characters */
        Map<String, String> m6691();

        @Deprecated
        /* renamed from: 麤  reason: contains not printable characters */
        Map<String, String> m6692();

        /* renamed from: 齉  reason: contains not printable characters */
        Method m6693();

        /* renamed from: 龘  reason: contains not printable characters */
        void m6694(Map<String, String> map) throws IOException, ResponseException;
    }

    public interface AsyncRunner {
        /* renamed from: 靐  reason: contains not printable characters */
        void m18854(ClientHandler clientHandler);

        /* renamed from: 龘  reason: contains not printable characters */
        void m18855();

        /* renamed from: 龘  reason: contains not printable characters */
        void m18856(ClientHandler clientHandler);
    }

    public class ClientHandler implements Runnable {

        /* renamed from: 靐  reason: contains not printable characters */
        private final InputStream f15097;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Socket f15098;

        public ClientHandler(InputStream inputStream, Socket socket) {
            this.f15097 = inputStream;
            this.f15098 = socket;
        }

        public void run() {
            OutputStream outputStream = null;
            try {
                outputStream = this.f15098.getOutputStream();
                HTTPSession hTTPSession = new HTTPSession(NanoHTTPD.this.f6140.m18901(), this.f15097, outputStream, this.f15098.getInetAddress());
                while (!this.f15098.isClosed()) {
                    hTTPSession.m6687();
                }
            } catch (Exception e) {
                if ((!(e instanceof SocketException) || !"NanoHttpd Shutdown".equals(e.getMessage())) && !(e instanceof SocketTimeoutException)) {
                    NanoHTTPD.f6131.log(Level.SEVERE, "Communication with the client broken, or an bug in the handler code", e);
                }
            } finally {
                NanoHTTPD.m6653(outputStream);
                NanoHTTPD.m6653((Object) this.f15097);
                NanoHTTPD.m6653((Object) this.f15098);
                NanoHTTPD.this.f6141.m18856(this);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18857() {
            NanoHTTPD.m6653((Object) this.f15097);
            NanoHTTPD.m6653((Object) this.f15098);
        }
    }

    protected static class ContentType {

        /* renamed from: 靐  reason: contains not printable characters */
        private static final Pattern f15100 = Pattern.compile("[ |\t]*(charset)[ |\t]*=[ |\t]*['|\"]?([^\"^'^;^,]*)['|\"]?", 2);

        /* renamed from: 齉  reason: contains not printable characters */
        private static final Pattern f15101 = Pattern.compile("[ |\t]*(boundary)[ |\t]*=[ |\t]*['|\"]?([^\"^'^;^,]*)['|\"]?", 2);

        /* renamed from: 龘  reason: contains not printable characters */
        private static final Pattern f15102 = Pattern.compile("[ |\t]*([^/^ ^;^,]+/[^ ^;^,]+)", 2);

        /* renamed from: ʻ  reason: contains not printable characters */
        private final String f15103;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final String f15104;

        /* renamed from: 连任  reason: contains not printable characters */
        private final String f15105;

        /* renamed from: 麤  reason: contains not printable characters */
        private final String f15106;

        public ContentType(String str) {
            this.f15106 = str;
            if (str != null) {
                this.f15105 = m18858(str, f15102, "", 1);
                this.f15103 = m18858(str, f15100, (String) null, 2);
            } else {
                this.f15105 = "";
                this.f15103 = "UTF-8";
            }
            if ("multipart/form-data".equalsIgnoreCase(this.f15105)) {
                this.f15104 = m18858(str, f15101, (String) null, 2);
            } else {
                this.f15104 = null;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m18858(String str, Pattern pattern, String str2, int i) {
            Matcher matcher = pattern.matcher(str);
            return matcher.find() ? matcher.group(i) : str2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public ContentType m18859() {
            return this.f15103 == null ? new ContentType(this.f15106 + "; charset=UTF-8") : this;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public boolean m18860() {
            return "multipart/form-data".equalsIgnoreCase(this.f15105);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m18861() {
            return this.f15105;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public String m18862() {
            return this.f15104;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public String m18863() {
            return this.f15103 == null ? "US-ASCII" : this.f15103;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m18864() {
            return this.f15106;
        }
    }

    public static class Cookie {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f15107;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String f15108;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f15109;

        /* renamed from: 龘  reason: contains not printable characters */
        public String m18865() {
            return String.format("%s=%s; expires=%s", new Object[]{this.f15109, this.f15107, this.f15108});
        }
    }

    public class CookieHandler implements Iterable<String> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final HashMap<String, String> f15110 = new HashMap<>();

        /* renamed from: 齉  reason: contains not printable characters */
        private final ArrayList<Cookie> f15111 = new ArrayList<>();

        public CookieHandler(Map<String, String> map) {
            String str = map.get("cookie");
            if (str != null) {
                for (String trim : str.split(";")) {
                    String[] split = trim.trim().split("=");
                    if (split.length == 2) {
                        this.f15110.put(split[0], split[1]);
                    }
                }
            }
        }

        public Iterator<String> iterator() {
            return this.f15110.keySet().iterator();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18866(Response response) {
            Iterator<Cookie> it2 = this.f15111.iterator();
            while (it2.hasNext()) {
                response.m18889("Set-Cookie", it2.next().m18865());
            }
        }
    }

    public static class DefaultAsyncRunner implements AsyncRunner {

        /* renamed from: 靐  reason: contains not printable characters */
        private final List<ClientHandler> f15113 = Collections.synchronizedList(new ArrayList());

        /* renamed from: 龘  reason: contains not printable characters */
        private long f15114;

        /* renamed from: 靐  reason: contains not printable characters */
        public void m18867(ClientHandler clientHandler) {
            this.f15114++;
            Thread thread = new Thread(clientHandler);
            thread.setDaemon(true);
            thread.setName("NanoHttpd Request Processor (#" + this.f15114 + ")");
            this.f15113.add(clientHandler);
            thread.start();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18868() {
            Iterator it2 = new ArrayList(this.f15113).iterator();
            while (it2.hasNext()) {
                ((ClientHandler) it2.next()).m18857();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18869(ClientHandler clientHandler) {
            this.f15113.remove(clientHandler);
        }
    }

    public static class DefaultServerSocketFactory implements ServerSocketFactory {
        /* renamed from: 龘  reason: contains not printable characters */
        public ServerSocket m18870() throws IOException {
            return new ServerSocket();
        }
    }

    public static class DefaultTempFile implements TempFile {

        /* renamed from: 靐  reason: contains not printable characters */
        private final OutputStream f15115 = new FileOutputStream(this.f15116);

        /* renamed from: 龘  reason: contains not printable characters */
        private final File f15116;

        public DefaultTempFile(File file) throws IOException {
            this.f15116 = File.createTempFile("NanoHTTPD-", "", file);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m18871() {
            return this.f15116.getAbsolutePath();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18872() throws Exception {
            NanoHTTPD.m6653((Object) this.f15115);
            if (!this.f15116.delete()) {
                throw new Exception("could not delete temporary file: " + this.f15116.getAbsolutePath());
            }
        }
    }

    public static class DefaultTempFileManager implements TempFileManager {

        /* renamed from: 靐  reason: contains not printable characters */
        private final List<TempFile> f15117;

        /* renamed from: 龘  reason: contains not printable characters */
        private final File f15118 = new File(System.getProperty("java.io.tmpdir"));

        public DefaultTempFileManager() {
            if (!this.f15118.exists()) {
                this.f15118.mkdirs();
            }
            this.f15117 = new ArrayList();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public TempFile m18873(String str) throws Exception {
            DefaultTempFile defaultTempFile = new DefaultTempFile(this.f15118);
            this.f15117.add(defaultTempFile);
            return defaultTempFile;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18874() {
            for (TempFile r0 : this.f15117) {
                try {
                    r0.m18898();
                } catch (Exception e) {
                    NanoHTTPD.f6131.log(Level.WARNING, "could not delete file ", e);
                }
            }
            this.f15117.clear();
        }
    }

    private class DefaultTempFileManagerFactory implements TempFileManagerFactory {
        private DefaultTempFileManagerFactory() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public TempFileManager m18875() {
            return new DefaultTempFileManager();
        }
    }

    public enum Method {
        GET,
        PUT,
        POST,
        DELETE,
        HEAD,
        OPTIONS,
        TRACE,
        CONNECT,
        PATCH,
        PROPFIND,
        PROPPATCH,
        MKCOL,
        MOVE,
        COPY,
        LOCK,
        UNLOCK;

        /* renamed from: 龘  reason: contains not printable characters */
        static Method m18876(String str) {
            if (str == null) {
                return null;
            }
            try {
                return valueOf(str);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
    }

    public static class Response implements Closeable {
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public final Map<String, String> f15121 = new HashMap();

        /* renamed from: ʼ  reason: contains not printable characters */
        private Method f15122;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f15123;

        /* renamed from: ˑ  reason: contains not printable characters */
        private boolean f15124;

        /* renamed from: ٴ  reason: contains not printable characters */
        private boolean f15125;

        /* renamed from: 连任  reason: contains not printable characters */
        private final Map<String, String> f15126 = new HashMap<String, String>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public String put(String str, String str2) {
                Response.this.f15121.put(str == null ? str : str.toLowerCase(), str2);
                return (String) super.put(str, str2);
            }
        };

        /* renamed from: 靐  reason: contains not printable characters */
        private String f15127;

        /* renamed from: 麤  reason: contains not printable characters */
        private long f15128;

        /* renamed from: 齉  reason: contains not printable characters */
        private InputStream f15129;

        /* renamed from: 龘  reason: contains not printable characters */
        private IStatus f15130;

        public enum Status implements IStatus {
            SWITCH_PROTOCOL(101, "Switching Protocols"),
            OK(200, "OK"),
            CREATED(201, "Created"),
            ACCEPTED(202, "Accepted"),
            NO_CONTENT(204, "No Content"),
            PARTIAL_CONTENT(206, "Partial Content"),
            MULTI_STATUS(207, "Multi-Status"),
            REDIRECT(301, "Moved Permanently"),
            FOUND(302, "Found"),
            REDIRECT_SEE_OTHER(303, "See Other"),
            NOT_MODIFIED(304, "Not Modified"),
            TEMPORARY_REDIRECT(307, "Temporary Redirect"),
            BAD_REQUEST(400, "Bad Request"),
            UNAUTHORIZED(401, "Unauthorized"),
            FORBIDDEN(403, "Forbidden"),
            NOT_FOUND(404, "Not Found"),
            METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
            NOT_ACCEPTABLE(406, "Not Acceptable"),
            REQUEST_TIMEOUT(408, "Request Timeout"),
            CONFLICT(409, "Conflict"),
            GONE(410, "Gone"),
            LENGTH_REQUIRED(411, "Length Required"),
            PRECONDITION_FAILED(412, "Precondition Failed"),
            PAYLOAD_TOO_LARGE(413, "Payload Too Large"),
            UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),
            RANGE_NOT_SATISFIABLE(416, "Requested Range Not Satisfiable"),
            EXPECTATION_FAILED(417, "Expectation Failed"),
            TOO_MANY_REQUESTS(429, "Too Many Requests"),
            INTERNAL_ERROR(500, "Internal Server Error"),
            NOT_IMPLEMENTED(501, "Not Implemented"),
            SERVICE_UNAVAILABLE(503, "Service Unavailable"),
            UNSUPPORTED_HTTP_VERSION(505, "HTTP Version Not Supported");
            
            private final String description;
            private final int requestStatus;

            private Status(int i, String str) {
                this.requestStatus = i;
                this.description = str;
            }

            public static Status lookup(int i) {
                for (Status status : values()) {
                    if (status.getRequestStatus() == i) {
                        return status;
                    }
                }
                return null;
            }

            public String getDescription() {
                return "" + this.requestStatus + StringUtils.SPACE + this.description;
            }

            public int getRequestStatus() {
                return this.requestStatus;
            }
        }

        private static class ChunkedOutputStream extends FilterOutputStream {
            public ChunkedOutputStream(OutputStream outputStream) {
                super(outputStream);
            }

            public void write(int i) throws IOException {
                write(new byte[]{(byte) i}, 0, 1);
            }

            public void write(byte[] bArr) throws IOException {
                write(bArr, 0, bArr.length);
            }

            public void write(byte[] bArr, int i, int i2) throws IOException {
                if (i2 != 0) {
                    this.out.write(String.format("%x\r\n", new Object[]{Integer.valueOf(i2)}).getBytes());
                    this.out.write(bArr, i, i2);
                    this.out.write("\r\n".getBytes());
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m18893() throws IOException {
                this.out.write("0\r\n\r\n".getBytes());
            }
        }

        public interface IStatus {
            String getDescription();
        }

        protected Response(IStatus iStatus, String str, InputStream inputStream, long j) {
            boolean z = false;
            this.f15130 = iStatus;
            this.f15127 = str;
            if (inputStream == null) {
                this.f15129 = new ByteArrayInputStream(new byte[0]);
                this.f15128 = 0;
            } else {
                this.f15129 = inputStream;
                this.f15128 = j;
            }
            this.f15123 = this.f15128 < 0 ? true : z;
            this.f15125 = true;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m18877(OutputStream outputStream, long j) throws IOException {
            if (this.f15124) {
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
                m18878(gZIPOutputStream, -1);
                gZIPOutputStream.finish();
                return;
            }
            m18878(outputStream, j);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private void m18878(OutputStream outputStream, long j) throws IOException {
            byte[] bArr = new byte[((int) 16384)];
            boolean z = j == -1;
            while (true) {
                if (j > 0 || z) {
                    int read = this.f15129.read(bArr, 0, (int) (z ? 16384 : Math.min(j, 16384)));
                    if (read > 0) {
                        outputStream.write(bArr, 0, read);
                        if (!z) {
                            j -= (long) read;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m18880(OutputStream outputStream, long j) throws IOException {
            if (this.f15122 == Method.HEAD || !this.f15123) {
                m18877(outputStream, j);
                return;
            }
            ChunkedOutputStream chunkedOutputStream = new ChunkedOutputStream(outputStream);
            m18877(chunkedOutputStream, -1);
            chunkedOutputStream.m18893();
        }

        public void close() throws IOException {
            if (this.f15129 != null) {
                this.f15129.close();
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m18881() {
            return this.f15127;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m18882(boolean z) {
            this.f15125 = z;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m18883(boolean z) {
            this.f15123 = z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m18884(PrintWriter printWriter, long j) {
            String r0 = m18885("content-length");
            long j2 = j;
            if (r0 != null) {
                try {
                    j2 = Long.parseLong(r0);
                } catch (NumberFormatException e) {
                    NanoHTTPD.f6131.severe("content-length was no number " + r0);
                }
            }
            printWriter.print("Content-Length: " + j2 + "\r\n");
            return j2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m18885(String str) {
            return this.f15121.get(str.toLowerCase());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18886(Method method) {
            this.f15122 = method;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18887(OutputStream outputStream) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone(TimeZones.GMT_ID));
            try {
                if (this.f15130 == null) {
                    throw new Error("sendResponse(): Status can't be null.");
                }
                PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream, new ContentType(this.f15127).m18863())), false);
                printWriter.append("HTTP/1.1 ").append(this.f15130.getDescription()).append(" \r\n");
                if (this.f15127 != null) {
                    m18888(printWriter, OAuth.HeaderType.CONTENT_TYPE, this.f15127);
                }
                if (m18885("date") == null) {
                    m18888(printWriter, "Date", simpleDateFormat.format(new Date()));
                }
                for (Map.Entry next : this.f15126.entrySet()) {
                    m18888(printWriter, (String) next.getKey(), (String) next.getValue());
                }
                if (m18885("connection") == null) {
                    m18888(printWriter, "Connection", this.f15125 ? "keep-alive" : "close");
                }
                if (m18885("content-length") != null) {
                    this.f15124 = false;
                }
                if (this.f15124) {
                    m18888(printWriter, "Content-Encoding", "gzip");
                    m18883(true);
                }
                long j = this.f15129 != null ? this.f15128 : 0;
                if (this.f15122 != Method.HEAD && this.f15123) {
                    m18888(printWriter, "Transfer-Encoding", "chunked");
                } else if (!this.f15124) {
                    j = m18884(printWriter, j);
                }
                printWriter.append("\r\n");
                printWriter.flush();
                m18880(outputStream, j);
                outputStream.flush();
                NanoHTTPD.m6653((Object) this.f15129);
            } catch (IOException e) {
                NanoHTTPD.f6131.log(Level.SEVERE, "Could not send response to the client", e);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18888(PrintWriter printWriter, String str, String str2) {
            printWriter.append(str).append(": ").append(str2).append("\r\n");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18889(String str, String str2) {
            this.f15126.put(str, str2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18890(boolean z) {
            this.f15124 = z;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m18891() {
            return "close".equals(m18885("connection"));
        }
    }

    public static final class ResponseException extends Exception {
        private static final long serialVersionUID = 6569838532917408380L;
        private final Response.Status status;

        public ResponseException(Response.Status status2, String str) {
            super(str);
            this.status = status2;
        }

        public ResponseException(Response.Status status2, String str, Exception exc) {
            super(str, exc);
            this.status = status2;
        }

        public Response.Status getStatus() {
            return this.status;
        }
    }

    public class ServerRunnable implements Runnable {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f15131;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean f15132 = false;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public IOException f15133;

        public ServerRunnable(int i) {
            this.f15131 = i;
        }

        public void run() {
            try {
                NanoHTTPD.this.f6137.bind(NanoHTTPD.this.f6135 != null ? new InetSocketAddress(NanoHTTPD.this.f6135, NanoHTTPD.this.f6136) : new InetSocketAddress(NanoHTTPD.this.f6136));
                this.f15132 = true;
                do {
                    try {
                        Socket accept = NanoHTTPD.this.f6137.accept();
                        if (this.f15131 > 0) {
                            accept.setSoTimeout(this.f15131);
                        }
                        NanoHTTPD.this.f6141.m18854(NanoHTTPD.this.m6665(accept, accept.getInputStream()));
                    } catch (IOException e) {
                        NanoHTTPD.f6131.log(Level.FINE, "Communication with the client broken", e);
                    }
                } while (!NanoHTTPD.this.f6137.isClosed());
            } catch (IOException e2) {
                this.f15133 = e2;
            }
        }
    }

    public interface ServerSocketFactory {
        /* renamed from: 龘  reason: contains not printable characters */
        ServerSocket m18896() throws IOException;
    }

    public interface TempFile {
        /* renamed from: 靐  reason: contains not printable characters */
        String m18897();

        /* renamed from: 龘  reason: contains not printable characters */
        void m18898() throws Exception;
    }

    public interface TempFileManager {
        /* renamed from: 龘  reason: contains not printable characters */
        TempFile m18899(String str) throws Exception;

        /* renamed from: 龘  reason: contains not printable characters */
        void m18900();
    }

    public interface TempFileManagerFactory {
        /* renamed from: 龘  reason: contains not printable characters */
        TempFileManager m18901();
    }

    public NanoHTTPD(int i) {
        this((String) null, i);
    }

    public NanoHTTPD(String str, int i) {
        this.f6138 = new DefaultServerSocketFactory();
        this.f6135 = str;
        this.f6136 = i;
        m6671((TempFileManagerFactory) new DefaultTempFileManagerFactory());
        m6670((AsyncRunner) new DefaultAsyncRunner());
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static final void m6653(Object obj) {
        if (obj != null) {
            try {
                if (obj instanceof Closeable) {
                    ((Closeable) obj).close();
                } else if (obj instanceof Socket) {
                    ((Socket) obj).close();
                } else if (obj instanceof ServerSocket) {
                    ((ServerSocket) obj).close();
                } else {
                    throw new IllegalArgumentException("Unknown object to close");
                }
            } catch (IOException e) {
                f6131.log(Level.SEVERE, "Could not close", e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Response m6656(Response.IStatus iStatus, String str, InputStream inputStream, long j) {
        return new Response(iStatus, str, inputStream, j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Response m6657(Response.IStatus iStatus, String str, String str2) {
        byte[] bArr;
        ContentType contentType = new ContentType(str);
        if (str2 == null) {
            return m6656(iStatus, str, new ByteArrayInputStream(new byte[0]), 0);
        }
        try {
            if (!Charset.forName(contentType.m18863()).newEncoder().canEncode(str2)) {
                contentType = contentType.m18859();
            }
            bArr = str2.getBytes(contentType.m18863());
        } catch (UnsupportedEncodingException e) {
            f6131.log(Level.SEVERE, "encoding problem, responding nothing", e);
            bArr = new byte[0];
        }
        return m6656(iStatus, contentType.m18864(), new ByteArrayInputStream(bArr), (long) bArr.length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static String m6659(String str) {
        try {
            return URLDecoder.decode(str, InternalZipTyphoonApp.CHARSET_UTF8);
        } catch (UnsupportedEncodingException e) {
            f6131.log(Level.WARNING, "Encoding not supported, ignored", e);
            return null;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m6661() {
        return (this.f6137 == null || this.f6139 == null) ? false : true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m6662() {
        return m6661() && !this.f6137.isClosed() && this.f6139.isAlive();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m6663() {
        try {
            m6653((Object) this.f6137);
            this.f6141.m18855();
            if (this.f6139 != null) {
                this.f6139.join();
            }
        } catch (Exception e) {
            f6131.log(Level.SEVERE, "Could not stop all connections", e);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public ServerSocketFactory m6664() {
        return this.f6138;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public ClientHandler m6665(Socket socket, InputStream inputStream) {
        return new ClientHandler(inputStream, socket);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Response m6666(IHTTPSession iHTTPSession) {
        HashMap hashMap = new HashMap();
        Method r2 = iHTTPSession.m6693();
        if (Method.PUT.equals(r2) || Method.POST.equals(r2)) {
            try {
                iHTTPSession.m6694(hashMap);
            } catch (IOException e) {
                return m6657(Response.Status.INTERNAL_ERROR, "text/plain", "SERVER INTERNAL ERROR: IOException: " + e.getMessage());
            } catch (ResponseException e2) {
                return m6657(e2.getStatus(), "text/plain", e2.getMessage());
            }
        }
        Map<String, String> r4 = iHTTPSession.m6692();
        r4.put("NanoHttpd.QUERY_STRING", iHTTPSession.m6690());
        return m6667(iHTTPSession.m6689(), r2, iHTTPSession.m6691(), r4, hashMap);
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public Response m6667(String str, Method method, Map<String, String> map, Map<String, String> map2, Map<String, String> map3) {
        return m6657(Response.Status.NOT_FOUND, "text/plain", "Not Found");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public ServerRunnable m6668(int i) {
        return new ServerRunnable(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6669(int i, boolean z) throws IOException {
        this.f6137 = m6664().m18896();
        this.f6137.setReuseAddress(true);
        ServerRunnable r0 = m6668(i);
        this.f6139 = new Thread(r0);
        this.f6139.setDaemon(z);
        this.f6139.setName("NanoHttpd Main Listener");
        this.f6139.start();
        while (!r0.f15132 && r0.f15133 == null) {
            try {
                Thread.sleep(10);
            } catch (Throwable th) {
            }
        }
        if (r0.f15133 != null) {
            throw r0.f15133;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6670(AsyncRunner asyncRunner) {
        this.f6141 = asyncRunner;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6671(TempFileManagerFactory tempFileManagerFactory) {
        this.f6140 = tempFileManagerFactory;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6672(Response response) {
        return response.m18881() != null && (response.m18881().toLowerCase().contains("text/") || response.m18881().toLowerCase().contains("/json"));
    }
}
