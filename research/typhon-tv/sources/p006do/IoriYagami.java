package p006do;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLSocket;
import p006do.p007do.GoroDaimon;

/* renamed from: do.IoriYagami  reason: invalid package */
public final class IoriYagami {

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final LuckyGlauber[] f5969 = {LuckyGlauber.f14637, LuckyGlauber.f14643, LuckyGlauber.f14638, LuckyGlauber.f14644, LuckyGlauber.f14650, LuckyGlauber.f14649, LuckyGlauber.f14610, LuckyGlauber.f14622, LuckyGlauber.f14611, LuckyGlauber.f14623, LuckyGlauber.f14592, LuckyGlauber.f14593, LuckyGlauber.f14700, LuckyGlauber.f14642, LuckyGlauber.f14670};

    /* renamed from: 靐  reason: contains not printable characters */
    public static final IoriYagami f5970 = new KyoKusanagi(f5972).m6460(Vanessa.TLS_1_0).m6458(true).m6462();

    /* renamed from: 齉  reason: contains not printable characters */
    public static final IoriYagami f5971 = new KyoKusanagi(false).m6462();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final IoriYagami f5972 = new KyoKusanagi(true).m6459(f5969).m6460(Vanessa.TLS_1_3, Vanessa.TLS_1_2, Vanessa.TLS_1_1, Vanessa.TLS_1_0).m6458(true).m6462();
    @Nullable

    /* renamed from: ʻ  reason: contains not printable characters */
    final String[] f5973;
    @Nullable

    /* renamed from: ʼ  reason: contains not printable characters */
    final String[] f5974;

    /* renamed from: 连任  reason: contains not printable characters */
    final boolean f5975;

    /* renamed from: 麤  reason: contains not printable characters */
    final boolean f5976;

    /* renamed from: do.IoriYagami$KyoKusanagi */
    public static final class KyoKusanagi {
        @Nullable

        /* renamed from: 靐  reason: contains not printable characters */
        String[] f5977;

        /* renamed from: 麤  reason: contains not printable characters */
        boolean f5978;
        @Nullable

        /* renamed from: 齉  reason: contains not printable characters */
        String[] f5979;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f5980;

        public KyoKusanagi(IoriYagami ioriYagami) {
            this.f5980 = ioriYagami.f5976;
            this.f5977 = ioriYagami.f5973;
            this.f5979 = ioriYagami.f5974;
            this.f5978 = ioriYagami.f5975;
        }

        KyoKusanagi(boolean z) {
            this.f5980 = z;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m6457(String... strArr) {
            if (!this.f5980) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one TLS version is required");
            } else {
                this.f5979 = (String[]) strArr.clone();
                return this;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6458(boolean z) {
            if (!this.f5980) {
                throw new IllegalStateException("no TLS extensions for cleartext connections");
            }
            this.f5978 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6459(LuckyGlauber... luckyGlauberArr) {
            if (!this.f5980) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            }
            String[] strArr = new String[luckyGlauberArr.length];
            for (int i = 0; i < luckyGlauberArr.length; i++) {
                strArr[i] = luckyGlauberArr[i].f14701;
            }
            return m6461(strArr);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6460(Vanessa... vanessaArr) {
            if (!this.f5980) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            }
            String[] strArr = new String[vanessaArr.length];
            for (int i = 0; i < vanessaArr.length; i++) {
                strArr[i] = vanessaArr[i].f;
            }
            return m6457(strArr);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6461(String... strArr) {
            if (!this.f5980) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one cipher suite is required");
            } else {
                this.f5977 = (String[]) strArr.clone();
                return this;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public IoriYagami m6462() {
            return new IoriYagami(this);
        }
    }

    IoriYagami(KyoKusanagi kyoKusanagi) {
        this.f5976 = kyoKusanagi.f5980;
        this.f5973 = kyoKusanagi.f5977;
        this.f5974 = kyoKusanagi.f5979;
        this.f5975 = kyoKusanagi.f5978;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private IoriYagami m6450(SSLSocket sSLSocket, boolean z) {
        String[] r0 = this.f5973 != null ? GoroDaimon.m18427((Comparator<? super String>) LuckyGlauber.f14694, sSLSocket.getEnabledCipherSuites(), this.f5973) : sSLSocket.getEnabledCipherSuites();
        String[] r1 = this.f5974 != null ? GoroDaimon.m18427((Comparator<? super String>) GoroDaimon.f14730, sSLSocket.getEnabledProtocols(), this.f5974) : sSLSocket.getEnabledProtocols();
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        int r3 = GoroDaimon.m18413(LuckyGlauber.f14694, supportedCipherSuites, "TLS_FALLBACK_SCSV");
        if (z && r3 != -1) {
            r0 = GoroDaimon.m18428(r0, supportedCipherSuites[r3]);
        }
        return new KyoKusanagi(this).m6461(r0).m6457(r1).m6462();
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof IoriYagami)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        IoriYagami ioriYagami = (IoriYagami) obj;
        if (this.f5976 == ioriYagami.f5976) {
            return !this.f5976 || (Arrays.equals(this.f5973, ioriYagami.f5973) && Arrays.equals(this.f5974, ioriYagami.f5974) && this.f5975 == ioriYagami.f5975);
        }
        return false;
    }

    public int hashCode() {
        if (!this.f5976) {
            return 17;
        }
        return (this.f5975 ? 0 : 1) + ((((Arrays.hashCode(this.f5973) + 527) * 31) + Arrays.hashCode(this.f5974)) * 31);
    }

    public String toString() {
        if (!this.f5976) {
            return "ConnectionSpec()";
        }
        return "ConnectionSpec(cipherSuites=" + (this.f5973 != null ? m6451().toString() : "[all enabled]") + ", tlsVersions=" + (this.f5974 != null ? m6453().toString() : "[all enabled]") + ", supportsTlsExtensions=" + this.f5975 + ")";
    }

    @Nullable
    /* renamed from: 靐  reason: contains not printable characters */
    public List<LuckyGlauber> m6451() {
        if (this.f5973 != null) {
            return LuckyGlauber.m18355(this.f5973);
        }
        return null;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m6452() {
        return this.f5975;
    }

    @Nullable
    /* renamed from: 齉  reason: contains not printable characters */
    public List<Vanessa> m6453() {
        if (this.f5974 != null) {
            return Vanessa.m18387(this.f5974);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6454(SSLSocket sSLSocket, boolean z) {
        IoriYagami r0 = m6450(sSLSocket, z);
        if (r0.f5974 != null) {
            sSLSocket.setEnabledProtocols(r0.f5974);
        }
        if (r0.f5973 != null) {
            sSLSocket.setEnabledCipherSuites(r0.f5973);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6455() {
        return this.f5976;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6456(SSLSocket sSLSocket) {
        if (!this.f5976) {
            return false;
        }
        if (this.f5974 == null || GoroDaimon.m18406(GoroDaimon.f14730, this.f5974, sSLSocket.getEnabledProtocols())) {
            return this.f5973 == null || GoroDaimon.m18406(LuckyGlauber.f14694, this.f5973, sSLSocket.getEnabledCipherSuites());
        }
        return false;
    }
}
