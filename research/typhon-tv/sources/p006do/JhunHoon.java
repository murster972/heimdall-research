package p006do;

import java.io.Closeable;
import javax.annotation.Nullable;
import p006do.ShingoYabuki;

/* renamed from: do.JhunHoon  reason: invalid package */
public final class JhunHoon implements Closeable {

    /* renamed from: ʻ  reason: contains not printable characters */
    final ShingoYabuki f5981;
    @Nullable

    /* renamed from: ʼ  reason: contains not printable characters */
    final Krizalid f5982;
    @Nullable

    /* renamed from: ʽ  reason: contains not printable characters */
    final JhunHoon f5983;

    /* renamed from: ʾ  reason: contains not printable characters */
    private volatile ChinGentsai f5984;

    /* renamed from: ˈ  reason: contains not printable characters */
    final long f5985;
    @Nullable

    /* renamed from: ˑ  reason: contains not printable characters */
    final JhunHoon f5986;
    @Nullable

    /* renamed from: ٴ  reason: contains not printable characters */
    final JhunHoon f5987;

    /* renamed from: ᐧ  reason: contains not printable characters */
    final long f5988;
    @Nullable

    /* renamed from: 连任  reason: contains not printable characters */
    final Goenitz f5989;

    /* renamed from: 靐  reason: contains not printable characters */
    final KDash f5990;

    /* renamed from: 麤  reason: contains not printable characters */
    final String f5991;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f5992;

    /* renamed from: 龘  reason: contains not printable characters */
    final Whip f5993;

    /* renamed from: do.JhunHoon$KyoKusanagi */
    public static class KyoKusanagi {

        /* renamed from: ʻ  reason: contains not printable characters */
        ShingoYabuki.KyoKusanagi f5994;

        /* renamed from: ʼ  reason: contains not printable characters */
        Krizalid f5995;

        /* renamed from: ʽ  reason: contains not printable characters */
        JhunHoon f5996;

        /* renamed from: ˈ  reason: contains not printable characters */
        long f5997;

        /* renamed from: ˑ  reason: contains not printable characters */
        JhunHoon f5998;

        /* renamed from: ٴ  reason: contains not printable characters */
        JhunHoon f5999;

        /* renamed from: ᐧ  reason: contains not printable characters */
        long f6000;
        @Nullable

        /* renamed from: 连任  reason: contains not printable characters */
        Goenitz f6001;

        /* renamed from: 靐  reason: contains not printable characters */
        KDash f6002;

        /* renamed from: 麤  reason: contains not printable characters */
        String f6003;

        /* renamed from: 齉  reason: contains not printable characters */
        int f6004;

        /* renamed from: 龘  reason: contains not printable characters */
        Whip f6005;

        public KyoKusanagi() {
            this.f6004 = -1;
            this.f5994 = new ShingoYabuki.KyoKusanagi();
        }

        KyoKusanagi(JhunHoon jhunHoon) {
            this.f6004 = -1;
            this.f6005 = jhunHoon.f5993;
            this.f6002 = jhunHoon.f5990;
            this.f6004 = jhunHoon.f5992;
            this.f6003 = jhunHoon.f5991;
            this.f6001 = jhunHoon.f5989;
            this.f5994 = jhunHoon.f5981.m6582();
            this.f5995 = jhunHoon.f5982;
            this.f5996 = jhunHoon.f5983;
            this.f5998 = jhunHoon.f5986;
            this.f5999 = jhunHoon.f5987;
            this.f6000 = jhunHoon.f5988;
            this.f5997 = jhunHoon.f5985;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m6475(JhunHoon jhunHoon) {
            if (jhunHoon.f5982 != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m6476(String str, JhunHoon jhunHoon) {
            if (jhunHoon.f5982 != null) {
                throw new IllegalArgumentException(str + ".body != null");
            } else if (jhunHoon.f5983 != null) {
                throw new IllegalArgumentException(str + ".networkResponse != null");
            } else if (jhunHoon.f5986 != null) {
                throw new IllegalArgumentException(str + ".cacheResponse != null");
            } else if (jhunHoon.f5987 != null) {
                throw new IllegalArgumentException(str + ".priorResponse != null");
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m6477(long j) {
            this.f5997 = j;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m6478(@Nullable JhunHoon jhunHoon) {
            if (jhunHoon != null) {
                m6476("cacheResponse", jhunHoon);
            }
            this.f5998 = jhunHoon;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public KyoKusanagi m6479(@Nullable JhunHoon jhunHoon) {
            if (jhunHoon != null) {
                m6475(jhunHoon);
            }
            this.f5999 = jhunHoon;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6480(int i) {
            this.f6004 = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6481(long j) {
            this.f6000 = j;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6482(@Nullable Goenitz goenitz) {
            this.f6001 = goenitz;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6483(@Nullable JhunHoon jhunHoon) {
            if (jhunHoon != null) {
                m6476("networkResponse", jhunHoon);
            }
            this.f5996 = jhunHoon;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6484(KDash kDash) {
            this.f6002 = kDash;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6485(@Nullable Krizalid krizalid) {
            this.f5995 = krizalid;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6486(ShingoYabuki shingoYabuki) {
            this.f5994 = shingoYabuki.m6582();
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6487(Whip whip) {
            this.f6005 = whip;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6488(String str) {
            this.f6003 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6489(String str, String str2) {
            this.f5994.m18385(str, str2);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public JhunHoon m6490() {
            if (this.f6005 == null) {
                throw new IllegalStateException("request == null");
            } else if (this.f6002 == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.f6004 < 0) {
                throw new IllegalStateException("code < 0: " + this.f6004);
            } else if (this.f6003 != null) {
                return new JhunHoon(this);
            } else {
                throw new IllegalStateException("message == null");
            }
        }
    }

    JhunHoon(KyoKusanagi kyoKusanagi) {
        this.f5993 = kyoKusanagi.f6005;
        this.f5990 = kyoKusanagi.f6002;
        this.f5992 = kyoKusanagi.f6004;
        this.f5991 = kyoKusanagi.f6003;
        this.f5989 = kyoKusanagi.f6001;
        this.f5981 = kyoKusanagi.f5994.m18386();
        this.f5982 = kyoKusanagi.f5995;
        this.f5983 = kyoKusanagi.f5996;
        this.f5986 = kyoKusanagi.f5998;
        this.f5987 = kyoKusanagi.f5999;
        this.f5988 = kyoKusanagi.f6000;
        this.f5985 = kyoKusanagi.f5997;
    }

    public void close() {
        this.f5982.close();
    }

    public String toString() {
        return "Response{protocol=" + this.f5990 + ", code=" + this.f5992 + ", message=" + this.f5991 + ", url=" + this.f5993.m6592() + '}';
    }

    @Nullable
    /* renamed from: ʻ  reason: contains not printable characters */
    public Krizalid m6463() {
        return this.f5982;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public KyoKusanagi m6464() {
        return new KyoKusanagi(this);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public ChinGentsai m6465() {
        ChinGentsai chinGentsai = this.f5984;
        if (chinGentsai != null) {
            return chinGentsai;
        }
        ChinGentsai r0 = ChinGentsai.m6431(this.f5981);
        this.f5984 = r0;
        return r0;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public long m6466() {
        return this.f5988;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public long m6467() {
        return this.f5985;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public ShingoYabuki m6468() {
        return this.f5981;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m6469() {
        return this.f5992;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Goenitz m6470() {
        return this.f5989;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m6471() {
        return this.f5992 >= 200 && this.f5992 < 300;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Whip m6472() {
        return this.f5993;
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m6473(String str) {
        return m6474(str, (String) null);
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m6474(String str, @Nullable String str2) {
        String r0 = this.f5981.m6585(str);
        return r0 != null ? r0 : str2;
    }
}
