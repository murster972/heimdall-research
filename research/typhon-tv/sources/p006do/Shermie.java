package p006do;

import java.io.IOException;
import javax.annotation.Nullable;

/* renamed from: do.Shermie  reason: invalid package */
public interface Shermie {

    /* renamed from: do.Shermie$KyoKusanagi */
    public interface KyoKusanagi {
        @Nullable
        /* renamed from: 靐  reason: contains not printable characters */
        BrianBattler m6575();

        /* renamed from: 龘  reason: contains not printable characters */
        JhunHoon m6576(Whip whip) throws IOException;

        /* renamed from: 龘  reason: contains not printable characters */
        Whip m6577();
    }

    JhunHoon a(KyoKusanagi kyoKusanagi) throws IOException;
}
