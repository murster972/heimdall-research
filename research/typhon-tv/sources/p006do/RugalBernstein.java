package p006do;

import com.google.android.exoplayer2.C;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import p006do.p007do.GoroDaimon;
import p006do.p007do.p017byte.ChangKoehan;
import p006do.p007do.p021if.ChinGentsai;
import p006do.p007do.p021if.HeavyD;

/* renamed from: do.RugalBernstein  reason: invalid package */
public final class RugalBernstein {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Executor f6083 = new ThreadPoolExecutor(0, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, 60, TimeUnit.SECONDS, new SynchronousQueue(), GoroDaimon.m18420("OkHttp ConnectionPool", true));

    /* renamed from: 齉  reason: contains not printable characters */
    static final /* synthetic */ boolean f6084 = (!RugalBernstein.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    private final long f6085;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Runnable f6086;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Deque<p006do.p007do.p021if.GoroDaimon> f6087;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f6088;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean f6089;

    /* renamed from: 龘  reason: contains not printable characters */
    final ChinGentsai f6090;

    public RugalBernstein() {
        this(5, 5, TimeUnit.MINUTES);
    }

    public RugalBernstein(int i, long j, TimeUnit timeUnit) {
        this.f6086 = new Runnable() {
            public void run() {
                while (true) {
                    long r0 = RugalBernstein.this.m6560(System.nanoTime());
                    if (r0 != -1) {
                        if (r0 > 0) {
                            long j = r0 / C.MICROS_PER_SECOND;
                            long j2 = r0 - (j * C.MICROS_PER_SECOND);
                            synchronized (RugalBernstein.this) {
                                try {
                                    RugalBernstein.this.wait(j, (int) j2);
                                } catch (InterruptedException e) {
                                }
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        };
        this.f6087 = new ArrayDeque();
        this.f6090 = new ChinGentsai();
        this.f6088 = i;
        this.f6085 = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m6558(p006do.p007do.p021if.GoroDaimon goroDaimon, long j) {
        List<Reference<HeavyD>> list = goroDaimon.f14887;
        int i = 0;
        while (i < list.size()) {
            Reference reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                ChangKoehan.m18443().m18452("A connection to " + goroDaimon.m18608().m18379().m6512() + " was leaked. Did you forget to close a response body?", ((HeavyD.KyoKusanagi) reference).f14902);
                list.remove(i);
                goroDaimon.f14889 = true;
                if (list.isEmpty()) {
                    goroDaimon.f14885 = j - this.f6085;
                    return 0;
                }
            }
        }
        return list.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m6559(p006do.p007do.p021if.GoroDaimon goroDaimon) {
        if (!f6084 && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (goroDaimon.f14889 || this.f6088 == 0) {
            this.f6087.remove(goroDaimon);
            return true;
        } else {
            notifyAll();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m6560(long j) {
        p006do.p007do.p021if.GoroDaimon goroDaimon = null;
        long j2 = Long.MIN_VALUE;
        synchronized (this) {
            int i = 0;
            int i2 = 0;
            for (p006do.p007do.p021if.GoroDaimon next : this.f6087) {
                if (m6558(next, j) > 0) {
                    i2++;
                } else {
                    i++;
                    long j3 = j - next.f14885;
                    if (j3 <= j2) {
                        j3 = j2;
                        next = goroDaimon;
                    }
                    j2 = j3;
                    goroDaimon = next;
                }
            }
            if (j2 >= this.f6085 || i > this.f6088) {
                this.f6087.remove(goroDaimon);
                GoroDaimon.m18423(goroDaimon.m18605());
                return 0;
            } else if (i > 0) {
                long j4 = this.f6085 - j2;
                return j4;
            } else if (i2 > 0) {
                long j5 = this.f6085;
                return j5;
            } else {
                this.f6089 = false;
                return -1;
            }
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public p006do.p007do.p021if.GoroDaimon m6561(KyoKusanagi kyoKusanagi, HeavyD heavyD, Ramon ramon) {
        if (f6084 || Thread.holdsLock(this)) {
            for (p006do.p007do.p021if.GoroDaimon next : this.f6087) {
                if (next.m18613(kyoKusanagi, ramon)) {
                    heavyD.m18628(next);
                    return next;
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public Socket m6562(KyoKusanagi kyoKusanagi, HeavyD heavyD) {
        if (f6084 || Thread.holdsLock(this)) {
            for (p006do.p007do.p021if.GoroDaimon next : this.f6087) {
                if (next.m18613(kyoKusanagi, (Ramon) null) && next.m18606() && next != heavyD.m18622()) {
                    return heavyD.m18623(next);
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6563(p006do.p007do.p021if.GoroDaimon goroDaimon) {
        if (f6084 || Thread.holdsLock(this)) {
            if (!this.f6089) {
                this.f6089 = true;
                f6083.execute(this.f6086);
            }
            this.f6087.add(goroDaimon);
            return;
        }
        throw new AssertionError();
    }
}
