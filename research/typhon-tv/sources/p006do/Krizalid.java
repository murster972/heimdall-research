package p006do;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import javax.annotation.Nullable;
import p006do.p007do.GoroDaimon;
import p009if.ChangKoehan;

/* renamed from: do.Krizalid  reason: invalid package */
public abstract class Krizalid implements Closeable {
    /* renamed from: ʻ  reason: contains not printable characters */
    private Charset m6491() {
        Chris r0 = m6498();
        return r0 != null ? r0.m6443(GoroDaimon.f14739) : GoroDaimon.f14739;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Krizalid m6492(@Nullable final Chris chris, final long j, final ChangKoehan changKoehan) {
        if (changKoehan != null) {
            return new Krizalid() {
                /* renamed from: 靐  reason: contains not printable characters */
                public long m6499() {
                    return j;
                }

                /* renamed from: 麤  reason: contains not printable characters */
                public ChangKoehan m6500() {
                    return changKoehan;
                }

                @Nullable
                /* renamed from: 龘  reason: contains not printable characters */
                public Chris m6501() {
                    return Chris.this;
                }
            };
        }
        throw new NullPointerException("source == null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Krizalid m6493(@Nullable Chris chris, byte[] bArr) {
        return m6492(chris, (long) bArr.length, new p009if.GoroDaimon().m6754(bArr));
    }

    public void close() {
        GoroDaimon.m18422((Closeable) m6496());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final String m6494() throws IOException {
        ChangKoehan r1 = m6496();
        try {
            return r1.m6710(GoroDaimon.m18417(r1, m6491()));
        } finally {
            GoroDaimon.m18422((Closeable) r1);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract long m6495();

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract ChangKoehan m6496();

    /* renamed from: 齉  reason: contains not printable characters */
    public final InputStream m6497() {
        return m6496().m6698();
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Chris m6498();
}
