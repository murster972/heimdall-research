package p006do.p007do.p019char;

import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import p006do.p007do.GoroDaimon;

/* renamed from: do.do.char.ChinGentsai  reason: invalid package */
public final class ChinGentsai implements HostnameVerifier {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ChinGentsai f14786 = new ChinGentsai();

    private ChinGentsai() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m18492(String str, X509Certificate x509Certificate) {
        List<String> r3 = m18495(x509Certificate, 7);
        int size = r3.size();
        for (int i = 0; i < size; i++) {
            if (str.equalsIgnoreCase(r3.get(i))) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m18493(String str, X509Certificate x509Certificate) {
        String r0;
        String lowerCase = str.toLowerCase(Locale.US);
        List<String> r6 = m18495(x509Certificate, 2);
        int size = r6.size();
        int i = 0;
        boolean z = false;
        while (i < size) {
            if (m18496(lowerCase, r6.get(i))) {
                return true;
            }
            i++;
            z = true;
        }
        if (z || (r0 = new GoroDaimon(x509Certificate.getSubjectX500Principal()).m18505("cn")) == null) {
            return false;
        }
        return m18496(lowerCase, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<String> m18494(X509Certificate x509Certificate) {
        List<String> r0 = m18495(x509Certificate, 7);
        List<String> r1 = m18495(x509Certificate, 2);
        ArrayList arrayList = new ArrayList(r0.size() + r1.size());
        arrayList.addAll(r0);
        arrayList.addAll(r1);
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<String> m18495(X509Certificate x509Certificate, int i) {
        Integer num;
        String str;
        ArrayList arrayList = new ArrayList();
        try {
            Collection<List<?>> subjectAlternativeNames = x509Certificate.getSubjectAlternativeNames();
            if (subjectAlternativeNames == null) {
                return Collections.emptyList();
            }
            for (List next : subjectAlternativeNames) {
                if (!(next == null || next.size() < 2 || (num = (Integer) next.get(0)) == null || num.intValue() != i || (str = (String) next.get(1)) == null)) {
                    arrayList.add(str);
                }
            }
            return arrayList;
        } catch (CertificateParsingException e) {
            return Collections.emptyList();
        }
    }

    public boolean verify(String str, SSLSession sSLSession) {
        try {
            return m18497(str, (X509Certificate) sSLSession.getPeerCertificates()[0]);
        } catch (SSLException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18496(String str, String str2) {
        if (str == null || str.length() == 0 || str.startsWith(".") || str.endsWith("..") || str2 == null || str2.length() == 0 || str2.startsWith(".") || str2.endsWith("..")) {
            return false;
        }
        if (!str.endsWith(".")) {
            str = str + '.';
        }
        if (!str2.endsWith(".")) {
            str2 = str2 + '.';
        }
        String lowerCase = str2.toLowerCase(Locale.US);
        if (!lowerCase.contains("*")) {
            return str.equals(lowerCase);
        }
        if (!lowerCase.startsWith("*.") || lowerCase.indexOf(42, 1) != -1 || str.length() < lowerCase.length() || "*.".equals(lowerCase)) {
            return false;
        }
        String substring = lowerCase.substring(1);
        if (!str.endsWith(substring)) {
            return false;
        }
        int length = str.length() - substring.length();
        return length <= 0 || str.lastIndexOf(46, length + -1) == -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18497(String str, X509Certificate x509Certificate) {
        return GoroDaimon.m18409(str) ? m18492(str, x509Certificate) : m18493(str, x509Certificate);
    }
}
