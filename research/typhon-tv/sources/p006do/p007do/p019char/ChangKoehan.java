package p006do.p007do.p019char;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.x500.X500Principal;

/* renamed from: do.do.char.ChangKoehan  reason: invalid package */
public abstract class ChangKoehan {

    /* renamed from: do.do.char.ChangKoehan$BenimaruNikaido */
    static final class BenimaruNikaido extends ChangKoehan {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<X500Principal, Set<X509Certificate>> f14783 = new LinkedHashMap();

        BenimaruNikaido(X509Certificate... x509CertificateArr) {
            for (X509Certificate x509Certificate : x509CertificateArr) {
                X500Principal subjectX500Principal = x509Certificate.getSubjectX500Principal();
                Set set = this.f14783.get(subjectX500Principal);
                if (set == null) {
                    set = new LinkedHashSet(1);
                    this.f14783.put(subjectX500Principal, set);
                }
                set.add(x509Certificate);
            }
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            return (obj instanceof BenimaruNikaido) && ((BenimaruNikaido) obj).f14783.equals(this.f14783);
        }

        public int hashCode() {
            return this.f14783.hashCode();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public X509Certificate m18490(X509Certificate x509Certificate) {
            Set<X509Certificate> set = this.f14783.get(x509Certificate.getIssuerX500Principal());
            if (set == null) {
                return null;
            }
            for (X509Certificate x509Certificate2 : set) {
                try {
                    x509Certificate.verify(x509Certificate2.getPublicKey());
                    return x509Certificate2;
                } catch (Exception e) {
                }
            }
            return null;
        }
    }

    /* renamed from: do.do.char.ChangKoehan$KyoKusanagi */
    static final class KyoKusanagi extends ChangKoehan {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Method f14784;

        /* renamed from: 龘  reason: contains not printable characters */
        private final X509TrustManager f14785;

        KyoKusanagi(X509TrustManager x509TrustManager, Method method) {
            this.f14784 = method;
            this.f14785 = x509TrustManager;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof KyoKusanagi)) {
                return false;
            }
            KyoKusanagi kyoKusanagi = (KyoKusanagi) obj;
            return this.f14785.equals(kyoKusanagi.f14785) && this.f14784.equals(kyoKusanagi.f14784);
        }

        public int hashCode() {
            return this.f14785.hashCode() + (this.f14784.hashCode() * 31);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public X509Certificate m18491(X509Certificate x509Certificate) {
            try {
                TrustAnchor trustAnchor = (TrustAnchor) this.f14784.invoke(this.f14785, new Object[]{x509Certificate});
                if (trustAnchor != null) {
                    return trustAnchor.getTrustedCert();
                }
                return null;
            } catch (IllegalAccessException e) {
                throw new AssertionError();
            } catch (InvocationTargetException e2) {
                return null;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ChangKoehan m18487(X509TrustManager x509TrustManager) {
        try {
            Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", new Class[]{X509Certificate.class});
            declaredMethod.setAccessible(true);
            return new KyoKusanagi(x509TrustManager, declaredMethod);
        } catch (NoSuchMethodException e) {
            return m18488(x509TrustManager.getAcceptedIssuers());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ChangKoehan m18488(X509Certificate... x509CertificateArr) {
        return new BenimaruNikaido(x509CertificateArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract X509Certificate m18489(X509Certificate x509Certificate);
}
