package p006do.p007do.p019char;

import java.security.GeneralSecurityException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;

/* renamed from: do.do.char.KyoKusanagi  reason: invalid package */
public final class KyoKusanagi extends BenimaruNikaido {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ChangKoehan f14794;

    public KyoKusanagi(ChangKoehan changKoehan) {
        this.f14794 = changKoehan;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m18506(X509Certificate x509Certificate, X509Certificate x509Certificate2) {
        if (!x509Certificate.getIssuerDN().equals(x509Certificate2.getSubjectDN())) {
            return false;
        }
        try {
            x509Certificate.verify(x509Certificate2.getPublicKey());
            return true;
        } catch (GeneralSecurityException e) {
            return false;
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof KyoKusanagi) && ((KyoKusanagi) obj).f14794.equals(this.f14794);
    }

    public int hashCode() {
        return this.f14794.hashCode();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<Certificate> m18507(List<Certificate> list, String str) throws SSLPeerUnverifiedException {
        boolean z = false;
        ArrayDeque arrayDeque = new ArrayDeque(list);
        ArrayList arrayList = new ArrayList();
        arrayList.add(arrayDeque.removeFirst());
        int i = 0;
        while (true) {
            boolean z2 = z;
            if (i < 9) {
                X509Certificate x509Certificate = (X509Certificate) arrayList.get(arrayList.size() - 1);
                X509Certificate r1 = this.f14794.m18489(x509Certificate);
                if (r1 != null) {
                    if (arrayList.size() > 1 || !x509Certificate.equals(r1)) {
                        arrayList.add(r1);
                    }
                    if (m18506(r1, r1)) {
                        return arrayList;
                    }
                    z = true;
                } else {
                    Iterator it2 = arrayDeque.iterator();
                    while (it2.hasNext()) {
                        X509Certificate x509Certificate2 = (X509Certificate) it2.next();
                        if (m18506(x509Certificate, x509Certificate2)) {
                            it2.remove();
                            arrayList.add(x509Certificate2);
                            z = z2;
                        }
                    }
                    if (z2) {
                        return arrayList;
                    }
                    throw new SSLPeerUnverifiedException("Failed to find a trusted cert that signed " + x509Certificate);
                }
                i++;
            } else {
                throw new SSLPeerUnverifiedException("Certificate chain too long: " + arrayList);
            }
        }
    }
}
