package p006do.p007do.p019char;

import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import javax.security.auth.x500.X500Principal;

/* renamed from: do.do.char.GoroDaimon  reason: invalid package */
final class GoroDaimon {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f14787;

    /* renamed from: ʼ  reason: contains not printable characters */
    private char[] f14788;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f14789;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f14790 = this.f14793.length();

    /* renamed from: 麤  reason: contains not printable characters */
    private int f14791;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f14792;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f14793;

    GoroDaimon(X500Principal x500Principal) {
        this.f14793 = x500Principal.getName("RFC2253");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private char m18498() {
        int i;
        int i2;
        int r1 = m18503(this.f14792);
        this.f14792++;
        if (r1 < 128) {
            return (char) r1;
        }
        if (r1 < 192 || r1 > 247) {
            return '?';
        }
        if (r1 <= 223) {
            i = 1;
            i2 = r1 & 31;
        } else if (r1 <= 239) {
            i = 2;
            i2 = r1 & 15;
        } else {
            i = 3;
            i2 = r1 & 7;
        }
        int i3 = i2;
        for (int i4 = 0; i4 < i; i4++) {
            this.f14792++;
            if (this.f14792 == this.f14790 || this.f14788[this.f14792] != '\\') {
                return '?';
            }
            this.f14792++;
            int r12 = m18503(this.f14792);
            this.f14792++;
            if ((r12 & PsExtractor.AUDIO_STREAM) != 128) {
                return '?';
            }
            i3 = (i3 << 6) + (r12 & 63);
        }
        return (char) i3;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private char m18499() {
        this.f14792++;
        if (this.f14792 == this.f14790) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f14793);
        }
        switch (this.f14788[this.f14792]) {
            case ' ':
            case '\"':
            case '#':
            case '%':
            case '*':
            case '+':
            case ',':
            case ';':
            case '<':
            case '=':
            case '>':
            case '\\':
            case '_':
                return this.f14788[this.f14792];
            default:
                return m18498();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m18500() {
        this.f14792++;
        this.f14791 = this.f14792;
        this.f14789 = this.f14791;
        while (this.f14792 != this.f14790) {
            if (this.f14788[this.f14792] == '\"') {
                this.f14792++;
                while (this.f14792 < this.f14790 && this.f14788[this.f14792] == ' ') {
                    this.f14792++;
                }
                return new String(this.f14788, this.f14791, this.f14789 - this.f14791);
            }
            if (this.f14788[this.f14792] == '\\') {
                this.f14788[this.f14789] = m18499();
            } else {
                this.f14788[this.f14789] = this.f14788[this.f14792];
            }
            this.f14792++;
            this.f14789++;
        }
        throw new IllegalStateException("Unexpected end of DN: " + this.f14793);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private String m18501() {
        this.f14791 = this.f14792;
        this.f14789 = this.f14792;
        while (this.f14792 < this.f14790) {
            switch (this.f14788[this.f14792]) {
                case ' ':
                    this.f14787 = this.f14789;
                    this.f14792++;
                    char[] cArr = this.f14788;
                    int i = this.f14789;
                    this.f14789 = i + 1;
                    cArr[i] = ' ';
                    while (this.f14792 < this.f14790 && this.f14788[this.f14792] == ' ') {
                        char[] cArr2 = this.f14788;
                        int i2 = this.f14789;
                        this.f14789 = i2 + 1;
                        cArr2[i2] = ' ';
                        this.f14792++;
                    }
                    if (this.f14792 != this.f14790 && this.f14788[this.f14792] != ',' && this.f14788[this.f14792] != '+' && this.f14788[this.f14792] != ';') {
                        break;
                    } else {
                        return new String(this.f14788, this.f14791, this.f14787 - this.f14791);
                    }
                    break;
                case '+':
                case ',':
                case ';':
                    return new String(this.f14788, this.f14791, this.f14789 - this.f14791);
                case '\\':
                    char[] cArr3 = this.f14788;
                    int i3 = this.f14789;
                    this.f14789 = i3 + 1;
                    cArr3[i3] = m18499();
                    this.f14792++;
                    break;
                default:
                    char[] cArr4 = this.f14788;
                    int i4 = this.f14789;
                    this.f14789 = i4 + 1;
                    cArr4[i4] = this.f14788[this.f14792];
                    this.f14792++;
                    break;
            }
        }
        return new String(this.f14788, this.f14791, this.f14789 - this.f14791);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private String m18502() {
        if (this.f14792 + 4 >= this.f14790) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f14793);
        }
        this.f14791 = this.f14792;
        this.f14792++;
        while (true) {
            if (this.f14792 == this.f14790 || this.f14788[this.f14792] == '+' || this.f14788[this.f14792] == ',' || this.f14788[this.f14792] == ';') {
                this.f14789 = this.f14792;
            } else if (this.f14788[this.f14792] == ' ') {
                this.f14789 = this.f14792;
                this.f14792++;
                while (this.f14792 < this.f14790 && this.f14788[this.f14792] == ' ') {
                    this.f14792++;
                }
            } else {
                if (this.f14788[this.f14792] >= 'A' && this.f14788[this.f14792] <= 'F') {
                    char[] cArr = this.f14788;
                    int i = this.f14792;
                    cArr[i] = (char) (cArr[i] + ' ');
                }
                this.f14792++;
            }
        }
        this.f14789 = this.f14792;
        int i2 = this.f14789 - this.f14791;
        if (i2 < 5 || (i2 & 1) == 0) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f14793);
        }
        byte[] bArr = new byte[(i2 / 2)];
        int i3 = this.f14791 + 1;
        for (int i4 = 0; i4 < bArr.length; i4++) {
            bArr[i4] = (byte) m18503(i3);
            i3 += 2;
        }
        return new String(this.f14788, this.f14791, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m18503(int i) {
        int i2;
        int i3;
        if (i + 1 >= this.f14790) {
            throw new IllegalStateException("Malformed DN: " + this.f14793);
        }
        char c = this.f14788[i];
        if (c >= '0' && c <= '9') {
            i2 = c - '0';
        } else if (c >= 'a' && c <= 'f') {
            i2 = c - 'W';
        } else if (c < 'A' || c > 'F') {
            throw new IllegalStateException("Malformed DN: " + this.f14793);
        } else {
            i2 = c - '7';
        }
        char c2 = this.f14788[i + 1];
        if (c2 >= '0' && c2 <= '9') {
            i3 = c2 - '0';
        } else if (c2 >= 'a' && c2 <= 'f') {
            i3 = c2 - 'W';
        } else if (c2 < 'A' || c2 > 'F') {
            throw new IllegalStateException("Malformed DN: " + this.f14793);
        } else {
            i3 = c2 - '7';
        }
        return (i2 << 4) + i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m18504() {
        while (this.f14792 < this.f14790 && this.f14788[this.f14792] == ' ') {
            this.f14792++;
        }
        if (this.f14792 == this.f14790) {
            return null;
        }
        this.f14791 = this.f14792;
        this.f14792++;
        while (this.f14792 < this.f14790 && this.f14788[this.f14792] != '=' && this.f14788[this.f14792] != ' ') {
            this.f14792++;
        }
        if (this.f14792 >= this.f14790) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f14793);
        }
        this.f14789 = this.f14792;
        if (this.f14788[this.f14792] == ' ') {
            while (this.f14792 < this.f14790 && this.f14788[this.f14792] != '=' && this.f14788[this.f14792] == ' ') {
                this.f14792++;
            }
            if (this.f14788[this.f14792] != '=' || this.f14792 == this.f14790) {
                throw new IllegalStateException("Unexpected end of DN: " + this.f14793);
            }
        }
        this.f14792++;
        while (this.f14792 < this.f14790 && this.f14788[this.f14792] == ' ') {
            this.f14792++;
        }
        if (this.f14789 - this.f14791 > 4 && this.f14788[this.f14791 + 3] == '.' && ((this.f14788[this.f14791] == 'O' || this.f14788[this.f14791] == 'o') && ((this.f14788[this.f14791 + 1] == 'I' || this.f14788[this.f14791 + 1] == 'i') && (this.f14788[this.f14791 + 2] == 'D' || this.f14788[this.f14791 + 2] == 'd')))) {
            this.f14791 += 4;
        }
        return new String(this.f14788, this.f14791, this.f14789 - this.f14791);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m18505(String str) {
        this.f14792 = 0;
        this.f14791 = 0;
        this.f14789 = 0;
        this.f14787 = 0;
        this.f14788 = this.f14793.toCharArray();
        String r0 = m18504();
        if (r0 == null) {
            return null;
        }
        do {
            String str2 = "";
            if (this.f14792 == this.f14790) {
                return null;
            }
            switch (this.f14788[this.f14792]) {
                case '\"':
                    str2 = m18500();
                    break;
                case '#':
                    str2 = m18502();
                    break;
                case '+':
                case ',':
                case ';':
                    break;
                default:
                    str2 = m18501();
                    break;
            }
            if (str.equalsIgnoreCase(r0)) {
                return str2;
            }
            if (this.f14792 >= this.f14790) {
                return null;
            }
            if (this.f14788[this.f14792] == ',' || this.f14788[this.f14792] == ';' || this.f14788[this.f14792] == '+') {
                this.f14792++;
                r0 = m18504();
            } else {
                throw new IllegalStateException("Malformed DN: " + this.f14793);
            }
        } while (r0 != null);
        throw new IllegalStateException("Malformed DN: " + this.f14793);
    }
}
