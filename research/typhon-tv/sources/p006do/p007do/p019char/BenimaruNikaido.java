package p006do.p007do.p019char;

import java.security.cert.Certificate;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.X509TrustManager;
import p006do.p007do.p017byte.ChangKoehan;

/* renamed from: do.do.char.BenimaruNikaido  reason: invalid package */
public abstract class BenimaruNikaido {
    /* renamed from: 龘  reason: contains not printable characters */
    public static BenimaruNikaido m18485(X509TrustManager x509TrustManager) {
        return ChangKoehan.m18443().m18449(x509TrustManager);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract List<Certificate> m18486(List<Certificate> list, String str) throws SSLPeerUnverifiedException;
}
