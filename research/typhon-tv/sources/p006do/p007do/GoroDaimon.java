package p006do.p007do;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.IDN;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import org.apache.commons.lang3.time.TimeZones;
import p006do.Bao;
import p006do.Chris;
import p006do.Krizalid;
import p006do.YashiroNanakase;
import p009if.ChangKoehan;
import p009if.ChoiBounge;
import p009if.Shermie;

/* renamed from: do.do.GoroDaimon  reason: invalid package */
public final class GoroDaimon {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final TimeZone f14729 = TimeZone.getTimeZone(TimeZones.GMT_ID);

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final Comparator<String> f14730 = new Comparator<String>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(String str, String str2) {
            return str.compareTo(str2);
        }
    };

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final ChoiBounge f14731 = ChoiBounge.b("efbbbf");

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final Charset f14732 = Charset.forName("UTF-16BE");

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final Charset f14733 = Charset.forName("UTF-16LE");

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final ChoiBounge f14734 = ChoiBounge.b("ffff0000");

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final Pattern f14735 = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final ChoiBounge f14736 = ChoiBounge.b("feff");

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final ChoiBounge f14737 = ChoiBounge.b("fffe");

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final ChoiBounge f14738 = ChoiBounge.b("0000ffff");

    /* renamed from: 连任  reason: contains not printable characters */
    public static final Charset f14739 = Charset.forName("UTF-8");

    /* renamed from: 靐  reason: contains not printable characters */
    public static final String[] f14740 = new String[0];

    /* renamed from: 麤  reason: contains not printable characters */
    public static final Bao f14741 = Bao.a((Chris) null, f14743);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Krizalid f14742 = Krizalid.m6493((Chris) null, f14743);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final byte[] f14743 = new byte[0];

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final Charset f14744 = Charset.forName("UTF-32BE");

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static final Charset f14745 = Charset.forName("UTF-32LE");

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m18403(String str) {
        int i = 0;
        int length = str.length();
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m18404(String str, int i, int i2) {
        int i3 = i2 - 1;
        while (i3 >= i) {
            switch (str.charAt(i3)) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    i3--;
                default:
                    return i3 + 1;
            }
        }
        return i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m18405(Shermie shermie, int i, TimeUnit timeUnit) throws IOException {
        long nanoTime = System.nanoTime();
        long r0 = shermie.m19010().m18945() ? shermie.m19010().m18944() - nanoTime : Long.MAX_VALUE;
        shermie.m19010().m18946(Math.min(r0, timeUnit.toNanos((long) i)) + nanoTime);
        try {
            p009if.GoroDaimon goroDaimon = new p009if.GoroDaimon();
            while (shermie.m19009(goroDaimon, PlaybackStateCompat.ACTION_PLAY_FROM_URI) != -1) {
                goroDaimon.m6732();
            }
            if (r0 == Long.MAX_VALUE) {
                shermie.m19010().m18941();
            } else {
                shermie.m19010().m18946(r0 + nanoTime);
            }
            return true;
        } catch (InterruptedIOException e) {
            if (r0 == Long.MAX_VALUE) {
                shermie.m19010().m18941();
            } else {
                shermie.m19010().m18946(r0 + nanoTime);
            }
            return false;
        } catch (Throwable th) {
            if (r0 == Long.MAX_VALUE) {
                shermie.m19010().m18941();
            } else {
                shermie.m19010().m18946(r0 + nanoTime);
            }
            throw th;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m18406(Comparator<String> comparator, String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length == 0 || strArr2.length == 0) {
            return false;
        }
        for (String str : strArr) {
            for (String compare : strArr2) {
                if (comparator.compare(str, compare) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static boolean m18407(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                return true;
            }
            if (" #%/:?@[\\]".indexOf(charAt) != -1) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m18408(String str, int i, int i2) {
        int r0 = m18410(str, i, i2);
        return str.substring(r0, m18404(str, r0, i2));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m18409(String str) {
        return f14735.matcher(str).matches();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m18410(String str, int i, int i2) {
        int i3 = i;
        while (i3 < i2) {
            switch (str.charAt(i3)) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    i3++;
                default:
                    return i3;
            }
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m18411(String str, int i, int i2, char c) {
        for (int i3 = i; i3 < i2; i3++) {
            if (str.charAt(i3) == c) {
                return i3;
            }
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m18412(String str, int i, int i2, String str2) {
        for (int i3 = i; i3 < i2; i3++) {
            if (str2.indexOf(str.charAt(i3)) != -1) {
                return i3;
            }
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m18413(Comparator<String> comparator, String[] strArr, String str) {
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            if (comparator.compare(strArr[i], str) == 0) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m18414(YashiroNanakase yashiroNanakase, boolean z) {
        String r0 = yashiroNanakase.m6608().contains(":") ? "[" + yashiroNanakase.m6608() + "]" : yashiroNanakase.m6608();
        return (z || yashiroNanakase.m6609() != YashiroNanakase.m6598(yashiroNanakase.m6618())) ? r0 + ":" + yashiroNanakase.m6609() : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m18415(String str) {
        try {
            String lowerCase = IDN.toASCII(str).toLowerCase(Locale.US);
            if (!lowerCase.isEmpty() && !m18407(lowerCase)) {
                return lowerCase;
            }
            return null;
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m18416(String str, Object... objArr) {
        return String.format(Locale.US, str, objArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Charset m18417(ChangKoehan changKoehan, Charset charset) throws IOException {
        if (changKoehan.m6713(0, f14731)) {
            changKoehan.m6704((long) f14731.g());
            return f14739;
        } else if (changKoehan.m6713(0, f14736)) {
            changKoehan.m6704((long) f14736.g());
            return f14732;
        } else if (changKoehan.m6713(0, f14737)) {
            changKoehan.m6704((long) f14737.g());
            return f14733;
        } else if (changKoehan.m6713(0, f14738)) {
            changKoehan.m6704((long) f14738.g());
            return f14744;
        } else if (!changKoehan.m6713(0, f14734)) {
            return charset;
        } else {
            changKoehan.m6704((long) f14734.g());
            return f14745;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> List<T> m18418(List<T> list) {
        return Collections.unmodifiableList(new ArrayList(list));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> List<T> m18419(T... tArr) {
        return Collections.unmodifiableList(Arrays.asList((Object[]) tArr.clone()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ThreadFactory m18420(final String str, final boolean z) {
        return new ThreadFactory() {
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, str);
                thread.setDaemon(z);
                return thread;
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m18421(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m18422(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m18423(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (AssertionError e) {
                if (!m18425(e)) {
                    throw e;
                }
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m18424(Shermie shermie, int i, TimeUnit timeUnit) {
        try {
            return m18405(shermie, i, timeUnit);
        } catch (IOException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m18425(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m18426(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String[] m18427(Comparator<? super String> comparator, String[] strArr, String[] strArr2) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            int length = strArr2.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (comparator.compare(str, strArr2[i]) == 0) {
                    arrayList.add(str);
                    break;
                } else {
                    i++;
                }
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String[] m18428(String[] strArr, String str) {
        String[] strArr2 = new String[(strArr.length + 1)];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
        strArr2[strArr2.length - 1] = str;
        return strArr2;
    }
}
