package p006do.p007do.p008do;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import p006do.JhunHoon;
import p006do.ShingoYabuki;
import p006do.Whip;
import p006do.p007do.p020for.ChangKoehan;
import p006do.p007do.p020for.ChinGentsai;

/* renamed from: do.do.do.GoroDaimon  reason: invalid package */
public final class GoroDaimon {
    @Nullable

    /* renamed from: 靐  reason: contains not printable characters */
    public final JhunHoon f6129;
    @Nullable

    /* renamed from: 龘  reason: contains not printable characters */
    public final Whip f6130;

    /* renamed from: do.do.do.GoroDaimon$KyoKusanagi */
    public static class KyoKusanagi {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Date f14821;

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f14822;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Date f14823;

        /* renamed from: ˈ  reason: contains not printable characters */
        private int f14824 = -1;

        /* renamed from: ˑ  reason: contains not printable characters */
        private long f14825;

        /* renamed from: ٴ  reason: contains not printable characters */
        private long f14826;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private String f14827;

        /* renamed from: 连任  reason: contains not printable characters */
        private String f14828;

        /* renamed from: 靐  reason: contains not printable characters */
        final Whip f14829;

        /* renamed from: 麤  reason: contains not printable characters */
        private Date f14830;

        /* renamed from: 齉  reason: contains not printable characters */
        final JhunHoon f14831;

        /* renamed from: 龘  reason: contains not printable characters */
        final long f14832;

        public KyoKusanagi(long j, Whip whip, JhunHoon jhunHoon) {
            this.f14832 = j;
            this.f14829 = whip;
            this.f14831 = jhunHoon;
            if (jhunHoon != null) {
                this.f14825 = jhunHoon.m6466();
                this.f14826 = jhunHoon.m6467();
                ShingoYabuki r1 = jhunHoon.m6468();
                int r2 = r1.m6583();
                for (int i = 0; i < r2; i++) {
                    String r3 = r1.m6584(i);
                    String r4 = r1.m6579(i);
                    if ("Date".equalsIgnoreCase(r3)) {
                        this.f14830 = ChinGentsai.m18549(r4);
                        this.f14828 = r4;
                    } else if ("Expires".equalsIgnoreCase(r3)) {
                        this.f14823 = ChinGentsai.m18549(r4);
                    } else if ("Last-Modified".equalsIgnoreCase(r3)) {
                        this.f14821 = ChinGentsai.m18549(r4);
                        this.f14822 = r4;
                    } else if ("ETag".equalsIgnoreCase(r3)) {
                        this.f14827 = r4;
                    } else if ("Age".equalsIgnoreCase(r3)) {
                        this.f14824 = ChangKoehan.m18540(r4, -1);
                    }
                }
            }
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean m18525() {
            return this.f14831.m6465().m6439() == -1 && this.f14823 == null;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private GoroDaimon m18526() {
            String str;
            String str2;
            long j = 0;
            if (this.f14831 == null) {
                return new GoroDaimon(this.f14829, (JhunHoon) null);
            }
            if (this.f14829.m6587() && this.f14831.m6470() == null) {
                return new GoroDaimon(this.f14829, (JhunHoon) null);
            }
            if (!GoroDaimon.m6647(this.f14831, this.f14829)) {
                return new GoroDaimon(this.f14829, (JhunHoon) null);
            }
            p006do.ChinGentsai r6 = this.f14829.m6586();
            if (r6.m6440() || m18529(this.f14829)) {
                return new GoroDaimon(this.f14829, (JhunHoon) null);
            }
            long r8 = m18527();
            long r0 = m18528();
            if (r6.m6439() != -1) {
                r0 = Math.min(r0, TimeUnit.SECONDS.toMillis((long) r6.m6439()));
            }
            long millis = r6.m6434() != -1 ? TimeUnit.SECONDS.toMillis((long) r6.m6434()) : 0;
            p006do.ChinGentsai r7 = this.f14831.m6465();
            if (!r7.m6432() && r6.m6433() != -1) {
                j = TimeUnit.SECONDS.toMillis((long) r6.m6433());
            }
            if (r7.m6440() || r8 + millis >= j + r0) {
                if (this.f14827 != null) {
                    str = "If-None-Match";
                    str2 = this.f14827;
                } else if (this.f14821 != null) {
                    str = "If-Modified-Since";
                    str2 = this.f14822;
                } else if (this.f14830 == null) {
                    return new GoroDaimon(this.f14829, (JhunHoon) null);
                } else {
                    str = "If-Modified-Since";
                    str2 = this.f14828;
                }
                ShingoYabuki.KyoKusanagi r2 = this.f14829.m6591().m6582();
                p006do.p007do.KyoKusanagi.f14748.m18437(r2, str, str2);
                return new GoroDaimon(this.f14829.m6588().m18396(r2.m18386()).m18393(), this.f14831);
            }
            JhunHoon.KyoKusanagi r4 = this.f14831.m6464();
            if (millis + r8 >= r0) {
                r4.m6489("Warning", "110 HttpURLConnection \"Response is stale\"");
            }
            if (r8 > 86400000 && m18525()) {
                r4.m6489("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
            }
            return new GoroDaimon((Whip) null, r4.m6490());
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private long m18527() {
            long j = 0;
            if (this.f14830 != null) {
                j = Math.max(0, this.f14826 - this.f14830.getTime());
            }
            if (this.f14824 != -1) {
                j = Math.max(j, TimeUnit.SECONDS.toMillis((long) this.f14824));
            }
            return j + (this.f14826 - this.f14825) + (this.f14832 - this.f14826);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private long m18528() {
            p006do.ChinGentsai r0 = this.f14831.m6465();
            if (r0.m6439() != -1) {
                return TimeUnit.SECONDS.toMillis((long) r0.m6439());
            }
            if (this.f14823 != null) {
                long time = this.f14823.getTime() - (this.f14830 != null ? this.f14830.getTime() : this.f14826);
                if (time <= 0) {
                    time = 0;
                }
                return time;
            } else if (this.f14821 == null || this.f14831.m6472().m6592().m6616() != null) {
                return 0;
            } else {
                long time2 = (this.f14830 != null ? this.f14830.getTime() : this.f14825) - this.f14821.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static boolean m18529(Whip whip) {
            return (whip.m6593("If-Modified-Since") == null && whip.m6593("If-None-Match") == null) ? false : true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public GoroDaimon m18530() {
            GoroDaimon r0 = m18526();
            return (r0.f6130 == null || !this.f14829.m6586().m6435()) ? r0 : new GoroDaimon((Whip) null, (JhunHoon) null);
        }
    }

    GoroDaimon(Whip whip, JhunHoon jhunHoon) {
        this.f6130 = whip;
        this.f6129 = jhunHoon;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m6647(JhunHoon jhunHoon, Whip whip) {
        switch (jhunHoon.m6469()) {
            case 200:
            case 203:
            case 204:
            case 300:
            case 301:
            case 308:
            case 404:
            case 405:
            case 410:
            case 414:
            case 501:
                break;
            case 302:
            case 307:
                if (jhunHoon.m6473("Expires") == null && jhunHoon.m6465().m6439() == -1 && !jhunHoon.m6465().m6436() && !jhunHoon.m6465().m6438()) {
                    return false;
                }
            default:
                return false;
        }
        return !jhunHoon.m6465().m6437() && !whip.m6586().m6437();
    }
}
