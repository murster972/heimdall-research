package p006do.p007do.p008do;

import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;

/* renamed from: do.do.do.ChinGentsai  reason: invalid package */
public final class ChinGentsai implements Closeable, Flushable {

    /* renamed from: ٴ  reason: contains not printable characters */
    static final /* synthetic */ boolean f14795 = (!ChinGentsai.class.desiredAssertionStatus());

    /* renamed from: 龘  reason: contains not printable characters */
    static final Pattern f14796 = Pattern.compile("[a-z0-9_-]{1,120}");

    /* renamed from: ʻ  reason: contains not printable characters */
    int f14797;

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean f14798;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f14799;

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f14800;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Executor f14801;

    /* renamed from: ˈ  reason: contains not printable characters */
    private long f14802;

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean f14803;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private long f14804;

    /* renamed from: 连任  reason: contains not printable characters */
    final LinkedHashMap<String, BenimaruNikaido> f14805;

    /* renamed from: 靐  reason: contains not printable characters */
    final p006do.p007do.p024try.KyoKusanagi f14806;

    /* renamed from: 麤  reason: contains not printable characters */
    p009if.ChinGentsai f14807;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f14808;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final Runnable f14809;

    /* renamed from: do.do.do.ChinGentsai$BenimaruNikaido */
    private final class BenimaruNikaido {

        /* renamed from: ʻ  reason: contains not printable characters */
        KyoKusanagi f14810;

        /* renamed from: ʼ  reason: contains not printable characters */
        long f14811;

        /* renamed from: 连任  reason: contains not printable characters */
        boolean f14812;

        /* renamed from: 靐  reason: contains not printable characters */
        final long[] f14813;

        /* renamed from: 麤  reason: contains not printable characters */
        final File[] f14814;

        /* renamed from: 齉  reason: contains not printable characters */
        final File[] f14815;

        /* renamed from: 龘  reason: contains not printable characters */
        final String f14816;

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18522(p009if.ChinGentsai chinGentsai) throws IOException {
            for (long r4 : this.f14813) {
                chinGentsai.m18934(32).m18927(r4);
            }
        }
    }

    /* renamed from: do.do.do.ChinGentsai$KyoKusanagi */
    public final class KyoKusanagi {

        /* renamed from: 靐  reason: contains not printable characters */
        final boolean[] f14817;

        /* renamed from: 麤  reason: contains not printable characters */
        private boolean f14818;

        /* renamed from: 齉  reason: contains not printable characters */
        final /* synthetic */ ChinGentsai f14819;

        /* renamed from: 龘  reason: contains not printable characters */
        final BenimaruNikaido f14820;

        /* renamed from: 靐  reason: contains not printable characters */
        public void m18523() throws IOException {
            synchronized (this.f14819) {
                if (this.f14818) {
                    throw new IllegalStateException();
                }
                if (this.f14820.f14810 == this) {
                    this.f14819.m18519(this, false);
                }
                this.f14818 = true;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18524() {
            if (this.f14820.f14810 == this) {
                for (int i = 0; i < this.f14819.f14808; i++) {
                    try {
                        this.f14819.f14806.m18848(this.f14820.f14814[i]);
                    } catch (IOException e) {
                    }
                }
                this.f14820.f14810 = null;
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private synchronized void m18516() {
        if (m18517()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    public synchronized void close() throws IOException {
        if (!this.f14798 || this.f14799) {
            this.f14799 = true;
        } else {
            for (BenimaruNikaido benimaruNikaido : (BenimaruNikaido[]) this.f14805.values().toArray(new BenimaruNikaido[this.f14805.size()])) {
                if (benimaruNikaido.f14810 != null) {
                    benimaruNikaido.f14810.m18523();
                }
            }
            m18518();
            this.f14807.close();
            this.f14807 = null;
            this.f14799 = true;
        }
    }

    public synchronized void flush() throws IOException {
        if (this.f14798) {
            m18516();
            m18518();
            this.f14807.flush();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized boolean m18517() {
        return this.f14799;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m18518() throws IOException {
        while (this.f14802 > this.f14804) {
            m18521(this.f14805.values().iterator().next());
        }
        this.f14803 = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18519(KyoKusanagi kyoKusanagi, boolean z) throws IOException {
        synchronized (this) {
            BenimaruNikaido benimaruNikaido = kyoKusanagi.f14820;
            if (benimaruNikaido.f14810 != kyoKusanagi) {
                throw new IllegalStateException();
            }
            if (z) {
                if (!benimaruNikaido.f14812) {
                    int i = 0;
                    while (true) {
                        if (i < this.f14808) {
                            if (!kyoKusanagi.f14817[i]) {
                                kyoKusanagi.m18523();
                                throw new IllegalStateException("Newly created entry didn't create value for index " + i);
                            } else if (!this.f14806.m18846(benimaruNikaido.f14814[i])) {
                                kyoKusanagi.m18523();
                                break;
                            } else {
                                i++;
                            }
                        }
                    }
                }
            }
            for (int i2 = 0; i2 < this.f14808; i2++) {
                File file = benimaruNikaido.f14814[i2];
                if (!z) {
                    this.f14806.m18848(file);
                } else if (this.f14806.m18846(file)) {
                    File file2 = benimaruNikaido.f14815[i2];
                    this.f14806.m18849(file, file2);
                    long j = benimaruNikaido.f14813[i2];
                    long r6 = this.f14806.m18847(file2);
                    benimaruNikaido.f14813[i2] = r6;
                    this.f14802 = (this.f14802 - j) + r6;
                }
            }
            this.f14797++;
            benimaruNikaido.f14810 = null;
            if (benimaruNikaido.f14812 || z) {
                benimaruNikaido.f14812 = true;
                this.f14807.m18935("CLEAN").m18934(32);
                this.f14807.m18935(benimaruNikaido.f14816);
                benimaruNikaido.m18522(this.f14807);
                this.f14807.m18934(10);
                if (z) {
                    long j2 = this.f14800;
                    this.f14800 = 1 + j2;
                    benimaruNikaido.f14811 = j2;
                }
            } else {
                this.f14805.remove(benimaruNikaido.f14816);
                this.f14807.m18935("REMOVE").m18934(32);
                this.f14807.m18935(benimaruNikaido.f14816);
                this.f14807.m18934(10);
            }
            this.f14807.flush();
            if (this.f14802 > this.f14804 || m18520()) {
                this.f14801.execute(this.f14809);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18520() {
        return this.f14797 >= 2000 && this.f14797 >= this.f14805.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18521(BenimaruNikaido benimaruNikaido) throws IOException {
        if (benimaruNikaido.f14810 != null) {
            benimaruNikaido.f14810.m18524();
        }
        for (int i = 0; i < this.f14808; i++) {
            this.f14806.m18848(benimaruNikaido.f14815[i]);
            this.f14802 -= benimaruNikaido.f14813[i];
            benimaruNikaido.f14813[i] = 0;
        }
        this.f14797++;
        this.f14807.m18935("REMOVE").m18934(32).m18935(benimaruNikaido.f14816).m18934(10);
        this.f14805.remove(benimaruNikaido.f14816);
        if (!m18520()) {
            return true;
        }
        this.f14801.execute(this.f14809);
        return true;
    }
}
