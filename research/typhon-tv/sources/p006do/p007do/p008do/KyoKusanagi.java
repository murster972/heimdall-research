package p006do.p007do.p008do;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import net.pubnative.library.request.PubnativeRequest;
import p006do.JhunHoon;
import p006do.KDash;
import p006do.Krizalid;
import p006do.Shermie;
import p006do.ShingoYabuki;
import p006do.Whip;
import p006do.p007do.GoroDaimon;
import p006do.p007do.p008do.GoroDaimon;
import p006do.p007do.p020for.ChoiBounge;
import p006do.p007do.p020for.LuckyGlauber;
import p009if.ChangKoehan;
import p009if.ChinGentsai;
import p009if.Chris;
import p009if.LeonaHeidern;
import p009if.YashiroNanakase;

/* renamed from: do.do.do.KyoKusanagi  reason: invalid package */
public final class KyoKusanagi implements Shermie {

    /* renamed from: 龘  reason: contains not printable characters */
    final ChangKoehan f14833;

    public KyoKusanagi(ChangKoehan changKoehan) {
        this.f14833 = changKoehan;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static JhunHoon m18531(JhunHoon jhunHoon) {
        return (jhunHoon == null || jhunHoon.m6463() == null) ? jhunHoon : jhunHoon.m6464().m6485((Krizalid) null).m6490();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private JhunHoon m18532(final BenimaruNikaido benimaruNikaido, JhunHoon jhunHoon) throws IOException {
        YashiroNanakase r0;
        if (benimaruNikaido == null || (r0 = benimaruNikaido.m18509()) == null) {
            return jhunHoon;
        }
        final ChangKoehan r1 = jhunHoon.m6463().m6496();
        final ChinGentsai r02 = LeonaHeidern.m18967(r0);
        return jhunHoon.m6464().m6485((Krizalid) new LuckyGlauber(jhunHoon.m6468(), LeonaHeidern.m18966((p009if.Shermie) new p009if.Shermie() {

            /* renamed from: 龘  reason: contains not printable characters */
            boolean f14838;

            public void close() throws IOException {
                if (!this.f14838 && !GoroDaimon.m18424((p009if.Shermie) this, 100, TimeUnit.MILLISECONDS)) {
                    this.f14838 = true;
                    benimaruNikaido.m18508();
                }
                r1.close();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public long m18535(p009if.GoroDaimon goroDaimon, long j) throws IOException {
                try {
                    long r4 = r1.m19009(goroDaimon, j);
                    if (r4 == -1) {
                        if (!this.f14838) {
                            this.f14838 = true;
                            r02.close();
                        }
                        return -1;
                    }
                    goroDaimon.m6775(r02.m18930(), goroDaimon.m6731() - r4, r4);
                    r02.m18926();
                    return r4;
                } catch (IOException e) {
                    if (!this.f14838) {
                        this.f14838 = true;
                        benimaruNikaido.m18508();
                    }
                    throw e;
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Chris m18536() {
                return r1.m19010();
            }
        }))).m6490();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ShingoYabuki m18533(ShingoYabuki shingoYabuki, ShingoYabuki shingoYabuki2) {
        ShingoYabuki.KyoKusanagi kyoKusanagi = new ShingoYabuki.KyoKusanagi();
        int r3 = shingoYabuki.m6583();
        for (int i = 0; i < r3; i++) {
            String r4 = shingoYabuki.m6584(i);
            String r5 = shingoYabuki.m6579(i);
            if ((!"Warning".equalsIgnoreCase(r4) || !r5.startsWith(PubnativeRequest.LEGACY_ZONE_ID)) && (!m18534(r4) || shingoYabuki2.m6585(r4) == null)) {
                p006do.p007do.KyoKusanagi.f14748.m18437(kyoKusanagi, r4, r5);
            }
        }
        int r1 = shingoYabuki2.m6583();
        for (int i2 = 0; i2 < r1; i2++) {
            String r32 = shingoYabuki2.m6584(i2);
            if (!"Content-Length".equalsIgnoreCase(r32) && m18534(r32)) {
                p006do.p007do.KyoKusanagi.f14748.m18437(kyoKusanagi, r32, shingoYabuki2.m6579(i2));
            }
        }
        return kyoKusanagi.m18386();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m18534(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    public JhunHoon a(Shermie.KyoKusanagi kyoKusanagi) throws IOException {
        JhunHoon jhunHoon = null;
        JhunHoon r0 = this.f14833 != null ? this.f14833.m18511(kyoKusanagi.m6577()) : jhunHoon;
        GoroDaimon r2 = new GoroDaimon.KyoKusanagi(System.currentTimeMillis(), kyoKusanagi.m6577(), r0).m18530();
        Whip whip = r2.f6130;
        JhunHoon jhunHoon2 = r2.f6129;
        if (this.f14833 != null) {
            this.f14833.m18515(r2);
        }
        if (r0 != null && jhunHoon2 == null) {
            p006do.p007do.GoroDaimon.m18422((Closeable) r0.m6463());
        }
        if (whip == null && jhunHoon2 == null) {
            return new JhunHoon.KyoKusanagi().m6487(kyoKusanagi.m6577()).m6484(KDash.HTTP_1_1).m6480(504).m6488("Unsatisfiable Request (only-if-cached)").m6485(p006do.p007do.GoroDaimon.f14742).m6481(-1).m6477(System.currentTimeMillis()).m6490();
        }
        if (whip == null) {
            return jhunHoon2.m6464().m6478(m18531(jhunHoon2)).m6490();
        }
        try {
            jhunHoon = kyoKusanagi.m6576(whip);
            if (jhunHoon2 != null) {
                if (jhunHoon.m6469() == 304) {
                    JhunHoon r02 = jhunHoon2.m6464().m6486(m18533(jhunHoon2.m6468(), jhunHoon.m6468())).m6481(jhunHoon.m6466()).m6477(jhunHoon.m6467()).m6478(m18531(jhunHoon2)).m6483(m18531(jhunHoon)).m6490();
                    jhunHoon.m6463().close();
                    this.f14833.m18513();
                    this.f14833.m18514(jhunHoon2, r02);
                    return r02;
                }
                p006do.p007do.GoroDaimon.m18422((Closeable) jhunHoon2.m6463());
            }
            JhunHoon r03 = jhunHoon.m6464().m6478(m18531(jhunHoon2)).m6483(m18531(jhunHoon)).m6490();
            if (this.f14833 == null) {
                return r03;
            }
            if (p006do.p007do.p020for.ChangKoehan.m18541(r03) && GoroDaimon.m6647(r03, whip)) {
                return m18532(this.f14833.m18512(r03), r03);
            }
            if (!ChoiBounge.m18555(whip.m6589())) {
                return r03;
            }
            try {
                this.f14833.m18510(whip);
                return r03;
            } catch (IOException e) {
                return r03;
            }
        } finally {
            if (jhunHoon == null && r0 != null) {
                p006do.p007do.GoroDaimon.m18422((Closeable) r0.m6463());
            }
        }
    }
}
