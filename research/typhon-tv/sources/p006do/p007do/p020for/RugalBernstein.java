package p006do.p007do.p020for;

import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.HttpRetryException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import org.apache.oltu.oauth2.common.OAuth;
import p006do.Bao;
import p006do.JhunHoon;
import p006do.Krizalid;
import p006do.KyoKusanagi;
import p006do.Orochi;
import p006do.Ramon;
import p006do.Shermie;
import p006do.Whip;
import p006do.YashiroNanakase;
import p006do.p007do.p021if.ChangKoehan;
import p006do.p007do.p021if.GoroDaimon;
import p006do.p007do.p021if.HeavyD;

/* renamed from: do.do.for.RugalBernstein  reason: invalid package */
public final class RugalBernstein implements Shermie {

    /* renamed from: 连任  reason: contains not printable characters */
    private volatile boolean f14857;

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f14858;

    /* renamed from: 麤  reason: contains not printable characters */
    private Object f14859;

    /* renamed from: 齉  reason: contains not printable characters */
    private HeavyD f14860;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Orochi f14861;

    public RugalBernstein(Orochi orochi, boolean z) {
        this.f14861 = orochi;
        this.f14858 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private KyoKusanagi m18573(YashiroNanakase yashiroNanakase) {
        p006do.HeavyD heavyD;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (yashiroNanakase.m6622()) {
            sSLSocketFactory = this.f14861.m6539();
            hostnameVerifier = this.f14861.m6540();
            heavyD = this.f14861.m6530();
        } else {
            heavyD = null;
            hostnameVerifier = null;
            sSLSocketFactory = null;
        }
        return new KyoKusanagi(yashiroNanakase.m6608(), yashiroNanakase.m6609(), this.f14861.m6526(), this.f14861.m6536(), sSLSocketFactory, hostnameVerifier, heavyD, this.f14861.m6528(), this.f14861.m6544(), this.f14861.m6529(), this.f14861.m6531(), this.f14861.m6542());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Whip m18574(JhunHoon jhunHoon) throws IOException {
        String r0;
        YashiroNanakase r02;
        Bao bao = null;
        if (jhunHoon == null) {
            throw new IllegalStateException();
        }
        GoroDaimon r03 = this.f14860.m18622();
        Ramon r04 = r03 != null ? r03.m18333() : null;
        int r2 = jhunHoon.m6469();
        String r3 = jhunHoon.m6472().m6589();
        switch (r2) {
            case 300:
            case 301:
            case 302:
            case 303:
                break;
            case 307:
            case 308:
                if (!r3.equals(OAuth.HttpMethod.GET) && !r3.equals("HEAD")) {
                    return null;
                }
            case 401:
                return this.f14861.m6527().m6429(r04, jhunHoon);
            case 407:
                if ((r04 != null ? r04.m18376() : this.f14861.m6544()).type() == Proxy.Type.HTTP) {
                    return this.f14861.m6528().m6429(r04, jhunHoon);
                }
                throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
            case 408:
                if (!(jhunHoon.m6472().m6590() instanceof SaishuKusanagi)) {
                    return jhunHoon.m6472();
                }
                return null;
            default:
                return null;
        }
        if (!this.f14861.m6532() || (r0 = jhunHoon.m6473("Location")) == null || (r02 = jhunHoon.m6472().m6592().m6621(r0)) == null) {
            return null;
        }
        if (!r02.m6618().equals(jhunHoon.m6472().m6592().m6618()) && !this.f14861.m6549()) {
            return null;
        }
        Whip.KyoKusanagi r22 = jhunHoon.m6472().m6588();
        if (ChoiBounge.m18554(r3)) {
            boolean r4 = ChoiBounge.m18553(r3);
            if (ChoiBounge.m18551(r3)) {
                r22.m18399(OAuth.HttpMethod.GET, (Bao) null);
            } else {
                if (r4) {
                    bao = jhunHoon.m6472().m6590();
                }
                r22.m18399(r3, bao);
            }
            if (!r4) {
                r22.m18392("Transfer-Encoding");
                r22.m18392("Content-Length");
                r22.m18392(OAuth.HeaderType.CONTENT_TYPE);
            }
        }
        if (!m18575(jhunHoon, r02)) {
            r22.m18392(OAuth.HeaderType.AUTHORIZATION);
        }
        return r22.m18397(r02).m18393();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m18575(JhunHoon jhunHoon, YashiroNanakase yashiroNanakase) {
        YashiroNanakase r0 = jhunHoon.m6472().m6592();
        return r0.m6608().equals(yashiroNanakase.m6608()) && r0.m6609() == yashiroNanakase.m6609() && r0.m6618().equals(yashiroNanakase.m6618());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m18576(IOException iOException, boolean z) {
        boolean z2 = true;
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (!(iOException instanceof InterruptedIOException)) {
            return (!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException);
        }
        if (!(iOException instanceof SocketTimeoutException) || z) {
            z2 = false;
        }
        return z2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m18577(IOException iOException, boolean z, Whip whip) {
        this.f14860.m18629(iOException);
        if (!this.f14861.m6533()) {
            return false;
        }
        return (!z || !(whip.m6590() instanceof SaishuKusanagi)) && m18576(iOException, z) && this.f14860.m18621();
    }

    public JhunHoon a(Shermie.KyoKusanagi kyoKusanagi) throws IOException {
        Whip r1 = kyoKusanagi.m6577();
        this.f14860 = new HeavyD(this.f14861.m6548(), m18573(r1.m6592()), this.f14859);
        JhunHoon jhunHoon = null;
        int i = 0;
        Whip whip = r1;
        while (!this.f14857) {
            try {
                JhunHoon r12 = ((HeavyD) kyoKusanagi).m18566(whip, this.f14860, (GoroDaimon) null, (GoroDaimon) null);
                if (jhunHoon != null) {
                    r12 = r12.m6464().m6479(jhunHoon.m6464().m6485((Krizalid) null).m6490()).m6490();
                }
                whip = m18574(r12);
                if (whip == null) {
                    if (!this.f14858) {
                        this.f14860.m18625();
                    }
                    return r12;
                }
                p006do.p007do.GoroDaimon.m18422((Closeable) r12.m6463());
                i++;
                if (i > 20) {
                    this.f14860.m18625();
                    throw new ProtocolException("Too many follow-up requests: " + i);
                } else if (whip.m6590() instanceof SaishuKusanagi) {
                    this.f14860.m18625();
                    throw new HttpRetryException("Cannot retry streamed HTTP body", r12.m6469());
                } else {
                    if (!m18575(r12, whip.m6592())) {
                        this.f14860.m18625();
                        this.f14860 = new HeavyD(this.f14861.m6548(), m18573(whip.m6592()), this.f14859);
                    } else if (this.f14860.m18626() != null) {
                        throw new IllegalStateException("Closing the body of " + r12 + " didn't close its backing stream. Bad interceptor?");
                    }
                    jhunHoon = r12;
                }
            } catch (ChangKoehan e) {
                if (!m18577(e.a(), false, whip)) {
                    throw e.a();
                }
            } catch (IOException e2) {
                if (!m18577(e2, !(e2 instanceof p006do.p007do.p023new.KyoKusanagi), whip)) {
                    throw e2;
                }
            } catch (Throwable th) {
                this.f14860.m18629((IOException) null);
                this.f14860.m18625();
                throw th;
            }
        }
        this.f14860.m18625();
        throw new IOException("Canceled");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18578(Object obj) {
        this.f14859 = obj;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18579() {
        return this.f14857;
    }
}
