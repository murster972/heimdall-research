package p006do.p007do.p020for;

import java.io.IOException;
import java.net.ProtocolException;
import p006do.KDash;

/* renamed from: do.do.for.IoriYagami  reason: invalid package */
public final class IoriYagami {

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f14851;

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f14852;

    /* renamed from: 龘  reason: contains not printable characters */
    public final KDash f14853;

    public IoriYagami(KDash kDash, int i, String str) {
        this.f14853 = kDash;
        this.f14851 = i;
        this.f14852 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static IoriYagami m18568(String str) throws IOException {
        KDash kDash;
        String str2;
        int i = 9;
        if (str.startsWith("HTTP/1.")) {
            if (str.length() < 9 || str.charAt(8) != ' ') {
                throw new ProtocolException("Unexpected status line: " + str);
            }
            int charAt = str.charAt(7) - '0';
            if (charAt == 0) {
                kDash = KDash.HTTP_1_0;
            } else if (charAt == 1) {
                kDash = KDash.HTTP_1_1;
            } else {
                throw new ProtocolException("Unexpected status line: " + str);
            }
        } else if (str.startsWith("ICY ")) {
            kDash = KDash.HTTP_1_0;
            i = 4;
        } else {
            throw new ProtocolException("Unexpected status line: " + str);
        }
        if (str.length() < i + 3) {
            throw new ProtocolException("Unexpected status line: " + str);
        }
        try {
            int parseInt = Integer.parseInt(str.substring(i, i + 3));
            if (str.length() <= i + 3) {
                str2 = "";
            } else if (str.charAt(i + 3) != ' ') {
                throw new ProtocolException("Unexpected status line: " + str);
            } else {
                str2 = str.substring(i + 4);
            }
            return new IoriYagami(kDash, parseInt, str2);
        } catch (NumberFormatException e) {
            throw new ProtocolException("Unexpected status line: " + str);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f14853 == KDash.HTTP_1_0 ? "HTTP/1.0" : "HTTP/1.1");
        sb.append(' ').append(this.f14851);
        if (this.f14852 != null) {
            sb.append(' ').append(this.f14852);
        }
        return sb.toString();
    }
}
