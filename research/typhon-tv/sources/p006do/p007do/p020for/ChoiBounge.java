package p006do.p007do.p020for;

import com.mopub.volley.toolbox.HttpClientStack;
import org.apache.oltu.oauth2.common.OAuth;

/* renamed from: do.do.for.ChoiBounge  reason: invalid package */
public final class ChoiBounge {
    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean m18551(String str) {
        return !str.equals("PROPFIND");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m18552(String str) {
        return str.equals(OAuth.HttpMethod.POST) || str.equals(OAuth.HttpMethod.PUT) || str.equals(HttpClientStack.HttpPatch.METHOD_NAME) || str.equals("PROPPATCH") || str.equals("REPORT");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m18553(String str) {
        return str.equals("PROPFIND");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m18554(String str) {
        return m18552(str) || str.equals("OPTIONS") || str.equals(OAuth.HttpMethod.DELETE) || str.equals("PROPFIND") || str.equals("MKCOL") || str.equals("LOCK");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m18555(String str) {
        return str.equals(OAuth.HttpMethod.POST) || str.equals(HttpClientStack.HttpPatch.METHOD_NAME) || str.equals(OAuth.HttpMethod.PUT) || str.equals(OAuth.HttpMethod.DELETE) || str.equals("MOVE");
    }
}
