package p006do.p007do.p020for;

import java.io.IOException;
import java.net.ProtocolException;
import p006do.JhunHoon;
import p006do.Shermie;
import p006do.Whip;
import p006do.p007do.p021if.GoroDaimon;
import p006do.p007do.p021if.HeavyD;
import p009if.ChinGentsai;
import p009if.LeonaHeidern;

/* renamed from: do.do.for.BenimaruNikaido  reason: invalid package */
public final class BenimaruNikaido implements Shermie {

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f14839;

    public BenimaruNikaido(boolean z) {
        this.f14839 = z;
    }

    public JhunHoon a(Shermie.KyoKusanagi kyoKusanagi) throws IOException {
        JhunHoon.KyoKusanagi kyoKusanagi2;
        HeavyD heavyD = (HeavyD) kyoKusanagi;
        GoroDaimon r2 = heavyD.m18563();
        HeavyD r3 = heavyD.m18564();
        GoroDaimon goroDaimon = (GoroDaimon) heavyD.m18562();
        Whip r4 = heavyD.m18567();
        long currentTimeMillis = System.currentTimeMillis();
        r2.m18561(r4);
        JhunHoon.KyoKusanagi kyoKusanagi3 = null;
        if (!ChoiBounge.m18554(r4.m6589()) || r4.m6590() == null) {
            kyoKusanagi2 = null;
        } else {
            if ("100-continue".equalsIgnoreCase(r4.m6593("Expect"))) {
                r2.m18560();
                kyoKusanagi3 = r2.m18557(true);
            }
            if (kyoKusanagi3 == null) {
                ChinGentsai r0 = LeonaHeidern.m18967(r2.m18559(r4, r4.m6590().b()));
                r4.m6590().a(r0);
                r0.close();
                kyoKusanagi2 = kyoKusanagi3;
            } else {
                if (!goroDaimon.m18606()) {
                    r3.m18624();
                }
                kyoKusanagi2 = kyoKusanagi3;
            }
        }
        r2.m18556();
        if (kyoKusanagi2 == null) {
            kyoKusanagi2 = r2.m18557(false);
        }
        JhunHoon r02 = kyoKusanagi2.m6487(r4).m6482(r3.m18622().m18607()).m6481(currentTimeMillis).m6477(System.currentTimeMillis()).m6490();
        int r1 = r02.m6469();
        JhunHoon r03 = (!this.f14839 || r1 != 101) ? r02.m6464().m6485(r2.m18558(r02)).m6490() : r02.m6464().m6485(p006do.p007do.GoroDaimon.f14742).m6490();
        if ("close".equalsIgnoreCase(r03.m6472().m6593("Connection")) || "close".equalsIgnoreCase(r03.m6473("Connection"))) {
            r3.m18624();
        }
        if ((r1 != 204 && r1 != 205) || r03.m6463().m6495() <= 0) {
            return r03;
        }
        throw new ProtocolException("HTTP " + r1 + " had non-zero Content-Length: " + r03.m6463().m6495());
    }
}
