package p006do.p007do.p020for;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.List;
import java.util.regex.Pattern;
import p006do.JhunHoon;
import p006do.LeonaHeidern;
import p006do.SaishuKusanagi;
import p006do.ShingoYabuki;
import p006do.YashiroNanakase;

/* renamed from: do.do.for.ChangKoehan  reason: invalid package */
public final class ChangKoehan {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f14840 = Pattern.compile(" +([^ \"=]*)=(:?\"([^\"]*)\"|([^ \"=]*)) *(:?,|$)");

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m18540(String str, int i) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 2147483647L) {
                return MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
            }
            if (parseLong < 0) {
                return 0;
            }
            return (int) parseLong;
        } catch (NumberFormatException e) {
            return i;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m18541(JhunHoon jhunHoon) {
        if (jhunHoon.m6472().m6589().equals("HEAD")) {
            return false;
        }
        int r2 = jhunHoon.m6469();
        if ((r2 >= 100 && r2 < 200) || r2 == 204 || r2 == 304) {
            return m18544(jhunHoon) != -1 || "chunked".equalsIgnoreCase(jhunHoon.m6473("Transfer-Encoding"));
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m18542(String str, int i) {
        while (i < str.length() && ((r0 = str.charAt(i)) == ' ' || r0 == 9)) {
            i++;
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m18543(String str, int i, String str2) {
        while (i < str.length() && str2.indexOf(str.charAt(i)) == -1) {
            i++;
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m18544(JhunHoon jhunHoon) {
        return m18545(jhunHoon.m6468());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m18545(ShingoYabuki shingoYabuki) {
        return m18546(shingoYabuki.m6585("Content-Length"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m18546(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m18547(LeonaHeidern leonaHeidern, YashiroNanakase yashiroNanakase, ShingoYabuki shingoYabuki) {
        if (leonaHeidern != LeonaHeidern.f14586) {
            List<SaishuKusanagi> r0 = SaishuKusanagi.m6570(yashiroNanakase, shingoYabuki);
            if (!r0.isEmpty()) {
                leonaHeidern.m18350(yashiroNanakase, r0);
            }
        }
    }
}
