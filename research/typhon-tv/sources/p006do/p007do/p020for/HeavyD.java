package p006do.p007do.p020for;

import java.io.IOException;
import java.util.List;
import p006do.BrianBattler;
import p006do.JhunHoon;
import p006do.Shermie;
import p006do.Whip;
import p006do.p007do.p021if.GoroDaimon;

/* renamed from: do.do.for.HeavyD  reason: invalid package */
public final class HeavyD implements Shermie.KyoKusanagi {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Whip f14844;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f14845;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f14846;

    /* renamed from: 靐  reason: contains not printable characters */
    private final p006do.p007do.p021if.HeavyD f14847;

    /* renamed from: 麤  reason: contains not printable characters */
    private final GoroDaimon f14848;

    /* renamed from: 齉  reason: contains not printable characters */
    private final GoroDaimon f14849;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<Shermie> f14850;

    public HeavyD(List<Shermie> list, p006do.p007do.p021if.HeavyD heavyD, GoroDaimon goroDaimon, GoroDaimon goroDaimon2, int i, Whip whip) {
        this.f14850 = list;
        this.f14848 = goroDaimon2;
        this.f14847 = heavyD;
        this.f14849 = goroDaimon;
        this.f14846 = i;
        this.f14844 = whip;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public BrianBattler m18562() {
        return this.f14848;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public GoroDaimon m18563() {
        return this.f14849;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public p006do.p007do.p021if.HeavyD m18564() {
        return this.f14847;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public JhunHoon m18565(Whip whip) throws IOException {
        return m18566(whip, this.f14847, this.f14849, this.f14848);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public JhunHoon m18566(Whip whip, p006do.p007do.p021if.HeavyD heavyD, GoroDaimon goroDaimon, GoroDaimon goroDaimon2) throws IOException {
        if (this.f14846 >= this.f14850.size()) {
            throw new AssertionError();
        }
        this.f14845++;
        if (this.f14849 != null && !this.f14848.m18614(whip.m6592())) {
            throw new IllegalStateException("network interceptor " + this.f14850.get(this.f14846 - 1) + " must retain the same host and port");
        } else if (this.f14849 == null || this.f14845 <= 1) {
            HeavyD heavyD2 = new HeavyD(this.f14850, heavyD, goroDaimon, goroDaimon2, this.f14846 + 1, whip);
            Shermie shermie = this.f14850.get(this.f14846);
            JhunHoon a = shermie.a(heavyD2);
            if (goroDaimon != null && this.f14846 + 1 < this.f14850.size() && heavyD2.f14845 != 1) {
                throw new IllegalStateException("network interceptor " + shermie + " must call proceed() exactly once");
            } else if (a != null) {
                return a;
            } else {
                throw new NullPointerException("interceptor " + shermie + " returned null");
            }
        } else {
            throw new IllegalStateException("network interceptor " + this.f14850.get(this.f14846 - 1) + " must call proceed() exactly once");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Whip m18567() {
        return this.f14844;
    }
}
