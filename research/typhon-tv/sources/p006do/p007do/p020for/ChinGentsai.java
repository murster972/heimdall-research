package p006do.p007do.p020for;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import p006do.p007do.GoroDaimon;

/* renamed from: do.do.for.ChinGentsai  reason: invalid package */
public final class ChinGentsai {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f14841 = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z", "EEE MMM d yyyy HH:mm:ss z"};

    /* renamed from: 齉  reason: contains not printable characters */
    private static final DateFormat[] f14842 = new DateFormat[f14841.length];

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ThreadLocal<DateFormat> f14843 = new ThreadLocal<DateFormat>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public DateFormat initialValue() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
            simpleDateFormat.setLenient(false);
            simpleDateFormat.setTimeZone(GoroDaimon.f14729);
            return simpleDateFormat;
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m18548(Date date) {
        return f14843.get().format(date);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Date m18549(String str) {
        if (str.length() == 0) {
            return null;
        }
        ParsePosition parsePosition = new ParsePosition(0);
        Date parse = f14843.get().parse(str, parsePosition);
        if (parsePosition.getIndex() == str.length()) {
            return parse;
        }
        synchronized (f14841) {
            int length = f14841.length;
            for (int i = 0; i < length; i++) {
                DateFormat dateFormat = f14842[i];
                if (dateFormat == null) {
                    dateFormat = new SimpleDateFormat(f14841[i], Locale.US);
                    dateFormat.setTimeZone(GoroDaimon.f14729);
                    f14842[i] = dateFormat;
                }
                parsePosition.setIndex(0);
                Date parse2 = dateFormat.parse(str, parsePosition);
                if (parsePosition.getIndex() != 0) {
                    return parse2;
                }
            }
            return null;
        }
    }
}
