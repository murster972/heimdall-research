package p006do.p007do.p020for;

import java.net.Proxy;
import p006do.Whip;
import p006do.YashiroNanakase;

/* renamed from: do.do.for.BrianBattler  reason: invalid package */
public final class BrianBattler {
    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m18537(Whip whip, Proxy.Type type) {
        return !whip.m6587() && type == Proxy.Type.HTTP;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m18538(Whip whip, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(whip.m6589());
        sb.append(' ');
        if (m18537(whip, type)) {
            sb.append(whip.m6592());
        } else {
            sb.append(m18539(whip.m6592()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m18539(YashiroNanakase yashiroNanakase) {
        String r0 = yashiroNanakase.m6610();
        String r1 = yashiroNanakase.m6615();
        return r1 != null ? r0 + '?' + r1 : r0;
    }
}
