package p006do.p007do.p020for;

import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.IOException;
import java.util.List;
import org.apache.oltu.oauth2.common.OAuth;
import p006do.Bao;
import p006do.Chris;
import p006do.JhunHoon;
import p006do.Krizalid;
import p006do.LeonaHeidern;
import p006do.SaishuKusanagi;
import p006do.Shermie;
import p006do.ShingoYabuki;
import p006do.Whip;
import p006do.p007do.ChinGentsai;
import p006do.p007do.GoroDaimon;
import p009if.IoriYagami;

/* renamed from: do.do.for.KyoKusanagi  reason: invalid package */
public final class KyoKusanagi implements Shermie {

    /* renamed from: 龘  reason: contains not printable characters */
    private final LeonaHeidern f14854;

    public KyoKusanagi(LeonaHeidern leonaHeidern) {
        this.f14854 = leonaHeidern;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m18569(List<SaishuKusanagi> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            SaishuKusanagi saishuKusanagi = list.get(i);
            sb.append(saishuKusanagi.m6573()).append('=').append(saishuKusanagi.m6572());
        }
        return sb.toString();
    }

    public JhunHoon a(Shermie.KyoKusanagi kyoKusanagi) throws IOException {
        boolean z = false;
        Whip r1 = kyoKusanagi.m6577();
        Whip.KyoKusanagi r2 = r1.m6588();
        Bao r3 = r1.m6590();
        if (r3 != null) {
            Chris a = r3.a();
            if (a != null) {
                r2.m18400(OAuth.HeaderType.CONTENT_TYPE, a.toString());
            }
            long b = r3.b();
            if (b != -1) {
                r2.m18400("Content-Length", Long.toString(b));
                r2.m18392("Transfer-Encoding");
            } else {
                r2.m18400("Transfer-Encoding", "chunked");
                r2.m18392("Content-Length");
            }
        }
        if (r1.m6593("Host") == null) {
            r2.m18400("Host", GoroDaimon.m18414(r1.m6592(), false));
        }
        if (r1.m6593("Connection") == null) {
            r2.m18400("Connection", "Keep-Alive");
        }
        if (r1.m6593("Accept-Encoding") == null && r1.m6593("Range") == null) {
            z = true;
            r2.m18400("Accept-Encoding", "gzip");
        }
        List<SaishuKusanagi> r32 = this.f14854.m18349(r1.m6592());
        if (!r32.isEmpty()) {
            r2.m18400("Cookie", m18569(r32));
        }
        if (r1.m6593(AbstractSpiCall.HEADER_USER_AGENT) == null) {
            r2.m18400(AbstractSpiCall.HEADER_USER_AGENT, ChinGentsai.m18402());
        }
        JhunHoon r22 = kyoKusanagi.m6576(r2.m18393());
        ChangKoehan.m18547(this.f14854, r1.m6592(), r22.m6468());
        JhunHoon.KyoKusanagi r12 = r22.m6464().m6487(r1);
        if (z && "gzip".equalsIgnoreCase(r22.m6473("Content-Encoding")) && ChangKoehan.m18541(r22)) {
            IoriYagami ioriYagami = new IoriYagami(r22.m6463().m6496());
            ShingoYabuki r23 = r22.m6468().m6582().m18381("Content-Encoding").m18381("Content-Length").m18386();
            r12.m6486(r23);
            r12.m6485((Krizalid) new LuckyGlauber(r23, p009if.LeonaHeidern.m18966((p009if.Shermie) ioriYagami)));
        }
        return r12.m6490();
    }
}
