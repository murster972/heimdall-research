package p006do.p007do;

import java.net.Socket;
import javax.net.ssl.SSLSocket;
import p006do.IoriYagami;
import p006do.JhunHoon;
import p006do.Ramon;
import p006do.RugalBernstein;
import p006do.ShingoYabuki;
import p006do.p007do.p021if.ChinGentsai;
import p006do.p007do.p021if.GoroDaimon;
import p006do.p007do.p021if.HeavyD;

/* renamed from: do.do.KyoKusanagi  reason: invalid package */
public abstract class KyoKusanagi {

    /* renamed from: 龘  reason: contains not printable characters */
    public static KyoKusanagi f14748;

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m18430(RugalBernstein rugalBernstein, GoroDaimon goroDaimon);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m18431(JhunHoon.KyoKusanagi kyoKusanagi);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ChinGentsai m18432(RugalBernstein rugalBernstein);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract GoroDaimon m18433(RugalBernstein rugalBernstein, p006do.KyoKusanagi kyoKusanagi, HeavyD heavyD, Ramon ramon);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Socket m18434(RugalBernstein rugalBernstein, p006do.KyoKusanagi kyoKusanagi, HeavyD heavyD);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m18435(IoriYagami ioriYagami, SSLSocket sSLSocket, boolean z);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m18436(ShingoYabuki.KyoKusanagi kyoKusanagi, String str);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m18437(ShingoYabuki.KyoKusanagi kyoKusanagi, String str, String str2);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m18438(p006do.KyoKusanagi kyoKusanagi, p006do.KyoKusanagi kyoKusanagi2);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m18439(RugalBernstein rugalBernstein, GoroDaimon goroDaimon);
}
