package p006do.p007do.p024try;

import java.io.File;
import java.io.IOException;

/* renamed from: do.do.try.KyoKusanagi  reason: invalid package */
public interface KyoKusanagi {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final KyoKusanagi f15096 = new KyoKusanagi() {
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m18850(File file) {
            return file.exists();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public long m18851(File file) {
            return file.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18852(File file) throws IOException {
            if (!file.delete() && file.exists()) {
                throw new IOException("failed to delete " + file);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18853(File file, File file2) throws IOException {
            m18852(file2);
            if (!file.renameTo(file2)) {
                throw new IOException("failed to rename " + file + " to " + file2);
            }
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m18846(File file);

    /* renamed from: 齉  reason: contains not printable characters */
    long m18847(File file);

    /* renamed from: 龘  reason: contains not printable characters */
    void m18848(File file) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m18849(File file, File file2) throws IOException;
}
