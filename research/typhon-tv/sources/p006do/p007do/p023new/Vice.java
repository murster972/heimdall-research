package p006do.p007do.p023new;

import java.io.IOException;

/* renamed from: do.do.new.Vice  reason: invalid package */
public final class Vice extends IOException {
    public final BenimaruNikaido a;

    public Vice(BenimaruNikaido benimaruNikaido) {
        super("stream was reset: " + benimaruNikaido);
        this.a = benimaruNikaido;
    }
}
