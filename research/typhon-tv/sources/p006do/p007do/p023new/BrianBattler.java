package p006do.p007do.p023new;

import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import p009if.ChangKoehan;
import p009if.Chris;
import p009if.Shermie;
import p009if.YashiroNanakase;

/* renamed from: do.do.new.BrianBattler  reason: invalid package */
public final class BrianBattler {

    /* renamed from: ˑ  reason: contains not printable characters */
    static final /* synthetic */ boolean f14928 = (!BrianBattler.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    final GoroDaimon f14929 = new GoroDaimon();

    /* renamed from: ʼ  reason: contains not printable characters */
    final GoroDaimon f14930 = new GoroDaimon();

    /* renamed from: ʽ  reason: contains not printable characters */
    BenimaruNikaido f14931 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final BenimaruNikaido f14932;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f14933;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final List<GoroDaimon> f14934;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private List<GoroDaimon> f14935;

    /* renamed from: 连任  reason: contains not printable characters */
    final KyoKusanagi f14936;

    /* renamed from: 靐  reason: contains not printable characters */
    long f14937;

    /* renamed from: 麤  reason: contains not printable characters */
    final HeavyD f14938;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f14939;

    /* renamed from: 龘  reason: contains not printable characters */
    long f14940 = 0;

    /* renamed from: do.do.new.BrianBattler$BenimaruNikaido */
    private final class BenimaruNikaido implements Shermie {

        /* renamed from: 齉  reason: contains not printable characters */
        static final /* synthetic */ boolean f14941 = (!BrianBattler.class.desiredAssertionStatus());

        /* renamed from: ʻ  reason: contains not printable characters */
        private final p009if.GoroDaimon f14942 = new p009if.GoroDaimon();

        /* renamed from: ʼ  reason: contains not printable characters */
        private final long f14943;

        /* renamed from: 连任  reason: contains not printable characters */
        private final p009if.GoroDaimon f14944 = new p009if.GoroDaimon();

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f14945;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f14947;

        BenimaruNikaido(long j) {
            this.f14943 = j;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m18673() throws IOException {
            BrianBattler.this.f14929.m6789();
            while (this.f14942.m6731() == 0 && !this.f14945 && !this.f14947 && BrianBattler.this.f14931 == null) {
                try {
                    BrianBattler.this.m18658();
                } finally {
                    BrianBattler.this.f14929.m18678();
                }
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private void m18674() throws IOException {
            if (this.f14947) {
                throw new IOException("stream closed");
            } else if (BrianBattler.this.f14931 != null) {
                throw new Vice(BrianBattler.this.f14931);
            }
        }

        public void close() throws IOException {
            synchronized (BrianBattler.this) {
                this.f14947 = true;
                this.f14942.m6732();
                BrianBattler.this.notifyAll();
            }
            BrianBattler.this.m18660();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m18675(p009if.GoroDaimon goroDaimon, long j) throws IOException {
            long r0;
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            }
            synchronized (BrianBattler.this) {
                m18673();
                m18674();
                if (this.f14942.m6731() == 0) {
                    r0 = -1;
                } else {
                    r0 = this.f14942.m6768(goroDaimon, Math.min(j, this.f14942.m6731()));
                    BrianBattler.this.f14940 += r0;
                    if (BrianBattler.this.f14940 >= ((long) (BrianBattler.this.f14938.f15010.m18818() / 2))) {
                        BrianBattler.this.f14938.m18735(BrianBattler.this.f14939, BrianBattler.this.f14940);
                        BrianBattler.this.f14940 = 0;
                    }
                    synchronized (BrianBattler.this.f14938) {
                        BrianBattler.this.f14938.f15016 += r0;
                        if (BrianBattler.this.f14938.f15016 >= ((long) (BrianBattler.this.f14938.f15010.m18818() / 2))) {
                            BrianBattler.this.f14938.m18735(0, BrianBattler.this.f14938.f15016);
                            BrianBattler.this.f14938.f15016 = 0;
                        }
                    }
                }
            }
            return r0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Chris m18676() {
            return BrianBattler.this.f14929;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18677(ChangKoehan changKoehan, long j) throws IOException {
            boolean z;
            boolean z2;
            if (f14941 || !Thread.holdsLock(BrianBattler.this)) {
                while (j > 0) {
                    synchronized (BrianBattler.this) {
                        z = this.f14945;
                        z2 = this.f14942.m6731() + j > this.f14943;
                    }
                    if (z2) {
                        changKoehan.m6704(j);
                        BrianBattler.this.m18663(BenimaruNikaido.FLOW_CONTROL_ERROR);
                        return;
                    } else if (z) {
                        changKoehan.m6704(j);
                        return;
                    } else {
                        long r4 = changKoehan.m19009(this.f14944, j);
                        if (r4 == -1) {
                            throw new EOFException();
                        }
                        j -= r4;
                        synchronized (BrianBattler.this) {
                            boolean z3 = this.f14942.m6731() == 0;
                            this.f14942.m6769((Shermie) this.f14944);
                            if (z3) {
                                BrianBattler.this.notifyAll();
                            }
                        }
                    }
                }
                return;
            }
            throw new AssertionError();
        }
    }

    /* renamed from: do.do.new.BrianBattler$GoroDaimon */
    class GoroDaimon extends p009if.KyoKusanagi {
        GoroDaimon() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m18678() throws IOException {
            if (m6790()) {
                throw m18679((IOException) null);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public IOException m18679(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18680() {
            BrianBattler.this.m18663(BenimaruNikaido.CANCEL);
        }
    }

    /* renamed from: do.do.new.BrianBattler$KyoKusanagi */
    final class KyoKusanagi implements YashiroNanakase {

        /* renamed from: 齉  reason: contains not printable characters */
        static final /* synthetic */ boolean f14949 = (!BrianBattler.class.desiredAssertionStatus());

        /* renamed from: 连任  reason: contains not printable characters */
        private final p009if.GoroDaimon f14950 = new p009if.GoroDaimon();

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f14951;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f14953;

        KyoKusanagi() {
        }

        /* JADX INFO: finally extract failed */
        /* renamed from: 龘  reason: contains not printable characters */
        private void m18681(boolean z) throws IOException {
            long min;
            synchronized (BrianBattler.this) {
                BrianBattler.this.f14930.m6789();
                while (BrianBattler.this.f14937 <= 0 && !this.f14951 && !this.f14953 && BrianBattler.this.f14931 == null) {
                    try {
                        BrianBattler.this.m18658();
                    } catch (Throwable th) {
                        BrianBattler.this.f14930.m18678();
                        throw th;
                    }
                }
                BrianBattler.this.f14930.m18678();
                BrianBattler.this.m18661();
                min = Math.min(BrianBattler.this.f14937, this.f14950.m6731());
                BrianBattler.this.f14937 -= min;
            }
            BrianBattler.this.f14930.m6789();
            try {
                BrianBattler.this.f14938.m18740(BrianBattler.this.f14939, z && min == this.f14950.m6731(), this.f14950, min);
            } finally {
                BrianBattler.this.f14930.m18678();
            }
        }

        public void a_(p009if.GoroDaimon goroDaimon, long j) throws IOException {
            if (f14949 || !Thread.holdsLock(BrianBattler.this)) {
                this.f14950.a_(goroDaimon, j);
                while (this.f14950.m6731() >= 16384) {
                    m18681(false);
                }
                return;
            }
            throw new AssertionError();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
            if (r6.f14952.f14936.f14951 != false) goto L_0x004e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
            if (r6.f14950.m6731() <= 0) goto L_0x0042;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
            if (r6.f14950.m6731() <= 0) goto L_0x004e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
            m18681(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0042, code lost:
            r6.f14952.f14938.m18740(r6.f14952.f14939, true, (p009if.GoroDaimon) null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x004e, code lost:
            r1 = r6.f14952;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0050, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            r6.f14953 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0054, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0055, code lost:
            r6.f14952.f14938.m18724();
            r6.f14952.m18660();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() throws java.io.IOException {
            /*
                r6 = this;
                r4 = 0
                r2 = 1
                boolean r0 = f14949
                if (r0 != 0) goto L_0x0015
                do.do.new.BrianBattler r0 = p006do.p007do.p023new.BrianBattler.this
                boolean r0 = java.lang.Thread.holdsLock(r0)
                if (r0 == 0) goto L_0x0015
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x0015:
                do.do.new.BrianBattler r1 = p006do.p007do.p023new.BrianBattler.this
                monitor-enter(r1)
                boolean r0 = r6.f14953     // Catch:{ all -> 0x003f }
                if (r0 == 0) goto L_0x001e
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
            L_0x001d:
                return
            L_0x001e:
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                do.do.new.BrianBattler r0 = p006do.p007do.p023new.BrianBattler.this
                do.do.new.BrianBattler$KyoKusanagi r0 = r0.f14936
                boolean r0 = r0.f14951
                if (r0 != 0) goto L_0x004e
                if.GoroDaimon r0 = r6.f14950
                long r0 = r0.m6731()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0042
            L_0x0031:
                if.GoroDaimon r0 = r6.f14950
                long r0 = r0.m6731()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x004e
                r6.m18681(r2)
                goto L_0x0031
            L_0x003f:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                throw r0
            L_0x0042:
                do.do.new.BrianBattler r0 = p006do.p007do.p023new.BrianBattler.this
                do.do.new.HeavyD r0 = r0.f14938
                do.do.new.BrianBattler r1 = p006do.p007do.p023new.BrianBattler.this
                int r1 = r1.f14939
                r3 = 0
                r0.m18740((int) r1, (boolean) r2, (p009if.GoroDaimon) r3, (long) r4)
            L_0x004e:
                do.do.new.BrianBattler r1 = p006do.p007do.p023new.BrianBattler.this
                monitor-enter(r1)
                r0 = 1
                r6.f14953 = r0     // Catch:{ all -> 0x0062 }
                monitor-exit(r1)     // Catch:{ all -> 0x0062 }
                do.do.new.BrianBattler r0 = p006do.p007do.p023new.BrianBattler.this
                do.do.new.HeavyD r0 = r0.f14938
                r0.m18724()
                do.do.new.BrianBattler r0 = p006do.p007do.p023new.BrianBattler.this
                r0.m18660()
                goto L_0x001d
            L_0x0062:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0062 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: p006do.p007do.p023new.BrianBattler.KyoKusanagi.close():void");
        }

        public void flush() throws IOException {
            if (f14949 || !Thread.holdsLock(BrianBattler.this)) {
                synchronized (BrianBattler.this) {
                    BrianBattler.this.m18661();
                }
                while (this.f14950.m6731() > 0) {
                    m18681(false);
                    BrianBattler.this.f14938.m18724();
                }
                return;
            }
            throw new AssertionError();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Chris m18682() {
            return BrianBattler.this.f14930;
        }
    }

    BrianBattler(int i, HeavyD heavyD, boolean z, boolean z2, List<GoroDaimon> list) {
        if (heavyD == null) {
            throw new NullPointerException("connection == null");
        } else if (list == null) {
            throw new NullPointerException("requestHeaders == null");
        } else {
            this.f14939 = i;
            this.f14938 = heavyD;
            this.f14937 = (long) heavyD.f15007.m18818();
            this.f14932 = new BenimaruNikaido((long) heavyD.f15010.m18818());
            this.f14936 = new KyoKusanagi();
            this.f14932.f14945 = z2;
            this.f14936.f14951 = z;
            this.f14934 = list;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m18654(BenimaruNikaido benimaruNikaido) {
        if (f14928 || !Thread.holdsLock(this)) {
            synchronized (this) {
                if (this.f14931 != null) {
                    return false;
                }
                if (this.f14932.f14945 && this.f14936.f14951) {
                    return false;
                }
                this.f14931 = benimaruNikaido;
                notifyAll();
                this.f14938.m18723(this.f14939);
                return true;
            }
        }
        throw new AssertionError();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Chris m18655() {
        return this.f14930;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Shermie m18656() {
        return this.f14932;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public YashiroNanakase m18657() {
        synchronized (this) {
            if (!this.f14933 && !m18667()) {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.f14936;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m18658() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException e) {
            throw new InterruptedIOException();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m18659() {
        boolean r0;
        if (f14928 || !Thread.holdsLock(this)) {
            synchronized (this) {
                this.f14932.f14945 = true;
                r0 = m18664();
                notifyAll();
            }
            if (!r0) {
                this.f14938.m18723(this.f14939);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m18660() throws IOException {
        boolean z;
        boolean r1;
        if (f14928 || !Thread.holdsLock(this)) {
            synchronized (this) {
                z = !this.f14932.f14945 && this.f14932.f14947 && (this.f14936.f14951 || this.f14936.f14953);
                r1 = m18664();
            }
            if (z) {
                m18670(BenimaruNikaido.CANCEL);
            } else if (!r1) {
                this.f14938.m18723(this.f14939);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m18661() throws IOException {
        if (this.f14936.f14953) {
            throw new IOException("stream closed");
        } else if (this.f14936.f14951) {
            throw new IOException("stream finished");
        } else if (this.f14931 != null) {
            throw new Vice(this.f14931);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Chris m18662() {
        return this.f14929;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m18663(BenimaruNikaido benimaruNikaido) {
        if (m18654(benimaruNikaido)) {
            this.f14938.m18736(this.f14939, benimaruNikaido);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized boolean m18664() {
        boolean z = false;
        synchronized (this) {
            if (this.f14931 == null) {
                if ((!this.f14932.f14945 && !this.f14932.f14947) || ((!this.f14936.f14951 && !this.f14936.f14953) || !this.f14933)) {
                    z = true;
                }
            }
        }
        return z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized List<GoroDaimon> m18665() throws IOException {
        List<GoroDaimon> list;
        if (!m18667()) {
            throw new IllegalStateException("servers cannot read response headers");
        }
        this.f14929.m6789();
        while (this.f14935 == null && this.f14931 == null) {
            try {
                m18658();
            } catch (Throwable th) {
                this.f14929.m18678();
                throw th;
            }
        }
        this.f14929.m18678();
        list = this.f14935;
        if (list != null) {
            this.f14935 = null;
        } else {
            throw new Vice(this.f14931);
        }
        return list;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized void m18666(BenimaruNikaido benimaruNikaido) {
        if (this.f14931 == null) {
            this.f14931 = benimaruNikaido;
            notifyAll();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m18667() {
        return this.f14938.f15019 == ((this.f14939 & 1) == 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m18668() {
        return this.f14939;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18669(long j) {
        this.f14937 += j;
        if (j > 0) {
            notifyAll();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18670(BenimaruNikaido benimaruNikaido) throws IOException {
        if (m18654(benimaruNikaido)) {
            this.f14938.m18725(this.f14939, benimaruNikaido);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18671(ChangKoehan changKoehan, int i) throws IOException {
        if (f14928 || !Thread.holdsLock(this)) {
            this.f14932.m18677(changKoehan, (long) i);
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18672(List<GoroDaimon> list) {
        boolean z = true;
        if (f14928 || !Thread.holdsLock(this)) {
            synchronized (this) {
                this.f14933 = true;
                if (this.f14935 == null) {
                    this.f14935 = list;
                    z = m18664();
                    notifyAll();
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(this.f14935);
                    arrayList.add((Object) null);
                    arrayList.addAll(list);
                    this.f14935 = arrayList;
                }
            }
            if (!z) {
                this.f14938.m18723(this.f14939);
                return;
            }
            return;
        }
        throw new AssertionError();
    }
}
