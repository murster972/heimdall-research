package p006do.p007do.p023new;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import p006do.p007do.p023new.ChinGentsai;
import p009if.ChinGentsai;
import p009if.GoroDaimon;

/* renamed from: do.do.new.RugalBernstein  reason: invalid package */
final class RugalBernstein implements Closeable {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Logger f15086 = Logger.getLogger(ChangKoehan.class.getName());

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f15087 = 16384;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f15088;

    /* renamed from: 连任  reason: contains not printable characters */
    private final GoroDaimon f15089 = new GoroDaimon();

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f15090;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ChinGentsai f15091;

    /* renamed from: 龘  reason: contains not printable characters */
    final ChinGentsai.BenimaruNikaido f15092 = new ChinGentsai.BenimaruNikaido(this.f15089);

    RugalBernstein(p009if.ChinGentsai chinGentsai, boolean z) {
        this.f15091 = chinGentsai;
        this.f15090 = z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m18826(int i, long j) throws IOException {
        while (j > 0) {
            int min = (int) Math.min((long) this.f15087, j);
            j -= (long) min;
            m18833(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
            this.f15091.a_(this.f15089, (long) min);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m18827(p009if.ChinGentsai chinGentsai, int i) throws IOException {
        chinGentsai.m18934((i >>> 16) & 255);
        chinGentsai.m18934((i >>> 8) & 255);
        chinGentsai.m18934(i & 255);
    }

    public synchronized void close() throws IOException {
        this.f15088 = true;
        this.f15091.close();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m18828() throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        }
        this.f15091.flush();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m18829(Mature mature) throws IOException {
        int i = 0;
        synchronized (this) {
            if (this.f15088) {
                throw new IOException("closed");
            }
            m18833(0, mature.m18816() * 6, (byte) 4, (byte) 0);
            while (i < 10) {
                if (mature.m18825(i)) {
                    this.f15091.m18928(i == 4 ? 3 : i == 7 ? 4 : i);
                    this.f15091.m18932(mature.m18817(i));
                }
                i++;
            }
            this.f15091.flush();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m18830() {
        return this.f15087;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18831() throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        } else if (this.f15090) {
            if (f15086.isLoggable(Level.FINE)) {
                f15086.fine(p006do.p007do.GoroDaimon.m18416(">> CONNECTION %s", ChangKoehan.f14957.e()));
            }
            this.f15091.m18929(ChangKoehan.f14957.h());
            this.f15091.flush();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18832(int i, byte b, GoroDaimon goroDaimon, int i2) throws IOException {
        m18833(i, i2, (byte) 0, b);
        if (i2 > 0) {
            this.f15091.a_(goroDaimon, (long) i2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18833(int i, int i2, byte b, byte b2) throws IOException {
        if (f15086.isLoggable(Level.FINE)) {
            f15086.fine(ChangKoehan.m18686(false, i, i2, b, b2));
        }
        if (i2 > this.f15087) {
            throw ChangKoehan.m18684("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(this.f15087), Integer.valueOf(i2));
        } else if ((Integer.MIN_VALUE & i) != 0) {
            throw ChangKoehan.m18684("reserved bit set: %s", Integer.valueOf(i));
        } else {
            m18827(this.f15091, i2);
            this.f15091.m18934((int) b & 255);
            this.f15091.m18934((int) b2 & 255);
            this.f15091.m18932(Integer.MAX_VALUE & i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18834(int i, int i2, List<GoroDaimon> list) throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        }
        this.f15092.m18696(list);
        long r2 = this.f15089.m6731();
        int min = (int) Math.min((long) (this.f15087 - 4), r2);
        m18833(i, min + 4, (byte) 5, r2 == ((long) min) ? (byte) 4 : 0);
        this.f15091.m18932(Integer.MAX_VALUE & i2);
        this.f15091.a_(this.f15089, (long) min);
        if (r2 > ((long) min)) {
            m18826(i, r2 - ((long) min));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18835(int i, long j) throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        } else if (j == 0 || j > 2147483647L) {
            throw ChangKoehan.m18684("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
        } else {
            m18833(i, 4, (byte) 8, (byte) 0);
            this.f15091.m18932((int) j);
            this.f15091.flush();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18836(int i, BenimaruNikaido benimaruNikaido) throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        } else if (benimaruNikaido.g == -1) {
            throw new IllegalArgumentException();
        } else {
            m18833(i, 4, (byte) 3, (byte) 0);
            this.f15091.m18932(benimaruNikaido.g);
            this.f15091.flush();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18837(int i, BenimaruNikaido benimaruNikaido, byte[] bArr) throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        } else if (benimaruNikaido.g == -1) {
            throw ChangKoehan.m18684("errorCode.httpCode == -1", new Object[0]);
        } else {
            m18833(0, bArr.length + 8, (byte) 7, (byte) 0);
            this.f15091.m18932(i);
            this.f15091.m18932(benimaruNikaido.g);
            if (bArr.length > 0) {
                this.f15091.m18929(bArr);
            }
            this.f15091.flush();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18838(Mature mature) throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        }
        this.f15087 = mature.m18819(this.f15087);
        if (mature.m18820() != -1) {
            this.f15092.m18693(mature.m18820());
        }
        m18833(0, 0, (byte) 4, (byte) 1);
        this.f15091.flush();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18839(boolean z, int i, int i2) throws IOException {
        byte b = 0;
        synchronized (this) {
            if (this.f15088) {
                throw new IOException("closed");
            }
            if (z) {
                b = 1;
            }
            m18833(0, 8, (byte) 6, b);
            this.f15091.m18932(i);
            this.f15091.m18932(i2);
            this.f15091.flush();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18840(boolean z, int i, int i2, List<GoroDaimon> list) throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        }
        m18842(z, i, list);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18841(boolean z, int i, GoroDaimon goroDaimon, int i2) throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        }
        byte b = 0;
        if (z) {
            b = (byte) 1;
        }
        m18832(i, b, goroDaimon, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18842(boolean z, int i, List<GoroDaimon> list) throws IOException {
        if (this.f15088) {
            throw new IOException("closed");
        }
        this.f15092.m18696(list);
        long r2 = this.f15089.m6731();
        int min = (int) Math.min((long) this.f15087, r2);
        byte b = r2 == ((long) min) ? (byte) 4 : 0;
        if (z) {
            b = (byte) (b | 1);
        }
        m18833(i, min, (byte) 1, b);
        this.f15091.a_(this.f15089, (long) min);
        if (r2 > ((long) min)) {
            m18826(i, r2 - ((long) min));
        }
    }
}
