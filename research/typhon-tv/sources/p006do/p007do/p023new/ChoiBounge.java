package p006do.p007do.p023new;

import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import p006do.JhunHoon;
import p006do.KDash;
import p006do.Krizalid;
import p006do.Orochi;
import p006do.ShingoYabuki;
import p006do.Whip;
import p006do.p007do.p020for.BrianBattler;
import p006do.p007do.p020for.GoroDaimon;
import p006do.p007do.p020for.IoriYagami;
import p006do.p007do.p021if.HeavyD;
import p009if.LeonaHeidern;
import p009if.LuckyGlauber;
import p009if.Shermie;
import p009if.YashiroNanakase;

/* renamed from: do.do.new.ChoiBounge  reason: invalid package */
public final class ChoiBounge implements GoroDaimon {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final p009if.ChoiBounge f14978 = p009if.ChoiBounge.a("transfer-encoding");

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final p009if.ChoiBounge f14979 = p009if.ChoiBounge.a("te");

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final p009if.ChoiBounge f14980 = p009if.ChoiBounge.a("encoding");

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final p009if.ChoiBounge f14981 = p009if.ChoiBounge.a("upgrade");

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final List<p009if.ChoiBounge> f14982 = p006do.p007do.GoroDaimon.m18419((T[]) new p009if.ChoiBounge[]{f14985, f14987, f14986, f14984, f14979, f14978, f14980, f14981, GoroDaimon.f14997, GoroDaimon.f14996, GoroDaimon.f14994, GoroDaimon.f14993});

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final List<p009if.ChoiBounge> f14983 = p006do.p007do.GoroDaimon.m18419((T[]) new p009if.ChoiBounge[]{f14985, f14987, f14986, f14984, f14979, f14978, f14980, f14981});

    /* renamed from: 连任  reason: contains not printable characters */
    private static final p009if.ChoiBounge f14984 = p009if.ChoiBounge.a("proxy-connection");

    /* renamed from: 靐  reason: contains not printable characters */
    private static final p009if.ChoiBounge f14985 = p009if.ChoiBounge.a("connection");

    /* renamed from: 麤  reason: contains not printable characters */
    private static final p009if.ChoiBounge f14986 = p009if.ChoiBounge.a("keep-alive");

    /* renamed from: 齉  reason: contains not printable characters */
    private static final p009if.ChoiBounge f14987 = p009if.ChoiBounge.a("host");

    /* renamed from: ʾ  reason: contains not printable characters */
    private final HeavyD f14988;

    /* renamed from: ʿ  reason: contains not printable characters */
    private BrianBattler f14989;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Orochi f14990;

    /* renamed from: 龘  reason: contains not printable characters */
    final HeavyD f14991;

    /* renamed from: do.do.new.ChoiBounge$KyoKusanagi */
    class KyoKusanagi extends LuckyGlauber {
        KyoKusanagi(Shermie shermie) {
            super(shermie);
        }

        public void close() throws IOException {
            ChoiBounge.this.f14991.m18630(false, (GoroDaimon) ChoiBounge.this);
            super.close();
        }
    }

    public ChoiBounge(Orochi orochi, HeavyD heavyD, HeavyD heavyD2) {
        this.f14990 = orochi;
        this.f14991 = heavyD;
        this.f14988 = heavyD2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static List<GoroDaimon> m18714(Whip whip) {
        ShingoYabuki r1 = whip.m6591();
        ArrayList arrayList = new ArrayList(r1.m6583() + 4);
        arrayList.add(new GoroDaimon(GoroDaimon.f14997, whip.m6589()));
        arrayList.add(new GoroDaimon(GoroDaimon.f14996, BrianBattler.m18539(whip.m6592())));
        String r0 = whip.m6593("Host");
        if (r0 != null) {
            arrayList.add(new GoroDaimon(GoroDaimon.f14993, r0));
        }
        arrayList.add(new GoroDaimon(GoroDaimon.f14994, whip.m6592().m6618()));
        int r3 = r1.m6583();
        for (int i = 0; i < r3; i++) {
            p009if.ChoiBounge a = p009if.ChoiBounge.a(r1.m6584(i).toLowerCase(Locale.US));
            if (!f14982.contains(a)) {
                arrayList.add(new GoroDaimon(a, r1.m6579(i)));
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static JhunHoon.KyoKusanagi m18715(List<GoroDaimon> list) throws IOException {
        ShingoYabuki.KyoKusanagi kyoKusanagi;
        ShingoYabuki.KyoKusanagi kyoKusanagi2 = new ShingoYabuki.KyoKusanagi();
        int size = list.size();
        int i = 0;
        IoriYagami ioriYagami = null;
        while (i < size) {
            GoroDaimon goroDaimon = list.get(i);
            if (goroDaimon == null) {
                if (ioriYagami != null && ioriYagami.f14851 == 100) {
                    kyoKusanagi = new ShingoYabuki.KyoKusanagi();
                    ioriYagami = null;
                }
                kyoKusanagi = kyoKusanagi2;
            } else {
                p009if.ChoiBounge choiBounge = goroDaimon.f14999;
                String a = goroDaimon.f15000.a();
                if (choiBounge.equals(GoroDaimon.f14995)) {
                    ioriYagami = IoriYagami.m18568("HTTP/1.1 " + a);
                    kyoKusanagi = kyoKusanagi2;
                } else {
                    if (!f14983.contains(choiBounge)) {
                        p006do.p007do.KyoKusanagi.f14748.m18437(kyoKusanagi2, choiBounge.a(), a);
                    }
                    kyoKusanagi = kyoKusanagi2;
                }
            }
            i++;
            kyoKusanagi2 = kyoKusanagi;
        }
        if (ioriYagami != null) {
            return new JhunHoon.KyoKusanagi().m6484(KDash.HTTP_2).m6480(ioriYagami.f14851).m6488(ioriYagami.f14852).m6486(kyoKusanagi2.m18386());
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m18716() throws IOException {
        this.f14989.m18657().close();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public JhunHoon.KyoKusanagi m18717(boolean z) throws IOException {
        JhunHoon.KyoKusanagi r0 = m18715(this.f14989.m18665());
        if (!z || p006do.p007do.KyoKusanagi.f14748.m18431(r0) != 100) {
            return r0;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Krizalid m18718(JhunHoon jhunHoon) throws IOException {
        return new p006do.p007do.p020for.LuckyGlauber(jhunHoon.m6468(), LeonaHeidern.m18966((Shermie) new KyoKusanagi(this.f14989.m18656())));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public YashiroNanakase m18719(Whip whip, long j) {
        return this.f14989.m18657();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18720() throws IOException {
        this.f14988.m18724();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18721(Whip whip) throws IOException {
        if (this.f14989 == null) {
            this.f14989 = this.f14988.m18734(m18714(whip), whip.m6590() != null);
            this.f14989.m18662().m18947((long) this.f14990.m6543(), TimeUnit.MILLISECONDS);
            this.f14989.m18655().m18947((long) this.f14990.m6545(), TimeUnit.MILLISECONDS);
        }
    }
}
