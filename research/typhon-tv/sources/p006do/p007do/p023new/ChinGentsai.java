package p006do.p007do.p023new;

import com.mopub.common.TyphoonApp;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.oltu.oauth2.common.OAuth;
import p009if.ChangKoehan;
import p009if.ChoiBounge;
import p009if.GoroDaimon;
import p009if.LeonaHeidern;
import p009if.Shermie;

/* renamed from: do.do.new.ChinGentsai  reason: invalid package */
final class ChinGentsai {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Map<ChoiBounge, Integer> f14958 = m18688();

    /* renamed from: 龘  reason: contains not printable characters */
    static final GoroDaimon[] f14959 = {new GoroDaimon(GoroDaimon.f14993, ""), new GoroDaimon(GoroDaimon.f14997, (String) OAuth.HttpMethod.GET), new GoroDaimon(GoroDaimon.f14997, (String) OAuth.HttpMethod.POST), new GoroDaimon(GoroDaimon.f14996, (String) InternalZipTyphoonApp.ZIP_FILE_SEPARATOR), new GoroDaimon(GoroDaimon.f14996, "/index.html"), new GoroDaimon(GoroDaimon.f14994, (String) TyphoonApp.HTTP), new GoroDaimon(GoroDaimon.f14994, (String) TyphoonApp.HTTPS), new GoroDaimon(GoroDaimon.f14995, "200"), new GoroDaimon(GoroDaimon.f14995, "204"), new GoroDaimon(GoroDaimon.f14995, "206"), new GoroDaimon(GoroDaimon.f14995, "304"), new GoroDaimon(GoroDaimon.f14995, "400"), new GoroDaimon(GoroDaimon.f14995, "404"), new GoroDaimon(GoroDaimon.f14995, "500"), new GoroDaimon("accept-charset", ""), new GoroDaimon("accept-encoding", "gzip, deflate"), new GoroDaimon("accept-language", ""), new GoroDaimon("accept-ranges", ""), new GoroDaimon("accept", ""), new GoroDaimon("access-control-allow-origin", ""), new GoroDaimon((String) PubnativeRequest.Parameters.AGE, ""), new GoroDaimon("allow", ""), new GoroDaimon("authorization", ""), new GoroDaimon("cache-control", ""), new GoroDaimon("content-disposition", ""), new GoroDaimon("content-encoding", ""), new GoroDaimon("content-language", ""), new GoroDaimon("content-length", ""), new GoroDaimon("content-location", ""), new GoroDaimon("content-range", ""), new GoroDaimon("content-type", ""), new GoroDaimon("cookie", ""), new GoroDaimon("date", ""), new GoroDaimon("etag", ""), new GoroDaimon("expect", ""), new GoroDaimon("expires", ""), new GoroDaimon("from", ""), new GoroDaimon("host", ""), new GoroDaimon("if-match", ""), new GoroDaimon("if-modified-since", ""), new GoroDaimon("if-none-match", ""), new GoroDaimon("if-range", ""), new GoroDaimon("if-unmodified-since", ""), new GoroDaimon("last-modified", ""), new GoroDaimon("link", ""), new GoroDaimon("location", ""), new GoroDaimon("max-forwards", ""), new GoroDaimon("proxy-authenticate", ""), new GoroDaimon("proxy-authorization", ""), new GoroDaimon("range", ""), new GoroDaimon("referer", ""), new GoroDaimon("refresh", ""), new GoroDaimon("retry-after", ""), new GoroDaimon("server", ""), new GoroDaimon("set-cookie", ""), new GoroDaimon("strict-transport-security", ""), new GoroDaimon("transfer-encoding", ""), new GoroDaimon("user-agent", ""), new GoroDaimon("vary", ""), new GoroDaimon("via", ""), new GoroDaimon("www-authenticate", "")};

    /* renamed from: do.do.new.ChinGentsai$BenimaruNikaido */
    static final class BenimaruNikaido {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f14960;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final GoroDaimon f14961;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final boolean f14962;

        /* renamed from: ˑ  reason: contains not printable characters */
        private int f14963;

        /* renamed from: ٴ  reason: contains not printable characters */
        private boolean f14964;

        /* renamed from: 连任  reason: contains not printable characters */
        int f14965;

        /* renamed from: 靐  reason: contains not printable characters */
        int f14966;

        /* renamed from: 麤  reason: contains not printable characters */
        int f14967;

        /* renamed from: 齉  reason: contains not printable characters */
        GoroDaimon[] f14968;

        /* renamed from: 龘  reason: contains not printable characters */
        int f14969;

        BenimaruNikaido(int i, boolean z, GoroDaimon goroDaimon) {
            this.f14963 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
            this.f14968 = new GoroDaimon[8];
            this.f14967 = this.f14968.length - 1;
            this.f14965 = 0;
            this.f14960 = 0;
            this.f14969 = i;
            this.f14966 = i;
            this.f14962 = z;
            this.f14961 = goroDaimon;
        }

        BenimaruNikaido(GoroDaimon goroDaimon) {
            this(4096, true, goroDaimon);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private int m18689(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.f14968.length;
                while (true) {
                    length--;
                    if (length < this.f14967 || i <= 0) {
                        System.arraycopy(this.f14968, this.f14967 + 1, this.f14968, this.f14967 + 1 + i2, this.f14965);
                        Arrays.fill(this.f14968, this.f14967 + 1, this.f14967 + 1 + i2, (Object) null);
                        this.f14967 += i2;
                    } else {
                        i -= this.f14968[length].f15001;
                        this.f14960 -= this.f14968[length].f15001;
                        this.f14965--;
                        i2++;
                    }
                }
                System.arraycopy(this.f14968, this.f14967 + 1, this.f14968, this.f14967 + 1 + i2, this.f14965);
                Arrays.fill(this.f14968, this.f14967 + 1, this.f14967 + 1 + i2, (Object) null);
                this.f14967 += i2;
            }
            return i2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m18690() {
            if (this.f14966 >= this.f14960) {
                return;
            }
            if (this.f14966 == 0) {
                m18691();
            } else {
                m18689(this.f14960 - this.f14966);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m18691() {
            Arrays.fill(this.f14968, (Object) null);
            this.f14967 = this.f14968.length - 1;
            this.f14965 = 0;
            this.f14960 = 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m18692(GoroDaimon goroDaimon) {
            int i = goroDaimon.f15001;
            if (i > this.f14966) {
                m18691();
                return;
            }
            m18689((this.f14960 + i) - this.f14966);
            if (this.f14965 + 1 > this.f14968.length) {
                GoroDaimon[] goroDaimonArr = new GoroDaimon[(this.f14968.length * 2)];
                System.arraycopy(this.f14968, 0, goroDaimonArr, this.f14968.length, this.f14968.length);
                this.f14967 = this.f14968.length - 1;
                this.f14968 = goroDaimonArr;
            }
            int i2 = this.f14967;
            this.f14967 = i2 - 1;
            this.f14968[i2] = goroDaimon;
            this.f14965++;
            this.f14960 = i + this.f14960;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18693(int i) {
            this.f14969 = i;
            int min = Math.min(i, 16384);
            if (this.f14966 != min) {
                if (min < this.f14966) {
                    this.f14963 = Math.min(this.f14963, min);
                }
                this.f14964 = true;
                this.f14966 = min;
                m18690();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18694(int i, int i2, int i3) {
            if (i < i2) {
                this.f14961.m6770(i3 | i);
                return;
            }
            this.f14961.m6770(i3 | i2);
            int i4 = i - i2;
            while (i4 >= 128) {
                this.f14961.m6770((i4 & 127) | 128);
                i4 >>>= 7;
            }
            this.f14961.m6770(i4);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18695(ChoiBounge choiBounge) throws IOException {
            if (!this.f14962 || IoriYagami.m18775().m18777(choiBounge) >= choiBounge.g()) {
                m18694(choiBounge.g(), 127, 0);
                this.f14961.m6774(choiBounge);
                return;
            }
            GoroDaimon goroDaimon = new GoroDaimon();
            IoriYagami.m18775().m18778(choiBounge, goroDaimon);
            ChoiBounge r0 = goroDaimon.m6736();
            m18694(r0.g(), 127, 128);
            this.f14961.m6774(r0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18696(List<GoroDaimon> list) throws IOException {
            int i;
            int i2;
            if (this.f14964) {
                if (this.f14963 < this.f14966) {
                    m18694(this.f14963, 31, 32);
                }
                this.f14964 = false;
                this.f14963 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
                m18694(this.f14966, 31, 32);
            }
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                GoroDaimon goroDaimon = list.get(i3);
                ChoiBounge f = goroDaimon.f14999.f();
                ChoiBounge choiBounge = goroDaimon.f15000;
                Integer num = ChinGentsai.f14958.get(f);
                if (num != null) {
                    int intValue = num.intValue() + 1;
                    if (intValue > 1 && intValue < 8) {
                        if (p006do.p007do.GoroDaimon.m18426((Object) ChinGentsai.f14959[intValue - 1].f15000, (Object) choiBounge)) {
                            i = intValue;
                            i2 = intValue;
                        } else if (p006do.p007do.GoroDaimon.m18426((Object) ChinGentsai.f14959[intValue].f15000, (Object) choiBounge)) {
                            i2 = intValue + 1;
                            i = intValue;
                        }
                    }
                    i = intValue;
                    i2 = -1;
                } else {
                    i = -1;
                    i2 = -1;
                }
                if (i2 == -1) {
                    int i4 = this.f14967 + 1;
                    int length = this.f14968.length;
                    while (true) {
                        if (i4 >= length) {
                            break;
                        }
                        if (p006do.p007do.GoroDaimon.m18426((Object) this.f14968[i4].f14999, (Object) f)) {
                            if (p006do.p007do.GoroDaimon.m18426((Object) this.f14968[i4].f15000, (Object) choiBounge)) {
                                i2 = (i4 - this.f14967) + ChinGentsai.f14959.length;
                                break;
                            } else if (i == -1) {
                                i = (i4 - this.f14967) + ChinGentsai.f14959.length;
                            }
                        }
                        i4++;
                    }
                }
                if (i2 != -1) {
                    m18694(i2, 127, 128);
                } else if (i == -1) {
                    this.f14961.m6770(64);
                    m18695(f);
                    m18695(choiBounge);
                    m18692(goroDaimon);
                } else if (!f.a(GoroDaimon.f14998) || GoroDaimon.f14993.equals(f)) {
                    m18694(i, 63, 64);
                    m18695(choiBounge);
                    m18692(goroDaimon);
                } else {
                    m18694(i, 15, 0);
                    m18695(choiBounge);
                }
            }
        }
    }

    /* renamed from: do.do.new.ChinGentsai$KyoKusanagi */
    static final class KyoKusanagi {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final ChangKoehan f14970;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f14971;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f14972;

        /* renamed from: 连任  reason: contains not printable characters */
        private final List<GoroDaimon> f14973;

        /* renamed from: 靐  reason: contains not printable characters */
        int f14974;

        /* renamed from: 麤  reason: contains not printable characters */
        int f14975;

        /* renamed from: 齉  reason: contains not printable characters */
        int f14976;

        /* renamed from: 龘  reason: contains not printable characters */
        GoroDaimon[] f14977;

        KyoKusanagi(int i, int i2, Shermie shermie) {
            this.f14973 = new ArrayList();
            this.f14977 = new GoroDaimon[8];
            this.f14974 = this.f14977.length - 1;
            this.f14976 = 0;
            this.f14975 = 0;
            this.f14971 = i;
            this.f14972 = i2;
            this.f14970 = LeonaHeidern.m18966(shermie);
        }

        KyoKusanagi(int i, Shermie shermie) {
            this(i, i, shermie);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private ChoiBounge m18697(int i) {
            return m18700(i) ? ChinGentsai.f14959[i].f14999 : this.f14977[m18707(i - ChinGentsai.f14959.length)].f14999;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m18698() throws IOException {
            this.f14973.add(new GoroDaimon(ChinGentsai.m18687(m18711()), m18711()));
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private void m18699() throws IOException {
            m18709(-1, new GoroDaimon(ChinGentsai.m18687(m18711()), m18711()));
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean m18700(int i) {
            return i >= 0 && i <= ChinGentsai.f14959.length + -1;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        private int m18701() throws IOException {
            return this.f14970.m6705() & 255;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private void m18702() {
            Arrays.fill(this.f14977, (Object) null);
            this.f14974 = this.f14977.length - 1;
            this.f14976 = 0;
            this.f14975 = 0;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private void m18703(int i) throws IOException {
            m18709(-1, new GoroDaimon(m18697(i), m18711()));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m18704(int i) throws IOException {
            if (m18700(i)) {
                this.f14973.add(ChinGentsai.f14959[i]);
                return;
            }
            int r0 = m18707(i - ChinGentsai.f14959.length);
            if (r0 < 0 || r0 > this.f14977.length - 1) {
                throw new IOException("Header index too large " + (i + 1));
            }
            this.f14973.add(this.f14977[r0]);
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m18705() {
            if (this.f14972 >= this.f14975) {
                return;
            }
            if (this.f14972 == 0) {
                m18702();
            } else {
                m18708(this.f14975 - this.f14972);
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m18706(int i) throws IOException {
            this.f14973.add(new GoroDaimon(m18697(i), m18711()));
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private int m18707(int i) {
            return this.f14974 + 1 + i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m18708(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.f14977.length;
                while (true) {
                    length--;
                    if (length < this.f14974 || i <= 0) {
                        System.arraycopy(this.f14977, this.f14974 + 1, this.f14977, this.f14974 + 1 + i2, this.f14976);
                        this.f14974 += i2;
                    } else {
                        i -= this.f14977[length].f15001;
                        this.f14975 -= this.f14977[length].f15001;
                        this.f14976--;
                        i2++;
                    }
                }
                System.arraycopy(this.f14977, this.f14974 + 1, this.f14977, this.f14974 + 1 + i2, this.f14976);
                this.f14974 += i2;
            }
            return i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m18709(int i, GoroDaimon goroDaimon) {
            this.f14973.add(goroDaimon);
            int i2 = goroDaimon.f15001;
            if (i != -1) {
                i2 -= this.f14977[m18707(i)].f15001;
            }
            if (i2 > this.f14972) {
                m18702();
                return;
            }
            int r1 = m18708((this.f14975 + i2) - this.f14972);
            if (i == -1) {
                if (this.f14976 + 1 > this.f14977.length) {
                    GoroDaimon[] goroDaimonArr = new GoroDaimon[(this.f14977.length * 2)];
                    System.arraycopy(this.f14977, 0, goroDaimonArr, this.f14977.length, this.f14977.length);
                    this.f14974 = this.f14977.length - 1;
                    this.f14977 = goroDaimonArr;
                }
                int i3 = this.f14974;
                this.f14974 = i3 - 1;
                this.f14977[i3] = goroDaimon;
                this.f14976++;
            } else {
                this.f14977[r1 + m18707(i) + i] = goroDaimon;
            }
            this.f14975 = i2 + this.f14975;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public List<GoroDaimon> m18710() {
            ArrayList arrayList = new ArrayList(this.f14973);
            this.f14973.clear();
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public ChoiBounge m18711() throws IOException {
            int r1 = m18701();
            boolean z = (r1 & 128) == 128;
            int r12 = m18712(r1, 127);
            return z ? ChoiBounge.a(IoriYagami.m18775().m18779(this.f14970.m6706((long) r12))) : this.f14970.m6707((long) r12);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m18712(int i, int i2) throws IOException {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int r1 = m18701();
                if ((r1 & 128) == 0) {
                    return (r1 << i4) + i2;
                }
                i2 += (r1 & 127) << i4;
                i4 += 7;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18713() throws IOException {
            while (!this.f14970.m6708()) {
                byte r0 = this.f14970.m6705() & 255;
                if (r0 == 128) {
                    throw new IOException("index == 0");
                } else if ((r0 & 128) == 128) {
                    m18704(m18712((int) r0, 127) - 1);
                } else if (r0 == 64) {
                    m18699();
                } else if ((r0 & 64) == 64) {
                    m18703(m18712((int) r0, 63) - 1);
                } else if ((r0 & 32) == 32) {
                    this.f14972 = m18712((int) r0, 31);
                    if (this.f14972 < 0 || this.f14972 > this.f14971) {
                        throw new IOException("Invalid dynamic table size update " + this.f14972);
                    }
                    m18705();
                } else if (r0 == 16 || r0 == 0) {
                    m18698();
                } else {
                    m18706(m18712((int) r0, 15) - 1);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ChoiBounge m18687(ChoiBounge choiBounge) throws IOException {
        int i = 0;
        int g = choiBounge.g();
        while (i < g) {
            byte a = choiBounge.a(i);
            if (a < 65 || a > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + choiBounge.a());
            }
        }
        return choiBounge;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Map<ChoiBounge, Integer> m18688() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(f14959.length);
        for (int i = 0; i < f14959.length; i++) {
            if (!linkedHashMap.containsKey(f14959[i].f14999)) {
                linkedHashMap.put(f14959[i].f14999, Integer.valueOf(i));
            }
        }
        return Collections.unmodifiableMap(linkedHashMap);
    }
}
