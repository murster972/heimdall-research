package p006do.p007do.p023new;

import java.util.concurrent.CountDownLatch;

/* renamed from: do.do.new.SaishuKusanagi  reason: invalid package */
final class SaishuKusanagi {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f15093 = -1;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f15094 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    private final CountDownLatch f15095 = new CountDownLatch(1);

    SaishuKusanagi() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m18843() {
        if (this.f15094 != -1 || this.f15093 == -1) {
            throw new IllegalStateException();
        }
        this.f15094 = System.nanoTime();
        this.f15095.countDown();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m18844() {
        if (this.f15094 != -1 || this.f15093 == -1) {
            throw new IllegalStateException();
        }
        this.f15094 = this.f15093 - 1;
        this.f15095.countDown();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18845() {
        if (this.f15093 != -1) {
            throw new IllegalStateException();
        }
        this.f15093 = System.nanoTime();
    }
}
