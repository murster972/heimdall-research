package p006do.p007do.p023new;

import java.io.IOException;
import p006do.p007do.GoroDaimon;
import p009if.ChoiBounge;

/* renamed from: do.do.new.ChangKoehan  reason: invalid package */
public final class ChangKoehan {

    /* renamed from: 靐  reason: contains not printable characters */
    static final String[] f14954 = new String[64];

    /* renamed from: 麤  reason: contains not printable characters */
    private static final String[] f14955 = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};

    /* renamed from: 齉  reason: contains not printable characters */
    static final String[] f14956 = new String[256];

    /* renamed from: 龘  reason: contains not printable characters */
    static final ChoiBounge f14957 = ChoiBounge.a("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");

    static {
        for (int i = 0; i < f14956.length; i++) {
            f14956[i] = GoroDaimon.m18416("%8s", Integer.toBinaryString(i)).replace(' ', '0');
        }
        f14954[0] = "";
        f14954[1] = "END_STREAM";
        int[] iArr = {1};
        f14954[8] = "PADDED";
        for (int i2 : iArr) {
            f14954[i2 | 8] = f14954[i2] + "|PADDED";
        }
        f14954[4] = "END_HEADERS";
        f14954[32] = "PRIORITY";
        f14954[36] = "END_HEADERS|PRIORITY";
        for (int i3 : new int[]{4, 32, 36}) {
            for (int i4 : iArr) {
                f14954[i4 | i3] = f14954[i4] + '|' + f14954[i3];
                f14954[i4 | i3 | 8] = f14954[i4] + '|' + f14954[i3] + "|PADDED";
            }
        }
        for (int i5 = 0; i5 < f14954.length; i5++) {
            if (f14954[i5] == null) {
                f14954[i5] = f14956[i5];
            }
        }
    }

    private ChangKoehan() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static IOException m18683(String str, Object... objArr) throws IOException {
        throw new IOException(GoroDaimon.m18416(str, objArr));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static IllegalArgumentException m18684(String str, Object... objArr) {
        throw new IllegalArgumentException(GoroDaimon.m18416(str, objArr));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m18685(byte b, byte b2) {
        if (b2 == 0) {
            return "";
        }
        switch (b) {
            case 2:
            case 3:
            case 7:
            case 8:
                return f14956[b2];
            case 4:
            case 6:
                return b2 == 1 ? "ACK" : f14956[b2];
            default:
                String str = b2 < f14954.length ? f14954[b2] : f14956[b2];
                return (b != 5 || (b2 & 4) == 0) ? (b != 0 || (b2 & 32) == 0) ? str : str.replace("PRIORITY", "COMPRESSED") : str.replace("HEADERS", "PUSH_PROMISE");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m18686(boolean z, int i, int i2, byte b, byte b2) {
        String r0 = b < f14955.length ? f14955[b] : GoroDaimon.m18416("0x%02x", Byte.valueOf(b));
        String r2 = m18685(b, b2);
        Object[] objArr = new Object[5];
        objArr[0] = z ? "<<" : ">>";
        objArr[1] = Integer.valueOf(i);
        objArr[2] = Integer.valueOf(i2);
        objArr[3] = r0;
        objArr[4] = r2;
        return GoroDaimon.m18416("%s 0x%08x %5d %-13s %s", objArr);
    }
}
