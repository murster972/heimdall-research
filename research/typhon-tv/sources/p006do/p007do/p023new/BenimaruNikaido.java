package p006do.p007do.p023new;

/* renamed from: do.do.new.BenimaruNikaido  reason: invalid package */
public enum BenimaruNikaido {
    NO_ERROR(0),
    PROTOCOL_ERROR(1),
    INTERNAL_ERROR(2),
    FLOW_CONTROL_ERROR(3),
    REFUSED_STREAM(7),
    CANCEL(8);
    
    public final int g;

    private BenimaruNikaido(int i) {
        this.g = i;
    }

    public static BenimaruNikaido a(int i) {
        for (BenimaruNikaido benimaruNikaido : values()) {
            if (benimaruNikaido.g == i) {
                return benimaruNikaido;
            }
        }
        return null;
    }
}
