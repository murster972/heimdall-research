package p006do.p007do.p023new;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import p006do.p007do.p023new.ChinGentsai;
import p009if.ChangKoehan;
import p009if.ChoiBounge;
import p009if.Chris;
import p009if.GoroDaimon;
import p009if.Shermie;

/* renamed from: do.do.new.LuckyGlauber  reason: invalid package */
final class LuckyGlauber implements Closeable {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Logger f15073 = Logger.getLogger(ChangKoehan.class.getName());

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f15074;

    /* renamed from: 靐  reason: contains not printable characters */
    final ChinGentsai.KyoKusanagi f15075 = new ChinGentsai.KyoKusanagi(4096, this.f15076);

    /* renamed from: 麤  reason: contains not printable characters */
    private final KyoKusanagi f15076 = new KyoKusanagi(this.f15077);

    /* renamed from: 齉  reason: contains not printable characters */
    private final ChangKoehan f15077;

    /* renamed from: do.do.new.LuckyGlauber$BenimaruNikaido */
    interface BenimaruNikaido {
        /* renamed from: 龘  reason: contains not printable characters */
        void m18803();

        /* renamed from: 龘  reason: contains not printable characters */
        void m18804(int i, int i2, int i3, boolean z);

        /* renamed from: 龘  reason: contains not printable characters */
        void m18805(int i, int i2, List<GoroDaimon> list) throws IOException;

        /* renamed from: 龘  reason: contains not printable characters */
        void m18806(int i, long j);

        /* renamed from: 龘  reason: contains not printable characters */
        void m18807(int i, BenimaruNikaido benimaruNikaido);

        /* renamed from: 龘  reason: contains not printable characters */
        void m18808(int i, BenimaruNikaido benimaruNikaido, ChoiBounge choiBounge);

        /* renamed from: 龘  reason: contains not printable characters */
        void m18809(boolean z, int i, int i2);

        /* renamed from: 龘  reason: contains not printable characters */
        void m18810(boolean z, int i, int i2, List<GoroDaimon> list);

        /* renamed from: 龘  reason: contains not printable characters */
        void m18811(boolean z, int i, ChangKoehan changKoehan, int i2) throws IOException;

        /* renamed from: 龘  reason: contains not printable characters */
        void m18812(boolean z, Mature mature);
    }

    /* renamed from: do.do.new.LuckyGlauber$KyoKusanagi */
    static final class KyoKusanagi implements Shermie {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final ChangKoehan f15078;

        /* renamed from: 连任  reason: contains not printable characters */
        short f15079;

        /* renamed from: 靐  reason: contains not printable characters */
        byte f15080;

        /* renamed from: 麤  reason: contains not printable characters */
        int f15081;

        /* renamed from: 齉  reason: contains not printable characters */
        int f15082;

        /* renamed from: 龘  reason: contains not printable characters */
        int f15083;

        KyoKusanagi(ChangKoehan changKoehan) {
            this.f15078 = changKoehan;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m18813() throws IOException {
            int i = this.f15082;
            int r1 = LuckyGlauber.m18797(this.f15078);
            this.f15081 = r1;
            this.f15083 = r1;
            byte r12 = (byte) (this.f15078.m6705() & 255);
            this.f15080 = (byte) (this.f15078.m6705() & 255);
            if (LuckyGlauber.f15073.isLoggable(Level.FINE)) {
                LuckyGlauber.f15073.fine(ChangKoehan.m18686(true, this.f15082, this.f15083, r12, this.f15080));
            }
            this.f15082 = this.f15078.m6696() & MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
            if (r12 != 9) {
                throw ChangKoehan.m18683("%s != TYPE_CONTINUATION", Byte.valueOf(r12));
            } else if (this.f15082 != i) {
                throw ChangKoehan.m18683("TYPE_CONTINUATION streamId changed", new Object[0]);
            }
        }

        public void close() throws IOException {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m18814(GoroDaimon goroDaimon, long j) throws IOException {
            while (this.f15081 == 0) {
                this.f15078.m6704((long) this.f15079);
                this.f15079 = 0;
                if ((this.f15080 & 4) != 0) {
                    return -1;
                }
                m18813();
            }
            long r2 = this.f15078.m19009(goroDaimon, Math.min(j, (long) this.f15081));
            if (r2 == -1) {
                return -1;
            }
            this.f15081 = (int) (((long) this.f15081) - r2);
            return r2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Chris m18815() {
            return this.f15078.m19010();
        }
    }

    LuckyGlauber(ChangKoehan changKoehan, boolean z) {
        this.f15077 = changKoehan;
        this.f15074 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m18788(BenimaruNikaido benimaruNikaido, int i, byte b, int i2) throws IOException {
        short s = 0;
        if (i2 == 0) {
            throw ChangKoehan.m18683("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        }
        if ((b & 8) != 0) {
            s = (short) (this.f15077.m6705() & 255);
        }
        benimaruNikaido.m18805(i2, this.f15077.m6696() & MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, m18798(m18796(i - 4, b, s), s, b, i2));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m18789(BenimaruNikaido benimaruNikaido, int i, byte b, int i2) throws IOException {
        boolean z = true;
        if (i != 8) {
            throw ChangKoehan.m18683("TYPE_PING length != 8: %s", Integer.valueOf(i));
        } else if (i2 != 0) {
            throw ChangKoehan.m18683("TYPE_PING streamId != 0", new Object[0]);
        } else {
            int r2 = this.f15077.m6696();
            int r3 = this.f15077.m6696();
            if ((b & 1) == 0) {
                z = false;
            }
            benimaruNikaido.m18809(z, r2, r3);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m18790(BenimaruNikaido benimaruNikaido, int i, byte b, int i2) throws IOException {
        if (i < 8) {
            throw ChangKoehan.m18683("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
        } else if (i2 != 0) {
            throw ChangKoehan.m18683("TYPE_GOAWAY streamId != 0", new Object[0]);
        } else {
            int r1 = this.f15077.m6696();
            int r0 = this.f15077.m6696();
            int i3 = i - 8;
            BenimaruNikaido a = BenimaruNikaido.a(r0);
            if (a == null) {
                throw ChangKoehan.m18683("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(r0));
            }
            ChoiBounge choiBounge = ChoiBounge.b;
            if (i3 > 0) {
                choiBounge = this.f15077.m6707((long) i3);
            }
            benimaruNikaido.m18808(r1, a, choiBounge);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m18791(BenimaruNikaido benimaruNikaido, int i, byte b, int i2) throws IOException {
        if (i != 4) {
            throw ChangKoehan.m18683("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
        }
        long r0 = ((long) this.f15077.m6696()) & 2147483647L;
        if (r0 == 0) {
            throw ChangKoehan.m18683("windowSizeIncrement was 0", Long.valueOf(r0));
        } else {
            benimaruNikaido.m18806(i2, r0);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m18792(BenimaruNikaido benimaruNikaido, int i, byte b, int i2) throws IOException {
        if (i2 != 0) {
            throw ChangKoehan.m18683("TYPE_SETTINGS streamId != 0", new Object[0]);
        } else if ((b & 1) != 0) {
            if (i != 0) {
                throw ChangKoehan.m18683("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
            }
            benimaruNikaido.m18803();
        } else if (i % 6 != 0) {
            throw ChangKoehan.m18683("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
        } else {
            Mature mature = new Mature();
            for (int i3 = 0; i3 < i; i3 += 6) {
                short r0 = this.f15077.m6702();
                int r4 = this.f15077.m6696();
                switch (r0) {
                    case 2:
                        if (!(r4 == 0 || r4 == 1)) {
                            throw ChangKoehan.m18683("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                        }
                    case 3:
                        r0 = 4;
                        break;
                    case 4:
                        r0 = 7;
                        if (r4 >= 0) {
                            break;
                        } else {
                            throw ChangKoehan.m18683("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                        }
                    case 5:
                        if (r4 >= 16384 && r4 <= 16777215) {
                            break;
                        } else {
                            throw ChangKoehan.m18683("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(r4));
                        }
                        break;
                }
                mature.m18822(r0, r4);
            }
            benimaruNikaido.m18812(false, mature);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m18793(BenimaruNikaido benimaruNikaido, int i, byte b, int i2) throws IOException {
        boolean z = true;
        short s = 0;
        if (i2 == 0) {
            throw ChangKoehan.m18683("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
        }
        boolean z2 = (b & 1) != 0;
        if ((b & 32) == 0) {
            z = false;
        }
        if (z) {
            throw ChangKoehan.m18683("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
        }
        if ((b & 8) != 0) {
            s = (short) (this.f15077.m6705() & 255);
        }
        benimaruNikaido.m18811(z2, i2, this.f15077, m18796(i, b, s));
        this.f15077.m6704((long) s);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m18794(BenimaruNikaido benimaruNikaido, int i, byte b, int i2) throws IOException {
        if (i != 4) {
            throw ChangKoehan.m18683("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
        } else if (i2 == 0) {
            throw ChangKoehan.m18683("TYPE_RST_STREAM streamId == 0", new Object[0]);
        } else {
            int r0 = this.f15077.m6696();
            BenimaruNikaido a = BenimaruNikaido.a(r0);
            if (a == null) {
                throw ChangKoehan.m18683("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(r0));
            } else {
                benimaruNikaido.m18807(i2, a);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m18795(BenimaruNikaido benimaruNikaido, int i, byte b, int i2) throws IOException {
        if (i != 5) {
            throw ChangKoehan.m18683("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
        } else if (i2 == 0) {
            throw ChangKoehan.m18683("TYPE_PRIORITY streamId == 0", new Object[0]);
        } else {
            m18799(benimaruNikaido, i2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m18796(int i, byte b, short s) throws IOException {
        if ((b & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        throw ChangKoehan.m18683("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m18797(ChangKoehan changKoehan) throws IOException {
        return ((changKoehan.m6705() & 255) << 16) | ((changKoehan.m6705() & 255) << 8) | (changKoehan.m6705() & 255);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<GoroDaimon> m18798(int i, short s, byte b, int i2) throws IOException {
        KyoKusanagi kyoKusanagi = this.f15076;
        this.f15076.f15081 = i;
        kyoKusanagi.f15083 = i;
        this.f15076.f15079 = s;
        this.f15076.f15080 = b;
        this.f15076.f15082 = i2;
        this.f15075.m18713();
        return this.f15075.m18710();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18799(BenimaruNikaido benimaruNikaido, int i) throws IOException {
        int r1 = this.f15077.m6696();
        benimaruNikaido.m18804(i, r1 & MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, (this.f15077.m6705() & 255) + 1, (Integer.MIN_VALUE & r1) != 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18800(BenimaruNikaido benimaruNikaido, int i, byte b, int i2) throws IOException {
        short s = 0;
        if (i2 == 0) {
            throw ChangKoehan.m18683("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        }
        boolean z = (b & 1) != 0;
        if ((b & 8) != 0) {
            s = (short) (this.f15077.m6705() & 255);
        }
        if ((b & 32) != 0) {
            m18799(benimaruNikaido, i2);
            i -= 5;
        }
        benimaruNikaido.m18810(z, i2, -1, m18798(m18796(i, b, s), s, b, i2));
    }

    public void close() throws IOException {
        this.f15077.close();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18801(BenimaruNikaido benimaruNikaido) throws IOException {
        if (!this.f15074) {
            ChoiBounge r0 = this.f15077.m6707((long) ChangKoehan.f14957.g());
            if (f15073.isLoggable(Level.FINE)) {
                f15073.fine(p006do.p007do.GoroDaimon.m18416("<< CONNECTION %s", r0.e()));
            }
            if (!ChangKoehan.f14957.equals(r0)) {
                throw ChangKoehan.m18683("Expected a connection header but was %s", r0.a());
            }
        } else if (!m18802(true, benimaruNikaido)) {
            throw ChangKoehan.m18683("Required SETTINGS preface not received", new Object[0]);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18802(boolean z, BenimaruNikaido benimaruNikaido) throws IOException {
        try {
            this.f15077.m6711(9);
            int r2 = m18797(this.f15077);
            if (r2 < 0 || r2 > 16384) {
                throw ChangKoehan.m18683("FRAME_SIZE_ERROR: %s", Integer.valueOf(r2));
            }
            byte r3 = (byte) (this.f15077.m6705() & 255);
            if (!z || r3 == 4) {
                byte r1 = (byte) (this.f15077.m6705() & 255);
                int r4 = this.f15077.m6696() & MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
                if (f15073.isLoggable(Level.FINE)) {
                    f15073.fine(ChangKoehan.m18686(true, r4, r2, r3, r1));
                }
                switch (r3) {
                    case 0:
                        m18793(benimaruNikaido, r2, r1, r4);
                        return true;
                    case 1:
                        m18800(benimaruNikaido, r2, r1, r4);
                        return true;
                    case 2:
                        m18795(benimaruNikaido, r2, r1, r4);
                        return true;
                    case 3:
                        m18794(benimaruNikaido, r2, r1, r4);
                        return true;
                    case 4:
                        m18792(benimaruNikaido, r2, r1, r4);
                        return true;
                    case 5:
                        m18788(benimaruNikaido, r2, r1, r4);
                        return true;
                    case 6:
                        m18789(benimaruNikaido, r2, r1, r4);
                        return true;
                    case 7:
                        m18790(benimaruNikaido, r2, r1, r4);
                        return true;
                    case 8:
                        m18791(benimaruNikaido, r2, r1, r4);
                        return true;
                    default:
                        this.f15077.m6704((long) r2);
                        return true;
                }
            } else {
                throw ChangKoehan.m18683("Expected a SETTINGS frame but was %s", Byte.valueOf(r3));
            }
        } catch (IOException e) {
            return false;
        }
    }
}
