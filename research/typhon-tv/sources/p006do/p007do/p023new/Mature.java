package p006do.p007do.p023new;

import java.util.Arrays;

/* renamed from: do.do.new.Mature  reason: invalid package */
public final class Mature {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int[] f15084 = new int[10];

    /* renamed from: 龘  reason: contains not printable characters */
    private int f15085;

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m18816() {
        return Integer.bitCount(this.f15085);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m18817(int i) {
        return this.f15084[i];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m18818() {
        if ((128 & this.f15085) != 0) {
            return this.f15084[7];
        }
        return 65535;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m18819(int i) {
        return (32 & this.f15085) != 0 ? this.f15084[5] : i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m18820() {
        if ((2 & this.f15085) != 0) {
            return this.f15084[1];
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m18821(int i) {
        return (16 & this.f15085) != 0 ? this.f15084[4] : i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Mature m18822(int i, int i2) {
        if (i < this.f15084.length) {
            this.f15085 = (1 << i) | this.f15085;
            this.f15084[i] = i2;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18823() {
        this.f15085 = 0;
        Arrays.fill(this.f15084, 0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18824(Mature mature) {
        for (int i = 0; i < 10; i++) {
            if (mature.m18825(i)) {
                m18822(i, mature.m18817(i));
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18825(int i) {
        return ((1 << i) & this.f15085) != 0;
    }
}
