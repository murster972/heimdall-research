package p006do.p007do.p023new;

import p009if.ChoiBounge;

/* renamed from: do.do.new.GoroDaimon  reason: invalid package */
public final class GoroDaimon {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final ChoiBounge f14993 = ChoiBounge.a(":authority");

    /* renamed from: 连任  reason: contains not printable characters */
    public static final ChoiBounge f14994 = ChoiBounge.a(":scheme");

    /* renamed from: 靐  reason: contains not printable characters */
    public static final ChoiBounge f14995 = ChoiBounge.a(":status");

    /* renamed from: 麤  reason: contains not printable characters */
    public static final ChoiBounge f14996 = ChoiBounge.a(":path");

    /* renamed from: 齉  reason: contains not printable characters */
    public static final ChoiBounge f14997 = ChoiBounge.a(":method");

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ChoiBounge f14998 = ChoiBounge.a(":");

    /* renamed from: ʼ  reason: contains not printable characters */
    public final ChoiBounge f14999;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final ChoiBounge f15000;

    /* renamed from: ˑ  reason: contains not printable characters */
    final int f15001;

    public GoroDaimon(ChoiBounge choiBounge, ChoiBounge choiBounge2) {
        this.f14999 = choiBounge;
        this.f15000 = choiBounge2;
        this.f15001 = choiBounge.g() + 32 + choiBounge2.g();
    }

    public GoroDaimon(ChoiBounge choiBounge, String str) {
        this(choiBounge, ChoiBounge.a(str));
    }

    public GoroDaimon(String str, String str2) {
        this(ChoiBounge.a(str), ChoiBounge.a(str2));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof GoroDaimon)) {
            return false;
        }
        GoroDaimon goroDaimon = (GoroDaimon) obj;
        return this.f14999.equals(goroDaimon.f14999) && this.f15000.equals(goroDaimon.f15000);
    }

    public int hashCode() {
        return ((this.f14999.hashCode() + 527) * 31) + this.f15000.hashCode();
    }

    public String toString() {
        return p006do.p007do.GoroDaimon.m18416("%s: %s", this.f14999.a(), this.f15000.a());
    }
}
