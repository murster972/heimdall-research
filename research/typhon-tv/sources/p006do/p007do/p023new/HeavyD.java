package p006do.p007do.p023new;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import p006do.p007do.p023new.LuckyGlauber;
import p009if.ChangKoehan;
import p009if.ChinGentsai;
import p009if.ChoiBounge;

/* renamed from: do.do.new.HeavyD  reason: invalid package */
public final class HeavyD implements Closeable {

    /* renamed from: ˎ  reason: contains not printable characters */
    static final /* synthetic */ boolean f15002 = (!HeavyD.class.desiredAssertionStatus());

    /* renamed from: 龘  reason: contains not printable characters */
    static final ExecutorService f15003 = new ThreadPoolExecutor(0, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, 60, TimeUnit.SECONDS, new SynchronousQueue(), p006do.p007do.GoroDaimon.m18420("OkHttp Http2Connection", true));

    /* renamed from: ʻ  reason: contains not printable characters */
    int f15004;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f15005;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f15006;

    /* renamed from: ʾ  reason: contains not printable characters */
    final Mature f15007 = new Mature();

    /* renamed from: ʿ  reason: contains not printable characters */
    boolean f15008 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final ExecutorService f15009;

    /* renamed from: ˈ  reason: contains not printable characters */
    Mature f15010 = new Mature();

    /* renamed from: ˉ  reason: contains not printable characters */
    private Map<Integer, SaishuKusanagi> f15011;

    /* renamed from: ˊ  reason: contains not printable characters */
    final GoroDaimon f15012;

    /* renamed from: ˋ  reason: contains not printable characters */
    final Set<Integer> f15013 = new LinkedHashSet();

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f15014;

    /* renamed from: ˑ  reason: contains not printable characters */
    final LeonaHeidern f15015;

    /* renamed from: ٴ  reason: contains not printable characters */
    long f15016 = 0;

    /* renamed from: ᐧ  reason: contains not printable characters */
    long f15017;

    /* renamed from: 连任  reason: contains not printable characters */
    final String f15018;

    /* renamed from: 靐  reason: contains not printable characters */
    final boolean f15019;

    /* renamed from: 麤  reason: contains not printable characters */
    final Map<Integer, BrianBattler> f15020 = new LinkedHashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    final BenimaruNikaido f15021;

    /* renamed from: ﹶ  reason: contains not printable characters */
    final Socket f15022;

    /* renamed from: ﾞ  reason: contains not printable characters */
    final RugalBernstein f15023;

    /* renamed from: do.do.new.HeavyD$BenimaruNikaido */
    public static abstract class BenimaruNikaido {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final BenimaruNikaido f15050 = new BenimaruNikaido() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m18755(BrianBattler brianBattler) throws IOException {
                brianBattler.m18670(BenimaruNikaido.REFUSED_STREAM);
            }
        };

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract void m18753(BrianBattler brianBattler) throws IOException;

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18754(HeavyD heavyD) {
        }
    }

    /* renamed from: do.do.new.HeavyD$GoroDaimon */
    class GoroDaimon extends p006do.p007do.BenimaruNikaido implements LuckyGlauber.BenimaruNikaido {

        /* renamed from: 龘  reason: contains not printable characters */
        final LuckyGlauber f15052;

        GoroDaimon(LuckyGlauber luckyGlauber) {
            super("OkHttp %s", HeavyD.this.f15018);
            this.f15052 = luckyGlauber;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m18756(final Mature mature) {
            HeavyD.f15003.execute(new p006do.p007do.BenimaruNikaido("OkHttp %s ACK Settings", new Object[]{HeavyD.this.f15018}) {
                /* renamed from: 靐  reason: contains not printable characters */
                public void m18770() {
                    try {
                        HeavyD.this.f15023.m18838(mature);
                    } catch (IOException e) {
                    }
                }
            });
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m18757() {
            BenimaruNikaido benimaruNikaido;
            BenimaruNikaido benimaruNikaido2 = BenimaruNikaido.INTERNAL_ERROR;
            BenimaruNikaido benimaruNikaido3 = BenimaruNikaido.INTERNAL_ERROR;
            try {
                this.f15052.m18801((LuckyGlauber.BenimaruNikaido) this);
                do {
                } while (this.f15052.m18802(false, (LuckyGlauber.BenimaruNikaido) this));
                try {
                    HeavyD.this.m18743(BenimaruNikaido.NO_ERROR, BenimaruNikaido.CANCEL);
                } catch (IOException e) {
                }
                p006do.p007do.GoroDaimon.m18422((Closeable) this.f15052);
            } catch (IOException e2) {
                benimaruNikaido = BenimaruNikaido.PROTOCOL_ERROR;
                try {
                    HeavyD.this.m18743(benimaruNikaido, BenimaruNikaido.PROTOCOL_ERROR);
                } catch (IOException e3) {
                }
                p006do.p007do.GoroDaimon.m18422((Closeable) this.f15052);
            } catch (Throwable th) {
                th = th;
                HeavyD.this.m18743(benimaruNikaido, benimaruNikaido3);
                p006do.p007do.GoroDaimon.m18422((Closeable) this.f15052);
                throw th;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18758() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18759(int i, int i2, int i3, boolean z) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18760(int i, int i2, List<GoroDaimon> list) {
            HeavyD.this.m18738(i2, list);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18761(int i, long j) {
            if (i == 0) {
                synchronized (HeavyD.this) {
                    HeavyD.this.f15017 += j;
                    HeavyD.this.notifyAll();
                }
                return;
            }
            BrianBattler r1 = HeavyD.this.m18733(i);
            if (r1 != null) {
                synchronized (r1) {
                    r1.m18669(j);
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18762(int i, BenimaruNikaido benimaruNikaido) {
            if (HeavyD.this.m18728(i)) {
                HeavyD.this.m18731(i, benimaruNikaido);
                return;
            }
            BrianBattler r0 = HeavyD.this.m18723(i);
            if (r0 != null) {
                r0.m18666(benimaruNikaido);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18763(int i, BenimaruNikaido benimaruNikaido, ChoiBounge choiBounge) {
            BrianBattler[] brianBattlerArr;
            if (choiBounge.g() > 0) {
            }
            synchronized (HeavyD.this) {
                brianBattlerArr = (BrianBattler[]) HeavyD.this.f15020.values().toArray(new BrianBattler[HeavyD.this.f15020.size()]);
                HeavyD.this.f15006 = true;
            }
            for (BrianBattler brianBattler : brianBattlerArr) {
                if (brianBattler.m18668() > i && brianBattler.m18667()) {
                    brianBattler.m18666(BenimaruNikaido.REFUSED_STREAM);
                    HeavyD.this.m18723(brianBattler.m18668());
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18764(boolean z, int i, int i2) {
            if (z) {
                SaishuKusanagi r0 = HeavyD.this.m18729(i);
                if (r0 != null) {
                    r0.m18843();
                    return;
                }
                return;
            }
            HeavyD.this.m18745(true, i, i2, (SaishuKusanagi) null);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0073, code lost:
            r0.m18672(r12);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0076, code lost:
            if (r9 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0078, code lost:
            r0.m18659();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m18765(boolean r9, int r10, int r11, java.util.List<p006do.p007do.p023new.GoroDaimon> r12) {
            /*
                r8 = this;
                do.do.new.HeavyD r0 = p006do.p007do.p023new.HeavyD.this
                boolean r0 = r0.m18728(r10)
                if (r0 == 0) goto L_0x000e
                do.do.new.HeavyD r0 = p006do.p007do.p023new.HeavyD.this
                r0.m18739(r10, r12, r9)
            L_0x000d:
                return
            L_0x000e:
                do.do.new.HeavyD r6 = p006do.p007do.p023new.HeavyD.this
                monitor-enter(r6)
                do.do.new.HeavyD r0 = p006do.p007do.p023new.HeavyD.this     // Catch:{ all -> 0x0019 }
                boolean r0 = r0.f15006     // Catch:{ all -> 0x0019 }
                if (r0 == 0) goto L_0x001c
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                goto L_0x000d
            L_0x0019:
                r0 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                throw r0
            L_0x001c:
                do.do.new.HeavyD r0 = p006do.p007do.p023new.HeavyD.this     // Catch:{ all -> 0x0019 }
                do.do.new.BrianBattler r0 = r0.m18733((int) r10)     // Catch:{ all -> 0x0019 }
                if (r0 != 0) goto L_0x0072
                do.do.new.HeavyD r0 = p006do.p007do.p023new.HeavyD.this     // Catch:{ all -> 0x0019 }
                int r0 = r0.f15004     // Catch:{ all -> 0x0019 }
                if (r10 > r0) goto L_0x002c
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                goto L_0x000d
            L_0x002c:
                int r0 = r10 % 2
                do.do.new.HeavyD r1 = p006do.p007do.p023new.HeavyD.this     // Catch:{ all -> 0x0019 }
                int r1 = r1.f15005     // Catch:{ all -> 0x0019 }
                int r1 = r1 % 2
                if (r0 != r1) goto L_0x0038
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                goto L_0x000d
            L_0x0038:
                do.do.new.BrianBattler r0 = new do.do.new.BrianBattler     // Catch:{ all -> 0x0019 }
                do.do.new.HeavyD r2 = p006do.p007do.p023new.HeavyD.this     // Catch:{ all -> 0x0019 }
                r3 = 0
                r1 = r10
                r4 = r9
                r5 = r12
                r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0019 }
                do.do.new.HeavyD r1 = p006do.p007do.p023new.HeavyD.this     // Catch:{ all -> 0x0019 }
                r1.f15004 = r10     // Catch:{ all -> 0x0019 }
                do.do.new.HeavyD r1 = p006do.p007do.p023new.HeavyD.this     // Catch:{ all -> 0x0019 }
                java.util.Map<java.lang.Integer, do.do.new.BrianBattler> r1 = r1.f15020     // Catch:{ all -> 0x0019 }
                java.lang.Integer r2 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0019 }
                r1.put(r2, r0)     // Catch:{ all -> 0x0019 }
                java.util.concurrent.ExecutorService r1 = p006do.p007do.p023new.HeavyD.f15003     // Catch:{ all -> 0x0019 }
                do.do.new.HeavyD$GoroDaimon$1 r2 = new do.do.new.HeavyD$GoroDaimon$1     // Catch:{ all -> 0x0019 }
                java.lang.String r3 = "OkHttp %s stream %d"
                r4 = 2
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0019 }
                r5 = 0
                do.do.new.HeavyD r7 = p006do.p007do.p023new.HeavyD.this     // Catch:{ all -> 0x0019 }
                java.lang.String r7 = r7.f15018     // Catch:{ all -> 0x0019 }
                r4[r5] = r7     // Catch:{ all -> 0x0019 }
                r5 = 1
                java.lang.Integer r7 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0019 }
                r4[r5] = r7     // Catch:{ all -> 0x0019 }
                r2.<init>(r3, r4, r0)     // Catch:{ all -> 0x0019 }
                r1.execute(r2)     // Catch:{ all -> 0x0019 }
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                goto L_0x000d
            L_0x0072:
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                r0.m18672((java.util.List<p006do.p007do.p023new.GoroDaimon>) r12)
                if (r9 == 0) goto L_0x000d
                r0.m18659()
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: p006do.p007do.p023new.HeavyD.GoroDaimon.m18765(boolean, int, int, java.util.List):void");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18766(boolean z, int i, ChangKoehan changKoehan, int i2) throws IOException {
            if (HeavyD.this.m18728(i)) {
                HeavyD.this.m18737(i, changKoehan, i2, z);
                return;
            }
            BrianBattler r0 = HeavyD.this.m18733(i);
            if (r0 == null) {
                HeavyD.this.m18736(i, BenimaruNikaido.PROTOCOL_ERROR);
                changKoehan.m6704((long) i2);
                return;
            }
            r0.m18671(changKoehan, i2);
            if (z) {
                r0.m18659();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18767(boolean z, Mature mature) {
            BrianBattler[] brianBattlerArr;
            long j;
            synchronized (HeavyD.this) {
                int r2 = HeavyD.this.f15007.m18818();
                if (z) {
                    HeavyD.this.f15007.m18823();
                }
                HeavyD.this.f15007.m18824(mature);
                m18756(mature);
                int r4 = HeavyD.this.f15007.m18818();
                if (r4 == -1 || r4 == r2) {
                    brianBattlerArr = null;
                    j = 0;
                } else {
                    j = (long) (r4 - r2);
                    if (!HeavyD.this.f15008) {
                        HeavyD.this.m18741(j);
                        HeavyD.this.f15008 = true;
                    }
                    brianBattlerArr = !HeavyD.this.f15020.isEmpty() ? (BrianBattler[]) HeavyD.this.f15020.values().toArray(new BrianBattler[HeavyD.this.f15020.size()]) : null;
                }
                HeavyD.f15003.execute(new p006do.p007do.BenimaruNikaido("OkHttp %s settings", HeavyD.this.f15018) {
                    /* renamed from: 靐  reason: contains not printable characters */
                    public void m18769() {
                        HeavyD.this.f15021.m18754(HeavyD.this);
                    }
                });
            }
            if (brianBattlerArr != null && j != 0) {
                for (BrianBattler brianBattler : brianBattlerArr) {
                    synchronized (brianBattler) {
                        brianBattler.m18669(j);
                    }
                }
            }
        }
    }

    /* renamed from: do.do.new.HeavyD$KyoKusanagi */
    public static class KyoKusanagi {

        /* renamed from: ʻ  reason: contains not printable characters */
        LeonaHeidern f15058 = LeonaHeidern.f15072;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f15059;

        /* renamed from: 连任  reason: contains not printable characters */
        BenimaruNikaido f15060 = BenimaruNikaido.f15050;

        /* renamed from: 靐  reason: contains not printable characters */
        String f15061;

        /* renamed from: 麤  reason: contains not printable characters */
        ChinGentsai f15062;

        /* renamed from: 齉  reason: contains not printable characters */
        ChangKoehan f15063;

        /* renamed from: 龘  reason: contains not printable characters */
        Socket f15064;

        public KyoKusanagi(boolean z) {
            this.f15059 = z;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18771(BenimaruNikaido benimaruNikaido) {
            this.f15060 = benimaruNikaido;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18772(Socket socket, String str, ChangKoehan changKoehan, ChinGentsai chinGentsai) {
            this.f15064 = socket;
            this.f15061 = str;
            this.f15063 = changKoehan;
            this.f15062 = chinGentsai;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public HeavyD m18773() throws IOException {
            return new HeavyD(this);
        }
    }

    HeavyD(KyoKusanagi kyoKusanagi) {
        int i = 2;
        this.f15015 = kyoKusanagi.f15058;
        this.f15019 = kyoKusanagi.f15059;
        this.f15021 = kyoKusanagi.f15060;
        this.f15005 = kyoKusanagi.f15059 ? 1 : 2;
        if (kyoKusanagi.f15059) {
            this.f15005 += 2;
        }
        this.f15014 = kyoKusanagi.f15059 ? 1 : i;
        if (kyoKusanagi.f15059) {
            this.f15010.m18822(7, 16777216);
        }
        this.f15018 = kyoKusanagi.f15061;
        this.f15009 = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), p006do.p007do.GoroDaimon.m18420(p006do.p007do.GoroDaimon.m18416("OkHttp %s Push Observer", this.f15018), true));
        this.f15007.m18822(7, 65535);
        this.f15007.m18822(5, 16384);
        this.f15017 = (long) this.f15007.m18818();
        this.f15022 = kyoKusanagi.f15064;
        this.f15023 = new RugalBernstein(kyoKusanagi.f15062, this.f15019);
        this.f15012 = new GoroDaimon(new LuckyGlauber(kyoKusanagi.f15063, this.f15019));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private BrianBattler m18722(int i, List<GoroDaimon> list, boolean z) throws IOException {
        int i2;
        BrianBattler brianBattler;
        boolean z2 = false;
        boolean z3 = !z;
        synchronized (this.f15023) {
            synchronized (this) {
                if (this.f15006) {
                    throw new KyoKusanagi();
                }
                i2 = this.f15005;
                this.f15005 += 2;
                brianBattler = new BrianBattler(i2, this, z3, false, list);
                if (!z || this.f15017 == 0 || brianBattler.f14937 == 0) {
                    z2 = true;
                }
                if (brianBattler.m18664()) {
                    this.f15020.put(Integer.valueOf(i2), brianBattler);
                }
            }
            if (i == 0) {
                this.f15023.m18840(z3, i2, i, list);
            } else if (this.f15019) {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            } else {
                this.f15023.m18834(i, i2, list);
            }
        }
        if (z2) {
            this.f15023.m18828();
        }
        return brianBattler;
    }

    public void close() throws IOException {
        m18743(BenimaruNikaido.NO_ERROR, BenimaruNikaido.CANCEL);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized BrianBattler m18723(int i) {
        BrianBattler remove;
        remove = this.f15020.remove(Integer.valueOf(i));
        notifyAll();
        return remove;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m18724() throws IOException {
        this.f15023.m18828();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m18725(int i, BenimaruNikaido benimaruNikaido) throws IOException {
        this.f15023.m18836(i, benimaruNikaido);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m18726(boolean z, int i, int i2, SaishuKusanagi saishuKusanagi) throws IOException {
        synchronized (this.f15023) {
            if (saishuKusanagi != null) {
                saishuKusanagi.m18845();
            }
            this.f15023.m18839(z, i, i2);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized boolean m18727() {
        return this.f15006;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m18728(int i) {
        return i != 0 && (i & 1) == 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized SaishuKusanagi m18729(int i) {
        return this.f15011 != null ? this.f15011.remove(Integer.valueOf(i)) : null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m18730() throws IOException {
        m18744(true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m18731(int i, BenimaruNikaido benimaruNikaido) {
        final int i2 = i;
        final BenimaruNikaido benimaruNikaido2 = benimaruNikaido;
        this.f15009.execute(new p006do.p007do.BenimaruNikaido("OkHttp %s Push Reset[%s]", new Object[]{this.f15018, Integer.valueOf(i)}) {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m18752() {
                HeavyD.this.f15015.m18780(i2, benimaruNikaido2);
                synchronized (HeavyD.this) {
                    HeavyD.this.f15013.remove(Integer.valueOf(i2));
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized int m18732() {
        return this.f15007.m18821(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized BrianBattler m18733(int i) {
        return this.f15020.get(Integer.valueOf(i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BrianBattler m18734(List<GoroDaimon> list, boolean z) throws IOException {
        return m18722(0, list, z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18735(int i, long j) {
        final int i2 = i;
        final long j2 = j;
        f15003.execute(new p006do.p007do.BenimaruNikaido("OkHttp Window Update %s stream %d", new Object[]{this.f15018, Integer.valueOf(i)}) {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m18747() {
                try {
                    HeavyD.this.f15023.m18835(i2, j2);
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18736(int i, BenimaruNikaido benimaruNikaido) {
        final int i2 = i;
        final BenimaruNikaido benimaruNikaido2 = benimaruNikaido;
        f15003.execute(new p006do.p007do.BenimaruNikaido("OkHttp %s stream %d", new Object[]{this.f15018, Integer.valueOf(i)}) {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m18746() {
                try {
                    HeavyD.this.m18725(i2, benimaruNikaido2);
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18737(int i, ChangKoehan changKoehan, int i2, boolean z) throws IOException {
        final p009if.GoroDaimon goroDaimon = new p009if.GoroDaimon();
        changKoehan.m6711((long) i2);
        changKoehan.m19009(goroDaimon, (long) i2);
        if (goroDaimon.m6731() != ((long) i2)) {
            throw new IOException(goroDaimon.m6731() + " != " + i2);
        }
        final int i3 = i;
        final int i4 = i2;
        final boolean z2 = z;
        this.f15009.execute(new p006do.p007do.BenimaruNikaido("OkHttp %s Push Data[%s]", new Object[]{this.f15018, Integer.valueOf(i)}) {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m18751() {
                try {
                    boolean r0 = HeavyD.this.f15015.m18781(i3, goroDaimon, i4, z2);
                    if (r0) {
                        HeavyD.this.f15023.m18836(i3, BenimaruNikaido.CANCEL);
                    }
                    if (r0 || z2) {
                        synchronized (HeavyD.this) {
                            HeavyD.this.f15013.remove(Integer.valueOf(i3));
                        }
                    }
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18738(int i, List<GoroDaimon> list) {
        synchronized (this) {
            if (this.f15013.contains(Integer.valueOf(i))) {
                m18736(i, BenimaruNikaido.PROTOCOL_ERROR);
                return;
            }
            this.f15013.add(Integer.valueOf(i));
            final int i2 = i;
            final List<GoroDaimon> list2 = list;
            this.f15009.execute(new p006do.p007do.BenimaruNikaido("OkHttp %s Push Request[%s]", new Object[]{this.f15018, Integer.valueOf(i)}) {
                /* renamed from: 靐  reason: contains not printable characters */
                public void m18749() {
                    if (HeavyD.this.f15015.m18782(i2, (List<GoroDaimon>) list2)) {
                        try {
                            HeavyD.this.f15023.m18836(i2, BenimaruNikaido.CANCEL);
                            synchronized (HeavyD.this) {
                                HeavyD.this.f15013.remove(Integer.valueOf(i2));
                            }
                        } catch (IOException e) {
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18739(int i, List<GoroDaimon> list, boolean z) {
        final int i2 = i;
        final List<GoroDaimon> list2 = list;
        final boolean z2 = z;
        this.f15009.execute(new p006do.p007do.BenimaruNikaido("OkHttp %s Push Headers[%s]", new Object[]{this.f15018, Integer.valueOf(i)}) {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m18750() {
                boolean r0 = HeavyD.this.f15015.m18783(i2, list2, z2);
                if (r0) {
                    try {
                        HeavyD.this.f15023.m18836(i2, BenimaruNikaido.CANCEL);
                    } catch (IOException e) {
                        return;
                    }
                }
                if (r0 || z2) {
                    synchronized (HeavyD.this) {
                        HeavyD.this.f15013.remove(Integer.valueOf(i2));
                    }
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18740(int i, boolean z, p009if.GoroDaimon goroDaimon, long j) throws IOException {
        int min;
        if (j == 0) {
            this.f15023.m18841(z, i, goroDaimon, 0);
            return;
        }
        while (j > 0) {
            synchronized (this) {
                while (this.f15017 <= 0) {
                    try {
                        if (!this.f15020.containsKey(Integer.valueOf(i))) {
                            throw new IOException("stream closed");
                        }
                        wait();
                    } catch (InterruptedException e) {
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j, this.f15017), this.f15023.m18830());
                this.f15017 -= (long) min;
            }
            j -= (long) min;
            this.f15023.m18841(z && j == 0, i, goroDaimon, min);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18741(long j) {
        this.f15017 += j;
        if (j > 0) {
            notifyAll();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18742(BenimaruNikaido benimaruNikaido) throws IOException {
        synchronized (this.f15023) {
            synchronized (this) {
                if (!this.f15006) {
                    this.f15006 = true;
                    int i = this.f15004;
                    this.f15023.m18837(i, benimaruNikaido, p006do.p007do.GoroDaimon.f14743);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18743(BenimaruNikaido benimaruNikaido, BenimaruNikaido benimaruNikaido2) throws IOException {
        IOException iOException;
        BrianBattler[] brianBattlerArr;
        SaishuKusanagi[] saishuKusanagiArr;
        if (f15002 || !Thread.holdsLock(this)) {
            try {
                m18742(benimaruNikaido);
                iOException = null;
            } catch (IOException e) {
                iOException = e;
            }
            synchronized (this) {
                if (!this.f15020.isEmpty()) {
                    this.f15020.clear();
                    brianBattlerArr = (BrianBattler[]) this.f15020.values().toArray(new BrianBattler[this.f15020.size()]);
                } else {
                    brianBattlerArr = null;
                }
                if (this.f15011 != null) {
                    this.f15011 = null;
                    saishuKusanagiArr = (SaishuKusanagi[]) this.f15011.values().toArray(new SaishuKusanagi[this.f15011.size()]);
                } else {
                    saishuKusanagiArr = null;
                }
            }
            if (brianBattlerArr != null) {
                IOException iOException2 = iOException;
                for (BrianBattler r1 : brianBattlerArr) {
                    try {
                        r1.m18670(benimaruNikaido2);
                    } catch (IOException e2) {
                        if (iOException2 != null) {
                            iOException2 = e2;
                        }
                    }
                }
                iOException = iOException2;
            }
            if (saishuKusanagiArr != null) {
                for (SaishuKusanagi r3 : saishuKusanagiArr) {
                    r3.m18844();
                }
            }
            try {
                this.f15023.close();
                e = iOException;
            } catch (IOException e3) {
                e = e3;
                if (iOException != null) {
                    e = iOException;
                }
            }
            try {
                this.f15022.close();
            } catch (IOException e4) {
                e = e4;
            }
            if (e != null) {
                throw e;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18744(boolean z) throws IOException {
        if (z) {
            this.f15023.m18831();
            this.f15023.m18829(this.f15010);
            int r0 = this.f15010.m18818();
            if (r0 != 65535) {
                this.f15023.m18835(0, (long) (r0 - 65535));
            }
        }
        new Thread(this.f15012).start();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18745(boolean z, int i, int i2, SaishuKusanagi saishuKusanagi) {
        final boolean z2 = z;
        final int i3 = i;
        final int i4 = i2;
        final SaishuKusanagi saishuKusanagi2 = saishuKusanagi;
        f15003.execute(new p006do.p007do.BenimaruNikaido("OkHttp %s ping %08x%08x", new Object[]{this.f15018, Integer.valueOf(i), Integer.valueOf(i2)}) {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m18748() {
                try {
                    HeavyD.this.m18726(z2, i3, i4, saishuKusanagi2);
                } catch (IOException e) {
                }
            }
        });
    }
}
