package p006do.p007do.p017byte;

import android.util.Log;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import p006do.KDash;
import p006do.p007do.GoroDaimon;

/* renamed from: do.do.byte.KyoKusanagi  reason: invalid package */
class KyoKusanagi extends ChangKoehan {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final BenimaruNikaido f14764 = BenimaruNikaido.m18476();

    /* renamed from: 连任  reason: contains not printable characters */
    private final ChinGentsai<Socket> f14765;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ChinGentsai<Socket> f14766;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ChinGentsai<Socket> f14767;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ChinGentsai<Socket> f14768;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Class<?> f14769;

    /* renamed from: do.do.byte.KyoKusanagi$BenimaruNikaido */
    static final class BenimaruNikaido {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Method f14770;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Method f14771;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Method f14772;

        BenimaruNikaido(Method method, Method method2, Method method3) {
            this.f14772 = method;
            this.f14770 = method2;
            this.f14771 = method3;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static BenimaruNikaido m18476() {
            Method method;
            Method method2;
            Method method3;
            try {
                Class<?> cls = Class.forName("dalvik.system.CloseGuard");
                method3 = cls.getMethod("get", new Class[0]);
                method2 = cls.getMethod("open", new Class[]{String.class});
                method = cls.getMethod("warnIfOpen", new Class[0]);
            } catch (Exception e) {
                method = null;
                method2 = null;
                method3 = null;
            }
            return new BenimaruNikaido(method3, method2, method);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Object m18477(String str) {
            if (this.f14772 != null) {
                try {
                    Object invoke = this.f14772.invoke((Object) null, new Object[0]);
                    this.f14770.invoke(invoke, new Object[]{str});
                    return invoke;
                } catch (Exception e) {
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m18478(Object obj) {
            if (obj == null) {
                return false;
            }
            try {
                this.f14771.invoke(obj, new Object[0]);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    }

    /* renamed from: do.do.byte.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    static final class C0035KyoKusanagi extends p006do.p007do.p019char.BenimaruNikaido {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Method f14773;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object f14774;

        C0035KyoKusanagi(Object obj, Method method) {
            this.f14774 = obj;
            this.f14773 = method;
        }

        public boolean equals(Object obj) {
            return obj instanceof C0035KyoKusanagi;
        }

        public int hashCode() {
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public List<Certificate> m18479(List<Certificate> list, String str) throws SSLPeerUnverifiedException {
            try {
                return (List) this.f14773.invoke(this.f14774, new Object[]{(X509Certificate[]) list.toArray(new X509Certificate[list.size()]), "RSA", str});
            } catch (InvocationTargetException e) {
                SSLPeerUnverifiedException sSLPeerUnverifiedException = new SSLPeerUnverifiedException(e.getMessage());
                sSLPeerUnverifiedException.initCause(e);
                throw sSLPeerUnverifiedException;
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }
    }

    KyoKusanagi(Class<?> cls, ChinGentsai<Socket> chinGentsai, ChinGentsai<Socket> chinGentsai2, ChinGentsai<Socket> chinGentsai3, ChinGentsai<Socket> chinGentsai4) {
        this.f14769 = cls;
        this.f14766 = chinGentsai;
        this.f14768 = chinGentsai2;
        this.f14767 = chinGentsai3;
        this.f14765 = chinGentsai4;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ChangKoehan m18467() {
        Class<?> cls;
        ChinGentsai chinGentsai;
        ChinGentsai chinGentsai2;
        ChinGentsai chinGentsai3;
        try {
            cls = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
            try {
                ChinGentsai chinGentsai4 = new ChinGentsai((Class<?>) null, "setUseSessionTickets", Boolean.TYPE);
                ChinGentsai chinGentsai5 = new ChinGentsai((Class<?>) null, "setHostname", String.class);
                try {
                    Class.forName("android.net.Network");
                    chinGentsai = new ChinGentsai(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
                    try {
                        chinGentsai2 = new ChinGentsai((Class<?>) null, "setAlpnProtocols", byte[].class);
                        chinGentsai3 = chinGentsai;
                    } catch (ClassNotFoundException e) {
                        chinGentsai2 = null;
                        chinGentsai3 = chinGentsai;
                        return new KyoKusanagi(cls, chinGentsai4, chinGentsai5, chinGentsai3, chinGentsai2);
                    }
                } catch (ClassNotFoundException e2) {
                    chinGentsai = null;
                    chinGentsai2 = null;
                    chinGentsai3 = chinGentsai;
                    return new KyoKusanagi(cls, chinGentsai4, chinGentsai5, chinGentsai3, chinGentsai2);
                }
                return new KyoKusanagi(cls, chinGentsai4, chinGentsai5, chinGentsai3, chinGentsai2);
            } catch (ClassNotFoundException e3) {
                return null;
            }
        } catch (ClassNotFoundException e4) {
            cls = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Object m18468(String str) {
        return this.f14764.m18477(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public p006do.p007do.p019char.BenimaruNikaido m18469(X509TrustManager x509TrustManager) {
        try {
            Class<?> cls = Class.forName("android.net.http.X509TrustManagerExtensions");
            return new C0035KyoKusanagi(cls.getConstructor(new Class[]{X509TrustManager.class}).newInstance(new Object[]{x509TrustManager}), cls.getMethod("checkServerTrusted", new Class[]{X509Certificate[].class, String.class, String.class}));
        } catch (Exception e) {
            return super.m18449(x509TrustManager);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m18470(SSLSocket sSLSocket) {
        if (this.f14767 == null || !this.f14767.m18462(sSLSocket)) {
            return null;
        }
        byte[] bArr = (byte[]) this.f14767.m18459(sSLSocket, new Object[0]);
        return bArr != null ? new String(bArr, GoroDaimon.f14739) : null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18471(int i, String str, Throwable th) {
        int min;
        int i2 = i == 5 ? 5 : 3;
        if (th != null) {
            str = str + 10 + Log.getStackTraceString(th);
        }
        int i3 = 0;
        int length = str.length();
        while (i3 < length) {
            int indexOf = str.indexOf(10, i3);
            if (indexOf == -1) {
                indexOf = length;
            }
            while (true) {
                min = Math.min(indexOf, i3 + 4000);
                Log.println(i2, "OkHttp", str.substring(i3, min));
                if (min >= indexOf) {
                    break;
                }
                i3 = min;
            }
            i3 = min + 1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18472(String str, Object obj) {
        if (!this.f14764.m18478(obj)) {
            m18471(5, str, (Throwable) null);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18473(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        try {
            socket.connect(inetSocketAddress, i);
        } catch (AssertionError e) {
            if (GoroDaimon.m18425(e)) {
                throw new IOException(e);
            }
            throw e;
        } catch (SecurityException e2) {
            IOException iOException = new IOException("Exception in connect");
            iOException.initCause(e2);
            throw iOException;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18474(SSLSocket sSLSocket, String str, List<KDash> list) {
        if (str != null) {
            this.f14766.m18458(sSLSocket, true);
            this.f14768.m18458(sSLSocket, str);
        }
        if (this.f14765 != null && this.f14765.m18462(sSLSocket)) {
            this.f14765.m18459(sSLSocket, m18444(list));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18475(String str) {
        try {
            Class<?> cls = Class.forName("android.security.NetworkSecurityPolicy");
            Object invoke = cls.getMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]);
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", new Class[]{String.class}).invoke(invoke, new Object[]{str})).booleanValue();
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            return super.m18455(str);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
            throw new AssertionError();
        }
    }
}
