package p006do.p007do.p017byte;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import javax.net.ssl.SSLSocket;
import p006do.KDash;

/* renamed from: do.do.byte.GoroDaimon  reason: invalid package */
class GoroDaimon extends ChangKoehan {

    /* renamed from: 连任  reason: contains not printable characters */
    private final Class<?> f14756;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Method f14757;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Class<?> f14758;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Method f14759;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Method f14760;

    /* renamed from: do.do.byte.GoroDaimon$KyoKusanagi */
    private static class KyoKusanagi implements InvocationHandler {

        /* renamed from: 靐  reason: contains not printable characters */
        String f14761;

        /* renamed from: 齉  reason: contains not printable characters */
        private final List<String> f14762;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f14763;

        KyoKusanagi(List<String> list) {
            this.f14762 = list;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String name = method.getName();
            Class<?> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = p006do.p007do.GoroDaimon.f14740;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return true;
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.f14763 = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.f14762;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.f14762.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.f14761 = str;
                            return str;
                        }
                    }
                    String str2 = this.f14762.get(0);
                    this.f14761 = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.f14761 = (String) objArr[0];
                    return null;
                }
            }
        }
    }

    GoroDaimon(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
        this.f14760 = method;
        this.f14757 = method2;
        this.f14759 = method3;
        this.f14758 = cls;
        this.f14756 = cls2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ChangKoehan m18463() {
        try {
            Class<?> cls = Class.forName("org.eclipse.jetty.alpn.ALPN");
            Class<?> cls2 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$Provider");
            Class<?> cls3 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ClientProvider");
            Class<?> cls4 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ServerProvider");
            return new GoroDaimon(cls.getMethod("put", new Class[]{SSLSocket.class, cls2}), cls.getMethod("get", new Class[]{SSLSocket.class}), cls.getMethod("remove", new Class[]{SSLSocket.class}), cls3, cls4);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m18464(SSLSocket sSLSocket) {
        try {
            this.f14759.invoke((Object) null, new Object[]{sSLSocket});
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m18465(SSLSocket sSLSocket) {
        try {
            KyoKusanagi kyoKusanagi = (KyoKusanagi) Proxy.getInvocationHandler(this.f14757.invoke((Object) null, new Object[]{sSLSocket}));
            if (kyoKusanagi.f14763 || kyoKusanagi.f14761 != null) {
                return kyoKusanagi.f14763 ? null : kyoKusanagi.f14761;
            }
            ChangKoehan.m18443().m18451(4, "ALPN callback dropped: HTTP/2 is disabled. Is alpn-boot on the boot class path?", (Throwable) null);
            return null;
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18466(SSLSocket sSLSocket, String str, List<KDash> list) {
        List<String> r0 = m18446(list);
        try {
            Object newProxyInstance = Proxy.newProxyInstance(ChangKoehan.class.getClassLoader(), new Class[]{this.f14758, this.f14756}, new KyoKusanagi(r0));
            this.f14760.invoke((Object) null, new Object[]{sSLSocket, newProxyInstance});
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new AssertionError(e);
        }
    }
}
