package p006do.p007do.p017byte;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import p006do.KDash;
import p006do.Orochi;
import p006do.p007do.p019char.BenimaruNikaido;
import p006do.p007do.p019char.KyoKusanagi;
import p009if.GoroDaimon;

/* renamed from: do.do.byte.ChangKoehan  reason: invalid package */
public class ChangKoehan {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Logger f14751 = Logger.getLogger(Orochi.class.getName());

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ChangKoehan f14752 = m18445();

    /* renamed from: 靐  reason: contains not printable characters */
    public static ChangKoehan m18443() {
        return f14752;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static byte[] m18444(List<KDash> list) {
        GoroDaimon goroDaimon = new GoroDaimon();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            KDash kDash = list.get(i);
            if (kDash != KDash.HTTP_1_0) {
                goroDaimon.m6770(kDash.toString().length());
                goroDaimon.m6771(kDash.toString());
            }
        }
        return goroDaimon.m6738();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ChangKoehan m18445() {
        ChangKoehan r0 = KyoKusanagi.m18467();
        if (r0 != null) {
            return r0;
        }
        BenimaruNikaido r02 = BenimaruNikaido.m18440();
        if (r02 != null) {
            return r02;
        }
        ChangKoehan r03 = GoroDaimon.m18463();
        return r03 == null ? new ChangKoehan() : r03;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<String> m18446(List<KDash> list) {
        ArrayList arrayList = new ArrayList(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            KDash kDash = list.get(i);
            if (kDash != KDash.HTTP_1_0) {
                arrayList.add(kDash.toString());
            }
        }
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Object m18447(String str) {
        if (f14751.isLoggable(Level.FINE)) {
            return new Throwable(str);
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m18448(SSLSocket sSLSocket) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public BenimaruNikaido m18449(X509TrustManager x509TrustManager) {
        return new KyoKusanagi(p006do.p007do.p019char.ChangKoehan.m18487(x509TrustManager));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m18450(SSLSocket sSLSocket) {
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18451(int i, String str, Throwable th) {
        f14751.log(i == 5 ? Level.WARNING : Level.INFO, str, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18452(String str, Object obj) {
        if (obj == null) {
            str = str + " To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);";
        }
        m18451(5, str, (Throwable) obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18453(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        socket.connect(inetSocketAddress, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18454(SSLSocket sSLSocket, String str, List<KDash> list) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18455(String str) {
        return true;
    }
}
