package p006do.p007do.p017byte;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import p006do.KDash;

/* renamed from: do.do.byte.BenimaruNikaido  reason: invalid package */
final class BenimaruNikaido extends ChangKoehan {

    /* renamed from: 靐  reason: contains not printable characters */
    final Method f14749;

    /* renamed from: 龘  reason: contains not printable characters */
    final Method f14750;

    BenimaruNikaido(Method method, Method method2) {
        this.f14750 = method;
        this.f14749 = method2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static BenimaruNikaido m18440() {
        try {
            return new BenimaruNikaido(SSLParameters.class.getMethod("setApplicationProtocols", new Class[]{String[].class}), SSLSocket.class.getMethod("getApplicationProtocol", new Class[0]));
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m18441(SSLSocket sSLSocket) {
        try {
            String str = (String) this.f14749.invoke(sSLSocket, new Object[0]);
            if (str == null || str.equals("")) {
                return null;
            }
            return str;
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18442(SSLSocket sSLSocket, String str, List<KDash> list) {
        try {
            SSLParameters sSLParameters = sSLSocket.getSSLParameters();
            List<String> r1 = m18446(list);
            this.f14750.invoke(sSLParameters, new Object[]{r1.toArray(new String[r1.size()])});
            sSLSocket.setSSLParameters(sSLParameters);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new AssertionError();
        }
    }
}
