package p006do.p007do.p017byte;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: do.do.byte.ChinGentsai  reason: invalid package */
class ChinGentsai<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f14753;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Class[] f14754;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Class<?> f14755;

    ChinGentsai(Class<?> cls, String str, Class... clsArr) {
        this.f14755 = cls;
        this.f14753 = str;
        this.f14754 = clsArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Method m18456(Class<?> cls) {
        if (this.f14753 == null) {
            return null;
        }
        Method r1 = m18457(cls, this.f14753, this.f14754);
        if (r1 == null || this.f14755 == null || this.f14755.isAssignableFrom(r1.getReturnType())) {
            return r1;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Method m18457(Class<?> cls, String str, Class[] clsArr) {
        try {
            Method method = cls.getMethod(str, clsArr);
            try {
                if ((method.getModifiers() & 1) == 0) {
                    return null;
                }
                return method;
            } catch (NoSuchMethodException e) {
                return method;
            }
        } catch (NoSuchMethodException e2) {
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Object m18458(T t, Object... objArr) {
        try {
            return m18461(t, objArr);
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Object m18459(T t, Object... objArr) {
        try {
            return m18460(t, objArr);
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Object m18460(T t, Object... objArr) throws InvocationTargetException {
        Method r0 = m18456(t.getClass());
        if (r0 == null) {
            throw new AssertionError("Method " + this.f14753 + " not supported for object " + t);
        }
        try {
            return r0.invoke(t, objArr);
        } catch (IllegalAccessException e) {
            AssertionError assertionError = new AssertionError("Unexpectedly could not call: " + r0);
            assertionError.initCause(e);
            throw assertionError;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18461(T t, Object... objArr) throws InvocationTargetException {
        Method r1 = m18456(t.getClass());
        if (r1 == null) {
            return null;
        }
        try {
            return r1.invoke(t, objArr);
        } catch (IllegalAccessException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18462(T t) {
        return m18456(t.getClass()) != null;
    }
}
