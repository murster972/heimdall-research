package p006do.p007do.p018case;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.IDN;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import p006do.p007do.GoroDaimon;
import p009if.ChangKoehan;
import p009if.IoriYagami;
import p009if.LeonaHeidern;
import p009if.Shermie;

/* renamed from: do.do.case.KyoKusanagi  reason: invalid package */
public final class KyoKusanagi {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f14775 = new String[0];

    /* renamed from: 麤  reason: contains not printable characters */
    private static final KyoKusanagi f14776 = new KyoKusanagi();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final String[] f14777 = {"*"};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final byte[] f14778 = {42};

    /* renamed from: ʻ  reason: contains not printable characters */
    private final CountDownLatch f14779 = new CountDownLatch(1);

    /* renamed from: ʼ  reason: contains not printable characters */
    private byte[] f14780;

    /* renamed from: ʽ  reason: contains not printable characters */
    private byte[] f14781;

    /* renamed from: 连任  reason: contains not printable characters */
    private final AtomicBoolean f14782 = new AtomicBoolean(false);

    /* JADX INFO: finally extract failed */
    /* renamed from: 靐  reason: contains not printable characters */
    private void m18480() {
        byte[] bArr;
        byte[] bArr2;
        InputStream resourceAsStream = KyoKusanagi.class.getClassLoader().getResourceAsStream("publicsuffixes.gz");
        if (resourceAsStream != null) {
            ChangKoehan r3 = LeonaHeidern.m18966((Shermie) new IoriYagami(LeonaHeidern.m18969(resourceAsStream)));
            try {
                bArr2 = new byte[r3.m6696()];
                r3.m6712(bArr2);
                bArr = new byte[r3.m6696()];
                r3.m6712(bArr);
                GoroDaimon.m18422((Closeable) r3);
            } catch (IOException e) {
                p006do.p007do.p017byte.ChangKoehan.m18443().m18451(5, "Failed to read public suffix list", (Throwable) e);
                GoroDaimon.m18422((Closeable) r3);
                bArr = null;
                bArr2 = null;
            } catch (Throwable th) {
                GoroDaimon.m18422((Closeable) r3);
                throw th;
            }
        } else {
            bArr = null;
            bArr2 = null;
        }
        synchronized (this) {
            this.f14780 = bArr2;
            this.f14781 = bArr;
        }
        this.f14779.countDown();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static KyoKusanagi m18481() {
        return f14776;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m18482(byte[] bArr, byte[][] bArr2, int i) {
        byte b;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int length = bArr.length;
        while (i7 < length) {
            int i8 = (i7 + length) / 2;
            while (i8 > -1 && bArr[i8] != 10) {
                i8--;
            }
            int i9 = i8 + 1;
            int i10 = 1;
            while (bArr[i9 + i10] != 10) {
                i10++;
            }
            int i11 = (i9 + i10) - i9;
            int i12 = 0;
            int i13 = 0;
            boolean z = false;
            int i14 = i;
            while (true) {
                if (z) {
                    b = 46;
                    z = false;
                } else {
                    b = bArr2[i14][i12] & 255;
                }
                i2 = b - (bArr[i9 + i13] & 255);
                if (i2 != 0) {
                    i3 = i13;
                    i4 = i12;
                    break;
                }
                i13++;
                i4 = i12 + 1;
                if (i13 == i11) {
                    i3 = i13;
                    break;
                }
                if (bArr2[i14].length == i4) {
                    if (i14 == bArr2.length - 1) {
                        i3 = i13;
                        break;
                    }
                    i14++;
                    i4 = -1;
                    z = true;
                }
                i12 = i4;
            }
            if (i2 < 0) {
                i6 = i9 - 1;
                i5 = i7;
            } else if (i2 > 0) {
                i5 = i10 + i9 + 1;
                i6 = length;
            } else {
                int i15 = i11 - i3;
                int length2 = bArr2[i14].length - i4;
                for (int i16 = i14 + 1; i16 < bArr2.length; i16++) {
                    length2 += bArr2[i16].length;
                }
                if (length2 < i15) {
                    i6 = i9 - 1;
                    i5 = i7;
                } else if (length2 <= i15) {
                    return new String(bArr, i9, i11, GoroDaimon.f14739);
                } else {
                    i5 = i10 + i9 + 1;
                    i6 = length;
                }
            }
            length = i6;
            i7 = i5;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String[] m18483(String[] strArr) {
        String str;
        String str2;
        String str3 = null;
        int i = 0;
        if (this.f14782.get() || !this.f14782.compareAndSet(false, true)) {
            try {
                this.f14779.await();
            } catch (InterruptedException e) {
            }
        } else {
            m18480();
        }
        synchronized (this) {
            if (this.f14780 == null) {
                throw new IllegalStateException("Unable to load publicsuffixes.gz resource from the classpath.");
            }
        }
        byte[][] bArr = new byte[strArr.length][];
        for (int i2 = 0; i2 < strArr.length; i2++) {
            bArr[i2] = strArr[i2].getBytes(GoroDaimon.f14739);
        }
        int i3 = 0;
        while (true) {
            if (i3 >= bArr.length) {
                str = null;
                break;
            }
            str = m18482(this.f14780, bArr, i3);
            if (str != null) {
                break;
            }
            i3++;
        }
        if (bArr.length > 1) {
            byte[][] bArr2 = (byte[][]) bArr.clone();
            int i4 = 0;
            while (true) {
                if (i4 >= bArr2.length - 1) {
                    break;
                }
                bArr2[i4] = f14778;
                String r5 = m18482(this.f14780, bArr2, i4);
                if (r5 != null) {
                    str2 = r5;
                    break;
                }
                i4++;
            }
        }
        str2 = null;
        if (str2 != null) {
            while (true) {
                if (i >= bArr.length - 1) {
                    break;
                }
                String r0 = m18482(this.f14781, bArr, i);
                if (r0 != null) {
                    str3 = r0;
                    break;
                }
                i++;
            }
        }
        if (str3 != null) {
            return ("!" + str3).split("\\.");
        }
        if (str == null && str2 == null) {
            return f14777;
        }
        String[] split = str != null ? str.split("\\.") : f14775;
        String[] split2 = str2 != null ? str2.split("\\.") : f14775;
        return split.length > split2.length ? split : split2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m18484(String str) {
        if (str == null) {
            throw new NullPointerException("domain == null");
        }
        String[] split = IDN.toUnicode(str).split("\\.");
        String[] r1 = m18483(split);
        if (split.length == r1.length && r1[0].charAt(0) != '!') {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        String[] split2 = str.split("\\.");
        for (int length = r1[0].charAt(0) == '!' ? split.length - r1.length : split.length - (r1.length + 1); length < split2.length; length++) {
            sb.append(split2[length]).append('.');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
