package p006do.p007do.p022int;

import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import p006do.JhunHoon;
import p006do.Krizalid;
import p006do.Orochi;
import p006do.ShingoYabuki;
import p006do.Whip;
import p006do.p007do.p020for.IoriYagami;
import p006do.p007do.p020for.LuckyGlauber;
import p006do.p007do.p021if.HeavyD;
import p009if.BrianBattler;
import p009if.Chris;
import p009if.LeonaHeidern;
import p009if.Shermie;
import p009if.YashiroNanakase;

/* renamed from: do.do.int.KyoKusanagi  reason: invalid package */
public final class KyoKusanagi implements p006do.p007do.p020for.GoroDaimon {

    /* renamed from: 连任  reason: contains not printable characters */
    int f14904 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    final HeavyD f14905;

    /* renamed from: 麤  reason: contains not printable characters */
    final p009if.ChinGentsai f14906;

    /* renamed from: 齉  reason: contains not printable characters */
    final p009if.ChangKoehan f14907;

    /* renamed from: 龘  reason: contains not printable characters */
    final Orochi f14908;

    /* renamed from: do.do.int.KyoKusanagi$BenimaruNikaido */
    private final class BenimaruNikaido implements YashiroNanakase {

        /* renamed from: 靐  reason: contains not printable characters */
        private final BrianBattler f14909 = new BrianBattler(KyoKusanagi.this.f14906.m19014());

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f14910;

        BenimaruNikaido() {
        }

        public void a_(p009if.GoroDaimon goroDaimon, long j) throws IOException {
            if (this.f14910) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                KyoKusanagi.this.f14906.m18925(j);
                KyoKusanagi.this.f14906.m18935("\r\n");
                KyoKusanagi.this.f14906.a_(goroDaimon, j);
                KyoKusanagi.this.f14906.m18935("\r\n");
            }
        }

        public synchronized void close() throws IOException {
            if (!this.f14910) {
                this.f14910 = true;
                KyoKusanagi.this.f14906.m18935("0\r\n\r\n");
                KyoKusanagi.this.m18645(this.f14909);
                KyoKusanagi.this.f14904 = 3;
            }
        }

        public synchronized void flush() throws IOException {
            if (!this.f14910) {
                KyoKusanagi.this.f14906.flush();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Chris m18646() {
            return this.f14909;
        }
    }

    /* renamed from: do.do.int.KyoKusanagi$ChangKoehan */
    private class ChangKoehan extends C0036KyoKusanagi {

        /* renamed from: 连任  reason: contains not printable characters */
        private long f14912;

        ChangKoehan(long j) throws IOException {
            super();
            this.f14912 = j;
            if (this.f14912 == 0) {
                m18653(true);
            }
        }

        public void close() throws IOException {
            if (!this.f14926) {
                if (this.f14912 != 0 && !p006do.p007do.GoroDaimon.m18424((Shermie) this, 100, TimeUnit.MILLISECONDS)) {
                    m18653(false);
                }
                this.f14926 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m18647(p009if.GoroDaimon goroDaimon, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f14926) {
                throw new IllegalStateException("closed");
            } else if (this.f14912 == 0) {
                return -1;
            } else {
                long r2 = KyoKusanagi.this.f14907.m19009(goroDaimon, Math.min(this.f14912, j));
                if (r2 == -1) {
                    m18653(false);
                    throw new ProtocolException("unexpected end of stream");
                }
                this.f14912 -= r2;
                if (this.f14912 == 0) {
                    m18653(true);
                }
                return r2;
            }
        }
    }

    /* renamed from: do.do.int.KyoKusanagi$ChinGentsai */
    private final class ChinGentsai implements YashiroNanakase {

        /* renamed from: 靐  reason: contains not printable characters */
        private final BrianBattler f14914 = new BrianBattler(KyoKusanagi.this.f14906.m19014());

        /* renamed from: 麤  reason: contains not printable characters */
        private long f14915;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f14916;

        ChinGentsai(long j) {
            this.f14915 = j;
        }

        public void a_(p009if.GoroDaimon goroDaimon, long j) throws IOException {
            if (this.f14916) {
                throw new IllegalStateException("closed");
            }
            p006do.p007do.GoroDaimon.m18421(goroDaimon.m6731(), 0, j);
            if (j > this.f14915) {
                throw new ProtocolException("expected " + this.f14915 + " bytes but received " + j);
            }
            KyoKusanagi.this.f14906.a_(goroDaimon, j);
            this.f14915 -= j;
        }

        public void close() throws IOException {
            if (!this.f14916) {
                this.f14916 = true;
                if (this.f14915 > 0) {
                    throw new ProtocolException("unexpected end of stream");
                }
                KyoKusanagi.this.m18645(this.f14914);
                KyoKusanagi.this.f14904 = 3;
            }
        }

        public void flush() throws IOException {
            if (!this.f14916) {
                KyoKusanagi.this.f14906.flush();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Chris m18648() {
            return this.f14914;
        }
    }

    /* renamed from: do.do.int.KyoKusanagi$ChoiBounge */
    private class ChoiBounge extends C0036KyoKusanagi {

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f14918;

        ChoiBounge() {
            super();
        }

        public void close() throws IOException {
            if (!this.f14926) {
                if (!this.f14918) {
                    m18653(false);
                }
                this.f14926 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m18649(p009if.GoroDaimon goroDaimon, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f14926) {
                throw new IllegalStateException("closed");
            } else if (this.f14918) {
                return -1;
            } else {
                long r2 = KyoKusanagi.this.f14907.m19009(goroDaimon, j);
                if (r2 != -1) {
                    return r2;
                }
                this.f14918 = true;
                m18653(true);
                return -1;
            }
        }
    }

    /* renamed from: do.do.int.KyoKusanagi$GoroDaimon */
    private class GoroDaimon extends C0036KyoKusanagi {

        /* renamed from: ʻ  reason: contains not printable characters */
        private long f14920 = -1;

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f14921 = true;

        /* renamed from: 连任  reason: contains not printable characters */
        private final p006do.YashiroNanakase f14922;

        GoroDaimon(p006do.YashiroNanakase yashiroNanakase) {
            super();
            this.f14922 = yashiroNanakase;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m18650() throws IOException {
            if (this.f14920 != -1) {
                KyoKusanagi.this.f14907.m6701();
            }
            try {
                this.f14920 = KyoKusanagi.this.f14907.m6699();
                String trim = KyoKusanagi.this.f14907.m6701().trim();
                if (this.f14920 < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.f14920 + trim + "\"");
                } else if (this.f14920 == 0) {
                    this.f14921 = false;
                    p006do.p007do.p020for.ChangKoehan.m18547(KyoKusanagi.this.f14908.m6524(), this.f14922, KyoKusanagi.this.m18636());
                    m18653(true);
                }
            } catch (NumberFormatException e) {
                throw new ProtocolException(e.getMessage());
            }
        }

        public void close() throws IOException {
            if (!this.f14926) {
                if (this.f14921 && !p006do.p007do.GoroDaimon.m18424((Shermie) this, 100, TimeUnit.MILLISECONDS)) {
                    m18653(false);
                }
                this.f14926 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m18651(p009if.GoroDaimon goroDaimon, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f14926) {
                throw new IllegalStateException("closed");
            } else if (!this.f14921) {
                return -1;
            } else {
                if (this.f14920 == 0 || this.f14920 == -1) {
                    m18650();
                    if (!this.f14921) {
                        return -1;
                    }
                }
                long r2 = KyoKusanagi.this.f14907.m19009(goroDaimon, Math.min(j, this.f14920));
                if (r2 == -1) {
                    m18653(false);
                    throw new ProtocolException("unexpected end of stream");
                }
                this.f14920 -= r2;
                return r2;
            }
        }
    }

    /* renamed from: do.do.int.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    private abstract class C0036KyoKusanagi implements Shermie {

        /* renamed from: 靐  reason: contains not printable characters */
        protected final BrianBattler f14924;

        /* renamed from: 齉  reason: contains not printable characters */
        protected boolean f14926;

        private C0036KyoKusanagi() {
            this.f14924 = new BrianBattler(KyoKusanagi.this.f14907.m19010());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Chris m18652() {
            return this.f14924;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m18653(boolean z) throws IOException {
            if (KyoKusanagi.this.f14904 != 6) {
                if (KyoKusanagi.this.f14904 != 5) {
                    throw new IllegalStateException("state: " + KyoKusanagi.this.f14904);
                }
                KyoKusanagi.this.m18645(this.f14924);
                KyoKusanagi.this.f14904 = 6;
                if (KyoKusanagi.this.f14905 != null) {
                    KyoKusanagi.this.f14905.m18630(!z, (p006do.p007do.p020for.GoroDaimon) KyoKusanagi.this);
                }
            }
        }
    }

    public KyoKusanagi(Orochi orochi, HeavyD heavyD, p009if.ChangKoehan changKoehan, p009if.ChinGentsai chinGentsai) {
        this.f14908 = orochi;
        this.f14905 = heavyD;
        this.f14907 = changKoehan;
        this.f14906 = chinGentsai;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Shermie m18631(JhunHoon jhunHoon) throws IOException {
        if (!p006do.p007do.p020for.ChangKoehan.m18541(jhunHoon)) {
            return m18633(0);
        }
        if ("chunked".equalsIgnoreCase(jhunHoon.m6473("Transfer-Encoding"))) {
            return m18639(jhunHoon.m6472().m6592());
        }
        long r0 = p006do.p007do.p020for.ChangKoehan.m18544(jhunHoon);
        return r0 != -1 ? m18633(r0) : m18632();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Shermie m18632() throws IOException {
        if (this.f14904 != 4) {
            throw new IllegalStateException("state: " + this.f14904);
        } else if (this.f14905 == null) {
            throw new IllegalStateException("streamAllocation == null");
        } else {
            this.f14904 = 5;
            this.f14905.m18624();
            return new ChoiBounge();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Shermie m18633(long j) throws IOException {
        if (this.f14904 != 4) {
            throw new IllegalStateException("state: " + this.f14904);
        }
        this.f14904 = 5;
        return new ChangKoehan(j);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m18634() throws IOException {
        this.f14906.flush();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public YashiroNanakase m18635() {
        if (this.f14904 != 1) {
            throw new IllegalStateException("state: " + this.f14904);
        }
        this.f14904 = 2;
        return new BenimaruNikaido();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public ShingoYabuki m18636() throws IOException {
        ShingoYabuki.KyoKusanagi kyoKusanagi = new ShingoYabuki.KyoKusanagi();
        while (true) {
            String r1 = this.f14907.m6701();
            if (r1.length() == 0) {
                return kyoKusanagi.m18386();
            }
            p006do.p007do.KyoKusanagi.f14748.m18436(kyoKusanagi, r1);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public JhunHoon.KyoKusanagi m18637(boolean z) throws IOException {
        if (this.f14904 == 1 || this.f14904 == 3) {
            try {
                IoriYagami r1 = IoriYagami.m18568(this.f14907.m6701());
                JhunHoon.KyoKusanagi r0 = new JhunHoon.KyoKusanagi().m6484(r1.f14853).m6480(r1.f14851).m6488(r1.f14852).m6486(m18636());
                if (z && r1.f14851 == 100) {
                    return null;
                }
                this.f14904 = 4;
                return r0;
            } catch (EOFException e) {
                IOException iOException = new IOException("unexpected end of stream on " + this.f14905);
                iOException.initCause(e);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.f14904);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Krizalid m18638(JhunHoon jhunHoon) throws IOException {
        return new LuckyGlauber(jhunHoon.m6468(), LeonaHeidern.m18966(m18631(jhunHoon)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Shermie m18639(p006do.YashiroNanakase yashiroNanakase) throws IOException {
        if (this.f14904 != 4) {
            throw new IllegalStateException("state: " + this.f14904);
        }
        this.f14904 = 5;
        return new GoroDaimon(yashiroNanakase);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public YashiroNanakase m18640(long j) {
        if (this.f14904 != 1) {
            throw new IllegalStateException("state: " + this.f14904);
        }
        this.f14904 = 2;
        return new ChinGentsai(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public YashiroNanakase m18641(Whip whip, long j) {
        if ("chunked".equalsIgnoreCase(whip.m6593("Transfer-Encoding"))) {
            return m18635();
        }
        if (j != -1) {
            return m18640(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18642() throws IOException {
        this.f14906.flush();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18643(ShingoYabuki shingoYabuki, String str) throws IOException {
        if (this.f14904 != 0) {
            throw new IllegalStateException("state: " + this.f14904);
        }
        this.f14906.m18935(str).m18935("\r\n");
        int r1 = shingoYabuki.m6583();
        for (int i = 0; i < r1; i++) {
            this.f14906.m18935(shingoYabuki.m6584(i)).m18935(": ").m18935(shingoYabuki.m6579(i)).m18935("\r\n");
        }
        this.f14906.m18935("\r\n");
        this.f14904 = 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18644(Whip whip) throws IOException {
        m18643(whip.m6591(), p006do.p007do.p020for.BrianBattler.m18538(whip, this.f14905.m18622().m18608().m18376().type()));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18645(BrianBattler brianBattler) {
        Chris r0 = brianBattler.m18922();
        brianBattler.m18921(Chris.f15141);
        r0.m18941();
        r0.m18943();
    }
}
