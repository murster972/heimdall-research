package p006do.p007do.p021if;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: do.do.if.ChangKoehan  reason: invalid package */
public final class ChangKoehan extends RuntimeException {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Method f14866;
    private IOException b;

    static {
        Method method;
        try {
            method = Throwable.class.getDeclaredMethod("addSuppressed", new Class[]{Throwable.class});
        } catch (Exception e) {
            method = null;
        }
        f14866 = method;
    }

    public ChangKoehan(IOException iOException) {
        super(iOException);
        this.b = iOException;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18583(IOException iOException, IOException iOException2) {
        if (f14866 != null) {
            try {
                f14866.invoke(iOException, new Object[]{iOException2});
            } catch (IllegalAccessException | InvocationTargetException e) {
            }
        }
    }

    public IOException a() {
        return this.b;
    }

    public void a(IOException iOException) {
        m18583(iOException, this.b);
        this.b = iOException;
    }
}
