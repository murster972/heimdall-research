package p006do.p007do.p021if;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import p006do.BrianBattler;
import p006do.Goenitz;
import p006do.JhunHoon;
import p006do.KDash;
import p006do.Orochi;
import p006do.Ramon;
import p006do.RugalBernstein;
import p006do.Whip;
import p006do.YashiroNanakase;
import p006do.p007do.p022int.KyoKusanagi;
import p006do.p007do.p023new.BenimaruNikaido;
import p006do.p007do.p023new.ChoiBounge;
import p006do.p007do.p023new.HeavyD;
import p009if.ChangKoehan;
import p009if.ChinGentsai;
import p009if.LeonaHeidern;
import p009if.Shermie;

/* renamed from: do.do.if.GoroDaimon  reason: invalid package */
public final class GoroDaimon extends HeavyD.BenimaruNikaido implements BrianBattler {

    /* renamed from: ʼ  reason: contains not printable characters */
    private final RugalBernstein f14877;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Ramon f14878;

    /* renamed from: ʾ  reason: contains not printable characters */
    private HeavyD f14879;

    /* renamed from: ʿ  reason: contains not printable characters */
    private ChangKoehan f14880;

    /* renamed from: ˈ  reason: contains not printable characters */
    private KDash f14881;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Socket f14882;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Socket f14883;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Goenitz f14884;

    /* renamed from: 连任  reason: contains not printable characters */
    public long f14885 = Long.MAX_VALUE;

    /* renamed from: 靐  reason: contains not printable characters */
    public int f14886;

    /* renamed from: 麤  reason: contains not printable characters */
    public final List<Reference<HeavyD>> f14887 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    public int f14888 = 1;

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean f14889;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private ChinGentsai f14890;

    public GoroDaimon(RugalBernstein rugalBernstein, Ramon ramon) {
        this.f14877 = rugalBernstein;
        this.f14878 = ramon;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private Whip m18599() {
        return new Whip.KyoKusanagi().m18397(this.f14878.m18379().m6512()).m18400("Host", p006do.p007do.GoroDaimon.m18414(this.f14878.m18379().m6512(), true)).m18400("Proxy-Connection", "Keep-Alive").m18400(AbstractSpiCall.HEADER_USER_AGENT, p006do.p007do.ChinGentsai.m18402()).m18393();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v10, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m18600(p006do.p007do.p021if.BenimaruNikaido r9) throws java.io.IOException {
        /*
            r8 = this;
            r2 = 0
            do.Ramon r0 = r8.f14878
            do.KyoKusanagi r3 = r0.m18379()
            javax.net.ssl.SSLSocketFactory r0 = r3.m6505()
            java.net.Socket r1 = r8.f14882     // Catch:{ AssertionError -> 0x0133 }
            do.YashiroNanakase r4 = r3.m6512()     // Catch:{ AssertionError -> 0x0133 }
            java.lang.String r4 = r4.m6608()     // Catch:{ AssertionError -> 0x0133 }
            do.YashiroNanakase r5 = r3.m6512()     // Catch:{ AssertionError -> 0x0133 }
            int r5 = r5.m6609()     // Catch:{ AssertionError -> 0x0133 }
            r6 = 1
            java.net.Socket r0 = r0.createSocket(r1, r4, r5, r6)     // Catch:{ AssertionError -> 0x0133 }
            javax.net.ssl.SSLSocket r0 = (javax.net.ssl.SSLSocket) r0     // Catch:{ AssertionError -> 0x0133 }
            do.IoriYagami r1 = r9.m18581((javax.net.ssl.SSLSocket) r0)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            boolean r4 = r1.m6452()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            if (r4 == 0) goto L_0x0041
            do.do.byte.ChangKoehan r4 = p006do.p007do.p017byte.ChangKoehan.m18443()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            do.YashiroNanakase r5 = r3.m6512()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r5 = r5.m6608()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.util.List r6 = r3.m6508()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            r4.m18454((javax.net.ssl.SSLSocket) r0, (java.lang.String) r5, (java.util.List<p006do.KDash>) r6)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
        L_0x0041:
            r0.startHandshake()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            javax.net.ssl.SSLSession r4 = r0.getSession()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            do.Goenitz r4 = p006do.Goenitz.m18344(r4)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            javax.net.ssl.HostnameVerifier r5 = r3.m6506()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            do.YashiroNanakase r6 = r3.m6512()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r6 = r6.m6608()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            javax.net.ssl.SSLSession r7 = r0.getSession()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            boolean r5 = r5.verify(r6, r7)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            if (r5 != 0) goto L_0x00dd
            java.util.List r1 = r4.m18345()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            r2 = 0
            java.lang.Object r1 = r1.get(r2)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.security.cert.X509Certificate r1 = (java.security.cert.X509Certificate) r1     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            javax.net.ssl.SSLPeerUnverifiedException r2 = new javax.net.ssl.SSLPeerUnverifiedException     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            r4.<init>()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r5 = "Hostname "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            do.YashiroNanakase r3 = r3.m6512()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r3 = r3.m6608()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r4 = " not verified:\n    certificate: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r4 = p006do.HeavyD.m6446((java.security.cert.Certificate) r1)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r4 = "\n    DN: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.security.Principal r4 = r1.getSubjectDN()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r4 = r4.getName()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r4 = "\n    subjectAltNames: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.util.List r1 = p006do.p007do.p019char.ChinGentsai.m18494(r1)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r1 = r1.toString()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            r2.<init>(r1)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            throw r2     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
        L_0x00c0:
            r1 = move-exception
            r2 = r0
        L_0x00c2:
            boolean r0 = p006do.p007do.GoroDaimon.m18425((java.lang.AssertionError) r1)     // Catch:{ all -> 0x00ce }
            if (r0 == 0) goto L_0x012f
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x00ce }
            r0.<init>(r1)     // Catch:{ all -> 0x00ce }
            throw r0     // Catch:{ all -> 0x00ce }
        L_0x00ce:
            r0 = move-exception
            r1 = r0
        L_0x00d0:
            if (r2 == 0) goto L_0x00d9
            do.do.byte.ChangKoehan r0 = p006do.p007do.p017byte.ChangKoehan.m18443()
            r0.m18448((javax.net.ssl.SSLSocket) r2)
        L_0x00d9:
            p006do.p007do.GoroDaimon.m18423((java.net.Socket) r2)
            throw r1
        L_0x00dd:
            do.HeavyD r5 = r3.m6507()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            do.YashiroNanakase r3 = r3.m6512()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r3 = r3.m6608()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.util.List r6 = r4.m18345()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            r5.m6449(r3, r6)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            boolean r1 = r1.m6452()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            if (r1 == 0) goto L_0x00fe
            do.do.byte.ChangKoehan r1 = p006do.p007do.p017byte.ChangKoehan.m18443()     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.lang.String r2 = r1.m18450((javax.net.ssl.SSLSocket) r0)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
        L_0x00fe:
            r8.f14883 = r0     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.net.Socket r1 = r8.f14883     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            if.Shermie r1 = p009if.LeonaHeidern.m18963((java.net.Socket) r1)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            if.ChangKoehan r1 = p009if.LeonaHeidern.m18966((p009if.Shermie) r1)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            r8.f14880 = r1     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            java.net.Socket r1 = r8.f14883     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            if.YashiroNanakase r1 = p009if.LeonaHeidern.m18973((java.net.Socket) r1)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            if.ChinGentsai r1 = p009if.LeonaHeidern.m18967((p009if.YashiroNanakase) r1)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            r8.f14890 = r1     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            r8.f14884 = r4     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            if (r2 == 0) goto L_0x012c
            do.KDash r1 = p006do.KDash.a(r2)     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
        L_0x0120:
            r8.f14881 = r1     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            if (r0 == 0) goto L_0x012b
            do.do.byte.ChangKoehan r1 = p006do.p007do.p017byte.ChangKoehan.m18443()
            r1.m18448((javax.net.ssl.SSLSocket) r0)
        L_0x012b:
            return
        L_0x012c:
            do.KDash r1 = p006do.KDash.HTTP_1_1     // Catch:{ AssertionError -> 0x00c0, all -> 0x0130 }
            goto L_0x0120
        L_0x012f:
            throw r1     // Catch:{ all -> 0x00ce }
        L_0x0130:
            r1 = move-exception
            r2 = r0
            goto L_0x00d0
        L_0x0133:
            r0 = move-exception
            r1 = r0
            goto L_0x00c2
        */
        throw new UnsupportedOperationException("Method not decompiled: p006do.p007do.p021if.GoroDaimon.m18600(do.do.if.BenimaruNikaido):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Whip m18601(int i, int i2, Whip whip, YashiroNanakase yashiroNanakase) throws IOException {
        JhunHoon r5;
        String str = "CONNECT " + p006do.p007do.GoroDaimon.m18414(yashiroNanakase, true) + " HTTP/1.1";
        do {
            KyoKusanagi kyoKusanagi = new KyoKusanagi((Orochi) null, (HeavyD) null, this.f14880, this.f14890);
            this.f14880.m19010().m18947((long) i, TimeUnit.MILLISECONDS);
            this.f14890.m19014().m18947((long) i2, TimeUnit.MILLISECONDS);
            kyoKusanagi.m18643(whip.m6591(), str);
            kyoKusanagi.m18634();
            r5 = kyoKusanagi.m18637(false).m6487(whip).m6490();
            long r0 = p006do.p007do.p020for.ChangKoehan.m18544(r5);
            if (r0 == -1) {
                r0 = 0;
            }
            Shermie r02 = kyoKusanagi.m18633(r0);
            p006do.p007do.GoroDaimon.m18405(r02, (int) MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, TimeUnit.MILLISECONDS);
            r02.close();
            switch (r5.m6469()) {
                case 200:
                    if (this.f14880.m6703().m6765() && this.f14890.m18930().m6765()) {
                        return null;
                    }
                    throw new IOException("TLS tunnel buffered too many bytes!");
                case 407:
                    whip = this.f14878.m18379().m6510().m6429(this.f14878, r5);
                    if (whip != null) {
                        break;
                    } else {
                        throw new IOException("Failed to authenticate with proxy");
                    }
                default:
                    throw new IOException("Unexpected response code for CONNECT: " + r5.m6469());
            }
        } while (!"close".equalsIgnoreCase(r5.m6473("Connection")));
        return whip;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18602(int i, int i2) throws IOException {
        Proxy r1 = this.f14878.m18376();
        this.f14882 = (r1.type() == Proxy.Type.DIRECT || r1.type() == Proxy.Type.HTTP) ? this.f14878.m18379().m6511().createSocket() : new Socket(r1);
        this.f14882.setSoTimeout(i2);
        try {
            p006do.p007do.p017byte.ChangKoehan.m18443().m18453(this.f14882, this.f14878.m18378(), i);
            try {
                this.f14880 = LeonaHeidern.m18966(LeonaHeidern.m18963(this.f14882));
                this.f14890 = LeonaHeidern.m18967(LeonaHeidern.m18973(this.f14882));
            } catch (NullPointerException e) {
                if ("throw with null exception".equals(e.getMessage())) {
                    throw new IOException(e);
                }
            }
        } catch (ConnectException e2) {
            ConnectException connectException = new ConnectException("Failed to connect to " + this.f14878.m18378());
            connectException.initCause(e2);
            throw connectException;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18603(int i, int i2, int i3) throws IOException {
        Whip r1 = m18599();
        YashiroNanakase r2 = r1.m6592();
        int i4 = 0;
        while (true) {
            i4++;
            if (i4 > 21) {
                throw new ProtocolException("Too many tunnel connections attempted: " + 21);
            }
            m18602(i, i2);
            r1 = m18601(i2, i3, r1, r2);
            if (r1 != null) {
                p006do.p007do.GoroDaimon.m18423(this.f14882);
                this.f14882 = null;
                this.f14890 = null;
                this.f14880 = null;
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18604(BenimaruNikaido benimaruNikaido) throws IOException {
        if (this.f14878.m18379().m6505() == null) {
            this.f14881 = KDash.HTTP_1_1;
            this.f14883 = this.f14882;
            return;
        }
        m18600(benimaruNikaido);
        if (this.f14881 == KDash.HTTP_2) {
            this.f14883.setSoTimeout(0);
            this.f14879 = new HeavyD.KyoKusanagi(true).m18772(this.f14883, this.f14878.m18379().m6512().m6608(), this.f14880, this.f14890).m18771(this).m18773();
            this.f14879.m18730();
        }
    }

    public String toString() {
        return "Connection{" + this.f14878.m18379().m6512().m6608() + ":" + this.f14878.m18379().m6512().m6609() + ", proxy=" + this.f14878.m18376() + " hostAddress=" + this.f14878.m18378() + " cipherSuite=" + (this.f14884 != null ? this.f14884.m18346() : "none") + " protocol=" + this.f14881 + '}';
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Socket m18605() {
        return this.f14883;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m18606() {
        return this.f14879 != null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Goenitz m18607() {
        return this.f14884;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Ramon m18608() {
        return this.f14878;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public p006do.p007do.p020for.GoroDaimon m18609(Orochi orochi, HeavyD heavyD) throws SocketException {
        if (this.f14879 != null) {
            return new ChoiBounge(orochi, heavyD, this.f14879);
        }
        this.f14883.setSoTimeout(orochi.m6543());
        this.f14880.m19010().m18947((long) orochi.m6543(), TimeUnit.MILLISECONDS);
        this.f14890.m19014().m18947((long) orochi.m6545(), TimeUnit.MILLISECONDS);
        return new KyoKusanagi(orochi, heavyD, this.f14880, this.f14890);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009d A[SYNTHETIC, Splitter:B:27:0x009d] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0086 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m18610(int r6, int r7, int r8, boolean r9) {
        /*
            r5 = this;
            r2 = 0
            do.KDash r0 = r5.f14881
            if (r0 == 0) goto L_0x000e
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "already connected"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            do.Ramon r0 = r5.f14878
            do.KyoKusanagi r0 = r0.m18379()
            java.util.List r0 = r0.m6502()
            do.do.if.BenimaruNikaido r3 = new do.do.if.BenimaruNikaido
            r3.<init>(r0)
            do.Ramon r1 = r5.f14878
            do.KyoKusanagi r1 = r1.m18379()
            javax.net.ssl.SSLSocketFactory r1 = r1.m6505()
            if (r1 != 0) goto L_0x007d
            do.IoriYagami r1 = p006do.IoriYagami.f5971
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x003f
            do.do.if.ChangKoehan r0 = new do.do.if.ChangKoehan
            java.net.UnknownServiceException r1 = new java.net.UnknownServiceException
            java.lang.String r2 = "CLEARTEXT communication not enabled for client"
            r1.<init>(r2)
            r0.<init>(r1)
            throw r0
        L_0x003f:
            do.Ramon r0 = r5.f14878
            do.KyoKusanagi r0 = r0.m18379()
            do.YashiroNanakase r0 = r0.m6512()
            java.lang.String r0 = r0.m6608()
            do.do.byte.ChangKoehan r1 = p006do.p007do.p017byte.ChangKoehan.m18443()
            boolean r1 = r1.m18455((java.lang.String) r0)
            if (r1 != 0) goto L_0x007d
            do.do.if.ChangKoehan r1 = new do.do.if.ChangKoehan
            java.net.UnknownServiceException r2 = new java.net.UnknownServiceException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "CLEARTEXT communication to "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = " not permitted by network security policy"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r1.<init>(r2)
            throw r1
        L_0x007d:
            r1 = r2
        L_0x007e:
            do.Ramon r0 = r5.f14878     // Catch:{ IOException -> 0x00a1 }
            boolean r0 = r0.m18377()     // Catch:{ IOException -> 0x00a1 }
            if (r0 == 0) goto L_0x009d
            r5.m18603(r6, r7, r8)     // Catch:{ IOException -> 0x00a1 }
        L_0x0089:
            r5.m18604((p006do.p007do.p021if.BenimaruNikaido) r3)     // Catch:{ IOException -> 0x00a1 }
            do.do.new.HeavyD r0 = r5.f14879
            if (r0 == 0) goto L_0x009c
            do.RugalBernstein r1 = r5.f14877
            monitor-enter(r1)
            do.do.new.HeavyD r0 = r5.f14879     // Catch:{ all -> 0x00ce }
            int r0 = r0.m18732()     // Catch:{ all -> 0x00ce }
            r5.f14888 = r0     // Catch:{ all -> 0x00ce }
            monitor-exit(r1)     // Catch:{ all -> 0x00ce }
        L_0x009c:
            return
        L_0x009d:
            r5.m18602((int) r6, (int) r7)     // Catch:{ IOException -> 0x00a1 }
            goto L_0x0089
        L_0x00a1:
            r0 = move-exception
            java.net.Socket r4 = r5.f14883
            p006do.p007do.GoroDaimon.m18423((java.net.Socket) r4)
            java.net.Socket r4 = r5.f14882
            p006do.p007do.GoroDaimon.m18423((java.net.Socket) r4)
            r5.f14883 = r2
            r5.f14882 = r2
            r5.f14880 = r2
            r5.f14890 = r2
            r5.f14884 = r2
            r5.f14881 = r2
            r5.f14879 = r2
            if (r1 != 0) goto L_0x00ca
            do.do.if.ChangKoehan r1 = new do.do.if.ChangKoehan
            r1.<init>(r0)
        L_0x00c1:
            if (r9 == 0) goto L_0x00c9
            boolean r0 = r3.m18582((java.io.IOException) r0)
            if (r0 != 0) goto L_0x007e
        L_0x00c9:
            throw r1
        L_0x00ca:
            r1.a(r0)
            goto L_0x00c1
        L_0x00ce:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00ce }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p006do.p007do.p021if.GoroDaimon.m18610(int, int, int, boolean):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18611(p006do.p007do.p023new.BrianBattler brianBattler) throws IOException {
        brianBattler.m18670(BenimaruNikaido.REFUSED_STREAM);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18612(HeavyD heavyD) {
        synchronized (this.f14877) {
            this.f14888 = heavyD.m18732();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18613(p006do.KyoKusanagi kyoKusanagi, @Nullable Ramon ramon) {
        if (this.f14887.size() >= this.f14888 || this.f14889 || !p006do.p007do.KyoKusanagi.f14748.m18438(this.f14878.m18379(), kyoKusanagi)) {
            return false;
        }
        if (kyoKusanagi.m6512().m6608().equals(m18608().m18379().m6512().m6608())) {
            return true;
        }
        if (this.f14879 == null || ramon == null || ramon.m18376().type() != Proxy.Type.DIRECT || this.f14878.m18376().type() != Proxy.Type.DIRECT || !this.f14878.m18378().equals(ramon.m18378()) || ramon.m18379().m6506() != p006do.p007do.p019char.ChinGentsai.f14786 || !m18614(kyoKusanagi.m6512())) {
            return false;
        }
        try {
            kyoKusanagi.m6507().m6449(kyoKusanagi.m6512().m6608(), m18607().m18345());
            return true;
        } catch (SSLPeerUnverifiedException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18614(YashiroNanakase yashiroNanakase) {
        if (yashiroNanakase.m6609() != this.f14878.m18379().m6512().m6609()) {
            return false;
        }
        if (yashiroNanakase.m6608().equals(this.f14878.m18379().m6512().m6608())) {
            return true;
        }
        return this.f14884 != null && p006do.p007do.p019char.ChinGentsai.f14786.m18497(yashiroNanakase.m6608(), (X509Certificate) this.f14884.m18345().get(0));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18615(boolean z) {
        int soTimeout;
        if (this.f14883.isClosed() || this.f14883.isInputShutdown() || this.f14883.isOutputShutdown()) {
            return false;
        }
        if (this.f14879 != null) {
            return !this.f14879.m18727();
        }
        if (!z) {
            return true;
        }
        try {
            soTimeout = this.f14883.getSoTimeout();
            this.f14883.setSoTimeout(1);
            if (this.f14880.m6708()) {
                this.f14883.setSoTimeout(soTimeout);
                return false;
            }
            this.f14883.setSoTimeout(soTimeout);
            return true;
        } catch (SocketTimeoutException e) {
            return true;
        } catch (IOException e2) {
            return false;
        } catch (Throwable th) {
            this.f14883.setSoTimeout(soTimeout);
            throw th;
        }
    }
}
