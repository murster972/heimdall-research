package p006do.p007do.p021if;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import p006do.Orochi;
import p006do.Ramon;
import p006do.RugalBernstein;
import p006do.p007do.p020for.GoroDaimon;
import p006do.p007do.p023new.BenimaruNikaido;
import p006do.p007do.p023new.Vice;

/* renamed from: do.do.if.HeavyD  reason: invalid package */
public final class HeavyD {

    /* renamed from: 靐  reason: contains not printable characters */
    static final /* synthetic */ boolean f14891 = (!HeavyD.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    private final ChoiBounge f14892;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f14893;

    /* renamed from: ʽ  reason: contains not printable characters */
    private GoroDaimon f14894;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f14895;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f14896;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private GoroDaimon f14897;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Object f14898;

    /* renamed from: 麤  reason: contains not printable characters */
    private final RugalBernstein f14899;

    /* renamed from: 齉  reason: contains not printable characters */
    private Ramon f14900;

    /* renamed from: 龘  reason: contains not printable characters */
    public final p006do.KyoKusanagi f14901;

    /* renamed from: do.do.if.HeavyD$KyoKusanagi */
    public static final class KyoKusanagi extends WeakReference<HeavyD> {

        /* renamed from: 龘  reason: contains not printable characters */
        public final Object f14902;

        KyoKusanagi(HeavyD heavyD, Object obj) {
            super(heavyD);
            this.f14902 = obj;
        }
    }

    public HeavyD(RugalBernstein rugalBernstein, p006do.KyoKusanagi kyoKusanagi, Object obj) {
        this.f14899 = rugalBernstein;
        this.f14901 = kyoKusanagi;
        this.f14892 = new ChoiBounge(kyoKusanagi, m18616());
        this.f14898 = obj;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private ChinGentsai m18616() {
        return p006do.p007do.KyoKusanagi.f14748.m18432(this.f14899);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m18617(GoroDaimon goroDaimon) {
        int size = goroDaimon.f14887.size();
        for (int i = 0; i < size; i++) {
            if (goroDaimon.f14887.get(i).get() == this) {
                goroDaimon.f14887.remove(i);
                return;
            }
        }
        throw new IllegalStateException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private GoroDaimon m18618(int i, int i2, int i3, boolean z) throws IOException {
        GoroDaimon goroDaimon;
        Socket socket = null;
        synchronized (this.f14899) {
            if (this.f14895) {
                throw new IllegalStateException("released");
            } else if (this.f14897 != null) {
                throw new IllegalStateException("codec != null");
            } else if (this.f14896) {
                throw new IOException("Canceled");
            } else {
                goroDaimon = this.f14894;
                if (goroDaimon == null || goroDaimon.f14889) {
                    p006do.p007do.KyoKusanagi.f14748.m18433(this.f14899, this.f14901, this, (Ramon) null);
                    if (this.f14894 != null) {
                        goroDaimon = this.f14894;
                    } else {
                        Ramon ramon = this.f14900;
                        if (ramon == null) {
                            ramon = this.f14892.m18596();
                        }
                        synchronized (this.f14899) {
                            if (this.f14896) {
                                throw new IOException("Canceled");
                            }
                            p006do.p007do.KyoKusanagi.f14748.m18433(this.f14899, this.f14901, this, ramon);
                            if (this.f14894 != null) {
                                goroDaimon = this.f14894;
                            } else {
                                this.f14900 = ramon;
                                this.f14893 = 0;
                                GoroDaimon goroDaimon2 = new GoroDaimon(this.f14899, ramon);
                                m18628(goroDaimon2);
                                goroDaimon2.m18610(i, i2, i3, z);
                                m18616().m18584(goroDaimon2.m18608());
                                synchronized (this.f14899) {
                                    p006do.p007do.KyoKusanagi.f14748.m18430(this.f14899, goroDaimon2);
                                    if (goroDaimon2.m18606()) {
                                        socket = p006do.p007do.KyoKusanagi.f14748.m18434(this.f14899, this.f14901, this);
                                        goroDaimon = this.f14894;
                                    } else {
                                        goroDaimon = goroDaimon2;
                                    }
                                }
                                p006do.p007do.GoroDaimon.m18423(socket);
                            }
                        }
                    }
                }
            }
        }
        return goroDaimon;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private GoroDaimon m18619(int i, int i2, int i3, boolean z, boolean z2) throws IOException {
        GoroDaimon r0;
        while (true) {
            r0 = m18618(i, i2, i3, z);
            synchronized (this.f14899) {
                if (r0.f14886 != 0) {
                    if (r0.m18615(z2)) {
                        break;
                    }
                    m18624();
                } else {
                    break;
                }
            }
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Socket m18620(boolean z, boolean z2, boolean z3) {
        Socket socket;
        if (f14891 || Thread.holdsLock(this.f14899)) {
            if (z3) {
                this.f14897 = null;
            }
            if (z2) {
                this.f14895 = true;
            }
            if (this.f14894 == null) {
                return null;
            }
            if (z) {
                this.f14894.f14889 = true;
            }
            if (this.f14897 != null) {
                return null;
            }
            if (!this.f14895 && !this.f14894.f14889) {
                return null;
            }
            m18617(this.f14894);
            if (this.f14894.f14887.isEmpty()) {
                this.f14894.f14885 = System.nanoTime();
                if (p006do.p007do.KyoKusanagi.f14748.m18439(this.f14899, this.f14894)) {
                    socket = this.f14894.m18605();
                    this.f14894 = null;
                    return socket;
                }
            }
            socket = null;
            this.f14894 = null;
            return socket;
        }
        throw new AssertionError();
    }

    public String toString() {
        GoroDaimon r0 = m18622();
        return r0 != null ? r0.toString() : this.f14901.toString();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m18621() {
        return this.f14900 != null || this.f14892.m18598();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized GoroDaimon m18622() {
        return this.f14894;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Socket m18623(GoroDaimon goroDaimon) {
        if (!f14891 && !Thread.holdsLock(this.f14899)) {
            throw new AssertionError();
        } else if (this.f14897 == null && this.f14894.f14887.size() == 1) {
            Socket r1 = m18620(true, false, false);
            this.f14894 = goroDaimon;
            goroDaimon.f14887.add(this.f14894.f14887.get(0));
            return r1;
        } else {
            throw new IllegalStateException();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m18624() {
        Socket r0;
        synchronized (this.f14899) {
            r0 = m18620(true, false, false);
        }
        p006do.p007do.GoroDaimon.m18423(r0);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m18625() {
        Socket r0;
        synchronized (this.f14899) {
            r0 = m18620(false, true, false);
        }
        p006do.p007do.GoroDaimon.m18423(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m18626() {
        GoroDaimon goroDaimon;
        synchronized (this.f14899) {
            goroDaimon = this.f14897;
        }
        return goroDaimon;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m18627(Orochi orochi, boolean z) {
        try {
            GoroDaimon r0 = m18619(orochi.m6546(), orochi.m6543(), orochi.m6545(), orochi.m6533(), z).m18609(orochi, this);
            synchronized (this.f14899) {
                this.f14897 = r0;
            }
            return r0;
        } catch (IOException e) {
            throw new ChangKoehan(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18628(GoroDaimon goroDaimon) {
        if (!f14891 && !Thread.holdsLock(this.f14899)) {
            throw new AssertionError();
        } else if (this.f14894 != null) {
            throw new IllegalStateException();
        } else {
            this.f14894 = goroDaimon;
            goroDaimon.f14887.add(new KyoKusanagi(this, this.f14898));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18629(IOException iOException) {
        Socket r0;
        boolean z = false;
        synchronized (this.f14899) {
            if (iOException instanceof Vice) {
                Vice vice = (Vice) iOException;
                if (vice.a == BenimaruNikaido.REFUSED_STREAM) {
                    this.f14893++;
                }
                if (vice.a != BenimaruNikaido.REFUSED_STREAM || this.f14893 > 1) {
                    this.f14900 = null;
                }
                r0 = m18620(z, false, true);
            } else {
                if (this.f14894 != null && (!this.f14894.m18606() || (iOException instanceof p006do.p007do.p023new.KyoKusanagi))) {
                    if (this.f14894.f14886 == 0) {
                        if (!(this.f14900 == null || iOException == null)) {
                            this.f14892.m18597(this.f14900, iOException);
                        }
                        this.f14900 = null;
                    }
                }
                r0 = m18620(z, false, true);
            }
            z = true;
            r0 = m18620(z, false, true);
        }
        p006do.p007do.GoroDaimon.m18423(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18630(boolean z, GoroDaimon goroDaimon) {
        Socket r0;
        synchronized (this.f14899) {
            if (goroDaimon != null) {
                if (goroDaimon == this.f14897) {
                    if (!z) {
                        this.f14894.f14886++;
                    }
                    r0 = m18620(z, false, true);
                }
            }
            throw new IllegalStateException("expected " + this.f14897 + " but was " + goroDaimon);
        }
        p006do.p007do.GoroDaimon.m18423(r0);
    }
}
