package p006do.p007do.p021if;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;
import p006do.IoriYagami;
import p006do.p007do.KyoKusanagi;

/* renamed from: do.do.if.BenimaruNikaido  reason: invalid package */
public final class BenimaruNikaido {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f14862 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f14863;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f14864;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<IoriYagami> f14865;

    public BenimaruNikaido(List<IoriYagami> list) {
        this.f14865 = list;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m18580(SSLSocket sSLSocket) {
        int i = this.f14862;
        while (true) {
            int i2 = i;
            if (i2 >= this.f14865.size()) {
                return false;
            }
            if (this.f14865.get(i2).m6456(sSLSocket)) {
                return true;
            }
            i = i2 + 1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public IoriYagami m18581(SSLSocket sSLSocket) throws IOException {
        IoriYagami ioriYagami;
        int i = this.f14862;
        int size = this.f14865.size();
        int i2 = i;
        while (true) {
            if (i2 >= size) {
                ioriYagami = null;
                break;
            }
            ioriYagami = this.f14865.get(i2);
            if (ioriYagami.m6456(sSLSocket)) {
                this.f14862 = i2 + 1;
                break;
            }
            i2++;
        }
        if (ioriYagami == null) {
            throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.f14863 + ", modes=" + this.f14865 + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
        }
        this.f14864 = m18580(sSLSocket);
        KyoKusanagi.f14748.m18435(ioriYagami, sSLSocket, this.f14863);
        return ioriYagami;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18582(IOException iOException) {
        this.f14863 = true;
        if (!this.f14864 || (iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return (iOException instanceof SSLHandshakeException) || (iOException instanceof SSLProtocolException);
        }
        return false;
    }
}
