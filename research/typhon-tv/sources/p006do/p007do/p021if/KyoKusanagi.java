package p006do.p007do.p021if;

import java.io.IOException;
import org.apache.oltu.oauth2.common.OAuth;
import p006do.JhunHoon;
import p006do.Orochi;
import p006do.Shermie;
import p006do.Whip;
import p006do.p007do.p020for.HeavyD;

/* renamed from: do.do.if.KyoKusanagi  reason: invalid package */
public final class KyoKusanagi implements Shermie {

    /* renamed from: 龘  reason: contains not printable characters */
    public final Orochi f14903;

    public KyoKusanagi(Orochi orochi) {
        this.f14903 = orochi;
    }

    public JhunHoon a(Shermie.KyoKusanagi kyoKusanagi) throws IOException {
        HeavyD heavyD = (HeavyD) kyoKusanagi;
        Whip r1 = heavyD.m18567();
        HeavyD r2 = heavyD.m18564();
        return heavyD.m18566(r1, r2, r2.m18627(this.f14903, !r1.m6589().equals(OAuth.HttpMethod.GET)), r2.m18622());
    }
}
