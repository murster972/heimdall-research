package p006do.p007do.p021if;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import p006do.KyoKusanagi;
import p006do.Ramon;
import p006do.YashiroNanakase;
import p006do.p007do.GoroDaimon;

/* renamed from: do.do.if.ChoiBounge  reason: invalid package */
public final class ChoiBounge {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f14868;

    /* renamed from: ʼ  reason: contains not printable characters */
    private List<InetSocketAddress> f14869 = Collections.emptyList();

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f14870;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final List<Ramon> f14871 = new ArrayList();

    /* renamed from: 连任  reason: contains not printable characters */
    private List<Proxy> f14872 = Collections.emptyList();

    /* renamed from: 靐  reason: contains not printable characters */
    private final ChinGentsai f14873;

    /* renamed from: 麤  reason: contains not printable characters */
    private InetSocketAddress f14874;

    /* renamed from: 齉  reason: contains not printable characters */
    private Proxy f14875;

    /* renamed from: 龘  reason: contains not printable characters */
    private final KyoKusanagi f14876;

    public ChoiBounge(KyoKusanagi kyoKusanagi, ChinGentsai chinGentsai) {
        this.f14876 = kyoKusanagi;
        this.f14873 = chinGentsai;
        m18594(kyoKusanagi.m6512(), kyoKusanagi.m6504());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private InetSocketAddress m18587() throws IOException {
        if (!m18590()) {
            throw new SocketException("No route to " + this.f14876.m6512().m6608() + "; exhausted inet socket addresses: " + this.f14869);
        }
        List<InetSocketAddress> list = this.f14869;
        int i = this.f14870;
        this.f14870 = i + 1;
        return list.get(i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m18588() {
        return !this.f14871.isEmpty();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private Ramon m18589() {
        return this.f14871.remove(0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean m18590() {
        return this.f14870 < this.f14869.size();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private Proxy m18591() throws IOException {
        if (!m18592()) {
            throw new SocketException("No route to " + this.f14876.m6512().m6608() + "; exhausted proxy configurations: " + this.f14872);
        }
        List<Proxy> list = this.f14872;
        int i = this.f14868;
        this.f14868 = i + 1;
        Proxy proxy = list.get(i);
        m18595(proxy);
        return proxy;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m18592() {
        return this.f14868 < this.f14872.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m18593(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        return address == null ? inetSocketAddress.getHostName() : address.getHostAddress();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18594(YashiroNanakase yashiroNanakase, Proxy proxy) {
        List<Proxy> r0;
        if (proxy != null) {
            this.f14872 = Collections.singletonList(proxy);
        } else {
            List<Proxy> select = this.f14876.m6503().select(yashiroNanakase.m6623());
            if (select == null || select.isEmpty()) {
                r0 = GoroDaimon.m18419((T[]) new Proxy[]{Proxy.NO_PROXY});
            } else {
                r0 = GoroDaimon.m18418(select);
            }
            this.f14872 = r0;
        }
        this.f14868 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18595(Proxy proxy) throws IOException {
        int i;
        String str;
        this.f14869 = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            str = this.f14876.m6512().m6608();
            i = this.f14876.m6512().m6609();
        } else {
            SocketAddress address = proxy.address();
            if (!(address instanceof InetSocketAddress)) {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
            InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
            str = m18593(inetSocketAddress);
            i = inetSocketAddress.getPort();
        }
        if (i < 1 || i > 65535) {
            throw new SocketException("No route to " + str + ":" + i + "; port is out of range");
        }
        if (proxy.type() == Proxy.Type.SOCKS) {
            this.f14869.add(InetSocketAddress.createUnresolved(str, i));
        } else {
            List<InetAddress> r4 = this.f14876.m6509().m18388(str);
            if (r4.isEmpty()) {
                throw new UnknownHostException(this.f14876.m6509() + " returned no addresses for " + str);
            }
            int size = r4.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f14869.add(new InetSocketAddress(r4.get(i2), i));
            }
        }
        this.f14870 = 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Ramon m18596() throws IOException {
        if (!m18590()) {
            if (m18592()) {
                this.f14875 = m18591();
            } else if (m18588()) {
                return m18589();
            } else {
                throw new NoSuchElementException();
            }
        }
        this.f14874 = m18587();
        Ramon ramon = new Ramon(this.f14876, this.f14875, this.f14874);
        if (!this.f14873.m18585(ramon)) {
            return ramon;
        }
        this.f14871.add(ramon);
        return m18596();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18597(Ramon ramon, IOException iOException) {
        if (!(ramon.m18376().type() == Proxy.Type.DIRECT || this.f14876.m6503() == null)) {
            this.f14876.m6503().connectFailed(this.f14876.m6512().m6623(), ramon.m18376().address(), iOException);
        }
        this.f14873.m18586(ramon);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18598() {
        return m18590() || m18592() || m18588();
    }
}
