package p006do;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import p006do.Maxima;
import p006do.p007do.GoroDaimon;

/* renamed from: do.Mature  reason: invalid package */
public final class Mature {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Deque<Maxima.KyoKusanagi> f6020 = new ArrayDeque();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Deque<Maxima> f6021 = new ArrayDeque();

    /* renamed from: 连任  reason: contains not printable characters */
    private final Deque<Maxima.KyoKusanagi> f6022 = new ArrayDeque();

    /* renamed from: 靐  reason: contains not printable characters */
    private int f6023 = 5;
    @Nullable

    /* renamed from: 麤  reason: contains not printable characters */
    private ExecutorService f6024;
    @Nullable

    /* renamed from: 齉  reason: contains not printable characters */
    private Runnable f6025;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f6026 = 64;

    /* renamed from: 靐  reason: contains not printable characters */
    private int m6514(Maxima.KyoKusanagi kyoKusanagi) {
        int i = 0;
        Iterator<Maxima.KyoKusanagi> it2 = this.f6020.iterator();
        while (true) {
            int i2 = i;
            if (!it2.hasNext()) {
                return i2;
            }
            i = it2.next().m18365().equals(kyoKusanagi.m18365()) ? i2 + 1 : i2;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m6515() {
        if (this.f6020.size() < this.f6026 && !this.f6022.isEmpty()) {
            Iterator<Maxima.KyoKusanagi> it2 = this.f6022.iterator();
            while (it2.hasNext()) {
                Maxima.KyoKusanagi next = it2.next();
                if (m6514(next) < this.f6023) {
                    it2.remove();
                    this.f6020.add(next);
                    m6519().execute(next);
                }
                if (this.f6020.size() >= this.f6026) {
                    return;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private <T> void m6516(Deque<T> deque, T t, boolean z) {
        int r0;
        Runnable runnable;
        synchronized (this) {
            if (!deque.remove(t)) {
                throw new AssertionError("Call wasn't in-flight!");
            }
            if (z) {
                m6515();
            }
            r0 = m6517();
            runnable = this.f6025;
        }
        if (r0 == 0 && runnable != null) {
            runnable.run();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized int m6517() {
        return this.f6020.size() + this.f6021.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6518(Maxima maxima) {
        m6516(this.f6021, maxima, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized ExecutorService m6519() {
        if (this.f6024 == null) {
            this.f6024 = new ThreadPoolExecutor(0, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, 60, TimeUnit.SECONDS, new SynchronousQueue(), GoroDaimon.m18420("OkHttp Dispatcher", false));
        }
        return this.f6024;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6520(Maxima.KyoKusanagi kyoKusanagi) {
        m6516(this.f6020, kyoKusanagi, true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m6521(Maxima maxima) {
        this.f6021.add(maxima);
    }
}
