package p006do;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

/* renamed from: do.Vice  reason: invalid package */
public interface Vice {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Vice f14716 = new Vice() {
        /* renamed from: 龘  reason: contains not printable characters */
        public List<InetAddress> m18389(String str) throws UnknownHostException {
            if (str != null) {
                return Arrays.asList(InetAddress.getAllByName(str));
            }
            throw new UnknownHostException("hostname == null");
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    List<InetAddress> m18388(String str) throws UnknownHostException;
}
