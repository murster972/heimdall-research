package p006do;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;
import p006do.p007do.GoroDaimon;

/* renamed from: do.ShingoYabuki  reason: invalid package */
public final class ShingoYabuki {

    /* renamed from: 龘  reason: contains not printable characters */
    private final String[] f6104;

    /* renamed from: do.ShingoYabuki$KyoKusanagi */
    public static final class KyoKusanagi {

        /* renamed from: 龘  reason: contains not printable characters */
        final List<String> f14714 = new ArrayList(20);

        /* renamed from: 麤  reason: contains not printable characters */
        private void m18380(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str.isEmpty()) {
                throw new IllegalArgumentException("name is empty");
            } else {
                int length = str.length();
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if (charAt <= ' ' || charAt >= 127) {
                        throw new IllegalArgumentException(GoroDaimon.m18416("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), str));
                    }
                }
                if (str2 == null) {
                    throw new NullPointerException("value for name " + str + " == null");
                }
                int length2 = str2.length();
                int i2 = 0;
                while (i2 < length2) {
                    char charAt2 = str2.charAt(i2);
                    if ((charAt2 > 31 || charAt2 == 9) && charAt2 < 127) {
                        i2++;
                    } else {
                        throw new IllegalArgumentException(GoroDaimon.m18416("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt2), Integer.valueOf(i2), str, str2));
                    }
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m18381(String str) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f14714.size()) {
                    return this;
                }
                if (str.equalsIgnoreCase(this.f14714.get(i2))) {
                    this.f14714.remove(i2);
                    this.f14714.remove(i2);
                    i2 -= 2;
                }
                i = i2 + 2;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m18382(String str, String str2) {
            this.f14714.add(str);
            this.f14714.add(str2.trim());
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public KyoKusanagi m18383(String str, String str2) {
            m18380(str, str2);
            m18381(str);
            m18382(str, str2);
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18384(String str) {
            int indexOf = str.indexOf(":", 1);
            return indexOf != -1 ? m18382(str.substring(0, indexOf), str.substring(indexOf + 1)) : str.startsWith(":") ? m18382("", str.substring(1)) : m18382("", str);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18385(String str, String str2) {
            m18380(str, str2);
            return m18382(str, str2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ShingoYabuki m18386() {
            return new ShingoYabuki(this);
        }
    }

    ShingoYabuki(KyoKusanagi kyoKusanagi) {
        this.f6104 = (String[]) kyoKusanagi.f14714.toArray(new String[kyoKusanagi.f14714.size()]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m6578(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof ShingoYabuki) && Arrays.equals(((ShingoYabuki) obj).f6104, this.f6104);
    }

    public int hashCode() {
        return Arrays.hashCode(this.f6104);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int r2 = m6583();
        for (int i = 0; i < r2; i++) {
            sb.append(m6584(i)).append(": ").append(m6579(i)).append(StringUtils.LF);
        }
        return sb.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m6579(int i) {
        return this.f6104[(i * 2) + 1];
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public List<String> m6580(String str) {
        ArrayList arrayList = null;
        int r2 = m6583();
        for (int i = 0; i < r2; i++) {
            if (str.equalsIgnoreCase(m6584(i))) {
                if (arrayList == null) {
                    arrayList = new ArrayList(2);
                }
                arrayList.add(m6579(i));
            }
        }
        return arrayList != null ? Collections.unmodifiableList(arrayList) : Collections.emptyList();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Set<String> m6581() {
        TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
        int r2 = m6583();
        for (int i = 0; i < r2; i++) {
            treeSet.add(m6584(i));
        }
        return Collections.unmodifiableSet(treeSet);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public KyoKusanagi m6582() {
        KyoKusanagi kyoKusanagi = new KyoKusanagi();
        Collections.addAll(kyoKusanagi.f14714, this.f6104);
        return kyoKusanagi;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m6583() {
        return this.f6104.length / 2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6584(int i) {
        return this.f6104[i * 2];
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public String m6585(String str) {
        return m6578(this.f6104, str);
    }
}
