package p006do;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import p006do.p007do.GoroDaimon;
import p009if.ChoiBounge;

/* renamed from: do.HeavyD  reason: invalid package */
public final class HeavyD {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final HeavyD f5966 = new KyoKusanagi().m18348();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Set<BenimaruNikaido> f5967;
    @Nullable

    /* renamed from: 齉  reason: contains not printable characters */
    private final p006do.p007do.p019char.BenimaruNikaido f5968;

    /* renamed from: do.HeavyD$BenimaruNikaido */
    static final class BenimaruNikaido {

        /* renamed from: 靐  reason: contains not printable characters */
        final String f14580;

        /* renamed from: 麤  reason: contains not printable characters */
        final ChoiBounge f14581;

        /* renamed from: 齉  reason: contains not printable characters */
        final String f14582;

        /* renamed from: 龘  reason: contains not printable characters */
        final String f14583;

        public boolean equals(Object obj) {
            return (obj instanceof BenimaruNikaido) && this.f14583.equals(((BenimaruNikaido) obj).f14583) && this.f14582.equals(((BenimaruNikaido) obj).f14582) && this.f14581.equals(((BenimaruNikaido) obj).f14581);
        }

        public int hashCode() {
            return ((((this.f14583.hashCode() + 527) * 31) + this.f14582.hashCode()) * 31) + this.f14581.hashCode();
        }

        public String toString() {
            return this.f14582 + this.f14581.b();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m18347(String str) {
            if (!this.f14583.startsWith("*.")) {
                return str.equals(this.f14580);
            }
            return str.regionMatches(false, str.indexOf(46) + 1, this.f14580, 0, this.f14580.length());
        }
    }

    /* renamed from: do.HeavyD$KyoKusanagi */
    public static final class KyoKusanagi {

        /* renamed from: 龘  reason: contains not printable characters */
        private final List<BenimaruNikaido> f14584 = new ArrayList();

        /* renamed from: 龘  reason: contains not printable characters */
        public HeavyD m18348() {
            return new HeavyD(new LinkedHashSet(this.f14584), (p006do.p007do.p019char.BenimaruNikaido) null);
        }
    }

    HeavyD(Set<BenimaruNikaido> set, @Nullable p006do.p007do.p019char.BenimaruNikaido benimaruNikaido) {
        this.f5967 = set;
        this.f5968 = benimaruNikaido;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static ChoiBounge m6444(X509Certificate x509Certificate) {
        return ChoiBounge.a(x509Certificate.getPublicKey().getEncoded()).d();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ChoiBounge m6445(X509Certificate x509Certificate) {
        return ChoiBounge.a(x509Certificate.getPublicKey().getEncoded()).c();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m6446(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha256/" + m6444((X509Certificate) certificate).b();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    public boolean equals(@Nullable Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof HeavyD) && GoroDaimon.m18426((Object) this.f5968, (Object) ((HeavyD) obj).f5968) && this.f5967.equals(((HeavyD) obj).f5967);
    }

    public int hashCode() {
        return ((this.f5968 != null ? this.f5968.hashCode() : 0) * 31) + this.f5967.hashCode();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public HeavyD m6447(p006do.p007do.p019char.BenimaruNikaido benimaruNikaido) {
        return GoroDaimon.m18426((Object) this.f5968, (Object) benimaruNikaido) ? this : new HeavyD(this.f5967, benimaruNikaido);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<BenimaruNikaido> m6448(String str) {
        List<BenimaruNikaido> emptyList = Collections.emptyList();
        for (BenimaruNikaido next : this.f5967) {
            if (next.m18347(str)) {
                if (emptyList.isEmpty()) {
                    emptyList = new ArrayList<>();
                }
                emptyList.add(next);
            }
        }
        return emptyList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6449(String str, List<Certificate> list) throws SSLPeerUnverifiedException {
        List<BenimaruNikaido> r8 = m6448(str);
        if (!r8.isEmpty()) {
            if (this.f5968 != null) {
                list = this.f5968.m18486(list, str);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                int size2 = r8.size();
                int i2 = 0;
                ChoiBounge choiBounge = null;
                ChoiBounge choiBounge2 = null;
                while (i2 < size2) {
                    BenimaruNikaido benimaruNikaido = r8.get(i2);
                    if (benimaruNikaido.f14582.equals("sha256/")) {
                        if (choiBounge == null) {
                            choiBounge = m6444(x509Certificate);
                        }
                        if (benimaruNikaido.f14581.equals(choiBounge)) {
                            return;
                        }
                    } else if (benimaruNikaido.f14582.equals("sha1/")) {
                        if (choiBounge2 == null) {
                            choiBounge2 = m6445(x509Certificate);
                        }
                        if (benimaruNikaido.f14581.equals(choiBounge2)) {
                            return;
                        }
                    } else {
                        throw new AssertionError();
                    }
                    i2++;
                    choiBounge = choiBounge;
                }
            }
            StringBuilder append = new StringBuilder().append("Certificate pinning failure!").append("\n  Peer certificate chain:");
            int size3 = list.size();
            for (int i3 = 0; i3 < size3; i3++) {
                X509Certificate x509Certificate2 = (X509Certificate) list.get(i3);
                append.append("\n    ").append(m6446((Certificate) x509Certificate2)).append(": ").append(x509Certificate2.getSubjectDN().getName());
            }
            append.append("\n  Pinned certificates for ").append(str).append(":");
            int size4 = r8.size();
            for (int i4 = 0; i4 < size4; i4++) {
                append.append("\n    ").append(r8.get(i4));
            }
            throw new SSLPeerUnverifiedException(append.toString());
        }
    }
}
