package p006do;

import java.net.Proxy;
import java.net.ProxySelector;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import p006do.ChizuruKagura;
import p006do.JhunHoon;
import p006do.ShingoYabuki;
import p006do.p007do.GoroDaimon;
import p006do.p007do.p008do.ChangKoehan;
import p006do.p007do.p019char.BenimaruNikaido;
import p006do.p007do.p019char.ChinGentsai;
import p006do.p007do.p021if.HeavyD;

/* renamed from: do.Orochi  reason: invalid package */
public class Orochi implements Cloneable {

    /* renamed from: 靐  reason: contains not printable characters */
    static final List<IoriYagami> f6027 = GoroDaimon.m18419((T[]) new IoriYagami[]{IoriYagami.f5972, IoriYagami.f5971});

    /* renamed from: 龘  reason: contains not printable characters */
    static final List<KDash> f6028 = GoroDaimon.m18419((T[]) new KDash[]{KDash.HTTP_2, KDash.HTTP_1_1});

    /* renamed from: ʻ  reason: contains not printable characters */
    final List<IoriYagami> f6029;

    /* renamed from: ʼ  reason: contains not printable characters */
    final List<Shermie> f6030;

    /* renamed from: ʽ  reason: contains not printable characters */
    final List<Shermie> f6031;
    @Nullable

    /* renamed from: ʾ  reason: contains not printable characters */
    final ChangKoehan f6032;

    /* renamed from: ʿ  reason: contains not printable characters */
    final SocketFactory f6033;

    /* renamed from: ˆ  reason: contains not printable characters */
    final BenimaruNikaido f6034;
    @Nullable

    /* renamed from: ˈ  reason: contains not printable characters */
    final GoroDaimon f6035;

    /* renamed from: ˉ  reason: contains not printable characters */
    final RugalBernstein f6036;

    /* renamed from: ˊ  reason: contains not printable characters */
    final HostnameVerifier f6037;

    /* renamed from: ˋ  reason: contains not printable characters */
    final HeavyD f6038;

    /* renamed from: ˎ  reason: contains not printable characters */
    final BenimaruNikaido f6039;

    /* renamed from: ˏ  reason: contains not printable characters */
    final Vice f6040;

    /* renamed from: ˑ  reason: contains not printable characters */
    final ChizuruKagura.KyoKusanagi f6041;

    /* renamed from: י  reason: contains not printable characters */
    final boolean f6042;

    /* renamed from: ـ  reason: contains not printable characters */
    final boolean f6043;

    /* renamed from: ٴ  reason: contains not printable characters */
    final ProxySelector f6044;

    /* renamed from: ᐧ  reason: contains not printable characters */
    final LeonaHeidern f6045;

    /* renamed from: ᴵ  reason: contains not printable characters */
    final boolean f6046;

    /* renamed from: ᵎ  reason: contains not printable characters */
    final int f6047;

    /* renamed from: ᵔ  reason: contains not printable characters */
    final int f6048;

    /* renamed from: ᵢ  reason: contains not printable characters */
    final int f6049;

    /* renamed from: ⁱ  reason: contains not printable characters */
    final int f6050;

    /* renamed from: 连任  reason: contains not printable characters */
    final List<KDash> f6051;
    @Nullable

    /* renamed from: 麤  reason: contains not printable characters */
    final Proxy f6052;

    /* renamed from: 齉  reason: contains not printable characters */
    final Mature f6053;
    @Nullable

    /* renamed from: ﹶ  reason: contains not printable characters */
    final SSLSocketFactory f6054;
    @Nullable

    /* renamed from: ﾞ  reason: contains not printable characters */
    final BenimaruNikaido f6055;

    /* renamed from: do.Orochi$KyoKusanagi */
    public static final class KyoKusanagi {

        /* renamed from: ʻ  reason: contains not printable characters */
        final List<Shermie> f6056;

        /* renamed from: ʼ  reason: contains not printable characters */
        ChizuruKagura.KyoKusanagi f6057;

        /* renamed from: ʽ  reason: contains not printable characters */
        ProxySelector f6058;
        @Nullable

        /* renamed from: ʾ  reason: contains not printable characters */
        SSLSocketFactory f6059;
        @Nullable

        /* renamed from: ʿ  reason: contains not printable characters */
        BenimaruNikaido f6060;

        /* renamed from: ˆ  reason: contains not printable characters */
        Vice f6061;

        /* renamed from: ˈ  reason: contains not printable characters */
        SocketFactory f6062;

        /* renamed from: ˉ  reason: contains not printable characters */
        boolean f6063;

        /* renamed from: ˊ  reason: contains not printable characters */
        BenimaruNikaido f6064;

        /* renamed from: ˋ  reason: contains not printable characters */
        BenimaruNikaido f6065;

        /* renamed from: ˎ  reason: contains not printable characters */
        RugalBernstein f6066;

        /* renamed from: ˏ  reason: contains not printable characters */
        boolean f6067;

        /* renamed from: ˑ  reason: contains not printable characters */
        LeonaHeidern f6068;

        /* renamed from: י  reason: contains not printable characters */
        boolean f6069;

        /* renamed from: ـ  reason: contains not printable characters */
        int f6070;
        @Nullable

        /* renamed from: ٴ  reason: contains not printable characters */
        GoroDaimon f6071;
        @Nullable

        /* renamed from: ᐧ  reason: contains not printable characters */
        ChangKoehan f6072;

        /* renamed from: ᴵ  reason: contains not printable characters */
        int f6073;

        /* renamed from: ᵎ  reason: contains not printable characters */
        int f6074;

        /* renamed from: ᵔ  reason: contains not printable characters */
        int f6075;

        /* renamed from: 连任  reason: contains not printable characters */
        final List<Shermie> f6076;
        @Nullable

        /* renamed from: 靐  reason: contains not printable characters */
        Proxy f6077;

        /* renamed from: 麤  reason: contains not printable characters */
        List<IoriYagami> f6078;

        /* renamed from: 齉  reason: contains not printable characters */
        List<KDash> f6079;

        /* renamed from: 龘  reason: contains not printable characters */
        Mature f6080;

        /* renamed from: ﹶ  reason: contains not printable characters */
        HostnameVerifier f6081;

        /* renamed from: ﾞ  reason: contains not printable characters */
        HeavyD f6082;

        public KyoKusanagi() {
            this.f6076 = new ArrayList();
            this.f6056 = new ArrayList();
            this.f6080 = new Mature();
            this.f6079 = Orochi.f6028;
            this.f6078 = Orochi.f6027;
            this.f6057 = ChizuruKagura.m18339(ChizuruKagura.f14572);
            this.f6058 = ProxySelector.getDefault();
            this.f6068 = LeonaHeidern.f14586;
            this.f6062 = SocketFactory.getDefault();
            this.f6081 = ChinGentsai.f14786;
            this.f6082 = HeavyD.f5966;
            this.f6064 = BenimaruNikaido.f5944;
            this.f6065 = BenimaruNikaido.f5944;
            this.f6066 = new RugalBernstein();
            this.f6061 = Vice.f14716;
            this.f6063 = true;
            this.f6067 = true;
            this.f6069 = true;
            this.f6070 = 10000;
            this.f6073 = 10000;
            this.f6074 = 10000;
            this.f6075 = 0;
        }

        KyoKusanagi(Orochi orochi) {
            this.f6076 = new ArrayList();
            this.f6056 = new ArrayList();
            this.f6080 = orochi.f6053;
            this.f6077 = orochi.f6052;
            this.f6079 = orochi.f6051;
            this.f6078 = orochi.f6029;
            this.f6076.addAll(orochi.f6030);
            this.f6056.addAll(orochi.f6031);
            this.f6057 = orochi.f6041;
            this.f6058 = orochi.f6044;
            this.f6068 = orochi.f6045;
            this.f6072 = orochi.f6032;
            this.f6071 = orochi.f6035;
            this.f6062 = orochi.f6033;
            this.f6059 = orochi.f6054;
            this.f6060 = orochi.f6055;
            this.f6081 = orochi.f6037;
            this.f6082 = orochi.f6038;
            this.f6064 = orochi.f6039;
            this.f6065 = orochi.f6034;
            this.f6066 = orochi.f6036;
            this.f6061 = orochi.f6040;
            this.f6063 = orochi.f6042;
            this.f6067 = orochi.f6043;
            this.f6069 = orochi.f6046;
            this.f6070 = orochi.f6047;
            this.f6073 = orochi.f6048;
            this.f6074 = orochi.f6049;
            this.f6075 = orochi.f6050;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static int m6550(String str, long j, TimeUnit timeUnit) {
            if (j < 0) {
                throw new IllegalArgumentException(str + " < 0");
            } else if (timeUnit == null) {
                throw new NullPointerException("unit == null");
            } else {
                long millis = timeUnit.toMillis(j);
                if (millis > 2147483647L) {
                    throw new IllegalArgumentException(str + " too large.");
                } else if (millis != 0 || j <= 0) {
                    return (int) millis;
                } else {
                    throw new IllegalArgumentException(str + " too small.");
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m6551(Shermie shermie) {
            this.f6056.add(shermie);
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m6552(boolean z) {
            this.f6067 = z;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Orochi m6553() {
            return new Orochi(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6554(long j, TimeUnit timeUnit) {
            this.f6073 = m6550("timeout", j, timeUnit);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6555(Shermie shermie) {
            this.f6076.add(shermie);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6556(boolean z) {
            this.f6063 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public List<Shermie> m6557() {
            return this.f6056;
        }
    }

    static {
        p006do.p007do.KyoKusanagi.f14748 = new p006do.p007do.KyoKusanagi() {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m18366(RugalBernstein rugalBernstein, p006do.p007do.p021if.GoroDaimon goroDaimon) {
                rugalBernstein.m6563(goroDaimon);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public int m18367(JhunHoon.KyoKusanagi kyoKusanagi) {
                return kyoKusanagi.f6004;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public p006do.p007do.p021if.ChinGentsai m18368(RugalBernstein rugalBernstein) {
                return rugalBernstein.f6090;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public p006do.p007do.p021if.GoroDaimon m18369(RugalBernstein rugalBernstein, KyoKusanagi kyoKusanagi, HeavyD heavyD, Ramon ramon) {
                return rugalBernstein.m6561(kyoKusanagi, heavyD, ramon);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Socket m18370(RugalBernstein rugalBernstein, KyoKusanagi kyoKusanagi, HeavyD heavyD) {
                return rugalBernstein.m6562(kyoKusanagi, heavyD);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m18371(IoriYagami ioriYagami, SSLSocket sSLSocket, boolean z) {
                ioriYagami.m6454(sSLSocket, z);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m18372(ShingoYabuki.KyoKusanagi kyoKusanagi, String str) {
                kyoKusanagi.m18384(str);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m18373(ShingoYabuki.KyoKusanagi kyoKusanagi, String str, String str2) {
                kyoKusanagi.m18382(str, str2);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m18374(KyoKusanagi kyoKusanagi, KyoKusanagi kyoKusanagi2) {
                return kyoKusanagi.m6513(kyoKusanagi2);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m18375(RugalBernstein rugalBernstein, p006do.p007do.p021if.GoroDaimon goroDaimon) {
                return rugalBernstein.m6559(goroDaimon);
            }
        };
    }

    public Orochi() {
        this(new KyoKusanagi());
    }

    Orochi(KyoKusanagi kyoKusanagi) {
        this.f6053 = kyoKusanagi.f6080;
        this.f6052 = kyoKusanagi.f6077;
        this.f6051 = kyoKusanagi.f6079;
        this.f6029 = kyoKusanagi.f6078;
        this.f6030 = GoroDaimon.m18418(kyoKusanagi.f6076);
        this.f6031 = GoroDaimon.m18418(kyoKusanagi.f6056);
        this.f6041 = kyoKusanagi.f6057;
        this.f6044 = kyoKusanagi.f6058;
        this.f6045 = kyoKusanagi.f6068;
        this.f6035 = kyoKusanagi.f6071;
        this.f6032 = kyoKusanagi.f6072;
        this.f6033 = kyoKusanagi.f6062;
        boolean z = false;
        for (IoriYagami r0 : this.f6029) {
            z = z || r0.m6455();
        }
        if (kyoKusanagi.f6059 != null || !z) {
            this.f6054 = kyoKusanagi.f6059;
            this.f6055 = kyoKusanagi.f6060;
        } else {
            X509TrustManager r02 = m6522();
            this.f6054 = m6523(r02);
            this.f6055 = BenimaruNikaido.m18485(r02);
        }
        this.f6037 = kyoKusanagi.f6081;
        this.f6038 = kyoKusanagi.f6082.m6447(this.f6055);
        this.f6039 = kyoKusanagi.f6064;
        this.f6034 = kyoKusanagi.f6065;
        this.f6036 = kyoKusanagi.f6066;
        this.f6040 = kyoKusanagi.f6061;
        this.f6042 = kyoKusanagi.f6063;
        this.f6043 = kyoKusanagi.f6067;
        this.f6046 = kyoKusanagi.f6069;
        this.f6047 = kyoKusanagi.f6070;
        this.f6048 = kyoKusanagi.f6073;
        this.f6049 = kyoKusanagi.f6074;
        this.f6050 = kyoKusanagi.f6075;
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private X509TrustManager m6522() {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance.init((KeyStore) null);
            TrustManager[] trustManagers = instance.getTrustManagers();
            if (trustManagers.length == 1 && (trustManagers[0] instanceof X509TrustManager)) {
                return (X509TrustManager) trustManagers[0];
            }
            throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
        } catch (GeneralSecurityException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private SSLSocketFactory m6523(X509TrustManager x509TrustManager) {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init((KeyManager[]) null, new TrustManager[]{x509TrustManager}, (SecureRandom) null);
            return instance.getSocketFactory();
        } catch (GeneralSecurityException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public LeonaHeidern m6524() {
        return this.f6045;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public ChangKoehan m6525() {
        return this.f6035 != null ? this.f6035.f14579 : this.f6032;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Vice m6526() {
        return this.f6040;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public BenimaruNikaido m6527() {
        return this.f6034;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public BenimaruNikaido m6528() {
        return this.f6039;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public List<KDash> m6529() {
        return this.f6051;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public HeavyD m6530() {
        return this.f6038;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public List<IoriYagami> m6531() {
        return this.f6029;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m6532() {
        return this.f6043;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m6533() {
        return this.f6046;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public Mature m6534() {
        return this.f6053;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public List<Shermie> m6535() {
        return this.f6030;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public SocketFactory m6536() {
        return this.f6033;
    }

    /* renamed from: י  reason: contains not printable characters */
    public List<Shermie> m6537() {
        return this.f6031;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ـ  reason: contains not printable characters */
    public ChizuruKagura.KyoKusanagi m6538() {
        return this.f6041;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public SSLSocketFactory m6539() {
        return this.f6054;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public HostnameVerifier m6540() {
        return this.f6037;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public KyoKusanagi m6541() {
        return new KyoKusanagi(this);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public ProxySelector m6542() {
        return this.f6044;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m6543() {
        return this.f6048;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Proxy m6544() {
        return this.f6052;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m6545() {
        return this.f6049;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m6546() {
        return this.f6047;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ChangKoehan m6547(Whip whip) {
        return new Maxima(this, whip, false);
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public RugalBernstein m6548() {
        return this.f6036;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m6549() {
        return this.f6042;
    }
}
