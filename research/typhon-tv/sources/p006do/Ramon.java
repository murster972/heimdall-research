package p006do;

import java.net.InetSocketAddress;
import java.net.Proxy;
import javax.annotation.Nullable;

/* renamed from: do.Ramon  reason: invalid package */
public final class Ramon {

    /* renamed from: 靐  reason: contains not printable characters */
    final Proxy f14710;

    /* renamed from: 齉  reason: contains not printable characters */
    final InetSocketAddress f14711;

    /* renamed from: 龘  reason: contains not printable characters */
    final KyoKusanagi f14712;

    public Ramon(KyoKusanagi kyoKusanagi, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (kyoKusanagi == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress == null) {
            throw new NullPointerException("inetSocketAddress == null");
        } else {
            this.f14712 = kyoKusanagi;
            this.f14710 = proxy;
            this.f14711 = inetSocketAddress;
        }
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof Ramon) && ((Ramon) obj).f14712.equals(this.f14712) && ((Ramon) obj).f14710.equals(this.f14710) && ((Ramon) obj).f14711.equals(this.f14711);
    }

    public int hashCode() {
        return ((((this.f14712.hashCode() + 527) * 31) + this.f14710.hashCode()) * 31) + this.f14711.hashCode();
    }

    public String toString() {
        return "Route{" + this.f14711 + "}";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Proxy m18376() {
        return this.f14710;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m18377() {
        return this.f14712.f6012 != null && this.f14710.type() == Proxy.Type.HTTP;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public InetSocketAddress m18378() {
        return this.f14711;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public KyoKusanagi m18379() {
        return this.f14712;
    }
}
