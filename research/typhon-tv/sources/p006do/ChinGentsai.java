package p006do;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import p006do.p007do.p020for.ChangKoehan;

/* renamed from: do.ChinGentsai  reason: invalid package */
public final class ChinGentsai {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final ChinGentsai f5945 = new KyoKusanagi().m18335().m18338(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, TimeUnit.SECONDS).m18336();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ChinGentsai f5946 = new KyoKusanagi().m18337().m18336();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f5947;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f5948;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final boolean f5949;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final boolean f5950;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final boolean f5951;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f5952;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final boolean f5953;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean f5954;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f5955;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f5956;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f5957;
    @Nullable

    /* renamed from: 齉  reason: contains not printable characters */
    String f5958;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final boolean f5959;

    /* renamed from: do.ChinGentsai$KyoKusanagi */
    public static final class KyoKusanagi {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f14564;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f14565;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f14566;

        /* renamed from: 连任  reason: contains not printable characters */
        int f14567 = -1;

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f14568;

        /* renamed from: 麤  reason: contains not printable characters */
        int f14569 = -1;

        /* renamed from: 齉  reason: contains not printable characters */
        int f14570 = -1;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f14571;

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m18335() {
            this.f14564 = true;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public ChinGentsai m18336() {
            return new ChinGentsai(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18337() {
            this.f14571 = true;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18338(int i, TimeUnit timeUnit) {
            if (i < 0) {
                throw new IllegalArgumentException("maxStale < 0: " + i);
            }
            long seconds = timeUnit.toSeconds((long) i);
            this.f14569 = seconds > 2147483647L ? MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT : (int) seconds;
            return this;
        }
    }

    ChinGentsai(KyoKusanagi kyoKusanagi) {
        this.f5957 = kyoKusanagi.f14571;
        this.f5956 = kyoKusanagi.f14568;
        this.f5947 = kyoKusanagi.f14570;
        this.f5948 = -1;
        this.f5949 = false;
        this.f5953 = false;
        this.f5954 = false;
        this.f5955 = kyoKusanagi.f14569;
        this.f5952 = kyoKusanagi.f14567;
        this.f5950 = kyoKusanagi.f14564;
        this.f5951 = kyoKusanagi.f14565;
        this.f5959 = kyoKusanagi.f14566;
    }

    private ChinGentsai(boolean z, boolean z2, int i, int i2, boolean z3, boolean z4, boolean z5, int i3, int i4, boolean z6, boolean z7, boolean z8, @Nullable String str) {
        this.f5957 = z;
        this.f5956 = z2;
        this.f5947 = i;
        this.f5948 = i2;
        this.f5949 = z3;
        this.f5953 = z4;
        this.f5954 = z5;
        this.f5955 = i3;
        this.f5952 = i4;
        this.f5950 = z6;
        this.f5951 = z7;
        this.f5959 = z8;
        this.f5958 = str;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private String m6430() {
        StringBuilder sb = new StringBuilder();
        if (this.f5957) {
            sb.append("no-cache, ");
        }
        if (this.f5956) {
            sb.append("no-store, ");
        }
        if (this.f5947 != -1) {
            sb.append("max-age=").append(this.f5947).append(", ");
        }
        if (this.f5948 != -1) {
            sb.append("s-maxage=").append(this.f5948).append(", ");
        }
        if (this.f5949) {
            sb.append("private, ");
        }
        if (this.f5953) {
            sb.append("public, ");
        }
        if (this.f5954) {
            sb.append("must-revalidate, ");
        }
        if (this.f5955 != -1) {
            sb.append("max-stale=").append(this.f5955).append(", ");
        }
        if (this.f5952 != -1) {
            sb.append("min-fresh=").append(this.f5952).append(", ");
        }
        if (this.f5950) {
            sb.append("only-if-cached, ");
        }
        if (this.f5951) {
            sb.append("no-transform, ");
        }
        if (this.f5959) {
            sb.append("immutable, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ChinGentsai m6431(ShingoYabuki shingoYabuki) {
        boolean z;
        String str;
        int i;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = -1;
        int i3 = -1;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        int i4 = -1;
        int i5 = -1;
        boolean z7 = false;
        boolean z8 = false;
        boolean z9 = false;
        boolean z10 = true;
        String str2 = null;
        int r20 = shingoYabuki.m6583();
        int i6 = 0;
        while (i6 < r20) {
            String r17 = shingoYabuki.m6584(i6);
            String r15 = shingoYabuki.m6579(i6);
            if (r17.equalsIgnoreCase("Cache-Control")) {
                if (str2 != null) {
                    z10 = false;
                } else {
                    str2 = r15;
                }
            } else if (r17.equalsIgnoreCase("Pragma")) {
                z10 = false;
            } else {
                z = z2;
                i6++;
                z2 = z;
            }
            z = z2;
            for (int i7 = 0; i7 < r15.length(); i7 = i) {
                int r3 = ChangKoehan.m18543(r15, i7, "=,;");
                String trim = r15.substring(i7, r3).trim();
                if (r3 == r15.length() || r15.charAt(r3) == ',' || r15.charAt(r3) == ';') {
                    i = r3 + 1;
                    str = null;
                } else {
                    int r172 = ChangKoehan.m18542(r15, r3 + 1);
                    if (r172 >= r15.length() || r15.charAt(r172) != '\"') {
                        i = ChangKoehan.m18543(r15, r172, ",;");
                        str = r15.substring(r172, i).trim();
                    } else {
                        int i8 = r172 + 1;
                        int r22 = ChangKoehan.m18543(r15, i8, "\"");
                        str = r15.substring(i8, r22);
                        i = r22 + 1;
                    }
                }
                if ("no-cache".equalsIgnoreCase(trim)) {
                    z = true;
                } else if ("no-store".equalsIgnoreCase(trim)) {
                    z3 = true;
                } else if ("max-age".equalsIgnoreCase(trim)) {
                    i2 = ChangKoehan.m18540(str, -1);
                } else if ("s-maxage".equalsIgnoreCase(trim)) {
                    i3 = ChangKoehan.m18540(str, -1);
                } else if ("private".equalsIgnoreCase(trim)) {
                    z4 = true;
                } else if ("public".equalsIgnoreCase(trim)) {
                    z5 = true;
                } else if ("must-revalidate".equalsIgnoreCase(trim)) {
                    z6 = true;
                } else if ("max-stale".equalsIgnoreCase(trim)) {
                    i4 = ChangKoehan.m18540(str, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                } else if ("min-fresh".equalsIgnoreCase(trim)) {
                    i5 = ChangKoehan.m18540(str, -1);
                } else if ("only-if-cached".equalsIgnoreCase(trim)) {
                    z7 = true;
                } else if ("no-transform".equalsIgnoreCase(trim)) {
                    z8 = true;
                } else if ("immutable".equalsIgnoreCase(trim)) {
                    z9 = true;
                }
            }
            i6++;
            z2 = z;
        }
        return new ChinGentsai(z2, z3, i2, i3, z4, z5, z6, i4, i5, z7, z8, z9, !z10 ? null : str2);
    }

    public String toString() {
        String str = this.f5958;
        if (str != null) {
            return str;
        }
        String r0 = m6430();
        this.f5958 = r0;
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m6432() {
        return this.f5954;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6433() {
        return this.f5955;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6434() {
        return this.f5952;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m6435() {
        return this.f5950;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m6436() {
        return this.f5953;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m6437() {
        return this.f5956;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m6438() {
        return this.f5949;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m6439() {
        return this.f5947;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6440() {
        return this.f5957;
    }
}
