package p006do;

import javax.annotation.Nullable;
import org.apache.oltu.oauth2.common.OAuth;
import p006do.ShingoYabuki;
import p006do.p007do.p020for.ChoiBounge;

/* renamed from: do.Whip  reason: invalid package */
public final class Whip {

    /* renamed from: ʻ  reason: contains not printable characters */
    private volatile ChinGentsai f6105;

    /* renamed from: 连任  reason: contains not printable characters */
    final Object f6106;

    /* renamed from: 靐  reason: contains not printable characters */
    final String f6107;
    @Nullable

    /* renamed from: 麤  reason: contains not printable characters */
    final Bao f6108;

    /* renamed from: 齉  reason: contains not printable characters */
    final ShingoYabuki f6109;

    /* renamed from: 龘  reason: contains not printable characters */
    final YashiroNanakase f6110;

    /* renamed from: do.Whip$KyoKusanagi */
    public static class KyoKusanagi {

        /* renamed from: 连任  reason: contains not printable characters */
        Object f14717;

        /* renamed from: 靐  reason: contains not printable characters */
        String f14718;

        /* renamed from: 麤  reason: contains not printable characters */
        Bao f14719;

        /* renamed from: 齉  reason: contains not printable characters */
        ShingoYabuki.KyoKusanagi f14720;

        /* renamed from: 龘  reason: contains not printable characters */
        YashiroNanakase f14721;

        public KyoKusanagi() {
            this.f14718 = OAuth.HttpMethod.GET;
            this.f14720 = new ShingoYabuki.KyoKusanagi();
        }

        KyoKusanagi(Whip whip) {
            this.f14721 = whip.f6110;
            this.f14718 = whip.f6107;
            this.f14719 = whip.f6108;
            this.f14717 = whip.f6106;
            this.f14720 = whip.f6109.m6582();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m18390() {
            return m18399("HEAD", (Bao) null);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m18391(Bao bao) {
            return m18399(OAuth.HttpMethod.PUT, bao);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m18392(String str) {
            this.f14720.m18381(str);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Whip m18393() {
            if (this.f14721 != null) {
                return new Whip(this);
            }
            throw new IllegalStateException("url == null");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18394() {
            return m18399(OAuth.HttpMethod.GET, (Bao) null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18395(Bao bao) {
            return m18399(OAuth.HttpMethod.POST, bao);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18396(ShingoYabuki shingoYabuki) {
            this.f14720 = shingoYabuki.m6582();
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18397(YashiroNanakase yashiroNanakase) {
            if (yashiroNanakase == null) {
                throw new NullPointerException("url == null");
            }
            this.f14721 = yashiroNanakase;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18398(String str) {
            if (str == null) {
                throw new NullPointerException("url == null");
            }
            if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                str = "http:" + str.substring(3);
            } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                str = "https:" + str.substring(4);
            }
            YashiroNanakase r0 = YashiroNanakase.m6594(str);
            if (r0 != null) {
                return m18397(r0);
            }
            throw new IllegalArgumentException("unexpected url: " + str);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18399(String str, @Nullable Bao bao) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (bao != null && !ChoiBounge.m18554(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (bao != null || !ChoiBounge.m18552(str)) {
                this.f14718 = str;
                this.f14719 = bao;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m18400(String str, String str2) {
            this.f14720.m18383(str, str2);
            return this;
        }
    }

    Whip(KyoKusanagi kyoKusanagi) {
        this.f6110 = kyoKusanagi.f14721;
        this.f6107 = kyoKusanagi.f14718;
        this.f6109 = kyoKusanagi.f14720.m18386();
        this.f6108 = kyoKusanagi.f14719;
        this.f6106 = kyoKusanagi.f14717 != null ? kyoKusanagi.f14717 : this;
    }

    public String toString() {
        return "Request{method=" + this.f6107 + ", url=" + this.f6110 + ", tag=" + (this.f6106 != this ? this.f6106 : null) + '}';
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public ChinGentsai m6586() {
        ChinGentsai chinGentsai = this.f6105;
        if (chinGentsai != null) {
            return chinGentsai;
        }
        ChinGentsai r0 = ChinGentsai.m6431(this.f6109);
        this.f6105 = r0;
        return r0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m6587() {
        return this.f6110.m6622();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public KyoKusanagi m6588() {
        return new KyoKusanagi(this);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m6589() {
        return this.f6107;
    }

    @Nullable
    /* renamed from: 麤  reason: contains not printable characters */
    public Bao m6590() {
        return this.f6108;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public ShingoYabuki m6591() {
        return this.f6109;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public YashiroNanakase m6592() {
        return this.f6110;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6593(String str) {
        return this.f6109.m6585(str);
    }
}
