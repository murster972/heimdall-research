package p006do;

import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/* renamed from: do.LuckyGlauber  reason: invalid package */
public final class LuckyGlauber {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final LuckyGlauber f14587 = m18354("SSL_RSA_WITH_RC4_128_SHA", 5);

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public static final LuckyGlauber f14588 = m18354("TLS_DH_anon_WITH_AES_128_CBC_SHA", 52);

    /* renamed from: ʻʼ  reason: contains not printable characters */
    public static final LuckyGlauber f14589 = m18354("TLS_PSK_WITH_AES_128_CBC_SHA", 140);

    /* renamed from: ʻʽ  reason: contains not printable characters */
    public static final LuckyGlauber f14590 = m18354("TLS_PSK_WITH_AES_256_CBC_SHA", 141);

    /* renamed from: ʻʾ  reason: contains not printable characters */
    public static final LuckyGlauber f14591 = m18354("TLS_RSA_WITH_SEED_CBC_SHA", 150);

    /* renamed from: ʻʿ  reason: contains not printable characters */
    public static final LuckyGlauber f14592 = m18354("TLS_RSA_WITH_AES_128_GCM_SHA256", 156);

    /* renamed from: ʻˆ  reason: contains not printable characters */
    public static final LuckyGlauber f14593 = m18354("TLS_RSA_WITH_AES_256_GCM_SHA384", 157);

    /* renamed from: ʻˈ  reason: contains not printable characters */
    public static final LuckyGlauber f14594 = m18354("TLS_DHE_RSA_WITH_AES_128_GCM_SHA256", 158);

    /* renamed from: ʻˉ  reason: contains not printable characters */
    public static final LuckyGlauber f14595 = m18354("TLS_DHE_RSA_WITH_AES_256_GCM_SHA384", 159);

    /* renamed from: ʻˊ  reason: contains not printable characters */
    public static final LuckyGlauber f14596 = m18354("TLS_DHE_DSS_WITH_AES_128_GCM_SHA256", 162);

    /* renamed from: ʻˋ  reason: contains not printable characters */
    public static final LuckyGlauber f14597 = m18354("TLS_DHE_DSS_WITH_AES_256_GCM_SHA384", 163);

    /* renamed from: ʻˎ  reason: contains not printable characters */
    public static final LuckyGlauber f14598 = m18354("TLS_DH_anon_WITH_AES_128_GCM_SHA256", 166);

    /* renamed from: ʻˏ  reason: contains not printable characters */
    public static final LuckyGlauber f14599 = m18354("TLS_DH_anon_WITH_AES_256_GCM_SHA384", 167);

    /* renamed from: ʻˑ  reason: contains not printable characters */
    public static final LuckyGlauber f14600 = m18354("TLS_EMPTY_RENEGOTIATION_INFO_SCSV", 255);

    /* renamed from: ʻי  reason: contains not printable characters */
    public static final LuckyGlauber f14601 = m18354("TLS_FALLBACK_SCSV", 22016);

    /* renamed from: ʻـ  reason: contains not printable characters */
    public static final LuckyGlauber f14602 = m18354("TLS_ECDH_ECDSA_WITH_NULL_SHA", 49153);

    /* renamed from: ʻٴ  reason: contains not printable characters */
    public static final LuckyGlauber f14603 = m18354("TLS_ECDH_ECDSA_WITH_RC4_128_SHA", 49154);

    /* renamed from: ʻᐧ  reason: contains not printable characters */
    public static final LuckyGlauber f14604 = m18354("TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA", 49155);

    /* renamed from: ʻᴵ  reason: contains not printable characters */
    public static final LuckyGlauber f14605 = m18354("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA", 49156);

    /* renamed from: ʻᵎ  reason: contains not printable characters */
    public static final LuckyGlauber f14606 = m18354("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA", 49157);

    /* renamed from: ʻᵔ  reason: contains not printable characters */
    public static final LuckyGlauber f14607 = m18354("TLS_ECDHE_ECDSA_WITH_NULL_SHA", 49158);

    /* renamed from: ʻᵢ  reason: contains not printable characters */
    public static final LuckyGlauber f14608 = m18354("TLS_ECDHE_ECDSA_WITH_RC4_128_SHA", 49159);

    /* renamed from: ʻⁱ  reason: contains not printable characters */
    public static final LuckyGlauber f14609 = m18354("TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA", 49160);

    /* renamed from: ʻﹳ  reason: contains not printable characters */
    public static final LuckyGlauber f14610 = m18354("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", 49161);

    /* renamed from: ʻﹶ  reason: contains not printable characters */
    public static final LuckyGlauber f14611 = m18354("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA", 49162);

    /* renamed from: ʻﾞ  reason: contains not printable characters */
    public static final LuckyGlauber f14612 = m18354("TLS_ECDH_RSA_WITH_NULL_SHA", 49163);

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final LuckyGlauber f14613 = m18354("SSL_RSA_EXPORT_WITH_DES40_CBC_SHA", 8);

    /* renamed from: ʼʻ  reason: contains not printable characters */
    public static final LuckyGlauber f14614 = m18354("TLS_ECDH_RSA_WITH_RC4_128_SHA", 49164);

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public static final LuckyGlauber f14615 = m18354("TLS_DHE_DSS_WITH_AES_256_CBC_SHA", 56);

    /* renamed from: ʼʽ  reason: contains not printable characters */
    public static final LuckyGlauber f14616 = m18354("TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA", 49165);

    /* renamed from: ʼʾ  reason: contains not printable characters */
    public static final LuckyGlauber f14617 = m18354("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA", 49166);

    /* renamed from: ʼʿ  reason: contains not printable characters */
    public static final LuckyGlauber f14618 = m18354("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA", 49167);

    /* renamed from: ʼˆ  reason: contains not printable characters */
    public static final LuckyGlauber f14619 = m18354("TLS_ECDHE_RSA_WITH_NULL_SHA", 49168);

    /* renamed from: ʼˈ  reason: contains not printable characters */
    public static final LuckyGlauber f14620 = m18354("TLS_ECDHE_RSA_WITH_RC4_128_SHA", 49169);

    /* renamed from: ʼˉ  reason: contains not printable characters */
    public static final LuckyGlauber f14621 = m18354("TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA", 49170);

    /* renamed from: ʼˊ  reason: contains not printable characters */
    public static final LuckyGlauber f14622 = m18354("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA", 49171);

    /* renamed from: ʼˋ  reason: contains not printable characters */
    public static final LuckyGlauber f14623 = m18354("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA", 49172);

    /* renamed from: ʼˎ  reason: contains not printable characters */
    public static final LuckyGlauber f14624 = m18354("TLS_ECDH_anon_WITH_NULL_SHA", 49173);

    /* renamed from: ʼˏ  reason: contains not printable characters */
    public static final LuckyGlauber f14625 = m18354("TLS_ECDH_anon_WITH_RC4_128_SHA", 49174);

    /* renamed from: ʼˑ  reason: contains not printable characters */
    public static final LuckyGlauber f14626 = m18354("TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA", 49175);

    /* renamed from: ʼי  reason: contains not printable characters */
    public static final LuckyGlauber f14627 = m18354("TLS_ECDH_anon_WITH_AES_128_CBC_SHA", 49176);

    /* renamed from: ʼـ  reason: contains not printable characters */
    public static final LuckyGlauber f14628 = m18354("TLS_ECDH_anon_WITH_AES_256_CBC_SHA", 49177);

    /* renamed from: ʼٴ  reason: contains not printable characters */
    public static final LuckyGlauber f14629 = m18354("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", 49187);

    /* renamed from: ʼᐧ  reason: contains not printable characters */
    public static final LuckyGlauber f14630 = m18354("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384", 49188);

    /* renamed from: ʼᴵ  reason: contains not printable characters */
    public static final LuckyGlauber f14631 = m18354("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256", 49189);

    /* renamed from: ʼᵎ  reason: contains not printable characters */
    public static final LuckyGlauber f14632 = m18354("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384", 49190);

    /* renamed from: ʼᵔ  reason: contains not printable characters */
    public static final LuckyGlauber f14633 = m18354("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", 49191);

    /* renamed from: ʼᵢ  reason: contains not printable characters */
    public static final LuckyGlauber f14634 = m18354("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384", 49192);

    /* renamed from: ʼⁱ  reason: contains not printable characters */
    public static final LuckyGlauber f14635 = m18354("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256", 49193);

    /* renamed from: ʼﹳ  reason: contains not printable characters */
    public static final LuckyGlauber f14636 = m18354("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384", 49194);

    /* renamed from: ʼﹶ  reason: contains not printable characters */
    public static final LuckyGlauber f14637 = m18354("TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", 49195);

    /* renamed from: ʼﾞ  reason: contains not printable characters */
    public static final LuckyGlauber f14638 = m18354("TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384", 49196);

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final LuckyGlauber f14639 = m18354("SSL_RSA_WITH_DES_CBC_SHA", 9);

    /* renamed from: ʽʻ  reason: contains not printable characters */
    public static final LuckyGlauber f14640 = m18354("TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256", 49197);

    /* renamed from: ʽʼ  reason: contains not printable characters */
    public static final LuckyGlauber f14641 = m18354("TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384", 49198);

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public static final LuckyGlauber f14642 = m18354("TLS_RSA_WITH_AES_256_CBC_SHA", 53);

    /* renamed from: ʽʾ  reason: contains not printable characters */
    public static final LuckyGlauber f14643 = m18354("TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", 49199);

    /* renamed from: ʽʿ  reason: contains not printable characters */
    public static final LuckyGlauber f14644 = m18354("TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", 49200);

    /* renamed from: ʽˆ  reason: contains not printable characters */
    public static final LuckyGlauber f14645 = m18354("TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256", 49201);

    /* renamed from: ʽˈ  reason: contains not printable characters */
    public static final LuckyGlauber f14646 = m18354("TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384", 49202);

    /* renamed from: ʽˉ  reason: contains not printable characters */
    public static final LuckyGlauber f14647 = m18354("TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA", 49205);

    /* renamed from: ʽˊ  reason: contains not printable characters */
    public static final LuckyGlauber f14648 = m18354("TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA", 49206);

    /* renamed from: ʽˋ  reason: contains not printable characters */
    public static final LuckyGlauber f14649 = m18354("TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256", 52392);

    /* renamed from: ʽˎ  reason: contains not printable characters */
    public static final LuckyGlauber f14650 = m18354("TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256", 52393);

    /* renamed from: ʽˑ  reason: contains not printable characters */
    private static final Map<String, LuckyGlauber> f14651 = new TreeMap(f14694);

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final LuckyGlauber f14652 = m18354("SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA", 20);

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public static final LuckyGlauber f14653 = m18354("TLS_DH_anon_WITH_AES_256_CBC_SHA", 58);

    /* renamed from: ʿ  reason: contains not printable characters */
    public static final LuckyGlauber f14654 = m18354("SSL_DHE_RSA_WITH_DES_CBC_SHA", 21);

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public static final LuckyGlauber f14655 = m18354("TLS_DHE_RSA_WITH_AES_256_CBC_SHA", 57);

    /* renamed from: ˆ  reason: contains not printable characters */
    public static final LuckyGlauber f14656 = m18354("SSL_DH_anon_WITH_3DES_EDE_CBC_SHA", 27);

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public static final LuckyGlauber f14657 = m18354("TLS_RSA_WITH_AES_128_CBC_SHA256", 60);

    /* renamed from: ˈ  reason: contains not printable characters */
    public static final LuckyGlauber f14658 = m18354("SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA", 19);

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public static final LuckyGlauber f14659 = m18354("TLS_DHE_DSS_WITH_AES_128_CBC_SHA256", 64);

    /* renamed from: ˉ  reason: contains not printable characters */
    public static final LuckyGlauber f14660 = m18354("TLS_KRB5_WITH_DES_CBC_SHA", 30);

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public static final LuckyGlauber f14661 = m18354("TLS_RSA_WITH_AES_256_CBC_SHA256", 61);

    /* renamed from: ˊ  reason: contains not printable characters */
    public static final LuckyGlauber f14662 = m18354("SSL_DH_anon_WITH_RC4_128_MD5", 24);

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public static final LuckyGlauber f14663 = m18354("TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA", 68);

    /* renamed from: ˋ  reason: contains not printable characters */
    public static final LuckyGlauber f14664 = m18354("SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA", 25);

    /* renamed from: ˋˋ  reason: contains not printable characters */
    public static final LuckyGlauber f14665 = m18354("TLS_RSA_WITH_CAMELLIA_128_CBC_SHA", 65);

    /* renamed from: ˎ  reason: contains not printable characters */
    public static final LuckyGlauber f14666 = m18354("SSL_DH_anon_WITH_DES_CBC_SHA", 26);

    /* renamed from: ˎˎ  reason: contains not printable characters */
    public static final LuckyGlauber f14667 = m18354("TLS_DHE_RSA_WITH_AES_128_CBC_SHA256", 103);

    /* renamed from: ˏ  reason: contains not printable characters */
    public static final LuckyGlauber f14668 = m18354("TLS_KRB5_WITH_3DES_EDE_CBC_SHA", 31);

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public static final LuckyGlauber f14669 = m18354("TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA", 69);

    /* renamed from: ˑ  reason: contains not printable characters */
    public static final LuckyGlauber f14670 = m18354("SSL_RSA_WITH_3DES_EDE_CBC_SHA", 10);

    /* renamed from: ˑˑ  reason: contains not printable characters */
    public static final LuckyGlauber f14671 = m18354("TLS_DHE_DSS_WITH_AES_256_CBC_SHA256", 106);

    /* renamed from: י  reason: contains not printable characters */
    public static final LuckyGlauber f14672 = m18354("TLS_KRB5_WITH_RC4_128_SHA", 32);

    /* renamed from: יי  reason: contains not printable characters */
    public static final LuckyGlauber f14673 = m18354("TLS_DH_anon_WITH_AES_128_CBC_SHA256", 108);

    /* renamed from: ـ  reason: contains not printable characters */
    public static final LuckyGlauber f14674 = m18354("TLS_KRB5_WITH_DES_CBC_MD5", 34);

    /* renamed from: ــ  reason: contains not printable characters */
    public static final LuckyGlauber f14675 = m18354("TLS_RSA_WITH_NULL_SHA256", 59);

    /* renamed from: ٴ  reason: contains not printable characters */
    public static final LuckyGlauber f14676 = m18354("SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA", 17);

    /* renamed from: ٴٴ  reason: contains not printable characters */
    public static final LuckyGlauber f14677 = m18354("TLS_PSK_WITH_RC4_128_SHA", TsExtractor.TS_STREAM_TYPE_DTS);

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static final LuckyGlauber f14678 = m18354("SSL_DHE_DSS_WITH_DES_CBC_SHA", 18);

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public static final LuckyGlauber f14679 = m18354("TLS_DHE_DSS_WITH_AES_128_CBC_SHA", 50);

    /* renamed from: ᴵ  reason: contains not printable characters */
    public static final LuckyGlauber f14680 = m18354("TLS_KRB5_WITH_3DES_EDE_CBC_MD5", 35);

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public static final LuckyGlauber f14681 = m18354("TLS_DHE_RSA_WITH_AES_128_CBC_SHA", 51);

    /* renamed from: ᵎ  reason: contains not printable characters */
    public static final LuckyGlauber f14682 = m18354("TLS_KRB5_WITH_RC4_128_MD5", 36);

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    public static final LuckyGlauber f14683 = m18354("TLS_DH_anon_WITH_AES_256_CBC_SHA256", 109);

    /* renamed from: ᵔ  reason: contains not printable characters */
    public static final LuckyGlauber f14684 = m18354("TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA", 38);

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    public static final LuckyGlauber f14685 = m18354("TLS_DHE_RSA_WITH_AES_256_CBC_SHA256", 107);

    /* renamed from: ᵢ  reason: contains not printable characters */
    public static final LuckyGlauber f14686 = m18354("TLS_KRB5_EXPORT_WITH_RC4_40_SHA", 40);

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    public static final LuckyGlauber f14687 = m18354("TLS_RSA_WITH_CAMELLIA_256_CBC_SHA", 132);

    /* renamed from: ⁱ  reason: contains not printable characters */
    public static final LuckyGlauber f14688 = m18354("TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5", 41);

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    public static final LuckyGlauber f14689 = m18354("TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA", TsExtractor.TS_STREAM_TYPE_E_AC3);

    /* renamed from: 连任  reason: contains not printable characters */
    public static final LuckyGlauber f14690 = m18354("SSL_RSA_WITH_RC4_128_MD5", 4);

    /* renamed from: 靐  reason: contains not printable characters */
    public static final LuckyGlauber f14691 = m18354("SSL_RSA_WITH_NULL_MD5", 1);

    /* renamed from: 麤  reason: contains not printable characters */
    public static final LuckyGlauber f14692 = m18354("SSL_RSA_EXPORT_WITH_RC4_40_MD5", 3);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final LuckyGlauber f14693 = m18354("SSL_RSA_WITH_NULL_SHA", 2);

    /* renamed from: 龘  reason: contains not printable characters */
    static final Comparator<String> f14694 = new Comparator<String>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(String str, String str2) {
            int min = Math.min(str.length(), str2.length());
            for (int i = 4; i < min; i++) {
                char charAt = str.charAt(i);
                char charAt2 = str2.charAt(i);
                if (charAt != charAt2) {
                    return charAt < charAt2 ? -1 : 1;
                }
            }
            int length = str.length();
            int length2 = str2.length();
            if (length != length2) {
                return length >= length2 ? 1 : -1;
            }
            return 0;
        }
    };

    /* renamed from: ﹳ  reason: contains not printable characters */
    public static final LuckyGlauber f14695 = m18354("TLS_KRB5_EXPORT_WITH_RC4_40_MD5", 43);

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    public static final LuckyGlauber f14696 = m18354("TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA", 136);

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static final LuckyGlauber f14697 = m18354("SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA", 22);

    /* renamed from: ﹶﹶ  reason: contains not printable characters */
    public static final LuckyGlauber f14698 = m18354("TLS_PSK_WITH_3DES_EDE_CBC_SHA", 139);

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static final LuckyGlauber f14699 = m18354("SSL_DH_anon_EXPORT_WITH_RC4_40_MD5", 23);

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public static final LuckyGlauber f14700 = m18354("TLS_RSA_WITH_AES_128_CBC_SHA", 47);

    /* renamed from: ʽˏ  reason: contains not printable characters */
    final String f14701;

    private LuckyGlauber(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f14701 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized LuckyGlauber m18353(String str) {
        LuckyGlauber luckyGlauber;
        synchronized (LuckyGlauber.class) {
            luckyGlauber = f14651.get(str);
            if (luckyGlauber == null) {
                luckyGlauber = new LuckyGlauber(str);
                f14651.put(str, luckyGlauber);
            }
        }
        return luckyGlauber;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static LuckyGlauber m18354(String str, int i) {
        return m18353(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static List<LuckyGlauber> m18355(String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String r3 : strArr) {
            arrayList.add(m18353(r3));
        }
        return Collections.unmodifiableList(arrayList);
    }

    public String toString() {
        return this.f14701;
    }
}
