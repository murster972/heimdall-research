package p006do;

import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import p006do.p007do.GoroDaimon;

/* renamed from: do.Goenitz  reason: invalid package */
public final class Goenitz {

    /* renamed from: 靐  reason: contains not printable characters */
    private final LuckyGlauber f14574;

    /* renamed from: 麤  reason: contains not printable characters */
    private final List<Certificate> f14575;

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<Certificate> f14576;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Vanessa f14577;

    private Goenitz(Vanessa vanessa, LuckyGlauber luckyGlauber, List<Certificate> list, List<Certificate> list2) {
        this.f14577 = vanessa;
        this.f14574 = luckyGlauber;
        this.f14576 = list;
        this.f14575 = list2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Goenitz m18344(SSLSession sSLSession) {
        Certificate[] certificateArr;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        }
        LuckyGlauber r2 = LuckyGlauber.m18353(cipherSuite);
        String protocol = sSLSession.getProtocol();
        if (protocol == null) {
            throw new IllegalStateException("tlsVersion == null");
        }
        Vanessa a = Vanessa.a(protocol);
        try {
            certificateArr = sSLSession.getPeerCertificates();
        } catch (SSLPeerUnverifiedException e) {
            certificateArr = null;
        }
        List r0 = certificateArr != null ? GoroDaimon.m18419((T[]) certificateArr) : Collections.emptyList();
        Certificate[] localCertificates = sSLSession.getLocalCertificates();
        return new Goenitz(a, r2, r0, localCertificates != null ? GoroDaimon.m18419((T[]) localCertificates) : Collections.emptyList());
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof Goenitz)) {
            return false;
        }
        Goenitz goenitz = (Goenitz) obj;
        return this.f14577.equals(goenitz.f14577) && this.f14574.equals(goenitz.f14574) && this.f14576.equals(goenitz.f14576) && this.f14575.equals(goenitz.f14575);
    }

    public int hashCode() {
        return ((((((this.f14577.hashCode() + 527) * 31) + this.f14574.hashCode()) * 31) + this.f14576.hashCode()) * 31) + this.f14575.hashCode();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public List<Certificate> m18345() {
        return this.f14576;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public LuckyGlauber m18346() {
        return this.f14574;
    }
}
