package p006do;

import com.mopub.common.TyphoonApp;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import p006do.p007do.GoroDaimon;

/* renamed from: do.YashiroNanakase  reason: invalid package */
public final class YashiroNanakase {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final char[] f6111 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f6112;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final List<String> f6113;
    @Nullable

    /* renamed from: ʽ  reason: contains not printable characters */
    private final List<String> f6114;
    @Nullable

    /* renamed from: ˑ  reason: contains not printable characters */
    private final String f6115;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final String f6116;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f6117;

    /* renamed from: 靐  reason: contains not printable characters */
    final String f6118;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f6119;

    /* renamed from: 龘  reason: contains not printable characters */
    final String f6120;

    /* renamed from: do.YashiroNanakase$KyoKusanagi */
    public static final class KyoKusanagi {

        /* renamed from: ʻ  reason: contains not printable characters */
        final List<String> f6121 = new ArrayList();
        @Nullable

        /* renamed from: ʼ  reason: contains not printable characters */
        List<String> f6122;
        @Nullable

        /* renamed from: ʽ  reason: contains not printable characters */
        String f6123;

        /* renamed from: 连任  reason: contains not printable characters */
        int f6124 = -1;

        /* renamed from: 靐  reason: contains not printable characters */
        String f6125 = "";
        @Nullable

        /* renamed from: 麤  reason: contains not printable characters */
        String f6126;

        /* renamed from: 齉  reason: contains not printable characters */
        String f6127 = "";
        @Nullable

        /* renamed from: 龘  reason: contains not printable characters */
        String f6128;

        /* renamed from: do.YashiroNanakase$KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
        enum C0034KyoKusanagi {
            SUCCESS,
            MISSING_SCHEME,
            UNSUPPORTED_SCHEME,
            INVALID_PORT,
            INVALID_HOST
        }

        public KyoKusanagi() {
            this.f6121.add("");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
            return null;
         */
        @javax.annotation.Nullable
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static java.net.InetAddress m6624(java.lang.String r12, int r13, int r14) {
            /*
                r11 = 1
                r7 = -1
                r4 = 0
                r5 = 0
                r0 = 16
                byte[] r8 = new byte[r0]
                r1 = r13
                r0 = r7
                r2 = r7
                r3 = r5
            L_0x000c:
                if (r1 >= r14) goto L_0x002d
                int r6 = r8.length
                if (r3 != r6) goto L_0x0013
                r0 = r4
            L_0x0012:
                return r0
            L_0x0013:
                int r6 = r1 + 2
                if (r6 > r14) goto L_0x0034
                java.lang.String r6 = "::"
                r9 = 2
                boolean r6 = r12.regionMatches(r1, r6, r5, r9)
                if (r6 == 0) goto L_0x0034
                if (r2 == r7) goto L_0x0025
                r0 = r4
                goto L_0x0012
            L_0x0025:
                int r0 = r1 + 2
                int r1 = r3 + 2
                if (r0 != r14) goto L_0x00a7
                r2 = r1
                r3 = r1
            L_0x002d:
                int r0 = r8.length
                if (r3 == r0) goto L_0x0098
                if (r2 != r7) goto L_0x0089
                r0 = r4
                goto L_0x0012
            L_0x0034:
                if (r3 == 0) goto L_0x00a5
                java.lang.String r6 = ":"
                boolean r6 = r12.regionMatches(r1, r6, r5, r11)
                if (r6 == 0) goto L_0x0058
                int r0 = r1 + 1
            L_0x0041:
                r1 = r5
                r6 = r0
            L_0x0043:
                if (r6 >= r14) goto L_0x004f
                char r9 = r12.charAt(r6)
                int r9 = p006do.YashiroNanakase.m6597((char) r9)
                if (r9 != r7) goto L_0x0071
            L_0x004f:
                int r9 = r6 - r0
                if (r9 == 0) goto L_0x0056
                r10 = 4
                if (r9 <= r10) goto L_0x0077
            L_0x0056:
                r0 = r4
                goto L_0x0012
            L_0x0058:
                java.lang.String r6 = "."
                boolean r1 = r12.regionMatches(r1, r6, r5, r11)
                if (r1 == 0) goto L_0x006f
                int r1 = r3 + -2
                boolean r0 = m6636((java.lang.String) r12, (int) r0, (int) r14, (byte[]) r8, (int) r1)
                if (r0 != 0) goto L_0x006b
                r0 = r4
                goto L_0x0012
            L_0x006b:
                int r1 = r3 + 2
                r3 = r1
                goto L_0x002d
            L_0x006f:
                r0 = r4
                goto L_0x0012
            L_0x0071:
                int r1 = r1 << 4
                int r1 = r1 + r9
                int r6 = r6 + 1
                goto L_0x0043
            L_0x0077:
                int r9 = r3 + 1
                int r10 = r1 >>> 8
                r10 = r10 & 255(0xff, float:3.57E-43)
                byte r10 = (byte) r10
                r8[r3] = r10
                int r3 = r9 + 1
                r1 = r1 & 255(0xff, float:3.57E-43)
                byte r1 = (byte) r1
                r8[r9] = r1
                r1 = r6
                goto L_0x000c
            L_0x0089:
                int r0 = r8.length
                int r1 = r3 - r2
                int r0 = r0 - r1
                int r1 = r3 - r2
                java.lang.System.arraycopy(r8, r2, r8, r0, r1)
                int r0 = r8.length
                int r0 = r0 - r3
                int r0 = r0 + r2
                java.util.Arrays.fill(r8, r2, r0, r5)
            L_0x0098:
                java.net.InetAddress r0 = java.net.InetAddress.getByAddress(r8)     // Catch:{ UnknownHostException -> 0x009e }
                goto L_0x0012
            L_0x009e:
                r0 = move-exception
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x00a5:
                r0 = r1
                goto L_0x0041
            L_0x00a7:
                r2 = r1
                r3 = r1
                goto L_0x0041
            */
            throw new UnsupportedOperationException("Method not decompiled: p006do.YashiroNanakase.KyoKusanagi.m6624(java.lang.String, int, int):java.net.InetAddress");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean m6625(String str) {
            return str.equals(".") || str.equalsIgnoreCase("%2e");
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private static int m6626(String str, int i, int i2) {
            try {
                int parseInt = Integer.parseInt(YashiroNanakase.m6599(str, i, i2, "", false, false, false, true));
                if (parseInt <= 0 || parseInt > 65535) {
                    return -1;
                }
                return parseInt;
            } catch (NumberFormatException e) {
                return -1;
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean m6627(String str) {
            return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private static String m6628(String str, int i, int i2) {
            String r0 = YashiroNanakase.m6600(str, i, i2, false);
            if (!r0.contains(":")) {
                return GoroDaimon.m18415(r0);
            }
            InetAddress r02 = (!r0.startsWith("[") || !r0.endsWith("]")) ? m6624(r0, 0, r0.length()) : m6624(r0, 1, r0.length() - 1);
            if (r02 == null) {
                return null;
            }
            byte[] address = r02.getAddress();
            if (address.length == 16) {
                return m6633(address);
            }
            throw new AssertionError();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private static int m6629(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
                return -1;
            }
            int i3 = i + 1;
            while (i3 < i2) {
                char charAt2 = str.charAt(i3);
                if ((charAt2 >= 'a' && charAt2 <= 'z') || ((charAt2 >= 'A' && charAt2 <= 'Z') || ((charAt2 >= '0' && charAt2 <= '9') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.'))) {
                    i3++;
                } else if (charAt2 == ':') {
                    return i3;
                } else {
                    return -1;
                }
            }
            return -1;
        }

        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x000f, code lost:
            if (r0 >= r5) goto L_0x000a;
         */
        /* renamed from: 麤  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static int m6630(java.lang.String r3, int r4, int r5) {
            /*
                r0 = r4
            L_0x0001:
                if (r0 >= r5) goto L_0x001a
                char r1 = r3.charAt(r0)
                switch(r1) {
                    case 58: goto L_0x001b;
                    case 91: goto L_0x000d;
                    default: goto L_0x000a;
                }
            L_0x000a:
                int r0 = r0 + 1
                goto L_0x0001
            L_0x000d:
                int r0 = r0 + 1
                if (r0 >= r5) goto L_0x000a
                char r1 = r3.charAt(r0)
                r2 = 93
                if (r1 != r2) goto L_0x000d
                goto L_0x000a
            L_0x001a:
                r0 = r5
            L_0x001b:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: p006do.YashiroNanakase.KyoKusanagi.m6630(java.lang.String, int, int):int");
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m6631() {
            if (!this.f6121.remove(this.f6121.size() - 1).isEmpty() || this.f6121.isEmpty()) {
                this.f6121.add("");
            } else {
                this.f6121.set(this.f6121.size() - 1, "");
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private static int m6632(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static String m6633(byte[] bArr) {
            int i = 0;
            int i2 = -1;
            int i3 = 0;
            int i4 = 0;
            while (i3 < bArr.length) {
                int i5 = i3;
                while (i5 < 16 && bArr[i5] == 0 && bArr[i5 + 1] == 0) {
                    i5 += 2;
                }
                int i6 = i5 - i3;
                if (i6 > i4 && i6 >= 4) {
                    i4 = i6;
                    i2 = i3;
                }
                i3 = i5 + 2;
            }
            p009if.GoroDaimon goroDaimon = new p009if.GoroDaimon();
            while (i < bArr.length) {
                if (i == i2) {
                    goroDaimon.m6770(58);
                    i += i4;
                    if (i == 16) {
                        goroDaimon.m6770(58);
                    }
                } else {
                    if (i > 0) {
                        goroDaimon.m6770(58);
                    }
                    goroDaimon.m6721((long) (((bArr[i] & 255) << 8) | (bArr[i + 1] & 255)));
                    i += 2;
                }
            }
            return goroDaimon.m6737();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m6634(String str, int i, int i2) {
            if (i != i2) {
                char charAt = str.charAt(i);
                if (charAt == '/' || charAt == '\\') {
                    this.f6121.clear();
                    this.f6121.add("");
                    i++;
                } else {
                    this.f6121.set(this.f6121.size() - 1, "");
                }
                int i3 = i;
                while (i3 < i2) {
                    int r3 = GoroDaimon.m18412(str, i3, i2, "/\\");
                    boolean z = r3 < i2;
                    m6635(str, i3, r3, z, true);
                    if (z) {
                        r3++;
                    }
                    i3 = r3;
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m6635(String str, int i, int i2, boolean z, boolean z2) {
            String r1 = YashiroNanakase.m6599(str, i, i2, " \"<>^`{}|/\\?#", z2, false, false, true);
            if (!m6625(r1)) {
                if (m6627(r1)) {
                    m6631();
                    return;
                }
                if (this.f6121.get(this.f6121.size() - 1).isEmpty()) {
                    this.f6121.set(this.f6121.size() - 1, r1);
                } else {
                    this.f6121.add(r1);
                }
                if (z) {
                    this.f6121.add("");
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static boolean m6636(String str, int i, int i2, byte[] bArr, int i3) {
            int i4 = i;
            int i5 = i3;
            while (i4 < i2) {
                if (i5 == bArr.length) {
                    return false;
                }
                if (i5 != i3) {
                    if (str.charAt(i4) != '.') {
                        return false;
                    }
                    i4++;
                }
                int i6 = 0;
                int i7 = i4;
                while (i7 < i2) {
                    char charAt = str.charAt(i7);
                    if (charAt < '0' || charAt > '9') {
                        break;
                    } else if (i6 == 0 && i4 != i7) {
                        return false;
                    } else {
                        i6 = ((i6 * 10) + charAt) - 48;
                        if (i6 > 255) {
                            return false;
                        }
                        i7++;
                    }
                }
                if (i7 - i4 == 0) {
                    return false;
                }
                bArr[i5] = (byte) i6;
                i4 = i7;
                i5++;
            }
            return i5 == i3 + 4;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f6128);
            sb.append("://");
            if (!this.f6125.isEmpty() || !this.f6127.isEmpty()) {
                sb.append(this.f6125);
                if (!this.f6127.isEmpty()) {
                    sb.append(':');
                    sb.append(this.f6127);
                }
                sb.append('@');
            }
            if (this.f6126.indexOf(58) != -1) {
                sb.append('[');
                sb.append(this.f6126);
                sb.append(']');
            } else {
                sb.append(this.f6126);
            }
            int r1 = m6643();
            if (r1 != YashiroNanakase.m6598(this.f6128)) {
                sb.append(':');
                sb.append(r1);
            }
            YashiroNanakase.m6606(sb, this.f6121);
            if (this.f6122 != null) {
                sb.append('?');
                YashiroNanakase.m6596(sb, this.f6122);
            }
            if (this.f6123 != null) {
                sb.append('#');
                sb.append(this.f6123);
            }
            return sb.toString();
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public KyoKusanagi m6637(@Nullable String str) {
            this.f6122 = str != null ? YashiroNanakase.m6595(YashiroNanakase.m6601(str, " \"'<>#", true, false, true, true)) : null;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m6638() {
            int size = this.f6121.size();
            for (int i = 0; i < size; i++) {
                this.f6121.set(i, YashiroNanakase.m6601(this.f6121.get(i), "[]", true, true, false, true));
            }
            if (this.f6122 != null) {
                int size2 = this.f6122.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    String str = this.f6122.get(i2);
                    if (str != null) {
                        this.f6122.set(i2, YashiroNanakase.m6601(str, "\\^`{|}", true, true, true, true));
                    }
                }
            }
            if (this.f6123 != null) {
                this.f6123 = YashiroNanakase.m6601(this.f6123, " \"#<>\\^`{|}", true, true, false, false);
            }
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public KyoKusanagi m6639(String str) {
            if (str == null) {
                throw new NullPointerException("username == null");
            }
            this.f6125 = YashiroNanakase.m6601(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public KyoKusanagi m6640(String str) {
            if (str == null) {
                throw new NullPointerException("host == null");
            }
            String r0 = m6628(str, 0, str.length());
            if (r0 == null) {
                throw new IllegalArgumentException("unexpected host: " + str);
            }
            this.f6126 = r0;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public KyoKusanagi m6641(String str) {
            if (str == null) {
                throw new NullPointerException("password == null");
            }
            this.f6127 = YashiroNanakase.m6601(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public YashiroNanakase m6642() {
            if (this.f6128 == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.f6126 != null) {
                return new YashiroNanakase(this);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m6643() {
            return this.f6124 != -1 ? this.f6124 : YashiroNanakase.m6598(this.f6128);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public C0034KyoKusanagi m6644(@Nullable YashiroNanakase yashiroNanakase, String str) {
            int i;
            int r2 = GoroDaimon.m18410(str, 0, str.length());
            int r11 = GoroDaimon.m18404(str, r2, str.length());
            if (m6629(str, r2, r11) != -1) {
                if (str.regionMatches(true, r2, "https:", 0, 6)) {
                    this.f6128 = TyphoonApp.HTTPS;
                    r2 += "https:".length();
                } else if (!str.regionMatches(true, r2, "http:", 0, 5)) {
                    return C0034KyoKusanagi.UNSUPPORTED_SCHEME;
                } else {
                    this.f6128 = TyphoonApp.HTTP;
                    r2 += "http:".length();
                }
            } else if (yashiroNanakase == null) {
                return C0034KyoKusanagi.MISSING_SCHEME;
            } else {
                this.f6128 = yashiroNanakase.f6120;
            }
            int r1 = m6632(str, r2, r11);
            if (r1 >= 2 || yashiroNanakase == null || !yashiroNanakase.f6120.equals(this.f6128)) {
                int i2 = r1 + r2;
                boolean z = false;
                boolean z2 = false;
                while (true) {
                    int r10 = GoroDaimon.m18412(str, i2, r11, "@/\\?#");
                    switch (r10 != r11 ? str.charAt(r10) : 65535) {
                        case 65535:
                        case '#':
                        case '/':
                        case '?':
                        case '\\':
                            int r0 = m6630(str, i2, r10);
                            if (r0 + 1 < r10) {
                                this.f6126 = m6628(str, i2, r0);
                                this.f6124 = m6626(str, r0 + 1, r10);
                                if (this.f6124 == -1) {
                                    return C0034KyoKusanagi.INVALID_PORT;
                                }
                            } else {
                                this.f6126 = m6628(str, i2, r0);
                                this.f6124 = YashiroNanakase.m6598(this.f6128);
                            }
                            if (this.f6126 != null) {
                                r2 = r10;
                                break;
                            } else {
                                return C0034KyoKusanagi.INVALID_HOST;
                            }
                        case '@':
                            if (!z) {
                                int r22 = GoroDaimon.m18411(str, i2, r10, ':');
                                String r02 = YashiroNanakase.m6599(str, i2, r22, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                                if (z2) {
                                    r02 = this.f6125 + "%40" + r02;
                                }
                                this.f6125 = r02;
                                if (r22 != r10) {
                                    z = true;
                                    this.f6127 = YashiroNanakase.m6599(str, r22 + 1, r10, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                                }
                                z2 = true;
                            } else {
                                this.f6127 += "%40" + YashiroNanakase.m6599(str, i2, r10, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                            }
                            i2 = r10 + 1;
                            continue;
                    }
                    z = z;
                    z2 = z2;
                }
            } else {
                this.f6125 = yashiroNanakase.m6620();
                this.f6127 = yashiroNanakase.m6617();
                this.f6126 = yashiroNanakase.f6118;
                this.f6124 = yashiroNanakase.f6119;
                this.f6121.clear();
                this.f6121.addAll(yashiroNanakase.m6614());
                if (r2 == r11 || str.charAt(r2) == '#') {
                    m6637(yashiroNanakase.m6615());
                }
            }
            int r03 = GoroDaimon.m18412(str, r2, r11, "?#");
            m6634(str, r2, r03);
            if (r03 >= r11 || str.charAt(r03) != '?') {
                i = r03;
            } else {
                i = GoroDaimon.m18411(str, r03, r11, '#');
                this.f6122 = YashiroNanakase.m6595(YashiroNanakase.m6599(str, r03 + 1, i, " \"'<>#", true, false, true, true));
            }
            if (i < r11 && str.charAt(i) == '#') {
                this.f6123 = YashiroNanakase.m6599(str, i + 1, r11, "", true, false, false, false);
            }
            return C0034KyoKusanagi.SUCCESS;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6645(int i) {
            if (i <= 0 || i > 65535) {
                throw new IllegalArgumentException("unexpected port: " + i);
            }
            this.f6124 = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public KyoKusanagi m6646(String str) {
            if (str == null) {
                throw new NullPointerException("scheme == null");
            }
            if (str.equalsIgnoreCase(TyphoonApp.HTTP)) {
                this.f6128 = TyphoonApp.HTTP;
            } else if (str.equalsIgnoreCase(TyphoonApp.HTTPS)) {
                this.f6128 = TyphoonApp.HTTPS;
            } else {
                throw new IllegalArgumentException("unexpected scheme: " + str);
            }
            return this;
        }
    }

    YashiroNanakase(KyoKusanagi kyoKusanagi) {
        String str = null;
        this.f6120 = kyoKusanagi.f6128;
        this.f6117 = m6602(kyoKusanagi.f6125, false);
        this.f6112 = m6602(kyoKusanagi.f6127, false);
        this.f6118 = kyoKusanagi.f6126;
        this.f6119 = kyoKusanagi.m6643();
        this.f6113 = m6603(kyoKusanagi.f6121, false);
        this.f6114 = kyoKusanagi.f6122 != null ? m6603(kyoKusanagi.f6122, true) : null;
        this.f6115 = kyoKusanagi.f6123 != null ? m6602(kyoKusanagi.f6123, false) : str;
        this.f6116 = kyoKusanagi.toString();
    }

    @Nullable
    /* renamed from: 连任  reason: contains not printable characters */
    public static YashiroNanakase m6594(String str) {
        KyoKusanagi kyoKusanagi = new KyoKusanagi();
        if (kyoKusanagi.m6644((YashiroNanakase) null, str) == KyoKusanagi.C0034KyoKusanagi.SUCCESS) {
            return kyoKusanagi.m6642();
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static List<String> m6595(String str) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i <= str.length()) {
            int indexOf = str.indexOf(38, i);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i, indexOf));
                arrayList.add((Object) null);
            } else {
                arrayList.add(str.substring(i, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i = indexOf + 1;
        }
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static void m6596(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i += 2) {
            String str = list.get(i);
            String str2 = list.get(i + 1);
            if (i > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m6597(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'a' && c <= 'f') {
            return (c - 'a') + 10;
        }
        if (c < 'A' || c > 'F') {
            return -1;
        }
        return (c - 'A') + 10;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m6598(String str) {
        if (str.equals(TyphoonApp.HTTP)) {
            return 80;
        }
        return str.equals(TyphoonApp.HTTPS) ? 443 : -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6599(String str, int i, int i2, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        int i3 = i;
        while (i3 < i2) {
            int codePointAt = str.codePointAt(i3);
            if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || ((codePointAt == 37 && (!z || (z2 && !m6607(str, i3, i2)))) || (codePointAt == 43 && z3)))) {
                p009if.GoroDaimon goroDaimon = new p009if.GoroDaimon();
                goroDaimon.m6776(str, i, i3);
                m6604(goroDaimon, str, i3, i2, str2, z, z2, z3, z4);
                return goroDaimon.m6737();
            }
            i3 += Character.charCount(codePointAt);
        }
        return str.substring(i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6600(String str, int i, int i2, boolean z) {
        for (int i3 = i; i3 < i2; i3++) {
            char charAt = str.charAt(i3);
            if (charAt == '%' || (charAt == '+' && z)) {
                p009if.GoroDaimon goroDaimon = new p009if.GoroDaimon();
                goroDaimon.m6776(str, i, i3);
                m6605(goroDaimon, str, i3, i2, z);
                return goroDaimon.m6737();
            }
        }
        return str.substring(i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6601(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        return m6599(str, 0, str.length(), str2, z, z2, z3, z4);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6602(String str, boolean z) {
        return m6600(str, 0, str.length(), z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<String> m6603(List<String> list, boolean z) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            String str = list.get(i);
            arrayList.add(str != null ? m6602(str, z) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m6604(p009if.GoroDaimon goroDaimon, String str, int i, int i2, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        p009if.GoroDaimon goroDaimon2 = null;
        while (i < i2) {
            int codePointAt = str.codePointAt(i);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt == 43 && z3) {
                    goroDaimon.m6771(z ? "+" : "%2B");
                } else if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && (!z || (z2 && !m6607(str, i, i2)))))) {
                    if (goroDaimon2 == null) {
                        goroDaimon2 = new p009if.GoroDaimon();
                    }
                    goroDaimon2.m6750(codePointAt);
                    while (!goroDaimon2.m6765()) {
                        byte r1 = goroDaimon2.m6758() & 255;
                        goroDaimon.m6770(37);
                        goroDaimon.m6770((int) f6111[(r1 >> 4) & 15]);
                        goroDaimon.m6770((int) f6111[r1 & 15]);
                    }
                } else {
                    goroDaimon.m6750(codePointAt);
                }
            }
            i += Character.charCount(codePointAt);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m6605(p009if.GoroDaimon goroDaimon, String str, int i, int i2, boolean z) {
        int i3 = i;
        while (i3 < i2) {
            int codePointAt = str.codePointAt(i3);
            if (codePointAt != 37 || i3 + 2 >= i2) {
                if (codePointAt == 43 && z) {
                    goroDaimon.m6770(32);
                }
                goroDaimon.m6750(codePointAt);
            } else {
                int r2 = m6597(str.charAt(i3 + 1));
                int r3 = m6597(str.charAt(i3 + 2));
                if (!(r2 == -1 || r3 == -1)) {
                    goroDaimon.m6770((r2 << 4) + r3);
                    i3 += 2;
                }
                goroDaimon.m6750(codePointAt);
            }
            i3 += Character.charCount(codePointAt);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m6606(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            sb.append('/');
            sb.append(list.get(i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m6607(String str, int i, int i2) {
        return i + 2 < i2 && str.charAt(i) == '%' && m6597(str.charAt(i + 1)) != -1 && m6597(str.charAt(i + 2)) != -1;
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof YashiroNanakase) && ((YashiroNanakase) obj).f6116.equals(this.f6116);
    }

    public int hashCode() {
        return this.f6116.hashCode();
    }

    public String toString() {
        return this.f6116;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m6608() {
        return this.f6118;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6609() {
        return this.f6119;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m6610() {
        int indexOf = this.f6116.indexOf(47, this.f6120.length() + 3);
        return this.f6116.substring(indexOf, GoroDaimon.m18412(this.f6116, indexOf, this.f6116.length(), "?#"));
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public String m6611() {
        return m6619("/...").m6639("").m6641("").m6642().toString();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public KyoKusanagi m6612() {
        KyoKusanagi kyoKusanagi = new KyoKusanagi();
        kyoKusanagi.f6128 = this.f6120;
        kyoKusanagi.f6125 = m6620();
        kyoKusanagi.f6127 = m6617();
        kyoKusanagi.f6126 = this.f6118;
        kyoKusanagi.f6124 = this.f6119 != m6598(this.f6120) ? this.f6119 : -1;
        kyoKusanagi.f6121.clear();
        kyoKusanagi.f6121.addAll(m6614());
        kyoKusanagi.m6637(m6615());
        kyoKusanagi.f6123 = m6613();
        return kyoKusanagi;
    }

    @Nullable
    /* renamed from: ˈ  reason: contains not printable characters */
    public String m6613() {
        if (this.f6115 == null) {
            return null;
        }
        return this.f6116.substring(this.f6116.indexOf(35) + 1);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public List<String> m6614() {
        int indexOf = this.f6116.indexOf(47, this.f6120.length() + 3);
        int r1 = GoroDaimon.m18412(this.f6116, indexOf, this.f6116.length(), "?#");
        ArrayList arrayList = new ArrayList();
        while (indexOf < r1) {
            int i = indexOf + 1;
            indexOf = GoroDaimon.m18411(this.f6116, i, r1, '/');
            arrayList.add(this.f6116.substring(i, indexOf));
        }
        return arrayList;
    }

    @Nullable
    /* renamed from: ٴ  reason: contains not printable characters */
    public String m6615() {
        if (this.f6114 == null) {
            return null;
        }
        int indexOf = this.f6116.indexOf(63) + 1;
        return this.f6116.substring(indexOf, GoroDaimon.m18411(this.f6116, indexOf + 1, this.f6116.length(), '#'));
    }

    @Nullable
    /* renamed from: ᐧ  reason: contains not printable characters */
    public String m6616() {
        if (this.f6114 == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        m6596(sb, this.f6114);
        return sb.toString();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m6617() {
        if (this.f6112.isEmpty()) {
            return "";
        }
        int indexOf = this.f6116.indexOf(64);
        return this.f6116.substring(this.f6116.indexOf(58, this.f6120.length() + 3) + 1, indexOf);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m6618() {
        return this.f6120;
    }

    @Nullable
    /* renamed from: 麤  reason: contains not printable characters */
    public KyoKusanagi m6619(String str) {
        KyoKusanagi kyoKusanagi = new KyoKusanagi();
        if (kyoKusanagi.m6644(this, str) == KyoKusanagi.C0034KyoKusanagi.SUCCESS) {
            return kyoKusanagi;
        }
        return null;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m6620() {
        if (this.f6117.isEmpty()) {
            return "";
        }
        int length = this.f6120.length() + 3;
        return this.f6116.substring(length, GoroDaimon.m18412(this.f6116, length, this.f6116.length(), ":@"));
    }

    @Nullable
    /* renamed from: 齉  reason: contains not printable characters */
    public YashiroNanakase m6621(String str) {
        KyoKusanagi r0 = m6619(str);
        if (r0 != null) {
            return r0.m6642();
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m6622() {
        return this.f6120.equals(TyphoonApp.HTTPS);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public URI m6623() {
        String kyoKusanagi = m6612().m6638().toString();
        try {
            return new URI(kyoKusanagi);
        } catch (URISyntaxException e) {
            try {
                return URI.create(kyoKusanagi.replaceAll("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]", ""));
            } catch (Exception e2) {
                throw new RuntimeException(e);
            }
        }
    }
}
