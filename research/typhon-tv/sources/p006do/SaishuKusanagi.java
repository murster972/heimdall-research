package p006do;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import p006do.p007do.GoroDaimon;
import p006do.p007do.p018case.KyoKusanagi;
import p006do.p007do.p020for.ChinGentsai;

/* renamed from: do.SaishuKusanagi  reason: invalid package */
public final class SaishuKusanagi {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Pattern f6091 = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Pattern f6092 = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Pattern f6093 = Pattern.compile("(\\d{1,2})[^\\d]*");

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f6094 = Pattern.compile("(\\d{2,4})[^\\d]*");

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f6095;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final long f6096;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String f6097;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final boolean f6098;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final boolean f6099;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final String f6100;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean f6101;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final boolean f6102;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f6103;

    private SaishuKusanagi(String str, String str2, long j, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4) {
        this.f6103 = str;
        this.f6095 = str2;
        this.f6096 = j;
        this.f6097 = str3;
        this.f6100 = str4;
        this.f6101 = z;
        this.f6102 = z2;
        this.f6098 = z3;
        this.f6099 = z4;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m6564(String str) {
        if (str.endsWith(".")) {
            throw new IllegalArgumentException();
        }
        if (str.startsWith(".")) {
            str = str.substring(1);
        }
        String r0 = GoroDaimon.m18415(str);
        if (r0 != null) {
            return r0;
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m6565(String str, int i, int i2, boolean z) {
        for (int i3 = i; i3 < i2; i3++) {
            char charAt = str.charAt(i3);
            if (((charAt < ' ' && charAt != 9) || charAt >= 127 || (charAt >= '0' && charAt <= '9') || ((charAt >= 'a' && charAt <= 'z') || ((charAt >= 'A' && charAt <= 'Z') || charAt == ':'))) == (!z)) {
                return i3;
            }
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m6566(String str) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong <= 0) {
                return Long.MIN_VALUE;
            }
            return parseLong;
        } catch (NumberFormatException e) {
            if (str.matches("-?\\d+")) {
                return !str.startsWith("-") ? Long.MAX_VALUE : Long.MIN_VALUE;
            }
            throw e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m6567(String str, int i, int i2) {
        int r6 = m6565(str, i, i2, false);
        int i3 = -1;
        int i4 = -1;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        int i8 = -1;
        Matcher matcher = f6092.matcher(str);
        while (r6 < i2) {
            int r8 = m6565(str, r6 + 1, i2, true);
            matcher.region(r6, r8);
            if (i3 == -1 && matcher.usePattern(f6092).matches()) {
                i3 = Integer.parseInt(matcher.group(1));
                i4 = Integer.parseInt(matcher.group(2));
                i5 = Integer.parseInt(matcher.group(3));
            } else if (i6 == -1 && matcher.usePattern(f6093).matches()) {
                i6 = Integer.parseInt(matcher.group(1));
            } else if (i7 == -1 && matcher.usePattern(f6091).matches()) {
                i7 = f6091.pattern().indexOf(matcher.group(1).toLowerCase(Locale.US)) / 4;
            } else if (i8 == -1 && matcher.usePattern(f6094).matches()) {
                i8 = Integer.parseInt(matcher.group(1));
            }
            r6 = m6565(str, r8 + 1, i2, false);
        }
        if (i8 >= 70 && i8 <= 99) {
            i8 += 1900;
        }
        if (i8 >= 0 && i8 <= 69) {
            i8 += 2000;
        }
        if (i8 < 1601) {
            throw new IllegalArgumentException();
        } else if (i7 == -1) {
            throw new IllegalArgumentException();
        } else if (i6 < 1 || i6 > 31) {
            throw new IllegalArgumentException();
        } else if (i3 < 0 || i3 > 23) {
            throw new IllegalArgumentException();
        } else if (i4 < 0 || i4 > 59) {
            throw new IllegalArgumentException();
        } else if (i5 < 0 || i5 > 59) {
            throw new IllegalArgumentException();
        } else {
            GregorianCalendar gregorianCalendar = new GregorianCalendar(GoroDaimon.f14729);
            gregorianCalendar.setLenient(false);
            gregorianCalendar.set(1, i8);
            gregorianCalendar.set(2, i7 - 1);
            gregorianCalendar.set(5, i6);
            gregorianCalendar.set(11, i3);
            gregorianCalendar.set(12, i4);
            gregorianCalendar.set(13, i5);
            gregorianCalendar.set(14, 0);
            return gregorianCalendar.getTimeInMillis();
        }
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    static SaishuKusanagi m6568(long j, YashiroNanakase yashiroNanakase, String str) {
        long j2;
        String str2;
        String str3;
        int length = str.length();
        int r3 = GoroDaimon.m18411(str, 0, length, ';');
        int r5 = GoroDaimon.m18411(str, 0, r3, '=');
        if (r5 == r3) {
            return null;
        }
        String r4 = GoroDaimon.m18408(str, 0, r5);
        if (r4.isEmpty() || GoroDaimon.m18403(r4) != -1) {
            return null;
        }
        String r52 = GoroDaimon.m18408(str, r5 + 1, r3);
        if (GoroDaimon.m18403(r52) != -1) {
            return null;
        }
        long j3 = 253402300799999L;
        long j4 = -1;
        String str4 = null;
        String str5 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = true;
        boolean z4 = false;
        int i = r3 + 1;
        while (i < length) {
            int r9 = GoroDaimon.m18411(str, i, length, ';');
            int r6 = GoroDaimon.m18411(str, i, r9, '=');
            String r18 = GoroDaimon.m18408(str, i, r6);
            String r32 = r6 < r9 ? GoroDaimon.m18408(str, r6 + 1, r9) : "";
            if (r18.equalsIgnoreCase("expires")) {
                try {
                    j3 = m6567(r32, 0, r32.length());
                    z4 = true;
                    str3 = str4;
                } catch (IllegalArgumentException e) {
                    str3 = str4;
                }
            } else if (r18.equalsIgnoreCase("max-age")) {
                try {
                    j4 = m6566(r32);
                    z4 = true;
                    str3 = str4;
                } catch (NumberFormatException e2) {
                    str3 = str4;
                }
            } else if (r18.equalsIgnoreCase("domain")) {
                try {
                    z3 = false;
                    str3 = m6564(r32);
                } catch (IllegalArgumentException e3) {
                    str3 = str4;
                }
            } else if (r18.equalsIgnoreCase("path")) {
                str5 = r32;
                str3 = str4;
            } else if (r18.equalsIgnoreCase("secure")) {
                z = true;
                str3 = str4;
            } else if (r18.equalsIgnoreCase("httponly")) {
                z2 = true;
                str3 = str4;
            } else {
                str3 = str4;
            }
            i = r9 + 1;
            str4 = str3;
        }
        if (j4 == Long.MIN_VALUE) {
            j2 = Long.MIN_VALUE;
        } else if (j4 != -1) {
            j2 = (j4 <= 9223372036854775L ? 1000 * j4 : Long.MAX_VALUE) + j;
            if (j2 < j || j2 > 253402300799999L) {
                j2 = 253402300799999L;
            }
        } else {
            j2 = j3;
        }
        String r33 = yashiroNanakase.m6608();
        if (str4 == null) {
            str4 = r33;
        } else if (!m6571(r33, str4)) {
            return null;
        }
        if (r33.length() != str4.length() && KyoKusanagi.m18481().m18484(str4) == null) {
            return null;
        }
        if (str5 == null || !str5.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
            String r2 = yashiroNanakase.m6610();
            int lastIndexOf = r2.lastIndexOf(47);
            str2 = lastIndexOf != 0 ? r2.substring(0, lastIndexOf) : InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
        } else {
            str2 = str5;
        }
        return new SaishuKusanagi(r4, r52, j2, str4, str2, z, z2, z3, z4);
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public static SaishuKusanagi m6569(YashiroNanakase yashiroNanakase, String str) {
        return m6568(System.currentTimeMillis(), yashiroNanakase, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<SaishuKusanagi> m6570(YashiroNanakase yashiroNanakase, ShingoYabuki shingoYabuki) {
        List<String> r3 = shingoYabuki.m6580("Set-Cookie");
        ArrayList arrayList = null;
        int size = r3.size();
        for (int i = 0; i < size; i++) {
            SaishuKusanagi r5 = m6569(yashiroNanakase, r3.get(i));
            if (r5 != null) {
                ArrayList arrayList2 = arrayList == null ? new ArrayList() : arrayList;
                arrayList2.add(r5);
                arrayList = arrayList2;
            }
        }
        return arrayList != null ? Collections.unmodifiableList(arrayList) : Collections.emptyList();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m6571(String str, String str2) {
        if (str.equals(str2)) {
            return true;
        }
        return str.endsWith(str2) && str.charAt((str.length() - str2.length()) + -1) == '.' && !GoroDaimon.m18409(str);
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof SaishuKusanagi)) {
            return false;
        }
        SaishuKusanagi saishuKusanagi = (SaishuKusanagi) obj;
        return saishuKusanagi.f6103.equals(this.f6103) && saishuKusanagi.f6095.equals(this.f6095) && saishuKusanagi.f6097.equals(this.f6097) && saishuKusanagi.f6100.equals(this.f6100) && saishuKusanagi.f6096 == this.f6096 && saishuKusanagi.f6101 == this.f6101 && saishuKusanagi.f6102 == this.f6102 && saishuKusanagi.f6099 == this.f6099 && saishuKusanagi.f6098 == this.f6098;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.f6099 ? 0 : 1) + (((this.f6102 ? 0 : 1) + (((this.f6101 ? 0 : 1) + ((((((((((this.f6103.hashCode() + 527) * 31) + this.f6095.hashCode()) * 31) + this.f6097.hashCode()) * 31) + this.f6100.hashCode()) * 31) + ((int) (this.f6096 ^ (this.f6096 >>> 32)))) * 31)) * 31)) * 31)) * 31;
        if (!this.f6098) {
            i = 1;
        }
        return hashCode + i;
    }

    public String toString() {
        return m6574(false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m6572() {
        return this.f6095;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6573() {
        return this.f6103;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m6574(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f6103);
        sb.append('=');
        sb.append(this.f6095);
        if (this.f6099) {
            if (this.f6096 == Long.MIN_VALUE) {
                sb.append("; max-age=0");
            } else {
                sb.append("; expires=").append(ChinGentsai.m18548(new Date(this.f6096)));
            }
        }
        if (!this.f6098) {
            sb.append("; domain=");
            if (z) {
                sb.append(".");
            }
            sb.append(this.f6097);
        }
        sb.append("; path=").append(this.f6100);
        if (this.f6101) {
            sb.append("; secure");
        }
        if (this.f6102) {
            sb.append("; httponly");
        }
        return sb.toString();
    }
}
