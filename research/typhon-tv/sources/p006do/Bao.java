package p006do;

import java.io.IOException;
import java.nio.charset.Charset;
import javax.annotation.Nullable;
import p006do.p007do.GoroDaimon;
import p009if.ChinGentsai;

/* renamed from: do.Bao  reason: invalid package */
public abstract class Bao {
    public static Bao a(@Nullable Chris chris, String str) {
        Charset charset = GoroDaimon.f14739;
        if (chris != null && (charset = chris.m6442()) == null) {
            charset = GoroDaimon.f14739;
            chris = Chris.m6441(chris + "; charset=utf-8");
        }
        return a(chris, str.getBytes(charset));
    }

    public static Bao a(@Nullable Chris chris, byte[] bArr) {
        return a(chris, bArr, 0, bArr.length);
    }

    public static Bao a(@Nullable final Chris chris, final byte[] bArr, final int i, final int i2) {
        if (bArr == null) {
            throw new NullPointerException("content == null");
        }
        GoroDaimon.m18421((long) bArr.length, (long) i, (long) i2);
        return new Bao() {
            @Nullable
            public Chris a() {
                return Chris.this;
            }

            public void a(ChinGentsai chinGentsai) throws IOException {
                chinGentsai.m18936(bArr, i, i2);
            }

            public long b() {
                return (long) i2;
            }
        };
    }

    @Nullable
    public abstract Chris a();

    public abstract void a(ChinGentsai chinGentsai) throws IOException;

    public long b() throws IOException {
        return -1;
    }
}
