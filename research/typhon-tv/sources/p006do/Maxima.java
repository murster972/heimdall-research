package p006do;

import android.support.v4.app.NotificationCompat;
import java.io.IOException;
import java.util.ArrayList;
import p006do.ChizuruKagura;
import p006do.p007do.BenimaruNikaido;
import p006do.p007do.p017byte.ChangKoehan;
import p006do.p007do.p020for.GoroDaimon;
import p006do.p007do.p020for.HeavyD;
import p006do.p007do.p020for.RugalBernstein;

/* renamed from: do.Maxima  reason: invalid package */
final class Maxima implements ChangKoehan {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f14702;

    /* renamed from: 连任  reason: contains not printable characters */
    final boolean f14703;

    /* renamed from: 靐  reason: contains not printable characters */
    final RugalBernstein f14704;

    /* renamed from: 麤  reason: contains not printable characters */
    final Whip f14705;

    /* renamed from: 齉  reason: contains not printable characters */
    final ChizuruKagura f14706;

    /* renamed from: 龘  reason: contains not printable characters */
    final Orochi f14707;

    /* renamed from: do.Maxima$KyoKusanagi */
    final class KyoKusanagi extends BenimaruNikaido {

        /* renamed from: 齉  reason: contains not printable characters */
        private final ChoiBounge f14708;

        /* renamed from: 龘  reason: contains not printable characters */
        final /* synthetic */ Maxima f14709;

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m18364() {
            boolean z = true;
            try {
                JhunHoon r0 = this.f14709.m18358();
                if (this.f14709.f14704.m18579()) {
                    try {
                        this.f14708.m18343((ChangKoehan) this.f14709, new IOException("Canceled"));
                    } catch (IOException e) {
                        e = e;
                    }
                } else {
                    this.f14708.m18342((ChangKoehan) this.f14709, r0);
                }
                this.f14709.f14707.m6534().m6520(this);
                return;
            } catch (IOException e2) {
                e = e2;
                z = false;
            }
            if (z) {
                try {
                    ChangKoehan.m18443().m18451(4, "Callback failure for " + this.f14709.m18361(), (Throwable) e);
                } catch (Throwable th) {
                    this.f14709.f14707.m6534().m6520(this);
                    throw th;
                }
            } else {
                this.f14708.m18343((ChangKoehan) this.f14709, e);
            }
            this.f14709.f14707.m6534().m6520(this);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m18365() {
            return this.f14709.f14705.m6592().m6608();
        }
    }

    Maxima(Orochi orochi, Whip whip, boolean z) {
        ChizuruKagura.KyoKusanagi r0 = orochi.m6538();
        this.f14707 = orochi;
        this.f14705 = whip;
        this.f14703 = z;
        this.f14704 = new RugalBernstein(orochi, z);
        this.f14706 = r0.m18341(this);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m18357() {
        this.f14704.m18578(ChangKoehan.m18443().m18447("response.body().close()"));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public JhunHoon m18358() throws IOException {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.f14707.m6535());
        arrayList.add(this.f14704);
        arrayList.add(new p006do.p007do.p020for.KyoKusanagi(this.f14707.m6524()));
        arrayList.add(new p006do.p007do.p008do.KyoKusanagi(this.f14707.m6525()));
        arrayList.add(new p006do.p007do.p021if.KyoKusanagi(this.f14707));
        if (!this.f14703) {
            arrayList.addAll(this.f14707.m6537());
        }
        arrayList.add(new p006do.p007do.p020for.BenimaruNikaido(this.f14703));
        return new HeavyD(arrayList, (p006do.p007do.p021if.HeavyD) null, (GoroDaimon) null, (p006do.p007do.p021if.GoroDaimon) null, 0, this.f14705).m6576(this.f14705);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public String m18359() {
        return this.f14705.m6592().m6611();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m18360() {
        return this.f14704.m18579();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m18361() {
        return (m18360() ? "canceled " : "") + (this.f14703 ? "web socket" : NotificationCompat.CATEGORY_CALL) + " to " + m18359();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Maxima clone() {
        return new Maxima(this.f14707, this.f14705, this.f14703);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public JhunHoon m18363() throws IOException {
        synchronized (this) {
            if (this.f14702) {
                throw new IllegalStateException("Already Executed");
            }
            this.f14702 = true;
        }
        m18357();
        try {
            this.f14707.m6534().m6521(this);
            JhunHoon r0 = m18358();
            if (r0 != null) {
                return r0;
            }
            throw new IOException("Canceled");
        } finally {
            this.f14707.m6534().m6518(this);
        }
    }
}
