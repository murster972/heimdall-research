package p006do;

import com.mopub.common.TyphoonApp;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import p006do.YashiroNanakase;
import p006do.p007do.GoroDaimon;

/* renamed from: do.KyoKusanagi  reason: invalid package */
public final class KyoKusanagi {

    /* renamed from: ʻ  reason: contains not printable characters */
    final List<IoriYagami> f6009;

    /* renamed from: ʼ  reason: contains not printable characters */
    final ProxySelector f6010;
    @Nullable

    /* renamed from: ʽ  reason: contains not printable characters */
    final Proxy f6011;
    @Nullable

    /* renamed from: ˑ  reason: contains not printable characters */
    final SSLSocketFactory f6012;
    @Nullable

    /* renamed from: ٴ  reason: contains not printable characters */
    final HostnameVerifier f6013;
    @Nullable

    /* renamed from: ᐧ  reason: contains not printable characters */
    final HeavyD f6014;

    /* renamed from: 连任  reason: contains not printable characters */
    final List<KDash> f6015;

    /* renamed from: 靐  reason: contains not printable characters */
    final Vice f6016;

    /* renamed from: 麤  reason: contains not printable characters */
    final BenimaruNikaido f6017;

    /* renamed from: 齉  reason: contains not printable characters */
    final SocketFactory f6018;

    /* renamed from: 龘  reason: contains not printable characters */
    final YashiroNanakase f6019;

    public KyoKusanagi(String str, int i, Vice vice, SocketFactory socketFactory, @Nullable SSLSocketFactory sSLSocketFactory, @Nullable HostnameVerifier hostnameVerifier, @Nullable HeavyD heavyD, BenimaruNikaido benimaruNikaido, @Nullable Proxy proxy, List<KDash> list, List<IoriYagami> list2, ProxySelector proxySelector) {
        this.f6019 = new YashiroNanakase.KyoKusanagi().m6646(sSLSocketFactory != null ? TyphoonApp.HTTPS : TyphoonApp.HTTP).m6640(str).m6645(i).m6642();
        if (vice == null) {
            throw new NullPointerException("dns == null");
        }
        this.f6016 = vice;
        if (socketFactory == null) {
            throw new NullPointerException("socketFactory == null");
        }
        this.f6018 = socketFactory;
        if (benimaruNikaido == null) {
            throw new NullPointerException("proxyAuthenticator == null");
        }
        this.f6017 = benimaruNikaido;
        if (list == null) {
            throw new NullPointerException("protocols == null");
        }
        this.f6015 = GoroDaimon.m18418(list);
        if (list2 == null) {
            throw new NullPointerException("connectionSpecs == null");
        }
        this.f6009 = GoroDaimon.m18418(list2);
        if (proxySelector == null) {
            throw new NullPointerException("proxySelector == null");
        }
        this.f6010 = proxySelector;
        this.f6011 = proxy;
        this.f6012 = sSLSocketFactory;
        this.f6013 = hostnameVerifier;
        this.f6014 = heavyD;
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof KyoKusanagi) && this.f6019.equals(((KyoKusanagi) obj).f6019) && m6513((KyoKusanagi) obj);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.f6013 != null ? this.f6013.hashCode() : 0) + (((this.f6012 != null ? this.f6012.hashCode() : 0) + (((this.f6011 != null ? this.f6011.hashCode() : 0) + ((((((((((((this.f6019.hashCode() + 527) * 31) + this.f6016.hashCode()) * 31) + this.f6017.hashCode()) * 31) + this.f6015.hashCode()) * 31) + this.f6009.hashCode()) * 31) + this.f6010.hashCode()) * 31)) * 31)) * 31)) * 31;
        if (this.f6014 != null) {
            i = this.f6014.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder append = new StringBuilder().append("Address{").append(this.f6019.m6608()).append(":").append(this.f6019.m6609());
        if (this.f6011 != null) {
            append.append(", proxy=").append(this.f6011);
        } else {
            append.append(", proxySelector=").append(this.f6010);
        }
        append.append("}");
        return append.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public List<IoriYagami> m6502() {
        return this.f6009;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public ProxySelector m6503() {
        return this.f6010;
    }

    @Nullable
    /* renamed from: ʽ  reason: contains not printable characters */
    public Proxy m6504() {
        return this.f6011;
    }

    @Nullable
    /* renamed from: ˑ  reason: contains not printable characters */
    public SSLSocketFactory m6505() {
        return this.f6012;
    }

    @Nullable
    /* renamed from: ٴ  reason: contains not printable characters */
    public HostnameVerifier m6506() {
        return this.f6013;
    }

    @Nullable
    /* renamed from: ᐧ  reason: contains not printable characters */
    public HeavyD m6507() {
        return this.f6014;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public List<KDash> m6508() {
        return this.f6015;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Vice m6509() {
        return this.f6016;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public BenimaruNikaido m6510() {
        return this.f6017;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public SocketFactory m6511() {
        return this.f6018;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public YashiroNanakase m6512() {
        return this.f6019;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6513(KyoKusanagi kyoKusanagi) {
        return this.f6016.equals(kyoKusanagi.f6016) && this.f6017.equals(kyoKusanagi.f6017) && this.f6015.equals(kyoKusanagi.f6015) && this.f6009.equals(kyoKusanagi.f6009) && this.f6010.equals(kyoKusanagi.f6010) && GoroDaimon.m18426((Object) this.f6011, (Object) kyoKusanagi.f6011) && GoroDaimon.m18426((Object) this.f6012, (Object) kyoKusanagi.f6012) && GoroDaimon.m18426((Object) this.f6013, (Object) kyoKusanagi.f6013) && GoroDaimon.m18426((Object) this.f6014, (Object) kyoKusanagi.f6014) && m6512().m6609() == kyoKusanagi.m6512().m6609();
    }
}
