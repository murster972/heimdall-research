package p006do;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;

/* renamed from: do.Chris  reason: invalid package */
public final class Chris {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Pattern f5960 = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Pattern f5961 = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");
    @Nullable

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f5962;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f5963;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f5964;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f5965;

    private Chris(String str, String str2, String str3, @Nullable String str4) {
        this.f5965 = str;
        this.f5964 = str2;
        this.f5963 = str3;
        this.f5962 = str4;
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public static Chris m6441(String str) {
        Matcher matcher = f5961.matcher(str);
        if (!matcher.lookingAt()) {
            return null;
        }
        String lowerCase = matcher.group(1).toLowerCase(Locale.US);
        String lowerCase2 = matcher.group(2).toLowerCase(Locale.US);
        Matcher matcher2 = f5960.matcher(str);
        String str2 = null;
        for (int end = matcher.end(); end < str.length(); end = matcher2.end()) {
            matcher2.region(end, str.length());
            if (!matcher2.lookingAt()) {
                return null;
            }
            String group = matcher2.group(1);
            if (group != null && group.equalsIgnoreCase("charset")) {
                String group2 = matcher2.group(2);
                if (group2 == null) {
                    group2 = matcher2.group(3);
                } else if (group2.startsWith("'") && group2.endsWith("'") && group2.length() > 2) {
                    group2 = group2.substring(1, group2.length() - 1);
                }
                if (str2 != null && !group2.equalsIgnoreCase(str2)) {
                    return null;
                }
                str2 = group2;
            }
        }
        return new Chris(str, lowerCase, lowerCase2, str2);
    }

    public boolean equals(@Nullable Object obj) {
        return (obj instanceof Chris) && ((Chris) obj).f5965.equals(this.f5965);
    }

    public int hashCode() {
        return this.f5965.hashCode();
    }

    public String toString() {
        return this.f5965;
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public Charset m6442() {
        return m6443((Charset) null);
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public Charset m6443(@Nullable Charset charset) {
        try {
            return this.f5962 != null ? Charset.forName(this.f5962) : charset;
        } catch (IllegalArgumentException e) {
            return charset;
        }
    }
}
