package p006do;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import p006do.p007do.p008do.ChangKoehan;
import p006do.p007do.p008do.ChinGentsai;

/* renamed from: do.GoroDaimon  reason: invalid package */
public final class GoroDaimon implements Closeable, Flushable {

    /* renamed from: 靐  reason: contains not printable characters */
    final ChinGentsai f14578;

    /* renamed from: 龘  reason: contains not printable characters */
    final ChangKoehan f14579;

    public void close() throws IOException {
        this.f14578.close();
    }

    public void flush() throws IOException {
        this.f14578.flush();
    }
}
