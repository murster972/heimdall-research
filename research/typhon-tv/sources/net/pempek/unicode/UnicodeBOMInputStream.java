package net.pempek.unicode;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

public class UnicodeBOMInputStream extends InputStream {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BOM f15869;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f15870;

    /* renamed from: 龘  reason: contains not printable characters */
    private final PushbackInputStream f15871;

    public static final class BOM {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final BOM f15872 = new BOM(new byte[]{0, 0, -2, -1}, "UTF-32 big-endian");

        /* renamed from: 连任  reason: contains not printable characters */
        public static final BOM f15873 = new BOM(new byte[]{-1, -2, 0, 0}, "UTF-32 little-endian");

        /* renamed from: 靐  reason: contains not printable characters */
        public static final BOM f15874 = new BOM(new byte[]{-17, -69, -65}, "UTF-8");

        /* renamed from: 麤  reason: contains not printable characters */
        public static final BOM f15875 = new BOM(new byte[]{-2, -1}, "UTF-16 big-endian");

        /* renamed from: 齉  reason: contains not printable characters */
        public static final BOM f15876 = new BOM(new byte[]{-1, -2}, "UTF-16 little-endian");

        /* renamed from: 龘  reason: contains not printable characters */
        public static final BOM f15877 = new BOM(new byte[0], "NONE");

        /* renamed from: ʼ  reason: contains not printable characters */
        final byte[] f15878;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final String f15879;

        private BOM(byte[] bArr, String str) {
            if (bArr == null) {
                throw new RuntimeException("invalid BOM: null is not allowed");
            }
            this.f15878 = bArr;
            this.f15879 = str;
        }

        public final String toString() {
            return this.f15879;
        }
    }

    public UnicodeBOMInputStream(InputStream inputStream) throws NullPointerException, IOException {
        this(inputStream, true);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public UnicodeBOMInputStream(java.io.InputStream r10, boolean r11) throws java.lang.NullPointerException, java.io.IOException {
        /*
            r9 = this;
            r8 = 2
            r7 = -1
            r6 = -2
            r5 = 1
            r4 = 0
            r9.<init>()
            r9.f15870 = r4
            if (r10 != 0) goto L_0x0015
            java.lang.NullPointerException r2 = new java.lang.NullPointerException
            java.lang.String r3 = "invalid input stream: null is not allowed"
            r2.<init>(r3)
            throw r2
        L_0x0015:
            java.io.PushbackInputStream r2 = new java.io.PushbackInputStream
            r3 = 4
            r2.<init>(r10, r3)
            r9.f15871 = r2
            r2 = 4
            byte[] r0 = new byte[r2]
            java.io.PushbackInputStream r2 = r9.f15871
            int r1 = r2.read(r0)
            switch(r1) {
                case 2: goto L_0x0085;
                case 3: goto L_0x006e;
                case 4: goto L_0x0042;
                default: goto L_0x0029;
            }
        L_0x0029:
            net.pempek.unicode.UnicodeBOMInputStream$BOM r2 = net.pempek.unicode.UnicodeBOMInputStream.BOM.f15877
            r9.f15869 = r2
        L_0x002d:
            if (r1 <= 0) goto L_0x0034
            java.io.PushbackInputStream r2 = r9.f15871
            r2.unread(r0, r4, r1)
        L_0x0034:
            if (r11 == 0) goto L_0x0041
            net.pempek.unicode.UnicodeBOMInputStream$BOM r2 = r9.m19868()
            net.pempek.unicode.UnicodeBOMInputStream$BOM r3 = net.pempek.unicode.UnicodeBOMInputStream.BOM.f15877
            if (r2 == r3) goto L_0x0041
            r9.m19867()
        L_0x0041:
            return
        L_0x0042:
            byte r2 = r0[r4]
            if (r2 != r7) goto L_0x0058
            byte r2 = r0[r5]
            if (r2 != r6) goto L_0x0058
            byte r2 = r0[r8]
            if (r2 != 0) goto L_0x0058
            r2 = 3
            byte r2 = r0[r2]
            if (r2 != 0) goto L_0x0058
            net.pempek.unicode.UnicodeBOMInputStream$BOM r2 = net.pempek.unicode.UnicodeBOMInputStream.BOM.f15873
            r9.f15869 = r2
            goto L_0x002d
        L_0x0058:
            byte r2 = r0[r4]
            if (r2 != 0) goto L_0x006e
            byte r2 = r0[r5]
            if (r2 != 0) goto L_0x006e
            byte r2 = r0[r8]
            if (r2 != r6) goto L_0x006e
            r2 = 3
            byte r2 = r0[r2]
            if (r2 != r7) goto L_0x006e
            net.pempek.unicode.UnicodeBOMInputStream$BOM r2 = net.pempek.unicode.UnicodeBOMInputStream.BOM.f15872
            r9.f15869 = r2
            goto L_0x002d
        L_0x006e:
            byte r2 = r0[r4]
            r3 = -17
            if (r2 != r3) goto L_0x0085
            byte r2 = r0[r5]
            r3 = -69
            if (r2 != r3) goto L_0x0085
            byte r2 = r0[r8]
            r3 = -65
            if (r2 != r3) goto L_0x0085
            net.pempek.unicode.UnicodeBOMInputStream$BOM r2 = net.pempek.unicode.UnicodeBOMInputStream.BOM.f15874
            r9.f15869 = r2
            goto L_0x002d
        L_0x0085:
            byte r2 = r0[r4]
            if (r2 != r7) goto L_0x0092
            byte r2 = r0[r5]
            if (r2 != r6) goto L_0x0092
            net.pempek.unicode.UnicodeBOMInputStream$BOM r2 = net.pempek.unicode.UnicodeBOMInputStream.BOM.f15876
            r9.f15869 = r2
            goto L_0x002d
        L_0x0092:
            byte r2 = r0[r4]
            if (r2 != r6) goto L_0x0029
            byte r2 = r0[r5]
            if (r2 != r7) goto L_0x0029
            net.pempek.unicode.UnicodeBOMInputStream$BOM r2 = net.pempek.unicode.UnicodeBOMInputStream.BOM.f15875
            r9.f15869 = r2
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: net.pempek.unicode.UnicodeBOMInputStream.<init>(java.io.InputStream, boolean):void");
    }

    public int available() throws IOException {
        return this.f15871.available();
    }

    public void close() throws IOException {
        this.f15871.close();
    }

    public synchronized void mark(int i) {
        this.f15871.mark(i);
    }

    public boolean markSupported() {
        return this.f15871.markSupported();
    }

    public int read() throws IOException {
        return this.f15871.read();
    }

    public int read(byte[] bArr) throws IOException, NullPointerException {
        return this.f15871.read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) throws IOException, NullPointerException {
        return this.f15871.read(bArr, i, i2);
    }

    public synchronized void reset() throws IOException {
        this.f15871.reset();
    }

    public long skip(long j) throws IOException {
        return this.f15871.skip(j);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized UnicodeBOMInputStream m19867() throws IOException {
        if (!this.f15870) {
            this.f15871.skip((long) this.f15869.f15878.length);
            this.f15870 = true;
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final BOM m19868() {
        return this.f15869;
    }
}
