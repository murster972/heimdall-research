package net.lingala.zip4j.unzip;

import java.io.File;
import java.util.ArrayList;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.UnzipParameters;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.progress.ProgressMonitor;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.lingala.zip4j.util.Zip4jUtil;

public class Unzip {
    private ZipModel zipModel;

    public Unzip(ZipModel zipModel2) throws ZipException {
        if (zipModel2 == null) {
            throw new ZipException("ZipModel is null");
        }
        this.zipModel = zipModel2;
    }

    private long calculateTotalWork(ArrayList arrayList) throws ZipException {
        if (arrayList == null) {
            throw new ZipException("fileHeaders is null, cannot calculate total work");
        }
        long j = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            FileHeader fileHeader = (FileHeader) arrayList.get(i);
            j += (fileHeader.getZip64ExtendedInfo() == null || fileHeader.getZip64ExtendedInfo().getUnCompressedSize() <= 0) ? fileHeader.getCompressedSize() : fileHeader.getZip64ExtendedInfo().getCompressedSize();
        }
        return j;
    }

    private void checkOutputDirectoryStructure(FileHeader fileHeader, String str, String str2) throws ZipException {
        if (fileHeader == null || !Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("Cannot check output directory structure...one of the parameters was null");
        }
        String fileName = fileHeader.getFileName();
        if (Zip4jUtil.isStringNotNullAndNotEmpty(str2)) {
            fileName = str2;
        }
        if (Zip4jUtil.isStringNotNullAndNotEmpty(fileName)) {
            try {
                File file = new File(new File(str + fileName).getParent());
                if (!file.exists()) {
                    file.mkdirs();
                }
            } catch (Exception e) {
                throw new ZipException((Throwable) e);
            }
        }
    }

    /* access modifiers changed from: private */
    public void initExtractAll(ArrayList arrayList, UnzipParameters unzipParameters, ProgressMonitor progressMonitor, String str) throws ZipException {
        for (int i = 0; i < arrayList.size(); i++) {
            initExtractFile((FileHeader) arrayList.get(i), str, unzipParameters, (String) null, progressMonitor);
            if (progressMonitor.isCancelAllTasks()) {
                progressMonitor.setResult(3);
                progressMonitor.setState(0);
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0068, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0069, code lost:
        r12.endProgressMonitorError(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006c, code lost:
        throw r1;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0068 A[ExcHandler: ZipException (r1v1 'e' net.lingala.zip4j.exception.ZipException A[CUSTOM_DECLARE]), Splitter:B:3:0x000b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initExtractFile(net.lingala.zip4j.model.FileHeader r8, java.lang.String r9, net.lingala.zip4j.model.UnzipParameters r10, java.lang.String r11, net.lingala.zip4j.progress.ProgressMonitor r12) throws net.lingala.zip4j.exception.ZipException {
        /*
            r7 = this;
            if (r8 != 0) goto L_0x000b
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r6 = "fileHeader is null"
            r5.<init>((java.lang.String) r6)
            throw r5
        L_0x000b:
            java.lang.String r5 = r8.getFileName()     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            r12.setFileName(r5)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            java.lang.String r5 = net.lingala.zip4j.util.InternalZipTyphoonApp.FILE_SEPARATOR     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            boolean r5 = r9.endsWith(r5)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            if (r5 != 0) goto L_0x002d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            r5.<init>()     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            java.lang.String r6 = net.lingala.zip4j.util.InternalZipTyphoonApp.FILE_SEPARATOR     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            java.lang.String r9 = r5.toString()     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
        L_0x002d:
            boolean r5 = r8.isDirectory()     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            if (r5 == 0) goto L_0x006d
            java.lang.String r3 = r8.getFileName()     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            boolean r5 = net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(r3)     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            if (r5 != 0) goto L_0x003e
        L_0x003d:
            return
        L_0x003e:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            r5.<init>()     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            boolean r5 = r2.exists()     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            if (r5 != 0) goto L_0x003d
            r2.mkdirs()     // Catch:{ Exception -> 0x005e, ZipException -> 0x0068 }
            goto L_0x003d
        L_0x005e:
            r1 = move-exception
            r12.endProgressMonitorError(r1)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            r5.<init>((java.lang.Throwable) r1)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            throw r5     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
        L_0x0068:
            r1 = move-exception
            r12.endProgressMonitorError(r1)
            throw r1
        L_0x006d:
            r7.checkOutputDirectoryStructure(r8, r9, r11)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            net.lingala.zip4j.unzip.UnzipEngine r4 = new net.lingala.zip4j.unzip.UnzipEngine     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            net.lingala.zip4j.model.ZipModel r5 = r7.zipModel     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            r4.<init>(r5, r8)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            r4.unzipFile(r12, r9, r11, r10)     // Catch:{ Exception -> 0x007b, ZipException -> 0x0068 }
            goto L_0x003d
        L_0x007b:
            r1 = move-exception
            r12.endProgressMonitorError(r1)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            r5.<init>((java.lang.Throwable) r1)     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
            throw r5     // Catch:{ ZipException -> 0x0068, Exception -> 0x0085 }
        L_0x0085:
            r1 = move-exception
            r12.endProgressMonitorError(r1)
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException
            r5.<init>((java.lang.Throwable) r1)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.unzip.Unzip.initExtractFile(net.lingala.zip4j.model.FileHeader, java.lang.String, net.lingala.zip4j.model.UnzipParameters, java.lang.String, net.lingala.zip4j.progress.ProgressMonitor):void");
    }

    public void extractAll(UnzipParameters unzipParameters, String str, ProgressMonitor progressMonitor, boolean z) throws ZipException {
        CentralDirectory centralDirectory = this.zipModel.getCentralDirectory();
        if (centralDirectory == null || centralDirectory.getFileHeaders() == null) {
            throw new ZipException("invalid central directory in zipModel");
        }
        final ArrayList fileHeaders = centralDirectory.getFileHeaders();
        progressMonitor.setCurrentOperation(1);
        progressMonitor.setTotalWork(calculateTotalWork(fileHeaders));
        progressMonitor.setState(1);
        if (z) {
            final UnzipParameters unzipParameters2 = unzipParameters;
            final ProgressMonitor progressMonitor2 = progressMonitor;
            final String str2 = str;
            new Thread(InternalZipTyphoonApp.THREAD_NAME) {
                public void run() {
                    try {
                        Unzip.this.initExtractAll(fileHeaders, unzipParameters2, progressMonitor2, str2);
                        progressMonitor2.endProgressMonitorSuccess();
                    } catch (ZipException e) {
                    }
                }
            }.start();
            return;
        }
        initExtractAll(fileHeaders, unzipParameters, progressMonitor, str);
    }

    public void extractFile(FileHeader fileHeader, String str, UnzipParameters unzipParameters, String str2, ProgressMonitor progressMonitor, boolean z) throws ZipException {
        if (fileHeader == null) {
            throw new ZipException("fileHeader is null");
        }
        progressMonitor.setCurrentOperation(1);
        progressMonitor.setTotalWork(fileHeader.getCompressedSize());
        progressMonitor.setState(1);
        progressMonitor.setPercentDone(0);
        progressMonitor.setFileName(fileHeader.getFileName());
        if (z) {
            final FileHeader fileHeader2 = fileHeader;
            final String str3 = str;
            final UnzipParameters unzipParameters2 = unzipParameters;
            final String str4 = str2;
            final ProgressMonitor progressMonitor2 = progressMonitor;
            new Thread(InternalZipTyphoonApp.THREAD_NAME) {
                public void run() {
                    try {
                        Unzip.this.initExtractFile(fileHeader2, str3, unzipParameters2, str4, progressMonitor2);
                        progressMonitor2.endProgressMonitorSuccess();
                    } catch (ZipException e) {
                    }
                }
            }.start();
            return;
        }
        initExtractFile(fileHeader, str, unzipParameters, str2, progressMonitor);
        progressMonitor.endProgressMonitorSuccess();
    }

    public ZipInputStream getInputStream(FileHeader fileHeader) throws ZipException {
        return new UnzipEngine(this.zipModel, fileHeader).getInputStream();
    }
}
