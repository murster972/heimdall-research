package net.lingala.zip4j.unzip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.zip.CRC32;
import net.lingala.zip4j.core.HeaderReader;
import net.lingala.zip4j.crypto.AESDecrypter;
import net.lingala.zip4j.crypto.IDecrypter;
import net.lingala.zip4j.crypto.StandardDecrypter;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.InflaterInputStream;
import net.lingala.zip4j.io.PartInputStream;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.AESExtraDataRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.LocalFileHeader;
import net.lingala.zip4j.model.UnzipParameters;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.progress.ProgressMonitor;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.lingala.zip4j.util.Raw;
import net.lingala.zip4j.util.Zip4jUtil;

public class UnzipEngine {
    private CRC32 crc;
    private int currSplitFileCounter = 0;
    private IDecrypter decrypter;
    private FileHeader fileHeader;
    private LocalFileHeader localFileHeader;
    private ZipModel zipModel;

    public UnzipEngine(ZipModel zipModel2, FileHeader fileHeader2) throws ZipException {
        if (zipModel2 == null || fileHeader2 == null) {
            throw new ZipException("Invalid parameters passed to StoreUnzip. One or more of the parameters were null");
        }
        this.zipModel = zipModel2;
        this.fileHeader = fileHeader2;
        this.crc = new CRC32();
    }

    private int calculateAESSaltLength(AESExtraDataRecord aESExtraDataRecord) throws ZipException {
        if (aESExtraDataRecord == null) {
            throw new ZipException("unable to determine salt length: AESExtraDataRecord is null");
        }
        switch (aESExtraDataRecord.getAesStrength()) {
            case 1:
                return 8;
            case 2:
                return 12;
            case 3:
                return 16;
            default:
                throw new ZipException("unable to determine salt length: invalid aes key strength");
        }
    }

    private boolean checkLocalHeader() throws ZipException {
        boolean z;
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = checkSplitFile();
            if (randomAccessFile == null) {
                randomAccessFile = new RandomAccessFile(new File(this.zipModel.getZipFile()), InternalZipTyphoonApp.READ_MODE);
            }
            this.localFileHeader = new HeaderReader(randomAccessFile).readLocalFileHeader(this.fileHeader);
            if (this.localFileHeader == null) {
                throw new ZipException("error reading local file header. Is this a valid zip file?");
            }
            if (this.localFileHeader.getCompressionMethod() != this.fileHeader.getCompressionMethod()) {
                z = false;
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (IOException | Exception e) {
                    }
                }
            } else {
                z = true;
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (IOException | Exception e2) {
                    }
                }
            }
            return z;
        } catch (FileNotFoundException e3) {
            throw new ZipException((Throwable) e3);
        } catch (Throwable th) {
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (IOException | Exception e4) {
                }
            }
            throw th;
        }
    }

    private RandomAccessFile checkSplitFile() throws ZipException {
        if (!this.zipModel.isSplitArchive()) {
            return null;
        }
        int diskNumberStart = this.fileHeader.getDiskNumberStart();
        this.currSplitFileCounter = diskNumberStart + 1;
        String zipFile = this.zipModel.getZipFile();
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(diskNumberStart == this.zipModel.getEndCentralDirRecord().getNoOfThisDisk() ? this.zipModel.getZipFile() : diskNumberStart >= 9 ? zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z" + (diskNumberStart + 1) : zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z0" + (diskNumberStart + 1), InternalZipTyphoonApp.READ_MODE);
            if (this.currSplitFileCounter != 1) {
                return randomAccessFile;
            }
            byte[] bArr = new byte[4];
            randomAccessFile.read(bArr);
            if (((long) Raw.readIntLittleEndian(bArr, 0)) == 134695760) {
                return randomAccessFile;
            }
            throw new ZipException("invalid first part split file signature");
        } catch (FileNotFoundException e) {
            throw new ZipException((Throwable) e);
        } catch (IOException e2) {
            throw new ZipException((Throwable) e2);
        }
    }

    private void closeStreams(InputStream inputStream, OutputStream outputStream) throws ZipException {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                if (e != null) {
                    if (Zip4jUtil.isStringNotNullAndNotEmpty(e.getMessage()) && e.getMessage().indexOf(" - Wrong Password?") >= 0) {
                        throw new ZipException(e.getMessage());
                    }
                }
                if (outputStream != null) {
                    try {
                        outputStream.close();
                        return;
                    } catch (IOException e2) {
                        return;
                    }
                } else {
                    return;
                }
            } catch (Throwable th) {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e3) {
                    }
                }
                throw th;
            }
        }
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException e4) {
            }
        }
    }

    private RandomAccessFile createFileHandler(String str) throws ZipException {
        if (this.zipModel == null || !Zip4jUtil.isStringNotNullAndNotEmpty(this.zipModel.getZipFile())) {
            throw new ZipException("input parameter is null in getFilePointer");
        }
        try {
            return this.zipModel.isSplitArchive() ? checkSplitFile() : new RandomAccessFile(new File(this.zipModel.getZipFile()), str);
        } catch (FileNotFoundException e) {
            throw new ZipException((Throwable) e);
        } catch (Exception e2) {
            throw new ZipException((Throwable) e2);
        }
    }

    private byte[] getAESPasswordVerifier(RandomAccessFile randomAccessFile) throws ZipException {
        try {
            byte[] bArr = new byte[2];
            randomAccessFile.read(bArr);
            return bArr;
        } catch (IOException e) {
            throw new ZipException((Throwable) e);
        }
    }

    private byte[] getAESSalt(RandomAccessFile randomAccessFile) throws ZipException {
        if (this.localFileHeader.getAesExtraDataRecord() == null) {
            return null;
        }
        try {
            byte[] bArr = new byte[calculateAESSaltLength(this.localFileHeader.getAesExtraDataRecord())];
            randomAccessFile.seek(this.localFileHeader.getOffsetStartOfData());
            randomAccessFile.read(bArr);
            return bArr;
        } catch (IOException e) {
            throw new ZipException((Throwable) e);
        }
    }

    private String getOutputFileNameWithPath(String str, String str2) throws ZipException {
        return str + System.getProperty("file.separator") + (Zip4jUtil.isStringNotNullAndNotEmpty(str2) ? str2 : this.fileHeader.getFileName());
    }

    private FileOutputStream getOutputStream(String str, String str2) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("invalid output path");
        }
        try {
            File file = new File(getOutputFileNameWithPath(str, str2));
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            if (file.exists()) {
                file.delete();
            }
            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            throw new ZipException((Throwable) e);
        }
    }

    private byte[] getStandardDecrypterHeaderBytes(RandomAccessFile randomAccessFile) throws ZipException {
        try {
            byte[] bArr = new byte[12];
            randomAccessFile.seek(this.localFileHeader.getOffsetStartOfData());
            randomAccessFile.read(bArr, 0, 12);
            return bArr;
        } catch (IOException e) {
            throw new ZipException((Throwable) e);
        } catch (Exception e2) {
            throw new ZipException((Throwable) e2);
        }
    }

    private void init(RandomAccessFile randomAccessFile) throws ZipException {
        if (this.localFileHeader == null) {
            throw new ZipException("local file header is null, cannot initialize input stream");
        }
        try {
            initDecrypter(randomAccessFile);
        } catch (ZipException e) {
            throw e;
        } catch (Exception e2) {
            throw new ZipException((Throwable) e2);
        }
    }

    private void initDecrypter(RandomAccessFile randomAccessFile) throws ZipException {
        if (this.localFileHeader == null) {
            throw new ZipException("local file header is null, cannot init decrypter");
        } else if (!this.localFileHeader.isEncrypted()) {
        } else {
            if (this.localFileHeader.getEncryptionMethod() == 0) {
                this.decrypter = new StandardDecrypter(this.fileHeader, getStandardDecrypterHeaderBytes(randomAccessFile));
            } else if (this.localFileHeader.getEncryptionMethod() == 99) {
                this.decrypter = new AESDecrypter(this.localFileHeader, getAESSalt(randomAccessFile), getAESPasswordVerifier(randomAccessFile));
            } else {
                throw new ZipException("unsupported encryption method");
            }
        }
    }

    public void checkCRC() throws ZipException {
        if (this.fileHeader == null) {
            return;
        }
        if (this.fileHeader.getEncryptionMethod() == 99) {
            if (this.decrypter != null && (this.decrypter instanceof AESDecrypter)) {
                byte[] calculatedAuthenticationBytes = ((AESDecrypter) this.decrypter).getCalculatedAuthenticationBytes();
                byte[] storedMac = ((AESDecrypter) this.decrypter).getStoredMac();
                byte[] bArr = new byte[10];
                if (bArr == null || storedMac == null) {
                    throw new ZipException("CRC (MAC) check failed for " + this.fileHeader.getFileName());
                }
                System.arraycopy(calculatedAuthenticationBytes, 0, bArr, 0, 10);
                if (!Arrays.equals(bArr, storedMac)) {
                    throw new ZipException("invalid CRC (MAC) for file: " + this.fileHeader.getFileName());
                }
            }
        } else if ((this.crc.getValue() & InternalZipTyphoonApp.ZIP_64_LIMIT) != this.fileHeader.getCrc32()) {
            String str = "invalid CRC for file: " + this.fileHeader.getFileName();
            if (this.localFileHeader.isEncrypted() && this.localFileHeader.getEncryptionMethod() == 0) {
                str = str + " - Wrong Password?";
            }
            throw new ZipException(str);
        }
    }

    public IDecrypter getDecrypter() {
        return this.decrypter;
    }

    public FileHeader getFileHeader() {
        return this.fileHeader;
    }

    public ZipInputStream getInputStream() throws ZipException {
        if (this.fileHeader == null) {
            throw new ZipException("file header is null, cannot get inputstream");
        }
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = createFileHandler(InternalZipTyphoonApp.READ_MODE);
            if (!checkLocalHeader()) {
                throw new ZipException("local header and file header do not match");
            }
            init(randomAccessFile);
            long compressedSize = this.localFileHeader.getCompressedSize();
            long offsetStartOfData = this.localFileHeader.getOffsetStartOfData();
            if (this.localFileHeader.isEncrypted()) {
                if (this.localFileHeader.getEncryptionMethod() == 99) {
                    if (this.decrypter instanceof AESDecrypter) {
                        compressedSize -= (long) ((((AESDecrypter) this.decrypter).getPasswordVerifierLength() + ((AESDecrypter) this.decrypter).getSaltLength()) + 10);
                        offsetStartOfData += (long) (((AESDecrypter) this.decrypter).getPasswordVerifierLength() + ((AESDecrypter) this.decrypter).getSaltLength());
                    } else {
                        throw new ZipException("invalid decryptor when trying to calculate compressed size for AES encrypted file: " + this.fileHeader.getFileName());
                    }
                } else if (this.localFileHeader.getEncryptionMethod() == 0) {
                    compressedSize -= 12;
                    offsetStartOfData += 12;
                }
            }
            int compressionMethod = this.fileHeader.getCompressionMethod();
            if (this.fileHeader.getEncryptionMethod() == 99) {
                if (this.fileHeader.getAesExtraDataRecord() != null) {
                    compressionMethod = this.fileHeader.getAesExtraDataRecord().getCompressionMethod();
                } else {
                    throw new ZipException("AESExtraDataRecord does not exist for AES encrypted file: " + this.fileHeader.getFileName());
                }
            }
            randomAccessFile.seek(offsetStartOfData);
            switch (compressionMethod) {
                case 0:
                    return new ZipInputStream(new PartInputStream(randomAccessFile, offsetStartOfData, compressedSize, this));
                case 8:
                    return new ZipInputStream(new InflaterInputStream(randomAccessFile, offsetStartOfData, compressedSize, this));
                default:
                    throw new ZipException("compression type not supported");
            }
        } catch (ZipException e) {
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (IOException e2) {
                }
            }
            throw e;
        } catch (Exception e3) {
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (IOException e4) {
                }
            }
            throw new ZipException((Throwable) e3);
        }
    }

    public LocalFileHeader getLocalFileHeader() {
        return this.localFileHeader;
    }

    public ZipModel getZipModel() {
        return this.zipModel;
    }

    public RandomAccessFile startNextSplitFile() throws IOException, FileNotFoundException {
        String zipFile = this.zipModel.getZipFile();
        String zipFile2 = this.currSplitFileCounter == this.zipModel.getEndCentralDirRecord().getNoOfThisDisk() ? this.zipModel.getZipFile() : this.currSplitFileCounter >= 9 ? zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z" + (this.currSplitFileCounter + 1) : zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z0" + (this.currSplitFileCounter + 1);
        this.currSplitFileCounter++;
        try {
            if (Zip4jUtil.checkFileExists(zipFile2)) {
                return new RandomAccessFile(zipFile2, InternalZipTyphoonApp.READ_MODE);
            }
            throw new IOException("zip split file does not exist: " + zipFile2);
        } catch (ZipException e) {
            throw new IOException(e.getMessage());
        }
    }

    public void unzipFile(ProgressMonitor progressMonitor, String str, String str2, UnzipParameters unzipParameters) throws ZipException {
        if (this.zipModel == null || this.fileHeader == null || !Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("Invalid parameters passed during unzipping file. One or more of the parameters were null");
        }
        ZipInputStream zipInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            byte[] bArr = new byte[4096];
            zipInputStream = getInputStream();
            fileOutputStream = getOutputStream(str, str2);
            do {
                int read = zipInputStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                    progressMonitor.updateWorkCompleted((long) read);
                } else {
                    closeStreams(zipInputStream, fileOutputStream);
                    UnzipUtil.applyFileAttributes(this.fileHeader, new File(getOutputFileNameWithPath(str, str2)), unzipParameters);
                    closeStreams(zipInputStream, fileOutputStream);
                    return;
                }
            } while (!progressMonitor.isCancelAllTasks());
            progressMonitor.setResult(3);
            progressMonitor.setState(0);
            closeStreams(zipInputStream, fileOutputStream);
        } catch (IOException e) {
            throw new ZipException((Throwable) e);
        } catch (Exception e2) {
            throw new ZipException((Throwable) e2);
        } catch (Throwable th) {
            closeStreams(zipInputStream, fileOutputStream);
            throw th;
        }
    }

    public void updateCRC(int i) {
        this.crc.update(i);
    }

    public void updateCRC(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            this.crc.update(bArr, i, i2);
        }
    }
}
