package net.lingala.zip4j.crypto;

import net.lingala.zip4j.crypto.engine.ZipCryptoEngine;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

public class StandardDecrypter implements IDecrypter {
    private byte[] crc = new byte[4];
    private FileHeader fileHeader;
    private ZipCryptoEngine zipCryptoEngine;

    public StandardDecrypter(FileHeader fileHeader2, byte[] bArr) throws ZipException {
        if (fileHeader2 == null) {
            throw new ZipException("one of more of the input parameters were null in StandardDecryptor");
        }
        this.fileHeader = fileHeader2;
        this.zipCryptoEngine = new ZipCryptoEngine();
        init(bArr);
    }

    public int decryptData(byte[] bArr) throws ZipException {
        return decryptData(bArr, 0, bArr.length);
    }

    public int decryptData(byte[] bArr, int i, int i2) throws ZipException {
        if (i < 0 || i2 < 0) {
            throw new ZipException("one of the input parameters were null in standard decrpyt data");
        }
        int i3 = i;
        while (i3 < i + i2) {
            try {
                byte decryptByte = (this.zipCryptoEngine.decryptByte() ^ (bArr[i3] & 255)) & 255;
                this.zipCryptoEngine.updateKeys((byte) decryptByte);
                bArr[i3] = (byte) decryptByte;
                i3++;
            } catch (Exception e) {
                throw new ZipException((Throwable) e);
            }
        }
        return i2;
    }

    public void init(byte[] bArr) throws ZipException {
        byte[] crcBuff = this.fileHeader.getCrcBuff();
        this.crc[3] = (byte) (crcBuff[3] & 255);
        this.crc[2] = (byte) ((crcBuff[3] >> 8) & 255);
        this.crc[1] = (byte) ((crcBuff[3] >> 16) & 255);
        this.crc[0] = (byte) ((crcBuff[3] >> 24) & 255);
        if (this.crc[2] > 0 || this.crc[1] > 0 || this.crc[0] > 0) {
            throw new IllegalStateException("Invalid CRC in File Header");
        } else if (this.fileHeader.getPassword() == null || this.fileHeader.getPassword().length <= 0) {
            throw new ZipException("Wrong password!", 5);
        } else {
            this.zipCryptoEngine.initKeys(this.fileHeader.getPassword());
            try {
                byte b = bArr[0];
                for (int i = 0; i < 12; i++) {
                    this.zipCryptoEngine.updateKeys((byte) (this.zipCryptoEngine.decryptByte() ^ b));
                    if (i + 1 != 12) {
                        b = bArr[i + 1];
                    }
                }
            } catch (Exception e) {
                throw new ZipException((Throwable) e);
            }
        }
    }
}
