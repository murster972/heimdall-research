package net.lingala.zip4j.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipModel;

public class Zip4jUtil {
    public static boolean checkArrayListTypes(ArrayList arrayList, int i) throws ZipException {
        if (arrayList == null) {
            throw new ZipException("input arraylist is null, cannot check types");
        } else if (arrayList.size() <= 0) {
            return true;
        } else {
            boolean z = false;
            switch (i) {
                case 1:
                    int i2 = 0;
                    while (true) {
                        if (i2 >= arrayList.size()) {
                            break;
                        } else if (!(arrayList.get(i2) instanceof File)) {
                            z = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                case 2:
                    int i3 = 0;
                    while (true) {
                        if (i3 >= arrayList.size()) {
                            break;
                        } else if (!(arrayList.get(i3) instanceof String)) {
                            z = true;
                            break;
                        } else {
                            i3++;
                        }
                    }
            }
            return !z;
        }
    }

    public static boolean checkFileExists(File file) throws ZipException {
        if (file != null) {
            return file.exists();
        }
        throw new ZipException("cannot check if file exists: input file is null");
    }

    public static boolean checkFileExists(String str) throws ZipException {
        if (isStringNotNullAndNotEmpty(str)) {
            return checkFileExists(new File(str));
        }
        throw new ZipException("path is null");
    }

    public static boolean checkFileReadAccess(String str) throws ZipException {
        if (!isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("path is null");
        } else if (!checkFileExists(str)) {
            throw new ZipException("file does not exist: " + str);
        } else {
            try {
                return new File(str).canRead();
            } catch (Exception e) {
                throw new ZipException("cannot read zip file");
            }
        }
    }

    public static boolean checkFileWriteAccess(String str) throws ZipException {
        if (!isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("path is null");
        } else if (!checkFileExists(str)) {
            throw new ZipException("file does not exist: " + str);
        } else {
            try {
                return new File(str).canWrite();
            } catch (Exception e) {
                throw new ZipException("cannot read zip file");
            }
        }
    }

    public static boolean checkOutputFolder(String str) throws ZipException {
        if (!isStringNotNullAndNotEmpty(str)) {
            throw new ZipException((Throwable) new NullPointerException("output path is null"));
        }
        File file = new File(str);
        if (!file.exists()) {
            try {
                file.mkdirs();
                if (!file.isDirectory()) {
                    throw new ZipException("output folder is not valid");
                } else if (file.canWrite()) {
                    return true;
                } else {
                    throw new ZipException("no write access to destination folder");
                }
            } catch (Exception e) {
                throw new ZipException("Cannot create destination folder");
            }
        } else if (!file.isDirectory()) {
            throw new ZipException("output folder is not valid");
        } else if (file.canWrite()) {
            return true;
        } else {
            throw new ZipException("no write access to output folder");
        }
    }

    public static byte[] convertCharset(String str) throws ZipException {
        try {
            String detectCharSet = detectCharSet(str);
            return detectCharSet.equals(InternalZipTyphoonApp.CHARSET_CP850) ? str.getBytes(InternalZipTyphoonApp.CHARSET_CP850) : detectCharSet.equals(InternalZipTyphoonApp.CHARSET_UTF8) ? str.getBytes(InternalZipTyphoonApp.CHARSET_UTF8) : str.getBytes();
        } catch (UnsupportedEncodingException e) {
            return str.getBytes();
        } catch (Exception e2) {
            throw new ZipException((Throwable) e2);
        }
    }

    public static String decodeFileName(byte[] bArr, boolean z) {
        if (!z) {
            return getCp850EncodedString(bArr);
        }
        try {
            return new String(bArr, InternalZipTyphoonApp.CHARSET_UTF8);
        } catch (UnsupportedEncodingException e) {
            return new String(bArr);
        }
    }

    public static String detectCharSet(String str) throws ZipException {
        if (str == null) {
            throw new ZipException("input string is null, cannot detect charset");
        }
        try {
            return str.equals(new String(str.getBytes(InternalZipTyphoonApp.CHARSET_CP850), InternalZipTyphoonApp.CHARSET_CP850)) ? InternalZipTyphoonApp.CHARSET_CP850 : str.equals(new String(str.getBytes(InternalZipTyphoonApp.CHARSET_UTF8), InternalZipTyphoonApp.CHARSET_UTF8)) ? InternalZipTyphoonApp.CHARSET_UTF8 : InternalZipTyphoonApp.CHARSET_DEFAULT;
        } catch (UnsupportedEncodingException e) {
            return InternalZipTyphoonApp.CHARSET_DEFAULT;
        } catch (Exception e2) {
            return InternalZipTyphoonApp.CHARSET_DEFAULT;
        }
    }

    public static long dosToJavaTme(int i) {
        int i2 = (i >> 11) & 31;
        int i3 = (i >> 16) & 31;
        int i4 = ((i >> 21) & 15) - 1;
        Calendar instance = Calendar.getInstance();
        instance.set(((i >> 25) & 127) + 1980, i4, i3, i2, (i >> 5) & 63, (i & 31) * 2);
        instance.set(14, 0);
        return instance.getTime().getTime();
    }

    public static String getAbsoluteFilePath(String str) throws ZipException {
        if (isStringNotNullAndNotEmpty(str)) {
            return new File(str).getAbsolutePath();
        }
        throw new ZipException("filePath is null or empty, cannot get absolute file path");
    }

    public static long[] getAllHeaderSignatures() {
        return new long[]{67324752, 134695760, 33639248, 101010256, 84233040, 134630224, 134695760, 117853008, 101075792, 1, 39169};
    }

    public static String getCp850EncodedString(byte[] bArr) {
        try {
            return new String(bArr, InternalZipTyphoonApp.CHARSET_CP850);
        } catch (UnsupportedEncodingException e) {
            return new String(bArr);
        }
    }

    public static int getEncodedStringLength(String str) throws ZipException {
        if (isStringNotNullAndNotEmpty(str)) {
            return getEncodedStringLength(str, detectCharSet(str));
        }
        throw new ZipException("input string is null, cannot calculate encoded String length");
    }

    public static int getEncodedStringLength(String str, String str2) throws ZipException {
        ByteBuffer wrap;
        if (!isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("input string is null, cannot calculate encoded String length");
        } else if (!isStringNotNullAndNotEmpty(str2)) {
            throw new ZipException("encoding is not defined, cannot calculate string length");
        } else {
            try {
                wrap = str2.equals(InternalZipTyphoonApp.CHARSET_CP850) ? ByteBuffer.wrap(str.getBytes(InternalZipTyphoonApp.CHARSET_CP850)) : str2.equals(InternalZipTyphoonApp.CHARSET_UTF8) ? ByteBuffer.wrap(str.getBytes(InternalZipTyphoonApp.CHARSET_UTF8)) : ByteBuffer.wrap(str.getBytes(str2));
            } catch (UnsupportedEncodingException e) {
                wrap = ByteBuffer.wrap(str.getBytes());
            } catch (Exception e2) {
                throw new ZipException((Throwable) e2);
            }
            return wrap.limit();
        }
    }

    public static FileHeader getFileHeader(ZipModel zipModel, String str) throws ZipException {
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot determine file header for fileName: " + str);
        } else if (!isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("file name is null, cannot determine file header for fileName: " + str);
        } else {
            FileHeader fileHeaderWithExactMatch = getFileHeaderWithExactMatch(zipModel, str);
            if (fileHeaderWithExactMatch != null) {
                return fileHeaderWithExactMatch;
            }
            String replaceAll = str.replaceAll("\\\\", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
            FileHeader fileHeaderWithExactMatch2 = getFileHeaderWithExactMatch(zipModel, replaceAll);
            return fileHeaderWithExactMatch2 == null ? getFileHeaderWithExactMatch(zipModel, replaceAll.replaceAll(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR, "\\\\")) : fileHeaderWithExactMatch2;
        }
    }

    public static FileHeader getFileHeaderWithExactMatch(ZipModel zipModel, String str) throws ZipException {
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot determine file header with exact match for fileName: " + str);
        } else if (!isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("file name is null, cannot determine file header with exact match for fileName: " + str);
        } else if (zipModel.getCentralDirectory() == null) {
            throw new ZipException("central directory is null, cannot determine file header with exact match for fileName: " + str);
        } else if (zipModel.getCentralDirectory().getFileHeaders() == null) {
            throw new ZipException("file Headers are null, cannot determine file header with exact match for fileName: " + str);
        } else if (zipModel.getCentralDirectory().getFileHeaders().size() <= 0) {
            return null;
        } else {
            ArrayList fileHeaders = zipModel.getCentralDirectory().getFileHeaders();
            for (int i = 0; i < fileHeaders.size(); i++) {
                FileHeader fileHeader = (FileHeader) fileHeaders.get(i);
                String fileName = fileHeader.getFileName();
                if (isStringNotNullAndNotEmpty(fileName) && str.equalsIgnoreCase(fileName)) {
                    return fileHeader;
                }
            }
            return null;
        }
    }

    public static long getFileLengh(File file) throws ZipException {
        if (file == null) {
            throw new ZipException("input file is null, cannot calculate file length");
        } else if (file.isDirectory()) {
            return -1;
        } else {
            return file.length();
        }
    }

    public static long getFileLengh(String str) throws ZipException {
        if (isStringNotNullAndNotEmpty(str)) {
            return getFileLengh(new File(str));
        }
        throw new ZipException("invalid file name");
    }

    public static String getFileNameFromFilePath(File file) throws ZipException {
        if (file == null) {
            throw new ZipException("input file is null, cannot get file name");
        } else if (file.isDirectory()) {
            return null;
        } else {
            return file.getName();
        }
    }

    public static ArrayList getFilesInDirectoryRec(File file, boolean z) throws ZipException {
        if (file == null) {
            throw new ZipException("input path is null, cannot read files in the directory");
        }
        ArrayList arrayList = new ArrayList();
        List asList = Arrays.asList(file.listFiles());
        if (file.canRead()) {
            for (int i = 0; i < asList.size(); i++) {
                File file2 = (File) asList.get(i);
                if (file2.isHidden() && !z) {
                    break;
                }
                arrayList.add(file2);
                if (file2.isDirectory()) {
                    arrayList.addAll(getFilesInDirectoryRec(file2, z));
                }
            }
        }
        return arrayList;
    }

    public static int getIndexOfFileHeader(ZipModel zipModel, FileHeader fileHeader) throws ZipException {
        if (zipModel == null || fileHeader == null) {
            throw new ZipException("input parameters is null, cannot determine index of file header");
        } else if (zipModel.getCentralDirectory() == null) {
            throw new ZipException("central directory is null, ccannot determine index of file header");
        } else if (zipModel.getCentralDirectory().getFileHeaders() == null) {
            throw new ZipException("file Headers are null, cannot determine index of file header");
        } else if (zipModel.getCentralDirectory().getFileHeaders().size() <= 0) {
            return -1;
        } else {
            String fileName = fileHeader.getFileName();
            if (!isStringNotNullAndNotEmpty(fileName)) {
                throw new ZipException("file name in file header is empty or null, cannot determine index of file header");
            }
            ArrayList fileHeaders = zipModel.getCentralDirectory().getFileHeaders();
            for (int i = 0; i < fileHeaders.size(); i++) {
                String fileName2 = ((FileHeader) fileHeaders.get(i)).getFileName();
                if (isStringNotNullAndNotEmpty(fileName2) && fileName.equalsIgnoreCase(fileName2)) {
                    return i;
                }
            }
            return -1;
        }
    }

    public static long getLastModifiedFileTime(File file, TimeZone timeZone) throws ZipException {
        if (file == null) {
            throw new ZipException("input file is null, cannot read last modified file time");
        } else if (file.exists()) {
            return file.lastModified();
        } else {
            throw new ZipException("input file does not exist, cannot read last modified file time");
        }
    }

    public static String getRelativeFileName(String str, String str2, String str3) throws ZipException {
        String str4;
        if (!isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("input file path/name is empty, cannot calculate relative file name");
        }
        if (isStringNotNullAndNotEmpty(str3)) {
            String path = new File(str3).getPath();
            if (!path.endsWith(InternalZipTyphoonApp.FILE_SEPARATOR)) {
                path = path + InternalZipTyphoonApp.FILE_SEPARATOR;
            }
            String substring = str.substring(path.length());
            if (substring.startsWith(System.getProperty("file.separator"))) {
                substring = substring.substring(1);
            }
            File file = new File(str);
            str4 = file.isDirectory() ? substring.replaceAll("\\\\", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR : substring.substring(0, substring.lastIndexOf(file.getName())).replaceAll("\\\\", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) + file.getName();
        } else {
            File file2 = new File(str);
            str4 = file2.isDirectory() ? file2.getName() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR : getFileNameFromFilePath(new File(str));
        }
        if (isStringNotNullAndNotEmpty(str2)) {
            str4 = str2 + str4;
        }
        if (isStringNotNullAndNotEmpty(str4)) {
            return str4;
        }
        throw new ZipException("Error determining file name");
    }

    public static ArrayList getSplitZipFiles(ZipModel zipModel) throws ZipException {
        if (zipModel == null) {
            throw new ZipException("cannot get split zip files: zipmodel is null");
        } else if (zipModel.getEndCentralDirRecord() == null) {
            return null;
        } else {
            ArrayList arrayList = new ArrayList();
            String zipFile = zipModel.getZipFile();
            String name = new File(zipFile).getName();
            if (!isStringNotNullAndNotEmpty(zipFile)) {
                throw new ZipException("cannot get split zip files: zipfile is null");
            } else if (!zipModel.isSplitArchive()) {
                arrayList.add(zipFile);
                return arrayList;
            } else {
                int noOfThisDisk = zipModel.getEndCentralDirRecord().getNoOfThisDisk();
                if (noOfThisDisk == 0) {
                    arrayList.add(zipFile);
                    return arrayList;
                }
                for (int i = 0; i <= noOfThisDisk; i++) {
                    if (i == noOfThisDisk) {
                        arrayList.add(zipModel.getZipFile());
                    } else {
                        String str = ".z0";
                        if (i > 9) {
                            str = ".z";
                        }
                        arrayList.add((name.indexOf(".") >= 0 ? zipFile.substring(0, zipFile.lastIndexOf(".")) : zipFile) + str + (i + 1));
                    }
                }
                return arrayList;
            }
        }
    }

    public static String getZipFileNameWithoutExt(String str) throws ZipException {
        if (!isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("zip file name is empty or null, cannot determine zip file name");
        }
        String str2 = str;
        if (str.indexOf(System.getProperty("file.separator")) >= 0) {
            str2 = str.substring(str.lastIndexOf(System.getProperty("file.separator")));
        }
        return str2.indexOf(".") > 0 ? str2.substring(0, str2.lastIndexOf(".")) : str2;
    }

    public static boolean isStringNotNullAndNotEmpty(String str) {
        return str != null && str.trim().length() > 0;
    }

    public static boolean isSupportedCharset(String str) throws ZipException {
        if (!isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("charset is null or empty, cannot check if it is supported");
        }
        try {
            new String("a".getBytes(), str);
            return true;
        } catch (UnsupportedEncodingException e) {
            return false;
        } catch (Exception e2) {
            throw new ZipException((Throwable) e2);
        }
    }

    public static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().indexOf("win") >= 0;
    }

    public static long javaToDosTime(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        int i = instance.get(1);
        if (i < 1980) {
            return 2162688;
        }
        return (long) (((i - 1980) << 25) | ((instance.get(2) + 1) << 21) | (instance.get(5) << 16) | (instance.get(11) << 11) | (instance.get(12) << 5) | (instance.get(13) >> 1));
    }

    public static void setFileArchive(File file) throws ZipException {
    }

    public static void setFileHidden(File file) throws ZipException {
    }

    public static void setFileReadOnly(File file) throws ZipException {
        if (file == null) {
            throw new ZipException("input file is null. cannot set read only file attribute");
        } else if (file.exists()) {
            file.setReadOnly();
        }
    }

    public static void setFileSystemMode(File file) throws ZipException {
    }
}
