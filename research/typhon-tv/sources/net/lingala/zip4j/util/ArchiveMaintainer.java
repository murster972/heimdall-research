package net.lingala.zip4j.util;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.progress.ProgressMonitor;

public class ArchiveMaintainer {
    private long calculateTotalWorkForMergeOp(ZipModel zipModel) throws ZipException {
        long j = 0;
        if (zipModel.isSplitArchive()) {
            int noOfThisDisk = zipModel.getEndCentralDirRecord().getNoOfThisDisk();
            String zipFile = zipModel.getZipFile();
            for (int i = 0; i <= noOfThisDisk; i++) {
                j += Zip4jUtil.getFileLengh(new File(0 == zipModel.getEndCentralDirRecord().getNoOfThisDisk() ? zipModel.getZipFile() : 0 >= 9 ? zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z" + 1 : zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z0" + 1));
            }
        }
        return j;
    }

    private long calculateTotalWorkForRemoveOp(ZipModel zipModel, FileHeader fileHeader) throws ZipException {
        return Zip4jUtil.getFileLengh(new File(zipModel.getZipFile())) - fileHeader.getCompressedSize();
    }

    private void copyFile(RandomAccessFile randomAccessFile, OutputStream outputStream, long j, long j2, ProgressMonitor progressMonitor) throws ZipException {
        if (randomAccessFile == null || outputStream == null) {
            throw new ZipException("input or output stream is null, cannot copy file");
        } else if (j < 0) {
            throw new ZipException("starting offset is negative, cannot copy file");
        } else if (j2 < 0) {
            throw new ZipException("end offset is negative, cannot copy file");
        } else if (j > j2) {
            throw new ZipException("start offset is greater than end offset, cannot copy file");
        } else if (j != j2) {
            if (progressMonitor.isCancelAllTasks()) {
                progressMonitor.setResult(3);
                progressMonitor.setState(0);
                return;
            }
            try {
                randomAccessFile.seek(j);
                long j3 = 0;
                long j4 = j2 - j;
                byte[] bArr = j2 - j < PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM ? new byte[((int) (j2 - j))] : new byte[4096];
                while (true) {
                    int read = randomAccessFile.read(bArr);
                    if (read != -1) {
                        outputStream.write(bArr, 0, read);
                        progressMonitor.updateWorkCompleted((long) read);
                        if (progressMonitor.isCancelAllTasks()) {
                            progressMonitor.setResult(3);
                            return;
                        }
                        j3 += (long) read;
                        if (j3 == j4) {
                            return;
                        }
                        if (((long) bArr.length) + j3 > j4) {
                            bArr = new byte[((int) (j4 - j3))];
                        }
                    } else {
                        return;
                    }
                }
            } catch (IOException e) {
                throw new ZipException((Throwable) e);
            } catch (Exception e2) {
                throw new ZipException((Throwable) e2);
            }
        }
    }

    private RandomAccessFile createFileHandler(ZipModel zipModel, String str) throws ZipException {
        if (zipModel == null || !Zip4jUtil.isStringNotNullAndNotEmpty(zipModel.getZipFile())) {
            throw new ZipException("input parameter is null in getFilePointer, cannot create file handler to remove file");
        }
        try {
            return new RandomAccessFile(new File(zipModel.getZipFile()), str);
        } catch (FileNotFoundException e) {
            throw new ZipException((Throwable) e);
        }
    }

    private RandomAccessFile createSplitZipFileHandler(ZipModel zipModel, int i) throws ZipException {
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot create split file handler");
        } else if (i < 0) {
            throw new ZipException("invlaid part number, cannot create split file handler");
        } else {
            try {
                String zipFile = zipModel.getZipFile();
                String zipFile2 = i == zipModel.getEndCentralDirRecord().getNoOfThisDisk() ? zipModel.getZipFile() : i >= 9 ? zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z" + (i + 1) : zipFile.substring(0, zipFile.lastIndexOf(".")) + ".z0" + (i + 1);
                File file = new File(zipFile2);
                if (Zip4jUtil.checkFileExists(file)) {
                    return new RandomAccessFile(file, InternalZipTyphoonApp.READ_MODE);
                }
                throw new ZipException("split file does not exist: " + zipFile2);
            } catch (FileNotFoundException e) {
                throw new ZipException((Throwable) e);
            } catch (Exception e2) {
                throw new ZipException((Throwable) e2);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0141, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        r27.endProgressMonitorError(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x014c, code lost:
        throw new net.lingala.zip4j.exception.ZipException((java.lang.Throwable) r13);
     */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0141 A[ExcHandler: Exception (r13v0 'e' java.lang.Exception A[CUSTOM_DECLARE]), PHI: r6 r7 
      PHI: (r6v1 java.io.RandomAccessFile) = (r6v0 java.io.RandomAccessFile), (r6v0 java.io.RandomAccessFile), (r6v4 java.io.RandomAccessFile), (r6v4 java.io.RandomAccessFile), (r6v5 java.io.RandomAccessFile), (r6v5 java.io.RandomAccessFile), (r6v5 java.io.RandomAccessFile), (r6v5 java.io.RandomAccessFile) binds: [B:8:0x002f, B:27:0x005e, B:59:0x010d, B:60:?, B:54:0x0103, B:55:?, B:56:0x0106, B:57:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r7v1 java.io.OutputStream) = (r7v0 java.io.OutputStream), (r7v0 java.io.OutputStream), (r7v4 java.io.OutputStream), (r7v4 java.io.OutputStream), (r7v4 java.io.OutputStream), (r7v4 java.io.OutputStream), (r7v4 java.io.OutputStream), (r7v4 java.io.OutputStream) binds: [B:8:0x002f, B:27:0x005e, B:59:0x010d, B:60:?, B:54:0x0103, B:55:?, B:56:0x0106, B:57:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:8:0x002f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initMergeSplitZipFile(net.lingala.zip4j.model.ZipModel r25, java.io.File r26, net.lingala.zip4j.progress.ProgressMonitor r27) throws net.lingala.zip4j.exception.ZipException {
        /*
            r24 = this;
            if (r25 != 0) goto L_0x0010
            net.lingala.zip4j.exception.ZipException r13 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r5 = "one of the input parameters is null, cannot merge split zip file"
            r13.<init>((java.lang.String) r5)
            r0 = r27
            r0.endProgressMonitorError(r13)
            throw r13
        L_0x0010:
            boolean r5 = r25.isSplitArchive()
            if (r5 != 0) goto L_0x0024
            net.lingala.zip4j.exception.ZipException r13 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r5 = "archive not a split zip file"
            r13.<init>((java.lang.String) r5)
            r0 = r27
            r0.endProgressMonitorError(r13)
            throw r13
        L_0x0024:
            r7 = 0
            r6 = 0
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>()
            r22 = 0
            r19 = 0
            net.lingala.zip4j.model.EndCentralDirRecord r5 = r25.getEndCentralDirRecord()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            int r21 = r5.getNoOfThisDisk()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            if (r21 > 0) goto L_0x005a
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            java.lang.String r8 = "corrupt zip model, archive not a split zip file"
            r5.<init>((java.lang.String) r8)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            throw r5     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
        L_0x0042:
            r13 = move-exception
            r0 = r27
            r0.endProgressMonitorError(r13)     // Catch:{ all -> 0x004e }
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x004e }
            r5.<init>((java.lang.Throwable) r13)     // Catch:{ all -> 0x004e }
            throw r5     // Catch:{ all -> 0x004e }
        L_0x004e:
            r5 = move-exception
            if (r7 == 0) goto L_0x0054
            r7.close()     // Catch:{ IOException -> 0x0155 }
        L_0x0054:
            if (r6 == 0) goto L_0x0059
            r6.close()     // Catch:{ IOException -> 0x0158 }
        L_0x0059:
            throw r5
        L_0x005a:
            r0 = r24
            r1 = r26
            java.io.OutputStream r7 = r0.prepareOutputStreamForMerge(r1)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r17 = 0
        L_0x0064:
            r0 = r17
            r1 = r21
            if (r0 > r1) goto L_0x010d
            r0 = r24
            r1 = r25
            r2 = r17
            java.io.RandomAccessFile r6 = r0.createSplitZipFileHandler(r1, r2)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r20 = 0
            java.lang.Long r14 = new java.lang.Long     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            long r8 = r6.length()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r14.<init>(r8)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            if (r17 != 0) goto L_0x00bb
            net.lingala.zip4j.model.CentralDirectory r5 = r25.getCentralDirectory()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            if (r5 == 0) goto L_0x00bb
            net.lingala.zip4j.model.CentralDirectory r5 = r25.getCentralDirectory()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            if (r5 == 0) goto L_0x00bb
            net.lingala.zip4j.model.CentralDirectory r5 = r25.getCentralDirectory()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            int r5 = r5.size()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            if (r5 <= 0) goto L_0x00bb
            r5 = 4
            byte[] r4 = new byte[r5]     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r8 = 0
            r6.seek(r8)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r6.read(r4)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r5 = 0
            int r5 = net.lingala.zip4j.util.Raw.readIntLittleEndian(r4, r5)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            long r8 = (long) r5     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r10 = 134695760(0x8074b50, double:6.65485477E-316)
            int r5 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r5 != 0) goto L_0x00bb
            r20 = 4
            r19 = 1
        L_0x00bb:
            r0 = r17
            r1 = r21
            if (r0 != r1) goto L_0x00ce
            java.lang.Long r14 = new java.lang.Long     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            net.lingala.zip4j.model.EndCentralDirRecord r5 = r25.getEndCentralDirRecord()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            long r8 = r5.getOffsetOfStartOfCentralDir()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r14.<init>(r8)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
        L_0x00ce:
            r0 = r20
            long r8 = (long) r0     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            long r10 = r14.longValue()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r5 = r24
            r12 = r27
            r5.copyFile(r6, r7, r8, r10, r12)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            long r8 = r14.longValue()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r0 = r20
            long r10 = (long) r0     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            long r8 = r8 - r10
            long r22 = r22 + r8
            boolean r5 = r27.isCancelAllTasks()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            if (r5 == 0) goto L_0x0103
            r5 = 3
            r0 = r27
            r0.setResult(r5)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r5 = 0
            r0 = r27
            r0.setState(r5)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            if (r7 == 0) goto L_0x00fd
            r7.close()     // Catch:{ IOException -> 0x014d }
        L_0x00fd:
            if (r6 == 0) goto L_0x0102
            r6.close()     // Catch:{ IOException -> 0x014f }
        L_0x0102:
            return
        L_0x0103:
            r15.add(r14)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r6.close()     // Catch:{ IOException -> 0x0151, Exception -> 0x0141 }
        L_0x0109:
            int r17 = r17 + 1
            goto L_0x0064
        L_0x010d:
            java.lang.Object r18 = r25.clone()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            net.lingala.zip4j.model.ZipModel r18 = (net.lingala.zip4j.model.ZipModel) r18     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            net.lingala.zip4j.model.EndCentralDirRecord r5 = r18.getEndCentralDirRecord()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r0 = r22
            r5.setOffsetOfStartOfCentralDir(r0)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r0 = r24
            r1 = r18
            r2 = r19
            r0.updateSplitZipModel(r1, r15, r2)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            net.lingala.zip4j.core.HeaderWriter r16 = new net.lingala.zip4j.core.HeaderWriter     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r16.<init>()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r0 = r16
            r1 = r18
            r0.finalizeZipFileWithoutValidations(r1, r7)     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            r27.endProgressMonitorSuccess()     // Catch:{ IOException -> 0x0042, Exception -> 0x0141 }
            if (r7 == 0) goto L_0x0139
            r7.close()     // Catch:{ IOException -> 0x0153 }
        L_0x0139:
            if (r6 == 0) goto L_0x0102
            r6.close()     // Catch:{ IOException -> 0x013f }
            goto L_0x0102
        L_0x013f:
            r5 = move-exception
            goto L_0x0102
        L_0x0141:
            r13 = move-exception
            r0 = r27
            r0.endProgressMonitorError(r13)     // Catch:{ all -> 0x004e }
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x004e }
            r5.<init>((java.lang.Throwable) r13)     // Catch:{ all -> 0x004e }
            throw r5     // Catch:{ all -> 0x004e }
        L_0x014d:
            r5 = move-exception
            goto L_0x00fd
        L_0x014f:
            r5 = move-exception
            goto L_0x0102
        L_0x0151:
            r5 = move-exception
            goto L_0x0109
        L_0x0153:
            r5 = move-exception
            goto L_0x0139
        L_0x0155:
            r8 = move-exception
            goto L_0x0054
        L_0x0158:
            r8 = move-exception
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.util.ArchiveMaintainer.initMergeSplitZipFile(net.lingala.zip4j.model.ZipModel, java.io.File, net.lingala.zip4j.progress.ProgressMonitor):void");
    }

    private OutputStream prepareOutputStreamForMerge(File file) throws ZipException {
        if (file == null) {
            throw new ZipException("outFile is null, cannot create outputstream");
        }
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            throw new ZipException((Throwable) e);
        } catch (Exception e2) {
            throw new ZipException((Throwable) e2);
        }
    }

    private void restoreFileName(File file, String str) throws ZipException {
        if (!file.delete()) {
            throw new ZipException("cannot delete old zip file");
        } else if (!new File(str).renameTo(file)) {
            throw new ZipException("cannot rename modified zip file");
        }
    }

    private void updateSplitEndCentralDirectory(ZipModel zipModel) throws ZipException {
        if (zipModel == null) {
            try {
                throw new ZipException("zip model is null - cannot update end of central directory for split zip model");
            } catch (ZipException e) {
                throw e;
            } catch (Exception e2) {
                throw new ZipException((Throwable) e2);
            }
        } else if (zipModel.getCentralDirectory() == null) {
            throw new ZipException("corrupt zip model - getCentralDirectory, cannot update split zip model");
        } else {
            zipModel.getEndCentralDirRecord().setNoOfThisDisk(0);
            zipModel.getEndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(0);
            zipModel.getEndCentralDirRecord().setTotNoOfEntriesInCentralDir(zipModel.getCentralDirectory().getFileHeaders().size());
            zipModel.getEndCentralDirRecord().setTotNoOfEntriesInCentralDirOnThisDisk(zipModel.getCentralDirectory().getFileHeaders().size());
        }
    }

    private void updateSplitFileHeader(ZipModel zipModel, ArrayList arrayList, boolean z) throws ZipException {
        try {
            if (zipModel.getCentralDirectory() == null) {
                throw new ZipException("corrupt zip model - getCentralDirectory, cannot update split zip model");
            }
            int size = zipModel.getCentralDirectory().getFileHeaders().size();
            int i = 0;
            if (z) {
                i = 4;
            }
            for (int i2 = 0; i2 < size; i2++) {
                long j = 0;
                for (int i3 = 0; i3 < ((FileHeader) zipModel.getCentralDirectory().getFileHeaders().get(i2)).getDiskNumberStart(); i3++) {
                    j += ((Long) arrayList.get(i3)).longValue();
                }
                ((FileHeader) zipModel.getCentralDirectory().getFileHeaders().get(i2)).setOffsetLocalHeader((((FileHeader) zipModel.getCentralDirectory().getFileHeaders().get(i2)).getOffsetLocalHeader() + j) - ((long) i));
                ((FileHeader) zipModel.getCentralDirectory().getFileHeaders().get(i2)).setDiskNumberStart(0);
            }
        } catch (ZipException e) {
            throw e;
        } catch (Exception e2) {
            throw new ZipException((Throwable) e2);
        }
    }

    private void updateSplitZip64EndCentralDirLocator(ZipModel zipModel, ArrayList arrayList) throws ZipException {
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot update split Zip64 end of central directory locator");
        } else if (zipModel.getZip64EndCentralDirLocator() != null) {
            zipModel.getZip64EndCentralDirLocator().setNoOfDiskStartOfZip64EndOfCentralDirRec(0);
            long j = 0;
            for (int i = 0; i < arrayList.size(); i++) {
                j += ((Long) arrayList.get(i)).longValue();
            }
            zipModel.getZip64EndCentralDirLocator().setOffsetZip64EndOfCentralDirRec(zipModel.getZip64EndCentralDirLocator().getOffsetZip64EndOfCentralDirRec() + j);
            zipModel.getZip64EndCentralDirLocator().setTotNumberOfDiscs(1);
        }
    }

    private void updateSplitZip64EndCentralDirRec(ZipModel zipModel, ArrayList arrayList) throws ZipException {
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot update split Zip64 end of central directory record");
        } else if (zipModel.getZip64EndCentralDirRecord() != null) {
            zipModel.getZip64EndCentralDirRecord().setNoOfThisDisk(0);
            zipModel.getZip64EndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(0);
            zipModel.getZip64EndCentralDirRecord().setTotNoOfEntriesInCentralDirOnThisDisk((long) zipModel.getEndCentralDirRecord().getTotNoOfEntriesInCentralDir());
            long j = 0;
            for (int i = 0; i < arrayList.size(); i++) {
                j += ((Long) arrayList.get(i)).longValue();
            }
            zipModel.getZip64EndCentralDirRecord().setOffsetStartCenDirWRTStartDiskNo(zipModel.getZip64EndCentralDirRecord().getOffsetStartCenDirWRTStartDiskNo() + j);
        }
    }

    private void updateSplitZipModel(ZipModel zipModel, ArrayList arrayList, boolean z) throws ZipException {
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot update split zip model");
        }
        zipModel.setSplitArchive(false);
        updateSplitFileHeader(zipModel, arrayList, z);
        updateSplitEndCentralDirectory(zipModel);
        if (zipModel.isZip64Format()) {
            updateSplitZip64EndCentralDirLocator(zipModel, arrayList);
            updateSplitZip64EndCentralDirRec(zipModel, arrayList);
        }
    }

    public void initProgressMonitorForMergeOp(ZipModel zipModel, ProgressMonitor progressMonitor) throws ZipException {
        if (zipModel == null) {
            throw new ZipException("zip model is null, cannot calculate total work for merge op");
        }
        progressMonitor.setCurrentOperation(4);
        progressMonitor.setFileName(zipModel.getZipFile());
        progressMonitor.setTotalWork(calculateTotalWorkForMergeOp(zipModel));
        progressMonitor.setState(1);
    }

    public void initProgressMonitorForRemoveOp(ZipModel zipModel, FileHeader fileHeader, ProgressMonitor progressMonitor) throws ZipException {
        if (zipModel == null || fileHeader == null || progressMonitor == null) {
            throw new ZipException("one of the input parameters is null, cannot calculate total work");
        }
        progressMonitor.setCurrentOperation(2);
        progressMonitor.setFileName(fileHeader.getFileName());
        progressMonitor.setTotalWork(calculateTotalWorkForRemoveOp(zipModel, fileHeader));
        progressMonitor.setState(1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:140:0x035c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0036 A[SYNTHETIC, Splitter:B:18:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003b A[Catch:{ IOException -> 0x0352 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.HashMap initRemoveZipFile(net.lingala.zip4j.model.ZipModel r44, net.lingala.zip4j.model.FileHeader r45, net.lingala.zip4j.progress.ProgressMonitor r46) throws net.lingala.zip4j.exception.ZipException {
        /*
            r43 = this;
            if (r45 == 0) goto L_0x0004
            if (r44 != 0) goto L_0x000d
        L_0x0004:
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r8 = "input parameters is null in maintain zip file, cannot remove file from archive"
            r5.<init>((java.lang.String) r8)
            throw r5
        L_0x000d:
            r36 = 0
            r41 = 0
            r6 = 0
            r38 = 0
            r40 = 0
            java.util.HashMap r37 = new java.util.HashMap
            r37.<init>()
            int r28 = net.lingala.zip4j.util.Zip4jUtil.getIndexOfFileHeader(r44, r45)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            if (r28 >= 0) goto L_0x004a
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.String r8 = "file header not found in zip model, cannot remove file"
            r5.<init>((java.lang.String) r8)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            throw r5     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
        L_0x002a:
            r4 = move-exception
            r7 = r36
        L_0x002d:
            r0 = r46
            r0.endProgressMonitorError(r4)     // Catch:{ all -> 0x0033 }
            throw r4     // Catch:{ all -> 0x0033 }
        L_0x0033:
            r5 = move-exception
        L_0x0034:
            if (r6 == 0) goto L_0x0039
            r6.close()     // Catch:{ IOException -> 0x0352 }
        L_0x0039:
            if (r7 == 0) goto L_0x003e
            r7.close()     // Catch:{ IOException -> 0x0352 }
        L_0x003e:
            if (r38 == 0) goto L_0x035c
            r0 = r43
            r1 = r41
            r2 = r40
            r0.restoreFileName(r1, r2)
        L_0x0049:
            throw r5
        L_0x004a:
            boolean r5 = r44.isSplitArchive()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            if (r5 == 0) goto L_0x0067
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.String r8 = "This is a split archive. Zip file format does not allow updating split/spanned files"
            r5.<init>((java.lang.String) r8)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            throw r5     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
        L_0x0059:
            r4 = move-exception
            r7 = r36
        L_0x005c:
            r0 = r46
            r0.endProgressMonitorError(r4)     // Catch:{ all -> 0x0033 }
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x0033 }
            r5.<init>((java.lang.Throwable) r4)     // Catch:{ all -> 0x0033 }
            throw r5     // Catch:{ all -> 0x0033 }
        L_0x0067:
            long r22 = java.lang.System.currentTimeMillis()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            r5.<init>()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.String r8 = r44.getZipFile()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            r8 = 1000(0x3e8, double:4.94E-321)
            long r8 = r22 % r8
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.String r40 = r5.toString()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.io.File r39 = new java.io.File     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            r39.<init>(r40)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
        L_0x0089:
            boolean r5 = r39.exists()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            if (r5 == 0) goto L_0x00b2
            long r22 = java.lang.System.currentTimeMillis()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            r5.<init>()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.String r8 = r44.getZipFile()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            r8 = 1000(0x3e8, double:4.94E-321)
            long r8 = r22 % r8
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.lang.String r40 = r5.toString()     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            java.io.File r39 = new java.io.File     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            r39.<init>(r40)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            goto L_0x0089
        L_0x00b2:
            net.lingala.zip4j.io.SplitOutputStream r7 = new net.lingala.zip4j.io.SplitOutputStream     // Catch:{ FileNotFoundException -> 0x00f3 }
            java.io.File r5 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00f3 }
            r0 = r40
            r5.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00f3 }
            r7.<init>((java.io.File) r5)     // Catch:{ FileNotFoundException -> 0x00f3 }
            java.io.File r42 = new java.io.File     // Catch:{ ZipException -> 0x036d, Exception -> 0x036a }
            java.lang.String r5 = r44.getZipFile()     // Catch:{ ZipException -> 0x036d, Exception -> 0x036a }
            r0 = r42
            r0.<init>(r5)     // Catch:{ ZipException -> 0x036d, Exception -> 0x036a }
            java.lang.String r5 = "r"
            r0 = r43
            r1 = r44
            java.io.RandomAccessFile r6 = r0.createFileHandler(r1, r5)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.core.HeaderReader r25 = new net.lingala.zip4j.core.HeaderReader     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r25
            r0.<init>(r6)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r25
            r1 = r45
            net.lingala.zip4j.model.LocalFileHeader r29 = r0.readLocalFileHeader(r1)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            if (r29 != 0) goto L_0x0101
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.lang.String r8 = "invalid local file header, cannot remove file from archive"
            r5.<init>((java.lang.String) r8)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            throw r5     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
        L_0x00ee:
            r4 = move-exception
            r41 = r42
            goto L_0x002d
        L_0x00f3:
            r21 = move-exception
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            r0 = r21
            r5.<init>((java.lang.Throwable) r0)     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
            throw r5     // Catch:{ ZipException -> 0x002a, Exception -> 0x0059, all -> 0x00fc }
        L_0x00fc:
            r5 = move-exception
            r7 = r36
            goto L_0x0034
        L_0x0101:
            long r18 = r45.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r45.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            if (r5 == 0) goto L_0x0121
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r45.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r8 = r5.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r12 = -1
            int r5 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r5 == 0) goto L_0x0121
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r45.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r18 = r5.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
        L_0x0121:
            r32 = -1
            net.lingala.zip4j.model.EndCentralDirRecord r5 = r44.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r10 = r5.getOffsetOfStartOfCentralDir()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            boolean r5 = r44.isZip64Format()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            if (r5 == 0) goto L_0x013f
            net.lingala.zip4j.model.Zip64EndCentralDirRecord r5 = r44.getZip64EndCentralDirRecord()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            if (r5 == 0) goto L_0x013f
            net.lingala.zip4j.model.Zip64EndCentralDirRecord r5 = r44.getZip64EndCentralDirRecord()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r10 = r5.getOffsetStartCenDirWRTStartDiskNo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
        L_0x013f:
            net.lingala.zip4j.model.CentralDirectory r5 = r44.getCentralDirectory()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.util.ArrayList r24 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r5 = r24.size()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r5 = r5 + -1
            r0 = r28
            if (r0 != r5) goto L_0x016f
            r8 = 1
            long r32 = r10 - r8
        L_0x0155:
            r8 = 0
            int r5 = (r18 > r8 ? 1 : (r18 == r8 ? 0 : -1))
            if (r5 < 0) goto L_0x0161
            r8 = 0
            int r5 = (r32 > r8 ? 1 : (r32 == r8 ? 0 : -1))
            if (r5 >= 0) goto L_0x01a4
        L_0x0161:
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.lang.String r8 = "invalid offset for start and end of local file, cannot remove file"
            r5.<init>((java.lang.String) r8)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            throw r5     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
        L_0x016a:
            r4 = move-exception
            r41 = r42
            goto L_0x005c
        L_0x016f:
            int r5 = r28 + 1
            r0 = r24
            java.lang.Object r31 = r0.get(r5)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.FileHeader r31 = (net.lingala.zip4j.model.FileHeader) r31     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            if (r31 == 0) goto L_0x0155
            long r8 = r31.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r12 = 1
            long r32 = r8 - r12
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r31.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            if (r5 == 0) goto L_0x0155
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r31.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r8 = r5.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r12 = -1
            int r5 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r5 == 0) goto L_0x0155
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r31.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r8 = r5.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r12 = 1
            long r32 = r8 - r12
            goto L_0x0155
        L_0x01a4:
            if (r28 != 0) goto L_0x01ea
            net.lingala.zip4j.model.CentralDirectory r5 = r44.getCentralDirectory()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r5 = r5.size()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r8 = 1
            if (r5 <= r8) goto L_0x01c0
            r8 = 1
            long r8 = r8 + r32
            r5 = r43
            r12 = r46
            r5.copyFile(r6, r7, r8, r10, r12)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
        L_0x01c0:
            boolean r5 = r46.isCancelAllTasks()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            if (r5 == 0) goto L_0x0233
            r5 = 3
            r0 = r46
            r0.setResult(r5)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r5 = 0
            r0 = r46
            r0.setState(r5)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r37 = 0
            if (r6 == 0) goto L_0x01d9
            r6.close()     // Catch:{ IOException -> 0x021c }
        L_0x01d9:
            if (r7 == 0) goto L_0x01de
            r7.close()     // Catch:{ IOException -> 0x021c }
        L_0x01de:
            if (r38 == 0) goto L_0x0226
            r0 = r43
            r1 = r42
            r2 = r40
            r0.restoreFileName(r1, r2)
        L_0x01e9:
            return r37
        L_0x01ea:
            int r5 = r24.size()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r5 = r5 + -1
            r0 = r28
            if (r0 != r5) goto L_0x0205
            r16 = 0
            r13 = r43
            r14 = r6
            r15 = r7
            r20 = r46
            r13.copyFile(r14, r15, r16, r18, r20)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            goto L_0x01c0
        L_0x0200:
            r5 = move-exception
            r41 = r42
            goto L_0x0034
        L_0x0205:
            r16 = 0
            r13 = r43
            r14 = r6
            r15 = r7
            r20 = r46
            r13.copyFile(r14, r15, r16, r18, r20)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r8 = 1
            long r8 = r8 + r32
            r5 = r43
            r12 = r46
            r5.copyFile(r6, r7, r8, r10, r12)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            goto L_0x01c0
        L_0x021c:
            r4 = move-exception
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r8 = "cannot close input stream or output stream when trying to delete a file from zip file"
            r5.<init>((java.lang.String) r8)
            throw r5
        L_0x0226:
            java.io.File r30 = new java.io.File
            r0 = r30
            r1 = r40
            r0.<init>(r1)
            r30.delete()
            goto L_0x01e9
        L_0x0233:
            net.lingala.zip4j.model.EndCentralDirRecord r8 = r44.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r7
            net.lingala.zip4j.io.SplitOutputStream r0 = (net.lingala.zip4j.io.SplitOutputStream) r0     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r5 = r0
            long r12 = r5.getFilePointer()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r8.setOffsetOfStartOfCentralDir(r12)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.EndCentralDirRecord r5 = r44.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.EndCentralDirRecord r8 = r44.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r8 = r8.getTotNoOfEntriesInCentralDir()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r8 = r8 + -1
            r5.setTotNoOfEntriesInCentralDir(r8)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.EndCentralDirRecord r5 = r44.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.EndCentralDirRecord r8 = r44.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r8 = r8.getTotNoOfEntriesInCentralDirOnThisDisk()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r8 = r8 + -1
            r5.setTotNoOfEntriesInCentralDirOnThisDisk(r8)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.CentralDirectory r5 = r44.getCentralDirectory()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r28
            r5.remove(r0)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r27 = r28
        L_0x0273:
            net.lingala.zip4j.model.CentralDirectory r5 = r44.getCentralDirectory()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r5 = r5.size()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r27
            if (r0 >= r5) goto L_0x0301
            net.lingala.zip4j.model.CentralDirectory r5 = r44.getCentralDirectory()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r27
            java.lang.Object r5 = r5.get(r0)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.FileHeader r5 = (net.lingala.zip4j.model.FileHeader) r5     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r34 = r5.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.CentralDirectory r5 = r44.getCentralDirectory()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r27
            java.lang.Object r5 = r5.get(r0)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.FileHeader r5 = (net.lingala.zip4j.model.FileHeader) r5     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r5.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            if (r5 == 0) goto L_0x02e3
            net.lingala.zip4j.model.CentralDirectory r5 = r44.getCentralDirectory()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r27
            java.lang.Object r5 = r5.get(r0)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.FileHeader r5 = (net.lingala.zip4j.model.FileHeader) r5     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r5.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r8 = r5.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r12 = -1
            int r5 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r5 == 0) goto L_0x02e3
            net.lingala.zip4j.model.CentralDirectory r5 = r44.getCentralDirectory()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r27
            java.lang.Object r5 = r5.get(r0)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.FileHeader r5 = (net.lingala.zip4j.model.FileHeader) r5     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.Zip64ExtendedInfo r5 = r5.getZip64ExtendedInfo()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r34 = r5.getOffsetLocalHeader()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
        L_0x02e3:
            net.lingala.zip4j.model.CentralDirectory r5 = r44.getCentralDirectory()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.util.ArrayList r5 = r5.getFileHeaders()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r27
            java.lang.Object r5 = r5.get(r0)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            net.lingala.zip4j.model.FileHeader r5 = (net.lingala.zip4j.model.FileHeader) r5     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r8 = r32 - r18
            long r8 = r34 - r8
            r12 = 1
            long r8 = r8 - r12
            r5.setOffsetLocalHeader(r8)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            int r27 = r27 + 1
            goto L_0x0273
        L_0x0301:
            net.lingala.zip4j.core.HeaderWriter r26 = new net.lingala.zip4j.core.HeaderWriter     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r26.<init>()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r26
            r1 = r44
            r0.finalizeZipFile(r1, r7)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r38 = 1
            java.lang.String r5 = "offsetCentralDir"
            net.lingala.zip4j.model.EndCentralDirRecord r8 = r44.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            long r8 = r8.getOffsetOfStartOfCentralDir()     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            java.lang.String r8 = java.lang.Long.toString(r8)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            r0 = r37
            r0.put(r5, r8)     // Catch:{ ZipException -> 0x00ee, Exception -> 0x016a, all -> 0x0200 }
            if (r6 == 0) goto L_0x0328
            r6.close()     // Catch:{ IOException -> 0x033a }
        L_0x0328:
            if (r7 == 0) goto L_0x032d
            r7.close()     // Catch:{ IOException -> 0x033a }
        L_0x032d:
            if (r38 == 0) goto L_0x0344
            r0 = r43
            r1 = r42
            r2 = r40
            r0.restoreFileName(r1, r2)
            goto L_0x01e9
        L_0x033a:
            r4 = move-exception
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r8 = "cannot close input stream or output stream when trying to delete a file from zip file"
            r5.<init>((java.lang.String) r8)
            throw r5
        L_0x0344:
            java.io.File r30 = new java.io.File
            r0 = r30
            r1 = r40
            r0.<init>(r1)
            r30.delete()
            goto L_0x01e9
        L_0x0352:
            r4 = move-exception
            net.lingala.zip4j.exception.ZipException r5 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r8 = "cannot close input stream or output stream when trying to delete a file from zip file"
            r5.<init>((java.lang.String) r8)
            throw r5
        L_0x035c:
            java.io.File r30 = new java.io.File
            r0 = r30
            r1 = r40
            r0.<init>(r1)
            r30.delete()
            goto L_0x0049
        L_0x036a:
            r4 = move-exception
            goto L_0x005c
        L_0x036d:
            r4 = move-exception
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.util.ArchiveMaintainer.initRemoveZipFile(net.lingala.zip4j.model.ZipModel, net.lingala.zip4j.model.FileHeader, net.lingala.zip4j.progress.ProgressMonitor):java.util.HashMap");
    }

    public void mergeSplitZipFiles(ZipModel zipModel, File file, ProgressMonitor progressMonitor, boolean z) throws ZipException {
        if (z) {
            final ZipModel zipModel2 = zipModel;
            final File file2 = file;
            final ProgressMonitor progressMonitor2 = progressMonitor;
            new Thread(InternalZipTyphoonApp.THREAD_NAME) {
                public void run() {
                    try {
                        ArchiveMaintainer.this.initMergeSplitZipFile(zipModel2, file2, progressMonitor2);
                    } catch (ZipException e) {
                    }
                }
            }.start();
            return;
        }
        initMergeSplitZipFile(zipModel, file, progressMonitor);
    }

    public HashMap removeZipFile(ZipModel zipModel, FileHeader fileHeader, ProgressMonitor progressMonitor, boolean z) throws ZipException {
        if (z) {
            final ZipModel zipModel2 = zipModel;
            final FileHeader fileHeader2 = fileHeader;
            final ProgressMonitor progressMonitor2 = progressMonitor;
            new Thread(InternalZipTyphoonApp.THREAD_NAME) {
                public void run() {
                    try {
                        ArchiveMaintainer.this.initRemoveZipFile(zipModel2, fileHeader2, progressMonitor2);
                        progressMonitor2.endProgressMonitorSuccess();
                    } catch (ZipException e) {
                    }
                }
            }.start();
            return null;
        }
        HashMap initRemoveZipFile = initRemoveZipFile(zipModel, fileHeader, progressMonitor);
        progressMonitor.endProgressMonitorSuccess();
        return initRemoveZipFile;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b1 A[SYNTHETIC, Splitter:B:40:0x00b1] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:44:0x00b6=Splitter:B:44:0x00b6, B:35:0x00a8=Splitter:B:35:0x00a8} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setComment(net.lingala.zip4j.model.ZipModel r11, java.lang.String r12) throws net.lingala.zip4j.exception.ZipException {
        /*
            r10 = this;
            if (r12 != 0) goto L_0x000b
            net.lingala.zip4j.exception.ZipException r8 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r9 = "comment is null, cannot update Zip file with comment"
            r8.<init>((java.lang.String) r9)
            throw r8
        L_0x000b:
            if (r11 != 0) goto L_0x0016
            net.lingala.zip4j.exception.ZipException r8 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r9 = "zipModel is null, cannot update Zip file with comment"
            r8.<init>((java.lang.String) r9)
            throw r8
        L_0x0016:
            r3 = r12
            byte[] r0 = r12.getBytes()
            int r1 = r12.length()
            java.lang.String r8 = "windows-1254"
            boolean r8 = net.lingala.zip4j.util.Zip4jUtil.isSupportedCharset(r8)
            if (r8 == 0) goto L_0x0043
            java.lang.String r4 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x0051 }
            java.lang.String r8 = "windows-1254"
            byte[] r8 = r12.getBytes(r8)     // Catch:{ UnsupportedEncodingException -> 0x0051 }
            java.lang.String r9 = "windows-1254"
            r4.<init>(r8, r9)     // Catch:{ UnsupportedEncodingException -> 0x0051 }
            java.lang.String r8 = "windows-1254"
            byte[] r0 = r4.getBytes(r8)     // Catch:{ UnsupportedEncodingException -> 0x00c8 }
            int r1 = r4.length()     // Catch:{ UnsupportedEncodingException -> 0x00c8 }
            r3 = r4
        L_0x0043:
            r8 = 65535(0xffff, float:9.1834E-41)
            if (r1 <= r8) goto L_0x005c
            net.lingala.zip4j.exception.ZipException r8 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r9 = "comment length exceeds maximum length"
            r8.<init>((java.lang.String) r9)
            throw r8
        L_0x0051:
            r2 = move-exception
        L_0x0052:
            r3 = r12
            byte[] r0 = r12.getBytes()
            int r1 = r12.length()
            goto L_0x0043
        L_0x005c:
            net.lingala.zip4j.model.EndCentralDirRecord r8 = r11.getEndCentralDirRecord()
            r8.setComment(r3)
            net.lingala.zip4j.model.EndCentralDirRecord r8 = r11.getEndCentralDirRecord()
            r8.setCommentBytes(r0)
            net.lingala.zip4j.model.EndCentralDirRecord r8 = r11.getEndCentralDirRecord()
            r8.setCommentLength(r1)
            r6 = 0
            net.lingala.zip4j.core.HeaderWriter r5 = new net.lingala.zip4j.core.HeaderWriter     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00b5 }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00b5 }
            net.lingala.zip4j.io.SplitOutputStream r7 = new net.lingala.zip4j.io.SplitOutputStream     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00b5 }
            java.lang.String r8 = r11.getZipFile()     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00b5 }
            r7.<init>((java.lang.String) r8)     // Catch:{ FileNotFoundException -> 0x00c6, IOException -> 0x00b5 }
            boolean r8 = r11.isZip64Format()     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00c3, all -> 0x00c0 }
            if (r8 == 0) goto L_0x009a
            net.lingala.zip4j.model.Zip64EndCentralDirRecord r8 = r11.getZip64EndCentralDirRecord()     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00c3, all -> 0x00c0 }
            long r8 = r8.getOffsetStartCenDirWRTStartDiskNo()     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00c3, all -> 0x00c0 }
            r7.seek(r8)     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00c3, all -> 0x00c0 }
        L_0x0091:
            r5.finalizeZipFileWithoutValidations(r11, r7)     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00c3, all -> 0x00c0 }
            if (r7 == 0) goto L_0x0099
            r7.close()     // Catch:{ IOException -> 0x00bc }
        L_0x0099:
            return
        L_0x009a:
            net.lingala.zip4j.model.EndCentralDirRecord r8 = r11.getEndCentralDirRecord()     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00c3, all -> 0x00c0 }
            long r8 = r8.getOffsetOfStartOfCentralDir()     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00c3, all -> 0x00c0 }
            r7.seek(r8)     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00c3, all -> 0x00c0 }
            goto L_0x0091
        L_0x00a6:
            r2 = move-exception
            r6 = r7
        L_0x00a8:
            net.lingala.zip4j.exception.ZipException r8 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x00ae }
            r8.<init>((java.lang.Throwable) r2)     // Catch:{ all -> 0x00ae }
            throw r8     // Catch:{ all -> 0x00ae }
        L_0x00ae:
            r8 = move-exception
        L_0x00af:
            if (r6 == 0) goto L_0x00b4
            r6.close()     // Catch:{ IOException -> 0x00be }
        L_0x00b4:
            throw r8
        L_0x00b5:
            r2 = move-exception
        L_0x00b6:
            net.lingala.zip4j.exception.ZipException r8 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x00ae }
            r8.<init>((java.lang.Throwable) r2)     // Catch:{ all -> 0x00ae }
            throw r8     // Catch:{ all -> 0x00ae }
        L_0x00bc:
            r8 = move-exception
            goto L_0x0099
        L_0x00be:
            r9 = move-exception
            goto L_0x00b4
        L_0x00c0:
            r8 = move-exception
            r6 = r7
            goto L_0x00af
        L_0x00c3:
            r2 = move-exception
            r6 = r7
            goto L_0x00b6
        L_0x00c6:
            r2 = move-exception
            goto L_0x00a8
        L_0x00c8:
            r2 = move-exception
            r3 = r4
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.util.ArchiveMaintainer.setComment(net.lingala.zip4j.model.ZipModel, java.lang.String):void");
    }
}
