package net.lingala.zip4j.util;

import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.progress.ProgressMonitor;

public class CRCUtil {
    private static final int BUF_SIZE = 16384;

    public static long computeFileCRC(String str) throws ZipException {
        return computeFileCRC(str, (ProgressMonitor) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r6 = r1.getValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        if (r4 == null) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006b, code lost:
        throw new net.lingala.zip4j.exception.ZipException("error while closing the file after calculating crc");
     */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0076 A[SYNTHETIC, Splitter:B:39:0x0076] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:43:0x007b=Splitter:B:43:0x007b, B:34:0x006d=Splitter:B:34:0x006d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long computeFileCRC(java.lang.String r8, net.lingala.zip4j.progress.ProgressMonitor r9) throws net.lingala.zip4j.exception.ZipException {
        /*
            boolean r6 = net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(r8)
            if (r6 != 0) goto L_0x000f
            net.lingala.zip4j.exception.ZipException r6 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r7 = "input file is null or empty, cannot calculate CRC for the file"
            r6.<init>((java.lang.String) r7)
            throw r6
        L_0x000f:
            r3 = 0
            net.lingala.zip4j.util.Zip4jUtil.checkFileReadAccess(r8)     // Catch:{ IOException -> 0x006c, Exception -> 0x007a }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x006c, Exception -> 0x007a }
            java.io.File r6 = new java.io.File     // Catch:{ IOException -> 0x006c, Exception -> 0x007a }
            r6.<init>(r8)     // Catch:{ IOException -> 0x006c, Exception -> 0x007a }
            r4.<init>(r6)     // Catch:{ IOException -> 0x006c, Exception -> 0x007a }
            r6 = 16384(0x4000, float:2.2959E-41)
            byte[] r0 = new byte[r6]     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            r5 = -2
            java.util.zip.CRC32 r1 = new java.util.zip.CRC32     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            r1.<init>()     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
        L_0x0027:
            int r5 = r4.read(r0)     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            r6 = -1
            if (r5 == r6) goto L_0x0058
            r6 = 0
            r1.update(r0, r6, r5)     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            if (r9 == 0) goto L_0x0027
            long r6 = (long) r5     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            r9.updateWorkCompleted(r6)     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            boolean r6 = r9.isCancelAllTasks()     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            if (r6 == 0) goto L_0x0027
            r6 = 3
            r9.setResult(r6)     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            r6 = 0
            r9.setState(r6)     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            r6 = 0
            if (r4 == 0) goto L_0x004d
            r4.close()     // Catch:{ IOException -> 0x004e }
        L_0x004d:
            return r6
        L_0x004e:
            r2 = move-exception
            net.lingala.zip4j.exception.ZipException r6 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r7 = "error while closing the file after calculating crc"
            r6.<init>((java.lang.String) r7)
            throw r6
        L_0x0058:
            long r6 = r1.getValue()     // Catch:{ IOException -> 0x0091, Exception -> 0x008e, all -> 0x008b }
            if (r4 == 0) goto L_0x004d
            r4.close()     // Catch:{ IOException -> 0x0062 }
            goto L_0x004d
        L_0x0062:
            r2 = move-exception
            net.lingala.zip4j.exception.ZipException r6 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r7 = "error while closing the file after calculating crc"
            r6.<init>((java.lang.String) r7)
            throw r6
        L_0x006c:
            r2 = move-exception
        L_0x006d:
            net.lingala.zip4j.exception.ZipException r6 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x0073 }
            r6.<init>((java.lang.Throwable) r2)     // Catch:{ all -> 0x0073 }
            throw r6     // Catch:{ all -> 0x0073 }
        L_0x0073:
            r6 = move-exception
        L_0x0074:
            if (r3 == 0) goto L_0x0079
            r3.close()     // Catch:{ IOException -> 0x0081 }
        L_0x0079:
            throw r6
        L_0x007a:
            r2 = move-exception
        L_0x007b:
            net.lingala.zip4j.exception.ZipException r6 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x0073 }
            r6.<init>((java.lang.Throwable) r2)     // Catch:{ all -> 0x0073 }
            throw r6     // Catch:{ all -> 0x0073 }
        L_0x0081:
            r2 = move-exception
            net.lingala.zip4j.exception.ZipException r6 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r7 = "error while closing the file after calculating crc"
            r6.<init>((java.lang.String) r7)
            throw r6
        L_0x008b:
            r6 = move-exception
            r3 = r4
            goto L_0x0074
        L_0x008e:
            r2 = move-exception
            r3 = r4
            goto L_0x007b
        L_0x0091:
            r2 = move-exception
            r3 = r4
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.util.CRCUtil.computeFileCRC(java.lang.String, net.lingala.zip4j.progress.ProgressMonitor):long");
    }
}
