package net.lingala.zip4j.model;

public class Zip64ExtendedInfo {
    private long compressedSize = -1;
    private int diskNumberStart = -1;
    private int header;
    private long offsetLocalHeader = -1;
    private int size;
    private long unCompressedSize = -1;

    public long getCompressedSize() {
        return this.compressedSize;
    }

    public int getDiskNumberStart() {
        return this.diskNumberStart;
    }

    public int getHeader() {
        return this.header;
    }

    public long getOffsetLocalHeader() {
        return this.offsetLocalHeader;
    }

    public int getSize() {
        return this.size;
    }

    public long getUnCompressedSize() {
        return this.unCompressedSize;
    }

    public void setCompressedSize(long j) {
        this.compressedSize = j;
    }

    public void setDiskNumberStart(int i) {
        this.diskNumberStart = i;
    }

    public void setHeader(int i) {
        this.header = i;
    }

    public void setOffsetLocalHeader(long j) {
        this.offsetLocalHeader = j;
    }

    public void setSize(int i) {
        this.size = i;
    }

    public void setUnCompressedSize(long j) {
        this.unCompressedSize = j;
    }
}
