package net.lingala.zip4j.model;

import java.util.TimeZone;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.lingala.zip4j.util.Zip4jUtil;

public class ZipParameters implements Cloneable {
    private int aesKeyStrength = -1;
    private int compressionLevel;
    private int compressionMethod = 8;
    private String defaultFolderPath;
    private boolean encryptFiles = false;
    private int encryptionMethod = -1;
    private String fileNameInZip;
    private boolean includeRootFolder = true;
    private boolean isSourceExternalStream;
    private char[] password;
    private boolean readHiddenFiles = true;
    private String rootFolderInZip;
    private int sourceFileCRC;
    private TimeZone timeZone = TimeZone.getDefault();

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public int getAesKeyStrength() {
        return this.aesKeyStrength;
    }

    public int getCompressionLevel() {
        return this.compressionLevel;
    }

    public int getCompressionMethod() {
        return this.compressionMethod;
    }

    public String getDefaultFolderPath() {
        return this.defaultFolderPath;
    }

    public int getEncryptionMethod() {
        return this.encryptionMethod;
    }

    public String getFileNameInZip() {
        return this.fileNameInZip;
    }

    public char[] getPassword() {
        return this.password;
    }

    public String getRootFolderInZip() {
        return this.rootFolderInZip;
    }

    public int getSourceFileCRC() {
        return this.sourceFileCRC;
    }

    public TimeZone getTimeZone() {
        return this.timeZone;
    }

    public boolean isEncryptFiles() {
        return this.encryptFiles;
    }

    public boolean isIncludeRootFolder() {
        return this.includeRootFolder;
    }

    public boolean isReadHiddenFiles() {
        return this.readHiddenFiles;
    }

    public boolean isSourceExternalStream() {
        return this.isSourceExternalStream;
    }

    public void setAesKeyStrength(int i) {
        this.aesKeyStrength = i;
    }

    public void setCompressionLevel(int i) {
        this.compressionLevel = i;
    }

    public void setCompressionMethod(int i) {
        this.compressionMethod = i;
    }

    public void setDefaultFolderPath(String str) {
        this.defaultFolderPath = str;
    }

    public void setEncryptFiles(boolean z) {
        this.encryptFiles = z;
    }

    public void setEncryptionMethod(int i) {
        this.encryptionMethod = i;
    }

    public void setFileNameInZip(String str) {
        this.fileNameInZip = str;
    }

    public void setIncludeRootFolder(boolean z) {
        this.includeRootFolder = z;
    }

    public void setPassword(String str) {
        if (str != null) {
            setPassword(str.toCharArray());
        }
    }

    public void setPassword(char[] cArr) {
        this.password = cArr;
    }

    public void setReadHiddenFiles(boolean z) {
        this.readHiddenFiles = z;
    }

    public void setRootFolderInZip(String str) {
        if (Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            if (!str.endsWith("\\") && !str.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                str = str + InternalZipTyphoonApp.FILE_SEPARATOR;
            }
            str = str.replaceAll("\\\\", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
        }
        this.rootFolderInZip = str;
    }

    public void setSourceExternalStream(boolean z) {
        this.isSourceExternalStream = z;
    }

    public void setSourceFileCRC(int i) {
        this.sourceFileCRC = i;
    }

    public void setTimeZone(TimeZone timeZone2) {
        this.timeZone = timeZone2;
    }
}
