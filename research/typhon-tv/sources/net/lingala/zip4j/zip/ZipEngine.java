package net.lingala.zip4j.zip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.progress.ProgressMonitor;
import net.lingala.zip4j.util.ArchiveMaintainer;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.lingala.zip4j.util.Zip4jUtil;

public class ZipEngine {
    private ZipModel zipModel;

    public ZipEngine(ZipModel zipModel2) throws ZipException {
        if (zipModel2 == null) {
            throw new ZipException("zip model is null in ZipEngine constructor");
        }
        this.zipModel = zipModel2;
    }

    private long calculateTotalWork(ArrayList arrayList, ZipParameters zipParameters) throws ZipException {
        FileHeader fileHeader;
        if (arrayList == null) {
            throw new ZipException("file list is null, cannot calculate total work");
        }
        long j = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if ((arrayList.get(i) instanceof File) && ((File) arrayList.get(i)).exists()) {
                j = (!zipParameters.isEncryptFiles() || zipParameters.getEncryptionMethod() != 0) ? j + Zip4jUtil.getFileLengh((File) arrayList.get(i)) : j + (Zip4jUtil.getFileLengh((File) arrayList.get(i)) * 2);
                if (!(this.zipModel.getCentralDirectory() == null || this.zipModel.getCentralDirectory().getFileHeaders() == null || this.zipModel.getCentralDirectory().getFileHeaders().size() <= 0 || (fileHeader = Zip4jUtil.getFileHeader(this.zipModel, Zip4jUtil.getRelativeFileName(((File) arrayList.get(i)).getAbsolutePath(), zipParameters.getRootFolderInZip(), zipParameters.getDefaultFolderPath()))) == null)) {
                    j += Zip4jUtil.getFileLengh(new File(this.zipModel.getZipFile())) - fileHeader.getCompressedSize();
                }
            }
        }
        return j;
    }

    private void checkParameters(ZipParameters zipParameters) throws ZipException {
        if (zipParameters == null) {
            throw new ZipException("cannot validate zip parameters");
        } else if (zipParameters.getCompressionMethod() != 0 && zipParameters.getCompressionMethod() != 8) {
            throw new ZipException("unsupported compression type");
        } else if (zipParameters.getCompressionMethod() == 8 && zipParameters.getCompressionLevel() < 0 && zipParameters.getCompressionLevel() > 9) {
            throw new ZipException("invalid compression level. compression level dor deflate should be in the range of 0-9");
        } else if (!zipParameters.isEncryptFiles()) {
            zipParameters.setAesKeyStrength(-1);
            zipParameters.setEncryptionMethod(-1);
        } else if (zipParameters.getEncryptionMethod() != 0 && zipParameters.getEncryptionMethod() != 99) {
            throw new ZipException("unsupported encryption method");
        } else if (zipParameters.getPassword() == null || zipParameters.getPassword().length <= 0) {
            throw new ZipException("input password is empty or null");
        }
    }

    private EndCentralDirRecord createEndOfCentralDirectoryRecord() {
        EndCentralDirRecord endCentralDirRecord = new EndCentralDirRecord();
        endCentralDirRecord.setSignature(InternalZipTyphoonApp.ENDSIG);
        endCentralDirRecord.setNoOfThisDisk(0);
        endCentralDirRecord.setTotNoOfEntriesInCentralDir(0);
        endCentralDirRecord.setTotNoOfEntriesInCentralDirOnThisDisk(0);
        endCentralDirRecord.setOffsetOfStartOfCentralDir(0);
        return endCentralDirRecord;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        r9.closeEntry();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01c8, code lost:
        if (r5 == null) goto L_0x0173;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01ca, code lost:
        r5.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x008c A[SYNTHETIC, Splitter:B:29:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0091 A[SYNTHETIC, Splitter:B:32:0x0091] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initAddFiles(java.util.ArrayList r19, net.lingala.zip4j.model.ZipParameters r20, net.lingala.zip4j.progress.ProgressMonitor r21) throws net.lingala.zip4j.exception.ZipException {
        /*
            r18 = this;
            if (r19 == 0) goto L_0x0004
            if (r20 != 0) goto L_0x000d
        L_0x0004:
            net.lingala.zip4j.exception.ZipException r13 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r14 = "one of the input parameters is null when adding files"
            r13.<init>((java.lang.String) r14)
            throw r13
        L_0x000d:
            int r13 = r19.size()
            if (r13 > 0) goto L_0x001c
            net.lingala.zip4j.exception.ZipException r13 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r14 = "no files to add"
            r13.<init>((java.lang.String) r14)
            throw r13
        L_0x001c:
            r0 = r18
            net.lingala.zip4j.model.ZipModel r13 = r0.zipModel
            net.lingala.zip4j.model.EndCentralDirRecord r13 = r13.getEndCentralDirRecord()
            if (r13 != 0) goto L_0x0031
            r0 = r18
            net.lingala.zip4j.model.ZipModel r13 = r0.zipModel
            net.lingala.zip4j.model.EndCentralDirRecord r14 = r18.createEndOfCentralDirectoryRecord()
            r13.setEndCentralDirRecord(r14)
        L_0x0031:
            r8 = 0
            r5 = 0
            r0 = r18
            r1 = r20
            r0.checkParameters(r1)     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            r18.removeFilesIfExists(r19, r20, r21)     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            r0 = r18
            net.lingala.zip4j.model.ZipModel r13 = r0.zipModel     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            java.lang.String r13 = r13.getZipFile()     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            boolean r7 = net.lingala.zip4j.util.Zip4jUtil.checkFileExists((java.lang.String) r13)     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            net.lingala.zip4j.io.SplitOutputStream r12 = new net.lingala.zip4j.io.SplitOutputStream     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            java.io.File r13 = new java.io.File     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            r0 = r18
            net.lingala.zip4j.model.ZipModel r14 = r0.zipModel     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            java.lang.String r14 = r14.getZipFile()     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            r13.<init>(r14)     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            r0 = r18
            net.lingala.zip4j.model.ZipModel r14 = r0.zipModel     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            long r14 = r14.getSplitLength()     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            r12.<init>((java.io.File) r13, (long) r14)     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            net.lingala.zip4j.io.ZipOutputStream r9 = new net.lingala.zip4j.io.ZipOutputStream     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            r0 = r18
            net.lingala.zip4j.model.ZipModel r13 = r0.zipModel     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            r9.<init>(r12, r13)     // Catch:{ ZipException -> 0x0208, Exception -> 0x0202 }
            if (r7 == 0) goto L_0x00a4
            r0 = r18
            net.lingala.zip4j.model.ZipModel r13 = r0.zipModel     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            net.lingala.zip4j.model.EndCentralDirRecord r13 = r13.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            if (r13 != 0) goto L_0x0095
            net.lingala.zip4j.exception.ZipException r13 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r14 = "invalid end of central directory record"
            r13.<init>((java.lang.String) r14)     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            throw r13     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
        L_0x0081:
            r2 = move-exception
            r8 = r9
        L_0x0083:
            r0 = r21
            r0.endProgressMonitorError(r2)     // Catch:{ all -> 0x0089 }
            throw r2     // Catch:{ all -> 0x0089 }
        L_0x0089:
            r13 = move-exception
        L_0x008a:
            if (r5 == 0) goto L_0x008f
            r5.close()     // Catch:{ IOException -> 0x01f7 }
        L_0x008f:
            if (r8 == 0) goto L_0x0094
            r8.close()     // Catch:{ IOException -> 0x01fa }
        L_0x0094:
            throw r13
        L_0x0095:
            r0 = r18
            net.lingala.zip4j.model.ZipModel r13 = r0.zipModel     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            net.lingala.zip4j.model.EndCentralDirRecord r13 = r13.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            long r14 = r13.getOffsetOfStartOfCentralDir()     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            r12.seek(r14)     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
        L_0x00a4:
            r13 = 4096(0x1000, float:5.74E-42)
            byte[] r10 = new byte[r13]     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            r11 = -1
            r4 = 0
            r6 = r5
        L_0x00ab:
            int r13 = r19.size()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r4 >= r13) goto L_0x01d2
            boolean r13 = r21.isCancelAllTasks()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r13 == 0) goto L_0x00cf
            r13 = 3
            r0 = r21
            r0.setResult(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r13 = 0
            r0 = r21
            r0.setState(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r6 == 0) goto L_0x00c8
            r6.close()     // Catch:{ IOException -> 0x01e5 }
        L_0x00c8:
            if (r9 == 0) goto L_0x00cd
            r9.close()     // Catch:{ IOException -> 0x01e8 }
        L_0x00cd:
            r5 = r6
        L_0x00ce:
            return
        L_0x00cf:
            java.lang.Object r3 = r20.clone()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            net.lingala.zip4j.model.ZipParameters r3 = (net.lingala.zip4j.model.ZipParameters) r3     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r0 = r19
            java.lang.Object r13 = r0.get(r4)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            java.io.File r13 = (java.io.File) r13     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            java.lang.String r13 = r13.getAbsolutePath()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r0 = r21
            r0.setFileName(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r0 = r19
            java.lang.Object r13 = r0.get(r4)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            java.io.File r13 = (java.io.File) r13     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            boolean r13 = r13.isDirectory()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r13 != 0) goto L_0x0156
            boolean r13 = r3.isEncryptFiles()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r13 == 0) goto L_0x0140
            int r13 = r3.getEncryptionMethod()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r13 != 0) goto L_0x0140
            r13 = 3
            r0 = r21
            r0.setCurrentOperation(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r0 = r19
            java.lang.Object r13 = r0.get(r4)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            java.io.File r13 = (java.io.File) r13     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            java.lang.String r13 = r13.getAbsolutePath()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r0 = r21
            long r14 = net.lingala.zip4j.util.CRCUtil.computeFileCRC(r13, r0)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            int r13 = (int) r14     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r3.setSourceFileCRC(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r13 = 0
            r0 = r21
            r0.setCurrentOperation(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            boolean r13 = r21.isCancelAllTasks()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r13 == 0) goto L_0x0140
            r13 = 3
            r0 = r21
            r0.setResult(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r13 = 0
            r0 = r21
            r0.setState(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r6 == 0) goto L_0x0139
            r6.close()     // Catch:{ IOException -> 0x01eb }
        L_0x0139:
            if (r9 == 0) goto L_0x013e
            r9.close()     // Catch:{ IOException -> 0x01ee }
        L_0x013e:
            r5 = r6
            goto L_0x00ce
        L_0x0140:
            r0 = r19
            java.lang.Object r13 = r0.get(r4)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            java.io.File r13 = (java.io.File) r13     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            long r14 = net.lingala.zip4j.util.Zip4jUtil.getFileLengh((java.io.File) r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r16 = 0
            int r13 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r13 != 0) goto L_0x0156
            r13 = 0
            r3.setCompressionMethod(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
        L_0x0156:
            r0 = r19
            java.lang.Object r13 = r0.get(r4)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            java.io.File r13 = (java.io.File) r13     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r9.putNextEntry(r13, r3)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r0 = r19
            java.lang.Object r13 = r0.get(r4)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            java.io.File r13 = (java.io.File) r13     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            boolean r13 = r13.isDirectory()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r13 == 0) goto L_0x0178
            r9.closeEntry()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r5 = r6
        L_0x0173:
            int r4 = r4 + 1
            r6 = r5
            goto L_0x00ab
        L_0x0178:
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r0 = r19
            java.lang.Object r13 = r0.get(r4)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            java.io.File r13 = (java.io.File) r13     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r5.<init>(r13)     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
        L_0x0185:
            int r11 = r5.read(r10)     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            r13 = -1
            if (r11 == r13) goto L_0x01c5
            boolean r13 = r21.isCancelAllTasks()     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            if (r13 == 0) goto L_0x01ad
            r13 = 3
            r0 = r21
            r0.setResult(r13)     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            r13 = 0
            r0 = r21
            r0.setState(r13)     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            if (r5 == 0) goto L_0x01a3
            r5.close()     // Catch:{ IOException -> 0x01f1 }
        L_0x01a3:
            if (r9 == 0) goto L_0x00ce
            r9.close()     // Catch:{ IOException -> 0x01aa }
            goto L_0x00ce
        L_0x01aa:
            r13 = move-exception
            goto L_0x00ce
        L_0x01ad:
            r13 = 0
            r9.write(r10, r13, r11)     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            long r14 = (long) r11     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            r0 = r21
            r0.updateWorkCompleted(r14)     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            goto L_0x0185
        L_0x01b8:
            r2 = move-exception
            r8 = r9
        L_0x01ba:
            r0 = r21
            r0.endProgressMonitorError(r2)     // Catch:{ all -> 0x0089 }
            net.lingala.zip4j.exception.ZipException r13 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x0089 }
            r13.<init>((java.lang.Throwable) r2)     // Catch:{ all -> 0x0089 }
            throw r13     // Catch:{ all -> 0x0089 }
        L_0x01c5:
            r9.closeEntry()     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            if (r5 == 0) goto L_0x0173
            r5.close()     // Catch:{ ZipException -> 0x0081, Exception -> 0x01b8, all -> 0x01ce }
            goto L_0x0173
        L_0x01ce:
            r13 = move-exception
            r8 = r9
            goto L_0x008a
        L_0x01d2:
            r9.finish()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            r21.endProgressMonitorSuccess()     // Catch:{ ZipException -> 0x020b, Exception -> 0x0204, all -> 0x01fd }
            if (r6 == 0) goto L_0x01dd
            r6.close()     // Catch:{ IOException -> 0x01f3 }
        L_0x01dd:
            if (r9 == 0) goto L_0x01e2
            r9.close()     // Catch:{ IOException -> 0x01f5 }
        L_0x01e2:
            r5 = r6
            goto L_0x00ce
        L_0x01e5:
            r13 = move-exception
            goto L_0x00c8
        L_0x01e8:
            r13 = move-exception
            goto L_0x00cd
        L_0x01eb:
            r13 = move-exception
            goto L_0x0139
        L_0x01ee:
            r13 = move-exception
            goto L_0x013e
        L_0x01f1:
            r13 = move-exception
            goto L_0x01a3
        L_0x01f3:
            r13 = move-exception
            goto L_0x01dd
        L_0x01f5:
            r13 = move-exception
            goto L_0x01e2
        L_0x01f7:
            r14 = move-exception
            goto L_0x008f
        L_0x01fa:
            r14 = move-exception
            goto L_0x0094
        L_0x01fd:
            r13 = move-exception
            r5 = r6
            r8 = r9
            goto L_0x008a
        L_0x0202:
            r2 = move-exception
            goto L_0x01ba
        L_0x0204:
            r2 = move-exception
            r5 = r6
            r8 = r9
            goto L_0x01ba
        L_0x0208:
            r2 = move-exception
            goto L_0x0083
        L_0x020b:
            r2 = move-exception
            r5 = r6
            r8 = r9
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.zip.ZipEngine.initAddFiles(java.util.ArrayList, net.lingala.zip4j.model.ZipParameters, net.lingala.zip4j.progress.ProgressMonitor):void");
    }

    private RandomAccessFile prepareFileOutputStream() throws ZipException {
        String zipFile = this.zipModel.getZipFile();
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(zipFile)) {
            throw new ZipException("invalid output path");
        }
        try {
            File file = new File(zipFile);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            return new RandomAccessFile(file, InternalZipTyphoonApp.WRITE_MODE);
        } catch (FileNotFoundException e) {
            throw new ZipException((Throwable) e);
        }
    }

    private void removeFilesIfExists(ArrayList arrayList, ZipParameters zipParameters, ProgressMonitor progressMonitor) throws ZipException {
        if (this.zipModel != null && this.zipModel.getCentralDirectory() != null && this.zipModel.getCentralDirectory().getFileHeaders() != null && this.zipModel.getCentralDirectory().getFileHeaders().size() > 0) {
            RandomAccessFile randomAccessFile = null;
            int i = 0;
            while (i < arrayList.size()) {
                try {
                    FileHeader fileHeader = Zip4jUtil.getFileHeader(this.zipModel, Zip4jUtil.getRelativeFileName(((File) arrayList.get(i)).getAbsolutePath(), zipParameters.getRootFolderInZip(), zipParameters.getDefaultFolderPath()));
                    if (fileHeader != null) {
                        if (randomAccessFile != null) {
                            randomAccessFile.close();
                            randomAccessFile = null;
                        }
                        ArchiveMaintainer archiveMaintainer = new ArchiveMaintainer();
                        progressMonitor.setCurrentOperation(2);
                        HashMap initRemoveZipFile = archiveMaintainer.initRemoveZipFile(this.zipModel, fileHeader, progressMonitor);
                        if (progressMonitor.isCancelAllTasks()) {
                            progressMonitor.setResult(3);
                            progressMonitor.setState(0);
                            if (randomAccessFile != null) {
                                try {
                                    randomAccessFile.close();
                                    return;
                                } catch (IOException e) {
                                    return;
                                }
                            } else {
                                return;
                            }
                        } else {
                            progressMonitor.setCurrentOperation(0);
                            if (randomAccessFile == null) {
                                randomAccessFile = prepareFileOutputStream();
                                if (!(initRemoveZipFile == null || initRemoveZipFile.get(InternalZipTyphoonApp.OFFSET_CENTRAL_DIR) == null)) {
                                    long parseLong = Long.parseLong((String) initRemoveZipFile.get(InternalZipTyphoonApp.OFFSET_CENTRAL_DIR));
                                    if (parseLong >= 0) {
                                        randomAccessFile.seek(parseLong);
                                    }
                                }
                            }
                        }
                    }
                    i++;
                } catch (NumberFormatException e2) {
                    throw new ZipException("NumberFormatException while parsing offset central directory. Cannot update already existing file header");
                } catch (Exception e3) {
                    throw new ZipException("Error while parsing offset central directory. Cannot update already existing file header");
                } catch (IOException e4) {
                    try {
                        throw new ZipException((Throwable) e4);
                    } catch (Throwable th) {
                        if (randomAccessFile != null) {
                            try {
                                randomAccessFile.close();
                            } catch (IOException e5) {
                            }
                        }
                        throw th;
                    }
                }
            }
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (IOException e6) {
                }
            }
        }
    }

    public void addFiles(ArrayList arrayList, ZipParameters zipParameters, ProgressMonitor progressMonitor, boolean z) throws ZipException {
        if (arrayList == null || zipParameters == null) {
            throw new ZipException("one of the input parameters is null when adding files");
        } else if (arrayList.size() <= 0) {
            throw new ZipException("no files to add");
        } else {
            progressMonitor.setCurrentOperation(0);
            progressMonitor.setState(1);
            progressMonitor.setResult(1);
            if (z) {
                progressMonitor.setTotalWork(calculateTotalWork(arrayList, zipParameters));
                progressMonitor.setFileName(((File) arrayList.get(0)).getAbsolutePath());
                final ArrayList arrayList2 = arrayList;
                final ZipParameters zipParameters2 = zipParameters;
                final ProgressMonitor progressMonitor2 = progressMonitor;
                new Thread(InternalZipTyphoonApp.THREAD_NAME) {
                    public void run() {
                        try {
                            ZipEngine.this.initAddFiles(arrayList2, zipParameters2, progressMonitor2);
                        } catch (ZipException e) {
                        }
                    }
                }.start();
                return;
            }
            initAddFiles(arrayList, zipParameters, progressMonitor);
        }
    }

    public void addFolderToZip(File file, ZipParameters zipParameters, ProgressMonitor progressMonitor, boolean z) throws ZipException {
        if (file == null || zipParameters == null) {
            throw new ZipException("one of the input parameters is null, cannot add folder to zip");
        } else if (!Zip4jUtil.checkFileExists(file.getAbsolutePath())) {
            throw new ZipException("input folder does not exist");
        } else if (!file.isDirectory()) {
            throw new ZipException("input file is not a folder, user addFileToZip method to add files");
        } else if (!Zip4jUtil.checkFileReadAccess(file.getAbsolutePath())) {
            throw new ZipException("cannot read folder: " + file.getAbsolutePath());
        } else {
            zipParameters.setDefaultFolderPath(zipParameters.isIncludeRootFolder() ? file.getAbsolutePath() != null ? file.getAbsoluteFile().getParentFile() != null ? file.getAbsoluteFile().getParentFile().getAbsolutePath() : "" : file.getParentFile() != null ? file.getParentFile().getAbsolutePath() : "" : file.getAbsolutePath());
            ArrayList filesInDirectoryRec = Zip4jUtil.getFilesInDirectoryRec(file, zipParameters.isReadHiddenFiles());
            if (zipParameters.isIncludeRootFolder()) {
                if (filesInDirectoryRec == null) {
                    filesInDirectoryRec = new ArrayList();
                }
                filesInDirectoryRec.add(file);
            }
            addFiles(filesInDirectoryRec, zipParameters, progressMonitor, z);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a1 A[SYNTHETIC, Splitter:B:39:0x00a1] */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:33:0x0093=Splitter:B:33:0x0093, B:15:0x004d=Splitter:B:15:0x004d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addStreamToZip(java.io.InputStream r11, net.lingala.zip4j.model.ZipParameters r12) throws net.lingala.zip4j.exception.ZipException {
        /*
            r10 = this;
            if (r11 == 0) goto L_0x0004
            if (r12 != 0) goto L_0x000d
        L_0x0004:
            net.lingala.zip4j.exception.ZipException r7 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r8 = "one of the input parameters is null, cannot add stream to zip"
            r7.<init>((java.lang.String) r8)
            throw r7
        L_0x000d:
            r2 = 0
            r10.checkParameters(r12)     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            net.lingala.zip4j.model.ZipModel r7 = r10.zipModel     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            java.lang.String r7 = r7.getZipFile()     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            boolean r1 = net.lingala.zip4j.util.Zip4jUtil.checkFileExists((java.lang.String) r7)     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            net.lingala.zip4j.io.SplitOutputStream r6 = new net.lingala.zip4j.io.SplitOutputStream     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            java.io.File r7 = new java.io.File     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            net.lingala.zip4j.model.ZipModel r8 = r10.zipModel     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            java.lang.String r8 = r8.getZipFile()     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            r7.<init>(r8)     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            net.lingala.zip4j.model.ZipModel r8 = r10.zipModel     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            long r8 = r8.getSplitLength()     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            r6.<init>((java.io.File) r7, (long) r8)     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            net.lingala.zip4j.io.ZipOutputStream r3 = new net.lingala.zip4j.io.ZipOutputStream     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            net.lingala.zip4j.model.ZipModel r7 = r10.zipModel     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            r3.<init>(r6, r7)     // Catch:{ ZipException -> 0x00ae, Exception -> 0x00ac }
            if (r1 == 0) goto L_0x0062
            net.lingala.zip4j.model.ZipModel r7 = r10.zipModel     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            net.lingala.zip4j.model.EndCentralDirRecord r7 = r7.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            if (r7 != 0) goto L_0x0055
            net.lingala.zip4j.exception.ZipException r7 = new net.lingala.zip4j.exception.ZipException     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            java.lang.String r8 = "invalid end of central directory record"
            r7.<init>((java.lang.String) r8)     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            throw r7     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
        L_0x004b:
            r0 = move-exception
            r2 = r3
        L_0x004d:
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r7 = move-exception
        L_0x004f:
            if (r2 == 0) goto L_0x0054
            r2.close()     // Catch:{ IOException -> 0x00a7 }
        L_0x0054:
            throw r7
        L_0x0055:
            net.lingala.zip4j.model.ZipModel r7 = r10.zipModel     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            net.lingala.zip4j.model.EndCentralDirRecord r7 = r7.getEndCentralDirRecord()     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            long r8 = r7.getOffsetOfStartOfCentralDir()     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            r6.seek(r8)     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
        L_0x0062:
            r7 = 4096(0x1000, float:5.74E-42)
            byte[] r4 = new byte[r7]     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            r5 = -1
            r7 = 0
            r3.putNextEntry(r7, r12)     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            java.lang.String r7 = r12.getFileNameInZip()     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            java.lang.String r8 = "/"
            boolean r7 = r7.endsWith(r8)     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            if (r7 != 0) goto L_0x0099
            java.lang.String r7 = r12.getFileNameInZip()     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            java.lang.String r8 = "\\"
            boolean r7 = r7.endsWith(r8)     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            if (r7 != 0) goto L_0x0099
        L_0x0085:
            int r5 = r11.read(r4)     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            r7 = -1
            if (r5 == r7) goto L_0x0099
            r7 = 0
            r3.write(r4, r7, r5)     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            goto L_0x0085
        L_0x0091:
            r0 = move-exception
            r2 = r3
        L_0x0093:
            net.lingala.zip4j.exception.ZipException r7 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x004e }
            r7.<init>((java.lang.Throwable) r0)     // Catch:{ all -> 0x004e }
            throw r7     // Catch:{ all -> 0x004e }
        L_0x0099:
            r3.closeEntry()     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            r3.finish()     // Catch:{ ZipException -> 0x004b, Exception -> 0x0091, all -> 0x00a9 }
            if (r3 == 0) goto L_0x00a4
            r3.close()     // Catch:{ IOException -> 0x00a5 }
        L_0x00a4:
            return
        L_0x00a5:
            r7 = move-exception
            goto L_0x00a4
        L_0x00a7:
            r8 = move-exception
            goto L_0x0054
        L_0x00a9:
            r7 = move-exception
            r2 = r3
            goto L_0x004f
        L_0x00ac:
            r0 = move-exception
            goto L_0x0093
        L_0x00ae:
            r0 = move-exception
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.zip.ZipEngine.addStreamToZip(java.io.InputStream, net.lingala.zip4j.model.ZipParameters):void");
    }
}
