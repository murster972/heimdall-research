package net.lingala.zip4j.core;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.UnzipParameters;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.progress.ProgressMonitor;
import net.lingala.zip4j.unzip.Unzip;
import net.lingala.zip4j.util.ArchiveMaintainer;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.lingala.zip4j.util.Zip4jUtil;
import net.lingala.zip4j.zip.ZipEngine;

public class ZipFile {
    private String file;
    private String fileNameCharset;
    private boolean isEncrypted;
    private int mode;
    private ProgressMonitor progressMonitor;
    private boolean runInThread;
    private ZipModel zipModel;

    public ZipFile(File file2) throws ZipException {
        if (file2 == null) {
            throw new ZipException("Input zip file parameter is not null", 1);
        }
        this.file = file2.getPath();
        this.mode = 2;
        this.progressMonitor = new ProgressMonitor();
        this.runInThread = false;
    }

    public ZipFile(String str) throws ZipException {
        this(new File(str));
    }

    private void addFolder(File file2, ZipParameters zipParameters, boolean z) throws ZipException {
        checkZipModel();
        if (this.zipModel == null) {
            throw new ZipException("internal error: zip model is null");
        } else if (!z || !this.zipModel.isSplitArchive()) {
            new ZipEngine(this.zipModel).addFolderToZip(file2, zipParameters, this.progressMonitor, this.runInThread);
        } else {
            throw new ZipException("This is a split archive. Zip file format does not allow updating split/spanned files");
        }
    }

    private void checkZipModel() throws ZipException {
        if (this.zipModel != null) {
            return;
        }
        if (Zip4jUtil.checkFileExists(this.file)) {
            readZipInfo();
        } else {
            createNewZipModel();
        }
    }

    private void createNewZipModel() {
        this.zipModel = new ZipModel();
        this.zipModel.setZipFile(this.file);
        this.zipModel.setFileNameCharset(this.fileNameCharset);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x006c A[SYNTHETIC, Splitter:B:30:0x006c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void readZipInfo() throws net.lingala.zip4j.exception.ZipException {
        /*
            r6 = this;
            java.lang.String r4 = r6.file
            boolean r4 = net.lingala.zip4j.util.Zip4jUtil.checkFileExists((java.lang.String) r4)
            if (r4 != 0) goto L_0x0011
            net.lingala.zip4j.exception.ZipException r4 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r5 = "zip file does not exist"
            r4.<init>((java.lang.String) r5)
            throw r4
        L_0x0011:
            java.lang.String r4 = r6.file
            boolean r4 = net.lingala.zip4j.util.Zip4jUtil.checkFileReadAccess(r4)
            if (r4 != 0) goto L_0x0022
            net.lingala.zip4j.exception.ZipException r4 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r5 = "no read access for the input zip file"
            r4.<init>((java.lang.String) r5)
            throw r4
        L_0x0022:
            int r4 = r6.mode
            r5 = 2
            if (r4 == r5) goto L_0x0030
            net.lingala.zip4j.exception.ZipException r4 = new net.lingala.zip4j.exception.ZipException
            java.lang.String r5 = "Invalid mode"
            r4.<init>((java.lang.String) r5)
            throw r4
        L_0x0030:
            r2 = 0
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x0062 }
            java.io.File r4 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0062 }
            java.lang.String r5 = r6.file     // Catch:{ FileNotFoundException -> 0x0062 }
            r4.<init>(r5)     // Catch:{ FileNotFoundException -> 0x0062 }
            java.lang.String r5 = "r"
            r3.<init>(r4, r5)     // Catch:{ FileNotFoundException -> 0x0062 }
            net.lingala.zip4j.model.ZipModel r4 = r6.zipModel     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
            if (r4 != 0) goto L_0x005c
            net.lingala.zip4j.core.HeaderReader r1 = new net.lingala.zip4j.core.HeaderReader     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
            java.lang.String r4 = r6.fileNameCharset     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
            net.lingala.zip4j.model.ZipModel r4 = r1.readAllHeaders(r4)     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
            r6.zipModel = r4     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
            net.lingala.zip4j.model.ZipModel r4 = r6.zipModel     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
            if (r4 == 0) goto L_0x005c
            net.lingala.zip4j.model.ZipModel r4 = r6.zipModel     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
            java.lang.String r5 = r6.file     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
            r4.setZipFile(r5)     // Catch:{ FileNotFoundException -> 0x0077, all -> 0x0074 }
        L_0x005c:
            if (r3 == 0) goto L_0x0061
            r3.close()     // Catch:{ IOException -> 0x0070 }
        L_0x0061:
            return
        L_0x0062:
            r0 = move-exception
        L_0x0063:
            net.lingala.zip4j.exception.ZipException r4 = new net.lingala.zip4j.exception.ZipException     // Catch:{ all -> 0x0069 }
            r4.<init>((java.lang.Throwable) r0)     // Catch:{ all -> 0x0069 }
            throw r4     // Catch:{ all -> 0x0069 }
        L_0x0069:
            r4 = move-exception
        L_0x006a:
            if (r2 == 0) goto L_0x006f
            r2.close()     // Catch:{ IOException -> 0x0072 }
        L_0x006f:
            throw r4
        L_0x0070:
            r4 = move-exception
            goto L_0x0061
        L_0x0072:
            r5 = move-exception
            goto L_0x006f
        L_0x0074:
            r4 = move-exception
            r2 = r3
            goto L_0x006a
        L_0x0077:
            r0 = move-exception
            r2 = r3
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.core.ZipFile.readZipInfo():void");
    }

    public void addFile(File file2, ZipParameters zipParameters) throws ZipException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(file2);
        addFiles(arrayList, zipParameters);
    }

    public void addFiles(ArrayList arrayList, ZipParameters zipParameters) throws ZipException {
        checkZipModel();
        if (this.zipModel == null) {
            throw new ZipException("internal error: zip model is null");
        } else if (arrayList == null) {
            throw new ZipException("input file ArrayList is null, cannot add files");
        } else if (!Zip4jUtil.checkArrayListTypes(arrayList, 1)) {
            throw new ZipException("One or more elements in the input ArrayList is not of type File");
        } else if (zipParameters == null) {
            throw new ZipException("input parameters are null, cannot add files to zip");
        } else if (this.progressMonitor.getState() == 1) {
            throw new ZipException("invalid operation - Zip4j is in busy state");
        } else if (!Zip4jUtil.checkFileExists(this.file) || !this.zipModel.isSplitArchive()) {
            new ZipEngine(this.zipModel).addFiles(arrayList, zipParameters, this.progressMonitor, this.runInThread);
        } else {
            throw new ZipException("Zip file already exists. Zip file format does not allow updating split/spanned files");
        }
    }

    public void addFolder(File file2, ZipParameters zipParameters) throws ZipException {
        if (file2 == null) {
            throw new ZipException("input path is null, cannot add folder to zip file");
        } else if (zipParameters == null) {
            throw new ZipException("input parameters are null, cannot add folder to zip file");
        } else {
            addFolder(file2, zipParameters, true);
        }
    }

    public void addFolder(String str, ZipParameters zipParameters) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("input path is null or empty, cannot add folder to zip file");
        }
        addFolder(new File(str), zipParameters);
    }

    public void addStream(InputStream inputStream, ZipParameters zipParameters) throws ZipException {
        if (inputStream == null) {
            throw new ZipException("inputstream is null, cannot add file to zip");
        } else if (zipParameters == null) {
            throw new ZipException("zip parameters are null");
        } else {
            setRunInThread(false);
            checkZipModel();
            if (this.zipModel == null) {
                throw new ZipException("internal error: zip model is null");
            } else if (!Zip4jUtil.checkFileExists(this.file) || !this.zipModel.isSplitArchive()) {
                new ZipEngine(this.zipModel).addStreamToZip(inputStream, zipParameters);
            } else {
                throw new ZipException("Zip file already exists. Zip file format does not allow updating split/spanned files");
            }
        }
    }

    public void createZipFile(File file2, ZipParameters zipParameters) throws ZipException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(file2);
        createZipFile(arrayList, zipParameters, false, -1);
    }

    public void createZipFile(File file2, ZipParameters zipParameters, boolean z, long j) throws ZipException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(file2);
        createZipFile(arrayList, zipParameters, z, j);
    }

    public void createZipFile(ArrayList arrayList, ZipParameters zipParameters) throws ZipException {
        createZipFile(arrayList, zipParameters, false, -1);
    }

    public void createZipFile(ArrayList arrayList, ZipParameters zipParameters, boolean z, long j) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(this.file)) {
            throw new ZipException("zip file path is empty");
        } else if (Zip4jUtil.checkFileExists(this.file)) {
            throw new ZipException("zip file: " + this.file + " already exists. To add files to existing zip file use addFile method");
        } else if (arrayList == null) {
            throw new ZipException("input file ArrayList is null, cannot create zip file");
        } else if (!Zip4jUtil.checkArrayListTypes(arrayList, 1)) {
            throw new ZipException("One or more elements in the input ArrayList is not of type File");
        } else {
            createNewZipModel();
            this.zipModel.setSplitArchive(z);
            this.zipModel.setSplitLength(j);
            addFiles(arrayList, zipParameters);
        }
    }

    public void createZipFileFromFolder(File file2, ZipParameters zipParameters, boolean z, long j) throws ZipException {
        if (file2 == null) {
            throw new ZipException("folderToAdd is null, cannot create zip file from folder");
        } else if (zipParameters == null) {
            throw new ZipException("input parameters are null, cannot create zip file from folder");
        } else if (Zip4jUtil.checkFileExists(this.file)) {
            throw new ZipException("zip file: " + this.file + " already exists. To add files to existing zip file use addFolder method");
        } else {
            createNewZipModel();
            this.zipModel.setSplitArchive(z);
            if (z) {
                this.zipModel.setSplitLength(j);
            }
            addFolder(file2, zipParameters, false);
        }
    }

    public void createZipFileFromFolder(String str, ZipParameters zipParameters, boolean z, long j) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("folderToAdd is empty or null, cannot create Zip File from folder");
        }
        createZipFileFromFolder(new File(str), zipParameters, z, j);
    }

    public void extractAll(String str) throws ZipException {
        extractAll(str, (UnzipParameters) null);
    }

    public void extractAll(String str, UnzipParameters unzipParameters) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("output path is null or invalid");
        } else if (!Zip4jUtil.checkOutputFolder(str)) {
            throw new ZipException("invalid output path");
        } else {
            if (this.zipModel == null) {
                readZipInfo();
            }
            if (this.zipModel == null) {
                throw new ZipException("Internal error occurred when extracting zip file");
            } else if (this.progressMonitor.getState() == 1) {
                throw new ZipException("invalid operation - Zip4j is in busy state");
            } else {
                new Unzip(this.zipModel).extractAll(unzipParameters, str, this.progressMonitor, this.runInThread);
            }
        }
    }

    public void extractFile(String str, String str2) throws ZipException {
        extractFile(str, str2, (UnzipParameters) null);
    }

    public void extractFile(String str, String str2, UnzipParameters unzipParameters) throws ZipException {
        extractFile(str, str2, unzipParameters, (String) null);
    }

    public void extractFile(String str, String str2, UnzipParameters unzipParameters, String str3) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("file to extract is null or empty, cannot extract file");
        } else if (!Zip4jUtil.isStringNotNullAndNotEmpty(str2)) {
            throw new ZipException("destination string path is empty or null, cannot extract file");
        } else {
            readZipInfo();
            FileHeader fileHeader = Zip4jUtil.getFileHeader(this.zipModel, str);
            if (fileHeader == null) {
                throw new ZipException("file header not found for given file name, cannot extract file");
            } else if (this.progressMonitor.getState() == 1) {
                throw new ZipException("invalid operation - Zip4j is in busy state");
            } else {
                fileHeader.extractFile(this.zipModel, str2, unzipParameters, str3, this.progressMonitor, this.runInThread);
            }
        }
    }

    public void extractFile(FileHeader fileHeader, String str) throws ZipException {
        extractFile(fileHeader, str, (UnzipParameters) null);
    }

    public void extractFile(FileHeader fileHeader, String str, UnzipParameters unzipParameters) throws ZipException {
        extractFile(fileHeader, str, unzipParameters, (String) null);
    }

    public void extractFile(FileHeader fileHeader, String str, UnzipParameters unzipParameters, String str2) throws ZipException {
        if (fileHeader == null) {
            throw new ZipException("input file header is null, cannot extract file");
        } else if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("destination path is empty or null, cannot extract file");
        } else {
            readZipInfo();
            if (this.progressMonitor.getState() == 1) {
                throw new ZipException("invalid operation - Zip4j is in busy state");
            }
            fileHeader.extractFile(this.zipModel, str, unzipParameters, str2, this.progressMonitor, this.runInThread);
        }
    }

    public String getComment() throws ZipException {
        return getComment((String) null);
    }

    public String getComment(String str) throws ZipException {
        if (str == null) {
            str = Zip4jUtil.isSupportedCharset(InternalZipTyphoonApp.CHARSET_COMMENTS_DEFAULT) ? InternalZipTyphoonApp.CHARSET_COMMENTS_DEFAULT : InternalZipTyphoonApp.CHARSET_DEFAULT;
        }
        if (Zip4jUtil.checkFileExists(this.file)) {
            checkZipModel();
            if (this.zipModel == null) {
                throw new ZipException("zip model is null, cannot read comment");
            } else if (this.zipModel.getEndCentralDirRecord() == null) {
                throw new ZipException("end of central directory record is null, cannot read comment");
            } else if (this.zipModel.getEndCentralDirRecord().getCommentBytes() == null || this.zipModel.getEndCentralDirRecord().getCommentBytes().length <= 0) {
                return null;
            } else {
                try {
                    return new String(this.zipModel.getEndCentralDirRecord().getCommentBytes(), str);
                } catch (UnsupportedEncodingException e) {
                    throw new ZipException((Throwable) e);
                }
            }
        } else {
            throw new ZipException("zip file does not exist, cannot read comment");
        }
    }

    public File getFile() {
        return new File(this.file);
    }

    public FileHeader getFileHeader(String str) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("input file name is emtpy or null, cannot get FileHeader");
        }
        readZipInfo();
        if (this.zipModel == null || this.zipModel.getCentralDirectory() == null) {
            return null;
        }
        return Zip4jUtil.getFileHeader(this.zipModel, str);
    }

    public List getFileHeaders() throws ZipException {
        readZipInfo();
        if (this.zipModel == null || this.zipModel.getCentralDirectory() == null) {
            return null;
        }
        return this.zipModel.getCentralDirectory().getFileHeaders();
    }

    public ZipInputStream getInputStream(FileHeader fileHeader) throws ZipException {
        if (fileHeader == null) {
            throw new ZipException("FileHeader is null, cannot get InputStream");
        }
        checkZipModel();
        if (this.zipModel != null) {
            return new Unzip(this.zipModel).getInputStream(fileHeader);
        }
        throw new ZipException("zip model is null, cannot get inputstream");
    }

    public ProgressMonitor getProgressMonitor() {
        return this.progressMonitor;
    }

    public ArrayList getSplitZipFiles() throws ZipException {
        checkZipModel();
        return Zip4jUtil.getSplitZipFiles(this.zipModel);
    }

    public boolean isEncrypted() throws ZipException {
        if (this.zipModel == null) {
            readZipInfo();
            if (this.zipModel == null) {
                throw new ZipException("Zip Model is null");
            }
        }
        if (this.zipModel.getCentralDirectory() == null || this.zipModel.getCentralDirectory().getFileHeaders() == null) {
            throw new ZipException("invalid zip file");
        }
        ArrayList fileHeaders = this.zipModel.getCentralDirectory().getFileHeaders();
        int i = 0;
        while (true) {
            if (i < fileHeaders.size()) {
                FileHeader fileHeader = (FileHeader) fileHeaders.get(i);
                if (fileHeader != null && fileHeader.isEncrypted()) {
                    this.isEncrypted = true;
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        return this.isEncrypted;
    }

    public boolean isRunInThread() {
        return this.runInThread;
    }

    public boolean isSplitArchive() throws ZipException {
        if (this.zipModel == null) {
            readZipInfo();
            if (this.zipModel == null) {
                throw new ZipException("Zip Model is null");
            }
        }
        return this.zipModel.isSplitArchive();
    }

    public boolean isValidZipFile() {
        try {
            readZipInfo();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void mergeSplitFiles(File file2) throws ZipException {
        if (file2 == null) {
            throw new ZipException("outputZipFile is null, cannot merge split files");
        } else if (file2.exists()) {
            throw new ZipException("output Zip File already exists");
        } else {
            checkZipModel();
            if (this.zipModel == null) {
                throw new ZipException("zip model is null, corrupt zip file?");
            }
            ArchiveMaintainer archiveMaintainer = new ArchiveMaintainer();
            archiveMaintainer.initProgressMonitorForMergeOp(this.zipModel, this.progressMonitor);
            archiveMaintainer.mergeSplitZipFiles(this.zipModel, file2, this.progressMonitor, this.runInThread);
        }
    }

    public void removeFile(String str) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("file name is empty or null, cannot remove file");
        }
        if (this.zipModel == null && Zip4jUtil.checkFileExists(this.file)) {
            readZipInfo();
        }
        if (this.zipModel.isSplitArchive()) {
            throw new ZipException("Zip file format does not allow updating split/spanned files");
        }
        FileHeader fileHeader = Zip4jUtil.getFileHeader(this.zipModel, str);
        if (fileHeader == null) {
            throw new ZipException("could not find file header for file: " + str);
        }
        removeFile(fileHeader);
    }

    public void removeFile(FileHeader fileHeader) throws ZipException {
        if (fileHeader == null) {
            throw new ZipException("file header is null, cannot remove file");
        }
        if (this.zipModel == null && Zip4jUtil.checkFileExists(this.file)) {
            readZipInfo();
        }
        if (this.zipModel.isSplitArchive()) {
            throw new ZipException("Zip file format does not allow updating split/spanned files");
        }
        ArchiveMaintainer archiveMaintainer = new ArchiveMaintainer();
        archiveMaintainer.initProgressMonitorForRemoveOp(this.zipModel, fileHeader, this.progressMonitor);
        archiveMaintainer.removeZipFile(this.zipModel, fileHeader, this.progressMonitor, this.runInThread);
    }

    public void setComment(String str) throws ZipException {
        if (str == null) {
            throw new ZipException("input comment is null, cannot update zip file");
        } else if (!Zip4jUtil.checkFileExists(this.file)) {
            throw new ZipException("zip file does not exist, cannot set comment for zip file");
        } else {
            readZipInfo();
            if (this.zipModel == null) {
                throw new ZipException("zipModel is null, cannot update zip file");
            } else if (this.zipModel.getEndCentralDirRecord() == null) {
                throw new ZipException("end of central directory is null, cannot set comment");
            } else {
                new ArchiveMaintainer().setComment(this.zipModel, str);
            }
        }
    }

    public void setFileNameCharset(String str) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new ZipException("null or empty charset name");
        } else if (!Zip4jUtil.isSupportedCharset(str)) {
            throw new ZipException("unsupported charset: " + str);
        } else {
            this.fileNameCharset = str;
        }
    }

    public void setPassword(String str) throws ZipException {
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(str)) {
            throw new NullPointerException();
        }
        setPassword(str.toCharArray());
    }

    public void setPassword(char[] cArr) throws ZipException {
        if (this.zipModel == null) {
            readZipInfo();
            if (this.zipModel == null) {
                throw new ZipException("Zip Model is null");
            }
        }
        if (this.zipModel.getCentralDirectory() == null || this.zipModel.getCentralDirectory().getFileHeaders() == null) {
            throw new ZipException("invalid zip file");
        }
        for (int i = 0; i < this.zipModel.getCentralDirectory().getFileHeaders().size(); i++) {
            if (this.zipModel.getCentralDirectory().getFileHeaders().get(i) != null && ((FileHeader) this.zipModel.getCentralDirectory().getFileHeaders().get(i)).isEncrypted()) {
                ((FileHeader) this.zipModel.getCentralDirectory().getFileHeaders().get(i)).setPassword(cArr);
            }
        }
    }

    public void setRunInThread(boolean z) {
        this.runInThread = z;
    }
}
