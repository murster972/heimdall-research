package net.lingala.zip4j.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.zip.Inflater;
import net.lingala.zip4j.unzip.UnzipEngine;

public class InflaterInputStream extends PartInputStream {
    private byte[] buff = new byte[4096];
    private long bytesWritten;
    private Inflater inflater = new Inflater(true);
    private byte[] oneByteBuff = new byte[1];
    private long uncompressedSize;
    private UnzipEngine unzipEngine;

    public InflaterInputStream(RandomAccessFile randomAccessFile, long j, long j2, UnzipEngine unzipEngine2) {
        super(randomAccessFile, j, j2, unzipEngine2);
        this.unzipEngine = unzipEngine2;
        this.bytesWritten = 0;
        this.uncompressedSize = unzipEngine2.getFileHeader().getUncompressedSize();
    }

    private void fill() throws IOException {
        int read = super.read(this.buff, 0, this.buff.length);
        if (read == -1) {
            throw new EOFException("Unexpected end of ZLIB input stream");
        }
        this.inflater.setInput(this.buff, 0, read);
    }

    private void finishInflating() throws IOException {
        do {
        } while (super.read(new byte[1024], 0, 1024) != -1);
        checkAndReadAESMacBytes();
    }

    public int available() {
        return this.inflater.finished() ? 0 : 1;
    }

    public void close() throws IOException {
        this.inflater.end();
        super.close();
    }

    public UnzipEngine getUnzipEngine() {
        return super.getUnzipEngine();
    }

    public int read() throws IOException {
        if (read(this.oneByteBuff, 0, 1) == -1) {
            return -1;
        }
        return this.oneByteBuff[0] & 255;
    }

    public int read(byte[] bArr) throws IOException {
        if (bArr != null) {
            return read(bArr, 0, bArr.length);
        }
        throw new NullPointerException("input buffer is null");
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 146 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read(byte[] r9, int r10, int r11) throws java.io.IOException {
        /*
            r8 = this;
            r3 = -1
            if (r9 != 0) goto L_0x000c
            java.lang.NullPointerException r3 = new java.lang.NullPointerException
            java.lang.String r4 = "input buffer is null"
            r3.<init>(r4)
            throw r3
        L_0x000c:
            if (r10 < 0) goto L_0x0014
            if (r11 < 0) goto L_0x0014
            int r4 = r9.length
            int r4 = r4 - r10
            if (r11 <= r4) goto L_0x001a
        L_0x0014:
            java.lang.IndexOutOfBoundsException r3 = new java.lang.IndexOutOfBoundsException
            r3.<init>()
            throw r3
        L_0x001a:
            if (r11 != 0) goto L_0x001e
            r1 = 0
        L_0x001d:
            return r1
        L_0x001e:
            long r4 = r8.bytesWritten     // Catch:{ DataFormatException -> 0x005a }
            long r6 = r8.uncompressedSize     // Catch:{ DataFormatException -> 0x005a }
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 < 0) goto L_0x0036
            r8.finishInflating()     // Catch:{ DataFormatException -> 0x005a }
            r1 = r3
            goto L_0x001d
        L_0x002b:
            java.util.zip.Inflater r4 = r8.inflater     // Catch:{ DataFormatException -> 0x005a }
            boolean r4 = r4.needsInput()     // Catch:{ DataFormatException -> 0x005a }
            if (r4 == 0) goto L_0x0036
            r8.fill()     // Catch:{ DataFormatException -> 0x005a }
        L_0x0036:
            java.util.zip.Inflater r4 = r8.inflater     // Catch:{ DataFormatException -> 0x005a }
            int r1 = r4.inflate(r9, r10, r11)     // Catch:{ DataFormatException -> 0x005a }
            if (r1 != 0) goto L_0x0053
            java.util.zip.Inflater r4 = r8.inflater     // Catch:{ DataFormatException -> 0x005a }
            boolean r4 = r4.finished()     // Catch:{ DataFormatException -> 0x005a }
            if (r4 != 0) goto L_0x004e
            java.util.zip.Inflater r4 = r8.inflater     // Catch:{ DataFormatException -> 0x005a }
            boolean r4 = r4.needsDictionary()     // Catch:{ DataFormatException -> 0x005a }
            if (r4 == 0) goto L_0x002b
        L_0x004e:
            r8.finishInflating()     // Catch:{ DataFormatException -> 0x005a }
            r1 = r3
            goto L_0x001d
        L_0x0053:
            long r4 = r8.bytesWritten     // Catch:{ DataFormatException -> 0x005a }
            long r6 = (long) r1     // Catch:{ DataFormatException -> 0x005a }
            long r4 = r4 + r6
            r8.bytesWritten = r4     // Catch:{ DataFormatException -> 0x005a }
            goto L_0x001d
        L_0x005a:
            r0 = move-exception
            java.lang.String r2 = "Invalid ZLIB data format"
            java.lang.String r3 = r0.getMessage()
            if (r3 == 0) goto L_0x0068
            java.lang.String r2 = r0.getMessage()
        L_0x0068:
            net.lingala.zip4j.unzip.UnzipEngine r3 = r8.unzipEngine
            if (r3 == 0) goto L_0x0098
            net.lingala.zip4j.unzip.UnzipEngine r3 = r8.unzipEngine
            net.lingala.zip4j.model.LocalFileHeader r3 = r3.getLocalFileHeader()
            boolean r3 = r3.isEncrypted()
            if (r3 == 0) goto L_0x0098
            net.lingala.zip4j.unzip.UnzipEngine r3 = r8.unzipEngine
            net.lingala.zip4j.model.LocalFileHeader r3 = r3.getLocalFileHeader()
            int r3 = r3.getEncryptionMethod()
            if (r3 != 0) goto L_0x0098
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r2)
            java.lang.String r4 = " - Wrong Password?"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r2 = r3.toString()
        L_0x0098:
            java.io.IOException r3 = new java.io.IOException
            r3.<init>(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: net.lingala.zip4j.io.InflaterInputStream.read(byte[], int, int):int");
    }

    public void seek(long j) throws IOException {
        super.seek(j);
    }

    public long skip(long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("negative skip length");
        }
        int min = (int) Math.min(j, 2147483647L);
        int i = 0;
        byte[] bArr = new byte[512];
        while (i < min) {
            int i2 = min - i;
            if (i2 > bArr.length) {
                i2 = bArr.length;
            }
            int read = read(bArr, 0, i2);
            if (read == -1) {
                break;
            }
            i += read;
        }
        return (long) i;
    }
}
