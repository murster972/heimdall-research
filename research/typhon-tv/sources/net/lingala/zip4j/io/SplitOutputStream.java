package net.lingala.zip4j.io;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.lingala.zip4j.util.Raw;
import net.lingala.zip4j.util.Zip4jUtil;

public class SplitOutputStream extends OutputStream {
    private long bytesWrittenForThisPart;
    private int currSplitFileCounter;
    private File outFile;
    private RandomAccessFile raf;
    private long splitLength;
    private File zipFile;

    public SplitOutputStream(File file) throws FileNotFoundException, ZipException {
        this(file, -1);
    }

    public SplitOutputStream(File file, long j) throws FileNotFoundException, ZipException {
        if (j < 0 || j >= PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
            this.raf = new RandomAccessFile(file, InternalZipTyphoonApp.WRITE_MODE);
            this.splitLength = j;
            this.outFile = file;
            this.zipFile = file;
            this.currSplitFileCounter = 0;
            this.bytesWrittenForThisPart = 0;
            return;
        }
        throw new ZipException("split length less than minimum allowed split length of 65536 Bytes");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SplitOutputStream(String str) throws FileNotFoundException, ZipException {
        this(Zip4jUtil.isStringNotNullAndNotEmpty(str) ? new File(str) : null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SplitOutputStream(String str, long j) throws FileNotFoundException, ZipException {
        this(!Zip4jUtil.isStringNotNullAndNotEmpty(str) ? new File(str) : null, j);
    }

    private boolean isHeaderData(byte[] bArr) {
        if (bArr == null || bArr.length < 4) {
            return false;
        }
        int readIntLittleEndian = Raw.readIntLittleEndian(bArr, 0);
        long[] allHeaderSignatures = Zip4jUtil.getAllHeaderSignatures();
        if (allHeaderSignatures == null || allHeaderSignatures.length <= 0) {
            return false;
        }
        for (int i = 0; i < allHeaderSignatures.length; i++) {
            if (allHeaderSignatures[i] != 134695760 && allHeaderSignatures[i] == ((long) readIntLittleEndian)) {
                return true;
            }
        }
        return false;
    }

    private void startNextSplitFile() throws IOException {
        try {
            String zipFileNameWithoutExt = Zip4jUtil.getZipFileNameWithoutExt(this.outFile.getName());
            String absolutePath = this.zipFile.getAbsolutePath();
            String str = this.outFile.getParent() == null ? "" : this.outFile.getParent() + System.getProperty("file.separator");
            File file = this.currSplitFileCounter < 9 ? new File(str + zipFileNameWithoutExt + ".z0" + (this.currSplitFileCounter + 1)) : new File(str + zipFileNameWithoutExt + ".z" + (this.currSplitFileCounter + 1));
            this.raf.close();
            if (file.exists()) {
                throw new IOException("split file: " + file.getName() + " already exists in the current directory, cannot rename this file");
            } else if (!this.zipFile.renameTo(file)) {
                throw new IOException("cannot rename newly created split file");
            } else {
                this.zipFile = new File(absolutePath);
                this.raf = new RandomAccessFile(this.zipFile, InternalZipTyphoonApp.WRITE_MODE);
                this.currSplitFileCounter++;
            }
        } catch (ZipException e) {
            throw new IOException(e.getMessage());
        }
    }

    public boolean checkBuffSizeAndStartNextSplitFile(int i) throws ZipException {
        if (i < 0) {
            throw new ZipException("negative buffersize for checkBuffSizeAndStartNextSplitFile");
        } else if (isBuffSizeFitForCurrSplitFile(i)) {
            return false;
        } else {
            try {
                startNextSplitFile();
                this.bytesWrittenForThisPart = 0;
                return true;
            } catch (IOException e) {
                throw new ZipException((Throwable) e);
            }
        }
    }

    public void close() throws IOException {
        if (this.raf != null) {
            this.raf.close();
        }
    }

    public void flush() throws IOException {
    }

    public int getCurrSplitFileCounter() {
        return this.currSplitFileCounter;
    }

    public long getFilePointer() throws IOException {
        return this.raf.getFilePointer();
    }

    public long getSplitLength() {
        return this.splitLength;
    }

    public boolean isBuffSizeFitForCurrSplitFile(int i) throws ZipException {
        if (i >= 0) {
            return this.splitLength < PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH || this.bytesWrittenForThisPart + ((long) i) <= this.splitLength;
        }
        throw new ZipException("negative buffersize for isBuffSizeFitForCurrSplitFile");
    }

    public boolean isSplitZipFile() {
        return this.splitLength != -1;
    }

    public void seek(long j) throws IOException {
        this.raf.seek(j);
    }

    public void write(int i) throws IOException {
        write(new byte[]{(byte) i}, 0, 1);
    }

    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) throws IOException {
        if (i2 > 0) {
            if (this.splitLength == -1) {
                this.raf.write(bArr, i, i2);
                this.bytesWrittenForThisPart += (long) i2;
            } else if (this.splitLength < PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                throw new IOException("split length less than minimum allowed split length of 65536 Bytes");
            } else if (this.bytesWrittenForThisPart >= this.splitLength) {
                startNextSplitFile();
                this.raf.write(bArr, i, i2);
                this.bytesWrittenForThisPart = (long) i2;
            } else if (this.bytesWrittenForThisPart + ((long) i2) <= this.splitLength) {
                this.raf.write(bArr, i, i2);
                this.bytesWrittenForThisPart += (long) i2;
            } else if (isHeaderData(bArr)) {
                startNextSplitFile();
                this.raf.write(bArr, i, i2);
                this.bytesWrittenForThisPart = (long) i2;
            } else {
                this.raf.write(bArr, i, (int) (this.splitLength - this.bytesWrittenForThisPart));
                startNextSplitFile();
                this.raf.write(bArr, ((int) (this.splitLength - this.bytesWrittenForThisPart)) + i, (int) (((long) i2) - (this.splitLength - this.bytesWrittenForThisPart)));
                this.bytesWrittenForThisPart = ((long) i2) - (this.splitLength - this.bytesWrittenForThisPart);
            }
        }
    }
}
