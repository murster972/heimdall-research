package net.lingala.zip4j.io;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.zip.CRC32;
import net.lingala.zip4j.core.HeaderWriter;
import net.lingala.zip4j.crypto.AESEncrpyter;
import net.lingala.zip4j.crypto.IEncrypter;
import net.lingala.zip4j.crypto.StandardEncrypter;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.AESExtraDataRecord;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.LocalFileHeader;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.lingala.zip4j.util.Raw;
import net.lingala.zip4j.util.Zip4jUtil;

public class CipherOutputStream extends BaseOutputStream {
    private long bytesWrittenForThisFile = 0;
    protected CRC32 crc = new CRC32();
    private IEncrypter encrypter;
    protected FileHeader fileHeader;
    protected LocalFileHeader localFileHeader;
    protected OutputStream outputStream;
    private byte[] pendingBuffer = new byte[16];
    private int pendingBufferLength = 0;
    private File sourceFile;
    private long totalBytesRead = 0;
    private long totalBytesWritten = 0;
    protected ZipModel zipModel;
    protected ZipParameters zipParameters;

    public CipherOutputStream(OutputStream outputStream2, ZipModel zipModel2) {
        this.outputStream = outputStream2;
        initZipModel(zipModel2);
    }

    private void createFileHeader() throws ZipException {
        String relativeFileName;
        int i;
        this.fileHeader = new FileHeader();
        this.fileHeader.setSignature(33639248);
        this.fileHeader.setVersionMadeBy(20);
        this.fileHeader.setVersionNeededToExtract(20);
        if (!this.zipParameters.isEncryptFiles() || this.zipParameters.getEncryptionMethod() != 99) {
            this.fileHeader.setCompressionMethod(this.zipParameters.getCompressionMethod());
        } else {
            this.fileHeader.setCompressionMethod(99);
            this.fileHeader.setAesExtraDataRecord(generateAESExtraDataRecord(this.zipParameters));
        }
        if (this.zipParameters.isEncryptFiles()) {
            this.fileHeader.setEncrypted(true);
            this.fileHeader.setEncryptionMethod(this.zipParameters.getEncryptionMethod());
        }
        if (this.zipParameters.isSourceExternalStream()) {
            this.fileHeader.setLastModFileTime((int) Zip4jUtil.javaToDosTime(System.currentTimeMillis()));
            if (!Zip4jUtil.isStringNotNullAndNotEmpty(this.zipParameters.getFileNameInZip())) {
                throw new ZipException("fileNameInZip is null or empty");
            }
            relativeFileName = this.zipParameters.getFileNameInZip();
        } else {
            this.fileHeader.setLastModFileTime((int) Zip4jUtil.javaToDosTime(Zip4jUtil.getLastModifiedFileTime(this.sourceFile, this.zipParameters.getTimeZone())));
            this.fileHeader.setUncompressedSize(this.sourceFile.length());
            relativeFileName = Zip4jUtil.getRelativeFileName(this.sourceFile.getAbsolutePath(), this.zipParameters.getRootFolderInZip(), this.zipParameters.getDefaultFolderPath());
        }
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(relativeFileName)) {
            throw new ZipException("fileName is null or empty. unable to create file header");
        }
        this.fileHeader.setFileName(relativeFileName);
        if (Zip4jUtil.isStringNotNullAndNotEmpty(this.zipModel.getFileNameCharset())) {
            this.fileHeader.setFileNameLength(Zip4jUtil.getEncodedStringLength(relativeFileName, this.zipModel.getFileNameCharset()));
        } else {
            this.fileHeader.setFileNameLength(Zip4jUtil.getEncodedStringLength(relativeFileName));
        }
        if (this.outputStream instanceof SplitOutputStream) {
            this.fileHeader.setDiskNumberStart(((SplitOutputStream) this.outputStream).getCurrSplitFileCounter());
        } else {
            this.fileHeader.setDiskNumberStart(0);
        }
        int i2 = 0;
        if (!this.zipParameters.isSourceExternalStream()) {
            i2 = getFileAttributes(this.sourceFile);
        }
        this.fileHeader.setExternalFileAttr(new byte[]{(byte) i2, 0, 0, 0});
        if (this.zipParameters.isSourceExternalStream()) {
            this.fileHeader.setDirectory(relativeFileName.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) || relativeFileName.endsWith("\\"));
        } else {
            this.fileHeader.setDirectory(this.sourceFile.isDirectory());
        }
        if (this.fileHeader.isDirectory()) {
            this.fileHeader.setCompressedSize(0);
            this.fileHeader.setUncompressedSize(0);
        } else if (!this.zipParameters.isSourceExternalStream()) {
            long fileLengh = Zip4jUtil.getFileLengh(this.sourceFile);
            if (this.zipParameters.getCompressionMethod() != 0) {
                this.fileHeader.setCompressedSize(0);
            } else if (this.zipParameters.getEncryptionMethod() == 0) {
                this.fileHeader.setCompressedSize(12 + fileLengh);
            } else if (this.zipParameters.getEncryptionMethod() == 99) {
                switch (this.zipParameters.getAesKeyStrength()) {
                    case 1:
                        i = 8;
                        break;
                    case 3:
                        i = 16;
                        break;
                    default:
                        throw new ZipException("invalid aes key strength, cannot determine key sizes");
                }
                this.fileHeader.setCompressedSize(((long) i) + fileLengh + 10 + 2);
            } else {
                this.fileHeader.setCompressedSize(0);
            }
            this.fileHeader.setUncompressedSize(fileLengh);
        }
        if (this.zipParameters.isEncryptFiles() && this.zipParameters.getEncryptionMethod() == 0) {
            this.fileHeader.setCrc32((long) this.zipParameters.getSourceFileCRC());
        }
        byte[] bArr = new byte[2];
        bArr[0] = Raw.bitArrayToByte(generateGeneralPurposeBitArray(this.fileHeader.isEncrypted(), this.zipParameters.getCompressionMethod()));
        boolean isStringNotNullAndNotEmpty = Zip4jUtil.isStringNotNullAndNotEmpty(this.zipModel.getFileNameCharset());
        if ((!isStringNotNullAndNotEmpty || !this.zipModel.getFileNameCharset().equalsIgnoreCase(InternalZipTyphoonApp.CHARSET_UTF8)) && (isStringNotNullAndNotEmpty || !Zip4jUtil.detectCharSet(this.fileHeader.getFileName()).equals(InternalZipTyphoonApp.CHARSET_UTF8))) {
            bArr[1] = 0;
        } else {
            bArr[1] = 8;
        }
        this.fileHeader.setGeneralPurposeFlag(bArr);
    }

    private void createLocalFileHeader() throws ZipException {
        if (this.fileHeader == null) {
            throw new ZipException("file header is null, cannot create local file header");
        }
        this.localFileHeader = new LocalFileHeader();
        this.localFileHeader.setSignature(67324752);
        this.localFileHeader.setVersionNeededToExtract(this.fileHeader.getVersionNeededToExtract());
        this.localFileHeader.setCompressionMethod(this.fileHeader.getCompressionMethod());
        this.localFileHeader.setLastModFileTime(this.fileHeader.getLastModFileTime());
        this.localFileHeader.setUncompressedSize(this.fileHeader.getUncompressedSize());
        this.localFileHeader.setFileNameLength(this.fileHeader.getFileNameLength());
        this.localFileHeader.setFileName(this.fileHeader.getFileName());
        this.localFileHeader.setEncrypted(this.fileHeader.isEncrypted());
        this.localFileHeader.setEncryptionMethod(this.fileHeader.getEncryptionMethod());
        this.localFileHeader.setAesExtraDataRecord(this.fileHeader.getAesExtraDataRecord());
        this.localFileHeader.setCrc32(this.fileHeader.getCrc32());
        this.localFileHeader.setCompressedSize(this.fileHeader.getCompressedSize());
        this.localFileHeader.setGeneralPurposeFlag((byte[]) this.fileHeader.getGeneralPurposeFlag().clone());
    }

    private void encryptAndWrite(byte[] bArr, int i, int i2) throws IOException {
        if (this.encrypter != null) {
            try {
                this.encrypter.encryptData(bArr, i, i2);
            } catch (ZipException e) {
                throw new IOException(e.getMessage());
            }
        }
        this.outputStream.write(bArr, i, i2);
        this.totalBytesWritten += (long) i2;
        this.bytesWrittenForThisFile += (long) i2;
    }

    private AESExtraDataRecord generateAESExtraDataRecord(ZipParameters zipParameters2) throws ZipException {
        if (zipParameters2 == null) {
            throw new ZipException("zip parameters are null, cannot generate AES Extra Data record");
        }
        AESExtraDataRecord aESExtraDataRecord = new AESExtraDataRecord();
        aESExtraDataRecord.setSignature(39169);
        aESExtraDataRecord.setDataSize(7);
        aESExtraDataRecord.setVendorID("AE");
        aESExtraDataRecord.setVersionNumber(2);
        if (zipParameters2.getAesKeyStrength() == 1) {
            aESExtraDataRecord.setAesStrength(1);
        } else if (zipParameters2.getAesKeyStrength() == 3) {
            aESExtraDataRecord.setAesStrength(3);
        } else {
            throw new ZipException("invalid AES key strength, cannot generate AES Extra data record");
        }
        aESExtraDataRecord.setCompressionMethod(zipParameters2.getCompressionMethod());
        return aESExtraDataRecord;
    }

    private int[] generateGeneralPurposeBitArray(boolean z, int i) {
        int[] iArr = new int[8];
        if (z) {
            iArr[0] = 1;
        } else {
            iArr[0] = 0;
        }
        if (i != 8) {
            iArr[1] = 0;
            iArr[2] = 0;
        }
        iArr[3] = 1;
        return iArr;
    }

    private int getFileAttributes(File file) throws ZipException {
        if (file == null) {
            throw new ZipException("input file is null, cannot get file attributes");
        } else if (!file.exists()) {
            return 0;
        } else {
            if (file.isDirectory()) {
                return file.isHidden() ? 18 : 16;
            }
            if (!file.canWrite() && file.isHidden()) {
                return 3;
            }
            if (!file.canWrite()) {
                return 1;
            }
            return file.isHidden() ? 2 : 0;
        }
    }

    private void initEncrypter() throws ZipException {
        if (!this.zipParameters.isEncryptFiles()) {
            this.encrypter = null;
            return;
        }
        switch (this.zipParameters.getEncryptionMethod()) {
            case 0:
                this.encrypter = new StandardEncrypter(this.zipParameters.getPassword(), (this.localFileHeader.getLastModFileTime() & 65535) << 16);
                return;
            case 99:
                this.encrypter = new AESEncrpyter(this.zipParameters.getPassword(), this.zipParameters.getAesKeyStrength());
                return;
            default:
                throw new ZipException("invalid encprytion method");
        }
    }

    private void initZipModel(ZipModel zipModel2) {
        if (zipModel2 == null) {
            this.zipModel = new ZipModel();
        } else {
            this.zipModel = zipModel2;
        }
        if (this.zipModel.getEndCentralDirRecord() == null) {
            this.zipModel.setEndCentralDirRecord(new EndCentralDirRecord());
        }
        if (this.zipModel.getCentralDirectory() == null) {
            this.zipModel.setCentralDirectory(new CentralDirectory());
        }
        if (this.zipModel.getCentralDirectory().getFileHeaders() == null) {
            this.zipModel.getCentralDirectory().setFileHeaders(new ArrayList());
        }
        if (this.zipModel.getLocalFileHeaderList() == null) {
            this.zipModel.setLocalFileHeaderList(new ArrayList());
        }
        if ((this.outputStream instanceof SplitOutputStream) && ((SplitOutputStream) this.outputStream).isSplitZipFile()) {
            this.zipModel.setSplitArchive(true);
            this.zipModel.setSplitLength(((SplitOutputStream) this.outputStream).getSplitLength());
        }
        this.zipModel.getEndCentralDirRecord().setSignature(InternalZipTyphoonApp.ENDSIG);
    }

    public void close() throws IOException {
        if (this.outputStream != null) {
            this.outputStream.close();
        }
    }

    public void closeEntry() throws IOException, ZipException {
        if (this.pendingBufferLength != 0) {
            encryptAndWrite(this.pendingBuffer, 0, this.pendingBufferLength);
            this.pendingBufferLength = 0;
        }
        if (this.zipParameters.isEncryptFiles() && this.zipParameters.getEncryptionMethod() == 99) {
            if (this.encrypter instanceof AESEncrpyter) {
                this.outputStream.write(((AESEncrpyter) this.encrypter).getFinalMac());
                this.bytesWrittenForThisFile += 10;
                this.totalBytesWritten += 10;
            } else {
                throw new ZipException("invalid encrypter for AES encrypted file");
            }
        }
        this.fileHeader.setCompressedSize(this.bytesWrittenForThisFile);
        this.localFileHeader.setCompressedSize(this.bytesWrittenForThisFile);
        if (this.zipParameters.isSourceExternalStream()) {
            this.fileHeader.setUncompressedSize(this.totalBytesRead);
            if (this.localFileHeader.getUncompressedSize() != this.totalBytesRead) {
                this.localFileHeader.setUncompressedSize(this.totalBytesRead);
            }
        }
        long value = this.crc.getValue();
        if (this.fileHeader.isEncrypted() && this.fileHeader.getEncryptionMethod() == 99) {
            value = 0;
        }
        if (!this.zipParameters.isEncryptFiles() || this.zipParameters.getEncryptionMethod() != 99) {
            this.fileHeader.setCrc32(value);
            this.localFileHeader.setCrc32(value);
        } else {
            this.fileHeader.setCrc32(0);
            this.localFileHeader.setCrc32(0);
        }
        this.zipModel.getLocalFileHeaderList().add(this.localFileHeader);
        this.zipModel.getCentralDirectory().getFileHeaders().add(this.fileHeader);
        this.totalBytesWritten += (long) new HeaderWriter().writeExtendedLocalHeader(this.localFileHeader, this.outputStream);
        this.crc.reset();
        this.bytesWrittenForThisFile = 0;
        this.encrypter = null;
        this.totalBytesRead = 0;
    }

    public void decrementCompressedFileSize(int i) {
        if (i > 0 && ((long) i) <= this.bytesWrittenForThisFile) {
            this.bytesWrittenForThisFile -= (long) i;
        }
    }

    public void finish() throws IOException, ZipException {
        this.zipModel.getEndCentralDirRecord().setOffsetOfStartOfCentralDir(this.totalBytesWritten);
        new HeaderWriter().finalizeZipFile(this.zipModel, this.outputStream);
    }

    public File getSourceFile() {
        return this.sourceFile;
    }

    public void putNextEntry(File file, ZipParameters zipParameters2) throws ZipException {
        if (!zipParameters2.isSourceExternalStream() && file == null) {
            throw new ZipException("input file is null");
        } else if (zipParameters2.isSourceExternalStream() || Zip4jUtil.checkFileExists(file)) {
            try {
                this.sourceFile = file;
                this.zipParameters = (ZipParameters) zipParameters2.clone();
                if (!zipParameters2.isSourceExternalStream()) {
                    if (this.sourceFile.isDirectory()) {
                        this.zipParameters.setEncryptFiles(false);
                        this.zipParameters.setEncryptionMethod(-1);
                        this.zipParameters.setCompressionMethod(0);
                    }
                } else if (!Zip4jUtil.isStringNotNullAndNotEmpty(this.zipParameters.getFileNameInZip())) {
                    throw new ZipException("file name is empty for external stream");
                } else if (this.zipParameters.getFileNameInZip().endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) || this.zipParameters.getFileNameInZip().endsWith("\\")) {
                    this.zipParameters.setEncryptFiles(false);
                    this.zipParameters.setEncryptionMethod(-1);
                    this.zipParameters.setCompressionMethod(0);
                }
                createFileHeader();
                createLocalFileHeader();
                if (this.zipModel.isSplitArchive() && (this.zipModel.getCentralDirectory() == null || this.zipModel.getCentralDirectory().getFileHeaders() == null || this.zipModel.getCentralDirectory().getFileHeaders().size() == 0)) {
                    byte[] bArr = new byte[4];
                    Raw.writeIntLittleEndian(bArr, 0, 134695760);
                    this.outputStream.write(bArr);
                    this.totalBytesWritten += 4;
                }
                if (this.outputStream instanceof SplitOutputStream) {
                    if (this.totalBytesWritten == 4) {
                        this.fileHeader.setOffsetLocalHeader(4);
                    } else {
                        this.fileHeader.setOffsetLocalHeader(((SplitOutputStream) this.outputStream).getFilePointer());
                    }
                } else if (this.totalBytesWritten == 4) {
                    this.fileHeader.setOffsetLocalHeader(4);
                } else {
                    this.fileHeader.setOffsetLocalHeader(this.totalBytesWritten);
                }
                this.totalBytesWritten += (long) new HeaderWriter().writeLocalFileHeader(this.zipModel, this.localFileHeader, this.outputStream);
                if (this.zipParameters.isEncryptFiles()) {
                    initEncrypter();
                    if (this.encrypter != null) {
                        if (zipParameters2.getEncryptionMethod() == 0) {
                            byte[] headerBytes = ((StandardEncrypter) this.encrypter).getHeaderBytes();
                            this.outputStream.write(headerBytes);
                            this.totalBytesWritten += (long) headerBytes.length;
                            this.bytesWrittenForThisFile += (long) headerBytes.length;
                        } else if (zipParameters2.getEncryptionMethod() == 99) {
                            byte[] saltBytes = ((AESEncrpyter) this.encrypter).getSaltBytes();
                            byte[] derivedPasswordVerifier = ((AESEncrpyter) this.encrypter).getDerivedPasswordVerifier();
                            this.outputStream.write(saltBytes);
                            this.outputStream.write(derivedPasswordVerifier);
                            this.totalBytesWritten += (long) (saltBytes.length + derivedPasswordVerifier.length);
                            this.bytesWrittenForThisFile += (long) (saltBytes.length + derivedPasswordVerifier.length);
                        }
                    }
                }
                this.crc.reset();
            } catch (CloneNotSupportedException e) {
                throw new ZipException((Throwable) e);
            } catch (ZipException e2) {
                throw e2;
            } catch (Exception e3) {
                throw new ZipException((Throwable) e3);
            }
        } else {
            throw new ZipException("input file does not exist");
        }
    }

    public void setSourceFile(File file) {
        this.sourceFile = file;
    }

    /* access modifiers changed from: protected */
    public void updateTotalBytesRead(int i) {
        if (i > 0) {
            this.totalBytesRead += (long) i;
        }
    }

    public void write(int i) throws IOException {
        write(new byte[]{(byte) i}, 0, 1);
    }

    public void write(byte[] bArr) throws IOException {
        if (bArr == null) {
            throw new NullPointerException();
        } else if (bArr.length != 0) {
            write(bArr, 0, bArr.length);
        }
    }

    public void write(byte[] bArr, int i, int i2) throws IOException {
        if (i2 != 0) {
            if (this.zipParameters.isEncryptFiles() && this.zipParameters.getEncryptionMethod() == 99) {
                if (this.pendingBufferLength != 0) {
                    if (i2 >= 16 - this.pendingBufferLength) {
                        System.arraycopy(bArr, i, this.pendingBuffer, this.pendingBufferLength, 16 - this.pendingBufferLength);
                        encryptAndWrite(this.pendingBuffer, 0, this.pendingBuffer.length);
                        i = 16 - this.pendingBufferLength;
                        i2 -= i;
                        this.pendingBufferLength = 0;
                    } else {
                        System.arraycopy(bArr, i, this.pendingBuffer, this.pendingBufferLength, i2);
                        this.pendingBufferLength += i2;
                        return;
                    }
                }
                if (!(i2 == 0 || i2 % 16 == 0)) {
                    System.arraycopy(bArr, (i2 + i) - (i2 % 16), this.pendingBuffer, 0, i2 % 16);
                    this.pendingBufferLength = i2 % 16;
                    i2 -= this.pendingBufferLength;
                }
            }
            if (i2 != 0) {
                encryptAndWrite(bArr, i, i2);
            }
        }
    }
}
