package net.lingala.zip4j.io;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.io.RandomAccessFile;
import net.lingala.zip4j.crypto.AESDecrypter;
import net.lingala.zip4j.crypto.IDecrypter;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.unzip.UnzipEngine;

public class PartInputStream extends BaseInputStream {
    private byte[] aesBlockByte = new byte[16];
    private int aesBytesReturned = 0;
    private long bytesRead;
    private int count = -1;
    private IDecrypter decrypter;
    private boolean isAESEncryptedFile = false;
    private long length;
    private byte[] oneByteBuff = new byte[1];
    private RandomAccessFile raf;
    private UnzipEngine unzipEngine;

    public PartInputStream(RandomAccessFile randomAccessFile, long j, long j2, UnzipEngine unzipEngine2) {
        boolean z = true;
        this.raf = randomAccessFile;
        this.unzipEngine = unzipEngine2;
        this.decrypter = unzipEngine2.getDecrypter();
        this.bytesRead = 0;
        this.length = j2;
        this.isAESEncryptedFile = (!unzipEngine2.getFileHeader().isEncrypted() || unzipEngine2.getFileHeader().getEncryptionMethod() != 99) ? false : z;
    }

    public int available() {
        long j = this.length - this.bytesRead;
        return j > 2147483647L ? MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT : (int) j;
    }

    /* access modifiers changed from: protected */
    public void checkAndReadAESMacBytes() throws IOException {
        if (this.isAESEncryptedFile && this.decrypter != null && (this.decrypter instanceof AESDecrypter) && ((AESDecrypter) this.decrypter).getStoredMac() == null) {
            byte[] bArr = new byte[10];
            int read = this.raf.read(bArr);
            if (read != 10) {
                if (this.unzipEngine.getZipModel().isSplitArchive()) {
                    this.raf.close();
                    this.raf = this.unzipEngine.startNextSplitFile();
                    int read2 = read + this.raf.read(bArr, read, 10 - read);
                } else {
                    throw new IOException("Error occured while reading stored AES authentication bytes");
                }
            }
            ((AESDecrypter) this.unzipEngine.getDecrypter()).setStoredMac(bArr);
        }
    }

    public void close() throws IOException {
        this.raf.close();
    }

    public UnzipEngine getUnzipEngine() {
        return this.unzipEngine;
    }

    public int read() throws IOException {
        if (this.bytesRead >= this.length) {
            return -1;
        }
        if (this.isAESEncryptedFile) {
            if (this.aesBytesReturned == 0 || this.aesBytesReturned == 16) {
                if (read(this.aesBlockByte) == -1) {
                    return -1;
                }
                this.aesBytesReturned = 0;
            }
            byte[] bArr = this.aesBlockByte;
            int i = this.aesBytesReturned;
            this.aesBytesReturned = i + 1;
            return bArr[i] & 255;
        } else if (read(this.oneByteBuff, 0, 1) != -1) {
            return this.oneByteBuff[0] & 255;
        } else {
            return -1;
        }
    }

    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (((long) i2) <= this.length - this.bytesRead || (i2 = (int) (this.length - this.bytesRead)) != 0) {
            if ((this.unzipEngine.getDecrypter() instanceof AESDecrypter) && this.bytesRead + ((long) i2) < this.length && i2 % 16 != 0) {
                i2 -= i2 % 16;
            }
            synchronized (this.raf) {
                this.count = this.raf.read(bArr, i, i2);
                if (this.count < i2 && this.unzipEngine.getZipModel().isSplitArchive()) {
                    this.raf.close();
                    this.raf = this.unzipEngine.startNextSplitFile();
                    if (this.count < 0) {
                        this.count = 0;
                    }
                    int read = this.raf.read(bArr, this.count, i2 - this.count);
                    if (read > 0) {
                        this.count += read;
                    }
                }
            }
            if (this.count > 0) {
                if (this.decrypter != null) {
                    try {
                        this.decrypter.decryptData(bArr, i, this.count);
                    } catch (ZipException e) {
                        throw new IOException(e.getMessage());
                    }
                }
                this.bytesRead += (long) this.count;
            }
            if (this.bytesRead >= this.length) {
                checkAndReadAESMacBytes();
            }
            return this.count;
        }
        checkAndReadAESMacBytes();
        return -1;
    }

    public void seek(long j) throws IOException {
        this.raf.seek(j);
    }

    public long skip(long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException();
        }
        if (j > this.length - this.bytesRead) {
            j = this.length - this.bytesRead;
        }
        this.bytesRead += j;
        return j;
    }
}
