package net.danlew.android.joda;

import android.content.Context;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import net.danlew.android.joda.R;
import org.joda.time.DateTimeZone;
import org.joda.time.tz.DateTimeZoneBuilder;
import org.joda.time.tz.Provider;

public class ResourceZoneInfoProvider implements Provider {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<String, Object> f15867;

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f15868;

    public ResourceZoneInfoProvider(Context context) throws IOException {
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null");
        }
        this.f15868 = context.getApplicationContext();
        this.f15867 = m19862(m19860("ZoneInfoMap"));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private InputStream m19860(String str) throws IOException {
        if (this.f15868 == null) {
            throw new RuntimeException("Need to call JodaTimeAndroid.init() before using joda-time-android");
        }
        String r2 = ResUtils.m19859(str);
        int r1 = ResUtils.m19858(R.raw.class, r2);
        if (r1 != 0) {
            return this.f15868.getResources().openRawResource(r1);
        }
        throw new IOException("Resource not found: \"" + str + "\" (resName: \"" + r2 + "\")");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private DateTimeZone m19861(String str) {
        DateTimeZone dateTimeZone;
        InputStream inputStream = null;
        try {
            inputStream = m19860(str);
            dateTimeZone = DateTimeZoneBuilder.m21636(inputStream, str);
            this.f15867.put(str, new SoftReference(dateTimeZone));
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        } catch (IOException e2) {
            m19866((Exception) e2);
            this.f15867.remove(str);
            dateTimeZone = null;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                }
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
        return dateTimeZone;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Map<String, Object> m19862(InputStream inputStream) throws IOException {
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        try {
            m19863(dataInputStream, concurrentHashMap);
            concurrentHashMap.put("UTC", new SoftReference(DateTimeZone.UTC));
            return concurrentHashMap;
        } finally {
            try {
                dataInputStream.close();
            } catch (IOException e) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m19863(DataInputStream dataInputStream, Map<String, Object> map) throws IOException {
        int readUnsignedShort = dataInputStream.readUnsignedShort();
        String[] strArr = new String[readUnsignedShort];
        for (int i = 0; i < readUnsignedShort; i++) {
            strArr[i] = dataInputStream.readUTF().intern();
        }
        int readUnsignedShort2 = dataInputStream.readUnsignedShort();
        int i2 = 0;
        while (i2 < readUnsignedShort2) {
            try {
                map.put(strArr[dataInputStream.readUnsignedShort()], strArr[dataInputStream.readUnsignedShort()]);
                i2++;
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IOException("Corrupt zone info map");
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Set<String> m19864() {
        return new TreeSet(this.f15867.keySet());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeZone m19865(String str) {
        Object obj;
        if (str == null || (obj = this.f15867.get(str)) == null) {
            return null;
        }
        if (str.equals(obj)) {
            return m19861(str);
        }
        if (!(obj instanceof SoftReference)) {
            return m19865((String) obj);
        }
        DateTimeZone dateTimeZone = (DateTimeZone) ((SoftReference) obj).get();
        return dateTimeZone == null ? m19861(str) : dateTimeZone;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19866(Exception exc) {
        exc.printStackTrace();
    }
}
