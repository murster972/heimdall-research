package net.danlew.android.joda;

import android.content.Context;
import android.content.IntentFilter;
import java.io.IOException;
import org.joda.time.DateTimeZone;

public final class JodaTimeAndroid {

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean f6182 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m6827(Context context) {
        if (!f6182) {
            f6182 = true;
            try {
                DateTimeZone.setProvider(new ResourceZoneInfoProvider(context));
                context.getApplicationContext().registerReceiver(new TimeZoneChangedReceiver(), new IntentFilter("android.intent.action.TIMEZONE_CHANGED"));
            } catch (IOException e) {
                throw new RuntimeException("Could not read ZoneInfoMap. You are probably using Proguard wrong.", e);
            }
        }
    }
}
