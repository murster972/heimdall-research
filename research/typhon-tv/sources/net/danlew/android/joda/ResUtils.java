package net.danlew.android.joda;

import android.util.Log;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ResUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static Map<Class<?>, Map<String, Integer>> f15866 = new ConcurrentHashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m19857(String str) {
        File file = new File(str);
        ArrayList arrayList = new ArrayList();
        do {
            arrayList.add(file.getName());
            file = file.getParentFile();
        } while (file != null);
        StringBuffer stringBuffer = new StringBuffer();
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (stringBuffer.length() > 0) {
                stringBuffer.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            }
            stringBuffer.append((String) arrayList.get(size));
        }
        return stringBuffer.toString().replace('-', '_').replace("+", "plus").toLowerCase(Locale.US);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m19858(Class<?> cls, String str) {
        Map map;
        if (!f15866.containsKey(cls)) {
            map = new ConcurrentHashMap();
            f15866.put(cls, map);
        } else {
            map = f15866.get(cls);
        }
        if (map.containsKey(str)) {
            return ((Integer) map.get(str)).intValue();
        }
        try {
            int i = cls.getField(str).getInt((Object) null);
            if (i == 0) {
                return i;
            }
            map.put(str, Integer.valueOf(i));
            return i;
        } catch (Exception e) {
            Log.e("JodaTimeAndroid", "Failed to retrieve identifier: type=" + cls + " name=" + str, e);
            return 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19859(String str) {
        return "joda_" + m19857(str);
    }
}
