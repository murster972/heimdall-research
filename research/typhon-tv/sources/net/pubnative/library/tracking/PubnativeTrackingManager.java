package net.pubnative.library.tracking;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import net.pubnative.library.network.PubnativeHttpRequest;
import net.pubnative.library.tracking.model.PubnativeTrackingURLModel;

public class PubnativeTrackingManager {
    private static final long ITEM_VALIDITY_TIME = 1800000;
    protected static final String SHARED_FAILED_LIST = "failed";
    protected static final String SHARED_PENDING_LIST = "pending";
    private static final String SHARED_PREFERENCES = "net.pubnative.library.tracking.PubnativeTrackingManager";
    /* access modifiers changed from: private */
    public static final String TAG = PubnativeTrackingManager.class.getSimpleName();
    /* access modifiers changed from: private */
    public static boolean sIsTracking = false;

    protected static PubnativeTrackingURLModel dequeueItem(Context context, String str) {
        Log.v(TAG, "dequeueItem: " + str);
        List<PubnativeTrackingURLModel> list = getList(context, str);
        if (list.size() <= 0) {
            return null;
        }
        PubnativeTrackingURLModel pubnativeTrackingURLModel = list.get(0);
        list.remove(0);
        setList(context, str, list);
        return pubnativeTrackingURLModel;
    }

    protected static void enqueueFailedList(Context context) {
        Log.v(TAG, "enqueueFailedList");
        List<PubnativeTrackingURLModel> list = getList(context, SHARED_FAILED_LIST);
        List<PubnativeTrackingURLModel> list2 = getList(context, SHARED_PENDING_LIST);
        list2.addAll(list);
        setList(context, SHARED_PENDING_LIST, list2);
        list.clear();
        setList(context, SHARED_FAILED_LIST, list);
    }

    protected static void enqueueItem(Context context, String str, PubnativeTrackingURLModel pubnativeTrackingURLModel) {
        Log.v(TAG, "enqueueItem: " + str);
        List<PubnativeTrackingURLModel> list = getList(context, str);
        list.add(pubnativeTrackingURLModel);
        setList(context, str, list);
    }

    protected static List<PubnativeTrackingURLModel> getList(Context context, String str) {
        Log.v(TAG, "getList: " + str);
        String string = getSharedPreferences(context).getString(str, (String) null);
        return string == null ? new ArrayList() : (List) new Gson().fromJson(string, new TypeToken<List<PubnativeTrackingURLModel>>() {
        }.getType());
    }

    protected static SharedPreferences getSharedPreferences(Context context) {
        Log.v(TAG, "getSharedPreferences");
        return context.getSharedPreferences(SHARED_PREFERENCES, 0);
    }

    protected static void setList(Context context, String str, List<PubnativeTrackingURLModel> list) {
        Log.v(TAG, "setList: " + str);
        SharedPreferences.Editor edit = getSharedPreferences(context).edit();
        if (list == null) {
            edit.remove(str);
        } else {
            edit.putString(str, new Gson().toJson((Object) list));
        }
        edit.apply();
    }

    public static synchronized void track(Context context, String str) {
        synchronized (PubnativeTrackingManager.class) {
            Log.v(TAG, "track");
            if (context == null) {
                Log.e(TAG, "track - ERROR: Context parameter is null");
            } else if (TextUtils.isEmpty(str)) {
                Log.e(TAG, "track - ERROR: url parameter is null");
            } else {
                enqueueFailedList(context);
                PubnativeTrackingURLModel pubnativeTrackingURLModel = new PubnativeTrackingURLModel();
                pubnativeTrackingURLModel.url = str;
                pubnativeTrackingURLModel.startTimestamp = System.currentTimeMillis();
                enqueueItem(context, SHARED_PENDING_LIST, pubnativeTrackingURLModel);
                trackNextItem(context);
            }
        }
    }

    protected static synchronized void trackNextItem(final Context context) {
        synchronized (PubnativeTrackingManager.class) {
            Log.v(TAG, "trackNextItem");
            if (sIsTracking) {
                Log.w(TAG, "trackNextItem - Currently tracking, dropping the call, will be resumed soon");
            } else {
                sIsTracking = true;
                final PubnativeTrackingURLModel dequeueItem = dequeueItem(context, SHARED_PENDING_LIST);
                if (dequeueItem == null) {
                    Log.v(TAG, "trackNextItem - tracking finished, no more items to track");
                    sIsTracking = false;
                } else if (dequeueItem.startTimestamp + ITEM_VALIDITY_TIME < System.currentTimeMillis()) {
                    Log.v(TAG, "trackNextItem - discarding item");
                    sIsTracking = false;
                    trackNextItem(context);
                } else {
                    new PubnativeHttpRequest().start(context, dequeueItem.url, new PubnativeHttpRequest.Listener() {
                        public void onPubnativeHttpRequestFail(PubnativeHttpRequest pubnativeHttpRequest, Exception exc) {
                            Log.e(PubnativeTrackingManager.TAG, "onPubnativeHttpRequestFail", exc);
                            PubnativeTrackingManager.enqueueItem(context, PubnativeTrackingManager.SHARED_FAILED_LIST, dequeueItem);
                            boolean unused = PubnativeTrackingManager.sIsTracking = false;
                            PubnativeTrackingManager.trackNextItem(context);
                        }

                        public void onPubnativeHttpRequestFinish(PubnativeHttpRequest pubnativeHttpRequest, String str, int i) {
                            Log.v(PubnativeTrackingManager.TAG, "onPubnativeHttpRequestFinish" + str);
                            boolean unused = PubnativeTrackingManager.sIsTracking = false;
                            PubnativeTrackingManager.trackNextItem(context);
                        }

                        public void onPubnativeHttpRequestStart(PubnativeHttpRequest pubnativeHttpRequest) {
                            Log.v(PubnativeTrackingManager.TAG, "onPubnativeHttpRequestStart");
                        }
                    });
                }
            }
        }
    }
}
