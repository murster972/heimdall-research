package net.pubnative.library.tracking;

import android.util.Log;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import net.pubnative.library.tracking.PubnativeImpressionTracker;

public class PubnativeImpressionManager {
    private static final String TAG = PubnativeImpressionManager.class.getSimpleName();
    private static PubnativeImpressionManager instance;
    protected List<PubnativeImpressionTracker> mTrackers;

    private PubnativeImpressionManager() {
    }

    public static PubnativeImpressionManager getInstance() {
        if (instance == null) {
            instance = new PubnativeImpressionManager();
            instance.mTrackers = new ArrayList();
        }
        return instance;
    }

    public static void startTrackingView(View view, PubnativeImpressionTracker.Listener listener) {
        getInstance().addView(view, listener);
    }

    public static void stopTrackingAll(PubnativeImpressionTracker.Listener listener) {
        getInstance().stopTracking(listener);
    }

    public static void stopTrackingView(View view) {
        getInstance().removeView(view);
    }

    /* access modifiers changed from: protected */
    public void addView(View view, PubnativeImpressionTracker.Listener listener) {
        PubnativeImpressionTracker pubnativeImpressionTracker;
        if (view == null) {
            Log.w(TAG, "trying to start tracking null view, dropping this calll");
        } else if (listener == null) {
            Log.w(TAG, "trying to start tracking with null listener");
        } else {
            if (containsTracker(view)) {
                if (!this.mTrackers.get(indexOfTracker(view)).equals(listener)) {
                    removeView(view);
                }
            }
            if (containsTracker(listener)) {
                pubnativeImpressionTracker = this.mTrackers.get(indexOfTracker(view));
            } else {
                pubnativeImpressionTracker = new PubnativeImpressionTracker();
                pubnativeImpressionTracker.setListener(listener);
                this.mTrackers.add(pubnativeImpressionTracker);
            }
            pubnativeImpressionTracker.addView(view);
        }
    }

    /* access modifiers changed from: protected */
    public boolean containsTracker(View view) {
        return indexOfTracker(view) >= 0;
    }

    /* access modifiers changed from: protected */
    public boolean containsTracker(PubnativeImpressionTracker.Listener listener) {
        return indexOfTracker(listener) >= 0;
    }

    /* access modifiers changed from: protected */
    public int indexOfTracker(View view) {
        for (int i = 0; i < this.mTrackers.size(); i++) {
            if (this.mTrackers.get(i).equals(view)) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public int indexOfTracker(PubnativeImpressionTracker.Listener listener) {
        for (int i = 0; i < this.mTrackers.size(); i++) {
            if (this.mTrackers.get(i).equals(listener)) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public void removeView(View view) {
        if (view == null) {
            Log.w(TAG, "trying to remove null view, dropping this call");
        } else if (containsTracker(view)) {
            PubnativeImpressionTracker pubnativeImpressionTracker = this.mTrackers.get(indexOfTracker(view));
            pubnativeImpressionTracker.removeView(view);
            if (pubnativeImpressionTracker.isEmpty()) {
                pubnativeImpressionTracker.clear();
                this.mTrackers.remove(pubnativeImpressionTracker);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void stopTracking(PubnativeImpressionTracker.Listener listener) {
        if (listener == null) {
            Log.w(TAG, "trying to remove all views from null listener, dropping this call");
        } else if (containsTracker(listener)) {
            this.mTrackers.get(indexOfTracker(listener)).clear();
            this.mTrackers.remove(listener);
        }
    }
}
