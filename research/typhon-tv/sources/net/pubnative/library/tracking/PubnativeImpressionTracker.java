package net.pubnative.library.tracking;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import com.mopub.common.AdType;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.pubnative.library.tracking.PubnativeVisibilityTracker;

public class PubnativeImpressionTracker {
    private static final double DEFAULT_MIN_VISIBLE_PERCENT = 0.5d;
    private static final String TAG = PubnativeImpressionTracker.class.getSimpleName();
    private static final int VISIBILITY_CHECK_MILLIS = 250;
    private static final int VISIBILITY_TIME_MILLIS = 1000;
    protected Handler mHandler = new Handler(Looper.getMainLooper());
    protected WeakReference<Listener> mImpressionListener = null;
    protected Runnable mImpressionRunnable = new ImpressionRunnable();
    protected List<View> mTrackingViews = new ArrayList();
    protected PubnativeVisibilityTracker.Listener mVisibilityListener = new PubnativeVisibilityTracker.Listener() {
        public void onVisibilityCheck(List<View> list, List<View> list2) {
            if (PubnativeImpressionTracker.this.mImpressionListener == null || PubnativeImpressionTracker.this.mImpressionListener.get() == null) {
                PubnativeImpressionTracker.this.clear();
                return;
            }
            for (View next : list) {
                if (!PubnativeImpressionTracker.this.mVisibleViews.containsKey(next)) {
                    PubnativeImpressionTracker.this.mVisibleViews.put(next, Long.valueOf(SystemClock.uptimeMillis()));
                }
            }
            for (View remove : list2) {
                PubnativeImpressionTracker.this.mVisibleViews.remove(remove);
            }
            if (!PubnativeImpressionTracker.this.mVisibleViews.isEmpty()) {
                PubnativeImpressionTracker.this.scheduleNextRun();
            }
        }
    };
    protected PubnativeVisibilityTracker mVisibilityTracker = null;
    protected HashMap<View, Long> mVisibleViews = new HashMap<>();

    protected class ImpressionRunnable implements Runnable {
        private List<View> mRemovedViews = new ArrayList();

        ImpressionRunnable() {
        }

        public void run() {
            for (Map.Entry next : PubnativeImpressionTracker.this.mVisibleViews.entrySet()) {
                View view = (View) next.getKey();
                if (SystemClock.uptimeMillis() - ((Long) next.getValue()).longValue() >= 1000) {
                    if (!(PubnativeImpressionTracker.this.mImpressionListener == null || PubnativeImpressionTracker.this.mImpressionListener.get() == null)) {
                        ((Listener) PubnativeImpressionTracker.this.mImpressionListener.get()).onImpression(view);
                    }
                    this.mRemovedViews.add(view);
                }
            }
            for (View stopTrackingView : this.mRemovedViews) {
                PubnativeImpressionManager.stopTrackingView(stopTrackingView);
            }
            this.mRemovedViews.clear();
            if (!PubnativeImpressionTracker.this.mVisibleViews.isEmpty()) {
                PubnativeImpressionTracker.this.scheduleNextRun();
            }
        }
    }

    public interface Listener {
        void onImpression(View view);
    }

    public void addView(View view) {
        Log.v(TAG, "addView");
        if (!this.mTrackingViews.contains(view)) {
            this.mTrackingViews.add(view);
            getVisibilityTracker().addView(view, DEFAULT_MIN_VISIBLE_PERCENT);
        }
    }

    public void clear() {
        Log.v(TAG, AdType.CLEAR);
        for (View stopTrackingView : this.mTrackingViews) {
            PubnativeImpressionManager.stopTrackingView(stopTrackingView);
        }
        this.mHandler.removeMessages(0);
        this.mTrackingViews.clear();
        this.mVisibleViews.clear();
        if (this.mVisibilityTracker != null) {
            this.mVisibilityTracker.clear();
            this.mVisibilityTracker = null;
        }
    }

    public boolean equals(Object obj) {
        Log.v(TAG, "equals");
        return obj instanceof View ? this.mTrackingViews.contains(obj) : obj instanceof Listener ? this.mImpressionListener.equals(obj) : super.equals(obj);
    }

    /* access modifiers changed from: protected */
    public PubnativeVisibilityTracker getVisibilityTracker() {
        if (this.mVisibilityTracker == null) {
            this.mVisibilityTracker = new PubnativeVisibilityTracker();
            this.mVisibilityTracker.setListener(this.mVisibilityListener);
        }
        return this.mVisibilityTracker;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public boolean isEmpty() {
        Log.v(TAG, "isEmpty");
        return this.mTrackingViews.isEmpty();
    }

    public void removeView(View view) {
        Log.v(TAG, "removeView");
        this.mTrackingViews.remove(view);
        this.mVisibleViews.remove(view);
        getVisibilityTracker().removeView(view);
    }

    /* access modifiers changed from: protected */
    public void scheduleNextRun() {
        if (!this.mHandler.hasMessages(0)) {
            this.mHandler.postDelayed(this.mImpressionRunnable, 250);
        }
    }

    public void setListener(Listener listener) {
        Log.v(TAG, "setListener");
        this.mImpressionListener = new WeakReference<>(listener);
    }
}
