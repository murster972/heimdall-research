package net.pubnative.library.tracking;

import android.graphics.Rect;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class PubnativeVisibilityTracker {
    /* access modifiers changed from: private */
    public static final String TAG = PubnativeVisibilityTracker.class.getSimpleName();
    private static final int VISIBILITY_CHECK_DELAY = 100;
    protected WeakReference<View> mDeviceView = null;
    protected Handler mHandler = new Handler();
    protected boolean mIsVisibilityCheckScheduled = false;
    protected WeakReference<Listener> mListener = null;
    protected ViewTreeObserver.OnPreDrawListener mOnPreDrawListener = new ViewTreeObserver.OnPreDrawListener() {
        public boolean onPreDraw() {
            if (PubnativeVisibilityTracker.this.mListener == null || PubnativeVisibilityTracker.this.mListener.get() == null) {
                PubnativeVisibilityTracker.this.clear();
                return true;
            }
            PubnativeVisibilityTracker.this.scheduleVisibilityCheck();
            return true;
        }
    };
    protected List<PubnativeVisibilityTrackerItem> mTrackedViews = new ArrayList();
    protected VisibilityRunnable mVisibilityRunnable = new VisibilityRunnable();

    public interface Listener {
        void onVisibilityCheck(List<View> list, List<View> list2);
    }

    protected class PubnativeVisibilityTrackerItem {
        private final String TAG = PubnativeVisibilityTrackerItem.class.getSimpleName();
        public double mMinVisibilityPercent;
        public View mTrackingView;

        protected PubnativeVisibilityTrackerItem() {
        }

        public boolean equals(Object obj) {
            Log.v(this.TAG, "equals");
            return obj instanceof View ? obj.equals(this.mTrackingView) : super.equals(obj);
        }
    }

    protected class VisibilityRunnable implements Runnable {
        private ArrayList<View> mInvisibleViews = new ArrayList<>();
        private Rect mVisibleRect = new Rect();
        private ArrayList<View> mVisibleViews = new ArrayList<>();

        VisibilityRunnable() {
        }

        /* access modifiers changed from: protected */
        public boolean isVisible(PubnativeVisibilityTrackerItem pubnativeVisibilityTrackerItem) {
            View view = pubnativeVisibilityTrackerItem.mTrackingView;
            if (view == null || !view.isShown() || view.getParent() == null || !view.getLocalVisibleRect(this.mVisibleRect)) {
                return false;
            }
            Log.i(PubnativeVisibilityTracker.TAG, "tracking view at: " + System.currentTimeMillis());
            return ((double) ((float) (this.mVisibleRect.height() * this.mVisibleRect.width()))) / ((double) ((float) (view.getHeight() * view.getWidth()))) >= pubnativeVisibilityTrackerItem.mMinVisibilityPercent;
        }

        public void run() {
            PubnativeVisibilityTracker.this.mIsVisibilityCheckScheduled = false;
            for (PubnativeVisibilityTrackerItem next : PubnativeVisibilityTracker.this.mTrackedViews) {
                if (isVisible(next)) {
                    this.mVisibleViews.add(next.mTrackingView);
                } else {
                    this.mInvisibleViews.add(next.mTrackingView);
                }
            }
            if (!(PubnativeVisibilityTracker.this.mListener == null || PubnativeVisibilityTracker.this.mListener.get() == null)) {
                ((Listener) PubnativeVisibilityTracker.this.mListener.get()).onVisibilityCheck(this.mVisibleViews, this.mInvisibleViews);
            }
            this.mInvisibleViews.clear();
            this.mVisibleViews.clear();
        }
    }

    public void addView(View view, double d) {
        if (this.mDeviceView == null) {
            this.mDeviceView = new WeakReference<>(view);
            ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnPreDrawListener(this.mOnPreDrawListener);
            } else {
                Log.d(TAG, "Unable to start tracking, Window ViewTreeObserver is not alive");
            }
        }
        if (!containsTrackedView(view)) {
            PubnativeVisibilityTrackerItem pubnativeVisibilityTrackerItem = new PubnativeVisibilityTrackerItem();
            pubnativeVisibilityTrackerItem.mTrackingView = view;
            pubnativeVisibilityTrackerItem.mMinVisibilityPercent = d;
            this.mTrackedViews.add(pubnativeVisibilityTrackerItem);
            scheduleVisibilityCheck();
        }
    }

    public void clear() {
        this.mHandler.removeMessages(0);
        this.mTrackedViews.clear();
        this.mIsVisibilityCheckScheduled = false;
        View view = (View) this.mDeviceView.get();
        if (!(view == null || this.mOnPreDrawListener == null)) {
            ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.mOnPreDrawListener);
            }
            this.mOnPreDrawListener = null;
        }
        this.mListener = null;
    }

    /* access modifiers changed from: protected */
    public boolean containsTrackedView(View view) {
        return indexOfTrackedView(view) >= 0;
    }

    /* access modifiers changed from: protected */
    public int indexOfTrackedView(View view) {
        for (int i = 0; i < this.mTrackedViews.size(); i++) {
            if (this.mTrackedViews.get(i).equals(view)) {
                return i;
            }
        }
        return -1;
    }

    public void removeView(View view) {
        this.mTrackedViews.remove(view);
    }

    /* access modifiers changed from: protected */
    public void scheduleVisibilityCheck() {
        if (!this.mIsVisibilityCheckScheduled) {
            this.mIsVisibilityCheckScheduled = true;
            this.mHandler.postDelayed(this.mVisibilityRunnable, 100);
        }
    }

    public void setListener(Listener listener) {
        this.mListener = new WeakReference<>(listener);
    }
}
