package net.pubnative.library.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.net.HttpURLConnection;
import java.net.URL;
import net.pubnative.library.utils.SystemUtils;
import org.apache.oltu.oauth2.common.OAuth;

public class PubnativeHttpRequest {
    public static final int HTTP_INVALID_REQUEST = 422;
    public static final int HTTP_OK = 200;
    private static final String TAG = PubnativeHttpRequest.class.getSimpleName();
    protected static int sConnectionTimeout = 4000;
    protected Handler mHandler = null;
    protected Listener mListener = null;

    public interface Listener {
        void onPubnativeHttpRequestFail(PubnativeHttpRequest pubnativeHttpRequest, Exception exc);

        void onPubnativeHttpRequestFinish(PubnativeHttpRequest pubnativeHttpRequest, String str, int i);

        void onPubnativeHttpRequestStart(PubnativeHttpRequest pubnativeHttpRequest);
    }

    /* access modifiers changed from: private */
    public void initiateRequest(final String str, final String str2) {
        Log.v(TAG, "initiateRequest");
        new Thread(new Runnable() {
            public void run() {
                PubnativeHttpRequest.this.doRequest(str, str2);
            }
        }).start();
    }

    public static void setConnectionTimeout(int i) {
        Log.v(TAG, "setConnectionTimeout");
        sConnectionTimeout = i;
    }

    /* access modifiers changed from: protected */
    public void doRequest(String str, String str2) {
        Log.v(TAG, "doRequest: " + str);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setRequestProperty(AbstractSpiCall.HEADER_USER_AGENT, str2);
            httpURLConnection.setRequestMethod(OAuth.HttpMethod.GET);
            httpURLConnection.setConnectTimeout(sConnectionTimeout);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            invokeFinish(getString(httpURLConnection.getInputStream()), httpURLConnection.getResponseCode());
        } catch (Exception e) {
            invokeFail(e);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0020 A[SYNTHETIC, Splitter:B:11:0x0020] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0037 A[SYNTHETIC, Splitter:B:23:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getString(java.io.InputStream r9) {
        /*
            r8 = this;
            r4 = 0
            r0 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0042, all -> 0x0034 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0042, all -> 0x0034 }
            r6.<init>(r9)     // Catch:{ IOException -> 0x0042, all -> 0x0034 }
            r1.<init>(r6)     // Catch:{ IOException -> 0x0042, all -> 0x0034 }
        L_0x0011:
            java.lang.String r3 = r1.readLine()     // Catch:{ IOException -> 0x001b, all -> 0x003f }
            if (r3 == 0) goto L_0x002a
            r5.append(r3)     // Catch:{ IOException -> 0x001b, all -> 0x003f }
            goto L_0x0011
        L_0x001b:
            r2 = move-exception
            r0 = r1
        L_0x001d:
            r5 = 0
            if (r0 == 0) goto L_0x0023
            r0.close()     // Catch:{ IOException -> 0x003b }
        L_0x0023:
            if (r5 == 0) goto L_0x0029
            java.lang.String r4 = r5.toString()
        L_0x0029:
            return r4
        L_0x002a:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ IOException -> 0x0031 }
            r0 = r1
            goto L_0x0023
        L_0x0031:
            r6 = move-exception
            r0 = r1
            goto L_0x0023
        L_0x0034:
            r6 = move-exception
        L_0x0035:
            if (r0 == 0) goto L_0x003a
            r0.close()     // Catch:{ IOException -> 0x003d }
        L_0x003a:
            throw r6
        L_0x003b:
            r6 = move-exception
            goto L_0x0023
        L_0x003d:
            r7 = move-exception
            goto L_0x003a
        L_0x003f:
            r6 = move-exception
            r0 = r1
            goto L_0x0035
        L_0x0042:
            r2 = move-exception
            goto L_0x001d
        L_0x0044:
            r0 = r1
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: net.pubnative.library.network.PubnativeHttpRequest.getString(java.io.InputStream):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void invokeFail(final Exception exc) {
        Log.v(TAG, "invokeFail: " + exc);
        this.mHandler.post(new Runnable() {
            public void run() {
                if (PubnativeHttpRequest.this.mListener != null) {
                    PubnativeHttpRequest.this.mListener.onPubnativeHttpRequestFail(PubnativeHttpRequest.this, exc);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void invokeFinish(final String str, final int i) {
        Log.v(TAG, "invokeFinish");
        this.mHandler.post(new Runnable() {
            public void run() {
                if (PubnativeHttpRequest.this.mListener != null) {
                    PubnativeHttpRequest.this.mListener.onPubnativeHttpRequestFinish(PubnativeHttpRequest.this, str, i);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void invokeStart() {
        Log.v(TAG, "invokeStart");
        this.mHandler.post(new Runnable() {
            public void run() {
                if (PubnativeHttpRequest.this.mListener != null) {
                    PubnativeHttpRequest.this.mListener.onPubnativeHttpRequestStart(PubnativeHttpRequest.this);
                }
            }
        });
    }

    public void start(final Context context, final String str, Listener listener) {
        Log.v(TAG, "start: " + str);
        this.mListener = listener;
        this.mHandler = new Handler(Looper.getMainLooper());
        if (this.mListener == null) {
            Log.w(TAG, "Warning: null listener specified");
        }
        if (TextUtils.isEmpty(str)) {
            invokeFail(new IllegalArgumentException("PubnativeHttpRequest - Error: null or empty url, dropping call"));
            return;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    PubnativeHttpRequest.this.invokeStart();
                    String webViewUserAgent = SystemUtils.getWebViewUserAgent(context);
                    if (TextUtils.isEmpty(webViewUserAgent)) {
                        PubnativeHttpRequest.this.invokeFail(new Exception("PubnativeHttpRequest - Error: User agent cannot be retrieved"));
                    } else {
                        PubnativeHttpRequest.this.initiateRequest(str, webViewUserAgent);
                    }
                }
            });
        } else {
            invokeFail(new Exception("PubnativeHttpRequest - Error: internet connection not detected, dropping call"));
        }
    }
}
