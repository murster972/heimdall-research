package net.pubnative.library;

public final class R {

    public static final class id {
        public static final int ic_context_icon = 2131296504;
        public static final int tv_context_text = 2131296966;
    }

    public static final class layout {
        public static final int content_info_layout = 2131492936;
    }

    public static final class string {
        public static final int app_name = 2131820607;
    }
}
