package net.pubnative.library.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.net.URL;

public class ImageDownloader {
    private static final String TAG = ImageDownloader.class.getSimpleName();
    private Handler mHandler;
    /* access modifiers changed from: private */
    public WeakReference<Bitmap> mImage;
    /* access modifiers changed from: private */
    public Listener mListener;

    public interface Listener {
        void onImageFailed(String str, Exception exc);

        void onImageLoad(String str, Bitmap bitmap);
    }

    private void downloadImage(final String str) {
        Log.v(TAG, "downloadImage");
        new Thread(new Runnable() {
            public void run() {
                try {
                    URL url = new URL(str);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(url.openConnection().getInputStream(), new Rect(), options);
                    options.inSampleSize = ImageDownloader.this.calculateInSampleSize(options);
                    options.inJustDecodeBounds = false;
                    WeakReference unused = ImageDownloader.this.mImage = new WeakReference(BitmapFactory.decodeStream(url.openConnection().getInputStream(), (Rect) null, options));
                } catch (Exception e) {
                    ImageDownloader.this.invokeFail(str, e);
                } catch (OutOfMemoryError e2) {
                    WeakReference unused2 = ImageDownloader.this.mImage = null;
                    ImageDownloader.this.invokeFail(str, new Exception("Out of memory during image downloading"));
                } finally {
                    ImageDownloader.this.invokeLoad(str);
                }
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public int calculateInSampleSize(BitmapFactory.Options options) {
        Log.v(TAG, "calculateInSampleSize");
        int i = options.outHeight;
        int i2 = options.outWidth;
        int freeMemory = (int) Runtime.getRuntime().freeMemory();
        int i3 = i2 * i * 4;
        if (i3 > freeMemory) {
            return i3 / freeMemory;
        }
        return 1;
    }

    /* access modifiers changed from: protected */
    public void invokeFail(final String str, final Exception exc) {
        Log.v(TAG, "invokeFail");
        this.mHandler.post(new Runnable() {
            public void run() {
                if (ImageDownloader.this.mListener != null) {
                    ImageDownloader.this.mListener.onImageFailed(str, exc);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void invokeLoad(final String str) {
        Log.v(TAG, "invokeLoad");
        this.mHandler.post(new Runnable() {
            public void run() {
                if (ImageDownloader.this.mListener != null && ImageDownloader.this.mImage != null && ImageDownloader.this.mImage.get() != null) {
                    ImageDownloader.this.mListener.onImageLoad(str, (Bitmap) ImageDownloader.this.mImage.get());
                }
            }
        });
    }

    public void load(String str, Listener listener) {
        Log.v(TAG, "load");
        this.mHandler = new Handler(Looper.getMainLooper());
        if (listener == null) {
            Log.e(TAG, "Listener is not set, dropping call");
        } else if (TextUtils.isEmpty(str)) {
            invokeFail(str, new Exception("URL is not valid"));
        } else {
            this.mListener = listener;
            downloadImage(str);
        }
    }
}
