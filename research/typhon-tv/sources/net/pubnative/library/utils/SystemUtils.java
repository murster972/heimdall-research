package net.pubnative.library.utils;

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;

public class SystemUtils {
    private static final String TAG = SystemUtils.class.getSimpleName();
    private static String sWebViewUserAgent = null;

    public static String getWebViewUserAgent(Context context) {
        Log.v(TAG, "getWebViewUserAgent");
        if (sWebViewUserAgent == null) {
            try {
                sWebViewUserAgent = new WebView(context).getSettings().getUserAgentString();
            } catch (Exception e) {
                Log.w(TAG, "getWebViewUserAgent - Error: cannot inject user agent");
            }
        }
        return sWebViewUserAgent;
    }
}
