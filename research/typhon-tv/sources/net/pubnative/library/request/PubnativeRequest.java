package net.pubnative.library.request;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.pubnative.AdvertisingIdClient;
import net.pubnative.library.network.PubnativeHttpRequest;
import net.pubnative.library.request.model.PubnativeAdModel;
import net.pubnative.library.utils.Crypto;

public class PubnativeRequest implements AdvertisingIdClient.Listener, PubnativeHttpRequest.Listener {
    protected static final String BASE_URL = "=";
    @Deprecated
    public static final String LEGACY_ZONE_ID = "1";
    private static String TAG = PubnativeRequest.class.getSimpleName();
    protected Context mContext = null;
    protected boolean mIsRunning = false;
    protected Listener mListener = null;
    protected PubnativeHttpRequest mRequest = null;
    protected Map<String, String> mRequestParameters = new HashMap();

    @Deprecated
    public enum Endpoint {
        NATIVE
    }

    public interface Listener {
        void onPubnativeRequestFailed(PubnativeRequest pubnativeRequest, Exception exc);

        void onPubnativeRequestSuccess(PubnativeRequest pubnativeRequest, List<PubnativeAdModel> list);
    }

    public interface Parameters {
        public static final String AD_COUNT = "adcount";
        public static final String AGE = "age";
        public static final String ANDROID_ADVERTISER_ID = "gid";
        public static final String ANDROID_ADVERTISER_ID_MD5 = "gidmd5";
        public static final String ANDROID_ADVERTISER_ID_SHA1 = "gidsha1";
        public static final String APP_TOKEN = "apptoken";
        public static final String APP_VERSION = "appver";
        public static final String ASSET_FIELDS = "af";
        public static final String ASSET_LAYOUT = "al";
        public static final String COPPA = "coppa";
        public static final String DEVICE_MODEL = "devicemodel";
        public static final String GENDER = "gender";
        public static final String KEYWORDS = "keywords";
        public static final String LAT = "lat";
        public static final String LOCALE = "locale";
        public static final String LONG = "long";
        public static final String META_FIELDS = "mf";
        public static final String NO_USER_ID = "dnt";
        public static final String OS = "os";
        public static final String OS_VERSION = "osver";
        public static final String TEST = "test";
        public static final String VIDEO = "video";
        public static final String ZONE_ID = "zoneid";
    }

    /* access modifiers changed from: protected */
    public void doRequest() {
        Log.v(TAG, "doRequest");
        String requestURL = getRequestURL();
        if (requestURL == null) {
            invokeOnFail(new Exception("PubnativeRequest - Error: invalid request URL"));
            return;
        }
        this.mRequest = new PubnativeHttpRequest();
        this.mRequest.start(this.mContext, requestURL, this);
    }

    /* access modifiers changed from: protected */
    public void fillDefaultParameters() {
        Log.v(TAG, "startRequest");
        this.mRequestParameters.put(Parameters.OS, AbstractSpiCall.ANDROID_CLIENT_TYPE);
        this.mRequestParameters.put(Parameters.DEVICE_MODEL, Build.MODEL);
        this.mRequestParameters.put(Parameters.OS_VERSION, Build.VERSION.RELEASE);
        this.mRequestParameters.put(Parameters.LOCALE, Locale.getDefault().getLanguage());
        if (!this.mRequestParameters.containsKey(Parameters.ASSET_LAYOUT) && !this.mRequestParameters.containsKey(Parameters.ASSET_FIELDS)) {
            setParameterArray(Parameters.ASSET_FIELDS, new String[]{PubnativeAsset.TITLE, PubnativeAsset.DESCRIPTION, PubnativeAsset.ICON, PubnativeAsset.BANNER, PubnativeAsset.CALL_TO_ACTION, PubnativeAsset.RATING});
        }
        String str = this.mRequestParameters.get(Parameters.META_FIELDS);
        ArrayList arrayList = new ArrayList();
        if (str != null) {
            Arrays.asList(TextUtils.split(str, ","));
        }
        arrayList.add(PubnativeMeta.REVENUE_MODEL);
        arrayList.add(PubnativeMeta.CONTENT_INFO);
        setParameterArray(Parameters.META_FIELDS, (String[]) arrayList.toArray(new String[0]));
    }

    /* access modifiers changed from: protected */
    public String getRequestURL() {
        Log.v(TAG, "getRequestURL");
        Uri.Builder buildUpon = Uri.parse(BASE_URL).buildUpon();
        for (String next : this.mRequestParameters.keySet()) {
            String str = this.mRequestParameters.get(next);
            if (!(next == null || str == null)) {
                buildUpon.appendQueryParameter(next, str);
            }
        }
        return buildUpon.build().toString();
    }

    /* access modifiers changed from: protected */
    public void invokeOnFail(Exception exc) {
        Log.v(TAG, "invokeOnFail: " + exc);
        this.mIsRunning = false;
        if (this.mListener != null) {
            this.mListener.onPubnativeRequestFailed(this, exc);
        }
        this.mListener = null;
    }

    /* access modifiers changed from: protected */
    public void invokeOnSuccess(List<PubnativeAdModel> list) {
        Log.v(TAG, "invokeOnSuccess");
        this.mIsRunning = false;
        if (this.mListener != null) {
            this.mListener.onPubnativeRequestSuccess(this, list);
        }
        this.mListener = null;
    }

    /* access modifiers changed from: protected */
    public boolean isCoppaModeEnabled() {
        String str = this.mRequestParameters.get(Parameters.COPPA);
        if (!TextUtils.isEmpty(str)) {
            return str.equalsIgnoreCase(LEGACY_ZONE_ID);
        }
        return false;
    }

    public void onAdvertisingIdClientFail(Exception exc) {
        Log.v(TAG, "onAdvertisingIdClientFail");
        setAdvertisingID((AdvertisingIdClient.AdInfo) null);
        doRequest();
    }

    public void onAdvertisingIdClientFinish(AdvertisingIdClient.AdInfo adInfo) {
        Log.v(TAG, "onAdvertisingIdClientFinish");
        setAdvertisingID(adInfo);
        doRequest();
    }

    public void onPubnativeHttpRequestFail(PubnativeHttpRequest pubnativeHttpRequest, Exception exc) {
        Log.v(TAG, "onPubnativeHttpRequestFail: " + exc);
        invokeOnFail(exc);
    }

    public void onPubnativeHttpRequestFinish(PubnativeHttpRequest pubnativeHttpRequest, String str, int i) {
        Log.v(TAG, "onPubnativeHttpRequestFinish");
        if (200 == i || 422 == i) {
            processStream(str);
        } else {
            invokeOnFail(new Exception("PubnativeRequest - Response error: " + i));
        }
    }

    public void onPubnativeHttpRequestStart(PubnativeHttpRequest pubnativeHttpRequest) {
        Log.v(TAG, "onPubnativeHttpRequestStart");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: net.pubnative.library.request.model.api.PubnativeAPIV3ResponseModel} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void processStream(java.lang.String r10) {
        /*
            r9 = this;
            java.lang.String r6 = TAG
            java.lang.String r7 = "processStream"
            android.util.Log.v(r6, r7)
            r2 = 0
            r4 = 0
            com.google.gson.Gson r6 = new com.google.gson.Gson     // Catch:{ Exception -> 0x001f }
            r6.<init>()     // Catch:{ Exception -> 0x001f }
            java.lang.Class<net.pubnative.library.request.model.api.PubnativeAPIV3ResponseModel> r7 = net.pubnative.library.request.model.api.PubnativeAPIV3ResponseModel.class
            java.lang.Object r6 = r6.fromJson((java.lang.String) r10, r7)     // Catch:{ Exception -> 0x001f }
            r0 = r6
            net.pubnative.library.request.model.api.PubnativeAPIV3ResponseModel r0 = (net.pubnative.library.request.model.api.PubnativeAPIV3ResponseModel) r0     // Catch:{ Exception -> 0x001f }
            r2 = r0
        L_0x0019:
            if (r4 == 0) goto L_0x0022
            r9.invokeOnFail(r4)
        L_0x001e:
            return
        L_0x001f:
            r3 = move-exception
            r4 = r3
            goto L_0x0019
        L_0x0022:
            if (r2 != 0) goto L_0x0030
            java.lang.Exception r6 = new java.lang.Exception
            java.lang.String r7 = "PubnativeRequest - Parse error"
            r6.<init>(r7)
            r9.invokeOnFail(r6)
            goto L_0x001e
        L_0x0030:
            java.lang.String r6 = "ok"
            java.lang.String r7 = r2.status
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x0067
            r5 = 0
            java.util.List<net.pubnative.library.request.model.api.PubnativeAPIV3AdModel> r6 = r2.ads
            if (r6 == 0) goto L_0x0063
            java.util.List<net.pubnative.library.request.model.api.PubnativeAPIV3AdModel> r6 = r2.ads
            java.util.Iterator r6 = r6.iterator()
        L_0x0046:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x0063
            java.lang.Object r1 = r6.next()
            net.pubnative.library.request.model.api.PubnativeAPIV3AdModel r1 = (net.pubnative.library.request.model.api.PubnativeAPIV3AdModel) r1
            if (r5 != 0) goto L_0x0059
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
        L_0x0059:
            android.content.Context r7 = r9.mContext
            net.pubnative.library.request.model.PubnativeAdModel r7 = net.pubnative.library.request.model.PubnativeAdModel.create(r7, r1)
            r5.add(r7)
            goto L_0x0046
        L_0x0063:
            r9.invokeOnSuccess(r5)
            goto L_0x001e
        L_0x0067:
            java.lang.Exception r6 = new java.lang.Exception
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "PubnativeRequest - Server error: "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r2.error_message
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7)
            r9.invokeOnFail(r6)
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: net.pubnative.library.request.PubnativeRequest.processStream(java.lang.String):void");
    }

    /* access modifiers changed from: protected */
    public void setAdvertisingID(AdvertisingIdClient.AdInfo adInfo) {
        String str = null;
        if (adInfo != null && !adInfo.isLimitAdTrackingEnabled()) {
            str = adInfo.getId();
        }
        if (TextUtils.isEmpty(str)) {
            this.mRequestParameters.put(Parameters.NO_USER_ID, LEGACY_ZONE_ID);
            return;
        }
        this.mRequestParameters.put(Parameters.ANDROID_ADVERTISER_ID, str);
        this.mRequestParameters.put(Parameters.ANDROID_ADVERTISER_ID_SHA1, Crypto.sha1(str));
        this.mRequestParameters.put(Parameters.ANDROID_ADVERTISER_ID_MD5, Crypto.md5(str));
    }

    public void setCoppaMode(boolean z) {
        Log.v(TAG, "setCoppaMode");
        setParameter(Parameters.COPPA, z ? LEGACY_ZONE_ID : "0");
    }

    public void setParameter(String str, String str2) {
        Log.v(TAG, "setParameter: " + str + " : " + str2);
        if (TextUtils.isEmpty(str)) {
            Log.e(TAG, "Invalid key passed for parameter");
        } else if (TextUtils.isEmpty(str2)) {
            this.mRequestParameters.remove(str);
        } else {
            this.mRequestParameters.put(str, str2);
        }
    }

    public void setParameterArray(String str, String[] strArr) {
        Log.v(TAG, "setParameter: " + str + " : " + strArr);
        if (TextUtils.isEmpty(str)) {
            Log.e(TAG, "Invalid key passed for parameter");
        } else if (strArr == null) {
            this.mRequestParameters.remove(str);
        } else {
            this.mRequestParameters.put(str, TextUtils.join(",", strArr));
        }
    }

    public void setTestMode(boolean z) {
        Log.v(TAG, "setTestMode");
        setParameter(Parameters.TEST, z ? LEGACY_ZONE_ID : "0");
    }

    public void setTimeout(int i) {
        Log.v(TAG, "setTimeout");
        PubnativeHttpRequest.setConnectionTimeout(i);
    }

    @Deprecated
    public void start(Context context, Endpoint endpoint, Listener listener) {
        Log.v(TAG, TtmlNode.START);
        start(context, listener);
    }

    public void start(Context context, Listener listener) {
        Log.v(TAG, TtmlNode.START);
        if (listener == null) {
            Log.w(TAG, "start - listener is null and required, dropping call");
        } else if (context == null) {
            Log.w(TAG, "start - context is null and required, dropping call");
        } else if (this.mIsRunning) {
            Log.w(TAG, "start - this request is already running, dropping the call");
        } else {
            this.mIsRunning = true;
            this.mListener = listener;
            this.mContext = context;
            fillDefaultParameters();
            if (isCoppaModeEnabled()) {
                setAdvertisingID((AdvertisingIdClient.AdInfo) null);
                doRequest();
                return;
            }
            AdvertisingIdClient.getAdvertisingId(this.mContext, new AdvertisingIdClient.Listener() {
                public void onAdvertisingIdClientFail(Exception exc) {
                    PubnativeRequest.this.setAdvertisingID((AdvertisingIdClient.AdInfo) null);
                    PubnativeRequest.this.doRequest();
                }

                public void onAdvertisingIdClientFinish(AdvertisingIdClient.AdInfo adInfo) {
                    PubnativeRequest.this.setAdvertisingID(adInfo);
                    PubnativeRequest.this.doRequest();
                }
            });
        }
    }
}
