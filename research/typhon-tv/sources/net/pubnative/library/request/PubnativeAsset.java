package net.pubnative.library.request;

public interface PubnativeAsset {
    public static final String BANNER = "banner";
    public static final String CALL_TO_ACTION = "cta";
    public static final String DESCRIPTION = "description";
    public static final String HTML_BANNER = "htmlbanner";
    public static final String ICON = "icon";
    public static final String RATING = "rating";
    public static final String STANDARD_BANNER = "standardbanner";
    public static final String TITLE = "title";
    public static final String VAST = "vast2";
}
