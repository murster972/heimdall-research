package net.pubnative.library.request;

public interface PubnativeMeta {
    public static final String BUNDLE_ID = "bundleid";
    public static final String CAMPAIGN_ID = "campaignid";
    public static final String CONTENT_INFO = "contentinfo";
    public static final String CREATIVE_ID = "creativeid";
    public static final String POINTS = "points";
    public static final String REVENUE_MODEL = "revenuemodel";
}
