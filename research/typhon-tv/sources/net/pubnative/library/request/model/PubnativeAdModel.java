package net.pubnative.library.request.model;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.pubnative.URLDriller;
import net.pubnative.library.request.PubnativeAsset;
import net.pubnative.library.request.PubnativeMeta;
import net.pubnative.library.request.model.api.PubnativeAPIV3AdModel;
import net.pubnative.library.request.model.api.PubnativeAPIV3DataModel;
import net.pubnative.library.tracking.PubnativeImpressionManager;
import net.pubnative.library.tracking.PubnativeImpressionTracker;
import net.pubnative.library.tracking.PubnativeTrackingManager;
import net.pubnative.library.utils.SystemUtils;
import net.pubnative.library.widget.PubnativeContentInfoWidget;
import net.pubnative.library.widget.PubnativeWebView;

public class PubnativeAdModel implements Serializable, URLDriller.Listener, PubnativeImpressionTracker.Listener {
    private static final String DATA_CONTENTINFO_ICON_KEY = "icon";
    private static final String DATA_CONTENTINFO_LINK_KEY = "link";
    private static final String DATA_TRACKING_KEY = "tracking";
    /* access modifiers changed from: private */
    public static String TAG = PubnativeAdModel.class.getSimpleName();
    private static final int URL_DRILLER_DEPTH = 15;
    private transient View mAdView = null;
    protected String mClickFinalURL = null;
    private transient View mClickableView = null;
    protected Context mContext = null;
    protected PubnativeAPIV3AdModel mData = null;
    protected boolean mIsClickCachingEnabled = false;
    protected boolean mIsClickInBackgroundEnabled = true;
    protected boolean mIsClickLoaderEnabled = true;
    protected boolean mIsClickPreparing = false;
    private transient boolean mIsImpressionConfirmed = false;
    protected boolean mIsWaitingForClickCache = false;
    protected transient Listener mListener = null;
    private transient RelativeLayout mLoadingView = null;
    protected UUID mUUID = null;
    protected List<String> mUsedAssets = null;

    public interface Listener {
        void onPubnativeAdModelClick(PubnativeAdModel pubnativeAdModel, View view);

        void onPubnativeAdModelImpression(PubnativeAdModel pubnativeAdModel, View view);

        void onPubnativeAdModelOpenOffer(PubnativeAdModel pubnativeAdModel);
    }

    public static PubnativeAdModel create(Context context, PubnativeAPIV3AdModel pubnativeAPIV3AdModel) {
        PubnativeAdModel pubnativeAdModel = new PubnativeAdModel();
        pubnativeAdModel.mData = pubnativeAPIV3AdModel;
        pubnativeAdModel.mContext = context;
        return pubnativeAdModel;
    }

    /* access modifiers changed from: protected */
    public void confirmBeacons(String str, Context context) {
        Log.v(TAG, "confirmBeacons: " + str);
        if (this.mData == null) {
            Log.w(TAG, "confirmBeacons - Error: ad data not present");
            return;
        }
        List<PubnativeAPIV3DataModel> beacons = this.mData.getBeacons(str);
        if (beacons != null) {
            for (PubnativeAPIV3DataModel next : beacons) {
                String url = next.getURL();
                String stringField = next.getStringField("js");
                if (!TextUtils.isEmpty(url)) {
                    PubnativeTrackingManager.track(context, url);
                } else if (!TextUtils.isEmpty(stringField)) {
                    try {
                        new PubnativeWebView(context).loadBeacon(stringField);
                    } catch (Exception e) {
                        Log.e(TAG, "confirmImpressionBeacons - JS Error: " + e);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void confirmClickBeacons(Context context) {
        Log.v(TAG, "confirmClickBeacons");
        confirmBeacons("click", context);
    }

    /* access modifiers changed from: protected */
    public void confirmImpressionBeacons(Context context) {
        Log.v(TAG, "confirmImpressionBeacons");
        if (this.mUsedAssets != null) {
            for (String track : this.mUsedAssets) {
                PubnativeTrackingManager.track(context, track);
            }
        }
        confirmBeacons("impression", context);
    }

    /* access modifiers changed from: protected */
    public List<PubnativeBeacon> createBeacons(String str) {
        ArrayList arrayList = null;
        if (this.mData == null) {
            Log.w(TAG, "getBeacons - Error: ad data not present");
        } else {
            List<PubnativeAPIV3DataModel> beacons = this.mData.getBeacons(str);
            if (beacons != null && beacons.size() > 0) {
                arrayList = new ArrayList();
                for (PubnativeAPIV3DataModel next : beacons) {
                    PubnativeBeacon pubnativeBeacon = new PubnativeBeacon();
                    pubnativeBeacon.js = next.getStringField("js");
                    pubnativeBeacon.type = str;
                    pubnativeBeacon.url = next.getURL();
                }
            }
        }
        return arrayList;
    }

    public void fetch() {
        prepareClickURL();
    }

    public PubnativeAPIV3DataModel getAsset(String str) {
        return getAsset(str, true);
    }

    /* access modifiers changed from: protected */
    public PubnativeAPIV3DataModel getAsset(String str, boolean z) {
        Log.v(TAG, "getAsset");
        PubnativeAPIV3DataModel pubnativeAPIV3DataModel = null;
        if (this.mData == null) {
            Log.w(TAG, "getAsset - Error: ad data not present");
        } else {
            pubnativeAPIV3DataModel = this.mData.getAsset(str);
            if (pubnativeAPIV3DataModel != null) {
                recordAsset(pubnativeAPIV3DataModel.getStringField(DATA_TRACKING_KEY));
            }
        }
        return pubnativeAPIV3DataModel;
    }

    public int getAssetGroupId() {
        Log.v(TAG, "getAssetGroupId");
        if (this.mData != null) {
            return this.mData.assetgroupid;
        }
        Log.w(TAG, "getAssetGroupId - Error: ad data not present");
        return 0;
    }

    public String getAssetUrl(String str) {
        Log.v(TAG, "getAssetUrl");
        PubnativeAPIV3DataModel asset = getAsset(str);
        if (asset != null) {
            return asset.getURL();
        }
        return null;
    }

    public String getBannerUrl() {
        Log.v(TAG, "getBannerUrl");
        PubnativeAPIV3DataModel asset = getAsset(PubnativeAsset.BANNER);
        if (asset != null) {
            return asset.getURL();
        }
        return null;
    }

    @Deprecated
    public String getBeacon(String str) {
        Log.v(TAG, "getBeacon");
        if (TextUtils.isEmpty(str)) {
            Log.e(TAG, "getBeacon - Error: beacon type is null or empty");
            return null;
        }
        for (PubnativeBeacon next : getBeacons()) {
            if (str.equalsIgnoreCase(next.type)) {
                return next.url;
            }
        }
        return null;
    }

    @Deprecated
    public List<PubnativeBeacon> getBeacons() {
        Log.v(TAG, "getBeacons");
        ArrayList arrayList = new ArrayList();
        if (this.mData == null) {
            Log.w(TAG, "getBeacons - Error: ad data not present");
        } else {
            arrayList.addAll(createBeacons("impression"));
            arrayList.addAll(createBeacons("click"));
        }
        return arrayList;
    }

    public String getClickUrl() {
        Log.v(TAG, "getClickUrl");
        if (this.mData != null) {
            return this.mData.link;
        }
        Log.w(TAG, "getClickUrl - Error: ad data not present");
        return null;
    }

    public View getContentInfo(Context context) {
        PubnativeAPIV3DataModel meta = getMeta(PubnativeMeta.CONTENT_INFO);
        if (context == null) {
            Log.e(TAG, "getContentInfo - not a valid context");
            return null;
        } else if (meta == null) {
            Log.e(TAG, "getContentInfo - contentInfo data not found");
            return null;
        } else if (TextUtils.isEmpty(meta.getStringField("icon"))) {
            Log.e(TAG, "getContentInfo - contentInfo icon not found");
            return null;
        } else if (TextUtils.isEmpty(meta.getStringField(DATA_CONTENTINFO_LINK_KEY))) {
            Log.e(TAG, "getContentInfo - contentInfo link not found");
            return null;
        } else if (TextUtils.isEmpty(meta.getText())) {
            Log.e(TAG, "getContentInfo - contentInfo text not found");
            return null;
        } else {
            final PubnativeContentInfoWidget pubnativeContentInfoWidget = new PubnativeContentInfoWidget(context);
            pubnativeContentInfoWidget.setIconUrl(meta.getStringField("icon"));
            pubnativeContentInfoWidget.setIconClickUrl(meta.getStringField(DATA_CONTENTINFO_LINK_KEY));
            pubnativeContentInfoWidget.setContextText(meta.getText());
            pubnativeContentInfoWidget.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    pubnativeContentInfoWidget.openLayout();
                }
            });
            return pubnativeContentInfoWidget;
        }
    }

    public String getCtaText() {
        Log.v(TAG, "getCtaText");
        PubnativeAPIV3DataModel asset = getAsset(PubnativeAsset.CALL_TO_ACTION);
        if (asset != null) {
            return asset.getText();
        }
        return null;
    }

    public String getDescription() {
        Log.v(TAG, "getDescription");
        PubnativeAPIV3DataModel asset = getAsset(PubnativeAsset.DESCRIPTION);
        if (asset != null) {
            return asset.getText();
        }
        return null;
    }

    public String getIconUrl() {
        Log.v(TAG, "getIconUrl");
        PubnativeAPIV3DataModel asset = getAsset("icon");
        if (asset != null) {
            return asset.getURL();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public RelativeLayout getLoadingView() {
        Log.v(TAG, "getLoadingView");
        if (this.mLoadingView == null) {
            this.mLoadingView = new RelativeLayout(this.mAdView.getContext());
            this.mLoadingView.setGravity(17);
            this.mLoadingView.setBackgroundColor(Color.argb(77, 0, 0, 0));
            this.mLoadingView.setClickable(true);
            this.mLoadingView.addView(new ProgressBar(this.mAdView.getContext()));
        }
        return this.mLoadingView;
    }

    public PubnativeAPIV3DataModel getMeta(String str) {
        Log.v(TAG, "getMeta");
        if (this.mData != null) {
            return this.mData.getMeta(str);
        }
        Log.w(TAG, "getMeta - Error: ad data not present");
        return null;
    }

    @Deprecated
    public String getPortraitBannerUrl() {
        Log.v(TAG, "getPortraitBannerUrl");
        return null;
    }

    public int getRating() {
        Double number;
        Log.v(TAG, "getRating");
        PubnativeAPIV3DataModel asset = getAsset(PubnativeAsset.RATING);
        if (asset == null || (number = asset.getNumber()) == null) {
            return 0;
        }
        return number.intValue();
    }

    /* access modifiers changed from: protected */
    public ViewGroup getRootView() {
        Log.v(TAG, "getRootView");
        if (this.mAdView != null) {
            return (ViewGroup) this.mAdView.getRootView();
        }
        Log.w(TAG, "getRootView - Error: not assigned ad view, cannot retrieve root view");
        return null;
    }

    public String getTitle() {
        Log.v(TAG, "getTitle");
        PubnativeAPIV3DataModel asset = getAsset(PubnativeAsset.TITLE);
        if (asset != null) {
            return asset.getText();
        }
        return null;
    }

    @Deprecated
    public String getType() {
        Log.v(TAG, "getType");
        return getAsset(PubnativeAsset.VAST, false) != null ? "video" : "native";
    }

    public String getVast() {
        Log.v(TAG, "getVast");
        PubnativeAPIV3DataModel asset = getAsset(PubnativeAsset.VAST);
        if (asset != null) {
            return asset.getStringField(PubnativeAsset.VAST);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void hideLoadingView() {
        Log.v(TAG, "hideLoadingView");
        if (getLoadingView() == null) {
            Log.w(TAG, "loading view is still not loaded, thus you cannot hide it");
        } else if (this.mLoadingView.getParent() == null) {
            Log.w(TAG, "loading view is still not attached to any view");
        } else {
            ((ViewGroup) this.mLoadingView.getParent()).removeView(this.mLoadingView);
        }
    }

    /* access modifiers changed from: protected */
    public void invokeOnClick(View view) {
        Log.v(TAG, "invokeOnClick");
        if (this.mListener != null) {
            this.mListener.onPubnativeAdModelClick(this, view);
        }
    }

    /* access modifiers changed from: protected */
    public void invokeOnImpression(View view) {
        Log.v(TAG, "invokeOnImpression");
        this.mIsImpressionConfirmed = true;
        if (this.mListener != null) {
            this.mListener.onPubnativeAdModelImpression(this, view);
        }
    }

    /* access modifiers changed from: protected */
    public void invokeOnOpenOffer() {
        Log.v(TAG, "invokeOnOpenOffer");
        if (this.mListener != null) {
            this.mListener.onPubnativeAdModelOpenOffer(this);
        }
    }

    public boolean isRevenueModelCPA() {
        PubnativeAPIV3DataModel meta = getMeta(PubnativeMeta.REVENUE_MODEL);
        if (meta != null) {
            return meta.getText().equalsIgnoreCase("cpa");
        }
        return false;
    }

    public void onImpression(View view) {
        Log.v(TAG, "onImpressionDetected");
        confirmImpressionBeacons(view.getContext());
        invokeOnImpression(view);
    }

    /* access modifiers changed from: protected */
    public void onPrepareClickURLFinish(String str) {
        this.mClickFinalURL = str;
        this.mIsClickPreparing = false;
        if (this.mIsWaitingForClickCache) {
            this.mIsWaitingForClickCache = false;
            openCachedClick(this.mContext);
            hideLoadingView();
        }
    }

    public void onURLDrillerFail(String str, Exception exc) {
        Log.v(TAG, "onURLDrillerFail: " + exc);
        if (this.mClickFinalURL == null) {
            openURL(str);
        }
        hideLoadingView();
    }

    public void onURLDrillerFinish(String str) {
        Log.v(TAG, "onURLDrillerFinish: " + str);
        if (this.mClickFinalURL == null) {
            openURL(str);
        }
        hideLoadingView();
    }

    public void onURLDrillerRedirect(String str) {
        Log.v(TAG, "onURLDrillerRedirect: " + str);
    }

    public void onURLDrillerStart(String str) {
        Log.v(TAG, "onURLDrillerStart: " + str);
    }

    /* access modifiers changed from: protected */
    public void openCachedClick(Context context) {
        URLDriller uRLDriller = new URLDriller();
        uRLDriller.setDrillSize(15);
        uRLDriller.setUserAgent(SystemUtils.getWebViewUserAgent(context));
        uRLDriller.setListener(this);
        uRLDriller.drill(getClickUrl() + "&cached=true&uuid=" + this.mUUID.toString());
        openURL(this.mClickFinalURL);
    }

    /* access modifiers changed from: protected */
    public void openURL(String str) {
        Log.v(TAG, "openURL: " + str);
        if (TextUtils.isEmpty(str)) {
            Log.w(TAG, "Error: ending URL cannot be opened - " + str);
        } else if (this.mClickableView == null) {
            Log.w(TAG, "Error: clickable view not set");
        } else {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.setFlags(268435456);
                this.mClickableView.getContext().startActivity(intent);
                invokeOnOpenOffer();
            } catch (Exception e) {
                Log.w(TAG, "openURL: Error - " + e.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void prepareClickURL() {
        if (isRevenueModelCPA() && this.mIsClickCachingEnabled && this.mClickFinalURL == null && !this.mIsClickPreparing) {
            this.mIsClickPreparing = true;
            this.mUUID = UUID.randomUUID();
            URLDriller uRLDriller = new URLDriller();
            uRLDriller.setDrillSize(15);
            uRLDriller.setListener(new URLDriller.Listener() {
                public void onURLDrillerFail(String str, Exception exc) {
                    PubnativeAdModel.this.onPrepareClickURLFinish(str);
                }

                public void onURLDrillerFinish(String str) {
                    PubnativeAdModel.this.onPrepareClickURLFinish(str);
                }

                public void onURLDrillerRedirect(String str) {
                }

                public void onURLDrillerStart(String str) {
                }
            });
            uRLDriller.drill(getClickUrl() + "&uxc=true&uuid=" + this.mUUID.toString());
        }
    }

    /* access modifiers changed from: protected */
    public void recordAsset(String str) {
        Log.v(TAG, "recordAsset");
        if (!TextUtils.isEmpty(str)) {
            if (this.mUsedAssets == null) {
                this.mUsedAssets = new ArrayList();
            }
            if (!this.mUsedAssets.contains(str)) {
                this.mUsedAssets.add(str);
            }
        }
    }

    public void setUseClickCaching(boolean z) {
        Log.v(TAG, "setUseClickCaching");
        this.mIsClickCachingEnabled = z;
    }

    public void setUseClickInBackground(boolean z) {
        Log.v(TAG, "setUseClickInBackground");
        this.mIsClickInBackgroundEnabled = z;
    }

    public void setUseClickLoader(boolean z) {
        Log.v(TAG, "setUseClickLoader");
        this.mIsClickLoaderEnabled = z;
    }

    /* access modifiers changed from: protected */
    public void showLoadingView() {
        Log.v(TAG, "showLoadingView");
        if (getRootView() == null) {
            Log.w(TAG, "showLoadingView - Error: impossible to retrieve root view");
            return;
        }
        hideLoadingView();
        getRootView().addView(getLoadingView(), new ViewGroup.LayoutParams(-1, -1));
    }

    public void startTracking(View view, View view2, Listener listener) {
        Log.v(TAG, "startTracking");
        if (listener == null) {
            Log.w(TAG, "startTracking - listener is null, start tracking without callbacks");
        }
        this.mListener = listener;
        stopTracking();
        startTrackingImpression(view);
        startTrackingClicks(view2);
    }

    public void startTracking(View view, Listener listener) {
        Log.v(TAG, "startTracking: both ad view & clickable view are same");
        startTracking(view, view, listener);
    }

    /* access modifiers changed from: protected */
    public void startTrackingClicks(View view) {
        Log.d(TAG, "startTrackingClicks");
        if (TextUtils.isEmpty(getClickUrl())) {
            Log.w(TAG, "startTrackingClicks - Error: click url is empty, clicks won't be tracked");
        } else if (view == null) {
            Log.w(TAG, "startTrackingClicks - Error: click view is null, clicks won't be tracked");
        } else {
            prepareClickURL();
            this.mClickableView = view;
            this.mClickableView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Log.v(PubnativeAdModel.TAG, "onClick detected");
                    if (PubnativeAdModel.this.mIsClickLoaderEnabled) {
                        PubnativeAdModel.this.showLoadingView();
                    }
                    PubnativeAdModel.this.invokeOnClick(view);
                    PubnativeAdModel.this.confirmClickBeacons(view.getContext());
                    if (!PubnativeAdModel.this.mIsClickInBackgroundEnabled) {
                        PubnativeAdModel.this.openURL(PubnativeAdModel.this.getClickUrl());
                    } else if (!PubnativeAdModel.this.mIsClickCachingEnabled) {
                        URLDriller uRLDriller = new URLDriller();
                        uRLDriller.setDrillSize(15);
                        uRLDriller.setUserAgent(SystemUtils.getWebViewUserAgent(view.getContext()));
                        uRLDriller.setListener(PubnativeAdModel.this);
                        uRLDriller.drill(PubnativeAdModel.this.getClickUrl());
                    } else if (PubnativeAdModel.this.mClickFinalURL == null) {
                        PubnativeAdModel.this.mIsWaitingForClickCache = true;
                    } else {
                        PubnativeAdModel.this.openCachedClick(view.getContext());
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void startTrackingImpression(View view) {
        Log.d(TAG, "startTrackingImpression");
        if (view == null) {
            Log.w(TAG, "startTrackingImpression - ad view is null, cannot start tracking");
        } else if (this.mIsImpressionConfirmed) {
            Log.v(TAG, "startTrackingImpression - impression is already confirmed, dropping impression tracking");
        } else {
            this.mAdView = view;
            PubnativeImpressionManager.startTrackingView(view, this);
        }
    }

    public void stopTracking() {
        Log.v(TAG, "stopTracking");
        stopTrackingImpression();
        stopTrackingClicks();
    }

    /* access modifiers changed from: protected */
    public void stopTrackingClicks() {
        Log.v(TAG, "stopTrackingClicks");
        if (this.mClickableView != null) {
            this.mClickableView.setOnClickListener((View.OnClickListener) null);
        }
    }

    /* access modifiers changed from: protected */
    public void stopTrackingImpression() {
        Log.v(TAG, "stopTrackingImpression");
        PubnativeImpressionManager.stopTrackingAll(this);
    }
}
