package net.pubnative.library.request.model;

@Deprecated
class PubnativeBeacon {
    String js;
    String type;
    String url;

    interface BeaconType {
        public static final String CLICK = "click";
        public static final String IMPRESSION = "impression";
    }

    PubnativeBeacon() {
    }
}
