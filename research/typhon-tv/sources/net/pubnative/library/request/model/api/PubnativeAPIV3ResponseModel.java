package net.pubnative.library.request.model.api;

import java.util.List;

public class PubnativeAPIV3ResponseModel {
    public List<PubnativeAPIV3AdModel> ads;
    public String error_message;
    public List<PubnativeAPIV3ExtModel> ext;
    public String status;

    public interface Status {
        public static final String ERROR = "error";
        public static final String OK = "ok";
    }
}
