package net.pubnative.library.request.model.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PubnativeAPIV3AdModel implements Serializable {
    public int assetgroupid;
    public List<PubnativeAPIV3DataModel> assets;
    public List<PubnativeAPIV3DataModel> beacons;
    public String link;
    public List<PubnativeAPIV3DataModel> meta;

    public interface Beacon {
        public static final String CLICK = "click";
        public static final String IMPRESSION = "impression";
    }

    /* access modifiers changed from: protected */
    public PubnativeAPIV3DataModel find(String str, List<PubnativeAPIV3DataModel> list) {
        if (list == null) {
            return null;
        }
        for (PubnativeAPIV3DataModel next : list) {
            if (str.equals(next.type)) {
                return next;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public List<PubnativeAPIV3DataModel> findAll(String str, List<PubnativeAPIV3DataModel> list) {
        ArrayList arrayList = null;
        if (list != null) {
            for (PubnativeAPIV3DataModel next : list) {
                if (str.equals(next.type)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }

    public PubnativeAPIV3DataModel getAsset(String str) {
        return find(str, this.assets);
    }

    public List<PubnativeAPIV3DataModel> getBeacons(String str) {
        return findAll(str, this.beacons);
    }

    public PubnativeAPIV3DataModel getMeta(String str) {
        return find(str, this.meta);
    }
}
