package net.pubnative.library.request.model.api;

import com.google.android.exoplayer2.util.MimeTypes;
import java.io.Serializable;
import java.util.Map;

public class PubnativeAPIV3DataModel implements Serializable {
    public Map data;
    public String type;

    /* access modifiers changed from: protected */
    public Object getDataField(String str) {
        if (this.data == null || !this.data.containsKey(str)) {
            return null;
        }
        return this.data.get(str);
    }

    public Double getNumber() {
        return getNumberField("number");
    }

    public Double getNumberField(String str) {
        return (Double) getDataField(str);
    }

    public String getStringField(String str) {
        return (String) getDataField(str);
    }

    public String getText() {
        return getStringField(MimeTypes.BASE_TYPE_TEXT);
    }

    public String getURL() {
        return getStringField("url");
    }
}
