package net.pubnative.library.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;

public class PubnativeWebView extends WebView {
    private static final String TAG = PubnativeWebView.class.getSimpleName();

    public PubnativeWebView(Context context) {
        super(context);
        init();
    }

    public PubnativeWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public PubnativeWebView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        getSettings().setJavaScriptEnabled(true);
    }

    public void loadBeacon(String str) {
        Log.v(TAG, "loadBeacon");
        loadUrl("javascript:" + str);
    }
}
