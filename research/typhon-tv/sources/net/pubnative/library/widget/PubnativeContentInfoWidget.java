package net.pubnative.library.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import net.pubnative.library.R;
import net.pubnative.library.utils.ImageDownloader;

public class PubnativeContentInfoWidget extends RelativeLayout implements View.OnClickListener {
    /* access modifiers changed from: private */
    public static final String TAG = PubnativeContentInfoWidget.class.getSimpleName();
    private Runnable mCloseTask = new Runnable() {
        public void run() {
            PubnativeContentInfoWidget.this.closeLayout();
        }
    };
    private RelativeLayout mContainerView;
    /* access modifiers changed from: private */
    public ImageView mContentInfoIcon;
    private TextView mContentInfoText;
    private Handler mHandler;

    public PubnativeContentInfoWidget(Context context) {
        super(context);
        init(context);
    }

    public PubnativeContentInfoWidget(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public PubnativeContentInfoWidget(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    public void closeLayout() {
        this.mContentInfoText.setVisibility(8);
    }

    public void init(Context context) {
        LayoutInflater from = LayoutInflater.from(context);
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mContainerView = (RelativeLayout) from.inflate(R.layout.content_info_layout, this, true);
        this.mContentInfoIcon = (ImageView) this.mContainerView.findViewById(R.id.ic_context_icon);
        this.mContentInfoText = (TextView) this.mContainerView.findViewById(R.id.tv_context_text);
    }

    public void onClick(View view) {
        openLayout();
    }

    public void openLayout() {
        this.mContentInfoText.setVisibility(0);
        this.mHandler.postDelayed(this.mCloseTask, 3000);
    }

    public void setContextText(String str) {
        if (str != null && !str.isEmpty()) {
            this.mContentInfoText.setText(str);
        }
    }

    public void setIconClickUrl(final String str) {
        this.mContentInfoText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setFlags(268435456);
                    intent.setData(Uri.parse(str));
                    view.getContext().startActivity(intent);
                } catch (Exception e) {
                    Log.e(PubnativeContentInfoWidget.TAG, "error on click content info text", e);
                }
            }
        });
    }

    public void setIconUrl(String str) {
        new ImageDownloader().load(str, new ImageDownloader.Listener() {
            public void onImageFailed(String str, Exception exc) {
            }

            public void onImageLoad(String str, Bitmap bitmap) {
                PubnativeContentInfoWidget.this.mContentInfoIcon.setImageBitmap(bitmap);
            }
        });
    }
}
