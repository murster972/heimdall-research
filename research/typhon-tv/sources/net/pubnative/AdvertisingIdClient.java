package net.pubnative;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import java.util.concurrent.LinkedBlockingQueue;

public class AdvertisingIdClient {
    private static final String TAG = AdvertisingIdClient.class.getSimpleName();
    protected Handler mHandler;
    protected Listener mListener;

    public class AdInfo {
        private final String mAdvertisingId;
        private final boolean mLimitAdTrackingEnabled;

        AdInfo(String str, boolean z) {
            this.mAdvertisingId = str;
            this.mLimitAdTrackingEnabled = z;
        }

        public String getId() {
            return this.mAdvertisingId;
        }

        public boolean isLimitAdTrackingEnabled() {
            return this.mLimitAdTrackingEnabled;
        }
    }

    protected class AdvertisingConnection implements ServiceConnection {
        private final LinkedBlockingQueue<IBinder> queue = new LinkedBlockingQueue<>(1);
        boolean retrieved = false;

        protected AdvertisingConnection() {
        }

        public IBinder getBinder() throws InterruptedException {
            if (this.retrieved) {
                throw new IllegalStateException();
            }
            this.retrieved = true;
            return this.queue.take();
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.queue.put(iBinder);
            } catch (InterruptedException e) {
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }
    }

    protected class AdvertisingInterface implements IInterface {
        private IBinder binder;

        public AdvertisingInterface(IBinder iBinder) {
            this.binder = iBinder;
        }

        public IBinder asBinder() {
            return this.binder;
        }

        public String getId() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.binder.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readString();
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        public boolean isLimitAdTrackingEnabled(boolean z) throws RemoteException {
            boolean z2 = true;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(z ? 1 : 0);
                this.binder.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z2 = false;
                }
                return z2;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
    }

    public interface Listener {
        void onAdvertisingIdClientFail(Exception exc);

        void onAdvertisingIdClientFinish(AdInfo adInfo);
    }

    public static synchronized void getAdvertisingId(Context context, Listener listener) {
        synchronized (AdvertisingIdClient.class) {
            new AdvertisingIdClient().start(context, listener);
        }
    }

    /* access modifiers changed from: private */
    public void getAdvertisingIdInfo(Context context) {
        Log.v(TAG, "getAdvertisingIdInfo");
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            AdvertisingConnection advertisingConnection = new AdvertisingConnection();
            try {
                if (context.bindService(intent, advertisingConnection, 1)) {
                    AdvertisingInterface advertisingInterface = new AdvertisingInterface(advertisingConnection.getBinder());
                    invokeFinish(new AdInfo(advertisingInterface.getId(), advertisingInterface.isLimitAdTrackingEnabled(true)));
                }
                context.unbindService(advertisingConnection);
            } catch (Exception e) {
                Log.e(TAG, "getAdvertisingIdInfo - Error: " + e);
                invokeFail(e);
                context.unbindService(advertisingConnection);
            } catch (Throwable th) {
                context.unbindService(advertisingConnection);
                throw th;
            }
        } catch (Exception e2) {
            Log.e(TAG, "getAdvertisingIdInfo - Error: " + e2);
            invokeFail(e2);
        }
    }

    /* access modifiers changed from: protected */
    public void invokeFail(final Exception exc) {
        Log.v(TAG, "invokeFail: " + exc);
        this.mHandler.post(new Runnable() {
            public void run() {
                if (AdvertisingIdClient.this.mListener != null) {
                    AdvertisingIdClient.this.mListener.onAdvertisingIdClientFail(exc);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void invokeFinish(final AdInfo adInfo) {
        Log.v(TAG, "invokeFinish");
        this.mHandler.post(new Runnable() {
            public void run() {
                if (AdvertisingIdClient.this.mListener != null) {
                    AdvertisingIdClient.this.mListener.onAdvertisingIdClientFinish(adInfo);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void start(final Context context, Listener listener) {
        if (listener == null) {
            Log.e(TAG, "getAdvertisingId - Error: null listener, dropping call");
            return;
        }
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mListener = listener;
        if (context == null) {
            invokeFail(new Exception(TAG + " - Error: context null"));
        } else {
            new Thread(new Runnable() {
                public void run() {
                    AdvertisingIdClient.this.getAdvertisingIdInfo(context);
                }
            }).start();
        }
    }
}
