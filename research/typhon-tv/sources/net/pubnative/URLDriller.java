package net.pubnative;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.net.HttpURLConnection;
import java.net.URL;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class URLDriller {
    private static final String TAG = URLDriller.class.getSimpleName();
    private int mDrillSize = 0;
    protected Handler mHandler;
    protected Listener mListener;
    private String mUserAgent = null;

    public interface Listener {
        void onURLDrillerFail(String str, Exception exc);

        void onURLDrillerFinish(String str);

        void onURLDrillerRedirect(String str);

        void onURLDrillerStart(String str);
    }

    /* access modifiers changed from: protected */
    public void doDrill(String str) {
        doDrill(str, 0);
    }

    /* access modifiers changed from: protected */
    public void doDrill(String str, int i) {
        Log.v(TAG, "doDrill: " + str);
        try {
            URL url = new URL(str);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            if (this.mUserAgent != null) {
                httpURLConnection.setRequestProperty(AbstractSpiCall.HEADER_USER_AGENT, this.mUserAgent);
            }
            httpURLConnection.setInstanceFollowRedirects(false);
            httpURLConnection.connect();
            httpURLConnection.setReadTimeout(5000);
            int responseCode = httpURLConnection.getResponseCode();
            Log.v(TAG, " - Status: " + responseCode);
            switch (responseCode) {
                case 200:
                    Log.v(TAG, " - Done: " + str);
                    invokeFinish(str);
                    return;
                case 301:
                case 302:
                case 303:
                    String headerField = httpURLConnection.getHeaderField("Location");
                    Log.v(TAG, " - Redirecting: " + headerField);
                    if (headerField.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                        headerField = url.getProtocol() + "://" + url.getHost() + headerField;
                    }
                    invokeRedirect(headerField);
                    httpURLConnection.disconnect();
                    if (this.mDrillSize == 0) {
                        doDrill(headerField);
                        return;
                    } else if (this.mDrillSize <= 0 || i >= this.mDrillSize) {
                        invokeFinish(str);
                        return;
                    } else {
                        doDrill(headerField, i + 1);
                        return;
                    }
                default:
                    Exception exc = new Exception("Drilling error: Invalid URL, Status: " + responseCode);
                    Log.e(TAG, exc.toString());
                    invokeFail(str, exc);
                    return;
            }
        } catch (Exception e) {
            Log.e(TAG, "Drilling error: " + e);
            invokeFail(str, e);
        }
        Log.e(TAG, "Drilling error: " + e);
        invokeFail(str, e);
    }

    public void drill(final String str) {
        if (TextUtils.isEmpty(str)) {
            invokeFail(str, new IllegalArgumentException("URLDrill error: url is null or empty"));
            return;
        }
        this.mHandler = new Handler(Looper.getMainLooper());
        new Thread(new Runnable() {
            public void run() {
                URLDriller.this.invokeStart(str);
                URLDriller.this.doDrill(str);
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void invokeFail(final String str, final Exception exc) {
        Log.v(TAG, "invokeFail: " + exc);
        this.mHandler.post(new Runnable() {
            public void run() {
                if (URLDriller.this.mListener != null) {
                    URLDriller.this.mListener.onURLDrillerFail(str == null ? "" : str, exc);
                }
                URLDriller.this.mListener = null;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void invokeFinish(final String str) {
        Log.v(TAG, "invokeFinish");
        this.mHandler.post(new Runnable() {
            public void run() {
                if (URLDriller.this.mListener != null) {
                    URLDriller.this.mListener.onURLDrillerFinish(str);
                }
                URLDriller.this.mListener = null;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void invokeRedirect(final String str) {
        Log.v(TAG, "invokeRedirect");
        this.mHandler.post(new Runnable() {
            public void run() {
                if (URLDriller.this.mListener != null) {
                    URLDriller.this.mListener.onURLDrillerRedirect(str);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void invokeStart(final String str) {
        Log.v(TAG, "invokeStart");
        this.mHandler.post(new Runnable() {
            public void run() {
                if (URLDriller.this.mListener != null) {
                    URLDriller.this.mListener.onURLDrillerStart(str);
                }
            }
        });
    }

    public void setDrillSize(int i) {
        this.mDrillSize = i;
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    public void setUserAgent(String str) {
        this.mUserAgent = str;
    }
}
