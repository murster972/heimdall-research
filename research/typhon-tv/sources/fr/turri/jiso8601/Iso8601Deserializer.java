package fr.turri.jiso8601;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import org.apache.commons.lang3.time.TimeZones;

public class Iso8601Deserializer {
    /* renamed from: ʻ  reason: contains not printable characters */
    private static Calendar m18902(Calendar calendar, String str) {
        calendar.set(1, Integer.parseInt(str.substring(0, 4)));
        calendar.set(6, Integer.parseInt(str.substring(4)));
        return calendar;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static Calendar m18903(Calendar calendar, String str) {
        calendar.set(1, Integer.parseInt(str.substring(0, 4)));
        calendar.set(3, Integer.parseInt(str.substring(5, 7)));
        calendar.set(7, str.length() == 7 ? 2 : Integer.parseInt(str.substring(7)) + 1);
        return calendar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Calendar m18904(String str) {
        if (str.indexOf(84) == -1) {
            return m18910(str, str);
        }
        int indexOf = str.indexOf(84);
        return m18911(m18910(str.substring(0, indexOf), str), str.substring(indexOf + 1));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static Calendar m18905(Calendar calendar, String str, String str2) {
        int parseInt = Integer.parseInt(str.substring(0, 4));
        int parseInt2 = Integer.parseInt(str.substring(4, 6)) - 1;
        if (str.length() == 6) {
            calendar.set(parseInt, parseInt2, 1);
        } else if (str.length() == 8) {
            calendar.set(parseInt, parseInt2, Integer.parseInt(str.substring(6)));
        } else {
            throw new RuntimeException("Can't parse " + str2);
        }
        return calendar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m18906(Calendar calendar, String str) {
        String replace = str.replace(',', '.');
        int indexOf = replace.indexOf(46);
        double d = 0.0d;
        if (indexOf != -1) {
            d = Double.parseDouble("0" + replace.substring(indexOf));
            replace = replace.substring(0, indexOf);
        }
        if (replace.length() >= 2) {
            calendar.set(11, Integer.parseInt(replace.substring(0, 2)));
        }
        if (replace.length() > 2) {
            calendar.set(12, Integer.parseInt(replace.substring(2, 4)));
        } else {
            d *= 60.0d;
        }
        if (replace.length() > 4) {
            calendar.set(13, Integer.parseInt(replace.substring(4, 6)));
        } else {
            d *= 60.0d;
        }
        calendar.set(14, (int) (1000.0d * d));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static Calendar m18907(Calendar calendar, String str) {
        calendar.set(Integer.parseInt(str), 0, 1);
        return calendar;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m18908(String str) {
        int indexOf = str.indexOf(43);
        return indexOf != -1 ? indexOf : str.indexOf(45);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static Calendar m18909(Calendar calendar, String str) {
        calendar.set(Integer.parseInt(str) * 100, 0, 1);
        return calendar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Calendar m18910(String str, String str2) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        gregorianCalendar.setMinimalDaysInFirstWeek(4);
        gregorianCalendar.setFirstDayOfWeek(2);
        gregorianCalendar.set(11, 0);
        gregorianCalendar.set(12, 0);
        gregorianCalendar.set(13, 0);
        gregorianCalendar.set(14, 0);
        String replaceAll = str.replaceAll("-", "");
        return replaceAll.indexOf(87) != -1 ? m18903(gregorianCalendar, replaceAll) : replaceAll.length() == 7 ? m18902(gregorianCalendar, replaceAll) : m18912(gregorianCalendar, replaceAll, str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Calendar m18911(Calendar calendar, String str) {
        String replace = str.replace(":", "");
        int indexOf = replace.indexOf(90);
        if (indexOf != -1) {
            m18906(calendar, replace.substring(0, indexOf));
        } else {
            int r1 = m18908(replace);
            if (r1 == -1) {
                m18906(calendar, replace);
                calendar.setTimeZone(TimeZone.getDefault());
            } else {
                m18906(calendar, replace.substring(0, r1));
                calendar.setTimeZone(TimeZone.getTimeZone(TimeZones.GMT_ID + replace.substring(r1)));
            }
        }
        return calendar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Calendar m18912(Calendar calendar, String str, String str2) {
        return str.length() == 2 ? m18909(calendar, str) : str.length() == 4 ? m18907(calendar, str) : m18905(calendar, str, str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Date m18913(String str) {
        return m18904(str).getTime();
    }
}
