package io.fabric.sdk.android.services.persistence;

import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import java.io.File;

public class FileStoreImpl implements FileStore {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f15399;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f15400;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f15401;

    public FileStoreImpl(Kit kit) {
        if (kit.getContext() == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.f15401 = kit.getContext();
        this.f15399 = kit.getPath();
        this.f15400 = "Android/" + this.f15401.getPackageName();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public File m19394() {
        return m19395(this.f15401.getFilesDir());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public File m19395(File file) {
        if (file == null) {
            Fabric.m19034().m19090("Fabric", "Null File");
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            Fabric.m19034().m19085("Fabric", "Couldn't create file");
        }
        return null;
    }
}
