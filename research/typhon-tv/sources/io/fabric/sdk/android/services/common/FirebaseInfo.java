package io.fabric.sdk.android.services.common;

import android.content.Context;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;

public class FirebaseInfo {
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m19190(Context context) {
        if (CommonUtils.m19179(context, "com.crashlytics.useFirebaseAppId", false)) {
            return true;
        }
        return (CommonUtils.m19158(context, "google_app_id", "string") != 0) && !(!TextUtils.isEmpty(new ApiKey().m19132(context)) || !TextUtils.isEmpty(new ApiKey().m19131(context)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m19191(Context context) {
        int r0 = CommonUtils.m19158(context, "google_app_id", "string");
        if (r0 == 0) {
            return null;
        }
        Fabric.m19034().m19090("Fabric", "Generating Crashlytics ApiKey from google_app_id in Strings");
        return m19192(context.getResources().getString(r0));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m19192(String str) {
        return CommonUtils.m19150(str).substring(0, 40);
    }
}
