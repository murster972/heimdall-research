package io.fabric.sdk.android.services.concurrency;

import android.annotation.TargetApi;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PriorityThreadPoolExecutor extends ThreadPoolExecutor {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final int f15345 = (f15347 + 1);

    /* renamed from: 齉  reason: contains not printable characters */
    private static final int f15346 = ((f15347 * 2) + 1);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int f15347 = Runtime.getRuntime().availableProcessors();

    protected static final class PriorityThreadFactory implements ThreadFactory {

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f15348;

        public PriorityThreadFactory(int i) {
            this.f15348 = i;
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setPriority(this.f15348);
            thread.setName("Queue");
            return thread;
        }
    }

    <T extends Runnable & Dependency & Task & PriorityProvider> PriorityThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, DependencyPriorityBlockingQueue<T> dependencyPriorityBlockingQueue, ThreadFactory threadFactory) {
        super(i, i2, j, timeUnit, dependencyPriorityBlockingQueue, threadFactory);
        prestartAllCoreThreads();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static PriorityThreadPoolExecutor m19278() {
        return m19279(f15345, f15346);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends Runnable & Dependency & Task & PriorityProvider> PriorityThreadPoolExecutor m19279(int i, int i2) {
        return new PriorityThreadPoolExecutor(i, i2, 1, TimeUnit.SECONDS, new DependencyPriorityBlockingQueue(), new PriorityThreadFactory(10));
    }

    /* access modifiers changed from: protected */
    public void afterExecute(Runnable runnable, Throwable th) {
        Task task = (Task) runnable;
        task.setFinished(true);
        task.setError(th);
        getQueue().recycleBlockedQueue();
        super.afterExecute(runnable, th);
    }

    @TargetApi(9)
    public void execute(Runnable runnable) {
        if (PriorityTask.isProperDelegate(runnable)) {
            super.execute(runnable);
        } else {
            super.execute(newTaskFor(runnable, (Object) null));
        }
    }

    /* access modifiers changed from: protected */
    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new PriorityFutureTask(runnable, t);
    }

    /* access modifiers changed from: protected */
    public <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new PriorityFutureTask(callable);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DependencyPriorityBlockingQueue getQueue() {
        return (DependencyPriorityBlockingQueue) super.getQueue();
    }
}
