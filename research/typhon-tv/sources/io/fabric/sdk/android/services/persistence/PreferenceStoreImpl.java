package io.fabric.sdk.android.services.persistence;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import io.fabric.sdk.android.Kit;

public class PreferenceStoreImpl implements PreferenceStore {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f6179;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f6180;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SharedPreferences f6181;

    public PreferenceStoreImpl(Context context, String str) {
        if (context == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.f6180 = context;
        this.f6179 = str;
        this.f6181 = this.f6180.getSharedPreferences(this.f6179, 0);
    }

    @Deprecated
    public PreferenceStoreImpl(Kit kit) {
        this(kit.getContext(), kit.getClass().getName());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public SharedPreferences.Editor m6824() {
        return this.f6181.edit();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SharedPreferences m6825() {
        return this.f6181;
    }

    @TargetApi(9)
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6826(SharedPreferences.Editor editor) {
        if (Build.VERSION.SDK_INT < 9) {
            return editor.commit();
        }
        editor.apply();
        return true;
    }
}
