package io.fabric.sdk.android.services.concurrency;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AsyncTask<Params, Progress, Result> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final ThreadFactory f15313 = new ThreadFactory() {

        /* renamed from: 龘  reason: contains not printable characters */
        private final AtomicInteger f15327 = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.f15327.getAndIncrement());
        }
    };

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final BlockingQueue<Runnable> f15314 = new LinkedBlockingQueue(128);

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final InternalHandler f15315 = new InternalHandler();

    /* renamed from: ˑ  reason: contains not printable characters */
    private static volatile Executor f15316 = f15320;

    /* renamed from: 连任  reason: contains not printable characters */
    private static final int f15317 = ((f15321 * 2) + 1);

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Executor f15318 = new ThreadPoolExecutor(f15319, f15317, 1, TimeUnit.SECONDS, f15314, f15313);

    /* renamed from: 麤  reason: contains not printable characters */
    private static final int f15319 = (f15321 + 1);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Executor f15320 = new SerialExecutor();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int f15321 = Runtime.getRuntime().availableProcessors();

    /* renamed from: ʾ  reason: contains not printable characters */
    private final AtomicBoolean f15322 = new AtomicBoolean();
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public final AtomicBoolean f15323 = new AtomicBoolean();

    /* renamed from: ˈ  reason: contains not printable characters */
    private volatile Status f15324 = Status.PENDING;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final WorkerRunnable<Params, Result> f15325 = new WorkerRunnable<Params, Result>() {
        public Result call() throws Exception {
            AsyncTask.this.f15323.set(true);
            Process.setThreadPriority(10);
            return AsyncTask.this.m19248(AsyncTask.this.m19259((Params[]) this.f15338));
        }
    };

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final FutureTask<Result> f15326 = new FutureTask<Result>(this.f15325) {
        /* access modifiers changed from: protected */
        public void done() {
            try {
                AsyncTask.this.m19250(get());
            } catch (InterruptedException e) {
                Log.w("AsyncTask", e);
            } catch (ExecutionException e2) {
                throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
            } catch (CancellationException e3) {
                AsyncTask.this.m19250(null);
            }
        }
    };

    private static class AsyncTaskResult<Data> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Data[] f15331;

        /* renamed from: 龘  reason: contains not printable characters */
        final AsyncTask f15332;

        AsyncTaskResult(AsyncTask asyncTask, Data... dataArr) {
            this.f15332 = asyncTask;
            this.f15331 = dataArr;
        }
    }

    private static class InternalHandler extends Handler {
        public InternalHandler() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            AsyncTaskResult asyncTaskResult = (AsyncTaskResult) message.obj;
            switch (message.what) {
                case 1:
                    asyncTaskResult.f15332.m19246(asyncTaskResult.f15331[0]);
                    return;
                case 2:
                    asyncTaskResult.f15332.m19255((Progress[]) asyncTaskResult.f15331);
                    return;
                default:
                    return;
            }
        }
    }

    private static class SerialExecutor implements Executor {

        /* renamed from: 靐  reason: contains not printable characters */
        Runnable f15333;

        /* renamed from: 龘  reason: contains not printable characters */
        final LinkedList<Runnable> f15334;

        private SerialExecutor() {
            this.f15334 = new LinkedList<>();
        }

        public synchronized void execute(final Runnable runnable) {
            this.f15334.offer(new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } finally {
                        SerialExecutor.this.m19263();
                    }
                }
            });
            if (this.f15333 == null) {
                m19263();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public synchronized void m19263() {
            Runnable poll = this.f15334.poll();
            this.f15333 = poll;
            if (poll != null) {
                AsyncTask.f15318.execute(this.f15333);
            }
        }
    }

    public enum Status {
        PENDING,
        RUNNING,
        FINISHED
    }

    private static abstract class WorkerRunnable<Params, Result> implements Callable<Result> {

        /* renamed from: 靐  reason: contains not printable characters */
        Params[] f15338;

        private WorkerRunnable() {
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m19246(Result result) {
        if (m19256()) {
            m19254(result);
        } else {
            m19261(result);
        }
        this.f15324 = Status.FINISHED;
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public Result m19248(Result result) {
        f15315.obtainMessage(1, new AsyncTaskResult(this, result)).sendToTarget();
        return result;
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m19250(Result result) {
        if (!this.f15323.get()) {
            m19248(result);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Status m19253() {
        return this.f15324;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m19254(Result result) {
        m19257();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m19255(Progress... progressArr) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m19256() {
        return this.f15322.get();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m19257() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final AsyncTask<Params, Progress, Result> m19258(Executor executor, Params... paramsArr) {
        if (this.f15324 != Status.PENDING) {
            switch (this.f15324) {
                case RUNNING:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case FINISHED:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.f15324 = Status.RUNNING;
        m19260();
        this.f15325.f15338 = paramsArr;
        executor.execute(this.f15326);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Result m19259(Params... paramsArr);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19260() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19261(Result result) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m19262(boolean z) {
        this.f15322.set(true);
        return this.f15326.cancel(z);
    }
}
