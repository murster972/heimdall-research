package io.fabric.sdk.android.services.network;

import io.fabric.sdk.android.Fabric;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

class PinningTrustManager implements X509TrustManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final X509Certificate[] f15391 = new X509Certificate[0];

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Set<X509Certificate> f15392 = Collections.synchronizedSet(new HashSet());

    /* renamed from: 连任  reason: contains not printable characters */
    private final List<byte[]> f15393 = new LinkedList();

    /* renamed from: 靐  reason: contains not printable characters */
    private final TrustManager[] f15394;

    /* renamed from: 麤  reason: contains not printable characters */
    private final long f15395;

    /* renamed from: 齉  reason: contains not printable characters */
    private final SystemKeyStore f15396;

    public PinningTrustManager(SystemKeyStore systemKeyStore, PinningInfoProvider pinningInfoProvider) {
        this.f15394 = m19388(systemKeyStore);
        this.f15396 = systemKeyStore;
        this.f15395 = pinningInfoProvider.getPinCreationTimeInMillis();
        for (String r0 : pinningInfoProvider.getPins()) {
            this.f15393.add(m19387(r0));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19384(X509Certificate[] x509CertificateArr) throws CertificateException {
        if (this.f15395 == -1 || System.currentTimeMillis() - this.f15395 <= 15552000000L) {
            X509Certificate[] r1 = CertificateChainCleaner.m19308(x509CertificateArr, this.f15396);
            int length = r1.length;
            int i = 0;
            while (i < length) {
                if (!m19386(r1[i])) {
                    i++;
                } else {
                    return;
                }
            }
            throw new CertificateException("No valid pins found in chain!");
        }
        Fabric.m19034().m19085("Fabric", "Certificate pins are stale, (" + (System.currentTimeMillis() - this.f15395) + " millis vs " + 15552000000L + " millis) falling back to system trust.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19385(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        for (TrustManager trustManager : this.f15394) {
            ((X509TrustManager) trustManager).checkServerTrusted(x509CertificateArr, str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m19386(X509Certificate x509Certificate) throws CertificateException {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(x509Certificate.getPublicKey().getEncoded());
            for (byte[] equals : this.f15393) {
                if (Arrays.equals(equals, digest)) {
                    return true;
                }
            }
            return false;
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private byte[] m19387(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private TrustManager[] m19388(SystemKeyStore systemKeyStore) {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance("X509");
            instance.init(systemKeyStore.f15398);
            return instance.getTrustManagers();
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        } catch (KeyStoreException e2) {
            throw new AssertionError(e2);
        }
    }

    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        throw new CertificateException("Client certificates not supported!");
    }

    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        if (!this.f15392.contains(x509CertificateArr[0])) {
            m19385(x509CertificateArr, str);
            m19384(x509CertificateArr);
            this.f15392.add(x509CertificateArr[0]);
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        return f15391;
    }
}
