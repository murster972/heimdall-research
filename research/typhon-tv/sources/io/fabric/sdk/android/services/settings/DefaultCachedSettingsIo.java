package io.fabric.sdk.android.services.settings;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.persistence.FileStoreImpl;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import org.json.JSONObject;

class DefaultCachedSettingsIo implements CachedSettingsIo {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Kit f15436;

    public DefaultCachedSettingsIo(Kit kit) {
        this.f15436 = kit;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public JSONObject m19407() {
        Fabric.m19034().m19090("Fabric", "Reading cached settings...");
        FileInputStream fileInputStream = null;
        JSONObject jSONObject = null;
        try {
            File file = new File(new FileStoreImpl(this.f15436).m19394(), "com.crashlytics.settings.json");
            if (file.exists()) {
                FileInputStream fileInputStream2 = new FileInputStream(file);
                try {
                    jSONObject = new JSONObject(CommonUtils.m19165((InputStream) fileInputStream2));
                    fileInputStream = fileInputStream2;
                } catch (Exception e) {
                    e = e;
                    fileInputStream = fileInputStream2;
                    try {
                        Fabric.m19034().m19083("Fabric", "Failed to fetch cached settings", e);
                        CommonUtils.m19176((Closeable) fileInputStream, "Error while closing settings cache file.");
                        return jSONObject;
                    } catch (Throwable th) {
                        th = th;
                        CommonUtils.m19176((Closeable) fileInputStream, "Error while closing settings cache file.");
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileInputStream = fileInputStream2;
                    CommonUtils.m19176((Closeable) fileInputStream, "Error while closing settings cache file.");
                    throw th;
                }
            } else {
                Fabric.m19034().m19090("Fabric", "No cached settings found.");
            }
            CommonUtils.m19176((Closeable) fileInputStream, "Error while closing settings cache file.");
        } catch (Exception e2) {
            e = e2;
        }
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19408(long j, JSONObject jSONObject) {
        Fabric.m19034().m19090("Fabric", "Writing settings to cache file...");
        if (jSONObject != null) {
            FileWriter fileWriter = null;
            try {
                jSONObject.put("expires_at", j);
                FileWriter fileWriter2 = new FileWriter(new File(new FileStoreImpl(this.f15436).m19394(), "com.crashlytics.settings.json"));
                try {
                    fileWriter2.write(jSONObject.toString());
                    fileWriter2.flush();
                    CommonUtils.m19176((Closeable) fileWriter2, "Failed to close settings writer.");
                } catch (Exception e) {
                    e = e;
                    fileWriter = fileWriter2;
                    try {
                        Fabric.m19034().m19083("Fabric", "Failed to cache settings", e);
                        CommonUtils.m19176((Closeable) fileWriter, "Failed to close settings writer.");
                    } catch (Throwable th) {
                        th = th;
                        CommonUtils.m19176((Closeable) fileWriter, "Failed to close settings writer.");
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileWriter = fileWriter2;
                    CommonUtils.m19176((Closeable) fileWriter, "Failed to close settings writer.");
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                Fabric.m19034().m19083("Fabric", "Failed to cache settings", e);
                CommonUtils.m19176((Closeable) fileWriter, "Failed to close settings writer.");
            }
        }
    }
}
