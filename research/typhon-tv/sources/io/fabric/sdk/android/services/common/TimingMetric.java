package io.fabric.sdk.android.services.common;

import android.os.SystemClock;
import android.util.Log;

public class TimingMetric {

    /* renamed from: 连任  reason: contains not printable characters */
    private long f15308;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f15309;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f15310;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f15311;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f15312;

    public TimingMetric(String str, String str2) {
        this.f15312 = str;
        this.f15309 = str2;
        this.f15311 = !Log.isLoggable(str2, 2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m19243() {
        Log.v(this.f15309, this.f15312 + ": " + this.f15308 + "ms");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m19244() {
        if (!this.f15311) {
            if (this.f15308 == 0) {
                this.f15308 = SystemClock.elapsedRealtime() - this.f15310;
                m19243();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m19245() {
        if (!this.f15311) {
            this.f15310 = SystemClock.elapsedRealtime();
            this.f15308 = 0;
        }
    }
}
