package io.fabric.sdk.android.services.settings;

import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.common.ApiKey;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.common.SystemCurrentTimeProvider;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class Settings {

    /* renamed from: 靐  reason: contains not printable characters */
    private final CountDownLatch f15467;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f15468;

    /* renamed from: 齉  reason: contains not printable characters */
    private SettingsController f15469;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AtomicReference<SettingsData> f15470;

    static class LazyHolder {
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public static final Settings f15471 = new Settings();
    }

    private Settings() {
        this.f15470 = new AtomicReference<>();
        this.f15467 = new CountDownLatch(1);
        this.f15468 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Settings m19434() {
        return LazyHolder.f15471;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19435(SettingsData settingsData) {
        this.f15470.set(settingsData);
        this.f15467.countDown();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public SettingsData m19436() {
        try {
            this.f15467.await();
            return this.f15470.get();
        } catch (InterruptedException e) {
            Fabric.m19034().m19082("Fabric", "Interrupted while waiting for settings data.");
            return null;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized boolean m19437() {
        SettingsData r0;
        r0 = this.f15469.m19442(SettingsCacheBehavior.SKIP_CACHE_LOOKUP);
        m19435(r0);
        if (r0 == null) {
            Fabric.m19034().m19083("Fabric", "Failed to force reload of settings from Crashlytics.", (Throwable) null);
        }
        return r0 != null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized boolean m19438() {
        SettingsData r0;
        r0 = this.f15469.m19441();
        m19435(r0);
        return r0 != null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Settings m19439(Kit kit, IdManager idManager, HttpRequestFactory httpRequestFactory, String str, String str2, String str3) {
        Settings settings;
        if (this.f15468) {
            settings = this;
        } else {
            if (this.f15469 == null) {
                Context context = kit.getContext();
                String r23 = idManager.m19210();
                String r6 = new ApiKey().m19134(context);
                String r25 = idManager.m19204();
                SystemCurrentTimeProvider systemCurrentTimeProvider = new SystemCurrentTimeProvider();
                DefaultSettingsJsonTransform defaultSettingsJsonTransform = new DefaultSettingsJsonTransform();
                DefaultCachedSettingsIo defaultCachedSettingsIo = new DefaultCachedSettingsIo(kit);
                String r15 = CommonUtils.m19143(context);
                this.f15469 = new DefaultSettingsController(kit, new SettingsRequest(r6, idManager.m19201(), idManager.m19200(), idManager.m19207(), idManager.m19208(), CommonUtils.m19171(CommonUtils.m19138(context)), str2, str, DeliveryMechanism.determineFrom(r25).getId(), r15), systemCurrentTimeProvider, defaultSettingsJsonTransform, defaultCachedSettingsIo, new DefaultSettingsSpiCall(kit, str3, String.format(Locale.US, "=", new Object[]{r23}), httpRequestFactory));
            }
            this.f15468 = true;
            settings = this;
        }
        return settings;
    }
}
