package io.fabric.sdk.android.services.settings;

import io.fabric.sdk.android.services.common.CurrentTimeProvider;
import org.json.JSONException;
import org.json.JSONObject;

public interface SettingsJsonTransform {
    /* renamed from: 龘  reason: contains not printable characters */
    SettingsData m19444(CurrentTimeProvider currentTimeProvider, JSONObject jSONObject) throws JSONException;
}
