package io.fabric.sdk.android.services.cache;

import android.content.Context;

public class MemoryValueCache<T> extends AbstractValueCache<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private T f15235;

    public MemoryValueCache() {
        this((ValueCache) null);
    }

    public MemoryValueCache(ValueCache<T> valueCache) {
        super(valueCache);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public T m19106(Context context) {
        return this.f15235;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19107(Context context, T t) {
        this.f15235 = t;
    }
}
