package io.fabric.sdk.android.services.settings;

import android.content.Context;
import android.graphics.BitmapFactory;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.common.CommonUtils;

public class IconRequest {

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f15449;

    /* renamed from: 麤  reason: contains not printable characters */
    public final int f15450;

    /* renamed from: 齉  reason: contains not printable characters */
    public final int f15451;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f15452;

    public IconRequest(String str, int i, int i2, int i3) {
        this.f15452 = str;
        this.f15449 = i;
        this.f15451 = i2;
        this.f15450 = i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static IconRequest m19433(Context context, String str) {
        if (str == null) {
            return null;
        }
        try {
            int r1 = CommonUtils.m19140(context);
            Fabric.m19034().m19090("Fabric", "App icon resource ID is " + r1);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(context.getResources(), r1, options);
            return new IconRequest(str, r1, options.outWidth, options.outHeight);
        } catch (Exception e) {
            Fabric.m19034().m19083("Fabric", "Failed to load icon", e);
            return null;
        }
    }
}
