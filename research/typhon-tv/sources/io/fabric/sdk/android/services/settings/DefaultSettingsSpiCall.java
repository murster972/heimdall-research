package io.fabric.sdk.android.services.settings;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class DefaultSettingsSpiCall extends AbstractSpiCall implements SettingsSpiCall {
    public DefaultSettingsSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory) {
        this(kit, str, str2, httpRequestFactory, HttpMethod.GET);
    }

    DefaultSettingsSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, HttpMethod httpMethod) {
        super(kit, str, str2, httpRequestFactory, httpMethod);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Map<String, String> m19426(SettingsRequest settingsRequest) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", settingsRequest.f15485);
        hashMap.put("display_version", settingsRequest.f15484);
        hashMap.put("source", Integer.toString(settingsRequest.f15486));
        if (settingsRequest.f15487 != null) {
            hashMap.put("icon_hash", settingsRequest.f15487);
        }
        String str = settingsRequest.f15483;
        if (!CommonUtils.m19152(str)) {
            hashMap.put("instance", str);
        }
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private HttpRequest m19427(HttpRequest httpRequest, SettingsRequest settingsRequest) {
        m19429(httpRequest, AbstractSpiCall.HEADER_API_KEY, settingsRequest.f15492);
        m19429(httpRequest, AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE);
        m19429(httpRequest, AbstractSpiCall.HEADER_CLIENT_VERSION, this.kit.getVersion());
        m19429(httpRequest, AbstractSpiCall.HEADER_ACCEPT, "application/json");
        m19429(httpRequest, "X-CRASHLYTICS-DEVICE-MODEL", settingsRequest.f15489);
        m19429(httpRequest, "X-CRASHLYTICS-OS-BUILD-VERSION", settingsRequest.f15491);
        m19429(httpRequest, "X-CRASHLYTICS-OS-DISPLAY-VERSION", settingsRequest.f15490);
        m19429(httpRequest, "X-CRASHLYTICS-INSTALLATION-ID", settingsRequest.f15488);
        return httpRequest;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private JSONObject m19428(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e) {
            Fabric.m19034().m19091("Fabric", "Failed to parse settings JSON from " + getUrl(), (Throwable) e);
            Fabric.m19034().m19090("Fabric", "Settings response " + str);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19429(HttpRequest httpRequest, String str, String str2) {
        if (str2 != null) {
            httpRequest.m19358(str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public JSONObject m19430(HttpRequest httpRequest) {
        int r0 = httpRequest.m19344();
        Fabric.m19034().m19090("Fabric", "Settings result was: " + r0);
        if (m19432(r0)) {
            return m19428(httpRequest.m19343());
        }
        Fabric.m19034().m19082("Fabric", "Failed to retrieve settings from " + getUrl());
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public JSONObject m19431(SettingsRequest settingsRequest) {
        JSONObject jSONObject;
        HttpRequest httpRequest = null;
        try {
            Map<String, String> r2 = m19426(settingsRequest);
            httpRequest = m19427(getHttpRequest(r2), settingsRequest);
            Fabric.m19034().m19090("Fabric", "Requesting settings from " + getUrl());
            Fabric.m19034().m19090("Fabric", "Settings query params were: " + r2);
            jSONObject = m19430(httpRequest);
            if (httpRequest != null) {
                Fabric.m19034().m19090("Fabric", "Settings request ID: " + httpRequest.m19346(AbstractSpiCall.HEADER_REQUEST_ID));
            }
        } catch (HttpRequest.HttpRequestException e) {
            Fabric.m19034().m19083("Fabric", "Settings request failed.", e);
            jSONObject = null;
            if (httpRequest != null) {
                Fabric.m19034().m19090("Fabric", "Settings request ID: " + httpRequest.m19346(AbstractSpiCall.HEADER_REQUEST_ID));
            }
        } catch (Throwable th) {
            if (httpRequest != null) {
                Fabric.m19034().m19090("Fabric", "Settings request ID: " + httpRequest.m19346(AbstractSpiCall.HEADER_REQUEST_ID));
            }
            throw th;
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19432(int i) {
        return i == 200 || i == 201 || i == 202 || i == 203;
    }
}
