package io.fabric.sdk.android.services.settings;

public class SettingsRequest {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String f15483;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String f15484;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String f15485;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int f15486;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final String f15487;

    /* renamed from: 连任  reason: contains not printable characters */
    public final String f15488;

    /* renamed from: 靐  reason: contains not printable characters */
    public final String f15489;

    /* renamed from: 麤  reason: contains not printable characters */
    public final String f15490;

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f15491;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f15492;

    public SettingsRequest(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, int i, String str9) {
        this.f15492 = str;
        this.f15489 = str2;
        this.f15491 = str3;
        this.f15490 = str4;
        this.f15488 = str5;
        this.f15483 = str6;
        this.f15484 = str7;
        this.f15485 = str8;
        this.f15486 = i;
        this.f15487 = str9;
    }
}
