package io.fabric.sdk.android.services.settings;

public class AppSettingsData {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean f15427;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final AppIconSettingsData f15428;

    /* renamed from: 连任  reason: contains not printable characters */
    public final String f15429;

    /* renamed from: 靐  reason: contains not printable characters */
    public final String f15430;

    /* renamed from: 麤  reason: contains not printable characters */
    public final String f15431;

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f15432;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f15433;

    public AppSettingsData(String str, String str2, String str3, String str4, String str5, boolean z, AppIconSettingsData appIconSettingsData) {
        this.f15433 = str;
        this.f15430 = str2;
        this.f15432 = str3;
        this.f15431 = str4;
        this.f15429 = str5;
        this.f15427 = z;
        this.f15428 = appIconSettingsData;
    }
}
