package io.fabric.sdk.android.services.common;

import android.content.Context;
import com.mopub.common.GpsHelper;
import io.fabric.sdk.android.Fabric;

class AdvertisingInfoReflectionStrategy implements AdvertisingInfoStrategy {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f15242;

    public AdvertisingInfoReflectionStrategy(Context context) {
        this.f15242 = context.getApplicationContext();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m19119() {
        try {
            return (String) Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getMethod("getId", new Class[0]).invoke(m19120(), new Object[0]);
        } catch (Exception e) {
            Fabric.m19034().m19085("Fabric", "Could not call getId on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
            return null;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private Object m19120() {
        try {
            return Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke((Object) null, new Object[]{this.f15242});
        } catch (Exception e) {
            Fabric.m19034().m19085("Fabric", "Could not call getAdvertisingIdInfo on com.google.android.gms.ads.identifier.AdvertisingIdClient");
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m19121() {
        try {
            return ((Boolean) Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getMethod(GpsHelper.IS_LIMIT_AD_TRACKING_ENABLED_KEY, new Class[0]).invoke(m19120(), new Object[0])).booleanValue();
        } catch (Exception e) {
            Fabric.m19034().m19085("Fabric", "Could not call isLimitAdTrackingEnabled on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public AdvertisingInfo m19122() {
        if (m19123(this.f15242)) {
            return new AdvertisingInfo(m19119(), m19121());
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19123(Context context) {
        try {
            return ((Integer) Class.forName("com.google.android.gms.common.GooglePlayServicesUtil").getMethod("isGooglePlayServicesAvailable", new Class[]{Context.class}).invoke((Object) null, new Object[]{context})).intValue() == 0;
        } catch (Exception e) {
            return false;
        }
    }
}
