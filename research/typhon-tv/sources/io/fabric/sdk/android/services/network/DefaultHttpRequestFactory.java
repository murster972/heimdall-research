package io.fabric.sdk.android.services.network;

import com.mopub.common.TyphoonApp;
import io.fabric.sdk.android.DefaultLogger;
import io.fabric.sdk.android.Logger;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public class DefaultHttpRequestFactory implements HttpRequestFactory {

    /* renamed from: 靐  reason: contains not printable characters */
    private PinningInfoProvider f15366;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f15367;

    /* renamed from: 齉  reason: contains not printable characters */
    private SSLSocketFactory f15368;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Logger f15369;

    public DefaultHttpRequestFactory() {
        this(new DefaultLogger());
    }

    public DefaultHttpRequestFactory(Logger logger) {
        this.f15369 = logger;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private synchronized SSLSocketFactory m19309() {
        if (this.f15368 == null && !this.f15367) {
            this.f15368 = m19310();
        }
        return this.f15368;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private synchronized SSLSocketFactory m19310() {
        SSLSocketFactory sSLSocketFactory;
        this.f15367 = true;
        try {
            sSLSocketFactory = NetworkUtils.m19383(this.f15366);
            this.f15369.m19090("Fabric", "Custom SSL pinning enabled");
        } catch (Exception e) {
            this.f15369.m19083("Fabric", "Exception while validating pinned certs", e);
            sSLSocketFactory = null;
        }
        return sSLSocketFactory;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m19311() {
        this.f15367 = false;
        this.f15368 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m19312(String str) {
        return str != null && str.toLowerCase(Locale.US).startsWith(TyphoonApp.HTTPS);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19313(HttpMethod httpMethod, String str) {
        return m19314(httpMethod, str, Collections.emptyMap());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19314(HttpMethod httpMethod, String str, Map<String, String> map) {
        HttpRequest r0;
        SSLSocketFactory r1;
        switch (httpMethod) {
            case GET:
                r0 = HttpRequest.m19327((CharSequence) str, (Map<?, ?>) map, true);
                break;
            case POST:
                r0 = HttpRequest.m19322((CharSequence) str, (Map<?, ?>) map, true);
                break;
            case PUT:
                r0 = HttpRequest.m19324((CharSequence) str);
                break;
            case DELETE:
                r0 = HttpRequest.m19319((CharSequence) str);
                break;
            default:
                throw new IllegalArgumentException("Unsupported HTTP method!");
        }
        if (!(!m19312(str) || this.f15366 == null || (r1 = m19309()) == null)) {
            ((HttpsURLConnection) r0.m19367()).setSSLSocketFactory(r1);
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19315(PinningInfoProvider pinningInfoProvider) {
        if (this.f15366 != pinningInfoProvider) {
            this.f15366 = pinningInfoProvider;
            m19311();
        }
    }
}
