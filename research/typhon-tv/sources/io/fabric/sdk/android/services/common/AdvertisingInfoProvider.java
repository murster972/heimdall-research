package io.fabric.sdk.android.services.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.persistence.PreferenceStore;
import io.fabric.sdk.android.services.persistence.PreferenceStoreImpl;

class AdvertisingInfoProvider {

    /* renamed from: 靐  reason: contains not printable characters */
    private final PreferenceStore f15238;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f15239;

    public AdvertisingInfoProvider(Context context) {
        this.f15239 = context.getApplicationContext();
        this.f15238 = new PreferenceStoreImpl(context, "TwitterAdvertisingInfoPreferences");
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public AdvertisingInfo m19109() {
        AdvertisingInfo r1 = m19117().m19128();
        if (!m19111(r1)) {
            r1 = m19116().m19128();
            if (!m19111(r1)) {
                Fabric.m19034().m19090("Fabric", "AdvertisingInfo not present");
            } else {
                Fabric.m19034().m19090("Fabric", "Using AdvertisingInfo from Service Provider");
            }
        } else {
            Fabric.m19034().m19090("Fabric", "Using AdvertisingInfo from Reflection Provider");
        }
        return r1;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"CommitPrefEdits"})
    /* renamed from: 靐  reason: contains not printable characters */
    public void m19110(AdvertisingInfo advertisingInfo) {
        if (m19111(advertisingInfo)) {
            this.f15238.m19398(this.f15238.m19396().putString("advertising_id", advertisingInfo.f15237).putBoolean("limit_ad_tracking_enabled", advertisingInfo.f15236));
        } else {
            this.f15238.m19398(this.f15238.m19396().remove("advertising_id").remove("limit_ad_tracking_enabled"));
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m19111(AdvertisingInfo advertisingInfo) {
        return advertisingInfo != null && !TextUtils.isEmpty(advertisingInfo.f15237);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19113(final AdvertisingInfo advertisingInfo) {
        new Thread(new BackgroundPriorityRunnable() {
            public void onRun() {
                AdvertisingInfo r0 = AdvertisingInfoProvider.this.m19109();
                if (!advertisingInfo.equals(r0)) {
                    Fabric.m19034().m19090("Fabric", "Asychronously getting Advertising Info and storing it to preferences");
                    AdvertisingInfoProvider.this.m19110(r0);
                }
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public AdvertisingInfo m19115() {
        return new AdvertisingInfo(this.f15238.m19397().getString("advertising_id", ""), this.f15238.m19397().getBoolean("limit_ad_tracking_enabled", false));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public AdvertisingInfoStrategy m19116() {
        return new AdvertisingInfoServiceStrategy(this.f15239);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public AdvertisingInfoStrategy m19117() {
        return new AdvertisingInfoReflectionStrategy(this.f15239);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public AdvertisingInfo m19118() {
        AdvertisingInfo r0 = m19115();
        if (m19111(r0)) {
            Fabric.m19034().m19090("Fabric", "Using AdvertisingInfo from Preference Store");
            m19113(r0);
            AdvertisingInfo advertisingInfo = r0;
            return r0;
        }
        AdvertisingInfo r02 = m19109();
        m19110(r02);
        AdvertisingInfo advertisingInfo2 = r02;
        return r02;
    }
}
