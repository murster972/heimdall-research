package io.fabric.sdk.android.services.common;

import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.cache.MemoryValueCache;
import io.fabric.sdk.android.services.cache.ValueLoader;

public class InstallerPackageNameProvider {

    /* renamed from: 靐  reason: contains not printable characters */
    private final MemoryValueCache<String> f15289 = new MemoryValueCache<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private final ValueLoader<String> f15290 = new ValueLoader<String>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public String load(Context context) throws Exception {
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            return installerPackageName == null ? "" : installerPackageName;
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    public String m19212(Context context) {
        try {
            String r1 = this.f15289.m19104(context, this.f15290);
            if ("".equals(r1)) {
                return null;
            }
            return r1;
        } catch (Exception e) {
            Fabric.m19034().m19083("Fabric", "Failed to determine installer package name", e);
            return null;
        }
    }
}
