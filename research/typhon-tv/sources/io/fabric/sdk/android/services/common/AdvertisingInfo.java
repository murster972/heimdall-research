package io.fabric.sdk.android.services.common;

class AdvertisingInfo {

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean f15236;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f15237;

    AdvertisingInfo(String str, boolean z) {
        this.f15237 = str;
        this.f15236 = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdvertisingInfo advertisingInfo = (AdvertisingInfo) obj;
        if (this.f15236 != advertisingInfo.f15236) {
            return false;
        }
        if (this.f15237 != null) {
            if (this.f15237.equals(advertisingInfo.f15237)) {
                return true;
            }
        } else if (advertisingInfo.f15237 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.f15237 != null ? this.f15237.hashCode() : 0) * 31;
        if (this.f15236) {
            i = 1;
        }
        return hashCode + i;
    }
}
