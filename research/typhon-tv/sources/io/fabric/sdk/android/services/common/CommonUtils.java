package io.fabric.sdk.android.services.common;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

public class CommonUtils {

    /* renamed from: 靐  reason: contains not printable characters */
    private static Boolean f15247 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private static long f15248 = -1;

    /* renamed from: 齉  reason: contains not printable characters */
    private static final char[] f15249 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Comparator<File> f15250 = new Comparator<File>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(File file, File file2) {
            return (int) (file.lastModified() - file2.lastModified());
        }
    };

    enum Architecture {
        X86_32,
        X86_64,
        ARM_UNKNOWN,
        PPC,
        PPC64,
        ARMV6,
        ARMV7,
        UNKNOWN,
        ARMV7S,
        ARM64;
        

        /* renamed from: ᐧ  reason: contains not printable characters */
        private static final Map<String, Architecture> f15257 = null;

        static {
            f15257 = new HashMap(4);
            f15257.put("armeabi-v7a", ARMV7);
            f15257.put("armeabi", ARMV6);
            f15257.put("arm64-v8a", ARM64);
            f15257.put("x86", X86_32);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static Architecture m19181() {
            String str = Build.CPU_ABI;
            if (TextUtils.isEmpty(str)) {
                Fabric.m19034().m19090("Fabric", "Architecture#getValue()::Build.CPU_ABI returned null or empty");
                return UNKNOWN;
            }
            Architecture architecture = f15257.get(str.toLowerCase(Locale.US));
            return architecture == null ? UNKNOWN : architecture;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m19135(Context context) {
        return "sdk".equals(Build.PRODUCT) || "google_sdk".equals(Build.PRODUCT) || Settings.Secure.getString(context.getContentResolver(), "android_id") == null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m19136(Context context) {
        boolean r2 = m19135(context);
        String str = Build.TAGS;
        if ((!r2 && str != null && str.contains("test-keys")) || new File("/system/app/Superuser.apk").exists()) {
            return true;
        }
        return !r2 && new File("/system/xbin/su").exists();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m19137(Context context) {
        int i = 0;
        if (m19135(context)) {
            i = 0 | 1;
        }
        if (m19136(context)) {
            i |= 2;
        }
        return m19155() ? i | 4 : i;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static String m19138(Context context) {
        int r1 = m19158(context, "io.fabric.android.build_id", "string");
        if (r1 == 0) {
            r1 = m19158(context, "com.crashlytics.android.build_id", "string");
        }
        if (r1 == 0) {
            return null;
        }
        String string = context.getResources().getString(r1);
        Fabric.m19034().m19090("Fabric", "Build ID is: " + string);
        return string;
    }

    @SuppressLint({"MissingPermission"})
    /* renamed from: ʿ  reason: contains not printable characters */
    public static boolean m19139(Context context) {
        if (m19156(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() == null || 0 == 0) ? false : true;
        }
        return true;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static int m19140(Context context) {
        return context.getApplicationContext().getApplicationInfo().icon;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static boolean m19141(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static String m19142(Context context) {
        int i = context.getApplicationContext().getApplicationInfo().icon;
        return i > 0 ? context.getResources().getResourcePackageName(i) : context.getPackageName();
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public static String m19143(Context context) {
        InputStream inputStream = null;
        try {
            inputStream = context.getResources().openRawResource(m19140(context));
            String r2 = m19149(inputStream);
            if (m19152(r2)) {
                r2 = null;
            }
            m19176((Closeable) inputStream, "Failed to close icon input stream.");
            return r2;
        } catch (Exception e) {
            Fabric.m19034().m19083("Fabric", "Could not calculate hash for app icon.", e);
            m19176((Closeable) inputStream, "Failed to close icon input stream.");
            return null;
        } catch (Throwable th) {
            m19176((Closeable) inputStream, "Failed to close icon input stream.");
            throw th;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean m19144(Context context) {
        if (f15247 == null) {
            f15247 = Boolean.valueOf(m19179(context, "com.crashlytics.Trace", false));
        }
        return f15247.booleanValue();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static synchronized long m19145() {
        long j;
        synchronized (CommonUtils.class) {
            if (f15248 == -1) {
                long j2 = 0;
                String r3 = m19164(new File("/proc/meminfo"), "MemTotal");
                if (!TextUtils.isEmpty(r3)) {
                    String upperCase = r3.toUpperCase(Locale.US);
                    try {
                        if (upperCase.endsWith("KB")) {
                            j2 = m19160(upperCase, "KB", 1024);
                        } else if (upperCase.endsWith("MB")) {
                            j2 = m19160(upperCase, "MB", 1048576);
                        } else if (upperCase.endsWith("GB")) {
                            j2 = m19160(upperCase, "GB", 1073741824);
                        } else {
                            Fabric.m19034().m19090("Fabric", "Unexpected meminfo format while computing RAM: " + upperCase);
                        }
                    } catch (NumberFormatException e) {
                        Fabric.m19034().m19083("Fabric", "Unexpected meminfo format while computing RAM: " + upperCase, e);
                    }
                }
                f15248 = j2;
            }
            j = f15248;
        }
        return j;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static long m19146(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m19147(int i) {
        switch (i) {
            case 2:
                return "V";
            case 3:
                return "D";
            case 4:
                return "I";
            case 5:
                return "W";
            case 6:
                return "E";
            case 7:
                return "A";
            default:
                return "?";
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m19148(Context context, String str) {
        int r0 = m19158(context, str, "string");
        return r0 > 0 ? context.getString(r0) : "";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m19149(InputStream inputStream) {
        return m19166(inputStream, "SHA-1");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m19150(String str) {
        return m19168(str, "SHA-256");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m19151(Context context) {
        return !m19135(context) && ((SensorManager) context.getSystemService("sensor")).getDefaultSensor(8) != null;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m19152(String str) {
        return str == null || str.length() == 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static long m19153(String str) {
        StatFs statFs = new StatFs(str);
        long blockSize = (long) statFs.getBlockSize();
        return (blockSize * ((long) statFs.getBlockCount())) - (blockSize * ((long) statFs.getAvailableBlocks()));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Float m19154(Context context) {
        Intent registerReceiver = context.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return null;
        }
        return Float.valueOf(((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1)));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m19155() {
        return Debug.isDebuggerConnected() || Debug.waitingForDebugger();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m19156(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m19157() {
        return Architecture.m19181().ordinal();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m19158(Context context, String str, String str2) {
        return context.getResources().getIdentifier(str, str2, m19142(context));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m19159(Context context, boolean z) {
        Float r0 = m19154(context);
        if (!z || r0 == null) {
            return 1;
        }
        if (((double) r0.floatValue()) >= 99.0d) {
            return 3;
        }
        return ((double) r0.floatValue()) < 99.0d ? 2 : 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static long m19160(String str, String str2, int i) {
        return Long.parseLong(str.split(str2)[0].trim()) * ((long) i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ActivityManager.RunningAppProcessInfo m19161(String str, Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.processName.equals(str)) {
                return next;
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static SharedPreferences m19162(Context context) {
        return context.getSharedPreferences("com.crashlytics.prefs", 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19163(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("value must be zero or greater");
        }
        return String.format(Locale.US, "%1$10s", new Object[]{Integer.valueOf(i)}).replace(' ', '0');
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19164(File file, String str) {
        String str2 = null;
        if (file.exists()) {
            BufferedReader bufferedReader = null;
            try {
                BufferedReader bufferedReader2 = new BufferedReader(new FileReader(file), 1024);
                while (true) {
                    try {
                        String readLine = bufferedReader2.readLine();
                        if (readLine == null) {
                            break;
                        }
                        String[] split = Pattern.compile("\\s*:\\s*").split(readLine, 2);
                        if (split.length > 1 && split[0].equals(str)) {
                            str2 = split[1];
                            break;
                        }
                    } catch (Exception e) {
                        e = e;
                        bufferedReader = bufferedReader2;
                        try {
                            Fabric.m19034().m19083("Fabric", "Error parsing " + file, e);
                            m19176((Closeable) bufferedReader, "Failed to close system file reader.");
                            return str2;
                        } catch (Throwable th) {
                            th = th;
                            m19176((Closeable) bufferedReader, "Failed to close system file reader.");
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        bufferedReader = bufferedReader2;
                        m19176((Closeable) bufferedReader, "Failed to close system file reader.");
                        throw th;
                    }
                }
                m19176((Closeable) bufferedReader2, "Failed to close system file reader.");
            } catch (Exception e2) {
                e = e2;
                Fabric.m19034().m19083("Fabric", "Error parsing " + file, e);
                m19176((Closeable) bufferedReader, "Failed to close system file reader.");
                return str2;
            }
        }
        return str2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19165(InputStream inputStream) throws IOException {
        Scanner useDelimiter = new Scanner(inputStream).useDelimiter("\\A");
        return useDelimiter.hasNext() ? useDelimiter.next() : "";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m19166(InputStream inputStream, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    return m19169(instance.digest());
                }
                instance.update(bArr, 0, read);
            }
        } catch (Exception e) {
            Fabric.m19034().m19083("Fabric", "Could not calculate hash for app icon.", e);
            return "";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19167(String str) {
        return m19168(str, "SHA-1");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m19168(String str, String str2) {
        return m19170(str.getBytes(), str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19169(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b = bArr[i] & 255;
            cArr[i * 2] = f15249[b >>> 4];
            cArr[(i * 2) + 1] = f15249[b & 15];
        }
        return new String(cArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m19170(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            instance.update(bArr);
            return m19169(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            Fabric.m19034().m19083("Fabric", "Could not create hashing algorithm: " + str + ", returning empty string.", e);
            return "";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19171(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        ArrayList<String> arrayList = new ArrayList<>();
        for (String str : strArr) {
            if (str != null) {
                arrayList.add(str.replace("-", "").toLowerCase(Locale.US));
            }
        }
        Collections.sort(arrayList);
        StringBuilder sb = new StringBuilder();
        for (String append : arrayList) {
            sb.append(append);
        }
        String sb2 = sb.toString();
        if (sb2.length() > 0) {
            return m19167(sb2);
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m19172(Context context, int i, String str, String str2) {
        if (m19144(context)) {
            Fabric.m19034().m19088(i, "Fabric", str2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m19173(Context context, String str) {
        if (m19144(context)) {
            Fabric.m19034().m19090("Fabric", str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m19174(Context context, String str, Throwable th) {
        if (m19144(context)) {
            Fabric.m19034().m19082("Fabric", str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m19175(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m19176(Closeable closeable, String str) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Fabric.m19034().m19083("Fabric", str, e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m19177(Flushable flushable, String str) {
        if (flushable != null) {
            try {
                flushable.flush();
            } catch (IOException e) {
                Fabric.m19034().m19083("Fabric", str, e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m19178(InputStream inputStream, OutputStream outputStream, byte[] bArr) throws IOException {
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m19179(Context context, String str, boolean z) {
        Resources resources;
        if (context == null || (resources = context.getResources()) == null) {
            return z;
        }
        int r0 = m19158(context, str, "bool");
        if (r0 > 0) {
            return resources.getBoolean(r0);
        }
        int r02 = m19158(context, str, "string");
        return r02 > 0 ? Boolean.parseBoolean(context.getString(r02)) : z;
    }
}
