package io.fabric.sdk.android.services.concurrency.internal;

public class RetryState {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Backoff f15352;

    /* renamed from: 齉  reason: contains not printable characters */
    private final RetryPolicy f15353;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f15354;

    public RetryState(int i, Backoff backoff, RetryPolicy retryPolicy) {
        this.f15354 = i;
        this.f15352 = backoff;
        this.f15353 = retryPolicy;
    }

    public RetryState(Backoff backoff, RetryPolicy retryPolicy) {
        this(0, backoff, retryPolicy);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public RetryState m19281() {
        return new RetryState(this.f15354 + 1, this.f15352, this.f15353);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public RetryState m19282() {
        return new RetryState(this.f15352, this.f15353);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m19283() {
        return this.f15352.getDelayMillis(this.f15354);
    }
}
