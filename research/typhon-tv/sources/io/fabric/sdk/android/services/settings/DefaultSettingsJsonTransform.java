package io.fabric.sdk.android.services.settings;

import android.support.v4.app.NotificationCompat;
import com.crashlytics.android.beta.BuildConfig;
import com.mopub.mobileads.VastIconXmlManager;
import io.fabric.sdk.android.services.common.CurrentTimeProvider;
import net.pubnative.library.request.PubnativeAsset;
import org.json.JSONException;
import org.json.JSONObject;

class DefaultSettingsJsonTransform implements SettingsJsonTransform {
    DefaultSettingsJsonTransform() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private PromptSettingsData m19417(JSONObject jSONObject) throws JSONException {
        return new PromptSettingsData(jSONObject.optString(PubnativeAsset.TITLE, "Send Crash Report?"), jSONObject.optString("message", "Looks like we crashed! Please help us fix the problem by sending a crash report."), jSONObject.optString("send_button_title", "Send"), jSONObject.optBoolean("show_cancel_button", true), jSONObject.optString("cancel_button_title", "Don't Send"), jSONObject.optBoolean("show_always_send_button", true), jSONObject.optString("always_send_button_title", "Always Send"));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private BetaSettingsData m19418(JSONObject jSONObject) throws JSONException {
        return new BetaSettingsData(jSONObject.optString("update_endpoint", SettingsJsonTyphoonApp.f15482), jSONObject.optInt("update_suspend_duration", 3600));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private SessionSettingsData m19419(JSONObject jSONObject) throws JSONException {
        return new SessionSettingsData(jSONObject.optInt("log_buffer_size", 64000), jSONObject.optInt("max_chained_exception_depth", 8), jSONObject.optInt("max_custom_exception_events", 64), jSONObject.optInt("max_custom_key_value_pairs", 64), jSONObject.optInt("identifier_mask", 255), jSONObject.optBoolean("send_session_without_crash", false), jSONObject.optInt("max_complete_sessions_count", 4));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private AppIconSettingsData m19420(JSONObject jSONObject) throws JSONException {
        return new AppIconSettingsData(jSONObject.getString("hash"), jSONObject.getInt(VastIconXmlManager.WIDTH), jSONObject.getInt(VastIconXmlManager.HEIGHT));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private AnalyticsSettingsData m19421(JSONObject jSONObject) {
        return new AnalyticsSettingsData(jSONObject.optString("url", "="), jSONObject.optInt("flush_interval_secs", 600), jSONObject.optInt("max_byte_size_per_file", 8000), jSONObject.optInt("max_file_count_per_send", 1), jSONObject.optInt("max_pending_send_file_count", 100), jSONObject.optBoolean("forward_to_google_analytics", false), jSONObject.optBoolean("include_purchase_events_in_forwarded_events", false), jSONObject.optBoolean("track_custom_events", true), jSONObject.optBoolean("track_predefined_events", true), jSONObject.optInt("sampling_rate", 1), jSONObject.optBoolean("flush_on_background", true));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private FeaturesSettingsData m19422(JSONObject jSONObject) {
        return new FeaturesSettingsData(jSONObject.optBoolean("prompt_enabled", false), jSONObject.optBoolean("collect_logged_exceptions", true), jSONObject.optBoolean("collect_reports", true), jSONObject.optBoolean("collect_analytics", false), jSONObject.optBoolean("firebase_crashlytics_enabled", false));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m19423(CurrentTimeProvider currentTimeProvider, long j, JSONObject jSONObject) throws JSONException {
        return jSONObject.has("expires_at") ? jSONObject.getLong("expires_at") : currentTimeProvider.m19184() + (1000 * j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private AppSettingsData m19424(JSONObject jSONObject) throws JSONException {
        String string = jSONObject.getString("identifier");
        String string2 = jSONObject.getString(NotificationCompat.CATEGORY_STATUS);
        String string3 = jSONObject.getString("url");
        String string4 = jSONObject.getString("reports_url");
        String string5 = jSONObject.getString("ndk_reports_url");
        boolean optBoolean = jSONObject.optBoolean("update_required", false);
        AppIconSettingsData appIconSettingsData = null;
        if (jSONObject.has(PubnativeAsset.ICON) && jSONObject.getJSONObject(PubnativeAsset.ICON).has("hash")) {
            appIconSettingsData = m19420(jSONObject.getJSONObject(PubnativeAsset.ICON));
        }
        return new AppSettingsData(string, string2, string3, string4, string5, optBoolean, appIconSettingsData);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SettingsData m19425(CurrentTimeProvider currentTimeProvider, JSONObject jSONObject) throws JSONException {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        return new SettingsData(m19423(currentTimeProvider, (long) optInt2, jSONObject), m19424(jSONObject.getJSONObject("app")), m19419(jSONObject.getJSONObject("session")), m19417(jSONObject.getJSONObject("prompt")), m19422(jSONObject.getJSONObject("features")), m19421(jSONObject.getJSONObject("analytics")), m19418(jSONObject.getJSONObject(BuildConfig.ARTIFACT_ID)), optInt, optInt2);
    }
}
