package io.fabric.sdk.android.services.concurrency.internal;

public class ExponentialBackoff implements Backoff {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f15350;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f15351;

    public ExponentialBackoff(long j, int i) {
        this.f15351 = j;
        this.f15350 = i;
    }

    public long getDelayMillis(int i) {
        return (long) (((double) this.f15351) * Math.pow((double) this.f15350, (double) i));
    }
}
