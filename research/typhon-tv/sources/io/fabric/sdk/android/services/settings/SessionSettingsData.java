package io.fabric.sdk.android.services.settings;

public class SessionSettingsData {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean f15460;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int f15461;

    /* renamed from: 连任  reason: contains not printable characters */
    public final int f15462;

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f15463;

    /* renamed from: 麤  reason: contains not printable characters */
    public final int f15464;

    /* renamed from: 齉  reason: contains not printable characters */
    public final int f15465;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f15466;

    public SessionSettingsData(int i, int i2, int i3, int i4, int i5, boolean z, int i6) {
        this.f15466 = i;
        this.f15463 = i2;
        this.f15465 = i3;
        this.f15464 = i4;
        this.f15462 = i5;
        this.f15460 = z;
        this.f15461 = i6;
    }
}
