package io.fabric.sdk.android.services.settings;

public class PromptSettingsData {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean f15453;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String f15454;

    /* renamed from: 连任  reason: contains not printable characters */
    public final String f15455;

    /* renamed from: 靐  reason: contains not printable characters */
    public final String f15456;

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean f15457;

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f15458;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f15459;

    public PromptSettingsData(String str, String str2, String str3, boolean z, String str4, boolean z2, String str5) {
        this.f15459 = str;
        this.f15456 = str2;
        this.f15458 = str3;
        this.f15457 = z;
        this.f15455 = str4;
        this.f15453 = z2;
        this.f15454 = str5;
    }
}
