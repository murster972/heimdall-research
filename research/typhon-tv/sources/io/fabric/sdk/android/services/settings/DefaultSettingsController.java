package io.fabric.sdk.android.services.settings;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.CurrentTimeProvider;
import io.fabric.sdk.android.services.persistence.PreferenceStore;
import io.fabric.sdk.android.services.persistence.PreferenceStoreImpl;
import org.json.JSONException;
import org.json.JSONObject;

class DefaultSettingsController implements SettingsController {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Kit f15437;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final PreferenceStore f15438 = new PreferenceStoreImpl(this.f15437);

    /* renamed from: 连任  reason: contains not printable characters */
    private final SettingsSpiCall f15439;

    /* renamed from: 靐  reason: contains not printable characters */
    private final SettingsJsonTransform f15440;

    /* renamed from: 麤  reason: contains not printable characters */
    private final CachedSettingsIo f15441;

    /* renamed from: 齉  reason: contains not printable characters */
    private final CurrentTimeProvider f15442;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SettingsRequest f15443;

    public DefaultSettingsController(Kit kit, SettingsRequest settingsRequest, CurrentTimeProvider currentTimeProvider, SettingsJsonTransform settingsJsonTransform, CachedSettingsIo cachedSettingsIo, SettingsSpiCall settingsSpiCall) {
        this.f15437 = kit;
        this.f15443 = settingsRequest;
        this.f15442 = currentTimeProvider;
        this.f15440 = settingsJsonTransform;
        this.f15441 = cachedSettingsIo;
        this.f15439 = settingsSpiCall;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private SettingsData m19409(SettingsCacheBehavior settingsCacheBehavior) {
        try {
            if (SettingsCacheBehavior.SKIP_CACHE_LOOKUP.equals(settingsCacheBehavior)) {
                return null;
            }
            JSONObject r4 = this.f15441.m19404();
            if (r4 != null) {
                SettingsData r3 = this.f15440.m19444(this.f15442, r4);
                if (r3 != null) {
                    m19410(r4, "Loaded cached settings: ");
                    long r0 = this.f15442.m19184();
                    if (SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION.equals(settingsCacheBehavior) || !r3.m19443(r0)) {
                        SettingsData settingsData = r3;
                        Fabric.m19034().m19090("Fabric", "Returning cached settings.");
                        return settingsData;
                    }
                    Fabric.m19034().m19090("Fabric", "Cached settings have expired.");
                    return null;
                }
                Fabric.m19034().m19083("Fabric", "Failed to transform cached settings data.", (Throwable) null);
                return null;
            }
            Fabric.m19034().m19090("Fabric", "No cached settings data found.");
            return null;
        } catch (Exception e) {
            Fabric.m19034().m19083("Fabric", "Failed to get cached settings", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19410(JSONObject jSONObject, String str) throws JSONException {
        Fabric.m19034().m19090("Fabric", str + jSONObject.toString());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m19411() {
        return CommonUtils.m19171(CommonUtils.m19138(this.f15437.getContext()));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m19412() {
        return !m19413().equals(m19411());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m19413() {
        return this.f15438.m19397().getString("existing_instance_identifier", "");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SettingsData m19414() {
        return m19415(SettingsCacheBehavior.USE_CACHE);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SettingsData m19415(SettingsCacheBehavior settingsCacheBehavior) {
        JSONObject r1;
        SettingsData settingsData = null;
        try {
            if (!Fabric.m19035() && !m19412()) {
                settingsData = m19409(settingsCacheBehavior);
            }
            if (settingsData == null && (r1 = this.f15439.m19445(this.f15443)) != null) {
                settingsData = this.f15440.m19444(this.f15442, r1);
                this.f15441.m19405(settingsData.f15474, r1);
                m19410(r1, "Loaded settings: ");
                m19416(m19411());
            }
            return settingsData == null ? m19409(SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION) : settingsData;
        } catch (Exception e) {
            Fabric.m19034().m19083("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"CommitPrefEdits"})
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19416(String str) {
        SharedPreferences.Editor r0 = this.f15438.m19396();
        r0.putString("existing_instance_identifier", str);
        return this.f15438.m19398(r0);
    }
}
