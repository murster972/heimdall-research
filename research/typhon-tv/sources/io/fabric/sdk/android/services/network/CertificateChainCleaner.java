package io.fabric.sdk.android.services.network;

import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;

final class CertificateChainCleaner {
    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m19307(X509Certificate x509Certificate, X509Certificate x509Certificate2) {
        if (!x509Certificate.getSubjectX500Principal().equals(x509Certificate2.getIssuerX500Principal())) {
            return false;
        }
        try {
            x509Certificate2.verify(x509Certificate.getPublicKey());
            return true;
        } catch (GeneralSecurityException e) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static X509Certificate[] m19308(X509Certificate[] x509CertificateArr, SystemKeyStore systemKeyStore) throws CertificateException {
        LinkedList linkedList = new LinkedList();
        boolean z = false;
        if (systemKeyStore.m19392(x509CertificateArr[0])) {
            z = true;
        }
        linkedList.add(x509CertificateArr[0]);
        int i = 1;
        while (i < x509CertificateArr.length) {
            if (systemKeyStore.m19392(x509CertificateArr[i])) {
                z = true;
            }
            if (!m19307(x509CertificateArr[i], x509CertificateArr[i - 1])) {
                break;
            }
            linkedList.add(x509CertificateArr[i]);
            i++;
        }
        X509Certificate r2 = systemKeyStore.m19391(x509CertificateArr[i - 1]);
        if (r2 != null) {
            linkedList.add(r2);
            z = true;
        }
        if (z) {
            return (X509Certificate[]) linkedList.toArray(new X509Certificate[linkedList.size()]);
        }
        throw new CertificateException("Didn't find a trust anchor in chain cleanup!");
    }
}
