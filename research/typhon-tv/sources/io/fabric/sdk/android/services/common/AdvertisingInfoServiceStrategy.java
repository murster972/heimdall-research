package io.fabric.sdk.android.services.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import io.fabric.sdk.android.Fabric;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

class AdvertisingInfoServiceStrategy implements AdvertisingInfoStrategy {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f15243;

    private static final class AdvertisingConnection implements ServiceConnection {

        /* renamed from: 靐  reason: contains not printable characters */
        private final LinkedBlockingQueue<IBinder> f15244;

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean f15245;

        private AdvertisingConnection() {
            this.f15245 = false;
            this.f15244 = new LinkedBlockingQueue<>(1);
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.f15244.put(iBinder);
            } catch (InterruptedException e) {
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            this.f15244.clear();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public IBinder m19125() {
            if (this.f15245) {
                Fabric.m19034().m19082("Fabric", "getBinder already called");
            }
            this.f15245 = true;
            try {
                return this.f15244.poll(200, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                return null;
            }
        }
    }

    private static final class AdvertisingInterface implements IInterface {

        /* renamed from: 龘  reason: contains not printable characters */
        private final IBinder f15246;

        public AdvertisingInterface(IBinder iBinder) {
            this.f15246 = iBinder;
        }

        public IBinder asBinder() {
            return this.f15246;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m19126() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            boolean z = false;
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(1);
                this.f15246.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                z = obtain2.readInt() != 0;
            } catch (Exception e) {
                Fabric.m19034().m19090("Fabric", "Could not get parcel from Google Play Service to capture Advertising limitAdTracking");
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
            return z;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m19127() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            String str = null;
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.f15246.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                str = obtain2.readString();
            } catch (Exception e) {
                Fabric.m19034().m19090("Fabric", "Could not get parcel from Google Play Service to capture AdvertisingId");
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
            return str;
        }
    }

    public AdvertisingInfoServiceStrategy(Context context) {
        this.f15243 = context.getApplicationContext();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public AdvertisingInfo m19124() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            Fabric.m19034().m19090("Fabric", "AdvertisingInfoServiceStrategy cannot be called on the main thread");
            return null;
        }
        try {
            this.f15243.getPackageManager().getPackageInfo("com.android.vending", 0);
            AdvertisingConnection advertisingConnection = new AdvertisingConnection();
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            try {
                if (this.f15243.bindService(intent, advertisingConnection, 1)) {
                    AdvertisingInterface advertisingInterface = new AdvertisingInterface(advertisingConnection.m19125());
                    AdvertisingInfo advertisingInfo = new AdvertisingInfo(advertisingInterface.m19127(), advertisingInterface.m19126());
                    this.f15243.unbindService(advertisingConnection);
                    return advertisingInfo;
                }
                Fabric.m19034().m19090("Fabric", "Could not bind to Google Play Service to capture AdvertisingId");
                return null;
            } catch (Exception e) {
                Fabric.m19034().m19086("Fabric", "Exception in binding to Google Play Service to capture AdvertisingId", e);
                this.f15243.unbindService(advertisingConnection);
                return null;
            } catch (Throwable th) {
                Fabric.m19034().m19091("Fabric", "Could not bind to Google Play Service to capture AdvertisingId", th);
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            Fabric.m19034().m19090("Fabric", "Unable to find Google Play Services package name");
            return null;
        } catch (Exception e3) {
            Fabric.m19034().m19091("Fabric", "Unable to determine if Google Play Services is available", (Throwable) e3);
            return null;
        }
    }
}
