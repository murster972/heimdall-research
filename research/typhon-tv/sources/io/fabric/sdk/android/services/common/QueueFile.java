package io.fabric.sdk.android.services.common;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueueFile implements Closeable {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Logger f15292 = Logger.getLogger(QueueFile.class.getName());

    /* renamed from: ʻ  reason: contains not printable characters */
    private Element f15293;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final byte[] f15294 = new byte[16];

    /* renamed from: 连任  reason: contains not printable characters */
    private Element f15295;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f15296;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final RandomAccessFile f15297;

    /* renamed from: 龘  reason: contains not printable characters */
    int f15298;

    static class Element {

        /* renamed from: 龘  reason: contains not printable characters */
        static final Element f15302 = new Element(0, 0);

        /* renamed from: 靐  reason: contains not printable characters */
        final int f15303;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f15304;

        Element(int i, int i2) {
            this.f15303 = i;
            this.f15304 = i2;
        }

        public String toString() {
            return getClass().getSimpleName() + "[position = " + this.f15303 + ", length = " + this.f15304 + "]";
        }
    }

    private final class ElementInputStream extends InputStream {

        /* renamed from: 靐  reason: contains not printable characters */
        private int f15305;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f15306;

        private ElementInputStream(Element element) {
            this.f15305 = QueueFile.this.m19216(element.f15303 + 4);
            this.f15306 = element.f15304;
        }

        public int read() throws IOException {
            if (this.f15306 == 0) {
                return -1;
            }
            QueueFile.this.f15297.seek((long) this.f15305);
            int read = QueueFile.this.f15297.read();
            this.f15305 = QueueFile.this.m19216(this.f15305 + 1);
            this.f15306--;
            return read;
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            Object unused = QueueFile.m19218(bArr, "buffer");
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new ArrayIndexOutOfBoundsException();
            } else if (this.f15306 > 0) {
                if (i2 > this.f15306) {
                    i2 = this.f15306;
                }
                QueueFile.this.m19219(this.f15305, bArr, i, i2);
                this.f15305 = QueueFile.this.m19216(this.f15305 + i2);
                this.f15306 -= i2;
                int i3 = i2;
                return i2;
            } else {
                int i4 = i2;
                return -1;
            }
        }
    }

    public interface ElementReader {
        void read(InputStream inputStream, int i) throws IOException;
    }

    public QueueFile(File file) throws IOException {
        if (!file.exists()) {
            m19231(file);
        }
        this.f15297 = m19217(file);
        m19215();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m19214() {
        return this.f15298 - m19236();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m19215() throws IOException {
        this.f15297.seek(0);
        this.f15297.readFully(this.f15294);
        this.f15298 = m19224(this.f15294, 0);
        if (((long) this.f15298) > this.f15297.length()) {
            throw new IOException("File is truncated. Expected length: " + this.f15298 + ", Actual length: " + this.f15297.length());
        }
        this.f15296 = m19224(this.f15294, 4);
        int r0 = m19224(this.f15294, 8);
        int r1 = m19224(this.f15294, 12);
        this.f15295 = m19225(r0);
        this.f15293 = m19225(r1);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m19216(int i) {
        return i < this.f15298 ? i : (i + 16) - this.f15298;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static RandomAccessFile m19217(File file) throws FileNotFoundException {
        return new RandomAccessFile(file, "rwd");
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> T m19218(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m19219(int i, byte[] bArr, int i2, int i3) throws IOException {
        int r5 = m19216(i);
        if (r5 + i3 <= this.f15298) {
            this.f15297.seek((long) r5);
            this.f15297.readFully(bArr, i2, i3);
            return;
        }
        int i4 = this.f15298 - r5;
        this.f15297.seek((long) r5);
        this.f15297.readFully(bArr, i2, i4);
        this.f15297.seek(16);
        this.f15297.readFully(bArr, i2 + i4, i3 - i4);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m19220(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m19221(int i) throws IOException {
        this.f15297.setLength((long) i);
        this.f15297.getChannel().force(true);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m19222(int i) throws IOException {
        int i2;
        int i3 = i + 4;
        int r12 = m19214();
        if (r12 < i3) {
            int i4 = this.f15298;
            do {
                r12 += i4;
                i2 = i4 << 1;
                i4 = i2;
            } while (r12 < i3);
            m19221(i2);
            int r8 = m19216(this.f15293.f15303 + 4 + this.f15293.f15304);
            if (r8 < this.f15295.f15303) {
                FileChannel channel = this.f15297.getChannel();
                channel.position((long) this.f15298);
                int i5 = r8 - 4;
                if (channel.transferTo(16, (long) i5, channel) != ((long) i5)) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            if (this.f15293.f15303 < this.f15295.f15303) {
                int i6 = (this.f15298 + this.f15293.f15303) - 16;
                m19228(i2, this.f15296, this.f15295.f15303, i6);
                this.f15293 = new Element(i6, this.f15293.f15304);
            } else {
                m19228(i2, this.f15296, this.f15295.f15303, this.f15293.f15303);
            }
            this.f15298 = i2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m19224(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << 16) + ((bArr[i + 2] & 255) << 8) + (bArr[i + 3] & 255);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Element m19225(int i) throws IOException {
        if (i == 0) {
            return Element.f15302;
        }
        this.f15297.seek((long) i);
        return new Element(i, this.f15297.readInt());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19228(int i, int i2, int i3, int i4) throws IOException {
        m19232(this.f15294, i, i2, i3, i4);
        this.f15297.seek(0);
        this.f15297.write(this.f15294);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19229(int i, byte[] bArr, int i2, int i3) throws IOException {
        int r5 = m19216(i);
        if (r5 + i3 <= this.f15298) {
            this.f15297.seek((long) r5);
            this.f15297.write(bArr, i2, i3);
            return;
        }
        int i4 = this.f15298 - r5;
        this.f15297.seek((long) r5);
        this.f15297.write(bArr, i2, i4);
        this.f15297.seek(16);
        this.f15297.write(bArr, i2 + i4, i3 - i4);
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    private static void m19231(File file) throws IOException {
        File file2 = new File(file.getPath() + ".tmp");
        RandomAccessFile r1 = m19217(file2);
        try {
            r1.setLength(PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM);
            r1.seek(0);
            byte[] bArr = new byte[16];
            m19232(bArr, 4096, 0, 0, 0);
            r1.write(bArr);
            r1.close();
            if (!file2.renameTo(file)) {
                throw new IOException("Rename failed!");
            }
        } catch (Throwable th) {
            r1.close();
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m19232(byte[] bArr, int... iArr) {
        int i = 0;
        for (int r1 : iArr) {
            m19220(bArr, i, r1);
            i += 4;
        }
    }

    public synchronized void close() throws IOException {
        this.f15297.close();
    }

    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName()).append('[');
        sb.append("fileLength=").append(this.f15298);
        sb.append(", size=").append(this.f15296);
        sb.append(", first=").append(this.f15295);
        sb.append(", last=").append(this.f15293);
        sb.append(", element lengths=[");
        try {
            m19237((ElementReader) new ElementReader() {

                /* renamed from: 龘  reason: contains not printable characters */
                boolean f15301 = true;

                public void read(InputStream inputStream, int i) throws IOException {
                    if (this.f15301) {
                        this.f15301 = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(i);
                }
            });
        } catch (IOException e) {
            f15292.log(Level.WARNING, "read error", e);
        }
        sb.append("]]");
        return sb.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized boolean m19233() {
        return this.f15296 == 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized void m19234() throws IOException {
        m19228(4096, 0, 0, 0);
        this.f15296 = 0;
        this.f15295 = Element.f15302;
        this.f15293 = Element.f15302;
        if (this.f15298 > 4096) {
            m19221(4096);
        }
        this.f15298 = 4096;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized void m19235() throws IOException {
        if (m19233()) {
            throw new NoSuchElementException();
        } else if (this.f15296 == 1) {
            m19234();
        } else {
            int r1 = m19216(this.f15295.f15303 + 4 + this.f15295.f15304);
            m19219(r1, this.f15294, 0, 4);
            int r0 = m19224(this.f15294, 0);
            m19228(this.f15298, this.f15296 - 1, r1, this.f15293.f15303);
            this.f15296--;
            this.f15295 = new Element(r1, r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19236() {
        if (this.f15296 == 0) {
            return 16;
        }
        return this.f15293.f15303 >= this.f15295.f15303 ? (this.f15293.f15303 - this.f15295.f15303) + 4 + this.f15293.f15304 + 16 : (((this.f15293.f15303 + 4) + this.f15293.f15304) + this.f15298) - this.f15295.f15303;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m19237(ElementReader elementReader) throws IOException {
        int i = this.f15295.f15303;
        for (int i2 = 0; i2 < this.f15296; i2++) {
            Element r0 = m19225(i);
            elementReader.read(new ElementInputStream(r0), r0.f15304);
            i = m19216(r0.f15303 + 4 + r0.f15304);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19238(byte[] bArr) throws IOException {
        m19239(bArr, 0, bArr.length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m19239(byte[] bArr, int i, int i2) throws IOException {
        m19218(bArr, "buffer");
        if ((i | i2) < 0 || i2 > bArr.length - i) {
            throw new IndexOutOfBoundsException();
        }
        m19222(i2);
        boolean r3 = m19233();
        Element element = new Element(r3 ? 16 : m19216(this.f15293.f15303 + 4 + this.f15293.f15304), i2);
        m19220(this.f15294, 0, i2);
        m19229(element.f15303, this.f15294, 0, 4);
        m19229(element.f15303 + 4, bArr, i, i2);
        m19228(this.f15298, this.f15296 + 1, r3 ? element.f15303 : this.f15295.f15303, element.f15303);
        this.f15293 = element;
        this.f15296++;
        if (r3) {
            this.f15295 = this.f15293;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19240(int i, int i2) {
        return (m19236() + 4) + i <= i2;
    }
}
