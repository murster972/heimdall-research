package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class PriorityFutureTask<V> extends FutureTask<V> implements Dependency<Task>, PriorityProvider, Task {

    /* renamed from: 靐  reason: contains not printable characters */
    final Object f15344;

    public PriorityFutureTask(Runnable runnable, V v) {
        super(runnable, v);
        this.f15344 = m19276((Object) runnable);
    }

    public PriorityFutureTask(Callable<V> callable) {
        super(callable);
        this.f15344 = m19276((Object) callable);
    }

    public boolean areDependenciesMet() {
        return ((Dependency) ((PriorityProvider) m19275())).areDependenciesMet();
    }

    public int compareTo(Object obj) {
        return ((PriorityProvider) m19275()).compareTo(obj);
    }

    public Collection<Task> getDependencies() {
        return ((Dependency) ((PriorityProvider) m19275())).getDependencies();
    }

    public Priority getPriority() {
        return ((PriorityProvider) m19275()).getPriority();
    }

    public boolean isFinished() {
        return ((Task) ((PriorityProvider) m19275())).isFinished();
    }

    public void setError(Throwable th) {
        ((Task) ((PriorityProvider) m19275())).setError(th);
    }

    public void setFinished(boolean z) {
        ((Task) ((PriorityProvider) m19275())).setFinished(z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T extends Dependency<Task> & PriorityProvider & Task> T m19275() {
        return (Dependency) this.f15344;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public <T extends Dependency<Task> & PriorityProvider & Task> T m19276(Object obj) {
        return PriorityTask.isProperDelegate(obj) ? (Dependency) obj : new PriorityTask();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void addDependency(Task task) {
        ((Dependency) ((PriorityProvider) m19275())).addDependency(task);
    }
}
