package io.fabric.sdk.android.services.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;
import org.apache.oltu.oauth2.common.OAuth;

public class HttpRequest {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f15372 = new String[0];

    /* renamed from: 齉  reason: contains not printable characters */
    private static ConnectionFactory f15373 = ConnectionFactory.f15389;

    /* renamed from: ʻ  reason: contains not printable characters */
    private RequestOutputStream f15374;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f15375;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f15376 = true;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f15377;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f15378 = false;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public int f15379 = 8192;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private String f15380;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f15381;

    /* renamed from: 麤  reason: contains not printable characters */
    private HttpURLConnection f15382 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public final URL f15383;

    protected static abstract class CloseOperation<V> extends Operation<V> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final boolean f15387;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Closeable f15388;

        protected CloseOperation(Closeable closeable, boolean z) {
            this.f15388 = closeable;
            this.f15387 = z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m19372() throws IOException {
            if (this.f15388 instanceof Flushable) {
                ((Flushable) this.f15388).flush();
            }
            if (this.f15387) {
                try {
                    this.f15388.close();
                } catch (IOException e) {
                }
            } else {
                this.f15388.close();
            }
        }
    }

    public interface ConnectionFactory {

        /* renamed from: 龘  reason: contains not printable characters */
        public static final ConnectionFactory f15389 = new ConnectionFactory() {
            /* renamed from: 龘  reason: contains not printable characters */
            public HttpURLConnection m19375(URL url) throws IOException {
                return (HttpURLConnection) url.openConnection();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public HttpURLConnection m19376(URL url, Proxy proxy) throws IOException {
                return (HttpURLConnection) url.openConnection(proxy);
            }
        };

        /* renamed from: 龘  reason: contains not printable characters */
        HttpURLConnection m19373(URL url) throws IOException;

        /* renamed from: 龘  reason: contains not printable characters */
        HttpURLConnection m19374(URL url, Proxy proxy) throws IOException;
    }

    public static class HttpRequestException extends RuntimeException {
        private static final long serialVersionUID = -1170466989781746231L;

        protected HttpRequestException(IOException iOException) {
            super(iOException);
        }

        public IOException getCause() {
            return (IOException) super.getCause();
        }
    }

    protected static abstract class Operation<V> implements Callable<V> {
        protected Operation() {
        }

        public V call() throws HttpRequestException {
            boolean z;
            try {
                V r2 = m19377();
                try {
                    m19378();
                } catch (IOException e) {
                    if (0 == 0) {
                        throw new HttpRequestException(e);
                    }
                }
                return r2;
            } catch (HttpRequestException e2) {
                z = true;
                throw e2;
            } catch (IOException e3) {
                z = true;
                throw new HttpRequestException(e3);
            } catch (Throwable th) {
                try {
                    m19378();
                } catch (IOException e4) {
                    if (!z) {
                        throw new HttpRequestException(e4);
                    }
                }
                throw th;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public abstract V m19377() throws HttpRequestException, IOException;

        /* access modifiers changed from: protected */
        /* renamed from: 齉  reason: contains not printable characters */
        public abstract void m19378() throws IOException;
    }

    public static class RequestOutputStream extends BufferedOutputStream {

        /* renamed from: 龘  reason: contains not printable characters */
        private final CharsetEncoder f15390;

        public RequestOutputStream(OutputStream outputStream, String str, int i) {
            super(outputStream, i);
            this.f15390 = Charset.forName(HttpRequest.m19316(str)).newEncoder();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public RequestOutputStream m19379(String str) throws IOException {
            ByteBuffer encode = this.f15390.encode(CharBuffer.wrap(str));
            super.write(encode.array(), 0, encode.limit());
            return this;
        }
    }

    public HttpRequest(CharSequence charSequence, String str) throws HttpRequestException {
        try {
            this.f15383 = new URL(charSequence.toString());
            this.f15381 = str;
        } catch (MalformedURLException e) {
            throw new HttpRequestException(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m19316(String str) {
        return (str == null || str.length() <= 0) ? "UTF-8" : str;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private Proxy m19317() {
        return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.f15380, this.f15377));
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private HttpURLConnection m19318() {
        try {
            HttpURLConnection r0 = this.f15380 != null ? f15373.m19374(this.f15383, m19317()) : f15373.m19373(this.f15383);
            r0.setRequestMethod(this.f15381);
            return r0;
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static HttpRequest m19319(CharSequence charSequence) throws HttpRequestException {
        return new HttpRequest(charSequence, OAuth.HttpMethod.DELETE);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static HttpRequest m19321(CharSequence charSequence) throws HttpRequestException {
        return new HttpRequest(charSequence, OAuth.HttpMethod.GET);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static HttpRequest m19322(CharSequence charSequence, Map<?, ?> map, boolean z) {
        String r0 = m19329(charSequence, map);
        if (z) {
            r0 = m19328((CharSequence) r0);
        }
        return m19325((CharSequence) r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static StringBuilder m19323(String str, StringBuilder sb) {
        int indexOf = str.indexOf(63);
        int length = sb.length() - 1;
        if (indexOf == -1) {
            sb.append('?');
        } else if (indexOf < length && str.charAt(length) != '&') {
            sb.append('&');
        }
        return sb;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static HttpRequest m19324(CharSequence charSequence) throws HttpRequestException {
        return new HttpRequest(charSequence, OAuth.HttpMethod.PUT);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static HttpRequest m19325(CharSequence charSequence) throws HttpRequestException {
        return new HttpRequest(charSequence, OAuth.HttpMethod.POST);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static HttpRequest m19327(CharSequence charSequence, Map<?, ?> map, boolean z) {
        String r0 = m19329(charSequence, map);
        if (z) {
            r0 = m19328((CharSequence) r0);
        }
        return m19321((CharSequence) r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19328(CharSequence charSequence) throws HttpRequestException {
        try {
            URL url = new URL(charSequence.toString());
            String host = url.getHost();
            int port = url.getPort();
            if (port != -1) {
                host = host + ':' + Integer.toString(port);
            }
            try {
                String aSCIIString = new URI(url.getProtocol(), host, url.getPath(), url.getQuery(), (String) null).toASCIIString();
                int indexOf = aSCIIString.indexOf(63);
                return (indexOf <= 0 || indexOf + 1 >= aSCIIString.length()) ? aSCIIString : aSCIIString.substring(0, indexOf + 1) + aSCIIString.substring(indexOf + 1).replace("+", "%2B");
            } catch (URISyntaxException e) {
                IOException iOException = new IOException("Parsing URI failed");
                iOException.initCause(e);
                throw new HttpRequestException(iOException);
            }
        } catch (IOException e2) {
            throw new HttpRequestException(e2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19329(CharSequence charSequence, Map<?, ?> map) {
        String charSequence2 = charSequence.toString();
        if (map == null || map.isEmpty()) {
            return charSequence2;
        }
        StringBuilder sb = new StringBuilder(charSequence2);
        m19330(charSequence2, sb);
        m19323(charSequence2, sb);
        Iterator<Map.Entry<?, ?>> it2 = map.entrySet().iterator();
        Map.Entry next = it2.next();
        sb.append(next.getKey().toString());
        sb.append('=');
        Object value = next.getValue();
        if (value != null) {
            sb.append(value);
        }
        while (it2.hasNext()) {
            sb.append('&');
            Map.Entry next2 = it2.next();
            sb.append(next2.getKey().toString());
            sb.append('=');
            Object value2 = next2.getValue();
            if (value2 != null) {
                sb.append(value2);
            }
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static StringBuilder m19330(String str, StringBuilder sb) {
        if (str.indexOf(58) + 2 == str.lastIndexOf(47)) {
            sb.append('/');
        }
        return sb;
    }

    public String toString() {
        return m19369() + ' ' + m19368();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public HttpRequest m19331(CharSequence charSequence) throws HttpRequestException {
        try {
            m19336();
            this.f15374.m19379(charSequence.toString());
            return this;
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public HttpRequest m19332(String str, String str2) throws HttpRequestException {
        return m19331((CharSequence) str).m19331((CharSequence) ": ").m19331((CharSequence) str2).m19331((CharSequence) "\r\n");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public BufferedInputStream m19333() throws HttpRequestException {
        return new BufferedInputStream(m19334(), this.f15379);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public InputStream m19334() throws HttpRequestException {
        InputStream errorStream;
        if (m19344() < 400) {
            try {
                errorStream = m19367().getInputStream();
            } catch (IOException e) {
                throw new HttpRequestException(e);
            }
        } else {
            errorStream = m19367().getErrorStream();
            if (errorStream == null) {
                try {
                    errorStream = m19367().getInputStream();
                } catch (IOException e2) {
                    throw new HttpRequestException(e2);
                }
            }
        }
        if (!this.f15378 || !"gzip".equals(m19339())) {
            return errorStream;
        }
        try {
            return new GZIPInputStream(errorStream);
        } catch (IOException e3) {
            throw new HttpRequestException(e3);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m19335() {
        return m19347(OAuth.HeaderType.CONTENT_TYPE, "charset");
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public HttpRequest m19336() throws IOException {
        if (this.f15374 == null) {
            m19367().setDoOutput(true);
            this.f15374 = new RequestOutputStream(m19367().getOutputStream(), m19352(m19367().getRequestProperty(OAuth.HeaderType.CONTENT_TYPE), "charset"), this.f15379);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʿ  reason: contains not printable characters */
    public HttpRequest m19337() throws IOException {
        if (!this.f15375) {
            this.f15375 = true;
            m19348("multipart/form-data; boundary=00content0boundary00").m19336();
            this.f15374.m19379("--00content0boundary00\r\n");
        } else {
            this.f15374.m19379("\r\n--00content0boundary00\r\n");
        }
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public HttpRequest m19338() throws HttpRequestException {
        try {
            return m19341();
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public String m19339() {
        return m19346("Content-Encoding");
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public int m19340() {
        return m19351("Content-Length");
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public HttpRequest m19341() throws IOException {
        if (this.f15374 != null) {
            if (this.f15375) {
                this.f15374.m19379("\r\n--00content0boundary00--\r\n");
            }
            if (this.f15376) {
                try {
                    this.f15374.close();
                } catch (IOException e) {
                }
            } else {
                this.f15374.close();
            }
            this.f15374 = null;
        }
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public HttpRequest m19342(String str, String str2) {
        return m19345(str, (String) null, str2);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m19343() throws HttpRequestException {
        return m19366(m19335());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m19344() throws HttpRequestException {
        try {
            m19341();
            return m19367().getResponseCode();
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public HttpRequest m19345(String str, String str2, String str3) throws HttpRequestException {
        return m19363(str, str2, (String) null, str3);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m19346(String str) throws HttpRequestException {
        m19338();
        return m19367().getHeaderField(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m19347(String str, String str2) {
        return m19352(m19346(str), str2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public HttpRequest m19348(String str) {
        return m19349(str, (String) null);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public HttpRequest m19349(String str, String str2) {
        return (str2 == null || str2.length() <= 0) ? m19358(OAuth.HeaderType.CONTENT_TYPE, str) : m19358(OAuth.HeaderType.CONTENT_TYPE, str + "; charset=" + str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public ByteArrayOutputStream m19350() {
        int r0 = m19340();
        return r0 > 0 ? new ByteArrayOutputStream(r0) : new ByteArrayOutputStream();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m19351(String str) throws HttpRequestException {
        return m19354(str, -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m19352(String str, String str2) {
        int i;
        String trim;
        int length;
        if (str == null || str.length() == 0) {
            return null;
        }
        int length2 = str.length();
        int indexOf = str.indexOf(59) + 1;
        if (indexOf == 0 || indexOf == length2) {
            return null;
        }
        int indexOf2 = str.indexOf(59, indexOf);
        if (indexOf2 == -1) {
            indexOf2 = length2;
        }
        while (indexOf < i) {
            int indexOf3 = str.indexOf(61, indexOf);
            if (indexOf3 != -1 && indexOf3 < i && str2.equals(str.substring(indexOf, indexOf3).trim()) && (length = trim.length()) != 0) {
                return (length > 2 && '\"' == trim.charAt(0) && '\"' == trim.charAt(length + -1)) ? trim.substring(1, length - 1) : trim;
            }
            indexOf = i + 1;
            i = str.indexOf(59, indexOf);
            if (i == -1) {
                i = length2;
            }
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m19353() throws HttpRequestException {
        return 200 == m19344();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19354(String str, int i) throws HttpRequestException {
        m19338();
        return m19367().getHeaderFieldInt(str, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19355(int i) {
        m19367().setConnectTimeout(i);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19356(InputStream inputStream, OutputStream outputStream) throws IOException {
        final InputStream inputStream2 = inputStream;
        final OutputStream outputStream2 = outputStream;
        return (HttpRequest) new CloseOperation<HttpRequest>(inputStream, this.f15376) {
            /* renamed from: 龘  reason: contains not printable characters */
            public HttpRequest m19370() throws IOException {
                byte[] bArr = new byte[HttpRequest.this.f15379];
                while (true) {
                    int read = inputStream2.read(bArr);
                    if (read == -1) {
                        return HttpRequest.this;
                    }
                    outputStream2.write(bArr, 0, read);
                }
            }
        }.call();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19357(String str, Number number) throws HttpRequestException {
        return m19359(str, (String) null, number);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19358(String str, String str2) {
        m19367().setRequestProperty(str, str2);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19359(String str, String str2, Number number) throws HttpRequestException {
        return m19345(str, str2, number != null ? number.toString() : null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19360(String str, String str2, String str3) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"").append(str);
        if (str2 != null) {
            sb.append("\"; filename=\"").append(str2);
        }
        sb.append('\"');
        m19332("Content-Disposition", sb.toString());
        if (str3 != null) {
            m19332(OAuth.HeaderType.CONTENT_TYPE, str3);
        }
        return m19331((CharSequence) "\r\n");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x001f A[SYNTHETIC, Splitter:B:15:0x001f] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public io.fabric.sdk.android.services.network.HttpRequest m19361(java.lang.String r6, java.lang.String r7, java.lang.String r8, java.io.File r9) throws io.fabric.sdk.android.services.network.HttpRequest.HttpRequestException {
        /*
            r5 = this;
            r1 = 0
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0015 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0015 }
            r3.<init>(r9)     // Catch:{ IOException -> 0x0015 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0015 }
            io.fabric.sdk.android.services.network.HttpRequest r3 = r5.m19362((java.lang.String) r6, (java.lang.String) r7, (java.lang.String) r8, (java.io.InputStream) r2)     // Catch:{ IOException -> 0x002a, all -> 0x0027 }
            if (r2 == 0) goto L_0x0014
            r2.close()     // Catch:{ IOException -> 0x0023 }
        L_0x0014:
            return r3
        L_0x0015:
            r0 = move-exception
        L_0x0016:
            io.fabric.sdk.android.services.network.HttpRequest$HttpRequestException r3 = new io.fabric.sdk.android.services.network.HttpRequest$HttpRequestException     // Catch:{ all -> 0x001c }
            r3.<init>(r0)     // Catch:{ all -> 0x001c }
            throw r3     // Catch:{ all -> 0x001c }
        L_0x001c:
            r3 = move-exception
        L_0x001d:
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ IOException -> 0x0025 }
        L_0x0022:
            throw r3
        L_0x0023:
            r4 = move-exception
            goto L_0x0014
        L_0x0025:
            r4 = move-exception
            goto L_0x0022
        L_0x0027:
            r3 = move-exception
            r1 = r2
            goto L_0x001d
        L_0x002a:
            r0 = move-exception
            r1 = r2
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: io.fabric.sdk.android.services.network.HttpRequest.m19361(java.lang.String, java.lang.String, java.lang.String, java.io.File):io.fabric.sdk.android.services.network.HttpRequest");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19362(String str, String str2, String str3, InputStream inputStream) throws HttpRequestException {
        try {
            m19337();
            m19360(str, str2, str3);
            m19356(inputStream, (OutputStream) this.f15374);
            return this;
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19363(String str, String str2, String str3, String str4) throws HttpRequestException {
        try {
            m19337();
            m19360(str, str2, str3);
            this.f15374.m19379(str4);
            return this;
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19364(Map.Entry<String, String> entry) {
        return m19358(entry.getKey(), entry.getValue());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpRequest m19365(boolean z) {
        m19367().setUseCaches(z);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m19366(String str) throws HttpRequestException {
        ByteArrayOutputStream r1 = m19350();
        try {
            m19356((InputStream) m19333(), (OutputStream) r1);
            return r1.toString(m19316(str));
        } catch (IOException e) {
            throw new HttpRequestException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HttpURLConnection m19367() {
        if (this.f15382 == null) {
            this.f15382 = m19318();
        }
        return this.f15382;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public URL m19368() {
        return m19367().getURL();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public String m19369() {
        return m19367().getRequestMethod();
    }
}
