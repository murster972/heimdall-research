package io.fabric.sdk.android.services.common;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;

public class ApiKey {
    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m19129(Context context) {
        if (Fabric.m19035() || CommonUtils.m19141(context)) {
            throw new IllegalArgumentException(m19133());
        }
        Fabric.m19034().m19082("Fabric", m19133());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m19130(Context context) {
        return new FirebaseInfo().m19191(context);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m19131(Context context) {
        int r1 = CommonUtils.m19158(context, "io.fabric.ApiKey", "string");
        if (r1 == 0) {
            Fabric.m19034().m19090("Fabric", "Falling back to Crashlytics key lookup from Strings");
            r1 = CommonUtils.m19158(context, "com.crashlytics.ApiKey", "string");
        }
        if (r1 != 0) {
            return context.getResources().getString(r1);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m19132(Context context) {
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle == null) {
                return null;
            }
            String string = bundle.getString("io.fabric.ApiKey");
            if ("@string/twitter_consumer_secret".equals(string)) {
                Fabric.m19034().m19090("Fabric", "Ignoring bad default value for Fabric ApiKey set by FirebaseUI-Auth");
                string = null;
            }
            if (string != null) {
                return string;
            }
            Fabric.m19034().m19090("Fabric", "Falling back to Crashlytics key lookup from Manifest");
            return bundle.getString("com.crashlytics.ApiKey");
        } catch (Exception e) {
            Fabric.m19034().m19090("Fabric", "Caught non-fatal exception while retrieving apiKey: " + e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m19133() {
        return "Fabric could not be initialized, API key missing from AndroidManifest.xml. Add the following tag to your Application element \n\t<meta-data android:name=\"io.fabric.ApiKey\" android:value=\"YOUR_API_KEY\"/>";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m19134(Context context) {
        String r0 = m19132(context);
        if (TextUtils.isEmpty(r0)) {
            r0 = m19131(context);
        }
        if (TextUtils.isEmpty(r0)) {
            r0 = m19130(context);
        }
        if (TextUtils.isEmpty(r0)) {
            m19129(context);
        }
        return r0;
    }
}
