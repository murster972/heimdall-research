package io.fabric.sdk.android.services.cache;

import android.content.Context;

public abstract class AbstractValueCache<T> implements ValueCache<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ValueCache<T> f15234;

    public AbstractValueCache(ValueCache<T> valueCache) {
        this.f15234 = valueCache;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m19102(Context context, T t) {
        if (t == null) {
            throw new NullPointerException();
        }
        m19105(context, t);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract T m19103(Context context);

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized T m19104(Context context, ValueLoader<T> valueLoader) throws Exception {
        T r0;
        r0 = m19103(context);
        if (r0 == null) {
            r0 = this.f15234 != null ? this.f15234.m19108(context, valueLoader) : valueLoader.load(context);
            m19102(context, r0);
        }
        return r0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m19105(Context context, T t);
}
