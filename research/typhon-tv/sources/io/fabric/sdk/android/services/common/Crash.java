package io.fabric.sdk.android.services.common;

public abstract class Crash {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f15263;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f15264;

    public static class FatalException extends Crash {
        public FatalException(String str, String str2) {
            super(str, str2);
        }
    }

    public static class LoggedException extends Crash {
        public LoggedException(String str, String str2) {
            super(str, str2);
        }
    }

    public Crash(String str, String str2) {
        this.f15264 = str;
        this.f15263 = str2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m19182() {
        return this.f15263;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m19183() {
        return this.f15264;
    }
}
