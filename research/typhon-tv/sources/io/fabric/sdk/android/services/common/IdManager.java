package io.fabric.sdk.android.services.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class IdManager {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final String f15274 = Pattern.quote(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Pattern f15275 = Pattern.compile("[^\\p{Alnum}]");

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ReentrantLock f15276 = new ReentrantLock();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final InstallerPackageNameProvider f15277;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final String f15278;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Collection<Kit> f15279;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final String f15280;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final boolean f15281;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean f15282;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final Context f15283;

    /* renamed from: 靐  reason: contains not printable characters */
    AdvertisingInfo f15284;

    /* renamed from: 麤  reason: contains not printable characters */
    FirebaseInfo f15285;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean f15286;

    /* renamed from: 龘  reason: contains not printable characters */
    AdvertisingInfoProvider f15287;

    public enum DeviceIdentifierType {
        WIFI_MAC_ADDRESS(1),
        BLUETOOTH_MAC_ADDRESS(2),
        FONT_TOKEN(53),
        ANDROID_ID(100),
        ANDROID_DEVICE_ID(101),
        ANDROID_SERIAL(102),
        ANDROID_ADVERTISING_ID(103);
        
        public final int protobufIndex;

        private DeviceIdentifierType(int i) {
            this.protobufIndex = i;
        }
    }

    public IdManager(Context context, String str, String str2, Collection<Kit> collection) {
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        } else if (str == null) {
            throw new IllegalArgumentException("appIdentifier must not be null");
        } else if (collection == null) {
            throw new IllegalArgumentException("kits must not be null");
        } else {
            this.f15283 = context;
            this.f15280 = str;
            this.f15278 = str2;
            this.f15279 = collection;
            this.f15277 = new InstallerPackageNameProvider();
            this.f15287 = new AdvertisingInfoProvider(context);
            this.f15285 = new FirebaseInfo();
            this.f15281 = CommonUtils.m19179(context, "com.crashlytics.CollectDeviceIdentifiers", true);
            if (!this.f15281) {
                Fabric.m19034().m19090("Fabric", "Device ID collection disabled for " + context.getPackageName());
            }
            this.f15282 = CommonUtils.m19179(context, "com.crashlytics.CollectUserIdentifiers", true);
            if (!this.f15282) {
                Fabric.m19034().m19090("Fabric", "User information collection disabled for " + context.getPackageName());
            }
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private Boolean m19193() {
        AdvertisingInfo r0 = m19203();
        if (r0 != null) {
            return Boolean.valueOf(r0.f15236);
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m19194(String str) {
        return str.replaceAll(f15274, "");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m19195(SharedPreferences sharedPreferences) {
        AdvertisingInfo r0 = m19203();
        if (r0 != null) {
            m19198(sharedPreferences, r0.f15237);
        }
    }

    @SuppressLint({"CommitPrefEdits"})
    /* renamed from: 龘  reason: contains not printable characters */
    private String m19196(SharedPreferences sharedPreferences) {
        this.f15276.lock();
        try {
            String string = sharedPreferences.getString("crashlytics.installation.id", (String) null);
            if (string == null) {
                string = m19197(UUID.randomUUID().toString());
                sharedPreferences.edit().putString("crashlytics.installation.id", string).commit();
            }
            return string;
        } finally {
            this.f15276.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m19197(String str) {
        if (str == null) {
            return null;
        }
        return f15275.matcher(str).replaceAll("").toLowerCase(Locale.US);
    }

    @SuppressLint({"CommitPrefEdits"})
    /* renamed from: 龘  reason: contains not printable characters */
    private void m19198(SharedPreferences sharedPreferences, String str) {
        this.f15276.lock();
        try {
            if (!TextUtils.isEmpty(str)) {
                String string = sharedPreferences.getString("crashlytics.advertising.id", (String) null);
                if (TextUtils.isEmpty(string)) {
                    sharedPreferences.edit().putString("crashlytics.advertising.id", str).commit();
                } else if (!string.equals(str)) {
                    sharedPreferences.edit().remove("crashlytics.installation.id").putString("crashlytics.advertising.id", str).commit();
                }
                this.f15276.unlock();
            }
        } finally {
            this.f15276.unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19199(Map<DeviceIdentifierType, String> map, DeviceIdentifierType deviceIdentifierType, String str) {
        if (str != null) {
            map.put(deviceIdentifierType, str);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m19200() {
        return m19194(Build.VERSION.INCREMENTAL);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m19201() {
        return String.format(Locale.US, "%s/%s", new Object[]{m19194(Build.MANUFACTURER), m19194(Build.MODEL)});
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Map<DeviceIdentifierType, String> m19202() {
        HashMap hashMap = new HashMap();
        for (Kit next : this.f15279) {
            if (next instanceof DeviceIdentifierProvider) {
                for (Map.Entry next2 : ((DeviceIdentifierProvider) next).getDeviceIdentifiers().entrySet()) {
                    m19199(hashMap, (DeviceIdentifierType) next2.getKey(), (String) next2.getValue());
                }
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public synchronized AdvertisingInfo m19203() {
        if (!this.f15286) {
            this.f15284 = this.f15287.m19118();
            this.f15286 = true;
        }
        return this.f15284;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public String m19204() {
        return this.f15277.m19212(this.f15283);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public Boolean m19205() {
        if (m19206()) {
            return m19193();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m19206() {
        return this.f15281 && !this.f15285.m19190(this.f15283);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m19207() {
        return m19194(Build.VERSION.RELEASE);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m19208() {
        String str = this.f15278;
        if (str != null) {
            return str;
        }
        SharedPreferences r1 = CommonUtils.m19162(this.f15283);
        m19195(r1);
        String string = r1.getString("crashlytics.installation.id", (String) null);
        return string == null ? m19196(r1) : string;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m19209() {
        return m19207() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + m19200();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m19210() {
        return this.f15280;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19211() {
        return this.f15282;
    }
}
