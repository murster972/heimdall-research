package io.fabric.sdk.android.services.events;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.QueueFile;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueueFileEventStorage implements EventsStorage {

    /* renamed from: ʻ  reason: contains not printable characters */
    private File f15358;

    /* renamed from: 连任  reason: contains not printable characters */
    private QueueFile f15359 = new QueueFile(this.f15361);

    /* renamed from: 靐  reason: contains not printable characters */
    private final File f15360;

    /* renamed from: 麤  reason: contains not printable characters */
    private final File f15361;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f15362;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f15363;

    public QueueFileEventStorage(Context context, File file, String str, String str2) throws IOException {
        this.f15363 = context;
        this.f15360 = file;
        this.f15362 = str2;
        this.f15361 = new File(this.f15360, str);
        m19295();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m19295() {
        this.f15358 = new File(this.f15360, this.f15362);
        if (!this.f15358.exists()) {
            this.f15358.mkdirs();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19296(File file, File file2) throws IOException {
        OutputStream outputStream = null;
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(file);
            try {
                outputStream = m19301(file2);
                CommonUtils.m19178((InputStream) fileInputStream2, outputStream, new byte[1024]);
                CommonUtils.m19176((Closeable) fileInputStream2, "Failed to close file input stream");
                CommonUtils.m19176((Closeable) outputStream, "Failed to close output stream");
                file.delete();
            } catch (Throwable th) {
                th = th;
                fileInputStream = fileInputStream2;
                CommonUtils.m19176((Closeable) fileInputStream, "Failed to close file input stream");
                CommonUtils.m19176((Closeable) outputStream, "Failed to close output stream");
                file.delete();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            CommonUtils.m19176((Closeable) fileInputStream, "Failed to close file input stream");
            CommonUtils.m19176((Closeable) outputStream, "Failed to close output stream");
            file.delete();
            throw th;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m19297() {
        return this.f15359.m19233();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19298() {
        try {
            this.f15359.close();
        } catch (IOException e) {
        }
        this.f15361.delete();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public List<File> m19299() {
        return Arrays.asList(this.f15358.listFiles());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19300() {
        return this.f15359.m19236();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public OutputStream m19301(File file) throws IOException {
        return new FileOutputStream(file);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<File> m19302(int i) {
        ArrayList arrayList = new ArrayList();
        for (File add : this.f15358.listFiles()) {
            arrayList.add(add);
            if (arrayList.size() >= i) {
                break;
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19303(String str) throws IOException {
        this.f15359.close();
        m19296(this.f15361, new File(this.f15358, str));
        this.f15359 = new QueueFile(this.f15361);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19304(List<File> list) {
        for (File next : list) {
            CommonUtils.m19173(this.f15363, String.format("deleting sent analytics file %s", new Object[]{next.getName()}));
            next.delete();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19305(byte[] bArr) throws IOException {
        this.f15359.m19238(bArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19306(int i, int i2) {
        return this.f15359.m19240(i, i2);
    }
}
