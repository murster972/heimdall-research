package io.fabric.sdk.android.services.settings;

public class FeaturesSettingsData {

    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean f15444;

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean f15445;

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean f15446;

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean f15447;

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean f15448;

    public FeaturesSettingsData(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.f15448 = z;
        this.f15445 = z2;
        this.f15447 = z3;
        this.f15446 = z4;
        this.f15444 = z5;
    }
}
