package io.fabric.sdk.android.services.events;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;

public class TimeBasedFileRollOverRunnable implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final FileRollOverManager f15364;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f15365;

    public TimeBasedFileRollOverRunnable(Context context, FileRollOverManager fileRollOverManager) {
        this.f15365 = context;
        this.f15364 = fileRollOverManager;
    }

    public void run() {
        try {
            CommonUtils.m19173(this.f15365, "Performing time based file roll over.");
            if (!this.f15364.rollFileOver()) {
                this.f15364.cancelTimeBasedFileRollOver();
            }
        } catch (Exception e) {
            CommonUtils.m19174(this.f15365, "Failed to roll over file", (Throwable) e);
        }
    }
}
