package io.fabric.sdk.android.services.settings;

import io.fabric.sdk.android.KitInfo;
import java.util.Collection;

public class AppRequestData {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String f15416;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final int f15417;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String f15418;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final String f15419;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final IconRequest f15420;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final Collection<KitInfo> f15421;

    /* renamed from: 连任  reason: contains not printable characters */
    public final String f15422;

    /* renamed from: 靐  reason: contains not printable characters */
    public final String f15423;

    /* renamed from: 麤  reason: contains not printable characters */
    public final String f15424;

    /* renamed from: 齉  reason: contains not printable characters */
    public final String f15425;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f15426;

    public AppRequestData(String str, String str2, String str3, String str4, String str5, String str6, int i, String str7, String str8, IconRequest iconRequest, Collection<KitInfo> collection) {
        this.f15426 = str;
        this.f15423 = str2;
        this.f15425 = str3;
        this.f15424 = str4;
        this.f15422 = str5;
        this.f15416 = str6;
        this.f15417 = i;
        this.f15418 = str7;
        this.f15419 = str8;
        this.f15420 = iconRequest;
        this.f15421 = collection;
    }
}
