package io.fabric.sdk.android.services.events;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.CurrentTimeProvider;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class EventsFilesManager<T> {
    public static final int MAX_BYTE_SIZE_PER_FILE = 8000;
    public static final int MAX_FILES_IN_BATCH = 1;
    public static final int MAX_FILES_TO_KEEP = 100;
    public static final String ROLL_OVER_FILE_NAME_SEPARATOR = "_";
    protected final Context context;
    protected final CurrentTimeProvider currentTimeProvider;
    private final int defaultMaxFilesToKeep;
    protected final EventsStorage eventStorage;
    protected volatile long lastRollOverTime;
    protected final List<EventsStorageListener> rollOverListeners = new CopyOnWriteArrayList();
    protected final EventTransform<T> transform;

    static class FileWithTimestamp {

        /* renamed from: 靐  reason: contains not printable characters */
        final long f15356;

        /* renamed from: 龘  reason: contains not printable characters */
        final File f15357;

        public FileWithTimestamp(File file, long j) {
            this.f15357 = file;
            this.f15356 = j;
        }
    }

    public EventsFilesManager(Context context2, EventTransform<T> eventTransform, CurrentTimeProvider currentTimeProvider2, EventsStorage eventsStorage, int i) throws IOException {
        this.context = context2.getApplicationContext();
        this.transform = eventTransform;
        this.eventStorage = eventsStorage;
        this.currentTimeProvider = currentTimeProvider2;
        this.lastRollOverTime = this.currentTimeProvider.m19184();
        this.defaultMaxFilesToKeep = i;
    }

    private void rollFileOverIfNeeded(int i) throws IOException {
        if (!this.eventStorage.m19293(i, getMaxByteSizePerFile())) {
            CommonUtils.m19172(this.context, 4, "Fabric", String.format(Locale.US, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over", new Object[]{Integer.valueOf(this.eventStorage.m19288()), Integer.valueOf(i), Integer.valueOf(getMaxByteSizePerFile())}));
            rollFileOver();
        }
    }

    private void triggerRollOverOnListeners(String str) {
        for (EventsStorageListener onRollOver : this.rollOverListeners) {
            try {
                onRollOver.onRollOver(str);
            } catch (Exception e) {
                CommonUtils.m19174(this.context, "One of the roll over listeners threw an exception", (Throwable) e);
            }
        }
    }

    public void deleteAllEventsFiles() {
        this.eventStorage.m19291(this.eventStorage.m19287());
        this.eventStorage.m19286();
    }

    public void deleteOldestInRollOverIfOverMax() {
        List<File> r0 = this.eventStorage.m19287();
        int maxFilesToKeep = getMaxFilesToKeep();
        if (r0.size() > maxFilesToKeep) {
            int size = r0.size() - maxFilesToKeep;
            CommonUtils.m19173(this.context, String.format(Locale.US, "Found %d files in  roll over directory, this is greater than %d, deleting %d oldest files", new Object[]{Integer.valueOf(r0.size()), Integer.valueOf(maxFilesToKeep), Integer.valueOf(size)}));
            TreeSet treeSet = new TreeSet(new Comparator<FileWithTimestamp>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public int compare(FileWithTimestamp fileWithTimestamp, FileWithTimestamp fileWithTimestamp2) {
                    return (int) (fileWithTimestamp.f15356 - fileWithTimestamp2.f15356);
                }
            });
            for (File next : r0) {
                treeSet.add(new FileWithTimestamp(next, parseCreationTimestampFromFileName(next.getName())));
            }
            ArrayList arrayList = new ArrayList();
            Iterator it2 = treeSet.iterator();
            while (it2.hasNext()) {
                arrayList.add(((FileWithTimestamp) it2.next()).f15357);
                if (arrayList.size() == size) {
                    break;
                }
            }
            this.eventStorage.m19291((List<File>) arrayList);
        }
    }

    public void deleteSentFiles(List<File> list) {
        this.eventStorage.m19291(list);
    }

    /* access modifiers changed from: protected */
    public abstract String generateUniqueRollOverFileName();

    public List<File> getBatchOfFilesToSend() {
        return this.eventStorage.m19289(1);
    }

    public long getLastRollOverTime() {
        return this.lastRollOverTime;
    }

    /* access modifiers changed from: protected */
    public int getMaxByteSizePerFile() {
        return 8000;
    }

    /* access modifiers changed from: protected */
    public int getMaxFilesToKeep() {
        return this.defaultMaxFilesToKeep;
    }

    public long parseCreationTimestampFromFileName(String str) {
        String[] split = str.split(ROLL_OVER_FILE_NAME_SEPARATOR);
        if (split.length != 3) {
            return 0;
        }
        try {
            return Long.valueOf(split[2]).longValue();
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public void registerRollOverListener(EventsStorageListener eventsStorageListener) {
        if (eventsStorageListener != null) {
            this.rollOverListeners.add(eventsStorageListener);
        }
    }

    public boolean rollFileOver() throws IOException {
        boolean z = false;
        String str = null;
        if (!this.eventStorage.m19285()) {
            str = generateUniqueRollOverFileName();
            this.eventStorage.m19290(str);
            CommonUtils.m19172(this.context, 4, "Fabric", String.format(Locale.US, "generated new file %s", new Object[]{str}));
            this.lastRollOverTime = this.currentTimeProvider.m19184();
            z = true;
        }
        triggerRollOverOnListeners(str);
        return z;
    }

    public void writeEvent(T t) throws IOException {
        byte[] bytes = this.transform.toBytes(t);
        rollFileOverIfNeeded(bytes.length);
        this.eventStorage.m19292(bytes);
    }
}
