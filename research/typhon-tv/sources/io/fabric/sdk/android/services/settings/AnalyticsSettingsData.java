package io.fabric.sdk.android.services.settings;

public class AnalyticsSettingsData {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean f15402;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean f15403;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean f15404;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean f15405;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean f15406;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final int f15407;

    /* renamed from: 连任  reason: contains not printable characters */
    public final int f15408;

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f15409;

    /* renamed from: 麤  reason: contains not printable characters */
    public final int f15410;

    /* renamed from: 齉  reason: contains not printable characters */
    public final int f15411;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f15412;

    public AnalyticsSettingsData(String str, int i, int i2, int i3, int i4, boolean z, boolean z2, boolean z3, boolean z4, int i5, boolean z5) {
        this.f15412 = str;
        this.f15409 = i;
        this.f15411 = i2;
        this.f15410 = i3;
        this.f15408 = i4;
        this.f15402 = z;
        this.f15403 = z2;
        this.f15404 = z3;
        this.f15405 = z4;
        this.f15407 = i5;
        this.f15406 = z5;
    }
}
