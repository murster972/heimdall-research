package io.fabric.sdk.android.services.settings;

import android.content.res.Resources;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.KitInfo;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.ResponseParser;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Locale;
import org.apache.oltu.oauth2.common.OAuth;

abstract class AbstractAppSpiCall extends AbstractSpiCall {
    public AbstractAppSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, HttpMethod httpMethod) {
        super(kit, str, str2, httpRequestFactory, httpMethod);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private HttpRequest m19399(HttpRequest httpRequest, AppRequestData appRequestData) {
        String str;
        HttpRequest r8 = httpRequest.m19342("app[identifier]", appRequestData.f15423).m19342("app[name]", appRequestData.f15416).m19342("app[display_version]", appRequestData.f15425).m19342("app[build_version]", appRequestData.f15424).m19357("app[source]", (Number) Integer.valueOf(appRequestData.f15417)).m19342("app[minimum_sdk_version]", appRequestData.f15418).m19342("app[built_sdk_version]", appRequestData.f15419);
        if (!CommonUtils.m19152(appRequestData.f15422)) {
            r8.m19342("app[instance_identifier]", appRequestData.f15422);
        }
        if (appRequestData.f15420 != null) {
            InputStream inputStream = null;
            try {
                inputStream = this.kit.getContext().getResources().openRawResource(appRequestData.f15420.f15449);
                r8.m19342("app[icon][hash]", appRequestData.f15420.f15452).m19362("app[icon][data]", "icon.png", "application/octet-stream", inputStream).m19357("app[icon][width]", (Number) Integer.valueOf(appRequestData.f15420.f15451)).m19357("app[icon][height]", (Number) Integer.valueOf(appRequestData.f15420.f15450));
            } catch (Resources.NotFoundException e) {
                Fabric.m19034().m19083("Fabric", "Failed to find app icon with resource ID: " + appRequestData.f15420.f15449, e);
            } finally {
                str = "Failed to close app icon InputStream.";
                CommonUtils.m19176((Closeable) inputStream, str);
            }
        }
        if (appRequestData.f15421 != null) {
            for (KitInfo next : appRequestData.f15421) {
                r8.m19342(m19402(next), next.m19079());
                r8.m19342(m19401(next), next.m19080());
            }
        }
        return r8;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private HttpRequest m19400(HttpRequest httpRequest, AppRequestData appRequestData) {
        return httpRequest.m19358(AbstractSpiCall.HEADER_API_KEY, appRequestData.f15426).m19358(AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE).m19358(AbstractSpiCall.HEADER_CLIENT_VERSION, this.kit.getVersion());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m19401(KitInfo kitInfo) {
        return String.format(Locale.US, "app[build][libraries][%s][type]", new Object[]{kitInfo.m19081()});
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m19402(KitInfo kitInfo) {
        return String.format(Locale.US, "app[build][libraries][%s][version]", new Object[]{kitInfo.m19081()});
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19403(AppRequestData appRequestData) {
        HttpRequest r0 = m19399(m19400(getHttpRequest(), appRequestData), appRequestData);
        Fabric.m19034().m19090("Fabric", "Sending app info to " + getUrl());
        if (appRequestData.f15420 != null) {
            Fabric.m19034().m19090("Fabric", "App icon hash is " + appRequestData.f15420.f15452);
            Fabric.m19034().m19090("Fabric", "App icon size is " + appRequestData.f15420.f15451 + "x" + appRequestData.f15420.f15450);
        }
        int r2 = r0.m19344();
        Fabric.m19034().m19090("Fabric", (OAuth.HttpMethod.POST.equals(r0.m19369()) ? "Create" : "Update") + " app request ID: " + r0.m19346(AbstractSpiCall.HEADER_REQUEST_ID));
        Fabric.m19034().m19090("Fabric", "Result was " + r2);
        return ResponseParser.m19241(r2) == 0;
    }
}
