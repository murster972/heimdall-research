package io.fabric.sdk.android.services.concurrency;

import io.fabric.sdk.android.services.concurrency.AsyncTask;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

public abstract class PriorityAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> implements Dependency<Task>, PriorityProvider, Task {

    /* renamed from: 龘  reason: contains not printable characters */
    private final PriorityTask f15340 = new PriorityTask();

    private static class ProxyExecutor<Result> implements Executor {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final PriorityAsyncTask f15341;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Executor f15342;

        public ProxyExecutor(Executor executor, PriorityAsyncTask priorityAsyncTask) {
            this.f15342 = executor;
            this.f15341 = priorityAsyncTask;
        }

        public void execute(Runnable runnable) {
            this.f15342.execute(new PriorityFutureTask<Result>(runnable, (Object) null) {
                /* renamed from: 龘  reason: contains not printable characters */
                public <T extends Dependency<Task> & PriorityProvider & Task> T m19274() {
                    return ProxyExecutor.this.f15341;
                }
            });
        }
    }

    public boolean areDependenciesMet() {
        return ((Dependency) ((PriorityProvider) m19270())).areDependenciesMet();
    }

    public int compareTo(Object obj) {
        return Priority.m19269(this, obj);
    }

    public Collection<Task> getDependencies() {
        return ((Dependency) ((PriorityProvider) m19270())).getDependencies();
    }

    public Priority getPriority() {
        return ((PriorityProvider) m19270()).getPriority();
    }

    public boolean isFinished() {
        return ((Task) ((PriorityProvider) m19270())).isFinished();
    }

    public void setError(Throwable th) {
        ((Task) ((PriorityProvider) m19270())).setError(th);
    }

    public void setFinished(boolean z) {
        ((Task) ((PriorityProvider) m19270())).setFinished(z);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public <T extends Dependency<Task> & PriorityProvider & Task> T m19270() {
        return this.f15340;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void addDependency(Task task) {
        if (m19253() != AsyncTask.Status.PENDING) {
            throw new IllegalStateException("Must not add Dependency after task is running");
        }
        ((Dependency) ((PriorityProvider) m19270())).addDependency(task);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m19272(ExecutorService executorService, Params... paramsArr) {
        super.m19258((Executor) new ProxyExecutor(executorService, this), paramsArr);
    }
}
