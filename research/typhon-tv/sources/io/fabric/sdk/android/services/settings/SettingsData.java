package io.fabric.sdk.android.services.settings;

public class SettingsData {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final BetaSettingsData f15473;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final long f15474;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f15475;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int f15476;

    /* renamed from: 连任  reason: contains not printable characters */
    public final AnalyticsSettingsData f15477;

    /* renamed from: 靐  reason: contains not printable characters */
    public final SessionSettingsData f15478;

    /* renamed from: 麤  reason: contains not printable characters */
    public final FeaturesSettingsData f15479;

    /* renamed from: 齉  reason: contains not printable characters */
    public final PromptSettingsData f15480;

    /* renamed from: 龘  reason: contains not printable characters */
    public final AppSettingsData f15481;

    public SettingsData(long j, AppSettingsData appSettingsData, SessionSettingsData sessionSettingsData, PromptSettingsData promptSettingsData, FeaturesSettingsData featuresSettingsData, AnalyticsSettingsData analyticsSettingsData, BetaSettingsData betaSettingsData, int i, int i2) {
        this.f15474 = j;
        this.f15481 = appSettingsData;
        this.f15478 = sessionSettingsData;
        this.f15480 = promptSettingsData;
        this.f15479 = featuresSettingsData;
        this.f15475 = i;
        this.f15476 = i2;
        this.f15477 = analyticsSettingsData;
        this.f15473 = betaSettingsData;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19443(long j) {
        return this.f15474 < j;
    }
}
