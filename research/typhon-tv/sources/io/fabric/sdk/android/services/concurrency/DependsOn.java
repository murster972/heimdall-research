package io.fabric.sdk.android.services.concurrency;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface DependsOn {
    /* renamed from: 龘  reason: contains not printable characters */
    Class<?>[] m6823();
}
