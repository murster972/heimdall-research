package io.fabric.sdk.android.services.events;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface EventsStorage {
    /* renamed from: 靐  reason: contains not printable characters */
    boolean m19285();

    /* renamed from: 麤  reason: contains not printable characters */
    void m19286();

    /* renamed from: 齉  reason: contains not printable characters */
    List<File> m19287();

    /* renamed from: 龘  reason: contains not printable characters */
    int m19288();

    /* renamed from: 龘  reason: contains not printable characters */
    List<File> m19289(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    void m19290(String str) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m19291(List<File> list);

    /* renamed from: 龘  reason: contains not printable characters */
    void m19292(byte[] bArr) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m19293(int i, int i2);
}
