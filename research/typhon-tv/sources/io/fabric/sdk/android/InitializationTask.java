package io.fabric.sdk.android;

import io.fabric.sdk.android.services.common.TimingMetric;
import io.fabric.sdk.android.services.concurrency.Priority;
import io.fabric.sdk.android.services.concurrency.PriorityAsyncTask;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;

class InitializationTask<Result> extends PriorityAsyncTask<Void, Void, Result> {

    /* renamed from: 龘  reason: contains not printable characters */
    final Kit<Result> f15219;

    public InitializationTask(Kit<Result> kit) {
        this.f15219 = kit;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private TimingMetric m19073(String str) {
        TimingMetric timingMetric = new TimingMetric(this.f15219.getIdentifier() + "." + str, "KitInitialization");
        timingMetric.m19245();
        return timingMetric;
    }

    public Priority getPriority() {
        return Priority.HIGH;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m19074(Result result) {
        this.f15219.onCancelled(result);
        this.f15219.initializationCallback.m19069((Exception) new InitializationException(this.f15219.getIdentifier() + " Initialization was cancelled"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Result m19075(Void... voidArr) {
        TimingMetric r1 = m19073("doInBackground");
        Result result = null;
        if (!m19256()) {
            result = this.f15219.doInBackground();
        }
        r1.m19244();
        return result;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19077() {
        super.m19260();
        TimingMetric r2 = m19073("onPreExecute");
        try {
            boolean onPreExecute = this.f15219.onPreExecute();
            r2.m19244();
            if (!onPreExecute) {
                m19262(true);
            }
        } catch (UnmetDependencyException e) {
            throw e;
        } catch (Exception e2) {
            Fabric.m19034().m19083("Fabric", "Failure onPreExecute()", e2);
            r2.m19244();
            if (0 == 0) {
                m19262(true);
            }
        } catch (Throwable th) {
            r2.m19244();
            if (0 == 0) {
                m19262(true);
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19078(Result result) {
        this.f15219.onPostExecute(result);
        this.f15219.initializationCallback.m19070(result);
    }
}
