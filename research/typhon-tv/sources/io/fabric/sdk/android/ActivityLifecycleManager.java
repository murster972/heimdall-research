package io.fabric.sdk.android;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import java.util.HashSet;
import java.util.Set;

public class ActivityLifecycleManager {

    /* renamed from: 靐  reason: contains not printable characters */
    private ActivityLifecycleCallbacksWrapper f15181;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Application f15182;

    private static class ActivityLifecycleCallbacksWrapper {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Application f15183;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Set<Application.ActivityLifecycleCallbacks> f15184 = new HashSet();

        ActivityLifecycleCallbacksWrapper(Application application) {
            this.f15183 = application;
        }

        /* access modifiers changed from: private */
        @TargetApi(14)
        /* renamed from: 龘  reason: contains not printable characters */
        public void m19017() {
            for (Application.ActivityLifecycleCallbacks unregisterActivityLifecycleCallbacks : this.f15184) {
                this.f15183.unregisterActivityLifecycleCallbacks(unregisterActivityLifecycleCallbacks);
            }
        }

        /* access modifiers changed from: private */
        @TargetApi(14)
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m19020(final Callbacks callbacks) {
            if (this.f15183 == null) {
                return false;
            }
            AnonymousClass1 r0 = new Application.ActivityLifecycleCallbacks() {
                public void onActivityCreated(Activity activity, Bundle bundle) {
                    callbacks.onActivityCreated(activity, bundle);
                }

                public void onActivityDestroyed(Activity activity) {
                    callbacks.onActivityDestroyed(activity);
                }

                public void onActivityPaused(Activity activity) {
                    callbacks.onActivityPaused(activity);
                }

                public void onActivityResumed(Activity activity) {
                    callbacks.onActivityResumed(activity);
                }

                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                    callbacks.onActivitySaveInstanceState(activity, bundle);
                }

                public void onActivityStarted(Activity activity) {
                    callbacks.onActivityStarted(activity);
                }

                public void onActivityStopped(Activity activity) {
                    callbacks.onActivityStopped(activity);
                }
            };
            this.f15183.registerActivityLifecycleCallbacks(r0);
            this.f15184.add(r0);
            return true;
        }
    }

    public static abstract class Callbacks {
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
        }

        public void onActivityResumed(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    public ActivityLifecycleManager(Context context) {
        this.f15182 = (Application) context.getApplicationContext();
        if (Build.VERSION.SDK_INT >= 14) {
            this.f15181 = new ActivityLifecycleCallbacksWrapper(this.f15182);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19015() {
        if (this.f15181 != null) {
            this.f15181.m19017();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19016(Callbacks callbacks) {
        return this.f15181 != null && this.f15181.m19020(callbacks);
    }
}
