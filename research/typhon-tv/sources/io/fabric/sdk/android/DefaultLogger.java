package io.fabric.sdk.android;

import android.util.Log;

public class DefaultLogger implements Logger {

    /* renamed from: 龘  reason: contains not printable characters */
    private int f15187;

    public DefaultLogger() {
        this.f15187 = 4;
    }

    public DefaultLogger(int i) {
        this.f15187 = i;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m19021(String str, String str2) {
        m19022(str, str2, (Throwable) null);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m19022(String str, String str2, Throwable th) {
        if (m19033(str, 6)) {
            Log.e(str, str2, th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19023(String str, String str2) {
        m19024(str, str2, (Throwable) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19024(String str, String str2, Throwable th) {
        if (m19033(str, 2)) {
            Log.v(str, str2, th);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19025(String str, String str2) {
        m19026(str, str2, (Throwable) null);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19026(String str, String str2, Throwable th) {
        if (m19033(str, 5)) {
            Log.w(str, str2, th);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19027(String str, String str2) {
        m19028(str, str2, (Throwable) null);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19028(String str, String str2, Throwable th) {
        if (m19033(str, 4)) {
            Log.i(str, str2, th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19029(int i, String str, String str2) {
        m19030(i, str, str2, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19030(int i, String str, String str2, boolean z) {
        if (z || m19033(str, i)) {
            Log.println(i, str, str2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19031(String str, String str2) {
        m19032(str, str2, (Throwable) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19032(String str, String str2, Throwable th) {
        if (m19033(str, 3)) {
            Log.d(str, str2, th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19033(String str, int i) {
        return this.f15187 <= i;
    }
}
