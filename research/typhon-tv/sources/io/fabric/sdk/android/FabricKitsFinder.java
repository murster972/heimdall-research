package io.fabric.sdk.android;

import android.os.SystemClock;
import android.text.TextUtils;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

class FabricKitsFinder implements Callable<Map<String, KitInfo>> {

    /* renamed from: 龘  reason: contains not printable characters */
    final String f15217;

    FabricKitsFinder(String str) {
        this.f15217 = str;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private Map<String, KitInfo> m19064() throws Exception {
        KitInfo r3;
        HashMap hashMap = new HashMap();
        ZipFile r0 = m19067();
        Enumeration<? extends ZipEntry> entries = r0.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            if (zipEntry.getName().startsWith("fabric/") && zipEntry.getName().length() > "fabric/".length() && (r3 = m19066(zipEntry, r0)) != null) {
                hashMap.put(r3.m19081(), r3);
                Fabric.m19034().m19084("Fabric", String.format("Found kit:[%s] version:[%s]", new Object[]{r3.m19081(), r3.m19079()}));
            }
        }
        if (r0 != null) {
            try {
                r0.close();
            } catch (IOException e) {
            }
        }
        return hashMap;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private Map<String, KitInfo> m19065() {
        HashMap hashMap = new HashMap();
        try {
            Class.forName("com.google.android.gms.ads.AdView");
            KitInfo kitInfo = new KitInfo("com.google.firebase.firebase-ads", "0.0.0", "binary");
            hashMap.put(kitInfo.m19081(), kitInfo);
            Fabric.m19034().m19084("Fabric", "Found kit: com.google.firebase.firebase-ads");
        } catch (Exception e) {
        }
        return hashMap;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    private KitInfo m19066(ZipEntry zipEntry, ZipFile zipFile) {
        InputStream inputStream = null;
        try {
            inputStream = zipFile.getInputStream(zipEntry);
            Properties properties = new Properties();
            properties.load(inputStream);
            String property = properties.getProperty("fabric-identifier");
            String property2 = properties.getProperty("fabric-version");
            String property3 = properties.getProperty("fabric-build-type");
            if (TextUtils.isEmpty(property) || TextUtils.isEmpty(property2)) {
                throw new IllegalStateException("Invalid format of fabric file," + zipEntry.getName());
            }
            KitInfo kitInfo = new KitInfo(property, property2, property3);
            CommonUtils.m19175((Closeable) inputStream);
            return kitInfo;
        } catch (IOException e) {
            Fabric.m19034().m19083("Fabric", "Error when parsing fabric properties " + zipEntry.getName(), e);
            CommonUtils.m19175((Closeable) inputStream);
            return null;
        } catch (Throwable th) {
            CommonUtils.m19175((Closeable) inputStream);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public ZipFile m19067() throws IOException {
        return new ZipFile(this.f15217);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Map<String, KitInfo> call() throws Exception {
        HashMap hashMap = new HashMap();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        hashMap.putAll(m19065());
        hashMap.putAll(m19064());
        Fabric.m19034().m19084("Fabric", "finish scanning in " + (SystemClock.elapsedRealtime() - elapsedRealtime));
        return hashMap;
    }
}
