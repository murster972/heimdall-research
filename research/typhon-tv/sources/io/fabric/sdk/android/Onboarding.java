package io.fabric.sdk.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import io.fabric.sdk.android.services.common.ApiKey;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import io.fabric.sdk.android.services.network.DefaultHttpRequestFactory;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.settings.AppRequestData;
import io.fabric.sdk.android.services.settings.AppSettingsData;
import io.fabric.sdk.android.services.settings.CreateAppSpiCall;
import io.fabric.sdk.android.services.settings.IconRequest;
import io.fabric.sdk.android.services.settings.Settings;
import io.fabric.sdk.android.services.settings.SettingsData;
import io.fabric.sdk.android.services.settings.UpdateAppSpiCall;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

class Onboarding extends Kit<Boolean> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f15223;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f15224;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f15225;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f15226;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Future<Map<String, KitInfo>> f15227;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final Collection<Kit> f15228;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f15229;

    /* renamed from: 靐  reason: contains not printable characters */
    private PackageManager f15230;

    /* renamed from: 麤  reason: contains not printable characters */
    private PackageInfo f15231;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f15232;

    /* renamed from: 龘  reason: contains not printable characters */
    private final HttpRequestFactory f15233 = new DefaultHttpRequestFactory();

    public Onboarding(Future<Map<String, KitInfo>> future, Collection<Kit> collection) {
        this.f15227 = future;
        this.f15228 = collection;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m19093(String str, AppSettingsData appSettingsData, Collection<KitInfo> collection) {
        return new CreateAppSpiCall(this, m19099(), appSettingsData.f15432, this.f15233).m19406(m19096(IconRequest.m19433(getContext(), str), collection));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private SettingsData m19094() {
        try {
            Settings.m19434().m19439(this, this.idManager, this.f15233, this.f15229, this.f15223, m19099()).m19438();
            return Settings.m19434().m19436();
        } catch (Exception e) {
            Fabric.m19034().m19083("Fabric", "Error dealing with settings", e);
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m19095(String str, AppSettingsData appSettingsData, Collection<KitInfo> collection) {
        return m19097(appSettingsData, IconRequest.m19433(getContext(), str), collection);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private AppRequestData m19096(IconRequest iconRequest, Collection<KitInfo> collection) {
        Context context = getContext();
        return new AppRequestData(new ApiKey().m19134(context), getIdManager().m19210(), this.f15223, this.f15229, CommonUtils.m19171(CommonUtils.m19138(context)), this.f15225, DeliveryMechanism.determineFrom(this.f15224).getId(), this.f15226, "0", iconRequest, collection);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m19097(AppSettingsData appSettingsData, IconRequest iconRequest, Collection<KitInfo> collection) {
        return new UpdateAppSpiCall(this, m19099(), appSettingsData.f15432, this.f15233).m19446(m19096(iconRequest, collection));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m19098(String str, AppSettingsData appSettingsData, Collection<KitInfo> collection) {
        if ("new".equals(appSettingsData.f15430)) {
            if (m19093(str, appSettingsData, collection)) {
                return Settings.m19434().m19437();
            }
            Fabric.m19034().m19083("Fabric", "Failed to create app with Crashlytics service.", (Throwable) null);
            return false;
        } else if ("configured".equals(appSettingsData.f15430)) {
            return Settings.m19434().m19437();
        } else {
            if (!appSettingsData.f15427) {
                return true;
            }
            Fabric.m19034().m19090("Fabric", "Server says an update is required - forcing a full App update.");
            m19095(str, appSettingsData, collection);
            return true;
        }
    }

    public String getIdentifier() {
        return "io.fabric.sdk.android:fabric";
    }

    public String getVersion() {
        return "1.4.3.25";
    }

    /* access modifiers changed from: protected */
    public boolean onPreExecute() {
        try {
            this.f15224 = getIdManager().m19204();
            this.f15230 = getContext().getPackageManager();
            this.f15232 = getContext().getPackageName();
            this.f15231 = this.f15230.getPackageInfo(this.f15232, 0);
            this.f15229 = Integer.toString(this.f15231.versionCode);
            this.f15223 = this.f15231.versionName == null ? "0.0" : this.f15231.versionName;
            this.f15225 = this.f15230.getApplicationLabel(getContext().getApplicationInfo()).toString();
            this.f15226 = Integer.toString(getContext().getApplicationInfo().targetSdkVersion);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Fabric.m19034().m19083("Fabric", "Failed init", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m19099() {
        return CommonUtils.m19148(getContext(), "com.crashlytics.ApiEndpoint");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Boolean doInBackground() {
        String r2 = CommonUtils.m19143(getContext());
        boolean z = false;
        SettingsData r5 = m19094();
        if (r5 != null) {
            try {
                z = m19098(r2, r5.f15481, m19101((Map<String, KitInfo>) this.f15227 != null ? this.f15227.get() : new HashMap(), this.f15228).values());
            } catch (Exception e) {
                Fabric.m19034().m19083("Fabric", "Error performing auto configuration.", e);
            }
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Map<String, KitInfo> m19101(Map<String, KitInfo> map, Collection<Kit> collection) {
        for (Kit next : collection) {
            if (!map.containsKey(next.getIdentifier())) {
                map.put(next.getIdentifier(), new KitInfo(next.getIdentifier(), next.getVersion(), "binary"));
            }
        }
        return map;
    }
}
