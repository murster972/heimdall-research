package io.fabric.sdk.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import io.fabric.sdk.android.ActivityLifecycleManager;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.concurrency.DependsOn;
import io.fabric.sdk.android.services.concurrency.PriorityThreadPoolExecutor;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

public class Fabric {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Logger f15188 = new DefaultLogger();

    /* renamed from: 龘  reason: contains not printable characters */
    static volatile Fabric f15189;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Map<Class<? extends Kit>, Kit> f15190;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ExecutorService f15191;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Handler f15192;

    /* renamed from: ʾ  reason: contains not printable characters */
    private WeakReference<Activity> f15193;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public AtomicBoolean f15194 = new AtomicBoolean(false);

    /* renamed from: ˈ  reason: contains not printable characters */
    private ActivityLifecycleManager f15195;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final InitializationCallback<Fabric> f15196;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final InitializationCallback<?> f15197;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final IdManager f15198;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Context f15199;

    /* renamed from: 麤  reason: contains not printable characters */
    final boolean f15200;

    /* renamed from: 齉  reason: contains not printable characters */
    final Logger f15201;

    public static class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f15206;

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f15207;

        /* renamed from: ʽ  reason: contains not printable characters */
        private String f15208;

        /* renamed from: ˑ  reason: contains not printable characters */
        private InitializationCallback<Fabric> f15209;

        /* renamed from: 连任  reason: contains not printable characters */
        private Logger f15210;

        /* renamed from: 靐  reason: contains not printable characters */
        private Kit[] f15211;

        /* renamed from: 麤  reason: contains not printable characters */
        private Handler f15212;

        /* renamed from: 齉  reason: contains not printable characters */
        private PriorityThreadPoolExecutor f15213;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Context f15214;

        public Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.f15214 = context;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m19062(Kit... kitArr) {
            if (this.f15211 != null) {
                throw new IllegalStateException("Kits already set.");
            }
            this.f15211 = kitArr;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Fabric m19063() {
            if (this.f15213 == null) {
                this.f15213 = PriorityThreadPoolExecutor.m19278();
            }
            if (this.f15212 == null) {
                this.f15212 = new Handler(Looper.getMainLooper());
            }
            if (this.f15210 == null) {
                if (this.f15206) {
                    this.f15210 = new DefaultLogger(3);
                } else {
                    this.f15210 = new DefaultLogger();
                }
            }
            if (this.f15208 == null) {
                this.f15208 = this.f15214.getPackageName();
            }
            if (this.f15209 == null) {
                this.f15209 = InitializationCallback.f15218;
            }
            Map hashMap = this.f15211 == null ? new HashMap() : Fabric.m19039((Collection<? extends Kit>) Arrays.asList(this.f15211));
            Context applicationContext = this.f15214.getApplicationContext();
            return new Fabric(applicationContext, hashMap, this.f15213, this.f15212, this.f15210, this.f15206, this.f15209, new IdManager(applicationContext, this.f15208, this.f15207, hashMap.values()), Fabric.m19040(this.f15214));
        }
    }

    Fabric(Context context, Map<Class<? extends Kit>, Kit> map, PriorityThreadPoolExecutor priorityThreadPoolExecutor, Handler handler, Logger logger, boolean z, InitializationCallback initializationCallback, IdManager idManager, Activity activity) {
        this.f15199 = context;
        this.f15190 = map;
        this.f15191 = priorityThreadPoolExecutor;
        this.f15192 = handler;
        this.f15201 = logger;
        this.f15200 = z;
        this.f15196 = initializationCallback;
        this.f15197 = m19057(map.size());
        this.f15198 = idManager;
        m19056(activity);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static Logger m19034() {
        return f15189 == null ? f15188 : f15189.f15201;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static boolean m19035() {
        if (f15189 == null) {
            return false;
        }
        return f15189.f15200;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static boolean m19036() {
        return f15189 != null && f15189.f15194.get();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m19037() {
        this.f15195 = new ActivityLifecycleManager(this.f15199);
        this.f15195.m19016(new ActivityLifecycleManager.Callbacks() {
            public void onActivityCreated(Activity activity, Bundle bundle) {
                Fabric.this.m19056(activity);
            }

            public void onActivityResumed(Activity activity) {
                Fabric.this.m19056(activity);
            }

            public void onActivityStarted(Activity activity) {
                Fabric.this.m19056(activity);
            }
        });
        m19058(this.f15199);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static Map<Class<? extends Kit>, Kit> m19039(Collection<? extends Kit> collection) {
        HashMap hashMap = new HashMap(collection.size());
        m19048((Map<Class<? extends Kit>, Kit>) hashMap, collection);
        return hashMap;
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static Activity m19040(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static void m19042(Fabric fabric) {
        f15189 = fabric;
        fabric.m19037();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Fabric m19043() {
        if (f15189 != null) {
            return f15189;
        }
        throw new IllegalStateException("Must Initialize Fabric before using singleton()");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Fabric m19044(Context context, Kit... kitArr) {
        if (f15189 == null) {
            synchronized (Fabric.class) {
                if (f15189 == null) {
                    m19042(new Builder(context).m19062(kitArr).m19063());
                }
            }
        }
        return f15189;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends Kit> T m19045(Class<T> cls) {
        return (Kit) m19043().f15190.get(cls);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m19048(Map<Class<? extends Kit>, Kit> map, Collection<? extends Kit> collection) {
        for (Kit kit : collection) {
            map.put(kit.getClass(), kit);
            if (kit instanceof KitGroup) {
                m19048(map, ((KitGroup) kit).getKits());
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public ExecutorService m19049() {
        return this.f15191;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Collection<Kit> m19050() {
        return this.f15190.values();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public ActivityLifecycleManager m19051() {
        return this.f15195;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Activity m19052() {
        if (this.f15193 != null) {
            return (Activity) this.f15193.get();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Future<Map<String, KitInfo>> m19053(Context context) {
        return m19049().submit(new FabricKitsFinder(context.getPackageCodePath()));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m19054() {
        return "io.fabric.sdk.android:fabric";
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m19055() {
        return "1.4.3.25";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Fabric m19056(Activity activity) {
        this.f15193 = new WeakReference<>(activity);
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public InitializationCallback<?> m19057(final int i) {
        return new InitializationCallback() {

            /* renamed from: 龘  reason: contains not printable characters */
            final CountDownLatch f15205 = new CountDownLatch(i);

            /* renamed from: 龘  reason: contains not printable characters */
            public void m19060(Exception exc) {
                Fabric.this.f15196.m19069(exc);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m19061(Object obj) {
                this.f15205.countDown();
                if (this.f15205.getCount() == 0) {
                    Fabric.this.f15194.set(true);
                    Fabric.this.f15196.m19070(Fabric.this);
                }
            }
        };
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19058(Context context) {
        Future<Map<String, KitInfo>> r1 = m19053(context);
        Collection<Kit> r5 = m19050();
        Onboarding onboarding = new Onboarding(r1, r5);
        ArrayList<Kit> arrayList = new ArrayList<>(r5);
        Collections.sort(arrayList);
        onboarding.injectParameters(context, this, InitializationCallback.f15218, this.f15198);
        for (Kit injectParameters : arrayList) {
            injectParameters.injectParameters(context, this, this.f15197, this.f15198);
        }
        onboarding.initialize();
        StringBuilder append = m19034().m19092("Fabric", 3) ? new StringBuilder("Initializing ").append(m19054()).append(" [Version: ").append(m19055()).append("], with the following kits:\n") : null;
        for (Kit kit : arrayList) {
            kit.initializationTask.addDependency(onboarding.initializationTask);
            m19059(this.f15190, kit);
            kit.initialize();
            if (append != null) {
                append.append(kit.getIdentifier()).append(" [Version: ").append(kit.getVersion()).append("]\n");
            }
        }
        if (append != null) {
            m19034().m19090("Fabric", append.toString());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19059(Map<Class<? extends Kit>, Kit> map, Kit kit) {
        DependsOn dependsOn = kit.dependsOnAnnotation;
        if (dependsOn != null) {
            for (Class cls : dependsOn.m6823()) {
                if (cls.isInterface()) {
                    for (Kit next : map.values()) {
                        if (cls.isAssignableFrom(next.getClass())) {
                            kit.initializationTask.addDependency(next.initializationTask);
                        }
                    }
                } else if (map.get(cls) == null) {
                    throw new UnmetDependencyException("Referenced Kit was null, does the kit exist?");
                } else {
                    kit.initializationTask.addDependency(map.get(cls).initializationTask);
                }
            }
        }
    }
}
