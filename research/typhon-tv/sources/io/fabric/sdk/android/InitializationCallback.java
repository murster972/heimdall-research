package io.fabric.sdk.android;

public interface InitializationCallback<T> {

    /* renamed from: 麤  reason: contains not printable characters */
    public static final InitializationCallback f15218 = new Empty();

    public static class Empty implements InitializationCallback<Object> {
        private Empty() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m19071(Exception exc) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m19072(Object obj) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    void m19069(Exception exc);

    /* renamed from: 龘  reason: contains not printable characters */
    void m19070(T t);
}
