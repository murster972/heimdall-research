package io.presage;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ServiceInfo;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.finder.IFinderResult;
import io.presage.finder.KyoKusanagi;
import io.presage.helper.ChinGentsai;
import io.presage.p028case.BenimaruNikaido;
import io.presage.p028case.GoroDaimon;
import io.presage.p029char.ChangKoehan;
import io.presage.p034goto.ChizuruKagura;
import io.presage.p034goto.Goenitz;
import io.presage.p034goto.Mature;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.ShingoYabuki;
import io.presage.p034goto.Whip;
import io.presage.parser.ChinGentsai;
import io.presage.provider.PresageProvider;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class Presage {
    public static final String a = Presage.class.getSimpleName();
    private static Presage b;
    /* access modifiers changed from: private */
    public Context c;
    private ChinGentsai d;
    /* access modifiers changed from: private */
    public GoroDaimon e = null;
    /* access modifiers changed from: private */
    public Goenitz f;

    private Presage() {
    }

    private void a() {
        Application a2;
        if (this.c != null && this.e == null && (a2 = ShingoYabuki.a(this.c)) != null) {
            this.e = new BenimaruNikaido(a2, new io.presage.p028case.ChinGentsai() {
                public void a(Activity activity) {
                    SaishuKusanagi.a(Presage.a, "app leave");
                    ChizuruKagura.a().execute(new Runnable() {
                        public void run() {
                            io.presage.p034goto.GoroDaimon.a();
                        }
                    });
                }

                public void b(Activity activity) {
                    SaishuKusanagi.a(Presage.a, "app enter");
                    if (Presage.this.f != null) {
                        Presage.this.f.a();
                    }
                }

                public void c(Activity activity) {
                    SaishuKusanagi.a(Presage.a, "app home");
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void a(final int i) {
        if (Whip.a(this.c, "android.permission.GET_ACCOUNTS")) {
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep((long) (i * 1000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (Whip.a(Presage.this.c, "android.permission.GET_ACCOUNTS")) {
                        IFinderResult a2 = new KyoKusanagi(Presage.this.c).a();
                        Bundle bundle = new Bundle();
                        bundle.putInt(VastExtensionXmlManager.TYPE, 5);
                        bundle.putParcelable("android_accounts", a2);
                        Presage.getInstance().setContext(Presage.this.c);
                        Presage.getInstance().talkToService(bundle);
                    }
                }
            });
            thread.setName("android_accounts");
            thread.start();
        }
    }

    private void a(Intent intent) {
        try {
            this.c.startService(intent);
        } catch (Exception e2) {
            SaishuKusanagi.b(a, "start service error", e2);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        String str = null;
        if (this.c != null) {
            Uri a2 = PresageProvider.a(PresageProvider.KyoKusanagi.PROFIG_JSON, PresageProvider.a(this.c));
            if (a2 == null) {
                a2 = PresageProvider.a(PresageProvider.KyoKusanagi.PROFIG_JSON, PresageProvider.a(this.c));
            }
            io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(this.c.getContentResolver());
            Cursor a3 = kyoKusanagi.a(a2, (String[]) null, (String) null, (String[]) null, (String) null);
            Cursor a4 = a3 == null ? kyoKusanagi.a(a2, (String[]) null, (String) null, (String[]) null, (String) null) : a3;
            if (a4 != null) {
                if (a4.moveToLast()) {
                    str = a4.getString(a4.getColumnIndex("v"));
                    SaishuKusanagi.a(a, "profigJson: " + str);
                }
                a4.close();
                if (str != null) {
                    try {
                        JSONObject jSONObject = new JSONObject(str);
                        if (jSONObject != null) {
                            new io.presage.parser.ChinGentsai("profig", new ChinGentsai.KyoKusanagi() {
                                public void a() {
                                }

                                public void a(List<io.presage.parser.p040do.BenimaruNikaido> list, JSONObject jSONObject) {
                                    io.presage.parser.p040do.BenimaruNikaido benimaruNikaido;
                                    SharedPreferences.Editor edit;
                                    Iterator<io.presage.parser.p040do.BenimaruNikaido> it2 = list.iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            benimaruNikaido = null;
                                            break;
                                        }
                                        benimaruNikaido = it2.next();
                                        if (benimaruNikaido.b() != null && benimaruNikaido.b().equals(PresageProvider.b(Presage.this.c))) {
                                            break;
                                        }
                                    }
                                    if (benimaruNikaido != null) {
                                        SaishuKusanagi.a("updateInitManagerProfigJson", benimaruNikaido.toString());
                                        ChangKoehan.a().a(benimaruNikaido.d());
                                        ChangKoehan.a().c(benimaruNikaido.f().d());
                                        ChangKoehan.a().d(benimaruNikaido.f().e());
                                        ChangKoehan.a().b(benimaruNikaido.f().f());
                                        ChangKoehan.a().c(benimaruNikaido.f().g());
                                        ChangKoehan.a().a(benimaruNikaido.f().a());
                                        ChangKoehan.a().b(benimaruNikaido.f().b());
                                        ChangKoehan.a().a(benimaruNikaido.f().c());
                                        ChangKoehan.a().a(benimaruNikaido.f().h());
                                        if (benimaruNikaido.a() != null && Presage.this.c != null && (edit = Presage.this.c.getSharedPreferences("crash_report", 0).edit()) != null) {
                                            edit.putString("crash_report", benimaruNikaido.a());
                                            edit.apply();
                                        }
                                    }
                                }
                            }).a(jSONObject);
                        }
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        float f2;
        if (this.c != null) {
            io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(this.c.getContentResolver());
            Cursor a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.ADS_TIMEOUT, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
            if (a2 == null) {
                a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.ADS_TIMEOUT, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
            }
            if (a2 != null) {
                if (a2.moveToLast()) {
                    f2 = a2.getFloat(a2.getColumnIndex("v"));
                    SaishuKusanagi.a(a, "adsTimeout: " + f2);
                } else {
                    f2 = -1.0f;
                }
                a2.close();
                if (f2 != -1.0f) {
                    ChangKoehan.a().a(f2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.c != null) {
            io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(this.c.getContentResolver());
            Cursor a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.AAID, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
            if (a2 == null) {
                a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.AAID, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
            }
            if (a2 != null) {
                String str = "";
                if (a2.moveToLast()) {
                    str = a2.getString(a2.getColumnIndex("v"));
                    SaishuKusanagi.a(a, "aaid: " + str);
                }
                a2.close();
                if (str != "") {
                    ChangKoehan.a().a(str);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.c != null) {
            io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(this.c.getContentResolver());
            Cursor a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.ADS_OPTIN, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
            if (a2 == null) {
                a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.ADS_OPTIN, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
            }
            if (a2 != null) {
                if (a2.moveToLast()) {
                    ChangKoehan.a().b(a2.getInt(a2.getColumnIndex("v")) > 0);
                }
                a2.close();
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.c != null) {
            io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(this.c.getContentResolver());
            Cursor a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.LAUNCH_OPTIN, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
            if (a2 == null) {
                a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.LAUNCH_OPTIN, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
            }
            if (a2 != null) {
                if (a2.moveToLast()) {
                    ChangKoehan.a().c(a2.getInt(a2.getColumnIndex("v")) > 0);
                }
                a2.close();
            }
        }
    }

    public static Presage getInstance() {
        if (b == null) {
            b = new Presage();
        }
        return b;
    }

    public void setContext(Context context) {
        if (this.c == null) {
            this.c = context.getApplicationContext();
            io.presage.p034goto.p035do.KyoKusanagi.a(this.c);
            this.d = new io.presage.helper.ChinGentsai();
            this.d.a(this.c, this.c.getPackageName());
            this.d.c();
            if (this.f == null) {
                this.f = new Goenitz(this.c);
            }
        }
    }

    public void start() {
        if (this.c == null) {
            SaishuKusanagi.d(a, "context is null, you should call setContext() first");
            return;
        }
        String b2 = PresageProvider.b(this.c);
        if (b2 == null || b2.isEmpty()) {
            SaishuKusanagi.d(a, "api is null or empty");
            return;
        }
        try {
            if (Integer.parseInt(b2) == 0) {
                SaishuKusanagi.d(a, "api key is 0");
                return;
            }
            a();
            ChangKoehan.a().a(this.c, ChangKoehan.BenimaruNikaido.APP_PROCESS, new ChangKoehan.KyoKusanagi() {
                public void a() {
                    io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(Presage.this.c.getContentResolver());
                    Presage.this.e.a();
                    Presage.this.b();
                    Presage.this.c();
                    if (ChangKoehan.a().p()) {
                        io.presage.p034goto.KyoKusanagi.a().c();
                    }
                    if (Presage.this.c == null) {
                        SaishuKusanagi.d(Presage.a, "context is null, you should call setContext() first");
                        return;
                    }
                    Uri a2 = PresageProvider.a(PresageProvider.KyoKusanagi.PROFIG_JSON, PresageProvider.a(Presage.this.c));
                    if (a2 == null) {
                        a2 = PresageProvider.a(PresageProvider.KyoKusanagi.PROFIG_JSON, PresageProvider.a(Presage.this.c));
                    }
                    if (a2 == null) {
                        SaishuKusanagi.d(Presage.a, "Can't start (PROFIG_JSON empty)");
                        return;
                    }
                    kyoKusanagi.a(a2, true, (ContentObserver) new ContentObserver((Handler) null) {
                        public void onChange(boolean z, Uri uri) {
                            super.onChange(z, uri);
                            SaishuKusanagi.a(Presage.a, "presage provider profigJson updated");
                            Presage.this.b();
                        }
                    });
                    kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.ADS_TIMEOUT, PresageProvider.a(Presage.this.c)), true, (ContentObserver) new ContentObserver((Handler) null) {
                        public void onChange(boolean z, Uri uri) {
                            super.onChange(z, uri);
                            SaishuKusanagi.a(Presage.a, "presage provider Ads_timeout updated");
                            Presage.this.c();
                        }
                    });
                    kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.AAID, PresageProvider.a(Presage.this.c)), true, (ContentObserver) new ContentObserver((Handler) null) {
                        public void onChange(boolean z) {
                            super.onChange(z);
                            SaishuKusanagi.a(Presage.a, "presage provider aaid updated");
                            Presage.this.d();
                        }
                    });
                    kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.ADS_OPTIN, PresageProvider.a(Presage.this.c)), true, (ContentObserver) new ContentObserver((Handler) null) {
                        public void onChange(boolean z, Uri uri) {
                            super.onChange(z, uri);
                            SaishuKusanagi.a(Presage.a, "presage provider ads_optin updated");
                            Presage.this.e();
                            io.presage.p034goto.KyoKusanagi.a().c();
                        }
                    });
                    kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.LAUNCH_OPTIN, PresageProvider.a(Presage.this.c)), true, (ContentObserver) new ContentObserver((Handler) null) {
                        public void onChange(boolean z, Uri uri) {
                            super.onChange(z, uri);
                            SaishuKusanagi.a(Presage.a, "presage provider launch_optin updated");
                            Presage.this.f();
                        }
                    });
                    Cursor a3 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.AAID, PresageProvider.a(Presage.this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
                    Cursor a4 = a3 == null ? kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.AAID, PresageProvider.a(Presage.this.c)), (String[]) null, (String) null, (String[]) null, (String) null) : a3;
                    String str = "";
                    if (a4 != null) {
                        if (a4.moveToLast()) {
                            str = a4.getString(a4.getColumnIndex("v"));
                            SaishuKusanagi.a(Presage.a, "aaid: " + str);
                        }
                        a4.close();
                    }
                    if (str != "" && !str.equals(ChangKoehan.a().n())) {
                        Presage.this.c.sendBroadcast(new Intent("io.presage.receiver.action.UPDATE_PROFIG"));
                    }
                    Presage.this.a(10);
                }
            });
            e();
            f();
            talkToService((Bundle) null);
        } catch (Exception e2) {
            SaishuKusanagi.d(a, "invalid api key");
        }
    }

    public void start(String str) {
        if (this.c == null) {
            SaishuKusanagi.d(a, "context is null, you should call setContext() first");
        } else if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("api key is null or empty");
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put("package", this.c.getPackageName());
            contentValues.put("apikey", str);
            contentValues.put("update_time", Long.valueOf(System.currentTimeMillis()));
            new io.presage.provider.KyoKusanagi(this.c.getContentResolver()).a(PresageProvider.a(PresageProvider.KyoKusanagi.APIKEY, PresageProvider.a(this.c)), contentValues, (String) null, (String[]) null);
            Mature.e(this.c);
            start();
        }
    }

    public void talkToService(Bundle bundle) {
        int i;
        ServiceInfo a2 = Mature.a(this.c);
        if (a2 == null) {
            SaishuKusanagi.d(a, "no sdk to start -> this should not happen!!!");
            return;
        }
        ComponentName componentName = new ComponentName(a2.packageName, a2.name);
        SaishuKusanagi.a(a, "Best SDK to Start: " + componentName);
        ActivityManager.RunningServiceInfo c2 = Mature.c(this.c, PresageService.class.getName());
        if (c2 == null) {
            SaishuKusanagi.a(a, "no running service, so start the best one" + componentName);
            Intent intent = new Intent();
            intent.setComponent(componentName);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            a(intent);
        } else if (c2.service.equals(componentName)) {
            SaishuKusanagi.a(a, " the running service is the same one supposed to start, so we do nothing");
            Intent intent2 = new Intent();
            intent2.setComponent(componentName);
            if (bundle != null) {
                intent2.putExtras(bundle);
            }
            a(intent2);
            if (bundle == null) {
                io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(this.c.getContentResolver());
                Cursor a3 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.DATA_OPTIN_ABOVE_V_2_1, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
                if (a3 == null) {
                    a3 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.DATA_OPTIN_ABOVE_V_2_1, PresageProvider.a(this.c)), (String[]) null, (String) null, (String[]) null, (String) null);
                }
                if (a3 != null) {
                    i = a3.moveToLast() ? a3.getInt(a3.getColumnIndex("v")) : -1;
                    a3.close();
                } else {
                    i = -1;
                }
                if (i == -1) {
                    this.c.sendBroadcast(new Intent("io.presage.receiver.action.UPDATE_PROFIG"));
                }
            }
        } else {
            SaishuKusanagi.a(a, " the running service is not the supposed to start, so stop the running one and start the new one");
            Intent intent3 = new Intent();
            intent3.setComponent(c2.service);
            this.c.stopService(intent3);
            Intent intent4 = new Intent();
            intent4.setComponent(componentName);
            if (bundle != null) {
                intent4.putExtras(bundle);
            }
            a(intent4);
        }
    }
}
