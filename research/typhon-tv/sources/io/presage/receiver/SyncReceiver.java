package io.presage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.Presage;
import io.presage.p034goto.SaishuKusanagi;

public class SyncReceiver extends BroadcastReceiver {
    public static final String a = SyncReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        SaishuKusanagi.a(a, action);
        if (action != null && intent.getAction().equals("io.presage.receiver.SyncReceiver.SYNC")) {
            try {
                Bundle bundle = (Bundle) intent.getExtras().clone();
                bundle.putInt(VastExtensionXmlManager.TYPE, 4);
                Presage.getInstance().setContext(context);
                Presage.getInstance().talkToService(bundle);
            } catch (Exception e) {
                SaishuKusanagi.b(a, e.getMessage(), e);
            }
        }
    }
}
