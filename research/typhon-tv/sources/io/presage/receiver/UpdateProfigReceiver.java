package io.presage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.Presage;
import io.presage.p034goto.SaishuKusanagi;

public class UpdateProfigReceiver extends BroadcastReceiver {
    public static final String a = UpdateProfigReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction() != null && intent.getAction().equals("io.presage.receiver.action.UPDATE_PROFIG")) {
            Bundle bundle = new Bundle();
            bundle.putInt(VastExtensionXmlManager.TYPE, 6);
            try {
                Presage.getInstance().setContext(context);
                Presage.getInstance().talkToService(bundle);
            } catch (Exception e) {
                SaishuKusanagi.b(a, e.getMessage(), e);
            }
        }
    }
}
