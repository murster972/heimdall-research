package io.presage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.Presage;
import io.presage.p034goto.SaishuKusanagi;

public class RequestReceiver extends BroadcastReceiver {
    public static final String a = RequestReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra("url");
        int intExtra = intent.getIntExtra("method", -666);
        String[] stringArrayExtra = intent.getStringArrayExtra("headers");
        String stringExtra2 = intent.getStringExtra(TtmlNode.TAG_BODY);
        String stringExtra3 = intent.getStringExtra("bundle");
        String stringExtra4 = intent.getStringExtra("app_launch_package");
        String stringExtra5 = intent.getStringExtra("app_launch_rate");
        String stringExtra6 = intent.getStringExtra("app_launch_auto");
        String stringExtra7 = intent.getStringExtra("app_launch_class");
        SaishuKusanagi.a(a, "request-received : " + stringExtra + " || " + intExtra + " || " + stringArrayExtra + " || " + stringExtra2 + " || " + stringExtra3);
        if (stringExtra != null && intExtra != -666 && stringArrayExtra != null && stringExtra2 != null && stringExtra3 != null) {
            SaishuKusanagi.d(a, "request-received to process");
            Bundle bundle = new Bundle();
            bundle.putString("url", stringExtra);
            bundle.putInt("method", intExtra);
            bundle.putStringArray("headers", stringArrayExtra);
            bundle.putString(TtmlNode.TAG_BODY, stringExtra2);
            bundle.putString("bundle", stringExtra3);
            bundle.putString("app_launch_package", stringExtra4);
            bundle.putString("app_launch_rate", stringExtra5);
            bundle.putString("app_launch_auto", stringExtra6);
            bundle.putString("app_launch_class", stringExtra7);
            bundle.putInt(VastExtensionXmlManager.TYPE, 2);
            if (isOrderedBroadcast()) {
                abortBroadcast();
            }
            try {
                Presage.getInstance().setContext(context);
                Presage.getInstance().talkToService(bundle);
            } catch (Exception e) {
                SaishuKusanagi.b(a, e.getMessage(), e);
            }
        }
    }
}
