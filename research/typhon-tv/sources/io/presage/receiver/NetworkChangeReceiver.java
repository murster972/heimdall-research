package io.presage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.Presage;
import io.presage.PresageService;
import io.presage.finder.model.IpTracker;
import io.presage.p034goto.Mature;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.provider.PresageProvider;
import java.util.Date;

public class NetworkChangeReceiver extends BroadcastReceiver {
    public static final String a = NetworkChangeReceiver.class.getSimpleName();
    public static KyoKusanagi b = new KyoKusanagi(new Date(), "UNKNOWN");

    private static class KyoKusanagi {
        public Date a;
        public String b;

        public KyoKusanagi(Date date, String str) {
            this.a = date;
            this.b = str;
        }
    }

    private void a(Context context) {
        String b2 = PresageProvider.b(context);
        if (b2 != null && !b2.isEmpty()) {
            Presage.getInstance().setContext(context);
            Presage.getInstance().start();
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        SaishuKusanagi.a(a, action);
        if (action != null) {
            if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                IpTracker ipTracker = new IpTracker();
                if (activeNetworkInfo == null || 0 == 0) {
                    ipTracker.a(new Date().getTime());
                    ipTracker.a("No_Connection");
                } else {
                    String str = "UNKNOWN";
                    if (activeNetworkInfo.getType() == 1) {
                        str = "WIFI";
                    } else if (activeNetworkInfo.getType() == 0) {
                        str = activeNetworkInfo.getSubtypeName();
                    }
                    Date date = new Date();
                    long time = (date.getTime() - b.a.getTime()) / 1000;
                    if (!str.equals(b.b) || time >= 5) {
                        b.a = date;
                        b.b = str;
                        ipTracker.a(str);
                        ipTracker.a(date.getTime());
                    } else {
                        return;
                    }
                }
                Intent intent2 = new Intent(context, PresageService.class);
                intent2.setAction("ip_tracker");
                intent2.putExtra(VastExtensionXmlManager.TYPE, 1);
                intent2.putExtra("service_name", "ip_tracker");
                intent2.putExtra("ipTrackerTimestamp", ipTracker.b());
                intent2.putExtra("ipTrackerType", ipTracker.c());
                intent2.putExtra("package", context.getPackageName());
                try {
                    Presage.getInstance().setContext(context);
                    Presage.getInstance().talkToService(intent2.getExtras());
                } catch (Exception e) {
                    SaishuKusanagi.b(a, "failed to start the service", e);
                }
            } else if (action.equals("android.net.wifi.WIFI_STATE_CHANGED")) {
                try {
                    Mature.e(context);
                    a(context);
                } catch (Exception e2) {
                    SaishuKusanagi.b(a, "failed to restart the service", e2);
                }
            } else if (action.equals("io.presage.receiver.NetworkChangeReceiver.ONDESTROY")) {
                try {
                    Mature.e(context);
                    a(context);
                } catch (Exception e3) {
                    SaishuKusanagi.b(a, "failed to restart the service", e3);
                }
            }
        }
    }
}
