package io.presage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import io.presage.Presage;
import io.presage.p034goto.SaishuKusanagi;

public class AlarmReceiver extends BroadcastReceiver {
    public static final String a = AlarmReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        SaishuKusanagi.a(a, "onReceive");
        try {
            Presage.getInstance().setContext(context);
            Presage.getInstance().talkToService((Bundle) intent.getExtras().clone());
        } catch (Exception e) {
            SaishuKusanagi.b(a, e.getMessage(), e);
        }
    }
}
