package io.presage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.Presage;
import io.presage.ads.NewAd;
import io.presage.p034goto.SaishuKusanagi;
import org.json.JSONException;
import org.json.JSONObject;

public class InstallReceiver extends BroadcastReceiver {
    public static final String a = InstallReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        Bundle bundle = new Bundle();
        SaishuKusanagi.b("InstallReceiver", String.format("Receive Intent: %s", new Object[]{intent.toString()}));
        String str = null;
        try {
            String str2 = intent.getDataString().split(":")[1];
            bundle.putInt(VastExtensionXmlManager.TYPE, 3);
            if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction()) && !intent.getBooleanExtra("android.intent.extra.REPLACING", false)) {
                str = NewAd.EVENT_INSTALL;
            }
            if (("android.intent.action.PACKAGE_REMOVED".equals(intent.getAction()) || "android.intent.action.PACKAGE_FULLY_REMOVED".equals(intent.getAction())) && !intent.getBooleanExtra("android.intent.extra.REPLACING", false)) {
                str = "uninstall";
            }
            if ("android.intent.action.PACKAGE_REPLACED".equals(intent.getAction())) {
                str = "update";
            }
            if (str != null && str2 != null) {
                bundle.putString(NotificationCompat.CATEGORY_EVENT, str);
                bundle.putString("package", str2);
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put(NotificationCompat.CATEGORY_EVENT, (Object) str);
                    jSONObject.put("package", (Object) str2);
                    bundle.putString("data", jSONObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    Presage.getInstance().setContext(context);
                    Presage.getInstance().talkToService(bundle);
                } catch (Exception e2) {
                    SaishuKusanagi.b(a, e2.getMessage(), e2);
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
