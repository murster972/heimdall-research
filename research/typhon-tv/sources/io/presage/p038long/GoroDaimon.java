package io.presage.p038long;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.widget.Toast;
import com.Pinkamena;
import io.presage.formats.multiwebviews.GoroDaimon;
import io.presage.helper.Permissions;
import io.presage.model.Zone;
import io.presage.p034goto.Bao;
import io.presage.p034goto.ChangKoehan;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p038long.KyoKusanagi;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

/* renamed from: io.presage.long.GoroDaimon  reason: invalid package */
public class GoroDaimon extends WebView implements KyoKusanagi {
    private String a;
    private boolean b;
    private boolean c;
    private io.presage.formats.multiwebviews.GoroDaimon d;
    /* access modifiers changed from: private */
    public ChinGentsai e;
    private C0050GoroDaimon f;
    /* access modifiers changed from: private */
    public ChoiBounge g;
    /* access modifiers changed from: private */
    public ChangKoehan h;
    /* access modifiers changed from: private */
    public KyoKusanagi.C0051KyoKusanagi i;
    /* access modifiers changed from: private */
    public BenimaruNikaido j;
    private KyoKusanagi k;
    private GoroDaimon.ChinGentsai l = new GoroDaimon.ChinGentsai() {
        public void a(WebView webView, String str) {
            if (GoroDaimon.this.g != null) {
                GoroDaimon.this.g.b((GoroDaimon) webView, str);
            }
        }
    };
    private GoroDaimon.C0046GoroDaimon m = new GoroDaimon.C0046GoroDaimon() {
        public void a(WebView webView, String str) {
            if (GoroDaimon.this.h != null) {
                GoroDaimon.this.h.c((GoroDaimon) webView, str);
            }
        }
    };
    private GoroDaimon.BenimaruNikaido n = new GoroDaimon.BenimaruNikaido() {
        public void a(WebView webView, String str, boolean z) {
            SaishuKusanagi.a("ManagedWebView", "onOverrideUrl: " + str);
            if (GoroDaimon.this.e != null) {
                GoroDaimon.this.e.a(webView, str, z);
            }
        }
    };
    private GoroDaimon.KyoKusanagi o = new GoroDaimon.KyoKusanagi() {
        public void a(WebView webView, String str, String str2) {
            if (GoroDaimon.this.j != null) {
                GoroDaimon.this.j.a(webView, str, str2);
            }
        }
    };
    private View.OnTouchListener p = new io.presage.p034goto.ChangKoehan(new ChangKoehan.KyoKusanagi() {
        public void a(View view) {
            if (GoroDaimon.this.i != null) {
                GoroDaimon.this.i.a((KyoKusanagi) view);
            }
        }
    }, 2);

    /* renamed from: io.presage.long.GoroDaimon$BenimaruNikaido */
    public interface BenimaruNikaido {
        void a(WebView webView, String str, String str2);
    }

    /* renamed from: io.presage.long.GoroDaimon$ChangKoehan */
    public interface ChangKoehan {
        void c(GoroDaimon goroDaimon, String str);
    }

    /* renamed from: io.presage.long.GoroDaimon$ChinGentsai */
    public interface ChinGentsai {
        void a(WebView webView, String str, boolean z);
    }

    /* renamed from: io.presage.long.GoroDaimon$ChoiBounge */
    public interface ChoiBounge {
        void b(GoroDaimon goroDaimon, String str);
    }

    /* renamed from: io.presage.long.GoroDaimon$GoroDaimon  reason: collision with other inner class name */
    public interface C0050GoroDaimon {
        void a(GoroDaimon goroDaimon, String str);
    }

    /* renamed from: io.presage.long.GoroDaimon$KyoKusanagi */
    public interface KyoKusanagi {
        void a(GoroDaimon goroDaimon);
    }

    @TargetApi(7)
    public GoroDaimon(final Context context, Permissions permissions, String str) {
        super(context);
        this.a = str;
        this.b = false;
        this.c = false;
        getSettings().setJavaScriptEnabled(true);
        getSettings().setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT >= 11) {
            getSettings().setDisplayZoomControls(false);
        }
        this.d = new io.presage.formats.multiwebviews.GoroDaimon();
        setWebViewClient(this.d);
        setWebChromeClient(new io.presage.formats.multiwebviews.BenimaruNikaido(str, context));
        setDownloadListener(new DownloadListener() {
            @TargetApi(11)
            public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
                if (!io.presage.helper.ChinGentsai.b(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                    SaishuKusanagi.c("ManagedWebView", "Unable to download file. Permission WRITE_EXTERNAL_STORAGE not declared");
                    return;
                }
                SaishuKusanagi.b("ManagedWebView", String.format("Download %s file", new Object[]{str}));
                Uri parse = Uri.parse(str);
                String[] split = parse.getPath().split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                String str5 = split[split.length - 1];
                DownloadManager.Request request = new DownloadManager.Request(parse);
                request.setTitle(str5);
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(1);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "download");
                ((DownloadManager) context.getSystemService("download")).enqueue(request);
                Toast makeText = Toast.makeText(context, String.format("Start downloading %s", new Object[]{str5}), 0);
            }
        });
        getSettings().setUseWideViewPort(true);
        getSettings().setLoadWithOverviewMode(true);
        getSettings().setDomStorageEnabled(true);
        getSettings().setDatabaseEnabled(true);
        getSettings().setSupportMultipleWindows(true);
        getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(this, true);
            getSettings().setMixedContentMode(2);
        }
        if (Build.VERSION.SDK_INT <= 19) {
            getSettings().setDatabasePath(context.getDir("database", 0).getPath());
        }
        if (Build.VERSION.SDK_INT <= 18) {
            getSettings().setSavePassword(true);
        }
        getSettings().setLoadsImagesAutomatically(true);
        if (Build.VERSION.SDK_INT >= 17) {
            getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        getSettings().setSaveFormData(true);
        getSettings().setSupportZoom(true);
        this.d.a(this.n);
        this.d.a(this.l);
        this.d.a(this.m);
        this.d.a(this.o);
        setOnTouchListener(this.p);
    }

    public static GoroDaimon a(Context context, Permissions permissions, Zone zone) {
        GoroDaimon goroDaimon = new GoroDaimon(context, permissions, zone.getName());
        goroDaimon.setFocusable(true);
        goroDaimon.setFocusableInTouchMode(true);
        String url = zone.getUrl();
        Pinkamena.DianePie();
        goroDaimon.setLayoutParams(Bao.b(context, zone));
        Bao.a((View) goroDaimon, zone);
        if (zone.isTrackHistory()) {
            goroDaimon.a();
        }
        if (zone.isTrackLanding()) {
            goroDaimon.b();
        }
        return goroDaimon;
    }

    public void a() {
        this.b = true;
    }

    public void a(String str) {
        super.loadUrl("javascript:" + str);
    }

    public void a(String str, Object obj) {
        StringBuilder sb = new StringBuilder();
        sb.append("(function(n,p){p=p||{bubbles:false,cancelable:false,detail:undefined};var e;if(!window.CustomEvent){e=document.createEvent('CustomEvent');e.initCustomEvent(n,p.bubbles,p.cancelable,p.detail);}else{e=new CustomEvent(n,p);}window.dispatchEvent(e);})(").append("'").append(str).append("'").append(",{'detail':").append(new com.p003if.p004do.ChoiBounge().m14078(obj)).append("})");
        a(sb.toString());
    }

    public void b() {
        this.c = true;
    }

    public boolean c() {
        return this.b;
    }

    public boolean d() {
        return this.c;
    }

    public void e() {
        destroy();
    }

    public String getName() {
        return this.a;
    }

    public void goBack() {
        if (canGoBack()) {
            SaishuKusanagi.a("goBack", getUrl());
            if (this.k != null) {
                this.k.a(this);
            }
        }
        super.goBack();
    }

    public void loadUrl(String str) {
        super.loadUrl(str);
        if (this.f != null) {
            this.f.a(this, str);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        invalidate();
        super.onMeasure(i2, i3);
    }

    public void setOnClickViewListener(KyoKusanagi.C0051KyoKusanagi kyoKusanagi) {
        this.i = kyoKusanagi;
    }

    public void setOnGoBackListener(KyoKusanagi kyoKusanagi) {
        this.k = kyoKusanagi;
    }

    public void setOnInterceptRequestListener(BenimaruNikaido benimaruNikaido) {
        this.j = benimaruNikaido;
    }

    public void setOnLoadUrlListener(C0050GoroDaimon goroDaimon) {
        this.f = goroDaimon;
    }

    public void setOnOverrideUrlListener(ChinGentsai chinGentsai) {
        this.e = chinGentsai;
    }

    public void setOnPageFinishedListener(ChangKoehan changKoehan) {
        this.h = changKoehan;
    }

    public void setOnPageStartedListener(ChoiBounge choiBounge) {
        this.g = choiBounge;
    }
}
