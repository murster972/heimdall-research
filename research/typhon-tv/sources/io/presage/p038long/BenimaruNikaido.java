package io.presage.p038long;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import io.presage.formats.multiwebviews.video.AspectRatioFrameLayout;
import io.presage.formats.multiwebviews.video.KyoKusanagi;
import io.presage.model.Zone;
import io.presage.p034goto.Bao;
import io.presage.p034goto.ChangKoehan;
import io.presage.p038long.KyoKusanagi;

/* renamed from: io.presage.long.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido extends FrameLayout implements MediaPlayer.OnVideoSizeChangedListener, KyoKusanagi {
    private KyoKusanagi a;
    private String b;
    private AspectRatioFrameLayout c;
    private SurfaceView d;
    private int e;
    /* access modifiers changed from: private */
    public KyoKusanagi.C0051KyoKusanagi f;
    private View.OnTouchListener g = new ChangKoehan(new ChangKoehan.KyoKusanagi() {
        public void a(View view) {
            if (BenimaruNikaido.this.f != null) {
                BenimaruNikaido.this.f.a((KyoKusanagi) view);
            }
        }
    }, 2);

    private BenimaruNikaido(Context context) {
        super(context);
    }

    @TargetApi(5)
    public static BenimaruNikaido a(Context context, Zone zone) {
        BenimaruNikaido benimaruNikaido = new BenimaruNikaido(context.getApplicationContext());
        String background = zone.getBackground();
        if (background != null) {
            benimaruNikaido.e = Color.parseColor(background);
        } else {
            benimaruNikaido.e = -16777216;
        }
        benimaruNikaido.setBackgroundColor(benimaruNikaido.e);
        benimaruNikaido.setName(zone.getName());
        benimaruNikaido.d = new SurfaceView(context.getApplicationContext());
        benimaruNikaido.a = new io.presage.formats.multiwebviews.video.KyoKusanagi(context.getApplicationContext(), zone.getUrl(), zone.isVideoAutoPlay(), zone.isVideoMuted(), benimaruNikaido.d);
        benimaruNikaido.a.a((MediaPlayer.OnVideoSizeChangedListener) benimaruNikaido);
        benimaruNikaido.c = new AspectRatioFrameLayout(context.getApplicationContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        benimaruNikaido.c.addView(benimaruNikaido.d);
        benimaruNikaido.addView(benimaruNikaido.c, layoutParams);
        benimaruNikaido.setLayoutParams(Bao.b(context, zone));
        benimaruNikaido.setOnTouchListener(benimaruNikaido.g);
        return benimaruNikaido;
    }

    public String getName() {
        return this.b;
    }

    public io.presage.formats.multiwebviews.video.KyoKusanagi getVideoController() {
        return this.a;
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        float f2 = (float) (this.c.getLayoutParams().width / this.c.getLayoutParams().height);
        if (this.c != null) {
            this.c.setAspectRatio(i2 == 0 ? 1.0f : (f2 * ((float) i)) / ((float) i2));
        }
    }

    public void setName(String str) {
        this.b = str;
    }

    public void setOnClickViewListener(KyoKusanagi.C0051KyoKusanagi kyoKusanagi) {
        this.f = kyoKusanagi;
    }
}
