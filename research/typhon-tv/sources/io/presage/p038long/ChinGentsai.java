package io.presage.p038long;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/* renamed from: io.presage.long.ChinGentsai  reason: invalid package */
public class ChinGentsai extends FrameLayout {
    private ProgressBar a;
    private HashMap<String, KyoKusanagi> b = new HashMap<>();
    private HashSet<String> c = new HashSet<>();
    private KyoKusanagi d;

    /* renamed from: io.presage.long.ChinGentsai$KyoKusanagi */
    public interface KyoKusanagi {
        void a();
    }

    public ChinGentsai(Context context) {
        super(context);
    }

    public KyoKusanagi a(String str) {
        return this.b.get(str);
    }

    public void a() {
        if (this.a == null) {
            this.a = new ProgressBar(getContext());
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            layoutParams.gravity = 17;
            addView(this.a, 0, layoutParams);
        }
    }

    public void a(KyoKusanagi kyoKusanagi) {
        addView((View) kyoKusanagi);
        this.b.put(kyoKusanagi.getName(), kyoKusanagi);
        if (kyoKusanagi instanceof GoroDaimon) {
            this.c.add(kyoKusanagi.getName());
        }
    }

    public void b() {
        if (this.a != null) {
            removeView(this.a);
            this.a = null;
        }
    }

    public void b(String str) {
        removeView((View) this.b.remove(str));
        this.c.remove(str);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (this.d != null && 4 == keyEvent.getKeyCode() && 1 == keyEvent.getAction()) {
            this.d.a();
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public Set<String> getManagedViewNames() {
        return this.b.keySet();
    }

    public HashSet<String> getManagedWebViewNames() {
        return this.c;
    }

    public void setOnBackListener(KyoKusanagi kyoKusanagi) {
        this.d = kyoKusanagi;
    }
}
