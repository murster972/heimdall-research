package io.presage.p034goto;

import android.content.Context;
import com.p003if.p004do.BrianBattler;
import com.p003if.p004do.SaishuKusanagi;
import io.presage.p029char.BenimaruNikaido;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

/* renamed from: io.presage.goto.ChinGentsai  reason: invalid package */
public class ChinGentsai {
    public static final String a = ChinGentsai.class.getSimpleName();
    static String b = "/presage/cached/";
    public static String c = "_locked";
    private final String d = InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
    private String e = null;
    private Context f;

    public ChinGentsai(Context context, String str) {
        this.f = context;
        this.e = str;
    }

    private void a(File file) {
        if (file.isDirectory() && file.listFiles() != null) {
            for (File a2 : file.listFiles()) {
                a(a2);
            }
        }
        file.delete();
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.io.File r6, java.io.File r7) throws java.io.IOException {
        /*
            r4 = 0
            java.io.File r0 = r7.getParentFile()
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0012
            java.io.File r0 = r7.getParentFile()
            r0.mkdirs()
        L_0x0012:
            boolean r0 = r7.exists()
            if (r0 != 0) goto L_0x001b
            r7.createNewFile()
        L_0x001b:
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x0041 }
            r0.<init>(r6)     // Catch:{ all -> 0x0041 }
            java.nio.channels.FileChannel r1 = r0.getChannel()     // Catch:{ all -> 0x0041 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ all -> 0x0050 }
            r0.<init>(r7)     // Catch:{ all -> 0x0050 }
            java.nio.channels.FileChannel r0 = r0.getChannel()     // Catch:{ all -> 0x0050 }
            r2 = 0
            long r4 = r1.size()     // Catch:{ all -> 0x0054 }
            r0.transferFrom(r1, r2, r4)     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x003b
            r1.close()
        L_0x003b:
            if (r0 == 0) goto L_0x0040
            r0.close()
        L_0x0040:
            return
        L_0x0041:
            r0 = move-exception
            r2 = r0
            r3 = r4
            r1 = r4
        L_0x0045:
            if (r1 == 0) goto L_0x004a
            r1.close()
        L_0x004a:
            if (r3 == 0) goto L_0x004f
            r3.close()
        L_0x004f:
            throw r2
        L_0x0050:
            r0 = move-exception
            r2 = r0
            r3 = r4
            goto L_0x0045
        L_0x0054:
            r2 = move-exception
            r3 = r0
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.ChinGentsai.a(java.io.File, java.io.File):void");
    }

    private String[] a(File[] fileArr) {
        String[] strArr;
        int length = fileArr.length;
        int i = 0;
        String[] strArr2 = new String[0];
        while (i < length) {
            File file = fileArr[i];
            if (file.isDirectory()) {
                String[] a2 = a(file.listFiles());
                strArr = new String[(strArr2.length + a2.length)];
                System.arraycopy(strArr2, 0, strArr, 0, strArr2.length);
                System.arraycopy(a2, 0, strArr, strArr2.length, a2.length);
            } else {
                strArr = (String[]) Arrays.copyOf(strArr2, strArr2.length + 1);
                strArr[strArr.length - 1] = file.getAbsolutePath();
            }
            i++;
            strArr2 = strArr;
        }
        return strArr2;
    }

    private static void b(String str, String str2) {
        try {
            File file = new File(str);
            File file2 = new File(str2, file.getName());
            if (file.isDirectory()) {
                for (String file3 : file.list()) {
                    b(new File(file, file3).getPath(), file2.getPath());
                }
                return;
            }
            a(file, file2);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private String g(String str) {
        try {
            URL url = new URL(str);
            return url.getPath().substring(0, url.getPath().lastIndexOf(File.separator));
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private ArrayList<String> h(String str) {
        File file = new File(str);
        File[] listFiles = file.listFiles();
        ArrayList<String> arrayList = new ArrayList<>();
        if (listFiles == null) {
            file.mkdirs();
            SaishuKusanagi.a(a, file.getAbsolutePath() + " is NULL");
            return arrayList;
        }
        for (int i = 0; i < listFiles.length; i++) {
            if (!listFiles[i].getName().contains(c)) {
                arrayList.add(listFiles[i].getName());
            }
        }
        return arrayList;
    }

    public ArrayList<String> a() {
        return h(this.f.getFilesDir() + b);
    }

    public void a(String str) {
        File file = new File(this.f.getFilesDir() + b + this.e + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str);
        if (file.exists()) {
            try {
                a(file);
            } catch (Exception e2) {
                SaishuKusanagi.b(a, "deleteCampaign exception", e2);
            }
        }
    }

    public void a(String str, String str2) {
        b(this.f.getFilesDir() + b + str2 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str, this.f.getFilesDir() + b + this.e + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
    }

    public void a(ArrayList<String> arrayList) {
        File file = new File(this.f.getFilesDir() + b + this.e);
        if (file.exists()) {
            File[] listFiles = file.listFiles();
            for (File name : listFiles) {
                String name2 = name.getName();
                if (!name2.contains(c)) {
                    int i = 0;
                    while (i < arrayList.size() && !name2.equals(arrayList.get(i))) {
                        i++;
                    }
                    if (i == arrayList.size()) {
                        a(name2);
                    }
                }
            }
        }
    }

    public boolean a(String str, String[] strArr) {
        if (!b(str)) {
            return false;
        }
        for (String str2 : a(new File(this.f.getFilesDir().getPath() + b + this.e + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str).listFiles())) {
            boolean z = false;
            for (String contains : strArr) {
                if (str2.contains(contains)) {
                    z = true;
                }
            }
            if (!z) {
                a(str);
                return false;
            }
        }
        return true;
    }

    public BenimaruNikaido.C0043BenimaruNikaido[] a(BrianBattler brianBattler, String str, String str2, boolean z) {
        int i = 0;
        BenimaruNikaido.C0043BenimaruNikaido[] benimaruNikaidoArr = new BenimaruNikaido.C0043BenimaruNikaido[(brianBattler != null ? brianBattler.m14058() : 0)];
        if (brianBattler == null || brianBattler.m14058() == 0 || str == null || str.isEmpty() || str2 == null || str2.isEmpty()) {
            return benimaruNikaidoArr;
        }
        Iterator<SaishuKusanagi> it2 = brianBattler.iterator();
        while (it2.hasNext()) {
            SaishuKusanagi next = it2.next();
            try {
                if (next.m14146().isEmpty()) {
                    break;
                }
                URL url = new URL(next.m14146());
                int i2 = i + 1;
                try {
                    benimaruNikaidoArr[i] = new BenimaruNikaido.C0043BenimaruNikaido(next.m14146(), z ? this.f.getFilesDir() + b + this.e + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str + c + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str2 + url.getPath().substring(0, url.getPath().lastIndexOf(File.separator)) : this.f.getFilesDir() + b + this.e + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str2 + url.getPath().substring(0, url.getPath().lastIndexOf(File.separator)), url.getPath().substring(url.getPath().lastIndexOf(File.separator) + 1));
                    i = i2;
                } catch (MalformedURLException e2) {
                    e = e2;
                    i = i2;
                    e.printStackTrace();
                }
            } catch (MalformedURLException e3) {
                e = e3;
                e.printStackTrace();
            }
        }
        return benimaruNikaidoArr;
    }

    public String[] a(BrianBattler brianBattler) {
        if (brianBattler == null) {
            return new String[0];
        }
        String[] strArr = new String[brianBattler.m14058()];
        Iterator<SaishuKusanagi> it2 = brianBattler.iterator();
        int i = 0;
        while (it2.hasNext()) {
            strArr[i] = g(it2.next().m14146());
            i++;
        }
        return strArr;
    }

    public ArrayList<String> b() {
        return h(this.f.getFilesDir() + b + this.e);
    }

    public boolean b(String str) {
        File filesDir = this.f.getFilesDir();
        if (filesDir == null) {
            return false;
        }
        String path = filesDir.getPath();
        if (!path.isEmpty()) {
            return new File(path + b + this.e + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str).exists();
        }
        return false;
    }

    public String c(String str) {
        Iterator<String> it2 = a().iterator();
        while (it2.hasNext()) {
            String next = it2.next();
            Iterator<String> it3 = d(next).iterator();
            while (true) {
                if (it3.hasNext()) {
                    if (it3.next().equals(str)) {
                        return next;
                    }
                }
            }
        }
        return null;
    }

    public ArrayList<String> d(String str) {
        return h(this.f.getFilesDir() + b + str);
    }

    public void e(String str) {
        new File(this.f.getFilesDir() + b + this.e + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str + c + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR).renameTo(new File(this.f.getFilesDir() + b + this.e + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR));
    }

    public String f(String str) {
        File file;
        if (str == null || (file = new File(this.f.getFilesDir() + b + this.e + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) == null || !file.exists()) {
            return null;
        }
        File[] listFiles = file.listFiles();
        if (listFiles.length >= 1) {
            return listFiles[0].getName();
        }
        return null;
    }
}
