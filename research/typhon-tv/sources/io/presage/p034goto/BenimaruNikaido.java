package io.presage.p034goto;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: io.presage.goto.BenimaruNikaido  reason: invalid package */
public final class BenimaruNikaido {

    /* renamed from: io.presage.goto.BenimaruNikaido$BenimaruNikaido  reason: collision with other inner class name */
    private static final class C0048BenimaruNikaido implements ServiceConnection {
        boolean a;
        private final LinkedBlockingQueue<IBinder> b;

        private C0048BenimaruNikaido() {
            this.b = new LinkedBlockingQueue<>(1);
            this.a = false;
        }

        public IBinder a() throws InterruptedException {
            if (this.a) {
                throw new IllegalStateException();
            }
            this.a = true;
            return this.b.take();
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.b.put(iBinder);
            } catch (InterruptedException e) {
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }
    }

    /* renamed from: io.presage.goto.BenimaruNikaido$GoroDaimon */
    private static final class GoroDaimon implements IInterface {
        private IBinder a;

        public GoroDaimon(IBinder iBinder) {
            this.a = iBinder;
        }

        public String a() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.a.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readString();
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        public boolean a(boolean z) throws RemoteException {
            boolean z2 = true;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(z ? 1 : 0);
                this.a.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z2 = false;
                }
                return z2;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        public IBinder asBinder() {
            return this.a;
        }
    }

    /* renamed from: io.presage.goto.BenimaruNikaido$KyoKusanagi */
    public static final class KyoKusanagi {
        private final String a;
        private final boolean b;

        KyoKusanagi(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        public String a() {
            return this.a;
        }

        public boolean b() {
            return this.b;
        }
    }

    public static KyoKusanagi a(Context context) throws Exception {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("Cannot be called from the main thread");
        }
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            C0048BenimaruNikaido benimaruNikaido = new C0048BenimaruNikaido();
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            if (context.bindService(intent, benimaruNikaido, 1)) {
                try {
                    GoroDaimon goroDaimon = new GoroDaimon(benimaruNikaido.a());
                    KyoKusanagi kyoKusanagi = new KyoKusanagi(goroDaimon.a(), goroDaimon.a(true));
                    context.unbindService(benimaruNikaido);
                    return kyoKusanagi;
                } catch (Exception e) {
                    throw e;
                } catch (Throwable th) {
                    context.unbindService(benimaruNikaido);
                    throw th;
                }
            } else {
                throw new IOException("Google Play connection failed");
            }
        } catch (Exception e2) {
            throw e2;
        }
    }
}
