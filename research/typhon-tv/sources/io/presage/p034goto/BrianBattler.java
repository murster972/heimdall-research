package io.presage.p034goto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: io.presage.goto.BrianBattler  reason: invalid package */
public class BrianBattler {
    public static String a(String str) {
        return a(str, "MD5");
    }

    public static String a(String str, String str2) {
        try {
            byte[] digest = MessageDigest.getInstance(str2).digest(str.getBytes());
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                stringBuffer.append(Integer.toHexString((b & 255) | 256).substring(1, 3));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
