package io.presage.p034goto;

import android.app.Application;
import com.moat.analytics.mobile.ogury.MoatAdEvent;
import com.moat.analytics.mobile.ogury.MoatAdEventType;
import com.moat.analytics.mobile.ogury.MoatAnalytics;
import com.moat.analytics.mobile.ogury.MoatFactory;
import com.moat.analytics.mobile.ogury.MoatOptions;
import com.moat.analytics.mobile.ogury.NativeDisplayTracker;
import com.moat.analytics.mobile.ogury.ReactiveVideoTracker;
import com.moat.analytics.mobile.ogury.ReactiveVideoTrackerPlugin;
import io.presage.ads.NewAd;
import io.presage.model.Parameter;
import io.presage.p038long.ChinGentsai;
import io.presage.p038long.GoroDaimon;
import io.presage.provider.PresageProvider;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;

/* renamed from: io.presage.goto.LeonaHeidern  reason: invalid package */
public class LeonaHeidern {
    private ChinGentsai a;
    private NewAd b;
    private NativeDisplayTracker c;
    /* access modifiers changed from: private */
    public ReactiveVideoTracker d;
    private String e;
    private boolean f = false;
    private boolean g = false;
    private boolean h = false;
    private boolean i = false;
    /* access modifiers changed from: private */
    public int j;

    public LeonaHeidern(Application application, NewAd newAd, ChinGentsai chinGentsai) {
        if (application == null) {
            throw new NullPointerException("application is null");
        } else if (newAd == null) {
            throw new NullPointerException("ad is null");
        } else if (chinGentsai == null) {
            throw new NullPointerException("layout is null");
        } else {
            this.e = PresageProvider.b(chinGentsai.getContext().getApplicationContext());
            if (this.e == null) {
                throw new NullPointerException("apiKey is null");
            }
            this.a = chinGentsai;
            this.b = newAd;
            MoatOptions moatOptions = new MoatOptions();
            moatOptions.disableAdIdCollection = true;
            moatOptions.disableLocationServices = true;
            if (!"prod".equals("prod")) {
                moatOptions.loggingEnabled = true;
            }
            MoatAnalytics.getInstance().start(moatOptions, application);
        }
    }

    private void e() {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE, Integer.valueOf(LeonaHeidern.this.j)));
            }
        });
    }

    private void e(final int i2) {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_THIRD_QUARTILE, Integer.valueOf(i2)));
            }
        });
    }

    private void f(final int i2) {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_MID_POINT, Integer.valueOf(i2)));
            }
        });
    }

    private void g(final int i2) {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_FIRST_QUARTILE, Integer.valueOf(i2)));
            }
        });
    }

    public void a() {
        if (this.c != null) {
            this.c.stopTracking();
            this.c = null;
        }
        if (this.d != null) {
            this.d.stopTracking();
            this.d = null;
        }
    }

    public void a(final int i2) {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_PLAYING, Integer.valueOf(i2)));
            }
        });
    }

    public void a(GoroDaimon goroDaimon) {
        if (goroDaimon == null) {
            SaishuKusanagi.d("MOAT", "managedWebView is null");
            return;
        }
        MoatFactory create = MoatFactory.create();
        HashMap hashMap = new HashMap();
        SaishuKusanagi.a("MOAT", goroDaimon.getName());
        if (goroDaimon.getName().equals("controller")) {
            hashMap.put("moatClientLevel1", this.b.getCampaignId());
            hashMap.put("moatClientSlicer1", this.e);
            MoatAnalytics.getInstance().prepareNativeDisplayTracking("oguryinappdisplay404420442379");
            SaishuKusanagi.a("MOAT", goroDaimon.getName() + StringUtils.SPACE + goroDaimon.getRootView().getWidth() + StringUtils.SPACE + goroDaimon.getRootView().getHeight());
            this.c = create.createNativeDisplayTracker(goroDaimon.getRootView(), hashMap);
            if (this.c != null) {
                this.c.startTracking();
            } else {
                SaishuKusanagi.d("MOAT", "Error while starting tracking");
            }
        }
        if (goroDaimon.getName().equals("video-overlay")) {
            hashMap.put("level1", this.b.getCampaignId());
            hashMap.put("slicer1", this.e);
            SaishuKusanagi.a("MOAT", goroDaimon.getName() + StringUtils.SPACE + goroDaimon.getRootView().getWidth() + StringUtils.SPACE + goroDaimon.getRootView().getHeight());
            this.d = (ReactiveVideoTracker) create.createCustomTracker(new ReactiveVideoTrackerPlugin("oguryinappvideo193664102952"));
            Parameter parameter = this.b.getParameter("completed_video_duration");
            if (parameter != null) {
                this.j = ((int) ((Double) parameter.get()).doubleValue()) * 1000;
            } else {
                this.j = 10000;
            }
            this.d.trackVideoAd(hashMap, Integer.valueOf(this.j), goroDaimon);
            c();
        }
    }

    public void b() {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_START, 0));
            }
        });
    }

    public void b(final int i2) {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_PAUSED, Integer.valueOf(i2)));
            }
        });
    }

    public void c() {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.setPlayerVolume(MoatAdEvent.VOLUME_MUTED);
            }
        });
    }

    public void c(final int i2) {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_STOPPED, Integer.valueOf(i2)));
            }
        });
    }

    public void d() {
        this.a.post(new Runnable() {
            public void run() {
                LeonaHeidern.this.d.setPlayerVolume(MoatAdEvent.VOLUME_UNMUTED);
            }
        });
    }

    public void d(int i2) {
        if (i2 > this.j / 4 && !this.f) {
            this.f = true;
            g(i2);
        }
        if (i2 > (this.j / 4) * 2 && !this.g) {
            this.g = true;
            f(i2);
        }
        if (i2 > (this.j / 4) * 3 && !this.h) {
            this.h = true;
            e(i2);
        }
        if (i2 > this.j && !this.i) {
            this.i = true;
            e();
        }
    }
}
