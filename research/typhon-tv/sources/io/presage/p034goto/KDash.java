package io.presage.p034goto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.io.IOException;

/* renamed from: io.presage.goto.KDash  reason: invalid package */
public class KDash {
    private static Bitmap a(Context context, String str) {
        int i;
        int i2;
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
            i = displayMetrics.densityDpi;
        } catch (Exception e) {
            i = context.getResources().getDisplayMetrics().densityDpi;
        }
        switch (i) {
            case 120:
                i2 = 36;
                break;
            case 160:
                i2 = 48;
                break;
            case 240:
                i2 = 72;
                break;
            case 320:
                i2 = 96;
                break;
            case 480:
                i2 = 144;
                break;
            default:
                i2 = PsExtractor.AUDIO_STREAM;
                break;
        }
        return Whip.a(str, i2, i2);
    }

    private static void a(Context context, Intent intent, String str) {
        Intent intent2 = new Intent();
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        intent2.putExtra("android.intent.extra.shortcut.NAME", str);
        intent2.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        context.sendBroadcast(intent2);
    }

    private static void a(Context context, Intent intent, String str, String str2) throws IOException {
        Bitmap a = a(context, str2);
        if (a == null) {
            throw new IOException("Unable to get the bitmap from the url " + str2);
        }
        Intent intent2 = new Intent();
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        intent2.putExtra("android.intent.extra.shortcut.NAME", str);
        intent2.putExtra("android.intent.extra.shortcut.ICON", a);
        intent2.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        context.sendBroadcast(intent2);
    }

    public static void a(Context context, Class<? extends Activity> cls, String str) {
        SaishuKusanagi.b("Shortcut", String.format("Uninstalling %s shortcut.", new Object[]{str}));
        Intent intent = new Intent(context, cls);
        intent.setAction("android.intent.action.MAIN");
        a(context, intent, str);
    }

    public static void a(Context context, Class<? extends Activity> cls, String str, String str2, Bundle bundle) throws IOException {
        SaishuKusanagi.b("Shortcut", String.format("Installing %s shortcut.", new Object[]{str}));
        Intent intent = new Intent(context, cls);
        intent.addFlags(268435456);
        intent.addFlags(32768);
        intent.setAction("android.intent.action.MAIN");
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        a(context, intent, str, str2);
    }

    public static void a(Context context, String str, String str2) {
        SaishuKusanagi.b("Shortcut", String.format("Uninstalling %s shortcut.", new Object[]{str2}));
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        a(context, intent, str2);
    }

    public static void a(Context context, String str, String str2, String str3) throws IOException {
        a(context, new Intent("android.intent.action.VIEW", Uri.parse(str)), str2, str3);
    }
}
