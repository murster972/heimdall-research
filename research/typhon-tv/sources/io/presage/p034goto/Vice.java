package io.presage.p034goto;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@TargetApi(11)
/* renamed from: io.presage.goto.Vice  reason: invalid package */
public class Vice implements Set<String> {
    private Context a;
    private String b;

    public Vice(Context context, String str) {
        this.a = context;
        this.b = str;
    }

    /* renamed from: a */
    public boolean add(String str) {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("PERSISTED_SETS", 0);
        Set stringSet = sharedPreferences.getStringSet(this.b, (Set) null);
        if (stringSet == null) {
            stringSet = new HashSet();
        }
        stringSet.add(str);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putStringSet(this.b, stringSet);
        edit.commit();
        return true;
    }

    public boolean addAll(Collection<? extends String> collection) {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("PERSISTED_SETS", 0);
        Set stringSet = sharedPreferences.getStringSet(this.b, (Set) null);
        if (stringSet == null) {
            stringSet = new HashSet();
        }
        stringSet.addAll(collection);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putStringSet(this.b, stringSet);
        edit.commit();
        return true;
    }

    public void clear() {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("PERSISTED_SETS", 0).edit();
        edit.putStringSet(this.b, (Set) null);
        edit.commit();
    }

    public boolean contains(Object obj) {
        Set<String> stringSet = this.a.getSharedPreferences("PERSISTED_SETS", 0).getStringSet(this.b, (Set) null);
        if (stringSet == null) {
            return false;
        }
        return stringSet.contains(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        Set<String> stringSet = this.a.getSharedPreferences("PERSISTED_SETS", 0).getStringSet(this.b, (Set) null);
        if (stringSet == null) {
            return false;
        }
        return stringSet.containsAll(collection);
    }

    public boolean isEmpty() {
        Set<String> stringSet = this.a.getSharedPreferences("PERSISTED_SETS", 0).getStringSet(this.b, (Set) null);
        if (stringSet == null) {
            return true;
        }
        return stringSet.isEmpty();
    }

    public Iterator<String> iterator() {
        Set<String> stringSet = this.a.getSharedPreferences("PERSISTED_SETS", 0).getStringSet(this.b, (Set) null);
        if (stringSet == null) {
            return null;
        }
        return stringSet.iterator();
    }

    public boolean remove(Object obj) {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("PERSISTED_SETS", 0);
        Set<String> stringSet = sharedPreferences.getStringSet(this.b, (Set) null);
        if (stringSet == null || !stringSet.remove(obj)) {
            return false;
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putStringSet(this.b, stringSet);
        edit.commit();
        return true;
    }

    public boolean removeAll(Collection<?> collection) {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("PERSISTED_SETS", 0);
        Set<String> stringSet = sharedPreferences.getStringSet(this.b, (Set) null);
        if (stringSet == null || !stringSet.removeAll(collection)) {
            return false;
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putStringSet(this.b, stringSet);
        edit.commit();
        return true;
    }

    public boolean retainAll(Collection<?> collection) {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("PERSISTED_SETS", 0);
        Set<String> stringSet = sharedPreferences.getStringSet(this.b, (Set) null);
        if (stringSet == null || !stringSet.retainAll(collection)) {
            return false;
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putStringSet(this.b, stringSet);
        edit.commit();
        return true;
    }

    public int size() {
        Set<String> stringSet = this.a.getSharedPreferences("PERSISTED_SETS", 0).getStringSet(this.b, (Set) null);
        if (stringSet == null) {
            return 0;
        }
        return stringSet.size();
    }

    public Object[] toArray() {
        Set<String> stringSet = this.a.getSharedPreferences("PERSISTED_SETS", 0).getStringSet(this.b, (Set) null);
        return stringSet == null ? new Object[0] : stringSet.toArray();
    }

    public <T> T[] toArray(T[] tArr) {
        Set<String> stringSet = this.a.getSharedPreferences("PERSISTED_SETS", 0).getStringSet(this.b, (Set) null);
        if (stringSet == null) {
            return null;
        }
        return stringSet.toArray(tArr);
    }
}
