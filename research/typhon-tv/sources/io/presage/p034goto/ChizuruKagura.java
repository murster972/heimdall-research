package io.presage.p034goto;

import android.annotation.TargetApi;
import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@TargetApi(9)
/* renamed from: io.presage.goto.ChizuruKagura  reason: invalid package */
public class ChizuruKagura implements Executor {
    private static final BlockingQueue<Runnable> c = new LinkedBlockingQueue(128);
    private static final ThreadFactory d = new ThreadFactory() {
        private final AtomicInteger a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "PresageExecutor #" + this.a.getAndIncrement());
        }
    };
    private static final ThreadPoolExecutor e = new ThreadPoolExecutor(2, 2, 6, TimeUnit.SECONDS, c, d);
    private static ChizuruKagura f;
    final ArrayDeque<Runnable> a = new ArrayDeque<>();
    Runnable b;

    private ChizuruKagura() {
    }

    public static ChizuruKagura a() {
        if (f == null) {
            f = new ChizuruKagura();
            e.allowCoreThreadTimeOut(true);
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public synchronized void b() {
        Runnable poll = this.a.poll();
        this.b = poll;
        if (poll != null) {
            e.execute(this.b);
        }
    }

    public synchronized void execute(final Runnable runnable) {
        this.a.offer(new Runnable() {
            public void run() {
                try {
                    runnable.run();
                } finally {
                    ChizuruKagura.this.b();
                }
            }
        });
        if (this.b == null) {
            b();
        }
    }
}
