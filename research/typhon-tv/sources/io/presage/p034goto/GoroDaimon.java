package io.presage.p034goto;

import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.p029char.ChangKoehan;
import io.presage.p029char.ChinGentsai;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: io.presage.goto.GoroDaimon  reason: invalid package */
public class GoroDaimon {
    private static ConcurrentHashMap<String, ArrayList<BenimaruNikaido>> a = new ConcurrentHashMap<>();

    /* renamed from: io.presage.goto.GoroDaimon$BenimaruNikaido */
    public static class BenimaruNikaido {
        long a;
        KyoKusanagi b;
        boolean c;
        String d;

        public BenimaruNikaido(KyoKusanagi kyoKusanagi, String str) {
            this.a = 0;
            this.b = null;
            this.c = false;
            this.a = System.currentTimeMillis();
            this.b = kyoKusanagi;
            this.d = str;
        }

        public BenimaruNikaido(boolean z, String str) {
            this.a = 0;
            this.b = null;
            this.c = false;
            this.a = System.currentTimeMillis();
            this.b = KyoKusanagi.CAN_SHOW;
            this.c = z;
            this.d = str;
        }

        public String toString() {
            return "[" + this.a + "][" + this.b.name() + "][" + this.d + "]" + this.c;
        }
    }

    /* renamed from: io.presage.goto.GoroDaimon$KyoKusanagi */
    public enum KyoKusanagi {
        LOAD,
        CAN_SHOW,
        SHOW
    }

    public static synchronized void a() {
        synchronized (GoroDaimon.class) {
            if (ChangKoehan.a().j()) {
                if (a.size() != 0) {
                    try {
                        JSONObject c = c();
                        ChangKoehan.a().k().a(ChangKoehan.a().k().d("precache_log"), ChangKoehan.a().k().b().c(), 1, c.toString(), (ChinGentsai) null, (io.presage.p029char.p030do.KyoKusanagi) null);
                        b();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return;
    }

    public static void a(BenimaruNikaido benimaruNikaido) {
        if (ChangKoehan.a().j() && a != null && benimaruNikaido != null && benimaruNikaido.d != null) {
            if (a.containsKey(benimaruNikaido.d)) {
                ArrayList arrayList = a.get(benimaruNikaido.d);
                if (arrayList != null && arrayList.size() <= 20) {
                    if (arrayList.size() != 0) {
                        if (benimaruNikaido.b == KyoKusanagi.CAN_SHOW) {
                            if (((BenimaruNikaido) arrayList.get(arrayList.size() - 1)).b == KyoKusanagi.CAN_SHOW && benimaruNikaido.c == ((BenimaruNikaido) arrayList.get(arrayList.size() - 1)).c) {
                                return;
                            }
                        } else if (benimaruNikaido.b != KyoKusanagi.SHOW && benimaruNikaido.b == ((BenimaruNikaido) arrayList.get(arrayList.size() - 1)).b) {
                            return;
                        }
                    }
                    arrayList.add(benimaruNikaido);
                    return;
                }
                return;
            }
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(benimaruNikaido);
            a.put(benimaruNikaido.d, arrayList2);
        }
    }

    private static void b() {
        a = new ConcurrentHashMap<>();
    }

    private static JSONObject c() throws JSONException {
        JSONObject a2 = ChangKoehan.a().k().b().a();
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry<String, ArrayList<BenimaruNikaido>> value : a.entrySet()) {
            Iterator it2 = ((ArrayList) value.getValue()).iterator();
            while (it2.hasNext()) {
                BenimaruNikaido benimaruNikaido = (BenimaruNikaido) it2.next();
                if (benimaruNikaido != null) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put(VastExtensionXmlManager.TYPE, (Object) benimaruNikaido.b.name());
                    if (benimaruNikaido.b == KyoKusanagi.CAN_SHOW) {
                        jSONObject.put("return_canshow", benimaruNikaido.c);
                    }
                    if (benimaruNikaido.d != null) {
                        jSONObject.put("ad_unit_id", (Object) benimaruNikaido.d);
                    }
                    jSONObject.put("timestamp", System.currentTimeMillis() - benimaruNikaido.a);
                    jSONArray.put((Object) jSONObject);
                }
            }
        }
        a2.put("content", (Object) jSONArray);
        return a2;
    }
}
