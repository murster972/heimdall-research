package io.presage.p034goto;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import io.presage.BenimaruNikaido;
import io.presage.finder.model.IpTracker;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/* renamed from: io.presage.goto.ShingoYabuki  reason: invalid package */
public class ShingoYabuki {
    public static final String a = ShingoYabuki.class.getSimpleName();

    public static Application a(Context context) {
        if (context == null) {
            return null;
        }
        if (context instanceof Activity) {
            SaishuKusanagi.a(a, "the context is an activity");
            return ((Activity) context).getApplication();
        } else if (context instanceof Application) {
            SaishuKusanagi.a(a, "the context is an application");
            return (Application) context;
        } else if (context instanceof Service) {
            SaishuKusanagi.a(a, "the context is a service");
            return ((Service) context).getApplication();
        } else {
            SaishuKusanagi.a(a, "the context is of type unknown");
            return null;
        }
    }

    public static boolean a(Context context, BenimaruNikaido benimaruNikaido, Bundle bundle) {
        if (context == null || benimaruNikaido == null || bundle == null) {
            return false;
        }
        String packageName = context.getPackageName();
        String string = bundle.getString("package");
        if (string == null || !string.equalsIgnoreCase(packageName)) {
            return false;
        }
        IpTracker ipTracker = new IpTracker(Long.valueOf(bundle.getLong("ipTrackerTimestamp")), bundle.getString("ipTrackerType"));
        benimaruNikaido.a(ipTracker);
        return !ipTracker.c().equals("No_Connection");
    }

    public static boolean a(Context context, boolean z) {
        if (context == null) {
            throw new IllegalArgumentException("context is null");
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String format = simpleDateFormat.format(new Date());
        try {
            Date parse = simpleDateFormat.parse(format);
            SharedPreferences sharedPreferences = context.getSharedPreferences("presage", 0);
            String string = sharedPreferences.getString("profig_counter", (String) null);
            if (string != null) {
                String[] split = string.split(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                if (split != null && split.length == 2) {
                    try {
                        Date parse2 = simpleDateFormat.parse(split[0]);
                        SaishuKusanagi.b(a, "simple today " + parse);
                        SaishuKusanagi.b(a, "profig saved date " + parse2);
                        if (parse.equals(parse2)) {
                            try {
                                int parseInt = Integer.parseInt(split[1]);
                                if (parseInt >= 10) {
                                    return true;
                                }
                                if (!z) {
                                    return false;
                                }
                                sharedPreferences.edit().putString("profig_counter", String.format("%s_%d", new Object[]{format, Integer.valueOf(parseInt + 1)})).commit();
                                return false;
                            } catch (Exception e) {
                                SaishuKusanagi.b(a, "parse number error", e);
                                if (!z) {
                                    return false;
                                }
                                sharedPreferences.edit().putString("profig_counter", String.format("%s_%d", new Object[]{format, 1})).commit();
                                return false;
                            }
                        } else if (parse.after(parse2)) {
                            if (!z) {
                                return false;
                            }
                            sharedPreferences.edit().putString("profig_counter", String.format("%s_%d", new Object[]{format, 1})).commit();
                            return false;
                        } else if (!parse.before(parse2)) {
                            return false;
                        } else {
                            SaishuKusanagi.d(a, "time turns back, wired");
                            if (!z) {
                                return false;
                            }
                            sharedPreferences.edit().putString("profig_counter", String.format("%s_%d", new Object[]{format, 1})).commit();
                            return false;
                        }
                    } catch (ParseException e2) {
                        SaishuKusanagi.b(a, "parse date error", e2);
                        if (!z) {
                            return false;
                        }
                        sharedPreferences.edit().putString("profig_counter", String.format("%s_%d", new Object[]{format, 1})).commit();
                        return false;
                    }
                } else if (!z) {
                    return false;
                } else {
                    sharedPreferences.edit().putString("profig_counter", String.format("%s_%d", new Object[]{format, 1})).commit();
                    return false;
                }
            } else if (!z) {
                return false;
            } else {
                sharedPreferences.edit().putString("profig_counter", String.format("%s_%d", new Object[]{format, 1})).commit();
                return false;
            }
        } catch (ParseException e3) {
            SaishuKusanagi.d(a, "parse today error");
            return false;
        }
    }
}
