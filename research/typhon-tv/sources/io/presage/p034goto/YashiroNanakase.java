package io.presage.p034goto;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.DeadObjectException;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

/* renamed from: io.presage.goto.YashiroNanakase  reason: invalid package */
public class YashiroNanakase extends Thread {
    private static YashiroNanakase a = null;
    private PackageManager b = null;
    private Resources c = null;
    private ActivityManager d = null;
    private Drawable e = null;
    private boolean f = true;
    private final ConcurrentHashMap<String, KyoKusanagi> g = new ConcurrentHashMap<>();
    private final Semaphore h = new Semaphore(1, true);
    private final LinkedList<BenimaruNikaido> i = new LinkedList<>();

    /* renamed from: io.presage.goto.YashiroNanakase$BenimaruNikaido */
    private class BenimaruNikaido {
        public int a;
        public String b;
        public String c;

        public BenimaruNikaido(int i, String str, String str2) {
            this.a = i;
            this.b = str;
            this.c = str2;
        }
    }

    /* renamed from: io.presage.goto.YashiroNanakase$KyoKusanagi */
    private class KyoKusanagi {
        public String a;
        public Drawable b;

        private KyoKusanagi() {
        }
    }

    public static YashiroNanakase a(Context context) {
        if (a != null) {
            return a;
        }
        a = new YashiroNanakase();
        a.b = context.getPackageManager();
        a.c = context.getResources();
        a.d = (ActivityManager) context.getSystemService("activity");
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(new DisplayMetrics());
        a.start();
        return a;
    }

    public void a(int i2, String str, String str2) {
        try {
            this.h.acquire();
            this.i.add(new BenimaruNikaido(i2, str, str2));
            this.h.release();
            KyoKusanagi kyoKusanagi = new KyoKusanagi();
            kyoKusanagi.a = str2;
            kyoKusanagi.b = this.e;
            this.g.put(str2, kyoKusanagi);
        } catch (InterruptedException e2) {
        }
    }

    public boolean a() {
        PackageInfo packageInfo;
        String[] strArr;
        if (this.i.isEmpty()) {
            return false;
        }
        try {
            this.h.acquire();
            BenimaruNikaido remove = this.i.remove();
            this.h.release();
            String substring = (!remove.b.contains("system") || !remove.c.contains("system") || remove.c.contains(".") || remove.c.contains("osmcore")) ? remove.c.contains(":") ? remove.c.substring(0, remove.c.indexOf(":")) : remove.c : AbstractSpiCall.ANDROID_CLIENT_TYPE;
            try {
                packageInfo = this.b.getPackageInfo(substring, 0);
            } catch (Exception e2) {
                if (e2 instanceof DeadObjectException) {
                    return false;
                }
                packageInfo = null;
            }
            if (packageInfo == null && remove.a > 0) {
                try {
                    strArr = this.b.getPackagesForUid(remove.a);
                } catch (Exception e3) {
                    if (e3 instanceof DeadObjectException) {
                        return false;
                    }
                    strArr = null;
                }
                if (strArr != null) {
                    int i2 = 0;
                    while (i2 < strArr.length) {
                        if (strArr[i2] != null) {
                            try {
                                packageInfo = this.b.getPackageInfo(strArr[i2], 0);
                                i2 = strArr.length;
                            } catch (PackageManager.NameNotFoundException e4) {
                            } catch (Exception e5) {
                                return false;
                            }
                        }
                        i2++;
                    }
                }
            }
            KyoKusanagi kyoKusanagi = new KyoKusanagi();
            if (packageInfo != null) {
                try {
                    kyoKusanagi.a = packageInfo.applicationInfo.loadLabel(this.b).toString();
                } catch (Exception e6) {
                    return false;
                }
            } else {
                kyoKusanagi.a = substring;
                kyoKusanagi.b = this.e;
            }
            this.g.put(remove.c, kyoKusanagi);
            return true;
        } catch (InterruptedException e7) {
            return false;
        }
    }

    public boolean a(String str) {
        return this.g.get(str) != null;
    }

    public void run() {
        while (true) {
            if (!a()) {
                try {
                    sleep(500);
                } catch (InterruptedException e2) {
                }
            }
        }
    }
}
