package io.presage.p034goto;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import io.presage.provider.PresageProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: io.presage.goto.Mature  reason: invalid package */
public class Mature {
    public static final String a = Mature.class.getSimpleName();

    /* renamed from: io.presage.goto.Mature$KyoKusanagi */
    private static class KyoKusanagi {
        public String a;
        public long b;

        private KyoKusanagi() {
        }
    }

    public static ProviderInfo a(PackageInfo packageInfo) {
        ProviderInfo[] providerInfoArr = packageInfo.providers;
        if (providerInfoArr != null) {
            for (ProviderInfo providerInfo : providerInfoArr) {
                if (providerInfo.name.equals(PresageProvider.class.getCanonicalName())) {
                    return providerInfo;
                }
            }
        }
        return null;
    }

    public static ServiceInfo a(Context context) {
        List<ResolveInfo> a2;
        String str;
        if (context == null || context.getContentResolver() == null || (a2 = a(context, "io.presage.PresageService.PIVOT")) == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(context.getContentResolver());
        for (ResolveInfo next : a2) {
            try {
                ProviderInfo a3 = a(context.getPackageManager().getPackageInfo(next.serviceInfo.packageName, 8));
                Cursor a4 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.SDK_BUILD, a3.authority), (String[]) null, (String) null, (String[]) null, (String) null);
                Cursor a5 = a4 == null ? kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.SDK_BUILD, a3.authority), (String[]) null, (String) null, (String[]) null, (String) null) : a4;
                int i = -1;
                if (a5 != null) {
                    if (a5.moveToLast()) {
                        i = a5.getInt(a5.getColumnIndex("sdkbuild"));
                    }
                    a5.close();
                }
                int i2 = i;
                if (i2 == -1) {
                    SaishuKusanagi.d(a, "can not get sdk build version");
                } else if (i2 >= 100) {
                    String[] strArr = {"apikey"};
                    String str2 = "package = \"" + a3.packageName + "\"";
                    Cursor a6 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.APIKEY, a3.authority), strArr, str2, (String[]) null, (String) null);
                    Cursor a7 = a6 == null ? kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.APIKEY, a3.authority), strArr, str2, (String[]) null, (String) null) : a6;
                    if (a7 != null) {
                        str = a7.moveToLast() ? a7.getString(a7.getColumnIndex("apikey")) : null;
                        a7.close();
                    } else {
                        str = null;
                    }
                    if (str != null && !str.isEmpty()) {
                        try {
                            if (Integer.parseInt(str) == 0) {
                                SaishuKusanagi.d(a, "api key is 0");
                            } else {
                                Cursor a8 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.DATA_OPTIN_ABOVE_V_2_1, a3.authority), (String[]) null, (String) null, (String[]) null, (String) null);
                                Cursor a9 = a8 == null ? kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.DATA_OPTIN_ABOVE_V_2_1, a3.authority), (String[]) null, (String) null, (String[]) null, (String) null) : a8;
                                int i3 = 0;
                                if (a9 != null) {
                                    if (a9.moveToLast()) {
                                        i3 = a9.getInt(a9.getColumnIndex("v"));
                                    }
                                    a9.close();
                                }
                                if (i3 == 1) {
                                    if (!hashMap.containsKey(Integer.valueOf(i2))) {
                                        hashMap.put(Integer.valueOf(i2), new ArrayList());
                                    }
                                    ((List) hashMap.get(Integer.valueOf(i2))).add(next.serviceInfo);
                                }
                                if (!hashMap2.containsKey(Integer.valueOf(i2))) {
                                    hashMap2.put(Integer.valueOf(i2), new ArrayList());
                                }
                                ((List) hashMap2.get(Integer.valueOf(i2))).add(next.serviceInfo);
                            }
                        } catch (Exception e) {
                            SaishuKusanagi.d(a, "invalid api key");
                        }
                    }
                }
            } catch (Exception e2) {
                SaishuKusanagi.b(a, "the package name (" + next.serviceInfo.packageName + ") is not found or is dead", e2);
            }
        }
        if (!hashMap.isEmpty()) {
            return a((List<ServiceInfo>) (List) hashMap.get(Integer.valueOf(((Integer) Collections.max(hashMap.keySet())).intValue())));
        }
        if (hashMap2.isEmpty()) {
            SaishuKusanagi.a(a, "no app is with apikey :(");
            return null;
        }
        SaishuKusanagi.a(a, "none data optin");
        return a((List<ServiceInfo>) (List) hashMap2.get(Integer.valueOf(((Integer) Collections.max(hashMap2.keySet())).intValue())));
    }

    public static ServiceInfo a(List<ServiceInfo> list) {
        if (list == null) {
            return null;
        }
        Collections.sort(list, new Comparator<ServiceInfo>() {
            /* renamed from: a */
            public int compare(ServiceInfo serviceInfo, ServiceInfo serviceInfo2) {
                return serviceInfo.packageName.compareTo(serviceInfo2.packageName);
            }
        });
        return list.get(0);
    }

    public static List<ResolveInfo> a(Context context, String str) {
        try {
            return context.getPackageManager().queryIntentServices(new Intent(str), 0);
        } catch (RuntimeException e) {
            SaishuKusanagi.b(a, "can not query presage service", e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x029f A[SYNTHETIC, Splitter:B:106:0x029f] */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x001b A[EDGE_INSN: B:141:0x001b->B:135:0x001b ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0079 A[SYNTHETIC, Splitter:B:26:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a3 A[SYNTHETIC, Splitter:B:38:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00b5 A[SYNTHETIC, Splitter:B:46:0x00b5] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00c3 A[Catch:{ Exception -> 0x0082 }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f1 A[Catch:{ Exception -> 0x0082 }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x028b A[SYNTHETIC, Splitter:B:98:0x028b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r12, java.util.List<io.presage.parser.p040do.BenimaruNikaido> r13, java.lang.String r14) {
        /*
            r8 = -1
            r7 = 0
            if (r12 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            io.presage.provider.KyoKusanagi r0 = new io.presage.provider.KyoKusanagi
            android.content.ContentResolver r1 = r12.getContentResolver()
            r0.<init>(r1)
            java.lang.String r1 = "io.presage.PresageService.PIVOT"
            java.util.List r1 = a(r12, r1)
            if (r1 == 0) goto L_0x0004
            java.util.Iterator r10 = r1.iterator()
        L_0x001b:
            boolean r1 = r10.hasNext()
            if (r1 == 0) goto L_0x0004
            java.lang.Object r1 = r10.next()
            r6 = r1
            android.content.pm.ResolveInfo r6 = (android.content.pm.ResolveInfo) r6
            android.content.pm.PackageManager r1 = r12.getPackageManager()     // Catch:{ Exception -> 0x0082 }
            android.content.pm.ServiceInfo r2 = r6.serviceInfo     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = r2.packageName     // Catch:{ Exception -> 0x0082 }
            r3 = 136(0x88, float:1.9E-43)
            android.content.pm.PackageInfo r1 = r1.getPackageInfo(r2, r3)     // Catch:{ Exception -> 0x0082 }
            android.content.pm.ProviderInfo r11 = a((android.content.pm.PackageInfo) r1)     // Catch:{ Exception -> 0x0082 }
            io.presage.provider.PresageProvider$KyoKusanagi r1 = io.presage.provider.PresageProvider.KyoKusanagi.SDK_BUILD     // Catch:{ Exception -> 0x0096, all -> 0x00b1 }
            java.lang.String r2 = r11.authority     // Catch:{ Exception -> 0x0096, all -> 0x00b1 }
            android.net.Uri r1 = io.presage.provider.PresageProvider.a(r1, r2)     // Catch:{ Exception -> 0x0096, all -> 0x00b1 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r9 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0096, all -> 0x00b1 }
            if (r9 != 0) goto L_0x02dd
            io.presage.provider.PresageProvider$KyoKusanagi r1 = io.presage.provider.PresageProvider.KyoKusanagi.SDK_BUILD     // Catch:{ Exception -> 0x02c2, all -> 0x02bb }
            java.lang.String r2 = r11.authority     // Catch:{ Exception -> 0x02c2, all -> 0x02bb }
            android.net.Uri r1 = io.presage.provider.PresageProvider.a(r1, r2)     // Catch:{ Exception -> 0x02c2, all -> 0x02bb }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r3 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x02c2, all -> 0x02bb }
        L_0x005c:
            if (r3 == 0) goto L_0x02da
            boolean r1 = r3.moveToLast()     // Catch:{ Exception -> 0x02c7 }
            if (r1 == 0) goto L_0x02d7
            java.lang.String r1 = "sdkbuild"
            int r1 = r3.getColumnIndex(r1)     // Catch:{ Exception -> 0x02c7 }
            int r2 = r3.getInt(r1)     // Catch:{ Exception -> 0x02c7 }
        L_0x006f:
            r3.close()     // Catch:{ Exception -> 0x02cb }
        L_0x0072:
            if (r3 == 0) goto L_0x0077
            r3.close()     // Catch:{ Exception -> 0x008c }
        L_0x0077:
            if (r2 != r8) goto L_0x00c3
            java.lang.String r1 = a     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "can not get sdk build version"
            io.presage.p034goto.SaishuKusanagi.d(r1, r2)     // Catch:{ Exception -> 0x0082 }
            goto L_0x001b
        L_0x0082:
            r1 = move-exception
            java.lang.String r2 = a
            java.lang.String r3 = "error in updateCacheProfig"
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x001b
        L_0x008c:
            r1 = move-exception
            java.lang.String r3 = a     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r1)     // Catch:{ Exception -> 0x0082 }
            goto L_0x0077
        L_0x0096:
            r1 = move-exception
            r2 = r8
            r3 = r7
        L_0x0099:
            java.lang.String r4 = a     // Catch:{ all -> 0x02bf }
            java.lang.String r5 = "can not get sdk build version"
            io.presage.p034goto.SaishuKusanagi.b(r4, r5, r1)     // Catch:{ all -> 0x02bf }
            if (r3 == 0) goto L_0x0077
            r3.close()     // Catch:{ Exception -> 0x00a7 }
            goto L_0x0077
        L_0x00a7:
            r1 = move-exception
            java.lang.String r3 = a     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r1)     // Catch:{ Exception -> 0x0082 }
            goto L_0x0077
        L_0x00b1:
            r1 = move-exception
            r3 = r7
        L_0x00b3:
            if (r3 == 0) goto L_0x00b8
            r3.close()     // Catch:{ Exception -> 0x00b9 }
        L_0x00b8:
            throw r1     // Catch:{ Exception -> 0x0082 }
        L_0x00b9:
            r2 = move-exception
            java.lang.String r3 = a     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r2)     // Catch:{ Exception -> 0x0082 }
            goto L_0x00b8
        L_0x00c3:
            r1 = 100
            if (r2 >= r1) goto L_0x0212
            android.content.pm.ServiceInfo r1 = r6.serviceInfo     // Catch:{ Exception -> 0x0082 }
            java.lang.String r1 = r1.packageName     // Catch:{ Exception -> 0x0082 }
            android.os.Bundle r1 = b(r12, r1)     // Catch:{ Exception -> 0x0082 }
            if (r1 == 0) goto L_0x02d4
            java.lang.String r2 = "presage_key"
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x0082 }
            if (r1 == 0) goto L_0x02d4
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0082 }
        L_0x00de:
            r3 = r1
        L_0x00df:
            if (r3 == 0) goto L_0x001b
            boolean r1 = r3.isEmpty()     // Catch:{ Exception -> 0x0082 }
            if (r1 != 0) goto L_0x001b
            java.util.Iterator r2 = r13.iterator()     // Catch:{ Exception -> 0x0082 }
        L_0x00eb:
            boolean r1 = r2.hasNext()     // Catch:{ Exception -> 0x0082 }
            if (r1 == 0) goto L_0x001b
            java.lang.Object r1 = r2.next()     // Catch:{ Exception -> 0x0082 }
            io.presage.parser.do.BenimaruNikaido r1 = (io.presage.parser.p040do.BenimaruNikaido) r1     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = r1.b()     // Catch:{ Exception -> 0x0082 }
            boolean r4 = r4.equals(r3)     // Catch:{ Exception -> 0x0082 }
            if (r4 == 0) goto L_0x00eb
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0082 }
            r2.<init>()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "k"
            java.lang.String r4 = "apikey"
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "v"
            java.lang.String r4 = r1.b()     // Catch:{ Exception -> 0x0082 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0082 }
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.MY_PROFIG_APIKEY     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = r11.authority     // Catch:{ Exception -> 0x0082 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0082 }
            r4 = 0
            r5 = 0
            r0.a(r3, r2, r4, r5)     // Catch:{ Exception -> 0x0082 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0082 }
            r2.<init>()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "k"
            java.lang.String r4 = "data_optin_v21"
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "v"
            boolean r4 = r1.c()     // Catch:{ Exception -> 0x0082 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x0082 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0082 }
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.DATA_OPTIN_ABOVE_V_2_1     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = r11.authority     // Catch:{ Exception -> 0x0082 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0082 }
            r4 = 0
            r5 = 0
            r0.a(r3, r2, r4, r5)     // Catch:{ Exception -> 0x0082 }
            io.presage.parser.do.BenimaruNikaido$ChinGentsai r2 = r1.g()     // Catch:{ Exception -> 0x0082 }
            if (r2 == 0) goto L_0x018f
            io.presage.parser.do.BenimaruNikaido$ChinGentsai r2 = r1.g()     // Catch:{ Exception -> 0x0082 }
            float r2 = r2.a()     // Catch:{ Exception -> 0x0082 }
            r3 = 0
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x018f
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0082 }
            r2.<init>()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "k"
            java.lang.String r4 = "ads_timeout"
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "v"
            io.presage.parser.do.BenimaruNikaido$ChinGentsai r4 = r1.g()     // Catch:{ Exception -> 0x0082 }
            float r4 = r4.a()     // Catch:{ Exception -> 0x0082 }
            java.lang.Float r4 = java.lang.Float.valueOf(r4)     // Catch:{ Exception -> 0x0082 }
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0082 }
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.ADS_TIMEOUT     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = r11.authority     // Catch:{ Exception -> 0x0082 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0082 }
            r4 = 0
            r5 = 0
            r0.a(r3, r2, r4, r5)     // Catch:{ Exception -> 0x0082 }
        L_0x018f:
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0082 }
            r2.<init>()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "k"
            java.lang.String r4 = "raw_string"
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "v"
            r2.put(r3, r14)     // Catch:{ Exception -> 0x0082 }
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.PROFIG_JSON     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = r11.authority     // Catch:{ Exception -> 0x0082 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0082 }
            r4 = 0
            r5 = 0
            r0.a(r3, r2, r4, r5)     // Catch:{ Exception -> 0x0082 }
            java.util.List r2 = r1.d()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "launch"
            boolean r2 = r2.contains(r3)     // Catch:{ Exception -> 0x0082 }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Exception -> 0x0082 }
            r3.<init>()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = "k"
            java.lang.String r5 = "launch_optin"
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = "v"
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x0082 }
            r3.put(r4, r2)     // Catch:{ Exception -> 0x0082 }
            io.presage.provider.PresageProvider$KyoKusanagi r2 = io.presage.provider.PresageProvider.KyoKusanagi.LAUNCH_OPTIN     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = r11.authority     // Catch:{ Exception -> 0x0082 }
            android.net.Uri r2 = io.presage.provider.PresageProvider.a(r2, r4)     // Catch:{ Exception -> 0x0082 }
            r4 = 0
            r5 = 0
            r0.a(r2, r3, r4, r5)     // Catch:{ Exception -> 0x0082 }
            java.util.List r1 = r1.d()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "ads"
            boolean r1 = r1.contains(r2)     // Catch:{ Exception -> 0x0082 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0082 }
            r2.<init>()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "k"
            java.lang.String r4 = "ads_optin"
            r2.put(r3, r4)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "v"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0082 }
            r2.put(r3, r1)     // Catch:{ Exception -> 0x0082 }
            io.presage.provider.PresageProvider$KyoKusanagi r1 = io.presage.provider.PresageProvider.KyoKusanagi.ADS_OPTIN     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = r11.authority     // Catch:{ Exception -> 0x0082 }
            android.net.Uri r1 = io.presage.provider.PresageProvider.a(r1, r3)     // Catch:{ Exception -> 0x0082 }
            r3 = 0
            r4 = 0
            r0.a(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0082 }
            goto L_0x001b
        L_0x0212:
            r1 = 1
            java.lang.String[] r2 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0082 }
            r1 = 0
            java.lang.String r3 = "apikey"
            r2[r1] = r3     // Catch:{ Exception -> 0x0082 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0082 }
            r1.<init>()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "package = \""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = r11.packageName     // Catch:{ Exception -> 0x0082 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = "\""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r3 = r1.toString()     // Catch:{ Exception -> 0x0082 }
            io.presage.provider.PresageProvider$KyoKusanagi r1 = io.presage.provider.PresageProvider.KyoKusanagi.APIKEY     // Catch:{ Exception -> 0x027e, all -> 0x029b }
            java.lang.String r4 = r11.authority     // Catch:{ Exception -> 0x027e, all -> 0x029b }
            android.net.Uri r1 = io.presage.provider.PresageProvider.a(r1, r4)     // Catch:{ Exception -> 0x027e, all -> 0x029b }
            r4 = 0
            r5 = 0
            android.database.Cursor r6 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x027e, all -> 0x029b }
            if (r6 != 0) goto L_0x02d2
            io.presage.provider.PresageProvider$KyoKusanagi r1 = io.presage.provider.PresageProvider.KyoKusanagi.APIKEY     // Catch:{ Exception -> 0x02b2, all -> 0x02ad }
            java.lang.String r4 = r11.authority     // Catch:{ Exception -> 0x02b2, all -> 0x02ad }
            android.net.Uri r1 = io.presage.provider.PresageProvider.a(r1, r4)     // Catch:{ Exception -> 0x02b2, all -> 0x02ad }
            r4 = 0
            r5 = 0
            android.database.Cursor r2 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x02b2, all -> 0x02ad }
        L_0x0256:
            if (r2 == 0) goto L_0x02d0
            boolean r1 = r2.moveToLast()     // Catch:{ Exception -> 0x02b6 }
            if (r1 == 0) goto L_0x02ce
            java.lang.String r1 = "apikey"
            int r1 = r2.getColumnIndex(r1)     // Catch:{ Exception -> 0x02b6 }
            java.lang.String r3 = r2.getString(r1)     // Catch:{ Exception -> 0x02b6 }
        L_0x0269:
            r2.close()     // Catch:{ Exception -> 0x02b9 }
        L_0x026c:
            if (r2 == 0) goto L_0x00df
            r2.close()     // Catch:{ Exception -> 0x0273 }
            goto L_0x00df
        L_0x0273:
            r1 = move-exception
            java.lang.String r2 = a     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r2, r4, r1)     // Catch:{ Exception -> 0x0082 }
            goto L_0x00df
        L_0x027e:
            r1 = move-exception
            r2 = r7
            r3 = r7
        L_0x0281:
            java.lang.String r4 = a     // Catch:{ all -> 0x02b0 }
            java.lang.String r5 = "can not get api key "
            io.presage.p034goto.SaishuKusanagi.b(r4, r5, r1)     // Catch:{ all -> 0x02b0 }
            if (r2 == 0) goto L_0x00df
            r2.close()     // Catch:{ Exception -> 0x0290 }
            goto L_0x00df
        L_0x0290:
            r1 = move-exception
            java.lang.String r2 = a     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r2, r4, r1)     // Catch:{ Exception -> 0x0082 }
            goto L_0x00df
        L_0x029b:
            r1 = move-exception
            r2 = r7
        L_0x029d:
            if (r2 == 0) goto L_0x02a2
            r2.close()     // Catch:{ Exception -> 0x02a3 }
        L_0x02a2:
            throw r1     // Catch:{ Exception -> 0x0082 }
        L_0x02a3:
            r2 = move-exception
            java.lang.String r3 = a     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r2)     // Catch:{ Exception -> 0x0082 }
            goto L_0x02a2
        L_0x02ad:
            r1 = move-exception
            r2 = r6
            goto L_0x029d
        L_0x02b0:
            r1 = move-exception
            goto L_0x029d
        L_0x02b2:
            r1 = move-exception
            r2 = r6
            r3 = r7
            goto L_0x0281
        L_0x02b6:
            r1 = move-exception
            r3 = r7
            goto L_0x0281
        L_0x02b9:
            r1 = move-exception
            goto L_0x0281
        L_0x02bb:
            r1 = move-exception
            r3 = r9
            goto L_0x00b3
        L_0x02bf:
            r1 = move-exception
            goto L_0x00b3
        L_0x02c2:
            r1 = move-exception
            r2 = r8
            r3 = r9
            goto L_0x0099
        L_0x02c7:
            r1 = move-exception
            r2 = r8
            goto L_0x0099
        L_0x02cb:
            r1 = move-exception
            goto L_0x0099
        L_0x02ce:
            r3 = r7
            goto L_0x0269
        L_0x02d0:
            r3 = r7
            goto L_0x026c
        L_0x02d2:
            r2 = r6
            goto L_0x0256
        L_0x02d4:
            r1 = r7
            goto L_0x00de
        L_0x02d7:
            r2 = r8
            goto L_0x006f
        L_0x02da:
            r2 = r8
            goto L_0x0072
        L_0x02dd:
            r3 = r9
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.Mature.a(android.content.Context, java.util.List, java.lang.String):void");
    }

    public static Bundle b(Context context, String str) {
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(str, 128).metaData;
            return bundle == null ? new Bundle() : bundle;
        } catch (Exception e) {
            return new Bundle();
        }
    }

    public static String[] b(Context context) {
        if (context == null) {
            return null;
        }
        io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(context.getContentResolver());
        ArrayList arrayList = new ArrayList();
        Cursor a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.APIKEY, PresageProvider.a(context)), (String[]) null, (String) null, (String[]) null, (String) null);
        Cursor a3 = a2 == null ? kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.APIKEY, PresageProvider.a(context)), (String[]) null, (String) null, (String[]) null, (String) null) : a2;
        if (a3 != null) {
            while (a3.moveToNext()) {
                String string = a3.getString(a3.getColumnIndex("package"));
                String string2 = a3.getString(a3.getColumnIndex("apikey"));
                if (!(string == null || context.getPackageName() == null)) {
                    if (context.getPackageName().equals(string)) {
                        arrayList.add(0, string2);
                    } else {
                        arrayList.add(string2);
                    }
                }
            }
            a3.close();
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public static ActivityManager.RunningServiceInfo c(Context context, String str) {
        ActivityManager.RunningServiceInfo runningServiceInfo = null;
        if (str == null) {
            return null;
        }
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        ArrayList<ActivityManager.RunningServiceInfo> arrayList = new ArrayList<>();
        try {
            for (ActivityManager.RunningServiceInfo next : activityManager.getRunningServices(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT)) {
                if (str.equals(next.service.getClassName())) {
                    arrayList.add(next);
                }
            }
            if (arrayList.size() == 0) {
                return null;
            }
            if (arrayList.size() == 1) {
                return (ActivityManager.RunningServiceInfo) arrayList.get(0);
            }
            SaishuKusanagi.a(a, "there are more than one running service -> not good");
            ServiceInfo a2 = a(context);
            if (a2 == null) {
                return null;
            }
            ComponentName componentName = new ComponentName(a2.packageName, a2.name);
            for (ActivityManager.RunningServiceInfo runningServiceInfo2 : arrayList) {
                if (!runningServiceInfo2.service.equals(componentName)) {
                    Intent intent = new Intent();
                    intent.setComponent(runningServiceInfo2.service);
                    context.stopService(intent);
                    runningServiceInfo2 = runningServiceInfo;
                }
                runningServiceInfo = runningServiceInfo2;
            }
            return runningServiceInfo;
        } catch (Exception e) {
            SaishuKusanagi.b(a, e.getMessage(), e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x00e1 A[SYNTHETIC, Splitter:B:44:0x00e1] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String[] c(android.content.Context r14) {
        /*
            r1 = 0
            r8 = 0
            if (r14 != 0) goto L_0x0006
            r0 = r8
        L_0x0005:
            return r0
        L_0x0006:
            io.presage.provider.KyoKusanagi r0 = new io.presage.provider.KyoKusanagi
            android.content.ContentResolver r2 = r14.getContentResolver()
            r0.<init>(r2)
            java.lang.String r2 = "io.presage.PresageService.PIVOT"
            java.util.List r2 = a(r14, r2)
            if (r2 != 0) goto L_0x001a
            r0 = r8
            goto L_0x0005
        L_0x001a:
            int r3 = r2.size()
            java.lang.String[] r9 = new java.lang.String[r3]
            java.util.Iterator r11 = r2.iterator()
            r7 = r1
        L_0x0025:
            boolean r1 = r11.hasNext()
            if (r1 == 0) goto L_0x00ef
            java.lang.Object r1 = r11.next()
            r6 = r1
            android.content.pm.ResolveInfo r6 = (android.content.pm.ResolveInfo) r6
            android.content.pm.PackageManager r1 = r14.getPackageManager()     // Catch:{ Exception -> 0x00f7, all -> 0x00dd }
            android.content.pm.ServiceInfo r2 = r6.serviceInfo     // Catch:{ Exception -> 0x00f7, all -> 0x00dd }
            java.lang.String r2 = r2.packageName     // Catch:{ Exception -> 0x00f7, all -> 0x00dd }
            r3 = 8
            android.content.pm.PackageInfo r12 = r1.getPackageInfo(r2, r3)     // Catch:{ Exception -> 0x00f7, all -> 0x00dd }
            android.content.pm.ProviderInfo r13 = a((android.content.pm.PackageInfo) r12)     // Catch:{ Exception -> 0x00f7, all -> 0x00dd }
            io.presage.provider.PresageProvider$KyoKusanagi r1 = io.presage.provider.PresageProvider.KyoKusanagi.SDK_VERSION     // Catch:{ Exception -> 0x00f7, all -> 0x00dd }
            java.lang.String r2 = r13.authority     // Catch:{ Exception -> 0x00f7, all -> 0x00dd }
            android.net.Uri r1 = io.presage.provider.PresageProvider.a(r1, r2)     // Catch:{ Exception -> 0x00f7, all -> 0x00dd }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r10 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00f7, all -> 0x00dd }
            if (r10 != 0) goto L_0x0103
            io.presage.provider.PresageProvider$KyoKusanagi r1 = io.presage.provider.PresageProvider.KyoKusanagi.SDK_VERSION     // Catch:{ Exception -> 0x00fa, all -> 0x00f2 }
            java.lang.String r2 = r13.authority     // Catch:{ Exception -> 0x00fa, all -> 0x00f2 }
            android.net.Uri r1 = io.presage.provider.PresageProvider.a(r1, r2)     // Catch:{ Exception -> 0x00fa, all -> 0x00f2 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r2 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00fa, all -> 0x00f2 }
        L_0x0066:
            if (r2 == 0) goto L_0x0100
            boolean r1 = r2.moveToLast()     // Catch:{ Exception -> 0x009e }
            if (r1 == 0) goto L_0x00fd
            java.lang.String r1 = "sdkversion"
            int r1 = r2.getColumnIndex(r1)     // Catch:{ Exception -> 0x009e }
            java.lang.String r1 = r2.getString(r1)     // Catch:{ Exception -> 0x009e }
        L_0x0079:
            r2.close()     // Catch:{ Exception -> 0x009e }
        L_0x007c:
            java.lang.String r3 = r12.packageName     // Catch:{ Exception -> 0x009e }
            java.lang.String r4 = r14.getPackageName()     // Catch:{ Exception -> 0x009e }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x009e }
            if (r3 == 0) goto L_0x009b
            if (r7 == 0) goto L_0x009b
            r3 = 0
            r3 = r9[r3]     // Catch:{ Exception -> 0x009e }
            r4 = 0
            r9[r4] = r1     // Catch:{ Exception -> 0x009e }
            r9[r7] = r3     // Catch:{ Exception -> 0x009e }
        L_0x0092:
            if (r2 == 0) goto L_0x0097
            r2.close()     // Catch:{ Exception -> 0x00d3 }
        L_0x0097:
            int r1 = r7 + 1
            r7 = r1
            goto L_0x0025
        L_0x009b:
            r9[r7] = r1     // Catch:{ Exception -> 0x009e }
            goto L_0x0092
        L_0x009e:
            r1 = move-exception
        L_0x009f:
            java.lang.String r3 = a     // Catch:{ all -> 0x00f5 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f5 }
            r4.<init>()     // Catch:{ all -> 0x00f5 }
            java.lang.String r5 = "the package name ("
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00f5 }
            android.content.pm.ServiceInfo r5 = r6.serviceInfo     // Catch:{ all -> 0x00f5 }
            java.lang.String r5 = r5.packageName     // Catch:{ all -> 0x00f5 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00f5 }
            java.lang.String r5 = ") is not found or is dead"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00f5 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00f5 }
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r1)     // Catch:{ all -> 0x00f5 }
            if (r2 == 0) goto L_0x0097
            r2.close()     // Catch:{ Exception -> 0x00c9 }
            goto L_0x0097
        L_0x00c9:
            r1 = move-exception
            java.lang.String r2 = a
            java.lang.String r3 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x0097
        L_0x00d3:
            r1 = move-exception
            java.lang.String r2 = a
            java.lang.String r3 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x0097
        L_0x00dd:
            r0 = move-exception
            r2 = r8
        L_0x00df:
            if (r2 == 0) goto L_0x00e4
            r2.close()     // Catch:{ Exception -> 0x00e5 }
        L_0x00e4:
            throw r0
        L_0x00e5:
            r1 = move-exception
            java.lang.String r2 = a
            java.lang.String r3 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x00e4
        L_0x00ef:
            r0 = r9
            goto L_0x0005
        L_0x00f2:
            r0 = move-exception
            r2 = r10
            goto L_0x00df
        L_0x00f5:
            r0 = move-exception
            goto L_0x00df
        L_0x00f7:
            r1 = move-exception
            r2 = r8
            goto L_0x009f
        L_0x00fa:
            r1 = move-exception
            r2 = r10
            goto L_0x009f
        L_0x00fd:
            r1 = r8
            goto L_0x0079
        L_0x0100:
            r1 = r8
            goto L_0x007c
        L_0x0103:
            r2 = r10
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.Mature.c(android.content.Context):java.lang.String[]");
    }

    public static void d(Context context) {
        Map<String, ?> all;
        Uri a2;
        if (context != null) {
            io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(context.getContentResolver());
            List<ResolveInfo> a3 = a(context, "io.presage.PresageService.PIVOT");
            if (a3 != null) {
                for (ResolveInfo next : a3) {
                    try {
                        ProviderInfo a4 = a(context.getPackageManager().getPackageInfo(next.serviceInfo.packageName, 136));
                        if (!(a4 == null || (a2 = PresageProvider.a(PresageProvider.KyoKusanagi.PROFIG, a4.authority)) == null)) {
                            kyoKusanagi.a(a2, (String) null, (String[]) null);
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.b(a, "the package name (" + next.serviceInfo.packageName + ") is not found or is dead", e);
                    }
                }
            }
            SharedPreferences sharedPreferences = context.getSharedPreferences("presage", 0);
            if (sharedPreferences != null && (all = sharedPreferences.getAll()) != null) {
                for (Map.Entry<String, ?> key : all.entrySet()) {
                    String str = (String) key.getKey();
                    if (str != null && (str.startsWith("CS") || str.startsWith("timing"))) {
                        sharedPreferences.edit().remove(str);
                    }
                }
                sharedPreferences.edit().commit();
            }
        }
    }

    public static void d(Context context, String str) {
        if (context != null) {
            io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(context.getContentResolver());
            List<ResolveInfo> a2 = a(context, "io.presage.PresageService.PIVOT");
            if (a2 != null) {
                for (ResolveInfo next : a2) {
                    try {
                        ProviderInfo a3 = a(context.getPackageManager().getPackageInfo(next.serviceInfo.packageName, 136));
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("k", "aaid");
                        contentValues.put("v", str);
                        kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.AAID, a3.authority), contentValues, (String) null, (String[]) null);
                    } catch (Exception e) {
                        SaishuKusanagi.b(a, "the package name (" + next.serviceInfo.packageName + ") is not found or is dead", e);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d9, code lost:
        r3 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00da, code lost:
        r4 = -1;
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f4, code lost:
        r3 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x03ca A[SYNTHETIC, Splitter:B:174:0x03ca] */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x041a A[SYNTHETIC, Splitter:B:185:0x041a] */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x042c A[SYNTHETIC, Splitter:B:193:0x042c] */
    /* JADX WARNING: Removed duplicated region for block: B:259:0x0286 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0094 A[SYNTHETIC, Splitter:B:31:0x0094] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e6 A[SYNTHETIC, Splitter:B:45:0x00e6] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00f4 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:14:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00f7 A[SYNTHETIC, Splitter:B:52:0x00f7] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0105 A[Catch:{ Exception -> 0x009d }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x01f8 A[SYNTHETIC, Splitter:B:98:0x01f8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void e(android.content.Context r20) {
        /*
            if (r20 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            io.presage.provider.KyoKusanagi r2 = new io.presage.provider.KyoKusanagi
            android.content.ContentResolver r3 = r20.getContentResolver()
            r2.<init>(r3)
            android.content.pm.PackageManager r11 = r20.getPackageManager()     // Catch:{ Exception -> 0x00c4 }
            if (r11 == 0) goto L_0x0002
            java.lang.String r3 = a
            java.lang.String r4 = "sync api key"
            io.presage.p034goto.SaishuKusanagi.a(r3, r4)
            java.lang.String r3 = "io.presage.PresageService.PIVOT"
            r0 = r20
            java.util.List r12 = a(r0, r3)
            if (r12 == 0) goto L_0x0002
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            java.util.HashMap r14 = new java.util.HashMap
            r14.<init>()
            java.util.Iterator r15 = r12.iterator()
        L_0x0033:
            boolean r3 = r15.hasNext()
            if (r3 == 0) goto L_0x021b
            java.lang.Object r3 = r15.next()
            r8 = r3
            android.content.pm.ResolveInfo r8 = (android.content.pm.ResolveInfo) r8
            android.content.pm.ServiceInfo r3 = r8.serviceInfo     // Catch:{ Exception -> 0x009d }
            java.lang.String r3 = r3.packageName     // Catch:{ Exception -> 0x009d }
            r4 = 8
            android.content.pm.PackageInfo r3 = r11.getPackageInfo(r3, r4)     // Catch:{ Exception -> 0x009d }
            android.content.pm.ProviderInfo r16 = a((android.content.pm.PackageInfo) r3)     // Catch:{ Exception -> 0x009d }
            r10 = 0
            r9 = -1
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.SDK_BUILD     // Catch:{ Exception -> 0x00d9, all -> 0x00f4 }
            r0 = r16
            java.lang.String r4 = r0.authority     // Catch:{ Exception -> 0x00d9, all -> 0x00f4 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x00d9, all -> 0x00f4 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r10 = r2.a(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00d9, all -> 0x00f4 }
            if (r10 != 0) goto L_0x0473
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.SDK_BUILD     // Catch:{ Exception -> 0x0455, all -> 0x00f4 }
            r0 = r16
            java.lang.String r4 = r0.authority     // Catch:{ Exception -> 0x0455, all -> 0x00f4 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0455, all -> 0x00f4 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r5 = r2.a(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0455, all -> 0x00f4 }
        L_0x0076:
            if (r5 == 0) goto L_0x0470
            boolean r3 = r5.moveToLast()     // Catch:{ Exception -> 0x045a }
            if (r3 == 0) goto L_0x046d
            java.lang.String r3 = "sdkbuild"
            int r3 = r5.getColumnIndex(r3)     // Catch:{ Exception -> 0x045a }
            int r4 = r5.getInt(r3)     // Catch:{ Exception -> 0x045a }
        L_0x0089:
            r5.close()     // Catch:{ Exception -> 0x045e }
        L_0x008c:
            if (r5 == 0) goto L_0x0091
            r5.close()     // Catch:{ Exception -> 0x00cf }
        L_0x0091:
            r3 = -1
            if (r4 != r3) goto L_0x0105
            java.lang.String r3 = a     // Catch:{ Exception -> 0x009d }
            java.lang.String r4 = "can not get sdk build version"
            io.presage.p034goto.SaishuKusanagi.d(r3, r4)     // Catch:{ Exception -> 0x009d }
            goto L_0x0033
        L_0x009d:
            r3 = move-exception
            java.lang.String r4 = a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "the package name ("
            java.lang.StringBuilder r5 = r5.append(r6)
            android.content.pm.ServiceInfo r6 = r8.serviceInfo
            java.lang.String r6 = r6.packageName
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ") is not found or is dead"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            io.presage.p034goto.SaishuKusanagi.b(r4, r5, r3)
            goto L_0x0033
        L_0x00c4:
            r2 = move-exception
            java.lang.String r3 = a
            java.lang.String r4 = "the PackageManager is not found or is dead"
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r2)
            goto L_0x0002
        L_0x00cf:
            r3 = move-exception
            java.lang.String r5 = a     // Catch:{ Exception -> 0x009d }
            java.lang.String r6 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r5, r6, r3)     // Catch:{ Exception -> 0x009d }
            goto L_0x0091
        L_0x00d9:
            r3 = move-exception
            r4 = r9
            r5 = r10
        L_0x00dc:
            java.lang.String r6 = a     // Catch:{ all -> 0x0451 }
            java.lang.String r7 = "can not get sdk build version"
            io.presage.p034goto.SaishuKusanagi.b(r6, r7, r3)     // Catch:{ all -> 0x0451 }
            if (r5 == 0) goto L_0x0091
            r5.close()     // Catch:{ Exception -> 0x00ea }
            goto L_0x0091
        L_0x00ea:
            r3 = move-exception
            java.lang.String r5 = a     // Catch:{ Exception -> 0x009d }
            java.lang.String r6 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r5, r6, r3)     // Catch:{ Exception -> 0x009d }
            goto L_0x0091
        L_0x00f4:
            r3 = move-exception
        L_0x00f5:
            if (r10 == 0) goto L_0x00fa
            r10.close()     // Catch:{ Exception -> 0x00fb }
        L_0x00fa:
            throw r3     // Catch:{ Exception -> 0x009d }
        L_0x00fb:
            r4 = move-exception
            java.lang.String r5 = a     // Catch:{ Exception -> 0x009d }
            java.lang.String r6 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r5, r6, r4)     // Catch:{ Exception -> 0x009d }
            goto L_0x00fa
        L_0x0105:
            r3 = 100
            if (r4 >= r3) goto L_0x013e
            android.content.pm.ServiceInfo r3 = r8.serviceInfo     // Catch:{ Exception -> 0x009d }
            java.lang.String r4 = r3.packageName     // Catch:{ Exception -> 0x009d }
            r0 = r20
            android.os.Bundle r3 = b(r0, r4)     // Catch:{ Exception -> 0x009d }
            if (r3 == 0) goto L_0x0033
            java.lang.String r5 = "presage_key"
            java.lang.Object r5 = r3.get(r5)     // Catch:{ Exception -> 0x009d }
            r3 = 0
            if (r5 == 0) goto L_0x0123
            java.lang.String r3 = r5.toString()     // Catch:{ Exception -> 0x009d }
        L_0x0123:
            if (r3 == 0) goto L_0x0033
            boolean r5 = r3.isEmpty()     // Catch:{ Exception -> 0x009d }
            if (r5 != 0) goto L_0x0033
            io.presage.goto.Mature$KyoKusanagi r5 = new io.presage.goto.Mature$KyoKusanagi     // Catch:{ Exception -> 0x009d }
            r6 = 0
            r5.<init>()     // Catch:{ Exception -> 0x009d }
            r5.a = r3     // Catch:{ Exception -> 0x009d }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x009d }
            r5.b = r6     // Catch:{ Exception -> 0x009d }
            r14.put(r4, r5)     // Catch:{ Exception -> 0x009d }
            goto L_0x0033
        L_0x013e:
            r13.add(r8)     // Catch:{ Exception -> 0x009d }
            r9 = 0
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.APIKEY     // Catch:{ Exception -> 0x0449, all -> 0x0441 }
            r0 = r16
            java.lang.String r4 = r0.authority     // Catch:{ Exception -> 0x0449, all -> 0x0441 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0449, all -> 0x0441 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r9 = r2.a(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0449, all -> 0x0441 }
            if (r9 != 0) goto L_0x046a
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.APIKEY     // Catch:{ Exception -> 0x044d, all -> 0x0445 }
            r0 = r16
            java.lang.String r4 = r0.authority     // Catch:{ Exception -> 0x044d, all -> 0x0445 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x044d, all -> 0x0445 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r4 = r2.a(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x044d, all -> 0x0445 }
        L_0x0168:
            if (r4 == 0) goto L_0x01ff
        L_0x016a:
            boolean r3 = r4.moveToNext()     // Catch:{ Exception -> 0x01a7 }
            if (r3 == 0) goto L_0x01fc
            io.presage.goto.Mature$KyoKusanagi r5 = new io.presage.goto.Mature$KyoKusanagi     // Catch:{ Exception -> 0x01a7 }
            r3 = 0
            r5.<init>()     // Catch:{ Exception -> 0x01a7 }
            java.lang.String r3 = "package"
            int r3 = r4.getColumnIndex(r3)     // Catch:{ Exception -> 0x01a7 }
            java.lang.String r6 = r4.getString(r3)     // Catch:{ Exception -> 0x01a7 }
            java.lang.String r3 = "apikey"
            int r3 = r4.getColumnIndex(r3)     // Catch:{ Exception -> 0x01a7 }
            java.lang.String r3 = r4.getString(r3)     // Catch:{ Exception -> 0x01a7 }
            r5.a = r3     // Catch:{ Exception -> 0x01a7 }
            java.lang.String r3 = "update_time"
            int r3 = r4.getColumnIndex(r3)     // Catch:{ Exception -> 0x01a7 }
            long r16 = r4.getLong(r3)     // Catch:{ Exception -> 0x01a7 }
            r0 = r16
            r5.b = r0     // Catch:{ Exception -> 0x01a7 }
            boolean r3 = r14.containsKey(r6)     // Catch:{ Exception -> 0x01a7 }
            if (r3 != 0) goto L_0x01de
            r14.put(r6, r5)     // Catch:{ Exception -> 0x01a7 }
            goto L_0x016a
        L_0x01a7:
            r3 = move-exception
        L_0x01a8:
            java.lang.String r5 = a     // Catch:{ all -> 0x01f5 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x01f5 }
            r6.<init>()     // Catch:{ all -> 0x01f5 }
            java.lang.String r7 = "the package name ("
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x01f5 }
            android.content.pm.ServiceInfo r7 = r8.serviceInfo     // Catch:{ all -> 0x01f5 }
            java.lang.String r7 = r7.packageName     // Catch:{ all -> 0x01f5 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x01f5 }
            java.lang.String r7 = ") is not found or is dead"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x01f5 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x01f5 }
            io.presage.p034goto.SaishuKusanagi.b(r5, r6, r3)     // Catch:{ all -> 0x01f5 }
            if (r4 == 0) goto L_0x0033
            r4.close()     // Catch:{ Exception -> 0x01d3 }
            goto L_0x0033
        L_0x01d3:
            r3 = move-exception
            java.lang.String r4 = a     // Catch:{ Exception -> 0x009d }
            java.lang.String r5 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r4, r5, r3)     // Catch:{ Exception -> 0x009d }
            goto L_0x0033
        L_0x01de:
            long r0 = r5.b     // Catch:{ Exception -> 0x01a7 }
            r16 = r0
            java.lang.Object r3 = r14.get(r6)     // Catch:{ Exception -> 0x01a7 }
            io.presage.goto.Mature$KyoKusanagi r3 = (io.presage.p034goto.Mature.KyoKusanagi) r3     // Catch:{ Exception -> 0x01a7 }
            long r0 = r3.b     // Catch:{ Exception -> 0x01a7 }
            r18 = r0
            int r3 = (r16 > r18 ? 1 : (r16 == r18 ? 0 : -1))
            if (r3 <= 0) goto L_0x016a
            r14.put(r6, r5)     // Catch:{ Exception -> 0x01a7 }
            goto L_0x016a
        L_0x01f5:
            r3 = move-exception
        L_0x01f6:
            if (r4 == 0) goto L_0x01fb
            r4.close()     // Catch:{ Exception -> 0x0211 }
        L_0x01fb:
            throw r3     // Catch:{ Exception -> 0x009d }
        L_0x01fc:
            r4.close()     // Catch:{ Exception -> 0x01a7 }
        L_0x01ff:
            if (r4 == 0) goto L_0x0033
            r4.close()     // Catch:{ Exception -> 0x0206 }
            goto L_0x0033
        L_0x0206:
            r3 = move-exception
            java.lang.String r4 = a     // Catch:{ Exception -> 0x009d }
            java.lang.String r5 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r4, r5, r3)     // Catch:{ Exception -> 0x009d }
            goto L_0x0033
        L_0x0211:
            r4 = move-exception
            java.lang.String r5 = a     // Catch:{ Exception -> 0x009d }
            java.lang.String r6 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.b(r5, r6, r4)     // Catch:{ Exception -> 0x009d }
            goto L_0x01fb
        L_0x021b:
            if (r14 == 0) goto L_0x0002
            boolean r3 = r14.isEmpty()
            if (r3 != 0) goto L_0x0002
            java.util.Set r3 = r14.entrySet()
            java.util.Iterator r7 = r3.iterator()
        L_0x022b:
            boolean r3 = r7.hasNext()
            if (r3 == 0) goto L_0x0282
            java.lang.Object r3 = r7.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            r6 = 0
            java.util.Iterator r8 = r12.iterator()
        L_0x023c:
            boolean r4 = r8.hasNext()
            if (r4 == 0) goto L_0x0467
            java.lang.Object r4 = r8.next()
            android.content.pm.ResolveInfo r4 = (android.content.pm.ResolveInfo) r4
            android.content.pm.ServiceInfo r5 = r4.serviceInfo
            if (r5 == 0) goto L_0x023c
            java.lang.Object r5 = r3.getKey()
            java.lang.String r5 = (java.lang.String) r5
            android.content.pm.ServiceInfo r4 = r4.serviceInfo
            java.lang.String r4 = r4.packageName
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x023c
            r4 = 1
        L_0x025d:
            if (r4 != 0) goto L_0x022b
            java.lang.String r4 = a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.Object r3 = r3.getKey()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.StringBuilder r3 = r5.append(r3)
            java.lang.String r5 = " already uninstalled"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            io.presage.p034goto.SaishuKusanagi.a(r4, r3)
            r7.remove()
            goto L_0x022b
        L_0x0282:
            java.util.Iterator r11 = r13.iterator()
        L_0x0286:
            boolean r3 = r11.hasNext()
            if (r3 == 0) goto L_0x0002
            java.lang.Object r3 = r11.next()
            r8 = r3
            android.content.pm.ResolveInfo r8 = (android.content.pm.ResolveInfo) r8
            android.content.pm.PackageManager r3 = r20.getPackageManager()     // Catch:{ Exception -> 0x0308 }
            android.content.pm.ServiceInfo r4 = r8.serviceInfo     // Catch:{ Exception -> 0x0308 }
            java.lang.String r4 = r4.packageName     // Catch:{ Exception -> 0x0308 }
            r5 = 8
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r4, r5)     // Catch:{ Exception -> 0x0308 }
            android.content.pm.ProviderInfo r12 = a((android.content.pm.PackageInfo) r3)     // Catch:{ Exception -> 0x0308 }
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.APIKEY     // Catch:{ Exception -> 0x0308 }
            java.lang.String r4 = r12.authority     // Catch:{ Exception -> 0x0308 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0308 }
            if (r3 != 0) goto L_0x02b7
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.APIKEY     // Catch:{ Exception -> 0x0308 }
            java.lang.String r4 = r12.authority     // Catch:{ Exception -> 0x0308 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0308 }
        L_0x02b7:
            if (r3 == 0) goto L_0x0286
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0308 }
            r5 = 0
            java.lang.String r6 = "package"
            r4[r5] = r6     // Catch:{ Exception -> 0x0308 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r4 = r2.a(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0308 }
            if (r4 == 0) goto L_0x0332
        L_0x02cb:
            boolean r5 = r4.moveToNext()     // Catch:{ Exception -> 0x0308 }
            if (r5 == 0) goto L_0x032f
            java.lang.String r5 = "package"
            int r5 = r4.getColumnIndex(r5)     // Catch:{ Exception -> 0x0308 }
            java.lang.String r5 = r4.getString(r5)     // Catch:{ Exception -> 0x0308 }
            boolean r6 = r14.containsKey(r5)     // Catch:{ Exception -> 0x0308 }
            if (r6 != 0) goto L_0x02cb
            java.lang.String r6 = a     // Catch:{ Exception -> 0x0308 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0308 }
            r7.<init>()     // Catch:{ Exception -> 0x0308 }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ Exception -> 0x0308 }
            java.lang.String r9 = " is already uninstalled"
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x0308 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0308 }
            io.presage.p034goto.SaishuKusanagi.b(r6, r7)     // Catch:{ Exception -> 0x0308 }
            java.lang.String r6 = "package = ?"
            r7 = 1
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ Exception -> 0x0308 }
            r9 = 0
            r7[r9] = r5     // Catch:{ Exception -> 0x0308 }
            r2.a((android.net.Uri) r3, (java.lang.String) r6, (java.lang.String[]) r7)     // Catch:{ Exception -> 0x0308 }
            goto L_0x02cb
        L_0x0308:
            r3 = move-exception
            java.lang.String r4 = a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "the package name ("
            java.lang.StringBuilder r5 = r5.append(r6)
            android.content.pm.ServiceInfo r6 = r8.serviceInfo
            java.lang.String r6 = r6.packageName
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ") is not found or is dead"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            io.presage.p034goto.SaishuKusanagi.b(r4, r5, r3)
            goto L_0x0286
        L_0x032f:
            r4.close()     // Catch:{ Exception -> 0x0308 }
        L_0x0332:
            java.util.Set r4 = r14.entrySet()     // Catch:{ Exception -> 0x0308 }
            java.util.Iterator r6 = r4.iterator()     // Catch:{ Exception -> 0x0308 }
        L_0x033a:
            boolean r4 = r6.hasNext()     // Catch:{ Exception -> 0x0308 }
            if (r4 == 0) goto L_0x037f
            java.lang.Object r4 = r6.next()     // Catch:{ Exception -> 0x0308 }
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4     // Catch:{ Exception -> 0x0308 }
            android.content.ContentValues r7 = new android.content.ContentValues     // Catch:{ Exception -> 0x0308 }
            r7.<init>()     // Catch:{ Exception -> 0x0308 }
            java.lang.String r9 = "package"
            java.lang.Object r5 = r4.getKey()     // Catch:{ Exception -> 0x0308 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x0308 }
            r7.put(r9, r5)     // Catch:{ Exception -> 0x0308 }
            java.lang.String r9 = "apikey"
            java.lang.Object r5 = r4.getValue()     // Catch:{ Exception -> 0x0308 }
            io.presage.goto.Mature$KyoKusanagi r5 = (io.presage.p034goto.Mature.KyoKusanagi) r5     // Catch:{ Exception -> 0x0308 }
            java.lang.String r5 = r5.a     // Catch:{ Exception -> 0x0308 }
            r7.put(r9, r5)     // Catch:{ Exception -> 0x0308 }
            java.lang.String r5 = "update_time"
            java.lang.Object r4 = r4.getValue()     // Catch:{ Exception -> 0x0308 }
            io.presage.goto.Mature$KyoKusanagi r4 = (io.presage.p034goto.Mature.KyoKusanagi) r4     // Catch:{ Exception -> 0x0308 }
            long r0 = r4.b     // Catch:{ Exception -> 0x0308 }
            r16 = r0
            java.lang.Long r4 = java.lang.Long.valueOf(r16)     // Catch:{ Exception -> 0x0308 }
            r7.put(r5, r4)     // Catch:{ Exception -> 0x0308 }
            r4 = 0
            r5 = 0
            r2.a(r3, r7, r4, r5)     // Catch:{ Exception -> 0x0308 }
            goto L_0x033a
        L_0x037f:
            android.content.pm.ServiceInfo r3 = r8.serviceInfo     // Catch:{ Exception -> 0x0308 }
            java.lang.String r3 = r3.packageName     // Catch:{ Exception -> 0x0308 }
            java.lang.Object r3 = r14.get(r3)     // Catch:{ Exception -> 0x0308 }
            io.presage.goto.Mature$KyoKusanagi r3 = (io.presage.p034goto.Mature.KyoKusanagi) r3     // Catch:{ Exception -> 0x0308 }
            if (r3 == 0) goto L_0x0286
            java.lang.String r13 = r3.a     // Catch:{ Exception -> 0x0308 }
            if (r13 == 0) goto L_0x0286
            r10 = 0
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.MY_PROFIG_APIKEY     // Catch:{ Exception -> 0x0308 }
            java.lang.String r4 = r12.authority     // Catch:{ Exception -> 0x0308 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0308 }
            if (r3 != 0) goto L_0x03a2
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.MY_PROFIG_APIKEY     // Catch:{ Exception -> 0x0308 }
            java.lang.String r4 = r12.authority     // Catch:{ Exception -> 0x0308 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0308 }
        L_0x03a2:
            if (r3 == 0) goto L_0x0286
            r9 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r4 = r2.a(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x03f1, all -> 0x0428 }
            if (r4 == 0) goto L_0x0464
            boolean r3 = r4.moveToLast()     // Catch:{ Exception -> 0x043c }
            if (r3 == 0) goto L_0x0461
            java.lang.String r3 = "v"
            int r3 = r4.getColumnIndex(r3)     // Catch:{ Exception -> 0x043c }
            java.lang.String r5 = r4.getString(r3)     // Catch:{ Exception -> 0x043c }
        L_0x03c0:
            r4.close()     // Catch:{ Exception -> 0x043f }
        L_0x03c3:
            if (r4 == 0) goto L_0x03c8
            r4.close()     // Catch:{ Exception -> 0x03e7 }
        L_0x03c8:
            if (r5 == 0) goto L_0x0286
            boolean r3 = r13.equals(r5)     // Catch:{ Exception -> 0x0308 }
            if (r3 != 0) goto L_0x0286
            java.lang.String r3 = a     // Catch:{ Exception -> 0x0308 }
            java.lang.String r4 = "api key is changed"
            io.presage.p034goto.SaishuKusanagi.c(r3, r4)     // Catch:{ Exception -> 0x0308 }
            io.presage.provider.PresageProvider$KyoKusanagi r3 = io.presage.provider.PresageProvider.KyoKusanagi.PROFIG     // Catch:{ Exception -> 0x0308 }
            java.lang.String r4 = r12.authority     // Catch:{ Exception -> 0x0308 }
            android.net.Uri r3 = io.presage.provider.PresageProvider.a(r3, r4)     // Catch:{ Exception -> 0x0308 }
            r4 = 0
            r5 = 0
            r2.a((android.net.Uri) r3, (java.lang.String) r4, (java.lang.String[]) r5)     // Catch:{ Exception -> 0x0308 }
            goto L_0x0286
        L_0x03e7:
            r3 = move-exception
            java.lang.String r3 = a     // Catch:{ Exception -> 0x0308 }
            java.lang.String r4 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.d(r3, r4)     // Catch:{ Exception -> 0x0308 }
            goto L_0x03c8
        L_0x03f1:
            r3 = move-exception
            r4 = r9
            r5 = r10
        L_0x03f4:
            java.lang.String r6 = a     // Catch:{ all -> 0x043a }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x043a }
            r7.<init>()     // Catch:{ all -> 0x043a }
            java.lang.String r9 = "the package name ("
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ all -> 0x043a }
            android.content.pm.ServiceInfo r9 = r8.serviceInfo     // Catch:{ all -> 0x043a }
            java.lang.String r9 = r9.packageName     // Catch:{ all -> 0x043a }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ all -> 0x043a }
            java.lang.String r9 = ") is not found or is dead"
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ all -> 0x043a }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x043a }
            io.presage.p034goto.SaishuKusanagi.b(r6, r7, r3)     // Catch:{ all -> 0x043a }
            if (r4 == 0) goto L_0x03c8
            r4.close()     // Catch:{ Exception -> 0x041e }
            goto L_0x03c8
        L_0x041e:
            r3 = move-exception
            java.lang.String r3 = a     // Catch:{ Exception -> 0x0308 }
            java.lang.String r4 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.d(r3, r4)     // Catch:{ Exception -> 0x0308 }
            goto L_0x03c8
        L_0x0428:
            r3 = move-exception
            r4 = r9
        L_0x042a:
            if (r4 == 0) goto L_0x042f
            r4.close()     // Catch:{ Exception -> 0x0430 }
        L_0x042f:
            throw r3     // Catch:{ Exception -> 0x0308 }
        L_0x0430:
            r4 = move-exception
            java.lang.String r4 = a     // Catch:{ Exception -> 0x0308 }
            java.lang.String r5 = "cursor close error"
            io.presage.p034goto.SaishuKusanagi.d(r4, r5)     // Catch:{ Exception -> 0x0308 }
            goto L_0x042f
        L_0x043a:
            r3 = move-exception
            goto L_0x042a
        L_0x043c:
            r3 = move-exception
            r5 = r10
            goto L_0x03f4
        L_0x043f:
            r3 = move-exception
            goto L_0x03f4
        L_0x0441:
            r3 = move-exception
            r4 = r9
            goto L_0x01f6
        L_0x0445:
            r3 = move-exception
            r4 = r9
            goto L_0x01f6
        L_0x0449:
            r3 = move-exception
            r4 = r9
            goto L_0x01a8
        L_0x044d:
            r3 = move-exception
            r4 = r9
            goto L_0x01a8
        L_0x0451:
            r3 = move-exception
            r10 = r5
            goto L_0x00f5
        L_0x0455:
            r3 = move-exception
            r4 = r9
            r5 = r10
            goto L_0x00dc
        L_0x045a:
            r3 = move-exception
            r4 = r9
            goto L_0x00dc
        L_0x045e:
            r3 = move-exception
            goto L_0x00dc
        L_0x0461:
            r5 = r10
            goto L_0x03c0
        L_0x0464:
            r5 = r10
            goto L_0x03c3
        L_0x0467:
            r4 = r6
            goto L_0x025d
        L_0x046a:
            r4 = r9
            goto L_0x0168
        L_0x046d:
            r4 = r9
            goto L_0x0089
        L_0x0470:
            r4 = r9
            goto L_0x008c
        L_0x0473:
            r5 = r10
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.Mature.e(android.content.Context):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0077 A[SYNTHETIC, Splitter:B:28:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0090 A[SYNTHETIC, Splitter:B:38:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean f(android.content.Context r10) {
        /*
            r7 = 1
            r6 = 0
            r8 = 0
            if (r10 != 0) goto L_0x0007
            r0 = r6
        L_0x0006:
            return r0
        L_0x0007:
            io.presage.provider.KyoKusanagi r0 = new io.presage.provider.KyoKusanagi     // Catch:{ Exception -> 0x006a, all -> 0x008b }
            android.content.ContentResolver r1 = r10.getContentResolver()     // Catch:{ Exception -> 0x006a, all -> 0x008b }
            r0.<init>(r1)     // Catch:{ Exception -> 0x006a, all -> 0x008b }
            io.presage.provider.PresageProvider$KyoKusanagi r1 = io.presage.provider.PresageProvider.KyoKusanagi.PROFIG_JSON     // Catch:{ Exception -> 0x006a, all -> 0x008b }
            java.lang.String r2 = io.presage.provider.PresageProvider.a(r10)     // Catch:{ Exception -> 0x006a, all -> 0x008b }
            android.net.Uri r1 = io.presage.provider.PresageProvider.a(r1, r2)     // Catch:{ Exception -> 0x006a, all -> 0x008b }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r9 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x006a, all -> 0x008b }
            if (r9 != 0) goto L_0x00bb
            io.presage.provider.KyoKusanagi r0 = new io.presage.provider.KyoKusanagi     // Catch:{ Exception -> 0x00ae, all -> 0x00a5 }
            android.content.ContentResolver r1 = r10.getContentResolver()     // Catch:{ Exception -> 0x00ae, all -> 0x00a5 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00ae, all -> 0x00a5 }
            io.presage.provider.PresageProvider$KyoKusanagi r1 = io.presage.provider.PresageProvider.KyoKusanagi.PROFIG_JSON     // Catch:{ Exception -> 0x00ae, all -> 0x00a5 }
            java.lang.String r2 = io.presage.provider.PresageProvider.a(r10)     // Catch:{ Exception -> 0x00ae, all -> 0x00a5 }
            android.net.Uri r1 = io.presage.provider.PresageProvider.a(r1, r2)     // Catch:{ Exception -> 0x00ae, all -> 0x00a5 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r2 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00ae, all -> 0x00a5 }
        L_0x003f:
            if (r2 == 0) goto L_0x00b9
            boolean r0 = r2.moveToLast()     // Catch:{ Exception -> 0x00b2, all -> 0x00a9 }
            if (r0 == 0) goto L_0x00b7
            java.lang.String r0 = "v"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2, all -> 0x00a9 }
            java.lang.String r1 = r2.getString(r0)     // Catch:{ Exception -> 0x00b2, all -> 0x00a9 }
        L_0x0052:
            r2.close()     // Catch:{ Exception -> 0x00b5 }
        L_0x0055:
            if (r2 == 0) goto L_0x005a
            r2.close()     // Catch:{ Exception -> 0x005e }
        L_0x005a:
            if (r1 != 0) goto L_0x0068
            r0 = r6
            goto L_0x0006
        L_0x005e:
            r0 = move-exception
            java.lang.String r2 = a
            java.lang.String r3 = "close cursor error"
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)
            goto L_0x005a
        L_0x0068:
            r0 = r7
            goto L_0x0006
        L_0x006a:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x006d:
            java.lang.String r3 = a     // Catch:{ all -> 0x00ac }
            java.lang.String r4 = "isProfigSaved error"
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r0)     // Catch:{ all -> 0x00ac }
            if (r2 == 0) goto L_0x007a
            r2.close()     // Catch:{ Exception -> 0x007e }
        L_0x007a:
            if (r1 != 0) goto L_0x0088
            r0 = r6
            goto L_0x0006
        L_0x007e:
            r0 = move-exception
            java.lang.String r2 = a
            java.lang.String r3 = "close cursor error"
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)
            goto L_0x007a
        L_0x0088:
            r0 = r7
            goto L_0x0006
        L_0x008b:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x008e:
            if (r2 == 0) goto L_0x0093
            r2.close()     // Catch:{ Exception -> 0x0098 }
        L_0x0093:
            if (r1 != 0) goto L_0x00a2
            r0 = r6
            goto L_0x0006
        L_0x0098:
            r0 = move-exception
            java.lang.String r2 = a
            java.lang.String r3 = "close cursor error"
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)
            goto L_0x0093
        L_0x00a2:
            r0 = r7
            goto L_0x0006
        L_0x00a5:
            r0 = move-exception
            r1 = r8
            r2 = r9
            goto L_0x008e
        L_0x00a9:
            r0 = move-exception
            r1 = r8
            goto L_0x008e
        L_0x00ac:
            r0 = move-exception
            goto L_0x008e
        L_0x00ae:
            r0 = move-exception
            r1 = r8
            r2 = r9
            goto L_0x006d
        L_0x00b2:
            r0 = move-exception
            r1 = r8
            goto L_0x006d
        L_0x00b5:
            r0 = move-exception
            goto L_0x006d
        L_0x00b7:
            r1 = r8
            goto L_0x0052
        L_0x00b9:
            r1 = r8
            goto L_0x0055
        L_0x00bb:
            r2 = r9
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.Mature.f(android.content.Context):boolean");
    }
}
