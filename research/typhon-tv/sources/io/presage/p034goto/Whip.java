package io.presage.p034goto;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.TypedValue;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import org.apache.commons.lang3.time.TimeZones;
import p009if.ChangKoehan;
import p009if.LeonaHeidern;

/* renamed from: io.presage.goto.Whip  reason: invalid package */
public class Whip {
    public static int a(Context context, int i) {
        return (int) TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics());
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.lang.String r7, int r8, int r9) {
        /*
            r4 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ IOException -> 0x0045, all -> 0x0063 }
            r1.<init>(r7)     // Catch:{ IOException -> 0x0045, all -> 0x0063 }
            java.lang.String r0 = r1.getProtocol()     // Catch:{ IOException -> 0x0045, all -> 0x0063 }
            java.lang.String r2 = "file"
            boolean r0 = r0.equals(r2)     // Catch:{ IOException -> 0x0045, all -> 0x0063 }
            if (r0 == 0) goto L_0x0032
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0045, all -> 0x0063 }
            java.lang.String r1 = r1.getPath()     // Catch:{ IOException -> 0x0045, all -> 0x0063 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x0045, all -> 0x0063 }
            r1 = r0
            r3 = r4
        L_0x001e:
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ IOException -> 0x0076 }
            if (r8 <= 0) goto L_0x0080
            if (r9 <= 0) goto L_0x0080
            r0 = 0
            android.graphics.Bitmap r4 = android.graphics.Bitmap.createScaledBitmap(r4, r8, r8, r0)     // Catch:{ IOException -> 0x007a }
            r0 = r4
        L_0x002c:
            if (r3 == 0) goto L_0x0031
            r3.disconnect()
        L_0x0031:
            return r0
        L_0x0032:
            java.net.URLConnection r0 = r1.openConnection()     // Catch:{ IOException -> 0x0045, all -> 0x0063 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0045, all -> 0x0063 }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ IOException -> 0x0072, all -> 0x006b }
            r0.connect()     // Catch:{ IOException -> 0x0072, all -> 0x006b }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ IOException -> 0x0072, all -> 0x006b }
            r3 = r0
            goto L_0x001e
        L_0x0045:
            r0 = move-exception
            r2 = r0
            r3 = r4
            r1 = r4
        L_0x0049:
            java.lang.String r0 = "Util"
            java.lang.String r4 = "Unable to get the bitmap from the url [%s]"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x006e }
            r6 = 0
            r5[r6] = r7     // Catch:{ all -> 0x006e }
            java.lang.String r4 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x006e }
            io.presage.p034goto.SaishuKusanagi.b(r0, r4, r2)     // Catch:{ all -> 0x006e }
            if (r3 == 0) goto L_0x007e
            r3.disconnect()
            r0 = r1
            goto L_0x0031
        L_0x0063:
            r0 = move-exception
            r1 = r0
        L_0x0065:
            if (r4 == 0) goto L_0x006a
            r4.disconnect()
        L_0x006a:
            throw r1
        L_0x006b:
            r1 = move-exception
            r4 = r0
            goto L_0x0065
        L_0x006e:
            r0 = move-exception
            r1 = r0
            r4 = r3
            goto L_0x0065
        L_0x0072:
            r2 = move-exception
            r3 = r0
            r1 = r4
            goto L_0x0049
        L_0x0076:
            r0 = move-exception
            r2 = r0
            r1 = r4
            goto L_0x0049
        L_0x007a:
            r0 = move-exception
            r2 = r0
            r1 = r4
            goto L_0x0049
        L_0x007e:
            r0 = r1
            goto L_0x0031
        L_0x0080:
            r0 = r4
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.Whip.a(java.lang.String, int, int):android.graphics.Bitmap");
    }

    public static String a() throws IOException {
        ChangKoehan r0 = LeonaHeidern.m18966(LeonaHeidern.m18968(new File("/sys/class/net/wlan0/address")));
        String r1 = r0.m6700();
        if (r0 != null) {
            r0.close();
        }
        return r1;
    }

    public static String a(String[] strArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < strArr.length; i++) {
            if (i == 0) {
                sb.append(strArr[i]);
            } else {
                sb.append("," + strArr[i]);
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static boolean a(Context context) {
        return ((KeyguardManager) context.getSystemService("keyguard")).inKeyguardRestrictedInputMode();
    }

    public static boolean a(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                return false;
            }
        }
        return true;
    }

    public static String b() {
        try {
            String format = new SimpleDateFormat("Z").format(Calendar.getInstance(TimeZone.getTimeZone(TimeZones.GMT_ID), Locale.getDefault()).getTime());
            return !a(format) ? "" : format;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String b(Context context) {
        try {
            String a = a();
            if (a != null && !a.isEmpty()) {
                return UUID.nameUUIDFromBytes(a.getBytes()).toString();
            }
        } catch (Exception e) {
            SaishuKusanagi.b("generateFakeAAID", "mac", e);
        }
        try {
            ApplicationInfo c = c(context);
            if (c == null) {
                return "00000000-0000-0000-0000-000000000000";
            }
            try {
                return UUID.nameUUIDFromBytes((context.getPackageManager().getPackageInfo(c.packageName, 128).firstInstallTime + "").getBytes()).toString();
            } catch (Exception e2) {
                SaishuKusanagi.b("generateFakeAAID", "firstInstallTime", e2);
                return "00000000-0000-0000-0000-000000000000";
            }
        } catch (Exception e3) {
            SaishuKusanagi.b("generateFakeAAID", "firstSystemApp", e3);
            return "00000000-0000-0000-0000-000000000000";
        }
    }

    private static ApplicationInfo c(Context context) {
        if (context == null) {
            return null;
        }
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return null;
        }
        List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(128);
        ArrayList arrayList = new ArrayList();
        for (ApplicationInfo next : installedApplications) {
            if (!((next.flags & 1) == 0 || next.packageName == null)) {
                arrayList.add(next);
            }
        }
        if (installedApplications.size() == 0) {
            return null;
        }
        Collections.sort(arrayList, new Comparator<ApplicationInfo>() {
            /* renamed from: a */
            public int compare(ApplicationInfo applicationInfo, ApplicationInfo applicationInfo2) {
                return applicationInfo.packageName.compareTo(applicationInfo2.packageName);
            }
        });
        return (ApplicationInfo) arrayList.get(0);
    }
}
