package io.presage.p034goto;

import android.content.Context;
import io.presage.p029char.ChangKoehan;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: io.presage.goto.KyoKusanagi  reason: invalid package */
public class KyoKusanagi implements Shermie<Object> {
    public static final String a = KyoKusanagi.class.getSimpleName();
    private static final KyoKusanagi b = new KyoKusanagi();
    private ConcurrentLinkedQueue<Object> c = new ConcurrentLinkedQueue<>();

    private KyoKusanagi() {
    }

    public static KyoKusanagi a() {
        return b;
    }

    public void a(Object obj) {
        if (!this.c.contains(obj)) {
            this.c.add(obj);
        }
    }

    public boolean a(Context context) {
        return !Mature.f(context) || !ChangKoehan.a().m();
    }

    public void c() {
        if (this.c != null) {
            Iterator<Object> it2 = this.c.iterator();
            while (it2.hasNext()) {
                Object next = it2.next();
                if (next instanceof Shermie) {
                    ((Shermie) next).c();
                }
            }
            if (this.c != null) {
                this.c.clear();
            }
        }
    }
}
