package io.presage.p034goto;

import android.content.Context;
import android.os.Build;
import io.presage.p032else.KyoKusanagi;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

/* renamed from: io.presage.goto.ChoiBounge  reason: invalid package */
public class ChoiBounge {
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0069 A[SYNTHETIC, Splitter:B:23:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006e A[Catch:{ Exception -> 0x007b }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0060=Splitter:B:18:0x0060, B:30:0x0075=Splitter:B:30:0x0075} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String[] r6) throws java.lang.Exception {
        /*
            r2 = 0
            r0 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            int r1 = r6.length
        L_0x0008:
            if (r0 >= r1) goto L_0x0018
            r3 = r6[r0]
            r4.append(r3)
            java.lang.String r3 = " "
            r4.append(r3)
            int r0 = r0 + 1
            goto L_0x0008
        L_0x0018:
            java.lang.String r0 = "\n"
            r4.append(r0)
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x005d, InterruptedException -> 0x0072, all -> 0x007d }
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ IOException -> 0x005d, InterruptedException -> 0x0072, all -> 0x007d }
            r3 = 0
            java.lang.String r5 = "su"
            r1[r3] = r5     // Catch:{ IOException -> 0x005d, InterruptedException -> 0x0072, all -> 0x007d }
            java.lang.Process r3 = r0.exec(r1)     // Catch:{ IOException -> 0x005d, InterruptedException -> 0x0072, all -> 0x007d }
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x0084, all -> 0x0081 }
            java.io.OutputStream r0 = r3.getOutputStream()     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x0084, all -> 0x0081 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x0084, all -> 0x0081 }
            java.lang.String r0 = r4.toString()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            r1.writeBytes(r0)     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            r1.flush()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            java.lang.String r0 = "exit $?\n\n"
            r1.writeBytes(r0)     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            r1.flush()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            r3.waitFor()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            int r0 = r3.exitValue()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            if (r3 == 0) goto L_0x0057
            r3.destroy()     // Catch:{ Exception -> 0x008e }
        L_0x0057:
            if (r1 == 0) goto L_0x005c
            r1.close()     // Catch:{ Exception -> 0x008e }
        L_0x005c:
            return r0
        L_0x005d:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x0060:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x0066 }
            r2.<init>(r0)     // Catch:{ all -> 0x0066 }
            throw r2     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r0 = move-exception
        L_0x0067:
            if (r3 == 0) goto L_0x006c
            r3.destroy()     // Catch:{ Exception -> 0x007b }
        L_0x006c:
            if (r1 == 0) goto L_0x0071
            r1.close()     // Catch:{ Exception -> 0x007b }
        L_0x0071:
            throw r0
        L_0x0072:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x0075:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x0066 }
            r2.<init>(r0)     // Catch:{ all -> 0x0066 }
            throw r2     // Catch:{ all -> 0x0066 }
        L_0x007b:
            r1 = move-exception
            goto L_0x0071
        L_0x007d:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x0067
        L_0x0081:
            r0 = move-exception
            r1 = r2
            goto L_0x0067
        L_0x0084:
            r0 = move-exception
            r1 = r2
            goto L_0x0075
        L_0x0087:
            r0 = move-exception
            goto L_0x0075
        L_0x0089:
            r0 = move-exception
            r1 = r2
            goto L_0x0060
        L_0x008c:
            r0 = move-exception
            goto L_0x0060
        L_0x008e:
            r1 = move-exception
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.ChoiBounge.a(java.lang.String[]):int");
    }

    public static String a(Context context) {
        String str;
        if (!d()) {
            str = "osmipcV";
        } else if (context.getFilesDir() == null) {
            return null;
        } else {
            str = context.getFilesDir().getAbsolutePath() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "osmipcV";
        }
        try {
            return str + context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            return str;
        }
    }

    private static void a(String str, KyoKusanagi kyoKusanagi) {
        if (!kyoKusanagi.b()) {
            return;
        }
        if (f()) {
            try {
                a(new String[]{"restorecon", str});
            } catch (Exception e) {
            }
        } else if (d()) {
            try {
                a(new String[]{"chcon", "u:object_r:app_data_file:s0", str});
            } catch (Exception e2) {
            }
        }
    }

    public static boolean a() {
        return Build.CPU_ABI.toLowerCase().contains("arm");
    }

    private static boolean a(String str, String str2) {
        if (str2 == null) {
            return false;
        }
        try {
            FileWriter fileWriter = new FileWriter(str, false);
            fileWriter.write(str2);
            fileWriter.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x006f A[SYNTHETIC, Splitter:B:22:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0074 A[Catch:{ Exception -> 0x00e6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d6 A[SYNTHETIC, Splitter:B:43:0x00d6] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00db A[Catch:{ Exception -> 0x00df }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(java.lang.String r6, java.lang.String r7, android.content.Context r8) {
        /*
            r2 = 0
            r0 = 0
            boolean r1 = a()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            if (r1 == 0) goto L_0x0078
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            r1.<init>()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r3 = "_arm"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
        L_0x001c:
            boolean r3 = d()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            if (r3 == 0) goto L_0x0036
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            r3.<init>()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r3 = "_pie"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
        L_0x0036:
            boolean r3 = e()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            if (r3 == 0) goto L_0x0050
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            r3.<init>()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r3 = "_22"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
        L_0x0050:
            android.content.res.AssetManager r3 = r8.getAssets()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.io.InputStream r3 = r3.open(r1)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00ec, all -> 0x00e1 }
            r1.<init>(r7)     // Catch:{ IOException -> 0x00ec, all -> 0x00e1 }
            r2 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x006c, all -> 0x00e4 }
        L_0x0061:
            int r4 = r3.read(r2)     // Catch:{ IOException -> 0x006c, all -> 0x00e4 }
            if (r4 <= 0) goto L_0x00c5
            r5 = 0
            r1.write(r2, r5, r4)     // Catch:{ IOException -> 0x006c, all -> 0x00e4 }
            goto L_0x0061
        L_0x006c:
            r2 = move-exception
        L_0x006d:
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ Exception -> 0x00e6 }
        L_0x0072:
            if (r3 == 0) goto L_0x0077
            r3.close()     // Catch:{ Exception -> 0x00e6 }
        L_0x0077:
            return r0
        L_0x0078:
            boolean r1 = c()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            if (r1 == 0) goto L_0x0093
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            r1.<init>()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r3 = "_x86"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            goto L_0x001c
        L_0x0093:
            boolean r1 = b()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            if (r1 == 0) goto L_0x00af
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            r1.<init>()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r3 = "_mips"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            goto L_0x001c
        L_0x00af:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            r1.<init>()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r3 = "_arm"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00e8, all -> 0x00d1 }
            goto L_0x001c
        L_0x00c5:
            if (r1 == 0) goto L_0x00ca
            r1.close()     // Catch:{ Exception -> 0x00f0 }
        L_0x00ca:
            if (r3 == 0) goto L_0x00cf
            r3.close()     // Catch:{ Exception -> 0x00f0 }
        L_0x00cf:
            r0 = 1
            goto L_0x0077
        L_0x00d1:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x00d4:
            if (r1 == 0) goto L_0x00d9
            r1.close()     // Catch:{ Exception -> 0x00df }
        L_0x00d9:
            if (r3 == 0) goto L_0x00de
            r3.close()     // Catch:{ Exception -> 0x00df }
        L_0x00de:
            throw r0
        L_0x00df:
            r1 = move-exception
            goto L_0x00de
        L_0x00e1:
            r0 = move-exception
            r1 = r2
            goto L_0x00d4
        L_0x00e4:
            r0 = move-exception
            goto L_0x00d4
        L_0x00e6:
            r1 = move-exception
            goto L_0x0077
        L_0x00e8:
            r1 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x006d
        L_0x00ec:
            r1 = move-exception
            r1 = r2
            goto L_0x006d
        L_0x00f0:
            r0 = move-exception
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.ChoiBounge.a(java.lang.String, java.lang.String, android.content.Context):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0069 A[SYNTHETIC, Splitter:B:23:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006e A[Catch:{ Exception -> 0x007b }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0060=Splitter:B:18:0x0060, B:30:0x0075=Splitter:B:30:0x0075} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(java.lang.String[] r6) throws java.lang.Exception {
        /*
            r2 = 0
            r0 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            int r1 = r6.length
        L_0x0008:
            if (r0 >= r1) goto L_0x0018
            r3 = r6[r0]
            r4.append(r3)
            java.lang.String r3 = " "
            r4.append(r3)
            int r0 = r0 + 1
            goto L_0x0008
        L_0x0018:
            java.lang.String r0 = "\n"
            r4.append(r0)
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x005d, InterruptedException -> 0x0072, all -> 0x007d }
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ IOException -> 0x005d, InterruptedException -> 0x0072, all -> 0x007d }
            r3 = 0
            java.lang.String r5 = "sh"
            r1[r3] = r5     // Catch:{ IOException -> 0x005d, InterruptedException -> 0x0072, all -> 0x007d }
            java.lang.Process r3 = r0.exec(r1)     // Catch:{ IOException -> 0x005d, InterruptedException -> 0x0072, all -> 0x007d }
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x0084, all -> 0x0081 }
            java.io.OutputStream r0 = r3.getOutputStream()     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x0084, all -> 0x0081 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x0084, all -> 0x0081 }
            java.lang.String r0 = r4.toString()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            r1.writeBytes(r0)     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            r1.flush()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            java.lang.String r0 = "exit $?\n\n"
            r1.writeBytes(r0)     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            r1.flush()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            r3.waitFor()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            int r0 = r3.exitValue()     // Catch:{ IOException -> 0x008c, InterruptedException -> 0x0087 }
            if (r3 == 0) goto L_0x0057
            r3.destroy()     // Catch:{ Exception -> 0x008e }
        L_0x0057:
            if (r1 == 0) goto L_0x005c
            r1.close()     // Catch:{ Exception -> 0x008e }
        L_0x005c:
            return r0
        L_0x005d:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x0060:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x0066 }
            r2.<init>(r0)     // Catch:{ all -> 0x0066 }
            throw r2     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r0 = move-exception
        L_0x0067:
            if (r3 == 0) goto L_0x006c
            r3.destroy()     // Catch:{ Exception -> 0x007b }
        L_0x006c:
            if (r1 == 0) goto L_0x0071
            r1.close()     // Catch:{ Exception -> 0x007b }
        L_0x0071:
            throw r0
        L_0x0072:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x0075:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x0066 }
            r2.<init>(r0)     // Catch:{ all -> 0x0066 }
            throw r2     // Catch:{ all -> 0x0066 }
        L_0x007b:
            r1 = move-exception
            goto L_0x0071
        L_0x007d:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x0067
        L_0x0081:
            r0 = move-exception
            r1 = r2
            goto L_0x0067
        L_0x0084:
            r0 = move-exception
            r1 = r2
            goto L_0x0075
        L_0x0087:
            r0 = move-exception
            goto L_0x0075
        L_0x0089:
            r0 = move-exception
            r1 = r2
            goto L_0x0060
        L_0x008c:
            r0 = move-exception
            goto L_0x0060
        L_0x008e:
            r1 = move-exception
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.ChoiBounge.b(java.lang.String[]):int");
    }

    public static String b(Context context) {
        if (context.getFilesDir() == null) {
            return null;
        }
        return context.getFilesDir().getAbsolutePath() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "osmcore";
    }

    public static boolean b() {
        return Build.CPU_ABI.toLowerCase().contains("mips");
    }

    public static String c(Context context) {
        return Integer.toString(context.getApplicationInfo().uid);
    }

    public static boolean c() {
        return Build.CPU_ABI.toLowerCase().contains("x86");
    }

    public static boolean d() {
        return Build.VERSION.SDK_INT >= 21;
    }

    public static boolean d(Context context) {
        String b;
        String a;
        boolean z = false;
        if (!(context == null || (b = b(context)) == null || (a = a(context)) == null)) {
            String c = c(context);
            KyoKusanagi a2 = KyoKusanagi.a(context);
            a(b, a2);
            if (a("osmcore", b, context) && a(b + ".token", a2.c())) {
                try {
                    FileChannel channel = new RandomAccessFile(new File(b + ".lock"), InternalZipTyphoonApp.WRITE_MODE).getChannel();
                    FileLock tryLock = channel.tryLock();
                    try {
                        b(new String[]{"chmod", "755", b});
                        if (!a2.b()) {
                            b(new String[]{b, b + ".token", a, c, "&"});
                        } else if (!d()) {
                            a(new String[]{b, b + ".token", a, c, "&"});
                        } else {
                            boolean g = g();
                            a(new String[]{"chcon", "u:object_r:system_file:s0", b});
                            if (!g) {
                                a(new String[]{"su", "-c", "\"" + b, b + ".token", a, c, " &\" &"});
                            } else {
                                a(new String[]{"su", "--context", "u:r:init:s0", "-c", "\"" + b, b + ".token", a, c, " &\" &"});
                            }
                            if (f()) {
                                a(new String[]{"restorecon", b});
                                a(new String[]{"restorecon", a});
                            } else {
                                a(new String[]{"chcon", "u:object_r:app_data_file:s0", b});
                            }
                        }
                        z = true;
                    } catch (Exception e) {
                    }
                    try {
                        tryLock.release();
                        channel.close();
                    } catch (Exception e2) {
                    }
                } catch (Exception e3) {
                }
            }
        }
        return z;
    }

    public static boolean e() {
        return Build.VERSION.SDK_INT == 22;
    }

    public static boolean f() {
        return Build.VERSION.SDK_INT > 22;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005e A[SYNTHETIC, Splitter:B:19:0x005e] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0063 A[Catch:{ Exception -> 0x0067 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x006e A[SYNTHETIC, Splitter:B:27:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0073 A[Catch:{ Exception -> 0x0077 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007e A[SYNTHETIC, Splitter:B:35:0x007e] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0083 A[Catch:{ Exception -> 0x0087 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean g() {
        /*
            r2 = 0
            r4 = 1
            r0 = 0
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0059, InterruptedException -> 0x0069, all -> 0x0079 }
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ IOException -> 0x0059, InterruptedException -> 0x0069, all -> 0x0079 }
            r5 = 0
            java.lang.String r6 = "sh"
            r3[r5] = r6     // Catch:{ IOException -> 0x0059, InterruptedException -> 0x0069, all -> 0x0079 }
            java.lang.Process r5 = r1.exec(r3)     // Catch:{ IOException -> 0x0059, InterruptedException -> 0x0069, all -> 0x0079 }
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0059, InterruptedException -> 0x0069, all -> 0x0079 }
            java.io.OutputStream r1 = r5.getOutputStream()     // Catch:{ IOException -> 0x0059, InterruptedException -> 0x0069, all -> 0x0079 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0059, InterruptedException -> 0x0069, all -> 0x0079 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x008e, all -> 0x0089 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x008e, all -> 0x0089 }
            java.io.InputStream r7 = r5.getInputStream()     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x008e, all -> 0x0089 }
            r6.<init>(r7)     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x008e, all -> 0x0089 }
            r1.<init>(r6)     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x008e, all -> 0x0089 }
            java.lang.String r2 = "su -h\n"
            r3.writeBytes(r2)     // Catch:{ IOException -> 0x0096, InterruptedException -> 0x0091, all -> 0x008c }
            r3.flush()     // Catch:{ IOException -> 0x0096, InterruptedException -> 0x0091, all -> 0x008c }
            java.lang.String r2 = "exit\n\n"
            r3.writeBytes(r2)     // Catch:{ IOException -> 0x0096, InterruptedException -> 0x0091, all -> 0x008c }
            r3.flush()     // Catch:{ IOException -> 0x0096, InterruptedException -> 0x0091, all -> 0x008c }
            r5.waitFor()     // Catch:{ IOException -> 0x0096, InterruptedException -> 0x0091, all -> 0x008c }
            java.lang.String r2 = r1.readLine()     // Catch:{ IOException -> 0x0096, InterruptedException -> 0x0091, all -> 0x008c }
            java.lang.String r5 = "SuperSU"
            boolean r2 = r2.contains(r5)     // Catch:{ IOException -> 0x0096, InterruptedException -> 0x0091, all -> 0x008c }
            if (r2 == 0) goto L_0x004e
            r0 = r4
        L_0x004e:
            if (r3 == 0) goto L_0x0053
            r3.close()     // Catch:{ Exception -> 0x0098 }
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ Exception -> 0x0098 }
        L_0x0058:
            return r0
        L_0x0059:
            r1 = move-exception
            r1 = r2
            r3 = r2
        L_0x005c:
            if (r3 == 0) goto L_0x0061
            r3.close()     // Catch:{ Exception -> 0x0067 }
        L_0x0061:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ Exception -> 0x0067 }
            goto L_0x0058
        L_0x0067:
            r1 = move-exception
            goto L_0x0058
        L_0x0069:
            r1 = move-exception
            r1 = r2
            r3 = r2
        L_0x006c:
            if (r3 == 0) goto L_0x0071
            r3.close()     // Catch:{ Exception -> 0x0077 }
        L_0x0071:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ Exception -> 0x0077 }
            goto L_0x0058
        L_0x0077:
            r1 = move-exception
            goto L_0x0058
        L_0x0079:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x007c:
            if (r3 == 0) goto L_0x0081
            r3.close()     // Catch:{ Exception -> 0x0087 }
        L_0x0081:
            if (r1 == 0) goto L_0x0086
            r1.close()     // Catch:{ Exception -> 0x0087 }
        L_0x0086:
            throw r0
        L_0x0087:
            r1 = move-exception
            goto L_0x0086
        L_0x0089:
            r0 = move-exception
            r1 = r2
            goto L_0x007c
        L_0x008c:
            r0 = move-exception
            goto L_0x007c
        L_0x008e:
            r1 = move-exception
            r1 = r2
            goto L_0x006c
        L_0x0091:
            r2 = move-exception
            goto L_0x006c
        L_0x0093:
            r1 = move-exception
            r1 = r2
            goto L_0x005c
        L_0x0096:
            r2 = move-exception
            goto L_0x005c
        L_0x0098:
            r1 = move-exception
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.ChoiBounge.g():boolean");
    }
}
