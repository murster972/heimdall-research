package io.presage.p034goto;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* renamed from: io.presage.goto.Maxima  reason: invalid package */
public class Maxima {
    static final /* synthetic */ boolean a = (!Maxima.class.desiredAssertionStatus());

    public static <T> FutureTask<T> a(Callable<T> callable) {
        return a(new FutureTask(callable));
    }

    public static <T> FutureTask<T> a(FutureTask<T> futureTask) {
        if (a()) {
            futureTask.run();
        } else {
            b(futureTask);
        }
        return futureTask;
    }

    public static void a(Runnable runnable) {
        a(new FutureTask(runnable, (Object) null));
    }

    public static boolean a() {
        return Looper.getMainLooper() == Looper.myLooper();
    }

    public static <T> FutureTask<T> b(FutureTask<T> futureTask) {
        new Handler(Looper.getMainLooper()).post(futureTask);
        return futureTask;
    }
}
