package io.presage.p034goto;

import android.view.MotionEvent;
import android.view.View;

/* renamed from: io.presage.goto.ChangKoehan  reason: invalid package */
public class ChangKoehan implements View.OnTouchListener {
    private KyoKusanagi a;
    private int b;
    private int c = 0;

    /* renamed from: io.presage.goto.ChangKoehan$KyoKusanagi */
    public interface KyoKusanagi {
        void a(View view);
    }

    public ChangKoehan(KyoKusanagi kyoKusanagi, int i) {
        this.a = kyoKusanagi;
        this.b = i;
    }

    private void a(View view) {
        if (this.a != null) {
            this.a.a(view);
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (this.c == 0) {
                    this.c = 1;
                } else {
                    this.c = 3;
                }
                if (this.b == 2) {
                    a(view);
                    break;
                }
                break;
            case 1:
                if (this.c != 2) {
                    this.c = 0;
                    if (this.b == 0) {
                        a(view);
                    }
                } else if (this.c == 2) {
                    this.c = 0;
                } else {
                    this.c = 3;
                }
                if (this.b == 1) {
                    a(view);
                    break;
                }
                break;
            case 2:
                if (this.c != 1 && this.c != 2) {
                    this.c = 3;
                    break;
                } else {
                    this.c = 2;
                    break;
                }
                break;
            default:
                this.c = 3;
                break;
        }
        return false;
    }
}
