package io.presage.p034goto;

import android.content.Context;
import io.presage.p029char.ChangKoehan;
import io.presage.p029char.ChinGentsai;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import org.json.JSONException;
import org.json.JSONObject;
import p006do.ShingoYabuki;

/* renamed from: io.presage.goto.Goenitz  reason: invalid package */
public class Goenitz implements Shermie<Chris<KyoKusanagi>> {
    public static final String a = Goenitz.class.getSimpleName();
    private Context b;
    private ConcurrentLinkedQueue<Chris<KyoKusanagi>> c = new ConcurrentLinkedQueue<>();

    /* renamed from: io.presage.goto.Goenitz$KyoKusanagi */
    enum KyoKusanagi {
        launch
    }

    public Goenitz(Context context) {
        this.b = context;
    }

    public void a() {
        if (KyoKusanagi.a().a(this.b)) {
            a(new Chris(KyoKusanagi.launch));
        } else if (ChangKoehan.a().q()) {
            Executors.newSingleThreadExecutor().submit(new Runnable() {
                public void run() {
                    JSONObject a2 = ChangKoehan.a().k().b().a();
                    try {
                        a2.put("content", (Object) new JSONObject());
                        String d = ChangKoehan.a().k().d("launch");
                        ShingoYabuki b = ChangKoehan.a().k().b().b();
                        ChangKoehan.a().k().a(ChangKoehan.a().o());
                        ChangKoehan.a().k().a(d, b, 1, a2.toString(), new ChinGentsai() {
                            public void a(int i, String str) {
                            }

                            public void a(String str) {
                            }
                        }, (io.presage.p029char.p030do.KyoKusanagi) null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void a(Chris<KyoKusanagi> chris) {
        if (this.c == null) {
            this.c = new ConcurrentLinkedQueue<>();
        }
        if (this.c.contains(chris)) {
            this.c.remove(chris);
        }
        this.c.add(chris);
        KyoKusanagi.a().a((Object) this);
    }

    public void c() {
        if (this.c != null) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator<Chris<KyoKusanagi>> it2 = this.c.iterator();
            while (it2.hasNext()) {
                Chris next = it2.next();
                if (currentTimeMillis - next.c() >= 4500) {
                    SaishuKusanagi.b(a, String.format("Request %s %s skipped", new Object[]{this + "", ((KyoKusanagi) next.a()).name()}));
                } else {
                    SaishuKusanagi.b(a, String.format("Sending queued request %s %s", new Object[]{this + "", ((KyoKusanagi) next.a()).name()}));
                    if (next.a() == KyoKusanagi.launch) {
                        a();
                    } else {
                        SaishuKusanagi.d(a, "request type is not good");
                    }
                }
            }
            if (this.c != null) {
                this.c.clear();
            }
        }
    }
}
