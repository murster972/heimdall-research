package io.presage.p034goto;

import java.io.Serializable;

/* renamed from: io.presage.goto.RugalBernstein  reason: invalid package */
public final class RugalBernstein implements Serializable, Comparable<RugalBernstein> {
    public static final RugalBernstein a = a("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff");
    private final long b;
    private final long c;

    RugalBernstein(long j, long j2) {
        this.b = j;
        this.c = j2;
    }

    private int a(short[] sArr, int i) {
        int i2 = 0;
        while (i < sArr.length && sArr[i] == 0) {
            i2++;
            i++;
        }
        return i2;
    }

    public static RugalBernstein a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("can not parse [null]");
        }
        long[] a2 = a(str, IoriYagami.a(IoriYagami.b(str)));
        IoriYagami.a(a2);
        return IoriYagami.b(a2);
    }

    private static long[] a(String str, String str2) {
        try {
            return IoriYagami.a(str2.split(":"));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("can not parse [" + str + "]");
        }
    }

    private String d() {
        int i = (int) ((this.c & 16711680) >> 16);
        int i2 = (int) (this.c & 255);
        StringBuilder sb = new StringBuilder("::ffff:");
        sb.append((int) ((this.c & 4278190080L) >> 24)).append(".").append(i).append(".").append((int) ((this.c & 65280) >> 8)).append(".").append(i2);
        return sb.toString();
    }

    private String e() {
        boolean z = true;
        String[] f = f();
        StringBuilder sb = new StringBuilder();
        int[] c2 = c();
        int i = c2[0];
        int i2 = c2[1];
        if (i2 <= 1) {
            z = false;
        }
        for (int i3 = 0; i3 < f.length; i3++) {
            if (!z || i3 != i) {
                if (i3 <= i || i3 >= i + i2) {
                    sb.append(f[i3]);
                    if (i3 < 7) {
                        sb.append(":");
                    }
                }
            } else if (i3 == 0) {
                sb.append("::");
            } else {
                sb.append(":");
            }
        }
        return sb.toString().toLowerCase();
    }

    private String[] f() {
        short[] h = h();
        String[] strArr = new String[h.length];
        for (int i = 0; i < h.length; i++) {
            strArr[i] = String.format("%x", new Object[]{Short.valueOf(h[i])});
        }
        return strArr;
    }

    private String[] g() {
        short[] h = h();
        String[] strArr = new String[h.length];
        for (int i = 0; i < h.length; i++) {
            strArr[i] = String.format("%04x", new Object[]{Short.valueOf(h[i])});
        }
        return strArr;
    }

    private short[] h() {
        short[] sArr = new short[8];
        for (int i = 0; i < 8; i++) {
            if (IoriYagami.a(i)) {
                sArr[i] = (short) ((int) (((this.b << (i * 16)) >>> 112) & 65535));
            } else {
                sArr[i] = (short) ((int) (((this.c << (i * 16)) >>> 112) & 65535));
            }
        }
        return sArr;
    }

    /* renamed from: a */
    public int compareTo(RugalBernstein rugalBernstein) {
        if (this.b == rugalBernstein.b) {
            if (this.c == rugalBernstein.c) {
                return 0;
            }
            return !IoriYagami.a(this.c, rugalBernstein.c) ? 1 : -1;
        } else if (this.b == rugalBernstein.b) {
            return 0;
        } else {
            return !IoriYagami.a(this.b, rugalBernstein.b) ? 1 : -1;
        }
    }

    public boolean a() {
        return this.b == 0 && (this.c & -281474976710656L) == 0 && (this.c & 281470681743360L) == 281470681743360L;
    }

    public String b() {
        String[] g = g();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < g.length - 1; i++) {
            sb.append(g[i]).append(":");
        }
        sb.append(g[g.length - 1]);
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public int[] c() {
        int i = -1;
        short[] h = h();
        int i2 = 0;
        for (int i3 = 0; i3 < h.length; i3++) {
            int a2 = a(h, i3);
            if (a2 > i2) {
                i = i3;
                i2 = a2;
            }
        }
        return new int[]{i, i2};
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        RugalBernstein rugalBernstein = (RugalBernstein) obj;
        if (this.b != rugalBernstein.b) {
            return false;
        }
        return this.c == rugalBernstein.c;
    }

    public int hashCode() {
        return (((int) (this.c ^ (this.c >>> 32))) * 31) + ((int) (this.b ^ (this.b >>> 32)));
    }

    public String toString() {
        return a() ? d() : e();
    }
}
