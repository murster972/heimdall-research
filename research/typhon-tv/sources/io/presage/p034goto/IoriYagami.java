package io.presage.p034goto;

import java.util.Arrays;
import java.util.regex.Pattern;

/* renamed from: io.presage.goto.IoriYagami  reason: invalid package */
public final class IoriYagami {
    private static final Pattern a = Pattern.compile("\\.");

    public static int a(String str, char c) {
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 >= str.length()) {
                return i3;
            }
            i = str.charAt(i2) == c ? i3 + 1 : i3;
            i2++;
        }
    }

    static String a(String str) {
        if (!str.contains("::")) {
            return str;
        }
        if (str.equals("::")) {
            return b(8);
        }
        int a2 = a(str, ':');
        return str.startsWith("::") ? str.replace("::", b(9 - a2)) : str.endsWith("::") ? str.replace("::", ":" + b(9 - a2)) : str.replace("::", ":" + b(8 - a2));
    }

    static void a(long[] jArr) {
        if (jArr.length != 8) {
            throw new IllegalArgumentException("an IPv6 address should contain 8 shorts [" + Arrays.toString(jArr) + "]");
        }
        int length = jArr.length;
        int i = 0;
        while (i < length) {
            long j = jArr[i];
            if (j < 0) {
                throw new IllegalArgumentException("each element should be positive [" + Arrays.toString(jArr) + "]");
            } else if (j > 65535) {
                throw new IllegalArgumentException("each element should be less than 0xFFFF [" + Arrays.toString(jArr) + "]");
            } else {
                i++;
            }
        }
    }

    static boolean a(int i) {
        return i >= 0 && i < 4;
    }

    static boolean a(long j, long j2) {
        boolean z = true;
        boolean z2 = j < j2;
        if ((j < 0) == (j2 < 0)) {
            z = false;
        }
        return z ^ z2;
    }

    static long[] a(String[] strArr) {
        long[] jArr = new long[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            jArr[i] = Long.parseLong(strArr[i], 16);
        }
        return jArr;
    }

    static RugalBernstein b(long[] jArr) {
        long j = 0;
        int i = 0;
        long j2 = 0;
        while (true) {
            long j3 = j;
            if (i >= jArr.length) {
                return new RugalBernstein(j3, j2);
            }
            if (a(i)) {
                j = (jArr[i] << (((jArr.length - i) - 1) * 16)) | j3;
            } else {
                j2 |= jArr[i] << (((jArr.length - i) - 1) * 16);
                j = j3;
            }
            i++;
        }
    }

    public static String b(int i) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("0:");
        }
        return sb.toString();
    }

    static String b(String str) {
        if (!str.contains(".")) {
            return str;
        }
        int lastIndexOf = str.lastIndexOf(":");
        String substring = str.substring(0, lastIndexOf + 1);
        String substring2 = str.substring(lastIndexOf + 1);
        if (substring2.contains(".")) {
            String[] split = a.split(substring2);
            if (split.length != 4) {
                throw new IllegalArgumentException(String.format("can not parse [%s]", new Object[]{str}));
            }
            StringBuilder sb = new StringBuilder();
            sb.append(substring);
            int parseInt = Integer.parseInt(split[0]);
            int parseInt2 = Integer.parseInt(split[1]);
            int parseInt3 = Integer.parseInt(split[2]);
            int parseInt4 = Integer.parseInt(split[3]);
            sb.append(String.format("%02x", new Object[]{Integer.valueOf(parseInt)}));
            sb.append(String.format("%02x", new Object[]{Integer.valueOf(parseInt2)}));
            sb.append(":");
            sb.append(String.format("%02x", new Object[]{Integer.valueOf(parseInt3)}));
            sb.append(String.format("%02x", new Object[]{Integer.valueOf(parseInt4)}));
            return sb.toString();
        }
        throw new IllegalArgumentException(String.format("can not parse [%s]", new Object[]{str}));
    }
}
