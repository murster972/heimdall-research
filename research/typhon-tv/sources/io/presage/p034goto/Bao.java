package io.presage.p034goto;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import io.presage.model.Zone;
import java.util.List;

/* renamed from: io.presage.goto.Bao  reason: invalid package */
public class Bao {
    public static WindowManager.LayoutParams a(Context context, Zone zone) {
        WindowManager.LayoutParams b = b();
        Zone.Size size = zone.getSize();
        if (size != null) {
            if (size.width > 0) {
                b.width = Whip.a(context, Math.round((float) size.width));
            }
            if (size.height > 0) {
                b.height = Whip.a(context, Math.round((float) size.height));
            }
        }
        List<String> gravity = zone.getGravity();
        if (gravity != null) {
            b.gravity = 0;
            for (String next : gravity) {
                try {
                    b.gravity |= Gravity.class.getDeclaredField(next.toUpperCase()).getInt((Object) null);
                } catch (NoSuchFieldException e) {
                    SaishuKusanagi.c("ViewUtil", next + "is not a field of the class Gravity.");
                } catch (IllegalAccessException e2) {
                    SaishuKusanagi.c("ViewUtil", "Unable to access the field " + next + " of the class Gravity.");
                }
            }
        }
        Zone.Position position = zone.getPosition();
        if (position != null) {
            if (position.x > 0) {
                b.x = Whip.a(context, Math.round((float) position.x));
            }
            if (position.y > 0) {
                b.y = Whip.a(context, Math.round((float) position.y));
            }
        }
        return b;
    }

    public static FrameLayout.LayoutParams a() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 51;
        return layoutParams;
    }

    public static void a(View view, Zone zone) {
        String background = zone.getBackground();
        if (background != null) {
            view.setBackgroundColor(Color.parseColor(background));
        } else {
            view.setBackgroundColor(0);
        }
    }

    public static WindowManager.LayoutParams b() {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.width = -1;
        layoutParams.height = -1;
        layoutParams.x = 0;
        layoutParams.y = 0;
        layoutParams.gravity = 51;
        layoutParams.type = 2002;
        layoutParams.format = -3;
        layoutParams.flags = 16777248;
        layoutParams.setTitle("Load Average");
        return layoutParams;
    }

    public static FrameLayout.LayoutParams b(Context context, Zone zone) {
        FrameLayout.LayoutParams a = a();
        Zone.Size size = zone.getSize();
        if (size != null) {
            if (size.width > 0) {
                a.width = Whip.a(context, Math.round((float) size.width));
            }
            if (size.height > 0) {
                a.height = Whip.a(context, Math.round((float) size.height));
            }
        }
        List<String> gravity = zone.getGravity();
        if (gravity != null) {
            a.gravity = 0;
            for (String next : gravity) {
                try {
                    a.gravity |= Gravity.class.getDeclaredField(next.toUpperCase()).getInt((Object) null);
                } catch (NoSuchFieldException e) {
                    SaishuKusanagi.c("ViewUtil", next + "is not a field of the class Gravity.");
                } catch (IllegalAccessException e2) {
                    SaishuKusanagi.c("ViewUtil", "Unable to access the field " + next + " of the class Gravity.");
                }
            }
        }
        Zone.Margins margins = zone.getMargins();
        if (margins != null) {
            a.setMargins(Whip.a(context, margins.left), Whip.a(context, margins.top), Whip.a(context, margins.right), Whip.a(context, margins.bottom));
        }
        return a;
    }
}
