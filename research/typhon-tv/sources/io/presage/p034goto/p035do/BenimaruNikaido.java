package io.presage.p034goto.p035do;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import io.presage.provider.PresageProvider;
import java.io.IOException;
import java.util.Collection;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import p006do.Bao;
import p006do.Chris;
import p006do.Orochi;
import p006do.ShingoYabuki;
import p006do.Whip;

/* renamed from: io.presage.goto.do.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido implements ChinGentsai {
    /* access modifiers changed from: private */
    public static Context c;
    private final String a = "presage";
    /* access modifiers changed from: private */
    public Orochi b;

    /* renamed from: io.presage.goto.do.BenimaruNikaido$KyoKusanagi */
    private class KyoKusanagi extends AsyncTask<GoroDaimon, Void, Void> {
        private String b;
        private Context c;

        public KyoKusanagi(Context context, String str) {
            this.b = str;
            this.c = context;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(GoroDaimon... goroDaimonArr) {
            ShingoYabuki r2 = new ShingoYabuki.KyoKusanagi().m18386();
            String string = BenimaruNikaido.c.getSharedPreferences("crash_report", 0).getString("crash_report", (String) null);
            JSONArray jSONArray = new JSONArray();
            for (GoroDaimon goroDaimon : goroDaimonArr) {
                if (goroDaimon != null) {
                    try {
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("created_at", System.currentTimeMillis());
                        jSONObject.put("sdk_version", (Object) "2.2.11-moat");
                        jSONObject.put("api_key", (Object) PresageProvider.b(this.c));
                        jSONObject.put("aaid", (Object) BenimaruNikaido.this.b());
                        jSONObject.put("package_name", (Object) this.b);
                        jSONObject.put("package_version", (Object) goroDaimon.a());
                        jSONObject.put("phone_model", (Object) goroDaimon.b());
                        jSONObject.put("android_version", (Object) goroDaimon.c());
                        jSONObject.put("exception_type", (Object) goroDaimon.e());
                        jSONObject.put("message", (Object) goroDaimon.f());
                        jSONObject.put("stacktrace", (Object) TextUtils.join(StringUtils.LF, goroDaimon.d()));
                        if (TextUtils.join(StringUtils.LF, goroDaimon.d()).contains("presage")) {
                            jSONArray.put((Object) jSONObject);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (!(string == null || jSONArray.length() == 0)) {
                try {
                    BenimaruNikaido.this.b.m6547(new Whip.KyoKusanagi().m18396(r2).m18398(string).m18395(Bao.a(Chris.m6441("application/json; charset=utf-8"), jSONArray.toString())).m18393()).m18334();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            return null;
        }
    }

    public BenimaruNikaido(Context context) {
        c = context;
    }

    /* access modifiers changed from: private */
    public String b() {
        try {
            String a2 = io.presage.p034goto.BenimaruNikaido.a(c).a();
            if (a2 != null) {
                return a2;
            }
            throw new Exception("aaid is null");
        } catch (Exception e) {
            return io.presage.p034goto.Whip.b(c);
        }
    }

    public void a(Collection<GoroDaimon> collection, String str, Context context) {
        this.b = new Orochi.KyoKusanagi().m6553();
        new KyoKusanagi(context, str).execute(collection.toArray(new GoroDaimon[0]));
    }
}
