package io.presage.p034goto.p035do;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import io.presage.p034goto.SaishuKusanagi;
import java.io.File;
import java.lang.Thread;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

/* renamed from: io.presage.goto.do.KyoKusanagi  reason: invalid package */
public class KyoKusanagi implements Thread.UncaughtExceptionHandler {
    private static String[] e = null;
    private final Thread.UncaughtExceptionHandler a;
    private final String b;
    private final String c;
    private final boolean d;

    public KyoKusanagi(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, String str, String str2, boolean z) {
        this.a = uncaughtExceptionHandler;
        this.b = str2;
        this.d = z;
        if (this.d) {
            this.c = "DEBUG-" + str;
        } else {
            this.c = str;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:156:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00de, code lost:
        android.util.Log.i("ExceptionHandler", "file did not contain valid version" + r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r5 = new java.io.File(r24 + net.lingala.zip4j.util.InternalZipTyphoonApp.ZIP_FILE_SEPARATOR).listFiles();
        r6 = r5.length;
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x011c, code lost:
        if (r4 >= r6) goto L_0x014a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x011e, code lost:
        r7 = r5[r4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0120, code lost:
        if (r27 == false) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0122, code lost:
        android.util.Log.v("ExceptionHandler", "Deleting stack at: " + r7.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0140, code lost:
        r7.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0143, code lost:
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0146, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0147, code lost:
        r4.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01c9, code lost:
        r0.add(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01d1, code lost:
        if (r27 == false) goto L_0x01f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01d3, code lost:
        android.util.Log.d("ExceptionHandler", "Transmitting stack trace: " + android.text.TextUtils.join(org.apache.commons.lang3.StringUtils.LF, r11.d()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01f8, code lost:
        r17 = r17 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.lang.String r24, io.presage.p034goto.p035do.ChinGentsai r25, android.content.Context r26, boolean r27, java.lang.String r28) {
        /*
            if (r27 == 0) goto L_0x001e
            java.lang.String r4 = "ExceptionHandler"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r6 = "Looking for exceptions in: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            android.util.Log.d(r4, r5)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
        L_0x001e:
            java.lang.String[] r18 = a((java.lang.String) r24)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            if (r18 == 0) goto L_0x0265
            r0 = r18
            int r4 = r0.length     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            if (r4 <= 0) goto L_0x0265
            if (r27 == 0) goto L_0x004f
            java.lang.String r4 = "ExceptionHandler"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r6 = "Found "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r0 = r18
            int r6 = r0.length     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r6 = " stacktrace(s)"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            android.util.Log.d(r4, r5)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
        L_0x004f:
            java.util.ArrayList r19 = new java.util.ArrayList     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r0 = r18
            int r4 = r0.length     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r0 = r19
            r0.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r4 = 0
            r17 = r4
        L_0x005c:
            r0 = r18
            int r4 = r0.length     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r0 = r17
            if (r0 >= r4) goto L_0x025a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r0 = r24
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r5 = r18[r17]     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r4 = r4.toString()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r5 = r18[r17]     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r6 = 0
            r7 = r18[r17]     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r8 = 45
            int r7 = r7.lastIndexOf(r8)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r5 = r5.substring(r6, r7)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            if (r27 == 0) goto L_0x00b5
            java.lang.String r6 = "ExceptionHandler"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r8 = "Stacktrace in file '"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r8 = "' belongs to version "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            android.util.Log.d(r6, r7)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
        L_0x00b5:
            java.io.BufferedReader r20 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.io.FileReader r6 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r6.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r0 = r20
            r0.<init>(r6)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r14 = 0
            r12 = 0
            r11 = 0
            r4 = 0
            r13 = r12
        L_0x00cb:
            java.lang.String r15 = r20.readLine()     // Catch:{ all -> 0x01fe }
            if (r15 == 0) goto L_0x01c9
            if (r14 != 0) goto L_0x00dc
            java.lang.String r12 = "VERSION1"
            boolean r12 = r12.equals(r15)     // Catch:{ all -> 0x01fe }
            r14 = r12
            goto L_0x00cb
        L_0x00dc:
            if (r14 != 0) goto L_0x014b
            java.lang.String r4 = "ExceptionHandler"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x01fe }
            r5.<init>()     // Catch:{ all -> 0x01fe }
            java.lang.String r6 = "file did not contain valid version"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x01fe }
            java.lang.StringBuilder r5 = r5.append(r15)     // Catch:{ all -> 0x01fe }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x01fe }
            android.util.Log.i(r4, r5)     // Catch:{ all -> 0x01fe }
            r20.close()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0146 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0146 }
            r5.<init>()     // Catch:{ Exception -> 0x0146 }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x0146 }
            java.lang.String r6 = "/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0146 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0146 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0146 }
            java.io.File[] r5 = r4.listFiles()     // Catch:{ Exception -> 0x0146 }
            int r6 = r5.length     // Catch:{ Exception -> 0x0146 }
            r4 = 0
        L_0x011c:
            if (r4 >= r6) goto L_0x014a
            r7 = r5[r4]     // Catch:{ Exception -> 0x0146 }
            if (r27 == 0) goto L_0x0140
            java.lang.String r8 = "ExceptionHandler"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0146 }
            r9.<init>()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r10 = "Deleting stack at: "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0146 }
            java.lang.String r10 = r7.getAbsolutePath()     // Catch:{ Exception -> 0x0146 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0146 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0146 }
            android.util.Log.v(r8, r9)     // Catch:{ Exception -> 0x0146 }
        L_0x0140:
            r7.delete()     // Catch:{ Exception -> 0x0146 }
            int r4 = r4 + 1
            goto L_0x011c
        L_0x0146:
            r4 = move-exception
            r4.printStackTrace()
        L_0x014a:
            return
        L_0x014b:
            if (r6 != 0) goto L_0x0150
            r6 = r15
            goto L_0x00cb
        L_0x0150:
            if (r7 != 0) goto L_0x0155
            r7 = r15
            goto L_0x00cb
        L_0x0155:
            if (r8 != 0) goto L_0x015a
            r8 = r15
            goto L_0x00cb
        L_0x015a:
            if (r9 != 0) goto L_0x015f
            r9 = r15
            goto L_0x00cb
        L_0x015f:
            if (r10 != 0) goto L_0x0164
            r10 = r15
            goto L_0x00cb
        L_0x0164:
            java.lang.String r12 = "===CAUSED_BY==="
            boolean r12 = android.text.TextUtils.equals(r12, r15)     // Catch:{ all -> 0x01fe }
            if (r12 == 0) goto L_0x0174
            r12 = 1
            r15 = 0
            r13 = r12
            r10 = r15
            r8 = r15
            goto L_0x00cb
        L_0x0174:
            if (r4 != 0) goto L_0x03c3
            io.presage.goto.do.GoroDaimon r4 = new io.presage.goto.do.GoroDaimon     // Catch:{ all -> 0x01fe }
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ all -> 0x01fe }
            r11.<init>()     // Catch:{ all -> 0x01fe }
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x01fe }
            r16 = r4
            r12 = r4
        L_0x0183:
            if (r13 == 0) goto L_0x03c8
            io.presage.goto.do.GoroDaimon r4 = new io.presage.goto.do.GoroDaimon     // Catch:{ all -> 0x01fe }
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ all -> 0x01fe }
            r11.<init>()     // Catch:{ all -> 0x01fe }
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x01fe }
            r0 = r16
            r0.a(r4)     // Catch:{ all -> 0x01fe }
            r13 = 0
        L_0x0195:
            java.lang.String r11 = ","
            java.lang.String[] r11 = r15.split(r11)     // Catch:{ all -> 0x01fe }
            java.lang.StackTraceElement r15 = new java.lang.StackTraceElement     // Catch:{ all -> 0x01fe }
            r16 = 0
            r16 = r11[r16]     // Catch:{ all -> 0x01fe }
            r21 = 1
            r21 = r11[r21]     // Catch:{ all -> 0x01fe }
            r22 = 2
            r22 = r11[r22]     // Catch:{ all -> 0x01fe }
            r23 = 3
            r11 = r11[r23]     // Catch:{ all -> 0x01fe }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x01fe }
            int r11 = r11.intValue()     // Catch:{ all -> 0x01fe }
            r0 = r16
            r1 = r21
            r2 = r22
            r15.<init>(r0, r1, r2, r11)     // Catch:{ all -> 0x01fe }
            java.util.List r11 = r4.d()     // Catch:{ all -> 0x01fe }
            r11.add(r15)     // Catch:{ all -> 0x01fe }
            r11 = r12
            goto L_0x00cb
        L_0x01c9:
            r0 = r19
            r0.add(r11)     // Catch:{ all -> 0x01fe }
            r20.close()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            if (r27 == 0) goto L_0x01f8
            java.lang.String r4 = "ExceptionHandler"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r6 = "Transmitting stack trace: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r6 = "\n"
            java.util.List r7 = r11.d()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r6 = android.text.TextUtils.join(r6, r7)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            android.util.Log.d(r4, r5)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
        L_0x01f8:
            int r4 = r17 + 1
            r17 = r4
            goto L_0x005c
        L_0x01fe:
            r4 = move-exception
            r20.close()     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
            throw r4     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
        L_0x0203:
            r4 = move-exception
            if (r27 == 0) goto L_0x020f
            java.lang.String r5 = "ExceptionHandler"
            java.lang.String r6 = "didn't find any stack traces"
            android.util.Log.i(r5, r6, r4)     // Catch:{ all -> 0x0372 }
        L_0x020f:
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x02b6 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02b6 }
            r5.<init>()     // Catch:{ Exception -> 0x02b6 }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x02b6 }
            java.lang.String r6 = "/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02b6 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x02b6 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x02b6 }
            java.io.File[] r5 = r4.listFiles()     // Catch:{ Exception -> 0x02b6 }
            int r6 = r5.length     // Catch:{ Exception -> 0x02b6 }
            r4 = 0
        L_0x0230:
            if (r4 >= r6) goto L_0x014a
            r7 = r5[r4]     // Catch:{ Exception -> 0x02b6 }
            if (r27 == 0) goto L_0x0254
            java.lang.String r8 = "ExceptionHandler"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02b6 }
            r9.<init>()     // Catch:{ Exception -> 0x02b6 }
            java.lang.String r10 = "Deleting stack at: "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x02b6 }
            java.lang.String r10 = r7.getAbsolutePath()     // Catch:{ Exception -> 0x02b6 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x02b6 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x02b6 }
            android.util.Log.v(r8, r9)     // Catch:{ Exception -> 0x02b6 }
        L_0x0254:
            r7.delete()     // Catch:{ Exception -> 0x02b6 }
            int r4 = r4 + 1
            goto L_0x0230
        L_0x025a:
            r0 = r25
            r1 = r19
            r2 = r28
            r3 = r26
            r0.a(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0203, IOException -> 0x02bc, Exception -> 0x0317 }
        L_0x0265:
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x02b0 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02b0 }
            r5.<init>()     // Catch:{ Exception -> 0x02b0 }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x02b0 }
            java.lang.String r6 = "/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02b0 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x02b0 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x02b0 }
            java.io.File[] r5 = r4.listFiles()     // Catch:{ Exception -> 0x02b0 }
            int r6 = r5.length     // Catch:{ Exception -> 0x02b0 }
            r4 = 0
        L_0x0286:
            if (r4 >= r6) goto L_0x014a
            r7 = r5[r4]     // Catch:{ Exception -> 0x02b0 }
            if (r27 == 0) goto L_0x02aa
            java.lang.String r8 = "ExceptionHandler"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02b0 }
            r9.<init>()     // Catch:{ Exception -> 0x02b0 }
            java.lang.String r10 = "Deleting stack at: "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x02b0 }
            java.lang.String r10 = r7.getAbsolutePath()     // Catch:{ Exception -> 0x02b0 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x02b0 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x02b0 }
            android.util.Log.v(r8, r9)     // Catch:{ Exception -> 0x02b0 }
        L_0x02aa:
            r7.delete()     // Catch:{ Exception -> 0x02b0 }
            int r4 = r4 + 1
            goto L_0x0286
        L_0x02b0:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x014a
        L_0x02b6:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x014a
        L_0x02bc:
            r4 = move-exception
            java.lang.String r5 = "ExceptionHandler"
            java.lang.String r6 = "Problem closing files"
            android.util.Log.w(r5, r6, r4)     // Catch:{ all -> 0x0372 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0311 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0311 }
            r5.<init>()     // Catch:{ Exception -> 0x0311 }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x0311 }
            java.lang.String r6 = "/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0311 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0311 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0311 }
            java.io.File[] r5 = r4.listFiles()     // Catch:{ Exception -> 0x0311 }
            int r6 = r5.length     // Catch:{ Exception -> 0x0311 }
            r4 = 0
        L_0x02e7:
            if (r4 >= r6) goto L_0x014a
            r7 = r5[r4]     // Catch:{ Exception -> 0x0311 }
            if (r27 == 0) goto L_0x030b
            java.lang.String r8 = "ExceptionHandler"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0311 }
            r9.<init>()     // Catch:{ Exception -> 0x0311 }
            java.lang.String r10 = "Deleting stack at: "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0311 }
            java.lang.String r10 = r7.getAbsolutePath()     // Catch:{ Exception -> 0x0311 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0311 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0311 }
            android.util.Log.v(r8, r9)     // Catch:{ Exception -> 0x0311 }
        L_0x030b:
            r7.delete()     // Catch:{ Exception -> 0x0311 }
            int r4 = r4 + 1
            goto L_0x02e7
        L_0x0311:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x014a
        L_0x0317:
            r4 = move-exception
            java.lang.String r5 = "ExceptionHandler"
            java.lang.String r6 = "Something really bad just happened that we weren't expecting"
            android.util.Log.e(r5, r6, r4)     // Catch:{ all -> 0x0372 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x036c }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x036c }
            r5.<init>()     // Catch:{ Exception -> 0x036c }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x036c }
            java.lang.String r6 = "/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x036c }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x036c }
            r4.<init>(r5)     // Catch:{ Exception -> 0x036c }
            java.io.File[] r5 = r4.listFiles()     // Catch:{ Exception -> 0x036c }
            int r6 = r5.length     // Catch:{ Exception -> 0x036c }
            r4 = 0
        L_0x0342:
            if (r4 >= r6) goto L_0x014a
            r7 = r5[r4]     // Catch:{ Exception -> 0x036c }
            if (r27 == 0) goto L_0x0366
            java.lang.String r8 = "ExceptionHandler"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x036c }
            r9.<init>()     // Catch:{ Exception -> 0x036c }
            java.lang.String r10 = "Deleting stack at: "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x036c }
            java.lang.String r10 = r7.getAbsolutePath()     // Catch:{ Exception -> 0x036c }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x036c }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x036c }
            android.util.Log.v(r8, r9)     // Catch:{ Exception -> 0x036c }
        L_0x0366:
            r7.delete()     // Catch:{ Exception -> 0x036c }
            int r4 = r4 + 1
            goto L_0x0342
        L_0x036c:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x014a
        L_0x0372:
            r4 = move-exception
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x03be }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03be }
            r6.<init>()     // Catch:{ Exception -> 0x03be }
            r0 = r24
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Exception -> 0x03be }
            java.lang.String r7 = "/"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x03be }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x03be }
            r5.<init>(r6)     // Catch:{ Exception -> 0x03be }
            java.io.File[] r6 = r5.listFiles()     // Catch:{ Exception -> 0x03be }
            int r7 = r6.length     // Catch:{ Exception -> 0x03be }
            r5 = 0
        L_0x0394:
            if (r5 >= r7) goto L_0x03c2
            r8 = r6[r5]     // Catch:{ Exception -> 0x03be }
            if (r27 == 0) goto L_0x03b8
            java.lang.String r9 = "ExceptionHandler"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03be }
            r10.<init>()     // Catch:{ Exception -> 0x03be }
            java.lang.String r11 = "Deleting stack at: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x03be }
            java.lang.String r11 = r8.getAbsolutePath()     // Catch:{ Exception -> 0x03be }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x03be }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x03be }
            android.util.Log.v(r9, r10)     // Catch:{ Exception -> 0x03be }
        L_0x03b8:
            r8.delete()     // Catch:{ Exception -> 0x03be }
            int r5 = r5 + 1
            goto L_0x0394
        L_0x03be:
            r5 = move-exception
            r5.printStackTrace()
        L_0x03c2:
            throw r4
        L_0x03c3:
            r16 = r4
            r12 = r11
            goto L_0x0183
        L_0x03c8:
            r4 = r16
            goto L_0x0195
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.p035do.KyoKusanagi.a(java.lang.String, io.presage.goto.do.ChinGentsai, android.content.Context, boolean, java.lang.String):void");
    }

    public static boolean a(Context context) {
        return a(context, new BenimaruNikaido(context), false);
    }

    public static boolean a(Context context, ChinGentsai chinGentsai, final boolean z) {
        Log.i("ExceptionHandler", "Registering default exceptions handler");
        try {
            final String absolutePath = context.getDir("stacktraces", 0).getAbsolutePath();
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                final String str = packageInfo.versionName;
                boolean z2 = a(absolutePath).length > 0;
                a(absolutePath, chinGentsai, context, z, packageInfo.packageName);
                new Thread() {
                    public void run() {
                        try {
                            Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
                            if (z && defaultUncaughtExceptionHandler != null) {
                                Log.d("ExceptionHandler", "current handler class=" + defaultUncaughtExceptionHandler.getClass().getName());
                            }
                            if (!(defaultUncaughtExceptionHandler instanceof KyoKusanagi)) {
                                Thread.setDefaultUncaughtExceptionHandler(new KyoKusanagi(defaultUncaughtExceptionHandler, str, absolutePath, z));
                            }
                        } catch (OutOfMemoryError e) {
                            SaishuKusanagi.b("ExceptionHandler", "register", e);
                        } catch (Exception e2) {
                            SaishuKusanagi.b("ExceptionHandler", "register", e2);
                        }
                    }
                }.start();
                return z2;
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
                return false;
            }
        } catch (OutOfMemoryError e3) {
            SaishuKusanagi.b("ExceptionHandler", "register", e3);
        } catch (Exception e4) {
            SaishuKusanagi.b("ExceptionHandler", "register", e4);
        }
        return false;
    }

    private static String[] a(String str) {
        if (e != null) {
            return e;
        }
        File file = new File(str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
        file.mkdirs();
        e = file.list();
        if (e == null) {
            e = new String[0];
        }
        return e;
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x01cb  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01de A[SYNTHETIC, Splitter:B:47:0x01de] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01e8 A[SYNTHETIC, Splitter:B:53:0x01e8] */
    /* JADX WARNING: Removed duplicated region for block: B:69:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void uncaughtException(java.lang.Thread r13, java.lang.Throwable r14) {
        /*
            r12 = this;
            r3 = 0
            r1 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r2 = r12.b     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String[] r0 = r0.list()     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            int r0 = r0.length     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r2 = 20
            if (r0 <= r2) goto L_0x0021
            java.lang.Thread$UncaughtExceptionHandler r0 = r12.a     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            if (r0 == 0) goto L_0x001b
            java.lang.Thread$UncaughtExceptionHandler r0 = r12.a     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r0.uncaughtException(r13, r14)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
        L_0x001b:
            if (r1 == 0) goto L_0x0020
            r1.close()     // Catch:{ Exception -> 0x01ec }
        L_0x0020:
            return
        L_0x0021:
            r0 = r3
        L_0x0022:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r2.<init>()     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r4 = r12.c     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r4 = "-"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r4 = java.lang.Integer.toString(r0)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r5.<init>()     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r6 = r12.b     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r6 = "/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r5 = ".stacktrace"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            int r0 = r0 + 1
            boolean r2 = r4.exists()     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            if (r2 != 0) goto L_0x0022
            boolean r0 = r12.d     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            if (r0 == 0) goto L_0x0090
            java.lang.String r0 = "ExceptionHandler"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r2.<init>()     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r5 = "Writing unhandled exception to: "
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r5 = r4.getAbsolutePath()     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            android.util.Log.d(r0, r2)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
        L_0x0090:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.io.PrintWriter r2 = new java.io.PrintWriter     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.io.OutputStreamWriter r0 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r0.<init>(r5)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x01d2, all -> 0x01e4 }
            java.lang.String r0 = "VERSION1"
            r2.println(r0)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r0 = android.os.Build.MODEL     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r2.print(r0)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r0 = "\n"
            r2.print(r0)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r0 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r2.print(r0)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r0 = "\n"
            r2.print(r0)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.Class r0 = r14.getClass()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r0 = r0.getCanonicalName()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r2.print(r0)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r0 = "\n"
            r2.print(r0)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r0 = r13.getName()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r2.println(r0)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r0 = ""
            r0 = r14
        L_0x00d7:
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            if (r1 == 0) goto L_0x018b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r1.<init>()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r4 = " : Free["
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.Runtime r4 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            long r6 = r4.freeMemory()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r4 = "] Total["
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.Runtime r4 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            long r6 = r4.totalMemory()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r4 = "] Max["
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.Runtime r4 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            long r6 = r4.maxMemory()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r4 = "]"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
        L_0x0126:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r4.<init>()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.Class r6 = r0.getClass()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r6 = r6.getName()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r6 = " : "
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r6 = r0.getMessage()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r2.println(r1)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.StackTraceElement[] r4 = r0.getStackTrace()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            int r6 = r4.length     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r1 = r3
        L_0x0157:
            if (r1 >= r6) goto L_0x018f
            r7 = r4[r1]     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r8 = ","
            r9 = 4
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r10 = 0
            java.lang.String r11 = r7.getClassName()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r9[r10] = r11     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r10 = 1
            java.lang.String r11 = r7.getMethodName()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r9[r10] = r11     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r10 = 2
            java.lang.String r11 = r7.getFileName()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r9[r10] = r11     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r10 = 3
            int r7 = r7.getLineNumber()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r9[r10] = r7     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r7 = android.text.TextUtils.join(r8, r9)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r2.println(r7)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            int r1 = r1 + 1
            goto L_0x0157
        L_0x018b:
            java.lang.String r1 = ""
            goto L_0x0126
        L_0x018f:
            java.lang.Throwable r0 = r0.getCause()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            if (r0 == 0) goto L_0x01a6
            java.lang.String r1 = "===CAUSED_BY==="
            r2.println(r1)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.Class r1 = r0.getClass()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            java.lang.String r1 = r1.getCanonicalName()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r2.println(r1)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
        L_0x01a6:
            r2.flush()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            if (r0 != 0) goto L_0x00d7
            java.io.FileDescriptor r0 = r5.getFD()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r0.sync()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            r2.close()     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            boolean r0 = r12.d     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
            if (r0 == 0) goto L_0x01c2
            java.lang.String r0 = "ExceptionHandler"
            java.lang.String r1 = "saved stacktrace to file"
            android.util.Log.d(r0, r1, r14)     // Catch:{ Exception -> 0x01f8, all -> 0x01f3 }
        L_0x01c2:
            if (r2 == 0) goto L_0x01c7
            r2.close()     // Catch:{ Exception -> 0x01ef }
        L_0x01c7:
            java.lang.Thread$UncaughtExceptionHandler r0 = r12.a
            if (r0 == 0) goto L_0x0020
            java.lang.Thread$UncaughtExceptionHandler r0 = r12.a
            r0.uncaughtException(r13, r14)
            goto L_0x0020
        L_0x01d2:
            r0 = move-exception
        L_0x01d3:
            java.lang.String r2 = "ExceptionHandler"
            java.lang.String r3 = "Exception thrown while logging stack trace"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x01f5 }
            if (r1 == 0) goto L_0x01c7
            r1.close()     // Catch:{ Exception -> 0x01e2 }
            goto L_0x01c7
        L_0x01e2:
            r0 = move-exception
            goto L_0x01c7
        L_0x01e4:
            r0 = move-exception
            r2 = r1
        L_0x01e6:
            if (r2 == 0) goto L_0x01eb
            r2.close()     // Catch:{ Exception -> 0x01f1 }
        L_0x01eb:
            throw r0
        L_0x01ec:
            r0 = move-exception
            goto L_0x0020
        L_0x01ef:
            r0 = move-exception
            goto L_0x01c7
        L_0x01f1:
            r1 = move-exception
            goto L_0x01eb
        L_0x01f3:
            r0 = move-exception
            goto L_0x01e6
        L_0x01f5:
            r0 = move-exception
            r2 = r1
            goto L_0x01e6
        L_0x01f8:
            r0 = move-exception
            r1 = r2
            goto L_0x01d3
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.p035do.KyoKusanagi.uncaughtException(java.lang.Thread, java.lang.Throwable):void");
    }
}
