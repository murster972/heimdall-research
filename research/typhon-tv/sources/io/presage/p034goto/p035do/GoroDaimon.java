package io.presage.p034goto.p035do;

import java.util.List;

/* renamed from: io.presage.goto.do.GoroDaimon  reason: invalid package */
public class GoroDaimon {
    private final String a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private final List<StackTraceElement> g;
    private GoroDaimon h;

    public GoroDaimon(String str, String str2, String str3, String str4, String str5, String str6, List<StackTraceElement> list) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.e = str5;
        this.f = str6;
        this.g = list;
        this.d = str4;
    }

    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(GoroDaimon goroDaimon) {
        this.h = goroDaimon;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public List<StackTraceElement> d() {
        return this.g;
    }

    public String e() {
        return this.d;
    }

    public String f() {
        return this.f;
    }
}
