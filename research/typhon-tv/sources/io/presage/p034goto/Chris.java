package io.presage.p034goto;

import android.os.Bundle;
import java.lang.Enum;

/* renamed from: io.presage.goto.Chris  reason: invalid package */
public class Chris<RequestType extends Enum<RequestType>> {
    RequestType a;
    Bundle b;
    long c;

    public Chris(RequestType requesttype) {
        if (requesttype == null) {
            throw new IllegalArgumentException("type should not be null");
        }
        this.a = requesttype;
        this.c = System.currentTimeMillis();
    }

    public RequestType a() {
        return this.a;
    }

    public void a(Bundle bundle) {
        this.b = bundle;
    }

    public Bundle b() {
        return this.b;
    }

    public long c() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Chris)) {
            return false;
        }
        return this.a == ((Chris) obj).a;
    }

    public int hashCode() {
        return this.a.name().hashCode();
    }
}
