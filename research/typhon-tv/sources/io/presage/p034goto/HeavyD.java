package io.presage.p034goto;

import java.util.Map;

/* renamed from: io.presage.goto.HeavyD  reason: invalid package */
public class HeavyD {
    private String a;
    private Map<String, Object> b;

    private HeavyD(String str, Map<String, Object> map) {
        this.a = str;
        this.b = map;
    }

    public static HeavyD a(String str) {
        return a(str, (Map<String, Object>) null);
    }

    public static HeavyD a(String str, Map<String, Object> map) {
        return new HeavyD(str, map);
    }

    public String a() {
        return this.a;
    }
}
