package io.presage.p034goto;

import android.os.Build;
import java.io.File;

/* renamed from: io.presage.goto.Orochi  reason: invalid package */
public class Orochi {
    public static boolean a() {
        return b() || c() || d();
    }

    private static boolean b() {
        String str = Build.TAGS;
        return str != null && str.contains("test-keys");
    }

    private static boolean c() {
        for (String file : new String[]{"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"}) {
            if (new File(file).exists()) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0059 A[SYNTHETIC, Splitter:B:30:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005e A[Catch:{ Exception -> 0x0062 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean d() {
        /*
            r3 = 0
            r0 = 1
            r1 = 0
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ Throwable -> 0x0045, all -> 0x0054 }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0045, all -> 0x0054 }
            r5 = 0
            java.lang.String r6 = "/system/xbin/which"
            r4[r5] = r6     // Catch:{ Throwable -> 0x0045, all -> 0x0054 }
            r5 = 1
            java.lang.String r6 = "su"
            r4[r5] = r6     // Catch:{ Throwable -> 0x0045, all -> 0x0054 }
            java.lang.Process r4 = r2.exec(r4)     // Catch:{ Throwable -> 0x0045, all -> 0x0054 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x006b, all -> 0x0064 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x006b, all -> 0x0064 }
            java.io.InputStream r6 = r4.getInputStream()     // Catch:{ Throwable -> 0x006b, all -> 0x0064 }
            r5.<init>(r6)     // Catch:{ Throwable -> 0x006b, all -> 0x0064 }
            r2.<init>(r5)     // Catch:{ Throwable -> 0x006b, all -> 0x0064 }
            java.lang.String r3 = r2.readLine()     // Catch:{ Throwable -> 0x006e, all -> 0x0067 }
            if (r3 == 0) goto L_0x0039
            if (r4 == 0) goto L_0x0033
            r4.destroy()     // Catch:{ Exception -> 0x0073 }
        L_0x0033:
            if (r2 == 0) goto L_0x0038
            r2.close()     // Catch:{ Exception -> 0x0073 }
        L_0x0038:
            return r0
        L_0x0039:
            if (r4 == 0) goto L_0x003e
            r4.destroy()     // Catch:{ Exception -> 0x0071 }
        L_0x003e:
            if (r2 == 0) goto L_0x0043
            r2.close()     // Catch:{ Exception -> 0x0071 }
        L_0x0043:
            r0 = r1
            goto L_0x0038
        L_0x0045:
            r0 = move-exception
            r0 = r3
            r4 = r3
        L_0x0048:
            if (r4 == 0) goto L_0x004d
            r4.destroy()     // Catch:{ Exception -> 0x0069 }
        L_0x004d:
            if (r0 == 0) goto L_0x0052
            r0.close()     // Catch:{ Exception -> 0x0069 }
        L_0x0052:
            r0 = r1
            goto L_0x0038
        L_0x0054:
            r0 = move-exception
            r2 = r3
            r4 = r3
        L_0x0057:
            if (r4 == 0) goto L_0x005c
            r4.destroy()     // Catch:{ Exception -> 0x0062 }
        L_0x005c:
            if (r2 == 0) goto L_0x0061
            r2.close()     // Catch:{ Exception -> 0x0062 }
        L_0x0061:
            throw r0
        L_0x0062:
            r1 = move-exception
            goto L_0x0061
        L_0x0064:
            r0 = move-exception
            r2 = r3
            goto L_0x0057
        L_0x0067:
            r0 = move-exception
            goto L_0x0057
        L_0x0069:
            r0 = move-exception
            goto L_0x0052
        L_0x006b:
            r0 = move-exception
            r0 = r3
            goto L_0x0048
        L_0x006e:
            r0 = move-exception
            r0 = r2
            goto L_0x0048
        L_0x0071:
            r0 = move-exception
            goto L_0x0043
        L_0x0073:
            r1 = move-exception
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.p034goto.Orochi.d():boolean");
    }
}
