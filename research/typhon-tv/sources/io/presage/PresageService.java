package io.presage;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.mopub.common.TyphoonApp;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.HeavyD;
import io.presage.ads.NewAd;
import io.presage.finder.HeavyD;
import io.presage.finder.IFinderResult;
import io.presage.finder.model.AppUsage;
import io.presage.finder.model.Ip;
import io.presage.finder.model.Profig;
import io.presage.helper.ChinGentsai;
import io.presage.helper.GoroDaimon;
import io.presage.helper.KyoKusanagi;
import io.presage.p029char.ChangKoehan;
import io.presage.p034goto.BrianBattler;
import io.presage.p034goto.ChoiBounge;
import io.presage.p034goto.Mature;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.ShingoYabuki;
import io.presage.p034goto.YashiroNanakase;
import io.presage.p041try.BenimaruNikaido;
import io.presage.parser.p040do.BenimaruNikaido;
import io.presage.provider.PresageProvider;
import io.presage.receiver.AlarmReceiver;
import io.presage.receiver.InstallReceiver;
import io.presage.receiver.RequestReceiver;
import io.presage.receiver.SyncReceiver;
import io.presage.receiver.UpdateProfigReceiver;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.json.JSONException;
import org.json.JSONObject;

public class PresageService extends Service implements KyoKusanagi.C0049KyoKusanagi, BenimaruNikaido.GoroDaimon {
    public static final String a = PresageService.class.getSimpleName();
    KyoKusanagi b = null;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public ChangKoehan d;
    /* access modifiers changed from: private */
    public GoroDaimon e;
    /* access modifiers changed from: private */
    public io.presage.helper.KyoKusanagi f;
    private ChinGentsai g;
    /* access modifiers changed from: private */
    public AlarmManager h;
    /* access modifiers changed from: private */
    public ConcurrentMap<String, PendingIntent> i;
    private RequestReceiver j;
    private InstallReceiver k;
    /* access modifiers changed from: private */
    public HeavyD l;
    private SyncReceiver m;
    private UpdateProfigReceiver n;
    private Looper o;
    private BenimaruNikaido p;
    private io.presage.p041try.BenimaruNikaido q = null;
    private ArrayList<io.presage.p036if.KyoKusanagi> r = new ArrayList<>();
    private Map<Integer, String> s = new HashMap();
    private Map<String, KyoKusanagi> t = new HashMap();
    private boolean u = false;
    private YashiroNanakase v = null;
    private io.presage.p032else.KyoKusanagi w = null;
    /* access modifiers changed from: private */
    public BenimaruNikaido x;
    /* access modifiers changed from: private */
    public io.presage.p029char.ChinGentsai y = new io.presage.p029char.ChinGentsai() {
        public void a(int i, String str) {
            int a2;
            if (!(PresageService.this.f == null || PresageService.this.f.c() == null || (a2 = PresageService.this.f.c().e().a()) <= 7200)) {
                long currentTimeMillis = System.currentTimeMillis() - ((long) ((a2 - 7200) * 1000));
                PresageService.this.getApplicationContext().getSharedPreferences("presage", 0).edit().putLong(String.format("timing_%s_last", new Object[]{"profig"}), currentTimeMillis).commit();
            }
            if (PresageService.this.i != null && PresageService.this.i.containsKey("profig")) {
                try {
                    PresageService.this.h.cancel((PendingIntent) PresageService.this.i.get("profig"));
                    PresageService.this.i.remove("profig");
                } catch (Exception e) {
                    SaishuKusanagi.b(PresageService.a, "cancel profig alarm error", e);
                }
            }
            Intent intent = new Intent(PresageService.this, AlarmReceiver.class);
            intent.setAction("profig");
            intent.putExtra(VastExtensionXmlManager.TYPE, 1);
            intent.putExtra("service_name", "profig");
            PendingIntent broadcast = PendingIntent.getBroadcast(PresageService.this, 0, intent, 0);
            synchronized (PresageService.this.i) {
                PresageService.this.i.put("profig", broadcast);
            }
            try {
                PresageService.this.h.setInexactRepeating(3, SystemClock.elapsedRealtime() + 7200000, 7200000, broadcast);
            } catch (Exception e2) {
                SaishuKusanagi.b(PresageService.a, "un able to create profig alarm ", e2);
            }
        }

        public void a(String str) {
            JSONObject jSONObject;
            try {
                jSONObject = new JSONObject(str);
            } catch (JSONException e) {
                SaishuKusanagi.b(PresageService.a, "can't create json object from profig reply", e);
                jSONObject = null;
            }
            if (jSONObject != null) {
                PresageService.this.getApplicationContext().getSharedPreferences("presage", 0).edit().putLong(String.format("timing_%s_last", new Object[]{"profig"}), System.currentTimeMillis()).commit();
                if (jSONObject.length() > 0) {
                    new io.presage.parser.ChinGentsai("profig", PresageService.this.f).a(jSONObject);
                    return;
                }
                try {
                    if (PresageService.this.f != null) {
                        PresageService.this.f.b();
                    }
                } catch (Exception e2) {
                    SaishuKusanagi.b(PresageService.a, "load local profig error when receiving empty profig from wsback", e2);
                }
            }
        }
    };
    private BroadcastReceiver z = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (PresageService.this.c) {
                if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                    PresageService.this.d();
                } else if (intent.getAction().equals("android.intent.action.SCREEN_ON")) {
                    PresageService.this.e();
                }
            }
        }
    };

    private final class BenimaruNikaido extends Handler {
        public BenimaruNikaido(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            IFinderResult a2;
            IFinderResult iFinderResult;
            JSONObject jSONObject;
            String str;
            String str2;
            String str3;
            String str4;
            HeavyD a3;
            boolean a4;
            if (message != null) {
                if ((message.arg2 == 1 && message.getData().getString("service_name") == "profig") || message.arg2 == 6 || message.arg2 == 3) {
                    Mature.e(PresageService.this.getApplicationContext());
                }
                String[] b = Mature.b(PresageService.this.getApplicationContext());
                if (b != null) {
                    ChangKoehan.a().k().a(b);
                }
                String[] c = Mature.c(PresageService.this.getApplicationContext());
                if (c != null) {
                    ChangKoehan.a().k().b(c);
                }
                String str5 = "2.2.11-moat/" + PresageProvider.b(PresageService.this.getApplicationContext()) + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + Build.VERSION.RELEASE;
                if (!str5.isEmpty()) {
                    ChangKoehan.a().k().c(str5);
                }
                switch (message.arg2) {
                    case 1:
                        String string = message.getData().getString("service_name");
                        if (string == null) {
                            return;
                        }
                        if ((!string.equals("ip_tracker") || ShingoYabuki.a(PresageService.this.getApplicationContext(), PresageService.this.x, message.getData())) && (a3 = PresageService.this.e.a(string)) != null) {
                            IFinderResult a5 = a3.a();
                            SharedPreferences sharedPreferences = PresageService.this.getSharedPreferences("presage", 0);
                            if (sharedPreferences != null && !"profig".equals(a3.b())) {
                                sharedPreferences.edit().putLong(String.format("timing_%s_last", new Object[]{a3.b()}), System.currentTimeMillis()).commit();
                            }
                            if (a5 != null) {
                                String a6 = a5.a();
                                if (string.equals("tasks")) {
                                    ChangKoehan.a().k().a(string, 2, a6, (io.presage.p029char.ChinGentsai) null, new io.presage.p029char.p030do.BenimaruNikaido());
                                    return;
                                }
                                if ("profig".equals(string)) {
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(ChangKoehan.a().k().a("Api-Key"));
                                    sb.append(ChangKoehan.a().k().a("Sdk-Version"));
                                    sb.append(a6);
                                    SaishuKusanagi.a("TAG", "profig checksum content");
                                    SaishuKusanagi.a("TAG", sb.toString());
                                    a4 = KyoKusanagi.a(PresageService.this.getApplicationContext(), string, BrianBattler.a(sb.toString()));
                                    try {
                                        if (!Mature.f(PresageService.this.getApplicationContext())) {
                                            a4 = true;
                                        }
                                    } catch (Exception e) {
                                        a4 = true;
                                    }
                                } else {
                                    a4 = KyoKusanagi.a(PresageService.this.getApplicationContext(), string, BrianBattler.a(a6));
                                }
                                if (a4) {
                                    SaishuKusanagi.a(PresageService.a, string + " checksum changed");
                                    if (string.equals("profig")) {
                                        Profig profig = (Profig) a5;
                                        ChangKoehan.a().k().b(profig.b());
                                        Mature.d(PresageService.this.getApplicationContext(), profig.b());
                                        if (!ShingoYabuki.a(PresageService.this.getApplicationContext(), true)) {
                                            ChangKoehan.a().k().a(string, 1, a6, PresageService.this.y, (io.presage.p029char.p030do.KyoKusanagi) null);
                                            return;
                                        } else {
                                            SaishuKusanagi.b(PresageService.a, "profig reaches max number for a day");
                                            return;
                                        }
                                    } else {
                                        if ("ips".equalsIgnoreCase(string)) {
                                            Log.i(PresageService.a, "aipi");
                                        }
                                        ChangKoehan.a().k().a(string, 1, a6, (io.presage.p029char.ChinGentsai) null, new io.presage.p029char.p030do.BenimaruNikaido());
                                        return;
                                    }
                                } else {
                                    SaishuKusanagi.a(PresageService.a, string + " checksum is not changed");
                                    if (!string.equals("profig")) {
                                        return;
                                    }
                                    if (!ShingoYabuki.a(PresageService.this.getApplicationContext(), true)) {
                                        ChangKoehan.a().k().a("empty_profig", 1, new JSONObject().toString(), PresageService.this.y, (io.presage.p029char.p030do.KyoKusanagi) null);
                                        return;
                                    } else {
                                        SaishuKusanagi.b(PresageService.a, "profig reaches max number for a day");
                                        return;
                                    }
                                }
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    case 2:
                        SaishuKusanagi.d(PresageService.a, "received an Pending install");
                        PresageService.this.d.a(message.getData());
                        return;
                    case 3:
                        Bundle data = message.getData();
                        String string2 = data.getString(NotificationCompat.CATEGORY_EVENT);
                        if (string2 != null) {
                            if (string2.equals(NewAd.EVENT_INSTALL)) {
                                SaishuKusanagi.a(PresageService.a, "received an install");
                                Bundle a7 = PresageService.this.d.a(data.getString("package"));
                                if (a7 != null) {
                                    String string3 = a7.getString("bundle");
                                    String string4 = a7.getString("app_launch_package");
                                    String string5 = a7.getString("app_launch_rate");
                                    String string6 = a7.getString("app_launch_auto");
                                    String string7 = a7.getString("app_launch_class");
                                    try {
                                        jSONObject = new JSONObject(a7.getString(TtmlNode.TAG_BODY));
                                    } catch (JSONException e2) {
                                        e2.printStackTrace();
                                        jSONObject = null;
                                    }
                                    io.presage.p033for.GoroDaimon goroDaimon = new io.presage.p033for.GoroDaimon();
                                    String str6 = "";
                                    String str7 = "";
                                    String str8 = "";
                                    if (jSONObject != null) {
                                        try {
                                            str6 = jSONObject.getString("campaign");
                                            str7 = jSONObject.getString("advertiser");
                                            str8 = jSONObject.getString("advert");
                                            str = jSONObject.getString("ad_unit_id");
                                            str2 = str8;
                                            str3 = str7;
                                            str4 = str6;
                                        } catch (JSONException e3) {
                                            e3.printStackTrace();
                                            str = "";
                                            str2 = str8;
                                            str3 = str7;
                                            str4 = str6;
                                        }
                                    } else {
                                        str = "";
                                        str2 = str8;
                                        str3 = str7;
                                        str4 = str6;
                                    }
                                    goroDaimon.a(VastExtensionXmlManager.TYPE, "appinstall");
                                    io.presage.actions.KyoKusanagi a8 = io.presage.actions.GoroDaimon.a().a(PresageService.this.getApplicationContext(), ChangKoehan.a().k(), "send_event", "send_ad_event", goroDaimon);
                                    a8.a(str3, str4, str2, str);
                                    a8.k();
                                    SaishuKusanagi.a(PresageService.a, "try to launch " + string3 + " auto =" + string6 + " rate= " + string5 + " package= " + string4 + " class " + string7);
                                    if (string6.equals("true")) {
                                        int i = 0;
                                        try {
                                            i = Integer.parseInt(string5);
                                        } catch (NumberFormatException e4) {
                                        }
                                        int nextInt = new Random().nextInt(100) + 1;
                                        SaishuKusanagi.a(PresageService.a, "rate = " + i + " luck = " + nextInt);
                                        if (i >= nextInt) {
                                            io.presage.p033for.GoroDaimon goroDaimon2 = new io.presage.p033for.GoroDaimon();
                                            goroDaimon2.a(VastExtensionXmlManager.TYPE, "autolaunch");
                                            io.presage.actions.KyoKusanagi a9 = io.presage.actions.GoroDaimon.a().a(PresageService.this.getApplicationContext(), ChangKoehan.a().k(), "send_event", "send_ad_event", goroDaimon2);
                                            a9.a(str3, str4, str2, str);
                                            a9.k();
                                            Intent intent = new Intent();
                                            intent.setComponent(new ComponentName(string4, string7));
                                            intent.setFlags(268435456);
                                            PresageService.this.startActivity(intent);
                                        }
                                    }
                                }
                            }
                            String string8 = data.getString("data");
                            if (PresageService.this.f == null || PresageService.this.f.c() == null || PresageService.this.f.c().d() == null) {
                                SaishuKusanagi.d(PresageService.a, "configurationHelper, profig, enabled() is null, this should not happen");
                                return;
                            } else if (PresageService.this.f.c().d().contains(TyphoonApp.VIDEO_TRACKING_EVENTS_KEY) && PresageService.this.f.c().c()) {
                                ChangKoehan.a().k().a(NotificationCompat.CATEGORY_EVENT, 1, string8, (io.presage.p029char.ChinGentsai) null, new io.presage.p029char.p030do.BenimaruNikaido());
                                return;
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    case 4:
                        Bundle data2 = message.getData();
                        if (PresageService.this.l == null) {
                            HeavyD unused = PresageService.this.l = new HeavyD(PresageService.this.getApplicationContext());
                        }
                        String string9 = data2.getString(HeavyD.BenimaruNikaido.BLACKLIST_IPS.name());
                        String string10 = data2.getString(HeavyD.BenimaruNikaido.BLACKLIST_APPS_FOR_USAGE.name());
                        String string11 = data2.getString(HeavyD.BenimaruNikaido.WHITELIST_APPS_FOR_IPS.name());
                        if (string9 != null && !string9.isEmpty()) {
                            PresageService.this.l.a(string9);
                        }
                        if (string10 != null && !string10.isEmpty()) {
                            PresageService.this.l.b(string10);
                        }
                        if (string11 != null && !string11.isEmpty()) {
                            PresageService.this.l.c(string11);
                        }
                        PresageService.this.l.a((HeavyD.KyoKusanagi) new HeavyD.KyoKusanagi() {
                            public void a(Set<String> set) {
                                ChangKoehan.a().a(set);
                                SaishuKusanagi.a(PresageService.a, "onBlacklistIpsUpdated");
                                SaishuKusanagi.a(PresageService.a, set.toString());
                            }

                            public void b(Set<String> set) {
                                ChangKoehan.a().b(set);
                                SaishuKusanagi.a(PresageService.a, "onBlacklistAppsForUsageUpdated");
                                SaishuKusanagi.a(PresageService.a, set.toString());
                            }

                            public void c(Set<String> set) {
                                ChangKoehan.a().c(set);
                                SaishuKusanagi.a(PresageService.a, "onWhitelistAppsForIpsUpdated");
                                SaishuKusanagi.a(PresageService.a, set.toString());
                            }
                        });
                        PresageService.this.l.a();
                        return;
                    case 5:
                        if (PresageService.this.f == null || PresageService.this.f.c() == null || PresageService.this.f.c().d() == null) {
                            SaishuKusanagi.d(PresageService.a, "configurationHelper, profig, enabled() is null, this should not happen");
                            return;
                        } else if (PresageService.this.f.c().d().contains("android_accounts") && PresageService.this.f.c().c() && (iFinderResult = (IFinderResult) message.getData().getParcelable("android_accounts")) != null) {
                            String a10 = iFinderResult.a();
                            if (KyoKusanagi.a(PresageService.this.getApplicationContext(), "android_accounts", BrianBattler.a(a10))) {
                                SaishuKusanagi.a(PresageService.a, "android_accounts checksum changed");
                                ChangKoehan.a().k().a("android_accounts", 1, a10, (io.presage.p029char.ChinGentsai) null, new io.presage.p029char.p030do.BenimaruNikaido());
                                return;
                            }
                            SaishuKusanagi.a(PresageService.a, "android_accounts checksum is not changed");
                            return;
                        } else {
                            return;
                        }
                    case 6:
                        io.presage.finder.HeavyD a11 = PresageService.this.e.a("profig");
                        if (a11 != null && (a2 = a11.a()) != null) {
                            String a12 = a2.a();
                            Profig profig2 = (Profig) a2;
                            ChangKoehan.a().k().b(profig2.b());
                            Mature.d(PresageService.this.getApplicationContext(), profig2.b());
                            if (!ShingoYabuki.a(PresageService.this.getApplicationContext(), true)) {
                                ChangKoehan.a().k().a("profig", 1, a12, PresageService.this.y, (io.presage.p029char.p030do.KyoKusanagi) null);
                                return;
                            } else {
                                SaishuKusanagi.b(PresageService.a, "profig reaches max number for a day");
                                return;
                            }
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            }
        }
    }

    private class KyoKusanagi {
        public String a;
        public Date b;
        public Date c;
        private long e;
        private int f;
        private long g;

        private KyoKusanagi() {
            this.e = 0;
            this.f = 0;
            this.g = 0;
        }

        /* access modifiers changed from: private */
        public void a(long j) {
            if (this.e != 0) {
                this.g = j - this.e;
            }
            this.e = j;
        }

        public long a() {
            return this.g;
        }

        public void a(int i) {
            this.f = i;
        }

        public void a(String str) {
            this.a = str;
        }

        public int b() {
            return this.f;
        }
    }

    private void a(KyoKusanagi kyoKusanagi) {
        if (kyoKusanagi == null) {
            return;
        }
        if (this.b == null) {
            this.b = kyoKusanagi;
            this.b.b = new Date();
        } else if (this.b.a.equals(kyoKusanagi.a)) {
            this.b.c = new Date();
        } else if (!this.b.a.equals(kyoKusanagi.a)) {
            ArrayList arrayList = new ArrayList();
            this.b.c = new Date();
            AppUsage appUsage = new AppUsage();
            appUsage.a(this.b.a);
            appUsage.a(this.b.b.getTime());
            appUsage.b(this.b.c.getTime());
            arrayList.add(appUsage);
            b((ArrayList<AppUsage>) arrayList);
            this.b = kyoKusanagi;
            this.b.b = new Date();
        }
    }

    private void a(io.presage.p041try.GoroDaimon goroDaimon) {
        ArrayList arrayList = new ArrayList();
        io.presage.p036if.BenimaruNikaido a2 = io.presage.p036if.BenimaruNikaido.a(goroDaimon.b().asReadOnlyBuffer());
        Date date = new Date();
        for (int i2 = 0; i2 < a2.a(); i2++) {
            io.presage.p036if.KyoKusanagi a3 = a2.a(i2);
            if (!(this.s == null || this.s.get(Integer.valueOf(a3.b())) == null)) {
                String str = this.s.get(Integer.valueOf(a3.b()));
                if (str.contains(":")) {
                    str = this.s.get(Integer.valueOf(a3.b())).substring(0, this.s.get(Integer.valueOf(a3.b())).indexOf(58));
                }
                if ((ChangKoehan.a().t() == null || ChangKoehan.a().t().contains(str)) && (ChangKoehan.a().r() == null || !HeavyD.a(ChangKoehan.a().r(), a3.a()))) {
                    arrayList.add(new Ip(Long.valueOf(date.getTime()), a3.a()));
                    this.r.add(a3);
                }
            }
            a((ArrayList<Ip>) arrayList);
        }
    }

    private void a(String str, BenimaruNikaido.ChoiBounge choiBounge) {
        int f2;
        char c2 = 65535;
        switch (str.hashCode()) {
            case -979812809:
                if (str.equals("profig")) {
                    c2 = 4;
                    break;
                }
                break;
            case -535854154:
                if (str.equals("android_accounts")) {
                    c2 = 1;
                    break;
                }
                break;
            case 104492:
                if (str.equals("ips")) {
                    c2 = 3;
                    break;
                }
                break;
            case 3000946:
                if (str.equals("apps")) {
                    c2 = 0;
                    break;
                }
                break;
            case 110132110:
                if (str.equals("tasks")) {
                    c2 = 5;
                    break;
                }
                break;
            case 1282903444:
                if (str.equals("apps_usage")) {
                    c2 = 2;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                f2 = choiBounge.b();
                break;
            case 1:
                f2 = choiBounge.e();
                break;
            case 2:
                f2 = choiBounge.d();
                break;
            case 3:
                f2 = choiBounge.c();
                break;
            case 4:
                f2 = choiBounge.a();
                break;
            case 5:
                f2 = choiBounge.f();
                break;
            default:
                f2 = 0;
                break;
        }
        if (f2 != 0) {
            long j2 = getSharedPreferences("presage", 0).getLong(String.format("timing_%s_last", new Object[]{str}), 0);
            Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
            intent.setAction(str);
            intent.putExtra(VastExtensionXmlManager.TYPE, 1);
            intent.putExtra("service_name", str);
            PendingIntent broadcast = PendingIntent.getBroadcast(this, 0, intent, 0);
            synchronized (this.i) {
                if (!(str == null || broadcast == null)) {
                    this.i.put(str, broadcast);
                }
            }
            long currentTimeMillis = System.currentTimeMillis();
            if ("profig".equals(str)) {
                if (j2 == 0) {
                    sendBroadcast(intent);
                    SaishuKusanagi.a(a, "no lastExecution for profig in SharePrefs, sent broadcast");
                    return;
                }
                io.presage.provider.KyoKusanagi kyoKusanagi = new io.presage.provider.KyoKusanagi(getApplicationContext().getContentResolver());
                if (kyoKusanagi != null) {
                    Cursor a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.PROFIG_JSON, PresageProvider.a(getApplicationContext())), (String[]) null, (String) null, (String[]) null, (String) null);
                    if (a2 == null) {
                        a2 = kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.PROFIG_JSON, PresageProvider.a(getApplicationContext())), (String[]) null, (String) null, (String[]) null, (String) null);
                    }
                    String str2 = null;
                    if (a2 != null) {
                        if (a2.moveToLast()) {
                            str2 = a2.getString(a2.getColumnIndex("v"));
                        }
                        a2.close();
                    }
                    if (str2 == null || str2.isEmpty()) {
                        SaishuKusanagi.a(a, "no raw profig in db, sent broadcast");
                        sendBroadcast(intent);
                        return;
                    }
                }
            }
            if (j2 != 0 && currentTimeMillis > j2) {
                long j3 = ((long) (f2 * 1000)) - (currentTimeMillis - j2);
                try {
                    this.h.setInexactRepeating(3, j3 < 0 ? 0 : SystemClock.elapsedRealtime() + j3, (long) (f2 * 1000), broadcast);
                } catch (Exception e2) {
                    SaishuKusanagi.d("service_name", e2.toString());
                }
            } else if (f2 == -1) {
                sendBroadcast(intent);
            } else {
                try {
                    this.h.setInexactRepeating(3, 0, (long) (f2 * 1000), broadcast);
                } catch (Exception e3) {
                    SaishuKusanagi.d("service_name", e3.toString());
                }
            }
        }
    }

    private void a(ArrayList<Ip> arrayList) {
        if (this.f == null || this.f.c() == null || this.f.c().h() == null) {
            SaishuKusanagi.d(a, "configurationHelper, profig, getMaxsize() is null, this should not happen");
            return;
        }
        this.x.a(arrayList);
        if (this.x.a("Ips") > ((long) this.f.c().h().a())) {
            Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
            intent.setAction("ips");
            intent.putExtra(VastExtensionXmlManager.TYPE, 1);
            intent.putExtra("service_name", "ips");
            sendBroadcast(intent);
        }
    }

    private void b() {
        io.presage.p034goto.p035do.KyoKusanagi.a(getApplicationContext());
        this.g = new ChinGentsai();
        this.g.a(getApplicationContext(), getApplicationContext().getPackageName());
        this.i = new ConcurrentHashMap();
        this.h = (AlarmManager) getSystemService(NotificationCompat.CATEGORY_ALARM);
        this.d = new ChangKoehan();
        this.e = new GoroDaimon(new io.presage.finder.ChangKoehan(getApplicationContext(), this.g));
        HandlerThread handlerThread = new HandlerThread("ServiceStartArguments", 10);
        handlerThread.start();
        this.o = handlerThread.getLooper();
        this.p = new BenimaruNikaido(this.o);
        ChangKoehan.a().a(getApplicationContext(), ChangKoehan.BenimaruNikaido.SERVICE_PROCESS, new ChangKoehan.KyoKusanagi() {
            public void a() {
                Cursor a2 = new io.presage.provider.KyoKusanagi(PresageService.this.getApplicationContext().getContentResolver()).a(PresageProvider.a(PresageProvider.KyoKusanagi.AAID, PresageProvider.a(PresageService.this.getApplicationContext())), (String[]) null, (String) null, (String[]) null, (String) null);
                if (a2 == null) {
                    a2 = new io.presage.provider.KyoKusanagi(PresageService.this.getApplicationContext().getContentResolver()).a(PresageProvider.a(PresageProvider.KyoKusanagi.AAID, PresageProvider.a(PresageService.this.getApplicationContext())), (String[]) null, (String) null, (String[]) null, (String) null);
                }
                String str = "";
                if (a2 != null) {
                    if (a2.moveToLast()) {
                        str = a2.getString(a2.getColumnIndex("v"));
                        SaishuKusanagi.a(PresageService.a, "aaid: " + str);
                    }
                    a2.close();
                    if (str != "" && !str.equals(ChangKoehan.a().n())) {
                        Mature.d(PresageService.this.getApplicationContext());
                    }
                }
                ChangKoehan.a().k();
                io.presage.helper.KyoKusanagi unused = PresageService.this.f = new io.presage.helper.KyoKusanagi(PresageService.this, PresageService.this);
                HeavyD unused2 = PresageService.this.l = new HeavyD(PresageService.this.getApplicationContext());
                ChangKoehan a3 = ChangKoehan.a();
                HeavyD unused3 = PresageService.this.l;
                a3.b(HeavyD.a(PresageService.this.getApplicationContext(), HeavyD.BenimaruNikaido.BLACKLIST_APPS_FOR_USAGE));
                ChangKoehan a4 = ChangKoehan.a();
                HeavyD unused4 = PresageService.this.l;
                a4.a(HeavyD.a(PresageService.this.getApplicationContext(), HeavyD.BenimaruNikaido.BLACKLIST_IPS));
                ChangKoehan a5 = ChangKoehan.a();
                HeavyD unused5 = PresageService.this.l;
                a5.c(HeavyD.a(PresageService.this.getApplicationContext(), HeavyD.BenimaruNikaido.WHITELIST_APPS_FOR_IPS));
                PresageService.this.h();
            }
        });
    }

    private void b(io.presage.parser.p040do.BenimaruNikaido benimaruNikaido) {
        BenimaruNikaido.ChoiBounge e2 = benimaruNikaido.e();
        synchronized (this.i) {
            for (String str : this.i.keySet()) {
                try {
                    this.h.cancel((PendingIntent) this.i.get(str));
                } catch (Exception e3) {
                    SaishuKusanagi.d("service_name", e3.toString());
                }
            }
            this.i.clear();
        }
        this.e.a(benimaruNikaido);
        a("profig", e2);
        a("apps", e2);
        a("android_accounts", e2);
        a("apps_usage", e2);
        a("ips", e2);
        a("tasks", e2);
    }

    private void b(io.presage.p041try.GoroDaimon goroDaimon) {
        io.presage.p036if.ChinGentsai a2 = io.presage.p036if.ChinGentsai.a(goroDaimon.b().asReadOnlyBuffer());
        this.s.clear();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < a2.a()) {
                io.presage.p036if.GoroDaimon a3 = a2.a(i3);
                if (ChangKoehan.a().s() == null || !ChangKoehan.a().s().contains(a3.a())) {
                    if (!this.t.containsKey(a3.a())) {
                        this.t.put(a3.a(), new KyoKusanagi());
                    }
                    this.t.get(a3.a()).a = a3.a();
                    this.t.get(a3.a()).a(a3.d());
                    this.t.get(a3.a()).a(a3.c());
                }
                if (!this.v.a(a3.a())) {
                    this.v.a(a3.c(), a3.b(), a3.a());
                }
                this.s.put(Integer.valueOf(a3.c()), a3.a());
                i2 = i3 + 1;
            } else {
                k();
                return;
            }
        }
    }

    private void b(ArrayList<AppUsage> arrayList) {
        if (this.f == null || this.f.c() == null || this.f.c().h() == null) {
            SaishuKusanagi.d(a, "configurationHelper, profig, getMaxsize() is null, this should not happen");
            return;
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= arrayList.size()) {
                break;
            }
            if (ChangKoehan.a().s() == null || !ChangKoehan.a().s().contains(arrayList.get(i3).b())) {
                this.x.a(arrayList.get(i3));
            }
            i2 = i3 + 1;
        }
        if (this.x.a("Apps") > ((long) this.f.c().h().b())) {
            Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
            intent.setAction("apps_usage");
            intent.putExtra(VastExtensionXmlManager.TYPE, 1);
            intent.putExtra("service_name", "apps_usage");
            sendBroadcast(intent);
        }
    }

    private void c() {
        if (!this.u) {
            g();
            this.u = true;
        }
        e();
    }

    private void c(io.presage.parser.p040do.BenimaruNikaido benimaruNikaido) {
        if (benimaruNikaido.i() != null) {
            Intent intent = new Intent();
            intent.setAction("io.presage.receiver.SyncReceiver.SYNC");
            if (benimaruNikaido.i().a() != null && !benimaruNikaido.i().a().isEmpty()) {
                intent.putExtra(HeavyD.BenimaruNikaido.BLACKLIST_IPS.name(), benimaruNikaido.i().a());
            }
            if (benimaruNikaido.i().b() != null && !benimaruNikaido.i().b().isEmpty()) {
                intent.putExtra(HeavyD.BenimaruNikaido.BLACKLIST_APPS_FOR_USAGE.name(), benimaruNikaido.i().b());
            }
            if (benimaruNikaido.i().c() != null && !benimaruNikaido.i().c().isEmpty()) {
                intent.putExtra(HeavyD.BenimaruNikaido.WHITELIST_APPS_FOR_IPS.name(), benimaruNikaido.i().c());
            }
            sendBroadcast(intent);
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        this.q.a((BenimaruNikaido.GoroDaimon) this);
        if (this.b != null) {
            KyoKusanagi kyoKusanagi = this.b;
            kyoKusanagi.a("");
            a(kyoKusanagi);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        byte[] f2 = f();
        this.q.a((BenimaruNikaido.GoroDaimon) this);
        this.q.a(f2, 0, this);
    }

    private byte[] f() {
        return new byte[]{5, 4};
    }

    private void g() {
        registerReceiver(this.z, new IntentFilter("android.intent.action.SCREEN_ON"));
        registerReceiver(this.z, new IntentFilter("android.intent.action.SCREEN_OFF"));
    }

    /* access modifiers changed from: private */
    public void h() {
        this.j = new RequestReceiver();
        registerReceiver(this.j, new IntentFilter("presage_request"));
        this.k = new InstallReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addCategory("android.intent.category.DEFAULT");
        intentFilter.addDataScheme("package");
        registerReceiver(this.k, intentFilter);
        this.m = new SyncReceiver();
        registerReceiver(this.m, new IntentFilter("io.presage.receiver.SyncReceiver.SYNC"));
        this.n = new UpdateProfigReceiver();
        registerReceiver(this.n, new IntentFilter("io.presage.receiver.action.UPDATE_PROFIG"));
    }

    private void i() {
        try {
            if (this.j != null) {
                unregisterReceiver(this.j);
            }
        } catch (Exception e2) {
            SaishuKusanagi.b("PresageService", e2.getMessage(), e2);
        } finally {
            this.j = null;
        }
        try {
            if (this.k != null) {
                unregisterReceiver(this.k);
            }
        } catch (Exception e3) {
            SaishuKusanagi.b("PresageService", e3.getMessage(), e3);
        } finally {
            this.k = null;
        }
        try {
            if (this.m != null) {
                unregisterReceiver(this.m);
            }
        } catch (Exception e4) {
            SaishuKusanagi.b("PresageService", e4.getMessage(), e4);
        } finally {
            this.m = null;
        }
        try {
            if (this.z != null) {
                unregisterReceiver(this.z);
            }
        } catch (Exception e5) {
            SaishuKusanagi.b("PresageService", e5.getMessage(), e5);
        } finally {
            this.z = null;
        }
        try {
            if (this.n != null) {
                unregisterReceiver(this.n);
            }
        } catch (Exception e6) {
            SaishuKusanagi.b("PresageService", e6.getMessage(), e6);
        } finally {
            this.n = null;
        }
    }

    private boolean j() {
        if (this.f != null && this.f.c() != null) {
            return this.f.c().d().contains("ips") || this.f.c().d().contains("apps_usage");
        }
        SaishuKusanagi.d(a, "configurationHelper, profig is null");
        return false;
    }

    private void k() {
        KyoKusanagi kyoKusanagi;
        long j2 = 0;
        KyoKusanagi kyoKusanagi2 = null;
        if (this.t != null) {
            Iterator<Map.Entry<String, KyoKusanagi>> it2 = this.t.entrySet().iterator();
            while (it2.hasNext()) {
                Map.Entry next = it2.next();
                if (!((KyoKusanagi) next.getValue()).a.equals((getApplicationContext() == null || getApplicationContext().getPackageName() == null) ? "null" : getApplicationContext().getPackageName().concat(":remote")) && !((KyoKusanagi) next.getValue()).a.contains("/system/bin/") && !((KyoKusanagi) next.getValue()).a.contains("/system/sbin/")) {
                    kyoKusanagi = (KyoKusanagi) next.getValue();
                    if (!this.s.containsKey(Integer.valueOf(kyoKusanagi.b()))) {
                        it2.remove();
                    }
                    if (kyoKusanagi.a() > j2) {
                        j2 = kyoKusanagi.a();
                        kyoKusanagi2 = kyoKusanagi;
                    }
                }
                kyoKusanagi = kyoKusanagi2;
                kyoKusanagi2 = kyoKusanagi;
            }
        }
        a(kyoKusanagi2);
    }

    public void a() {
        SaishuKusanagi.a(a, "onForce");
        sendBroadcast(new Intent("io.presage.receiver.action.UPDATE_PROFIG"));
    }

    public void a(io.presage.parser.p040do.BenimaruNikaido benimaruNikaido) {
        b(benimaruNikaido);
        c(benimaruNikaido);
    }

    public void a(byte[] bArr) {
        int i2 = 0;
        if (!this.c || !ChoiBounge.a()) {
            return;
        }
        if (!j()) {
            SaishuKusanagi.d(a, "onRecvData1: can't send a request to binary");
        } else if (bArr == null) {
            this.q.a(new byte[]{4, 5}, this.w.a(), this);
        } else {
            while (!this.r.isEmpty()) {
                this.r.remove(0);
            }
            this.r.clear();
            io.presage.p041try.ChinGentsai a2 = io.presage.p041try.ChinGentsai.a(ByteBuffer.wrap(bArr));
            while (i2 < a2.a()) {
                try {
                    io.presage.p041try.GoroDaimon a3 = a2.a(i2);
                    if (a3.a() == 4) {
                        b(a3);
                    } else if (a3.a() == 5) {
                        a(a3);
                    }
                    i2++;
                } catch (Exception e2) {
                    SaishuKusanagi.b(a, e2.getMessage(), e2);
                }
            }
            if (j()) {
                this.q.a(new byte[]{5, 4}, this.w.a(), this);
                return;
            }
            SaishuKusanagi.d(a, "onRecvData2: can't send a request to binary");
        }
    }

    public IBinder onBind(Intent intent) {
        SaishuKusanagi.a(a, "onBind");
        return null;
    }

    public void onConfigurationChanged(Configuration configuration) {
        SaishuKusanagi.a(a, "onConfigurationChanged");
        super.onConfigurationChanged(configuration);
    }

    public void onCreate() {
        super.onCreate();
        SaishuKusanagi.a(a, "onCreate");
        try {
            b();
            this.c = true;
        } catch (Exception e2) {
            this.c = false;
            SaishuKusanagi.b(a, "presage service failed to initialize", e2);
        } catch (OutOfMemoryError e3) {
            this.c = false;
            SaishuKusanagi.b(a, "presage service failed to initialize", e3);
        }
        if (this.c) {
            this.x = BenimaruNikaido.a((Context) this);
            if (ChoiBounge.a()) {
                this.v = YashiroNanakase.a(getApplicationContext());
                this.w = io.presage.p032else.KyoKusanagi.a((Context) this);
                io.presage.p041try.BenimaruNikaido.a((Context) this);
                this.q = io.presage.p041try.BenimaruNikaido.a();
                c();
                return;
            }
            SaishuKusanagi.b(a, "cpu is not arm");
            return;
        }
        try {
            stopSelf();
            Process.killProcess(Process.myPid());
        } catch (OutOfMemoryError e4) {
            SaishuKusanagi.b(a, "stopSelf", e4);
        } catch (Exception e5) {
            SaishuKusanagi.b(a, "stopSelf", e5);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        SaishuKusanagi.a(a, "onDestroy");
        try {
            synchronized (this.i) {
                for (String str : this.i.keySet()) {
                    try {
                        this.h.cancel((PendingIntent) this.i.get(str));
                    } catch (Exception e2) {
                        SaishuKusanagi.d("service_name", e2.toString());
                    }
                }
                this.i.clear();
            }
            i();
        } catch (Exception e3) {
            SaishuKusanagi.b(a, "onDestroy -> cancel all error", e3);
        }
    }

    public void onLowMemory() {
        SaishuKusanagi.a(a, "onLowMemory");
        super.onLowMemory();
    }

    public void onRebind(Intent intent) {
        SaishuKusanagi.a(a, "onRebind");
        super.onRebind(intent);
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        SaishuKusanagi.b(a, "onStartCommand");
        if (!this.c) {
            return 2;
        }
        if (intent != null) {
            Bundle extras = intent.getExtras();
            SaishuKusanagi.a(a, "onReceive");
            if (extras != null && extras.size() > 0) {
                Message obtainMessage = this.p.obtainMessage();
                obtainMessage.arg1 = i3;
                obtainMessage.arg2 = extras.getInt(VastExtensionXmlManager.TYPE);
                obtainMessage.setData(extras);
                this.p.sendMessage(obtainMessage);
            }
        }
        return 1;
    }

    public void onTaskRemoved(Intent intent) {
        SaishuKusanagi.a(a, "onTaskRemoved");
        super.onTaskRemoved(intent);
    }

    public void onTrimMemory(int i2) {
        SaishuKusanagi.a(a, "onTrimMemory");
        super.onTrimMemory(i2);
    }

    public boolean onUnbind(Intent intent) {
        SaishuKusanagi.a(a, "onUnbind");
        return super.onUnbind(intent);
    }
}
