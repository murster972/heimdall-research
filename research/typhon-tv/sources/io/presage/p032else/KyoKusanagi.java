package io.presage.p032else;

import android.content.Context;
import java.util.UUID;

/* renamed from: io.presage.else.KyoKusanagi  reason: invalid package */
public class KyoKusanagi {
    private static KyoKusanagi c = null;
    private int a = 15;
    private BenimaruNikaido b = null;

    private KyoKusanagi(Context context) {
        this.b = new BenimaruNikaido(context.getApplicationContext());
    }

    public static KyoKusanagi a(Context context) {
        if (c == null) {
            c = new KyoKusanagi(context.getApplicationContext());
        }
        return c;
    }

    public int a() {
        return this.a;
    }

    public void a(String str) {
        this.b.b("token", str);
    }

    public boolean b() {
        return false;
    }

    public String c() {
        String a2 = this.b.a("token", "");
        if (a2 != null && !a2.isEmpty()) {
            return a2;
        }
        String uuid = UUID.randomUUID().toString();
        a(uuid);
        return uuid;
    }
}
