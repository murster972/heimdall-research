package io.presage.p032else;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.provider.PresageProvider;
import java.util.HashMap;
import java.util.Map;

/* renamed from: io.presage.else.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido {
    private io.presage.provider.KyoKusanagi a = null;
    private Map<String, Boolean> b = new HashMap();
    private Map<String, String> c = new HashMap();
    private Context d;

    /* renamed from: io.presage.else.BenimaruNikaido$KyoKusanagi */
    private final class KyoKusanagi extends ContentObserver {
        public KyoKusanagi(Handler handler) {
            super(handler);
        }

        public void onChange(boolean z) {
            super.onChange(z);
            BenimaruNikaido.this.a();
        }
    }

    public BenimaruNikaido(Context context) {
        this.d = context;
        this.a = new io.presage.provider.KyoKusanagi(context.getContentResolver());
        this.a.a(PresageProvider.a(PresageProvider.KyoKusanagi.SETTING, PresageProvider.a(context)), true, (ContentObserver) new KyoKusanagi(new Handler()));
    }

    private boolean a(String str) {
        Cursor cursor;
        try {
            cursor = this.a.a(PresageProvider.a(PresageProvider.KyoKusanagi.SETTING, PresageProvider.a(this.d)), new String[]{"id"}, "id=?", new String[]{str}, (String) null);
        } catch (Exception e) {
            cursor = null;
        }
        if (cursor == null) {
            return false;
        }
        int count = cursor.getCount();
        cursor.close();
        return count != 0;
    }

    private String b(String str) {
        Cursor cursor;
        if (this.b.containsKey(str) && !this.b.get(str).booleanValue()) {
            return this.c.get(str);
        }
        try {
            cursor = this.a.a(PresageProvider.a(PresageProvider.KyoKusanagi.SETTING, PresageProvider.a(this.d)), new String[]{"id", "value"}, "id=?", new String[]{str}, (String) null);
        } catch (Exception e) {
            cursor = null;
        }
        if (cursor == null) {
            return null;
        }
        if (cursor.getCount() == 0) {
            cursor.close();
            return null;
        }
        cursor.moveToFirst();
        String string = cursor.getString(cursor.getColumnIndex("value"));
        cursor.close();
        this.c.put(str, string);
        this.b.put(str, false);
        return string;
    }

    private boolean c(String str, String str2) {
        Uri uri;
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", str);
        contentValues.put("value", str2);
        try {
            uri = this.a.a(PresageProvider.a(PresageProvider.KyoKusanagi.SETTING, PresageProvider.a(this.d)), contentValues);
        } catch (IllegalArgumentException e) {
            SaishuKusanagi.a("SettingsHelper", e.toString());
            uri = null;
        }
        return uri != null && !uri.getLastPathSegment().equals("noExist");
    }

    private boolean d(String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", str);
        contentValues.put("value", str2);
        return this.a.a(PresageProvider.a(PresageProvider.KyoKusanagi.SETTING, PresageProvider.a(this.d)), contentValues, "id=?", new String[]{str}) > 0;
    }

    public String a(String str, String str2) {
        return a(str) ? b(str) : str2;
    }

    public void a() {
        for (Map.Entry<String, Boolean> value : this.b.entrySet()) {
            value.setValue(true);
        }
    }

    public void b(String str, String str2) {
        if (a(str)) {
            d(str, str2);
        } else {
            c(str, str2);
        }
    }
}
