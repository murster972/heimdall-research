package io.presage.activities.p026do;

import io.presage.activities.PresageActivity;
import io.presage.ads.ChangKoehan;

/* renamed from: io.presage.activities.do.GoroDaimon  reason: invalid package */
public class GoroDaimon {
    private static GoroDaimon a = null;

    /* renamed from: io.presage.activities.do.GoroDaimon$KyoKusanagi */
    public enum KyoKusanagi {
        ACTIVITY_HELPER_TYPE_WEB_VIEW,
        ACTIVITY_HELPER_TYPE_VIDEO
    }

    private GoroDaimon() {
    }

    public static GoroDaimon a() {
        if (a == null) {
            a = new GoroDaimon();
        }
        return a;
    }

    public ChinGentsai a(PresageActivity presageActivity, BenimaruNikaido benimaruNikaido, ChangKoehan changKoehan) {
        switch (a(changKoehan)) {
            case ACTIVITY_HELPER_TYPE_WEB_VIEW:
                return new ChangKoehan(KyoKusanagi.ACTIVITY_HELPER_TYPE_WEB_VIEW, presageActivity, benimaruNikaido, changKoehan);
            default:
                return null;
        }
    }

    public KyoKusanagi a(ChangKoehan changKoehan) {
        return KyoKusanagi.ACTIVITY_HELPER_TYPE_WEB_VIEW;
    }
}
