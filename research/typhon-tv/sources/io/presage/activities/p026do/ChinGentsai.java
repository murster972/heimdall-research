package io.presage.activities.p026do;

import android.widget.FrameLayout;
import io.presage.activities.p026do.GoroDaimon;
import io.presage.activities.p026do.KyoKusanagi;

/* renamed from: io.presage.activities.do.ChinGentsai  reason: invalid package */
public interface ChinGentsai {
    GoroDaimon.KyoKusanagi a();

    void a(boolean z);

    KyoKusanagi.C0039KyoKusanagi b();

    void c();

    void d();

    FrameLayout g();

    FrameLayout.LayoutParams h();

    void i();

    void j();

    void k();
}
