package io.presage.activities.p026do;

import android.annotation.SuppressLint;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Chris;
import com.p003if.p004do.Goenitz;
import com.p003if.p004do.SaishuKusanagi;
import io.presage.activities.PresageActivity;
import io.presage.activities.p026do.GoroDaimon;
import io.presage.ads.NewAd;
import io.presage.p033for.GoroDaimon;

/* renamed from: io.presage.activities.do.ChangKoehan  reason: invalid package */
public class ChangKoehan extends KyoKusanagi {
    private WebView c;
    private String d;
    private FrameLayout e;
    private FrameLayout.LayoutParams f;

    /* renamed from: io.presage.activities.do.ChangKoehan$BenimaruNikaido */
    public class BenimaruNikaido {
        private io.presage.formats.KyoKusanagi b;
        private GoroDaimon c;

        public BenimaruNikaido(io.presage.ads.ChangKoehan changKoehan) {
            this.b = changKoehan.h();
            this.c = changKoehan.h().h();
        }

        @JavascriptInterface
        public String getAd() {
            try {
                return ChangKoehan.this.e() instanceof io.presage.ads.GoroDaimon ? ((io.presage.ads.GoroDaimon) ChangKoehan.this.e()).c().toString() : "";
            } catch (Exception e) {
                return "";
            }
        }

        @JavascriptInterface
        public String param(String str) {
            if (this.c.a(str) == null) {
                return new ChoiBounge().m14078(this.c.a(str));
            }
            try {
                SaishuKusanagi r0 = new Goenitz().m14115(this.c.a(str).toString());
                return r0.m14140() ? r0.m14139().toString() : r0.m14136() ? r0.m14137().toString() : r0.m14141() ? r0.m14138().toString() : r0.m14142() ? r0.m14147().toString() : new ChoiBounge().m14078(this.c.a(str));
            } catch (Chris e) {
                return new ChoiBounge().m14078(this.c.a(str));
            }
        }

        @JavascriptInterface
        public void sendAction(String str) {
            io.presage.p034goto.SaishuKusanagi.b("WebViewActivityHelper", String.format("%s %s %s", new Object[]{"PresageActivity", "sendAction:", str}));
            if (str.equals("close") || str.equals(NewAd.EVENT_CANCEL)) {
                ChangKoehan.this.a(str);
                if (str.equals("close")) {
                    this.b.a().b("home");
                }
            }
            this.b.a().b(str);
        }

        @JavascriptInterface
        public String stringParam(String str) {
            return this.c.b(str);
        }
    }

    /* renamed from: io.presage.activities.do.ChangKoehan$KyoKusanagi */
    class KyoKusanagi extends WebChromeClient {
        public KyoKusanagi() {
        }

        public void onConsoleMessage(String str, int i, String str2) {
            io.presage.p034goto.SaishuKusanagi.a("WebViewActivityHelper", String.format("%s %s %s %s %s %s", new Object[]{"PresageActivity", str, "-- From line", Integer.toString(i), "of", str2}));
        }
    }

    public ChangKoehan(GoroDaimon.KyoKusanagi kyoKusanagi, PresageActivity presageActivity, BenimaruNikaido benimaruNikaido, io.presage.ads.ChangKoehan changKoehan) {
        super(kyoKusanagi, presageActivity, benimaruNikaido, changKoehan);
        if (changKoehan.h() != null) {
            this.d = (String) changKoehan.h().a("webview_url");
        }
    }

    public void c() {
        super.c();
        if (this.c != null) {
            this.c.loadUrl("about:blank");
            this.e.removeAllViews();
            this.c.destroy();
            this.c = null;
        }
    }

    public FrameLayout g() {
        return this.e;
    }

    public FrameLayout.LayoutParams h() {
        return this.f;
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    public void i() {
        if (f() == null) {
            io.presage.p034goto.SaishuKusanagi.d("WebViewActivityHelper", "PresageActivity is destroyed ???");
            return;
        }
        this.e = new FrameLayout(f());
        this.e.setBackgroundColor(0);
        this.f = new FrameLayout.LayoutParams(-1, -1);
        this.f.setMargins(0, 0, 0, 0);
        this.c = new WebView(f());
        this.c.setBackgroundColor(0);
        this.c.clearHistory();
        this.c.clearAnimation();
        this.c.getSettings().setJavaScriptEnabled(true);
        this.c.addJavascriptInterface(new BenimaruNikaido(e()), "Presage");
        this.c.setWebChromeClient(new KyoKusanagi());
        this.c.setWebViewClient(new WebViewClient());
        this.c.setVerticalScrollBarEnabled(false);
        this.c.setHorizontalScrollBarEnabled(false);
        this.c.loadUrl(this.d);
        this.e.addView(this.c);
        this.a.a(NewAd.EVENT_SHOWN);
        this.c.setBackgroundResource(17170445);
        this.c.setTag("webview");
    }

    public void j() {
    }

    public void k() {
    }
}
