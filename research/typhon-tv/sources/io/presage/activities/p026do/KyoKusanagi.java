package io.presage.activities.p026do;

import io.presage.activities.PresageActivity;
import io.presage.activities.p026do.GoroDaimon;
import io.presage.ads.ChangKoehan;
import io.presage.ads.NewAd;
import java.lang.ref.WeakReference;

/* renamed from: io.presage.activities.do.KyoKusanagi  reason: invalid package */
public abstract class KyoKusanagi implements ChinGentsai {
    protected ChangKoehan a;
    GoroDaimon.KyoKusanagi b;
    private C0039KyoKusanagi c = C0039KyoKusanagi.STATE_DEFAULT;
    private WeakReference<PresageActivity> d;
    private BenimaruNikaido e;

    /* renamed from: io.presage.activities.do.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    public enum C0039KyoKusanagi {
        STATE_DEFAULT,
        STATE_CANCELED,
        STATE_CLOSED,
        STATE_ERRORED
    }

    public KyoKusanagi(GoroDaimon.KyoKusanagi kyoKusanagi, PresageActivity presageActivity, BenimaruNikaido benimaruNikaido, ChangKoehan changKoehan) {
        this.b = kyoKusanagi;
        this.d = new WeakReference<>(presageActivity);
        this.a = changKoehan;
        this.e = benimaruNikaido;
    }

    public GoroDaimon.KyoKusanagi a() {
        return this.b;
    }

    public void a(String str) {
        if (str.equals(NewAd.EVENT_CANCEL)) {
            this.c = C0039KyoKusanagi.STATE_CANCELED;
        } else if (str.equals("close")) {
            this.c = C0039KyoKusanagi.STATE_CLOSED;
        }
    }

    public void a(boolean z) {
        if (!z) {
            if (this.c == C0039KyoKusanagi.STATE_DEFAULT && this.a != null) {
                this.c = C0039KyoKusanagi.STATE_CANCELED;
            }
            if (this.d.get() != null) {
                ((PresageActivity) this.d.get()).finish();
            }
        }
    }

    public C0039KyoKusanagi b() {
        return this.c;
    }

    public void c() {
        j();
        k();
    }

    public void d() {
        this.c = C0039KyoKusanagi.STATE_ERRORED;
    }

    public ChangKoehan e() {
        return this.a;
    }

    public PresageActivity f() {
        return (PresageActivity) this.d.get();
    }
}
