package io.presage.activities.handlers;

import android.app.Activity;
import android.os.Bundle;
import com.Pinkamena;
import io.presage.activities.PresageActivity;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdController;
import io.presage.formats.NewFormatDescriptor;
import io.presage.formats.NewHelperDescriptor;
import io.presage.helper.Permissions;
import io.presage.p029char.ChoiBounge;
import io.presage.p034goto.SaishuKusanagi;

public class LaunchActivityFormatHandler extends PresageActivity.ActivityHandler {
    private NewAdController d;

    public LaunchActivityFormatHandler(Activity activity, ChoiBounge choiBounge, Permissions permissions) {
        super(activity, choiBounge, permissions);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = this.a.getIntent().getExtras();
        if (!extras.containsKey("ad")) {
            SaishuKusanagi.c("LaunchActivityFormatHan", "Unable to display an ad. The ad is not provided in the extras in the tag 'ad'.");
        } else {
            NewAd fromJsonString = NewAd.fromJsonString(extras.getString("ad"));
            NewFormatDescriptor formatDescriptor = fromJsonString.getFormatDescriptor();
            String str = (String) formatDescriptor.getParameterValue("helper", String.class);
            if (str == null) {
                SaishuKusanagi.c("LaunchActivityFormatHan", "Unable to display an ad. The helper name is not provided in the viewer in the tag 'params/helper'.");
            } else {
                this.d = io.presage.ads.ChoiBounge.a().a(extras.getString("controller")).a(extras.getInt("flags")).a(this.a, this.b, this.c, fromJsonString, new NewHelperDescriptor(str, formatDescriptor.getParameters()));
                NewAdController newAdController = this.d;
                Pinkamena.DianePie();
                return;
            }
        }
        this.a.finish();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.a.isFinishing() && this.d != null && this.d.isAdDisplayed()) {
            this.d.hideAd();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean("isResumed", true);
    }
}
