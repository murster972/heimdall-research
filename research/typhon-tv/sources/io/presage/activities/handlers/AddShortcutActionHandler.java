package io.presage.activities.handlers;

import android.app.Activity;
import android.os.Bundle;
import com.Pinkamena;
import io.presage.activities.PresageActivity;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdController;
import io.presage.helper.Permissions;
import io.presage.p029char.ChoiBounge;
import io.presage.p034goto.SaishuKusanagi;
import net.pubnative.library.request.PubnativeAsset;

public class AddShortcutActionHandler extends PresageActivity.ActivityHandler {
    private NewAdController d;

    public AddShortcutActionHandler(Activity activity, ChoiBounge choiBounge, Permissions permissions) {
        super(activity, choiBounge, permissions);
    }

    public void onCreate(Bundle bundle) {
        if (this.d == null) {
            Bundle extras = this.a.getIntent().getExtras();
            if (!extras.containsKey("ad")) {
                SaishuKusanagi.c("AddShortcutActionHandle", "Unable to display an ad. The ad is not provided in the extras in the tag 'ad'.");
            } else {
                NewAd fromJsonString = NewAd.fromJsonString(extras.getString("ad"));
                this.d = io.presage.ads.ChoiBounge.a().a(PubnativeAsset.ICON).a(16).a(this.a.getApplicationContext(), this.b, this.c, fromJsonString, fromJsonString.getFormatDescriptor());
                NewAdController newAdController = this.d;
                Pinkamena.DianePie();
            }
            this.a.finish();
        }
    }
}
