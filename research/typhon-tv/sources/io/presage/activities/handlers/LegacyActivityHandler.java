package io.presage.activities.handlers;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import io.presage.activities.PresageActivity;
import io.presage.activities.p026do.BenimaruNikaido;
import io.presage.activities.p026do.ChinGentsai;
import io.presage.activities.p026do.GoroDaimon;
import io.presage.ads.ChangKoehan;
import io.presage.ads.KyoKusanagi;
import io.presage.ads.NewAd;
import io.presage.helper.Permissions;
import io.presage.p029char.ChoiBounge;

public class LegacyActivityHandler extends PresageActivity.ActivityHandler implements BenimaruNikaido {
    private ChangKoehan d;
    private ChinGentsai e;

    public LegacyActivityHandler(Activity activity, ChoiBounge choiBounge, Permissions permissions) {
        super(activity, choiBounge, permissions);
    }

    public void dontShowAd(String str) {
        if (this.d != null && this.e.a() == GoroDaimon.KyoKusanagi.ACTIVITY_HELPER_TYPE_VIDEO) {
            this.d.a("format", str);
        }
        this.a.finish();
    }

    public void finishActivity() {
        this.a.finish();
    }

    public void onCreate(Bundle bundle) {
        Bundle extras = this.a.getIntent().getExtras();
        String str = null;
        if (extras != null) {
            str = extras.getString("io.presage.extras.ADVERT_ID");
        }
        if (str == null) {
            this.a.finish();
            return;
        }
        ChangKoehan a = KyoKusanagi.a((Context) this.a).a(str);
        this.d = a;
        if (a == null) {
            this.a.finish();
            return;
        }
        ((io.presage.formats.ChoiBounge) a.h()).a(this);
        this.e = GoroDaimon.a().a((PresageActivity) this.a, this, this.d);
        if (this.e == null) {
            this.a.finish();
            return;
        }
        this.e.i();
        this.a.setContentView(this.e.g(), this.e.h());
    }

    public void onDestroy() {
        if (this.e != null) {
            this.e.c();
            switch (this.e.b()) {
                case STATE_CANCELED:
                    this.d.b(NewAd.EVENT_CANCEL);
                    this.d = null;
                    this.a.finish();
                    return;
                case STATE_CLOSED:
                    this.d.b("close");
                    this.a.finish();
                    return;
                default:
                    return;
            }
        }
    }

    public void onPause() {
        if (this.e != null) {
            this.e.a(this.a.isFinishing());
        }
    }

    public void setErroredState() {
        this.e.d();
    }

    public void showAd() {
    }
}
