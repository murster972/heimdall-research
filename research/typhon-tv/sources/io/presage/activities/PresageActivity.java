package io.presage.activities;

import android.app.Activity;
import android.os.Bundle;
import io.presage.activities.handlers.AddShortcutActionHandler;
import io.presage.activities.handlers.LaunchActivityFormatHandler;
import io.presage.activities.handlers.LegacyActivityHandler;
import io.presage.helper.Permissions;
import io.presage.p029char.ChangKoehan;
import io.presage.p029char.ChoiBounge;
import io.presage.p034goto.SaishuKusanagi;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PresageActivity extends Activity {
    public static final String a = null;
    private static final Map<String, Class<? extends ActivityHandler>> b = new HashMap<String, Class<? extends ActivityHandler>>() {
        {
            put(PresageActivity.a, LegacyActivityHandler.class);
            put("add_shortcut_action", AddShortcutActionHandler.class);
            put("launch_activity", LaunchActivityFormatHandler.class);
        }
    };
    private ActivityHandler c = null;

    public static abstract class ActivityHandler {
        protected Activity a;
        protected ChoiBounge b;
        protected Permissions c;

        public ActivityHandler(Activity activity, ChoiBounge choiBounge, Permissions permissions) {
            this.a = activity;
            this.b = choiBounge;
            this.c = permissions;
        }

        public void onCreate(Bundle bundle) {
        }

        public void onDestroy() {
        }

        public void onPause() {
        }

        public void onRestart() {
        }

        public void onResume() {
        }

        public void onSaveInstanceState(Bundle bundle) {
        }

        public void onStart() {
        }

        public void onStop() {
        }
    }

    public String a() {
        return this.c == null ? "" : this.c.getClass().getSimpleName();
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRequestedOrientation(10);
        if (this.c != null) {
            this.c.onCreate(bundle);
            return;
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = extras.getString("activity_handler");
            Bundle bundle2 = extras.getBundle("permissions_bundle");
            Permissions permissions = bundle2 != null ? (Permissions) bundle2.getParcelable("permissions") : null;
            Class cls = b.get(string);
            if (cls != null) {
                try {
                    ChangKoehan.a().a(getApplicationContext(), ChangKoehan.BenimaruNikaido.APP_PROCESS, (ChangKoehan.KyoKusanagi) null);
                    this.c = (ActivityHandler) cls.getConstructor(new Class[]{Activity.class, ChoiBounge.class, Permissions.class}).newInstance(new Object[]{this, ChangKoehan.a().k(), permissions});
                    SaishuKusanagi.b("PresageActivity", String.format("Activity handler %s onCreate", new Object[]{this.c.getClass().getName()}));
                    this.c.onCreate(bundle);
                    return;
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                } catch (InvocationTargetException e3) {
                    e3.printStackTrace();
                } catch (NoSuchMethodException e4) {
                    e4.printStackTrace();
                }
            } else {
                SaishuKusanagi.c("PresageActivity", String.format("PresageActivity called with a wrong handler %s. Possible handlers are %s", new Object[]{string, Arrays.toString(b.keySet().toArray())}));
            }
        } else {
            SaishuKusanagi.c("PresageActivity", "PresageActivity called without any extra.");
        }
        SaishuKusanagi.c("PresageActivity", "Finishing the activity.");
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        SaishuKusanagi.b("PresageActivity", String.format("Activity handler %s onDestroy", new Object[]{a()}));
        if (this.c != null) {
            this.c.onDestroy();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        SaishuKusanagi.b("PresageActivity", String.format("Activity handler %s onPause", new Object[]{a()}));
        if (this.c != null) {
            this.c.onPause();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        SaishuKusanagi.b("PresageActivity", String.format("Activity handler %s onRestart", new Object[]{a()}));
        super.onRestart();
        if (this.c != null) {
            this.c.onRestart();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        SaishuKusanagi.b("PresageActivity", String.format("Activity handler %s onResume", new Object[]{a()}));
        super.onResume();
        if (this.c != null) {
            this.c.onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        SaishuKusanagi.b("PresageActivity", String.format("Activity handler %s onSaveInstanceState", new Object[]{a()}));
        if (this.c != null) {
            this.c.onSaveInstanceState(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        SaishuKusanagi.b("PresageActivity", String.format("Activity handler %s onStart", new Object[]{a()}));
        super.onStart();
        if (this.c != null) {
            this.c.onStart();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        SaishuKusanagi.b("PresageActivity", String.format("Activity handler %s onStop", new Object[]{a()}));
        if (this.c != null) {
            this.c.onStop();
        }
        super.onStop();
    }
}
