package io.presage;

import android.content.Context;
import io.presage.p034goto.RugalBernstein;
import io.presage.p034goto.SaishuKusanagi;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import p006do.JhunHoon;
import p009if.ChangKoehan;
import p009if.ChinGentsai;
import p009if.LeonaHeidern;

public class HeavyD {
    private static final String a = HeavyD.class.getSimpleName();
    private Context b;
    private String c;
    private String d;
    private String e;
    private EnumMap<BenimaruNikaido, String> f = new EnumMap<>(BenimaruNikaido.class);
    private KyoKusanagi g;

    public enum BenimaruNikaido {
        BLACKLIST_IPS,
        BLACKLIST_APPS_FOR_USAGE,
        WHITELIST_APPS_FOR_IPS
    }

    public interface KyoKusanagi {
        void a(Set<String> set);

        void b(Set<String> set);

        void c(Set<String> set);
    }

    public HeavyD(Context context) {
        this.b = context;
    }

    private String a(int i) {
        return ((i >> 24) & 255) + "." + ((i >> 16) & 255) + "." + ((i >> 8) & 255) + "." + (i & 255);
    }

    public static Set<String> a(Context context, BenimaruNikaido benimaruNikaido) {
        String str;
        switch (benimaruNikaido) {
            case BLACKLIST_IPS:
                str = "BlacklistIps";
                break;
            case BLACKLIST_APPS_FOR_USAGE:
                str = "BlacklistAppsForUsage";
                break;
            case WHITELIST_APPS_FOR_IPS:
                str = "WhitelistAppsForIps";
                break;
            default:
                return null;
        }
        HashSet hashSet = new HashSet();
        File file = new File(context.getFilesDir(), str);
        if (!file.exists()) {
            return hashSet;
        }
        try {
            ChangKoehan r0 = LeonaHeidern.m18966(LeonaHeidern.m18968(file));
            r0.m6700();
            while (!r0.m6708()) {
                hashSet.add(r0.m6700());
            }
            if (r0 != null) {
                r0.close();
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return hashSet;
    }

    private void a(JhunHoon jhunHoon, ChinGentsai chinGentsai) throws IOException {
        ChangKoehan r3;
        if (jhunHoon != null && chinGentsai != null && (r3 = jhunHoon.m6463().m6496()) != null && !r3.m6708()) {
            int r2 = r3.m6696();
            SaishuKusanagi.b(a, r2 + " ipv4");
            for (int i = 0; i < r2; i++) {
                chinGentsai.m18935(a(r3.m6696()) + StringUtils.LF);
            }
            int i2 = 0;
            while (!r3.m6708()) {
                StringBuilder sb = new StringBuilder();
                for (int i3 = 0; i3 < 8; i3++) {
                    sb.append(r3.m6707(2).e().toLowerCase());
                    if (i3 != 7) {
                        sb.append(":");
                    }
                }
                sb.append(StringUtils.LF);
                chinGentsai.m18935(sb.toString());
                i2++;
            }
            SaishuKusanagi.b(a, i2 + " ipv6");
        }
    }

    private boolean a(BenimaruNikaido benimaruNikaido, String str) {
        File file;
        switch (benimaruNikaido) {
            case BLACKLIST_IPS:
                file = new File(this.b.getFilesDir(), "BlacklistIps");
                break;
            case BLACKLIST_APPS_FOR_USAGE:
                file = new File(this.b.getFilesDir(), "BlacklistAppsForUsage");
                break;
            case WHITELIST_APPS_FOR_IPS:
                file = new File(this.b.getFilesDir(), "WhitelistAppsForIps");
                break;
            default:
                return true;
        }
        if (!file.exists()) {
            return true;
        }
        try {
            if (str.equals(LeonaHeidern.m18966(LeonaHeidern.m18968(file)).m6700())) {
                return false;
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return true;
    }

    private boolean a(Map.Entry<BenimaruNikaido, String> entry) {
        return a(entry.getKey(), d(entry.getValue()));
    }

    public static boolean a(Set<String> set, String str) {
        if (set == null || set.isEmpty()) {
            return false;
        }
        try {
            InetAddress byName = InetAddress.getByName(str);
            if (byName instanceof Inet6Address) {
                return set.contains(RugalBernstein.a(str).b().toLowerCase());
            }
            if (byName instanceof Inet4Address) {
                return set.contains(byName.getHostAddress());
            }
            return false;
        } catch (UnknownHostException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private String d(String str) {
        if (str == null) {
            return null;
        }
        String substring = str.substring(str.lastIndexOf(47) + 1);
        return substring.substring(substring.lastIndexOf(95) + 1, substring.indexOf(46));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0101, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0102, code lost:
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0124, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0127, code lost:
        r4.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0124 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:20:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0127  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r9 = this;
            io.presage.char.ChangKoehan r0 = io.presage.p029char.ChangKoehan.a()
            io.presage.char.ChoiBounge r0 = r0.k()
            if (r0 != 0) goto L_0x0155
            io.presage.char.ChoiBounge r0 = new io.presage.char.ChoiBounge
            android.content.Context r1 = r9.b
            r2 = 1
            r0.<init>(r1, r2)
            r2 = r0
        L_0x0013:
            java.util.EnumMap<io.presage.HeavyD$BenimaruNikaido, java.lang.String> r0 = r9.f
            if (r0 == 0) goto L_0x001f
            java.util.EnumMap<io.presage.HeavyD$BenimaruNikaido, java.lang.String> r0 = r9.f
            java.util.Set r0 = r0.entrySet()
            if (r0 != 0) goto L_0x0020
        L_0x001f:
            return
        L_0x0020:
            java.util.EnumMap<io.presage.HeavyD$BenimaruNikaido, java.lang.String> r0 = r9.f
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r5 = r0.iterator()
        L_0x002a:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x001f
            java.lang.Object r0 = r5.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getValue()
            if (r1 == 0) goto L_0x002a
            java.lang.Object r1 = r0.getValue()
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x002a
            int[] r3 = io.presage.HeavyD.AnonymousClass1.a
            java.lang.Object r1 = r0.getKey()
            io.presage.HeavyD$BenimaruNikaido r1 = (io.presage.HeavyD.BenimaruNikaido) r1
            int r1 = r1.ordinal()
            r1 = r3[r1]
            switch(r1) {
                case 1: goto L_0x005a;
                case 2: goto L_0x00e9;
                case 3: goto L_0x00ef;
                default: goto L_0x0059;
            }
        L_0x0059:
            goto L_0x001f
        L_0x005a:
            java.lang.String r1 = "BlacklistIps"
            r3 = r1
        L_0x005e:
            boolean r1 = r9.a((java.util.Map.Entry<io.presage.HeavyD.BenimaruNikaido, java.lang.String>) r0)
            if (r1 == 0) goto L_0x002a
            r4 = 0
            java.lang.Object r1 = r0.getValue()     // Catch:{ Exception -> 0x0152, all -> 0x0124 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0152, all -> 0x0124 }
            do.ShingoYabuki$KyoKusanagi r6 = new do.ShingoYabuki$KyoKusanagi     // Catch:{ Exception -> 0x0152, all -> 0x0124 }
            r6.<init>()     // Catch:{ Exception -> 0x0152, all -> 0x0124 }
            do.ShingoYabuki r6 = r6.m18386()     // Catch:{ Exception -> 0x0152, all -> 0x0124 }
            r7 = 0
            java.lang.String r8 = ""
            do.JhunHoon r4 = r2.a(r1, r6, r7, r8)     // Catch:{ Exception -> 0x0152, all -> 0x0124 }
            boolean r1 = r4.m6471()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            if (r1 == 0) goto L_0x00e2
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            android.content.Context r6 = r9.b     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.io.File r6 = r6.getFilesDir()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            r1.<init>(r6, r3)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            boolean r3 = r1.exists()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            if (r3 != 0) goto L_0x0096
            r1.createNewFile()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
        L_0x0096:
            if.YashiroNanakase r1 = p009if.LeonaHeidern.m18964((java.io.File) r1)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            if.ChinGentsai r3 = p009if.LeonaHeidern.m18967((p009if.YashiroNanakase) r1)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            r6.<init>()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.Object r1 = r0.getValue()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.String r1 = r9.d(r1)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.String r6 = "\n"
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            r3.m18935((java.lang.String) r1)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.Object r1 = r0.getKey()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            io.presage.HeavyD$BenimaruNikaido r6 = io.presage.HeavyD.BenimaruNikaido.BLACKLIST_IPS     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            if (r1 != r6) goto L_0x00f5
            r9.a((p006do.JhunHoon) r4, (p009if.ChinGentsai) r3)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
        L_0x00ca:
            r3.close()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            io.presage.HeavyD$KyoKusanagi r1 = r9.g     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            if (r1 == 0) goto L_0x00e2
            int[] r3 = io.presage.HeavyD.AnonymousClass1.a     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.Object r1 = r0.getKey()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            io.presage.HeavyD$BenimaruNikaido r1 = (io.presage.HeavyD.BenimaruNikaido) r1     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            int r1 = r1.ordinal()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            r1 = r3[r1]     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            switch(r1) {
                case 1: goto L_0x0112;
                case 2: goto L_0x012b;
                case 3: goto L_0x013d;
                default: goto L_0x00e2;
            }
        L_0x00e2:
            if (r4 == 0) goto L_0x002a
            r4.close()
            goto L_0x002a
        L_0x00e9:
            java.lang.String r1 = "BlacklistAppsForUsage"
            r3 = r1
            goto L_0x005e
        L_0x00ef:
            java.lang.String r1 = "WhitelistAppsForIps"
            r3 = r1
            goto L_0x005e
        L_0x00f5:
            do.Krizalid r1 = r4.m6463()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            if.ChangKoehan r1 = r1.m6496()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            r3.m18933((p009if.Shermie) r1)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            goto L_0x00ca
        L_0x0101:
            r0 = move-exception
            r1 = r4
        L_0x0103:
            java.lang.String r3 = a     // Catch:{ all -> 0x014f }
            java.lang.String r4 = "sync failed"
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r0)     // Catch:{ all -> 0x014f }
            if (r1 == 0) goto L_0x002a
            r1.close()
            goto L_0x002a
        L_0x0112:
            io.presage.HeavyD$KyoKusanagi r1 = r9.g     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            android.content.Context r3 = r9.b     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.Object r0 = r0.getKey()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            io.presage.HeavyD$BenimaruNikaido r0 = (io.presage.HeavyD.BenimaruNikaido) r0     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.util.Set r0 = a((android.content.Context) r3, (io.presage.HeavyD.BenimaruNikaido) r0)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            r1.a(r0)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            goto L_0x00e2
        L_0x0124:
            r0 = move-exception
        L_0x0125:
            if (r4 == 0) goto L_0x012a
            r4.close()
        L_0x012a:
            throw r0
        L_0x012b:
            io.presage.HeavyD$KyoKusanagi r1 = r9.g     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            android.content.Context r3 = r9.b     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.Object r0 = r0.getKey()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            io.presage.HeavyD$BenimaruNikaido r0 = (io.presage.HeavyD.BenimaruNikaido) r0     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.util.Set r0 = a((android.content.Context) r3, (io.presage.HeavyD.BenimaruNikaido) r0)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            r1.b(r0)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            goto L_0x00e2
        L_0x013d:
            io.presage.HeavyD$KyoKusanagi r1 = r9.g     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            android.content.Context r3 = r9.b     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.lang.Object r0 = r0.getKey()     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            io.presage.HeavyD$BenimaruNikaido r0 = (io.presage.HeavyD.BenimaruNikaido) r0     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            java.util.Set r0 = a((android.content.Context) r3, (io.presage.HeavyD.BenimaruNikaido) r0)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            r1.c(r0)     // Catch:{ Exception -> 0x0101, all -> 0x0124 }
            goto L_0x00e2
        L_0x014f:
            r0 = move-exception
            r4 = r1
            goto L_0x0125
        L_0x0152:
            r0 = move-exception
            r1 = r4
            goto L_0x0103
        L_0x0155:
            r2 = r0
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.HeavyD.a():void");
    }

    public void a(KyoKusanagi kyoKusanagi) {
        this.g = kyoKusanagi;
    }

    public void a(String str) {
        this.c = str;
        this.f.put(BenimaruNikaido.BLACKLIST_IPS, this.c);
    }

    public void b(String str) {
        this.d = str;
        this.f.put(BenimaruNikaido.BLACKLIST_APPS_FOR_USAGE, this.d);
    }

    public void c(String str) {
        this.e = str;
        this.f.put(BenimaruNikaido.WHITELIST_APPS_FOR_IPS, this.e);
    }
}
