package io.presage.ads.p027do;

import android.content.Context;
import android.os.Bundle;
import com.Pinkamena;
import io.presage.ChinGentsai;
import io.presage.ChoiBounge;
import io.presage.ads.PresageAd;
import io.presage.ads.PresageOptinVideo;
import io.presage.ads.optinvideo.RewardItem;
import io.presage.p029char.ChangKoehan;
import io.presage.p034goto.Chris;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Shermie;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: io.presage.ads.do.GoroDaimon  reason: invalid package */
public class GoroDaimon extends PresageAd implements Shermie<Chris<KyoKusanagi>> {
    public static final String a = GoroDaimon.class.getSimpleName();
    private final String b = "optin_video";
    private String c;
    /* access modifiers changed from: private */
    public RewardItem d;
    private ChoiBounge e;
    private ChinGentsai f;
    private ConcurrentLinkedQueue<Chris<KyoKusanagi>> g;
    /* access modifiers changed from: private */
    public PresageOptinVideo.PresageOptinVideoCallback h;

    /* renamed from: io.presage.ads.do.GoroDaimon$KyoKusanagi */
    enum KyoKusanagi {
        AdToServe,
        LoadWithoutParam,
        LoadWithParam
    }

    public GoroDaimon(Context context, String str) {
        super(context, str);
        e();
    }

    public GoroDaimon(Context context, String str, RewardItem rewardItem) {
        super(context, str);
        this.d = rewardItem;
        e();
    }

    public GoroDaimon(Context context, String str, RewardItem rewardItem, String str2) {
        super(context, str);
        this.d = rewardItem;
        this.c = str2;
        this.e.a(str2);
    }

    public GoroDaimon(Context context, String str, String str2) {
        super(context, str);
        this.c = str2;
        this.e.a(str2);
    }

    private void e() {
        this.e = new ChoiBounge(getContext(), ChangKoehan.a().l(), getAdUnitId(), "optin_video");
        this.g = new ConcurrentLinkedQueue<>();
        this.f = new ChinGentsai() {
            public void a() {
                if (GoroDaimon.this.h != null) {
                    GoroDaimon.this.h.onAdAvailable();
                }
            }

            public void a(int i) {
                if (GoroDaimon.this.h != null) {
                    GoroDaimon.this.h.onAdError(i);
                }
            }

            public void a(RewardItem rewardItem) {
                if (GoroDaimon.this.h == null) {
                    return;
                }
                if (GoroDaimon.this.d != null) {
                    GoroDaimon.this.h.onAdRewarded(GoroDaimon.this.d);
                } else {
                    GoroDaimon.this.h.onAdRewarded(rewardItem);
                }
            }

            public void b() {
                if (GoroDaimon.this.h != null) {
                    GoroDaimon.this.h.onAdNotAvailable();
                }
            }

            public void c() {
                if (GoroDaimon.this.h != null) {
                    GoroDaimon.this.h.onAdLoaded();
                }
            }

            public void d() {
                if (GoroDaimon.this.h != null) {
                    GoroDaimon.this.h.onAdClosed();
                }
            }

            public void e() {
                if (GoroDaimon.this.h != null) {
                    GoroDaimon.this.h.onAdDisplayed();
                }
            }
        };
    }

    public RewardItem a() {
        return this.d;
    }

    public void a(PresageOptinVideo.PresageOptinVideoCallback presageOptinVideoCallback) {
        this.h = presageOptinVideoCallback;
    }

    public void a(RewardItem rewardItem) {
        this.d = rewardItem;
    }

    public void a(Chris<KyoKusanagi> chris) {
        if (this.g == null) {
            this.g = new ConcurrentLinkedQueue<>();
        }
        if (this.g.contains(chris)) {
            this.g.remove(chris);
        }
        this.g.add(chris);
        io.presage.p034goto.KyoKusanagi.a().a((Object) this);
    }

    public void a(String str) {
        this.c = str;
        this.e.a(str);
    }

    public void adToServe() {
        if (io.presage.p034goto.KyoKusanagi.a().a(getContext())) {
            a((Chris<KyoKusanagi>) new Chris(KyoKusanagi.AdToServe));
        } else if (ChangKoehan.a().p() && this.e != null) {
            this.e.a((io.presage.GoroDaimon) this.f);
        } else if (this.h != null) {
            this.h.onAdNotAvailable();
        }
    }

    public String b() {
        return this.c;
    }

    public void c() {
        if (this.g != null) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator<Chris<KyoKusanagi>> it2 = this.g.iterator();
            while (it2.hasNext()) {
                Chris next = it2.next();
                if (currentTimeMillis - next.c() < 4500) {
                    SaishuKusanagi.b(a, String.format("Sending queued request %s %s", new Object[]{this + "", ((KyoKusanagi) next.a()).name()}));
                    switch ((KyoKusanagi) next.a()) {
                        case AdToServe:
                            adToServe();
                            break;
                        case LoadWithoutParam:
                            Pinkamena.DianePie();
                            break;
                        case LoadWithParam:
                            if (next.b() == null) {
                                Pinkamena.DianePie();
                                break;
                            } else {
                                int i = next.b().getInt("adToPrecache");
                                Pinkamena.DianePie();
                                break;
                            }
                        default:
                            SaishuKusanagi.d(a, "type is not good");
                            break;
                    }
                } else {
                    SaishuKusanagi.b(a, String.format("Request %s %s skipped", new Object[]{this + "", ((KyoKusanagi) next.a()).name()}));
                }
            }
            if (this.g != null) {
                this.g.clear();
            }
        }
    }

    public boolean canShow() {
        if (this.e == null) {
            return false;
        }
        return this.e.a();
    }

    public PresageOptinVideo.PresageOptinVideoCallback d() {
        return this.h;
    }

    public void destroy() {
    }

    public void load() {
        if (io.presage.p034goto.KyoKusanagi.a().a(getContext())) {
            a((Chris<KyoKusanagi>) new Chris(KyoKusanagi.LoadWithoutParam));
        } else if (ChangKoehan.a().p() && this.e != null) {
            this.e.b((io.presage.GoroDaimon) this.f);
        } else if (this.h != null) {
            this.h.onAdNotAvailable();
        }
    }

    public void load(int i) {
        if (io.presage.p034goto.KyoKusanagi.a().a(getContext())) {
            Chris chris = new Chris(KyoKusanagi.LoadWithParam);
            Bundle bundle = new Bundle();
            bundle.putInt("adToPrecache", i);
            chris.a(bundle);
            a((Chris<KyoKusanagi>) chris);
        } else if (ChangKoehan.a().p() && this.e != null) {
            this.e.a(i, (io.presage.GoroDaimon) this.f);
        } else if (this.h != null) {
            this.h.onAdNotAvailable();
        }
    }

    public void show() {
        if (ChangKoehan.a().p() && this.e != null) {
            this.e.c((io.presage.GoroDaimon) this.f);
        } else if (this.h != null) {
            this.h.onAdNotAvailable();
        }
    }
}
