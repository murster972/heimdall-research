package io.presage.ads.p027do;

import android.content.Context;
import io.presage.ChoiBounge;
import io.presage.IEulaHandler;
import io.presage.p029char.ChangKoehan;
import io.presage.p034goto.Chris;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Shermie;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: io.presage.ads.do.KyoKusanagi  reason: invalid package */
public class KyoKusanagi implements Shermie<Chris<C0040KyoKusanagi>> {
    public static final String a = KyoKusanagi.class.getSimpleName();
    private Context b;
    private ChoiBounge c;
    private IEulaHandler d;
    private ConcurrentLinkedQueue<Chris<C0040KyoKusanagi>> e;

    /* renamed from: io.presage.ads.do.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    enum C0040KyoKusanagi {
        eula
    }

    public KyoKusanagi(Context context, String str) {
        this.b = context.getApplicationContext();
        this.c = new ChoiBounge(context, ChangKoehan.a().l(), str, (String) null);
    }

    public void a() {
        if (io.presage.p034goto.KyoKusanagi.a().a(this.b)) {
            a((Chris<C0040KyoKusanagi>) new Chris(C0040KyoKusanagi.eula));
        } else if (ChangKoehan.a().p() && this.c != null) {
            this.c.a(this.d);
        } else if (this.d != null) {
            this.d.onEulaNotFound();
        }
    }

    public void a(IEulaHandler iEulaHandler) {
        this.d = iEulaHandler;
    }

    public void a(Chris<C0040KyoKusanagi> chris) {
        if (this.e == null) {
            this.e = new ConcurrentLinkedQueue<>();
        }
        if (this.e.contains(chris)) {
            this.e.remove(chris);
        }
        this.e.add(chris);
        io.presage.p034goto.KyoKusanagi.a().a((Object) this);
    }

    public IEulaHandler b() {
        return this.d;
    }

    public void c() {
        if (this.e != null) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator<Chris<C0040KyoKusanagi>> it2 = this.e.iterator();
            while (it2.hasNext()) {
                Chris next = it2.next();
                if (currentTimeMillis - next.c() >= 4500) {
                    SaishuKusanagi.b(a, String.format("Request %s %s skipped", new Object[]{this + "", ((C0040KyoKusanagi) next.a()).name()}));
                } else {
                    SaishuKusanagi.b(a, String.format("Sending queued request %s %s", new Object[]{this + "", ((C0040KyoKusanagi) next.a()).name()}));
                    if (next.a() == C0040KyoKusanagi.eula) {
                        a();
                    } else {
                        SaishuKusanagi.d(a, "request type is not good");
                    }
                }
            }
            if (this.e != null) {
                this.e.clear();
            }
        }
    }
}
