package io.presage.ads.p027do;

import android.content.Context;
import android.os.Bundle;
import com.Pinkamena;
import com.mopub.common.AdType;
import io.presage.ChoiBounge;
import io.presage.GoroDaimon;
import io.presage.ads.PresageAd;
import io.presage.ads.PresageInterstitial;
import io.presage.p029char.ChangKoehan;
import io.presage.p034goto.Chris;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Shermie;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: io.presage.ads.do.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido extends PresageAd implements Shermie<Chris<KyoKusanagi>> {
    public static final String a = BenimaruNikaido.class.getSimpleName();
    protected ChoiBounge b;
    private final String c = AdType.INTERSTITIAL;
    private GoroDaimon d;
    private ConcurrentLinkedQueue<Chris<KyoKusanagi>> e;
    /* access modifiers changed from: private */
    public PresageInterstitial.PresageInterstitialCallback f;

    /* renamed from: io.presage.ads.do.BenimaruNikaido$KyoKusanagi */
    enum KyoKusanagi {
        AdToServe,
        LoadWithoutParam,
        LoadWithParam
    }

    public BenimaruNikaido(Context context, String str) {
        super(context, str);
        a();
    }

    private void a() {
        this.b = new ChoiBounge(getContext(), ChangKoehan.a().l(), getAdUnitId(), AdType.INTERSTITIAL);
        this.e = new ConcurrentLinkedQueue<>();
        this.d = new GoroDaimon() {
            public void a() {
                if (BenimaruNikaido.this.f != null) {
                    BenimaruNikaido.this.f.onAdAvailable();
                }
            }

            public void a(int i) {
                if (BenimaruNikaido.this.f != null) {
                    BenimaruNikaido.this.f.onAdError(i);
                }
            }

            public void b() {
                if (BenimaruNikaido.this.f != null) {
                    BenimaruNikaido.this.f.onAdNotAvailable();
                }
            }

            public void c() {
                if (BenimaruNikaido.this.f != null) {
                    BenimaruNikaido.this.f.onAdLoaded();
                }
            }

            public void d() {
                if (BenimaruNikaido.this.f != null) {
                    BenimaruNikaido.this.f.onAdClosed();
                }
            }

            public void e() {
                if (BenimaruNikaido.this.f != null) {
                    BenimaruNikaido.this.f.onAdDisplayed();
                }
            }
        };
    }

    public void a(PresageInterstitial.PresageInterstitialCallback presageInterstitialCallback) {
        this.f = presageInterstitialCallback;
    }

    public void a(Chris<KyoKusanagi> chris) {
        if (this.e == null) {
            this.e = new ConcurrentLinkedQueue<>();
        }
        if (this.e.contains(chris)) {
            this.e.remove(chris);
        }
        this.e.add(chris);
        io.presage.p034goto.KyoKusanagi.a().a((Object) this);
    }

    public void adToServe() {
        if (io.presage.p034goto.KyoKusanagi.a().a(getContext())) {
            a((Chris<KyoKusanagi>) new Chris(KyoKusanagi.AdToServe));
        } else if (ChangKoehan.a().p() && this.b != null) {
            this.b.a(this.d);
        } else if (this.f != null) {
            this.f.onAdNotAvailable();
        }
    }

    public void c() {
        if (this.e != null) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator<Chris<KyoKusanagi>> it2 = this.e.iterator();
            while (it2.hasNext()) {
                Chris next = it2.next();
                if (currentTimeMillis - next.c() < 4500) {
                    SaishuKusanagi.b(a, String.format("Sending queued request %s %s", new Object[]{this + "", ((KyoKusanagi) next.a()).name()}));
                    switch ((KyoKusanagi) next.a()) {
                        case AdToServe:
                            adToServe();
                            break;
                        case LoadWithoutParam:
                            Pinkamena.DianePie();
                            break;
                        case LoadWithParam:
                            if (next.b() == null) {
                                Pinkamena.DianePie();
                                break;
                            } else {
                                int i = next.b().getInt("adToPrecache");
                                Pinkamena.DianePie();
                                break;
                            }
                        default:
                            SaishuKusanagi.d(a, "type is not good");
                            break;
                    }
                } else {
                    SaishuKusanagi.b(a, String.format("Request %s %s skipped", new Object[]{this + "", ((KyoKusanagi) next.a()).name()}));
                }
            }
            if (this.e != null) {
                this.e.clear();
            }
        }
    }

    public boolean canShow() {
        if (this.b == null) {
            return false;
        }
        return this.b.a();
    }

    public void destroy() {
    }

    public void load() {
        if (io.presage.p034goto.KyoKusanagi.a().a(getContext())) {
            a((Chris<KyoKusanagi>) new Chris(KyoKusanagi.LoadWithoutParam));
        } else if (ChangKoehan.a().p() && this.b != null) {
            this.b.b(this.d);
        } else if (this.f != null) {
            this.f.onAdNotAvailable();
        }
    }

    public void load(int i) {
        if (io.presage.p034goto.KyoKusanagi.a().a(getContext())) {
            Chris chris = new Chris(KyoKusanagi.LoadWithParam);
            Bundle bundle = new Bundle();
            bundle.putInt("adToPrecache", i);
            chris.a(bundle);
            a((Chris<KyoKusanagi>) chris);
        } else if (ChangKoehan.a().p() && this.b != null) {
            this.b.a(i, this.d);
        } else if (this.f != null) {
            this.f.onAdNotAvailable();
        }
    }

    public void show() {
        if (ChangKoehan.a().p() && this.b != null) {
            this.b.c(this.d);
        } else if (this.f != null) {
            this.f.onAdNotAvailable();
        }
    }
}
