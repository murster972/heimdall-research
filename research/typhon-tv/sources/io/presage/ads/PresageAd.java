package io.presage.ads;

import android.content.Context;

public abstract class PresageAd {
    private Context a;
    private String b;

    public PresageAd() {
    }

    public PresageAd(Context context) {
        this.a = context.getApplicationContext();
    }

    public PresageAd(Context context, String str) {
        if (str == null || str.isEmpty() || str.equalsIgnoreCase("null")) {
            throw new IllegalArgumentException("invalid ad unit id");
        }
        this.a = context.getApplicationContext();
        this.b = str;
    }

    public abstract void adToServe();

    public abstract boolean canShow();

    public abstract void destroy();

    public String getAdUnitId() {
        return this.b;
    }

    public Context getContext() {
        return this.a;
    }

    public abstract void load();

    public abstract void load(int i);

    public abstract void show();
}
