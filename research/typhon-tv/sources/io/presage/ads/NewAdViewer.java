package io.presage.ads;

import android.content.Context;
import android.content.Intent;
import io.presage.helper.Permissions;

public abstract class NewAdViewer {
    protected Context a;
    protected NewAdController b;
    protected NewAd c;
    protected int d;
    protected Permissions e;

    public NewAdViewer(NewAdController newAdController, NewAd newAd, Permissions permissions, int i) {
        this.a = newAdController.getContext();
        this.b = newAdController;
        this.c = newAd;
        this.d = i;
        this.e = permissions;
    }

    public boolean hasFlag(int i) {
        return (this.d & i) != 0;
    }

    public abstract void hide();

    /* access modifiers changed from: protected */
    public void onHide() {
        Intent intent = new Intent();
        intent.setAction(NewAd.ACTION_HIDE);
        intent.putExtra("id", this.c.getId());
        this.a.sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public void onShow() {
        Intent intent = new Intent();
        intent.setAction(NewAd.ACTION_SHOW);
        intent.putExtra("id", this.c.getId());
        this.a.sendBroadcast(intent);
    }

    public abstract void show();
}
