package io.presage.ads;

import android.content.Context;
import com.Pinkamena;
import io.presage.ads.p027do.BenimaruNikaido;
import io.presage.provider.PresageProvider;

public class PresageInterstitial extends PresageAd {
    private BenimaruNikaido a;

    public interface PresageInterstitialCallback {
        void onAdAvailable();

        void onAdClosed();

        void onAdDisplayed();

        void onAdError(int i);

        void onAdLoaded();

        void onAdNotAvailable();
    }

    public PresageInterstitial(Context context) {
        super(context);
        this.a = new BenimaruNikaido(context, String.format("%s_default", new Object[]{PresageProvider.b(context)}));
    }

    public PresageInterstitial(Context context, String str) {
        super(context, str);
        this.a = new BenimaruNikaido(context, str);
    }

    public void adToServe() {
        this.a.adToServe();
    }

    public boolean canShow() {
        return this.a.canShow();
    }

    public void destroy() {
        this.a.destroy();
    }

    public void load() {
        BenimaruNikaido benimaruNikaido = this.a;
        Pinkamena.DianePie();
    }

    public void load(int i) {
        BenimaruNikaido benimaruNikaido = this.a;
        Pinkamena.DianePie();
    }

    public void setPresageInterstitialCallback(PresageInterstitialCallback presageInterstitialCallback) {
        this.a.a(presageInterstitialCallback);
    }

    public void show() {
        BenimaruNikaido benimaruNikaido = this.a;
        Pinkamena.DianePie();
    }
}
