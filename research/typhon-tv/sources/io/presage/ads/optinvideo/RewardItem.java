package io.presage.ads.optinvideo;

import org.apache.commons.lang3.StringUtils;

public class RewardItem {
    private String a = "";
    private String b = "";

    public RewardItem(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String getAmount() {
        return this.b;
    }

    public String getType() {
        return this.a;
    }

    public void setAmount(String str) {
        this.b = str;
    }

    public void setType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.b + StringUtils.SPACE + this.a;
    }
}
