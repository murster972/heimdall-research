package io.presage.ads;

import android.view.View;
import io.presage.formats.NewFormatDescriptor;
import io.presage.model.AdUnit;
import io.presage.model.Advertiser;
import io.presage.model.Parameter;
import io.presage.p039new.LuckyGlauber;
import java.util.List;

public class NewAd {
    public static final String ACTION_HIDE = "io.presage.ad.HIDE";
    public static final String ACTION_REWARD = "io.presage.ad.REWARD";
    public static final String ACTION_SHOW = "io.presage.ad.SHOW";
    public static final String EVENT_CANCEL = "cancel";
    public static final String EVENT_CLOSE_SYSTEM_DIALOG = "close_system_dialog";
    public static final String EVENT_COMPLETED = "completed";
    public static final String EVENT_FINISH = "finish";
    public static final String EVENT_INSTALL = "install";
    public static final String EVENT_REWARD = "rewards";
    public static final String EVENT_SHORTCUT = "shortcut";
    public static final String EVENT_SHOWN = "shown";
    public static final String EXTRA_AD_ID = "id";
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "actions")
    private List<io.presage.model.KyoKusanagi> actions;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "ad_unit")
    private AdUnit adUnit;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "advertiser")
    private Advertiser advertiser;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "campaign_id")
    private String campaignId;
    private transient KyoKusanagi executeActionListener;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "format_assets")
    private List<String> formatAssets;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "format")
    private NewFormatDescriptor formatDescriptor;
    private transient BenimaruNikaido formatErrorListener;
    private transient GoroDaimon formatEventListener;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "id")
    private String id;
    private String jsonAd = null;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "link_url")
    private String linkUrl;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "moat")
    private boolean moat;
    private transient ChinGentsai pageFinishedListener;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "params")
    private List<Parameter> params;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "params_assets")
    private List<String> paramsAssets;
    @com.p003if.p004do.p005do.GoroDaimon(m6138 = "client_tracker_pattern")
    private String trackerPattern;
    private transient ChoiBounge videoCompletionCallback;
    private transient ChangKoehan videoErrorListener;

    public interface BenimaruNikaido {
        void onFormatError(String str, String str2);
    }

    public interface ChangKoehan {
        void onVideoError(String str);
    }

    public interface ChinGentsai {
        void onPageFinished(View view, String str, int i, String str2, String str3);
    }

    public interface ChoiBounge {
        String a();
    }

    public interface GoroDaimon {
        void onFormatEvent(String str);
    }

    public interface KyoKusanagi {
        void onExecuteAction(String str);
    }

    public static NewAd fromJsonString(String str) {
        NewAd newAd = (NewAd) LuckyGlauber.a().m14090(str, NewAd.class);
        newAd.setJsonAd(str);
        return newAd;
    }

    public io.presage.model.KyoKusanagi getActionDescriptor(String str) {
        if (this.actions == null || str == null) {
            return null;
        }
        for (io.presage.model.KyoKusanagi next : this.actions) {
            if (str.equals(next.a())) {
                return next;
            }
        }
        return null;
    }

    public AdUnit getAdUnit() {
        return this.adUnit;
    }

    public String getAdUnitId() {
        return this.adUnit != null ? this.adUnit.getId() : "";
    }

    public Advertiser getAdvertiser() {
        return this.advertiser;
    }

    public String getCampaignId() {
        return this.campaignId;
    }

    public String getClientTrackerPattern() {
        return this.trackerPattern;
    }

    public List<String> getFormatAssets() {
        return this.formatAssets;
    }

    public NewFormatDescriptor getFormatDescriptor() {
        return this.formatDescriptor;
    }

    public String getId() {
        return this.id;
    }

    public String getJsonAd() {
        return this.jsonAd;
    }

    public String getLinkUrl() {
        return this.linkUrl;
    }

    public Parameter getOverridedParameter(String str) {
        Parameter parameter = getParameter(str);
        return (parameter != null || this.formatDescriptor == null) ? parameter : this.formatDescriptor.getParameter(str);
    }

    public <T> T getOverridedParameterValue(String str, Class<T> cls) {
        Parameter overridedParameter = getOverridedParameter(str);
        if (overridedParameter == null) {
            return null;
        }
        return overridedParameter.get(cls);
    }

    public Parameter getParameter(String str) {
        if (this.params != null) {
            for (Parameter next : this.params) {
                if (next != null && str.equals(next.getName())) {
                    return next;
                }
            }
        }
        return null;
    }

    public List<Parameter> getParameters() {
        return this.params;
    }

    public List<String> getParamsAssets() {
        return this.paramsAssets;
    }

    public String getVideoCompletionRate() {
        if (this.videoCompletionCallback != null) {
            return this.videoCompletionCallback.a();
        }
        return null;
    }

    public boolean isMoatActivated() {
        return this.moat;
    }

    public void onExecuteAction(String str) {
        if (this.executeActionListener != null) {
            this.executeActionListener.onExecuteAction(str);
        }
    }

    public void onFormatError(String str, String str2) {
        if (this.formatErrorListener != null) {
            this.formatErrorListener.onFormatError(str, str2);
        }
    }

    public void onFormatEvent(String str) {
        if (this.formatEventListener != null) {
            this.formatEventListener.onFormatEvent(str);
        }
    }

    public void onPageFinished(View view, String str, int i, String str2, String str3) {
        if (this.pageFinishedListener != null) {
            this.pageFinishedListener.onPageFinished(view, str, i, str2, str3);
        }
    }

    public void onVideoError(String str) {
        if (this.videoErrorListener != null) {
            this.videoErrorListener.onVideoError(str);
        }
    }

    public void setJsonAd(String str) {
        this.jsonAd = str;
    }

    public void setOnExecuteActionListener(KyoKusanagi kyoKusanagi) {
        this.executeActionListener = kyoKusanagi;
    }

    public void setOnFormatErrorListener(BenimaruNikaido benimaruNikaido) {
        this.formatErrorListener = benimaruNikaido;
    }

    public void setOnFormatEventListener(GoroDaimon goroDaimon) {
        this.formatEventListener = goroDaimon;
    }

    public void setOnPageFinishedListener(ChinGentsai chinGentsai) {
        this.pageFinishedListener = chinGentsai;
    }

    public void setOnVideoErrorListener(ChangKoehan changKoehan) {
        this.videoErrorListener = changKoehan;
    }

    public void setVideoCompletionCallback(ChoiBounge choiBounge) {
        this.videoCompletionCallback = choiBounge;
    }
}
