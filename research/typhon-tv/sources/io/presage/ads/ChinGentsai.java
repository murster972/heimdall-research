package io.presage.ads;

import java.util.concurrent.CopyOnWriteArrayList;

public class ChinGentsai {
    private static ChinGentsai a;
    private CopyOnWriteArrayList<ChangKoehan> b = new CopyOnWriteArrayList<>();

    private ChinGentsai() {
    }

    public static ChinGentsai a() {
        if (a == null) {
            a = new ChinGentsai();
        }
        return a;
    }

    public void a(ChangKoehan changKoehan) {
        this.b.add(changKoehan);
    }

    public void b(ChangKoehan changKoehan) {
        this.b.remove(changKoehan);
    }
}
