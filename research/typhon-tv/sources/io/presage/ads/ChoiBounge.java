package io.presage.ads;

import android.content.Context;
import io.presage.ads.controller.FormatAdController;
import io.presage.ads.controller.IconAdController;
import io.presage.helper.Permissions;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeAsset;

public class ChoiBounge {
    private static final Map<String, Class<? extends NewAdController>> a = new HashMap<String, Class<? extends NewAdController>>() {
        {
            put("format", FormatAdController.class);
            put(PubnativeAsset.ICON, IconAdController.class);
        }
    };
    private static ChoiBounge b;

    public static class KyoKusanagi {
        private int a;
        private Class<? extends NewAdController> b;

        private KyoKusanagi(Class<? extends NewAdController> cls) {
            this.b = cls;
        }

        public KyoKusanagi a(int i) {
            this.a |= i;
            return this;
        }

        public NewAdController a(Context context, io.presage.p029char.ChoiBounge choiBounge, Permissions permissions, NewAd newAd, NewAdViewerDescriptor newAdViewerDescriptor) {
            try {
                return (NewAdController) this.b.getConstructor(new Class[]{Context.class, io.presage.p029char.ChoiBounge.class, Permissions.class, NewAd.class, NewAdViewerDescriptor.class, Integer.TYPE}).newInstance(new Object[]{context, choiBounge, permissions, newAd, newAdViewerDescriptor, Integer.valueOf(this.a)});
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            } catch (NoSuchMethodException e4) {
                e4.printStackTrace();
            }
            return null;
        }
    }

    private ChoiBounge() {
    }

    public static ChoiBounge a() {
        if (b == null) {
            b = new ChoiBounge();
        }
        return b;
    }

    public KyoKusanagi a(String str) {
        if (!a.containsKey(str)) {
            return null;
        }
        return new KyoKusanagi(a.get(str));
    }

    public String a(NewAdController newAdController) {
        for (String next : a.keySet()) {
            if (newAdController.getClass().equals(a.get(next))) {
                return next;
            }
        }
        return null;
    }
}
