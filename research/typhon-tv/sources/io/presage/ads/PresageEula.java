package io.presage.ads;

import android.content.Context;
import io.presage.IEulaHandler;
import io.presage.ads.p027do.KyoKusanagi;
import io.presage.provider.PresageProvider;

public class PresageEula {
    private KyoKusanagi a;

    public PresageEula(Context context) {
        this.a = new KyoKusanagi(context, String.format("%s_default", new Object[]{PresageProvider.b(context)}));
    }

    public IEulaHandler getIEulaHandler() {
        return this.a.b();
    }

    public void launchWithEula() {
        this.a.a();
    }

    public void setIEulaHandler(IEulaHandler iEulaHandler) {
        this.a.a(iEulaHandler);
    }
}
