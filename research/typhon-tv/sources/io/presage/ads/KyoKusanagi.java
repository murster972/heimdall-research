package io.presage.ads;

import android.content.Context;
import io.presage.helper.ChinGentsai;
import io.presage.p029char.ChoiBounge;
import io.presage.p034goto.LuckyGlauber;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class KyoKusanagi {
    private static KyoKusanagi a = null;
    private Map<String, ChangKoehan> b = new HashMap();
    private ChinGentsai c = new ChinGentsai();

    private KyoKusanagi(Context context) {
        this.c.a(context, context.getPackageName());
    }

    public static KyoKusanagi a(Context context) {
        if (a == null) {
            a = new KyoKusanagi(context.getApplicationContext());
        }
        return a;
    }

    public ChangKoehan a(Context context, ChoiBounge choiBounge, JSONObject jSONObject, LuckyGlauber luckyGlauber) {
        GoroDaimon goroDaimon = new GoroDaimon(context, this.c.a(), choiBounge, jSONObject, luckyGlauber);
        this.b.put(goroDaimon.g(), goroDaimon);
        return goroDaimon;
    }

    public ChangKoehan a(String str) {
        return this.b.get(str);
    }

    public void b(String str) {
        this.b.remove(str);
    }
}
