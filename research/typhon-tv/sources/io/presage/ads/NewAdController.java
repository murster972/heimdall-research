package io.presage.ads;

import android.content.Context;
import com.Pinkamena;
import io.presage.ads.NewAdViewerFactory;
import io.presage.helper.Permissions;
import io.presage.p029char.ChoiBounge;
import io.presage.p034goto.Maxima;
import io.presage.p034goto.SaishuKusanagi;

public abstract class NewAdController {
    protected Context a;
    protected NewAd b;
    protected NewAdViewerDescriptor c;
    protected NewAdViewer d;
    protected int e;
    protected Permissions f;
    private boolean g = false;
    private ChoiBounge h;

    public NewAdController(Context context, ChoiBounge choiBounge, Permissions permissions, NewAd newAd, NewAdViewerDescriptor newAdViewerDescriptor, int i) {
        this.a = context;
        this.b = newAd;
        this.c = newAdViewerDescriptor;
        this.e = i;
        this.h = choiBounge;
        this.f = permissions;
    }

    public Context getContext() {
        return this.a;
    }

    public ChoiBounge getWsManager() {
        return this.h;
    }

    public boolean hasFlag(int i) {
        return (this.e & i) != 0;
    }

    public void hideAd() {
        if (!this.g) {
            SaishuKusanagi.c("NewAdController", "The ad is already hidden (or is being hidden).");
            return;
        }
        this.g = false;
        Maxima.a((Runnable) new Runnable() {
            public void run() {
                SaishuKusanagi.b("NewAdController", String.format("Hidding the ad %s", new Object[]{NewAdController.this.b.getId()}));
                NewAdController.this.d.hide();
                NewAdController.this.d = null;
            }
        });
    }

    public boolean isAdDisplayed() {
        return this.g;
    }

    public void showAd() {
        if (this.g) {
            SaishuKusanagi.c("NewAdController", "The ad is already displayed (or is currently displaying).");
            return;
        }
        this.g = true;
        NewAdViewerFactory.KyoKusanagi viewer = NewAdViewerFactory.getInstance().getViewer(this.c);
        if (viewer == null) {
            SaishuKusanagi.c("NewAdController", String.format("Format type %s does not exist.", new Object[]{this.c.getType()}));
        } else {
            this.d = viewer.a(this, this.f, this.b, this.e);
            if (this.d == null) {
                SaishuKusanagi.c("NewAdController", "Unable to display the ad The viewer is null.");
            } else {
                SaishuKusanagi.b("NewAdController", String.format("Showing the ad %s using ther viewer %s", new Object[]{this.b.getId(), this.c.toString()}));
                NewAdViewer newAdViewer = this.d;
                Pinkamena.DianePie();
                return;
            }
        }
        this.g = false;
    }
}
