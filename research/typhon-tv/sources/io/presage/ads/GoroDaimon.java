package io.presage.ads;

import android.content.Context;
import io.presage.helper.Permissions;
import io.presage.p029char.ChoiBounge;
import io.presage.p034goto.HeavyD;
import io.presage.p034goto.LuckyGlauber;
import org.json.JSONObject;

public class GoroDaimon extends BenimaruNikaido {
    public GoroDaimon(Context context, Permissions permissions, ChoiBounge choiBounge, JSONObject jSONObject, LuckyGlauber luckyGlauber) {
        super(context, permissions, choiBounge, jSONObject, luckyGlauber);
    }

    public void m() {
        h().d();
        if (e() != null) {
            e().a(HeavyD.a("close"));
        }
    }
}
