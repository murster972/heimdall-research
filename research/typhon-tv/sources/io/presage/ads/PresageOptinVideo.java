package io.presage.ads;

import android.content.Context;
import com.Pinkamena;
import io.presage.ads.optinvideo.RewardItem;
import io.presage.ads.p027do.GoroDaimon;

public class PresageOptinVideo extends PresageAd {
    private GoroDaimon a;

    public interface PresageOptinVideoCallback {
        void onAdAvailable();

        void onAdClosed();

        void onAdDisplayed();

        void onAdError(int i);

        void onAdLoaded();

        void onAdNotAvailable();

        void onAdRewarded(RewardItem rewardItem);
    }

    public PresageOptinVideo(Context context, String str) {
        super(context, str);
        this.a = new GoroDaimon(context, str);
    }

    public PresageOptinVideo(Context context, String str, RewardItem rewardItem) {
        super(context, str);
        this.a = new GoroDaimon(context, str, rewardItem);
    }

    public PresageOptinVideo(Context context, String str, RewardItem rewardItem, String str2) {
        super(context, str);
        this.a = new GoroDaimon(context, str, rewardItem, str2);
    }

    public PresageOptinVideo(Context context, String str, String str2) {
        super(context, str);
        this.a = new GoroDaimon(context, str, str2);
    }

    public void adToServe() {
        this.a.adToServe();
    }

    public boolean canShow() {
        return this.a.canShow();
    }

    public void destroy() {
        this.a.destroy();
    }

    public PresageOptinVideoCallback getPresageOptinVideoCallback() {
        return this.a.d();
    }

    public RewardItem getRewardItem() {
        return this.a.a();
    }

    public String getUserId() {
        return this.a.b();
    }

    public void load() {
        GoroDaimon goroDaimon = this.a;
        Pinkamena.DianePie();
    }

    public void load(int i) {
        GoroDaimon goroDaimon = this.a;
        Pinkamena.DianePie();
    }

    public void setPresageOptinVideoCallback(PresageOptinVideoCallback presageOptinVideoCallback) {
        this.a.a(presageOptinVideoCallback);
    }

    public void setRewardItem(RewardItem rewardItem) {
        this.a.a(rewardItem);
    }

    public void setUserId(String str) {
        this.a.a(str);
    }

    public void show() {
        GoroDaimon goroDaimon = this.a;
        Pinkamena.DianePie();
    }
}
