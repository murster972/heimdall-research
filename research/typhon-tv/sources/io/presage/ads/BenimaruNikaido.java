package io.presage.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.formats.ChinGentsai;
import io.presage.formats.KyoKusanagi;
import io.presage.helper.Permissions;
import io.presage.p029char.ChoiBounge;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.HeavyD;
import io.presage.p034goto.LuckyGlauber;
import io.presage.p034goto.SaishuKusanagi;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class BenimaruNikaido implements ChangKoehan {
    public static final String a = BenimaruNikaido.class.getSimpleName();
    protected boolean b = false;
    private JSONObject c;
    private KyoKusanagi d;
    private io.presage.p033for.BenimaruNikaido e;
    private GoroDaimon f;
    private io.presage.p033for.KyoKusanagi g;
    private String h;
    private String i;
    private String j;
    private String k;
    private JSONArray l;
    private Context m;
    private ChoiBounge n;
    private ChinGentsai o;
    private io.presage.actions.GoroDaimon p;
    private io.presage.actions.p025do.KyoKusanagi q;
    private LuckyGlauber r;
    private Permissions s;

    public BenimaruNikaido(Context context, Permissions permissions, ChoiBounge choiBounge, JSONObject jSONObject, LuckyGlauber luckyGlauber) {
        a(jSONObject);
        try {
            this.j = this.c.optString("id", (String) null);
            if (this.c.has("ad_unit")) {
                this.k = this.c.getJSONObject("ad_unit").optString("id");
            }
        } catch (JSONException e2) {
            SaishuKusanagi.b(a, e2.getMessage(), e2);
        }
        this.m = context;
        this.s = permissions;
        this.n = choiBounge;
        ChinGentsai.a().a(this);
        this.q = new io.presage.actions.p025do.KyoKusanagi(i(), l());
        this.r = luckyGlauber;
    }

    @TargetApi(4)
    public void a() {
        if (!this.b) {
            try {
                this.f = new GoroDaimon(this.c.getJSONArray("params"));
                this.f.a("url", this.c.getString("link_url"));
                this.h = Integer.toString(this.c.optInt("campaign_id"));
                this.i = Integer.toString(this.c.optInt("campaign_folder"));
                if (this.c.has("ad_unit")) {
                    this.k = this.c.getJSONObject("ad_unit").optString("id");
                }
                this.e = new io.presage.p033for.BenimaruNikaido(c().getJSONObject("advertiser"));
                this.g = new io.presage.p033for.KyoKusanagi(i(), l(), c().getJSONArray("actions"), this.f);
                this.l = c().getJSONArray(NewAd.EVENT_FINISH);
                ApplicationInfo applicationInfo = i().getApplicationInfo();
                if (applicationInfo != null) {
                    int i2 = applicationInfo.labelRes;
                    if (i2 != 0) {
                        this.f.a("app_name", i().getString(i2));
                    } else {
                        this.f.a("app_name", applicationInfo.nonLocalizedLabel);
                    }
                    JSONObject jSONObject = this.c.getJSONObject("format");
                    this.d = j().a(i(), this.s, (String) jSONObject.get("name"), (String) jSONObject.get(VastExtensionXmlManager.TYPE), this, this.c.toString(), new GoroDaimon((JSONArray) jSONObject.get("params")).a(this.f));
                    if (!new io.presage.p034goto.ChinGentsai(this.m, f()).b(Integer.toString(this.c.optInt("campaign_id")))) {
                        SaishuKusanagi.b("AbstractAd", String.format("%s %s %s", new Object[]{"Ad", "create ad:", this.d.f()}));
                        a("served");
                    }
                    this.d.c();
                    if (e() != null) {
                        e().a(HeavyD.a("open"));
                    }
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            } catch (io.presage.formats.GoroDaimon e3) {
                a("format", e3.getMessage());
                SaishuKusanagi.a("AbstractAd", "The viewer of the parsed ad is inconsistent");
            }
        }
    }

    public void a(String str) {
        a(str, (HashMap<String, String>) null);
    }

    public void a(String str, String str2) {
        GoroDaimon goroDaimon = new GoroDaimon();
        goroDaimon.a(VastExtensionXmlManager.TYPE, "error");
        goroDaimon.a("error_type", str);
        goroDaimon.a("error_message", str2);
        io.presage.actions.KyoKusanagi a2 = k().a(i(), l(), "send_event", "send_ad_event", goroDaimon);
        a2.a(this.e.a(), this.h, this.j, this.k);
        a2.a(str, str2);
        a2.k();
    }

    public void a(String str, HashMap<String, String> hashMap) {
        GoroDaimon goroDaimon = new GoroDaimon();
        goroDaimon.a(VastExtensionXmlManager.TYPE, str);
        if (hashMap != null) {
            for (String next : hashMap.keySet()) {
                goroDaimon.a(next, hashMap.get(next));
            }
        }
        io.presage.actions.KyoKusanagi a2 = k().a(i(), l(), "send_event", "send_ad_event", goroDaimon);
        if (hashMap != null) {
            a2.a(hashMap.get("completion"));
        }
        a2.a(this.e.a(), this.h, this.j, this.k);
        a2.k();
    }

    public void a(JSONObject jSONObject) {
        this.c = jSONObject;
    }

    public void b() {
        SaishuKusanagi.b("AbstractAd", String.format("%s %s %s", new Object[]{"Ad", "destroy ad:", h().f()}));
        h().b();
        ChinGentsai.a().b(this);
        KyoKusanagi.a(this.m).b(g());
    }

    public void b(String str) {
        b(str, (HashMap<String, String>) null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:55:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(java.lang.String r11, java.util.HashMap<java.lang.String, java.lang.String> r12) {
        /*
            r10 = this;
            r9 = 1
            r2 = 0
            boolean r0 = r10.b
            if (r0 == 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            java.lang.String r0 = "AbstractAd"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "onAction : "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            io.presage.p034goto.SaishuKusanagi.a(r0, r1)
            io.presage.for.GoroDaimon r5 = new io.presage.for.GoroDaimon
            r5.<init>()
            java.lang.String r0 = "type"
            java.lang.String r1 = "pend_ad_event_install"
            r5.a(r0, r1)
            if (r12 == 0) goto L_0x004d
            java.util.Set r0 = r12.keySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x0039:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x004d
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r3 = r12.get(r0)
            r5.a(r0, r3)
            goto L_0x0039
        L_0x004d:
            java.lang.String r0 = "close"
            boolean r0 = r11.equals(r0)
            if (r0 != 0) goto L_0x005f
            java.lang.String r0 = "completed"
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x011d
        L_0x005f:
            io.presage.formats.KyoKusanagi r0 = r10.h()
            java.lang.String r7 = r0.g()
            org.json.JSONObject r0 = r10.c()     // Catch:{ JSONException -> 0x0114 }
            java.lang.String r1 = "bundle"
            java.lang.Object r6 = r0.get(r1)     // Catch:{ JSONException -> 0x0114 }
            org.json.JSONObject r0 = r10.c()     // Catch:{ JSONException -> 0x016c }
            java.lang.String r1 = "app_launch_package"
            java.lang.Object r4 = r0.get(r1)     // Catch:{ JSONException -> 0x016c }
            org.json.JSONObject r0 = r10.c()     // Catch:{ JSONException -> 0x0171 }
            java.lang.String r1 = "app_launch_class"
            java.lang.Object r3 = r0.get(r1)     // Catch:{ JSONException -> 0x0171 }
            org.json.JSONObject r0 = r10.c()     // Catch:{ JSONException -> 0x0175 }
            java.lang.String r1 = "app_launch_auto"
            java.lang.Object r1 = r0.get(r1)     // Catch:{ JSONException -> 0x0175 }
            org.json.JSONObject r0 = r10.c()     // Catch:{ JSONException -> 0x0178 }
            java.lang.String r8 = "app_launch_rate"
            java.lang.Object r2 = r0.get(r8)     // Catch:{ JSONException -> 0x0178 }
        L_0x009e:
            if (r7 == 0) goto L_0x00f3
            if (r6 == 0) goto L_0x00f3
            java.lang.String r0 = "launch_activity"
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x00f3
            java.lang.String r0 = "bundle"
            r5.a(r0, r6)
            java.lang.String r0 = "app_launch_package"
            r5.a(r0, r4)
            java.lang.String r0 = "app_launch_class"
            r5.a(r0, r3)
            java.lang.String r0 = "app_launch_auto"
            r5.a(r0, r1)
            java.lang.String r0 = "app_launch_rate"
            r5.a(r0, r2)
            io.presage.actions.GoroDaimon r0 = r10.k()
            android.content.Context r1 = r10.i()
            io.presage.char.ChoiBounge r2 = r10.l()
            java.lang.String r3 = "send_event"
            java.lang.String r4 = "pend_ad_event_install"
            io.presage.actions.KyoKusanagi r0 = r0.a(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x00f3
            io.presage.for.BenimaruNikaido r1 = r10.e
            java.lang.String r1 = r1.a()
            java.lang.String r2 = r10.h
            java.lang.String r3 = r10.j
            java.lang.String r4 = r10.k
            r0.a(r1, r2, r3, r4)
            r0.k()
        L_0x00f3:
            java.lang.String r0 = "completed"
            r10.a((java.lang.String) r0, (java.util.HashMap<java.lang.String, java.lang.String>) r12)
            r10.m()
            r10.b()
            r10.b = r9
        L_0x0101:
            io.presage.goto.LuckyGlauber r0 = r10.e()
            if (r0 == 0) goto L_0x0006
            io.presage.goto.LuckyGlauber r0 = r10.e()
            io.presage.goto.HeavyD r1 = io.presage.p034goto.HeavyD.a(r11)
            r0.a(r1)
            goto L_0x0006
        L_0x0114:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
            r6 = r2
        L_0x0119:
            r0.printStackTrace()
            goto L_0x009e
        L_0x011d:
            java.lang.String r0 = "finish "
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0135
            java.lang.String r0 = "finish "
            r10.a((java.lang.String) r0, (java.util.HashMap<java.lang.String, java.lang.String>) r12)
            r10.m()
            r10.b()
            r10.b = r9
            goto L_0x0101
        L_0x0135:
            java.lang.String r0 = "cancel"
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x014d
            java.lang.String r0 = "cancel"
            r10.a((java.lang.String) r0, (java.util.HashMap<java.lang.String, java.lang.String>) r12)
            r10.m()
            r10.b()
            r10.b = r9
            goto L_0x0101
        L_0x014d:
            io.presage.for.KyoKusanagi r0 = r10.g
            io.presage.actions.ChangKoehan r0 = r0.a(r11)
            if (r0 == 0) goto L_0x0101
            io.presage.for.BenimaruNikaido r1 = r10.e
            java.lang.String r1 = r1.a()
            java.lang.String r2 = r10.h
            java.lang.String r3 = r10.j
            java.lang.String r4 = r10.k
            r0.a(r1, r2, r3, r4)
            io.presage.actions.do.KyoKusanagi r1 = r10.d()
            r1.a(r0)
            goto L_0x0101
        L_0x016c:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
            goto L_0x0119
        L_0x0171:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x0119
        L_0x0175:
            r0 = move-exception
            r1 = r2
            goto L_0x0119
        L_0x0178:
            r0 = move-exception
            goto L_0x0119
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.ads.BenimaruNikaido.b(java.lang.String, java.util.HashMap):void");
    }

    public JSONObject c() {
        return this.c;
    }

    public io.presage.actions.p025do.KyoKusanagi d() {
        return this.q;
    }

    public LuckyGlauber e() {
        return this.r;
    }

    public String f() {
        return this.k;
    }

    public String g() {
        return this.j;
    }

    public KyoKusanagi h() {
        return this.d;
    }

    public Context i() {
        return this.m;
    }

    public ChinGentsai j() {
        if (this.o == null) {
            this.o = ChinGentsai.a();
        }
        return this.o;
    }

    public io.presage.actions.GoroDaimon k() {
        if (this.p == null) {
            this.p = io.presage.actions.GoroDaimon.a();
        }
        return this.p;
    }

    public ChoiBounge l() {
        return this.n;
    }
}
