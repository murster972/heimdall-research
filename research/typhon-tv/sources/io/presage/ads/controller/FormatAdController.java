package io.presage.ads.controller;

import android.content.Context;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdViewerDescriptor;
import io.presage.helper.Permissions;
import io.presage.p029char.ChoiBounge;

public class FormatAdController extends DefaultAdController {
    public FormatAdController(Context context, ChoiBounge choiBounge, Permissions permissions, NewAd newAd, NewAdViewerDescriptor newAdViewerDescriptor, int i) {
        super(context, choiBounge, permissions, newAd, newAdViewerDescriptor, i);
    }

    public String getAdHistorySource() {
        return "format";
    }
}
