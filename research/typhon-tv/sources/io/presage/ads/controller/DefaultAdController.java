package io.presage.ads.controller;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import com.Pinkamena;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.actions.KyoKusanagi;
import io.presage.actions.LuckyGlauber;
import io.presage.actions.NewAction;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdController;
import io.presage.ads.NewAdViewerDescriptor;
import io.presage.helper.Permissions;
import io.presage.model.Parameter;
import io.presage.p029char.ChangKoehan;
import io.presage.p029char.ChinGentsai;
import io.presage.p029char.ChoiBounge;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.ChizuruKagura;
import io.presage.p034goto.SaishuKusanagi;
import org.json.JSONException;
import org.json.JSONObject;
import p006do.ShingoYabuki;

public abstract class DefaultAdController extends NewAdController implements NewAd.BenimaruNikaido, NewAd.ChangKoehan, NewAd.ChinGentsai, NewAd.GoroDaimon, NewAd.KyoKusanagi {
    private boolean g;
    private String h = NewAd.EVENT_CANCEL;
    private Handler i;

    public DefaultAdController(Context context, ChoiBounge choiBounge, Permissions permissions, NewAd newAd, NewAdViewerDescriptor newAdViewerDescriptor, int i2) {
        super(context, choiBounge, permissions, newAd, newAdViewerDescriptor, i2);
        this.b.setOnFormatEventListener(this);
        this.b.setOnFormatErrorListener(this);
        this.b.setOnPageFinishedListener(this);
        this.b.setOnExecuteActionListener(this);
    }

    private void cancelHomeDefaultBehaviour() {
        if (this.i != null && this.i.hasMessages(0)) {
            this.i.removeMessages(0);
        }
    }

    /* access modifiers changed from: private */
    public void sendError(String str, String str2) {
        if (getWsManager() != null) {
            GoroDaimon goroDaimon = new GoroDaimon();
            goroDaimon.a(VastExtensionXmlManager.TYPE, "error");
            goroDaimon.a("error_type", str);
            goroDaimon.a("error_message", str2);
            KyoKusanagi a = io.presage.actions.GoroDaimon.a().a(getContext(), getWsManager(), "send_event", "send_ad_event", goroDaimon);
            a.a(this.b.getAdvertiser().getId(), this.b.getCampaignId(), this.b.getId(), this.b.getAdUnitId());
            a.a(str, str2);
            a.k();
            this.h = null;
        }
    }

    private void sendEvent(String str) {
        String videoCompletionRate;
        if (getWsManager() != null) {
            GoroDaimon goroDaimon = new GoroDaimon();
            goroDaimon.a(VastExtensionXmlManager.TYPE, str);
            KyoKusanagi a = io.presage.actions.GoroDaimon.a().a(getContext(), getWsManager(), "send_event", "send_ad_event", goroDaimon);
            if ((str.equals(NewAd.EVENT_COMPLETED) || str.equals(NewAd.EVENT_CANCEL)) && (videoCompletionRate = this.b.getVideoCompletionRate()) != null) {
                a.a(videoCompletionRate);
            }
            a.a(this.b.getAdvertiser().getId(), this.b.getCampaignId(), this.b.getId(), this.b.getAdUnitId());
            a.k();
        }
    }

    public abstract String getAdHistorySource();

    public void hideAd() {
        super.hideAd();
        this.b.setOnVideoErrorListener((NewAd.ChangKoehan) null);
        cancelHomeDefaultBehaviour();
        if (this.h != null) {
            sendEvent(this.h);
            this.h = null;
        }
        this.i = null;
    }

    public void onExecuteAction(String str) {
        io.presage.model.KyoKusanagi actionDescriptor = this.b.getActionDescriptor(str);
        if (actionDescriptor == null) {
            SaishuKusanagi.c("DefaultAdController", String.format("The action %s does not exist.", new Object[]{str}));
            return;
        }
        NewAction a = actionDescriptor.a(this.a, this.f, this.b.getParameters());
        if (a == null) {
            SaishuKusanagi.d("DefaultAdController", String.format("Unable to instantiate the action", new Object[]{str}));
            return;
        }
        try {
            a.execute();
        } catch (LuckyGlauber e) {
            sendError("action:" + str, e.getMessage());
        }
    }

    public void onFormatError(String str, String str2) {
        sendError(str, str2);
    }

    public void onFormatEvent(String str) {
        if (str.equals(NewAd.EVENT_SHOWN) && hasFlag(1)) {
            sendEvent(NewAd.EVENT_SHOWN);
        } else if (str.equals(NewAd.EVENT_COMPLETED)) {
            Parameter parameter = this.b.getFormatDescriptor().getParameter("ad_type");
            Parameter parameter2 = this.b.getParameter("bundle");
            if (!(parameter == null || parameter2 == null || !parameter.getAsString().equals("AppInstall"))) {
                GoroDaimon goroDaimon = new GoroDaimon();
                goroDaimon.a(VastExtensionXmlManager.TYPE, NewAd.EVENT_INSTALL);
                goroDaimon.a("bundle", parameter2);
                KyoKusanagi a = io.presage.actions.GoroDaimon.a().a(getContext(), getWsManager(), "send_event", "pend_ad_event_install", goroDaimon);
                a.a(this.b.getAdvertiser().getId(), this.b.getCampaignId(), this.b.getId(), this.b.getAdUnitId());
                a.k();
            }
            sendEvent(NewAd.EVENT_COMPLETED);
        } else if (str.equals(NewAd.EVENT_REWARD)) {
            Intent intent = new Intent();
            intent.setAction(NewAd.ACTION_REWARD);
            intent.putExtra("id", this.b.getId());
            this.a.sendBroadcast(intent);
        } else if (!str.equals(NewAd.EVENT_CLOSE_SYSTEM_DIALOG) || !hasFlag(16)) {
            if (this.g) {
                return;
            }
            if (str.equals(NewAd.EVENT_FINISH)) {
                this.h = NewAd.EVENT_FINISH;
                hideAd();
                this.g = true;
            } else if (str.equals(NewAd.EVENT_CANCEL)) {
                this.h = NewAd.EVENT_CANCEL;
                hideAd();
                this.g = true;
            }
        } else if (this.i != null && !this.i.hasMessages(0)) {
            this.i.sendEmptyMessageDelayed(0, 300);
        }
    }

    public void onPageFinished(View view, String str, int i2, String str2, String str3) {
        if (view instanceof io.presage.p038long.GoroDaimon) {
            io.presage.p038long.GoroDaimon goroDaimon = (io.presage.p038long.GoroDaimon) view;
            if (goroDaimon.c()) {
                JSONObject jSONObject = new JSONObject();
                final JSONObject a = getWsManager().b().a();
                try {
                    jSONObject.put("campaign_id", (Object) this.b.getCampaignId());
                    jSONObject.put("advert_id", (Object) this.b.getId());
                    jSONObject.put("advertiser_id", (Object) this.b.getAdvertiser().getId());
                    jSONObject.put("id", (Object) this.b.getAdUnitId());
                    jSONObject.put("url", (Object) str);
                    jSONObject.put("source", (Object) getAdHistorySource());
                    if (goroDaimon.d() && i2 == 1) {
                        jSONObject.put("landing", (Object) "true");
                    }
                    if (str2 != null) {
                        jSONObject.put("tracker_pattern", (Object) str2);
                    }
                    if (str3 != null) {
                        jSONObject.put("tracker_url", (Object) str3);
                    }
                    a.put("content", (Object) jSONObject);
                    if (getWsManager() != null) {
                        final ShingoYabuki b = getWsManager().b().b();
                        ChizuruKagura.a().execute(new Runnable() {
                            public void run() {
                                ChangKoehan.a().k().a(ChangKoehan.a().o());
                                DefaultAdController.this.getWsManager().a(DefaultAdController.this.getWsManager().d("ad_history"), b, 1, a.toString(), (ChinGentsai) null, (io.presage.p029char.p030do.KyoKusanagi) null);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onVideoError(String str) {
        sendError("video", str);
        hideAd();
    }

    @TargetApi(3)
    public void showAd() {
        this.i = new Handler(new Handler.Callback() {
            public boolean handleMessage(Message message) {
                DefaultAdController.this.sendError("blocked_ui", "Default behaviour closed the ad");
                DefaultAdController.this.hideAd();
                return true;
            }
        });
        this.b.setOnVideoErrorListener(this);
        Pinkamena.DianePie();
    }
}
