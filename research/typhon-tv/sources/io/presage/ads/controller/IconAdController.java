package io.presage.ads.controller;

import android.content.Context;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdViewerDescriptor;
import io.presage.helper.Permissions;
import io.presage.p029char.ChoiBounge;
import net.pubnative.library.request.PubnativeAsset;

public class IconAdController extends DefaultAdController {
    public IconAdController(Context context, ChoiBounge choiBounge, Permissions permissions, NewAd newAd, NewAdViewerDescriptor newAdViewerDescriptor, int i) {
        super(context, choiBounge, permissions, newAd, newAdViewerDescriptor, i);
    }

    public String getAdHistorySource() {
        return PubnativeAsset.ICON;
    }
}
