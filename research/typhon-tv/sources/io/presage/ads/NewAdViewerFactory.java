package io.presage.ads;

import io.presage.formats.ExecuteFormat;
import io.presage.formats.NewFormatDescriptor;
import io.presage.formats.NewHelperDescriptor;
import io.presage.formats.NewLaunchActivityFormat;
import io.presage.formats.multiwebviews.ActivityMultiViewHelper;
import io.presage.formats.multiwebviews.AlertMultiViewFormat;
import io.presage.helper.Permissions;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class NewAdViewerFactory {
    /* access modifiers changed from: private */
    public static final Map<String, Class<? extends NewAdViewer>> a = new HashMap<String, Class<? extends NewAdViewer>>() {
        {
            put("multi_webviews", AlertMultiViewFormat.class);
            put("launch_activity", NewLaunchActivityFormat.class);
            put("execute", ExecuteFormat.class);
        }
    };
    /* access modifiers changed from: private */
    public static final Map<String, Class<? extends NewAdViewer>> b = new HashMap<String, Class<? extends NewAdViewer>>() {
        {
            put("multi_webviews", ActivityMultiViewHelper.class);
        }
    };
    private static final Map<Class<? extends NewAdViewerDescriptor>, Map<String, Class<? extends NewAdViewer>>> c = new HashMap<Class<? extends NewAdViewerDescriptor>, Map<String, Class<? extends NewAdViewer>>>() {
        {
            put(NewFormatDescriptor.class, NewAdViewerFactory.a);
            put(NewHelperDescriptor.class, NewAdViewerFactory.b);
        }
    };
    private static NewAdViewerFactory d;

    public static class KyoKusanagi {
        private Class<? extends NewAdViewer> a;

        private KyoKusanagi(Class<? extends NewAdViewer> cls) {
            this.a = cls;
        }

        public NewAdViewer a(NewAdController newAdController, Permissions permissions, NewAd newAd, int i) {
            try {
                return (NewAdViewer) this.a.getConstructor(new Class[]{NewAdController.class, NewAd.class, Permissions.class, Integer.TYPE}).newInstance(new Object[]{newAdController, newAd, permissions, Integer.valueOf(i)});
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            } catch (NoSuchMethodException e4) {
                e4.printStackTrace();
            }
            return null;
        }
    }

    private NewAdViewerFactory() {
    }

    public static NewAdViewerFactory getInstance() {
        if (d == null) {
            d = new NewAdViewerFactory();
        }
        return d;
    }

    public KyoKusanagi getViewer(NewAdViewerDescriptor newAdViewerDescriptor) {
        if (newAdViewerDescriptor == null) {
            return null;
        }
        Map map = c.get(newAdViewerDescriptor.getClass());
        if (map == null) {
            return null;
        }
        if (!map.containsKey(newAdViewerDescriptor.getType())) {
            return null;
        }
        return new KyoKusanagi((Class) map.get(newAdViewerDescriptor.getType()));
    }
}
