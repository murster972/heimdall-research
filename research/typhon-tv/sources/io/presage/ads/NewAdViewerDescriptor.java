package io.presage.ads;

import com.p003if.p004do.p005do.GoroDaimon;
import io.presage.model.Parameter;
import java.util.List;

public abstract class NewAdViewerDescriptor {
    @GoroDaimon(m6138 = "type")
    private String a;
    @GoroDaimon(m6138 = "params")
    private List<Parameter> b;

    public NewAdViewerDescriptor() {
    }

    public NewAdViewerDescriptor(String str, List<Parameter> list) {
        this.a = str;
        this.b = list;
    }

    public Parameter getParameter(String str) {
        if (this.b != null) {
            for (Parameter next : this.b) {
                if (next != null && str.equals(next.getName())) {
                    return next;
                }
            }
        }
        return null;
    }

    public <T> T getParameterValue(String str, Class<T> cls) {
        Parameter parameter = getParameter(str);
        if (parameter == null) {
            return null;
        }
        return parameter.get(cls);
    }

    public List<Parameter> getParameters() {
        return this.b;
    }

    public String getType() {
        return this.a;
    }

    public String toString() {
        return getClass().getName() + ":" + this.a;
    }
}
