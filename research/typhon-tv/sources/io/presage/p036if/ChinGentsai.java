package io.presage.p036if;

import io.presage.flatbuffers.Table;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: io.presage.if.ChinGentsai  reason: invalid package */
public class ChinGentsai extends Table {
    public static ChinGentsai a(ByteBuffer byteBuffer) {
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        return new ChinGentsai().a(byteBuffer.getInt(byteBuffer.position()) + byteBuffer.position(), byteBuffer);
    }

    public int a() {
        int __offset = __offset(4);
        if (__offset != 0) {
            return __vector_len(__offset);
        }
        return 0;
    }

    public ChinGentsai a(int i, ByteBuffer byteBuffer) {
        this.bb_pos = i;
        this.bb = byteBuffer;
        return this;
    }

    public GoroDaimon a(int i) {
        return a(new GoroDaimon(), i);
    }

    public GoroDaimon a(GoroDaimon goroDaimon, int i) {
        int __offset = __offset(4);
        if (__offset != 0) {
            return goroDaimon.a(__indirect(__vector(__offset) + (i * 4)), this.bb);
        }
        return null;
    }
}
