package io.presage.p036if;

import io.presage.flatbuffers.Table;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: io.presage.if.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido extends Table {
    public static BenimaruNikaido a(ByteBuffer byteBuffer) {
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        return new BenimaruNikaido().a(byteBuffer.getInt(byteBuffer.position()) + byteBuffer.position(), byteBuffer);
    }

    public int a() {
        int __offset = __offset(4);
        if (__offset != 0) {
            return __vector_len(__offset);
        }
        return 0;
    }

    public BenimaruNikaido a(int i, ByteBuffer byteBuffer) {
        this.bb_pos = i;
        this.bb = byteBuffer;
        return this;
    }

    public KyoKusanagi a(int i) {
        return a(new KyoKusanagi(), i);
    }

    public KyoKusanagi a(KyoKusanagi kyoKusanagi, int i) {
        int __offset = __offset(4);
        if (__offset != 0) {
            return kyoKusanagi.a(__indirect(__vector(__offset) + (i * 4)), this.bb);
        }
        return null;
    }
}
