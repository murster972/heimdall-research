package io.presage.p036if;

import io.presage.flatbuffers.Table;
import java.nio.ByteBuffer;

/* renamed from: io.presage.if.KyoKusanagi  reason: invalid package */
public class KyoKusanagi extends Table {
    public KyoKusanagi a(int i, ByteBuffer byteBuffer) {
        this.bb_pos = i;
        this.bb = byteBuffer;
        return this;
    }

    public String a() {
        int __offset = __offset(12);
        if (__offset != 0) {
            return __string(__offset + this.bb_pos);
        }
        return null;
    }

    public int b() {
        int __offset = __offset(16);
        if (__offset != 0) {
            return this.bb.getInt(__offset + this.bb_pos);
        }
        return 0;
    }
}
