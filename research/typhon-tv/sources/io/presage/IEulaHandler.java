package io.presage;

public interface IEulaHandler {
    void onEulaClosed();

    void onEulaFound();

    void onEulaNotFound();
}
