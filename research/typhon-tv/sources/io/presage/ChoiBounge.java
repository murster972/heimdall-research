package io.presage;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.ads.optinvideo.RewardItem;
import io.presage.p029char.BenimaruNikaido;
import io.presage.p029char.ChangKoehan;
import io.presage.p034goto.ChinGentsai;
import io.presage.p034goto.GoroDaimon;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.parser.AdParser;
import io.presage.parser.BenimaruNikaido;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import p006do.ShingoYabuki;

public class ChoiBounge implements IEulaHandler, io.presage.p031do.BenimaruNikaido, io.presage.p031do.KyoKusanagi {
    public static final String a = ChoiBounge.class.getSimpleName();
    private static final ThreadFactory g = new ThreadFactory() {
        private final AtomicInteger a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "Advertising #" + this.a.getAndIncrement());
        }
    };
    /* access modifiers changed from: private */
    public Context b;
    private Handler c;
    /* access modifiers changed from: private */
    public AdParser d;
    /* access modifiers changed from: private */
    public GoroDaimon e;
    /* access modifiers changed from: private */
    public IEulaHandler f;
    /* access modifiers changed from: private */
    public ExecutorService h;
    /* access modifiers changed from: private */
    public io.presage.parser.BenimaruNikaido i;
    /* access modifiers changed from: private */
    public ChinGentsai j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public JSONObject n;
    private long o;
    private KyoKusanagi p;
    private LinkedList<Long> q;
    private LinkedList<Long> r;
    private int s;
    private String t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public RewardItem v;
    private io.presage.p034goto.GoroDaimon w = new io.presage.p034goto.GoroDaimon();

    private class BenimaruNikaido extends AsyncTask<Void, Void, Boolean> {
        private BenimaruNikaido() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(Void... voidArr) {
            try {
                if (!io.presage.helper.BenimaruNikaido.b(ChoiBounge.this.b)) {
                    return false;
                }
                JSONObject unused = ChoiBounge.this.a(KyoKusanagi.CAN_SHOW);
                if (!ChoiBounge.this.u) {
                    return false;
                }
                if (ChoiBounge.this.h != null) {
                    ChoiBounge.this.h.execute(new Runnable() {
                        public void run() {
                            try {
                                ChoiBounge.this.a(ChoiBounge.this.n, false);
                            } catch (Exception e) {
                                SaishuKusanagi.b("PresageAdvertising", "CanShowTask", e);
                            }
                        }
                    });
                }
                if (ChoiBounge.this.h()) {
                    return false;
                }
                if (ChoiBounge.this.n == null) {
                    return false;
                }
                if (!ChoiBounge.this.n.has("ad")) {
                    return false;
                }
                if (ChoiBounge.this.n.getJSONArray("ad").length() < 1) {
                    return false;
                }
                for (int i = 0; i < ChoiBounge.this.n.getJSONArray("ad").length(); i++) {
                    JSONObject jSONObject = ChoiBounge.this.n.getJSONArray("ad").getJSONObject(i);
                    if (!jSONObject.has("id")) {
                        return false;
                    }
                    if (jSONObject.isNull("id")) {
                        return false;
                    }
                    if (!jSONObject.has("campaign_id")) {
                        return false;
                    }
                    if (jSONObject.isNull("campaign_id")) {
                        return false;
                    }
                    if (!ChoiBounge.this.j.a(jSONObject.getString("campaign_id"), ChoiBounge.this.j.a(ChoiBounge.this.i.a(jSONObject.getString("campaign_id"))))) {
                        return false;
                    }
                }
                return true;
            } catch (Exception e) {
                SaishuKusanagi.b("PresageAdvertising", "CanShowTask", e);
                return false;
            }
        }
    }

    public enum GoroDaimon {
        Direct,
        Precach
    }

    private enum KyoKusanagi {
        LOAD("load"),
        CAN_SHOW("can_show"),
        SHOW("show");
        
        private final String d;

        private KyoKusanagi(String str) {
            this.d = str;
        }

        public String toString() {
            return this.d;
        }
    }

    public ChoiBounge(Context context, io.presage.helper.ChinGentsai chinGentsai, String str, String str2) {
        this.b = context.getApplicationContext();
        this.l = str;
        this.k = str2;
        this.h = Executors.newCachedThreadPool(g);
        this.c = new Handler(Looper.getMainLooper());
        this.d = new AdParser(this.b, ChangKoehan.a().k(), chinGentsai, "ad", this, io.presage.ads.KyoKusanagi.a(this.b));
        this.j = new ChinGentsai(this.b, str);
        this.q = new LinkedList<>();
        this.r = new LinkedList<>();
        this.s = 0;
        this.t = null;
        this.u = true;
    }

    /* access modifiers changed from: private */
    public JSONObject a(KyoKusanagi kyoKusanagi) throws Exception {
        if (this.n != null && System.currentTimeMillis() - this.o < ChangKoehan.a().e() * 1000) {
            SaishuKusanagi.b(a, "lastAdSyncCall was " + ((System.currentTimeMillis() - this.o) / 1000) + "s ago");
            if (!(this.p == KyoKusanagi.SHOW && kyoKusanagi == KyoKusanagi.LOAD)) {
                this.n = b(this.n.toString());
                this.p = kyoKusanagi;
                return this.n;
            }
        }
        this.o = System.currentTimeMillis();
        this.p = kyoKusanagi;
        if (this.s == 0) {
            this.s = ChangKoehan.a().d();
        }
        JSONObject a2 = ChangKoehan.a().k().b().a();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("ad_sync_type", (Object) kyoKusanagi.toString());
        if (this.k != null) {
            jSONObject.put(VastExtensionXmlManager.TYPE, (Object) this.k);
        }
        if (this.l != null) {
            jSONObject.put("ad_unit_id", (Object) this.l);
        }
        jSONObject.put("cached", (Object) j());
        jSONObject.put("ads_to_precache", this.s);
        if (this.t != null) {
            jSONObject.put("campaign_to_load", (Object) this.t);
        }
        a2.put("content", (Object) jSONObject);
        this.t = null;
        if (this.m != null) {
            jSONObject.put("app_user_id", (Object) this.m);
        }
        String d2 = ChangKoehan.a().k().d("ad_sync");
        ShingoYabuki b2 = ChangKoehan.a().k().b().b();
        ChangKoehan.a().k().a(ChangKoehan.a().o());
        String r0 = ChangKoehan.a().k().a(d2, b2, 1, a2.toString()).m6463().m6494();
        this.i = new io.presage.parser.BenimaruNikaido(r0);
        this.n = b(r0);
        io.presage.p034goto.GoroDaimon.a();
        return this.n;
    }

    /* access modifiers changed from: private */
    public void a(final int i2) {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.e != null) {
                            ChoiBounge.this.e.a(i2);
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIADHandler was NULL (onAdError)");
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject, final boolean z) {
        if (this.u) {
            if (jSONObject != null) {
                try {
                    if (jSONObject.has("cache") && jSONObject.getJSONArray("cache").length() != 0) {
                        this.i.a(this.b, this.l, new BenimaruNikaido.C0052BenimaruNikaido() {
                            public void a() {
                                if (z) {
                                    ChoiBounge.this.m();
                                }
                            }

                            public void b() {
                                if (z) {
                                    ChoiBounge.this.a(2);
                                }
                            }
                        });
                        this.j.a(this.i.a());
                    }
                } catch (Exception e2) {
                    SaishuKusanagi.b("PresageAdvertising", "precacheAdsFromAdSync", e2);
                    if (z) {
                        a(0);
                        return;
                    }
                    return;
                }
            }
            if (z) {
                k();
            }
            this.j.a(this.i.a());
        }
    }

    /* access modifiers changed from: private */
    public boolean a(JSONObject jSONObject) {
        String string;
        try {
            if (jSONObject.has("ad") && jSONObject.getJSONArray("ad").length() != 0) {
                JSONArray jSONArray = jSONObject.getJSONArray("ad");
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    if (jSONArray.getJSONObject(i2).has("ad_unit")) {
                        if (jSONArray.getJSONObject(i2).getJSONObject("ad_unit").has("reward_name") && jSONArray.getJSONObject(i2).getJSONObject("ad_unit").has("reward_value")) {
                            this.v = new RewardItem(jSONArray.getJSONObject(i2).getJSONObject("ad_unit").getString("reward_name"), jSONArray.getJSONObject(i2).getJSONObject("ad_unit").getString("reward_value"));
                        }
                        if (jSONArray.getJSONObject(i2).getJSONObject("ad_unit").has(VastExtensionXmlManager.TYPE) && (string = jSONArray.getJSONObject(i2).getJSONObject("ad_unit").getString(VastExtensionXmlManager.TYPE)) != null && !string.isEmpty() && this.k != null && !this.k.isEmpty() && !string.equals(this.k)) {
                            return false;
                        }
                    }
                }
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    private JSONObject b(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        if (this.j == null) {
            this.j = new ChinGentsai(this.b, this.l);
        }
        if (this.i == null) {
            this.i = new io.presage.parser.BenimaruNikaido(str);
        }
        if (!jSONObject.has("ad") || jSONObject.getJSONArray("ad").length() == 0) {
            SaishuKusanagi.d(a, "ad key doesn't exist or is empty");
        } else {
            JSONArray jSONArray = jSONObject.getJSONArray("ad");
            int i2 = 0;
            while (i2 < jSONArray.length()) {
                if (jSONArray.getJSONObject(i2).has("campaign_id")) {
                    String string = jSONArray.getJSONObject(i2).getString("campaign_id");
                    if (string.isEmpty()) {
                        throw new JSONException("campaign_id key is null");
                    }
                    String f2 = this.j.f(string);
                    if (f2 == null || f2.isEmpty()) {
                        SaishuKusanagi.a("prepareAdSyncResponse", "The campaign " + string + " isn't in the cache folder yet");
                    } else {
                        for (BenimaruNikaido.C0043BenimaruNikaido benimaruNikaido : this.j.a(this.i.a(string), string, f2, false)) {
                            str = str.replace(benimaruNikaido.a(), "file://" + benimaruNikaido.b() + File.separator + benimaruNikaido.c());
                        }
                        jSONObject.getJSONArray("ad").put(i2, (Object) new JSONObject(str).getJSONArray("ad").getJSONObject(i2));
                    }
                    i2++;
                } else {
                    throw new JSONException("campaign_id key in ad object doesn't exist");
                }
            }
        }
        this.u = a(jSONObject);
        return jSONObject;
    }

    /* access modifiers changed from: private */
    public void b(JSONObject jSONObject) {
        if (!this.u) {
            a(4);
            return;
        }
        try {
            if (!jSONObject.has("ad") || jSONObject.getJSONArray("ad").length() == 0) {
                k();
            } else {
                this.d.a(jSONObject, GoroDaimon.Precach);
            }
        } catch (Exception e2) {
            SaishuKusanagi.b("PresageAdvertising", "showAdFromAdSync", e2);
            a(0);
        }
    }

    private boolean g() {
        long currentTimeMillis = System.currentTimeMillis();
        long h2 = ChangKoehan.a().h();
        int f2 = ChangKoehan.a().f();
        if (this.q.size() == 0) {
            this.q.addLast(Long.valueOf(currentTimeMillis));
            return false;
        } else if (currentTimeMillis - this.q.getFirst().longValue() >= h2 * 1000) {
            this.q.pollFirst();
            this.q.addLast(Long.valueOf(currentTimeMillis));
            return false;
        } else if (this.q.size() >= f2) {
            return true;
        } else {
            this.q.addLast(Long.valueOf(currentTimeMillis));
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean h() {
        long currentTimeMillis = System.currentTimeMillis();
        long i2 = ChangKoehan.a().i();
        int g2 = ChangKoehan.a().g();
        if (this.r.size() == 0) {
            return false;
        }
        if (currentTimeMillis - this.r.getFirst().longValue() < i2 * 1000) {
            return this.r.size() >= g2;
        }
        return false;
    }

    private boolean i() {
        long currentTimeMillis = System.currentTimeMillis();
        long i2 = ChangKoehan.a().i();
        int g2 = ChangKoehan.a().g();
        if (this.r.size() == 0) {
            this.r.addLast(Long.valueOf(currentTimeMillis));
            return false;
        } else if (currentTimeMillis - this.r.getFirst().longValue() >= i2 * 1000) {
            this.r.pollFirst();
            this.r.addLast(Long.valueOf(currentTimeMillis));
            return false;
        } else if (this.r.size() >= g2) {
            return true;
        } else {
            this.r.addLast(Long.valueOf(currentTimeMillis));
            return false;
        }
    }

    private JSONArray j() {
        JSONArray jSONArray = new JSONArray();
        ArrayList<String> b2 = this.j.b();
        if (b2.size() < 1) {
            return jSONArray;
        }
        Iterator<String> it2 = b2.iterator();
        while (it2.hasNext()) {
            jSONArray.put((Object) it2.next());
        }
        return jSONArray;
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.e != null) {
                            ChoiBounge.this.e.b();
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIADHandler was NULL (onAdNotAvailable)");
                    }
                }
            });
        }
    }

    private void l() {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.e != null) {
                            ChoiBounge.this.e.a();
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIADHandler was NULL (onAdAvailable)");
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.e != null) {
                            ChoiBounge.this.e.c();
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIADHandler was NULL (onAdLoaded)");
                    }
                }
            });
        }
    }

    private void n() {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.e != null) {
                            ChoiBounge.this.e.d();
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIADHandler was NULL (onAdClosed)");
                    }
                }
            });
        }
    }

    private void o() {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.e != null) {
                            ChoiBounge.this.e.e();
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIADHandler was NULL (onAdDisplayed)");
                    }
                }
            });
        }
    }

    private void p() {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.f != null) {
                            ChoiBounge.this.f.onEulaFound();
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIEulaHandler was NULL (onEulaFound)");
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.f != null) {
                            ChoiBounge.this.f.onEulaNotFound();
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIEulaHandler was NULL (onEulaNotFound)");
                    }
                }
            });
        }
    }

    private void r() {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.f != null) {
                            ChoiBounge.this.f.onEulaClosed();
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIEulaHandler was NULL (onEulaClosed)");
                    }
                }
            });
        }
    }

    private void s() {
        if (this.c != null) {
            this.c.post(new Runnable() {
                public void run() {
                    try {
                        if (ChoiBounge.this.e != null) {
                            ((ChinGentsai) ChoiBounge.this.e).a(ChoiBounge.this.v);
                        }
                    } catch (Exception e) {
                        SaishuKusanagi.d(ChoiBounge.a, "mIADHandler was NULL (onAdRewarded)");
                    }
                }
            });
        }
    }

    public void a(int i2, GoroDaimon goroDaimon) {
        if (this.k == null) {
            a(4);
        } else if (i2 < 1) {
            SaishuKusanagi.d(a, "load was called with adsToPrecache=" + i2 + ". Must be > 0");
        } else {
            if (i2 > ChangKoehan.a().c()) {
                i2 = ChangKoehan.a().c();
            }
            this.s = i2;
            this.e = goroDaimon;
            if (!ChangKoehan.a().m() || this.h == null) {
                a(0);
            } else if (g()) {
                a(3);
            } else {
                this.h.execute(new Runnable() {
                    public void run() {
                        try {
                            JSONObject a2 = ChoiBounge.this.a(KyoKusanagi.LOAD);
                            if (!ChoiBounge.this.u) {
                                ChoiBounge.this.a(4);
                                return;
                            }
                            ChoiBounge.this.a(a2, true);
                            io.presage.p034goto.GoroDaimon.a(new GoroDaimon.BenimaruNikaido(GoroDaimon.KyoKusanagi.LOAD, ChoiBounge.this.l));
                        } catch (Exception e) {
                            SaishuKusanagi.b("PresageAdvertising", "load", e);
                            ChoiBounge.this.a(0);
                        }
                    }
                });
            }
        }
    }

    public void a(GoroDaimon goroDaimon) {
        this.e = goroDaimon;
        if (this.k == null) {
            a(4);
        } else if (!ChangKoehan.a().m() || this.h == null) {
            k();
        } else {
            this.h.execute(new Runnable() {
                public void run() {
                    JSONObject a2 = ChangKoehan.a().k().b().a();
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put(VastExtensionXmlManager.TYPE, (Object) ChoiBounge.this.k);
                        if (ChoiBounge.this.l != null) {
                            jSONObject.put("ad_unit_id", (Object) ChoiBounge.this.l);
                        }
                        if (ChoiBounge.this.m != null) {
                            jSONObject.put("app_user_id", (Object) ChoiBounge.this.m);
                        }
                        a2.put("content", (Object) jSONObject);
                        String d = ChangKoehan.a().k().d("ad_to_serve");
                        ShingoYabuki b = ChangKoehan.a().k().b().b();
                        ChangKoehan.a().k().a(ChangKoehan.a().o());
                        ChangKoehan.a().k().a(d, b, 1, a2.toString(), new io.presage.p029char.ChinGentsai() {
                            public void a(int i, String str) {
                                ChoiBounge.this.k();
                            }

                            public void a(String str) {
                                try {
                                    JSONObject jSONObject = new JSONObject(str);
                                    if (!((String) jSONObject.keys().next()).equals("ad")) {
                                        ChoiBounge.this.k();
                                    } else if (!ChoiBounge.this.a(jSONObject)) {
                                        ChoiBounge.this.a(4);
                                    } else {
                                        ChoiBounge.this.d.a(jSONObject, GoroDaimon.Direct);
                                    }
                                } catch (Exception e) {
                                    SaishuKusanagi.b("PresageAdvertising", "adToServe onResponseReceived", e);
                                    ChoiBounge.this.k();
                                }
                            }
                        }, (io.presage.p029char.p030do.KyoKusanagi) null);
                    } catch (JSONException e) {
                        ChoiBounge.this.k();
                        SaishuKusanagi.b("PresageAdvertising", "adToServe", e);
                    }
                }
            });
        }
    }

    public void a(IEulaHandler iEulaHandler) {
        this.f = iEulaHandler;
        if (!ChangKoehan.a().m() || this.h == null) {
            q();
            return;
        }
        this.h.execute(new Runnable() {
            public void run() {
                JSONObject a2 = ChangKoehan.a().k().b().a();
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put(VastExtensionXmlManager.TYPE, (Object) "eula");
                    a2.put("content", (Object) jSONObject);
                    String d = ChangKoehan.a().k().d("launch");
                    ShingoYabuki b = ChangKoehan.a().k().b().b();
                    ChangKoehan.a().k().a(ChangKoehan.a().o());
                    ChangKoehan.a().k().a(d, b, 1, a2.toString(), new io.presage.p029char.ChinGentsai() {
                        public void a(int i, String str) {
                            ChoiBounge.this.q();
                        }

                        public void a(String str) {
                            try {
                                JSONObject jSONObject = new JSONObject(str);
                                if (((String) jSONObject.keys().next()).equals("ad")) {
                                    ChoiBounge.this.d.a(jSONObject, (IEulaHandler) ChoiBounge.this, GoroDaimon.Direct);
                                } else {
                                    ChoiBounge.this.q();
                                }
                            } catch (Exception e) {
                                SaishuKusanagi.b(ChoiBounge.a, e.getMessage(), e);
                                ChoiBounge.this.q();
                            }
                        }
                    }, (io.presage.p029char.p030do.KyoKusanagi) null);
                } catch (JSONException e) {
                    ChoiBounge.this.k();
                    SaishuKusanagi.b(ChoiBounge.a, e.getMessage(), e);
                }
            }
        });
    }

    public void a(String str) {
        if (str != null && !str.isEmpty()) {
            this.m = str;
        }
    }

    public boolean a() {
        if (this.k == null) {
            a(4);
            return false;
        } else if (!ChangKoehan.a().m()) {
            return false;
        } else {
            try {
                boolean booleanValue = ((Boolean) new BenimaruNikaido().execute(new Void[0]).get((long) ChangKoehan.a().o(), TimeUnit.SECONDS)).booleanValue();
                io.presage.p034goto.GoroDaimon.a(new GoroDaimon.BenimaruNikaido(booleanValue, this.l));
                return booleanValue;
            } catch (InterruptedException e2) {
                SaishuKusanagi.b("PresageAdvertising", "canShow", e2);
                io.presage.p034goto.GoroDaimon.a(new GoroDaimon.BenimaruNikaido(false, this.l));
                return false;
            } catch (ExecutionException e3) {
                SaishuKusanagi.b("PresageAdvertising", "canShow", e3);
                io.presage.p034goto.GoroDaimon.a(new GoroDaimon.BenimaruNikaido(false, this.l));
                return false;
            } catch (TimeoutException e4) {
                SaishuKusanagi.b("PresageAdvertising", "canShow", e4);
                io.presage.p034goto.GoroDaimon.a(new GoroDaimon.BenimaruNikaido(false, this.l));
                return false;
            }
        }
    }

    public void b() {
        l();
    }

    public void b(GoroDaimon goroDaimon) {
        a(ChangKoehan.a().d(), goroDaimon);
    }

    public void c() {
        k();
    }

    public void c(GoroDaimon goroDaimon) {
        this.e = goroDaimon;
        if (this.k == null) {
            a(4);
        } else if (!ChangKoehan.a().m() || this.h == null) {
            k();
        } else if (i()) {
            a(3);
        } else {
            this.h.execute(new Runnable() {
                public void run() {
                    boolean z = false;
                    try {
                        JSONObject unused = ChoiBounge.this.a(KyoKusanagi.SHOW);
                        if (!ChoiBounge.this.u) {
                            ChoiBounge.this.a(4);
                            try {
                                io.presage.p034goto.GoroDaimon.a(new GoroDaimon.BenimaruNikaido(GoroDaimon.KyoKusanagi.SHOW, ChoiBounge.this.l));
                            } catch (Exception e) {
                                SaishuKusanagi.d(ChoiBounge.a, "Error during CacheLogger Feature");
                            }
                        } else {
                            if (!(!ChoiBounge.this.n.has("ad") || ChoiBounge.this.n.getJSONArray("ad").length() == 0 || ChoiBounge.this.n.getJSONArray("ad").getJSONObject(0) == null || ChoiBounge.this.n.getJSONArray("ad").getJSONObject(0).getString("campaign_id") == null)) {
                                z = ChoiBounge.this.j.a(ChoiBounge.this.n.getJSONArray("ad").getJSONObject(0).getString("campaign_id"), ChoiBounge.this.j.a(ChoiBounge.this.i.a(ChoiBounge.this.n.getJSONArray("ad").getJSONObject(0).getString("campaign_id"))));
                            }
                            ChoiBounge.this.a(ChoiBounge.this.n, false);
                            if (z) {
                                ChoiBounge.this.b(ChoiBounge.this.n);
                            } else {
                                ChoiBounge.this.k();
                            }
                            try {
                                io.presage.p034goto.GoroDaimon.a(new GoroDaimon.BenimaruNikaido(GoroDaimon.KyoKusanagi.SHOW, ChoiBounge.this.l));
                            } catch (Exception e2) {
                                SaishuKusanagi.d(ChoiBounge.a, "Error during CacheLogger Feature");
                            }
                        }
                    } catch (Exception e3) {
                        SaishuKusanagi.b("PresageAdvertising", "show", e3);
                        ChoiBounge.this.a(0);
                        try {
                            io.presage.p034goto.GoroDaimon.a(new GoroDaimon.BenimaruNikaido(GoroDaimon.KyoKusanagi.SHOW, ChoiBounge.this.l));
                        } catch (Exception e4) {
                            SaishuKusanagi.d(ChoiBounge.a, "Error during CacheLogger Feature");
                        }
                    } catch (Throwable th) {
                        try {
                            io.presage.p034goto.GoroDaimon.a(new GoroDaimon.BenimaruNikaido(GoroDaimon.KyoKusanagi.SHOW, ChoiBounge.this.l));
                        } catch (Exception e5) {
                            SaishuKusanagi.d(ChoiBounge.a, "Error during CacheLogger Feature");
                        }
                        throw th;
                    }
                }
            });
        }
    }

    public void d() {
        n();
    }

    public void e() {
        o();
    }

    public void f() {
        s();
    }

    public void onEulaClosed() {
        r();
    }

    public void onEulaFound() {
        p();
    }

    public void onEulaNotFound() {
        q();
    }
}
