package io.presage;

import android.os.Bundle;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ChangKoehan {
    private Map<String, KyoKusanagi> a = new HashMap();

    private class KyoKusanagi {
        /* access modifiers changed from: private */
        public Bundle b;
        private long c = new Date().getTime();

        public KyoKusanagi(Bundle bundle) {
            this.b = bundle;
        }

        public boolean a() {
            return new Date().getTime() - this.c <= 2700000;
        }
    }

    private void a() {
        for (String next : this.a.keySet()) {
            KyoKusanagi kyoKusanagi = this.a.get(next);
            if (kyoKusanagi == null) {
                this.a.remove(next);
            } else if (!kyoKusanagi.a()) {
                this.a.remove(next);
            }
        }
    }

    public Bundle a(String str) {
        KyoKusanagi kyoKusanagi = this.a.get(str);
        this.a.remove(str);
        if (kyoKusanagi == null || !kyoKusanagi.a()) {
            return null;
        }
        return kyoKusanagi.b;
    }

    public void a(Bundle bundle) {
        String string = bundle.getString("bundle");
        a();
        if (string != null) {
            this.a.put(string, new KyoKusanagi(bundle));
        }
    }
}
