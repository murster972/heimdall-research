package io.presage.flatbuffers;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class Table {
    protected ByteBuffer bb;
    protected int bb_pos;

    protected static boolean __has_identifier(ByteBuffer byteBuffer, String str) {
        if (str.length() != 4) {
            throw new AssertionError("FlatBuffers: file identifier must be length 4");
        }
        for (int i = 0; i < 4; i++) {
            if (str.charAt(i) != ((char) byteBuffer.get(byteBuffer.position() + 4 + i))) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public int __indirect(int i) {
        return this.bb.getInt(i) + i;
    }

    /* access modifiers changed from: protected */
    public int __offset(int i) {
        int i2 = this.bb_pos - this.bb.getInt(this.bb_pos);
        if (i < this.bb.getShort(i2)) {
            return this.bb.getShort(i2 + i);
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public String __string(int i) {
        int i2 = i + this.bb.getInt(i);
        if (this.bb.hasArray()) {
            return new String(this.bb.array(), i2 + 4, this.bb.getInt(i2), Charset.forName("UTF-8"));
        }
        byte[] bArr = new byte[this.bb.getInt(i2)];
        int position = this.bb.position();
        this.bb.position(i2 + 4);
        this.bb.get(bArr);
        this.bb.position(position);
        return new String(bArr, 0, bArr.length, Charset.forName("UTF-8"));
    }

    /* access modifiers changed from: protected */
    public Table __union(Table table, int i) {
        int i2 = this.bb_pos + i;
        table.bb_pos = i2 + this.bb.getInt(i2);
        table.bb = this.bb;
        return table;
    }

    /* access modifiers changed from: protected */
    public int __vector(int i) {
        int i2 = this.bb_pos + i;
        return i2 + this.bb.getInt(i2) + 4;
    }

    /* access modifiers changed from: protected */
    public ByteBuffer __vector_as_bytebuffer(int i, int i2) {
        int __offset = __offset(i);
        if (__offset == 0) {
            return null;
        }
        int position = this.bb.position();
        this.bb.position(__vector(__offset));
        ByteBuffer slice = this.bb.slice();
        this.bb.position(position);
        slice.limit(__vector_len(__offset) * i2);
        return slice;
    }

    /* access modifiers changed from: protected */
    public int __vector_len(int i) {
        int i2 = this.bb_pos + i;
        return this.bb.getInt(i2 + this.bb.getInt(i2));
    }
}
