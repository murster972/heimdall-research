package io.presage.flatbuffers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;

public class FlatBufferBuilder {
    static final Charset c = Charset.forName("UTF-8");
    static final /* synthetic */ boolean j = (!FlatBufferBuilder.class.desiredAssertionStatus());
    ByteBuffer a;
    int b;
    int d = 1;
    int[] e = null;
    int f;
    int[] g = new int[16];
    int h = 0;
    int i = 0;

    public FlatBufferBuilder(int i2) {
        i2 = i2 <= 0 ? 1 : i2;
        this.b = i2;
        this.a = a(i2);
    }

    public FlatBufferBuilder(ByteBuffer byteBuffer) {
        this.a = byteBuffer;
        this.a.clear();
        this.a.order(ByteOrder.LITTLE_ENDIAN);
        this.b = this.a.capacity();
    }

    static ByteBuffer a(int i2) {
        ByteBuffer allocate = ByteBuffer.allocate(i2);
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        return allocate;
    }

    static ByteBuffer a(ByteBuffer byteBuffer) {
        int capacity = byteBuffer.capacity();
        if ((-1073741824 & capacity) != 0) {
            throw new AssertionError("FlatBuffers: cannot grow buffer beyond 2 gigabytes.");
        }
        int i2 = capacity << 1;
        byteBuffer.position(0);
        ByteBuffer a2 = a(i2);
        a2.position(i2 - capacity);
        a2.put(byteBuffer);
        return a2;
    }

    public void Nested(int i2) {
        if (i2 != offset()) {
            throw new AssertionError("FlatBuffers: struct must be serialized inline.");
        }
    }

    public void addByte(byte b2) {
        prep(1, 0);
        putByte(b2);
    }

    public void addByte(int i2, byte b2, int i3) {
        if (b2 != i3) {
            addByte(b2);
            slot(i2);
        }
    }

    public void addDouble(double d2) {
        prep(8, 0);
        putDouble(d2);
    }

    public void addDouble(int i2, double d2, double d3) {
        if (d2 != d3) {
            addDouble(d2);
            slot(i2);
        }
    }

    public void addFloat(float f2) {
        prep(4, 0);
        putFloat(f2);
    }

    public void addFloat(int i2, float f2, double d2) {
        if (((double) f2) != d2) {
            addFloat(f2);
            slot(i2);
        }
    }

    public void addInt(int i2) {
        prep(4, 0);
        putInt(i2);
    }

    public void addInt(int i2, int i3, int i4) {
        if (i3 != i4) {
            addInt(i3);
            slot(i2);
        }
    }

    public void addLong(int i2, long j2, long j3) {
        if (j2 != j3) {
            addLong(j2);
            slot(i2);
        }
    }

    public void addLong(long j2) {
        prep(8, 0);
        putLong(j2);
    }

    public void addOffset(int i2) {
        prep(4, 0);
        if (j || i2 <= offset()) {
            putInt((offset() - i2) + 4);
            return;
        }
        throw new AssertionError();
    }

    public void addOffset(int i2, int i3, int i4) {
        if (i3 != i4) {
            addOffset(i3);
            slot(i2);
        }
    }

    public void addShort(int i2, short s, int i3) {
        if (s != i3) {
            addShort(s);
            slot(i2);
        }
    }

    public void addShort(short s) {
        prep(2, 0);
        putShort(s);
    }

    public void addStruct(int i2, int i3, int i4) {
        if (i3 != i4) {
            Nested(i3);
            slot(i2);
        }
    }

    public int createString(String str) {
        byte[] bytes = str.getBytes(c);
        addByte((byte) 0);
        startVector(1, bytes.length, 1);
        ByteBuffer byteBuffer = this.a;
        int length = this.b - bytes.length;
        this.b = length;
        byteBuffer.position(length);
        this.a.put(bytes, 0, bytes.length);
        return endVector();
    }

    public ByteBuffer dataBuffer() {
        return this.a;
    }

    public int endObject() {
        int i2 = 0;
        if (j || this.e != null) {
            addInt(0);
            int offset = offset();
            for (int length = this.e.length - 1; length >= 0; length--) {
                addShort((short) (this.e[length] != 0 ? offset - this.e[length] : 0));
            }
            addShort((short) (offset - this.f));
            addShort((short) ((this.e.length + 2) * 2));
            int i3 = 0;
            loop1:
            while (true) {
                if (i3 >= this.h) {
                    break;
                }
                int capacity = this.a.capacity() - this.g[i3];
                int i4 = this.b;
                short s = this.a.getShort(capacity);
                if (s == this.a.getShort(i4)) {
                    int i5 = 2;
                    while (i5 < s) {
                        if (this.a.getShort(capacity + i5) == this.a.getShort(i4 + i5)) {
                            i5 += 2;
                        }
                    }
                    i2 = this.g[i3];
                    break loop1;
                }
                i3++;
            }
            if (i2 != 0) {
                this.b = this.a.capacity() - offset;
                this.a.putInt(this.b, i2 - offset);
            } else {
                if (this.h == this.g.length) {
                    this.g = Arrays.copyOf(this.g, this.h * 2);
                }
                int[] iArr = this.g;
                int i6 = this.h;
                this.h = i6 + 1;
                iArr[i6] = offset();
                this.a.putInt(this.a.capacity() - offset, offset() - offset);
            }
            this.e = null;
            return offset;
        }
        throw new AssertionError();
    }

    public int endVector() {
        putInt(this.i);
        return offset();
    }

    public void finish(int i2) {
        prep(this.d, 4);
        addOffset(i2);
        this.a.position(this.b);
    }

    public void finish(int i2, String str) {
        prep(this.d, 8);
        if (str.length() != 4) {
            throw new AssertionError("FlatBuffers: file identifier must be length 4");
        }
        for (int i3 = 3; i3 >= 0; i3--) {
            addByte((byte) str.charAt(i3));
        }
        finish(i2);
    }

    public void notNested() {
        if (this.e != null) {
            throw new AssertionError("FlatBuffers: object serialization must not be nested.");
        }
    }

    public int offset() {
        return this.a.capacity() - this.b;
    }

    public void pad(int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            ByteBuffer byteBuffer = this.a;
            int i4 = this.b - 1;
            this.b = i4;
            byteBuffer.put(i4, (byte) 0);
        }
    }

    public void prep(int i2, int i3) {
        if (i2 > this.d) {
            this.d = i2;
        }
        int capacity = ((((this.a.capacity() - this.b) + i3) ^ -1) + 1) & (i2 - 1);
        while (this.b < capacity + i2 + i3) {
            int capacity2 = this.a.capacity();
            this.a = a(this.a);
            this.b = (this.a.capacity() - capacity2) + this.b;
        }
        pad(capacity);
    }

    public void putByte(byte b2) {
        ByteBuffer byteBuffer = this.a;
        int i2 = this.b - 1;
        this.b = i2;
        byteBuffer.put(i2, b2);
    }

    public void putDouble(double d2) {
        ByteBuffer byteBuffer = this.a;
        int i2 = this.b - 8;
        this.b = i2;
        byteBuffer.putDouble(i2, d2);
    }

    public void putFloat(float f2) {
        ByteBuffer byteBuffer = this.a;
        int i2 = this.b - 4;
        this.b = i2;
        byteBuffer.putFloat(i2, f2);
    }

    public void putInt(int i2) {
        ByteBuffer byteBuffer = this.a;
        int i3 = this.b - 4;
        this.b = i3;
        byteBuffer.putInt(i3, i2);
    }

    public void putLong(long j2) {
        ByteBuffer byteBuffer = this.a;
        int i2 = this.b - 8;
        this.b = i2;
        byteBuffer.putLong(i2, j2);
    }

    public void putShort(short s) {
        ByteBuffer byteBuffer = this.a;
        int i2 = this.b - 2;
        this.b = i2;
        byteBuffer.putShort(i2, s);
    }

    public void required(int i2, int i3) {
        int capacity = this.a.capacity() - i2;
        if (!(this.a.getShort((capacity - this.a.getInt(capacity)) + i3) != 0)) {
            throw new AssertionError("FlatBuffers: field " + i3 + " must be set");
        }
    }

    public byte[] sizedByteArray() {
        return sizedByteArray(this.b, this.a.capacity() - this.b);
    }

    public byte[] sizedByteArray(int i2, int i3) {
        byte[] bArr = new byte[i3];
        this.a.position(i2);
        this.a.get(bArr);
        return bArr;
    }

    public void slot(int i2) {
        this.e[i2] = offset();
    }

    public void startObject(int i2) {
        notNested();
        this.e = new int[i2];
        this.f = offset();
    }

    public void startVector(int i2, int i3, int i4) {
        notNested();
        this.i = i3;
        prep(4, i2 * i3);
        prep(i4, i2 * i3);
    }
}
