package io.presage.helper;

import android.os.Parcel;
import android.os.Parcelable;

public class Permissions implements Parcelable {
    public static final Parcelable.Creator<Permissions> CREATOR = new Parcelable.Creator<Permissions>() {
        /* renamed from: a */
        public Permissions createFromParcel(Parcel parcel) {
            return new Permissions(parcel);
        }

        /* renamed from: a */
        public Permissions[] newArray(int i) {
            return new Permissions[i];
        }
    };
    private String[] a;
    private boolean[] b;
    private int c;

    public Permissions() {
    }

    protected Permissions(Parcel parcel) {
        this.a = parcel.createStringArray();
        this.c = parcel.readInt();
        this.b = parcel.createBooleanArray();
    }

    public void a(int i) {
        this.c = i;
    }

    public void a(String[] strArr, boolean[] zArr) {
        this.a = strArr;
        this.b = zArr;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(this.a);
        parcel.writeInt(this.c);
        parcel.writeBooleanArray(this.b);
    }
}
