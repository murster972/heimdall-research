package io.presage.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import io.presage.p034goto.Whip;

public class BenimaruNikaido {
    public static NetworkInfo a(Context context) {
        if (Whip.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        }
        return null;
    }

    public static boolean b(Context context) {
        return (a(context) == null || 0 == 0) ? false : true;
    }

    public static boolean c(Context context) {
        NetworkInfo a = a(context);
        return (a == null || 0 == 0 || a.getType() != 1) ? false : true;
    }
}
