package io.presage.helper;

import android.content.Context;
import android.database.Cursor;
import io.presage.p034goto.Mature;
import io.presage.parser.ChinGentsai;
import io.presage.parser.p040do.BenimaruNikaido;
import io.presage.provider.PresageProvider;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class KyoKusanagi implements ChinGentsai.KyoKusanagi {
    private Context a;
    private BenimaruNikaido b;
    private C0049KyoKusanagi c;

    /* renamed from: io.presage.helper.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    public interface C0049KyoKusanagi {
        void a();

        void a(BenimaruNikaido benimaruNikaido);
    }

    public KyoKusanagi(Context context, C0049KyoKusanagi kyoKusanagi) {
        this.a = context;
        this.c = kyoKusanagi;
        b();
    }

    private void d() {
        this.b = BenimaruNikaido.a;
        this.b.b(PresageProvider.b(this.a));
        if (this.c != null) {
            this.c.a(this.b);
        }
    }

    public void a() {
        if (this.c != null) {
            this.c.a();
        }
    }

    public void a(List<BenimaruNikaido> list, JSONObject jSONObject) {
        Iterator<BenimaruNikaido> it2 = list.iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            BenimaruNikaido next = it2.next();
            if (next.b() != null && next.b().equals(PresageProvider.b(this.a))) {
                this.b = next;
                break;
            }
        }
        if (this.b != null) {
            Mature.a(this.a, list, jSONObject.toString());
            if (this.c != null) {
                this.c.a(this.b);
            }
        }
    }

    public void b() {
        String str = null;
        Cursor a2 = new io.presage.provider.KyoKusanagi(this.a.getContentResolver()).a(PresageProvider.a(PresageProvider.KyoKusanagi.PROFIG_JSON, PresageProvider.a(this.a)), (String[]) null, (String) null, (String[]) null, (String) null);
        if (a2 == null) {
            a2 = new io.presage.provider.KyoKusanagi(this.a.getContentResolver()).a(PresageProvider.a(PresageProvider.KyoKusanagi.PROFIG_JSON, PresageProvider.a(this.a)), (String[]) null, (String) null, (String[]) null, (String) null);
        }
        if (a2 != null) {
            if (a2.moveToLast()) {
                str = a2.getString(a2.getColumnIndex("v"));
            }
            a2.close();
        }
        if (str != null) {
            try {
                new ChinGentsai("profig", this).a(new JSONObject(str));
            } catch (JSONException e) {
                d();
                e.printStackTrace();
            }
        } else {
            d();
        }
    }

    public BenimaruNikaido c() {
        return this.b;
    }
}
