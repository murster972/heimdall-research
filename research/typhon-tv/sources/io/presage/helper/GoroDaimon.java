package io.presage.helper;

import io.presage.finder.ChinGentsai;
import io.presage.finder.HeavyD;
import io.presage.parser.p040do.BenimaruNikaido;
import java.util.HashMap;
import java.util.Map;

public class GoroDaimon {
    private Map<String, HeavyD> a = new HashMap();
    private ChinGentsai b;

    public GoroDaimon(ChinGentsai chinGentsai) {
        this.b = chinGentsai;
    }

    public HeavyD a(String str) {
        return this.a.get(str);
    }

    public void a(BenimaruNikaido benimaruNikaido) {
        this.a.clear();
        for (String next : benimaruNikaido.d()) {
            HeavyD a2 = this.b.a(next);
            if (a2 != null) {
                this.a.put(next, a2);
            }
        }
    }
}
