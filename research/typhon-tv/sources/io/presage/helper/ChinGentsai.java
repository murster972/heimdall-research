package io.presage.helper;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.p000do.p001do.p002do.BenimaruNikaido;
import android.util.Log;
import io.presage.p034goto.Mature;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.provider.KyoKusanagi;
import io.presage.provider.PresageProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ChinGentsai {
    public static final String a = ChinGentsai.class.getSimpleName();
    public static final HashMap<String, Integer> b = new HashMap<String, Integer>() {
        {
            put("android.permission.INTERNET", 1);
            put("android.permission.ACCESS_NETWORK_STATE", 2);
            put("android.permission.RECEIVE_BOOT_COMPLETED", 4);
            put("android.permission.SYSTEM_ALERT_WINDOW", 8);
            put("com.android.browser.permission.READ_HISTORY_BOOKMARKS", 16);
            put("com.android.browser.permission.WRITE_HISTORY_BOOKMARKS", 32);
            put("com.android.launcher.permission.INSTALL_SHORTCUT", 64);
            put("com.android.launcher.permission.UNINSTALL_SHORTCUT", 128);
            put("android.permission.WRITE_EXTERNAL_STORAGE", 256);
            put("android.permission.GET_ACCOUNTS", 512);
        }
    };
    public static final HashMap<Integer, String> c = new HashMap<Integer, String>() {
        {
            put(1, "android.permission.INTERNET");
            put(2, "android.permission.ACCESS_NETWORK_STATE");
            put(4, "android.permission.RECEIVE_BOOT_COMPLETED");
            put(8, "android.permission.SYSTEM_ALERT_WINDOW");
            put(16, "com.android.browser.permission.READ_HISTORY_BOOKMARKS");
            put(32, "com.android.browser.permission.WRITE_HISTORY_BOOKMARKS");
            put(64, "com.android.launcher.permission.INSTALL_SHORTCUT");
            put(128, "com.android.launcher.permission.UNINSTALL_SHORTCUT");
            put(256, "android.permission.WRITE_EXTERNAL_STORAGE");
            put(512, "android.permission.GET_ACCOUNTS");
        }
    };
    private Context d;
    private PackageManager e;
    private PackageInfo f;
    private Permissions g;
    private List<String> h;

    private void a(boolean z) {
        String str;
        try {
            str = this.f.applicationInfo.metaData.get("presage_key").toString();
        } catch (Exception e2) {
            str = null;
        }
        if (str != null && !str.isEmpty()) {
            KyoKusanagi kyoKusanagi = new KyoKusanagi(this.d.getContentResolver());
            ContentValues contentValues = new ContentValues();
            contentValues.put("package", this.d.getPackageName());
            contentValues.put("apikey", str);
            contentValues.put("update_time", Long.valueOf(System.currentTimeMillis()));
            kyoKusanagi.a(PresageProvider.a(PresageProvider.KyoKusanagi.APIKEY, PresageProvider.a(this.d)), contentValues, (String) null, (String[]) null);
            Mature.e(this.d);
        } else if (z) {
            a("[Missing metadata] : presage_key");
        }
    }

    public static boolean a(Context context, int i) {
        for (Integer intValue : c.keySet()) {
            if ((intValue.intValue() & i) == i && !b(context, c.get(Integer.valueOf(i)))) {
                return false;
            }
        }
        return true;
    }

    public static boolean b(Context context, String str) {
        int i;
        boolean z = true;
        try {
            i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.targetSdkVersion;
        } catch (Exception e2) {
            SaishuKusanagi.b(a, e2.getMessage(), e2);
            i = 0;
        }
        if (Build.VERSION.SDK_INT < 23) {
            try {
                if (context.getPackageManager().checkPermission(str, context.getPackageName()) != 0) {
                    z = false;
                }
                return z;
            } catch (RuntimeException e3) {
                return false;
            }
        } else if (i >= 23) {
            return context.checkSelfPermission(str) == 0;
        } else {
            if (BenimaruNikaido.m53(context, str) != 0) {
                z = false;
            }
            return z;
        }
    }

    private void d() {
        if (this.f.requestedPermissions != null) {
            String[] strArr = this.f.requestedPermissions;
            int length = strArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                String str = strArr[i];
                Integer num = b.get(str);
                int intValue = num != null ? num.intValue() | i2 : i2;
                this.h.add(str);
                i++;
                i2 = intValue;
            }
            int size = this.h.size();
            String[] strArr2 = new String[size];
            boolean[] zArr = new boolean[size];
            for (int i3 = 0; i3 < size; i3++) {
                String str2 = this.h.get(i3);
                strArr2[i3] = str2.substring(str2.lastIndexOf(".") + 1);
                zArr[i3] = this.e.checkPermission(this.h.get(i3), this.d.getPackageName()) == 0;
            }
            this.g.a(strArr2, zArr);
            this.g.a(i2);
        }
    }

    private void e() {
        if (this.f != null) {
            if (this.f.requestedPermissions == null) {
                a("[Missing permission] : INTERNET, ACCESS_NETWORK_STATE");
                return;
            }
            boolean z = false;
            boolean z2 = false;
            for (String str : this.f.requestedPermissions) {
                if ("android.permission.INTERNET".equals(str)) {
                    z2 = true;
                }
                if ("android.permission.ACCESS_NETWORK_STATE".equals(str)) {
                    z = true;
                }
            }
            if (!z2) {
                a("[Missing permission] : INTERNET");
            }
            if (!z) {
                a("[Missing permission] : ACCESS_NETWORK_STATE");
            }
        }
    }

    private void f() {
        if (this.f != null) {
            if (this.f.providers == null) {
                a("[Missing provider] : PresageProvider");
                return;
            }
            boolean z = false;
            for (ProviderInfo providerInfo : this.f.providers) {
                if ("io.presage.provider.PresageProvider".equals(providerInfo.name)) {
                    if (!providerInfo.enabled) {
                        a("[PresageProvider Wrong Param] : PresageProvider should be enabled");
                    }
                    if (!providerInfo.exported) {
                        a("[PresageProvider Wrong Param] : PresageProvider should be exported");
                    }
                    if (!(this.f.packageName + ".PresageProvider").equals(providerInfo.authority)) {
                        a("[PresageProvider Wrong Param] : authority should be set as android:authorities=\"${applicationId}.PresageProvider\"");
                    }
                    z = true;
                }
            }
            if (!z) {
                a("[Missing permission] : PresageProvider");
            }
        }
    }

    private void g() {
        boolean z;
        if (this.f != null) {
            if (this.f.services == null) {
                a("[Missing Service] : PresageService");
            }
            List<ResolveInfo> queryIntentServices = this.d.getPackageManager().queryIntentServices(new Intent("io.presage.PresageService.PIVOT"), 0);
            if (queryIntentServices == null || queryIntentServices.size() == 0) {
                a("[Missing Service] : missing PresageService with PIVOT Action");
            }
            Iterator<ResolveInfo> it2 = queryIntentServices.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (this.f.packageName.equals(it2.next().serviceInfo.packageName)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                a("[Missing Service] : missing PresageService with PIVOT Action");
            }
            for (ServiceInfo serviceInfo : this.f.services) {
                if (serviceInfo.name.equals("io.presage.PresageService")) {
                    if (!serviceInfo.enabled) {
                        a("[PresageService Wrong Param] : PresageService should be enabled, set android:enabled=\"true\" ");
                    }
                    if (!serviceInfo.exported) {
                        a("[PresageService Wrong Param] : PresageService should be exported, set android:exported=\"true\"");
                    }
                    if (!(this.f.packageName + ":remote").equals(serviceInfo.processName)) {
                        a("[PresageService Wrong Param] : PresageService's process should be set as android:process=\":remote\" ");
                        return;
                    }
                    return;
                }
            }
            a("[Missing Service] : missing PresageService");
        }
    }

    private void h() {
        if (this.f != null) {
            if (this.f.activities == null) {
                a("[Missing Activity] : PresageActivity");
            }
            for (ActivityInfo activityInfo : this.f.activities) {
                if (activityInfo.name.equals("io.presage.activities.PresageActivity") && activityInfo.launchMode == 0) {
                    int i = Build.VERSION.SDK_INT < 13 ? 176 : 1200;
                    if ((activityInfo.configChanges & i) != i) {
                        a("[Issue on Activity] : PresageActivity must be : android:configChanges=\"keyboard|keyboardHidden|orientation|screenSize\"");
                    } else {
                        return;
                    }
                } else if (activityInfo.name.equals("io.presage.activities.PresageActivity") && activityInfo.launchMode != 0) {
                    a("[Issue on Activity] : PresageActivity - bad launch mode, remove any launchMode from the manifest");
                }
            }
            a("[Missing Activity] : PresageActivity");
        }
    }

    private void i() {
        boolean z;
        if (this.f != null) {
            if (this.f.receivers == null) {
                a("[Missing Receiver] : NetworkChangeReceiver，AlarmReceiver");
            }
            List<ResolveInfo> queryBroadcastReceivers = this.d.getPackageManager().queryBroadcastReceivers(new Intent("io.presage.receiver.NetworkChangeReceiver.ONDESTROY"), 0);
            if (queryBroadcastReceivers == null || queryBroadcastReceivers.size() == 0) {
                a("[Missing Receiver] : NetworkChangeReceiver with Destroy Action");
            }
            Iterator<ResolveInfo> it2 = queryBroadcastReceivers.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (this.f.packageName.equals(it2.next().activityInfo.packageName)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                a("[Missing Receiver] : NetworkChangeReceiver with Destroy Action");
            }
            boolean z2 = false;
            boolean z3 = false;
            for (ActivityInfo activityInfo : this.f.receivers) {
                if ("io.presage.receiver.NetworkChangeReceiver".equals(activityInfo.name)) {
                    z3 = true;
                }
                if ("io.presage.receiver.AlarmReceiver".equals(activityInfo.name)) {
                    z2 = true;
                }
            }
            if (!z3) {
                a("[Missing Receiver] : NetworkChangeReceiver");
            }
            if (!z2) {
                a("[Missing Receiver] : AlarmReceiver");
            }
        }
    }

    public Permissions a() {
        return this.g;
    }

    public void a(Context context, String str) {
        this.g = new Permissions();
        this.h = new ArrayList();
        this.d = context;
        try {
            this.e = context.getPackageManager();
            this.f = this.e.getPackageInfo(str, 4271);
            d();
        } catch (Exception e2) {
            SaishuKusanagi.b("PackageHelper", "initialize", e2);
        }
    }

    public void a(String str) {
        Log.e("PRESAGE", str);
    }

    public String[] b() {
        ArrayList arrayList = new ArrayList();
        for (String next : this.h) {
            if (b(this.d, next)) {
                arrayList.add(next.substring(next.lastIndexOf(".") + 1));
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public void c() {
        if (this.f != null && this.d != null) {
            SharedPreferences sharedPreferences = this.d.getSharedPreferences("presage", 0);
            if (!sharedPreferences.getBoolean("manifchecked", false)) {
                a(true);
                e();
                h();
                i();
                g();
                f();
                sharedPreferences.edit().putBoolean("manifchecked", true).commit();
                return;
            }
            SaishuKusanagi.a(a, "manifest is already checked");
            String b2 = PresageProvider.b(this.d);
            if (b2 == null || b2.isEmpty()) {
                SaishuKusanagi.a("TAG", "api key in db is null or empty, we update it from manifest to db");
                a(false);
            }
        }
    }
}
