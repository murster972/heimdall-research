package io.presage.p041try;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

/* renamed from: io.presage.try.KyoKusanagi  reason: invalid package */
public class KyoKusanagi {
    private String a = "";
    private LocalSocket b = null;
    private LocalSocketAddress c = null;

    public KyoKusanagi(String str) {
        this.a = str;
    }

    public void a() throws IOException {
        if (this.b != null) {
            this.b.shutdownInput();
            this.b.shutdownOutput();
            this.b.close();
            this.b = null;
            this.c = null;
        }
    }

    public boolean a(int i) throws IOException {
        if (!this.a.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
            this.c = new LocalSocketAddress(this.a, LocalSocketAddress.Namespace.ABSTRACT);
        } else {
            this.c = new LocalSocketAddress(this.a, LocalSocketAddress.Namespace.FILESYSTEM);
        }
        this.b = new LocalSocket();
        this.b.connect(this.c);
        this.b.setSendBufferSize(131072);
        this.b.setReceiveBufferSize(1048576);
        this.b.setSoTimeout(i * 1000);
        return true;
    }

    public boolean b() {
        if (this.b == null) {
            return false;
        }
        return this.b.isConnected();
    }

    public OutputStream c() throws IOException {
        if (this.b == null) {
            return null;
        }
        return this.b.getOutputStream();
    }

    public InputStream d() throws IOException {
        if (this.b == null) {
            return null;
        }
        return this.b.getInputStream();
    }
}
