package io.presage.p041try;

import android.content.Context;
import android.os.AsyncTask;
import io.presage.flatbuffers.FlatBufferBuilder;
import io.presage.p034goto.ChoiBounge;
import io.presage.p034goto.SaishuKusanagi;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.Semaphore;

/* renamed from: io.presage.try.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido {
    private static BenimaruNikaido a = null;
    /* access modifiers changed from: private */
    public static PriorityQueue<C0055BenimaruNikaido> d = null;
    private static ChinGentsai f = null;
    private Context b = null;
    /* access modifiers changed from: private */
    public KyoKusanagi c = null;
    /* access modifiers changed from: private */
    public final Semaphore e = new Semaphore(1, true);

    /* renamed from: io.presage.try.BenimaruNikaido$BenimaruNikaido  reason: collision with other inner class name */
    private class C0055BenimaruNikaido {
        public byte[] a = null;
        public long b = 0;
        public GoroDaimon c = null;
        public byte[] d = null;

        C0055BenimaruNikaido(byte[] bArr, long j, GoroDaimon goroDaimon) {
            this.a = bArr;
            this.b = j;
            this.c = goroDaimon;
        }

        public boolean equals(Object obj) {
            return (obj instanceof C0055BenimaruNikaido) && ((C0055BenimaruNikaido) obj).c == this.c;
        }
    }

    /* renamed from: io.presage.try.BenimaruNikaido$ChinGentsai */
    private class ChinGentsai extends AsyncTask<Void, C0055BenimaruNikaido, Void> {
        private ChinGentsai() {
        }

        private boolean a() {
            if (!BenimaruNikaido.this.g()) {
                boolean unused = BenimaruNikaido.this.h();
                if (!BenimaruNikaido.this.f()) {
                    return false;
                }
            }
            return true;
        }

        private byte[] a(C0055BenimaruNikaido benimaruNikaido) {
            try {
                FlatBufferBuilder flatBufferBuilder = new FlatBufferBuilder(1);
                int[] iArr = new int[benimaruNikaido.a.length];
                for (int i = 0; i < benimaruNikaido.a.length; i++) {
                    iArr[i] = GoroDaimon.a(flatBufferBuilder, benimaruNikaido.a[i], GoroDaimon.a(flatBufferBuilder, new byte[1]));
                }
                int a2 = ChinGentsai.a(flatBufferBuilder, iArr);
                ChinGentsai.a(flatBufferBuilder);
                ChinGentsai.a(flatBufferBuilder, (byte) 0);
                ChinGentsai.a(flatBufferBuilder, a2);
                ChinGentsai.b(flatBufferBuilder, ChinGentsai.b(flatBufferBuilder));
                OutputStream c = BenimaruNikaido.this.c.c();
                InputStream d = BenimaruNikaido.this.c.d();
                c.write(flatBufferBuilder.sizedByteArray());
                byte[] bArr = new byte[4];
                if (d.read(bArr, 0, 4) == 0) {
                    throw new IOException("Unable to get transfer size");
                }
                int i2 = (bArr[0] & 255) | ((bArr[1] & 255) << 8) | ((bArr[2] & 255) << 16) | ((bArr[3] & 255) << 24);
                if (i2 > 1048576) {
                    throw new Exception("Excced memory limit");
                } else if (i2 == 0) {
                    throw new Exception("No Data");
                } else {
                    byte[] bArr2 = new byte[i2];
                    for (int i3 = 0; i3 != i2; i3 += d.read(bArr2, i3, i2 - i3)) {
                    }
                    return bArr2;
                }
            } catch (Exception e) {
                return null;
            }
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0062  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x006c  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void doInBackground(java.lang.Void... r10) {
            /*
                r9 = this;
                r2 = 0
                r8 = 1
                r1 = r2
            L_0x0003:
                boolean r0 = r9.a()
                if (r0 != 0) goto L_0x000f
                io.presage.try.BenimaruNikaido r0 = io.presage.p041try.BenimaruNikaido.this
                r0.a((int) r8)
                goto L_0x0003
            L_0x000f:
                io.presage.try.BenimaruNikaido r0 = io.presage.p041try.BenimaruNikaido.this     // Catch:{ InterruptedException -> 0x008c }
                java.util.concurrent.Semaphore r0 = r0.e     // Catch:{ InterruptedException -> 0x008c }
                r0.acquire()     // Catch:{ InterruptedException -> 0x008c }
                java.util.PriorityQueue r0 = io.presage.p041try.BenimaruNikaido.d     // Catch:{ InterruptedException -> 0x008c }
                java.lang.Object r0 = r0.peek()     // Catch:{ InterruptedException -> 0x008c }
                io.presage.try.BenimaruNikaido$BenimaruNikaido r0 = (io.presage.p041try.BenimaruNikaido.C0055BenimaruNikaido) r0     // Catch:{ InterruptedException -> 0x008c }
                io.presage.try.BenimaruNikaido r1 = io.presage.p041try.BenimaruNikaido.this     // Catch:{ InterruptedException -> 0x0089 }
                java.util.concurrent.Semaphore r1 = r1.e     // Catch:{ InterruptedException -> 0x0089 }
                r1.release()     // Catch:{ InterruptedException -> 0x0089 }
                r1 = r0
            L_0x002c:
                if (r1 != 0) goto L_0x0034
                io.presage.try.BenimaruNikaido r0 = io.presage.p041try.BenimaruNikaido.this
                r0.a((int) r8)
                goto L_0x0003
            L_0x0034:
                long r4 = r1.b
                long r6 = java.lang.System.currentTimeMillis()
                int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r0 <= 0) goto L_0x0044
                io.presage.try.BenimaruNikaido r0 = io.presage.p041try.BenimaruNikaido.this
                r0.a((int) r8)
                goto L_0x0003
            L_0x0044:
                io.presage.try.BenimaruNikaido r0 = io.presage.p041try.BenimaruNikaido.this     // Catch:{ InterruptedException -> 0x0069 }
                java.util.concurrent.Semaphore r0 = r0.e     // Catch:{ InterruptedException -> 0x0069 }
                r0.acquire()     // Catch:{ InterruptedException -> 0x0069 }
                java.util.PriorityQueue r0 = io.presage.p041try.BenimaruNikaido.d     // Catch:{ InterruptedException -> 0x0069 }
                java.lang.Object r0 = r0.poll()     // Catch:{ InterruptedException -> 0x0069 }
                io.presage.try.BenimaruNikaido$BenimaruNikaido r0 = (io.presage.p041try.BenimaruNikaido.C0055BenimaruNikaido) r0     // Catch:{ InterruptedException -> 0x0069 }
                io.presage.try.BenimaruNikaido r1 = io.presage.p041try.BenimaruNikaido.this     // Catch:{ InterruptedException -> 0x0086 }
                java.util.concurrent.Semaphore r1 = r1.e     // Catch:{ InterruptedException -> 0x0086 }
                r1.release()     // Catch:{ InterruptedException -> 0x0086 }
            L_0x0060:
                if (r0 != 0) goto L_0x006c
                io.presage.try.BenimaruNikaido r1 = io.presage.p041try.BenimaruNikaido.this
                r1.a((int) r8)
                r1 = r0
                goto L_0x0003
            L_0x0069:
                r0 = move-exception
            L_0x006a:
                r0 = r1
                goto L_0x0060
            L_0x006c:
                byte[] r1 = r9.a((io.presage.p041try.BenimaruNikaido.C0055BenimaruNikaido) r0)
                r0.d = r1
                byte[] r1 = r0.d
                if (r1 != 0) goto L_0x007b
                io.presage.try.BenimaruNikaido r1 = io.presage.p041try.BenimaruNikaido.this
                r1.d()
            L_0x007b:
                io.presage.try.BenimaruNikaido$BenimaruNikaido[] r1 = new io.presage.p041try.BenimaruNikaido.C0055BenimaruNikaido[r8]
                r3 = 0
                r1[r3] = r0
                r9.publishProgress(r1)
                r1 = r2
                goto L_0x0003
            L_0x0086:
                r1 = move-exception
                r1 = r0
                goto L_0x006a
            L_0x0089:
                r1 = move-exception
                r1 = r0
                goto L_0x002c
            L_0x008c:
                r0 = move-exception
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: io.presage.p041try.BenimaruNikaido.ChinGentsai.doInBackground(java.lang.Void[]):java.lang.Void");
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(C0055BenimaruNikaido... benimaruNikaidoArr) {
            if (benimaruNikaidoArr.length != 0) {
                C0055BenimaruNikaido benimaruNikaido = benimaruNikaidoArr[benimaruNikaidoArr.length - 1];
                benimaruNikaido.c.a(benimaruNikaido.d);
                benimaruNikaido.c = null;
                benimaruNikaido.a = null;
                benimaruNikaido.d = null;
            }
        }
    }

    /* renamed from: io.presage.try.BenimaruNikaido$GoroDaimon */
    public interface GoroDaimon {
        void a(byte[] bArr);
    }

    /* renamed from: io.presage.try.BenimaruNikaido$KyoKusanagi */
    private class KyoKusanagi implements Comparator<C0055BenimaruNikaido> {
        private KyoKusanagi() {
        }

        /* renamed from: a */
        public int compare(C0055BenimaruNikaido benimaruNikaido, C0055BenimaruNikaido benimaruNikaido2) {
            if (benimaruNikaido.b > benimaruNikaido2.b) {
                return 1;
            }
            return benimaruNikaido.b < benimaruNikaido2.b ? -1 : 0;
        }
    }

    private BenimaruNikaido() {
        try {
            Class.forName("android.os.AsyncTask");
        } catch (Throwable th) {
        }
        d = new PriorityQueue<>(1, new KyoKusanagi());
    }

    public static BenimaruNikaido a() {
        return a;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        try {
            Thread.sleep((long) (i * 1000));
        } catch (InterruptedException e2) {
        }
    }

    public static void a(Context context) {
        if (a == null) {
            a = new BenimaruNikaido();
            a.b = context;
            a.b();
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        try {
            io.presage.p032else.KyoKusanagi a2 = io.presage.p032else.KyoKusanagi.a(this.b);
            this.c.a(10);
            OutputStream c2 = this.c.c();
            SaishuKusanagi.a("TEST", "CONNECT 1");
            if (c2 == null) {
                throw new IOException();
            }
            SaishuKusanagi.a("TEST", "CONNECT 2");
            c2.write(a2.c().getBytes());
            return true;
        } catch (IOException | Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean g() {
        if (this.c == null) {
            return false;
        }
        return this.c.b();
    }

    /* access modifiers changed from: private */
    public boolean h() {
        SaishuKusanagi.a("TEST", "WOOOOT");
        return ChoiBounge.d(this.b);
    }

    public void a(GoroDaimon goroDaimon) {
        try {
            C0055BenimaruNikaido benimaruNikaido = new C0055BenimaruNikaido((byte[]) null, 0, goroDaimon);
            this.e.acquire();
            d.remove(benimaruNikaido);
            this.e.release();
        } catch (InterruptedException e2) {
        }
    }

    public boolean a(byte[] bArr, int i, GoroDaimon goroDaimon) {
        if (f == null) {
            try {
                f = new ChinGentsai();
                f.execute(new Void[0]);
            } catch (Exception e2) {
                f = null;
                return false;
            }
        }
        long currentTimeMillis = System.currentTimeMillis() + ((long) (i * 1000));
        try {
            this.e.acquire();
            C0055BenimaruNikaido benimaruNikaido = new C0055BenimaruNikaido(bArr, currentTimeMillis, goroDaimon);
            if (!d.contains(benimaruNikaido)) {
                d.add(benimaruNikaido);
            }
            this.e.release();
        } catch (InterruptedException e3) {
            e3.printStackTrace();
        }
        return true;
    }

    public void b() {
        if (this.c != null) {
            try {
                this.c.a();
            } catch (IOException e2) {
            }
        }
        this.c = new KyoKusanagi(ChoiBounge.a(a.b));
    }

    public void c() {
        try {
            this.e.acquire();
            d.clear();
            this.e.release();
        } catch (InterruptedException e2) {
        }
    }

    public void d() {
        if (this.c != null) {
            c();
            try {
                this.c.a();
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            if (this.c != null) {
                this.c.a();
            }
        } catch (IOException e2) {
        }
    }
}
