package io.presage.p028case;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Whip;
import java.util.ArrayList;
import java.util.List;

/* renamed from: io.presage.case.KyoKusanagi  reason: invalid package */
public class KyoKusanagi implements Application.ActivityLifecycleCallbacks {
    public static final String a = KyoKusanagi.class.getSimpleName();
    private C0042KyoKusanagi b;
    private BenimaruNikaido c;
    private List<Integer> d = new ArrayList();
    private int e = 0;

    /* renamed from: io.presage.case.KyoKusanagi$BenimaruNikaido */
    public interface BenimaruNikaido {
        void a(Activity activity);
    }

    /* renamed from: io.presage.case.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    public interface C0042KyoKusanagi {
        void a(Activity activity);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.e;
    }

    public void a(BenimaruNikaido benimaruNikaido) {
        this.c = benimaruNikaido;
    }

    public void a(C0042KyoKusanagi kyoKusanagi) {
        this.b = kyoKusanagi;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.d.size();
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
        if (activity != null) {
            SaishuKusanagi.a(a, String.format("%s paused (%s)", new Object[]{activity.getClass().getName(), Integer.valueOf(activity.getClass().hashCode())}));
            if (activity.getClass().hashCode() == this.e) {
                this.e = 0;
            }
        }
    }

    public void onActivityResumed(Activity activity) {
        if (activity != null) {
            SaishuKusanagi.a(a, String.format("%s resumed (%s)", new Object[]{activity.getClass().getName(), Integer.valueOf(activity.getClass().hashCode())}));
            this.e = activity.getClass().hashCode();
        }
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
        if (activity != null) {
            SaishuKusanagi.a(a, String.format("%s started (%s)", new Object[]{activity.getClass().getName(), Integer.valueOf(activity.getClass().hashCode())}));
            this.d.add(Integer.valueOf(activity.getClass().hashCode()));
            if (b() == 1 && !Whip.a(activity.getApplicationContext()) && this.b != null) {
                this.b.a(activity);
            }
        }
    }

    public void onActivityStopped(Activity activity) {
        if (activity != null) {
            SaishuKusanagi.a(a, String.format("%s stopped (%s)", new Object[]{activity.getClass().getName(), Integer.valueOf(activity.getClass().hashCode())}));
            this.d.remove(Integer.valueOf(activity.getClass().hashCode()));
            if (this.d.isEmpty() && this.c != null) {
                this.c.a(activity);
            }
        }
    }
}
