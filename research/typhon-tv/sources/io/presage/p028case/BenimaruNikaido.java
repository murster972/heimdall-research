package io.presage.p028case;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import io.presage.p028case.KyoKusanagi;
import io.presage.p034goto.SaishuKusanagi;
import java.util.List;

/* renamed from: io.presage.case.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido extends GoroDaimon {
    public static final String a = BenimaruNikaido.class.getSimpleName();
    private BroadcastReceiver c;
    /* access modifiers changed from: private */
    public KyoKusanagi d;
    private Application e;

    public BenimaruNikaido(Application application, ChinGentsai chinGentsai) {
        super(chinGentsai);
        this.e = application;
    }

    private boolean b() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.e.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return false;
        }
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.processName.contains(this.e.getPackageName()) && next.importance == 100) {
                return true;
            }
        }
        return false;
    }

    public void a() {
        this.d = new KyoKusanagi();
        if (b() && this.b != null) {
            this.b.b((Activity) null);
        }
        this.d.a((KyoKusanagi.C0042KyoKusanagi) new KyoKusanagi.C0042KyoKusanagi() {
            public void a(Activity activity) {
                SaishuKusanagi.a(BenimaruNikaido.a, String.format("onEnter %s", new Object[]{activity.getClass().getName()}));
                if (BenimaruNikaido.this.b != null) {
                    BenimaruNikaido.this.b.b(activity);
                }
            }
        });
        this.d.a((KyoKusanagi.BenimaruNikaido) new KyoKusanagi.BenimaruNikaido() {
            public void a(Activity activity) {
                BenimaruNikaido.this.b.a(activity);
            }
        });
        this.c = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if ("android.intent.action.CLOSE_SYSTEM_DIALOGS".equals(intent.getAction()) && "homekey".equals(intent.getStringExtra("reason")) && BenimaruNikaido.this.d.a() != 0) {
                    SaishuKusanagi.a(BenimaruNikaido.a, "Home key pressed from one of our activities");
                    BenimaruNikaido.this.b.c((Activity) null);
                }
            }
        };
        this.e.registerActivityLifecycleCallbacks(this.d);
        try {
            this.e.registerReceiver(this.c, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        } catch (Exception e2) {
            SaishuKusanagi.b(a, "register ACTION_CLOSE_SYSTEM_DIALOGS error", e2);
        }
    }
}
