package io.presage;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseUtils;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.finder.model.AppUsage;
import io.presage.finder.model.Ip;
import io.presage.finder.model.IpTracker;
import io.presage.p034goto.SaishuKusanagi;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;

public class BenimaruNikaido extends SQLiteOpenHelper {
    private static BenimaruNikaido a;

    private BenimaruNikaido(Context context) {
        super(context, "data.db", (SQLiteDatabase.CursorFactory) null, 1, new DefaultDatabaseErrorHandler());
    }

    public static synchronized BenimaruNikaido a(Context context) {
        BenimaruNikaido benimaruNikaido;
        synchronized (BenimaruNikaido.class) {
            if (a == null) {
                a = new BenimaruNikaido(context);
            }
            benimaruNikaido = a;
        }
        return benimaruNikaido;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r0 = getWritableDatabase();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000e, code lost:
        r0 = null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized android.database.sqlite.SQLiteDatabase d() {
        /*
            r1 = this;
            monitor-enter(r1)
            android.database.sqlite.SQLiteDatabase r0 = r1.getWritableDatabase()     // Catch:{ Exception -> 0x0007, all -> 0x0010 }
        L_0x0005:
            monitor-exit(r1)
            return r0
        L_0x0007:
            r0 = move-exception
            android.database.sqlite.SQLiteDatabase r0 = r1.getWritableDatabase()     // Catch:{ Exception -> 0x000d, all -> 0x0010 }
            goto L_0x0005
        L_0x000d:
            r0 = move-exception
            r0 = 0
            goto L_0x0005
        L_0x0010:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.BenimaruNikaido.d():android.database.sqlite.SQLiteDatabase");
    }

    public long a(String str) {
        try {
            SQLiteDatabase d = d();
            if (d == null || !d.isOpen()) {
                return 0;
            }
            return DatabaseUtils.queryNumEntries(d, str);
        } catch (Exception e) {
            SaishuKusanagi.b("DataDBHelper", e.getMessage(), e);
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:67:0x010a A[SYNTHETIC, Splitter:B:67:0x010a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public io.presage.finder.model.CollectionFinderResult a() {
        /*
            r9 = this;
            r1 = 0
            r2 = 0
            io.presage.finder.model.CollectionFinderResult r0 = new io.presage.finder.model.CollectionFinderResult     // Catch:{ Exception -> 0x00e6, all -> 0x0106 }
            java.lang.String r3 = "ips"
            r0.<init>(r3)     // Catch:{ Exception -> 0x00e6, all -> 0x0106 }
            java.lang.String r3 = "SELECT  * FROM Ips"
            android.database.sqlite.SQLiteDatabase r4 = r9.d()     // Catch:{ Exception -> 0x00e6, all -> 0x0106 }
            if (r4 == 0) goto L_0x0019
            boolean r5 = r4.isOpen()     // Catch:{ Exception -> 0x00e6, all -> 0x0106 }
            if (r5 != 0) goto L_0x002c
        L_0x0019:
            if (r1 == 0) goto L_0x001e
            r2.close()     // Catch:{ Exception -> 0x0020 }
        L_0x001e:
            r0 = r1
        L_0x001f:
            return r0
        L_0x0020:
            r0 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)
            goto L_0x001e
        L_0x002c:
            r2 = 0
            android.database.Cursor r2 = r4.rawQuery(r3, r2)     // Catch:{ Exception -> 0x00e6, all -> 0x0106 }
            if (r2 == 0) goto L_0x003f
            boolean r3 = r2.isClosed()     // Catch:{ Exception -> 0x011c }
            if (r3 != 0) goto L_0x003f
            int r3 = r2.getCount()     // Catch:{ Exception -> 0x011c }
            if (r3 != 0) goto L_0x005d
        L_0x003f:
            if (r2 == 0) goto L_0x004a
            boolean r0 = r2.isClosed()     // Catch:{ Exception -> 0x011c }
            if (r0 != 0) goto L_0x004a
            r2.close()     // Catch:{ Exception -> 0x011c }
        L_0x004a:
            if (r2 == 0) goto L_0x004f
            r2.close()     // Catch:{ Exception -> 0x0051 }
        L_0x004f:
            r0 = r1
            goto L_0x001f
        L_0x0051:
            r0 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)
            goto L_0x004f
        L_0x005d:
            boolean r3 = r2.moveToFirst()     // Catch:{ Exception -> 0x011c }
            if (r3 == 0) goto L_0x00ab
        L_0x0063:
            r3 = 0
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x011c }
            r5 = 1
            long r6 = r2.getLong(r5)     // Catch:{ Exception -> 0x011c }
            java.lang.Long r5 = java.lang.Long.valueOf(r6)     // Catch:{ Exception -> 0x011c }
            java.lang.String r6 = "IP_TEST"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011c }
            r7.<init>()     // Catch:{ Exception -> 0x011c }
            java.lang.String r8 = "{\"content\":[{\"timestamp\":\""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x011c }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ Exception -> 0x011c }
            java.lang.String r8 = "\",\"ip_visit\":\""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x011c }
            java.lang.StringBuilder r7 = r7.append(r3)     // Catch:{ Exception -> 0x011c }
            java.lang.String r8 = "\"}]}"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x011c }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x011c }
            io.presage.p034goto.SaishuKusanagi.a(r6, r7)     // Catch:{ Exception -> 0x011c }
            io.presage.finder.model.Ip r6 = new io.presage.finder.model.Ip     // Catch:{ Exception -> 0x011c }
            r6.<init>(r5, r3)     // Catch:{ Exception -> 0x011c }
            r0.a((io.presage.finder.IFinderResult) r6)     // Catch:{ Exception -> 0x011c }
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x011c }
            if (r3 != 0) goto L_0x0063
        L_0x00ab:
            r2.close()     // Catch:{ Exception -> 0x011c }
            if (r4 == 0) goto L_0x00b6
            boolean r3 = r4.isOpen()     // Catch:{ Exception -> 0x011c }
            if (r3 != 0) goto L_0x00ca
        L_0x00b6:
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ Exception -> 0x00bd }
            goto L_0x001f
        L_0x00bd:
            r1 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r1.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x001f
        L_0x00ca:
            java.lang.String r3 = "Ips"
            r5 = 0
            r6 = 0
            r4.delete(r3, r5, r6)     // Catch:{ Exception -> 0x011c }
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ Exception -> 0x00d9 }
            goto L_0x001f
        L_0x00d9:
            r1 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r1.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x001f
        L_0x00e6:
            r0 = move-exception
            r2 = r1
        L_0x00e8:
            java.lang.String r3 = "DataDBHelper"
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x011a }
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r0)     // Catch:{ all -> 0x011a }
            if (r2 == 0) goto L_0x00f7
            r2.close()     // Catch:{ Exception -> 0x00fa }
        L_0x00f7:
            r0 = r1
            goto L_0x001f
        L_0x00fa:
            r0 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)
            goto L_0x00f7
        L_0x0106:
            r0 = move-exception
            r2 = r1
        L_0x0108:
            if (r2 == 0) goto L_0x010d
            r2.close()     // Catch:{ Exception -> 0x010e }
        L_0x010d:
            throw r0
        L_0x010e:
            r1 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r1.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x010d
        L_0x011a:
            r0 = move-exception
            goto L_0x0108
        L_0x011c:
            r0 = move-exception
            goto L_0x00e8
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.BenimaruNikaido.a():io.presage.finder.model.CollectionFinderResult");
    }

    public void a(AppUsage appUsage) {
        try {
            SQLiteDatabase d = d();
            if (d != null && d.isOpen()) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", appUsage.b());
                contentValues.put(TtmlNode.START, Long.valueOf(appUsage.c()));
                contentValues.put(TtmlNode.END, Long.valueOf(appUsage.d()));
                d.insert("Apps", (String) null, contentValues);
            }
        } catch (Exception e) {
            SaishuKusanagi.b("DataDBHelper", e.getMessage(), e);
        }
    }

    public void a(IpTracker ipTracker) {
        try {
            SQLiteDatabase d = d();
            if (d != null && d.isOpen()) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("timestamp", ipTracker.b());
                contentValues.put(VastExtensionXmlManager.TYPE, ipTracker.c());
                d.insert("Ips_track", (String) null, contentValues);
            }
        } catch (Exception e) {
            SaishuKusanagi.b("DataDBHelper", e.getMessage(), e);
        }
    }

    public void a(ArrayList<Ip> arrayList) {
        try {
            SQLiteDatabase d = d();
            if (d != null && d.isOpen()) {
                StringBuilder sb = new StringBuilder();
                sb.append("INSERT OR IGNORE INTO ").append("Ips").append(StringUtils.SPACE);
                if (arrayList.size() > 0) {
                    sb.append("SELECT '").append(arrayList.get(0).c()).append("' AS '").append("ip").append("', '").append(arrayList.get(0).b()).append("' AS '").append("timestamp").append("' ");
                    for (int i = 1; i < arrayList.size(); i++) {
                        Ip ip = arrayList.get(i);
                        sb.append("UNION SELECT '").append(ip.c()).append("', '").append(ip.b()).append("' ");
                    }
                    d.execSQL(sb.toString());
                }
            }
        } catch (Exception e) {
            SaishuKusanagi.b("DataDBHelper", e.getMessage(), e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x00c0 A[SYNTHETIC, Splitter:B:56:0x00c0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public io.presage.finder.model.CollectionFinderResult b() {
        /*
            r8 = this;
            r1 = 0
            r2 = 0
            io.presage.finder.model.CollectionFinderResult r0 = new io.presage.finder.model.CollectionFinderResult     // Catch:{ Exception -> 0x009c, all -> 0x00bc }
            java.lang.String r3 = "ip_tracker"
            r0.<init>(r3)     // Catch:{ Exception -> 0x009c, all -> 0x00bc }
            java.lang.String r3 = "SELECT  * FROM Ips_track"
            android.database.sqlite.SQLiteDatabase r4 = r8.d()     // Catch:{ Exception -> 0x009c, all -> 0x00bc }
            if (r4 == 0) goto L_0x0019
            boolean r5 = r4.isOpen()     // Catch:{ Exception -> 0x009c, all -> 0x00bc }
            if (r5 != 0) goto L_0x002c
        L_0x0019:
            if (r1 == 0) goto L_0x001e
            r2.close()     // Catch:{ Exception -> 0x0020 }
        L_0x001e:
            r0 = r1
        L_0x001f:
            return r0
        L_0x0020:
            r0 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)
            goto L_0x001e
        L_0x002c:
            r2 = 0
            android.database.Cursor r2 = r4.rawQuery(r3, r2)     // Catch:{ Exception -> 0x009c, all -> 0x00bc }
            if (r2 == 0) goto L_0x003f
            boolean r3 = r2.isClosed()     // Catch:{ Exception -> 0x00d2 }
            if (r3 != 0) goto L_0x003f
            int r3 = r2.getCount()     // Catch:{ Exception -> 0x00d2 }
            if (r3 != 0) goto L_0x005d
        L_0x003f:
            if (r2 == 0) goto L_0x004a
            boolean r0 = r2.isClosed()     // Catch:{ Exception -> 0x00d2 }
            if (r0 != 0) goto L_0x004a
            r2.close()     // Catch:{ Exception -> 0x00d2 }
        L_0x004a:
            if (r2 == 0) goto L_0x004f
            r2.close()     // Catch:{ Exception -> 0x0051 }
        L_0x004f:
            r0 = r1
            goto L_0x001f
        L_0x0051:
            r0 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)
            goto L_0x004f
        L_0x005d:
            boolean r3 = r2.moveToFirst()     // Catch:{ Exception -> 0x00d2 }
            if (r3 == 0) goto L_0x007f
        L_0x0063:
            r3 = 0
            long r6 = r2.getLong(r3)     // Catch:{ Exception -> 0x00d2 }
            java.lang.Long r3 = java.lang.Long.valueOf(r6)     // Catch:{ Exception -> 0x00d2 }
            r5 = 1
            java.lang.String r5 = r2.getString(r5)     // Catch:{ Exception -> 0x00d2 }
            io.presage.finder.model.IpTracker r6 = new io.presage.finder.model.IpTracker     // Catch:{ Exception -> 0x00d2 }
            r6.<init>(r3, r5)     // Catch:{ Exception -> 0x00d2 }
            r0.a((io.presage.finder.IFinderResult) r6)     // Catch:{ Exception -> 0x00d2 }
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x00d2 }
            if (r3 != 0) goto L_0x0063
        L_0x007f:
            r2.close()     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r3 = "Ips_track"
            r5 = 0
            r6 = 0
            r4.delete(r3, r5, r6)     // Catch:{ Exception -> 0x00d2 }
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ Exception -> 0x0090 }
            goto L_0x001f
        L_0x0090:
            r1 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r1.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x001f
        L_0x009c:
            r0 = move-exception
            r2 = r1
        L_0x009e:
            java.lang.String r3 = "DataDBHelper"
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x00d0 }
            io.presage.p034goto.SaishuKusanagi.b(r3, r4, r0)     // Catch:{ all -> 0x00d0 }
            if (r2 == 0) goto L_0x00ad
            r2.close()     // Catch:{ Exception -> 0x00b0 }
        L_0x00ad:
            r0 = r1
            goto L_0x001f
        L_0x00b0:
            r0 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)
            goto L_0x00ad
        L_0x00bc:
            r0 = move-exception
            r2 = r1
        L_0x00be:
            if (r2 == 0) goto L_0x00c3
            r2.close()     // Catch:{ Exception -> 0x00c4 }
        L_0x00c3:
            throw r0
        L_0x00c4:
            r1 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r1.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x00c3
        L_0x00d0:
            r0 = move-exception
            goto L_0x00be
        L_0x00d2:
            r0 = move-exception
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.BenimaruNikaido.b():io.presage.finder.model.CollectionFinderResult");
    }

    /* JADX WARNING: Removed duplicated region for block: B:57:0x00d3 A[SYNTHETIC, Splitter:B:57:0x00d3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public io.presage.finder.model.CollectionFinderResult c() {
        /*
            r10 = this;
            r7 = 0
            r0 = 0
            io.presage.finder.model.CollectionFinderResult r6 = new io.presage.finder.model.CollectionFinderResult     // Catch:{ Exception -> 0x00af, all -> 0x00cf }
            java.lang.String r1 = "ip_tracker"
            r6.<init>(r1)     // Catch:{ Exception -> 0x00af, all -> 0x00cf }
            java.lang.String r1 = "SELECT  * FROM Apps"
            android.database.sqlite.SQLiteDatabase r9 = r10.d()     // Catch:{ Exception -> 0x00af, all -> 0x00cf }
            if (r9 == 0) goto L_0x0019
            boolean r2 = r9.isOpen()     // Catch:{ Exception -> 0x00af, all -> 0x00cf }
            if (r2 != 0) goto L_0x002c
        L_0x0019:
            if (r7 == 0) goto L_0x001e
            r0.close()     // Catch:{ Exception -> 0x0020 }
        L_0x001e:
            r0 = r7
        L_0x001f:
            return r0
        L_0x0020:
            r0 = move-exception
            java.lang.String r1 = "DataDBHelper"
            java.lang.String r2 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r1, r2, r0)
            goto L_0x001e
        L_0x002c:
            r0 = 0
            android.database.Cursor r8 = r9.rawQuery(r1, r0)     // Catch:{ Exception -> 0x00af, all -> 0x00cf }
            if (r8 == 0) goto L_0x003f
            boolean r0 = r8.isClosed()     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            if (r0 != 0) goto L_0x003f
            int r0 = r8.getCount()     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            if (r0 != 0) goto L_0x005d
        L_0x003f:
            if (r8 == 0) goto L_0x004a
            boolean r0 = r8.isClosed()     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            if (r0 != 0) goto L_0x004a
            r8.close()     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
        L_0x004a:
            if (r8 == 0) goto L_0x004f
            r8.close()     // Catch:{ Exception -> 0x0051 }
        L_0x004f:
            r0 = r7
            goto L_0x001f
        L_0x0051:
            r0 = move-exception
            java.lang.String r1 = "DataDBHelper"
            java.lang.String r2 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r1, r2, r0)
            goto L_0x004f
        L_0x005d:
            boolean r0 = r8.moveToFirst()     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            if (r0 == 0) goto L_0x0090
        L_0x0063:
            r0 = 0
            java.lang.String r1 = r8.getString(r0)     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            r0 = 1
            long r2 = r8.getLong(r0)     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            r0 = 2
            long r4 = r8.getLong(r0)     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            io.presage.finder.model.AppUsage r0 = new io.presage.finder.model.AppUsage     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            long r2 = r2.longValue()     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            long r4 = r4.longValue()     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            r0.<init>(r1, r2, r4)     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            r6.a((io.presage.finder.IFinderResult) r0)     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            boolean r0 = r8.moveToNext()     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            if (r0 != 0) goto L_0x0063
        L_0x0090:
            java.lang.String r0 = "Apps"
            r1 = 0
            r2 = 0
            r9.delete(r0, r1, r2)     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            r8.close()     // Catch:{ Exception -> 0x00e8, all -> 0x00e3 }
            if (r8 == 0) goto L_0x00a0
            r8.close()     // Catch:{ Exception -> 0x00a3 }
        L_0x00a0:
            r0 = r6
            goto L_0x001f
        L_0x00a3:
            r0 = move-exception
            java.lang.String r1 = "DataDBHelper"
            java.lang.String r2 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r1, r2, r0)
            goto L_0x00a0
        L_0x00af:
            r0 = move-exception
            r1 = r7
        L_0x00b1:
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x00e5 }
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r0)     // Catch:{ all -> 0x00e5 }
            if (r1 == 0) goto L_0x00c0
            r1.close()     // Catch:{ Exception -> 0x00c3 }
        L_0x00c0:
            r0 = r7
            goto L_0x001f
        L_0x00c3:
            r0 = move-exception
            java.lang.String r1 = "DataDBHelper"
            java.lang.String r2 = r0.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r1, r2, r0)
            goto L_0x00c0
        L_0x00cf:
            r0 = move-exception
            r8 = r7
        L_0x00d1:
            if (r8 == 0) goto L_0x00d6
            r8.close()     // Catch:{ Exception -> 0x00d7 }
        L_0x00d6:
            throw r0
        L_0x00d7:
            r1 = move-exception
            java.lang.String r2 = "DataDBHelper"
            java.lang.String r3 = r1.getMessage()
            io.presage.p034goto.SaishuKusanagi.b(r2, r3, r1)
            goto L_0x00d6
        L_0x00e3:
            r0 = move-exception
            goto L_0x00d1
        L_0x00e5:
            r0 = move-exception
            r8 = r1
            goto L_0x00d1
        L_0x00e8:
            r0 = move-exception
            r1 = r8
            goto L_0x00b1
        */
        throw new UnsupportedOperationException("Method not decompiled: io.presage.BenimaruNikaido.c():io.presage.finder.model.CollectionFinderResult");
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase != null) {
            try {
                sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Apps(name VARCHAR,start INTEGER,end INTEGER)");
                sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Ips(ip VARCHAR,timestamp INTEGER, PRIMARY KEY (ip, ip));");
                sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Ips_track(timestamp INTEGER, type VARCHAR);");
            } catch (Exception e) {
                SaishuKusanagi.b("DataDBHelper", e.getMessage(), e);
            }
        }
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase != null) {
            try {
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS Apps");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS Ips");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS Ips_track");
                onCreate(sQLiteDatabase);
            } catch (Exception e) {
                SaishuKusanagi.b("DataDBHelper", e.getMessage(), e);
            }
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase != null) {
            try {
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS Apps");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS Ips");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS Ips_track");
                onCreate(sQLiteDatabase);
            } catch (Exception e) {
                SaishuKusanagi.b("DataDBHelper", e.getMessage(), e);
            }
        }
    }
}
