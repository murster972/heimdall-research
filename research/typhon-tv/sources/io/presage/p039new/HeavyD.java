package io.presage.p039new;

import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.IoriYagami;
import com.p003if.p004do.RugalBernstein;
import com.p003if.p004do.SaishuKusanagi;
import com.p003if.p004do.Vice;
import com.p003if.p004do.p013for.KyoKusanagi;
import io.presage.ads.NewAd;
import io.presage.model.Parameter;
import io.presage.model.Zone;
import java.lang.reflect.Type;
import java.util.List;

/* renamed from: io.presage.new.HeavyD  reason: invalid package */
public class HeavyD implements IoriYagami<Parameter> {
    /* renamed from: a */
    public Parameter b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura {
        Vice r0 = saishuKusanagi.m14139();
        String r1 = r0.m14167("name").m14146();
        SaishuKusanagi r2 = r0.m14167("value");
        return "zones".equals(r1) ? new Parameter(r1, new ChoiBounge().m14087(r2, new KyoKusanagi<List<Zone>>() {
        }.b())) : "frame".equals(r1) ? new Parameter(r1, new ChoiBounge().m14086(r2, Zone.class)) : "ad".equals(r1) ? new Parameter(r1, new ChoiBounge().m14086(r2, NewAd.class)) : (Parameter) new ChoiBounge().m14087((SaishuKusanagi) r0, type);
    }
}
