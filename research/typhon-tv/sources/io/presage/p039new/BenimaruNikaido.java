package io.presage.p039new;

import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.IoriYagami;
import com.p003if.p004do.RugalBernstein;
import com.p003if.p004do.SaishuKusanagi;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: io.presage.new.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido<E> implements IoriYagami<List<E>> {
    private Class<E> a;

    public BenimaruNikaido(Class<E> cls) {
        this.a = cls;
    }

    /* renamed from: a */
    public List<E> b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura {
        ArrayList arrayList = new ArrayList();
        if (saishuKusanagi.m14136()) {
            Iterator<SaishuKusanagi> it2 = saishuKusanagi.m14137().iterator();
            while (it2.hasNext()) {
                arrayList.add(rugalBernstein.m14133(it2.next(), this.a));
            }
        } else if (saishuKusanagi.m14140()) {
            arrayList.add(rugalBernstein.m14133(saishuKusanagi, this.a));
        } else {
            throw new RuntimeException("Unexpected JSON type: " + saishuKusanagi.getClass());
        }
        return arrayList;
    }
}
