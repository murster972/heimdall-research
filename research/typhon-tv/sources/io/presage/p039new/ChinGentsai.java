package io.presage.p039new;

import android.content.Context;
import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.IoriYagami;
import com.p003if.p004do.RugalBernstein;
import com.p003if.p004do.SaishuKusanagi;
import io.presage.actions.NewFingerAccess;
import io.presage.helper.Permissions;
import java.lang.reflect.Type;
import net.pubnative.library.request.PubnativeAsset;

/* renamed from: io.presage.new.ChinGentsai  reason: invalid package */
public class ChinGentsai implements IoriYagami<NewFingerAccess> {
    private Context a;
    private Permissions b;

    public ChinGentsai(Context context, Permissions permissions) {
        this.a = context;
        this.b = permissions;
    }

    /* renamed from: a */
    public NewFingerAccess b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura {
        try {
            return new NewFingerAccess(this.a, this.b, saishuKusanagi.m14139().m14167("identifier").m14146(), saishuKusanagi.m14139().m14167(PubnativeAsset.TITLE).m14146(), saishuKusanagi.m14139().m14167(PubnativeAsset.ICON).m14146(), saishuKusanagi.m14139().m14167("url").m14146());
        } catch (IllegalStateException | NullPointerException e) {
            io.presage.p034goto.SaishuKusanagi.a("NewFingerAccessDsz", e.getMessage(), e);
            return null;
        }
    }
}
