package io.presage.p039new;

import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.IoriYagami;
import com.p003if.p004do.RugalBernstein;
import com.p003if.p004do.SaishuKusanagi;
import io.presage.model.Parameter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: io.presage.new.GoroDaimon  reason: invalid package */
public class GoroDaimon implements IoriYagami<List<Parameter>> {
    /* renamed from: a */
    public List<Parameter> b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura {
        ArrayList arrayList = new ArrayList();
        if (saishuKusanagi.m14136()) {
            Iterator<SaishuKusanagi> it2 = saishuKusanagi.m14137().iterator();
            while (it2.hasNext()) {
                arrayList.add((Parameter) rugalBernstein.m14133(it2.next(), Parameter.class));
            }
        } else if (saishuKusanagi.m14140()) {
            arrayList.add((Parameter) rugalBernstein.m14133(saishuKusanagi, Parameter.class));
        } else {
            throw new RuntimeException("Unexpected JSON type: " + saishuKusanagi.getClass());
        }
        return arrayList;
    }
}
