package io.presage.p039new;

import android.content.Context;
import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.IoriYagami;
import com.p003if.p004do.RugalBernstein;
import com.p003if.p004do.SaishuKusanagi;
import io.presage.actions.NewOpenBrowser;
import io.presage.helper.Permissions;
import java.lang.reflect.Type;

/* renamed from: io.presage.new.ChangKoehan  reason: invalid package */
public class ChangKoehan implements IoriYagami<NewOpenBrowser> {
    private Context a;
    private Permissions b;

    public ChangKoehan(Context context, Permissions permissions) {
        this.a = context;
        this.b = permissions;
    }

    /* renamed from: a */
    public NewOpenBrowser b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura {
        try {
            return new NewOpenBrowser(this.a, this.b, saishuKusanagi.m14139().m14167("url").m14146());
        } catch (IllegalStateException e) {
            io.presage.p034goto.SaishuKusanagi.c("NewOpenBrowserDsz", e.getMessage());
        } catch (NullPointerException e2) {
            io.presage.p034goto.SaishuKusanagi.c("NewOpenBrowserDsz", e2.getMessage());
        }
        return null;
    }
}
