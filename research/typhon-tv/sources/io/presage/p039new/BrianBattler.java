package io.presage.p039new;

import android.content.Context;
import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.IoriYagami;
import com.p003if.p004do.RugalBernstein;
import com.p003if.p004do.SaishuKusanagi;
import io.presage.actions.RemoveAdShortcut;
import io.presage.helper.Permissions;
import java.lang.reflect.Type;

/* renamed from: io.presage.new.BrianBattler  reason: invalid package */
public class BrianBattler implements IoriYagami<RemoveAdShortcut> {
    private Context a;
    private Permissions b;

    public BrianBattler(Context context, Permissions permissions) {
        this.a = context;
        this.b = permissions;
    }

    /* renamed from: a */
    public RemoveAdShortcut b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura {
        String str;
        String str2;
        try {
            str = saishuKusanagi.m14139().m14167("identifier").m14146();
            try {
                str2 = saishuKusanagi.m14139().m14167("icon_name").m14146();
            } catch (IllegalStateException | NullPointerException e) {
                e = e;
            }
        } catch (IllegalStateException e2) {
            e = e2;
            str = null;
        } catch (NullPointerException e3) {
            e = e3;
            str = null;
        }
        return new RemoveAdShortcut(this.a, this.b, str, str2);
        io.presage.p034goto.SaishuKusanagi.a("RemoveAdShortcutDsz", e.getMessage(), e);
        str2 = null;
        return new RemoveAdShortcut(this.a, this.b, str, str2);
    }
}
