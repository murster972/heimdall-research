package io.presage.p039new;

import android.content.Context;
import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.IoriYagami;
import com.p003if.p004do.RugalBernstein;
import com.p003if.p004do.SaishuKusanagi;
import io.presage.actions.AddAdShortcut;
import io.presage.ads.NewAd;
import io.presage.helper.Permissions;
import java.lang.reflect.Type;

/* renamed from: io.presage.new.KyoKusanagi  reason: invalid package */
public class KyoKusanagi implements IoriYagami<AddAdShortcut> {
    private Context a;
    private Permissions b;

    public KyoKusanagi(Context context, Permissions permissions) {
        this.a = context;
        this.b = permissions;
    }

    /* renamed from: a */
    public AddAdShortcut b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura {
        NewAd newAd;
        try {
            newAd = (NewAd) rugalBernstein.m14133(saishuKusanagi.m14139().m14164("ad"), NewAd.class);
        } catch (IllegalStateException | NullPointerException e) {
            io.presage.p034goto.SaishuKusanagi.a("AddAdShortcutDsz", e.getMessage(), e);
            newAd = null;
        }
        return new AddAdShortcut(this.a, this.b, newAd);
    }
}
