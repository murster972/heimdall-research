package io.presage.p039new;

import android.content.Context;
import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.HeavyD;
import com.p003if.p004do.p013for.KyoKusanagi;
import io.presage.actions.AddAdShortcut;
import io.presage.actions.NewFingerAccess;
import io.presage.actions.NewOpenBrowser;
import io.presage.actions.NewRemoveFingerAccess;
import io.presage.actions.RemoveAdShortcut;
import io.presage.actions.StartIntentFromUri;
import io.presage.helper.Permissions;
import io.presage.model.Parameter;
import java.util.List;

/* renamed from: io.presage.new.LuckyGlauber  reason: invalid package */
public class LuckyGlauber {
    private static HeavyD a = new HeavyD().m14118(Parameter.class, new HeavyD()).m14118(new KyoKusanagi<List<Parameter>>() {
    }.b(), new GoroDaimon()).m14118(new KyoKusanagi<List<io.presage.model.KyoKusanagi>>() {
    }.b(), new BenimaruNikaido(io.presage.model.KyoKusanagi.class));

    public static ChoiBounge a() {
        return a.m14117();
    }

    public static ChoiBounge a(Context context, Permissions permissions) {
        Context applicationContext = context.getApplicationContext();
        return a.m14118(AddAdShortcut.class, new KyoKusanagi(applicationContext, permissions)).m14118(RemoveAdShortcut.class, new BrianBattler(applicationContext, permissions)).m14118(StartIntentFromUri.class, new RugalBernstein(applicationContext, permissions)).m14118(NewFingerAccess.class, new ChinGentsai(applicationContext, permissions)).m14118(NewRemoveFingerAccess.class, new ChoiBounge(applicationContext, permissions)).m14118(NewOpenBrowser.class, new ChangKoehan(applicationContext, permissions)).m14117();
    }
}
