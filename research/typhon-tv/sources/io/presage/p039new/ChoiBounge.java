package io.presage.p039new;

import android.content.Context;
import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.IoriYagami;
import com.p003if.p004do.RugalBernstein;
import com.p003if.p004do.SaishuKusanagi;
import io.presage.actions.NewRemoveFingerAccess;
import io.presage.helper.Permissions;
import java.lang.reflect.Type;
import net.pubnative.library.request.PubnativeAsset;

/* renamed from: io.presage.new.ChoiBounge  reason: invalid package */
public class ChoiBounge implements IoriYagami<NewRemoveFingerAccess> {
    private Context a;
    private Permissions b;

    public ChoiBounge(Context context, Permissions permissions) {
        this.a = context;
        this.b = permissions;
    }

    /* renamed from: a */
    public NewRemoveFingerAccess b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura {
        try {
            return new NewRemoveFingerAccess(this.a, this.b, saishuKusanagi.m14139().m14167("identifier").m14146(), saishuKusanagi.m14139().m14167(PubnativeAsset.TITLE).m14146(), saishuKusanagi.m14139().m14167("url").m14146());
        } catch (IllegalStateException | NullPointerException e) {
            io.presage.p034goto.SaishuKusanagi.a("NewRemoveFingerAccDsz", e.getMessage(), e);
            return null;
        }
    }
}
