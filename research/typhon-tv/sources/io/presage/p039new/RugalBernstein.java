package io.presage.p039new;

import android.content.Context;
import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.IoriYagami;
import com.p003if.p004do.SaishuKusanagi;
import io.presage.actions.StartIntentFromUri;
import io.presage.helper.Permissions;
import java.lang.reflect.Type;

/* renamed from: io.presage.new.RugalBernstein  reason: invalid package */
public class RugalBernstein implements IoriYagami<StartIntentFromUri> {
    private Context a;
    private Permissions b;

    public RugalBernstein(Context context, Permissions permissions) {
        this.a = context;
        this.b = permissions;
    }

    /* renamed from: a */
    public StartIntentFromUri b(SaishuKusanagi saishuKusanagi, Type type, com.p003if.p004do.RugalBernstein rugalBernstein) throws ChizuruKagura {
        String str = null;
        try {
            str = saishuKusanagi.m14139().m14167("intent_uri").m14146();
        } catch (IllegalStateException e) {
            io.presage.p034goto.SaishuKusanagi.c("StartIntentFromUriDsz", e.getMessage());
        } catch (NullPointerException e2) {
            io.presage.p034goto.SaishuKusanagi.c("StartIntentFromUriDsz", e2.getMessage());
        }
        return new StartIntentFromUri(this.a, this.b, str);
    }
}
