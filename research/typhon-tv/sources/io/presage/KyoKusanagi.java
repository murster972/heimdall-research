package io.presage;

import android.content.Context;
import android.content.SharedPreferences;

public class KyoKusanagi {
    private static String a(String str) {
        return "CS_" + str;
    }

    public static boolean a(Context context, String str, String str2) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("presage", 0);
        String a = a(str);
        if (str2.equals(sharedPreferences.getString(a, "default"))) {
            return false;
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(a, str2);
        edit.commit();
        return true;
    }
}
