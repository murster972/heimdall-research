package io.presage.model;

import com.p003if.p004do.p005do.GoroDaimon;

public class AdUnit {
    @GoroDaimon(m6138 = "app_user_id")
    private String appUserId;
    @GoroDaimon(m6138 = "id")
    private String id;
    @GoroDaimon(m6138 = "reward_launch")
    private String rewardLaunch;
    @GoroDaimon(m6138 = "reward_name")
    private String rewardName;
    @GoroDaimon(m6138 = "reward_value")
    private String rewardValue;
    @GoroDaimon(m6138 = "type")
    private String type;

    public String getAppUserId() {
        return this.appUserId;
    }

    public String getId() {
        return this.id;
    }

    public String getRewardLaunch() {
        return this.rewardLaunch;
    }

    public String getRewardName() {
        return this.rewardName;
    }

    public String getRewardValue() {
        return this.rewardValue;
    }

    public String getType() {
        return this.type;
    }
}
