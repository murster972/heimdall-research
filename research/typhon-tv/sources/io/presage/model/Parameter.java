package io.presage.model;

import com.p003if.p004do.p005do.GoroDaimon;

public class Parameter {
    @GoroDaimon(m6138 = "name")
    private String name;
    @GoroDaimon(m6138 = "value")
    private Object value;

    public Parameter(String str, Object obj) {
        this.name = str;
        this.value = obj;
    }

    public Object get() {
        return this.value;
    }

    public <T> T get(Class<T> cls) {
        return this.value;
    }

    public Boolean getAsBoolean() {
        return (Boolean) this.value;
    }

    public Integer getAsInt() {
        return (Integer) this.value;
    }

    public String getAsString() {
        return String.valueOf(this.value);
    }

    public String getName() {
        return this.name;
    }
}
