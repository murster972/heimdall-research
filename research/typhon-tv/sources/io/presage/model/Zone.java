package io.presage.model;

import com.p003if.p004do.p005do.GoroDaimon;
import java.util.List;

public class Zone {
    @GoroDaimon(m6138 = "background")
    private String background;
    @GoroDaimon(m6138 = "gravity")
    private List<String> gravity;
    @GoroDaimon(m6138 = "margins")
    private Margins margins;
    @GoroDaimon(m6138 = "name")
    private String name;
    @GoroDaimon(m6138 = "position")
    private Position position;
    @GoroDaimon(m6138 = "size")
    private Size size;
    @GoroDaimon(m6138 = "tracking")
    private boolean trackHistory;
    @GoroDaimon(m6138 = "landing")
    private boolean trackLanding;
    @GoroDaimon(m6138 = "type")
    private String type;
    @GoroDaimon(m6138 = "url")
    private String url;
    @GoroDaimon(m6138 = "autoPlay")
    private boolean videoAutoPlay;
    @GoroDaimon(m6138 = "muted")
    private boolean videoMuted;

    public static class Margins {
        public int bottom;
        public int left;
        public int right;
        public int top;
    }

    public static class Position {
        public int x;
        public int y;
    }

    public static class Size {
        public int height;
        public int width;
    }

    public String getBackground() {
        return this.background;
    }

    public List<String> getGravity() {
        return this.gravity;
    }

    public Margins getMargins() {
        return this.margins;
    }

    public String getName() {
        return this.name;
    }

    public Position getPosition() {
        return this.position;
    }

    public Size getSize() {
        return this.size;
    }

    public String getType() {
        return this.type;
    }

    public String getUrl() {
        return this.url;
    }

    public boolean isTrackHistory() {
        return this.trackHistory;
    }

    public boolean isTrackLanding() {
        return this.trackLanding;
    }

    public boolean isVideo() {
        return this.type != null && this.type.equals("video");
    }

    public boolean isVideoAutoPlay() {
        return this.videoAutoPlay;
    }

    public boolean isVideoMuted() {
        return this.videoMuted;
    }
}
