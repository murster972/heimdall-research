package io.presage.model;

import android.content.Context;
import com.p003if.p004do.SaishuKusanagi;
import com.p003if.p004do.Vice;
import com.p003if.p004do.p005do.GoroDaimon;
import io.presage.actions.BrianBattler;
import io.presage.actions.NewAction;
import io.presage.helper.Permissions;
import io.presage.p039new.LuckyGlauber;
import java.util.List;

public class KyoKusanagi {
    @GoroDaimon(m6138 = "name")
    private String a;
    @GoroDaimon(m6138 = "type")
    private String b;
    @GoroDaimon(m6138 = "params")
    private List<Parameter> c;

    public NewAction a(Context context, Permissions permissions, List<Parameter> list) {
        Vice vice = new Vice();
        if (list != null) {
            for (Parameter next : list) {
                if (!vice.m14165(next.getName())) {
                    vice.m14171(next.getName(), next.getAsString());
                }
            }
        }
        if (this.c != null) {
            for (Parameter next2 : this.c) {
                if (!vice.m14165(next2.getName())) {
                    vice.m14171(next2.getName(), next2.getAsString());
                }
            }
        }
        return (NewAction) LuckyGlauber.a(context, permissions).m14086((SaishuKusanagi) vice, BrianBattler.a().b(this.b));
    }

    public String a() {
        return this.a;
    }
}
