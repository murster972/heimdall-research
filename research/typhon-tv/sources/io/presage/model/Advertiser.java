package io.presage.model;

import com.p003if.p004do.p005do.GoroDaimon;

public class Advertiser {
    @GoroDaimon(m6138 = "id")
    private String id;
    @GoroDaimon(m6138 = "name")
    private String name;

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
}
