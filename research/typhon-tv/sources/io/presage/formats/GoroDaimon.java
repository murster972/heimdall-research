package io.presage.formats;

public class GoroDaimon extends Exception {
    private int a;

    public GoroDaimon(int i) {
        this.a = i;
    }

    public String getMessage() {
        switch (this.a) {
            case 0:
                return "unable to parse viewer";
            case 1:
                return "alert viewer not usable";
            case 2:
                return "Video playback not available before 16";
            default:
                return "unknown error on the viewer";
        }
    }
}
