package io.presage.formats.multiwebviews;

import android.view.View;
import android.view.WindowManager;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdController;
import io.presage.ads.NewAdViewer;
import io.presage.helper.Permissions;
import io.presage.model.Zone;
import io.presage.p034goto.Bao;
import io.presage.p038long.ChinGentsai;
import io.presage.p038long.GoroDaimon;
import io.presage.p038long.KyoKusanagi;
import java.util.List;

public class AlertMultiViewFormat extends NewAdViewer {
    private ChinGentsai f;
    private KyoKusanagi g;

    public AlertMultiViewFormat(NewAdController newAdController, NewAd newAd, Permissions permissions, int i) {
        super(newAdController, newAd, permissions, i);
    }

    public void hide() {
        if (this.f != null) {
            ((WindowManager) this.a.getSystemService("window")).removeView(this.f);
            this.f = null;
        }
        if (this.g != null) {
            this.g.g();
            this.g = null;
        }
        onHide();
    }

    public void show() {
        WindowManager.LayoutParams layoutParams;
        this.f = new ChinGentsai(this.a);
        this.g = new KyoKusanagi(this.c, this.f, this.e);
        this.g.f();
        WindowManager windowManager = (WindowManager) this.a.getSystemService("window");
        Zone zone = (Zone) this.c.getOverridedParameterValue("frame", Zone.class);
        if (zone == null) {
            layoutParams = Bao.b();
        } else {
            WindowManager.LayoutParams a = Bao.a(this.a, zone);
            Bao.a((View) this.f, zone);
            layoutParams = a;
        }
        windowManager.addView(this.f, layoutParams);
        List<Zone> list = (List) this.c.getOverridedParameterValue("zones", List.class);
        if (list != null) {
            for (Zone a2 : list) {
                this.f.a((KyoKusanagi) GoroDaimon.a(this.a, this.e, a2));
            }
        }
        this.c.onFormatEvent(NewAd.EVENT_SHOWN);
        onShow();
    }
}
