package io.presage.formats.multiwebviews.video;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class KyoKusanagi implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, SurfaceHolder.Callback {
    private SurfaceView a;
    /* access modifiers changed from: private */
    public io.presage.formats.multiwebviews.KyoKusanagi b;
    private Uri c;
    /* access modifiers changed from: private */
    public MediaPlayer d;
    private Context e;
    private int f;
    private boolean g = false;
    private boolean h = false;
    /* access modifiers changed from: private */
    public Handler i = new Handler();
    /* access modifiers changed from: private */
    public boolean j = false;
    private boolean k = false;
    private Runnable l = new Runnable() {
        public void run() {
            try {
                if (KyoKusanagi.this.b != null && KyoKusanagi.this.d != null && KyoKusanagi.this.d.isPlaying()) {
                    KyoKusanagi.this.b.a(KyoKusanagi.this.a());
                    boolean unused = KyoKusanagi.this.j = true;
                    KyoKusanagi.this.i.postDelayed(this, 1000);
                }
            } catch (IllegalStateException e) {
            }
        }
    };

    @TargetApi(3)
    public KyoKusanagi(Context context, String str, boolean z, boolean z2, SurfaceView surfaceView) {
        boolean z3 = false;
        this.c = Uri.parse(str);
        this.f = 0;
        this.a = surfaceView;
        this.d = new MediaPlayer();
        this.d.setOnPreparedListener(this);
        this.d.setOnCompletionListener(this);
        this.d.setOnInfoListener(this);
        this.e = context;
        this.h = !z2 ? true : z3;
        e();
        this.a.getHolder().addCallback(this);
    }

    public String a() {
        int i2 = 0;
        if (this.d != null && this.j) {
            i2 = this.d.getCurrentPosition();
        }
        if (this.k) {
            i2 = this.f;
        }
        if (!(this.b == null || this.b.b() == null)) {
            this.b.b().d(i2);
        }
        return String.valueOf(i2);
    }

    public void a(MediaPlayer.OnVideoSizeChangedListener onVideoSizeChangedListener) {
        this.d.setOnVideoSizeChangedListener(onVideoSizeChangedListener);
    }

    public void a(io.presage.formats.multiwebviews.KyoKusanagi kyoKusanagi) {
    }

    public void b() {
        if (this.d != null && this.d.isPlaying()) {
            this.g = true;
            this.d.pause();
            if (this.b != null && this.b.b() != null) {
                this.b.b().b(this.d.getCurrentPosition());
            }
        }
    }

    public void b(io.presage.formats.multiwebviews.KyoKusanagi kyoKusanagi) {
        this.b = kyoKusanagi;
    }

    public void c() {
        if (this.d != null && !this.d.isPlaying()) {
            this.d.start();
            this.g = false;
            if (this.b != null && this.b.b() != null) {
                this.b.b().a(this.d.getCurrentPosition());
            }
        }
    }

    public void d() {
        if (this.d != null && this.g) {
            b();
        } else if (this.d != null && !this.g) {
            c();
        }
    }

    public void e() {
        if (this.d != null && this.h) {
            this.h = false;
            this.d.setVolume(1.0f, 1.0f);
            if (this.b != null && this.b.b() != null) {
                this.b.b().d();
            }
        } else if (this.d != null) {
            this.h = true;
            this.d.setVolume(0.0f, 0.0f);
            if (this.b != null && this.b.b() != null) {
                this.b.b().c();
            }
        }
    }

    public void f() {
        try {
            if (this.d != null && this.d.isPlaying()) {
                this.i.removeCallbacks(this.l);
                this.j = false;
                if (!(this.b == null || this.b.b() == null)) {
                    this.b.b().c(this.d.getCurrentPosition());
                }
                this.d.stop();
            }
        } catch (IllegalStateException e2) {
            this.i.removeCallbacks(this.l);
            this.j = false;
        }
    }

    public void g() {
        if (this.i != null && !this.j) {
            this.i.postDelayed(this.l, 1000);
        }
        if (this.d != null) {
            this.d.seekTo(0);
            this.d.start();
        }
    }

    public int h() {
        if (this.d != null) {
            return this.f;
        }
        if (this.f == 0) {
            return 300000;
        }
        return this.f;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.k = true;
        if (this.i != null) {
            this.j = false;
            this.i.removeCallbacks(this.l);
        }
        if (this.b != null) {
            this.b.a("" + h());
            this.b.d();
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        this.j = false;
        this.i.removeCallbacks(this.l);
        return false;
    }

    public boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
        if (this.b == null) {
            return false;
        }
        if (i2 == 701) {
            this.b.b(TtmlNode.START);
            return false;
        } else if (i2 != 702) {
            return false;
        } else {
            this.b.b(TtmlNode.END);
            return false;
        }
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        Surface surface = this.a.getHolder().getSurface();
        if (surface != null && surface.isValid()) {
            mediaPlayer.setDisplay(this.a.getHolder());
            mediaPlayer.start();
            this.f = mediaPlayer.getDuration();
            this.i.postDelayed(this.l, 1000);
            if (this.b != null) {
                this.b.c();
            }
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            this.d.setDataSource(this.e, this.c);
            Paint paint = new Paint();
            paint.setColor(0);
            Canvas lockCanvas = surfaceHolder.lockCanvas();
            lockCanvas.drawPaint(paint);
            surfaceHolder.getSurface().unlockCanvasAndPost(lockCanvas);
            this.d.prepareAsync();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        f();
        if (!(this.a == null || this.a.getHolder() == null || this.a.getHolder().getSurface() == null)) {
            this.a.getHolder().getSurface().release();
        }
        if (this.d != null) {
            this.d.release();
        }
    }
}
