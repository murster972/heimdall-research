package io.presage.formats.multiwebviews;

import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mopub.common.TyphoonApp;
import io.presage.p034goto.SaishuKusanagi;

public class GoroDaimon extends WebViewClient {
    private String a;
    private ChinGentsai b;
    private C0046GoroDaimon c;
    private BenimaruNikaido d;
    private KyoKusanagi e;

    public interface BenimaruNikaido {
        void a(WebView webView, String str, boolean z);
    }

    public interface ChinGentsai {
        void a(WebView webView, String str);
    }

    /* renamed from: io.presage.formats.multiwebviews.GoroDaimon$GoroDaimon  reason: collision with other inner class name */
    public interface C0046GoroDaimon {
        void a(WebView webView, String str);
    }

    public interface KyoKusanagi {
        void a(WebView webView, String str, String str2);
    }

    public void a(BenimaruNikaido benimaruNikaido) {
        this.d = benimaruNikaido;
    }

    public void a(ChinGentsai chinGentsai) {
        this.b = chinGentsai;
    }

    public void a(C0046GoroDaimon goroDaimon) {
        this.c = goroDaimon;
    }

    public void a(KyoKusanagi kyoKusanagi) {
        this.e = kyoKusanagi;
    }

    public void onPageFinished(WebView webView, String str) {
        SaishuKusanagi.b("MultiWebViewClient", String.format("Page finished to be loaded: %s", new Object[]{str}));
        super.onPageFinished(webView, str);
        if (this.c != null) {
            this.c.a(webView, str);
        }
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        SaishuKusanagi.b("MultiWebViewClient", String.format("Page started to be loaded: %s", new Object[]{str}));
        super.onPageStarted(webView, str, bitmap);
        this.a = str;
        if (this.b != null) {
            this.b.a(webView, str);
        }
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        WebResourceResponse shouldInterceptRequest = super.shouldInterceptRequest(webView, str);
        if (this.e != null) {
            this.e.a(webView, this.a, str);
        }
        return shouldInterceptRequest;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String scheme = Uri.parse(str).getScheme();
        boolean z = scheme != null && !scheme.equals(TyphoonApp.HTTP) && !scheme.equals(TyphoonApp.HTTPS);
        if (this.d != null) {
            this.d.a(webView, str, z);
        }
        return z;
    }
}
