package io.presage.formats.multiwebviews;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import io.presage.p034goto.SaishuKusanagi;

public class BenimaruNikaido extends WebChromeClient {
    private String a;
    private Context b;

    public BenimaruNikaido(String str, Context context) {
        this.a = str;
        this.b = context;
    }

    @TargetApi(8)
    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        SaishuKusanagi.a("MultiWebChromeClient", String.format("[%s] %s -- From line %s of %s", new Object[]{this.a, consoleMessage.message(), Integer.toString(consoleMessage.lineNumber()), consoleMessage.sourceId()}));
        return super.onConsoleMessage(consoleMessage);
    }

    public boolean onCreateWindow(final WebView webView, boolean z, boolean z2, Message message) {
        WebView webView2 = new WebView(this.b);
        webView2.setWebViewClient(new WebViewClient(webView) {
            final /* synthetic */ WebView a;

            {
                this.a = r2;
            }

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                if (this.a != null) {
                    SaishuKusanagi.a("onCreateWindow url", str);
                    if (str != null && !str.equalsIgnoreCase("about:blank")) {
                        webView.loadUrl(str);
                    }
                }
            }
        });
        ((WebView.WebViewTransport) message.obj).setWebView(webView2);
        message.sendToTarget();
        return true;
    }

    public void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
        SaishuKusanagi.d("MultiWebChromeClient", String.format("[%s] onExceededDatabaseQuota [%s]", new Object[]{this.a, str}));
        super.onExceededDatabaseQuota(str, str2, j, j2, j3, quotaUpdater);
    }

    public void onGeolocationPermissionsHidePrompt() {
        SaishuKusanagi.c("MultiWebChromeClient", String.format("[%s] onGeolocationPermissionsHidePrompt", new Object[]{this.a}));
        super.onGeolocationPermissionsHidePrompt();
    }

    public void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
        SaishuKusanagi.b("MultiWebChromeClient", String.format("[%s] onGeolocationPermissionsShowPrompt [%s]", new Object[]{this.a, str}));
        super.onGeolocationPermissionsShowPrompt(str, callback);
    }

    public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        SaishuKusanagi.c("MultiWebChromeClient", String.format("[%s] onJsAlert %s [%s]", new Object[]{this.a, str2, str}));
        return super.onJsAlert(webView, str, str2, jsResult);
    }

    public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        SaishuKusanagi.c("MultiWebChromeClient", String.format("[%s] onJsConfirm %s [%s]", new Object[]{this.a, str2, str}));
        return super.onJsConfirm(webView, str, str2, jsResult);
    }

    public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        SaishuKusanagi.c("MultiWebChromeClient", String.format("[%s] onJsPrompt %s [%s]", new Object[]{this.a, str2, str}));
        return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
    }

    public void onReachedMaxAppCacheSize(long j, long j2, WebStorage.QuotaUpdater quotaUpdater) {
        SaishuKusanagi.d("MultiWebChromeClient", String.format("[%s] onReachedMaxAppCacheSize", new Object[]{this.a}));
        super.onReachedMaxAppCacheSize(j, j2, quotaUpdater);
    }
}
