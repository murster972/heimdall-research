package io.presage.formats.multiwebviews;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v4.os.EnvironmentCompat;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.mopub.mobileads.VastIconXmlManager;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import io.presage.ads.NewAd;
import io.presage.helper.Permissions;
import io.presage.p029char.ChangKoehan;
import io.presage.p034goto.LeonaHeidern;
import io.presage.p034goto.Maxima;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p038long.ChinGentsai;
import io.presage.p038long.GoroDaimon;
import io.presage.p038long.KyoKusanagi;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.regex.Pattern;

public class KyoKusanagi implements ViewGroup.OnHierarchyChangeListener, NewAd.ChoiBounge, GoroDaimon.BenimaruNikaido, GoroDaimon.ChangKoehan, GoroDaimon.ChinGentsai, GoroDaimon.ChoiBounge, GoroDaimon.C0050GoroDaimon, GoroDaimon.KyoKusanagi, KyoKusanagi.C0051KyoKusanagi {
    /* access modifiers changed from: private */
    public ChinGentsai a;
    /* access modifiers changed from: private */
    public NewAd b;
    private Map<io.presage.p038long.GoroDaimon, GoroDaimon> c = new WeakHashMap();
    private BroadcastReceiver d;
    private BroadcastReceiver e;
    /* access modifiers changed from: private */
    public C0047KyoKusanagi f;
    /* access modifiers changed from: private */
    public BenimaruNikaido g;
    /* access modifiers changed from: private */
    public int h;
    private Permissions i;
    private LeonaHeidern j;

    private interface BenimaruNikaido {
        void a(String str);
    }

    private class GoroDaimon {
        public String a;
        public boolean b;
        public boolean c;
        public int d;
        public int e;

        private GoroDaimon() {
            this.b = false;
            this.c = false;
            this.d = 1;
            this.e = 0;
        }

        public void a(String str) {
            this.a = str;
            this.b = false;
            this.c = false;
        }
    }

    /* renamed from: io.presage.formats.multiwebviews.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    private interface C0047KyoKusanagi {
        void a();
    }

    public KyoKusanagi(NewAd newAd, ChinGentsai chinGentsai, Permissions permissions) {
        this.a = chinGentsai;
        this.b = newAd;
        this.b.setVideoCompletionCallback(this);
        this.h = chinGentsai.getContext().getResources().getConfiguration().orientation;
        this.i = permissions;
        if (newAd != null && newAd.isMoatActivated() && ChangKoehan.a().b().contains("moat")) {
            try {
                this.j = new LeonaHeidern((Application) chinGentsai.getContext().getApplicationContext(), newAd, chinGentsai);
            } catch (Exception e2) {
                this.j = null;
                SaishuKusanagi.b("MOAT", "Error in constructor", e2);
            }
        }
    }

    private void a(BenimaruNikaido benimaruNikaido) {
        this.g = benimaruNikaido;
    }

    private void a(C0047KyoKusanagi kyoKusanagi) {
        this.f = kyoKusanagi;
    }

    /* access modifiers changed from: private */
    public void a(final String str, final Object obj) {
        final HashSet hashSet = new HashSet(this.c.keySet());
        Maxima.a((Runnable) new Runnable() {
            public void run() {
                for (io.presage.p038long.GoroDaimon a2 : hashSet) {
                    a2.a(str, obj);
                }
            }
        });
    }

    private void d(io.presage.p038long.GoroDaimon goroDaimon, String str) {
        this.c.get(goroDaimon).a(str);
        a("page_load", (Object) goroDaimon.getName());
    }

    private void h() {
        if (this.d == null) {
            this.d = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (("android.intent.action.CLOSE_SYSTEM_DIALOGS".equals(intent.getAction()) || "android.intent.action.SCREEN_OFF".equals(intent.getAction())) && KyoKusanagi.this.f != null) {
                        KyoKusanagi.this.f.a();
                    }
                }
            };
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            this.a.getContext().registerReceiver(this.d, intentFilter);
        }
    }

    private void i() {
        if (this.d != null) {
            try {
                this.a.getContext().unregisterReceiver(this.d);
            } catch (IllegalArgumentException e2) {
                SaishuKusanagi.c("MultiViewLayoutControl", String.format("Broadcast receiver for %s already unregistered", new Object[]{"android.intent.action.CLOSE_SYSTEM_DIALOGS"}));
            }
            this.d = null;
        }
    }

    private void j() {
        if (this.e == null) {
            this.e = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    int i;
                    if ("android.intent.action.CONFIGURATION_CHANGED".equals(intent.getAction()) && KyoKusanagi.this.h != (i = KyoKusanagi.this.a.getContext().getApplicationContext().getResources().getConfiguration().orientation)) {
                        int unused = KyoKusanagi.this.h = i;
                        if (KyoKusanagi.this.g != null) {
                            KyoKusanagi.this.g.a(KyoKusanagi.this.e());
                        }
                    }
                }
            };
            this.a.getContext().registerReceiver(this.e, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"));
        }
    }

    private void k() {
        if (this.e != null) {
            try {
                this.a.getContext().unregisterReceiver(this.e);
            } catch (IllegalArgumentException e2) {
                SaishuKusanagi.c("MultiViewLayoutControl", String.format("Broadcast closeReceiver for %s already unregistered.", new Object[]{"android.intent.action.CONFIGURATION_CHANGED"}));
            }
            this.e = null;
        }
    }

    public String a() {
        if (this.a != null) {
            for (String a2 : this.a.getManagedViewNames()) {
                io.presage.p038long.KyoKusanagi a3 = this.a.a(a2);
                if (a3 instanceof io.presage.p038long.BenimaruNikaido) {
                    return ((io.presage.p038long.BenimaruNikaido) a3).getVideoController().a();
                }
            }
        }
        return null;
    }

    public void a(WebView webView, String str, String str2) {
        SaishuKusanagi.b("MultiViewLayoutControl", String.format("Request intercepted %s", new Object[]{str2}));
        if (!this.c.containsKey(webView)) {
            SaishuKusanagi.c("MultiViewLayoutControl", String.format("View has been removed. OnPageFinished skipped. %s", new Object[]{str}));
            return;
        }
        String clientTrackerPattern = this.b.getClientTrackerPattern();
        if (clientTrackerPattern != null && Pattern.matches(clientTrackerPattern, str2)) {
            GoroDaimon goroDaimon = this.c.get(webView);
            if (!goroDaimon.c) {
                goroDaimon.c = true;
                this.b.onPageFinished(webView, str, goroDaimon.d, clientTrackerPattern, str2);
            }
        }
    }

    public void a(WebView webView, String str, boolean z) {
        if (webView instanceof io.presage.p038long.GoroDaimon) {
            if (this.j != null) {
                this.j.a();
            }
            String name = ((io.presage.p038long.GoroDaimon) webView).getName();
            SaishuKusanagi.b("MultiViewLayoutControl", String.format("URL overrided for webview %s %s", new Object[]{name, str}));
            if (!this.c.containsKey(webView)) {
                SaishuKusanagi.c("MultiViewLayoutControl", String.format("View has been removed. onOverrideUrl skipped. %s", new Object[]{str}));
                return;
            }
            if (z) {
                HashMap hashMap = new HashMap();
                hashMap.put("name", name);
                hashMap.put("url", str);
                a("override_url", (Object) hashMap);
            }
            d((io.presage.p038long.GoroDaimon) webView, str);
        }
    }

    public void a(io.presage.p038long.GoroDaimon goroDaimon) {
        GoroDaimon goroDaimon2 = this.c.get(goroDaimon);
        if (goroDaimon2 != null) {
            goroDaimon2.a(goroDaimon.getUrl());
        }
    }

    public void a(io.presage.p038long.GoroDaimon goroDaimon, String str) {
        if (!this.c.containsKey(goroDaimon)) {
            SaishuKusanagi.c("MultiViewLayoutControl", String.format("View has been removed. (%s) onLoadUrl skipped.", new Object[0]));
        } else {
            d(goroDaimon, str);
        }
    }

    public void a(io.presage.p038long.KyoKusanagi kyoKusanagi) {
        SaishuKusanagi.b("MultiViewLayoutControl", String.format("%s click", new Object[]{KyoKusanagi.class.getSimpleName()}));
        a("click", (Object) kyoKusanagi.getName());
    }

    public void a(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("name", "video");
        hashMap.put(VastIconXmlManager.DURATION, str);
        a("video_progression", (Object) hashMap);
    }

    public LeonaHeidern b() {
        return this.j;
    }

    public void b(io.presage.p038long.GoroDaimon goroDaimon, String str) {
        SaishuKusanagi.b("MultiViewLayoutControl", String.format("Page started to be loaded. %s", new Object[]{str}));
        if (!this.c.containsKey(goroDaimon)) {
            SaishuKusanagi.c("MultiViewLayoutControl", String.format("View has been removed. onPageStarted skipped. %s", new Object[]{str}));
            return;
        }
        if (this.j != null) {
            this.j.a(goroDaimon);
        }
        HashMap hashMap = new HashMap();
        hashMap.put("name", goroDaimon.getName());
        hashMap.put("url", str);
        a("page_started", (Object) hashMap);
    }

    public void b(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("name", "video");
        hashMap.put("detail", str);
        a("video_buffer", (Object) hashMap);
    }

    public void c() {
        HashMap hashMap = new HashMap();
        hashMap.put("name", "video");
        a("video_prepared", (Object) hashMap);
        if (this.j != null) {
            this.j.b();
        }
    }

    public void c(io.presage.p038long.GoroDaimon goroDaimon, String str) {
        SaishuKusanagi.b("MultiViewLayoutControl", String.format("Page finished to be loaded. %s", new Object[]{str}));
        if (!this.c.containsKey(goroDaimon)) {
            SaishuKusanagi.c("MultiViewLayoutControl", String.format("View has been removed. OnPageFinished skipped. %s", new Object[]{str}));
            return;
        }
        GoroDaimon goroDaimon2 = this.c.get(goroDaimon);
        if (!goroDaimon2.b || (goroDaimon2.b && !goroDaimon2.a.equals(str))) {
            goroDaimon2.b = true;
            goroDaimon2.a = str;
            goroDaimon2.e = goroDaimon2.d;
            a("page_finished", (Object) goroDaimon.getName());
            if (this.b.getClientTrackerPattern() == null) {
                this.b.onPageFinished(goroDaimon, str, goroDaimon2.d, (String) null, (String) null);
            }
            goroDaimon2.d = goroDaimon2.e + 1;
        }
    }

    public void d() {
        HashMap hashMap = new HashMap();
        hashMap.put("name", "video");
        a("video_completed", (Object) hashMap);
    }

    /* access modifiers changed from: package-private */
    public String e() {
        switch (this.h) {
            case 1:
                return "portrait";
            case 2:
                return "landscape";
            default:
                return EnvironmentCompat.MEDIA_UNKNOWN;
        }
    }

    public void f() {
        h();
        j();
        this.a.setOnHierarchyChangeListener(this);
        this.a.a();
        a((BenimaruNikaido) new BenimaruNikaido() {
            public void a(String str) {
                SaishuKusanagi.b("MultiViewLayoutControl", String.format("onOrientationChanged %s", new Object[]{str}));
                KyoKusanagi.this.a(AbstractSpiCall.ANDROID_CLIENT_TYPE, (Object) str);
            }
        });
        a((C0047KyoKusanagi) new C0047KyoKusanagi() {
            public void a() {
                SaishuKusanagi.b("MultiViewLayoutControl", "Soft key pressed (home or recent apps).");
                KyoKusanagi.this.b.onFormatEvent(NewAd.EVENT_CLOSE_SYSTEM_DIALOG);
                KyoKusanagi.this.a(AbstractSpiCall.ANDROID_CLIENT_TYPE, (Object) NewAd.EVENT_CLOSE_SYSTEM_DIALOG);
            }
        });
        this.a.setOnBackListener(new ChinGentsai.KyoKusanagi() {
            public void a() {
                SaishuKusanagi.b("MultiViewLayoutControl", "Back button pressed.");
                KyoKusanagi.this.a(AbstractSpiCall.ANDROID_CLIENT_TYPE, (Object) "back");
            }
        });
    }

    public void g() {
        if (this.j != null) {
            this.j.a();
        }
        i();
        k();
        a((BenimaruNikaido) null);
        a((C0047KyoKusanagi) null);
        this.a.setOnBackListener((ChinGentsai.KyoKusanagi) null);
        final HashSet<String> managedWebViewNames = this.a.getManagedWebViewNames();
        if (managedWebViewNames != null && !managedWebViewNames.isEmpty()) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (managedWebViewNames != null) {
                        Iterator it2 = managedWebViewNames.iterator();
                        while (it2.hasNext()) {
                            io.presage.p038long.KyoKusanagi a2 = KyoKusanagi.this.a.a((String) it2.next());
                            if (a2 instanceof io.presage.p038long.GoroDaimon) {
                                ((io.presage.p038long.GoroDaimon) a2).e();
                            }
                        }
                    }
                }
            }, 2000);
        }
    }

    @SuppressLint({"AddJavascriptInterface"})
    public void onChildViewAdded(View view, View view2) {
        if (this.a.equals(view)) {
            if (view2 instanceof io.presage.p038long.GoroDaimon) {
                io.presage.p038long.GoroDaimon goroDaimon = (io.presage.p038long.GoroDaimon) view2;
                SaishuKusanagi.b("MultiViewLayoutControl", String.format("%s ManagedView added %s", new Object[]{KyoKusanagi.class.getSimpleName(), goroDaimon.getName()}));
                goroDaimon.setOnClickViewListener(this);
                goroDaimon.setOnLoadUrlListener(this);
                goroDaimon.setOnOverrideUrlListener(this);
                goroDaimon.setOnPageStartedListener(this);
                goroDaimon.setOnPageFinishedListener(this);
                goroDaimon.setOnInterceptRequestListener(this);
                goroDaimon.setOnGoBackListener(this);
                this.c.put(goroDaimon, new GoroDaimon());
                goroDaimon.addJavascriptInterface(new io.presage.p010byte.BenimaruNikaido(this.a, this.i), TtmlNode.TAG_LAYOUT);
                goroDaimon.addJavascriptInterface(new io.presage.p010byte.KyoKusanagi(this.b), "ad");
                goroDaimon.addJavascriptInterface(new io.presage.p010byte.GoroDaimon(goroDaimon.getContext(), this.i), "sdk");
            } else if (view2 instanceof io.presage.p038long.BenimaruNikaido) {
                io.presage.p038long.BenimaruNikaido benimaruNikaido = (io.presage.p038long.BenimaruNikaido) view2;
                SaishuKusanagi.b("MultiViewLayoutControl", String.format("%s ManagedView added %s", new Object[]{KyoKusanagi.class.getSimpleName(), benimaruNikaido.getName()}));
                benimaruNikaido.getVideoController().a(this);
                benimaruNikaido.getVideoController().b(this);
                benimaruNikaido.setOnClickViewListener(this);
            }
        }
    }

    public void onChildViewRemoved(View view, View view2) {
        if (this.a.equals(view)) {
            if (view2 instanceof io.presage.p038long.GoroDaimon) {
                SaishuKusanagi.b("MultiViewLayoutControl", String.format("%s ManagedView removed %s", new Object[]{KyoKusanagi.class.getSimpleName(), ((io.presage.p038long.GoroDaimon) view2).getName()}));
            } else if (view2 instanceof io.presage.p038long.BenimaruNikaido) {
                io.presage.p038long.BenimaruNikaido benimaruNikaido = (io.presage.p038long.BenimaruNikaido) view2;
                benimaruNikaido.getVideoController().a((KyoKusanagi) null);
                benimaruNikaido.getVideoController().b((KyoKusanagi) null);
                benimaruNikaido.setOnClickViewListener((KyoKusanagi.C0051KyoKusanagi) null);
            }
        }
    }
}
