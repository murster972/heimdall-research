package io.presage.formats.multiwebviews;

import android.app.Activity;
import android.view.View;
import android.widget.FrameLayout;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdController;
import io.presage.ads.NewAdViewer;
import io.presage.helper.Permissions;
import io.presage.model.Zone;
import io.presage.p034goto.Bao;
import io.presage.p038long.ChinGentsai;
import io.presage.p038long.GoroDaimon;
import io.presage.p038long.KyoKusanagi;
import java.lang.ref.WeakReference;
import java.util.List;

public class ActivityMultiViewHelper extends NewAdViewer {
    private ChinGentsai f;
    private KyoKusanagi g;
    private WeakReference<Activity> h = new WeakReference<>((Activity) this.a);

    public ActivityMultiViewHelper(NewAdController newAdController, NewAd newAd, Permissions permissions, int i) {
        super(newAdController, newAd, permissions, i);
    }

    public void hide() {
        Activity activity;
        if (this.g != null) {
            this.g.g();
            this.g = null;
        }
        if (this.h != null && ((activity = (Activity) this.h.get()) != null || !activity.isFinishing())) {
            activity.finish();
        }
        onHide();
    }

    public void show() {
        FrameLayout.LayoutParams b;
        Activity activity;
        this.f = new ChinGentsai(this.a);
        this.g = new KyoKusanagi(this.c, this.f, this.e);
        this.g.f();
        Zone zone = (Zone) this.c.getOverridedParameterValue("frame", Zone.class);
        if (zone == null) {
            b = Bao.a();
        } else {
            b = Bao.b(this.a, zone);
            Bao.a((View) this.f, zone);
        }
        if (!(this.h == null || (activity = (Activity) this.h.get()) == null)) {
            activity.setContentView(this.f, b);
        }
        List<Zone> list = (List) this.c.getOverridedParameter("zones").get();
        if (list != null) {
            for (Zone a : list) {
                this.f.a((KyoKusanagi) GoroDaimon.a(this.a, this.e, a));
            }
        }
        this.c.onFormatEvent(NewAd.EVENT_SHOWN);
        onShow();
    }
}
