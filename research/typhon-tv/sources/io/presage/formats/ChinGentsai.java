package io.presage.formats;

import android.content.Context;
import io.presage.ads.ChangKoehan;
import io.presage.helper.Permissions;
import io.presage.p033for.GoroDaimon;

public class ChinGentsai {
    private static ChinGentsai a = null;

    private ChinGentsai() {
    }

    public static ChinGentsai a() {
        if (a == null) {
            a = new ChinGentsai();
        }
        return a;
    }

    public KyoKusanagi a(Context context, Permissions permissions, String str, String str2, ChangKoehan changKoehan, String str3, GoroDaimon goroDaimon) throws GoroDaimon {
        if (str2.equals("webviews")) {
            if (io.presage.helper.ChinGentsai.b(context, "android.permission.SYSTEM_ALERT_WINDOW")) {
                return new LuckyGlauber(context, str, str2, changKoehan, goroDaimon, str3);
            }
            throw new GoroDaimon(1);
        } else if (str2.equals("launch_activity")) {
            return new ChoiBounge(context, str, str2, changKoehan, goroDaimon);
        } else {
            if (str2.equals("hidden")) {
                return new ChangKoehan(context, str, str2, changKoehan, goroDaimon);
            }
            throw new GoroDaimon(0);
        }
    }
}
