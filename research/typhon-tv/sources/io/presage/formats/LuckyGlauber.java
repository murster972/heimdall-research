package io.presage.formats;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.mopub.common.TyphoonApp;
import com.mopub.mobileads.VastIconXmlManager;
import com.p003if.p004do.ChoiBounge;
import io.presage.ads.ChangKoehan;
import io.presage.ads.NewAd;
import io.presage.formats.KyoKusanagi;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Whip;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

@Deprecated
public class LuckyGlauber extends HeavyD {
    public boolean e = false;
    private Handler f;
    /* access modifiers changed from: private */
    public String g;

    class GoroDaimon {
        /* access modifiers changed from: private */
        public LuckyGlauber b;
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public WebView d;
        private io.presage.p033for.GoroDaimon e;
        private String f;

        public GoroDaimon(String str, WebView webView, LuckyGlauber luckyGlauber, io.presage.p033for.GoroDaimon goroDaimon, String str2) {
            this.c = str;
            this.d = webView;
            this.b = luckyGlauber;
            this.e = goroDaimon;
            this.f = str2;
        }

        @JavascriptInterface
        public void close(int i) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (GoroDaimon.this.b.e) {
                        GoroDaimon.this.b.a().b(NewAd.EVENT_CANCEL);
                    } else {
                        GoroDaimon.this.b.a().b("close");
                    }
                }
            }, (long) i);
        }

        @JavascriptInterface
        public String getAd() {
            return this.f;
        }

        @JavascriptInterface
        public void loadComplete() {
            SaishuKusanagi.b("WebViews", String.format("%s [%s] loadComplete", new Object[]{"WebViews", this.c}));
            this.b.j();
        }

        @JavascriptInterface
        public String param(String str) {
            return new ChoiBounge().m14078(this.e.a(str)).toString();
        }

        @JavascriptInterface
        public void sendAction(String str) {
            SaishuKusanagi.b("WebViews", String.format("%s [%s] sendAction: %s", new Object[]{"WebViews", this.c, str}));
            if (str.equals("close")) {
                this.b.e = false;
                this.b.k();
            } else if (str.equals(NewAd.EVENT_CANCEL)) {
                this.b.e = true;
                this.b.k();
            } else {
                this.b.a().b(str);
            }
        }

        @JavascriptInterface
        public void setTimeout(final String str, int i) {
            SaishuKusanagi.b("WebViews", String.format("%s [%s] setTimeout: %s - %s", new Object[]{"WebViews", this.c, str, Integer.toString(i)}));
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    SaishuKusanagi.b("WebViews", String.format("%s [%s] setTimout call: %s", new Object[]{"WebViews", GoroDaimon.this.c, str}));
                    GoroDaimon.this.d.loadUrl("javascript:window." + str + "();");
                }
            }, (long) i);
        }

        @JavascriptInterface
        public String stringParam(String str) {
            return this.e.a(str).toString();
        }
    }

    class BenimaruNikaido extends WebChromeClient {
        private LuckyGlauber b;
        private String c;
        private io.presage.p033for.GoroDaimon d;

        public BenimaruNikaido(String str, LuckyGlauber luckyGlauber, io.presage.p033for.GoroDaimon goroDaimon) {
            this.c = str;
            this.b = luckyGlauber;
            this.d = goroDaimon;
        }

        public void onConsoleMessage(String str, int i, String str2) {
            SaishuKusanagi.a("WebViews", String.format("%s [%s] %s -- From line %s of %s", new Object[]{"WebViews", this.c, str, Integer.toString(i), str2}));
        }
    }

    class KyoKusanagi extends WebViewClient {
        public KyoKusanagi(String str, LuckyGlauber luckyGlauber, io.presage.p033for.GoroDaimon goroDaimon) {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            String scheme = Uri.parse(str).getScheme();
            return scheme != null && !scheme.equals(TyphoonApp.HTTP) && !scheme.equals(TyphoonApp.HTTPS);
        }
    }

    public LuckyGlauber(Context context, String str, String str2, ChangKoehan changKoehan, io.presage.p033for.GoroDaimon goroDaimon, String str3) {
        super(context, str, str2, changKoehan, goroDaimon);
        a((KyoKusanagi.C0045KyoKusanagi) changKoehan);
        this.f = new Handler(context.getMainLooper());
        this.g = str3;
    }

    public void b() {
        this.f.post(new Runnable() {
            public void run() {
                if (LuckyGlauber.this.e() != null) {
                    Iterator<BenimaruNikaido> it2 = LuckyGlauber.this.e().iterator();
                    while (it2.hasNext()) {
                        BenimaruNikaido next = it2.next();
                        ViewGroup viewGroup = (ViewGroup) next.b();
                        WebView webView = (WebView) viewGroup.findViewWithTag("webview");
                        webView.loadUrl("about:blank");
                        webView.destroy();
                        next.c();
                        viewGroup.removeAllViews();
                        LuckyGlauber.this.b = null;
                    }
                    LuckyGlauber.this.b = null;
                }
            }
        });
        super.b();
    }

    public void c() {
        this.b = new ArrayList();
        this.f.post(new Runnable() {
            public void run() {
                if (LuckyGlauber.this.h() != null && LuckyGlauber.this.e() != null) {
                    Iterator it2 = ((ArrayList) LuckyGlauber.this.a("zones")).iterator();
                    while (it2.hasNext()) {
                        Map map = (Map) it2.next();
                        WebView webView = new WebView(LuckyGlauber.this.i().getApplicationContext());
                        webView.clearHistory();
                        webView.clearAnimation();
                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.setWebViewClient(new KyoKusanagi((String) map.get("name"), this, LuckyGlauber.this.h()));
                        webView.setWebChromeClient(new BenimaruNikaido((String) map.get("name"), this, LuckyGlauber.this.h()));
                        webView.addJavascriptInterface(new GoroDaimon((String) map.get("name"), webView, this, LuckyGlauber.this.h(), LuckyGlauber.this.g), "Presage");
                        webView.setBackgroundColor(0);
                        webView.setTag("webview");
                        webView.setVerticalScrollBarEnabled(false);
                        webView.setHorizontalScrollBarEnabled(false);
                        SaishuKusanagi.b("WebViews", String.format("%s [%s] load url: %s", new Object[]{"WebViews", map.get("name").toString(), map.get("url").toString()}));
                        webView.loadUrl((String) map.get("url"));
                        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                        layoutParams.width = -1;
                        layoutParams.height = -1;
                        layoutParams.x = 0;
                        layoutParams.y = 0;
                        layoutParams.gravity = 51;
                        layoutParams.type = 2003;
                        layoutParams.format = -3;
                        layoutParams.flags = 262184;
                        layoutParams.setTitle("Load Average");
                        Map map2 = (Map) map.get("size");
                        if (map2 != null) {
                            Double d = (Double) map2.get(VastIconXmlManager.WIDTH);
                            if (d != null && d.doubleValue() > 0.0d) {
                                layoutParams.width = Whip.a(LuckyGlauber.this.i(), (int) Math.round(d.doubleValue()));
                            }
                            Double d2 = (Double) map2.get(VastIconXmlManager.HEIGHT);
                            if (d2 != null && d2.doubleValue() > 0.0d) {
                                layoutParams.height = Whip.a(LuckyGlauber.this.i(), (int) Math.round(d2.doubleValue()));
                            }
                        }
                        Map map3 = (Map) map.get("position");
                        if (map3 != null) {
                            Double d3 = (Double) map3.get("x");
                            if (d3 != null && d3.doubleValue() > 0.0d) {
                                layoutParams.x = Whip.a(LuckyGlauber.this.i(), (int) Math.round(d3.doubleValue()));
                            }
                            Double d4 = (Double) map3.get("y");
                            if (d4 != null && d4.doubleValue() > 0.0d) {
                                layoutParams.y = Whip.a(LuckyGlauber.this.i(), (int) Math.round(d4.doubleValue()));
                            }
                        }
                        ArrayList arrayList = (ArrayList) map.get("gravity");
                        if (arrayList != null) {
                            layoutParams.gravity = 0;
                            Iterator it3 = arrayList.iterator();
                            while (it3.hasNext()) {
                                String str = (String) it3.next();
                                if (str.equals("top")) {
                                    layoutParams.gravity |= 48;
                                } else if (str.equals(TtmlNode.LEFT)) {
                                    layoutParams.gravity |= 3;
                                } else if (str.equals("bottom")) {
                                    layoutParams.gravity |= 80;
                                } else if (str.equals(TtmlNode.RIGHT)) {
                                    layoutParams.gravity |= 5;
                                }
                            }
                        }
                        RelativeLayout relativeLayout = new RelativeLayout(LuckyGlauber.this.i());
                        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                        layoutParams2.bottomMargin = 0;
                        layoutParams2.leftMargin = 0;
                        layoutParams2.rightMargin = 0;
                        layoutParams2.topMargin = 0;
                        relativeLayout.setLayoutParams(layoutParams2);
                        relativeLayout.addView(webView, 0, new RelativeLayout.LayoutParams(-1, -1));
                        LuckyGlauber.this.e().add(new BenimaruNikaido(layoutParams, relativeLayout));
                    }
                    WindowManager windowManager = (WindowManager) LuckyGlauber.this.i().getSystemService("window");
                    if (LuckyGlauber.this.e() != null) {
                        Iterator<BenimaruNikaido> it4 = LuckyGlauber.this.e().iterator();
                        while (it4.hasNext()) {
                            BenimaruNikaido next = it4.next();
                            windowManager.addView(next.b(), next.a());
                        }
                    }
                }
            }
        });
    }

    public void d() {
        this.f.post(new Runnable() {
            public void run() {
                if (LuckyGlauber.this.i() != null) {
                    WindowManager windowManager = (WindowManager) LuckyGlauber.this.i().getSystemService("window");
                    if (LuckyGlauber.this.e() != null) {
                        try {
                            Iterator<BenimaruNikaido> it2 = LuckyGlauber.this.e().iterator();
                            while (it2.hasNext()) {
                                windowManager.removeView(it2.next().b());
                            }
                        } catch (IllegalArgumentException e) {
                            SaishuKusanagi.c("WebViews", String.format("%s hideAd error: %s", new Object[]{"Ad", e.toString()}));
                        }
                    }
                }
            }
        });
    }

    public void k() {
        this.f.post(new Runnable() {
            public void run() {
                if (LuckyGlauber.this.e() != null) {
                    Iterator<BenimaruNikaido> it2 = LuckyGlauber.this.e().iterator();
                    while (it2.hasNext()) {
                        ((WebView) ((ViewGroup) it2.next().b()).findViewWithTag("webview")).loadUrl("javascript:window.onPresageClose();");
                    }
                }
            }
        });
    }
}
