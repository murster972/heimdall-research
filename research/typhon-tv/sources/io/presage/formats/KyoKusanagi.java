package io.presage.formats;

import android.content.Context;
import io.presage.ads.ChangKoehan;
import io.presage.p033for.GoroDaimon;
import java.util.ArrayList;

@Deprecated
public abstract class KyoKusanagi {
    protected ChangKoehan a;
    @Deprecated
    protected ArrayList<BenimaruNikaido> b = new ArrayList<>();
    protected Context c;
    @Deprecated
    protected C0045KyoKusanagi d;
    private String e;
    private String f;
    private GoroDaimon g;

    /* renamed from: io.presage.formats.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    public interface C0045KyoKusanagi {
        void b(String str);
    }

    public KyoKusanagi(Context context, String str, String str2, ChangKoehan changKoehan, GoroDaimon goroDaimon) {
        this.c = context;
        this.e = str;
        this.f = str2;
        this.a = changKoehan;
        this.g = goroDaimon;
    }

    @Deprecated
    public C0045KyoKusanagi a() {
        return this.d;
    }

    @Deprecated
    public Object a(String str) {
        return this.g.a(str);
    }

    @Deprecated
    public void a(C0045KyoKusanagi kyoKusanagi) {
        this.d = kyoKusanagi;
    }

    public void b() {
        this.e = null;
        this.f = null;
        this.g = null;
        this.c = null;
    }

    public abstract void c();

    public abstract void d();

    @Deprecated
    public ArrayList<BenimaruNikaido> e() {
        return this.b;
    }

    public String f() {
        return this.e;
    }

    public String g() {
        return this.f;
    }

    @Deprecated
    public GoroDaimon h() {
        return this.g;
    }

    public Context i() {
        return this.c;
    }
}
