package io.presage.formats;

import io.presage.ads.NewAdViewerDescriptor;
import io.presage.model.Parameter;
import java.util.List;

public class NewHelperDescriptor extends NewAdViewerDescriptor {
    public NewHelperDescriptor() {
    }

    public NewHelperDescriptor(String str, List<Parameter> list) {
        super(str, list);
    }
}
