package io.presage.formats;

import android.view.View;
import android.view.WindowManager;

@Deprecated
public class BenimaruNikaido {
    private WindowManager.LayoutParams a;
    private View b;

    public BenimaruNikaido(WindowManager.LayoutParams layoutParams, View view) {
        this.a = layoutParams;
        this.b = view;
    }

    public WindowManager.LayoutParams a() {
        return this.a;
    }

    public View b() {
        return this.b;
    }

    public void c() {
        this.a = null;
        this.b = null;
    }
}
