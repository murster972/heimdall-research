package io.presage.formats;

import android.content.Context;
import io.presage.ads.NewAd;
import io.presage.formats.KyoKusanagi;
import io.presage.p033for.GoroDaimon;
import java.util.ArrayList;
import java.util.Iterator;

public class ChangKoehan extends HeavyD {
    public ChangKoehan(Context context, String str, String str2, io.presage.ads.ChangKoehan changKoehan, GoroDaimon goroDaimon) {
        super(context, str, str2, changKoehan, goroDaimon);
        a((KyoKusanagi.C0045KyoKusanagi) changKoehan);
    }

    public void c() {
        this.a.a(NewAd.EVENT_SHOWN);
        ArrayList arrayList = (ArrayList) a("actions");
        if (arrayList != null) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                a().b((String) it2.next());
            }
        }
        a().b("close");
    }

    public void d() {
    }
}
