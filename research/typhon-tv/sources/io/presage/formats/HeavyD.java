package io.presage.formats;

import android.content.Context;
import io.presage.ads.ChangKoehan;
import io.presage.ads.NewAd;
import io.presage.p033for.GoroDaimon;

public abstract class HeavyD extends KyoKusanagi {
    public HeavyD(Context context, String str, String str2, ChangKoehan changKoehan, GoroDaimon goroDaimon) {
        super(context, str, str2, changKoehan, goroDaimon);
    }

    public void j() {
        this.a.a(NewAd.EVENT_SHOWN);
    }
}
