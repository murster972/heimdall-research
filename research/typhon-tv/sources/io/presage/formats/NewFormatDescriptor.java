package io.presage.formats;

import io.presage.ads.NewAdViewerDescriptor;
import io.presage.model.Parameter;
import java.util.List;

public class NewFormatDescriptor extends NewAdViewerDescriptor {
    public NewFormatDescriptor() {
    }

    public NewFormatDescriptor(String str, List<Parameter> list) {
        super(str, list);
    }
}
