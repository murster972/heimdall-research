package io.presage.formats;

import android.content.Intent;
import android.os.Bundle;
import com.p003if.p004do.ChoiBounge;
import io.presage.activities.PresageActivity;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdController;
import io.presage.ads.NewAdViewer;
import io.presage.helper.Permissions;
import io.presage.p034goto.SaishuKusanagi;

public class NewLaunchActivityFormat extends NewAdViewer {
    public NewLaunchActivityFormat(NewAdController newAdController, NewAd newAd, Permissions permissions, int i) {
        super(newAdController, newAd, permissions, i);
    }

    public void hide() {
        SaishuKusanagi.c("NewLaunchActivityFormat", "Once launched, the launch activity cannot be hidden in this context.");
    }

    public void show() {
        Intent intent = new Intent(this.a, PresageActivity.class);
        intent.addFlags(268435456);
        intent.putExtra("activity_handler", "launch_activity");
        Bundle bundle = new Bundle();
        bundle.putParcelable("permissions", this.e);
        intent.putExtra("permissions_bundle", bundle);
        intent.putExtra("ad", new ChoiBounge().m14078((Object) this.c));
        intent.putExtra("controller", io.presage.ads.ChoiBounge.a().a(this.b));
        intent.putExtra("flags", this.d);
        this.a.startActivity(intent);
    }
}
