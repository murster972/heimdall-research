package io.presage.formats;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import io.presage.activities.PresageActivity;
import io.presage.activities.handlers.LegacyActivityHandler;
import io.presage.ads.ChangKoehan;
import io.presage.formats.KyoKusanagi;
import io.presage.p033for.GoroDaimon;

public class ChoiBounge extends KyoKusanagi {
    private LegacyActivityHandler e;

    public ChoiBounge(Context context, String str, String str2, ChangKoehan changKoehan, GoroDaimon goroDaimon) {
        super(context, str, str2, changKoehan, goroDaimon);
        a((KyoKusanagi.C0045KyoKusanagi) changKoehan);
    }

    public void a(LegacyActivityHandler legacyActivityHandler) {
        this.e = legacyActivityHandler;
    }

    public void b() {
        if (this.e != null) {
            this.e.finishActivity();
        }
        super.b();
    }

    @TargetApi(3)
    public void c() {
        Intent launchIntentForPackage;
        String str = (String) a("launch_type");
        if (str.equals("sdk")) {
            launchIntentForPackage = new Intent();
            launchIntentForPackage.setClassName(i(), PresageActivity.class.getName());
            launchIntentForPackage.setFlags(536870912);
            launchIntentForPackage.putExtra("io.presage.extras.ADVERT_ID", this.a.g());
        } else {
            launchIntentForPackage = str.equals("app") ? i().getPackageManager().getLaunchIntentForPackage((String) a("launch_package")) : null;
        }
        if (launchIntentForPackage != null) {
            launchIntentForPackage.addFlags(268435456);
            i().startActivity(launchIntentForPackage);
        }
    }

    public void d() {
    }
}
