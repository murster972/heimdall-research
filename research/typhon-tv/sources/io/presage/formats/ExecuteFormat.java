package io.presage.formats;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import io.presage.actions.LuckyGlauber;
import io.presage.actions.NewAction;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdController;
import io.presage.ads.NewAdViewer;
import io.presage.helper.Permissions;
import io.presage.model.KyoKusanagi;
import io.presage.p034goto.SaishuKusanagi;
import java.util.List;

public class ExecuteFormat extends NewAdViewer {
    public ExecuteFormat(NewAdController newAdController, NewAd newAd, Permissions permissions, int i) {
        super(newAdController, newAd, permissions, i);
    }

    public void hide() {
    }

    @TargetApi(3)
    public void show() {
        List<String> list = (List) this.c.getOverridedParameterValue("actions", List.class);
        if (list == null) {
            SaishuKusanagi.c("ExecuteFormat", "Hidden format called without any actions to execute.");
        }
        for (final String str : list) {
            KyoKusanagi actionDescriptor = this.c.getActionDescriptor(str);
            if (actionDescriptor == null) {
                SaishuKusanagi.c("ExecuteFormat", String.format("The action %s does not exist.", new Object[]{str}));
            } else {
                final NewAction a = actionDescriptor.a(this.b.getContext(), this.e, this.c.getParameters());
                if (a == null) {
                    SaishuKusanagi.c("ExecuteFormat", String.format("Unable to instantiate the action", new Object[]{str}));
                } else {
                    new AsyncTask<String, String, String>() {
                        /* access modifiers changed from: protected */
                        /* renamed from: a */
                        public String doInBackground(String... strArr) {
                            try {
                                a.execute();
                                return null;
                            } catch (LuckyGlauber e) {
                                ExecuteFormat.this.c.onFormatError("action:" + str, e.getMessage());
                                return null;
                            }
                        }
                    }.execute(new String[0]);
                }
            }
        }
    }
}
