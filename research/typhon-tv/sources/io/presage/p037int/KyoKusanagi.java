package io.presage.p037int;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* renamed from: io.presage.int.KyoKusanagi  reason: invalid package */
public class KyoKusanagi extends SQLiteOpenHelper {
    private static KyoKusanagi a;

    private KyoKusanagi(Context context) {
        super(context, "preference.db", (SQLiteDatabase.CursorFactory) null, 1);
    }

    private synchronized SQLiteDatabase a() {
        return getWritableDatabase();
    }

    public static synchronized KyoKusanagi a(Context context) {
        KyoKusanagi kyoKusanagi;
        synchronized (KyoKusanagi.class) {
            if (a == null) {
                a = new KyoKusanagi(context);
            }
            kyoKusanagi = a;
        }
        return kyoKusanagi;
    }

    public int a(ContentValues contentValues, String str, String[] strArr) {
        return a().update("preference", contentValues, str, strArr);
    }

    public long a(ContentValues contentValues) {
        return a().insert("preference", (String) null, contentValues);
    }

    public Cursor a(String[] strArr, String str, String[] strArr2, String str2) {
        return a().query("preference", strArr, str, strArr2, (String) null, (String) null, str2);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE preference ( id TEXT PRIMARY KEY, value TEXT);");
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase != null) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preference");
            onCreate(sQLiteDatabase);
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase != null) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preference");
            onCreate(sQLiteDatabase);
        }
    }
}
