package io.presage.p037int;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

/* renamed from: io.presage.int.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido extends SQLiteOpenHelper {
    private static BenimaruNikaido a;

    private BenimaruNikaido(Context context) {
        super(context, "presage.db", (SQLiteDatabase.CursorFactory) null, 2);
    }

    public static synchronized BenimaruNikaido a(Context context) {
        BenimaruNikaido benimaruNikaido;
        synchronized (BenimaruNikaido.class) {
            if (a == null) {
                a = new BenimaruNikaido(context);
            }
            benimaruNikaido = a;
        }
        return benimaruNikaido;
    }

    public long a(ContentValues contentValues) {
        if (contentValues.keySet().size() == 0) {
            return -1;
        }
        SQLiteDatabase a2 = a();
        a2.execSQL("DELETE FROM profig WHERE k = '" + contentValues.get("k") + "'");
        return a2.insert("profig", (String) null, contentValues);
    }

    public Cursor a(String str) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("profig");
        char c = 65535;
        switch (str.hashCode()) {
            case -1768268487:
                if (str.equals("data_optin_v21")) {
                    c = 0;
                    break;
                }
                break;
            case -1411271163:
                if (str.equals("apikey")) {
                    c = 6;
                    break;
                }
                break;
            case -1399609336:
                if (str.equals("raw_string")) {
                    c = 2;
                    break;
                }
                break;
            case -1336623918:
                if (str.equals("ads_timeout")) {
                    c = 1;
                    break;
                }
                break;
            case 2986299:
                if (str.equals("aaid")) {
                    c = 3;
                    break;
                }
                break;
            case 767390409:
                if (str.equals("ads_optin")) {
                    c = 4;
                    break;
                }
                break;
            case 1453035404:
                if (str.equals("launch_optin")) {
                    c = 5;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                sQLiteQueryBuilder.appendWhere("k = 'data_optin_v21'");
                break;
            case 1:
                sQLiteQueryBuilder.appendWhere("k = 'ads_timeout'");
                break;
            case 2:
                sQLiteQueryBuilder.appendWhere("k = 'raw_string'");
                break;
            case 3:
                sQLiteQueryBuilder.appendWhere("k = 'aaid'");
                break;
            case 4:
                sQLiteQueryBuilder.appendWhere("k = 'ads_optin'");
                break;
            case 5:
                sQLiteQueryBuilder.appendWhere("k = 'launch_optin'");
                break;
            case 6:
                sQLiteQueryBuilder.appendWhere("k = 'apikey'");
                break;
            default:
                return null;
        }
        return sQLiteQueryBuilder.query(a(), new String[]{"v"}, (String) null, (String[]) null, (String) null, (String) null, (String) null);
    }

    public synchronized SQLiteDatabase a() {
        return getWritableDatabase();
    }

    public void b() {
        a().delete("profig", (String) null, (String[]) null);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE profig (k TEXT, v TEXT)");
        sQLiteDatabase.execSQL("CREATE TABLE apikey (package TEXT PRIMARY KEY, apikey TEXT, update_time REAL)");
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase != null) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS profig");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS apikey");
            onCreate(sQLiteDatabase);
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase != null) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS profig");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS apikey");
            onCreate(sQLiteDatabase);
        }
    }
}
