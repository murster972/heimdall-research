package io.presage.p033for;

import android.content.Context;
import com.mopub.common.TyphoonApp;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.actions.ChangKoehan;
import io.presage.actions.GoroDaimon;
import io.presage.p029char.ChoiBounge;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: io.presage.for.KyoKusanagi  reason: invalid package */
public class KyoKusanagi {
    protected JSONArray a;
    protected GoroDaimon b;
    protected Context c;
    protected ChoiBounge d;
    private GoroDaimon e;

    public KyoKusanagi(Context context, ChoiBounge choiBounge, JSONArray jSONArray, GoroDaimon goroDaimon) {
        this.a = jSONArray;
        this.b = goroDaimon;
        this.c = context;
        this.d = choiBounge;
    }

    public ChangKoehan a(String str) {
        if (str.equals("home")) {
            return a().a(this.c, this.d, "home", TyphoonApp.INTENT_SCHEME, new GoroDaimon(new JSONArray()));
        }
        int length = this.a.length();
        int i = 0;
        while (i < length) {
            try {
                JSONObject jSONObject = this.a.getJSONObject(i);
                if (jSONObject.getString("name").equals(str)) {
                    return a().a(this.c, this.d, (String) jSONObject.get("name"), (String) jSONObject.get(VastExtensionXmlManager.TYPE), this.b.a((JSONArray) jSONObject.get("params")));
                }
                i++;
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }

    public GoroDaimon a() {
        if (this.e == null) {
            this.e = GoroDaimon.a();
        }
        return this.e;
    }
}
