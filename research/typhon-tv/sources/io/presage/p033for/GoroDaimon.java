package io.presage.p033for;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: io.presage.for.GoroDaimon  reason: invalid package */
public class GoroDaimon {
    protected JSONArray a;

    public GoroDaimon() {
        this.a = new JSONArray();
    }

    public GoroDaimon(JSONArray jSONArray) {
        this.a = jSONArray;
    }

    /* renamed from: a */
    public GoroDaimon clone() {
        int length = this.a.length();
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < length; i++) {
            try {
                jSONArray.put((Object) this.a.getJSONObject(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new GoroDaimon(jSONArray);
    }

    public GoroDaimon a(GoroDaimon goroDaimon) {
        return a(goroDaimon.clone().a);
    }

    public GoroDaimon a(JSONArray jSONArray) {
        JSONObject jSONObject;
        boolean z;
        JSONArray jSONArray2 = new JSONArray();
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            try {
                jSONArray2.put((Object) jSONArray.getJSONObject(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int length2 = this.a.length();
        for (int i2 = 0; i2 < length2; i2++) {
            jSONObject = null;
            try {
                jSONObject = this.a.getJSONObject(i2);
                int length3 = jSONArray.length();
                int i3 = 0;
                while (true) {
                    if (i3 >= length3) {
                        break;
                    } else if (jSONArray.getJSONObject(i3).getString("name").equals(jSONObject.getString("name"))) {
                        z = true;
                        break;
                    } else {
                        i3++;
                    }
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            if (!z && jSONObject != null) {
                jSONArray2.put((Object) jSONObject);
            }
        }
        return new GoroDaimon(jSONArray2);
        z = false;
        jSONArray2.put((Object) jSONObject);
    }

    public Object a(String str) {
        int length = this.a.length();
        int i = 0;
        while (i < length) {
            try {
                JSONObject jSONObject = this.a.getJSONObject(i);
                if (jSONObject.getString("name").equals(str)) {
                    return jSONObject.get("value");
                }
                i++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void a(String str, Object obj) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", (Object) str);
            jSONObject.put("value", obj);
            this.a.put((Object) jSONObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String b(String str) {
        Object a2 = a(str);
        if (a2 != null) {
            return !(a2 instanceof String) ? String.valueOf(a2) : (String) a2;
        }
        return null;
    }
}
