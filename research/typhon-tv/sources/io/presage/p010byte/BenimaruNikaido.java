package io.presage.p010byte;

import android.view.View;
import android.webkit.JavascriptInterface;
import com.Pinkamena;
import com.p003if.p004do.ChoiBounge;
import io.presage.helper.Permissions;
import io.presage.model.Zone;
import io.presage.p034goto.Bao;
import io.presage.p034goto.Maxima;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p038long.ChinGentsai;
import io.presage.p038long.GoroDaimon;
import io.presage.p038long.KyoKusanagi;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

/* renamed from: io.presage.byte.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido {
    /* access modifiers changed from: private */
    public ChinGentsai a;
    /* access modifiers changed from: private */
    public Permissions b;

    public BenimaruNikaido(ChinGentsai chinGentsai, Permissions permissions) {
        this.a = chinGentsai;
        this.b = permissions;
    }

    @JavascriptInterface
    public void addManagedView(final String str) {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    Zone zone = (Zone) new ChoiBounge().m14090(str, Zone.class);
                    BenimaruNikaido.this.a.a(zone.isVideo() ? io.presage.p038long.BenimaruNikaido.a(BenimaruNikaido.this.a.getContext(), zone) : GoroDaimon.a(BenimaruNikaido.this.a.getContext(), BenimaruNikaido.this.b, zone));
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public void addProgressBar() {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    BenimaruNikaido.this.a.a();
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public boolean canGoBack(final String str) {
        try {
            return ((Boolean) Maxima.a(new Callable<Boolean>() {
                /* renamed from: a */
                public Boolean call() throws Exception {
                    KyoKusanagi a2 = BenimaruNikaido.this.a.a(str);
                    if (a2 == null || !(a2 instanceof GoroDaimon)) {
                        return false;
                    }
                    return Boolean.valueOf(((GoroDaimon) a2).canGoBack());
                }
            }).get()).booleanValue();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
            return false;
        }
    }

    @JavascriptInterface
    public boolean canGoForward(final String str) {
        try {
            return ((Boolean) Maxima.a(new Callable<Boolean>() {
                /* renamed from: a */
                public Boolean call() throws Exception {
                    KyoKusanagi a2 = BenimaruNikaido.this.a.a(str);
                    if (a2 == null || !(a2 instanceof GoroDaimon)) {
                        return false;
                    }
                    return Boolean.valueOf(((GoroDaimon) a2).canGoForward());
                }
            }).get()).booleanValue();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
            return false;
        }
    }

    @JavascriptInterface
    public void execute(final String str, final String str2) {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    KyoKusanagi a2 = BenimaruNikaido.this.a.a(str);
                    if (a2 == null || !(a2 instanceof GoroDaimon)) {
                        return null;
                    }
                    ((GoroDaimon) a2).a(str2);
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public int getDuration(String str) {
        KyoKusanagi a2 = this.a.a(str);
        if (a2 == null || !(a2 instanceof io.presage.p038long.BenimaruNikaido)) {
            return 0;
        }
        return ((io.presage.p038long.BenimaruNikaido) a2).getVideoController().h();
    }

    @JavascriptInterface
    public String getManagedViewNames() {
        return new ChoiBounge().m14078((Object) this.a.getManagedViewNames());
    }

    @JavascriptInterface
    public String getManagedWebViewNames() {
        return new ChoiBounge().m14078((Object) this.a.getManagedWebViewNames());
    }

    @JavascriptInterface
    public void goBack(final String str) {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    KyoKusanagi a2 = BenimaruNikaido.this.a.a(str);
                    if (a2 == null || !(a2 instanceof GoroDaimon)) {
                        return null;
                    }
                    ((GoroDaimon) a2).goBack();
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public void goForward(final String str) {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    KyoKusanagi a2 = BenimaruNikaido.this.a.a(str);
                    if (a2 == null || !(a2 instanceof GoroDaimon)) {
                        return null;
                    }
                    ((GoroDaimon) a2).goForward();
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public void loadUrl(final String str, final String str2) {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    KyoKusanagi a2 = BenimaruNikaido.this.a.a(str);
                    if (a2 == null || !(a2 instanceof GoroDaimon)) {
                        return null;
                    }
                    GoroDaimon goroDaimon = (GoroDaimon) a2;
                    String str = str2;
                    Pinkamena.DianePie();
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public void mute(String str) {
        KyoKusanagi a2 = this.a.a(str);
        if (a2 != null && (a2 instanceof io.presage.p038long.BenimaruNikaido)) {
            ((io.presage.p038long.BenimaruNikaido) a2).getVideoController().e();
        }
    }

    @JavascriptInterface
    public void pause(String str) {
        KyoKusanagi a2 = this.a.a(str);
        if (a2 != null && (a2 instanceof io.presage.p038long.BenimaruNikaido)) {
            ((io.presage.p038long.BenimaruNikaido) a2).getVideoController().b();
        }
    }

    @JavascriptInterface
    public void play(String str) {
        KyoKusanagi a2 = this.a.a(str);
        if (a2 != null && (a2 instanceof io.presage.p038long.BenimaruNikaido)) {
            ((io.presage.p038long.BenimaruNikaido) a2).getVideoController().c();
        }
    }

    @JavascriptInterface
    public void playPause(String str) {
        KyoKusanagi a2 = this.a.a(str);
        if (a2 != null && (a2 instanceof io.presage.p038long.BenimaruNikaido)) {
            ((io.presage.p038long.BenimaruNikaido) a2).getVideoController().d();
        }
    }

    @JavascriptInterface
    public void reload(final String str) {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    KyoKusanagi a2 = BenimaruNikaido.this.a.a(str);
                    if (a2 == null || !(a2 instanceof GoroDaimon)) {
                        return null;
                    }
                    ((GoroDaimon) a2).reload();
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public void removeManagedView(final String str) {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    BenimaruNikaido.this.a.b(str);
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public void removeProgressBar() {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    BenimaruNikaido.this.a.b();
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public void replay(String str) {
        KyoKusanagi a2 = this.a.a(str);
        if (a2 != null && (a2 instanceof io.presage.p038long.BenimaruNikaido)) {
            ((io.presage.p038long.BenimaruNikaido) a2).getVideoController().g();
        }
    }

    @JavascriptInterface
    public void send(String str, String str2) {
        send(str, str2, (String) null);
    }

    @JavascriptInterface
    public void send(final String str, final String str2, final String str3) {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    KyoKusanagi a2 = BenimaruNikaido.this.a.a(str);
                    if (a2 == null || !(a2 instanceof GoroDaimon)) {
                        return null;
                    }
                    ((GoroDaimon) a2).a(str2, str3);
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }

    @JavascriptInterface
    public void stop(String str) {
        KyoKusanagi a2 = this.a.a(str);
        if (a2 != null && (a2 instanceof io.presage.p038long.BenimaruNikaido)) {
            ((io.presage.p038long.BenimaruNikaido) a2).getVideoController().f();
        }
    }

    @JavascriptInterface
    public void updateManagedView(final String str, final String str2) {
        try {
            Maxima.a(new Callable<Void>() {
                /* renamed from: a */
                public Void call() throws Exception {
                    KyoKusanagi a2 = BenimaruNikaido.this.a.a(str);
                    if (a2 == null) {
                        return null;
                    }
                    Zone zone = (Zone) new ChoiBounge().m14090(str2, Zone.class);
                    ((View) a2).setLayoutParams(Bao.b(BenimaruNikaido.this.a.getContext(), zone));
                    Bao.a((View) a2, zone);
                    return null;
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            SaishuKusanagi.c("MultiViewLayoutJs", "An error occurs during the execution of a task on the UI thread");
        }
    }
}
