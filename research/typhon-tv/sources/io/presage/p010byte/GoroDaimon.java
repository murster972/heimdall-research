package io.presage.p010byte;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.os.EnvironmentCompat;
import android.webkit.JavascriptInterface;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import io.presage.actions.BrianBattler;
import io.presage.actions.LuckyGlauber;
import io.presage.actions.NewAction;
import io.presage.helper.ChinGentsai;
import io.presage.helper.Permissions;
import java.util.Locale;

/* renamed from: io.presage.byte.GoroDaimon  reason: invalid package */
public class GoroDaimon {
    private Context a;
    private Permissions b;

    public GoroDaimon(Context context, Permissions permissions) {
        this.a = context;
        this.b = permissions;
    }

    @JavascriptInterface
    public boolean checkPermission(int i) {
        return ChinGentsai.a(this.a, i);
    }

    @JavascriptInterface
    public void execute(String str, String str2) throws LuckyGlauber {
        BrianBattler.KyoKusanagi a2 = BrianBattler.a().a(str);
        if (a2 == null) {
            throw new IllegalArgumentException("Action " + str + " does not exist.");
        }
        ((NewAction) io.presage.p039new.LuckyGlauber.a(this.a, this.b).m14090(str2, a2.a())).execute();
    }

    @JavascriptInterface
    public void forceOrientation(String str) {
        int i = str.equals("landscape") ? 0 : str.equals("portrait") ? 1 : 1;
        this.a.getResources().getConfiguration().orientation = i;
        ((Activity) this.a).setRequestedOrientation(i);
    }

    @JavascriptInterface
    public String getLocale() {
        StringBuilder sb = new StringBuilder();
        try {
            Locale locale = this.a.getResources().getConfiguration().locale;
            String language = locale.getLanguage();
            if (language.length() == 2) {
                sb.append(language);
                String country = locale.getCountry();
                if (country.length() == 2) {
                    sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb.append(country);
                }
            } else {
                sb.append("en");
            }
        } catch (Exception e) {
            if (sb.length() == 0) {
                sb.append("en");
            }
        }
        return sb.toString();
    }

    @JavascriptInterface
    public String getOrientation() {
        switch (this.a.getResources().getConfiguration().orientation) {
            case 1:
                return "portrait";
            case 2:
                return "landscape";
            default:
                return EnvironmentCompat.MEDIA_UNKNOWN;
        }
    }

    @JavascriptInterface
    public String getVersion() {
        return "2.2.11-moat";
    }

    @JavascriptInterface
    public boolean resolveIntent(String str) {
        PackageManager packageManager = this.a.getPackageManager();
        if (str == null || packageManager == null) {
            return false;
        }
        try {
            return packageManager.resolveActivity(Intent.parseUri(str, 0), 65536) != null;
        } catch (Exception e) {
            return false;
        }
    }
}
