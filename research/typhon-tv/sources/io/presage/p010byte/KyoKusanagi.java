package io.presage.p010byte;

import android.support.v4.app.NotificationCompat;
import android.webkit.JavascriptInterface;
import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Vice;
import io.presage.ads.NewAd;
import io.presage.p029char.ChangKoehan;
import io.presage.p029char.ChinGentsai;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import net.pubnative.library.request.model.api.PubnativeAPIV3ResponseModel;
import org.json.JSONException;
import org.json.JSONObject;
import p006do.ShingoYabuki;

/* renamed from: io.presage.byte.KyoKusanagi  reason: invalid package */
public class KyoKusanagi {
    private NewAd a;

    /* renamed from: io.presage.byte.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    private static class C0041KyoKusanagi implements Callable<String> {
        private String a;

        public C0041KyoKusanagi(String str) {
            this.a = str;
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.io.BufferedReader} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: java.lang.String} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String call() throws java.lang.Exception {
            /*
                r7 = this;
                r1 = 0
                java.lang.String r0 = ""
                java.net.URL r3 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0032 }
                java.lang.String r2 = r7.a     // Catch:{ MalformedURLException -> 0x0032 }
                r3.<init>(r2)     // Catch:{ MalformedURLException -> 0x0032 }
                java.lang.String r2 = r3.getProtocol()     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                java.lang.String r4 = "file"
                boolean r2 = r2.equals(r4)     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                if (r2 == 0) goto L_0x0035
                java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                java.io.FileReader r4 = new java.io.FileReader     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                java.lang.String r3 = r3.getPath()     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                r4.<init>(r3)     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                r2.<init>(r4)     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                r1 = r2
            L_0x0027:
                java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                if (r2 == 0) goto L_0x006e
                java.lang.String r0 = r0.concat(r2)     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                goto L_0x0027
            L_0x0032:
                r0 = move-exception
                r0 = r1
            L_0x0034:
                return r0
            L_0x0035:
                do.ShingoYabuki$KyoKusanagi r2 = new do.ShingoYabuki$KyoKusanagi     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                r2.<init>()     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                java.lang.String r3 = "Content-Type"
                java.lang.String r4 = "text/xml"
                do.ShingoYabuki$KyoKusanagi r2 = r2.m18385(r3, r4)     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                do.ShingoYabuki r2 = r2.m18386()     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                io.presage.char.ChangKoehan r3 = io.presage.p029char.ChangKoehan.a()     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                io.presage.char.ChoiBounge r3 = r3.k()     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                java.lang.String r4 = r7.a     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                r5 = 0
                java.lang.String r6 = ""
                do.JhunHoon r3 = r3.a(r4, r2, r5, r6)     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                do.Krizalid r3 = r3.m6463()     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                java.io.InputStream r3 = r3.m6497()     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                r4.<init>(r3)     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                r2.<init>(r4)     // Catch:{ Exception -> 0x0076, all -> 0x007f }
                r1 = r2
                goto L_0x0027
            L_0x006e:
                if (r1 == 0) goto L_0x0034
                r1.close()     // Catch:{ Exception -> 0x0074 }
                goto L_0x0034
            L_0x0074:
                r1 = move-exception
                goto L_0x0034
            L_0x0076:
                r2 = move-exception
                if (r1 == 0) goto L_0x0034
                r1.close()     // Catch:{ Exception -> 0x007d }
                goto L_0x0034
            L_0x007d:
                r1 = move-exception
                goto L_0x0034
            L_0x007f:
                r0 = move-exception
                if (r1 == 0) goto L_0x0085
                r1.close()     // Catch:{ Exception -> 0x0086 }
            L_0x0085:
                throw r0
            L_0x0086:
                r1 = move-exception
                goto L_0x0085
            */
            throw new UnsupportedOperationException("Method not decompiled: io.presage.p010byte.KyoKusanagi.C0041KyoKusanagi.call():java.lang.String");
        }
    }

    public KyoKusanagi(NewAd newAd) {
        this.a = newAd;
    }

    @JavascriptInterface
    public void execute(String str) {
        this.a.onExecuteAction(str);
    }

    @JavascriptInterface
    public String get(String str) {
        return new ChoiBounge().m14078((Object) this.a.getOverridedParameter(str));
    }

    @JavascriptInterface
    public String getAd() {
        return this.a.getJsonAd();
    }

    @JavascriptInterface
    public String getAdvertiserId() {
        return this.a.getAdvertiser().getId();
    }

    @JavascriptInterface
    public String getAdvertiserName() {
        return this.a.getAdvertiser().getName();
    }

    @JavascriptInterface
    public String getCampaignId() {
        return this.a.getCampaignId();
    }

    @JavascriptInterface
    public String getClientTrackerPattern() {
        return this.a.getClientTrackerPattern();
    }

    @JavascriptInterface
    public String getId() {
        return this.a.getId();
    }

    @JavascriptInterface
    public String getLinkUrl() {
        return this.a.getLinkUrl();
    }

    @JavascriptInterface
    public String getUrl(String str, long j) {
        Vice vice = new Vice();
        FutureTask futureTask = new FutureTask(new C0041KyoKusanagi(str));
        futureTask.run();
        try {
            vice.m14171(NotificationCompat.CATEGORY_STATUS, PubnativeAPIV3ResponseModel.Status.OK);
            vice.m14171("value", (String) futureTask.get(j, TimeUnit.MILLISECONDS));
        } catch (InterruptedException e) {
            vice.m14171(NotificationCompat.CATEGORY_STATUS, "error");
            vice.m14171("value", e.toString());
            e.printStackTrace();
        } catch (ExecutionException e2) {
            vice.m14171(NotificationCompat.CATEGORY_STATUS, "error");
            vice.m14171("value", e2.toString());
            e2.printStackTrace();
        } catch (TimeoutException e3) {
            vice.m14171(NotificationCompat.CATEGORY_STATUS, "error");
            vice.m14171("value", e3.toString());
            e3.printStackTrace();
        }
        return vice.toString();
    }

    @JavascriptInterface
    public void onFormatError(String str, String str2) {
        this.a.onFormatError(str, str2);
    }

    @JavascriptInterface
    public void onFormatEvent(String str) {
        this.a.onFormatEvent(str);
    }

    @JavascriptInterface
    public String sendVideoTrack(String str) {
        Vice vice = new Vice();
        JSONObject jSONObject = new JSONObject();
        JSONObject a2 = ChangKoehan.a().k().b().a();
        try {
            jSONObject.put(NotificationCompat.CATEGORY_EVENT, (Object) str);
            jSONObject.put("campaign_id", (Object) this.a.getCampaignId());
            jSONObject.put("advert_id", (Object) this.a.getId());
            jSONObject.put("advertiser_id", (Object) this.a.getAdvertiser().getId());
            jSONObject.put("id", (Object) this.a.getAdUnitId());
            a2.put("content", (Object) jSONObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ShingoYabuki b = ChangKoehan.a().k().b().b();
        ChangKoehan.a().k().a(ChangKoehan.a().o());
        ChangKoehan.a().k().a(ChangKoehan.a().k().d("videotracking"), b, 1, a2.toString(), (ChinGentsai) null, (io.presage.p029char.p030do.KyoKusanagi) null);
        vice.m14171(NotificationCompat.CATEGORY_STATUS, PubnativeAPIV3ResponseModel.Status.OK);
        vice.m14171("value", "");
        return vice.toString();
    }
}
