package io.presage.actions;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import io.presage.helper.Permissions;

public class NewOpenBrowser extends NewAction {
    private String c;

    public NewOpenBrowser(Context context, Permissions permissions, String str) {
        super(context, permissions);
        this.c = str;
    }

    public String execute() throws LuckyGlauber {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(this.c));
        intent.setFlags(268435456);
        this.a.startActivity(intent);
        return null;
    }
}
