package io.presage.actions;

import android.content.Context;
import io.presage.helper.ChinGentsai;
import io.presage.helper.Permissions;
import io.presage.p034goto.KDash;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Vice;
import java.io.IOException;

public class NewFingerAccess extends NewAction {
    private String c;
    private String d;
    private String e;
    private String f;

    public NewFingerAccess(Context context, Permissions permissions, String str, String str2, String str3, String str4) {
        super(context, permissions);
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
    }

    public String execute() throws LuckyGlauber {
        if (!ChinGentsai.b(this.a, "com.android.launcher.permission.INSTALL_SHORTCUT") || !ChinGentsai.b(this.a, "com.android.launcher.permission.UNINSTALL_SHORTCUT")) {
            SaishuKusanagi.c("NewFingerAccess", "The application does not have install or uninstall shortcut permissions.");
        } else {
            Vice vice = new Vice(this.a, "set_finger_access");
            if (vice.contains(this.c)) {
                KDash.a(this.a, this.f, this.d);
                vice.remove(this.c);
            }
            vice.add(this.c);
            try {
                KDash.a(this.a, this.f, this.d, this.e);
            } catch (IOException e2) {
                throw new LuckyGlauber(e2.getMessage());
            }
        }
        return null;
    }
}
