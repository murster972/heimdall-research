package io.presage.actions;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import io.presage.ads.NewAd;
import io.presage.p029char.ChoiBounge;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.SaishuKusanagi;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class IoriYagami extends KyoKusanagi {
    public IoriYagami(Context context, ChoiBounge choiBounge, String str, String str2, GoroDaimon goroDaimon) {
        super(context, choiBounge, str, str2, goroDaimon);
    }

    public String l() {
        Intent intent = new Intent("presage_request");
        intent.putExtra("bundle", b().b("bundle"));
        intent.putExtra("app_launch_auto", b().b("app_launch_auto"));
        intent.putExtra("app_launch_rate", b().b("app_launch_rate"));
        intent.putExtra("app_launch_package", b().b("app_launch_package"));
        intent.putExtra("app_launch_class", b().b("app_launch_class"));
        intent.putExtra("method", 1);
        intent.putExtra("url", j().d("track"));
        List<String> a = j().a();
        intent.putExtra("headers", (String[]) a.toArray(new String[a.size()]));
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(NotificationCompat.CATEGORY_EVENT, (Object) NewAd.EVENT_INSTALL);
            jSONObject.put("campaign", (Object) d());
            jSONObject.put("advertiser", (Object) c());
            jSONObject.put("advert", (Object) e());
            jSONObject.put("ad_unit_id", (Object) f());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        intent.putExtra(TtmlNode.TAG_BODY, jSONObject.toString());
        SaishuKusanagi.b("PendingInstall", String.format("%s %s %s", new Object[]{"PendingInstall", NewAd.EVENT_INSTALL, d()}));
        i().sendOrderedBroadcast(intent, (String) null);
        n();
        return null;
    }
}
