package io.presage.actions;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import io.presage.helper.Permissions;
import io.presage.p034goto.SaishuKusanagi;

public class StartIntentFromUri extends NewAction {
    private Intent c;

    @TargetApi(4)
    public StartIntentFromUri(Context context, Permissions permissions, String str) {
        super(context, permissions);
        try {
            this.c = Intent.parseUri(str, 0);
        } catch (Exception e) {
            SaishuKusanagi.c("StartIntentFromUri", "Could not parse the intent of the StartIntentFromUri action");
        }
    }

    public String execute() {
        if (this.c == null) {
            return null;
        }
        this.c.setFlags(268435456);
        this.a.startActivity(this.c);
        return null;
    }
}
