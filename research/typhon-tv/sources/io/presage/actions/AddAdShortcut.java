package io.presage.actions;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.p003if.p004do.ChoiBounge;
import io.presage.activities.PresageActivity;
import io.presage.ads.NewAd;
import io.presage.helper.Permissions;
import io.presage.p029char.ChangKoehan;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.KDash;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Vice;
import java.io.IOException;
import net.pubnative.library.request.PubnativeAsset;

public class AddAdShortcut extends NewAction implements NewAd.GoroDaimon {
    private NewAd c;
    private Context d;

    public AddAdShortcut(Context context, Permissions permissions, NewAd newAd) {
        super(context, permissions);
        this.c = newAd;
        this.d = context;
        newAd.setOnFormatEventListener(this);
    }

    public String execute() throws LuckyGlauber {
        String str = (String) this.c.getOverridedParameterValue("identifier", String.class);
        if (str == null) {
            SaishuKusanagi.c("AddAdShortcut", "Icon not installed. No identifier provided.");
        } else {
            String str2 = (String) this.c.getOverridedParameterValue(PubnativeAsset.ICON, String.class);
            if (str2 == null) {
                SaishuKusanagi.c("AddAdShortcut", "Icon not installed. No icon image provided.");
            } else {
                String str3 = (String) this.c.getOverridedParameterValue("icon_name", String.class);
                if (str3 == null) {
                    SaishuKusanagi.c("AddAdShortcut", "Icon not installed. No icon name provided.");
                } else {
                    Vice vice = new Vice(this.a, "set_shortcut");
                    if (vice.contains(str)) {
                        KDash.a(this.a, (Class<? extends Activity>) PresageActivity.class, str3);
                        vice.remove(str);
                    }
                    vice.add(str);
                    Bundle bundle = new Bundle();
                    bundle.putString("activity_handler", "add_shortcut_action");
                    bundle.putString("ad", new ChoiBounge().m14078((Object) this.c));
                    try {
                        KDash.a(this.a, PresageActivity.class, str3, str2, bundle);
                        this.c.onFormatEvent(NewAd.EVENT_SHORTCUT);
                    } catch (IOException e) {
                        throw new LuckyGlauber(e.getMessage());
                    }
                }
            }
        }
        return null;
    }

    public void onFormatEvent(String str) {
        if (str.equals(NewAd.EVENT_SHORTCUT)) {
            GoroDaimon goroDaimon = new GoroDaimon();
            goroDaimon.a(VastExtensionXmlManager.TYPE, NewAd.EVENT_SHORTCUT);
            if (this.c != null && this.d != null && ChangKoehan.a().m() && ChangKoehan.a().k() != null) {
                KyoKusanagi a = GoroDaimon.a().a(this.d, ChangKoehan.a().k(), "send_ad_event", "send_ad_event", goroDaimon);
                a.a(this.c.getAdvertiser().getId(), this.c.getCampaignId(), this.c.getId(), this.c.getAdUnitId());
                a.k();
            }
        }
    }
}
