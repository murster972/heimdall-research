package io.presage.actions.p025do;

import android.content.Context;
import com.mopub.common.TyphoonApp;
import io.presage.actions.BenimaruNikaido;
import io.presage.actions.ChangKoehan;
import io.presage.actions.GoroDaimon;
import io.presage.p029char.ChoiBounge;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;

/* renamed from: io.presage.actions.do.KyoKusanagi  reason: invalid package */
public class KyoKusanagi extends Thread {
    private ArrayList<ChangKoehan> a;
    /* access modifiers changed from: private */
    public boolean b;
    private BenimaruNikaido c;
    private Context d;
    private ChoiBounge e;
    private Timer f;
    private TimerTask g = new TimerTask() {
        public void run() {
            boolean unused = KyoKusanagi.this.b = true;
            KyoKusanagi.this.run();
        }
    };

    public KyoKusanagi(Context context, ChoiBounge choiBounge) {
        this.d = context;
        this.e = choiBounge;
        this.a = new ArrayList<>();
        this.b = false;
        this.f = null;
        this.c = new BenimaruNikaido();
    }

    public void a(ChangKoehan changKoehan) {
        if (!this.b) {
            if (this.f == null) {
                this.f = new Timer();
                this.f.schedule(this.g, 150);
            }
            this.a.add(changKoehan);
        }
    }

    public void run() {
        if (!this.a.contains("home") && this.a.contains("close")) {
            this.c.a(GoroDaimon.a().a(this.d, this.e, "home", TyphoonApp.INTENT_SCHEME, new io.presage.p033for.GoroDaimon(new JSONArray())));
        }
        Iterator<ChangKoehan> it2 = this.a.iterator();
        while (it2.hasNext()) {
            this.c.a(it2.next());
        }
        this.c.a();
    }
}
