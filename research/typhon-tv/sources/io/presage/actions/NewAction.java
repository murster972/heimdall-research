package io.presage.actions;

import android.content.Context;
import io.presage.helper.Permissions;

public abstract class NewAction {
    protected Context a;
    protected Permissions b;

    public NewAction(Context context, Permissions permissions) {
        this.a = context;
        this.b = permissions;
    }

    public abstract String execute() throws LuckyGlauber;
}
