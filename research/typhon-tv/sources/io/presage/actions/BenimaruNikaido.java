package io.presage.actions;

import java.util.Iterator;
import java.util.LinkedList;

public class BenimaruNikaido {
    private LinkedList<ChangKoehan> a = new LinkedList<>();
    private boolean b = false;

    public void a() {
        if (!this.b) {
            Iterator it2 = this.a.iterator();
            while (it2.hasNext()) {
                ChangKoehan changKoehan = (ChangKoehan) it2.next();
                if (changKoehan.a().equals("home")) {
                    changKoehan.k();
                    this.b = true;
                    return;
                }
            }
            if (!this.a.isEmpty()) {
                this.a.getFirst().k();
                this.b = true;
            }
        }
    }

    public void a(ChangKoehan changKoehan) {
        changKoehan.a(this);
        this.a.add(changKoehan);
    }

    public void b(ChangKoehan changKoehan) {
        this.a.remove(changKoehan);
        if (this.b && !this.a.isEmpty()) {
            this.a.getFirst().k();
        }
    }
}
