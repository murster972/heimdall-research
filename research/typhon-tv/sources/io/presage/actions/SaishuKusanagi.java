package io.presage.actions;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.p029char.ChangKoehan;
import io.presage.p029char.ChinGentsai;
import io.presage.p029char.ChoiBounge;
import io.presage.p029char.p030do.KyoKusanagi;
import io.presage.p033for.GoroDaimon;
import org.json.JSONObject;
import p006do.ShingoYabuki;

public class SaishuKusanagi extends KyoKusanagi {
    public static final String a = ChangKoehan.class.getSimpleName();
    private String b;

    public SaishuKusanagi(Context context, ChoiBounge choiBounge, String str, String str2, GoroDaimon goroDaimon) {
        super(context, choiBounge, str, str2, goroDaimon);
    }

    public void a(String str) {
        this.b = str;
    }

    public String l() {
        JSONObject jSONObject = new JSONObject();
        ChoiBounge k = ChangKoehan.a().k();
        if (k == null) {
            n();
        } else {
            JSONObject a2 = k.b().a();
            try {
                jSONObject.put(NotificationCompat.CATEGORY_EVENT, (Object) b().b(VastExtensionXmlManager.TYPE));
                jSONObject.put("campaign", (Object) d());
                jSONObject.put("advertiser", (Object) c());
                jSONObject.put("advert", (Object) e());
                jSONObject.put("ad_unit_id", (Object) f());
                if (h() != null) {
                    jSONObject.put("error_type", (Object) h());
                    jSONObject.put("error_message", (Object) g());
                }
                if (this.b != null) {
                    jSONObject.put("completion", (Object) this.b);
                }
                a2.put("content", (Object) jSONObject);
                ShingoYabuki b2 = ChangKoehan.a().k().b().b();
                j().a(ChangKoehan.a().o());
                j().a(j().d("track"), b2, 1, a2.toString(), (ChinGentsai) null, (KyoKusanagi) null);
                io.presage.p034goto.SaishuKusanagi.b("SendAdEvent", String.format("%s %s", new Object[]{"SendAdEvent", b().b(VastExtensionXmlManager.TYPE)}));
                n();
            } catch (Exception e) {
                io.presage.p034goto.SaishuKusanagi.b(a, e.getMessage(), e);
                n();
            }
        }
        return null;
    }
}
