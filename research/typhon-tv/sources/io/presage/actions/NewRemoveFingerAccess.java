package io.presage.actions;

import android.content.Context;
import io.presage.helper.ChinGentsai;
import io.presage.helper.Permissions;
import io.presage.p034goto.KDash;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Vice;

public class NewRemoveFingerAccess extends NewAction {
    private String c;
    private String d;
    private String e;

    public NewRemoveFingerAccess(Context context, Permissions permissions, String str, String str2, String str3) {
        super(context, permissions);
        this.c = str;
        this.d = str2;
        this.e = str3;
    }

    public String execute() throws LuckyGlauber {
        if (!ChinGentsai.b(this.a, "com.android.launcher.permission.UNINSTALL_SHORTCUT")) {
            SaishuKusanagi.c("NewRemoveFingerAccess", "The application does not have the uninstall shortcut permissions.");
        } else {
            Vice vice = new Vice(this.a, "set_finger_access");
            if (vice.contains(this.c)) {
                KDash.a(this.a, this.e, this.d);
                vice.remove(this.c);
            } else {
                SaishuKusanagi.c("NewRemoveFingerAccess", "Unable to remove an icon shortcut. Shortcut not installed.");
            }
        }
        return null;
    }
}
