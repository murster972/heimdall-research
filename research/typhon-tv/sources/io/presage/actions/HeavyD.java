package io.presage.actions;

import android.content.Context;
import android.content.Intent;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.p029char.ChangKoehan;
import io.presage.p029char.ChinGentsai;
import io.presage.p029char.ChoiBounge;
import io.presage.p029char.p030do.KyoKusanagi;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.SaishuKusanagi;
import org.json.JSONObject;
import p006do.ShingoYabuki;

public class HeavyD extends KyoKusanagi {
    public static final String a = ChangKoehan.class.getSimpleName();

    public HeavyD(Context context, ChoiBounge choiBounge, String str, String str2, GoroDaimon goroDaimon) {
        super(context, choiBounge, str, str2, goroDaimon);
    }

    public String l() {
        String b = b().b(VastExtensionXmlManager.TYPE);
        Object a2 = b().a("value");
        JSONObject a3 = j().b().a();
        JSONObject jSONObject = new JSONObject();
        try {
            String obj = a2.toString();
            if (obj == null) {
                throw new NullPointerException("valueToString is NULL");
            }
            jSONObject.put(b, (Object) obj);
            a3.put("content", (Object) jSONObject);
            ShingoYabuki b2 = j().b().b();
            j().a(ChangKoehan.a().o());
            j().a(j().d("presage"), b2, 1, a3.toString(), new ChinGentsai() {
                public void a(int i, String str) {
                }

                public void a(String str) {
                    HeavyD.this.i().sendBroadcast(new Intent("io.presage.receiver.action.UPDATE_PROFIG"));
                }
            }, (KyoKusanagi) null);
            SaishuKusanagi.b("ManagePresage", String.format("%s %s %s", new Object[]{"ManagePresage", b, a2}));
            n();
            return null;
        } catch (Exception e) {
            SaishuKusanagi.b(a, e.getMessage(), e);
            n();
        }
    }
}
