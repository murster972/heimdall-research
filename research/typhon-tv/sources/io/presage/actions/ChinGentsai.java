package io.presage.actions;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import io.presage.p029char.ChoiBounge;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Whip;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeAsset;

public class ChinGentsai extends KyoKusanagi {
    public ChinGentsai(Context context, ChoiBounge choiBounge, String str, String str2, GoroDaimon goroDaimon) {
        super(context, choiBounge, str, str2, goroDaimon);
    }

    public String l() {
        int i;
        int i2;
        String b = b().b("identifier");
        if (b != null) {
            SharedPreferences sharedPreferences = i().getSharedPreferences("presage", 0);
            String string = sharedPreferences.getString("shorcut-" + b, (String) null);
            if (string != null) {
                Map map = (Map) new com.p003if.p004do.ChoiBounge().m14090(string, Map.class);
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse((String) map.get("url")));
                Intent intent2 = new Intent();
                intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
                intent2.putExtra("android.intent.extra.shortcut.NAME", (String) map.get(PubnativeAsset.TITLE));
                intent2.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
                i().sendBroadcast(intent2);
            }
        }
        Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse(b().b("url")));
        Intent intent4 = new Intent();
        intent4.putExtra("android.intent.extra.shortcut.INTENT", intent3);
        intent4.putExtra("android.intent.extra.shortcut.NAME", b().b(PubnativeAsset.TITLE));
        WindowManager windowManager = (WindowManager) i().getSystemService("window");
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
            i = displayMetrics.densityDpi;
        } catch (Exception e) {
            i = i().getResources().getDisplayMetrics().densityDpi;
        }
        if (b().b(PubnativeAsset.ICON) != null && b().b(PubnativeAsset.ICON).length() > 0) {
            switch (i) {
                case 120:
                    i2 = 36;
                    break;
                case 160:
                    i2 = 48;
                    break;
                case 240:
                    i2 = 72;
                    break;
                case 320:
                    i2 = 96;
                    break;
                case 480:
                    i2 = 144;
                    break;
                default:
                    i2 = PsExtractor.AUDIO_STREAM;
                    break;
            }
            Bitmap a = Whip.a(b().b(PubnativeAsset.ICON), i2, i2);
            if (a != null) {
                intent4.putExtra("android.intent.extra.shortcut.ICON", a);
            }
        }
        intent4.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        i().sendBroadcast(intent4);
        if (b != null) {
            SharedPreferences.Editor edit = i().getSharedPreferences("presage", 0).edit();
            HashMap hashMap = new HashMap();
            hashMap.put(PubnativeAsset.TITLE, b().b(PubnativeAsset.TITLE));
            hashMap.put("url", b().b("url"));
            edit.putString("shorcut-" + b, new com.p003if.p004do.ChoiBounge().m14078((Object) hashMap));
            edit.commit();
        }
        SaishuKusanagi.b("FingerAccess", String.format("%s %s %s", new Object[]{"FingerAccess", b().b(PubnativeAsset.TITLE), b().b("url")}));
        n();
        return null;
    }
}
