package io.presage.actions;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.SaishuKusanagi;
import java.util.HashMap;
import java.util.Map;

public class ChoiBounge extends KyoKusanagi {
    public ChoiBounge(Context context, io.presage.p029char.ChoiBounge choiBounge, String str, String str2, GoroDaimon goroDaimon) {
        super(context, choiBounge, str, str2, goroDaimon);
    }

    public String l() {
        if (a().equals("home")) {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            intent.setFlags(268435456);
            i().startActivity(intent);
            a(500);
        } else {
            HashMap hashMap = (HashMap) new com.p003if.p004do.ChoiBounge().m14090(b().b("extra"), Map.class);
            String b = b().b("flag");
            String b2 = b().b("category");
            String b3 = b().b("data");
            String b4 = b().b("action");
            String b5 = b().b(VastExtensionXmlManager.TYPE);
            String b6 = b().b("component_pkg");
            String b7 = b().b("component_cls");
            Intent intent2 = (b5 == null || b5.length() <= 0) ? null : new Intent(b5);
            if (intent2 != null) {
                if (b3 != null && b3.length() > 0) {
                    intent2.setData(Uri.parse(b3));
                }
                if (b4 != null && b4.length() > 0) {
                    intent2.setAction(b4);
                }
                if (b != null && b.length() > 0) {
                    for (String trim : b.split(",")) {
                        intent2.addFlags(Integer.parseInt(trim.trim()));
                    }
                }
                if (hashMap != null) {
                    for (String str : hashMap.keySet()) {
                        intent2.putExtra(str, (String) hashMap.get(str));
                    }
                }
                if (b2 != null && b2.length() > 0) {
                    for (String trim2 : b2.split(",")) {
                        intent2.addCategory(trim2.trim());
                    }
                }
                if (b6 != null && b6.length() > 0 && b7 != null && b7.length() > 0) {
                    intent2.setComponent(new ComponentName(b6, b7));
                }
                try {
                    i().startActivity(intent2);
                } catch (ActivityNotFoundException e) {
                    SaishuKusanagi.c("IntentStart", "IntentStart action failed: ActivityNotFoundException");
                }
            }
            n();
        }
        return null;
    }
}
