package io.presage.actions;

import android.content.Context;
import com.mopub.common.TyphoonApp;
import io.presage.p029char.ChoiBounge;

public class GoroDaimon {
    private static GoroDaimon a = null;

    public static GoroDaimon a() {
        if (a == null) {
            a = new GoroDaimon();
        }
        return a;
    }

    public KyoKusanagi a(Context context, ChoiBounge choiBounge, String str, String str2, io.presage.p033for.GoroDaimon goroDaimon) {
        if (str2.equals("manage_presage")) {
            return new HeavyD(context, choiBounge, str, str2, goroDaimon);
        }
        if (str2.equals("send_ad_event")) {
            return new SaishuKusanagi(context, choiBounge, str, str2, goroDaimon);
        }
        if (str2.equals("pend_ad_event_install")) {
            return new IoriYagami(context, choiBounge, str, str2, goroDaimon);
        }
        if (str2.equals("open_browser")) {
            return new RugalBernstein(context, choiBounge, str, str2, goroDaimon);
        }
        if (str2.equals(TyphoonApp.INTENT_SCHEME)) {
            return new ChoiBounge(context, choiBounge, str, str2, goroDaimon);
        }
        if (str2.equals("home")) {
            return new ChoiBounge(context, choiBounge, "home", TyphoonApp.INTENT_SCHEME, goroDaimon);
        }
        if (str2.equals("finger_access")) {
            return new ChinGentsai(context, choiBounge, str, str2, goroDaimon);
        }
        return null;
    }
}
