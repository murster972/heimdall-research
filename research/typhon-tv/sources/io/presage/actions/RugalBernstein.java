package io.presage.actions;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import io.presage.p029char.ChoiBounge;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.SaishuKusanagi;

public class RugalBernstein extends KyoKusanagi {
    public RugalBernstein(Context context, ChoiBounge choiBounge, String str, String str2, GoroDaimon goroDaimon) {
        super(context, choiBounge, str, str2, goroDaimon);
    }

    public void e(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(b().b("url")));
        intent.setFlags(268435456);
        i().startActivity(intent);
        SaishuKusanagi.b("OpenBrowser", String.format("%s %s", new Object[]{"OpenBrowser", b().b("url")}));
        n();
    }

    public String l() {
        return null;
    }
}
