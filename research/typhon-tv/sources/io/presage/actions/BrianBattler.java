package io.presage.actions;

import java.util.HashMap;
import java.util.Map;

public class BrianBattler {
    private static final Map<String, Class<? extends NewAction>> a = new HashMap<String, Class<? extends NewAction>>() {
        {
            put("add_shortcut", AddAdShortcut.class);
            put("remove_shortcut", RemoveAdShortcut.class);
            put("intent_start", StartIntentFromUri.class);
            put("add_finger_access", NewFingerAccess.class);
            put("remove_finger_access", NewRemoveFingerAccess.class);
            put("open_browser", NewOpenBrowser.class);
        }
    };
    private static BrianBattler b;

    public static class KyoKusanagi {
        private Class<? extends NewAction> a;

        private KyoKusanagi(Class<? extends NewAction> cls) {
            this.a = cls;
        }

        public Class<? extends NewAction> a() {
            return this.a;
        }
    }

    private BrianBattler() {
    }

    public static BrianBattler a() {
        if (b == null) {
            b = new BrianBattler();
        }
        return b;
    }

    public KyoKusanagi a(String str) {
        if (!a.containsKey(str)) {
            return null;
        }
        return new KyoKusanagi(a.get(str));
    }

    public Class<? extends NewAction> b(String str) {
        if (!a.containsKey(str)) {
            return null;
        }
        return a.get(str);
    }
}
