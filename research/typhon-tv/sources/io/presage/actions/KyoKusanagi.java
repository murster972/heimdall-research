package io.presage.actions;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import io.presage.p029char.ChoiBounge;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.ChizuruKagura;
import java.util.Timer;
import java.util.TimerTask;

public abstract class KyoKusanagi implements ChangKoehan {
    private String a;
    private String b;
    private GoroDaimon c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private Context j;
    private BenimaruNikaido k;
    private ChoiBounge l;
    private Timer m;
    private TimerTask n;

    @TargetApi(3)
    /* renamed from: io.presage.actions.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    class C0038KyoKusanagi extends AsyncTask<String, Void, String> {
        private KyoKusanagi b;

        public C0038KyoKusanagi(KyoKusanagi kyoKusanagi) {
            this.b = kyoKusanagi;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String doInBackground(String... strArr) {
            return this.b.l();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(String str) {
            this.b.e(str);
        }
    }

    public KyoKusanagi(Context context, ChoiBounge choiBounge, String str, String str2, GoroDaimon goroDaimon) {
        this.a = str;
        this.b = str2;
        this.c = goroDaimon;
        this.j = context;
        a(choiBounge);
    }

    public String a() {
        return this.a;
    }

    public void a(long j2) {
        if (m() != null) {
            this.m = new Timer();
            this.n = new TimerTask() {
                public void run() {
                    KyoKusanagi.this.m().b(KyoKusanagi.this);
                }
            };
            this.m.schedule(this.n, j2);
        }
    }

    public void a(BenimaruNikaido benimaruNikaido) {
        this.k = benimaruNikaido;
    }

    public void a(ChoiBounge choiBounge) {
        this.l = choiBounge;
    }

    public void a(String str) {
    }

    public void a(String str, String str2) {
        this.i = str2;
        this.h = str;
    }

    public void a(String str, String str2, String str3, String str4) {
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = str4;
    }

    public GoroDaimon b() {
        return this.c;
    }

    public void b(String str) {
        this.e = str;
    }

    public String c() {
        return this.d;
    }

    public void c(String str) {
        this.f = str;
    }

    public String d() {
        return this.e;
    }

    public void d(String str) {
        this.g = str;
    }

    public String e() {
        return this.f;
    }

    public void e(String str) {
    }

    public String f() {
        return this.g;
    }

    public String g() {
        return this.i;
    }

    public String h() {
        return this.h;
    }

    public Context i() {
        return this.j;
    }

    public ChoiBounge j() {
        return this.l;
    }

    @TargetApi(11)
    public void k() {
        new C0038KyoKusanagi(this).executeOnExecutor(ChizuruKagura.a(), new String[0]);
    }

    public abstract String l();

    public BenimaruNikaido m() {
        return this.k;
    }

    public void n() {
        if (this.k != null) {
            this.k.b(this);
        }
    }
}
