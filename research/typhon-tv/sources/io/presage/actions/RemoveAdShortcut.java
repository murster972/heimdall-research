package io.presage.actions;

import android.app.Activity;
import android.content.Context;
import io.presage.activities.PresageActivity;
import io.presage.helper.ChinGentsai;
import io.presage.helper.Permissions;
import io.presage.p034goto.KDash;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Vice;

public class RemoveAdShortcut extends NewAction {
    private String c;
    private String d;

    public RemoveAdShortcut(Context context, Permissions permissions, String str, String str2) {
        super(context, permissions);
        this.c = str;
        this.d = str2;
    }

    public String execute() throws LuckyGlauber {
        if (!ChinGentsai.b(this.a, "com.android.launcher.permission.UNINSTALL_SHORTCUT")) {
            SaishuKusanagi.c("RemoveAdShortcut", "The application does not have uninstall shortcut permissions.");
        } else {
            Vice vice = new Vice(this.a, "set_shortcut");
            if (vice.contains(this.c)) {
                KDash.a(this.a, (Class<? extends Activity>) PresageActivity.class, this.d);
                vice.remove(this.c);
            } else {
                SaishuKusanagi.b("RemoveAdShortcut", "Unable to remove an icon shortcut. Shortcut not installed.");
            }
        }
        return null;
    }
}
