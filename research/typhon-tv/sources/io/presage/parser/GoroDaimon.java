package io.presage.parser;

import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.parser.p040do.KyoKusanagi;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoroDaimon extends KyoKusanagi {
    public GoroDaimon(String str) {
        super(str);
    }

    public List<KyoKusanagi> a(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = (JSONArray) jSONObject.get(a());
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                if (jSONObject2.getString(VastExtensionXmlManager.TYPE).equals("dnsResolution")) {
                    String string = jSONObject2.getString("id");
                    JSONObject jSONObject3 = jSONObject2.getJSONObject("input");
                    arrayList.add(new KyoKusanagi(string, new KyoKusanagi.C0054KyoKusanagi(jSONObject3.getString("host"), jSONObject3.getString("userAgent"))));
                }
            }
            return arrayList;
        } catch (JSONException e) {
            e.printStackTrace();
            return arrayList;
        } catch (Throwable th) {
            return arrayList;
        }
    }
}
