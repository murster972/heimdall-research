package io.presage.parser;

import io.presage.parser.p040do.BenimaruNikaido;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChinGentsai extends KyoKusanagi {
    private KyoKusanagi a;

    public interface KyoKusanagi {
        void a();

        void a(List<BenimaruNikaido> list, JSONObject jSONObject);
    }

    public ChinGentsai(String str) {
        super(str);
    }

    public ChinGentsai(String str, KyoKusanagi kyoKusanagi) {
        this(str);
        this.a = kyoKusanagi;
    }

    public void a(JSONObject jSONObject) {
        if (jSONObject.has("force")) {
            try {
                if (jSONObject.getBoolean("force") && this.a != null) {
                    this.a.a();
                    return;
                }
                return;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(a());
            BenimaruNikaido.GoroDaimon goroDaimon = null;
            if (jSONObject.has("sync")) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("sync");
                goroDaimon = new BenimaruNikaido.GoroDaimon();
                if (jSONObject2.has("url_blacklist_ips")) {
                    goroDaimon.a(jSONObject2.getString("url_blacklist_ips"));
                }
                if (jSONObject2.has("url_blacklist_apps_for_usage")) {
                    goroDaimon.b(jSONObject2.getString("url_blacklist_apps_for_usage"));
                }
                if (jSONObject2.has("url_whitelist_apps_for_ips")) {
                    goroDaimon.c(jSONObject2.getString("url_whitelist_apps_for_ips"));
                }
            }
            for (int i = 0; i < jSONArray.length(); i++) {
                BenimaruNikaido benimaruNikaido = new BenimaruNikaido();
                JSONObject jSONObject3 = jSONArray.getJSONObject(i);
                String string = jSONObject3.getString("api_key");
                boolean z = jSONObject3.getBoolean("data_optin");
                benimaruNikaido.b(string);
                benimaruNikaido.a(z);
                JSONArray jSONArray2 = jSONObject3.getJSONArray("enabled");
                ArrayList arrayList2 = new ArrayList();
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    arrayList2.add(jSONArray2.getString(i2));
                }
                benimaruNikaido.a((List<String>) arrayList2);
                JSONObject jSONObject4 = jSONObject3.getJSONObject("timing_finder");
                BenimaruNikaido.ChoiBounge choiBounge = new BenimaruNikaido.ChoiBounge();
                choiBounge.a(jSONObject4.optInt("profig", 21600));
                if (arrayList2.contains("apps")) {
                    choiBounge.b(jSONObject4.getInt("apps"));
                }
                if (arrayList2.contains("ips")) {
                    choiBounge.c(jSONObject4.getInt("ips"));
                }
                if (arrayList2.contains("apps_usage")) {
                    choiBounge.d(jSONObject4.getInt("apps_usage"));
                }
                if (arrayList2.contains("android_accounts")) {
                    choiBounge.e(jSONObject4.getInt("android_accounts"));
                }
                if (arrayList2.contains("tasks")) {
                    choiBounge.f(jSONObject4.getInt("tasks"));
                }
                benimaruNikaido.a(choiBounge);
                if (jSONObject3.has("timeout")) {
                    JSONObject jSONObject5 = jSONObject3.getJSONObject("timeout");
                    BenimaruNikaido.ChinGentsai chinGentsai = new BenimaruNikaido.ChinGentsai();
                    if (jSONObject5.has("ads")) {
                        chinGentsai.a((float) jSONObject5.getDouble("ads"));
                    }
                    benimaruNikaido.a(chinGentsai);
                }
                BenimaruNikaido.KyoKusanagi kyoKusanagi = new BenimaruNikaido.KyoKusanagi();
                if (jSONObject3.has("cache")) {
                    JSONObject jSONObject6 = jSONObject3.getJSONObject("cache");
                    if (jSONObject6.has("ads_to_precache")) {
                        JSONObject jSONObject7 = jSONObject6.getJSONObject("ads_to_precache");
                        if (jSONObject7.has("max")) {
                            kyoKusanagi.a(jSONObject7.getInt("max"));
                        }
                        if (jSONObject7.has("default")) {
                            kyoKusanagi.b(jSONObject7.getInt("default"));
                        }
                    }
                    if (jSONObject6.has("ad_sync_timing_lock")) {
                        kyoKusanagi.a(jSONObject6.getLong("ad_sync_timing_lock"));
                    }
                    if (jSONObject6.has("window_lock")) {
                        JSONObject jSONObject8 = jSONObject6.getJSONObject("window_lock");
                        if (jSONObject8.has("load")) {
                            kyoKusanagi.b(jSONObject8.getLong("load"));
                        }
                        if (jSONObject8.has("show")) {
                            kyoKusanagi.c(jSONObject8.getLong("show"));
                        }
                    }
                    if (jSONObject6.has("max_call")) {
                        JSONObject jSONObject9 = jSONObject6.getJSONObject("max_call");
                        if (jSONObject9.has("load")) {
                            kyoKusanagi.c(jSONObject9.getInt("load"));
                        }
                        if (jSONObject9.has("show")) {
                            kyoKusanagi.d(jSONObject9.getInt("show"));
                        }
                    }
                    if (jSONObject6.has("logs")) {
                        kyoKusanagi.a(jSONObject6.getBoolean("logs"));
                    }
                }
                benimaruNikaido.a(kyoKusanagi);
                if (jSONObject3.has("timing_execute_internal")) {
                    JSONObject jSONObject10 = jSONObject3.getJSONObject("timing_execute_internal");
                    if (jSONObject10.has("ips") && jSONObject10.has("apps_usage")) {
                        benimaruNikaido.a(new BenimaruNikaido.ChangKoehan(jSONObject10.getInt("ips"), jSONObject10.getInt("apps_usage")));
                    }
                }
                if (jSONObject3.has("max_size")) {
                    JSONObject jSONObject11 = jSONObject3.getJSONObject("max_size");
                    BenimaruNikaido.C0053BenimaruNikaido benimaruNikaido2 = new BenimaruNikaido.C0053BenimaruNikaido();
                    if (jSONObject11.has("ips")) {
                        benimaruNikaido2.a(jSONObject11.getInt("ips"));
                    }
                    if (jSONObject11.has("apps_usage")) {
                        benimaruNikaido2.b(jSONObject11.getInt("apps_usage"));
                    }
                    benimaruNikaido.a(benimaruNikaido2);
                }
                if (jSONObject3.has("logs")) {
                    JSONObject jSONObject12 = jSONObject3.getJSONObject("logs");
                    if (jSONObject12.has("crash_report")) {
                        benimaruNikaido.a(jSONObject12.getString("crash_report"));
                    }
                }
                if (goroDaimon != null) {
                    benimaruNikaido.a(goroDaimon);
                }
                arrayList.add(benimaruNikaido);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        if (this.a != null) {
            this.a.a(arrayList, jSONObject);
        }
    }
}
