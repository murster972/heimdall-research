package io.presage.parser;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import com.Pinkamena;
import com.google.android.exoplayer2.util.MimeTypes;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.p003if.p004do.BrianBattler;
import com.p003if.p004do.Goenitz;
import com.p003if.p004do.Vice;
import io.presage.ChoiBounge;
import io.presage.IEulaHandler;
import io.presage.ads.NewAd;
import io.presage.ads.NewAdController;
import io.presage.formats.GoroDaimon;
import io.presage.helper.ChinGentsai;
import io.presage.model.Parameter;
import io.presage.p029char.ChoiBounge;
import io.presage.p031do.BenimaruNikaido;
import io.presage.p034goto.HeavyD;
import io.presage.p034goto.LuckyGlauber;
import io.presage.p034goto.Maxima;
import io.presage.p034goto.SaishuKusanagi;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdParser {
    private String a;
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public io.presage.p031do.KyoKusanagi c;
    /* access modifiers changed from: private */
    public IEulaHandler d;
    private io.presage.ads.KyoKusanagi e;
    private ChoiBounge f;
    private ChinGentsai g;
    /* access modifiers changed from: private */
    public NewAd[] h;
    /* access modifiers changed from: private */
    public int i = -1;
    private JSONObject j;

    public static class AdParserBroadcastReceiver extends BroadcastReceiver {
        private WeakReference<AdParser> a;
        private NewAd b;
        private ChoiBounge.GoroDaimon c;

        public AdParserBroadcastReceiver(AdParser adParser, NewAd newAd, ChoiBounge.GoroDaimon goroDaimon) {
            this.a = new WeakReference<>(adParser);
            this.b = newAd;
            this.c = goroDaimon;
        }

        public void onReceive(Context context, Intent intent) {
            AdParser adParser = (AdParser) this.a.get();
            if (adParser != null) {
                String stringExtra = intent.getStringExtra("id");
                if (this.b.getId() != null && this.b.getId().equals(stringExtra)) {
                    String action = intent.getAction();
                    if (action.equals(NewAd.ACTION_HIDE)) {
                        if (adParser.c != null && adParser.i == 1) {
                            adParser.c.d();
                        }
                        if (adParser.d != null) {
                            adParser.d.onEulaClosed();
                            IEulaHandler unused = adParser.d = null;
                        }
                        AdParser.d(adParser);
                        if (adParser.i > 0) {
                            adParser.a(adParser.h[adParser.h.length - adParser.i], this.c);
                        }
                        adParser.b.unregisterReceiver(this);
                    } else if (action.equals(NewAd.ACTION_SHOW)) {
                        if (adParser.c != null && adParser.i == 1) {
                            adParser.c.e();
                        }
                    } else if (action.equals(NewAd.ACTION_REWARD) && adParser.c != null && adParser.i == 1) {
                        ((BenimaruNikaido) adParser.c).f();
                    }
                }
            }
        }
    }

    public static class KyoKusanagi implements LuckyGlauber {
        private WeakReference<AdParser> a;
        private ChoiBounge.GoroDaimon b;

        public KyoKusanagi(AdParser adParser, ChoiBounge.GoroDaimon goroDaimon) {
            this.a = new WeakReference<>(adParser);
            this.b = goroDaimon;
        }

        public void a(HeavyD heavyD) {
            AdParser adParser = (AdParser) this.a.get();
            if (adParser != null) {
                String a2 = heavyD.a();
                if (a2.equals("close")) {
                    if (adParser.c != null && adParser.i == 1) {
                        adParser.c.d();
                    }
                    if (adParser.d != null) {
                        adParser.d.onEulaClosed();
                        IEulaHandler unused = adParser.d = null;
                    }
                    AdParser.d(adParser);
                    if (adParser.i > 0) {
                        adParser.a(adParser.h[adParser.h.length - adParser.i], this.b);
                    }
                } else if (a2.equals("open") && adParser.c != null && adParser.i == 1) {
                    adParser.c.e();
                }
            }
        }
    }

    public AdParser(Context context, io.presage.p029char.ChoiBounge choiBounge, ChinGentsai chinGentsai, String str, io.presage.p031do.KyoKusanagi kyoKusanagi, io.presage.ads.KyoKusanagi kyoKusanagi2) {
        this.a = str;
        this.b = context;
        this.c = kyoKusanagi;
        this.e = kyoKusanagi2;
        this.f = choiBounge;
        this.g = chinGentsai;
    }

    /* access modifiers changed from: private */
    public void a(NewAd newAd, ChoiBounge.GoroDaimon goroDaimon) {
        if (a(newAd)) {
            try {
                if (((Map) new com.p003if.p004do.ChoiBounge().m14090(new String(this.j.toString()), Map.class)) != null && newAd != null) {
                    try {
                        b(newAd, goroDaimon);
                    } catch (GoroDaimon e2) {
                        io.presage.p033for.GoroDaimon goroDaimon2 = new io.presage.p033for.GoroDaimon();
                        goroDaimon2.a(VastExtensionXmlManager.TYPE, "error");
                        goroDaimon2.a("error_type", "video");
                        goroDaimon2.a("error_message", e2.getMessage());
                        io.presage.actions.KyoKusanagi a2 = io.presage.actions.GoroDaimon.a().a(this.b, this.f, "send_event", "send_ad_event", goroDaimon2);
                        a2.a(newAd.getAdvertiser().getId(), newAd.getCampaignId(), newAd.getId(), newAd.getAdUnitId());
                        a2.a("video", e2.getMessage());
                        a2.k();
                        if (this.c != null) {
                            this.c.c();
                            return;
                        }
                    }
                    if (this.c != null && this.i == 1) {
                        this.c.b();
                    }
                    if (this.d != null) {
                        this.d.onEulaFound();
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
                if (this.c != null) {
                    this.c.c();
                }
            }
        } else {
            try {
                this.e.a(this.b, this.f, this.j.getJSONArray("ad").getJSONObject(this.h.length - this.i), new KyoKusanagi(this, goroDaimon)).a();
                if (this.c != null && this.i == 1) {
                    this.c.b();
                }
                if (this.d != null) {
                    this.d.onEulaFound();
                }
            } catch (JSONException e4) {
                e4.printStackTrace();
            }
        }
    }

    private boolean a(NewAd newAd) {
        String type = newAd.getFormatDescriptor().getType();
        return "multi_webviews".equals(type) || "execute".equals(type) || ("launch_activity".equals(type) && "multi_webviews".equals((String) newAd.getFormatDescriptor().getParameterValue("helper", String.class)));
    }

    private boolean a(JSONObject jSONObject) {
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("ad");
            int i2 = 0;
            boolean z = true;
            while (i2 < jSONArray.length()) {
                try {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    if (jSONObject2 == null || jSONObject2.isNull("format") || jSONObject2.isNull("params") || jSONObject2.isNull("actions") || jSONObject2.isNull("id") || jSONObject2.isNull("name") || jSONObject2.isNull(NewAd.EVENT_FINISH)) {
                        SaishuKusanagi.b("AdParser", "No ad to showAd... Missing Key");
                        return false;
                    }
                    try {
                        JSONObject jSONObject3 = jSONObject2.getJSONObject("format");
                        jSONObject2.getJSONArray("params");
                        jSONObject2.getJSONArray("actions");
                        jSONObject2.getJSONArray(NewAd.EVENT_FINISH);
                        z = !jSONObject3.isNull("id") && !jSONObject3.isNull("name");
                        i2++;
                    } catch (JSONException e2) {
                        SaishuKusanagi.b("AdParser", "No ad to showAd... Not a map or a List");
                        return false;
                    }
                } catch (JSONException e3) {
                    e3.printStackTrace();
                    return false;
                }
            }
            return z;
        } catch (JSONException e4) {
            e4.printStackTrace();
            return false;
        }
    }

    private void b(NewAd newAd, ChoiBounge.GoroDaimon goroDaimon) throws GoroDaimon {
        new io.presage.p034goto.ChinGentsai(this.b, newAd.getAdUnitId());
        if (newAd.getAdvertiser() != null && goroDaimon == ChoiBounge.GoroDaimon.Direct) {
            io.presage.p033for.GoroDaimon goroDaimon2 = new io.presage.p033for.GoroDaimon();
            goroDaimon2.a(VastExtensionXmlManager.TYPE, "served");
            io.presage.actions.KyoKusanagi a2 = io.presage.actions.GoroDaimon.a().a(this.b, this.f, "send_event", "send_ad_event", goroDaimon2);
            a2.a(newAd.getAdvertiser().getId(), newAd.getCampaignId(), newAd.getId(), newAd.getAdUnitId());
            a2.k();
        }
        Parameter parameter = newAd.getFormatDescriptor().getParameter("is_video");
        if (parameter == null || !parameter.getAsBoolean().booleanValue() || Build.VERSION.SDK_INT >= 16) {
            final NewAdController a3 = io.presage.ads.ChoiBounge.a().a("format").a(1).a(16).a(this.b, this.f, this.g.a(), newAd, newAd.getFormatDescriptor());
            AdParserBroadcastReceiver adParserBroadcastReceiver = new AdParserBroadcastReceiver(this, newAd, goroDaimon);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(NewAd.ACTION_HIDE);
            intentFilter.addAction(NewAd.ACTION_SHOW);
            intentFilter.addAction(NewAd.ACTION_REWARD);
            this.b.registerReceiver(adParserBroadcastReceiver, intentFilter);
            Maxima.a((Runnable) new Runnable() {
                public void run() {
                    NewAdController newAdController = a3;
                    Pinkamena.DianePie();
                }
            });
            return;
        }
        throw new GoroDaimon(2);
    }

    static /* synthetic */ int d(AdParser adParser) {
        int i2 = adParser.i;
        adParser.i = i2 - 1;
        return i2;
    }

    public void a(JSONObject jSONObject, ChoiBounge.GoroDaimon goroDaimon) {
        this.j = jSONObject;
        if (a(jSONObject)) {
            try {
                BrianBattler r3 = ((Vice) new Goenitz().m14115(jSONObject.toString())).m14166("ad");
                this.h = new NewAd[r3.m14058()];
                this.i = this.h.length;
                for (int i2 = 0; i2 < r3.m14058(); i2++) {
                    try {
                        BrianBattler r0 = r3.m14059(i2).m14139().m14166("params");
                        if (r0 != null) {
                            Iterator<com.p003if.p004do.SaishuKusanagi> it2 = r0.iterator();
                            while (it2.hasNext()) {
                                com.p003if.p004do.SaishuKusanagi next = it2.next();
                                if (next.m14139().m14167("name").m14146().equals(MimeTypes.BASE_TYPE_APPLICATION)) {
                                    Vice r02 = next.m14139().m14167("value").m14139();
                                    r02.m14168("require_permissions");
                                    r02.m14170("require_permissions", new com.p003if.p004do.ChoiBounge().m14083((Object) this.g.b()));
                                }
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    this.h[i2] = (NewAd) io.presage.p039new.LuckyGlauber.a(this.b, this.g.a()).m14086(r3.m14059(i2), NewAd.class);
                }
                a(this.h[0], goroDaimon);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        } else if (this.c != null) {
            this.c.c();
        }
    }

    public void a(JSONObject jSONObject, IEulaHandler iEulaHandler, ChoiBounge.GoroDaimon goroDaimon) {
        this.d = iEulaHandler;
        a(jSONObject, goroDaimon);
    }
}
