package io.presage.parser;

import android.content.Context;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.p003if.p004do.BrianBattler;
import com.p003if.p004do.Goenitz;
import com.p003if.p004do.Vice;
import io.presage.p029char.BenimaruNikaido;
import io.presage.p029char.ChangKoehan;
import io.presage.p033for.GoroDaimon;
import io.presage.p034goto.ChinGentsai;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

public class BenimaruNikaido {
    private ArrayList<KyoKusanagi> a;

    /* renamed from: io.presage.parser.BenimaruNikaido$BenimaruNikaido  reason: collision with other inner class name */
    public interface C0052BenimaruNikaido {
        void a();

        void b();
    }

    public static class KyoKusanagi {
        private BrianBattler a;
        private String b;
        /* access modifiers changed from: private */
        public String c;

        public KyoKusanagi(BrianBattler brianBattler, String str, String str2) {
            this.a = brianBattler;
            this.b = str;
            this.c = str2;
        }

        public BrianBattler a() {
            return this.a;
        }

        public String b() {
            return this.b;
        }

        public String c() {
            return this.c;
        }
    }

    public BenimaruNikaido(String str) {
        try {
            Vice vice = (Vice) new Goenitz().m14115(str);
            this.a = new ArrayList<>();
            BrianBattler r2 = vice.m14166("cache");
            if (r2 != null) {
                for (int i = 0; i < r2.m14058(); i++) {
                    try {
                        this.a.add(new KyoKusanagi(r2.m14059(i).m14139().m14166("assets"), r2.m14059(i).m14139().m14167("advert_id").m14146(), r2.m14059(i).m14139().m14167("campaign_id").m14146()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public BrianBattler a(String str) {
        if (this.a == null) {
            return null;
        }
        Iterator<KyoKusanagi> it2 = this.a.iterator();
        while (it2.hasNext()) {
            KyoKusanagi next = it2.next();
            if (next.c().equals(str)) {
                return next.a();
            }
        }
        return null;
    }

    public ArrayList<String> a() {
        ArrayList<String> arrayList = new ArrayList<>();
        Iterator<KyoKusanagi> it2 = this.a.iterator();
        while (it2.hasNext()) {
            KyoKusanagi next = it2.next();
            if (!arrayList.contains(next.c)) {
                arrayList.add(next.c);
            }
        }
        return arrayList;
    }

    public void a(Context context, String str, C0052BenimaruNikaido benimaruNikaido) {
        if (this.a != null) {
            io.presage.p029char.BenimaruNikaido.a().a(ChangKoehan.a().k());
            ChinGentsai chinGentsai = new ChinGentsai(context, str);
            CountDownLatch countDownLatch = new CountDownLatch(this.a.size());
            Iterator<KyoKusanagi> it2 = this.a.iterator();
            while (it2.hasNext()) {
                KyoKusanagi next = it2.next();
                if (next.a() == null || next.b().isEmpty() || chinGentsai.a(next.c(), chinGentsai.a(next.a()))) {
                    countDownLatch.countDown();
                } else {
                    String c = chinGentsai.c(next.c());
                    if (c != null) {
                        chinGentsai.a(next.c(), c);
                        countDownLatch.countDown();
                        GoroDaimon goroDaimon = new GoroDaimon();
                        goroDaimon.a(VastExtensionXmlManager.TYPE, "loaded");
                        io.presage.actions.KyoKusanagi a2 = io.presage.actions.GoroDaimon.a().a(context, ChangKoehan.a().k(), "send_event", "send_ad_event", goroDaimon);
                        a2.b(next.c());
                        a2.c(next.b());
                        a2.d(str);
                        a2.k();
                    } else {
                        BenimaruNikaido.C0043BenimaruNikaido[] a3 = chinGentsai.a(next.a(), next.c(), next.b(), true);
                        final ChinGentsai chinGentsai2 = chinGentsai;
                        final KyoKusanagi kyoKusanagi = next;
                        final CountDownLatch countDownLatch2 = countDownLatch;
                        final Context context2 = context;
                        final String str2 = str;
                        io.presage.p029char.BenimaruNikaido.a().a(a3, (BenimaruNikaido.GoroDaimon) new BenimaruNikaido.GoroDaimon() {
                            public void a() {
                            }

                            public void b() {
                                chinGentsai2.a(kyoKusanagi.c() + ChinGentsai.c);
                                countDownLatch2.countDown();
                            }

                            public void c() {
                                chinGentsai2.e(kyoKusanagi.c());
                                countDownLatch2.countDown();
                                GoroDaimon goroDaimon = new GoroDaimon();
                                goroDaimon.a(VastExtensionXmlManager.TYPE, "loaded");
                                io.presage.actions.KyoKusanagi a2 = io.presage.actions.GoroDaimon.a().a(context2, ChangKoehan.a().k(), "send_event", "send_ad_event", goroDaimon);
                                a2.b(kyoKusanagi.c());
                                a2.c(kyoKusanagi.b());
                                a2.d(str2);
                                a2.k();
                            }
                        });
                    }
                }
            }
            try {
                countDownLatch.await();
                Iterator<KyoKusanagi> it3 = this.a.iterator();
                while (it3.hasNext()) {
                    KyoKusanagi next2 = it3.next();
                    if (chinGentsai.a(next2.c(), chinGentsai.a(next2.a()))) {
                        if (benimaruNikaido != null) {
                            benimaruNikaido.a();
                            return;
                        }
                        return;
                    }
                }
                benimaruNikaido.b();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
