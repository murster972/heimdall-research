package io.presage.parser.p040do;

/* renamed from: io.presage.parser.do.KyoKusanagi  reason: invalid package */
public class KyoKusanagi extends GoroDaimon {
    private C0054KyoKusanagi c;

    /* renamed from: io.presage.parser.do.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    public static class C0054KyoKusanagi {
        private String a;
        private String b;

        public C0054KyoKusanagi(String str, String str2) {
            this.a = str;
            this.b = str2;
        }

        public String a() {
            return this.a;
        }

        public void a(String str) {
            this.a = str;
        }

        public String b() {
            return this.b;
        }

        public String toString() {
            return "Input{host='" + this.a + '\'' + ", userAgent='" + this.b + '\'' + '}';
        }
    }

    public KyoKusanagi(String str) {
        super(str, "dnsResolution");
    }

    public KyoKusanagi(String str, C0054KyoKusanagi kyoKusanagi) {
        this(str);
        this.c = kyoKusanagi;
    }

    public C0054KyoKusanagi a() {
        return this.c;
    }

    public String toString() {
        return "DNSTask{id=" + this.a + "type=" + this.b + "input=" + this.c + '}';
    }
}
