package io.presage.parser.p040do;

/* renamed from: io.presage.parser.do.GoroDaimon  reason: invalid package */
public class GoroDaimon {
    protected String a;
    protected String b;

    public GoroDaimon(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.a.equals(((GoroDaimon) obj).a);
    }

    public String toString() {
        return "Task{id='" + this.a + '\'' + ", type='" + this.b + '\'' + '}';
    }
}
