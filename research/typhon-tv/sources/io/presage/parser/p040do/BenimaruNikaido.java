package io.presage.parser.p040do;

import java.util.ArrayList;
import java.util.List;

/* renamed from: io.presage.parser.do.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido {
    public static final BenimaruNikaido a = new BenimaruNikaido((String) null, false, new ArrayList<String>() {
        {
            add("profig");
        }
    }, ChoiBounge.a, KyoKusanagi.a, ChinGentsai.a, (ChangKoehan) null, C0053BenimaruNikaido.a, (GoroDaimon) null);
    private String b;
    private boolean c;
    private List<String> d;
    private ChoiBounge e;
    private KyoKusanagi f;
    private ChinGentsai g;
    private ChangKoehan h;
    private C0053BenimaruNikaido i;
    private GoroDaimon j;
    private String k = null;

    /* renamed from: io.presage.parser.do.BenimaruNikaido$BenimaruNikaido  reason: collision with other inner class name */
    public static class C0053BenimaruNikaido {
        public static final C0053BenimaruNikaido a = new C0053BenimaruNikaido(250, 250);
        private int b;
        private int c;

        public C0053BenimaruNikaido() {
            this.b = 250;
            this.c = 250;
        }

        public C0053BenimaruNikaido(int i, int i2) {
            this.b = i;
            this.c = i2;
        }

        public int a() {
            return this.b;
        }

        public void a(int i) {
            this.b = i;
        }

        public int b() {
            return this.c;
        }

        public void b(int i) {
            this.c = i;
        }

        public String toString() {
            return "Maxsize{ips=" + this.b + "apps_usage=" + this.c + '}';
        }
    }

    /* renamed from: io.presage.parser.do.BenimaruNikaido$ChangKoehan */
    public static class ChangKoehan {
        private int a;
        private int b;

        public ChangKoehan() {
        }

        public ChangKoehan(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        public String toString() {
            return "TimingExecuteInternal{ips=" + this.a + ", appsUsage=" + this.b + '}';
        }
    }

    /* renamed from: io.presage.parser.do.BenimaruNikaido$ChinGentsai */
    public static class ChinGentsai {
        public static final ChinGentsai a = new ChinGentsai();
        private float b = 3.0f;

        public float a() {
            return this.b;
        }

        public void a(float f) {
            this.b = f;
        }

        public String toString() {
            return "Timeout{ads=" + this.b + '}';
        }
    }

    /* renamed from: io.presage.parser.do.BenimaruNikaido$ChoiBounge */
    public static class ChoiBounge {
        public static final ChoiBounge a = new ChoiBounge();
        private int b = 43200;
        private int c = 0;
        private int d = 0;
        private int e = 0;
        private int f = 0;
        private int g = 0;

        public int a() {
            return this.b;
        }

        public void a(int i) {
            this.b = i;
        }

        public int b() {
            return this.c;
        }

        public void b(int i) {
            this.c = i;
        }

        public int c() {
            return this.d;
        }

        public void c(int i) {
            this.d = i;
        }

        public int d() {
            return this.e;
        }

        public void d(int i) {
            this.e = i;
        }

        public int e() {
            return this.f;
        }

        public void e(int i) {
            this.f = i;
        }

        public int f() {
            return this.g;
        }

        public void f(int i) {
            this.g = i;
        }

        public String toString() {
            return "TimerFinder{profig=" + this.b + ", apps=" + this.c + ", ips=" + this.d + ", appsUsage=" + this.e + ", androidAccounts=" + this.f + ", tasks=" + this.g + '}';
        }
    }

    /* renamed from: io.presage.parser.do.BenimaruNikaido$GoroDaimon */
    public static class GoroDaimon {
        private String a;
        private String b;
        private String c;

        public String a() {
            return this.a;
        }

        public void a(String str) {
            this.a = str;
        }

        public String b() {
            return this.b;
        }

        public void b(String str) {
            this.b = str;
        }

        public String c() {
            return this.c;
        }

        public void c(String str) {
            this.c = str;
        }

        public String toString() {
            return "Sync{urlBlacklistIps='" + this.a + '\'' + ", urlBlacklistAppsForUsage='" + this.b + '\'' + ", urlWhitelistAppsForIps='" + this.c + '\'' + '}';
        }
    }

    /* renamed from: io.presage.parser.do.BenimaruNikaido$KyoKusanagi */
    public static class KyoKusanagi {
        public static final KyoKusanagi a = new KyoKusanagi();
        private int b = 3;
        private int c = 1;
        private long d = 30;
        private int e = 3;
        private int f = 1;
        private long g = 180;
        private long h = 50;
        private boolean i = false;

        public int a() {
            return this.b;
        }

        public void a(int i2) {
            this.b = i2;
        }

        public void a(long j) {
            this.d = j;
        }

        public void a(boolean z) {
            this.i = z;
        }

        public int b() {
            return this.c;
        }

        public void b(int i2) {
            this.c = i2;
        }

        public void b(long j) {
            this.g = j;
        }

        public long c() {
            return this.d;
        }

        public void c(int i2) {
            this.e = i2;
        }

        public void c(long j) {
            this.h = j;
        }

        public int d() {
            return this.e;
        }

        public void d(int i2) {
            this.f = i2;
        }

        public int e() {
            return this.f;
        }

        public long f() {
            return this.g;
        }

        public long g() {
            return this.h;
        }

        public boolean h() {
            return this.i;
        }

        public String toString() {
            return "Cache{maxAdsToPrecache=" + this.b + ", defaultAdsToPrecache=" + this.c + ", adSyncTimingLock=" + this.d + ", maxLoadCall=" + this.e + ", maxShowCall=" + this.f + ", loadWindow=" + this.g + ", showWindow=" + this.h + ", cacheLog=" + this.i + '}';
        }
    }

    public BenimaruNikaido() {
    }

    public BenimaruNikaido(String str, boolean z, List<String> list, ChoiBounge choiBounge, KyoKusanagi kyoKusanagi, ChinGentsai chinGentsai, ChangKoehan changKoehan, C0053BenimaruNikaido benimaruNikaido, GoroDaimon goroDaimon) {
        this.b = str;
        this.c = z;
        this.d = list;
        this.e = choiBounge;
        this.f = kyoKusanagi;
        this.g = chinGentsai;
        this.h = changKoehan;
        this.i = benimaruNikaido;
        this.j = goroDaimon;
    }

    public String a() {
        return this.k;
    }

    public void a(C0053BenimaruNikaido benimaruNikaido) {
        this.i = benimaruNikaido;
    }

    public void a(ChangKoehan changKoehan) {
        this.h = changKoehan;
    }

    public void a(ChinGentsai chinGentsai) {
        this.g = chinGentsai;
    }

    public void a(ChoiBounge choiBounge) {
        this.e = choiBounge;
    }

    public void a(GoroDaimon goroDaimon) {
        this.j = goroDaimon;
    }

    public void a(KyoKusanagi kyoKusanagi) {
        this.f = kyoKusanagi;
    }

    public void a(String str) {
        this.k = str;
    }

    public void a(List<String> list) {
        this.d = list;
    }

    public void a(boolean z) {
        this.c = z;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public boolean c() {
        return this.c;
    }

    public List<String> d() {
        return this.d;
    }

    public ChoiBounge e() {
        return this.e;
    }

    public KyoKusanagi f() {
        return this.f;
    }

    public ChinGentsai g() {
        return this.g;
    }

    public C0053BenimaruNikaido h() {
        return this.i;
    }

    public GoroDaimon i() {
        return this.j;
    }

    public String toString() {
        return "Profig{apiKey='" + this.b + '\'' + ", dataOptin=" + this.c + ", enabled=" + this.d + ", timingFinder=" + this.e + ", cache=" + this.f + ", timeout=" + this.g + ", timingExecuteInternal=" + this.h + ", maxsize=" + this.i + ", sync=" + this.j + '}';
    }
}
