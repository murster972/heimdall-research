package io.presage.p029char;

import io.presage.p034goto.SaishuKusanagi;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import p006do.JhunHoon;
import p006do.ShingoYabuki;
import p009if.LeonaHeidern;
import p009if.Shermie;

/* renamed from: io.presage.char.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido {
    public static final String a = BenimaruNikaido.class.getSimpleName();
    private static Lock b = new ReentrantLock();
    private static final BenimaruNikaido c = new BenimaruNikaido();
    private ThreadPoolExecutor d = new ThreadPoolExecutor(2, 2, 6, TimeUnit.SECONDS, this.e, this.g);
    private BlockingQueue<Runnable> e = new LinkedBlockingQueue();
    /* access modifiers changed from: private */
    public ChoiBounge f;
    private final ThreadFactory g = new ThreadFactory() {
        private final AtomicInteger b = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "DownloadManager #" + this.b.getAndIncrement());
        }
    };

    /* renamed from: io.presage.char.BenimaruNikaido$BenimaruNikaido  reason: collision with other inner class name */
    public static class C0043BenimaruNikaido {
        private String a;
        private String b;
        private String c;

        public C0043BenimaruNikaido(String str, String str2, String str3) {
            if (str != null && !str.isEmpty() && str2 != null && !str2.isEmpty()) {
                if (!(str3 == null) && !str3.isEmpty()) {
                    this.a = str;
                    this.b = str2;
                    this.c = str3;
                    return;
                }
            }
            throw new IllegalArgumentException();
        }

        public String a() {
            return this.a;
        }

        public String b() {
            return this.b;
        }

        public String c() {
            return this.c;
        }
    }

    /* renamed from: io.presage.char.BenimaruNikaido$ChinGentsai */
    private class ChinGentsai implements Runnable {
        private C0043BenimaruNikaido[] b;
        private GoroDaimon c;

        public ChinGentsai(C0043BenimaruNikaido[] benimaruNikaidoArr, GoroDaimon goroDaimon) {
            this.b = benimaruNikaidoArr;
            this.c = goroDaimon;
        }

        public void run() {
            final CountDownLatch countDownLatch = new CountDownLatch(this.b.length);
            if (this.c != null) {
                this.c.a();
            }
            for (C0043BenimaruNikaido a2 : this.b) {
                BenimaruNikaido.this.a(a2, (KyoKusanagi) new KyoKusanagi() {
                    public void a(C0043BenimaruNikaido benimaruNikaido) {
                        SaishuKusanagi.a(BenimaruNikaido.a, "onStart: " + (benimaruNikaido == null ? "null" : benimaruNikaido.a()));
                    }

                    public void b(C0043BenimaruNikaido benimaruNikaido) {
                        SaishuKusanagi.a(BenimaruNikaido.a, "onFailed: " + (benimaruNikaido == null ? "null" : benimaruNikaido.a()));
                        countDownLatch.countDown();
                    }

                    public void c(C0043BenimaruNikaido benimaruNikaido) {
                        SaishuKusanagi.a(BenimaruNikaido.a, "onFinished: " + (benimaruNikaido == null ? "null" : benimaruNikaido.a()));
                        countDownLatch.countDown();
                    }
                });
            }
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            C0043BenimaruNikaido[] benimaruNikaidoArr = this.b;
            int length = benimaruNikaidoArr.length;
            int i = 0;
            while (i < length) {
                C0043BenimaruNikaido benimaruNikaido = benimaruNikaidoArr[i];
                File file = benimaruNikaido == null ? null : new File(benimaruNikaido.b(), benimaruNikaido.c());
                if (file != null && file.exists() && file.length() > 0) {
                    i++;
                } else if (this.c != null) {
                    this.c.b();
                    return;
                } else {
                    return;
                }
            }
            if (this.c != null) {
                this.c.c();
            }
        }
    }

    /* renamed from: io.presage.char.BenimaruNikaido$GoroDaimon */
    public interface GoroDaimon {
        void a();

        void b();

        void c();
    }

    /* renamed from: io.presage.char.BenimaruNikaido$KyoKusanagi */
    public interface KyoKusanagi {
        void a(C0043BenimaruNikaido benimaruNikaido);

        void b(C0043BenimaruNikaido benimaruNikaido);

        void c(C0043BenimaruNikaido benimaruNikaido);
    }

    private BenimaruNikaido() {
        this.d.allowCoreThreadTimeOut(true);
    }

    public static BenimaruNikaido a() {
        return c;
    }

    /* access modifiers changed from: private */
    public void a(File file) throws FileNotFoundException {
        if (!file.exists()) {
            b.lock();
            try {
                if (!file.exists()) {
                    SaishuKusanagi.a("Creating directory {}", file.getAbsolutePath());
                    if (!file.mkdirs()) {
                        throw new FileNotFoundException("Can't create directory " + file.getAbsolutePath());
                    }
                }
            } finally {
                b.unlock();
            }
        }
    }

    public void a(final C0043BenimaruNikaido benimaruNikaido, final KyoKusanagi kyoKusanagi) {
        if (this.d == null || this.f == null || benimaruNikaido == null) {
            SaishuKusanagi.d(a, "maybe you forget to call init method or sth bad happened(executor is null)");
            if (kyoKusanagi != null) {
                kyoKusanagi.b(benimaruNikaido);
                return;
            }
            return;
        }
        this.d.execute(new Runnable() {
            public void run() {
                boolean z = false;
                if (kyoKusanagi != null) {
                    kyoKusanagi.a(benimaruNikaido);
                }
                JhunHoon jhunHoon = null;
                try {
                    JhunHoon a2 = BenimaruNikaido.this.f.a(benimaruNikaido.a(), new ShingoYabuki.KyoKusanagi().m18386(), 0, "");
                    if (a2.m6471()) {
                        File file = new File(benimaruNikaido.b());
                        if (!file.exists()) {
                            BenimaruNikaido.this.a(file);
                        }
                        p009if.ChinGentsai r1 = LeonaHeidern.m18967(LeonaHeidern.m18964(new File(benimaruNikaido.b(), benimaruNikaido.c())));
                        r1.m18933((Shermie) a2.m6463().m6496());
                        r1.close();
                        z = true;
                    }
                    if (a2 != null) {
                        a2.close();
                    }
                } catch (Exception e) {
                    SaishuKusanagi.b(BenimaruNikaido.a, "download failed", e);
                    if (jhunHoon != null) {
                        jhunHoon.close();
                    }
                } catch (Throwable th) {
                    if (jhunHoon != null) {
                        jhunHoon.close();
                    }
                    throw th;
                }
                if (kyoKusanagi == null) {
                    return;
                }
                if (z) {
                    kyoKusanagi.c(benimaruNikaido);
                } else {
                    kyoKusanagi.b(benimaruNikaido);
                }
            }
        });
    }

    public void a(ChoiBounge choiBounge) {
        this.f = choiBounge;
    }

    public void a(C0043BenimaruNikaido[] benimaruNikaidoArr, GoroDaimon goroDaimon) {
        if (benimaruNikaidoArr == null) {
            throw new IllegalArgumentException();
        } else if (this.d == null || this.f == null) {
            SaishuKusanagi.d(a, "maybe you forget to call init method or sth bad happened(executor is null)");
            if (goroDaimon != null) {
                goroDaimon.b();
            }
        } else {
            new Thread(new ChinGentsai(benimaruNikaidoArr, goroDaimon)).start();
        }
    }
}
