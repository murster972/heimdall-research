package io.presage.p029char;

import io.presage.p034goto.SaishuKusanagi;
import java.io.IOException;
import p006do.Bao;
import p006do.Chris;
import p006do.JhunHoon;
import p006do.Krizalid;
import p006do.Shermie;
import p006do.ShingoYabuki;
import p006do.Whip;
import p006do.p007do.p020for.LuckyGlauber;
import p009if.ChinGentsai;
import p009if.IoriYagami;
import p009if.LeonaHeidern;
import p009if.RugalBernstein;
import p009if.YashiroNanakase;

/* renamed from: io.presage.char.GoroDaimon  reason: invalid package */
final class GoroDaimon implements Shermie {
    public static final String a = GoroDaimon.class.getSimpleName();

    GoroDaimon() {
    }

    private Bao a(final Bao bao) {
        return new Bao() {
            public Chris a() {
                return bao.a();
            }

            public void a(ChinGentsai chinGentsai) throws IOException {
                ChinGentsai r0 = LeonaHeidern.m18967((YashiroNanakase) new RugalBernstein(chinGentsai));
                bao.a(r0);
                r0.close();
            }

            public long b() {
                return -1;
            }
        };
    }

    private JhunHoon a(JhunHoon jhunHoon) throws IOException {
        if (jhunHoon.m6463() == null) {
            return jhunHoon;
        }
        IoriYagami ioriYagami = new IoriYagami(jhunHoon.m6463().m6496());
        ShingoYabuki r1 = jhunHoon.m6468().m6582().m18381("Content-Encoding").m18381("Content-Length").m18381("x-amz-meta-content-encoding").m18386();
        return jhunHoon.m6464().m6486(r1).m6485((Krizalid) new LuckyGlauber(r1, LeonaHeidern.m18966((p009if.Shermie) ioriYagami))).m6490();
    }

    public JhunHoon a(Shermie.KyoKusanagi kyoKusanagi) throws IOException {
        Whip r0 = kyoKusanagi.m6577();
        if (r0.m6590() == null || r0.m6593("Content-Encoding") != null) {
            JhunHoon r02 = kyoKusanagi.m6576(r0);
            return (r02.m6473("x-amz-meta-content-encoding") == null || !r02.m6473("x-amz-meta-content-encoding").equalsIgnoreCase("gzip")) ? r02 : a(r02);
        }
        Whip r03 = r0.m6588().m18400("Content-Encoding", "gzip").m18399(r0.m6589(), a(r0.m6590())).m18393();
        SaishuKusanagi.a(a, r03 + "");
        JhunHoon r04 = kyoKusanagi.m6576(r03);
        return (r04.m6473("x-amz-meta-content-encoding") == null || !r04.m6473("x-amz-meta-content-encoding").equalsIgnoreCase("gzip")) ? r04 : a(r04);
    }
}
