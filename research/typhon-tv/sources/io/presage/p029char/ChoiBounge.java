package io.presage.p029char;

import android.content.Context;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import io.presage.helper.BenimaruNikaido;
import io.presage.p029char.p030do.ChinGentsai;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Whip;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import org.apache.commons.lang3.CharUtils;
import p006do.Bao;
import p006do.Chris;
import p006do.JhunHoon;
import p006do.Orochi;
import p006do.Shermie;
import p006do.ShingoYabuki;
import p006do.Whip;

/* renamed from: io.presage.char.ChoiBounge  reason: invalid package */
public class ChoiBounge {
    public static final String a = ChoiBounge.class.getSimpleName();
    private static final KyoKusanagi b;
    private static final KyoKusanagi c;
    private static final Chris d = Chris.m6441("application/json; charset=utf-8");
    private static final Pattern f = Pattern.compile("[^\\w-]");
    private static final Pattern g = Pattern.compile("[\\s]");
    private final String e = System.getProperty("http.agent");
    private Orochi h;
    private ShingoYabuki i;
    private Context j;
    private KyoKusanagi k;
    private boolean l;

    /* renamed from: io.presage.char.ChoiBounge$KyoKusanagi */
    public enum KyoKusanagi {
        PROD_HTTP,
        PROD_HTTPS,
        STAGING_HTTP,
        STAGING_HTTPS,
        DEV_A_HTTP,
        DEV_A_HTTPS,
        DEV_B_HTTP,
        DEV_B_HTTPS,
        DEV_C_HTTP,
        DEV_C_HTTPS,
        QA_HTTP,
        QA_HTTPS;

        public String a() {
            switch (this) {
                case PROD_HTTP:
                    return String.format("http%s://%s-%s.%spresage.io/%s/%s", new Object[]{"", "%s", "%s", "", "%s", "%s"});
                case PROD_HTTPS:
                    return String.format("http%s://%s-%s.%spresage.io/%s/%s", new Object[]{"s", "%s", "%s", "", "%s", "%s"});
                case STAGING_HTTP:
                    return String.format("http%s://%s-%s.%spresage.io/%s/%s", new Object[]{"", "%s", "%s", "staging.", "%s", "%s"});
                case STAGING_HTTPS:
                    return String.format("http%s://%s-%s.%spresage.io/%s/%s", new Object[]{"s", "%s", "%s", "staging.", "%s", "%s"});
                case DEV_A_HTTP:
                    return String.format("http%s://%s-%s.dev%s.cloud.ogury.io/%s/%s", new Object[]{"", "%s", "%s", "a", "%s", "%s"});
                case DEV_A_HTTPS:
                    return String.format("http%s://%s-%s.dev%s.cloud.ogury.io/%s/%s", new Object[]{"s", "%s", "%s", "a", "%s", "%s"});
                case DEV_B_HTTP:
                    return String.format("http%s://%s-%s.dev%s.cloud.ogury.io/%s/%s", new Object[]{"", "%s", "%s", "b", "%s", "%s"});
                case DEV_B_HTTPS:
                    return String.format("http%s://%s-%s.dev%s.cloud.ogury.io/%s/%s", new Object[]{"s", "%s", "%s", "b", "%s", "%s"});
                case DEV_C_HTTP:
                    return String.format("http%s://%s-%s.dev%s.cloud.ogury.io/%s/%s", new Object[]{"", "%s", "%s", "c", "%s", "%s"});
                case DEV_C_HTTPS:
                    return String.format("http%s://%s-%s.dev%s.cloud.ogury.io/%s/%s", new Object[]{"s", "%s", "%s", "c", "%s", "%s"});
                case QA_HTTP:
                    return String.format("http%s://%s-%s.qa.cloud.ogury.io/%s/%s", new Object[]{"", "%s", "%s", "%s", "%s"});
                case QA_HTTPS:
                    return String.format("http%s://%s-%s.qa.cloud.ogury.io/%s/%s", new Object[]{"s", "%s", "%s", "%s", "%s"});
                default:
                    SaishuKusanagi.d(ChoiBounge.a, "server url wrong config, return prod https url");
                    return String.format("http%s://%s-%s.%spresage.io/%s/%s", new Object[]{"s", "%s", "%s", "", "%s", "%s"});
            }
        }
    }

    static {
        char c2 = 65535;
        switch ("prod".hashCode()) {
            case -1897523141:
                if ("prod".equals("staging")) {
                    c2 = 1;
                    break;
                }
                break;
            case 3600:
                if ("prod".equals("qa")) {
                    c2 = 2;
                    break;
                }
                break;
            case 99349:
                if ("prod".equals("dev")) {
                    c2 = 0;
                    break;
                }
                break;
            case 3449687:
                if ("prod".equals("prod")) {
                    c2 = 3;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                b = KyoKusanagi.DEV_A_HTTPS;
                c = KyoKusanagi.DEV_A_HTTP;
                break;
            case 1:
                b = KyoKusanagi.STAGING_HTTPS;
                c = KyoKusanagi.STAGING_HTTP;
                break;
            case 2:
                b = KyoKusanagi.QA_HTTPS;
                c = KyoKusanagi.QA_HTTP;
                break;
            case 3:
                b = KyoKusanagi.PROD_HTTPS;
                c = KyoKusanagi.PROD_HTTP;
                break;
            default:
                b = KyoKusanagi.PROD_HTTPS;
                c = KyoKusanagi.PROD_HTTP;
                break;
        }
    }

    public ChoiBounge(Context context, boolean z) {
        this.j = context.getApplicationContext();
        this.l = z;
        this.k = new KyoKusanagi(this.j);
        this.h = new Orochi.KyoKusanagi().m6555((Shermie) new GoroDaimon()).m6553();
        if (this.e == null || this.e.isEmpty()) {
            this.i = new ShingoYabuki.KyoKusanagi().m18386();
        } else {
            this.i = new ShingoYabuki.KyoKusanagi().m18385(AbstractSpiCall.HEADER_USER_AGENT, e(this.e)).m18386();
        }
        if (z) {
            ChinGentsai.a().a(this);
        }
    }

    private void c() {
        String str;
        try {
            NetworkInfo a2 = BenimaruNikaido.a(this.j);
            if (a2 == null) {
                str = "UNKNOWN";
            } else if (BenimaruNikaido.c(this.j)) {
                str = a2.getTypeName();
            } else {
                str = a2.getTypeName() + " - " + a2.getSubtypeName();
            }
        } catch (Exception e2) {
            str = "NONE";
        }
        this.i = this.i.m6582().m18381("Connectivity").m18385("Connectivity", str).m18386();
    }

    private void d() {
        this.i = this.i.m6582().m18381("Timezone").m18385("Timezone", Whip.b()).m18386();
    }

    private String e(String str) {
        String str2 = "";
        int length = str.length();
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt > 31 && charAt < 127) {
                str2 = str2 + charAt;
            }
        }
        return str2;
    }

    public JhunHoon a(p006do.Whip whip) throws Exception {
        c();
        d();
        return this.h.m6547(whip).m18334();
    }

    public JhunHoon a(String str, int i2, String str2) throws Exception {
        c();
        d();
        return a(d(str), this.i, i2, str2);
    }

    public JhunHoon a(String str, ShingoYabuki shingoYabuki, int i2, String str2) throws Exception {
        return this.h.m6547(b(str, shingoYabuki, i2, str2)).m18334();
    }

    public String a(String str) {
        if (this.i == null) {
            return null;
        }
        return this.i.m6585(str);
    }

    public List<String> a() {
        ArrayList arrayList = new ArrayList();
        if (this.i == null) {
            return null;
        }
        for (String next : this.i.m6581()) {
            arrayList.add(next + ":" + this.i.m6585(next));
        }
        return arrayList;
    }

    public void a(float f2) {
        this.h = this.h.m6541().m6554((long) (1000.0f * f2), TimeUnit.MILLISECONDS).m6553();
    }

    public void a(String str, int i2, String str2, ChinGentsai chinGentsai, io.presage.p029char.p030do.KyoKusanagi kyoKusanagi) {
        c();
        d();
        a(d(str), this.i, i2, str2, chinGentsai, kyoKusanagi);
    }

    public void a(String str, ShingoYabuki shingoYabuki, int i2, String str2, ChinGentsai chinGentsai, io.presage.p029char.p030do.KyoKusanagi kyoKusanagi) {
        try {
            JhunHoon a2 = a(str, shingoYabuki, i2, str2);
            int r1 = a2.m6469();
            switch (r1) {
                case 200:
                    String r0 = a2.m6463().m6494();
                    if (chinGentsai != null) {
                        chinGentsai.a(r0);
                        return;
                    }
                    return;
                default:
                    if (chinGentsai != null) {
                        chinGentsai.a(r1, a2.m6463().m6494());
                    }
                    if (kyoKusanagi != null && this.l) {
                        ChinGentsai.a().a(b(str, shingoYabuki, i2, str2), kyoKusanagi);
                        return;
                    }
                    return;
            }
        } catch (Exception e2) {
            SaishuKusanagi.b(a, "http call failed", e2);
            if (!(chinGentsai == null || chinGentsai == null)) {
                chinGentsai.a(-1, e2.getMessage());
            }
            if (kyoKusanagi != null && this.l) {
                ChinGentsai.a().a(b(str, shingoYabuki, i2, str2), kyoKusanagi);
            }
        }
    }

    public void a(String[] strArr) {
        if (this.i != null && strArr != null) {
            this.i = this.i.m6582().m18381("Api-Key").m18385("Api-Key", Whip.a(strArr)).m18386();
        }
    }

    public p006do.Whip b(String str, ShingoYabuki shingoYabuki, int i2, String str2) {
        switch (i2) {
            case 0:
                return new Whip.KyoKusanagi().m18396(shingoYabuki).m18398(str).m18394().m18393();
            case 1:
                return new Whip.KyoKusanagi().m18396(shingoYabuki).m18398(str).m18395(Bao.a(d, str2)).m18393();
            case 2:
                return new Whip.KyoKusanagi().m18396(shingoYabuki).m18398(str).m18391(Bao.a(d, str2)).m18393();
            default:
                SaishuKusanagi.d(a, "Wrong http method");
                return null;
        }
    }

    public KyoKusanagi b() {
        return this.k;
    }

    public void b(String str) {
        if (this.i != null && str != null) {
            this.i = this.i.m6582().m18381("User").m18385("User", str).m18386();
        }
    }

    public void b(String[] strArr) {
        if (this.i != null && strArr != null) {
            this.i = this.i.m6582().m18381("Sdk-Version").m18385("Sdk-Version", io.presage.p034goto.Whip.a(strArr)).m18386();
        }
    }

    public void c(String str) {
        if (this.i != null && str != null) {
            this.i = this.i.m6582().m18381(AbstractSpiCall.HEADER_USER_AGENT).m18385(AbstractSpiCall.HEADER_USER_AGENT, str).m18386();
        }
    }

    public String d(String str) {
        char c2 = 65535;
        switch (str.hashCode()) {
            case -2026260249:
                if (str.equals("ad_to_serve")) {
                    c2 = 12;
                    break;
                }
                break;
            case -1152260809:
                if (str.equals("ad_sync")) {
                    c2 = 16;
                    break;
                }
                break;
            case -1109843021:
                if (str.equals("launch")) {
                    c2 = 14;
                    break;
                }
                break;
            case -979812809:
                if (str.equals("profig")) {
                    c2 = 1;
                    break;
                }
                break;
            case -535854154:
                if (str.equals("android_accounts")) {
                    c2 = 4;
                    break;
                }
                break;
            case -342915644:
                if (str.equals("precache_log")) {
                    c2 = 17;
                    break;
                }
                break;
            case -318720807:
                if (str.equals("predict")) {
                    c2 = 15;
                    break;
                }
                break;
            case -318281521:
                if (str.equals("presage")) {
                    c2 = CharUtils.CR;
                    break;
                }
                break;
            case -32402071:
                if (str.equals("empty_profig")) {
                    c2 = 0;
                    break;
                }
                break;
            case 104492:
                if (str.equals("ips")) {
                    c2 = 8;
                    break;
                }
                break;
            case 3000946:
                if (str.equals("apps")) {
                    c2 = 2;
                    break;
                }
                break;
            case 96891546:
                if (str.equals(NotificationCompat.CATEGORY_EVENT)) {
                    c2 = 7;
                    break;
                }
                break;
            case 110132110:
                if (str.equals("tasks")) {
                    c2 = 3;
                    break;
                }
                break;
            case 110621003:
                if (str.equals("track")) {
                    c2 = 9;
                    break;
                }
                break;
            case 938969138:
                if (str.equals("videotracking")) {
                    c2 = 10;
                    break;
                }
                break;
            case 1046398072:
                if (str.equals("ad_history")) {
                    c2 = 11;
                    break;
                }
                break;
            case 1282903444:
                if (str.equals("apps_usage")) {
                    c2 = 6;
                    break;
                }
                break;
            case 1560310816:
                if (str.equals("ip_tracker")) {
                    c2 = 5;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                return String.format(c.a(), new Object[]{TtmlNode.TAG_P, "v2", "v2", TtmlNode.TAG_P});
            case 1:
                return String.format(b.a(), new Object[]{TtmlNode.TAG_P, "v2", "v2", TtmlNode.TAG_P});
            case 2:
                return String.format(b.a(), new Object[]{"a", "v2", "v2", "a"});
            case 3:
                return String.format(b.a(), new Object[]{"t", "v2", "v1", "t"});
            case 4:
                return String.format(b.a(), new Object[]{"ac", "v1", "v1", "ac"});
            case 5:
                return String.format(b.a(), new Object[]{"it", "v2", "v2", "it"});
            case 6:
                return String.format(b.a(), new Object[]{"au", "v2", "v2", "au"});
            case 7:
                return String.format(b.a(), new Object[]{"e", "v2", "v2", "e"});
            case 8:
                return String.format(b.a(), new Object[]{"i", "v2", "v2", "i"});
            case 9:
                return String.format(b.a(), new Object[]{"tr", "v1", "v1", "track"});
            case 10:
                return String.format(b.a(), new Object[]{"v", "v1", "v1", "videotracking"});
            case 11:
                return String.format(b.a(), new Object[]{"ah", "v1", "v1", "ad_history"});
            case 12:
                return String.format(b.a(), new Object[]{"as", "v1", "v1", "ad_to_serve"});
            case 13:
                return String.format(b.a(), new Object[]{"pr", "v1", "v1", "presage"});
            case 14:
                return String.format(b.a(), new Object[]{"l", "v1", "v1", "launch"});
            case 15:
                return String.format(b.a(), new Object[]{"pd", "v1", "v1", "predict"});
            case 16:
                return String.format(b.a(), new Object[]{"sy", "v1", "v1", "ad_sync"});
            case 17:
                return String.format(b.a(), new Object[]{"pl", "v2", "v2", "pl"});
            default:
                SaishuKusanagi.a(a, "get unknown url");
                return "";
        }
    }
}
