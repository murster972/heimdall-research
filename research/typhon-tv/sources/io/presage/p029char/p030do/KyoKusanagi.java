package io.presage.p029char.p030do;

import java.io.IOException;

/* renamed from: io.presage.char.do.KyoKusanagi  reason: invalid package */
public interface KyoKusanagi {
    public static final KyoKusanagi a = new KyoKusanagi() {
        public long a() throws IOException {
            return 0;
        }
    };
    public static final KyoKusanagi b = new KyoKusanagi() {
        public long a() throws IOException {
            return -1;
        }
    };

    long a() throws IOException;
}
