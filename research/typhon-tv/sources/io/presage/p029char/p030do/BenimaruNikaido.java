package io.presage.p029char.p030do;

import com.google.android.exoplayer2.C;
import java.io.IOException;

/* renamed from: io.presage.char.do.BenimaruNikaido  reason: invalid package */
public class BenimaruNikaido implements KyoKusanagi {
    long c;
    private int d;
    private final int e;
    private final double f;
    private final double g;
    private final int h;
    private final int i;
    private final GoroDaimon j;

    /* renamed from: io.presage.char.do.BenimaruNikaido$KyoKusanagi */
    public static class KyoKusanagi {
        int a = 30000;
        double b = 0.5d;
        double c = 1.5d;
        int d = 240000;
        int e = 900000;
        GoroDaimon f = GoroDaimon.a;
    }

    public BenimaruNikaido() {
        this(new KyoKusanagi());
    }

    protected BenimaruNikaido(KyoKusanagi kyoKusanagi) {
        this.e = kyoKusanagi.a;
        this.f = kyoKusanagi.b;
        this.g = kyoKusanagi.c;
        this.h = kyoKusanagi.d;
        this.i = kyoKusanagi.e;
        this.j = kyoKusanagi.f;
        b();
    }

    static int a(double d2, double d3, int i2) {
        double d4 = ((double) i2) * d2;
        double d5 = ((double) i2) - d4;
        return (int) (((((d4 + ((double) i2)) - d5) + 1.0d) * d3) + d5);
    }

    private void e() {
        if (((double) this.d) >= ((double) this.h) / this.g) {
            this.d = this.h;
        } else {
            this.d = (int) (((double) this.d) * this.g);
        }
    }

    public long a() throws IOException {
        if (d() > ((long) this.i)) {
            return -1;
        }
        int a = a(this.f, Math.random(), this.d);
        e();
        return (long) a;
    }

    public final void b() {
        this.d = this.e;
        this.c = this.j.a();
    }

    public final int c() {
        return this.d;
    }

    public final long d() {
        return (this.j.a() - this.c) / C.MICROS_PER_SECOND;
    }
}
