package io.presage.p029char.p030do;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import io.presage.p029char.ChoiBounge;
import io.presage.p034goto.SaishuKusanagi;
import java.io.IOException;
import java.util.HashSet;
import net.pubnative.library.request.PubnativeRequest;
import p006do.JhunHoon;
import p006do.Whip;

/* renamed from: io.presage.char.do.ChinGentsai  reason: invalid package */
public class ChinGentsai {
    public static final String a = ChinGentsai.class.getSimpleName();
    private static ChinGentsai e = new ChinGentsai();
    Looper b;
    Handler c;
    HashSet<String> d = new HashSet<>();
    /* access modifiers changed from: private */
    public ChoiBounge f;
    private HandlerThread g = new HandlerThread("Retry Thread");

    /* renamed from: io.presage.char.do.ChinGentsai$KyoKusanagi */
    private class KyoKusanagi implements Runnable {
        private Whip b;
        private KyoKusanagi c;

        public KyoKusanagi(Whip whip, KyoKusanagi kyoKusanagi) {
            this.b = whip;
            this.c = kyoKusanagi;
        }

        public void run() {
            if (ChinGentsai.this.f != null) {
                JhunHoon jhunHoon = null;
                try {
                    JhunHoon a2 = ChinGentsai.this.f.a(this.b);
                    if (a2 != null) {
                        a2.close();
                    }
                    if (a2 == null || !a2.m6471()) {
                        try {
                            long a3 = this.c.a();
                            BenimaruNikaido benimaruNikaido = (BenimaruNikaido) this.c;
                            SaishuKusanagi.a("url:", this.b.m6592().toString());
                            SaishuKusanagi.a(PubnativeRequest.Parameters.TEST, "total elapsed time: " + (benimaruNikaido.d() / 1000));
                            SaishuKusanagi.a(PubnativeRequest.Parameters.TEST, "Interval: " + (benimaruNikaido.c() / 1000));
                            SaishuKusanagi.a(PubnativeRequest.Parameters.TEST, "backoff: " + (a3 / 1000));
                            if (a3 != -1) {
                                ChinGentsai.this.c.postDelayed(this, a3);
                            } else {
                                ChinGentsai.this.d.remove(this.b.m6592().toString());
                            }
                        } catch (IOException e) {
                            ChinGentsai.this.d.remove(this.b.m6592().toString());
                            e.printStackTrace();
                        }
                    } else {
                        ChinGentsai.this.d.remove(this.b.m6592().toString());
                        SaishuKusanagi.a(ChinGentsai.a, "retry successfully");
                    }
                } catch (Exception e2) {
                    SaishuKusanagi.b(ChinGentsai.a, "http call failed", e2);
                    if (jhunHoon != null) {
                        jhunHoon.close();
                    }
                    if (jhunHoon == null || !jhunHoon.m6471()) {
                        try {
                            long a4 = this.c.a();
                            BenimaruNikaido benimaruNikaido2 = (BenimaruNikaido) this.c;
                            SaishuKusanagi.a("url:", this.b.m6592().toString());
                            SaishuKusanagi.a(PubnativeRequest.Parameters.TEST, "total elapsed time: " + (benimaruNikaido2.d() / 1000));
                            SaishuKusanagi.a(PubnativeRequest.Parameters.TEST, "Interval: " + (benimaruNikaido2.c() / 1000));
                            SaishuKusanagi.a(PubnativeRequest.Parameters.TEST, "backoff: " + (a4 / 1000));
                            if (a4 != -1) {
                                ChinGentsai.this.c.postDelayed(this, a4);
                            } else {
                                ChinGentsai.this.d.remove(this.b.m6592().toString());
                            }
                        } catch (IOException e3) {
                            ChinGentsai.this.d.remove(this.b.m6592().toString());
                            e3.printStackTrace();
                        }
                    } else {
                        ChinGentsai.this.d.remove(this.b.m6592().toString());
                        SaishuKusanagi.a(ChinGentsai.a, "retry successfully");
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    if (jhunHoon != null) {
                        jhunHoon.close();
                    }
                    if (jhunHoon == null || !jhunHoon.m6471()) {
                        try {
                            long a5 = this.c.a();
                            BenimaruNikaido benimaruNikaido3 = (BenimaruNikaido) this.c;
                            SaishuKusanagi.a("url:", this.b.m6592().toString());
                            SaishuKusanagi.a(PubnativeRequest.Parameters.TEST, "total elapsed time: " + (benimaruNikaido3.d() / 1000));
                            SaishuKusanagi.a(PubnativeRequest.Parameters.TEST, "Interval: " + (benimaruNikaido3.c() / 1000));
                            SaishuKusanagi.a(PubnativeRequest.Parameters.TEST, "backoff: " + (a5 / 1000));
                            if (a5 != -1) {
                                ChinGentsai.this.c.postDelayed(this, a5);
                            } else {
                                ChinGentsai.this.d.remove(this.b.m6592().toString());
                            }
                        } catch (IOException e4) {
                            ChinGentsai.this.d.remove(this.b.m6592().toString());
                            e4.printStackTrace();
                        }
                    } else {
                        ChinGentsai.this.d.remove(this.b.m6592().toString());
                        SaishuKusanagi.a(ChinGentsai.a, "retry successfully");
                    }
                    throw th2;
                }
            }
        }
    }

    private ChinGentsai() {
        this.g.start();
        this.b = this.g.getLooper();
        this.c = new Handler(this.b);
    }

    public static ChinGentsai a() {
        return e;
    }

    public void a(Whip whip, KyoKusanagi kyoKusanagi) {
        if (this.d.contains(whip.m6592().toString())) {
            SaishuKusanagi.a(a, "ignore " + whip.m6592().toString());
            return;
        }
        KyoKusanagi kyoKusanagi2 = new KyoKusanagi(whip, kyoKusanagi);
        this.d.add(whip.m6592().toString());
        this.c.post(kyoKusanagi2);
    }

    public void a(ChoiBounge choiBounge) {
        this.f = choiBounge;
    }
}
