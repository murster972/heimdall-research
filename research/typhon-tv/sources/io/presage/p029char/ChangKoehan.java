package io.presage.p029char;

import android.content.Context;
import io.presage.helper.ChinGentsai;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.p034goto.Whip;
import io.presage.provider.PresageProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/* renamed from: io.presage.char.ChangKoehan  reason: invalid package */
public class ChangKoehan {
    public static final String a = ChangKoehan.class.getSimpleName();
    private static ChangKoehan c;
    List<String> b;
    /* access modifiers changed from: private */
    public String d;
    private float e = 3.0f;
    private boolean f = false;
    private boolean g = false;
    private int h = 3;
    private int i = 1;
    private long j = 30;
    private int k = 3;
    private int l = 1;
    private long m = 180;
    private long n = 50;
    private boolean o = false;
    /* access modifiers changed from: private */
    public ChoiBounge p;
    /* access modifiers changed from: private */
    public boolean q = false;
    private ChinGentsai r;
    private Set<String> s;
    private Set<String> t;
    private Set<String> u;

    /* renamed from: io.presage.char.ChangKoehan$BenimaruNikaido */
    public enum BenimaruNikaido {
        APP_PROCESS,
        SERVICE_PROCESS
    }

    /* renamed from: io.presage.char.ChangKoehan$KyoKusanagi */
    public interface KyoKusanagi {
        void a();
    }

    private ChangKoehan() {
    }

    public static ChangKoehan a() {
        if (c == null) {
            c = new ChangKoehan();
        }
        return c;
    }

    public void a(float f2) {
        this.e = f2;
    }

    public void a(int i2) {
        this.h = i2;
    }

    public void a(long j2) {
        this.j = j2;
    }

    public void a(Context context, BenimaruNikaido benimaruNikaido, final KyoKusanagi kyoKusanagi) {
        if (!this.q) {
            this.b = new ArrayList();
            final Context applicationContext = context.getApplicationContext();
            switch (benimaruNikaido) {
                case APP_PROCESS:
                    this.p = new ChoiBounge(applicationContext, false);
                    break;
                case SERVICE_PROCESS:
                    this.p = new ChoiBounge(applicationContext, true);
                    break;
                default:
                    this.p = new ChoiBounge(applicationContext, true);
                    SaishuKusanagi.d(a, "init type is not good");
                    break;
            }
            this.r = new ChinGentsai();
            this.r.a(context, context.getPackageName());
            new Thread(new Runnable() {
                public void run() {
                    ChangKoehan.this.p.a(new String[]{PresageProvider.b(applicationContext)});
                    try {
                        String unused = ChangKoehan.this.d = io.presage.p034goto.BenimaruNikaido.a(applicationContext).a();
                        if (ChangKoehan.this.d == null) {
                            throw new Exception("aaid is null");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        String unused2 = ChangKoehan.this.d = Whip.b(applicationContext);
                    }
                    ChangKoehan.this.p.b(ChangKoehan.this.d);
                    boolean unused3 = ChangKoehan.this.q = true;
                    if (kyoKusanagi != null) {
                        kyoKusanagi.a();
                    }
                }
            }).start();
        }
    }

    public void a(String str) {
        this.d = str;
    }

    public void a(List<String> list) {
        this.b = list;
    }

    public void a(Set<String> set) {
        this.s = set;
    }

    public void a(boolean z) {
        this.o = z;
    }

    public List<String> b() {
        return this.b;
    }

    public void b(int i2) {
        this.i = i2;
    }

    public void b(long j2) {
        this.m = j2;
    }

    public void b(Set<String> set) {
        this.t = set;
    }

    public void b(boolean z) {
        this.f = z;
    }

    public int c() {
        return this.h;
    }

    public void c(int i2) {
        this.k = i2;
    }

    public void c(long j2) {
        this.n = j2;
    }

    public void c(Set<String> set) {
        this.u = set;
    }

    public void c(boolean z) {
        this.g = z;
    }

    public int d() {
        return this.i;
    }

    public void d(int i2) {
        this.l = i2;
    }

    public long e() {
        return this.j;
    }

    public int f() {
        return this.k;
    }

    public int g() {
        return this.l;
    }

    public long h() {
        return this.m;
    }

    public long i() {
        return this.n;
    }

    public boolean j() {
        return this.o;
    }

    public ChoiBounge k() {
        return this.p;
    }

    public ChinGentsai l() {
        return this.r;
    }

    public boolean m() {
        return this.q;
    }

    public String n() {
        return this.d;
    }

    public float o() {
        return this.e;
    }

    public boolean p() {
        return this.f;
    }

    public boolean q() {
        return this.g;
    }

    public Set<String> r() {
        return this.s;
    }

    public Set<String> s() {
        return this.t;
    }

    public Set<String> t() {
        return this.u;
    }
}
