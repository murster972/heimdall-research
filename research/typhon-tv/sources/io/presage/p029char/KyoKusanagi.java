package io.presage.p029char;

import android.content.Context;
import android.net.NetworkInfo;
import android.os.Build;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import io.presage.helper.BenimaruNikaido;
import io.presage.helper.ChinGentsai;
import io.presage.provider.PresageProvider;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import p006do.ShingoYabuki;

/* renamed from: io.presage.char.KyoKusanagi  reason: invalid package */
public class KyoKusanagi {
    public Context a;

    public KyoKusanagi(Context context) {
        this.a = context;
    }

    private String a(Context context) {
        try {
            return context.getResources().getConfiguration().locale.getISO3Country();
        } catch (Exception e) {
            return "ZZZ";
        }
    }

    private String b(Context context) {
        if (context == null) {
            return null;
        }
        return PresageProvider.b(context);
    }

    private String d() {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(new Date());
    }

    public JSONObject a() {
        String str;
        JSONObject jSONObject = new JSONObject();
        try {
            NetworkInfo a2 = BenimaruNikaido.a(this.a);
            if (a2 == null) {
                str = "UNKNOWN";
            } else if (BenimaruNikaido.c(this.a)) {
                str = a2.getTypeName();
            } else {
                str = a2.getTypeName() + " - " + a2.getSubtypeName();
            }
        } catch (Exception e) {
            str = "NONE";
        }
        try {
            jSONObject.put("connectivity", (Object) str);
            jSONObject.put("at", (Object) d());
            jSONObject.put("country", (Object) a(this.a));
            jSONObject.put("build", 211);
            ArrayList arrayList = new ArrayList();
            arrayList.add(b(this.a));
            jSONObject.put("apps_publishers", (Object) new JSONArray((Collection) arrayList));
            jSONObject.put("version", (Object) "2.2.11-moat");
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    public ShingoYabuki b() {
        ShingoYabuki.KyoKusanagi kyoKusanagi = new ShingoYabuki.KyoKusanagi();
        if (ChangKoehan.a().n() != null && !ChangKoehan.a().n().isEmpty()) {
            kyoKusanagi.m18385("User", ChangKoehan.a().n());
        }
        new ChinGentsai().a(this.a, this.a.getPackageName());
        String str = "2.2.11-moat/" + PresageProvider.b(this.a) + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + Build.VERSION.RELEASE;
        if (!str.isEmpty()) {
            kyoKusanagi.m18385(AbstractSpiCall.HEADER_USER_AGENT, str);
        }
        return kyoKusanagi.m18386();
    }

    public ShingoYabuki c() {
        ShingoYabuki.KyoKusanagi kyoKusanagi = new ShingoYabuki.KyoKusanagi();
        if (ChangKoehan.a().n() != null && !ChangKoehan.a().n().isEmpty()) {
            kyoKusanagi.m18385("User", ChangKoehan.a().n());
        }
        new ChinGentsai().a(this.a, this.a.getPackageName());
        String str = "2.2.11-moat/" + PresageProvider.b(this.a) + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + Build.VERSION.RELEASE;
        if (!str.isEmpty()) {
            kyoKusanagi.m18385(AbstractSpiCall.HEADER_USER_AGENT, str);
        }
        kyoKusanagi.m18385("Sdk-Version", "[2.2.11-moat]");
        kyoKusanagi.m18385("Api-Key", "[" + b(this.a) + "]");
        return kyoKusanagi.m18386();
    }
}
