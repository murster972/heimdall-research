package io.presage.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import io.presage.p037int.BenimaruNikaido;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class PresageProvider extends ContentProvider {
    public static final String a = PresageProvider.class.getSimpleName();
    private Uri b;
    private Uri c;
    private Uri d;
    private Uri e;
    private Uri f;
    private Uri g;
    private Uri h;
    private Uri i;
    private Uri j;
    private Uri k;
    private Uri l;
    private Uri m;
    private Uri n;
    private BenimaruNikaido o = null;
    private io.presage.p037int.KyoKusanagi p = null;

    public enum KyoKusanagi {
        SDK_VERSION,
        SDK_BUILD,
        PROFIG,
        ADS_TIMEOUT,
        DATA_OPTIN_ABOVE_V_2_1,
        DATA_OPTION,
        PROFIG_JSON,
        AAID,
        ADS_OPTIN,
        LAUNCH_OPTIN,
        MY_PROFIG_APIKEY,
        SETTING,
        APIKEY
    }

    public static Uri a(KyoKusanagi kyoKusanagi, String str) {
        switch (kyoKusanagi) {
            case SDK_VERSION:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "sdkversion");
            case SDK_BUILD:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "sdkbuild");
            case PROFIG:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "profig");
            case DATA_OPTIN_ABOVE_V_2_1:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "profig" + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "data_optin_v21");
            case DATA_OPTION:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "profig" + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "data_optin");
            case ADS_TIMEOUT:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "profig" + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "ads_timeout");
            case PROFIG_JSON:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "profig" + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "raw_string");
            case AAID:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "profig" + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "aaid");
            case ADS_OPTIN:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "profig" + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "ads_optin");
            case LAUNCH_OPTIN:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "profig" + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "launch_optin");
            case MY_PROFIG_APIKEY:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "profig" + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "apikey");
            case SETTING:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "settings");
            case APIKEY:
                return Uri.parse("content://" + str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + "apikey");
            default:
                return null;
        }
    }

    public static final String a(Context context) {
        return String.format("%s.PresageProvider", new Object[]{context.getPackageName()});
    }

    public static String b(Context context) {
        String str = null;
        if (!(context == null || context.getContentResolver() == null)) {
            String[] strArr = {"apikey"};
            String str2 = "package = \"" + context.getPackageName() + "\"";
            KyoKusanagi kyoKusanagi = new KyoKusanagi(context.getContentResolver());
            Cursor a2 = kyoKusanagi.a(a(KyoKusanagi.APIKEY, a(context)), strArr, str2, (String[]) null, (String) null);
            Cursor a3 = a2 == null ? kyoKusanagi.a(a(KyoKusanagi.APIKEY, a(context)), strArr, str2, (String[]) null, (String) null) : a2;
            if (a3 != null) {
                if (a3.moveToLast()) {
                    str = a3.getString(a3.getColumnIndex("apikey"));
                }
                a3.close();
            }
        }
        return str;
    }

    public int delete(Uri uri, String str, String[] strArr) {
        if (uri != null) {
            if (uri.toString().equals(this.f.toString())) {
                this.o.b();
            } else if (uri.toString().equals(this.n.toString())) {
                this.o.getWritableDatabase().delete("apikey", str, strArr);
            }
        }
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        if (uri == null || !uri.toString().equals(this.m.toString())) {
            return null;
        }
        long a2 = this.p.a(contentValues);
        new KyoKusanagi(getContext().getContentResolver()).a(uri, (ContentObserver) null);
        return a2 == -1 ? Uri.parse("settings/noExist") : Uri.parse("settings/" + contentValues.get("id"));
    }

    public boolean onCreate() {
        String a2 = a(getContext());
        this.d = a(KyoKusanagi.SDK_VERSION, a2);
        this.e = a(KyoKusanagi.SDK_BUILD, a2);
        this.f = a(KyoKusanagi.PROFIG, a2);
        this.c = a(KyoKusanagi.DATA_OPTIN_ABOVE_V_2_1, a2);
        this.b = a(KyoKusanagi.DATA_OPTION, a2);
        this.g = a(KyoKusanagi.ADS_TIMEOUT, a2);
        this.h = a(KyoKusanagi.PROFIG_JSON, a2);
        this.j = a(KyoKusanagi.ADS_OPTIN, a2);
        this.k = a(KyoKusanagi.LAUNCH_OPTIN, a2);
        this.i = a(KyoKusanagi.AAID, a2);
        this.l = a(KyoKusanagi.MY_PROFIG_APIKEY, a2);
        this.m = a(KyoKusanagi.SETTING, a2);
        this.n = a(KyoKusanagi.APIKEY, a2);
        this.o = BenimaruNikaido.a(getContext());
        this.p = io.presage.p037int.KyoKusanagi.a(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        if (uri == null) {
            return null;
        }
        if (uri.toString().equals(this.d.toString())) {
            MatrixCursor matrixCursor = new MatrixCursor(new String[]{"sdkversion"});
            matrixCursor.addRow(new Object[]{"2.2.11-moat"});
            return matrixCursor;
        } else if (uri.toString().equals(this.e.toString())) {
            MatrixCursor matrixCursor2 = new MatrixCursor(new String[]{"sdkbuild"});
            matrixCursor2.addRow(new Object[]{211});
            return matrixCursor2;
        } else if (uri.toString().equals(this.c.toString())) {
            return this.o.a("data_optin_v21");
        } else {
            if (uri.toString().equals(this.b.toString())) {
                MatrixCursor matrixCursor3 = new MatrixCursor(new String[]{"k", "v"});
                matrixCursor3.addRow(new Object[]{"data_optin", 1});
                return matrixCursor3;
            } else if (uri.toString().equals(this.g.toString())) {
                return this.o.a("ads_timeout");
            } else {
                if (uri.toString().equals(this.h.toString())) {
                    return this.o.a("raw_string");
                }
                if (uri.toString().equals(this.i.toString())) {
                    return this.o.a("aaid");
                }
                if (uri.toString().equals(this.j.toString())) {
                    return this.o.a("ads_optin");
                }
                if (uri.toString().equals(this.k.toString())) {
                    return this.o.a("launch_optin");
                }
                if (uri.toString().equals(this.l.toString())) {
                    return this.o.a("apikey");
                }
                if (uri.toString().equals(this.m.toString())) {
                    return this.p.a(strArr, str, strArr2, str2);
                }
                if (!uri.toString().equals(this.n.toString())) {
                    return null;
                }
                SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
                sQLiteQueryBuilder.setTables("apikey");
                return sQLiteQueryBuilder.query(this.o.a(), strArr, str, strArr2, (String) null, (String) null, str2);
            }
        }
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        if (uri == null) {
            return 0;
        }
        KyoKusanagi kyoKusanagi = new KyoKusanagi(getContext().getContentResolver());
        if (uri.toString().contains("profig")) {
            if (!contentValues.containsKey("k") || !contentValues.containsKey("v") || contentValues.size() != 2 || this.o.a(contentValues) == -1) {
                return 0;
            }
            kyoKusanagi.a(uri, (ContentObserver) null);
            return 0;
        } else if (uri.toString().contains("settings")) {
            int a2 = this.p.a(contentValues, str, strArr);
            kyoKusanagi.a(uri, (ContentObserver) null);
            return a2;
        } else if (!uri.toString().contains("apikey")) {
            return 0;
        } else {
            this.o.a().replace("apikey", (String) null, contentValues);
            kyoKusanagi.a(uri, (ContentObserver) null);
            return 0;
        }
    }
}
