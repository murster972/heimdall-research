package io.presage.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import io.presage.p034goto.SaishuKusanagi;

public class KyoKusanagi {
    private ContentResolver a;

    public KyoKusanagi(ContentResolver contentResolver) {
        this.a = contentResolver;
    }

    public int a(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        try {
            return this.a.update(uri, contentValues, str, strArr);
        } catch (Exception e) {
            SaishuKusanagi.d("SafeContentResolver", e.getMessage());
            return 0;
        }
    }

    public int a(Uri uri, String str, String[] strArr) {
        try {
            return this.a.delete(uri, str, strArr);
        } catch (Exception e) {
            SaishuKusanagi.d("SafeContentResolver", e.getMessage());
            return 0;
        }
    }

    public Cursor a(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        try {
            return this.a.query(uri, strArr, str, strArr2, str2);
        } catch (Exception e) {
            SaishuKusanagi.d("SafeContentResolver", e.getMessage());
            return null;
        }
    }

    public Uri a(Uri uri, ContentValues contentValues) {
        try {
            return this.a.insert(uri, contentValues);
        } catch (Exception e) {
            SaishuKusanagi.d("SafeContentResolver", e.getMessage());
            return null;
        }
    }

    public void a(Uri uri, ContentObserver contentObserver) {
        try {
            this.a.notifyChange(uri, contentObserver);
        } catch (Exception e) {
            SaishuKusanagi.d("SafeContentResolver", e.getMessage());
        }
    }

    public void a(Uri uri, boolean z, ContentObserver contentObserver) {
        try {
            this.a.registerContentObserver(uri, z, contentObserver);
        } catch (Exception e) {
            SaishuKusanagi.d("SafeContentResolver", e.getMessage());
        }
    }
}
