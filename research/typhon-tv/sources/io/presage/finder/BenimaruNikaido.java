package io.presage.finder;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import io.presage.finder.model.App;
import io.presage.finder.model.CollectionFinderResult;
import io.presage.p034goto.SaishuKusanagi;
import java.util.List;

public class BenimaruNikaido implements HeavyD {
    private Context a;

    public BenimaruNikaido(Context context) {
        this.a = context;
    }

    public IFinderResult a() {
        Intent intent;
        boolean z;
        CollectionFinderResult collectionFinderResult = new CollectionFinderResult("apps");
        try {
            List<PackageInfo> installedPackages = this.a.getPackageManager().getInstalledPackages(0);
            for (int i = 0; i < installedPackages.size(); i++) {
                PackageInfo packageInfo = installedPackages.get(i);
                if (!(packageInfo == null || packageInfo.packageName == null || packageInfo.applicationInfo == null)) {
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    try {
                        intent = this.a.getPackageManager().getLaunchIntentForPackage(packageInfo.packageName);
                    } catch (RuntimeException e) {
                        SaishuKusanagi.b("finder", e.getMessage(), e);
                        intent = null;
                    }
                    if (intent != null) {
                        if (intent.getCategories().contains("android.intent.category.HOME")) {
                            z = true;
                        } else if (intent.getCategories().contains("android.intent.category.LAUNCHER")) {
                            z = false;
                        }
                        App app = new App();
                        app.a(packageInfo.packageName);
                        app.b(packageInfo.versionName);
                        app.c(String.valueOf(packageInfo.versionCode));
                        app.a((applicationInfo.flags & 1) == 1);
                        app.b(z);
                        collectionFinderResult.a((IFinderResult) app);
                    }
                }
            }
            return collectionFinderResult;
        } catch (Exception e2) {
            return collectionFinderResult;
        }
    }

    public String b() {
        return "apps";
    }
}
