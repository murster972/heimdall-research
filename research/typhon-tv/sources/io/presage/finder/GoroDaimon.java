package io.presage.finder;

import android.content.Context;
import io.presage.BenimaruNikaido;

public class GoroDaimon implements HeavyD {
    private Context a;

    public GoroDaimon(Context context) {
        this.a = context;
    }

    public IFinderResult a() {
        return BenimaruNikaido.a(this.a).c();
    }

    public String b() {
        return "apps_usage";
    }
}
