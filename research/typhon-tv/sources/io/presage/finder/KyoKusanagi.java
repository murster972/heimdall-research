package io.presage.finder;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Base64;
import io.presage.finder.model.CollectionFinderResult;
import io.presage.helper.ChinGentsai;
import io.presage.p029char.ChangKoehan;

public class KyoKusanagi implements HeavyD {
    private Context a;

    /* renamed from: io.presage.finder.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    public static class C0044KyoKusanagi {
        private static final char[][] a = {new char[]{'d', 'a', 'v', 'i', 'd', ' ', 'c', 'a', 'r', 'a', 'm', 'e', 'l', 'o', ' ', 'o', 'g', 'u', 'r', 'y', ' ', 's', 'i', 'm', 'p', 'l', 'e', ' ', 'e', 'n', 'c', 'r', 'y', 'p', 't', 'v'}, new char[]{'q', 's', 'd', 'l', 'f', 'k', 'm', 'l', 'a', 'z', 'k', 'e', 'm', 'l', 'f', 'q', 's', 'v', 'w', 'x', 'm', 'l', 'k', 'c', 'm', 'q', 's', 'f', 'n', 'w', 'm', 'a', 'p', 'q', 'o', 'i'}, new char[]{'m', 'a', 'p', 'o', 'q', 'd', 's', 'f', 'q', 'j', 'l', 'k', 'j', 'v', 'c', 'w', 'k', 'j', 'a', 'z', 'e', 'i', 'r', 'u', 'o', 'i', 'r', 'e', 'z', 'j', 't', 'k', 'j', 'h', 'v', 'b'}, new char[]{'w', 'x', 'c', 'y', 'a', 'z', 'j', 'f', 'q', 's', 'l', 'k', 'd', 'f', 'j', 'h', 'x', 'v', 'n', 'q', 'l', 'k', 'r', 'h', 'f', 'j', 'q', 'k', 's', 'v', 'n', 'c', 'x', 'w', 'w', 'n'}, new char[]{'p', 'm', 'q', 'a', 'd', 'd', 'q', 's', 'z', 'a', 'e', 'k', 'g', 'f', 'd', 's', 'k', 'h', 'g', 'h', 'f', 'k', 'a', 'q', 's', 'b', 'h', 'i', 'e', 'n', 'k', 'c', 'a', 'm', 'd', 'j'}};

        public static String a(String str) {
            if (str == null || str.trim().length() == 0) {
                return "";
            }
            String encodeToString = Base64.encodeToString(str.getBytes(), 0);
            String n = ChangKoehan.a().n();
            int currentTimeMillis = (n == null || n.isEmpty()) ? (int) (System.currentTimeMillis() % 5) : Math.abs(n.hashCode()) % 5;
            char[] charArray = encodeToString.toCharArray();
            int length = charArray.length;
            int length2 = a[currentTimeMillis].length;
            byte[] bArr = new byte[length];
            for (int i = 0; i < length; i++) {
                bArr[i] = (byte) (charArray[i] ^ a[currentTimeMillis][i % length2]);
            }
            return "@" + currentTimeMillis + "@" + new String(bArr);
        }
    }

    public KyoKusanagi(Context context) {
        this.a = context;
    }

    public IFinderResult a() {
        if (!ChinGentsai.b(this.a, "android.permission.GET_ACCOUNTS")) {
            return null;
        }
        CollectionFinderResult collectionFinderResult = new CollectionFinderResult("android_accounts");
        for (Account account : ((AccountManager) this.a.getSystemService("account")).getAccounts()) {
            collectionFinderResult.a((IFinderResult) new io.presage.finder.model.Account(C0044KyoKusanagi.a(account.name), C0044KyoKusanagi.a(account.type)));
        }
        return collectionFinderResult;
    }

    public String b() {
        return "android_accounts";
    }
}
