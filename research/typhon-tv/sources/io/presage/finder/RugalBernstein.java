package io.presage.finder;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import io.presage.finder.model.Profig;
import io.presage.p034goto.BenimaruNikaido;
import io.presage.p034goto.Orochi;
import io.presage.p034goto.Whip;
import java.util.MissingResourceException;
import org.apache.commons.lang3.StringUtils;

public class RugalBernstein implements HeavyD {
    private Context a;

    public RugalBernstein(Context context) {
        this.a = context;
    }

    private void a(Profig.Device device) {
        DisplayMetrics displayMetrics;
        WindowManager windowManager = (WindowManager) this.a.getSystemService("window");
        Point point = new Point(0, 0);
        try {
            windowManager.getDefaultDisplay().getSize(point);
        } catch (Exception e) {
        }
        try {
            DisplayMetrics displayMetrics2 = new DisplayMetrics();
            windowManager.getDefaultDisplay().getMetrics(displayMetrics2);
            displayMetrics = displayMetrics2;
        } catch (Exception e2) {
            displayMetrics = this.a.getResources().getDisplayMetrics();
        }
        device.a(new Profig.Screen(point.x > point.y ? point.y : point.x, point.x > point.y ? point.x : point.y, displayMetrics.density));
    }

    private void a(Profig profig) {
        boolean z = false;
        try {
            BenimaruNikaido.KyoKusanagi a2 = BenimaruNikaido.a(this.a);
            String a3 = a2.a();
            if (a3 == null) {
                throw new Exception("aaid is null");
            }
            profig.b(a3);
            if (!a2.b()) {
                z = true;
            }
            profig.a(Boolean.valueOf(z));
            profig.b(false);
        } catch (Exception e) {
            profig.b(Whip.b(c()));
            profig.a((Boolean) true);
            profig.b(true);
        }
    }

    private void b(Profig.Device device) {
        String property = System.getProperty("java.vm.version");
        String property2 = System.getProperty("java.vm.name");
        device.e(property);
        device.c(property2);
    }

    private void b(Profig profig) {
        try {
            profig.d(this.a.getResources().getConfiguration().locale.getISO3Country());
        } catch (MissingResourceException e) {
            profig.d("ZZZ");
        }
        profig.c(this.a.getResources().getConfiguration().locale.getLanguage());
    }

    private Context c() {
        return this.a;
    }

    private void c(Profig.Device device) {
        device.d(System.getProperty("os.arch"));
    }

    private void c(Profig profig) {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        if (str2 == null) {
            str2 = "Unknown";
        } else if (!str2.startsWith(str)) {
            str2 = str + StringUtils.SPACE + str2;
        }
        Profig.Device device = new Profig.Device();
        device.a(str2);
        d(device);
        a(device);
        c(device);
        b(device);
        profig.a(device);
    }

    private void d(Profig.Device device) {
        device.b(Build.VERSION.RELEASE);
    }

    private void d(Profig profig) {
        boolean z = true;
        try {
            if (Build.VERSION.SDK_INT < 17 || Build.VERSION.SDK_INT >= 21) {
                if (Settings.Secure.getInt(this.a.getContentResolver(), "install_non_market_apps", 0) != 1) {
                    z = false;
                }
                profig.a(z);
            }
            if (Build.VERSION.SDK_INT >= 17) {
                if (Settings.Global.getInt(this.a.getContentResolver(), "install_non_market_apps", 0) != 1) {
                    z = false;
                }
                profig.a(z);
            }
            z = false;
            profig.a(z);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void e(Profig profig) {
        profig.a(Whip.b());
    }

    private void f(Profig profig) {
        profig.c(Orochi.a());
    }

    public IFinderResult a() {
        Profig profig = new Profig();
        e(profig);
        a(profig);
        b(profig);
        d(profig);
        c(profig);
        f(profig);
        return profig;
    }

    public String b() {
        return "profig";
    }
}
