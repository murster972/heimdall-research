package io.presage.finder;

import android.content.Context;
import io.presage.helper.ChinGentsai;

public class ChangKoehan extends ChinGentsai {
    public ChangKoehan(Context context, ChinGentsai chinGentsai) {
        super(context, chinGentsai);
    }

    public HeavyD a(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case -535854154:
                if (str.equals("android_accounts")) {
                    c = 1;
                    break;
                }
                break;
            case 104492:
                if (str.equals("ips")) {
                    c = 3;
                    break;
                }
                break;
            case 3000946:
                if (str.equals("apps")) {
                    c = 0;
                    break;
                }
                break;
            case 110132110:
                if (str.equals("tasks")) {
                    c = 2;
                    break;
                }
                break;
            case 1282903444:
                if (str.equals("apps_usage")) {
                    c = 4;
                    break;
                }
                break;
            case 1560310816:
                if (str.equals("ip_tracker")) {
                    c = 5;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return new BenimaruNikaido(this.a);
            case 1:
                return new KyoKusanagi(this.a);
            case 2:
                return new ChoiBounge(this.a);
            case 3:
                return new LuckyGlauber(this.a);
            case 4:
                return new GoroDaimon(this.a);
            case 5:
                return new BrianBattler(this.a);
            default:
                return super.a(str);
        }
    }
}
