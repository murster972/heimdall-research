package io.presage.finder;

import android.content.Context;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import io.presage.finder.model.DnsResult;
import io.presage.finder.model.DnsResults;
import io.presage.helper.ChinGentsai;
import io.presage.p029char.ChangKoehan;
import io.presage.p034goto.SaishuKusanagi;
import io.presage.parser.GoroDaimon;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Proxy;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.json.JSONException;
import org.json.JSONObject;
import p006do.JhunHoon;
import p006do.Orochi;
import p006do.Shermie;
import p006do.Whip;

public class ChoiBounge implements HeavyD {
    private Context a;
    private List<io.presage.parser.p040do.KyoKusanagi> b;

    class KyoKusanagi implements Shermie {
        public final String a = KyoKusanagi.class.getSimpleName();
        public boolean b = false;
        private List<DnsResult.Ip> d = new ArrayList();

        KyoKusanagi() {
        }

        public JhunHoon a(Shermie.KyoKusanagi kyoKusanagi) throws IOException {
            Whip r3 = kyoKusanagi.m6577();
            long nanoTime = System.nanoTime();
            SaishuKusanagi.a(this.a, String.format("Sending request %s on %s%n%s", new Object[]{r3.m6592(), kyoKusanagi.m6575(), ""}));
            if (!this.b) {
                if (kyoKusanagi.m6575().m18333().m18376() != null) {
                    this.b = kyoKusanagi.m6575().m18333().m18376().type() != Proxy.Type.DIRECT;
                } else {
                    this.b = true;
                }
            }
            JhunHoon r0 = kyoKusanagi.m6576(r3);
            String[] split = kyoKusanagi.m6575().m18333().m18378().getAddress().toString().split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
            if (split != null && split.length == 2) {
                this.d.add(new DnsResult.Ip(split[0], split[1], r0.m6469(), (String) null));
            }
            SaishuKusanagi.a(this.a, String.format("Received response for %s in %.1fms%n%s", new Object[]{r0.m6472().m6592(), Double.valueOf(((double) (System.nanoTime() - nanoTime)) / 1000000.0d), ""}));
            return r0;
        }

        public List<DnsResult.Ip> a() {
            return this.d == null ? this.d : new ArrayList(this.d);
        }
    }

    public ChoiBounge(Context context) {
        this.a = context;
    }

    private Orochi a(Orochi orochi, Shermie shermie) {
        Orochi.KyoKusanagi r0 = orochi.m6541();
        r0.m6557().clear();
        return r0.m6551(shermie).m6553();
    }

    private DnsResult a(io.presage.parser.p040do.KyoKusanagi kyoKusanagi, Orochi orochi) throws Exception {
        Whip r0 = new Whip.KyoKusanagi().m18400(AbstractSpiCall.HEADER_USER_AGENT, kyoKusanagi.a().b()).m18398("http://" + kyoKusanagi.a().a()).m18390().m18393();
        KyoKusanagi kyoKusanagi2 = new KyoKusanagi();
        JhunHoon r02 = a(orochi, (Shermie) kyoKusanagi2).m6547(r0).m18334();
        if (!(r02 == null || r02.m6463() == null)) {
            r02.m6463().close();
        }
        if (kyoKusanagi2.b) {
            SaishuKusanagi.a("DnsResolutionFinder", "proxy detected!");
            return null;
        }
        return new DnsResult(kyoKusanagi.b(), new DnsResult.Output(kyoKusanagi2.a()));
    }

    /* JADX INFO: finally extract failed */
    public IFinderResult a() {
        String str;
        boolean z;
        DnsResult.Output b2;
        List<DnsResult.Ip> b3;
        if (!ChinGentsai.b(this.a, "android.permission.INTERNET")) {
            return null;
        }
        io.presage.p029char.ChoiBounge k = ChangKoehan.a().k();
        if (k == null) {
            k = new io.presage.p029char.ChoiBounge(this.a, true);
        }
        try {
            JhunHoon a2 = k.a("tasks", 0, "");
            if (a2 == null) {
                SaishuKusanagi.a("DnsResolutionFinder", "http response is null");
                return null;
            } else if (a2.m6469() != 200) {
                a2.close();
                return null;
            } else {
                try {
                    str = a2.m6463().m6494();
                    a2.close();
                } catch (IOException e) {
                    SaishuKusanagi.b("DnsResolutionFinder", "find 2", e);
                    a2.close();
                    str = null;
                } catch (Throwable th) {
                    a2.close();
                    throw th;
                }
                if (str == null) {
                    SaishuKusanagi.a("DnsResolutionFinder", "http response string is null, :(");
                    return null;
                }
                try {
                    this.b = new GoroDaimon("tasks").a(new JSONObject(str));
                } catch (JSONException e2) {
                    SaishuKusanagi.b("DnsResolutionFinder", "find 3", e2);
                }
                if (this.b == null) {
                    return null;
                }
                Orochi r5 = new Orochi.KyoKusanagi().m6552(true).m6556(true).m6553();
                ArrayList arrayList = new ArrayList();
                for (io.presage.parser.p040do.KyoKusanagi next : this.b) {
                    try {
                        DnsResult a3 = a(next, r5);
                        if (a3 == null) {
                            return null;
                        }
                        arrayList.add(a3);
                    } catch (Exception e3) {
                        SaishuKusanagi.b("DnsResolutionFinder", "resolveDNS 1", e3);
                        if ((e3 instanceof UnknownHostException) && !next.a().a().startsWith("www.")) {
                            DnsResult.Ip ip = new DnsResult.Ip(next.a().a(), "", 0, e3.getMessage());
                            String str2 = "www." + next.a().a();
                            try {
                                InetAddress.getAllByName(str2);
                                z = true;
                            } catch (UnknownHostException e4) {
                                DnsResult.Ip ip2 = new DnsResult.Ip(str2, "", 0, e4.getMessage());
                                ArrayList arrayList2 = new ArrayList();
                                arrayList2.add(ip);
                                arrayList2.add(ip2);
                                arrayList.add(new DnsResult(next.b(), new DnsResult.Output((List<DnsResult.Ip>) arrayList2)));
                                z = false;
                            }
                            if (z) {
                                next.a().a(str2);
                                try {
                                    DnsResult a4 = a(next, r5);
                                    if (!(a4 == null || (b2 = a4.b()) == null || (b3 = b2.b()) == null)) {
                                        b3.add(0, ip);
                                        arrayList.add(a4);
                                    }
                                } catch (Exception e5) {
                                    SaishuKusanagi.b("DnsResolutionFinder", "resolveDNS 2", e5);
                                }
                            }
                        }
                    }
                }
                return new DnsResults((List<DnsResult>) arrayList);
            }
        } catch (Exception e6) {
            SaishuKusanagi.b("DnsResolutionFinder", "find 1", e6);
            return null;
        }
    }

    public String b() {
        return "tasks";
    }
}
