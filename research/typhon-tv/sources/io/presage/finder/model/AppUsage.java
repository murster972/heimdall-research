package io.presage.finder.model;

import android.os.Parcel;
import android.os.Parcelable;
import io.presage.finder.IFinderResult;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class AppUsage implements IFinderResult, Serializable {
    public static final Parcelable.Creator<AppUsage> CREATOR = new Parcelable.Creator<AppUsage>() {
        /* renamed from: a */
        public AppUsage createFromParcel(Parcel parcel) {
            AppUsage appUsage = new AppUsage();
            appUsage.a(parcel.readString());
            appUsage.a(parcel.readLong());
            appUsage.b(parcel.readLong());
            return appUsage;
        }

        /* renamed from: a */
        public AppUsage[] newArray(int i) {
            return new AppUsage[i];
        }
    };
    private String a;
    private long b;
    private long c;

    public AppUsage() {
    }

    public AppUsage(String str, long j, long j2) {
        this.a = str;
        this.b = j;
        this.c = j2;
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("pname", (Object) this.a);
            jSONObject.put(TtmlNode.START, this.b);
            jSONObject.put(TtmlNode.END, this.c);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public void a(long j) {
        this.b = j;
    }

    public void a(String str) {
        this.a = str;
    }

    public String b() {
        return this.a;
    }

    public void b(long j) {
        this.c = j;
    }

    public long c() {
        return this.b;
    }

    public long d() {
        return this.c;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeLong(this.b);
        parcel.writeLong(this.c);
    }
}
