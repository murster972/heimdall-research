package io.presage.finder.model;

import android.os.Parcel;
import android.os.Parcelable;
import io.presage.finder.IFinderResult;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DnsResults implements IFinderResult {
    public static final Parcelable.Creator<DnsResults> CREATOR = new Parcelable.Creator<DnsResults>() {
        /* renamed from: a */
        public DnsResults createFromParcel(Parcel parcel) {
            return new DnsResults(parcel);
        }

        /* renamed from: a */
        public DnsResults[] newArray(int i) {
            return new DnsResults[i];
        }
    };
    private List<DnsResult> a;

    protected DnsResults(Parcel parcel) {
        this.a = parcel.createTypedArrayList(DnsResult.CREATOR);
    }

    public DnsResults(List<DnsResult> list) {
        this.a = list;
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < this.a.size(); i++) {
                jSONArray.put(i, (Object) new JSONObject(this.a.get(i).a()));
            }
            jSONObject.put("tasks", (Object) jSONArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.a);
    }
}
