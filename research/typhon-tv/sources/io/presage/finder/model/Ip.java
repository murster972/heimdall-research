package io.presage.finder.model;

import android.os.Parcel;
import android.os.Parcelable;
import io.presage.finder.IFinderResult;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class Ip implements IFinderResult, Serializable {
    public static final Parcelable.Creator<Ip> CREATOR = new Parcelable.Creator<Ip>() {
        /* renamed from: a */
        public Ip createFromParcel(Parcel parcel) {
            return new Ip(parcel);
        }

        /* renamed from: a */
        public Ip[] newArray(int i) {
            return new Ip[i];
        }
    };
    private Long a;
    private String b;

    protected Ip(Parcel parcel) {
        this.a = Long.valueOf(parcel.readLong());
        this.b = parcel.readString();
    }

    public Ip(Long l, String str) {
        this.a = l;
        this.b = str;
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("timestamp", (Object) this.a);
            jSONObject.put("ip_visit", (Object) this.b);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public Long b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.a.longValue());
        parcel.writeString(this.b);
    }
}
