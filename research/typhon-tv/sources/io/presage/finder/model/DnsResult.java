package io.presage.finder.model;

import android.os.Parcel;
import android.os.Parcelable;
import io.presage.finder.IFinderResult;
import java.util.List;
import org.apache.oltu.oauth2.common.OAuth;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DnsResult implements IFinderResult {
    public static final Parcelable.Creator<DnsResult> CREATOR = new Parcelable.Creator<DnsResult>() {
        /* renamed from: a */
        public DnsResult createFromParcel(Parcel parcel) {
            return new DnsResult(parcel);
        }

        /* renamed from: a */
        public DnsResult[] newArray(int i) {
            return new DnsResult[i];
        }
    };
    private String a;
    private Output b;

    public static class Ip implements IFinderResult {
        public static final Parcelable.Creator<Ip> CREATOR = new Parcelable.Creator<Ip>() {
            /* renamed from: a */
            public Ip createFromParcel(Parcel parcel) {
                return new Ip(parcel);
            }

            /* renamed from: a */
            public Ip[] newArray(int i) {
                return new Ip[i];
            }
        };
        private String a;
        private String b;
        private int c;
        private String d;

        protected Ip(Parcel parcel) {
            this.a = parcel.readString();
            this.b = parcel.readString();
            this.c = parcel.readInt();
            this.d = parcel.readString();
        }

        public Ip(String str, String str2, int i, String str3) {
            this.a = str;
            this.b = str2;
            this.c = i;
            this.d = str3;
        }

        public String a() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("host", (Object) this.a);
                jSONObject.put("ip", (Object) this.b);
                if (this.c != 0) {
                    jSONObject.put(OAuth.OAUTH_CODE, this.c);
                }
                if (this.d != null) {
                    jSONObject.put("error", (Object) this.d);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jSONObject.toString();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.a);
            parcel.writeString(this.b);
            parcel.writeInt(this.c);
            parcel.writeString(this.d);
        }
    }

    public static class Output implements IFinderResult {
        public static final Parcelable.Creator<Output> CREATOR = new Parcelable.Creator<Output>() {
            /* renamed from: a */
            public Output createFromParcel(Parcel parcel) {
                return new Output(parcel);
            }

            /* renamed from: a */
            public Output[] newArray(int i) {
                return new Output[i];
            }
        };
        private List<Ip> a;

        protected Output(Parcel parcel) {
            this.a = parcel.createTypedArrayList(Ip.CREATOR);
        }

        public Output(List<Ip> list) {
            this.a = list;
        }

        public String a() {
            JSONObject jSONObject = new JSONObject();
            try {
                JSONArray jSONArray = new JSONArray();
                for (int i = 0; i < this.a.size(); i++) {
                    jSONArray.put(i, (Object) new JSONObject(this.a.get(i).a()));
                }
                jSONObject.put("ips", (Object) jSONArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jSONObject.toString();
        }

        public List<Ip> b() {
            return this.a;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeTypedList(this.a);
        }
    }

    protected DnsResult(Parcel parcel) {
        this.a = parcel.readString();
        this.b = (Output) parcel.readParcelable(Output.class.getClassLoader());
    }

    public DnsResult(String str, Output output) {
        this.a = str;
        this.b = output;
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("id", (Object) this.a);
            jSONObject.put("output", (Object) new JSONObject(this.b.a()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public Output b() {
        return this.b;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeParcelable(this.b, i);
    }
}
