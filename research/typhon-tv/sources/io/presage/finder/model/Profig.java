package io.presage.finder.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mopub.mobileads.VastIconXmlManager;
import io.presage.finder.IFinderResult;
import org.json.JSONException;
import org.json.JSONObject;

public class Profig implements IFinderResult {
    public static final Parcelable.Creator<Profig> CREATOR = new Parcelable.Creator<Profig>() {
        /* renamed from: a */
        public Profig createFromParcel(Parcel parcel) {
            return new Profig(parcel);
        }

        /* renamed from: a */
        public Profig[] newArray(int i) {
            return new Profig[i];
        }
    };
    private boolean a = false;
    private String b;
    private String c;
    private String d;
    private String e;
    private boolean f = false;
    private Boolean g = null;
    private Device h;
    private boolean i = false;
    private boolean j = false;

    public static class Device implements IFinderResult {
        public static final Parcelable.Creator<Device> CREATOR = new Parcelable.Creator<Device>() {
            /* renamed from: a */
            public Device createFromParcel(Parcel parcel) {
                return new Device(parcel);
            }

            /* renamed from: a */
            public Device[] newArray(int i) {
                return new Device[i];
            }
        };
        private String a;
        private Screen b;
        private String c;
        private String d;
        private String e;
        private String f;

        public Device() {
        }

        protected Device(Parcel parcel) {
            this.a = parcel.readString();
            this.b = (Screen) parcel.readParcelable(Screen.class.getClassLoader());
            this.c = parcel.readString();
            this.d = parcel.readString();
            this.e = parcel.readString();
            this.f = parcel.readString();
        }

        public String a() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("name", (Object) this.a);
                jSONObject.put("screen", (Object) new JSONObject(this.b.a()));
                jSONObject.put("os_version", (Object) this.c);
                jSONObject.put("vm_name", (Object) this.d);
                jSONObject.put("phone_arch", (Object) this.e);
                jSONObject.put("vm_version", (Object) this.f);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            return jSONObject.toString();
        }

        public void a(Screen screen) {
            this.b = screen;
        }

        public void a(String str) {
            this.a = str;
        }

        public void b(String str) {
            this.c = str;
        }

        public void c(String str) {
            this.d = str;
        }

        public void d(String str) {
            this.e = str;
        }

        public int describeContents() {
            return 0;
        }

        public void e(String str) {
            this.f = str;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.a);
            parcel.writeParcelable(this.b, i);
            parcel.writeString(this.c);
            parcel.writeString(this.d);
            parcel.writeString(this.e);
            parcel.writeString(this.f);
        }
    }

    public static class Screen implements IFinderResult {
        public static final Parcelable.Creator<Screen> CREATOR = new Parcelable.Creator<Screen>() {
            /* renamed from: a */
            public Screen createFromParcel(Parcel parcel) {
                return new Screen(parcel);
            }

            /* renamed from: a */
            public Screen[] newArray(int i) {
                return new Screen[i];
            }
        };
        private int a = -1;
        private int b = -1;
        private float c = -1.0f;

        public Screen() {
        }

        public Screen(int i, int i2, float f) {
            this.a = i;
            this.b = i2;
            this.c = f;
        }

        protected Screen(Parcel parcel) {
            this.a = parcel.readInt();
            this.b = parcel.readInt();
            this.c = parcel.readFloat();
        }

        public String a() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(VastIconXmlManager.WIDTH, this.a);
                jSONObject.put(VastIconXmlManager.HEIGHT, this.b);
                jSONObject.put("density", (double) this.c);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jSONObject.toString();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeInt(this.b);
            parcel.writeFloat(this.c);
        }
    }

    public Profig() {
    }

    protected Profig(Parcel parcel) {
        boolean z = true;
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readByte() != 0;
        this.g = (Boolean) parcel.readValue(Boolean.class.getClassLoader());
        this.h = (Device) parcel.readParcelable(Device.class.getClassLoader());
        this.i = parcel.readByte() != 0;
        this.j = parcel.readByte() == 0 ? false : z;
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("timezone", (Object) this.b);
            jSONObject.put("aaid", (Object) this.c);
            jSONObject.put("language_code", (Object) this.d);
            jSONObject.put("country_code", (Object) this.e);
            jSONObject.put("install_unknown_sources", this.f);
            jSONObject.put("aaid_optin", (Object) this.g);
            jSONObject.put("device", (Object) new JSONObject(this.h.a()));
            jSONObject.put("fake_aaid", this.i);
            jSONObject.put("rooted", this.j);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject.toString();
    }

    public void a(Device device) {
        this.h = device;
    }

    public void a(Boolean bool) {
        this.g = bool;
    }

    public void a(String str) {
        this.b = str;
    }

    public void a(boolean z) {
        this.f = z;
    }

    public String b() {
        return this.c;
    }

    public void b(String str) {
        this.c = str;
    }

    public void b(boolean z) {
        this.i = z;
    }

    public void c(String str) {
        this.d = str;
    }

    public void c(boolean z) {
        this.j = z;
    }

    public void d(String str) {
        this.e = str;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        byte b2 = 1;
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeByte(this.f ? (byte) 1 : 0);
        parcel.writeValue(this.g);
        parcel.writeParcelable(this.h, i2);
        parcel.writeByte(this.i ? (byte) 1 : 0);
        if (!this.j) {
            b2 = 0;
        }
        parcel.writeByte(b2);
    }
}
