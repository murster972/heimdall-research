package io.presage.finder.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.finder.IFinderResult;
import org.json.JSONException;
import org.json.JSONObject;

public class Account implements IFinderResult {
    public static final Parcelable.Creator<Account> CREATOR = new Parcelable.Creator<Account>() {
        /* renamed from: a */
        public Account createFromParcel(Parcel parcel) {
            return new Account(parcel.readString(), parcel.readString());
        }

        /* renamed from: a */
        public Account[] newArray(int i) {
            return new Account[i];
        }
    };
    private String a;
    private String b;

    public Account(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", (Object) this.a);
            jSONObject.put(VastExtensionXmlManager.TYPE, (Object) this.b);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
    }
}
