package io.presage.finder.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mopub.mobileads.VastExtensionXmlManager;
import io.presage.finder.IFinderResult;
import org.json.JSONException;
import org.json.JSONObject;

public class IpTracker implements IFinderResult {
    public static final Parcelable.Creator<IpTracker> CREATOR = new Parcelable.Creator<IpTracker>() {
        /* renamed from: a */
        public IpTracker createFromParcel(Parcel parcel) {
            return new IpTracker(parcel);
        }

        /* renamed from: a */
        public IpTracker[] newArray(int i) {
            return new IpTracker[i];
        }
    };
    private Long a;
    private String b;

    public IpTracker() {
    }

    protected IpTracker(Parcel parcel) {
        this.a = Long.valueOf(parcel.readLong());
        this.b = parcel.readString();
    }

    public IpTracker(Long l, String str) {
        this.a = l;
        this.b = str;
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("timestamp", (Object) this.a);
            jSONObject.put(VastExtensionXmlManager.TYPE, (Object) this.b);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public void a(long j) {
        this.a = Long.valueOf(j);
    }

    public void a(String str) {
        this.b = str;
    }

    public Long b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.a.longValue());
        parcel.writeString(this.b);
    }
}
