package io.presage.finder.model;

import android.os.Parcel;
import android.os.Parcelable;
import io.presage.finder.IFinderResult;
import org.json.JSONException;
import org.json.JSONObject;

public class App implements IFinderResult {
    public static final Parcelable.Creator<App> CREATOR = new Parcelable.Creator<App>() {
        /* renamed from: a */
        public App createFromParcel(Parcel parcel) {
            boolean z = true;
            App app = new App();
            app.a(parcel.readString());
            app.b(parcel.readString());
            app.c(parcel.readString());
            app.a(parcel.readInt() == 1);
            if (parcel.readInt() != 1) {
                z = false;
            }
            app.b(z);
            return app;
        }

        /* renamed from: a */
        public App[] newArray(int i) {
            return new App[i];
        }
    };
    private String a;
    private String b;
    private String c;
    private boolean d;
    private boolean e;

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("package", (Object) this.a);
            jSONObject.put("version_name", (Object) this.b);
            jSONObject.put("version_code", (Object) this.c);
            jSONObject.put("system", this.d);
            jSONObject.put("launcher", this.e);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject.toString();
    }

    public void a(String str) {
        this.a = str;
    }

    public void a(boolean z) {
        this.d = z;
    }

    public void b(String str) {
        this.b = str;
    }

    public void b(boolean z) {
        this.e = z;
    }

    public void c(String str) {
        this.c = str;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeInt(this.d ? 1 : 0);
        if (!this.e) {
            i2 = 0;
        }
        parcel.writeInt(i2);
    }
}
