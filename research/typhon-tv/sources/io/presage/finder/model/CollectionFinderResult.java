package io.presage.finder.model;

import android.os.Parcel;
import android.os.Parcelable;
import io.presage.finder.IFinderResult;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CollectionFinderResult implements IFinderResult {
    public static final Parcelable.Creator<CollectionFinderResult> CREATOR = new Parcelable.Creator<CollectionFinderResult>() {
        /* renamed from: a */
        public CollectionFinderResult createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            CollectionFinderResult collectionFinderResult = new CollectionFinderResult(readString);
            ArrayList arrayList = new ArrayList();
            char c = 65535;
            switch (readString.hashCode()) {
                case -535854154:
                    if (readString.equals("android_accounts")) {
                        c = 1;
                        break;
                    }
                    break;
                case 104492:
                    if (readString.equals("ips")) {
                        c = 2;
                        break;
                    }
                    break;
                case 3000946:
                    if (readString.equals("apps")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    for (App add : parcel.createTypedArrayList(App.CREATOR)) {
                        arrayList.add(add);
                    }
                    break;
                case 1:
                    for (Account add2 : parcel.createTypedArrayList(Account.CREATOR)) {
                        arrayList.add(add2);
                    }
                    break;
                case 2:
                    for (Ip add3 : parcel.createTypedArrayList(Ip.CREATOR)) {
                        arrayList.add(add3);
                    }
                    break;
            }
            collectionFinderResult.a((List<IFinderResult>) arrayList);
            return collectionFinderResult;
        }

        /* renamed from: a */
        public CollectionFinderResult[] newArray(int i) {
            return new CollectionFinderResult[i];
        }
    };
    private String a;
    private List<IFinderResult> b = new ArrayList();

    public CollectionFinderResult(String str) {
        this.a = str;
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONArray jSONArray = new JSONArray();
            for (IFinderResult a2 : this.b) {
                jSONArray.put((Object) new JSONObject(a2.a()));
            }
            jSONObject.put("content", (Object) jSONArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public void a(IFinderResult iFinderResult) {
        this.b.add(iFinderResult);
    }

    public void a(List<IFinderResult> list) {
        this.b = list;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeTypedList(this.b);
    }
}
