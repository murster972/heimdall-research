package android.p000do.p001do.p002do;

import android.content.Context;
import android.os.Process;
import android.support.v4.app.AppOpsManagerCompat;

/* renamed from: android.do.do.do.BenimaruNikaido  reason: invalid package */
public final class BenimaruNikaido {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m53(Context context, String str) {
        return m54(context, str, Process.myPid(), Process.myUid(), context.getPackageName());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m54(Context context, String str, int i, int i2, String str2) {
        if (context.checkPermission(str, i, i2) == -1) {
            return -1;
        }
        String permissionToOp = AppOpsManagerCompat.permissionToOp(str);
        if (permissionToOp == null) {
            return 0;
        }
        if (str2 == null) {
            String[] packagesForUid = context.getPackageManager().getPackagesForUid(i2);
            if (packagesForUid == null || packagesForUid.length <= 0) {
                return -1;
            }
            str2 = packagesForUid[0];
        }
        return AppOpsManagerCompat.noteProxyOp(context, permissionToOp, str2) != 0 ? -2 : 0;
    }
}
