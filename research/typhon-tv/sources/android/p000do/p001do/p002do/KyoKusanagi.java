package android.p000do.p001do.p002do;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: android.do.do.do.KyoKusanagi  reason: invalid package */
public final class KyoKusanagi {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Object f40 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static KyoKusanagi f41;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Handler f42;

    /* renamed from: 靐  reason: contains not printable characters */
    private final HashMap<BroadcastReceiver, ArrayList<IntentFilter>> f43 = new HashMap<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private final ArrayList<C0000KyoKusanagi> f44 = new ArrayList<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private final HashMap<String, ArrayList<BenimaruNikaido>> f45 = new HashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f46;

    /* renamed from: android.do.do.do.KyoKusanagi$BenimaruNikaido */
    private static class BenimaruNikaido {

        /* renamed from: 靐  reason: contains not printable characters */
        final BroadcastReceiver f48;

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f49;

        /* renamed from: 龘  reason: contains not printable characters */
        final IntentFilter f50;

        BenimaruNikaido(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {
            this.f50 = intentFilter;
            this.f48 = broadcastReceiver;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(128);
            sb.append("Receiver{");
            sb.append(this.f48);
            sb.append(" filter=");
            sb.append(this.f50);
            sb.append("}");
            return sb.toString();
        }
    }

    /* renamed from: android.do.do.do.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    private static class C0000KyoKusanagi {

        /* renamed from: 靐  reason: contains not printable characters */
        final ArrayList<BenimaruNikaido> f51;

        /* renamed from: 龘  reason: contains not printable characters */
        final Intent f52;

        C0000KyoKusanagi(Intent intent, ArrayList<BenimaruNikaido> arrayList) {
            this.f52 = intent;
            this.f51 = arrayList;
        }
    }

    private KyoKusanagi(Context context) {
        this.f46 = context;
        this.f42 = new Handler(context.getMainLooper()) {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1:
                        KyoKusanagi.this.m56();
                        return;
                    default:
                        super.handleMessage(message);
                        return;
                }
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static KyoKusanagi m55(Context context) {
        KyoKusanagi kyoKusanagi;
        synchronized (f40) {
            if (f41 == null) {
                f41 = new KyoKusanagi(context.getApplicationContext());
            }
            kyoKusanagi = f41;
        }
        return kyoKusanagi;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        if (r3 >= r4.length) goto L_0x0001;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        r5 = r4[r3];
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0028, code lost:
        if (r1 >= r5.f51.size()) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        r5.f51.get(r1).f48.onReceive(r8.f46, r5.f52);
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0042, code lost:
        r3 = r3 + 1;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m56() {
        /*
            r8 = this;
            r2 = 0
        L_0x0001:
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<android.content.IntentFilter>> r1 = r8.f43
            monitor-enter(r1)
            java.util.ArrayList<android.do.do.do.KyoKusanagi$KyoKusanagi> r0 = r8.f44     // Catch:{ all -> 0x003f }
            int r0 = r0.size()     // Catch:{ all -> 0x003f }
            if (r0 > 0) goto L_0x000e
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            return
        L_0x000e:
            android.do.do.do.KyoKusanagi$KyoKusanagi[] r4 = new android.p000do.p001do.p002do.KyoKusanagi.C0000KyoKusanagi[r0]     // Catch:{ all -> 0x003f }
            java.util.ArrayList<android.do.do.do.KyoKusanagi$KyoKusanagi> r0 = r8.f44     // Catch:{ all -> 0x003f }
            r0.toArray(r4)     // Catch:{ all -> 0x003f }
            java.util.ArrayList<android.do.do.do.KyoKusanagi$KyoKusanagi> r0 = r8.f44     // Catch:{ all -> 0x003f }
            r0.clear()     // Catch:{ all -> 0x003f }
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            r3 = r2
        L_0x001c:
            int r0 = r4.length
            if (r3 >= r0) goto L_0x0001
            r5 = r4[r3]
            r1 = r2
        L_0x0022:
            java.util.ArrayList<android.do.do.do.KyoKusanagi$BenimaruNikaido> r0 = r5.f51
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x0042
            java.util.ArrayList<android.do.do.do.KyoKusanagi$BenimaruNikaido> r0 = r5.f51
            java.lang.Object r0 = r0.get(r1)
            android.do.do.do.KyoKusanagi$BenimaruNikaido r0 = (android.p000do.p001do.p002do.KyoKusanagi.BenimaruNikaido) r0
            android.content.BroadcastReceiver r0 = r0.f48
            android.content.Context r6 = r8.f46
            android.content.Intent r7 = r5.f52
            r0.onReceive(r6, r7)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0022
        L_0x003f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            throw r0
        L_0x0042:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: android.p000do.p001do.p002do.KyoKusanagi.m56():void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m58(BroadcastReceiver broadcastReceiver) {
        int i;
        synchronized (this.f43) {
            ArrayList remove = this.f43.remove(broadcastReceiver);
            if (remove != null) {
                for (int i2 = 0; i2 < remove.size(); i2++) {
                    IntentFilter intentFilter = (IntentFilter) remove.get(i2);
                    for (int i3 = 0; i3 < intentFilter.countActions(); i3++) {
                        String action = intentFilter.getAction(i3);
                        ArrayList arrayList = this.f45.get(action);
                        if (arrayList != null) {
                            int i4 = 0;
                            while (i4 < arrayList.size()) {
                                if (((BenimaruNikaido) arrayList.get(i4)).f48 == broadcastReceiver) {
                                    arrayList.remove(i4);
                                    i = i4 - 1;
                                } else {
                                    i = i4;
                                }
                                i4 = i + 1;
                            }
                            if (arrayList.size() <= 0) {
                                this.f45.remove(action);
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m59(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        synchronized (this.f43) {
            BenimaruNikaido benimaruNikaido = new BenimaruNikaido(intentFilter, broadcastReceiver);
            ArrayList arrayList = this.f43.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new ArrayList(1);
                this.f43.put(broadcastReceiver, arrayList);
            }
            arrayList.add(intentFilter);
            for (int i = 0; i < intentFilter.countActions(); i++) {
                String action = intentFilter.getAction(i);
                ArrayList arrayList2 = this.f45.get(action);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList(1);
                    this.f45.put(action, arrayList2);
                }
                arrayList2.add(benimaruNikaido);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0183, code lost:
        return false;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m60(android.content.Intent r17) {
        /*
            r16 = this;
            r0 = r16
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<android.content.IntentFilter>> r13 = r0.f43
            monitor-enter(r13)
            java.lang.String r2 = r17.getAction()     // Catch:{ all -> 0x0110 }
            r0 = r16
            android.content.Context r1 = r0.f46     // Catch:{ all -> 0x0110 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ all -> 0x0110 }
            r0 = r17
            java.lang.String r3 = r0.resolveTypeIfNeeded(r1)     // Catch:{ all -> 0x0110 }
            android.net.Uri r5 = r17.getData()     // Catch:{ all -> 0x0110 }
            java.lang.String r4 = r17.getScheme()     // Catch:{ all -> 0x0110 }
            java.util.Set r6 = r17.getCategories()     // Catch:{ all -> 0x0110 }
            int r1 = r17.getFlags()     // Catch:{ all -> 0x0110 }
            r1 = r1 & 8
            if (r1 == 0) goto L_0x00d3
            r1 = 1
            r12 = r1
        L_0x002d:
            if (r12 == 0) goto L_0x0061
            java.lang.String r1 = "LocalBroadcastManager"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0110 }
            r7.<init>()     // Catch:{ all -> 0x0110 }
            java.lang.String r8 = "Resolving type "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r7 = r7.append(r3)     // Catch:{ all -> 0x0110 }
            java.lang.String r8 = " scheme "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ all -> 0x0110 }
            java.lang.String r8 = " of intent "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0110 }
            r0 = r17
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ all -> 0x0110 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0110 }
            android.util.Log.v(r1, r7)     // Catch:{ all -> 0x0110 }
        L_0x0061:
            r0 = r16
            java.util.HashMap<java.lang.String, java.util.ArrayList<android.do.do.do.KyoKusanagi$BenimaruNikaido>> r1 = r0.f45     // Catch:{ all -> 0x0110 }
            java.lang.String r7 = r17.getAction()     // Catch:{ all -> 0x0110 }
            java.lang.Object r1 = r1.get(r7)     // Catch:{ all -> 0x0110 }
            r0 = r1
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x0110 }
            r8 = r0
            if (r8 == 0) goto L_0x0182
            if (r12 == 0) goto L_0x008f
            java.lang.String r1 = "LocalBroadcastManager"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0110 }
            r7.<init>()     // Catch:{ all -> 0x0110 }
            java.lang.String r9 = "Action list: "
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0110 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0110 }
            android.util.Log.v(r1, r7)     // Catch:{ all -> 0x0110 }
        L_0x008f:
            r10 = 0
            r1 = 0
            r11 = r1
        L_0x0092:
            int r1 = r8.size()     // Catch:{ all -> 0x0110 }
            if (r11 >= r1) goto L_0x0147
            java.lang.Object r1 = r8.get(r11)     // Catch:{ all -> 0x0110 }
            r0 = r1
            android.do.do.do.KyoKusanagi$BenimaruNikaido r0 = (android.p000do.p001do.p002do.KyoKusanagi.BenimaruNikaido) r0     // Catch:{ all -> 0x0110 }
            r9 = r0
            if (r12 == 0) goto L_0x00be
            java.lang.String r1 = "LocalBroadcastManager"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0110 }
            r7.<init>()     // Catch:{ all -> 0x0110 }
            java.lang.String r14 = "Matching against filter "
            java.lang.StringBuilder r7 = r7.append(r14)     // Catch:{ all -> 0x0110 }
            android.content.IntentFilter r14 = r9.f50     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r7 = r7.append(r14)     // Catch:{ all -> 0x0110 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0110 }
            android.util.Log.v(r1, r7)     // Catch:{ all -> 0x0110 }
        L_0x00be:
            boolean r1 = r9.f49     // Catch:{ all -> 0x0110 }
            if (r1 == 0) goto L_0x00d7
            if (r12 == 0) goto L_0x0135
            java.lang.String r1 = "LocalBroadcastManager"
            java.lang.String r7 = "  Filter's target already added"
            android.util.Log.v(r1, r7)     // Catch:{ all -> 0x0110 }
            r1 = r10
        L_0x00ce:
            int r7 = r11 + 1
            r11 = r7
            r10 = r1
            goto L_0x0092
        L_0x00d3:
            r1 = 0
            r12 = r1
            goto L_0x002d
        L_0x00d7:
            android.content.IntentFilter r1 = r9.f50     // Catch:{ all -> 0x0110 }
            java.lang.String r7 = "LocalBroadcastManager"
            int r1 = r1.match(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0110 }
            if (r1 < 0) goto L_0x0113
            if (r12 == 0) goto L_0x0102
            java.lang.String r7 = "LocalBroadcastManager"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x0110 }
            r14.<init>()     // Catch:{ all -> 0x0110 }
            java.lang.String r15 = "  Filter matched!  match=0x"
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ all -> 0x0110 }
            java.lang.String r1 = java.lang.Integer.toHexString(r1)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r1 = r14.append(r1)     // Catch:{ all -> 0x0110 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0110 }
            android.util.Log.v(r7, r1)     // Catch:{ all -> 0x0110 }
        L_0x0102:
            if (r10 != 0) goto L_0x0185
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0110 }
            r1.<init>()     // Catch:{ all -> 0x0110 }
        L_0x0109:
            r1.add(r9)     // Catch:{ all -> 0x0110 }
            r7 = 1
            r9.f49 = r7     // Catch:{ all -> 0x0110 }
            goto L_0x00ce
        L_0x0110:
            r1 = move-exception
            monitor-exit(r13)     // Catch:{ all -> 0x0110 }
            throw r1
        L_0x0113:
            if (r12 == 0) goto L_0x0135
            switch(r1) {
                case -4: goto L_0x013b;
                case -3: goto L_0x0137;
                case -2: goto L_0x013f;
                case -1: goto L_0x0143;
                default: goto L_0x0118;
            }
        L_0x0118:
            java.lang.String r1 = "unknown reason"
        L_0x011b:
            java.lang.String r7 = "LocalBroadcastManager"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0110 }
            r9.<init>()     // Catch:{ all -> 0x0110 }
            java.lang.String r14 = "  Filter did not match: "
            java.lang.StringBuilder r9 = r9.append(r14)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r1 = r9.append(r1)     // Catch:{ all -> 0x0110 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0110 }
            android.util.Log.v(r7, r1)     // Catch:{ all -> 0x0110 }
        L_0x0135:
            r1 = r10
            goto L_0x00ce
        L_0x0137:
            java.lang.String r1 = "action"
            goto L_0x011b
        L_0x013b:
            java.lang.String r1 = "category"
            goto L_0x011b
        L_0x013f:
            java.lang.String r1 = "data"
            goto L_0x011b
        L_0x0143:
            java.lang.String r1 = "type"
            goto L_0x011b
        L_0x0147:
            if (r10 == 0) goto L_0x0182
            r1 = 0
            r2 = r1
        L_0x014b:
            int r1 = r10.size()     // Catch:{ all -> 0x0110 }
            if (r2 >= r1) goto L_0x015e
            java.lang.Object r1 = r10.get(r2)     // Catch:{ all -> 0x0110 }
            android.do.do.do.KyoKusanagi$BenimaruNikaido r1 = (android.p000do.p001do.p002do.KyoKusanagi.BenimaruNikaido) r1     // Catch:{ all -> 0x0110 }
            r3 = 0
            r1.f49 = r3     // Catch:{ all -> 0x0110 }
            int r1 = r2 + 1
            r2 = r1
            goto L_0x014b
        L_0x015e:
            r0 = r16
            java.util.ArrayList<android.do.do.do.KyoKusanagi$KyoKusanagi> r1 = r0.f44     // Catch:{ all -> 0x0110 }
            android.do.do.do.KyoKusanagi$KyoKusanagi r2 = new android.do.do.do.KyoKusanagi$KyoKusanagi     // Catch:{ all -> 0x0110 }
            r0 = r17
            r2.<init>(r0, r10)     // Catch:{ all -> 0x0110 }
            r1.add(r2)     // Catch:{ all -> 0x0110 }
            r0 = r16
            android.os.Handler r1 = r0.f42     // Catch:{ all -> 0x0110 }
            r2 = 1
            boolean r1 = r1.hasMessages(r2)     // Catch:{ all -> 0x0110 }
            if (r1 != 0) goto L_0x017f
            r0 = r16
            android.os.Handler r1 = r0.f42     // Catch:{ all -> 0x0110 }
            r2 = 1
            r1.sendEmptyMessage(r2)     // Catch:{ all -> 0x0110 }
        L_0x017f:
            r1 = 1
            monitor-exit(r13)     // Catch:{ all -> 0x0110 }
        L_0x0181:
            return r1
        L_0x0182:
            monitor-exit(r13)     // Catch:{ all -> 0x0110 }
            r1 = 0
            goto L_0x0181
        L_0x0185:
            r1 = r10
            goto L_0x0109
        */
        throw new UnsupportedOperationException("Method not decompiled: android.p000do.p001do.p002do.KyoKusanagi.m60(android.content.Intent):boolean");
    }
}
