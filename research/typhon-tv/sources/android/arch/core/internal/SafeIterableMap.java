package android.arch.core.internal;

import android.support.annotation.RestrictTo;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

@RestrictTo
public class SafeIterableMap<K, V> implements Iterable<Map.Entry<K, V>> {

    /* renamed from: 靐  reason: contains not printable characters */
    private Entry<K, V> f1;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f2 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    private WeakHashMap<Object<K, V>, Boolean> f3 = new WeakHashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public Entry<K, V> f4;

    static class AscendingIterator<K, V> extends ListIterator<K, V> {
        AscendingIterator(Entry<K, V> entry, Entry<K, V> entry2) {
            super(entry, entry2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Entry<K, V> m7(Entry<K, V> entry) {
            return entry.f7;
        }
    }

    private static class DescendingIterator<K, V> extends ListIterator<K, V> {
        DescendingIterator(Entry<K, V> entry, Entry<K, V> entry2) {
            super(entry, entry2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Entry<K, V> m8(Entry<K, V> entry) {
            return entry.f6;
        }
    }

    static class Entry<K, V> implements Map.Entry<K, V> {

        /* renamed from: 靐  reason: contains not printable characters */
        final V f5;

        /* renamed from: 麤  reason: contains not printable characters */
        Entry<K, V> f6;

        /* renamed from: 齉  reason: contains not printable characters */
        Entry<K, V> f7;

        /* renamed from: 龘  reason: contains not printable characters */
        final K f8;

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Entry)) {
                return false;
            }
            Entry entry = (Entry) obj;
            return this.f8.equals(entry.f8) && this.f5.equals(entry.f5);
        }

        public K getKey() {
            return this.f8;
        }

        public V getValue() {
            return this.f5;
        }

        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        public String toString() {
            return this.f8 + "=" + this.f5;
        }
    }

    private class IteratorWithAdditions implements Iterator<Map.Entry<K, V>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private Entry<K, V> f9;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f10;

        private IteratorWithAdditions() {
            this.f10 = true;
        }

        public boolean hasNext() {
            return this.f10 ? SafeIterableMap.this.f4 != null : (this.f9 == null || this.f9.f7 == null) ? false : true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Map.Entry<K, V> next() {
            if (this.f10) {
                this.f10 = false;
                this.f9 = SafeIterableMap.this.f4;
            } else {
                this.f9 = this.f9 != null ? this.f9.f7 : null;
            }
            return this.f9;
        }
    }

    private static abstract class ListIterator<K, V> implements Iterator<Map.Entry<K, V>> {

        /* renamed from: 靐  reason: contains not printable characters */
        Entry<K, V> f12;

        /* renamed from: 龘  reason: contains not printable characters */
        Entry<K, V> f13;

        ListIterator(Entry<K, V> entry, Entry<K, V> entry2) {
            this.f13 = entry2;
            this.f12 = entry;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private Entry<K, V> m10() {
            if (this.f12 == this.f13 || this.f13 == null) {
                return null;
            }
            return m11(this.f12);
        }

        public boolean hasNext() {
            return this.f12 != null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract Entry<K, V> m11(Entry<K, V> entry);

        /* renamed from: 龘  reason: contains not printable characters */
        public Map.Entry<K, V> next() {
            Entry<K, V> entry = this.f12;
            this.f12 = m10();
            return entry;
        }
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SafeIterableMap)) {
            return false;
        }
        SafeIterableMap safeIterableMap = (SafeIterableMap) obj;
        if (m6() != safeIterableMap.m6()) {
            return false;
        }
        Iterator it2 = iterator();
        Iterator it3 = safeIterableMap.iterator();
        while (it2.hasNext() && it3.hasNext()) {
            Map.Entry entry = (Map.Entry) it2.next();
            Object next = it3.next();
            if (entry == null && next != null) {
                return false;
            }
            if (entry != null && !entry.equals(next)) {
                return false;
            }
        }
        if (it2.hasNext() || it3.hasNext()) {
            z = false;
        }
        return z;
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        AscendingIterator ascendingIterator = new AscendingIterator(this.f4, this.f1);
        this.f3.put(ascendingIterator, false);
        return ascendingIterator;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            sb.append(((Map.Entry) it2.next()).toString());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Map.Entry<K, V> m2() {
        return this.f1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Iterator<Map.Entry<K, V>> m3() {
        DescendingIterator descendingIterator = new DescendingIterator(this.f1, this.f4);
        this.f3.put(descendingIterator, false);
        return descendingIterator;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Map.Entry<K, V> m4() {
        return this.f4;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public SafeIterableMap<K, V>.IteratorWithAdditions m5() {
        SafeIterableMap<K, V>.IteratorWithAdditions iteratorWithAdditions = new IteratorWithAdditions();
        this.f3.put(iteratorWithAdditions, false);
        return iteratorWithAdditions;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m6() {
        return this.f2;
    }
}
