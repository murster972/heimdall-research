package android.arch.core.internal;

import android.arch.core.internal.SafeIterableMap;
import android.support.annotation.RestrictTo;
import java.util.HashMap;

@RestrictTo
public class FastSafeIterableMap<K, V> extends SafeIterableMap<K, V> {

    /* renamed from: 龘  reason: contains not printable characters */
    private HashMap<K, SafeIterableMap.Entry<K, V>> f0 = new HashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m0(K k) {
        return this.f0.containsKey(k);
    }
}
