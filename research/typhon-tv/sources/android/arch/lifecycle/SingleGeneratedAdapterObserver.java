package android.arch.lifecycle;

import android.arch.lifecycle.Lifecycle;
import android.support.annotation.RestrictTo;

@RestrictTo
public class SingleGeneratedAdapterObserver implements GenericLifecycleObserver {

    /* renamed from: 龘  reason: contains not printable characters */
    private final GeneratedAdapter f39;

    /* renamed from: 龘  reason: contains not printable characters */
    public void m52(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
        this.f39.m24(lifecycleOwner, event, false, (MethodCallsLogger) null);
        this.f39.m24(lifecycleOwner, event, true, (MethodCallsLogger) null);
    }
}
