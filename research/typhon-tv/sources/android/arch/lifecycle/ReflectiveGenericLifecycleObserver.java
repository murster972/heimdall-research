package android.arch.lifecycle;

import android.arch.lifecycle.ClassesInfoCache;
import android.arch.lifecycle.Lifecycle;

class ReflectiveGenericLifecycleObserver implements GenericLifecycleObserver {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ClassesInfoCache.CallbackInfo f36;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f37;

    /* renamed from: 龘  reason: contains not printable characters */
    public void m43(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
        this.f36.m14(lifecycleOwner, event, this.f37);
    }
}
