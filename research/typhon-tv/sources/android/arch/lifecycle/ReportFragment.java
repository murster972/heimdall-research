package android.arch.lifecycle;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.arch.lifecycle.Lifecycle;
import android.os.Bundle;
import android.support.annotation.RestrictTo;

@RestrictTo
public class ReportFragment extends Fragment {

    /* renamed from: 龘  reason: contains not printable characters */
    private ActivityInitializationListener f38;

    interface ActivityInitializationListener {
        /* renamed from: 靐  reason: contains not printable characters */
        void m49();

        /* renamed from: 齉  reason: contains not printable characters */
        void m50();

        /* renamed from: 龘  reason: contains not printable characters */
        void m51();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m44(ActivityInitializationListener activityInitializationListener) {
        if (activityInitializationListener != null) {
            activityInitializationListener.m49();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m45(ActivityInitializationListener activityInitializationListener) {
        if (activityInitializationListener != null) {
            activityInitializationListener.m50();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m46(Activity activity) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("android.arch.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add(new ReportFragment(), "android.arch.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m47(Lifecycle.Event event) {
        Activity activity = getActivity();
        if (activity instanceof LifecycleRegistryOwner) {
            ((LifecycleRegistryOwner) activity).m42().m39(event);
        } else if (activity instanceof LifecycleOwner) {
            Lifecycle lifecycle = ((LifecycleOwner) activity).getLifecycle();
            if (lifecycle instanceof LifecycleRegistry) {
                ((LifecycleRegistry) lifecycle).m39(event);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m48(ActivityInitializationListener activityInitializationListener) {
        if (activityInitializationListener != null) {
            activityInitializationListener.m51();
        }
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        m48(this.f38);
        m47(Lifecycle.Event.ON_CREATE);
    }

    public void onDestroy() {
        super.onDestroy();
        m47(Lifecycle.Event.ON_DESTROY);
        this.f38 = null;
    }

    public void onPause() {
        super.onPause();
        m47(Lifecycle.Event.ON_PAUSE);
    }

    public void onResume() {
        super.onResume();
        m45(this.f38);
        m47(Lifecycle.Event.ON_RESUME);
    }

    public void onStart() {
        super.onStart();
        m44(this.f38);
        m47(Lifecycle.Event.ON_START);
    }

    public void onStop() {
        super.onStop();
        m47(Lifecycle.Event.ON_STOP);
    }
}
