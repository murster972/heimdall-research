package android.arch.lifecycle;

import android.arch.lifecycle.Lifecycle;
import android.support.annotation.RestrictTo;

@RestrictTo
public interface GeneratedAdapter {
    /* renamed from: 龘  reason: contains not printable characters */
    void m24(LifecycleOwner lifecycleOwner, Lifecycle.Event event, boolean z, MethodCallsLogger methodCallsLogger);
}
