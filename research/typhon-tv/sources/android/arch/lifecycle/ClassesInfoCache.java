package android.arch.lifecycle;

import android.arch.lifecycle.Lifecycle;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ClassesInfoCache {

    /* renamed from: 龘  reason: contains not printable characters */
    static ClassesInfoCache f14 = new ClassesInfoCache();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<Class, CallbackInfo> f15 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Map<Class, Boolean> f16 = new HashMap();

    static class CallbackInfo {

        /* renamed from: 龘  reason: contains not printable characters */
        final Map<Lifecycle.Event, List<MethodReference>> f17;

        /* renamed from: 龘  reason: contains not printable characters */
        private static void m13(List<MethodReference> list, LifecycleOwner lifecycleOwner, Lifecycle.Event event, Object obj) {
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    list.get(size).m15(lifecycleOwner, event, obj);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m14(LifecycleOwner lifecycleOwner, Lifecycle.Event event, Object obj) {
            m13(this.f17.get(event), lifecycleOwner, event, obj);
            m13(this.f17.get(Lifecycle.Event.ON_ANY), lifecycleOwner, event, obj);
        }
    }

    static class MethodReference {

        /* renamed from: 靐  reason: contains not printable characters */
        final Method f18;

        /* renamed from: 龘  reason: contains not printable characters */
        final int f19;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            MethodReference methodReference = (MethodReference) obj;
            return this.f19 == methodReference.f19 && this.f18.getName().equals(methodReference.f18.getName());
        }

        public int hashCode() {
            return (this.f19 * 31) + this.f18.getName().hashCode();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m15(LifecycleOwner lifecycleOwner, Lifecycle.Event event, Object obj) {
            try {
                switch (this.f19) {
                    case 0:
                        this.f18.invoke(obj, new Object[0]);
                        return;
                    case 1:
                        this.f18.invoke(obj, new Object[]{lifecycleOwner});
                        return;
                    case 2:
                        this.f18.invoke(obj, new Object[]{lifecycleOwner, event});
                        return;
                    default:
                        return;
                }
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Failed to call observer method", e.getCause());
            } catch (IllegalAccessException e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    ClassesInfoCache() {
    }
}
