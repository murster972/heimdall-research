package android.arch.lifecycle;

import android.arch.lifecycle.Lifecycle;

class FullLifecycleObserverAdapter implements GenericLifecycleObserver {

    /* renamed from: 龘  reason: contains not printable characters */
    private final FullLifecycleObserver f21;

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
        switch (event) {
            case ON_CREATE:
                this.f21.m22(lifecycleOwner);
                return;
            case ON_START:
                this.f21.m19(lifecycleOwner);
                return;
            case ON_RESUME:
                this.f21.m21(lifecycleOwner);
                return;
            case ON_PAUSE:
                this.f21.m20(lifecycleOwner);
                return;
            case ON_STOP:
                this.f21.m18(lifecycleOwner);
                return;
            case ON_DESTROY:
                this.f21.m17(lifecycleOwner);
                return;
            case ON_ANY:
                throw new IllegalArgumentException("ON_ANY must not been send by anybody");
            default:
                return;
        }
    }
}
