package android.arch.lifecycle;

import android.arch.core.internal.FastSafeIterableMap;
import android.arch.core.internal.SafeIterableMap;
import android.arch.lifecycle.Lifecycle;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class LifecycleRegistry extends Lifecycle {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f24 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ArrayList<Lifecycle.State> f25 = new ArrayList<>();

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f26 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private Lifecycle.State f27;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f28 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    private final WeakReference<LifecycleOwner> f29;

    /* renamed from: 龘  reason: contains not printable characters */
    private FastSafeIterableMap<Object, ObserverWithState> f30 = new FastSafeIterableMap<>();

    static class ObserverWithState {

        /* renamed from: 靐  reason: contains not printable characters */
        GenericLifecycleObserver f33;

        /* renamed from: 龘  reason: contains not printable characters */
        Lifecycle.State f34;

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m41(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
            Lifecycle.State r0 = LifecycleRegistry.m28(event);
            this.f34 = LifecycleRegistry.m36(this.f34, r0);
            this.f33.m25(lifecycleOwner, event);
            this.f34 = r0;
        }
    }

    public LifecycleRegistry(LifecycleOwner lifecycleOwner) {
        this.f29 = new WeakReference<>(lifecycleOwner);
        this.f27 = Lifecycle.State.INITIALIZED;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static Lifecycle.Event m27(Lifecycle.State state) {
        switch (state) {
            case INITIALIZED:
            case DESTROYED:
                return Lifecycle.Event.ON_CREATE;
            case CREATED:
                return Lifecycle.Event.ON_START;
            case STARTED:
                return Lifecycle.Event.ON_RESUME;
            case RESUMED:
                throw new IllegalArgumentException();
            default:
                throw new IllegalArgumentException("Unexpected state value " + state);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static Lifecycle.State m28(Lifecycle.Event event) {
        switch (event) {
            case ON_CREATE:
            case ON_STOP:
                return Lifecycle.State.CREATED;
            case ON_START:
            case ON_PAUSE:
                return Lifecycle.State.STARTED;
            case ON_RESUME:
                return Lifecycle.State.RESUMED;
            case ON_DESTROY:
                return Lifecycle.State.DESTROYED;
            default:
                throw new IllegalArgumentException("Unexpected event value " + event);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m29(Lifecycle.State state) {
        if (this.f27 != state) {
            this.f27 = state;
            if (this.f26 || this.f28 != 0) {
                this.f24 = true;
                return;
            }
            this.f26 = true;
            m33();
            this.f26 = false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m30(LifecycleOwner lifecycleOwner) {
        Iterator<Map.Entry<Object, ObserverWithState>> r0 = this.f30.m3();
        while (r0.hasNext() && !this.f24) {
            Map.Entry next = r0.next();
            ObserverWithState observerWithState = (ObserverWithState) next.getValue();
            while (observerWithState.f34.compareTo(this.f27) > 0 && !this.f24 && this.f30.m0(next.getKey())) {
                Lifecycle.Event r2 = m32(observerWithState.f34);
                m35(m28(r2));
                observerWithState.m41(lifecycleOwner, r2);
                m34();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m31() {
        if (this.f30.m6() == 0) {
            return true;
        }
        Lifecycle.State state = this.f30.m4().getValue().f34;
        Lifecycle.State state2 = this.f30.m2().getValue().f34;
        return state == state2 && this.f27 == state2;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static Lifecycle.Event m32(Lifecycle.State state) {
        switch (state) {
            case INITIALIZED:
                throw new IllegalArgumentException();
            case CREATED:
                return Lifecycle.Event.ON_DESTROY;
            case STARTED:
                return Lifecycle.Event.ON_STOP;
            case RESUMED:
                return Lifecycle.Event.ON_PAUSE;
            case DESTROYED:
                throw new IllegalArgumentException();
            default:
                throw new IllegalArgumentException("Unexpected state value " + state);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m33() {
        LifecycleOwner lifecycleOwner = (LifecycleOwner) this.f29.get();
        if (lifecycleOwner == null) {
            Log.w("LifecycleRegistry", "LifecycleOwner is garbage collected, you shouldn't try dispatch new events from it.");
            return;
        }
        while (!m31()) {
            this.f24 = false;
            if (this.f27.compareTo(this.f30.m4().getValue().f34) < 0) {
                m30(lifecycleOwner);
            }
            Map.Entry<Object, ObserverWithState> r1 = this.f30.m2();
            if (!this.f24 && r1 != null && this.f27.compareTo(r1.getValue().f34) > 0) {
                m37(lifecycleOwner);
            }
        }
        this.f24 = false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m34() {
        this.f25.remove(this.f25.size() - 1);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m35(Lifecycle.State state) {
        this.f25.add(state);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Lifecycle.State m36(Lifecycle.State state, Lifecycle.State state2) {
        return (state2 == null || state2.compareTo(state) >= 0) ? state : state2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m37(LifecycleOwner lifecycleOwner) {
        SafeIterableMap<K, V>.IteratorWithAdditions r0 = this.f30.m5();
        while (r0.hasNext() && !this.f24) {
            Map.Entry entry = (Map.Entry) r0.next();
            ObserverWithState observerWithState = (ObserverWithState) entry.getValue();
            while (observerWithState.f34.compareTo(this.f27) < 0 && !this.f24 && this.f30.m0(entry.getKey())) {
                m35(observerWithState.f34);
                observerWithState.m41(lifecycleOwner, m27(observerWithState.f34));
                m34();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Lifecycle.State m38() {
        return this.f27;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m39(Lifecycle.Event event) {
        m29(m28(event));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m40(Lifecycle.State state) {
        m29(state);
    }
}
