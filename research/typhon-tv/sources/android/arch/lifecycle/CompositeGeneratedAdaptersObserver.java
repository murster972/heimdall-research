package android.arch.lifecycle;

import android.arch.lifecycle.Lifecycle;
import android.support.annotation.RestrictTo;

@RestrictTo
public class CompositeGeneratedAdaptersObserver implements GenericLifecycleObserver {

    /* renamed from: 龘  reason: contains not printable characters */
    private final GeneratedAdapter[] f20;

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16(LifecycleOwner lifecycleOwner, Lifecycle.Event event) {
        MethodCallsLogger methodCallsLogger = new MethodCallsLogger();
        for (GeneratedAdapter r1 : this.f20) {
            r1.m24(lifecycleOwner, event, false, methodCallsLogger);
        }
        for (GeneratedAdapter r12 : this.f20) {
            r12.m24(lifecycleOwner, event, true, methodCallsLogger);
        }
    }
}
