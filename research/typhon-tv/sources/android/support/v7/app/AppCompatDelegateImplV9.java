package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.PointerIconCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.appcompat.R;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.view.StandaloneActionMode;
import android.support.v7.view.menu.ListMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.DecorContentParent;
import android.support.v7.widget.FitWindowsViewGroup;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.VectorEnabledTintResources;
import android.support.v7.widget.ViewStubCompat;
import android.support.v7.widget.ViewUtils;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.google.android.exoplayer2.util.MimeTypes;
import org.xmlpull.v1.XmlPullParser;

class AppCompatDelegateImplV9 extends AppCompatDelegateImplBase implements MenuBuilder.Callback, LayoutInflater.Factory2 {

    /* renamed from: ˆ  reason: contains not printable characters */
    private static final boolean f525 = (Build.VERSION.SDK_INT < 21);

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private final Runnable f526 = new Runnable() {
        public void run() {
            if ((AppCompatDelegateImplV9.this.f536 & 1) != 0) {
                AppCompatDelegateImplV9.this.m638(0);
            }
            if ((AppCompatDelegateImplV9.this.f536 & 4096) != 0) {
                AppCompatDelegateImplV9.this.m638(108);
            }
            AppCompatDelegateImplV9.this.f535 = false;
            AppCompatDelegateImplV9.this.f536 = 0;
        }
    };

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private Rect f527;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f528;

    /* renamed from: ʾ  reason: contains not printable characters */
    ActionMode f529;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private AppCompatViewInflater f530;

    /* renamed from: ʿ  reason: contains not printable characters */
    ActionBarContextView f531;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private Rect f532;

    /* renamed from: ˉ  reason: contains not printable characters */
    private DecorContentParent f533;

    /* renamed from: ˊ  reason: contains not printable characters */
    ViewPropertyAnimatorCompat f534 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    boolean f535;

    /* renamed from: ˎ  reason: contains not printable characters */
    int f536;

    /* renamed from: ˏ  reason: contains not printable characters */
    private ActionMenuPresenterCallback f537;

    /* renamed from: י  reason: contains not printable characters */
    private PanelMenuPresenterCallback f538;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f539;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private PanelFeatureState f540;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private ViewGroup f541;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private boolean f542;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private TextView f543;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private View f544;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f545;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f546;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f547;

    /* renamed from: ﹶ  reason: contains not printable characters */
    PopupWindow f548;

    /* renamed from: ﾞ  reason: contains not printable characters */
    Runnable f549;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private PanelFeatureState[] f550;

    private final class ActionMenuPresenterCallback implements MenuPresenter.Callback {
        ActionMenuPresenterCallback() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m677(MenuBuilder menuBuilder, boolean z) {
            AppCompatDelegateImplV9.this.m670(menuBuilder);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m678(MenuBuilder menuBuilder) {
            Window.Callback r0 = AppCompatDelegateImplV9.this.m575();
            if (r0 == null) {
                return true;
            }
            r0.onMenuOpened(108, menuBuilder);
            return true;
        }
    }

    class ActionModeCallbackWrapperV9 implements ActionMode.Callback {

        /* renamed from: 靐  reason: contains not printable characters */
        private ActionMode.Callback f559;

        public ActionModeCallbackWrapperV9(ActionMode.Callback callback) {
            this.f559 = callback;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m679(ActionMode actionMode, Menu menu) {
            return this.f559.m1440(actionMode, menu);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m680(ActionMode actionMode) {
            this.f559.m1441(actionMode);
            if (AppCompatDelegateImplV9.this.f548 != null) {
                AppCompatDelegateImplV9.this.f503.getDecorView().removeCallbacks(AppCompatDelegateImplV9.this.f549);
            }
            if (AppCompatDelegateImplV9.this.f531 != null) {
                AppCompatDelegateImplV9.this.m643();
                AppCompatDelegateImplV9.this.f534 = ViewCompat.animate(AppCompatDelegateImplV9.this.f531).alpha(0.0f);
                AppCompatDelegateImplV9.this.f534.setListener(new ViewPropertyAnimatorListenerAdapter() {
                    public void onAnimationEnd(View view) {
                        AppCompatDelegateImplV9.this.f531.setVisibility(8);
                        if (AppCompatDelegateImplV9.this.f548 != null) {
                            AppCompatDelegateImplV9.this.f548.dismiss();
                        } else if (AppCompatDelegateImplV9.this.f531.getParent() instanceof View) {
                            ViewCompat.requestApplyInsets((View) AppCompatDelegateImplV9.this.f531.getParent());
                        }
                        AppCompatDelegateImplV9.this.f531.removeAllViews();
                        AppCompatDelegateImplV9.this.f534.setListener((ViewPropertyAnimatorListener) null);
                        AppCompatDelegateImplV9.this.f534 = null;
                    }
                });
            }
            if (AppCompatDelegateImplV9.this.f502 != null) {
                AppCompatDelegateImplV9.this.f502.onSupportActionModeFinished(AppCompatDelegateImplV9.this.f529);
            }
            AppCompatDelegateImplV9.this.f529 = null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m681(ActionMode actionMode, Menu menu) {
            return this.f559.m1442(actionMode, menu);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m682(ActionMode actionMode, MenuItem menuItem) {
            return this.f559.m1443(actionMode, menuItem);
        }
    }

    private class ListMenuDecorView extends ContentFrameLayout {
        public ListMenuDecorView(Context context) {
            super(context);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m683(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImplV9.this.m676(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !m683((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            AppCompatDelegateImplV9.this.m648(0);
            return true;
        }

        public void setBackgroundResource(int i) {
            setBackgroundDrawable(AppCompatResources.m941(getContext(), i));
        }
    }

    protected static final class PanelFeatureState {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f563;

        /* renamed from: ʼ  reason: contains not printable characters */
        ViewGroup f564;

        /* renamed from: ʽ  reason: contains not printable characters */
        View f565;

        /* renamed from: ʾ  reason: contains not printable characters */
        boolean f566;

        /* renamed from: ʿ  reason: contains not printable characters */
        boolean f567;

        /* renamed from: ˈ  reason: contains not printable characters */
        Context f568;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f569 = false;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f570;

        /* renamed from: ˎ  reason: contains not printable characters */
        Bundle f571;

        /* renamed from: ˑ  reason: contains not printable characters */
        View f572;

        /* renamed from: ٴ  reason: contains not printable characters */
        MenuBuilder f573;

        /* renamed from: ᐧ  reason: contains not printable characters */
        ListMenuPresenter f574;

        /* renamed from: 连任  reason: contains not printable characters */
        int f575;

        /* renamed from: 靐  reason: contains not printable characters */
        int f576;

        /* renamed from: 麤  reason: contains not printable characters */
        int f577;

        /* renamed from: 齉  reason: contains not printable characters */
        int f578;

        /* renamed from: 龘  reason: contains not printable characters */
        int f579;

        /* renamed from: ﹶ  reason: contains not printable characters */
        boolean f580;

        /* renamed from: ﾞ  reason: contains not printable characters */
        public boolean f581;

        PanelFeatureState(int i) {
            this.f579 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public MenuView m684(MenuPresenter.Callback callback) {
            if (this.f573 == null) {
                return null;
            }
            if (this.f574 == null) {
                this.f574 = new ListMenuPresenter(this.f568, R.layout.abc_list_menu_item_layout);
                this.f574.setCallback(callback);
                this.f573.addMenuPresenter(this.f574);
            }
            return this.f574.m1535(this.f564);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m685(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(R.attr.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(R.attr.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(R.style.Theme_AppCompat_CompactMenu, true);
            }
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, 0);
            contextThemeWrapper.getTheme().setTo(newTheme);
            this.f568 = contextThemeWrapper;
            TypedArray obtainStyledAttributes = contextThemeWrapper.obtainStyledAttributes(R.styleable.AppCompatTheme);
            this.f576 = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTheme_panelBackground, 0);
            this.f563 = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m686(MenuBuilder menuBuilder) {
            if (menuBuilder != this.f573) {
                if (this.f573 != null) {
                    this.f573.removeMenuPresenter(this.f574);
                }
                this.f573 = menuBuilder;
                if (menuBuilder != null && this.f574 != null) {
                    menuBuilder.addMenuPresenter(this.f574);
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m687() {
            if (this.f565 == null) {
                return false;
            }
            return this.f572 != null || this.f574.m1536().getCount() > 0;
        }
    }

    private final class PanelMenuPresenterCallback implements MenuPresenter.Callback {
        PanelMenuPresenterCallback() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m688(MenuBuilder menuBuilder, boolean z) {
            MenuBuilder rootMenu = menuBuilder.getRootMenu();
            boolean z2 = rootMenu != menuBuilder;
            AppCompatDelegateImplV9 appCompatDelegateImplV9 = AppCompatDelegateImplV9.this;
            if (z2) {
                menuBuilder = rootMenu;
            }
            PanelFeatureState r1 = appCompatDelegateImplV9.m661((Menu) menuBuilder);
            if (r1 == null) {
                return;
            }
            if (z2) {
                AppCompatDelegateImplV9.this.m665(r1.f579, r1, rootMenu);
                AppCompatDelegateImplV9.this.m669(r1, true);
                return;
            }
            AppCompatDelegateImplV9.this.m669(r1, z);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m689(MenuBuilder menuBuilder) {
            Window.Callback r0;
            if (menuBuilder != null || !AppCompatDelegateImplV9.this.f495 || (r0 = AppCompatDelegateImplV9.this.m575()) == null || AppCompatDelegateImplV9.this.m574()) {
                return true;
            }
            r0.onMenuOpened(108, menuBuilder);
            return true;
        }
    }

    AppCompatDelegateImplV9(Context context, Window window, AppCompatCallback appCompatCallback) {
        super(context, window, appCompatCallback);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private int m621(int i) {
        if (i == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i != 9) {
            return i;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    /* renamed from: ـ  reason: contains not printable characters */
    private void m622() {
        if (!this.f539) {
            this.f541 = m623();
            CharSequence r1 = m576();
            if (!TextUtils.isEmpty(r1)) {
                m654(r1);
            }
            m624();
            m674(this.f541);
            this.f539 = true;
            PanelFeatureState r0 = m660(0, false);
            if (m574()) {
                return;
            }
            if (r0 == null || r0.f573 == null) {
                m629(108);
            }
        }
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private ViewGroup m623() {
        TypedArray obtainStyledAttributes = this.f506.obtainStyledAttributes(R.styleable.AppCompatTheme);
        if (!obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowActionBar)) {
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowNoTitle, false)) {
            m658(1);
        } else if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowActionBar, false)) {
            m658(108);
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowActionBarOverlay, false)) {
            m658(109);
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowActionModeOverlay, false)) {
            m658(10);
        }
        this.f501 = obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_android_windowIsFloating, false);
        obtainStyledAttributes.recycle();
        this.f503.getDecorView();
        LayoutInflater from = LayoutInflater.from(this.f506);
        ViewGroup viewGroup = null;
        if (this.f496) {
            viewGroup = this.f500 ? (ViewGroup) from.inflate(R.layout.abc_screen_simple_overlay_action_mode, (ViewGroup) null) : (ViewGroup) from.inflate(R.layout.abc_screen_simple, (ViewGroup) null);
            if (Build.VERSION.SDK_INT >= 21) {
                ViewCompat.setOnApplyWindowInsetsListener(viewGroup, new OnApplyWindowInsetsListener() {
                    public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                        int systemWindowInsetTop = windowInsetsCompat.getSystemWindowInsetTop();
                        int r0 = AppCompatDelegateImplV9.this.m639(systemWindowInsetTop);
                        if (systemWindowInsetTop != r0) {
                            windowInsetsCompat = windowInsetsCompat.replaceSystemWindowInsets(windowInsetsCompat.getSystemWindowInsetLeft(), r0, windowInsetsCompat.getSystemWindowInsetRight(), windowInsetsCompat.getSystemWindowInsetBottom());
                        }
                        return ViewCompat.onApplyWindowInsets(view, windowInsetsCompat);
                    }
                });
            } else {
                ((FitWindowsViewGroup) viewGroup).setOnFitSystemWindowsListener(new FitWindowsViewGroup.OnFitSystemWindowsListener() {
                    public void onFitSystemWindows(Rect rect) {
                        rect.top = AppCompatDelegateImplV9.this.m639(rect.top);
                    }
                });
            }
        } else if (this.f501) {
            viewGroup = (ViewGroup) from.inflate(R.layout.abc_dialog_title_material, (ViewGroup) null);
            this.f499 = false;
            this.f495 = false;
        } else if (this.f495) {
            TypedValue typedValue = new TypedValue();
            this.f506.getTheme().resolveAttribute(R.attr.actionBarTheme, typedValue, true);
            viewGroup = (ViewGroup) LayoutInflater.from(typedValue.resourceId != 0 ? new ContextThemeWrapper(this.f506, typedValue.resourceId) : this.f506).inflate(R.layout.abc_screen_toolbar, (ViewGroup) null);
            this.f533 = (DecorContentParent) viewGroup.findViewById(R.id.decor_content_parent);
            this.f533.setWindowCallback(m575());
            if (this.f499) {
                this.f533.initFeature(109);
            }
            if (this.f545) {
                this.f533.initFeature(2);
            }
            if (this.f546) {
                this.f533.initFeature(5);
            }
        }
        if (viewGroup == null) {
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.f495 + ", windowActionBarOverlay: " + this.f499 + ", android:windowIsFloating: " + this.f501 + ", windowActionModeOverlay: " + this.f500 + ", windowNoTitle: " + this.f496 + " }");
        }
        if (this.f533 == null) {
            this.f543 = (TextView) viewGroup.findViewById(R.id.title);
        }
        ViewUtils.makeOptionalFitsSystemWindows(viewGroup);
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(R.id.action_bar_activity_content);
        ViewGroup viewGroup2 = (ViewGroup) this.f503.findViewById(16908290);
        if (viewGroup2 != null) {
            while (viewGroup2.getChildCount() > 0) {
                View childAt = viewGroup2.getChildAt(0);
                viewGroup2.removeViewAt(0);
                contentFrameLayout.addView(childAt);
            }
            viewGroup2.setId(-1);
            contentFrameLayout.setId(16908290);
            if (viewGroup2 instanceof FrameLayout) {
                ((FrameLayout) viewGroup2).setForeground((Drawable) null);
            }
        }
        this.f503.setContentView(viewGroup);
        contentFrameLayout.setAttachListener(new ContentFrameLayout.OnAttachListener() {
            public void onAttachedFromWindow() {
            }

            public void onDetachedFromWindow() {
                AppCompatDelegateImplV9.this.m646();
            }
        });
        return viewGroup;
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private void m624() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.f541.findViewById(16908290);
        View decorView = this.f503.getDecorView();
        contentFrameLayout.setDecorPadding(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.f506.obtainStyledAttributes(R.styleable.AppCompatTheme);
        obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    private void m625() {
        if (this.f539) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean m626(int i, KeyEvent keyEvent) {
        if (this.f529 != null) {
            return false;
        }
        boolean z = false;
        PanelFeatureState r3 = m660(i, true);
        if (i != 0 || this.f533 == null || !this.f533.canShowOverflowMenu() || ViewConfiguration.get(this.f506).hasPermanentMenuKey()) {
            if (r3.f580 || r3.f567) {
                z = r3.f580;
                m669(r3, true);
            } else if (r3.f566) {
                boolean z2 = true;
                if (r3.f570) {
                    r3.f566 = false;
                    z2 = m628(r3, keyEvent);
                }
                if (z2) {
                    m632(r3, keyEvent);
                    z = true;
                }
            }
        } else if (this.f533.isOverflowMenuShowing()) {
            z = this.f533.hideOverflowMenu();
        } else if (!m574() && m628(r3, keyEvent)) {
            z = this.f533.showOverflowMenu();
        }
        if (!z) {
            return z;
        }
        AudioManager audioManager = (AudioManager) this.f506.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.playSoundEffect(0);
            return z;
        }
        Log.w("AppCompatDelegate", "Couldn't get audio manager");
        return z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m627(PanelFeatureState panelFeatureState) {
        Context context = this.f506;
        if ((panelFeatureState.f579 == 0 || panelFeatureState.f579 == 108) && this.f533 != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(R.attr.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(R.attr.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(R.attr.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, 0);
                contextThemeWrapper.getTheme().setTo(theme2);
                context = contextThemeWrapper;
            }
        }
        MenuBuilder menuBuilder = new MenuBuilder(context);
        menuBuilder.setCallback(this);
        panelFeatureState.m686(menuBuilder);
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m628(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        if (m574()) {
            return false;
        }
        if (panelFeatureState.f566) {
            return true;
        }
        if (!(this.f540 == null || this.f540 == panelFeatureState)) {
            m669(this.f540, false);
        }
        Window.Callback r0 = m575();
        if (r0 != null) {
            panelFeatureState.f572 = r0.onCreatePanelView(panelFeatureState.f579);
        }
        boolean z = panelFeatureState.f579 == 0 || panelFeatureState.f579 == 108;
        if (z && this.f533 != null) {
            this.f533.setMenuPrepared();
        }
        if (panelFeatureState.f572 == null && (!z || !(m573() instanceof ToolbarActionBar))) {
            if (panelFeatureState.f573 == null || panelFeatureState.f570) {
                if (panelFeatureState.f573 == null && (!m627(panelFeatureState) || panelFeatureState.f573 == null)) {
                    return false;
                }
                if (z && this.f533 != null) {
                    if (this.f537 == null) {
                        this.f537 = new ActionMenuPresenterCallback();
                    }
                    this.f533.setMenu(panelFeatureState.f573, this.f537);
                }
                panelFeatureState.f573.stopDispatchingItemsChanged();
                if (!r0.onCreatePanelMenu(panelFeatureState.f579, panelFeatureState.f573)) {
                    panelFeatureState.m686((MenuBuilder) null);
                    if (!z || this.f533 == null) {
                        return false;
                    }
                    this.f533.setMenu((Menu) null, this.f537);
                    return false;
                }
                panelFeatureState.f570 = false;
            }
            panelFeatureState.f573.stopDispatchingItemsChanged();
            if (panelFeatureState.f571 != null) {
                panelFeatureState.f573.restoreActionViewStates(panelFeatureState.f571);
                panelFeatureState.f571 = null;
            }
            if (!r0.onPreparePanel(0, panelFeatureState.f572, panelFeatureState.f573)) {
                if (z && this.f533 != null) {
                    this.f533.setMenu((Menu) null, this.f537);
                }
                panelFeatureState.f573.startDispatchingItemsChanged();
                return false;
            }
            panelFeatureState.f581 = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            panelFeatureState.f573.setQwertyMode(panelFeatureState.f581);
            panelFeatureState.f573.startDispatchingItemsChanged();
        }
        panelFeatureState.f566 = true;
        panelFeatureState.f567 = false;
        this.f540 = panelFeatureState;
        return true;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m629(int i) {
        this.f536 |= 1 << i;
        if (!this.f535) {
            ViewCompat.postOnAnimation(this.f503.getDecorView(), this.f526);
            this.f535 = true;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m630(int i, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() == 0) {
            PanelFeatureState r0 = m660(i, true);
            if (!r0.f580) {
                return m628(r0, keyEvent);
            }
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m631(PanelFeatureState panelFeatureState) {
        if (panelFeatureState.f572 != null) {
            panelFeatureState.f565 = panelFeatureState.f572;
            return true;
        } else if (panelFeatureState.f573 == null) {
            return false;
        } else {
            if (this.f538 == null) {
                this.f538 = new PanelMenuPresenterCallback();
            }
            panelFeatureState.f565 = (View) panelFeatureState.m684((MenuPresenter.Callback) this.f538);
            return panelFeatureState.f565 != null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m632(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        ViewGroup.LayoutParams layoutParams;
        if (!panelFeatureState.f580 && !m574()) {
            if (panelFeatureState.f579 == 0) {
                if ((this.f506.getResources().getConfiguration().screenLayout & 15) == 4) {
                    return;
                }
            }
            Window.Callback r9 = m575();
            if (r9 == null || r9.onMenuOpened(panelFeatureState.f579, panelFeatureState.f573)) {
                WindowManager windowManager = (WindowManager) this.f506.getSystemService("window");
                if (windowManager != null && m628(panelFeatureState, keyEvent)) {
                    int i = -2;
                    if (panelFeatureState.f564 == null || panelFeatureState.f569) {
                        if (panelFeatureState.f564 == null) {
                            if (!m634(panelFeatureState) || panelFeatureState.f564 == null) {
                                return;
                            }
                        } else if (panelFeatureState.f569 && panelFeatureState.f564.getChildCount() > 0) {
                            panelFeatureState.f564.removeAllViews();
                        }
                        if (m631(panelFeatureState) && panelFeatureState.m687()) {
                            ViewGroup.LayoutParams layoutParams2 = panelFeatureState.f565.getLayoutParams();
                            if (layoutParams2 == null) {
                                layoutParams2 = new ViewGroup.LayoutParams(-2, -2);
                            }
                            panelFeatureState.f564.setBackgroundResource(panelFeatureState.f576);
                            ViewParent parent = panelFeatureState.f565.getParent();
                            if (parent != null && (parent instanceof ViewGroup)) {
                                ((ViewGroup) parent).removeView(panelFeatureState.f565);
                            }
                            panelFeatureState.f564.addView(panelFeatureState.f565, layoutParams2);
                            if (!panelFeatureState.f565.hasFocus()) {
                                panelFeatureState.f565.requestFocus();
                            }
                        } else {
                            return;
                        }
                    } else if (!(panelFeatureState.f572 == null || (layoutParams = panelFeatureState.f572.getLayoutParams()) == null || layoutParams.width != -1)) {
                        i = -1;
                    }
                    panelFeatureState.f567 = false;
                    WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams(i, -2, panelFeatureState.f577, panelFeatureState.f575, PointerIconCompat.TYPE_HAND, 8519680, -3);
                    layoutParams3.gravity = panelFeatureState.f578;
                    layoutParams3.windowAnimations = panelFeatureState.f563;
                    windowManager.addView(panelFeatureState.f564, layoutParams3);
                    panelFeatureState.f580 = true;
                    return;
                }
                return;
            }
            m669(panelFeatureState, true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m633(MenuBuilder menuBuilder, boolean z) {
        if (this.f533 == null || !this.f533.canShowOverflowMenu() || (ViewConfiguration.get(this.f506).hasPermanentMenuKey() && !this.f533.isOverflowMenuShowPending())) {
            PanelFeatureState r1 = m660(0, true);
            r1.f569 = true;
            m669(r1, false);
            m632(r1, (KeyEvent) null);
            return;
        }
        Window.Callback r0 = m575();
        if (this.f533.isOverflowMenuShowing() && z) {
            this.f533.hideOverflowMenu();
            if (!m574()) {
                r0.onPanelClosed(108, m660(0, true).f573);
            }
        } else if (r0 != null && !m574()) {
            if (this.f535 && (this.f536 & 1) != 0) {
                this.f503.getDecorView().removeCallbacks(this.f526);
                this.f526.run();
            }
            PanelFeatureState r12 = m660(0, true);
            if (r12.f573 != null && !r12.f570 && r0.onPreparePanel(0, r12.f572, r12.f573)) {
                r0.onMenuOpened(108, r12.f573);
                this.f533.showOverflowMenu();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m634(PanelFeatureState panelFeatureState) {
        panelFeatureState.m685(m591());
        panelFeatureState.f564 = new ListMenuDecorView(panelFeatureState.f568);
        panelFeatureState.f578 = 81;
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m635(PanelFeatureState panelFeatureState, int i, KeyEvent keyEvent, int i2) {
        if (keyEvent.isSystem()) {
            return false;
        }
        boolean z = false;
        if ((panelFeatureState.f566 || m628(panelFeatureState, keyEvent)) && panelFeatureState.f573 != null) {
            z = panelFeatureState.f573.performShortcut(i, keyEvent, i2);
        }
        if (!z || (i2 & 1) != 0 || this.f533 != null) {
            return z;
        }
        m669(panelFeatureState, true);
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m636(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.f503.getDecorView();
        while (viewParent != null) {
            if (viewParent == decorView || !(viewParent instanceof View) || ViewCompat.isAttachedToWindow((View) viewParent)) {
                return false;
            }
            viewParent = viewParent.getParent();
        }
        return true;
    }

    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View r0 = m664(view, str, context, attributeSet);
        return r0 != null ? r0 : m650(view, str, context, attributeSet);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView((View) null, str, context, attributeSet);
    }

    public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
        PanelFeatureState r1;
        Window.Callback r0 = m575();
        if (r0 == null || m574() || (r1 = m661((Menu) menuBuilder.getRootMenu())) == null) {
            return false;
        }
        return r0.onMenuItemSelected(r1.f579, menuItem);
    }

    public void onMenuModeChange(MenuBuilder menuBuilder) {
        m633(menuBuilder, true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m637() {
        ActionBar r0 = m585();
        if (r0 == null || !r0.m418()) {
            m629(0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m638(int i) {
        PanelFeatureState r1;
        PanelFeatureState r12 = m660(i, true);
        if (r12.f573 != null) {
            Bundle bundle = new Bundle();
            r12.f573.saveActionViewStates(bundle);
            if (bundle.size() > 0) {
                r12.f571 = bundle;
            }
            r12.f573.stopDispatchingItemsChanged();
            r12.f573.clear();
        }
        r12.f570 = true;
        r12.f569 = true;
        if ((i == 108 || i == 0) && this.f533 != null && (r1 = m660(0, false)) != null) {
            r1.f566 = false;
            m628(r1, (KeyEvent) null);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m639(int i) {
        int i2 = 0;
        boolean z = false;
        if (this.f531 != null && (this.f531.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f531.getLayoutParams();
            boolean z2 = false;
            if (this.f531.isShown()) {
                if (this.f527 == null) {
                    this.f527 = new Rect();
                    this.f532 = new Rect();
                }
                Rect rect = this.f527;
                Rect rect2 = this.f532;
                rect.set(0, i, 0, 0);
                ViewUtils.computeFitSystemWindows(this.f541, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i : 0)) {
                    z2 = true;
                    marginLayoutParams.topMargin = i;
                    if (this.f544 == null) {
                        this.f544 = new View(this.f506);
                        this.f544.setBackgroundColor(this.f506.getResources().getColor(R.color.abc_input_method_navigation_guard));
                        this.f541.addView(this.f544, -1, new ViewGroup.LayoutParams(-1, i));
                    } else {
                        ViewGroup.LayoutParams layoutParams = this.f544.getLayoutParams();
                        if (layoutParams.height != i) {
                            layoutParams.height = i;
                            this.f544.setLayoutParams(layoutParams);
                        }
                    }
                }
                z = this.f544 != null;
                if (!this.f500 && z) {
                    i = 0;
                }
            } else if (marginLayoutParams.topMargin != 0) {
                z2 = true;
                marginLayoutParams.topMargin = 0;
            }
            if (z2) {
                this.f531.setLayoutParams(marginLayoutParams);
            }
        }
        if (this.f544 != null) {
            View view = this.f544;
            if (!z) {
                i2 = 8;
            }
            view.setVisibility(i2);
        }
        return i;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m640() {
        if (this.f535) {
            this.f503.getDecorView().removeCallbacks(this.f526);
        }
        super.m570();
        if (this.f493 != null) {
            this.f493.m421();
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m641() {
        m622();
        if (this.f495 && this.f493 == null) {
            if (this.f505 instanceof Activity) {
                this.f493 = new WindowDecorActionBar((Activity) this.f505, this.f499);
            } else if (this.f505 instanceof Dialog) {
                this.f493 = new WindowDecorActionBar((Dialog) this.f505);
            }
            if (this.f493 != null) {
                this.f493.m415(this.f528);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public final boolean m642() {
        return this.f539 && this.f541 != null && ViewCompat.isLaidOut(this.f541);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m643() {
        if (this.f534 != null) {
            this.f534.cancel();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean m644() {
        if (this.f529 != null) {
            this.f529.m1433();
            return true;
        }
        ActionBar r0 = m585();
        return r0 != null && r0.m420();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m645() {
        LayoutInflater from = LayoutInflater.from(this.f506);
        if (from.getFactory() == null) {
            LayoutInflaterCompat.setFactory2(from, this);
        } else if (!(from.getFactory2() instanceof AppCompatDelegateImplV9)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: י  reason: contains not printable characters */
    public void m646() {
        if (this.f533 != null) {
            this.f533.dismissPopups();
        }
        if (this.f548 != null) {
            this.f503.getDecorView().removeCallbacks(this.f549);
            if (this.f548.isShowing()) {
                try {
                    this.f548.dismiss();
                } catch (IllegalArgumentException e) {
                }
            }
            this.f548 = null;
        }
        m643();
        PanelFeatureState r0 = m660(0, false);
        if (r0 != null && r0.f573 != null) {
            r0.f573.close();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m647() {
        ActionBar r0 = m585();
        if (r0 != null) {
            r0.m417(true);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m648(int i) {
        m669(m660(i, true), true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public ActionMode m649(ActionMode.Callback callback) {
        Context context;
        m643();
        if (this.f529 != null) {
            this.f529.m1433();
        }
        if (!(callback instanceof ActionModeCallbackWrapperV9)) {
            callback = new ActionModeCallbackWrapperV9(callback);
        }
        ActionMode actionMode = null;
        if (this.f502 != null && !m574()) {
            try {
                actionMode = this.f502.onWindowStartingSupportActionMode(callback);
            } catch (AbstractMethodError e) {
            }
        }
        if (actionMode != null) {
            this.f529 = actionMode;
        } else {
            if (this.f531 == null) {
                if (this.f501) {
                    TypedValue typedValue = new TypedValue();
                    Resources.Theme theme = this.f506.getTheme();
                    theme.resolveAttribute(R.attr.actionBarTheme, typedValue, true);
                    if (typedValue.resourceId != 0) {
                        Resources.Theme newTheme = this.f506.getResources().newTheme();
                        newTheme.setTo(theme);
                        newTheme.applyStyle(typedValue.resourceId, true);
                        context = new ContextThemeWrapper(this.f506, 0);
                        context.getTheme().setTo(newTheme);
                    } else {
                        context = this.f506;
                    }
                    this.f531 = new ActionBarContextView(context);
                    this.f548 = new PopupWindow(context, (AttributeSet) null, R.attr.actionModePopupWindowStyle);
                    PopupWindowCompat.setWindowLayoutType(this.f548, 2);
                    this.f548.setContentView(this.f531);
                    this.f548.setWidth(-1);
                    context.getTheme().resolveAttribute(R.attr.actionBarSize, typedValue, true);
                    this.f531.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, context.getResources().getDisplayMetrics()));
                    this.f548.setHeight(-2);
                    this.f549 = new Runnable() {
                        public void run() {
                            AppCompatDelegateImplV9.this.f548.showAtLocation(AppCompatDelegateImplV9.this.f531, 55, 0, 0);
                            AppCompatDelegateImplV9.this.m643();
                            if (AppCompatDelegateImplV9.this.m642()) {
                                AppCompatDelegateImplV9.this.f531.setAlpha(0.0f);
                                AppCompatDelegateImplV9.this.f534 = ViewCompat.animate(AppCompatDelegateImplV9.this.f531).alpha(1.0f);
                                AppCompatDelegateImplV9.this.f534.setListener(new ViewPropertyAnimatorListenerAdapter() {
                                    public void onAnimationEnd(View view) {
                                        AppCompatDelegateImplV9.this.f531.setAlpha(1.0f);
                                        AppCompatDelegateImplV9.this.f534.setListener((ViewPropertyAnimatorListener) null);
                                        AppCompatDelegateImplV9.this.f534 = null;
                                    }

                                    public void onAnimationStart(View view) {
                                        AppCompatDelegateImplV9.this.f531.setVisibility(0);
                                    }
                                });
                                return;
                            }
                            AppCompatDelegateImplV9.this.f531.setAlpha(1.0f);
                            AppCompatDelegateImplV9.this.f531.setVisibility(0);
                        }
                    };
                } else {
                    ViewStubCompat viewStubCompat = (ViewStubCompat) this.f541.findViewById(R.id.action_mode_bar_stub);
                    if (viewStubCompat != null) {
                        viewStubCompat.setLayoutInflater(LayoutInflater.from(m591()));
                        this.f531 = (ActionBarContextView) viewStubCompat.inflate();
                    }
                }
            }
            if (this.f531 != null) {
                m643();
                this.f531.killMode();
                StandaloneActionMode standaloneActionMode = new StandaloneActionMode(this.f531.getContext(), this.f531, callback, this.f548 == null);
                if (callback.m1442((ActionMode) standaloneActionMode, standaloneActionMode.m1429())) {
                    standaloneActionMode.m1432();
                    this.f531.initForMode(standaloneActionMode);
                    this.f529 = standaloneActionMode;
                    if (m642()) {
                        this.f531.setAlpha(0.0f);
                        this.f534 = ViewCompat.animate(this.f531).alpha(1.0f);
                        this.f534.setListener(new ViewPropertyAnimatorListenerAdapter() {
                            public void onAnimationEnd(View view) {
                                AppCompatDelegateImplV9.this.f531.setAlpha(1.0f);
                                AppCompatDelegateImplV9.this.f534.setListener((ViewPropertyAnimatorListener) null);
                                AppCompatDelegateImplV9.this.f534 = null;
                            }

                            public void onAnimationStart(View view) {
                                AppCompatDelegateImplV9.this.f531.setVisibility(0);
                                AppCompatDelegateImplV9.this.f531.sendAccessibilityEvent(32);
                                if (AppCompatDelegateImplV9.this.f531.getParent() instanceof View) {
                                    ViewCompat.requestApplyInsets((View) AppCompatDelegateImplV9.this.f531.getParent());
                                }
                            }
                        });
                    } else {
                        this.f531.setAlpha(1.0f);
                        this.f531.setVisibility(0);
                        this.f531.sendAccessibilityEvent(32);
                        if (this.f531.getParent() instanceof View) {
                            ViewCompat.requestApplyInsets((View) this.f531.getParent());
                        }
                    }
                    if (this.f548 != null) {
                        this.f503.getDecorView().post(this.f549);
                    }
                } else {
                    this.f529 = null;
                }
            }
        }
        if (!(this.f529 == null || this.f502 == null)) {
            this.f502.onSupportActionModeStarted(this.f529);
        }
        return this.f529;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public View m650(View view, String str, Context context, AttributeSet attributeSet) {
        if (this.f530 == null) {
            this.f530 = new AppCompatViewInflater();
        }
        boolean z = false;
        if (f525) {
            z = attributeSet instanceof XmlPullParser ? ((XmlPullParser) attributeSet).getDepth() > 1 : m636((ViewParent) view);
        }
        return this.f530.m694(view, str, context, attributeSet, z, f525, true, VectorEnabledTintResources.shouldBeUsed());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m651(int i) {
        m622();
        ViewGroup viewGroup = (ViewGroup) this.f541.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f506).inflate(i, viewGroup);
        this.f505.onContentChanged();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m652(Bundle bundle) {
        m622();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m653(View view, ViewGroup.LayoutParams layoutParams) {
        m622();
        ((ViewGroup) this.f541.findViewById(16908290)).addView(view, layoutParams);
        this.f505.onContentChanged();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m654(CharSequence charSequence) {
        if (this.f533 != null) {
            this.f533.setWindowTitle(charSequence);
        } else if (m573() != null) {
            m573().m432(charSequence);
        } else if (this.f543 != null) {
            this.f543.setText(charSequence);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m655(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                boolean z = this.f542;
                this.f542 = false;
                PanelFeatureState r0 = m660(0, false);
                if (r0 == null || !r0.f580) {
                    if (m644()) {
                        return true;
                    }
                } else if (z) {
                    return true;
                } else {
                    m669(r0, true);
                    return true;
                }
                break;
            case 82:
                m626(0, keyEvent);
                return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m656(int i, Menu menu) {
        if (i != 108) {
            return false;
        }
        ActionBar r0 = m585();
        if (r0 == null) {
            return true;
        }
        r0.m419(true);
        return true;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m657() {
        ActionBar r0 = m585();
        if (r0 != null) {
            r0.m417(false);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m658(int i) {
        int r4 = m621(i);
        if (this.f496 && r4 == 108) {
            return false;
        }
        if (this.f495 && r4 == 1) {
            this.f495 = false;
        }
        switch (r4) {
            case 1:
                m625();
                this.f496 = true;
                return true;
            case 2:
                m625();
                this.f545 = true;
                return true;
            case 5:
                m625();
                this.f546 = true;
                return true;
            case 10:
                m625();
                this.f500 = true;
                return true;
            case 108:
                m625();
                this.f495 = true;
                return true;
            case 109:
                m625();
                this.f499 = true;
                return true;
            default:
                return this.f503.requestFeature(r4);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m659(int i, KeyEvent keyEvent) {
        boolean z = true;
        switch (i) {
            case 4:
                if ((keyEvent.getFlags() & 128) == 0) {
                    z = false;
                }
                this.f542 = z;
                break;
            case 82:
                m630(0, keyEvent);
                return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public PanelFeatureState m660(int i, boolean z) {
        PanelFeatureState[] panelFeatureStateArr = this.f550;
        if (panelFeatureStateArr == null || panelFeatureStateArr.length <= i) {
            PanelFeatureState[] panelFeatureStateArr2 = new PanelFeatureState[(i + 1)];
            if (panelFeatureStateArr != null) {
                System.arraycopy(panelFeatureStateArr, 0, panelFeatureStateArr2, 0, panelFeatureStateArr.length);
            }
            panelFeatureStateArr = panelFeatureStateArr2;
            this.f550 = panelFeatureStateArr2;
        }
        PanelFeatureState panelFeatureState = panelFeatureStateArr[i];
        if (panelFeatureState != null) {
            return panelFeatureState;
        }
        PanelFeatureState panelFeatureState2 = new PanelFeatureState(i);
        panelFeatureStateArr[i] = panelFeatureState2;
        return panelFeatureState2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public PanelFeatureState m661(Menu menu) {
        PanelFeatureState[] panelFeatureStateArr = this.f550;
        int length = panelFeatureStateArr != null ? panelFeatureStateArr.length : 0;
        for (int i = 0; i < length; i++) {
            PanelFeatureState panelFeatureState = panelFeatureStateArr[i];
            if (panelFeatureState != null && panelFeatureState.f573 == menu) {
                return panelFeatureState;
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ActionMode m662(ActionMode.Callback callback) {
        if (callback == null) {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
        if (this.f529 != null) {
            this.f529.m1433();
        }
        ActionModeCallbackWrapperV9 actionModeCallbackWrapperV9 = new ActionModeCallbackWrapperV9(callback);
        ActionBar r0 = m585();
        if (r0 != null) {
            this.f529 = r0.m435((ActionMode.Callback) actionModeCallbackWrapperV9);
            if (!(this.f529 == null || this.f502 == null)) {
                this.f502.onSupportActionModeStarted(this.f529);
            }
        }
        if (this.f529 == null) {
            this.f529 = m649((ActionMode.Callback) actionModeCallbackWrapperV9);
        }
        return this.f529;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T extends View> T m663(int i) {
        m622();
        return this.f503.findViewById(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public View m664(View view, String str, Context context, AttributeSet attributeSet) {
        View onCreateView;
        if (!(this.f505 instanceof LayoutInflater.Factory) || (onCreateView = ((LayoutInflater.Factory) this.f505).onCreateView(str, context, attributeSet)) == null) {
            return null;
        }
        return onCreateView;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m665(int i, PanelFeatureState panelFeatureState, Menu menu) {
        if (menu == null) {
            if (panelFeatureState == null && i >= 0 && i < this.f550.length) {
                panelFeatureState = this.f550[i];
            }
            if (panelFeatureState != null) {
                menu = panelFeatureState.f573;
            }
        }
        if ((panelFeatureState == null || panelFeatureState.f580) && !m574()) {
            this.f505.onPanelClosed(i, menu);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m666(int i, Menu menu) {
        if (i == 108) {
            ActionBar r0 = m585();
            if (r0 != null) {
                r0.m419(false);
            }
        } else if (i == 0) {
            PanelFeatureState r1 = m660(i, true);
            if (r1.f580) {
                m669(r1, false);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m667(Configuration configuration) {
        ActionBar r0;
        if (this.f495 && this.f539 && (r0 = m585()) != null) {
            r0.m438(configuration);
        }
        AppCompatDrawableManager.get().onConfigurationChanged(this.f506);
        m577();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m668(Bundle bundle) {
        if ((this.f505 instanceof Activity) && NavUtils.getParentActivityName((Activity) this.f505) != null) {
            ActionBar r0 = m573();
            if (r0 == null) {
                this.f528 = true;
            } else {
                r0.m415(true);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m669(PanelFeatureState panelFeatureState, boolean z) {
        if (!z || panelFeatureState.f579 != 0 || this.f533 == null || !this.f533.isOverflowMenuShowing()) {
            WindowManager windowManager = (WindowManager) this.f506.getSystemService("window");
            if (!(windowManager == null || !panelFeatureState.f580 || panelFeatureState.f564 == null)) {
                windowManager.removeView(panelFeatureState.f564);
                if (z) {
                    m665(panelFeatureState.f579, panelFeatureState, (Menu) null);
                }
            }
            panelFeatureState.f566 = false;
            panelFeatureState.f567 = false;
            panelFeatureState.f580 = false;
            panelFeatureState.f565 = null;
            panelFeatureState.f569 = true;
            if (this.f540 == panelFeatureState) {
                this.f540 = null;
                return;
            }
            return;
        }
        m670(panelFeatureState.f573);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m670(MenuBuilder menuBuilder) {
        if (!this.f547) {
            this.f547 = true;
            this.f533.dismissPopups();
            Window.Callback r0 = m575();
            if (r0 != null && !m574()) {
                r0.onPanelClosed(108, menuBuilder);
            }
            this.f547 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m671(Toolbar toolbar) {
        if (this.f505 instanceof Activity) {
            ActionBar r0 = m585();
            if (r0 instanceof WindowDecorActionBar) {
                throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
            }
            this.f494 = null;
            if (r0 != null) {
                r0.m421();
            }
            if (toolbar != null) {
                ToolbarActionBar toolbarActionBar = new ToolbarActionBar(toolbar, ((Activity) this.f505).getTitle(), this.f504);
                this.f493 = toolbarActionBar;
                this.f503.setCallback(toolbarActionBar.m841());
            } else {
                this.f493 = null;
                this.f503.setCallback(this.f504);
            }
            m637();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m672(View view) {
        m622();
        ViewGroup viewGroup = (ViewGroup) this.f541.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.f505.onContentChanged();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m673(View view, ViewGroup.LayoutParams layoutParams) {
        m622();
        ViewGroup viewGroup = (ViewGroup) this.f541.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.f505.onContentChanged();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m674(ViewGroup viewGroup) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m675(int i, KeyEvent keyEvent) {
        ActionBar r0 = m585();
        if (r0 != null && r0.m442(i, keyEvent)) {
            return true;
        }
        if (this.f540 == null || !m635(this.f540, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.f540 == null) {
                PanelFeatureState r2 = m660(0, true);
                m628(r2, keyEvent);
                boolean r1 = m635(r2, keyEvent.getKeyCode(), keyEvent, 1);
                r2.f566 = false;
                if (r1) {
                    return true;
                }
            }
            return false;
        } else if (this.f540 == null) {
            return true;
        } else {
            this.f540.f567 = true;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m676(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 82 && this.f505.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        return keyEvent.getAction() == 0 ? m659(keyCode, keyEvent) : m655(keyCode, keyEvent);
    }
}
