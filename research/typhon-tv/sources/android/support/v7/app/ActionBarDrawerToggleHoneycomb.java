package android.support.v7.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.lang.reflect.Method;

class ActionBarDrawerToggleHoneycomb {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int[] f373 = {16843531};

    static class SetIndicatorInfo {

        /* renamed from: 靐  reason: contains not printable characters */
        public Method f374;

        /* renamed from: 齉  reason: contains not printable characters */
        public ImageView f375;

        /* renamed from: 龘  reason: contains not printable characters */
        public Method f376;

        SetIndicatorInfo(Activity activity) {
            try {
                this.f376 = ActionBar.class.getDeclaredMethod("setHomeAsUpIndicator", new Class[]{Drawable.class});
                this.f374 = ActionBar.class.getDeclaredMethod("setHomeActionContentDescription", new Class[]{Integer.TYPE});
            } catch (NoSuchMethodException e) {
                View findViewById = activity.findViewById(16908332);
                if (findViewById != null) {
                    ViewGroup viewGroup = (ViewGroup) findViewById.getParent();
                    if (viewGroup.getChildCount() == 2) {
                        View childAt = viewGroup.getChildAt(0);
                        View childAt2 = childAt.getId() == 16908332 ? viewGroup.getChildAt(1) : childAt;
                        if (childAt2 instanceof ImageView) {
                            this.f375 = (ImageView) childAt2;
                        }
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Drawable m480(Activity activity) {
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(f373);
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        return drawable;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static SetIndicatorInfo m481(SetIndicatorInfo setIndicatorInfo, Activity activity, int i) {
        if (setIndicatorInfo == null) {
            setIndicatorInfo = new SetIndicatorInfo(activity);
        }
        if (setIndicatorInfo.f376 != null) {
            try {
                ActionBar actionBar = activity.getActionBar();
                setIndicatorInfo.f374.invoke(actionBar, new Object[]{Integer.valueOf(i)});
                if (Build.VERSION.SDK_INT <= 19) {
                    actionBar.setSubtitle(actionBar.getSubtitle());
                }
            } catch (Exception e) {
                Log.w("ActionBarDrawerToggleHC", "Couldn't set content description via JB-MR2 API", e);
            }
        }
        return setIndicatorInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static SetIndicatorInfo m482(SetIndicatorInfo setIndicatorInfo, Activity activity, Drawable drawable, int i) {
        SetIndicatorInfo setIndicatorInfo2 = new SetIndicatorInfo(activity);
        if (setIndicatorInfo2.f376 != null) {
            try {
                ActionBar actionBar = activity.getActionBar();
                setIndicatorInfo2.f376.invoke(actionBar, new Object[]{drawable});
                setIndicatorInfo2.f374.invoke(actionBar, new Object[]{Integer.valueOf(i)});
            } catch (Exception e) {
                Log.w("ActionBarDrawerToggleHC", "Couldn't set home-as-up indicator via JB-MR2 API", e);
            }
        } else if (setIndicatorInfo2.f375 != null) {
            setIndicatorInfo2.f375.setImageDrawable(drawable);
        } else {
            Log.w("ActionBarDrawerToggleHC", "Couldn't set home-as-up indicator");
        }
        return setIndicatorInfo2;
    }
}
