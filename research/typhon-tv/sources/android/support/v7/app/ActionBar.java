package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.appcompat.R;
import android.support.v7.view.ActionMode;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

public abstract class ActionBar {

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int gravity;

        public LayoutParams(int i) {
            this(-2, -1, i);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.gravity = 0;
            this.gravity = 8388627;
        }

        public LayoutParams(int i, int i2, int i3) {
            super(i, i2);
            this.gravity = 0;
            this.gravity = i3;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.gravity = 0;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ActionBarLayout);
            this.gravity = obtainStyledAttributes.getInt(R.styleable.ActionBarLayout_android_layout_gravity, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = 0;
            this.gravity = layoutParams.gravity;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = 0;
        }
    }

    public interface OnMenuVisibilityListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m444(boolean z);
    }

    @Deprecated
    public static abstract class Tab {
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract CharSequence m445();

        /* renamed from: 连任  reason: contains not printable characters */
        public abstract void m446();

        /* renamed from: 靐  reason: contains not printable characters */
        public abstract Drawable m447();

        /* renamed from: 麤  reason: contains not printable characters */
        public abstract View m448();

        /* renamed from: 齉  reason: contains not printable characters */
        public abstract CharSequence m449();

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract int m450();
    }

    @Deprecated
    public interface TabListener {
        /* renamed from: 靐  reason: contains not printable characters */
        void m451(Tab tab, FragmentTransaction fragmentTransaction);

        /* renamed from: 齉  reason: contains not printable characters */
        void m452(Tab tab, FragmentTransaction fragmentTransaction);

        /* renamed from: 龘  reason: contains not printable characters */
        void m453(Tab tab, FragmentTransaction fragmentTransaction);
    }

    @RestrictTo
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m415(boolean z) {
    }

    @RestrictTo
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m416() {
        return false;
    }

    @RestrictTo
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m417(boolean z) {
    }

    @RestrictTo
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m418() {
        return false;
    }

    @RestrictTo
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m419(boolean z) {
    }

    @RestrictTo
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m420() {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m421() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m422(boolean z) {
        if (z) {
            throw new UnsupportedOperationException("Hide on content scroll is not supported in this action bar configuration.");
        }
    }

    @RestrictTo
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m423() {
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract int m424();

    /* renamed from: 靐  reason: contains not printable characters */
    public void m425(int i) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m426(CharSequence charSequence);

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m427(boolean z);

    /* renamed from: 麤  reason: contains not printable characters */
    public Context m428() {
        return null;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m429(boolean z) {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m430();

    /* renamed from: 齉  reason: contains not printable characters */
    public void m431(int i) {
    }

    @RestrictTo
    /* renamed from: 齉  reason: contains not printable characters */
    public void m432(CharSequence charSequence) {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m433(boolean z);

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m434();

    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public ActionMode m435(ActionMode.Callback callback) {
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m436(float f) {
        if (f != 0.0f) {
            throw new UnsupportedOperationException("Setting a non-zero elevation is not supported in this action bar configuration.");
        }
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m437(int i);

    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public void m438(Configuration configuration) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m439(Drawable drawable) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m440(CharSequence charSequence);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m441(boolean z);

    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m442(int i, KeyEvent keyEvent) {
        return false;
    }

    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m443(KeyEvent keyEvent) {
        return false;
    }
}
