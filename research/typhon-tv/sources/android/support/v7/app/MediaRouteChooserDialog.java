package android.support.v7.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.mediarouter.R;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MediaRouteChooserDialog extends AppCompatDialog {

    /* renamed from: ʻ  reason: contains not printable characters */
    private RouteAdapter f616;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ListView f617;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f618;

    /* renamed from: ˑ  reason: contains not printable characters */
    private long f619;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Handler f620;

    /* renamed from: 连任  reason: contains not printable characters */
    private ArrayList<MediaRouter.RouteInfo> f621;

    /* renamed from: 靐  reason: contains not printable characters */
    private final MediaRouterCallback f622;

    /* renamed from: 麤  reason: contains not printable characters */
    private MediaRouteSelector f623;

    /* renamed from: 齉  reason: contains not printable characters */
    private TextView f624;

    /* renamed from: 龘  reason: contains not printable characters */
    private final MediaRouter f625;

    private final class MediaRouterCallback extends MediaRouter.Callback {
        MediaRouterCallback() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m730(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteChooserDialog.this.m724();
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m731(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteChooserDialog.this.dismiss();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m732(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteChooserDialog.this.m724();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m733(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteChooserDialog.this.m724();
        }
    }

    private final class RouteAdapter extends ArrayAdapter<MediaRouter.RouteInfo> implements AdapterView.OnItemClickListener {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Drawable f628;

        /* renamed from: 连任  reason: contains not printable characters */
        private final Drawable f629;

        /* renamed from: 靐  reason: contains not printable characters */
        private final LayoutInflater f630;

        /* renamed from: 麤  reason: contains not printable characters */
        private final Drawable f631;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Drawable f632;

        public RouteAdapter(Context context, List<MediaRouter.RouteInfo> list) {
            super(context, 0, list);
            this.f630 = LayoutInflater.from(context);
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(new int[]{R.attr.mediaRouteDefaultIconDrawable, R.attr.mediaRouteTvIconDrawable, R.attr.mediaRouteSpeakerIconDrawable, R.attr.mediaRouteSpeakerGroupIconDrawable});
            this.f632 = obtainStyledAttributes.getDrawable(0);
            this.f631 = obtainStyledAttributes.getDrawable(1);
            this.f629 = obtainStyledAttributes.getDrawable(2);
            this.f628 = obtainStyledAttributes.getDrawable(3);
            obtainStyledAttributes.recycle();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private Drawable m734(MediaRouter.RouteInfo routeInfo) {
            switch (routeInfo.m1203()) {
                case 1:
                    return this.f631;
                case 2:
                    return this.f629;
                default:
                    return routeInfo instanceof MediaRouter.RouteGroup ? this.f628 : this.f632;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Drawable m735(MediaRouter.RouteInfo routeInfo) {
            Uri r2 = routeInfo.m1199();
            if (r2 != null) {
                try {
                    Drawable createFromStream = Drawable.createFromStream(getContext().getContentResolver().openInputStream(r2), (String) null);
                    if (createFromStream != null) {
                        return createFromStream;
                    }
                } catch (IOException e) {
                    Log.w("MediaRouteChooserDialog", "Failed to load " + r2, e);
                }
            }
            return m734(routeInfo);
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            boolean z = true;
            View view2 = view;
            if (view2 == null) {
                view2 = this.f630.inflate(R.layout.mr_chooser_list_item, viewGroup, false);
            }
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) getItem(i);
            TextView textView = (TextView) view2.findViewById(R.id.mr_chooser_route_name);
            TextView textView2 = (TextView) view2.findViewById(R.id.mr_chooser_route_desc);
            textView.setText(routeInfo.m1221());
            String r0 = routeInfo.m1217();
            if (!(routeInfo.m1211() == 2 || routeInfo.m1211() == 1)) {
                z = false;
            }
            if (!z || TextUtils.isEmpty(r0)) {
                textView.setGravity(16);
                textView2.setVisibility(8);
                textView2.setText("");
            } else {
                textView.setGravity(80);
                textView2.setVisibility(0);
                textView2.setText(r0);
            }
            view2.setEnabled(routeInfo.m1200());
            ImageView imageView = (ImageView) view2.findViewById(R.id.mr_chooser_route_icon);
            if (imageView != null) {
                imageView.setImageDrawable(m735(routeInfo));
            }
            return view2;
        }

        public boolean isEnabled(int i) {
            return ((MediaRouter.RouteInfo) getItem(i)).m1200();
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) getItem(i);
            if (routeInfo.m1200()) {
                routeInfo.m1212();
                MediaRouteChooserDialog.this.dismiss();
            }
        }
    }

    static final class RouteComparator implements Comparator<MediaRouter.RouteInfo> {

        /* renamed from: 龘  reason: contains not printable characters */
        public static final RouteComparator f634 = new RouteComparator();

        RouteComparator() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(MediaRouter.RouteInfo routeInfo, MediaRouter.RouteInfo routeInfo2) {
            return routeInfo.m1221().compareToIgnoreCase(routeInfo2.m1221());
        }
    }

    public MediaRouteChooserDialog(Context context) {
        this(context, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MediaRouteChooserDialog(android.content.Context r2, int r3) {
        /*
            r1 = this;
            r0 = 0
            android.content.Context r2 = android.support.v7.app.MediaRouterThemeHelper.m811((android.content.Context) r2, (int) r3, (boolean) r0)
            int r0 = android.support.v7.app.MediaRouterThemeHelper.m804(r2)
            r1.<init>(r2, r0)
            android.support.v7.media.MediaRouteSelector r0 = android.support.v7.media.MediaRouteSelector.f939
            r1.f623 = r0
            android.support.v7.app.MediaRouteChooserDialog$1 r0 = new android.support.v7.app.MediaRouteChooserDialog$1
            r0.<init>()
            r1.f620 = r0
            android.content.Context r2 = r1.getContext()
            android.support.v7.media.MediaRouter r0 = android.support.v7.media.MediaRouter.m1109((android.content.Context) r2)
            r1.f625 = r0
            android.support.v7.app.MediaRouteChooserDialog$MediaRouterCallback r0 = new android.support.v7.app.MediaRouteChooserDialog$MediaRouterCallback
            r0.<init>()
            r1.f622 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.MediaRouteChooserDialog.<init>(android.content.Context, int):void");
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f618 = true;
        this.f625.m1118(this.f623, this.f622, 1);
        m724();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.mr_chooser_dialog);
        this.f621 = new ArrayList<>();
        this.f616 = new RouteAdapter(getContext(), this.f621);
        this.f617 = (ListView) findViewById(R.id.mr_chooser_list);
        this.f617.setAdapter(this.f616);
        this.f617.setOnItemClickListener(this.f616);
        this.f617.setEmptyView(findViewById(16908292));
        this.f624 = (TextView) findViewById(R.id.mr_chooser_title);
        m726();
    }

    public void onDetachedFromWindow() {
        this.f618 = false;
        this.f625.m1119((MediaRouter.Callback) this.f622);
        this.f620.removeMessages(1);
        super.onDetachedFromWindow();
    }

    public void setTitle(int i) {
        this.f624.setText(i);
    }

    public void setTitle(CharSequence charSequence) {
        this.f624.setText(charSequence);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m724() {
        if (this.f618) {
            ArrayList arrayList = new ArrayList(this.f625.m1114());
            m728((List<MediaRouter.RouteInfo>) arrayList);
            Collections.sort(arrayList, RouteComparator.f634);
            if (SystemClock.uptimeMillis() - this.f619 >= 300) {
                m725(arrayList);
                return;
            }
            this.f620.removeMessages(1);
            this.f620.sendMessageAtTime(this.f620.obtainMessage(1, arrayList), this.f619 + 300);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m725(List<MediaRouter.RouteInfo> list) {
        this.f619 = SystemClock.uptimeMillis();
        this.f621.clear();
        this.f621.addAll(list);
        this.f616.notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m726() {
        getWindow().setLayout(MediaRouteDialogHelper.m795(getContext()), -2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m727(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        } else if (!this.f623.equals(mediaRouteSelector)) {
            this.f623 = mediaRouteSelector;
            if (this.f618) {
                this.f625.m1119((MediaRouter.Callback) this.f622);
                this.f625.m1118(mediaRouteSelector, this.f622, 1);
            }
            m724();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m728(List<MediaRouter.RouteInfo> list) {
        int size = list.size();
        while (true) {
            int i = size;
            size = i - 1;
            if (i <= 0) {
                return;
            }
            if (!m729(list.get(size))) {
                list.remove(size);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m729(MediaRouter.RouteInfo routeInfo) {
        return !routeInfo.m1227() && routeInfo.m1200() && routeInfo.m1225(this.f623);
    }
}
