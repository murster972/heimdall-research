package android.support.v7.app;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class MediaRouteControllerDialogFragment extends DialogFragment {

    /* renamed from: 龘  reason: contains not printable characters */
    private MediaRouteControllerDialog f736;

    public MediaRouteControllerDialogFragment() {
        setCancelable(true);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f736 != null) {
            this.f736.m768();
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        this.f736 = m789(getContext(), bundle);
        return this.f736;
    }

    public void onStop() {
        super.onStop();
        if (this.f736 != null) {
            this.f736.m767(false);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaRouteControllerDialog m789(Context context, Bundle bundle) {
        return new MediaRouteControllerDialog(context);
    }
}
