package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.view.WindowCallbackWrapper;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.widget.DecorToolbar;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ToolbarWidgetWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import java.util.ArrayList;

class ToolbarActionBar extends ActionBar {

    /* renamed from: ʻ  reason: contains not printable characters */
    private ArrayList<ActionBar.OnMenuVisibilityListener> f771 = new ArrayList<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Runnable f772 = new Runnable() {
        public void run() {
            ToolbarActionBar.this.m842();
        }
    };

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Toolbar.OnMenuItemClickListener f773 = new Toolbar.OnMenuItemClickListener() {
        public boolean onMenuItemClick(MenuItem menuItem) {
            return ToolbarActionBar.this.f777.onMenuItemSelected(0, menuItem);
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f774;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean f775;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f776;

    /* renamed from: 齉  reason: contains not printable characters */
    Window.Callback f777;

    /* renamed from: 龘  reason: contains not printable characters */
    DecorToolbar f778;

    private final class ActionMenuPresenterCallback implements MenuPresenter.Callback {

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f781;

        ActionMenuPresenterCallback() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m864(MenuBuilder menuBuilder, boolean z) {
            if (!this.f781) {
                this.f781 = true;
                ToolbarActionBar.this.f778.dismissPopupMenus();
                if (ToolbarActionBar.this.f777 != null) {
                    ToolbarActionBar.this.f777.onPanelClosed(108, menuBuilder);
                }
                this.f781 = false;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m865(MenuBuilder menuBuilder) {
            if (ToolbarActionBar.this.f777 == null) {
                return false;
            }
            ToolbarActionBar.this.f777.onMenuOpened(108, menuBuilder);
            return true;
        }
    }

    private final class MenuBuilderCallback implements MenuBuilder.Callback {
        MenuBuilderCallback() {
        }

        public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
            return false;
        }

        public void onMenuModeChange(MenuBuilder menuBuilder) {
            if (ToolbarActionBar.this.f777 == null) {
                return;
            }
            if (ToolbarActionBar.this.f778.isOverflowMenuShowing()) {
                ToolbarActionBar.this.f777.onPanelClosed(108, menuBuilder);
            } else if (ToolbarActionBar.this.f777.onPreparePanel(0, (View) null, menuBuilder)) {
                ToolbarActionBar.this.f777.onMenuOpened(108, menuBuilder);
            }
        }
    }

    private class ToolbarCallbackWrapper extends WindowCallbackWrapper {
        public ToolbarCallbackWrapper(Window.Callback callback) {
            super(callback);
        }

        public View onCreatePanelView(int i) {
            return i == 0 ? new View(ToolbarActionBar.this.f778.getContext()) : super.onCreatePanelView(i);
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel && !ToolbarActionBar.this.f775) {
                ToolbarActionBar.this.f778.setMenuPrepared();
                ToolbarActionBar.this.f775 = true;
            }
            return onPreparePanel;
        }
    }

    ToolbarActionBar(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.f778 = new ToolbarWidgetWrapper(toolbar, false);
        this.f777 = new ToolbarCallbackWrapper(callback);
        this.f778.setWindowCallback(this.f777);
        toolbar.setOnMenuItemClickListener(this.f773);
        this.f778.setWindowTitle(charSequence);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private Menu m833() {
        if (!this.f776) {
            this.f778.setMenuCallbacks(new ActionMenuPresenterCallback(), new MenuBuilderCallback());
            this.f776 = true;
        }
        return this.f778.getMenu();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m834(boolean z) {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m835() {
        return this.f778.hideOverflowMenu();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m836(boolean z) {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m837() {
        this.f778.getViewGroup().removeCallbacks(this.f772);
        ViewCompat.postOnAnimation(this.f778.getViewGroup(), this.f772);
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m838(boolean z) {
        if (z != this.f774) {
            this.f774 = z;
            int size = this.f771.size();
            for (int i = 0; i < size; i++) {
                this.f771.get(i).m444(z);
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m839() {
        if (!this.f778.hasExpandedActionView()) {
            return false;
        }
        this.f778.collapseActionView();
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m840() {
        this.f778.getViewGroup().removeCallbacks(this.f772);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public Window.Callback m841() {
        return this.f777;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: android.view.Menu} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: android.support.v7.view.menu.MenuBuilder} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: ᐧ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m842() {
        /*
            r5 = this;
            r0 = 0
            android.view.Menu r1 = r5.m833()
            boolean r2 = r1 instanceof android.support.v7.view.menu.MenuBuilder
            if (r2 == 0) goto L_0x000d
            r2 = r1
            android.support.v7.view.menu.MenuBuilder r2 = (android.support.v7.view.menu.MenuBuilder) r2
            r0 = r2
        L_0x000d:
            if (r0 == 0) goto L_0x0012
            r0.stopDispatchingItemsChanged()
        L_0x0012:
            r1.clear()     // Catch:{ all -> 0x0031 }
            android.view.Window$Callback r2 = r5.f777     // Catch:{ all -> 0x0031 }
            r3 = 0
            boolean r2 = r2.onCreatePanelMenu(r3, r1)     // Catch:{ all -> 0x0031 }
            if (r2 == 0) goto L_0x0028
            android.view.Window$Callback r2 = r5.f777     // Catch:{ all -> 0x0031 }
            r3 = 0
            r4 = 0
            boolean r2 = r2.onPreparePanel(r3, r4, r1)     // Catch:{ all -> 0x0031 }
            if (r2 != 0) goto L_0x002b
        L_0x0028:
            r1.clear()     // Catch:{ all -> 0x0031 }
        L_0x002b:
            if (r0 == 0) goto L_0x0030
            r0.startDispatchingItemsChanged()
        L_0x0030:
            return
        L_0x0031:
            r2 = move-exception
            if (r0 == 0) goto L_0x0037
            r0.startDispatchingItemsChanged()
        L_0x0037:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.ToolbarActionBar.m842():void");
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m843() {
        return this.f778.showOverflowMenu();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m844() {
        return this.f778.getDisplayOptions();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m845(int i) {
        this.f778.setNavigationIcon(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m846(CharSequence charSequence) {
        this.f778.setSubtitle(charSequence);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m847(boolean z) {
        m857(z ? 4 : 0, 4);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Context m848() {
        return this.f778.getContext();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m849(boolean z) {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m850() {
        this.f778.setVisibility(8);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m851(int i) {
        this.f778.setNavigationContentDescription(i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m852(CharSequence charSequence) {
        this.f778.setWindowTitle(charSequence);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m853(boolean z) {
        m857(z ? 8 : 0, 8);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m854() {
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m855(float f) {
        ViewCompat.setElevation(this.f778.getViewGroup(), f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m856(int i) {
        switch (this.f778.getNavigationMode()) {
            case 1:
                this.f778.setDropdownSelectedPosition(i);
                return;
            default:
                throw new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m857(int i, int i2) {
        this.f778.setDisplayOptions((i & i2) | ((i2 ^ -1) & this.f778.getDisplayOptions()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m858(Configuration configuration) {
        super.m438(configuration);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m859(Drawable drawable) {
        this.f778.setNavigationIcon(drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m860(CharSequence charSequence) {
        this.f778.setTitle(charSequence);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m861(boolean z) {
        m857(z ? 2 : 0, 2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m862(int i, KeyEvent keyEvent) {
        Menu r1 = m833();
        if (r1 == null) {
            return false;
        }
        r1.setQwertyMode(KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1);
        return r1.performShortcut(i, keyEvent, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m863(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            m843();
        }
        return true;
    }
}
