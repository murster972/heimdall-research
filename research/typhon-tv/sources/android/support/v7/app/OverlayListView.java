package android.support.v7.app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.animation.Interpolator;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class OverlayListView extends ListView {

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<OverlayObject> f749 = new ArrayList();

    public static class OverlayObject {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Rect f750;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f751;

        /* renamed from: ʽ  reason: contains not printable characters */
        private float f752 = 1.0f;

        /* renamed from: ʾ  reason: contains not printable characters */
        private OnAnimationEndListener f753;

        /* renamed from: ˈ  reason: contains not printable characters */
        private boolean f754;

        /* renamed from: ˑ  reason: contains not printable characters */
        private float f755 = 1.0f;

        /* renamed from: ٴ  reason: contains not printable characters */
        private long f756;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private boolean f757;

        /* renamed from: 连任  reason: contains not printable characters */
        private long f758;

        /* renamed from: 靐  reason: contains not printable characters */
        private float f759 = 1.0f;

        /* renamed from: 麤  reason: contains not printable characters */
        private Interpolator f760;

        /* renamed from: 齉  reason: contains not printable characters */
        private Rect f761;

        /* renamed from: 龘  reason: contains not printable characters */
        private BitmapDrawable f762;

        public interface OnAnimationEndListener {
            /* renamed from: 龘  reason: contains not printable characters */
            void m827();
        }

        public OverlayObject(BitmapDrawable bitmapDrawable, Rect rect) {
            this.f762 = bitmapDrawable;
            this.f750 = rect;
            this.f761 = new Rect(rect);
            if (this.f762 != null && this.f761 != null) {
                this.f762.setAlpha((int) (this.f759 * 255.0f));
                this.f762.setBounds(this.f761);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m817(long j) {
            this.f756 = j;
            this.f757 = true;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m818() {
            return this.f757;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m819() {
            this.f757 = true;
            this.f754 = true;
            if (this.f753 != null) {
                this.f753.m827();
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m820(long j) {
            boolean z = true;
            if (this.f754) {
                return false;
            }
            float max = Math.max(0.0f, Math.min(1.0f, ((float) (j - this.f756)) / ((float) this.f758)));
            if (!this.f757) {
                max = 0.0f;
            }
            float interpolation = this.f760 == null ? max : this.f760.getInterpolation(max);
            int i = (int) (((float) this.f751) * interpolation);
            this.f761.top = this.f750.top + i;
            this.f761.bottom = this.f750.bottom + i;
            this.f759 = this.f752 + ((this.f755 - this.f752) * interpolation);
            if (!(this.f762 == null || this.f761 == null)) {
                this.f762.setAlpha((int) (this.f759 * 255.0f));
                this.f762.setBounds(this.f761);
            }
            if (this.f757 && max >= 1.0f) {
                this.f754 = true;
                if (this.f753 != null) {
                    this.f753.m827();
                }
            }
            if (this.f754) {
                z = false;
            }
            return z;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public BitmapDrawable m821() {
            return this.f762;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OverlayObject m822(float f, float f2) {
            this.f752 = f;
            this.f755 = f2;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OverlayObject m823(int i) {
            this.f751 = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OverlayObject m824(long j) {
            this.f758 = j;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OverlayObject m825(OnAnimationEndListener onAnimationEndListener) {
            this.f753 = onAnimationEndListener;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OverlayObject m826(Interpolator interpolator) {
            this.f760 = interpolator;
            return this;
        }
    }

    public OverlayListView(Context context) {
        super(context);
    }

    public OverlayListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public OverlayListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f749.size() > 0) {
            Iterator<OverlayObject> it2 = this.f749.iterator();
            while (it2.hasNext()) {
                OverlayObject next = it2.next();
                BitmapDrawable r0 = next.m821();
                if (r0 != null) {
                    r0.draw(canvas);
                }
                if (!next.m820(getDrawingTime())) {
                    it2.remove();
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m814() {
        for (OverlayObject r0 : this.f749) {
            r0.m819();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m815() {
        for (OverlayObject next : this.f749) {
            if (!next.m818()) {
                next.m817(getDrawingTime());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m816(OverlayObject overlayObject) {
        this.f749.add(overlayObject);
    }
}
