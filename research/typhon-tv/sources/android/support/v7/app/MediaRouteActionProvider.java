package android.support.v7.app;

import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;

public class MediaRouteActionProvider extends ActionProvider {

    /* renamed from: 连任  reason: contains not printable characters */
    private MediaRouteButton f592;

    /* renamed from: 靐  reason: contains not printable characters */
    private final MediaRouterCallback f593;

    /* renamed from: 麤  reason: contains not printable characters */
    private MediaRouteDialogFactory f594 = MediaRouteDialogFactory.m790();

    /* renamed from: 齉  reason: contains not printable characters */
    private MediaRouteSelector f595 = MediaRouteSelector.f939;

    /* renamed from: 龘  reason: contains not printable characters */
    private final MediaRouter f596;

    private static final class MediaRouterCallback extends MediaRouter.Callback {

        /* renamed from: 龘  reason: contains not printable characters */
        private final WeakReference<MediaRouteActionProvider> f597;

        public MediaRouterCallback(MediaRouteActionProvider mediaRouteActionProvider) {
            this.f597 = new WeakReference<>(mediaRouteActionProvider);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m700(MediaRouter mediaRouter) {
            MediaRouteActionProvider mediaRouteActionProvider = (MediaRouteActionProvider) this.f597.get();
            if (mediaRouteActionProvider != null) {
                mediaRouteActionProvider.m696();
            } else {
                mediaRouter.m1119((MediaRouter.Callback) this);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m701(MediaRouter mediaRouter, MediaRouter.ProviderInfo providerInfo) {
            m700(mediaRouter);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m702(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            m700(mediaRouter);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m703(MediaRouter mediaRouter, MediaRouter.ProviderInfo providerInfo) {
            m700(mediaRouter);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m704(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            m700(mediaRouter);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m705(MediaRouter mediaRouter, MediaRouter.ProviderInfo providerInfo) {
            m700(mediaRouter);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m706(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            m700(mediaRouter);
        }
    }

    public MediaRouteActionProvider(Context context) {
        super(context);
        this.f596 = MediaRouter.m1109(context);
        this.f593 = new MediaRouterCallback(this);
    }

    public boolean isVisible() {
        return this.f596.m1121(this.f595, 1);
    }

    public View onCreateActionView() {
        if (this.f592 != null) {
            Log.e("MediaRouteActionProvider", "onCreateActionView: this ActionProvider is already associated with a menu item. Don't reuse MediaRouteActionProvider instances! Abandoning the old menu item...");
        }
        this.f592 = m697();
        this.f592.setCheatSheetEnabled(true);
        this.f592.setRouteSelector(this.f595);
        this.f592.setDialogFactory(this.f594);
        this.f592.setLayoutParams(new ViewGroup.LayoutParams(-2, -1));
        return this.f592;
    }

    public boolean onPerformDefaultAction() {
        if (this.f592 != null) {
            return this.f592.m711();
        }
        return false;
    }

    public boolean overridesItemVisibility() {
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m696() {
        refreshVisibility();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaRouteButton m697() {
        return new MediaRouteButton(getContext());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m698(MediaRouteDialogFactory mediaRouteDialogFactory) {
        if (mediaRouteDialogFactory == null) {
            throw new IllegalArgumentException("factory must not be null");
        } else if (this.f594 != mediaRouteDialogFactory) {
            this.f594 = mediaRouteDialogFactory;
            if (this.f592 != null) {
                this.f592.setDialogFactory(mediaRouteDialogFactory);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m699(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        } else if (!this.f595.equals(mediaRouteSelector)) {
            if (!this.f595.m1099()) {
                this.f596.m1119((MediaRouter.Callback) this.f593);
            }
            if (!mediaRouteSelector.m1099()) {
                this.f596.m1117(mediaRouteSelector, (MediaRouter.Callback) this.f593);
            }
            this.f595 = mediaRouteSelector;
            m696();
            if (this.f592 != null) {
                this.f592.setRouteSelector(mediaRouteSelector);
            }
        }
    }
}
