package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AlertController;
import android.support.v7.appcompat.R;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class AlertDialog extends AppCompatDialog implements DialogInterface {

    /* renamed from: 龘  reason: contains not printable characters */
    final AlertController f485 = new AlertController(getContext(), this, getWindow());

    public static class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f486;

        /* renamed from: 龘  reason: contains not printable characters */
        private final AlertController.AlertParams f487;

        public Builder(Context context) {
            this(context, AlertDialog.m513(context, 0));
        }

        public Builder(Context context, int i) {
            this.f487 = new AlertController.AlertParams(new ContextThemeWrapper(context, AlertDialog.m513(context, i)));
            this.f486 = i;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m520(int i) {
            this.f487.f464 = i;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m521(int i, DialogInterface.OnClickListener onClickListener) {
            this.f487.f453 = this.f487.f465.getText(i);
            this.f487.f442 = onClickListener;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m522(View view) {
            this.f487.f449 = view;
            this.f487.f447 = 0;
            this.f487.f459 = false;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m523(CharSequence charSequence) {
            this.f487.f435 = charSequence;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m524(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.f487.f453 = charSequence;
            this.f487.f442 = onClickListener;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public AlertDialog m525() {
            AlertDialog alertDialog = new AlertDialog(this.f487.f465, this.f486);
            this.f487.m511(alertDialog.f485);
            alertDialog.setCancelable(this.f487.f467);
            if (this.f487.f467) {
                alertDialog.setCanceledOnTouchOutside(true);
            }
            alertDialog.setOnCancelListener(this.f487.f468);
            alertDialog.setOnDismissListener(this.f487.f444);
            if (this.f487.f445 != null) {
                alertDialog.setOnKeyListener(this.f487.f445);
            }
            return alertDialog;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m526(int i) {
            this.f487.f449 = null;
            this.f487.f447 = i;
            this.f487.f459 = false;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m527(int i, DialogInterface.OnClickListener onClickListener) {
            this.f487.f437 = this.f487.f465.getText(i);
            this.f487.f439 = onClickListener;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public AlertDialog m528() {
            AlertDialog r0 = m525();
            r0.show();
            return r0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Context m529() {
            return this.f487.f465;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m530(int i) {
            this.f487.f431 = this.f487.f465.getText(i);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m531(int i, DialogInterface.OnClickListener onClickListener) {
            this.f487.f448 = this.f487.f465.getText(i);
            this.f487.f452 = onClickListener;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m532(DialogInterface.OnKeyListener onKeyListener) {
            this.f487.f445 = onKeyListener;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m533(Drawable drawable) {
            this.f487.f463 = drawable;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m534(View view) {
            this.f487.f433 = view;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m535(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            this.f487.f441 = listAdapter;
            this.f487.f443 = onClickListener;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m536(CharSequence charSequence) {
            this.f487.f431 = charSequence;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m537(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.f487.f448 = charSequence;
            this.f487.f452 = onClickListener;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m538(boolean z) {
            this.f487.f467 = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m539(CharSequence[] charSequenceArr, int i, DialogInterface.OnClickListener onClickListener) {
            this.f487.f446 = charSequenceArr;
            this.f487.f443 = onClickListener;
            this.f487.f454 = i;
            this.f487.f469 = true;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m540(CharSequence[] charSequenceArr, DialogInterface.OnClickListener onClickListener) {
            this.f487.f446 = charSequenceArr;
            this.f487.f443 = onClickListener;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m541(CharSequence[] charSequenceArr, boolean[] zArr, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
            this.f487.f446 = charSequenceArr;
            this.f487.f456 = onMultiChoiceClickListener;
            this.f487.f460 = zArr;
            this.f487.f466 = true;
            return this;
        }
    }

    protected AlertDialog(Context context, int i) {
        super(context, m513(context, i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m513(Context context, int i) {
        if (((i >>> 24) & 255) >= 1) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f485.m503();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.f485.m509(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.f485.m499(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f485.m508(charSequence);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m514(int i) {
        this.f485.m496(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Button m515(int i) {
        return this.f485.m500(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ListView m516() {
        return this.f485.m495();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m517(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.f485.m505(i, charSequence, onClickListener, (Message) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m518(View view) {
        this.f485.m502(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m519(CharSequence charSequence) {
        this.f485.m498(charSequence);
    }
}
