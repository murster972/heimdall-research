package android.support.v7.app;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatMultiAutoCompleteTextView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.TintContextWrapper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InflateException;
import android.view.View;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

class AppCompatViewInflater {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final int[] f583 = {16843375};

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Map<String, Constructor<? extends View>> f584 = new ArrayMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final String[] f585 = {"android.widget.", "android.view.", "android.webkit."};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Class<?>[] f586 = {Context.class, AttributeSet.class};

    /* renamed from: 连任  reason: contains not printable characters */
    private final Object[] f587 = new Object[2];

    private static class DeclaredOnClickListener implements View.OnClickListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f588;

        /* renamed from: 麤  reason: contains not printable characters */
        private Context f589;

        /* renamed from: 齉  reason: contains not printable characters */
        private Method f590;

        /* renamed from: 龘  reason: contains not printable characters */
        private final View f591;

        public DeclaredOnClickListener(View view, String str) {
            this.f591 = view;
            this.f588 = str;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m695(Context context, String str) {
            Method method;
            while (context != null) {
                try {
                    if (!context.isRestricted() && (method = context.getClass().getMethod(this.f588, new Class[]{View.class})) != null) {
                        this.f590 = method;
                        this.f589 = context;
                        return;
                    }
                } catch (NoSuchMethodException e) {
                }
                context = context instanceof ContextWrapper ? ((ContextWrapper) context).getBaseContext() : null;
            }
            int id = this.f591.getId();
            throw new IllegalStateException("Could not find method " + this.f588 + "(View) in a parent or ancestor Context for android:onClick " + "attribute defined on view " + this.f591.getClass() + (id == -1 ? "" : " with id '" + this.f591.getContext().getResources().getResourceEntryName(id) + "'"));
        }

        public void onClick(View view) {
            if (this.f590 == null) {
                m695(this.f591.getContext(), this.f588);
            }
            try {
                this.f590.invoke(this.f589, new Object[]{view});
            } catch (IllegalAccessException e) {
                throw new IllegalStateException("Could not execute non-public method for android:onClick", e);
            } catch (InvocationTargetException e2) {
                throw new IllegalStateException("Could not execute method for android:onClick", e2);
            }
        }
    }

    AppCompatViewInflater() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Context m690(Context context, AttributeSet attributeSet, boolean z, boolean z2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.View, 0, 0);
        int i = 0;
        if (z) {
            i = obtainStyledAttributes.getResourceId(R.styleable.View_android_theme, 0);
        }
        if (z2 && i == 0 && (i = obtainStyledAttributes.getResourceId(R.styleable.View_theme, 0)) != 0) {
            Log.i("AppCompatViewInflater", "app:theme is now deprecated. Please move to using android:theme instead.");
        }
        obtainStyledAttributes.recycle();
        return i != 0 ? (!(context instanceof ContextThemeWrapper) || ((ContextThemeWrapper) context).m1446() != i) ? new ContextThemeWrapper(context, i) : context : context;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: 龘  reason: contains not printable characters */
    private View m691(Context context, String str, AttributeSet attributeSet) {
        if (str.equals("view")) {
            str = attributeSet.getAttributeValue((String) null, "class");
        }
        try {
            this.f587[0] = context;
            this.f587[1] = attributeSet;
            if (-1 == str.indexOf(46)) {
                for (String r4 : f585) {
                    View r2 = m692(context, str, r4);
                    if (r2 != null) {
                        this.f587[0] = null;
                        this.f587[1] = null;
                        return r2;
                    }
                }
                this.f587[0] = null;
                this.f587[1] = null;
                return null;
            }
            View r22 = m692(context, str, (String) null);
            this.f587[0] = null;
            this.f587[1] = null;
            return r22;
        } catch (Exception e) {
            this.f587[0] = null;
            this.f587[1] = null;
            return null;
        } catch (Throwable th) {
            this.f587[0] = null;
            this.f587[1] = null;
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private View m692(Context context, String str, String str2) throws ClassNotFoundException, InflateException {
        Constructor<? extends U> constructor = f584.get(str);
        if (constructor == null) {
            try {
                constructor = context.getClassLoader().loadClass(str2 != null ? str2 + str : str).asSubclass(View.class).getConstructor(f586);
                f584.put(str, constructor);
            } catch (Exception e) {
                return null;
            }
        }
        constructor.setAccessible(true);
        return (View) constructor.newInstance(this.f587);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m693(View view, AttributeSet attributeSet) {
        Context context = view.getContext();
        if (!(context instanceof ContextWrapper)) {
            return;
        }
        if (Build.VERSION.SDK_INT < 15 || ViewCompat.hasOnClickListeners(view)) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f583);
            String string = obtainStyledAttributes.getString(0);
            if (string != null) {
                view.setOnClickListener(new DeclaredOnClickListener(view, string));
            }
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m694(View view, String str, Context context, AttributeSet attributeSet, boolean z, boolean z2, boolean z3, boolean z4) {
        Context context2 = context;
        if (z && view != null) {
            context = view.getContext();
        }
        if (z2 || z3) {
            context = m690(context, attributeSet, z2, z3);
        }
        if (z4) {
            context = TintContextWrapper.wrap(context);
        }
        View view2 = null;
        char c = 65535;
        switch (str.hashCode()) {
            case -1946472170:
                if (str.equals("RatingBar")) {
                    c = 11;
                    break;
                }
                break;
            case -1455429095:
                if (str.equals("CheckedTextView")) {
                    c = 8;
                    break;
                }
                break;
            case -1346021293:
                if (str.equals("MultiAutoCompleteTextView")) {
                    c = 10;
                    break;
                }
                break;
            case -938935918:
                if (str.equals("TextView")) {
                    c = 0;
                    break;
                }
                break;
            case -937446323:
                if (str.equals("ImageButton")) {
                    c = 5;
                    break;
                }
                break;
            case -658531749:
                if (str.equals("SeekBar")) {
                    c = 12;
                    break;
                }
                break;
            case -339785223:
                if (str.equals("Spinner")) {
                    c = 4;
                    break;
                }
                break;
            case 776382189:
                if (str.equals("RadioButton")) {
                    c = 7;
                    break;
                }
                break;
            case 1125864064:
                if (str.equals("ImageView")) {
                    c = 1;
                    break;
                }
                break;
            case 1413872058:
                if (str.equals("AutoCompleteTextView")) {
                    c = 9;
                    break;
                }
                break;
            case 1601505219:
                if (str.equals("CheckBox")) {
                    c = 6;
                    break;
                }
                break;
            case 1666676343:
                if (str.equals("EditText")) {
                    c = 3;
                    break;
                }
                break;
            case 2001146706:
                if (str.equals("Button")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                view2 = new AppCompatTextView(context, attributeSet);
                break;
            case 1:
                view2 = new AppCompatImageView(context, attributeSet);
                break;
            case 2:
                view2 = new AppCompatButton(context, attributeSet);
                break;
            case 3:
                view2 = new AppCompatEditText(context, attributeSet);
                break;
            case 4:
                view2 = new AppCompatSpinner(context, attributeSet);
                break;
            case 5:
                view2 = new AppCompatImageButton(context, attributeSet);
                break;
            case 6:
                view2 = new AppCompatCheckBox(context, attributeSet);
                break;
            case 7:
                view2 = new AppCompatRadioButton(context, attributeSet);
                break;
            case 8:
                view2 = new AppCompatCheckedTextView(context, attributeSet);
                break;
            case 9:
                view2 = new AppCompatAutoCompleteTextView(context, attributeSet);
                break;
            case 10:
                view2 = new AppCompatMultiAutoCompleteTextView(context, attributeSet);
                break;
            case 11:
                view2 = new AppCompatRatingBar(context, attributeSet);
                break;
            case 12:
                view2 = new AppCompatSeekBar(context, attributeSet);
                break;
        }
        if (view2 == null && context2 != context) {
            view2 = m691(context, str, attributeSet);
        }
        if (view2 != null) {
            m693(view2, attributeSet);
        }
        return view2;
    }
}
