package android.support.v7.app;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.media.MediaRouteSelector;

public class MediaRouteChooserDialogFragment extends DialogFragment {

    /* renamed from: 靐  reason: contains not printable characters */
    private MediaRouteChooserDialog f635;

    /* renamed from: 齉  reason: contains not printable characters */
    private MediaRouteSelector f636;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f637 = "selector";

    public MediaRouteChooserDialogFragment() {
        setCancelable(true);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m737() {
        if (this.f636 == null) {
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.f636 = MediaRouteSelector.m1095(arguments.getBundle("selector"));
            }
            if (this.f636 == null) {
                this.f636 = MediaRouteSelector.f939;
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f635 != null) {
            this.f635.m726();
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        this.f635 = m738(getContext(), bundle);
        this.f635.m727(m739());
        return this.f635;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaRouteChooserDialog m738(Context context, Bundle bundle) {
        return new MediaRouteChooserDialog(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaRouteSelector m739() {
        m737();
        return this.f636;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m740(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        }
        m737();
        if (!this.f636.equals(mediaRouteSelector)) {
            this.f636 = mediaRouteSelector;
            Bundle arguments = getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putBundle("selector", mediaRouteSelector.m1096());
            setArguments(arguments);
            MediaRouteChooserDialog mediaRouteChooserDialog = (MediaRouteChooserDialog) getDialog();
            if (mediaRouteChooserDialog != null) {
                mediaRouteChooserDialog.m727(mediaRouteSelector);
            }
        }
    }
}
