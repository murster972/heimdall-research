package android.support.v7.app;

class TwilightCalculator {

    /* renamed from: 麤  reason: contains not printable characters */
    private static TwilightCalculator f785;

    /* renamed from: 靐  reason: contains not printable characters */
    public long f786;

    /* renamed from: 齉  reason: contains not printable characters */
    public int f787;

    /* renamed from: 龘  reason: contains not printable characters */
    public long f788;

    TwilightCalculator() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static TwilightCalculator m866() {
        if (f785 == null) {
            f785 = new TwilightCalculator();
        }
        return f785;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m867(long j, double d, double d2) {
        float f = ((float) (j - 946728000000L)) / 8.64E7f;
        float f2 = 6.24006f + (0.01720197f * f);
        double sin = 1.796593063d + ((double) f2) + (0.03341960161924362d * Math.sin((double) f2)) + (3.4906598739326E-4d * Math.sin((double) (2.0f * f2))) + (5.236000106378924E-6d * Math.sin((double) (3.0f * f2))) + 3.141592653589793d;
        double d3 = (-d2) / 360.0d;
        double round = ((double) (9.0E-4f + ((float) Math.round(((double) (f - 9.0E-4f)) - d3)))) + d3 + (0.0053d * Math.sin((double) f2)) + (-0.0069d * Math.sin(2.0d * sin));
        double asin = Math.asin(Math.sin(sin) * Math.sin(0.4092797040939331d));
        double d4 = d * 0.01745329238474369d;
        double sin2 = (Math.sin(-0.10471975803375244d) - (Math.sin(d4) * Math.sin(asin))) / (Math.cos(d4) * Math.cos(asin));
        if (sin2 >= 1.0d) {
            this.f787 = 1;
            this.f788 = -1;
            this.f786 = -1;
        } else if (sin2 <= -1.0d) {
            this.f787 = 0;
            this.f788 = -1;
            this.f786 = -1;
        } else {
            float acos = (float) (Math.acos(sin2) / 6.283185307179586d);
            this.f788 = Math.round((((double) acos) + round) * 8.64E7d) + 946728000000L;
            this.f786 = Math.round((round - ((double) acos)) * 8.64E7d) + 946728000000L;
            if (this.f786 >= j || this.f788 <= j) {
                this.f787 = 1;
            } else {
                this.f787 = 0;
            }
        }
    }
}
