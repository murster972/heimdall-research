package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

public abstract class AppCompatDelegate {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f488 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private static int f489 = -1;

    AppCompatDelegate() {
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static boolean m542() {
        return f488;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static int m543() {
        return f489;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static AppCompatDelegate m544(Activity activity, AppCompatCallback appCompatCallback) {
        return m546(activity, activity.getWindow(), appCompatCallback);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static AppCompatDelegate m545(Dialog dialog, AppCompatCallback appCompatCallback) {
        return m546(dialog.getContext(), dialog.getWindow(), appCompatCallback);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static AppCompatDelegate m546(Context context, Window window, AppCompatCallback appCompatCallback) {
        return Build.VERSION.SDK_INT >= 24 ? new AppCompatDelegateImplN(context, window, appCompatCallback) : Build.VERSION.SDK_INT >= 23 ? new AppCompatDelegateImplV23(context, window, appCompatCallback) : Build.VERSION.SDK_INT >= 14 ? new AppCompatDelegateImplV14(context, window, appCompatCallback) : Build.VERSION.SDK_INT >= 11 ? new AppCompatDelegateImplV11(context, window, appCompatCallback) : new AppCompatDelegateImplV9(context, window, appCompatCallback);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m547();

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m548();

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract ActionBarDrawerToggle.Delegate m549();

    /* renamed from: ˑ  reason: contains not printable characters */
    public abstract void m550();

    /* renamed from: ٴ  reason: contains not printable characters */
    public abstract boolean m551();

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract void m552();

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract MenuInflater m553();

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m554(int i);

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m555(Bundle bundle);

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m556(View view, ViewGroup.LayoutParams layoutParams);

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract void m557();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m558();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m559(Bundle bundle);

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract boolean m560(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ActionBar m561();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ActionMode m562(ActionMode.Callback callback);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract <T extends View> T m563(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m564(Configuration configuration);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m565(Bundle bundle);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m566(Toolbar toolbar);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m567(View view);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m568(View view, ViewGroup.LayoutParams layoutParams);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m569(CharSequence charSequence);
}
