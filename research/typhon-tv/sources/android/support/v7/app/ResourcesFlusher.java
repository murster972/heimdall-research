package android.support.v7.app;

import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import java.lang.reflect.Field;

class ResourcesFlusher {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean f763;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Field f764;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static boolean f765;

    /* renamed from: 连任  reason: contains not printable characters */
    private static Field f766;

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f767;

    /* renamed from: 麤  reason: contains not printable characters */
    private static boolean f768;

    /* renamed from: 齉  reason: contains not printable characters */
    private static Class f769;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Field f770;

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.util.Map} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m828(android.content.res.Resources r6) {
        /*
            r4 = 1
            boolean r3 = f767
            if (r3 != 0) goto L_0x0018
            java.lang.Class<android.content.res.Resources> r3 = android.content.res.Resources.class
            java.lang.String r5 = "mDrawableCache"
            java.lang.reflect.Field r3 = r3.getDeclaredField(r5)     // Catch:{ NoSuchFieldException -> 0x002e }
            f770 = r3     // Catch:{ NoSuchFieldException -> 0x002e }
            java.lang.reflect.Field r3 = f770     // Catch:{ NoSuchFieldException -> 0x002e }
            r5 = 1
            r3.setAccessible(r5)     // Catch:{ NoSuchFieldException -> 0x002e }
        L_0x0016:
            f767 = r4
        L_0x0018:
            java.lang.reflect.Field r3 = f770
            if (r3 == 0) goto L_0x0044
            r1 = 0
            java.lang.reflect.Field r3 = f770     // Catch:{ IllegalAccessException -> 0x0039 }
            java.lang.Object r3 = r3.get(r6)     // Catch:{ IllegalAccessException -> 0x0039 }
            r0 = r3
            java.util.Map r0 = (java.util.Map) r0     // Catch:{ IllegalAccessException -> 0x0039 }
            r1 = r0
        L_0x0027:
            if (r1 == 0) goto L_0x0044
            r1.clear()
            r3 = r4
        L_0x002d:
            return r3
        L_0x002e:
            r2 = move-exception
            java.lang.String r3 = "ResourcesFlusher"
            java.lang.String r5 = "Could not retrieve Resources#mDrawableCache field"
            android.util.Log.e(r3, r5, r2)
            goto L_0x0016
        L_0x0039:
            r2 = move-exception
            java.lang.String r3 = "ResourcesFlusher"
            java.lang.String r5 = "Could not retrieve value from Resources#mDrawableCache"
            android.util.Log.e(r3, r5, r2)
            goto L_0x0027
        L_0x0044:
            r3 = 0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.ResourcesFlusher.m828(android.content.res.Resources):boolean");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static boolean m829(Resources resources) {
        boolean z = true;
        if (!f765) {
            try {
                f764 = Resources.class.getDeclaredField("mResourcesImpl");
                f764.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mResourcesImpl field", e);
            }
            f765 = true;
        }
        if (f764 == null) {
            return false;
        }
        Object obj = null;
        try {
            obj = f764.get(resources);
        } catch (IllegalAccessException e2) {
            Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mResourcesImpl", e2);
        }
        if (obj == null) {
            return false;
        }
        if (!f767) {
            try {
                f770 = obj.getClass().getDeclaredField("mDrawableCache");
                f770.setAccessible(true);
            } catch (NoSuchFieldException e3) {
                Log.e("ResourcesFlusher", "Could not retrieve ResourcesImpl#mDrawableCache field", e3);
            }
            f767 = true;
        }
        Object obj2 = null;
        if (f770 != null) {
            try {
                obj2 = f770.get(obj);
            } catch (IllegalAccessException e4) {
                Log.e("ResourcesFlusher", "Could not retrieve value from ResourcesImpl#mDrawableCache", e4);
            }
        }
        if (obj2 == null || !m832(obj2)) {
            z = false;
        }
        return z;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static boolean m830(Resources resources) {
        boolean z = true;
        if (!f767) {
            try {
                f770 = Resources.class.getDeclaredField("mDrawableCache");
                f770.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", e);
            }
            f767 = true;
        }
        Object obj = null;
        if (f770 != null) {
            try {
                obj = f770.get(resources);
            } catch (IllegalAccessException e2) {
                Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", e2);
            }
        }
        if (obj == null) {
            return false;
        }
        if (obj == null || !m832(obj)) {
            z = false;
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m831(Resources resources) {
        if (Build.VERSION.SDK_INT >= 24) {
            return m829(resources);
        }
        if (Build.VERSION.SDK_INT >= 23) {
            return m830(resources);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            return m828(resources);
        }
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: android.util.LongSparseArray} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m832(java.lang.Object r8) {
        /*
            r5 = 0
            r6 = 1
            boolean r4 = f768
            if (r4 != 0) goto L_0x0011
            java.lang.String r4 = "android.content.res.ThemedResourceCache"
            java.lang.Class r4 = java.lang.Class.forName(r4)     // Catch:{ ClassNotFoundException -> 0x0017 }
            f769 = r4     // Catch:{ ClassNotFoundException -> 0x0017 }
        L_0x000f:
            f768 = r6
        L_0x0011:
            java.lang.Class r4 = f769
            if (r4 != 0) goto L_0x0022
            r4 = r5
        L_0x0016:
            return r4
        L_0x0017:
            r1 = move-exception
            java.lang.String r4 = "ResourcesFlusher"
            java.lang.String r7 = "Could not find ThemedResourceCache class"
            android.util.Log.e(r4, r7, r1)
            goto L_0x000f
        L_0x0022:
            boolean r4 = f763
            if (r4 != 0) goto L_0x0039
            java.lang.Class r4 = f769     // Catch:{ NoSuchFieldException -> 0x003f }
            java.lang.String r7 = "mUnthemedEntries"
            java.lang.reflect.Field r4 = r4.getDeclaredField(r7)     // Catch:{ NoSuchFieldException -> 0x003f }
            f766 = r4     // Catch:{ NoSuchFieldException -> 0x003f }
            java.lang.reflect.Field r4 = f766     // Catch:{ NoSuchFieldException -> 0x003f }
            r7 = 1
            r4.setAccessible(r7)     // Catch:{ NoSuchFieldException -> 0x003f }
        L_0x0037:
            f763 = r6
        L_0x0039:
            java.lang.reflect.Field r4 = f766
            if (r4 != 0) goto L_0x004a
            r4 = r5
            goto L_0x0016
        L_0x003f:
            r2 = move-exception
            java.lang.String r4 = "ResourcesFlusher"
            java.lang.String r7 = "Could not retrieve ThemedResourceCache#mUnthemedEntries field"
            android.util.Log.e(r4, r7, r2)
            goto L_0x0037
        L_0x004a:
            r3 = 0
            java.lang.reflect.Field r4 = f766     // Catch:{ IllegalAccessException -> 0x005c }
            java.lang.Object r4 = r4.get(r8)     // Catch:{ IllegalAccessException -> 0x005c }
            r0 = r4
            android.util.LongSparseArray r0 = (android.util.LongSparseArray) r0     // Catch:{ IllegalAccessException -> 0x005c }
            r3 = r0
        L_0x0055:
            if (r3 == 0) goto L_0x0067
            r3.clear()
            r4 = r6
            goto L_0x0016
        L_0x005c:
            r1 = move-exception
            java.lang.String r4 = "ResourcesFlusher"
            java.lang.String r7 = "Could not retrieve value from ThemedResourceCache#mUnthemedEntries"
            android.util.Log.e(r4, r7, r1)
            goto L_0x0055
        L_0x0067:
            r4 = r5
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.ResourcesFlusher.m832(java.lang.Object):boolean");
    }
}
