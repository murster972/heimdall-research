package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.mediarouter.R;
import android.support.v7.widget.TooltipCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;

public class MediaRouteButton extends View {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final SparseArray<Drawable.ConstantState> f598 = new SparseArray<>(2);

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final int[] f599 = {16842912};

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final int[] f600 = {16842911};
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public RemoteIndicatorLoader f601;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Drawable f602;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f603;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f604;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f605;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f606;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private ColorStateList f607;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f608;

    /* renamed from: 靐  reason: contains not printable characters */
    private final MediaRouterCallback f609;

    /* renamed from: 麤  reason: contains not printable characters */
    private MediaRouteDialogFactory f610;

    /* renamed from: 齉  reason: contains not printable characters */
    private MediaRouteSelector f611;

    /* renamed from: 龘  reason: contains not printable characters */
    private final MediaRouter f612;

    private final class MediaRouterCallback extends MediaRouter.Callback {
        MediaRouterCallback() {
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public void m712(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.m710();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m713(MediaRouter mediaRouter, MediaRouter.ProviderInfo providerInfo) {
            MediaRouteButton.this.m710();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m714(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.m710();
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m715(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.m710();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m716(MediaRouter mediaRouter, MediaRouter.ProviderInfo providerInfo) {
            MediaRouteButton.this.m710();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m717(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.m710();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m718(MediaRouter mediaRouter, MediaRouter.ProviderInfo providerInfo) {
            MediaRouteButton.this.m710();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m719(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteButton.this.m710();
        }
    }

    private final class RemoteIndicatorLoader extends AsyncTask<Void, Void, Drawable> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f614;

        RemoteIndicatorLoader(int i) {
            this.f614 = i;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private void m720(Drawable drawable) {
            if (drawable != null) {
                MediaRouteButton.f598.put(this.f614, drawable.getConstantState());
            }
            RemoteIndicatorLoader unused = MediaRouteButton.this.f601 = null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public void onCancelled(Drawable drawable) {
            m720(drawable);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Drawable doInBackground(Void... voidArr) {
            return MediaRouteButton.this.getContext().getResources().getDrawable(this.f614);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void onPostExecute(Drawable drawable) {
            m720(drawable);
            MediaRouteButton.this.setRemoteIndicatorDrawable(drawable);
        }
    }

    public MediaRouteButton(Context context) {
        this(context, (AttributeSet) null);
    }

    public MediaRouteButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.mediaRouteButtonStyle);
    }

    public MediaRouteButton(Context context, AttributeSet attributeSet, int i) {
        super(MediaRouterThemeHelper.m810(context), attributeSet, i);
        this.f611 = MediaRouteSelector.f939;
        this.f610 = MediaRouteDialogFactory.m790();
        Context context2 = getContext();
        this.f612 = MediaRouter.m1109(context2);
        this.f609 = new MediaRouterCallback();
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, R.styleable.MediaRouteButton, i, 0);
        this.f607 = obtainStyledAttributes.getColorStateList(R.styleable.MediaRouteButton_mediaRouteButtonTint);
        this.f604 = obtainStyledAttributes.getDimensionPixelSize(R.styleable.MediaRouteButton_android_minWidth, 0);
        this.f603 = obtainStyledAttributes.getDimensionPixelSize(R.styleable.MediaRouteButton_android_minHeight, 0);
        int resourceId = obtainStyledAttributes.getResourceId(R.styleable.MediaRouteButton_externalRouteEnabledDrawable, 0);
        obtainStyledAttributes.recycle();
        if (resourceId != 0) {
            Drawable.ConstantState constantState = f598.get(resourceId);
            if (constantState != null) {
                setRemoteIndicatorDrawable(constantState.newDrawable());
            } else {
                this.f601 = new RemoteIndicatorLoader(resourceId);
                this.f601.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            }
        }
        m707();
        setClickable(true);
    }

    private Activity getActivity() {
        for (Context context = getContext(); context instanceof ContextWrapper; context = ((ContextWrapper) context).getBaseContext()) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
        }
        return null;
    }

    private FragmentManager getFragmentManager() {
        Activity activity = getActivity();
        if (activity instanceof FragmentActivity) {
            return ((FragmentActivity) activity).getSupportFragmentManager();
        }
        return null;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m707() {
        setContentDescription(getContext().getString(this.f606 ? R.string.mr_cast_button_connecting : this.f605 ? R.string.mr_cast_button_connected : R.string.mr_cast_button_disconnected));
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f602 != null) {
            this.f602.setState(getDrawableState());
            invalidate();
        }
    }

    public MediaRouteDialogFactory getDialogFactory() {
        return this.f610;
    }

    public MediaRouteSelector getRouteSelector() {
        return this.f611;
    }

    public void jumpDrawablesToCurrentState() {
        if (getBackground() != null) {
            DrawableCompat.jumpToCurrentState(getBackground());
        }
        if (this.f602 != null) {
            DrawableCompat.jumpToCurrentState(this.f602);
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f608 = true;
        if (!this.f611.m1099()) {
            this.f612.m1117(this.f611, (MediaRouter.Callback) this.f609);
        }
        m710();
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (this.f606) {
            mergeDrawableStates(onCreateDrawableState, f600);
        } else if (this.f605) {
            mergeDrawableStates(onCreateDrawableState, f599);
        }
        return onCreateDrawableState;
    }

    public void onDetachedFromWindow() {
        this.f608 = false;
        if (!this.f611.m1099()) {
            this.f612.m1119((MediaRouter.Callback) this.f609);
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f602 != null) {
            int paddingLeft = getPaddingLeft();
            int width = getWidth() - getPaddingRight();
            int paddingTop = getPaddingTop();
            int height = getHeight() - getPaddingBottom();
            int intrinsicWidth = this.f602.getIntrinsicWidth();
            int intrinsicHeight = this.f602.getIntrinsicHeight();
            int i = paddingLeft + (((width - paddingLeft) - intrinsicWidth) / 2);
            int i2 = paddingTop + (((height - paddingTop) - intrinsicHeight) / 2);
            this.f602.setBounds(i, i2, i + intrinsicWidth, i2 + intrinsicHeight);
            this.f602.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int min;
        int min2;
        int i3 = 0;
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int max = Math.max(this.f604, this.f602 != null ? this.f602.getIntrinsicWidth() + getPaddingLeft() + getPaddingRight() : 0);
        int i4 = this.f603;
        if (this.f602 != null) {
            i3 = this.f602.getIntrinsicHeight() + getPaddingTop() + getPaddingBottom();
        }
        int max2 = Math.max(i4, i3);
        switch (mode) {
            case Integer.MIN_VALUE:
                min = Math.min(size, max);
                break;
            case 1073741824:
                min = size;
                break;
            default:
                min = max;
                break;
        }
        switch (mode2) {
            case Integer.MIN_VALUE:
                min2 = Math.min(size2, max2);
                break;
            case 1073741824:
                min2 = size2;
                break;
            default:
                min2 = max2;
                break;
        }
        setMeasuredDimension(min, min2);
    }

    public boolean performClick() {
        boolean performClick = super.performClick();
        if (!performClick) {
            playSoundEffect(0);
        }
        return m711() || performClick;
    }

    /* access modifiers changed from: package-private */
    public void setCheatSheetEnabled(boolean z) {
        TooltipCompat.setTooltipText(this, z ? getContext().getString(R.string.mr_button_content_description) : null);
    }

    public void setDialogFactory(MediaRouteDialogFactory mediaRouteDialogFactory) {
        if (mediaRouteDialogFactory == null) {
            throw new IllegalArgumentException("factory must not be null");
        }
        this.f610 = mediaRouteDialogFactory;
    }

    public void setRemoteIndicatorDrawable(Drawable drawable) {
        if (this.f601 != null) {
            this.f601.cancel(false);
        }
        if (this.f602 != null) {
            this.f602.setCallback((Drawable.Callback) null);
            unscheduleDrawable(this.f602);
        }
        if (drawable != null) {
            if (this.f607 != null) {
                drawable = DrawableCompat.wrap(drawable.mutate());
                DrawableCompat.setTintList(drawable, this.f607);
            }
            drawable.setCallback(this);
            drawable.setState(getDrawableState());
            drawable.setVisible(getVisibility() == 0, false);
        }
        this.f602 = drawable;
        refreshDrawableState();
        if (this.f608 && this.f602 != null && (this.f602.getCurrent() instanceof AnimationDrawable)) {
            AnimationDrawable animationDrawable = (AnimationDrawable) this.f602.getCurrent();
            if (this.f606) {
                if (!animationDrawable.isRunning()) {
                    animationDrawable.start();
                }
            } else if (this.f605) {
                if (animationDrawable.isRunning()) {
                    animationDrawable.stop();
                }
                animationDrawable.selectDrawable(animationDrawable.getNumberOfFrames() - 1);
            }
        }
    }

    public void setRouteSelector(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        } else if (!this.f611.equals(mediaRouteSelector)) {
            if (this.f608) {
                if (!this.f611.m1099()) {
                    this.f612.m1119((MediaRouter.Callback) this.f609);
                }
                if (!mediaRouteSelector.m1099()) {
                    this.f612.m1117(mediaRouteSelector, (MediaRouter.Callback) this.f609);
                }
            }
            this.f611 = mediaRouteSelector;
            m710();
        }
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        if (this.f602 != null) {
            this.f602.setVisible(getVisibility() == 0, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f602;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m710() {
        boolean z = false;
        MediaRouter.RouteInfo r4 = this.f612.m1113();
        boolean z2 = !r4.m1227() && r4.m1225(this.f611);
        if (z2 && r4.m1201()) {
            z = true;
        }
        boolean z3 = false;
        if (this.f605 != z2) {
            this.f605 = z2;
            z3 = true;
        }
        if (this.f606 != z) {
            this.f606 = z;
            z3 = true;
        }
        if (z3) {
            m707();
            refreshDrawableState();
        }
        if (this.f608) {
            setEnabled(this.f612.m1121(this.f611, 1));
        }
        if (this.f602 != null && (this.f602.getCurrent() instanceof AnimationDrawable)) {
            AnimationDrawable animationDrawable = (AnimationDrawable) this.f602.getCurrent();
            if (this.f608) {
                if ((z3 || z) && !animationDrawable.isRunning()) {
                    animationDrawable.start();
                }
            } else if (z2 && !z) {
                if (animationDrawable.isRunning()) {
                    animationDrawable.stop();
                }
                animationDrawable.selectDrawable(animationDrawable.getNumberOfFrames() - 1);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m711() {
        if (!this.f608) {
            return false;
        }
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null) {
            throw new IllegalStateException("The activity must be a subclass of FragmentActivity");
        }
        MediaRouter.RouteInfo r2 = this.f612.m1113();
        if (r2.m1227() || !r2.m1225(this.f611)) {
            if (fragmentManager.findFragmentByTag("android.support.v7.mediarouter:MediaRouteChooserDialogFragment") != null) {
                Log.w("MediaRouteButton", "showDialog(): Route chooser dialog already showing!");
                return false;
            }
            MediaRouteChooserDialogFragment r0 = this.f610.m791();
            r0.m740(this.f611);
            r0.show(fragmentManager, "android.support.v7.mediarouter:MediaRouteChooserDialogFragment");
        } else if (fragmentManager.findFragmentByTag("android.support.v7.mediarouter:MediaRouteControllerDialogFragment") != null) {
            Log.w("MediaRouteButton", "showDialog(): Route controller dialog already showing!");
            return false;
        } else {
            this.f610.m792().show(fragmentManager, "android.support.v7.mediarouter:MediaRouteControllerDialogFragment");
        }
        return true;
    }
}
