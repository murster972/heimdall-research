package android.support.v7.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.mediarouter.R;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;

final class MediaRouterThemeHelper {
    /* renamed from: ʻ  reason: contains not printable characters */
    private static int m802(Context context) {
        return m803(context) ? m805(context, 0) == -570425344 ? R.style.Theme_MediaRouter_Light : R.style.Theme_MediaRouter_Light_DarkControlPanel : m805(context, 0) == -570425344 ? R.style.Theme_MediaRouter_LightControlPanel : R.style.Theme_MediaRouter;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static boolean m803(Context context) {
        TypedValue typedValue = new TypedValue();
        return context.getTheme().resolveAttribute(android.support.v7.appcompat.R.attr.isLightTheme, typedValue, true) && typedValue.data != 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static int m804(Context context) {
        int r0 = m808(context, R.attr.mediaRouteTheme);
        return r0 == 0 ? m802(context) : r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static int m805(Context context, int i) {
        return ColorUtils.calculateContrast(-1, m809(context, i, android.support.v7.appcompat.R.attr.colorPrimary)) >= 3.0d ? -1 : -570425344;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static int m806(Context context) {
        int r1 = m809(context, 0, android.support.v7.appcompat.R.attr.colorPrimary);
        return ColorUtils.calculateContrast(r1, m809(context, 0, 16842801)) < 3.0d ? m809(context, 0, android.support.v7.appcompat.R.attr.colorAccent) : r1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static float m807(Context context) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(16842803, typedValue, true)) {
            return typedValue.getFloat();
        }
        return 0.5f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m808(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return typedValue.resourceId;
        }
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m809(Context context, int i, int i2) {
        if (i != 0) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, new int[]{i2});
            int color = obtainStyledAttributes.getColor(0, 0);
            obtainStyledAttributes.recycle();
            if (color != 0) {
                return color;
            }
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i2, typedValue, true);
        return typedValue.resourceId != 0 ? context.getResources().getColor(typedValue.resourceId) : typedValue.data;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Context m810(Context context) {
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, m802(context));
        int r1 = m808(contextThemeWrapper, R.attr.mediaRouteTheme);
        return r1 != 0 ? new ContextThemeWrapper(contextThemeWrapper, r1) : contextThemeWrapper;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Context m811(Context context, int i, boolean z) {
        if (i == 0) {
            i = m808(context, !z ? android.support.v7.appcompat.R.attr.dialogTheme : android.support.v7.appcompat.R.attr.alertDialogTheme);
        }
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, i);
        return m808(contextThemeWrapper, R.attr.mediaRouteTheme) != 0 ? new ContextThemeWrapper(contextThemeWrapper, m802(contextThemeWrapper)) : contextThemeWrapper;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m812(Context context, MediaRouteVolumeSlider mediaRouteVolumeSlider, View view) {
        int r1 = m805(context, 0);
        if (Color.alpha(r1) != 255) {
            r1 = ColorUtils.compositeColors(r1, ((Integer) view.getTag()).intValue());
        }
        mediaRouteVolumeSlider.m800(r1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m813(Context context, View view, View view2, boolean z) {
        int r0 = m809(context, 0, android.support.v7.appcompat.R.attr.colorPrimary);
        int r1 = m809(context, 0, android.support.v7.appcompat.R.attr.colorPrimaryDark);
        if (z && m805(context, 0) == -570425344) {
            r1 = r0;
            r0 = -1;
        }
        view.setBackgroundColor(r0);
        view2.setBackgroundColor(r1);
        view.setTag(Integer.valueOf(r0));
        view2.setTag(Integer.valueOf(r1));
    }
}
