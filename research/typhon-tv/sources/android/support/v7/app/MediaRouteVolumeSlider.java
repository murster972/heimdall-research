package android.support.v7.app;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.appcompat.R;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.util.Log;

class MediaRouteVolumeSlider extends AppCompatSeekBar {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f745;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f746;

    /* renamed from: 齉  reason: contains not printable characters */
    private Drawable f747;

    /* renamed from: 龘  reason: contains not printable characters */
    private final float f748;

    public MediaRouteVolumeSlider(Context context) {
        this(context, (AttributeSet) null);
    }

    public MediaRouteVolumeSlider(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.seekBarStyle);
    }

    public MediaRouteVolumeSlider(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f748 = MediaRouterThemeHelper.m807(context);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int i = isEnabled() ? 255 : (int) (255.0f * this.f748);
        this.f747.setColorFilter(this.f746, PorterDuff.Mode.SRC_IN);
        this.f747.setAlpha(i);
        getProgressDrawable().setColorFilter(this.f746, PorterDuff.Mode.SRC_IN);
        getProgressDrawable().setAlpha(i);
    }

    public void setThumb(Drawable drawable) {
        this.f747 = drawable;
        super.setThumb(this.f745 ? null : this.f747);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m800(int i) {
        if (this.f746 != i) {
            if (Color.alpha(i) != 255) {
                Log.e("MediaRouteVolumeSlider", "Volume slider color cannot be translucent: #" + Integer.toHexString(i));
            }
            this.f746 = i;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m801(boolean z) {
        if (this.f745 != z) {
            this.f745 = z;
            super.setThumb(this.f745 ? null : this.f747);
        }
    }
}
