package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.ViewPropertyAnimatorUpdateListener;
import android.support.v7.app.ActionBar;
import android.support.v7.appcompat.R;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.view.ActionMode;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.ViewPropertyAnimatorCompatSet;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.ActionBarContainer;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ActionBarOverlayLayout;
import android.support.v7.widget.DecorToolbar;
import android.support.v7.widget.ScrollingTabContainerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

@RestrictTo
public class WindowDecorActionBar extends ActionBar implements ActionBarOverlayLayout.ActionBarVisibilityCallback {

    /* renamed from: ˆ  reason: contains not printable characters */
    private static final Interpolator f799 = new AccelerateInterpolator();

    /* renamed from: ˉ  reason: contains not printable characters */
    private static final Interpolator f800 = new DecelerateInterpolator();

    /* renamed from: ˎ  reason: contains not printable characters */
    static final /* synthetic */ boolean f801 = (!WindowDecorActionBar.class.desiredAssertionStatus());

    /* renamed from: ʻ  reason: contains not printable characters */
    View f802;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private boolean f803 = true;

    /* renamed from: ʼ  reason: contains not printable characters */
    ScrollingTabContainerView f804;

    /* renamed from: ʽ  reason: contains not printable characters */
    ActionModeImpl f805;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f806;

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f807;

    /* renamed from: ʿ  reason: contains not printable characters */
    ViewPropertyAnimatorCompatSet f808;

    /* renamed from: ˈ  reason: contains not printable characters */
    boolean f809;

    /* renamed from: ˊ  reason: contains not printable characters */
    final ViewPropertyAnimatorListener f810 = new ViewPropertyAnimatorListenerAdapter() {
        public void onAnimationEnd(View view) {
            WindowDecorActionBar.this.f808 = null;
            WindowDecorActionBar.this.f828.requestLayout();
        }
    };

    /* renamed from: ˋ  reason: contains not printable characters */
    final ViewPropertyAnimatorUpdateListener f811 = new ViewPropertyAnimatorUpdateListener() {
        public void onAnimationUpdate(View view) {
            ((View) WindowDecorActionBar.this.f828.getParent()).invalidate();
        }
    };

    /* renamed from: ˏ  reason: contains not printable characters */
    private Context f812;

    /* renamed from: ˑ  reason: contains not printable characters */
    ActionMode f813;

    /* renamed from: י  reason: contains not printable characters */
    private Activity f814;

    /* renamed from: ـ  reason: contains not printable characters */
    private Dialog f815;

    /* renamed from: ٴ  reason: contains not printable characters */
    ActionMode.Callback f816;

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean f817 = true;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private int f818 = 0;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private ArrayList<TabImpl> f819 = new ArrayList<>();

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private boolean f820;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private TabImpl f821;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f822 = -1;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f823;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f824;

    /* renamed from: 连任  reason: contains not printable characters */
    ActionBarContextView f825;

    /* renamed from: 靐  reason: contains not printable characters */
    ActionBarOverlayLayout f826;

    /* renamed from: 麤  reason: contains not printable characters */
    DecorToolbar f827;

    /* renamed from: 齉  reason: contains not printable characters */
    ActionBarContainer f828;

    /* renamed from: 龘  reason: contains not printable characters */
    Context f829;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private ArrayList<ActionBar.OnMenuVisibilityListener> f830 = new ArrayList<>();

    /* renamed from: ﹶ  reason: contains not printable characters */
    boolean f831;

    /* renamed from: ﾞ  reason: contains not printable characters */
    final ViewPropertyAnimatorListener f832 = new ViewPropertyAnimatorListenerAdapter() {
        public void onAnimationEnd(View view) {
            if (WindowDecorActionBar.this.f817 && WindowDecorActionBar.this.f802 != null) {
                WindowDecorActionBar.this.f802.setTranslationY(0.0f);
                WindowDecorActionBar.this.f828.setTranslationY(0.0f);
            }
            WindowDecorActionBar.this.f828.setVisibility(8);
            WindowDecorActionBar.this.f828.setTransitioning(false);
            WindowDecorActionBar.this.f808 = null;
            WindowDecorActionBar.this.m887();
            if (WindowDecorActionBar.this.f826 != null) {
                ViewCompat.requestApplyInsets(WindowDecorActionBar.this.f826);
            }
        }
    };

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private boolean f833;

    @RestrictTo
    public class ActionModeImpl extends ActionMode implements MenuBuilder.Callback {

        /* renamed from: 连任  reason: contains not printable characters */
        private WeakReference<View> f837;

        /* renamed from: 靐  reason: contains not printable characters */
        private final Context f838;

        /* renamed from: 麤  reason: contains not printable characters */
        private ActionMode.Callback f839;

        /* renamed from: 齉  reason: contains not printable characters */
        private final MenuBuilder f840;

        public ActionModeImpl(Context context, ActionMode.Callback callback) {
            this.f838 = context;
            this.f839 = callback;
            this.f840 = new MenuBuilder(context).setDefaultShowAsAction(1);
            this.f840.setCallback(this);
        }

        public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
            if (this.f839 != null) {
                return this.f839.m1443((ActionMode) this, menuItem);
            }
            return false;
        }

        public void onMenuModeChange(MenuBuilder menuBuilder) {
            if (this.f839 != null) {
                m921();
                WindowDecorActionBar.this.f825.showOverflowMenu();
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public CharSequence m913() {
            return WindowDecorActionBar.this.f825.getTitle();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public CharSequence m914() {
            return WindowDecorActionBar.this.f825.getSubtitle();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m915() {
            return WindowDecorActionBar.this.f825.isTitleOptional();
        }

        /* renamed from: ˑ  reason: contains not printable characters */
        public View m916() {
            if (this.f837 != null) {
                return (View) this.f837.get();
            }
            return null;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public boolean m917() {
            this.f840.stopDispatchingItemsChanged();
            try {
                return this.f839.m1442((ActionMode) this, (Menu) this.f840);
            } finally {
                this.f840.startDispatchingItemsChanged();
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Menu m918() {
            return this.f840;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m919(int i) {
            m926((CharSequence) WindowDecorActionBar.this.f829.getResources().getString(i));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m920(CharSequence charSequence) {
            WindowDecorActionBar.this.f825.setTitle(charSequence);
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m921() {
            if (WindowDecorActionBar.this.f805 == this) {
                this.f840.stopDispatchingItemsChanged();
                try {
                    this.f839.m1440(this, this.f840);
                } finally {
                    this.f840.startDispatchingItemsChanged();
                }
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m922() {
            if (WindowDecorActionBar.this.f805 == this) {
                if (!WindowDecorActionBar.m881(WindowDecorActionBar.this.f809, WindowDecorActionBar.this.f807, false)) {
                    WindowDecorActionBar.this.f813 = this;
                    WindowDecorActionBar.this.f816 = this.f839;
                } else {
                    this.f839.m1441(this);
                }
                this.f839 = null;
                WindowDecorActionBar.this.m890(false);
                WindowDecorActionBar.this.f825.closeMode();
                WindowDecorActionBar.this.f827.getViewGroup().sendAccessibilityEvent(32);
                WindowDecorActionBar.this.f826.setHideOnContentScrollEnabled(WindowDecorActionBar.this.f831);
                WindowDecorActionBar.this.f805 = null;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MenuInflater m923() {
            return new SupportMenuInflater(this.f838);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m924(int i) {
            m920((CharSequence) WindowDecorActionBar.this.f829.getResources().getString(i));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m925(View view) {
            WindowDecorActionBar.this.f825.setCustomView(view);
            this.f837 = new WeakReference<>(view);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m926(CharSequence charSequence) {
            WindowDecorActionBar.this.f825.setSubtitle(charSequence);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m927(boolean z) {
            super.m1439(z);
            WindowDecorActionBar.this.f825.setTitleOptional(z);
        }
    }

    @RestrictTo
    public class TabImpl extends ActionBar.Tab {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f842;

        /* renamed from: ʼ  reason: contains not printable characters */
        private View f843;

        /* renamed from: 连任  reason: contains not printable characters */
        private CharSequence f844;

        /* renamed from: 靐  reason: contains not printable characters */
        private ActionBar.TabListener f845;

        /* renamed from: 麤  reason: contains not printable characters */
        private CharSequence f846;

        /* renamed from: 齉  reason: contains not printable characters */
        private Drawable f847;

        /* renamed from: 龘  reason: contains not printable characters */
        final /* synthetic */ WindowDecorActionBar f848;

        /* renamed from: ʻ  reason: contains not printable characters */
        public CharSequence m928() {
            return this.f844;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public ActionBar.TabListener m929() {
            return this.f845;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public void m930() {
            this.f848.m909((ActionBar.Tab) this);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Drawable m931() {
            return this.f847;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public View m932() {
            return this.f843;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public CharSequence m933() {
            return this.f846;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m934() {
            return this.f842;
        }
    }

    public WindowDecorActionBar(Activity activity, boolean z) {
        this.f814 = activity;
        View decorView = activity.getWindow().getDecorView();
        m880(decorView);
        if (!z) {
            this.f802 = decorView.findViewById(16908290);
        }
    }

    public WindowDecorActionBar(Dialog dialog) {
        this.f815 = dialog;
        m880(dialog.getWindow().getDecorView());
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m874() {
        if (this.f820) {
            this.f820 = false;
            if (this.f826 != null) {
                this.f826.setShowingForActionMode(false);
            }
            m875(false);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m875(boolean z) {
        if (m881(this.f809, this.f807, this.f820)) {
            if (!this.f803) {
                this.f803 = true;
                m886(z);
            }
        } else if (this.f803) {
            this.f803 = false;
            m888(z);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean m876() {
        return ViewCompat.isLaidOut(this.f828);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m877() {
        if (!this.f820) {
            this.f820 = true;
            if (this.f826 != null) {
                this.f826.setShowingForActionMode(true);
            }
            m875(false);
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m878(boolean z) {
        boolean z2 = true;
        this.f833 = z;
        if (!this.f833) {
            this.f827.setEmbeddedTabView((ScrollingTabContainerView) null);
            this.f828.setTabContainer(this.f804);
        } else {
            this.f828.setTabContainer((ScrollingTabContainerView) null);
            this.f827.setEmbeddedTabView(this.f804);
        }
        boolean z3 = m889() == 2;
        if (this.f804 != null) {
            if (z3) {
                this.f804.setVisibility(0);
                if (this.f826 != null) {
                    ViewCompat.requestApplyInsets(this.f826);
                }
            } else {
                this.f804.setVisibility(8);
            }
        }
        this.f827.setCollapsible(!this.f833 && z3);
        ActionBarOverlayLayout actionBarOverlayLayout = this.f826;
        if (this.f833 || !z3) {
            z2 = false;
        }
        actionBarOverlayLayout.setHasNonEmbeddedTabs(z2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private DecorToolbar m879(View view) {
        if (view instanceof DecorToolbar) {
            return (DecorToolbar) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException(new StringBuilder().append("Can't make a decor toolbar out of ").append(view).toString() != null ? view.getClass().getSimpleName() : "null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m880(View view) {
        this.f826 = (ActionBarOverlayLayout) view.findViewById(R.id.decor_content_parent);
        if (this.f826 != null) {
            this.f826.setActionBarVisibilityCallback(this);
        }
        this.f827 = m879(view.findViewById(R.id.action_bar));
        this.f825 = (ActionBarContextView) view.findViewById(R.id.action_context_bar);
        this.f828 = (ActionBarContainer) view.findViewById(R.id.action_bar_container);
        if (this.f827 == null || this.f825 == null || this.f828 == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.f829 = this.f827.getContext();
        boolean z = (this.f827.getDisplayOptions() & 4) != 0;
        if (z) {
            this.f823 = true;
        }
        ActionBarPolicy r1 = ActionBarPolicy.m1415(this.f829);
        m897(r1.m1416() || z);
        m878(r1.m1420());
        TypedArray obtainStyledAttributes = this.f829.obtainStyledAttributes((AttributeSet) null, R.styleable.ActionBar, R.attr.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(R.styleable.ActionBar_hideOnContentScroll, false)) {
            m891(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            m904((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m881(boolean z, boolean z2, boolean z3) {
        if (z3) {
            return true;
        }
        return !z && !z2;
    }

    public void enableContentAnimations(boolean z) {
        this.f817 = z;
    }

    public void hideForSystem() {
        if (!this.f807) {
            this.f807 = true;
            m875(true);
        }
    }

    public void onContentScrollStarted() {
        if (this.f808 != null) {
            this.f808.m1481();
            this.f808 = null;
        }
    }

    public void onContentScrollStopped() {
    }

    public void onWindowVisibilityChanged(int i) {
        this.f818 = i;
    }

    public void showForSystem() {
        if (this.f807) {
            this.f807 = false;
            m875(true);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m882(boolean z) {
        if (!this.f823) {
            m895(z);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m883(boolean z) {
        this.f806 = z;
        if (!z && this.f808 != null) {
            this.f808.m1481();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m884(boolean z) {
        if (z != this.f824) {
            this.f824 = z;
            int size = this.f830.size();
            for (int i = 0; i < size; i++) {
                this.f830.get(i).m444(z);
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m885() {
        if (this.f827 == null || !this.f827.hasExpandedActionView()) {
            return false;
        }
        this.f827.collapseActionView();
        return true;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m886(boolean z) {
        if (this.f808 != null) {
            this.f808.m1481();
        }
        this.f828.setVisibility(0);
        if (this.f818 != 0 || (!this.f806 && !z)) {
            this.f828.setAlpha(1.0f);
            this.f828.setTranslationY(0.0f);
            if (this.f817 && this.f802 != null) {
                this.f802.setTranslationY(0.0f);
            }
            this.f810.onAnimationEnd((View) null);
        } else {
            this.f828.setTranslationY(0.0f);
            float f = (float) (-this.f828.getHeight());
            if (z) {
                int[] iArr = {0, 0};
                this.f828.getLocationInWindow(iArr);
                f -= (float) iArr[1];
            }
            this.f828.setTranslationY(f);
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
            ViewPropertyAnimatorCompat translationY = ViewCompat.animate(this.f828).translationY(0.0f);
            translationY.setUpdateListener(this.f811);
            viewPropertyAnimatorCompatSet.m1483(translationY);
            if (this.f817 && this.f802 != null) {
                this.f802.setTranslationY(f);
                viewPropertyAnimatorCompatSet.m1483(ViewCompat.animate(this.f802).translationY(0.0f));
            }
            viewPropertyAnimatorCompatSet.m1486(f800);
            viewPropertyAnimatorCompatSet.m1482(250);
            viewPropertyAnimatorCompatSet.m1485(this.f810);
            this.f808 = viewPropertyAnimatorCompatSet;
            viewPropertyAnimatorCompatSet.m1487();
        }
        if (this.f826 != null) {
            ViewCompat.requestApplyInsets(this.f826);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m887() {
        if (this.f816 != null) {
            this.f816.m1441(this.f813);
            this.f813 = null;
            this.f816 = null;
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m888(boolean z) {
        if (this.f808 != null) {
            this.f808.m1481();
        }
        if (this.f818 != 0 || (!this.f806 && !z)) {
            this.f832.onAnimationEnd((View) null);
            return;
        }
        this.f828.setAlpha(1.0f);
        this.f828.setTransitioning(true);
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
        float f = (float) (-this.f828.getHeight());
        if (z) {
            int[] iArr = {0, 0};
            this.f828.getLocationInWindow(iArr);
            f -= (float) iArr[1];
        }
        ViewPropertyAnimatorCompat translationY = ViewCompat.animate(this.f828).translationY(f);
        translationY.setUpdateListener(this.f811);
        viewPropertyAnimatorCompatSet.m1483(translationY);
        if (this.f817 && this.f802 != null) {
            viewPropertyAnimatorCompatSet.m1483(ViewCompat.animate(this.f802).translationY(f));
        }
        viewPropertyAnimatorCompatSet.m1486(f799);
        viewPropertyAnimatorCompatSet.m1482(250);
        viewPropertyAnimatorCompatSet.m1485(this.f832);
        this.f808 = viewPropertyAnimatorCompatSet;
        viewPropertyAnimatorCompatSet.m1487();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int m889() {
        return this.f827.getNavigationMode();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m890(boolean z) {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat;
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2;
        if (z) {
            m877();
        } else {
            m874();
        }
        if (m876()) {
            if (z) {
                viewPropertyAnimatorCompat2 = this.f827.setupAnimatorToVisibility(4, 100);
                viewPropertyAnimatorCompat = this.f825.setupAnimatorToVisibility(0, 200);
            } else {
                viewPropertyAnimatorCompat = this.f827.setupAnimatorToVisibility(0, 200);
                viewPropertyAnimatorCompat2 = this.f825.setupAnimatorToVisibility(8, 100);
            }
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
            viewPropertyAnimatorCompatSet.m1484(viewPropertyAnimatorCompat2, viewPropertyAnimatorCompat);
            viewPropertyAnimatorCompatSet.m1487();
        } else if (z) {
            this.f827.setVisibility(4);
            this.f825.setVisibility(0);
        } else {
            this.f827.setVisibility(0);
            this.f825.setVisibility(8);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m891(boolean z) {
        if (!z || this.f826.isInOverlayMode()) {
            this.f831 = z;
            this.f826.setHideOnContentScrollEnabled(z);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m892() {
        return this.f827.getDisplayOptions();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m893(int i) {
        this.f827.setNavigationIcon(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m894(CharSequence charSequence) {
        this.f827.setSubtitle(charSequence);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m895(boolean z) {
        m906(z ? 4 : 0, 4);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Context m896() {
        if (this.f812 == null) {
            TypedValue typedValue = new TypedValue();
            this.f829.getTheme().resolveAttribute(R.attr.actionBarWidgetTheme, typedValue, true);
            int i = typedValue.resourceId;
            if (i != 0) {
                this.f812 = new ContextThemeWrapper(this.f829, i);
            } else {
                this.f812 = this.f829;
            }
        }
        return this.f812;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m897(boolean z) {
        this.f827.setHomeButtonEnabled(z);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m898() {
        if (!this.f809) {
            this.f809 = true;
            m875(false);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m899(int i) {
        this.f827.setNavigationContentDescription(i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m900(CharSequence charSequence) {
        this.f827.setWindowTitle(charSequence);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m901(boolean z) {
        m906(z ? 8 : 0, 8);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m902() {
        switch (this.f827.getNavigationMode()) {
            case 1:
                return this.f827.getDropdownSelectedPosition();
            case 2:
                if (this.f821 != null) {
                    return this.f821.m934();
                }
                return -1;
            default:
                return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ActionMode m903(ActionMode.Callback callback) {
        if (this.f805 != null) {
            this.f805.m922();
        }
        this.f826.setHideOnContentScrollEnabled(false);
        this.f825.killMode();
        ActionModeImpl actionModeImpl = new ActionModeImpl(this.f825.getContext(), callback);
        if (!actionModeImpl.m917()) {
            return null;
        }
        this.f805 = actionModeImpl;
        actionModeImpl.m921();
        this.f825.initForMode(actionModeImpl);
        m890(true);
        this.f825.sendAccessibilityEvent(32);
        return actionModeImpl;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m904(float f) {
        ViewCompat.setElevation(this.f828, f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m905(int i) {
        switch (this.f827.getNavigationMode()) {
            case 1:
                this.f827.setDropdownSelectedPosition(i);
                return;
            case 2:
                m909((ActionBar.Tab) this.f819.get(i));
                return;
            default:
                throw new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m906(int i, int i2) {
        int displayOptions = this.f827.getDisplayOptions();
        if ((i2 & 4) != 0) {
            this.f823 = true;
        }
        this.f827.setDisplayOptions((i & i2) | ((i2 ^ -1) & displayOptions));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m907(Configuration configuration) {
        m878(ActionBarPolicy.m1415(this.f829).m1420());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m908(Drawable drawable) {
        this.f827.setNavigationIcon(drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m909(ActionBar.Tab tab) {
        int i = -1;
        if (m889() != 2) {
            this.f822 = tab != null ? tab.m450() : -1;
            return;
        }
        FragmentTransaction disallowAddToBackStack = (!(this.f814 instanceof FragmentActivity) || this.f827.getViewGroup().isInEditMode()) ? null : ((FragmentActivity) this.f814).getSupportFragmentManager().beginTransaction().disallowAddToBackStack();
        if (this.f821 != tab) {
            ScrollingTabContainerView scrollingTabContainerView = this.f804;
            if (tab != null) {
                i = tab.m450();
            }
            scrollingTabContainerView.setTabSelected(i);
            if (this.f821 != null) {
                this.f821.m929().m451(this.f821, disallowAddToBackStack);
            }
            this.f821 = (TabImpl) tab;
            if (this.f821 != null) {
                this.f821.m929().m453(this.f821, disallowAddToBackStack);
            }
        } else if (this.f821 != null) {
            this.f821.m929().m452(this.f821, disallowAddToBackStack);
            this.f804.animateToTab(tab.m450());
        }
        if (disallowAddToBackStack != null && !disallowAddToBackStack.isEmpty()) {
            disallowAddToBackStack.commit();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m910(CharSequence charSequence) {
        this.f827.setTitle(charSequence);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m911(boolean z) {
        m906(z ? 2 : 0, 2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m912(int i, KeyEvent keyEvent) {
        Menu r1;
        if (this.f805 == null || (r1 = this.f805.m918()) == null) {
            return false;
        }
        r1.setQwertyMode(KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1);
        return r1.performShortcut(i, keyEvent, 0);
    }
}
