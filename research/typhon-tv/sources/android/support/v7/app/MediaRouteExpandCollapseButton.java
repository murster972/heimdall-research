package android.support.v7.app;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.AnimationDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.mediarouter.R;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

class MediaRouteExpandCollapseButton extends ImageButton {

    /* renamed from: ʻ  reason: contains not printable characters */
    View.OnClickListener f738;

    /* renamed from: 连任  reason: contains not printable characters */
    boolean f739;

    /* renamed from: 靐  reason: contains not printable characters */
    final AnimationDrawable f740;

    /* renamed from: 麤  reason: contains not printable characters */
    final String f741;

    /* renamed from: 齉  reason: contains not printable characters */
    final String f742;

    /* renamed from: 龘  reason: contains not printable characters */
    final AnimationDrawable f743;

    public MediaRouteExpandCollapseButton(Context context) {
        this(context, (AttributeSet) null);
    }

    public MediaRouteExpandCollapseButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MediaRouteExpandCollapseButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f743 = (AnimationDrawable) ContextCompat.getDrawable(context, R.drawable.mr_group_expand);
        this.f740 = (AnimationDrawable) ContextCompat.getDrawable(context, R.drawable.mr_group_collapse);
        PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(MediaRouterThemeHelper.m805(context, i), PorterDuff.Mode.SRC_IN);
        this.f743.setColorFilter(porterDuffColorFilter);
        this.f740.setColorFilter(porterDuffColorFilter);
        this.f742 = context.getString(R.string.mr_controller_expand_group);
        this.f741 = context.getString(R.string.mr_controller_collapse_group);
        setImageDrawable(this.f743.getFrame(0));
        setContentDescription(this.f742);
        super.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaRouteExpandCollapseButton.this.f739 = !MediaRouteExpandCollapseButton.this.f739;
                if (MediaRouteExpandCollapseButton.this.f739) {
                    MediaRouteExpandCollapseButton.this.setImageDrawable(MediaRouteExpandCollapseButton.this.f743);
                    MediaRouteExpandCollapseButton.this.f743.start();
                    MediaRouteExpandCollapseButton.this.setContentDescription(MediaRouteExpandCollapseButton.this.f741);
                } else {
                    MediaRouteExpandCollapseButton.this.setImageDrawable(MediaRouteExpandCollapseButton.this.f740);
                    MediaRouteExpandCollapseButton.this.f740.start();
                    MediaRouteExpandCollapseButton.this.setContentDescription(MediaRouteExpandCollapseButton.this.f742);
                }
                if (MediaRouteExpandCollapseButton.this.f738 != null) {
                    MediaRouteExpandCollapseButton.this.f738.onClick(view);
                }
            }
        });
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.f738 = onClickListener;
    }
}
