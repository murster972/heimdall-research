package android.support.v7.app;

import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.util.ObjectsCompat;
import android.support.v7.app.OverlayListView;
import android.support.v7.graphics.Palette;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.mediarouter.R;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class MediaRouteControllerDialog extends AlertDialog {

    /* renamed from: 靐  reason: contains not printable characters */
    static final boolean f638 = Log.isLoggable("MediaRouteCtrlDialog", 3);

    /* renamed from: 齉  reason: contains not printable characters */
    static final int f639 = ((int) TimeUnit.SECONDS.toMillis(30));

    /* renamed from: ʻ  reason: contains not printable characters */
    Context f640;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private final MediaRouterCallback f641;

    /* renamed from: ʻʼ  reason: contains not printable characters */
    private List<MediaRouter.RouteInfo> f642;

    /* renamed from: ʻʽ  reason: contains not printable characters */
    private Set<MediaRouter.RouteInfo> f643;

    /* renamed from: ʻʾ  reason: contains not printable characters */
    private int f644;

    /* renamed from: ʻʿ  reason: contains not printable characters */
    private int f645;

    /* renamed from: ʻˆ  reason: contains not printable characters */
    private int f646;

    /* renamed from: ʻˈ  reason: contains not printable characters */
    private final int f647;

    /* renamed from: ʻˉ  reason: contains not printable characters */
    private int f648;

    /* renamed from: ʻˊ  reason: contains not printable characters */
    private int f649;

    /* renamed from: ʻˋ  reason: contains not printable characters */
    private Interpolator f650;

    /* renamed from: ʻˎ  reason: contains not printable characters */
    private Interpolator f651;

    /* renamed from: ʻˏ  reason: contains not printable characters */
    private Interpolator f652;

    /* renamed from: ʻˑ  reason: contains not printable characters */
    private Interpolator f653;

    /* renamed from: ʼ  reason: contains not printable characters */
    FrameLayout f654;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private boolean f655;

    /* renamed from: ʽ  reason: contains not printable characters */
    OverlayListView f656;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f657;

    /* renamed from: ʾ  reason: contains not printable characters */
    VolumeChangeListener f658;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private View f659;

    /* renamed from: ʿ  reason: contains not printable characters */
    MediaRouter.RouteInfo f660;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private int f661;

    /* renamed from: ˆ  reason: contains not printable characters */
    FetchArtTask f662;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private Button f663;

    /* renamed from: ˈ  reason: contains not printable characters */
    SeekBar f664;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private ImageButton f665;

    /* renamed from: ˉ  reason: contains not printable characters */
    Bitmap f666;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private ImageButton f667;

    /* renamed from: ˊ  reason: contains not printable characters */
    MediaControllerCallback f668;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private FrameLayout f669;

    /* renamed from: ˋ  reason: contains not printable characters */
    PlaybackStateCompat f670;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private MediaRouteExpandCollapseButton f671;

    /* renamed from: ˎ  reason: contains not printable characters */
    MediaDescriptionCompat f672;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private FrameLayout f673;

    /* renamed from: ˏ  reason: contains not printable characters */
    Uri f674;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private LinearLayout f675;

    /* renamed from: ˑ  reason: contains not printable characters */
    VolumeGroupAdapter f676;

    /* renamed from: ˑˑ  reason: contains not printable characters */
    private ImageView f677;

    /* renamed from: י  reason: contains not printable characters */
    boolean f678;

    /* renamed from: יי  reason: contains not printable characters */
    private TextView f679;

    /* renamed from: ـ  reason: contains not printable characters */
    Bitmap f680;

    /* renamed from: ــ  reason: contains not printable characters */
    private Button f681;

    /* renamed from: ٴ  reason: contains not printable characters */
    Set<MediaRouter.RouteInfo> f682;

    /* renamed from: ٴٴ  reason: contains not printable characters */
    private LinearLayout f683;

    /* renamed from: ᐧ  reason: contains not printable characters */
    Set<MediaRouter.RouteInfo> f684;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    final AccessibilityManager f685;

    /* renamed from: ᴵ  reason: contains not printable characters */
    int f686;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    Runnable f687;

    /* renamed from: ᵎ  reason: contains not printable characters */
    boolean f688;

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    private TextView f689;

    /* renamed from: ᵔ  reason: contains not printable characters */
    boolean f690;

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    private TextView f691;

    /* renamed from: ᵢ  reason: contains not printable characters */
    boolean f692;

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    private boolean f693;

    /* renamed from: ⁱ  reason: contains not printable characters */
    boolean f694;

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    private LinearLayout f695;

    /* renamed from: 连任  reason: contains not printable characters */
    final MediaRouter.RouteInfo f696;

    /* renamed from: 麤  reason: contains not printable characters */
    final MediaRouter f697;

    /* renamed from: ﹳ  reason: contains not printable characters */
    boolean f698;

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    private RelativeLayout f699;

    /* renamed from: ﹶ  reason: contains not printable characters */
    Map<MediaRouter.RouteInfo, SeekBar> f700;

    /* renamed from: ﹶﹶ  reason: contains not printable characters */
    private View f701;

    /* renamed from: ﾞ  reason: contains not printable characters */
    MediaControllerCompat f702;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    int f703;

    private final class ClickListener implements View.OnClickListener {
        ClickListener() {
        }

        public void onClick(View view) {
            int i = 1;
            int id = view.getId();
            if (id == 16908313 || id == 16908314) {
                if (MediaRouteControllerDialog.this.f696.m1214()) {
                    MediaRouter mediaRouter = MediaRouteControllerDialog.this.f697;
                    if (id == 16908313) {
                        i = 2;
                    }
                    mediaRouter.m1115(i);
                }
                MediaRouteControllerDialog.this.dismiss();
            } else if (id == R.id.mr_control_playback_ctrl) {
                if (MediaRouteControllerDialog.this.f702 != null && MediaRouteControllerDialog.this.f670 != null) {
                    boolean z = MediaRouteControllerDialog.this.f670.getState() == 3;
                    int i2 = 0;
                    if (z && MediaRouteControllerDialog.this.m746()) {
                        MediaRouteControllerDialog.this.f702.getTransportControls().pause();
                        i2 = R.string.mr_controller_pause;
                    } else if (z && MediaRouteControllerDialog.this.m762()) {
                        MediaRouteControllerDialog.this.f702.getTransportControls().stop();
                        i2 = R.string.mr_controller_stop;
                    } else if (!z && MediaRouteControllerDialog.this.m745()) {
                        MediaRouteControllerDialog.this.f702.getTransportControls().play();
                        i2 = R.string.mr_controller_play;
                    }
                    if (MediaRouteControllerDialog.this.f685 != null && MediaRouteControllerDialog.this.f685.isEnabled() && i2 != 0) {
                        AccessibilityEvent obtain = AccessibilityEvent.obtain(16384);
                        obtain.setPackageName(MediaRouteControllerDialog.this.f640.getPackageName());
                        obtain.setClassName(getClass().getName());
                        obtain.getText().add(MediaRouteControllerDialog.this.f640.getString(i2));
                        MediaRouteControllerDialog.this.f685.sendAccessibilityEvent(obtain);
                    }
                }
            } else if (id == R.id.mr_close) {
                MediaRouteControllerDialog.this.dismiss();
            }
        }
    }

    private class FetchArtTask extends AsyncTask<Void, Void, Bitmap> {

        /* renamed from: 连任  reason: contains not printable characters */
        private long f724;

        /* renamed from: 靐  reason: contains not printable characters */
        private final Bitmap f725;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f726;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Uri f727;

        /* renamed from: 龘  reason: contains not printable characters */
        final /* synthetic */ MediaRouteControllerDialog f728;

        FetchArtTask(MediaRouteControllerDialog mediaRouteControllerDialog) {
            Uri uri = null;
            this.f728 = mediaRouteControllerDialog;
            Bitmap iconBitmap = mediaRouteControllerDialog.f672 == null ? null : mediaRouteControllerDialog.f672.getIconBitmap();
            if (mediaRouteControllerDialog.m758(iconBitmap)) {
                Log.w("MediaRouteCtrlDialog", "Can't fetch the given art bitmap because it's already recycled.");
                iconBitmap = null;
            }
            this.f725 = iconBitmap;
            this.f727 = mediaRouteControllerDialog.f672 != null ? mediaRouteControllerDialog.f672.getIconUri() : uri;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private InputStream m781(Uri uri) throws IOException {
            InputStream inputStream;
            String lowerCase = uri.getScheme().toLowerCase();
            if ("android.resource".equals(lowerCase) || "content".equals(lowerCase) || "file".equals(lowerCase)) {
                inputStream = this.f728.f640.getContentResolver().openInputStream(uri);
            } else {
                URLConnection openConnection = new URL(uri.toString()).openConnection();
                openConnection.setConnectTimeout(MediaRouteControllerDialog.f639);
                openConnection.setReadTimeout(MediaRouteControllerDialog.f639);
                inputStream = openConnection.getInputStream();
            }
            if (inputStream == null) {
                return null;
            }
            return new BufferedInputStream(inputStream);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.f724 = SystemClock.uptimeMillis();
            this.f728.m765();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Uri m782() {
            return this.f727;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Bitmap m783() {
            return this.f725;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Bitmap doInBackground(Void... voidArr) {
            int i = 0;
            Bitmap bitmap = null;
            if (this.f725 != null) {
                bitmap = this.f725;
            } else if (this.f727 != null) {
                InputStream inputStream = null;
                try {
                    inputStream = m781(this.f727);
                    if (inputStream == null) {
                        Log.w("MediaRouteCtrlDialog", "Unable to open: " + this.f727);
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e) {
                            }
                        }
                        return null;
                    }
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(inputStream, (Rect) null, options);
                    if (options.outWidth == 0 || options.outHeight == 0) {
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e2) {
                            }
                        }
                        return null;
                    }
                    try {
                        inputStream.reset();
                    } catch (IOException e3) {
                        inputStream.close();
                        inputStream = m781(this.f727);
                        if (inputStream == null) {
                            Log.w("MediaRouteCtrlDialog", "Unable to open: " + this.f727);
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e4) {
                                }
                            }
                            return null;
                        }
                    }
                    options.inJustDecodeBounds = false;
                    options.inSampleSize = Math.max(1, Integer.highestOneBit(options.outHeight / this.f728.m775(options.outWidth, options.outHeight)));
                    if (isCancelled()) {
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e5) {
                            }
                        }
                        return null;
                    }
                    bitmap = BitmapFactory.decodeStream(inputStream, (Rect) null, options);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e6) {
                        }
                    }
                } catch (IOException e7) {
                    Log.w("MediaRouteCtrlDialog", "Unable to open: " + this.f727, e7);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e8) {
                        }
                    }
                } catch (Throwable th) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e9) {
                        }
                    }
                    throw th;
                }
            }
            if (this.f728.m758(bitmap)) {
                Log.w("MediaRouteCtrlDialog", "Can't use recycled bitmap: " + bitmap);
                Bitmap bitmap2 = bitmap;
                return null;
            }
            if (bitmap != null && bitmap.getWidth() < bitmap.getHeight()) {
                Palette r4 = new Palette.Builder(bitmap).m987(1).m988();
                if (!r4.m980().isEmpty()) {
                    i = r4.m980().get(0).m995();
                }
                this.f726 = i;
            }
            Bitmap bitmap3 = bitmap;
            return bitmap;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void onPostExecute(Bitmap bitmap) {
            boolean z = true;
            this.f728.f662 = null;
            if (!ObjectsCompat.equals(this.f728.f666, this.f725) || !ObjectsCompat.equals(this.f728.f674, this.f727)) {
                this.f728.f666 = this.f725;
                this.f728.f680 = bitmap;
                this.f728.f674 = this.f727;
                this.f728.f686 = this.f726;
                this.f728.f678 = true;
                MediaRouteControllerDialog mediaRouteControllerDialog = this.f728;
                if (SystemClock.uptimeMillis() - this.f724 <= 120) {
                    z = false;
                }
                mediaRouteControllerDialog.m778(z);
            }
        }
    }

    private final class MediaControllerCallback extends MediaControllerCompat.Callback {
        MediaControllerCallback() {
        }

        public void onMetadataChanged(MediaMetadataCompat mediaMetadataCompat) {
            MediaRouteControllerDialog.this.f672 = mediaMetadataCompat == null ? null : mediaMetadataCompat.getDescription();
            MediaRouteControllerDialog.this.m764();
            MediaRouteControllerDialog.this.m778(false);
        }

        public void onPlaybackStateChanged(PlaybackStateCompat playbackStateCompat) {
            MediaRouteControllerDialog.this.f670 = playbackStateCompat;
            MediaRouteControllerDialog.this.m778(false);
        }

        public void onSessionDestroyed() {
            if (MediaRouteControllerDialog.this.f702 != null) {
                MediaRouteControllerDialog.this.f702.unregisterCallback(MediaRouteControllerDialog.this.f668);
                MediaRouteControllerDialog.this.f702 = null;
            }
        }
    }

    private final class MediaRouterCallback extends MediaRouter.Callback {
        MediaRouterCallback() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m786(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            SeekBar seekBar = MediaRouteControllerDialog.this.f700.get(routeInfo);
            int r0 = routeInfo.m1208();
            if (MediaRouteControllerDialog.f638) {
                Log.d("MediaRouteCtrlDialog", "onRouteVolumeChanged(), route.getVolume:" + r0);
            }
            if (seekBar != null && MediaRouteControllerDialog.this.f660 != routeInfo) {
                seekBar.setProgress(r0);
            }
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public void m787(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteControllerDialog.this.m778(false);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m788(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            MediaRouteControllerDialog.this.m778(true);
        }
    }

    private class VolumeChangeListener implements SeekBar.OnSeekBarChangeListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Runnable f731 = new Runnable() {
            public void run() {
                if (MediaRouteControllerDialog.this.f660 != null) {
                    MediaRouteControllerDialog.this.f660 = null;
                    if (MediaRouteControllerDialog.this.f688) {
                        MediaRouteControllerDialog.this.m778(MediaRouteControllerDialog.this.f690);
                    }
                }
            }
        };

        VolumeChangeListener() {
        }

        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            if (z) {
                MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) seekBar.getTag();
                if (MediaRouteControllerDialog.f638) {
                    Log.d("MediaRouteCtrlDialog", "onProgressChanged(): calling MediaRouter.RouteInfo.requestSetVolume(" + i + ")");
                }
                routeInfo.m1224(i);
            }
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            if (MediaRouteControllerDialog.this.f660 != null) {
                MediaRouteControllerDialog.this.f664.removeCallbacks(this.f731);
            }
            MediaRouteControllerDialog.this.f660 = (MediaRouter.RouteInfo) seekBar.getTag();
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            MediaRouteControllerDialog.this.f664.postDelayed(this.f731, 500);
        }
    }

    private class VolumeGroupAdapter extends ArrayAdapter<MediaRouter.RouteInfo> {

        /* renamed from: 龘  reason: contains not printable characters */
        final float f735;

        public VolumeGroupAdapter(Context context, List<MediaRouter.RouteInfo> list) {
            super(context, 0, list);
            this.f735 = MediaRouterThemeHelper.m807(context);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2 = view;
            if (view2 == null) {
                view2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mr_controller_volume_item, viewGroup, false);
            } else {
                MediaRouteControllerDialog.this.m769(view2);
            }
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) getItem(i);
            if (routeInfo != null) {
                boolean r2 = routeInfo.m1200();
                TextView textView = (TextView) view2.findViewById(R.id.mr_name);
                textView.setEnabled(r2);
                textView.setText(routeInfo.m1221());
                MediaRouteVolumeSlider mediaRouteVolumeSlider = (MediaRouteVolumeSlider) view2.findViewById(R.id.mr_volume_slider);
                MediaRouterThemeHelper.m812(viewGroup.getContext(), mediaRouteVolumeSlider, (View) MediaRouteControllerDialog.this.f656);
                mediaRouteVolumeSlider.setTag(routeInfo);
                MediaRouteControllerDialog.this.f700.put(routeInfo, mediaRouteVolumeSlider);
                mediaRouteVolumeSlider.m801(!r2);
                mediaRouteVolumeSlider.setEnabled(r2);
                if (r2) {
                    if (MediaRouteControllerDialog.this.m779(routeInfo)) {
                        mediaRouteVolumeSlider.setMax(routeInfo.m1209());
                        mediaRouteVolumeSlider.setProgress(routeInfo.m1208());
                        mediaRouteVolumeSlider.setOnSeekBarChangeListener(MediaRouteControllerDialog.this.f658);
                    } else {
                        mediaRouteVolumeSlider.setMax(100);
                        mediaRouteVolumeSlider.setProgress(100);
                        mediaRouteVolumeSlider.setEnabled(false);
                    }
                }
                ((ImageView) view2.findViewById(R.id.mr_volume_item_icon)).setAlpha(r2 ? 255 : (int) (255.0f * this.f735));
                ((LinearLayout) view2.findViewById(R.id.volume_item_container)).setVisibility(MediaRouteControllerDialog.this.f684.contains(routeInfo) ? 4 : 0);
                if (MediaRouteControllerDialog.this.f682 != null && MediaRouteControllerDialog.this.f682.contains(routeInfo)) {
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 0.0f);
                    alphaAnimation.setDuration(0);
                    alphaAnimation.setFillEnabled(true);
                    alphaAnimation.setFillAfter(true);
                    view2.clearAnimation();
                    view2.startAnimation(alphaAnimation);
                }
            }
            return view2;
        }

        public boolean isEnabled(int i) {
            return false;
        }
    }

    public MediaRouteControllerDialog(Context context) {
        this(context, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MediaRouteControllerDialog(android.content.Context r3, int r4) {
        /*
            r2 = this;
            r1 = 1
            android.content.Context r3 = android.support.v7.app.MediaRouterThemeHelper.m811((android.content.Context) r3, (int) r4, (boolean) r1)
            int r0 = android.support.v7.app.MediaRouterThemeHelper.m804(r3)
            r2.<init>(r3, r0)
            r2.f693 = r1
            android.support.v7.app.MediaRouteControllerDialog$1 r0 = new android.support.v7.app.MediaRouteControllerDialog$1
            r0.<init>()
            r2.f687 = r0
            android.content.Context r0 = r2.getContext()
            r2.f640 = r0
            android.support.v7.app.MediaRouteControllerDialog$MediaControllerCallback r0 = new android.support.v7.app.MediaRouteControllerDialog$MediaControllerCallback
            r0.<init>()
            r2.f668 = r0
            android.content.Context r0 = r2.f640
            android.support.v7.media.MediaRouter r0 = android.support.v7.media.MediaRouter.m1109((android.content.Context) r0)
            r2.f697 = r0
            android.support.v7.app.MediaRouteControllerDialog$MediaRouterCallback r0 = new android.support.v7.app.MediaRouteControllerDialog$MediaRouterCallback
            r0.<init>()
            r2.f641 = r0
            android.support.v7.media.MediaRouter r0 = r2.f697
            android.support.v7.media.MediaRouter$RouteInfo r0 = r0.m1113()
            r2.f696 = r0
            android.support.v7.media.MediaRouter r0 = r2.f697
            android.support.v4.media.session.MediaSessionCompat$Token r0 = r0.m1112()
            r2.m756((android.support.v4.media.session.MediaSessionCompat.Token) r0)
            android.content.Context r0 = r2.f640
            android.content.res.Resources r0 = r0.getResources()
            int r1 = android.support.v7.mediarouter.R.dimen.mr_controller_volume_group_list_padding_top
            int r0 = r0.getDimensionPixelSize(r1)
            r2.f647 = r0
            android.content.Context r0 = r2.f640
            java.lang.String r1 = "accessibility"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.view.accessibility.AccessibilityManager r0 = (android.view.accessibility.AccessibilityManager) r0
            r2.f685 = r0
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 21
            if (r0 < r1) goto L_0x0073
            int r0 = android.support.v7.mediarouter.R.interpolator.mr_linear_out_slow_in
            android.view.animation.Interpolator r0 = android.view.animation.AnimationUtils.loadInterpolator(r3, r0)
            r2.f651 = r0
            int r0 = android.support.v7.mediarouter.R.interpolator.mr_fast_out_slow_in
            android.view.animation.Interpolator r0 = android.view.animation.AnimationUtils.loadInterpolator(r3, r0)
            r2.f652 = r0
        L_0x0073:
            android.view.animation.AccelerateDecelerateInterpolator r0 = new android.view.animation.AccelerateDecelerateInterpolator
            r0.<init>()
            r2.f653 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.MediaRouteControllerDialog.<init>(android.content.Context, int):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m741(boolean z) {
        if (!z && this.f683.getVisibility() != 0) {
            return 0;
        }
        int paddingTop = 0 + this.f695.getPaddingTop() + this.f695.getPaddingBottom();
        if (z) {
            paddingTop += this.f699.getMeasuredHeight();
        }
        if (this.f683.getVisibility() == 0) {
            paddingTop += this.f683.getMeasuredHeight();
        }
        return (!z || this.f683.getVisibility() != 0) ? paddingTop : paddingTop + this.f701.getMeasuredHeight();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m742(boolean z) {
        int i = 8;
        this.f701.setVisibility((this.f683.getVisibility() != 0 || !z) ? 8 : 0);
        LinearLayout linearLayout = this.f695;
        if (this.f683.getVisibility() != 8 || z) {
            i = 0;
        }
        linearLayout.setVisibility(i);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private MediaRouter.RouteGroup m743() {
        if (this.f696 instanceof MediaRouter.RouteGroup) {
            return (MediaRouter.RouteGroup) this.f696;
        }
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m744(boolean z) {
        List<MediaRouter.RouteInfo> r2 = m743() == null ? null : m743().m1194();
        if (r2 == null) {
            this.f642.clear();
            this.f676.notifyDataSetChanged();
        } else if (MediaRouteDialogHelper.m799(this.f642, r2)) {
            this.f676.notifyDataSetChanged();
        } else {
            HashMap r1 = z ? MediaRouteDialogHelper.m798((ListView) this.f656, this.f676) : null;
            HashMap r0 = z ? MediaRouteDialogHelper.m797(this.f640, this.f656, this.f676) : null;
            this.f682 = MediaRouteDialogHelper.m793(this.f642, r2);
            this.f643 = MediaRouteDialogHelper.m794(this.f642, r2);
            this.f642.addAll(0, this.f682);
            this.f642.removeAll(this.f643);
            this.f676.notifyDataSetChanged();
            if (!z || !this.f692 || this.f682.size() + this.f643.size() <= 0) {
                this.f682 = null;
                this.f643 = null;
                return;
            }
            m752((Map<MediaRouter.RouteInfo, Rect>) r1, (Map<MediaRouter.RouteInfo, BitmapDrawable>) r0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m745() {
        return (this.f670.getActions() & 516) != 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m746() {
        return (this.f670.getActions() & 514) != 0;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m747() {
        if (m748()) {
            CharSequence title = this.f672 == null ? null : this.f672.getTitle();
            boolean z = !TextUtils.isEmpty(title);
            CharSequence subtitle = this.f672 == null ? null : this.f672.getSubtitle();
            boolean z2 = !TextUtils.isEmpty(subtitle);
            boolean z3 = false;
            boolean z4 = false;
            if (this.f696.m1206() != -1) {
                this.f691.setText(R.string.mr_controller_casting_screen);
                z3 = true;
            } else if (this.f670 == null || this.f670.getState() == 0) {
                this.f691.setText(R.string.mr_controller_no_media_selected);
                z3 = true;
            } else if (z || z2) {
                if (z) {
                    this.f691.setText(title);
                    z3 = true;
                }
                if (z2) {
                    this.f679.setText(subtitle);
                    z4 = true;
                }
            } else {
                this.f691.setText(R.string.mr_controller_no_info_available);
                z3 = true;
            }
            this.f691.setVisibility(z3 ? 0 : 8);
            this.f679.setVisibility(z4 ? 0 : 8);
            if (this.f670 != null) {
                boolean z5 = this.f670.getState() == 6 || this.f670.getState() == 3;
                Context context = this.f667.getContext();
                boolean z6 = true;
                int i = 0;
                int i2 = 0;
                if (z5 && m746()) {
                    i = R.attr.mediaRoutePauseDrawable;
                    i2 = R.string.mr_controller_pause;
                } else if (z5 && m762()) {
                    i = R.attr.mediaRouteStopDrawable;
                    i2 = R.string.mr_controller_stop;
                } else if (z5 || !m745()) {
                    z6 = false;
                } else {
                    i = R.attr.mediaRoutePlayDrawable;
                    i2 = R.string.mr_controller_play;
                }
                this.f667.setVisibility(z6 ? 0 : 8);
                if (z6) {
                    this.f667.setImageResource(MediaRouterThemeHelper.m808(context, i));
                    this.f667.setContentDescription(context.getResources().getText(i2));
                }
            }
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean m748() {
        return this.f659 == null && !(this.f672 == null && this.f670 == null);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m749() {
        int i = 8;
        if (!m779(this.f696)) {
            this.f683.setVisibility(8);
        } else if (this.f683.getVisibility() == 8) {
            this.f683.setVisibility(0);
            this.f664.setMax(this.f696.m1209());
            this.f664.setProgress(this.f696.m1208());
            MediaRouteExpandCollapseButton mediaRouteExpandCollapseButton = this.f671;
            if (m743() != null) {
                i = 0;
            }
            mediaRouteExpandCollapseButton.setVisibility(i);
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m750() {
        AnonymousClass12 r3 = new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                MediaRouteControllerDialog.this.m772(true);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        };
        boolean z = false;
        int firstVisiblePosition = this.f656.getFirstVisiblePosition();
        for (int i = 0; i < this.f656.getChildCount(); i++) {
            View childAt = this.f656.getChildAt(i);
            VolumeGroupAdapter volumeGroupAdapter = this.f676;
            if (this.f682.contains((MediaRouter.RouteInfo) volumeGroupAdapter.getItem(firstVisiblePosition + i))) {
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration((long) this.f648);
                alphaAnimation.setFillEnabled(true);
                alphaAnimation.setFillAfter(true);
                if (!z) {
                    z = true;
                    alphaAnimation.setAnimationListener(r3);
                }
                childAt.clearAnimation();
                childAt.startAnimation(alphaAnimation);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m751(final View view, int i) {
        final int r2 = m754(view);
        final int i2 = i;
        AnonymousClass7 r0 = new Animation() {
            /* access modifiers changed from: protected */
            public void applyTransformation(float f, Transformation transformation) {
                MediaRouteControllerDialog.m757(view, r2 - ((int) (((float) (r2 - i2)) * f)));
            }
        };
        r0.setDuration((long) this.f703);
        if (Build.VERSION.SDK_INT >= 21) {
            r0.setInterpolator(this.f650);
        }
        view.startAnimation(r0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m752(final Map<MediaRouter.RouteInfo, Rect> map, final Map<MediaRouter.RouteInfo, BitmapDrawable> map2) {
        this.f656.setEnabled(false);
        this.f656.requestLayout();
        this.f694 = true;
        this.f656.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                MediaRouteControllerDialog.this.f656.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                MediaRouteControllerDialog.this.m777((Map<MediaRouter.RouteInfo, Rect>) map, (Map<MediaRouter.RouteInfo, BitmapDrawable>) map2);
            }
        });
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m754(View view) {
        return view.getLayoutParams().height;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m756(MediaSessionCompat.Token token) {
        PlaybackStateCompat playbackStateCompat = null;
        if (this.f702 != null) {
            this.f702.unregisterCallback(this.f668);
            this.f702 = null;
        }
        if (token != null && this.f655) {
            try {
                this.f702 = new MediaControllerCompat(this.f640, token);
            } catch (RemoteException e) {
                Log.e("MediaRouteCtrlDialog", "Error creating media controller in setMediaSession.", e);
            }
            if (this.f702 != null) {
                this.f702.registerCallback(this.f668);
            }
            MediaMetadataCompat metadata = this.f702 == null ? null : this.f702.getMetadata();
            this.f672 = metadata == null ? null : metadata.getDescription();
            if (this.f702 != null) {
                playbackStateCompat = this.f702.getPlaybackState();
            }
            this.f670 = playbackStateCompat;
            m764();
            m778(false);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m757(View view, int i) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = i;
        view.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m758(Bitmap bitmap) {
        return bitmap != null && bitmap.isRecycled();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m759(Uri uri, Uri uri2) {
        if (uri == null || !uri.equals(uri2)) {
            return uri == null && uri2 == null;
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public boolean m762() {
        return (this.f670.getActions() & 1) != 0;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean m763() {
        Bitmap iconBitmap = this.f672 == null ? null : this.f672.getIconBitmap();
        Uri iconUri = this.f672 == null ? null : this.f672.getIconUri();
        Bitmap r2 = this.f662 == null ? this.f666 : this.f662.m783();
        Uri r3 = this.f662 == null ? this.f674 : this.f662.m782();
        if (r2 != iconBitmap) {
            return true;
        }
        return r2 == null && !m759(r3, iconUri);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f655 = true;
        this.f697.m1118(MediaRouteSelector.f939, this.f641, 2);
        m756(this.f697.m1112());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setBackgroundDrawableResource(17170445);
        setContentView(R.layout.mr_controller_material_dialog_b);
        findViewById(16908315).setVisibility(8);
        ClickListener clickListener = new ClickListener();
        this.f669 = (FrameLayout) findViewById(R.id.mr_expandable_area);
        this.f669.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaRouteControllerDialog.this.dismiss();
            }
        });
        this.f675 = (LinearLayout) findViewById(R.id.mr_dialog_area);
        this.f675.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
        int r0 = MediaRouterThemeHelper.m806(this.f640);
        this.f681 = (Button) findViewById(16908314);
        this.f681.setText(R.string.mr_controller_disconnect);
        this.f681.setTextColor(r0);
        this.f681.setOnClickListener(clickListener);
        this.f663 = (Button) findViewById(16908313);
        this.f663.setText(R.string.mr_controller_stop_casting);
        this.f663.setTextColor(r0);
        this.f663.setOnClickListener(clickListener);
        this.f689 = (TextView) findViewById(R.id.mr_name);
        this.f665 = (ImageButton) findViewById(R.id.mr_close);
        this.f665.setOnClickListener(clickListener);
        this.f673 = (FrameLayout) findViewById(R.id.mr_custom_control);
        this.f654 = (FrameLayout) findViewById(R.id.mr_default_control);
        AnonymousClass4 r2 = new View.OnClickListener() {
            public void onClick(View view) {
                PendingIntent sessionActivity;
                if (MediaRouteControllerDialog.this.f702 != null && (sessionActivity = MediaRouteControllerDialog.this.f702.getSessionActivity()) != null) {
                    try {
                        sessionActivity.send();
                        MediaRouteControllerDialog.this.dismiss();
                    } catch (PendingIntent.CanceledException e) {
                        Log.e("MediaRouteCtrlDialog", sessionActivity + " was not sent, it had been canceled.");
                    }
                }
            }
        };
        this.f677 = (ImageView) findViewById(R.id.mr_art);
        this.f677.setOnClickListener(r2);
        findViewById(R.id.mr_control_title_container).setOnClickListener(r2);
        this.f695 = (LinearLayout) findViewById(R.id.mr_media_main_control);
        this.f701 = findViewById(R.id.mr_control_divider);
        this.f699 = (RelativeLayout) findViewById(R.id.mr_playback_control);
        this.f691 = (TextView) findViewById(R.id.mr_control_title);
        this.f679 = (TextView) findViewById(R.id.mr_control_subtitle);
        this.f667 = (ImageButton) findViewById(R.id.mr_control_playback_ctrl);
        this.f667.setOnClickListener(clickListener);
        this.f683 = (LinearLayout) findViewById(R.id.mr_volume_control);
        this.f683.setVisibility(8);
        this.f664 = (SeekBar) findViewById(R.id.mr_volume_slider);
        this.f664.setTag(this.f696);
        this.f658 = new VolumeChangeListener();
        this.f664.setOnSeekBarChangeListener(this.f658);
        this.f656 = (OverlayListView) findViewById(R.id.mr_volume_group_list);
        this.f642 = new ArrayList();
        this.f676 = new VolumeGroupAdapter(this.f656.getContext(), this.f642);
        this.f656.setAdapter(this.f676);
        this.f684 = new HashSet();
        MediaRouterThemeHelper.m813(this.f640, this.f695, this.f656, m743() != null);
        MediaRouterThemeHelper.m812(this.f640, (MediaRouteVolumeSlider) this.f664, (View) this.f695);
        this.f700 = new HashMap();
        this.f700.put(this.f696, this.f664);
        this.f671 = (MediaRouteExpandCollapseButton) findViewById(R.id.mr_group_expand_collapse);
        this.f671.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaRouteControllerDialog.this.f692 = !MediaRouteControllerDialog.this.f692;
                if (MediaRouteControllerDialog.this.f692) {
                    MediaRouteControllerDialog.this.f656.setVisibility(0);
                }
                MediaRouteControllerDialog.this.m773();
                MediaRouteControllerDialog.this.m770(true);
            }
        });
        m773();
        this.f703 = this.f640.getResources().getInteger(R.integer.mr_controller_volume_group_list_animation_duration_ms);
        this.f648 = this.f640.getResources().getInteger(R.integer.mr_controller_volume_group_list_fade_in_duration_ms);
        this.f649 = this.f640.getResources().getInteger(R.integer.mr_controller_volume_group_list_fade_out_duration_ms);
        this.f659 = m776(bundle);
        if (this.f659 != null) {
            this.f673.addView(this.f659);
            this.f673.setVisibility(0);
        }
        this.f657 = true;
        m768();
    }

    public void onDetachedFromWindow() {
        this.f697.m1119((MediaRouter.Callback) this.f641);
        m756((MediaSessionCompat.Token) null);
        this.f655 = false;
        super.onDetachedFromWindow();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 25 && i != 24) {
            return super.onKeyDown(i, keyEvent);
        }
        this.f696.m1220(i == 25 ? -1 : 1);
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 25 || i == 24) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m764() {
        if (this.f659 == null && m763()) {
            if (this.f662 != null) {
                this.f662.cancel(true);
            }
            this.f662 = new FetchArtTask(this);
            this.f662.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m765() {
        this.f678 = false;
        this.f680 = null;
        this.f686 = 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m766() {
        if (this.f682 == null || this.f682.size() == 0) {
            m772(true);
        } else {
            m750();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m767(boolean z) {
        int firstVisiblePosition = this.f656.getFirstVisiblePosition();
        for (int i = 0; i < this.f656.getChildCount(); i++) {
            View childAt = this.f656.getChildAt(i);
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) this.f676.getItem(firstVisiblePosition + i);
            if (!z || this.f682 == null || !this.f682.contains(routeInfo)) {
                ((LinearLayout) childAt.findViewById(R.id.volume_item_container)).setVisibility(0);
                AnimationSet animationSet = new AnimationSet(true);
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 1.0f);
                alphaAnimation.setDuration(0);
                animationSet.addAnimation(alphaAnimation);
                new TranslateAnimation(0.0f, 0.0f, 0.0f, 0.0f).setDuration(0);
                animationSet.setFillAfter(true);
                animationSet.setFillEnabled(true);
                childAt.clearAnimation();
                childAt.startAnimation(animationSet);
            }
        }
        this.f656.m814();
        if (!z) {
            m772(false);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m768() {
        int r2 = MediaRouteDialogHelper.m795(this.f640);
        getWindow().setLayout(r2, -2);
        View decorView = getWindow().getDecorView();
        this.f661 = (r2 - decorView.getPaddingLeft()) - decorView.getPaddingRight();
        Resources resources = this.f640.getResources();
        this.f644 = resources.getDimensionPixelSize(R.dimen.mr_controller_volume_group_list_item_icon_size);
        this.f645 = resources.getDimensionPixelSize(R.dimen.mr_controller_volume_group_list_item_height);
        this.f646 = resources.getDimensionPixelSize(R.dimen.mr_controller_volume_group_list_max_height);
        this.f666 = null;
        this.f674 = null;
        m764();
        m778(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m769(View view) {
        m757((View) (LinearLayout) view.findViewById(R.id.volume_item_container), this.f645);
        View findViewById = view.findViewById(R.id.mr_volume_item_icon);
        ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
        layoutParams.width = this.f644;
        layoutParams.height = this.f644;
        findViewById.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m770(final boolean z) {
        this.f654.requestLayout();
        this.f654.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                MediaRouteControllerDialog.this.f654.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                if (MediaRouteControllerDialog.this.f694) {
                    MediaRouteControllerDialog.this.f698 = true;
                } else {
                    MediaRouteControllerDialog.this.m774(z);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m771() {
        m767(true);
        this.f656.requestLayout();
        this.f656.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                MediaRouteControllerDialog.this.f656.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                MediaRouteControllerDialog.this.m766();
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m772(boolean z) {
        this.f682 = null;
        this.f643 = null;
        this.f694 = false;
        if (this.f698) {
            this.f698 = false;
            m770(z);
        }
        this.f656.setEnabled(true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m773() {
        if (Build.VERSION.SDK_INT >= 21) {
            this.f650 = this.f692 ? this.f651 : this.f652;
        } else {
            this.f650 = this.f653;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m774(boolean z) {
        Bitmap bitmap;
        int r8 = m754((View) this.f695);
        m757((View) this.f695, -1);
        m742(m748());
        View decorView = getWindow().getDecorView();
        decorView.measure(View.MeasureSpec.makeMeasureSpec(getWindow().getAttributes().width, 1073741824), 0);
        m757((View) this.f695, r8);
        int i = 0;
        if (this.f659 == null && (this.f677.getDrawable() instanceof BitmapDrawable) && (bitmap = ((BitmapDrawable) this.f677.getDrawable()).getBitmap()) != null) {
            i = m775(bitmap.getWidth(), bitmap.getHeight());
            this.f677.setScaleType(bitmap.getWidth() >= bitmap.getHeight() ? ImageView.ScaleType.FIT_XY : ImageView.ScaleType.FIT_CENTER);
        }
        int r5 = m741(m748());
        int size = this.f642.size();
        int size2 = m743() == null ? 0 : this.f645 * m743().m1194().size();
        if (size > 0) {
            size2 += this.f647;
        }
        int min = this.f692 ? Math.min(size2, this.f646) : 0;
        int max = Math.max(i, min) + r5;
        Rect rect = new Rect();
        decorView.getWindowVisibleDisplayFrame(rect);
        int height = rect.height() - (this.f675.getMeasuredHeight() - this.f654.getMeasuredHeight());
        if (this.f659 != null || i <= 0 || max > height) {
            if (m754((View) this.f656) + this.f695.getMeasuredHeight() >= this.f654.getMeasuredHeight()) {
                this.f677.setVisibility(8);
            }
            i = 0;
            max = min + r5;
        } else {
            this.f677.setVisibility(0);
            m757((View) this.f677, i);
        }
        if (!m748() || max > height) {
            this.f699.setVisibility(8);
        } else {
            this.f699.setVisibility(0);
        }
        m742(this.f699.getVisibility() == 0);
        int r52 = m741(this.f699.getVisibility() == 0);
        int max2 = Math.max(i, min) + r52;
        if (max2 > height) {
            min -= max2 - height;
            max2 = height;
        }
        this.f695.clearAnimation();
        this.f656.clearAnimation();
        this.f654.clearAnimation();
        if (z) {
            m751((View) this.f695, r52);
            m751((View) this.f656, min);
            m751((View) this.f654, max2);
        } else {
            m757((View) this.f695, r52);
            m757((View) this.f656, min);
            m757((View) this.f654, max2);
        }
        m757((View) this.f669, rect.height());
        m744(z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m775(int i, int i2) {
        return i >= i2 ? (int) (((((float) this.f661) * ((float) i2)) / ((float) i)) + 0.5f) : (int) (((((float) this.f661) * 9.0f) / 16.0f) + 0.5f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public View m776(Bundle bundle) {
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m777(Map<MediaRouter.RouteInfo, Rect> map, Map<MediaRouter.RouteInfo, BitmapDrawable> map2) {
        OverlayListView.OverlayObject r18;
        if (this.f682 != null && this.f643 != null) {
            int size = this.f682.size() - this.f643.size();
            boolean z = false;
            AnonymousClass9 r0 = new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                    MediaRouteControllerDialog.this.f656.m815();
                    MediaRouteControllerDialog.this.f656.postDelayed(MediaRouteControllerDialog.this.f687, (long) MediaRouteControllerDialog.this.f703);
                }
            };
            int firstVisiblePosition = this.f656.getFirstVisiblePosition();
            for (int i = 0; i < this.f656.getChildCount(); i++) {
                View childAt = this.f656.getChildAt(i);
                MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) this.f676.getItem(firstVisiblePosition + i);
                Rect rect = map.get(routeInfo);
                int top = childAt.getTop();
                int i2 = rect != null ? rect.top : top + (this.f645 * size);
                AnimationSet animationSet = new AnimationSet(true);
                if (this.f682 != null && this.f682.contains(routeInfo)) {
                    i2 = top;
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 0.0f);
                    alphaAnimation.setDuration((long) this.f648);
                    animationSet.addAnimation(alphaAnimation);
                }
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (i2 - top), 0.0f);
                translateAnimation.setDuration((long) this.f703);
                animationSet.addAnimation(translateAnimation);
                animationSet.setFillAfter(true);
                animationSet.setFillEnabled(true);
                animationSet.setInterpolator(this.f650);
                if (!z) {
                    z = true;
                    animationSet.setAnimationListener(r0);
                }
                childAt.clearAnimation();
                childAt.startAnimation(animationSet);
                map.remove(routeInfo);
                map2.remove(routeInfo);
            }
            for (Map.Entry next : map2.entrySet()) {
                MediaRouter.RouteInfo routeInfo2 = (MediaRouter.RouteInfo) next.getKey();
                BitmapDrawable bitmapDrawable = (BitmapDrawable) next.getValue();
                Rect rect2 = map.get(routeInfo2);
                if (this.f643.contains(routeInfo2)) {
                    r18 = new OverlayListView.OverlayObject(bitmapDrawable, rect2).m822(1.0f, 0.0f).m824((long) this.f649).m826(this.f650);
                } else {
                    final MediaRouter.RouteInfo routeInfo3 = routeInfo2;
                    r18 = new OverlayListView.OverlayObject(bitmapDrawable, rect2).m823(size * this.f645).m824((long) this.f703).m826(this.f650).m825((OverlayListView.OverlayObject.OnAnimationEndListener) new OverlayListView.OverlayObject.OnAnimationEndListener() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m780() {
                            MediaRouteControllerDialog.this.f684.remove(routeInfo3);
                            MediaRouteControllerDialog.this.f676.notifyDataSetChanged();
                        }
                    });
                    this.f684.add(routeInfo2);
                }
                this.f656.m816(r18);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m778(boolean z) {
        int i = 0;
        if (this.f660 != null) {
            this.f688 = true;
            this.f690 |= z;
            return;
        }
        this.f688 = false;
        this.f690 = false;
        if (!this.f696.m1214() || this.f696.m1227()) {
            dismiss();
        } else if (this.f657) {
            this.f689.setText(this.f696.m1221());
            Button button = this.f681;
            if (!this.f696.m1204()) {
                i = 8;
            }
            button.setVisibility(i);
            if (this.f659 == null && this.f678) {
                if (m758(this.f680)) {
                    Log.w("MediaRouteCtrlDialog", "Can't set artwork image with recycled bitmap: " + this.f680);
                } else {
                    this.f677.setImageBitmap(this.f680);
                    this.f677.setBackgroundColor(this.f686);
                }
                m765();
            }
            m749();
            m747();
            m770(z);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m779(MediaRouter.RouteInfo routeInfo) {
        return this.f693 && routeInfo.m1207() == 1;
    }
}
