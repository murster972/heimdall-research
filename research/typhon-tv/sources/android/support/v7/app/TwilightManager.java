package android.support.v7.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import java.util.Calendar;

class TwilightManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private static TwilightManager f789;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f790;

    /* renamed from: 麤  reason: contains not printable characters */
    private final TwilightState f791 = new TwilightState();

    /* renamed from: 齉  reason: contains not printable characters */
    private final LocationManager f792;

    private static class TwilightState {

        /* renamed from: ʻ  reason: contains not printable characters */
        long f793;

        /* renamed from: 连任  reason: contains not printable characters */
        long f794;

        /* renamed from: 靐  reason: contains not printable characters */
        long f795;

        /* renamed from: 麤  reason: contains not printable characters */
        long f796;

        /* renamed from: 齉  reason: contains not printable characters */
        long f797;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f798;

        TwilightState() {
        }
    }

    TwilightManager(Context context, LocationManager locationManager) {
        this.f790 = context;
        this.f792 = locationManager;
    }

    @SuppressLint({"MissingPermission"})
    /* renamed from: 靐  reason: contains not printable characters */
    private Location m868() {
        Location location = null;
        Location location2 = null;
        if (PermissionChecker.checkSelfPermission(this.f790, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            location = m870("network");
        }
        if (PermissionChecker.checkSelfPermission(this.f790, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location2 = m870("gps");
        }
        return (location2 == null || location == null) ? location2 == null ? location : location2 : location2.getTime() > location.getTime() ? location2 : location;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m869() {
        return this.f791.f793 > System.currentTimeMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Location m870(String str) {
        try {
            if (this.f792.isProviderEnabled(str)) {
                return this.f792.getLastKnownLocation(str);
            }
        } catch (Exception e) {
            Log.d("TwilightManager", "Failed to get last known location", e);
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static TwilightManager m871(Context context) {
        if (f789 == null) {
            Context applicationContext = context.getApplicationContext();
            f789 = new TwilightManager(applicationContext, (LocationManager) applicationContext.getSystemService("location"));
        }
        return f789;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m872(Location location) {
        long j;
        TwilightState twilightState = this.f791;
        long currentTimeMillis = System.currentTimeMillis();
        TwilightCalculator r3 = TwilightCalculator.m866();
        r3.m867(currentTimeMillis - 86400000, location.getLatitude(), location.getLongitude());
        long j2 = r3.f788;
        r3.m867(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = r3.f787 == 1;
        long j3 = r3.f786;
        long j4 = r3.f788;
        r3.m867(86400000 + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j5 = r3.f786;
        if (j3 == -1 || j4 == -1) {
            j = currentTimeMillis + 43200000;
        } else {
            j = (currentTimeMillis > j4 ? 0 + j5 : currentTimeMillis > j3 ? 0 + j4 : 0 + j3) + 60000;
        }
        twilightState.f798 = z;
        twilightState.f795 = j2;
        twilightState.f797 = j3;
        twilightState.f796 = j4;
        twilightState.f794 = j5;
        twilightState.f793 = j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m873() {
        TwilightState twilightState = this.f791;
        if (m869()) {
            return twilightState.f798;
        }
        Location r2 = m868();
        if (r2 != null) {
            m872(r2);
            return twilightState.f798;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }
}
