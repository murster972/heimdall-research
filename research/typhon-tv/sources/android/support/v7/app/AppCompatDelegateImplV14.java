package android.support.v7.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegateImplBase;
import android.support.v7.view.ActionMode;
import android.support.v7.view.SupportActionModeWrapper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.Window;

class AppCompatDelegateImplV14 extends AppCompatDelegateImplV11 {

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f512 = -100;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f513;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f514 = true;

    /* renamed from: י  reason: contains not printable characters */
    private AutoNightModeManager f515;

    class AppCompatWindowCallbackV14 extends AppCompatDelegateImplBase.AppCompatWindowCallbackBase {
        AppCompatWindowCallbackV14(Window.Callback callback) {
            super(callback);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            return AppCompatDelegateImplV14.this.m613() ? m614(callback) : super.onWindowStartingActionMode(callback);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final ActionMode m614(ActionMode.Callback callback) {
            SupportActionModeWrapper.CallbackWrapper callbackWrapper = new SupportActionModeWrapper.CallbackWrapper(AppCompatDelegateImplV14.this.f506, callback);
            android.support.v7.view.ActionMode r1 = AppCompatDelegateImplV14.this.m662((ActionMode.Callback) callbackWrapper);
            if (r1 != null) {
                return callbackWrapper.m1463(r1);
            }
            return null;
        }
    }

    final class AutoNightModeManager {

        /* renamed from: 连任  reason: contains not printable characters */
        private IntentFilter f517;

        /* renamed from: 靐  reason: contains not printable characters */
        private TwilightManager f518;

        /* renamed from: 麤  reason: contains not printable characters */
        private BroadcastReceiver f519;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f520;

        AutoNightModeManager(TwilightManager twilightManager) {
            this.f518 = twilightManager;
            this.f520 = twilightManager.m873();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m615() {
            boolean r0 = this.f518.m873();
            if (r0 != this.f520) {
                this.f520 = r0;
                AppCompatDelegateImplV14.this.m606();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public final void m616() {
            if (this.f519 != null) {
                AppCompatDelegateImplV14.this.f506.unregisterReceiver(this.f519);
                this.f519 = null;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public final void m617() {
            m616();
            if (this.f519 == null) {
                this.f519 = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        AutoNightModeManager.this.m615();
                    }
                };
            }
            if (this.f517 == null) {
                this.f517 = new IntentFilter();
                this.f517.addAction("android.intent.action.TIME_SET");
                this.f517.addAction("android.intent.action.TIMEZONE_CHANGED");
                this.f517.addAction("android.intent.action.TIME_TICK");
            }
            AppCompatDelegateImplV14.this.f506.registerReceiver(this.f519, this.f517);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final int m618() {
            this.f520 = this.f518.m873();
            return this.f520 ? 2 : 1;
        }
    }

    AppCompatDelegateImplV14(Context context, Window window, AppCompatCallback appCompatCallback) {
        super(context, window, appCompatCallback);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m601(int i) {
        Resources resources = this.f506.getResources();
        Configuration configuration = resources.getConfiguration();
        int i2 = configuration.uiMode & 48;
        int i3 = i == 2 ? 32 : 16;
        if (i2 == i3) {
            return false;
        }
        if (m604()) {
            ((Activity) this.f506).recreate();
        } else {
            Configuration configuration2 = new Configuration(configuration);
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            configuration2.uiMode = (configuration2.uiMode & -49) | i3;
            resources.updateConfiguration(configuration2, displayMetrics);
            if (Build.VERSION.SDK_INT < 26) {
                ResourcesFlusher.m831(resources);
            }
        }
        return true;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    private int m602() {
        return this.f512 != -100 ? this.f512 : m543();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private void m603() {
        if (this.f515 == null) {
            this.f515 = new AutoNightModeManager(TwilightManager.m871(this.f506));
        }
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean m604() {
        if (!this.f513 || !(this.f506 instanceof Activity)) {
            return false;
        }
        try {
            return (this.f506.getPackageManager().getActivityInfo(new ComponentName(this.f506, this.f506.getClass()), 0).configChanges & 512) == 0;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e);
            return true;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m605() {
        super.m640();
        if (this.f515 != null) {
            this.f515.m616();
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m606() {
        boolean z = false;
        int r2 = m602();
        int r1 = m607(r2);
        if (r1 != -1) {
            z = m601(r1);
        }
        if (r2 == 0) {
            m603();
            this.f515.m617();
        }
        this.f513 = true;
        return z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m607(int i) {
        switch (i) {
            case -100:
                return -1;
            case 0:
                m603();
                return this.f515.m618();
            default:
                return i;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m608() {
        super.m657();
        if (this.f515 != null) {
            this.f515.m616();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m609() {
        super.m583();
        m606();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m610(Bundle bundle) {
        super.m584(bundle);
        if (this.f512 != -100) {
            bundle.putInt("appcompat:local_night_mode", this.f512);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Window.Callback m611(Window.Callback callback) {
        return new AppCompatWindowCallbackV14(callback);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m612(Bundle bundle) {
        super.m668(bundle);
        if (bundle != null && this.f512 == -100) {
            this.f512 = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m613() {
        return this.f514;
    }
}
