package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.appcompat.R;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.lang.ref.WeakReference;

class AlertController {

    /* renamed from: ʻ  reason: contains not printable characters */
    Message f377;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private TextView f378;

    /* renamed from: ʼ  reason: contains not printable characters */
    Button f379;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private View f380;

    /* renamed from: ʽ  reason: contains not printable characters */
    Message f381;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private TextView f382;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f383;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private int f384;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f385;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private int f386;

    /* renamed from: ˆ  reason: contains not printable characters */
    private CharSequence f387;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private int f388 = 0;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f389;

    /* renamed from: ˉ  reason: contains not printable characters */
    private View f390;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private final View.OnClickListener f391 = new View.OnClickListener() {
        public void onClick(View view) {
            Message obtain = (view != AlertController.this.f412 || AlertController.this.f411 == null) ? (view != AlertController.this.f409 || AlertController.this.f377 == null) ? (view != AlertController.this.f379 || AlertController.this.f381 == null) ? null : Message.obtain(AlertController.this.f381) : Message.obtain(AlertController.this.f377) : Message.obtain(AlertController.this.f411);
            if (obtain != null) {
                obtain.sendToTarget();
            }
            AlertController.this.f416.obtainMessage(1, AlertController.this.f413).sendToTarget();
        }
    };

    /* renamed from: ˊ  reason: contains not printable characters */
    private final Context f392;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final Window f393;

    /* renamed from: ˎ  reason: contains not printable characters */
    private CharSequence f394;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f395;

    /* renamed from: ˑ  reason: contains not printable characters */
    NestedScrollView f396;

    /* renamed from: י  reason: contains not printable characters */
    private int f397;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f398;

    /* renamed from: ــ  reason: contains not printable characters */
    private boolean f399;

    /* renamed from: ٴ  reason: contains not printable characters */
    ListAdapter f400;

    /* renamed from: ᐧ  reason: contains not printable characters */
    int f401 = -1;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private Drawable f402;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f403;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private ImageView f404;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f405;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f406 = false;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private CharSequence f407;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private CharSequence f408;

    /* renamed from: 连任  reason: contains not printable characters */
    Button f409;

    /* renamed from: 靐  reason: contains not printable characters */
    ListView f410;

    /* renamed from: 麤  reason: contains not printable characters */
    Message f411;

    /* renamed from: 齉  reason: contains not printable characters */
    Button f412;

    /* renamed from: 龘  reason: contains not printable characters */
    final AppCompatDialog f413;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private CharSequence f414;

    /* renamed from: ﹶ  reason: contains not printable characters */
    int f415;

    /* renamed from: ﾞ  reason: contains not printable characters */
    Handler f416;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private int f417 = 0;

    public static class AlertParams {

        /* renamed from: ʻ  reason: contains not printable characters */
        public CharSequence f431;

        /* renamed from: ʻʻ  reason: contains not printable characters */
        public Cursor f432;

        /* renamed from: ʼ  reason: contains not printable characters */
        public View f433;

        /* renamed from: ʼʼ  reason: contains not printable characters */
        public String f434;

        /* renamed from: ʽ  reason: contains not printable characters */
        public CharSequence f435;

        /* renamed from: ʽʽ  reason: contains not printable characters */
        public String f436;

        /* renamed from: ʾ  reason: contains not printable characters */
        public CharSequence f437;

        /* renamed from: ʾʾ  reason: contains not printable characters */
        public OnPrepareListViewListener f438;

        /* renamed from: ʿ  reason: contains not printable characters */
        public DialogInterface.OnClickListener f439;

        /* renamed from: ʿʿ  reason: contains not printable characters */
        public AdapterView.OnItemSelectedListener f440;

        /* renamed from: ˆ  reason: contains not printable characters */
        public ListAdapter f441;

        /* renamed from: ˈ  reason: contains not printable characters */
        public DialogInterface.OnClickListener f442;

        /* renamed from: ˉ  reason: contains not printable characters */
        public DialogInterface.OnClickListener f443;

        /* renamed from: ˊ  reason: contains not printable characters */
        public DialogInterface.OnDismissListener f444;

        /* renamed from: ˋ  reason: contains not printable characters */
        public DialogInterface.OnKeyListener f445;

        /* renamed from: ˎ  reason: contains not printable characters */
        public CharSequence[] f446;

        /* renamed from: ˏ  reason: contains not printable characters */
        public int f447;

        /* renamed from: ˑ  reason: contains not printable characters */
        public CharSequence f448;

        /* renamed from: י  reason: contains not printable characters */
        public View f449;

        /* renamed from: ـ  reason: contains not printable characters */
        public int f450;

        /* renamed from: ــ  reason: contains not printable characters */
        public boolean f451 = true;

        /* renamed from: ٴ  reason: contains not printable characters */
        public DialogInterface.OnClickListener f452;

        /* renamed from: ᐧ  reason: contains not printable characters */
        public CharSequence f453;

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        public int f454 = -1;

        /* renamed from: ᴵ  reason: contains not printable characters */
        public int f455;

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        public DialogInterface.OnMultiChoiceClickListener f456;

        /* renamed from: ᵎ  reason: contains not printable characters */
        public int f457;

        /* renamed from: ᵔ  reason: contains not printable characters */
        public int f458;

        /* renamed from: ᵢ  reason: contains not printable characters */
        public boolean f459 = false;

        /* renamed from: ⁱ  reason: contains not printable characters */
        public boolean[] f460;

        /* renamed from: 连任  reason: contains not printable characters */
        public int f461 = 0;

        /* renamed from: 靐  reason: contains not printable characters */
        public final LayoutInflater f462;

        /* renamed from: 麤  reason: contains not printable characters */
        public Drawable f463;

        /* renamed from: 齉  reason: contains not printable characters */
        public int f464 = 0;

        /* renamed from: 龘  reason: contains not printable characters */
        public final Context f465;

        /* renamed from: ﹳ  reason: contains not printable characters */
        public boolean f466;

        /* renamed from: ﹶ  reason: contains not printable characters */
        public boolean f467;

        /* renamed from: ﾞ  reason: contains not printable characters */
        public DialogInterface.OnCancelListener f468;

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public boolean f469;

        public interface OnPrepareListViewListener {
            /* renamed from: 龘  reason: contains not printable characters */
            void m512(ListView listView);
        }

        public AlertParams(Context context) {
            this.f465 = context;
            this.f467 = true;
            this.f462 = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: android.support.v7.app.AlertController$CheckedItemAdapter} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: android.widget.ListAdapter} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: android.widget.SimpleCursorAdapter} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: android.support.v7.app.AlertController$AlertParams$2} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: android.support.v7.app.AlertController$AlertParams$1} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: android.support.v7.app.AlertController$AlertParams$2} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: android.support.v7.app.AlertController$AlertParams$2} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: android.support.v7.app.AlertController$AlertParams$2} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v22, resolved type: android.support.v7.app.AlertController$AlertParams$2} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: android.support.v7.app.AlertController$AlertParams$2} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: 靐  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void m510(final android.support.v7.app.AlertController r11) {
            /*
                r10 = this;
                r4 = 16908308(0x1020014, float:2.3877285E-38)
                r9 = 1
                r5 = 0
                android.view.LayoutInflater r1 = r10.f462
                int r3 = r11.f389
                r7 = 0
                android.view.View r6 = r1.inflate(r3, r7)
                android.support.v7.app.AlertController$RecycleListView r6 = (android.support.v7.app.AlertController.RecycleListView) r6
                boolean r1 = r10.f466
                if (r1 == 0) goto L_0x005f
                android.database.Cursor r1 = r10.f432
                if (r1 != 0) goto L_0x0052
                android.support.v7.app.AlertController$AlertParams$1 r0 = new android.support.v7.app.AlertController$AlertParams$1
                android.content.Context r2 = r10.f465
                int r3 = r11.f383
                java.lang.CharSequence[] r5 = r10.f446
                r1 = r10
                r0.<init>(r2, r3, r4, r5, r6)
            L_0x0024:
                android.support.v7.app.AlertController$AlertParams$OnPrepareListViewListener r1 = r10.f438
                if (r1 == 0) goto L_0x002d
                android.support.v7.app.AlertController$AlertParams$OnPrepareListViewListener r1 = r10.f438
                r1.m512(r6)
            L_0x002d:
                r11.f400 = r0
                int r1 = r10.f454
                r11.f401 = r1
                android.content.DialogInterface$OnClickListener r1 = r10.f443
                if (r1 == 0) goto L_0x0093
                android.support.v7.app.AlertController$AlertParams$3 r1 = new android.support.v7.app.AlertController$AlertParams$3
                r1.<init>(r11)
                r6.setOnItemClickListener(r1)
            L_0x003f:
                android.widget.AdapterView$OnItemSelectedListener r1 = r10.f440
                if (r1 == 0) goto L_0x0048
                android.widget.AdapterView$OnItemSelectedListener r1 = r10.f440
                r6.setOnItemSelectedListener(r1)
            L_0x0048:
                boolean r1 = r10.f469
                if (r1 == 0) goto L_0x00a0
                r6.setChoiceMode(r9)
            L_0x004f:
                r11.f410 = r6
                return
            L_0x0052:
                android.support.v7.app.AlertController$AlertParams$2 r0 = new android.support.v7.app.AlertController$AlertParams$2
                android.content.Context r3 = r10.f465
                android.database.Cursor r4 = r10.f432
                r1 = r0
                r2 = r10
                r7 = r11
                r1.<init>(r3, r4, r5, r6, r7)
                goto L_0x0024
            L_0x005f:
                boolean r1 = r10.f469
                if (r1 == 0) goto L_0x007f
                int r2 = r11.f385
            L_0x0065:
                android.database.Cursor r1 = r10.f432
                if (r1 == 0) goto L_0x0082
                android.widget.SimpleCursorAdapter r0 = new android.widget.SimpleCursorAdapter
                android.content.Context r1 = r10.f465
                android.database.Cursor r3 = r10.f432
                java.lang.String[] r7 = new java.lang.String[r9]
                java.lang.String r8 = r10.f436
                r7[r5] = r8
                int[] r8 = new int[r9]
                r8[r5] = r4
                r4 = r7
                r5 = r8
                r0.<init>(r1, r2, r3, r4, r5)
                goto L_0x0024
            L_0x007f:
                int r2 = r11.f415
                goto L_0x0065
            L_0x0082:
                android.widget.ListAdapter r1 = r10.f441
                if (r1 == 0) goto L_0x0089
                android.widget.ListAdapter r0 = r10.f441
                goto L_0x0024
            L_0x0089:
                android.support.v7.app.AlertController$CheckedItemAdapter r0 = new android.support.v7.app.AlertController$CheckedItemAdapter
                android.content.Context r1 = r10.f465
                java.lang.CharSequence[] r3 = r10.f446
                r0.<init>(r1, r2, r4, r3)
                goto L_0x0024
            L_0x0093:
                android.content.DialogInterface$OnMultiChoiceClickListener r1 = r10.f456
                if (r1 == 0) goto L_0x003f
                android.support.v7.app.AlertController$AlertParams$4 r1 = new android.support.v7.app.AlertController$AlertParams$4
                r1.<init>(r6, r11)
                r6.setOnItemClickListener(r1)
                goto L_0x003f
            L_0x00a0:
                boolean r1 = r10.f466
                if (r1 == 0) goto L_0x004f
                r1 = 2
                r6.setChoiceMode(r1)
                goto L_0x004f
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AlertController.AlertParams.m510(android.support.v7.app.AlertController):void");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m511(AlertController alertController) {
            if (this.f433 != null) {
                alertController.m497(this.f433);
            } else {
                if (this.f431 != null) {
                    alertController.m508(this.f431);
                }
                if (this.f463 != null) {
                    alertController.m506(this.f463);
                }
                if (this.f464 != 0) {
                    alertController.m496(this.f464);
                }
                if (this.f461 != 0) {
                    alertController.m496(alertController.m501(this.f461));
                }
            }
            if (this.f435 != null) {
                alertController.m498(this.f435);
            }
            if (this.f448 != null) {
                alertController.m505(-1, this.f448, this.f452, (Message) null);
            }
            if (this.f453 != null) {
                alertController.m505(-2, this.f453, this.f442, (Message) null);
            }
            if (this.f437 != null) {
                alertController.m505(-3, this.f437, this.f439, (Message) null);
            }
            if (!(this.f446 == null && this.f432 == null && this.f441 == null)) {
                m510(alertController);
            }
            if (this.f449 != null) {
                if (this.f459) {
                    alertController.m507(this.f449, this.f450, this.f455, this.f457, this.f458);
                    return;
                }
                alertController.m502(this.f449);
            } else if (this.f447 != 0) {
                alertController.m504(this.f447);
            }
        }
    }

    private static final class ButtonHandler extends Handler {

        /* renamed from: 龘  reason: contains not printable characters */
        private WeakReference<DialogInterface> f482;

        public ButtonHandler(DialogInterface dialogInterface) {
            this.f482 = new WeakReference<>(dialogInterface);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case -3:
                case -2:
                case -1:
                    ((DialogInterface.OnClickListener) message.obj).onClick((DialogInterface) this.f482.get(), message.what);
                    return;
                case 1:
                    ((DialogInterface) message.obj).dismiss();
                    return;
                default:
                    return;
            }
        }
    }

    private static class CheckedItemAdapter extends ArrayAdapter<CharSequence> {
        public CheckedItemAdapter(Context context, int i, int i2, CharSequence[] charSequenceArr) {
            super(context, i, i2, charSequenceArr);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public boolean hasStableIds() {
            return true;
        }
    }

    public static class RecycleListView extends ListView {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f483;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f484;

        public RecycleListView(Context context) {
            this(context, (AttributeSet) null);
        }

        public RecycleListView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.RecycleListView);
            this.f483 = obtainStyledAttributes.getDimensionPixelOffset(R.styleable.RecycleListView_paddingBottomNoButtons, -1);
            this.f484 = obtainStyledAttributes.getDimensionPixelOffset(R.styleable.RecycleListView_paddingTopNoTitle, -1);
        }

        public void setHasDecor(boolean z, boolean z2) {
            if (!z2 || !z) {
                setPadding(getPaddingLeft(), z ? getPaddingTop() : this.f484, getPaddingRight(), z2 ? getPaddingBottom() : this.f483);
            }
        }
    }

    public AlertController(Context context, AppCompatDialog appCompatDialog, Window window) {
        this.f392 = context;
        this.f413 = appCompatDialog;
        this.f393 = window;
        this.f416 = new ButtonHandler(appCompatDialog);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes((AttributeSet) null, R.styleable.AlertDialog, R.attr.alertDialogStyle, 0);
        this.f386 = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_android_layout, 0);
        this.f384 = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_buttonPanelSideLayout, 0);
        this.f389 = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_listLayout, 0);
        this.f383 = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_multiChoiceItemLayout, 0);
        this.f385 = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_singleChoiceItemLayout, 0);
        this.f415 = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_listItemLayout, 0);
        this.f399 = obtainStyledAttributes.getBoolean(R.styleable.AlertDialog_showTitle, true);
        obtainStyledAttributes.recycle();
        appCompatDialog.supportRequestWindowFeature(1);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m483(ViewGroup viewGroup) {
        boolean z = false;
        if (this.f380 != null) {
            viewGroup.addView(this.f380, 0, new ViewGroup.LayoutParams(-1, -2));
            this.f393.findViewById(R.id.title_template).setVisibility(8);
            return;
        }
        this.f404 = (ImageView) this.f393.findViewById(16908294);
        if (!TextUtils.isEmpty(this.f394)) {
            z = true;
        }
        if (!z || !this.f399) {
            this.f393.findViewById(R.id.title_template).setVisibility(8);
            this.f404.setVisibility(8);
            viewGroup.setVisibility(8);
            return;
        }
        this.f378 = (TextView) this.f393.findViewById(R.id.alertTitle);
        this.f378.setText(this.f394);
        if (this.f417 != 0) {
            this.f404.setImageResource(this.f417);
        } else if (this.f402 != null) {
            this.f404.setImageDrawable(this.f402);
        } else {
            this.f378.setPadding(this.f404.getPaddingLeft(), this.f404.getPaddingTop(), this.f404.getPaddingRight(), this.f404.getPaddingBottom());
            this.f404.setVisibility(8);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m484() {
        View findViewById;
        View findViewById2;
        View findViewById3 = this.f393.findViewById(R.id.parentPanel);
        View findViewById4 = findViewById3.findViewById(R.id.topPanel);
        View findViewById5 = findViewById3.findViewById(R.id.contentPanel);
        View findViewById6 = findViewById3.findViewById(R.id.buttonPanel);
        ViewGroup viewGroup = (ViewGroup) findViewById3.findViewById(R.id.customPanel);
        m490(viewGroup);
        View findViewById7 = viewGroup.findViewById(R.id.topPanel);
        View findViewById8 = viewGroup.findViewById(R.id.contentPanel);
        View findViewById9 = viewGroup.findViewById(R.id.buttonPanel);
        ViewGroup r22 = m488(findViewById7, findViewById4);
        ViewGroup r6 = m488(findViewById8, findViewById5);
        ViewGroup r3 = m488(findViewById9, findViewById6);
        m487(r6);
        m485(r3);
        m483(r22);
        boolean z = (viewGroup == null || viewGroup.getVisibility() == 8) ? false : true;
        boolean z2 = (r22 == null || r22.getVisibility() == 8) ? false : true;
        boolean z3 = (r3 == null || r3.getVisibility() == 8) ? false : true;
        if (!(z3 || r6 == null || (findViewById2 = r6.findViewById(R.id.textSpacerNoButtons)) == null)) {
            findViewById2.setVisibility(0);
        }
        if (z2) {
            if (this.f396 != null) {
                this.f396.setClipToPadding(true);
            }
            View view = null;
            if (!(this.f387 == null && this.f410 == null)) {
                view = r22.findViewById(R.id.titleDividerNoCustom);
            }
            if (view != null) {
                view.setVisibility(0);
            }
        } else if (!(r6 == null || (findViewById = r6.findViewById(R.id.textSpacerNoTitle)) == null)) {
            findViewById.setVisibility(0);
        }
        if (this.f410 instanceof RecycleListView) {
            ((RecycleListView) this.f410).setHasDecor(z2, z3);
        }
        if (!z) {
            View view2 = this.f410 != null ? this.f410 : this.f396;
            if (view2 != null) {
                m491(r6, view2, (z2 ? 1 : 0) | (z3 ? 2 : 0), 3);
            }
        }
        ListView listView = this.f410;
        if (listView != null && this.f400 != null) {
            listView.setAdapter(this.f400);
            int i = this.f401;
            if (i > -1) {
                listView.setItemChecked(i, true);
                listView.setSelection(i);
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m485(ViewGroup viewGroup) {
        boolean z = false;
        boolean z2 = false;
        this.f412 = (Button) viewGroup.findViewById(16908313);
        this.f412.setOnClickListener(this.f391);
        if (TextUtils.isEmpty(this.f407)) {
            this.f412.setVisibility(8);
        } else {
            this.f412.setText(this.f407);
            this.f412.setVisibility(0);
            z2 = false | true;
        }
        this.f409 = (Button) viewGroup.findViewById(16908314);
        this.f409.setOnClickListener(this.f391);
        if (TextUtils.isEmpty(this.f408)) {
            this.f409.setVisibility(8);
        } else {
            this.f409.setText(this.f408);
            this.f409.setVisibility(0);
            z2 |= true;
        }
        this.f379 = (Button) viewGroup.findViewById(16908315);
        this.f379.setOnClickListener(this.f391);
        if (TextUtils.isEmpty(this.f414)) {
            this.f379.setVisibility(8);
        } else {
            this.f379.setText(this.f414);
            this.f379.setVisibility(0);
            z2 |= true;
        }
        if (m493(this.f392)) {
            if (z2) {
                m492(this.f412);
            } else if (z2) {
                m492(this.f409);
            } else if (z2) {
                m492(this.f379);
            }
        }
        if (z2) {
            z = true;
        }
        if (!z) {
            viewGroup.setVisibility(8);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private int m486() {
        return this.f384 == 0 ? this.f386 : this.f388 == 1 ? this.f384 : this.f386;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m487(ViewGroup viewGroup) {
        this.f396 = (NestedScrollView) this.f393.findViewById(R.id.scrollView);
        this.f396.setFocusable(false);
        this.f396.setNestedScrollingEnabled(false);
        this.f382 = (TextView) viewGroup.findViewById(16908299);
        if (this.f382 != null) {
            if (this.f387 != null) {
                this.f382.setText(this.f387);
                return;
            }
            this.f382.setVisibility(8);
            this.f396.removeView(this.f382);
            if (this.f410 != null) {
                ViewGroup viewGroup2 = (ViewGroup) this.f396.getParent();
                int indexOfChild = viewGroup2.indexOfChild(this.f396);
                viewGroup2.removeViewAt(indexOfChild);
                viewGroup2.addView(this.f410, indexOfChild, new ViewGroup.LayoutParams(-1, -1));
                return;
            }
            viewGroup.setVisibility(8);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ViewGroup m488(View view, View view2) {
        if (view == null) {
            if (view2 instanceof ViewStub) {
                view2 = ((ViewStub) view2).inflate();
            }
            return (ViewGroup) view2;
        }
        if (view2 != null) {
            ViewParent parent = view2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(view2);
            }
        }
        if (view instanceof ViewStub) {
            view = ((ViewStub) view).inflate();
        }
        return (ViewGroup) view;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m489(View view, View view2, View view3) {
        int i = 0;
        if (view2 != null) {
            view2.setVisibility(view.canScrollVertically(-1) ? 0 : 4);
        }
        if (view3 != null) {
            if (!view.canScrollVertically(1)) {
                i = 4;
            }
            view3.setVisibility(i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m490(ViewGroup viewGroup) {
        boolean z = false;
        View inflate = this.f390 != null ? this.f390 : this.f395 != 0 ? LayoutInflater.from(this.f392).inflate(this.f395, viewGroup, false) : null;
        if (inflate != null) {
            z = true;
        }
        if (!z || !m494(inflate)) {
            this.f393.setFlags(131072, 131072);
        }
        if (z) {
            FrameLayout frameLayout = (FrameLayout) this.f393.findViewById(R.id.custom);
            frameLayout.addView(inflate, new ViewGroup.LayoutParams(-1, -1));
            if (this.f406) {
                frameLayout.setPadding(this.f397, this.f398, this.f403, this.f405);
            }
            if (this.f410 != null) {
                ((LinearLayoutCompat.LayoutParams) viewGroup.getLayoutParams()).weight = 0.0f;
                return;
            }
            return;
        }
        viewGroup.setVisibility(8);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m491(ViewGroup viewGroup, View view, int i, int i2) {
        View findViewById = this.f393.findViewById(R.id.scrollIndicatorUp);
        View findViewById2 = this.f393.findViewById(R.id.scrollIndicatorDown);
        if (Build.VERSION.SDK_INT >= 23) {
            ViewCompat.setScrollIndicators(view, i, i2);
            if (findViewById != null) {
                viewGroup.removeView(findViewById);
            }
            if (findViewById2 != null) {
                viewGroup.removeView(findViewById2);
                return;
            }
            return;
        }
        if (findViewById != null && (i & 1) == 0) {
            viewGroup.removeView(findViewById);
            findViewById = null;
        }
        if (findViewById2 != null && (i & 2) == 0) {
            viewGroup.removeView(findViewById2);
            findViewById2 = null;
        }
        if (findViewById != null || findViewById2 != null) {
            final View view2 = findViewById;
            final View view3 = findViewById2;
            if (this.f387 != null) {
                this.f396.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                    public void onScrollChange(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
                        AlertController.m489(nestedScrollView, view2, view3);
                    }
                });
                this.f396.post(new Runnable() {
                    public void run() {
                        AlertController.m489(AlertController.this.f396, view2, view3);
                    }
                });
            } else if (this.f410 != null) {
                this.f410.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                        AlertController.m489(absListView, view2, view3);
                    }

                    public void onScrollStateChanged(AbsListView absListView, int i) {
                    }
                });
                this.f410.post(new Runnable() {
                    public void run() {
                        AlertController.m489(AlertController.this.f410, view2, view3);
                    }
                });
            } else {
                if (view2 != null) {
                    viewGroup.removeView(view2);
                }
                if (view3 != null) {
                    viewGroup.removeView(view3);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m492(Button button) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) button.getLayoutParams();
        layoutParams.gravity = 1;
        layoutParams.weight = 0.5f;
        button.setLayoutParams(layoutParams);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m493(Context context) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.alertDialogCenterButtons, typedValue, true);
        return typedValue.data != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m494(View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        while (childCount > 0) {
            childCount--;
            if (m494(viewGroup.getChildAt(childCount))) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ListView m495() {
        return this.f410;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m496(int i) {
        this.f402 = null;
        this.f417 = i;
        if (this.f404 == null) {
            return;
        }
        if (i != 0) {
            this.f404.setVisibility(0);
            this.f404.setImageResource(this.f417);
            return;
        }
        this.f404.setVisibility(8);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m497(View view) {
        this.f380 = view;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m498(CharSequence charSequence) {
        this.f387 = charSequence;
        if (this.f382 != null) {
            this.f382.setText(charSequence);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m499(int i, KeyEvent keyEvent) {
        return this.f396 != null && this.f396.executeKeyEvent(keyEvent);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Button m500(int i) {
        switch (i) {
            case -3:
                return this.f379;
            case -2:
                return this.f409;
            case -1:
                return this.f412;
            default:
                return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m501(int i) {
        TypedValue typedValue = new TypedValue();
        this.f392.getTheme().resolveAttribute(i, typedValue, true);
        return typedValue.resourceId;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m502(View view) {
        this.f390 = view;
        this.f395 = 0;
        this.f406 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m503() {
        this.f413.setContentView(m486());
        m484();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m504(int i) {
        this.f390 = null;
        this.f395 = i;
        this.f406 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m505(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, Message message) {
        if (message == null && onClickListener != null) {
            message = this.f416.obtainMessage(i, onClickListener);
        }
        switch (i) {
            case -3:
                this.f414 = charSequence;
                this.f381 = message;
                return;
            case -2:
                this.f408 = charSequence;
                this.f377 = message;
                return;
            case -1:
                this.f407 = charSequence;
                this.f411 = message;
                return;
            default:
                throw new IllegalArgumentException("Button does not exist");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m506(Drawable drawable) {
        this.f402 = drawable;
        this.f417 = 0;
        if (this.f404 == null) {
            return;
        }
        if (drawable != null) {
            this.f404.setVisibility(0);
            this.f404.setImageDrawable(drawable);
            return;
        }
        this.f404.setVisibility(8);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m507(View view, int i, int i2, int i3, int i4) {
        this.f390 = view;
        this.f395 = 0;
        this.f406 = true;
        this.f397 = i;
        this.f398 = i2;
        this.f403 = i3;
        this.f405 = i4;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m508(CharSequence charSequence) {
        this.f394 = charSequence;
        if (this.f378 != null) {
            this.f378.setText(charSequence);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m509(int i, KeyEvent keyEvent) {
        return this.f396 != null && this.f396.executeKeyEvent(keyEvent);
    }
}
