package android.support.v7.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggleHoneycomb;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class ActionBarDrawerToggle implements DrawerLayout.DrawerListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f356;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Drawable f357;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f358;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f359;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f360;

    /* renamed from: 连任  reason: contains not printable characters */
    private DrawerArrowDrawable f361;

    /* renamed from: 靐  reason: contains not printable characters */
    View.OnClickListener f362;

    /* renamed from: 麤  reason: contains not printable characters */
    private final DrawerLayout f363;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Delegate f364;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean f365;

    public interface Delegate {
        /* renamed from: 靐  reason: contains not printable characters */
        Context m460();

        /* renamed from: 齉  reason: contains not printable characters */
        boolean m461();

        /* renamed from: 龘  reason: contains not printable characters */
        Drawable m462();

        /* renamed from: 龘  reason: contains not printable characters */
        void m463(int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m464(Drawable drawable, int i);
    }

    public interface DelegateProvider {
        Delegate getDrawerToggleDelegate();
    }

    private static class IcsDelegate implements Delegate {

        /* renamed from: 靐  reason: contains not printable characters */
        ActionBarDrawerToggleHoneycomb.SetIndicatorInfo f367;

        /* renamed from: 龘  reason: contains not printable characters */
        final Activity f368;

        IcsDelegate(Activity activity) {
            this.f368 = activity;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Context m465() {
            ActionBar actionBar = this.f368.getActionBar();
            return actionBar != null ? actionBar.getThemedContext() : this.f368;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m466() {
            ActionBar actionBar = this.f368.getActionBar();
            return (actionBar == null || (actionBar.getDisplayOptions() & 4) == 0) ? false : true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Drawable m467() {
            return ActionBarDrawerToggleHoneycomb.m480(this.f368);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m468(int i) {
            this.f367 = ActionBarDrawerToggleHoneycomb.m481(this.f367, this.f368, i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m469(Drawable drawable, int i) {
            ActionBar actionBar = this.f368.getActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowHomeEnabled(true);
                this.f367 = ActionBarDrawerToggleHoneycomb.m482(this.f367, this.f368, drawable, i);
                actionBar.setDisplayShowHomeEnabled(false);
            }
        }
    }

    private static class JellybeanMr2Delegate implements Delegate {

        /* renamed from: 龘  reason: contains not printable characters */
        final Activity f369;

        JellybeanMr2Delegate(Activity activity) {
            this.f369 = activity;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Context m470() {
            ActionBar actionBar = this.f369.getActionBar();
            return actionBar != null ? actionBar.getThemedContext() : this.f369;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m471() {
            ActionBar actionBar = this.f369.getActionBar();
            return (actionBar == null || (actionBar.getDisplayOptions() & 4) == 0) ? false : true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Drawable m472() {
            TypedArray obtainStyledAttributes = m470().obtainStyledAttributes((AttributeSet) null, new int[]{16843531}, 16843470, 0);
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            obtainStyledAttributes.recycle();
            return drawable;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m473(int i) {
            ActionBar actionBar = this.f369.getActionBar();
            if (actionBar != null) {
                actionBar.setHomeActionContentDescription(i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m474(Drawable drawable, int i) {
            ActionBar actionBar = this.f369.getActionBar();
            if (actionBar != null) {
                actionBar.setHomeAsUpIndicator(drawable);
                actionBar.setHomeActionContentDescription(i);
            }
        }
    }

    static class ToolbarCompatDelegate implements Delegate {

        /* renamed from: 靐  reason: contains not printable characters */
        final Drawable f370;

        /* renamed from: 齉  reason: contains not printable characters */
        final CharSequence f371;

        /* renamed from: 龘  reason: contains not printable characters */
        final Toolbar f372;

        ToolbarCompatDelegate(Toolbar toolbar) {
            this.f372 = toolbar;
            this.f370 = toolbar.getNavigationIcon();
            this.f371 = toolbar.getNavigationContentDescription();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Context m475() {
            return this.f372.getContext();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m476() {
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Drawable m477() {
            return this.f370;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m478(int i) {
            if (i == 0) {
                this.f372.setNavigationContentDescription(this.f371);
            } else {
                this.f372.setNavigationContentDescription(i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m479(Drawable drawable, int i) {
            this.f372.setNavigationIcon(drawable);
            m478(i);
        }
    }

    public ActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int i, int i2) {
        this(activity, toolbar, drawerLayout, (DrawerArrowDrawable) null, i, i2);
    }

    ActionBarDrawerToggle(Activity activity, Toolbar toolbar, DrawerLayout drawerLayout, DrawerArrowDrawable drawerArrowDrawable, int i, int i2) {
        this.f356 = true;
        this.f365 = true;
        this.f360 = false;
        if (toolbar != null) {
            this.f364 = new ToolbarCompatDelegate(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (ActionBarDrawerToggle.this.f365) {
                        ActionBarDrawerToggle.this.m455();
                    } else if (ActionBarDrawerToggle.this.f362 != null) {
                        ActionBarDrawerToggle.this.f362.onClick(view);
                    }
                }
            });
        } else if (activity instanceof DelegateProvider) {
            this.f364 = ((DelegateProvider) activity).getDrawerToggleDelegate();
        } else if (Build.VERSION.SDK_INT >= 18) {
            this.f364 = new JellybeanMr2Delegate(activity);
        } else {
            this.f364 = new IcsDelegate(activity);
        }
        this.f363 = drawerLayout;
        this.f358 = i;
        this.f359 = i2;
        if (drawerArrowDrawable == null) {
            this.f361 = new DrawerArrowDrawable(this.f364.m460());
        } else {
            this.f361 = drawerArrowDrawable;
        }
        this.f357 = m456();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m454(float f) {
        if (f == 1.0f) {
            this.f361.m1016(true);
        } else if (f == 0.0f) {
            this.f361.m1016(false);
        }
        this.f361.m1017(f);
    }

    public void onDrawerClosed(View view) {
        m454(0.0f);
        if (this.f365) {
            m458(this.f358);
        }
    }

    public void onDrawerOpened(View view) {
        m454(1.0f);
        if (this.f365) {
            m458(this.f359);
        }
    }

    public void onDrawerSlide(View view, float f) {
        if (this.f356) {
            m454(Math.min(1.0f, Math.max(0.0f, f)));
        } else {
            m454(0.0f);
        }
    }

    public void onDrawerStateChanged(int i) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m455() {
        int drawerLockMode = this.f363.getDrawerLockMode((int) GravityCompat.START);
        if (this.f363.isDrawerVisible((int) GravityCompat.START) && drawerLockMode != 2) {
            this.f363.closeDrawer((int) GravityCompat.START);
        } else if (drawerLockMode != 1) {
            this.f363.openDrawer((int) GravityCompat.START);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public Drawable m456() {
        return this.f364.m462();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m457() {
        if (this.f363.isDrawerOpen((int) GravityCompat.START)) {
            m454(1.0f);
        } else {
            m454(0.0f);
        }
        if (this.f365) {
            m459(this.f361, this.f363.isDrawerOpen((int) GravityCompat.START) ? this.f359 : this.f358);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m458(int i) {
        this.f364.m463(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m459(Drawable drawable, int i) {
        if (!this.f360 && !this.f364.m461()) {
            Log.w("ActionBarDrawerToggle", "DrawerToggle may not show up because NavigationIcon is not visible. You may need to call actionbar.setDisplayHomeAsUpEnabled(true);");
            this.f360 = true;
        }
        this.f364.m464(drawable, i);
    }
}
