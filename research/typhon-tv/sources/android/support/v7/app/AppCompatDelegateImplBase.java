package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.appcompat.R;
import android.support.v7.view.ActionMode;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.WindowCallbackWrapper;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.TintTypedArray;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Window;
import java.lang.Thread;

abstract class AppCompatDelegateImplBase extends AppCompatDelegate {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static boolean f490 = true;

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final boolean f491 = (Build.VERSION.SDK_INT < 21);

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final int[] f492 = {16842836};

    /* renamed from: ʻ  reason: contains not printable characters */
    ActionBar f493;

    /* renamed from: ʼ  reason: contains not printable characters */
    MenuInflater f494;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f495;

    /* renamed from: ˈ  reason: contains not printable characters */
    boolean f496;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f497;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f498;

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean f499;

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean f500;

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean f501;

    /* renamed from: 连任  reason: contains not printable characters */
    final AppCompatCallback f502;

    /* renamed from: 靐  reason: contains not printable characters */
    final Window f503;

    /* renamed from: 麤  reason: contains not printable characters */
    final Window.Callback f504;

    /* renamed from: 齉  reason: contains not printable characters */
    final Window.Callback f505 = this.f503.getCallback();

    /* renamed from: 龘  reason: contains not printable characters */
    final Context f506;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private CharSequence f507;

    private class ActionBarDrawableToggleImpl implements ActionBarDrawerToggle.Delegate {
        ActionBarDrawableToggleImpl() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Context m594() {
            return AppCompatDelegateImplBase.this.m591();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m595() {
            ActionBar r0 = AppCompatDelegateImplBase.this.m585();
            return (r0 == null || (r0.m424() & 4) == 0) ? false : true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Drawable m596() {
            TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(m594(), (AttributeSet) null, new int[]{R.attr.homeAsUpIndicator});
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            obtainStyledAttributes.recycle();
            return drawable;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m597(int i) {
            ActionBar r0 = AppCompatDelegateImplBase.this.m585();
            if (r0 != null) {
                r0.m431(i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m598(Drawable drawable, int i) {
            ActionBar r0 = AppCompatDelegateImplBase.this.m585();
            if (r0 != null) {
                r0.m439(drawable);
                r0.m431(i);
            }
        }
    }

    class AppCompatWindowCallbackBase extends WindowCallbackWrapper {
        AppCompatWindowCallbackBase(Window.Callback callback) {
            super(callback);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImplBase.this.m590(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || AppCompatDelegateImplBase.this.m589(keyEvent.getKeyCode(), keyEvent);
        }

        public void onContentChanged() {
        }

        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof MenuBuilder)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            AppCompatDelegateImplBase.this.m581(i, menu);
            return true;
        }

        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            AppCompatDelegateImplBase.this.m587(i, menu);
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            MenuBuilder menuBuilder = menu instanceof MenuBuilder ? (MenuBuilder) menu : null;
            if (i == 0 && menuBuilder == null) {
                return false;
            }
            if (menuBuilder != null) {
                menuBuilder.setOverrideVisibleItems(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (menuBuilder == null) {
                return onPreparePanel;
            }
            menuBuilder.setOverrideVisibleItems(false);
            return onPreparePanel;
        }
    }

    static {
        if (f491 && !f490) {
            final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                /* renamed from: 龘  reason: contains not printable characters */
                private boolean m593(Throwable th) {
                    String message;
                    if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                        return false;
                    }
                    return message.contains("drawable") || message.contains("Drawable");
                }

                public void uncaughtException(Thread thread, Throwable th) {
                    if (m593(th)) {
                        Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                        notFoundException.initCause(th.getCause());
                        notFoundException.setStackTrace(th.getStackTrace());
                        defaultUncaughtExceptionHandler.uncaughtException(thread, notFoundException);
                        return;
                    }
                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                }
            });
        }
    }

    AppCompatDelegateImplBase(Context context, Window window, AppCompatCallback appCompatCallback) {
        this.f506 = context;
        this.f503 = window;
        this.f502 = appCompatCallback;
        if (this.f505 instanceof AppCompatWindowCallbackBase) {
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        this.f504 = m586(this.f505);
        this.f503.setCallback(this.f504);
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, (AttributeSet) null, f492);
        Drawable drawableIfKnown = obtainStyledAttributes.getDrawableIfKnown(0);
        if (drawableIfKnown != null) {
            this.f503.setBackgroundDrawable(drawableIfKnown);
        }
        obtainStyledAttributes.recycle();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m570() {
        this.f498 = true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final ActionBarDrawerToggle.Delegate m571() {
        return new ActionBarDrawableToggleImpl();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract void m572();

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public final ActionBar m573() {
        return this.f493;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m574() {
        return this.f498;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final Window.Callback m575() {
        return this.f503.getCallback();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final CharSequence m576() {
        return this.f505 instanceof Activity ? ((Activity) this.f505).getTitle() : this.f507;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m577() {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract ActionMode m578(ActionMode.Callback callback);

    /* renamed from: 靐  reason: contains not printable characters */
    public MenuInflater m579() {
        if (this.f494 == null) {
            m572();
            this.f494 = new SupportMenuInflater(this.f493 != null ? this.f493.m428() : this.f506);
        }
        return this.f494;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m580(CharSequence charSequence);

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract boolean m581(int i, Menu menu);

    /* renamed from: 麤  reason: contains not printable characters */
    public void m582() {
        this.f497 = false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m583() {
        this.f497 = true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m584(Bundle bundle) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ActionBar m585() {
        m572();
        return this.f493;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Window.Callback m586(Window.Callback callback) {
        return new AppCompatWindowCallbackBase(callback);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m587(int i, Menu menu);

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m588(CharSequence charSequence) {
        this.f507 = charSequence;
        m580(charSequence);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m589(int i, KeyEvent keyEvent);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m590(KeyEvent keyEvent);

    /* access modifiers changed from: package-private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public final Context m591() {
        Context context = null;
        ActionBar r0 = m585();
        if (r0 != null) {
            context = r0.m428();
        }
        return context == null ? this.f506 : context;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m592() {
        return false;
    }
}
