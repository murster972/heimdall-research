package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.view.View;

class AppCompatBackgroundHelper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private TintInfo f1481;

    /* renamed from: 连任  reason: contains not printable characters */
    private TintInfo f1482;

    /* renamed from: 靐  reason: contains not printable characters */
    private final AppCompatDrawableManager f1483;

    /* renamed from: 麤  reason: contains not printable characters */
    private TintInfo f1484;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f1485 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f1486;

    AppCompatBackgroundHelper(View view) {
        this.f1486 = view;
        this.f1483 = AppCompatDrawableManager.get();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m1723(Drawable drawable) {
        if (this.f1481 == null) {
            this.f1481 = new TintInfo();
        }
        TintInfo tintInfo = this.f1481;
        tintInfo.m2167();
        ColorStateList backgroundTintList = ViewCompat.getBackgroundTintList(this.f1486);
        if (backgroundTintList != null) {
            tintInfo.f1818 = true;
            tintInfo.f1820 = backgroundTintList;
        }
        PorterDuff.Mode backgroundTintMode = ViewCompat.getBackgroundTintMode(this.f1486);
        if (backgroundTintMode != null) {
            tintInfo.f1819 = true;
            tintInfo.f1817 = backgroundTintMode;
        }
        if (!tintInfo.f1818 && !tintInfo.f1819) {
            return false;
        }
        AppCompatDrawableManager.tintDrawable(drawable, tintInfo, this.f1486.getDrawableState());
        return true;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m1724() {
        int i = Build.VERSION.SDK_INT;
        return i > 21 ? this.f1484 != null : i == 21;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public PorterDuff.Mode m1725() {
        if (this.f1482 != null) {
            return this.f1482.f1817;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1726(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.f1484 == null) {
                this.f1484 = new TintInfo();
            }
            this.f1484.f1820 = colorStateList;
            this.f1484.f1818 = true;
        } else {
            this.f1484 = null;
        }
        m1727();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m1727() {
        Drawable background = this.f1486.getBackground();
        if (background == null) {
            return;
        }
        if (m1724() && m1723(background)) {
            return;
        }
        if (this.f1482 != null) {
            AppCompatDrawableManager.tintDrawable(background, this.f1482, this.f1486.getDrawableState());
        } else if (this.f1484 != null) {
            AppCompatDrawableManager.tintDrawable(background, this.f1484, this.f1486.getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ColorStateList m1728() {
        if (this.f1482 != null) {
            return this.f1482.f1820;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1729(int i) {
        this.f1485 = i;
        m1726(this.f1483 != null ? this.f1483.getTintList(this.f1486.getContext(), i) : null);
        m1727();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1730(ColorStateList colorStateList) {
        if (this.f1482 == null) {
            this.f1482 = new TintInfo();
        }
        this.f1482.f1820 = colorStateList;
        this.f1482.f1818 = true;
        m1727();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1731(PorterDuff.Mode mode) {
        if (this.f1482 == null) {
            this.f1482 = new TintInfo();
        }
        this.f1482.f1817 = mode;
        this.f1482.f1819 = true;
        m1727();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1732(Drawable drawable) {
        this.f1485 = -1;
        m1726((ColorStateList) null);
        m1727();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1733(AttributeSet attributeSet, int i) {
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(this.f1486.getContext(), attributeSet, R.styleable.ViewBackgroundHelper, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(R.styleable.ViewBackgroundHelper_android_background)) {
                this.f1485 = obtainStyledAttributes.getResourceId(R.styleable.ViewBackgroundHelper_android_background, -1);
                ColorStateList tintList = this.f1483.getTintList(this.f1486.getContext(), this.f1485);
                if (tintList != null) {
                    m1726(tintList);
                }
            }
            if (obtainStyledAttributes.hasValue(R.styleable.ViewBackgroundHelper_backgroundTint)) {
                ViewCompat.setBackgroundTintList(this.f1486, obtainStyledAttributes.getColorStateList(R.styleable.ViewBackgroundHelper_backgroundTint));
            }
            if (obtainStyledAttributes.hasValue(R.styleable.ViewBackgroundHelper_backgroundTintMode)) {
                ViewCompat.setBackgroundTintMode(this.f1486, DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(R.styleable.ViewBackgroundHelper_backgroundTintMode, -1), (PorterDuff.Mode) null));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }
}
