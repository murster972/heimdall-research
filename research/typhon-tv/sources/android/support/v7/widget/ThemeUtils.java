package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;
import android.util.AttributeSet;
import android.util.TypedValue;

class ThemeUtils {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final int[] f1807 = {16842913};

    /* renamed from: ʼ  reason: contains not printable characters */
    static final int[] f1808 = {-16842919, -16842908};

    /* renamed from: ʽ  reason: contains not printable characters */
    static final int[] f1809 = new int[0];

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final ThreadLocal<TypedValue> f1810 = new ThreadLocal<>();

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final int[] f1811 = new int[1];

    /* renamed from: 连任  reason: contains not printable characters */
    static final int[] f1812 = {16842912};

    /* renamed from: 靐  reason: contains not printable characters */
    static final int[] f1813 = {16842908};

    /* renamed from: 麤  reason: contains not printable characters */
    static final int[] f1814 = {16842919};

    /* renamed from: 齉  reason: contains not printable characters */
    static final int[] f1815 = {16843518};

    /* renamed from: 龘  reason: contains not printable characters */
    static final int[] f1816 = {-16842910};

    /* renamed from: 靐  reason: contains not printable characters */
    public static ColorStateList m2162(Context context, int i) {
        f1811[0] = i;
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, (AttributeSet) null, f1811);
        try {
            return obtainStyledAttributes.getColorStateList(0);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static int m2163(Context context, int i) {
        ColorStateList r0 = m2162(context, i);
        if (r0 != null && r0.isStateful()) {
            return r0.getColorForState(f1816, r0.getDefaultColor());
        }
        TypedValue r2 = m2166();
        context.getTheme().resolveAttribute(16842803, r2, true);
        return m2165(context, i, r2.getFloat());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m2164(Context context, int i) {
        f1811[0] = i;
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, (AttributeSet) null, f1811);
        try {
            return obtainStyledAttributes.getColor(0, 0);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m2165(Context context, int i, float f) {
        int r0 = m2164(context, i);
        return ColorUtils.setAlphaComponent(r0, Math.round(((float) Color.alpha(r0)) * f));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static TypedValue m2166() {
        TypedValue typedValue = f1810.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        f1810.set(typedValue2);
        return typedValue2;
    }
}
