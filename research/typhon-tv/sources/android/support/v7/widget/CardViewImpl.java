package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;

interface CardViewImpl {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m1869(CardViewDelegate cardViewDelegate);

    /* renamed from: ʼ  reason: contains not printable characters */
    void m1870(CardViewDelegate cardViewDelegate);

    /* renamed from: ʽ  reason: contains not printable characters */
    void m1871(CardViewDelegate cardViewDelegate);

    /* renamed from: ˑ  reason: contains not printable characters */
    ColorStateList m1872(CardViewDelegate cardViewDelegate);

    /* renamed from: 连任  reason: contains not printable characters */
    float m1873(CardViewDelegate cardViewDelegate);

    /* renamed from: 靐  reason: contains not printable characters */
    float m1874(CardViewDelegate cardViewDelegate);

    /* renamed from: 靐  reason: contains not printable characters */
    void m1875(CardViewDelegate cardViewDelegate, float f);

    /* renamed from: 麤  reason: contains not printable characters */
    float m1876(CardViewDelegate cardViewDelegate);

    /* renamed from: 齉  reason: contains not printable characters */
    float m1877(CardViewDelegate cardViewDelegate);

    /* renamed from: 齉  reason: contains not printable characters */
    void m1878(CardViewDelegate cardViewDelegate, float f);

    /* renamed from: 龘  reason: contains not printable characters */
    float m1879(CardViewDelegate cardViewDelegate);

    /* renamed from: 龘  reason: contains not printable characters */
    void m1880();

    /* renamed from: 龘  reason: contains not printable characters */
    void m1881(CardViewDelegate cardViewDelegate, float f);

    /* renamed from: 龘  reason: contains not printable characters */
    void m1882(CardViewDelegate cardViewDelegate, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    /* renamed from: 龘  reason: contains not printable characters */
    void m1883(CardViewDelegate cardViewDelegate, ColorStateList colorStateList);
}
