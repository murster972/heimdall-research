package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

class TintInfo {

    /* renamed from: 靐  reason: contains not printable characters */
    public PorterDuff.Mode f1817;

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f1818;

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f1819;

    /* renamed from: 龘  reason: contains not printable characters */
    public ColorStateList f1820;

    TintInfo() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2167() {
        this.f1820 = null;
        this.f1818 = false;
        this.f1817 = null;
        this.f1819 = false;
    }
}
