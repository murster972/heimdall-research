package android.support.v7.widget;

import android.support.v4.util.Pools;
import android.support.v7.widget.OpReorderer;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

class AdapterHelper implements OpReorderer.Callback {

    /* renamed from: ʻ  reason: contains not printable characters */
    final OpReorderer f1469;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Pools.Pool<UpdateOp> f1470;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f1471;

    /* renamed from: 连任  reason: contains not printable characters */
    final boolean f1472;

    /* renamed from: 靐  reason: contains not printable characters */
    final ArrayList<UpdateOp> f1473;

    /* renamed from: 麤  reason: contains not printable characters */
    Runnable f1474;

    /* renamed from: 齉  reason: contains not printable characters */
    final Callback f1475;

    /* renamed from: 龘  reason: contains not printable characters */
    final ArrayList<UpdateOp> f1476;

    interface Callback {
        /* renamed from: 靐  reason: contains not printable characters */
        void m1714(int i, int i2);

        /* renamed from: 靐  reason: contains not printable characters */
        void m1715(UpdateOp updateOp);

        /* renamed from: 麤  reason: contains not printable characters */
        void m1716(int i, int i2);

        /* renamed from: 齉  reason: contains not printable characters */
        void m1717(int i, int i2);

        /* renamed from: 龘  reason: contains not printable characters */
        RecyclerView.ViewHolder m1718(int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1719(int i, int i2);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1720(int i, int i2, Object obj);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1721(UpdateOp updateOp);
    }

    static class UpdateOp {

        /* renamed from: 靐  reason: contains not printable characters */
        int f1477;

        /* renamed from: 麤  reason: contains not printable characters */
        int f1478;

        /* renamed from: 齉  reason: contains not printable characters */
        Object f1479;

        /* renamed from: 龘  reason: contains not printable characters */
        int f1480;

        UpdateOp(int i, int i2, int i3, Object obj) {
            this.f1480 = i;
            this.f1477 = i2;
            this.f1478 = i3;
            this.f1479 = obj;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            UpdateOp updateOp = (UpdateOp) obj;
            if (this.f1480 != updateOp.f1480) {
                return false;
            }
            if (this.f1480 == 8 && Math.abs(this.f1478 - this.f1477) == 1 && this.f1478 == updateOp.f1477 && this.f1477 == updateOp.f1478) {
                return true;
            }
            if (this.f1478 != updateOp.f1478) {
                return false;
            }
            if (this.f1477 != updateOp.f1477) {
                return false;
            }
            return this.f1479 != null ? this.f1479.equals(updateOp.f1479) : updateOp.f1479 == null;
        }

        public int hashCode() {
            return (((this.f1480 * 31) + this.f1477) * 31) + this.f1478;
        }

        public String toString() {
            return Integer.toHexString(System.identityHashCode(this)) + "[" + m1722() + ",s:" + this.f1477 + "c:" + this.f1478 + ",p:" + this.f1479 + "]";
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m1722() {
            switch (this.f1480) {
                case 1:
                    return "add";
                case 2:
                    return "rm";
                case 4:
                    return "up";
                case 8:
                    return "mv";
                default:
                    return "??";
            }
        }
    }

    AdapterHelper(Callback callback) {
        this(callback, false);
    }

    AdapterHelper(Callback callback, boolean z) {
        this.f1470 = new Pools.SimplePool(30);
        this.f1476 = new ArrayList<>();
        this.f1473 = new ArrayList<>();
        this.f1471 = 0;
        this.f1475 = callback;
        this.f1472 = z;
        this.f1469 = new OpReorderer(this);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1688(UpdateOp updateOp) {
        m1689(updateOp);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m1689(UpdateOp updateOp) {
        this.f1473.add(updateOp);
        switch (updateOp.f1480) {
            case 1:
                this.f1475.m1717(updateOp.f1477, updateOp.f1478);
                return;
            case 2:
                this.f1475.m1714(updateOp.f1477, updateOp.f1478);
                return;
            case 4:
                this.f1475.m1720(updateOp.f1477, updateOp.f1478, updateOp.f1479);
                return;
            case 8:
                this.f1475.m1716(updateOp.f1477, updateOp.f1478);
                return;
            default:
                throw new IllegalArgumentException("Unknown update op type for " + updateOp);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m1690(UpdateOp updateOp) {
        int i;
        if (updateOp.f1480 == 1 || updateOp.f1480 == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int r8 = m1692(updateOp.f1477, updateOp.f1480);
        int i2 = 1;
        int i3 = updateOp.f1477;
        switch (updateOp.f1480) {
            case 2:
                i = 0;
                break;
            case 4:
                i = 1;
                break;
            default:
                throw new IllegalArgumentException("op should be remove or update." + updateOp);
        }
        for (int i4 = 1; i4 < updateOp.f1478; i4++) {
            int r9 = m1692(updateOp.f1477 + (i * i4), updateOp.f1480);
            boolean z = false;
            switch (updateOp.f1480) {
                case 2:
                    if (r9 != r8) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                case 4:
                    if (r9 != r8 + 1) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
            }
            if (z) {
                i2++;
            } else {
                UpdateOp r6 = m1706(updateOp.f1480, r8, i2, updateOp.f1479);
                m1709(r6, i3);
                m1708(r6);
                if (updateOp.f1480 == 4) {
                    i3 += i2;
                }
                r8 = r9;
                i2 = 1;
            }
        }
        Object obj = updateOp.f1479;
        m1708(updateOp);
        if (i2 > 0) {
            UpdateOp r62 = m1706(updateOp.f1480, r8, i2, obj);
            m1709(r62, i3);
            m1708(r62);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m1691(UpdateOp updateOp) {
        m1689(updateOp);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private int m1692(int i, int i2) {
        int i3;
        int i4;
        for (int size = this.f1473.size() - 1; size >= 0; size--) {
            UpdateOp updateOp = this.f1473.get(size);
            if (updateOp.f1480 == 8) {
                if (updateOp.f1477 < updateOp.f1478) {
                    i3 = updateOp.f1477;
                    i4 = updateOp.f1478;
                } else {
                    i3 = updateOp.f1478;
                    i4 = updateOp.f1477;
                }
                if (i < i3 || i > i4) {
                    if (i < updateOp.f1477) {
                        if (i2 == 1) {
                            updateOp.f1477++;
                            updateOp.f1478++;
                        } else if (i2 == 2) {
                            updateOp.f1477--;
                            updateOp.f1478--;
                        }
                    }
                } else if (i3 == updateOp.f1477) {
                    if (i2 == 1) {
                        updateOp.f1478++;
                    } else if (i2 == 2) {
                        updateOp.f1478--;
                    }
                    i++;
                } else {
                    if (i2 == 1) {
                        updateOp.f1477++;
                    } else if (i2 == 2) {
                        updateOp.f1477--;
                    }
                    i--;
                }
            } else if (updateOp.f1477 <= i) {
                if (updateOp.f1480 == 1) {
                    i -= updateOp.f1478;
                } else if (updateOp.f1480 == 2) {
                    i += updateOp.f1478;
                }
            } else if (i2 == 1) {
                updateOp.f1477++;
            } else if (i2 == 2) {
                updateOp.f1477--;
            }
        }
        for (int size2 = this.f1473.size() - 1; size2 >= 0; size2--) {
            UpdateOp updateOp2 = this.f1473.get(size2);
            if (updateOp2.f1480 == 8) {
                if (updateOp2.f1478 == updateOp2.f1477 || updateOp2.f1478 < 0) {
                    this.f1473.remove(size2);
                    m1708(updateOp2);
                }
            } else if (updateOp2.f1478 <= 0) {
                this.f1473.remove(size2);
                m1708(updateOp2);
            }
        }
        return i;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m1693(UpdateOp updateOp) {
        int i = updateOp.f1477;
        int i2 = 0;
        int i3 = updateOp.f1477 + updateOp.f1478;
        char c = 65535;
        for (int i4 = updateOp.f1477; i4 < i3; i4++) {
            if (this.f1475.m1718(i4) != null || m1694(i4)) {
                if (c == 0) {
                    m1690(m1706(4, i, i2, updateOp.f1479));
                    i2 = 0;
                    i = i4;
                }
                c = 1;
            } else {
                if (c == 1) {
                    m1689(m1706(4, i, i2, updateOp.f1479));
                    i2 = 0;
                    i = i4;
                }
                c = 0;
            }
            i2++;
        }
        if (i2 != updateOp.f1478) {
            Object obj = updateOp.f1479;
            m1708(updateOp);
            updateOp = m1706(4, i, i2, obj);
        }
        if (c == 0) {
            m1690(updateOp);
        } else {
            m1689(updateOp);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m1694(int i) {
        int size = this.f1473.size();
        for (int i2 = 0; i2 < size; i2++) {
            UpdateOp updateOp = this.f1473.get(i2);
            if (updateOp.f1480 == 8) {
                if (m1705(updateOp.f1478, i2 + 1) == i) {
                    return true;
                }
            } else if (updateOp.f1480 == 1) {
                int i3 = updateOp.f1477 + updateOp.f1478;
                for (int i4 = updateOp.f1477; i4 < i3; i4++) {
                    if (m1705(i4, i2 + 1) == i) {
                        return true;
                    }
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m1695(UpdateOp updateOp) {
        int i = updateOp.f1477;
        int i2 = 0;
        int i3 = updateOp.f1477 + updateOp.f1478;
        char c = 65535;
        int i4 = updateOp.f1477;
        while (i4 < i3) {
            boolean z = false;
            if (this.f1475.m1718(i4) != null || m1694(i4)) {
                if (c == 0) {
                    m1690(m1706(2, i, i2, (Object) null));
                    z = true;
                }
                c = 1;
            } else {
                if (c == 1) {
                    m1689(m1706(2, i, i2, (Object) null));
                    z = true;
                }
                c = 0;
            }
            if (z) {
                i4 -= i2;
                i3 -= i2;
                i2 = 1;
            } else {
                i2++;
            }
            i4++;
        }
        if (i2 != updateOp.f1478) {
            m1708(updateOp);
            updateOp = m1706(2, i, i2, (Object) null);
        }
        if (c == 0) {
            m1690(updateOp);
        } else {
            m1689(updateOp);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1696() {
        return !this.f1473.isEmpty() && !this.f1476.isEmpty();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m1697() {
        m1703();
        int size = this.f1476.size();
        for (int i = 0; i < size; i++) {
            UpdateOp updateOp = this.f1476.get(i);
            switch (updateOp.f1480) {
                case 1:
                    this.f1475.m1715(updateOp);
                    this.f1475.m1717(updateOp.f1477, updateOp.f1478);
                    break;
                case 2:
                    this.f1475.m1715(updateOp);
                    this.f1475.m1719(updateOp.f1477, updateOp.f1478);
                    break;
                case 4:
                    this.f1475.m1715(updateOp);
                    this.f1475.m1720(updateOp.f1477, updateOp.f1478, updateOp.f1479);
                    break;
                case 8:
                    this.f1475.m1715(updateOp);
                    this.f1475.m1716(updateOp.f1477, updateOp.f1478);
                    break;
            }
            if (this.f1474 != null) {
                this.f1474.run();
            }
        }
        m1710((List<UpdateOp>) this.f1476);
        this.f1471 = 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m1698(int i) {
        return m1705(i, 0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1699() {
        this.f1469.m1986(this.f1476);
        int size = this.f1476.size();
        for (int i = 0; i < size; i++) {
            UpdateOp updateOp = this.f1476.get(i);
            switch (updateOp.f1480) {
                case 1:
                    m1688(updateOp);
                    break;
                case 2:
                    m1695(updateOp);
                    break;
                case 4:
                    m1693(updateOp);
                    break;
                case 8:
                    m1691(updateOp);
                    break;
            }
            if (this.f1474 != null) {
                this.f1474.run();
            }
        }
        this.f1476.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m1700(int i, int i2) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.f1476.add(m1706(1, i, i2, (Object) null));
        this.f1471 |= 1;
        if (this.f1476.size() != 1) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m1701() {
        return this.f1476.size() > 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m1702(int i) {
        int size = this.f1476.size();
        for (int i2 = 0; i2 < size; i2++) {
            UpdateOp updateOp = this.f1476.get(i2);
            switch (updateOp.f1480) {
                case 1:
                    if (updateOp.f1477 > i) {
                        break;
                    } else {
                        i += updateOp.f1478;
                        break;
                    }
                case 2:
                    if (updateOp.f1477 <= i) {
                        if (updateOp.f1477 + updateOp.f1478 <= i) {
                            i -= updateOp.f1478;
                            break;
                        } else {
                            return -1;
                        }
                    } else {
                        continue;
                    }
                case 8:
                    if (updateOp.f1477 != i) {
                        if (updateOp.f1477 < i) {
                            i--;
                        }
                        if (updateOp.f1478 > i) {
                            break;
                        } else {
                            i++;
                            break;
                        }
                    } else {
                        i = updateOp.f1478;
                        break;
                    }
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m1703() {
        int size = this.f1473.size();
        for (int i = 0; i < size; i++) {
            this.f1475.m1715(this.f1473.get(i));
        }
        m1710((List<UpdateOp>) this.f1473);
        this.f1471 = 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m1704(int i, int i2) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.f1476.add(m1706(2, i, i2, (Object) null));
        this.f1471 |= 2;
        if (this.f1476.size() != 1) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m1705(int i, int i2) {
        int size = this.f1473.size();
        for (int i3 = i2; i3 < size; i3++) {
            UpdateOp updateOp = this.f1473.get(i3);
            if (updateOp.f1480 == 8) {
                if (updateOp.f1477 == i) {
                    i = updateOp.f1478;
                } else {
                    if (updateOp.f1477 < i) {
                        i--;
                    }
                    if (updateOp.f1478 <= i) {
                        i++;
                    }
                }
            } else if (updateOp.f1477 > i) {
                continue;
            } else if (updateOp.f1480 == 2) {
                if (i < updateOp.f1477 + updateOp.f1478) {
                    return -1;
                }
                i -= updateOp.f1478;
            } else if (updateOp.f1480 == 1) {
                i += updateOp.f1478;
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public UpdateOp m1706(int i, int i2, int i3, Object obj) {
        UpdateOp acquire = this.f1470.acquire();
        if (acquire == null) {
            return new UpdateOp(i, i2, i3, obj);
        }
        acquire.f1480 = i;
        acquire.f1477 = i2;
        acquire.f1478 = i3;
        acquire.f1479 = obj;
        return acquire;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1707() {
        m1710((List<UpdateOp>) this.f1476);
        m1710((List<UpdateOp>) this.f1473);
        this.f1471 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1708(UpdateOp updateOp) {
        if (!this.f1472) {
            updateOp.f1479 = null;
            this.f1470.release(updateOp);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1709(UpdateOp updateOp, int i) {
        this.f1475.m1721(updateOp);
        switch (updateOp.f1480) {
            case 2:
                this.f1475.m1719(i, updateOp.f1478);
                return;
            case 4:
                this.f1475.m1720(i, updateOp.f1478, updateOp.f1479);
                return;
            default:
                throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1710(List<UpdateOp> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            m1708(list.get(i));
        }
        list.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1711(int i) {
        return (this.f1471 & i) != 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1712(int i, int i2, int i3) {
        boolean z = true;
        if (i == i2) {
            return false;
        }
        if (i3 != 1) {
            throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
        }
        this.f1476.add(m1706(8, i, i2, (Object) null));
        this.f1471 |= 8;
        if (this.f1476.size() != 1) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1713(int i, int i2, Object obj) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.f1476.add(m1706(4, i, i2, obj));
        this.f1471 |= 4;
        if (this.f1476.size() != 1) {
            z = false;
        }
        return z;
    }
}
