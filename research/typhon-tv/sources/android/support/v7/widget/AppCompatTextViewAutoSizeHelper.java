package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.RestrictTo;
import android.support.v7.appcompat.R;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;

class AppCompatTextViewAutoSizeHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private static Hashtable<String, Method> f1530 = new Hashtable<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final RectF f1531 = new RectF();

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f1532 = -1.0f;

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f1533 = -1.0f;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int[] f1534 = new int[0];

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Context f1535;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f1536 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private TextPaint f1537;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final TextView f1538;

    /* renamed from: 连任  reason: contains not printable characters */
    private float f1539 = -1.0f;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f1540 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f1541 = 0;

    AppCompatTextViewAutoSizeHelper(TextView textView) {
        this.f1538 = textView;
        this.f1535 = this.f1538.getContext();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m1792() {
        int length = this.f1534.length;
        this.f1536 = length > 0;
        if (this.f1536) {
            this.f1541 = 1;
            this.f1532 = (float) this.f1534[0];
            this.f1533 = (float) this.f1534[length - 1];
            this.f1539 = -1.0f;
        }
        return this.f1536;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean m1793() {
        if (!m1795() || this.f1541 != 1) {
            this.f1540 = false;
        } else {
            if (!this.f1536 || this.f1534.length == 0) {
                int i = 1;
                float round = (float) Math.round(this.f1532);
                while (Math.round(this.f1539 + round) <= Math.round(this.f1533)) {
                    i++;
                    round += this.f1539;
                }
                int[] iArr = new int[i];
                float f = this.f1532;
                for (int i2 = 0; i2 < i; i2++) {
                    iArr[i2] = Math.round(f);
                    f += this.f1539;
                }
                this.f1534 = m1805(iArr);
            }
            this.f1540 = true;
        }
        return this.f1540;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m1794() {
        this.f1541 = 0;
        this.f1532 = -1.0f;
        this.f1533 = -1.0f;
        this.f1539 = -1.0f;
        this.f1534 = new int[0];
        this.f1540 = false;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean m1795() {
        return !(this.f1538 instanceof AppCompatEditText);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m1796(RectF rectF) {
        int length = this.f1534.length;
        if (length == 0) {
            throw new IllegalStateException("No available text sizes to choose from.");
        }
        int i = 0;
        int i2 = 0 + 1;
        int i3 = length - 1;
        while (i2 <= i3) {
            int i4 = (i2 + i3) / 2;
            if (m1804(this.f1534[i4], rectF)) {
                i = i2;
                i2 = i4 + 1;
            } else {
                i3 = i4 - 1;
                i = i3;
            }
        }
        return this.f1534[i];
    }

    @TargetApi(14)
    /* renamed from: 龘  reason: contains not printable characters */
    private StaticLayout m1797(CharSequence charSequence, Layout.Alignment alignment, int i) {
        float floatValue;
        float floatValue2;
        boolean booleanValue;
        if (Build.VERSION.SDK_INT >= 16) {
            floatValue = this.f1538.getLineSpacingMultiplier();
            floatValue2 = this.f1538.getLineSpacingExtra();
            booleanValue = this.f1538.getIncludeFontPadding();
        } else {
            floatValue = ((Float) m1799((Object) this.f1538, "getLineSpacingMultiplier", Float.valueOf(1.0f))).floatValue();
            floatValue2 = ((Float) m1799((Object) this.f1538, "getLineSpacingExtra", Float.valueOf(0.0f))).floatValue();
            booleanValue = ((Boolean) m1799((Object) this.f1538, "getIncludeFontPadding", true)).booleanValue();
        }
        return new StaticLayout(charSequence, this.f1537, i, alignment, floatValue, floatValue2, booleanValue);
    }

    @TargetApi(23)
    /* renamed from: 龘  reason: contains not printable characters */
    private StaticLayout m1798(CharSequence charSequence, Layout.Alignment alignment, int i, int i2) {
        TextDirectionHeuristic textDirectionHeuristic = (TextDirectionHeuristic) m1799((Object) this.f1538, "getTextDirectionHeuristic", TextDirectionHeuristics.FIRSTSTRONG_LTR);
        StaticLayout.Builder hyphenationFrequency = StaticLayout.Builder.obtain(charSequence, 0, charSequence.length(), this.f1537, i).setAlignment(alignment).setLineSpacing(this.f1538.getLineSpacingExtra(), this.f1538.getLineSpacingMultiplier()).setIncludePad(this.f1538.getIncludeFontPadding()).setBreakStrategy(this.f1538.getBreakStrategy()).setHyphenationFrequency(this.f1538.getHyphenationFrequency());
        if (i2 == -1) {
            i2 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        }
        return hyphenationFrequency.setMaxLines(i2).setTextDirection(textDirectionHeuristic).build();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private <T> T m1799(Object obj, String str, T t) {
        try {
            T invoke = m1800(str).invoke(obj, new Object[0]);
            return (invoke != null || 0 == 0) ? invoke : t;
        } catch (Exception e) {
            Log.w("ACTVAutoSizeHelper", "Failed to invoke TextView#" + str + "() method", e);
            if (0 != 0 || 1 == 0) {
                return null;
            }
            return t;
        } catch (Throwable th) {
            if (0 == 0 && 1 != 0) {
                T t2 = t;
            }
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Method m1800(String str) {
        try {
            Method method = f1530.get(str);
            if (method != null) {
                return method;
            }
            Method declaredMethod = TextView.class.getDeclaredMethod(str, new Class[0]);
            if (declaredMethod == null) {
                return declaredMethod;
            }
            declaredMethod.setAccessible(true);
            f1530.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            Log.w("ACTVAutoSizeHelper", "Failed to retrieve TextView#" + str + "() method", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1801(float f) {
        if (f != this.f1538.getPaint().getTextSize()) {
            this.f1538.getPaint().setTextSize(f);
            boolean z = false;
            if (Build.VERSION.SDK_INT >= 18) {
                z = this.f1538.isInLayout();
            }
            if (this.f1538.getLayout() != null) {
                this.f1540 = false;
                try {
                    Method r2 = m1800("nullLayouts");
                    if (r2 != null) {
                        r2.invoke(this.f1538, new Object[0]);
                    }
                } catch (Exception e) {
                    Log.w("ACTVAutoSizeHelper", "Failed to invoke TextView#nullLayouts() method", e);
                }
                if (!z) {
                    this.f1538.requestLayout();
                } else {
                    this.f1538.forceLayout();
                }
                this.f1538.invalidate();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1802(float f, float f2, float f3) throws IllegalArgumentException {
        if (f <= 0.0f) {
            throw new IllegalArgumentException("Minimum auto-size text size (" + f + "px) is less or equal to (0px)");
        } else if (f2 <= f) {
            throw new IllegalArgumentException("Maximum auto-size text size (" + f2 + "px) is less or equal to minimum auto-size " + "text size (" + f + "px)");
        } else if (f3 <= 0.0f) {
            throw new IllegalArgumentException("The auto-size step granularity (" + f3 + "px) is less or equal to (0px)");
        } else {
            this.f1541 = 1;
            this.f1532 = f;
            this.f1533 = f2;
            this.f1539 = f3;
            this.f1536 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1803(TypedArray typedArray) {
        int length = typedArray.length();
        int[] iArr = new int[length];
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                iArr[i] = typedArray.getDimensionPixelSize(i, -1);
            }
            this.f1534 = m1805(iArr);
            m1792();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m1804(int i, RectF rectF) {
        CharSequence transformation;
        CharSequence text = this.f1538.getText();
        TransformationMethod transformationMethod = this.f1538.getTransformationMethod();
        if (!(transformationMethod == null || (transformation = transformationMethod.getTransformation(text, this.f1538)) == null)) {
            text = transformation;
        }
        int maxLines = Build.VERSION.SDK_INT >= 16 ? this.f1538.getMaxLines() : -1;
        if (this.f1537 == null) {
            this.f1537 = new TextPaint();
        } else {
            this.f1537.reset();
        }
        this.f1537.set(this.f1538.getPaint());
        this.f1537.setTextSize((float) i);
        Layout.Alignment alignment = (Layout.Alignment) m1799((Object) this.f1538, "getLayoutAlignment", Layout.Alignment.ALIGN_NORMAL);
        StaticLayout r1 = Build.VERSION.SDK_INT >= 23 ? m1798(text, alignment, Math.round(rectF.right), maxLines) : m1797(text, alignment, Math.round(rectF.right));
        if (maxLines == -1 || (r1.getLineCount() <= maxLines && r1.getLineEnd(r1.getLineCount() - 1) == text.length())) {
            return ((float) r1.getHeight()) <= rectF.bottom;
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int[] m1805(int[] iArr) {
        if (r3 == 0) {
            return iArr;
        }
        Arrays.sort(iArr);
        ArrayList arrayList = new ArrayList();
        for (int i : iArr) {
            if (i > 0 && Collections.binarySearch(arrayList, Integer.valueOf(i)) < 0) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        if (r3 == arrayList.size()) {
            return iArr;
        }
        int size = arrayList.size();
        int[] iArr2 = new int[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr2[i2] = ((Integer) arrayList.get(i2)).intValue();
        }
        return iArr2;
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1806() {
        if (m1807()) {
            if (this.f1540) {
                if (this.f1538.getMeasuredHeight() > 0 && this.f1538.getMeasuredWidth() > 0) {
                    int measuredWidth = ((Boolean) m1799((Object) this.f1538, "getHorizontallyScrolling", false)).booleanValue() ? 1048576 : (this.f1538.getMeasuredWidth() - this.f1538.getTotalPaddingLeft()) - this.f1538.getTotalPaddingRight();
                    int height = (this.f1538.getHeight() - this.f1538.getCompoundPaddingBottom()) - this.f1538.getCompoundPaddingTop();
                    if (measuredWidth > 0 && height > 0) {
                        synchronized (f1531) {
                            f1531.setEmpty();
                            f1531.right = (float) measuredWidth;
                            f1531.bottom = (float) height;
                            float r3 = (float) m1796(f1531);
                            if (r3 != this.f1538.getTextSize()) {
                                m1814(0, r3);
                            }
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            this.f1540 = true;
        }
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1807() {
        return m1795() && this.f1541 != 0;
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 连任  reason: contains not printable characters */
    public int[] m1808() {
        return this.f1534;
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 靐  reason: contains not printable characters */
    public int m1809() {
        return Math.round(this.f1539);
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 麤  reason: contains not printable characters */
    public int m1810() {
        return Math.round(this.f1533);
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 齉  reason: contains not printable characters */
    public int m1811() {
        return Math.round(this.f1532);
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public int m1812() {
        return this.f1541;
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1813(int i) {
        if (m1795()) {
            switch (i) {
                case 0:
                    m1794();
                    return;
                case 1:
                    DisplayMetrics displayMetrics = this.f1535.getResources().getDisplayMetrics();
                    m1802(TypedValue.applyDimension(2, 12.0f, displayMetrics), TypedValue.applyDimension(2, 112.0f, displayMetrics), 1.0f);
                    if (m1793()) {
                        m1806();
                        return;
                    }
                    return;
                default:
                    throw new IllegalArgumentException("Unknown auto-size text type: " + i);
            }
        }
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1814(int i, float f) {
        m1801(TypedValue.applyDimension(i, f, (this.f1535 == null ? Resources.getSystem() : this.f1535.getResources()).getDisplayMetrics()));
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1815(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        if (m1795()) {
            DisplayMetrics displayMetrics = this.f1535.getResources().getDisplayMetrics();
            m1802(TypedValue.applyDimension(i4, (float) i, displayMetrics), TypedValue.applyDimension(i4, (float) i2, displayMetrics), TypedValue.applyDimension(i4, (float) i3, displayMetrics));
            if (m1793()) {
                m1806();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1816(AttributeSet attributeSet, int i) {
        int resourceId;
        float f = -1.0f;
        float f2 = -1.0f;
        float f3 = -1.0f;
        TypedArray obtainStyledAttributes = this.f1535.obtainStyledAttributes(attributeSet, R.styleable.AppCompatTextView, i, 0);
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextView_autoSizeTextType)) {
            this.f1541 = obtainStyledAttributes.getInt(R.styleable.AppCompatTextView_autoSizeTextType, 0);
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextView_autoSizeStepGranularity)) {
            f3 = obtainStyledAttributes.getDimension(R.styleable.AppCompatTextView_autoSizeStepGranularity, -1.0f);
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextView_autoSizeMinTextSize)) {
            f = obtainStyledAttributes.getDimension(R.styleable.AppCompatTextView_autoSizeMinTextSize, -1.0f);
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextView_autoSizeMaxTextSize)) {
            f2 = obtainStyledAttributes.getDimension(R.styleable.AppCompatTextView_autoSizeMaxTextSize, -1.0f);
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextView_autoSizePresetSizes) && (resourceId = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextView_autoSizePresetSizes, 0)) > 0) {
            TypedArray obtainTypedArray = obtainStyledAttributes.getResources().obtainTypedArray(resourceId);
            m1803(obtainTypedArray);
            obtainTypedArray.recycle();
        }
        obtainStyledAttributes.recycle();
        if (!m1795()) {
            this.f1541 = 0;
        } else if (this.f1541 == 1) {
            if (!this.f1536) {
                DisplayMetrics displayMetrics = this.f1535.getResources().getDisplayMetrics();
                if (f == -1.0f) {
                    f = TypedValue.applyDimension(2, 12.0f, displayMetrics);
                }
                if (f2 == -1.0f) {
                    f2 = TypedValue.applyDimension(2, 112.0f, displayMetrics);
                }
                if (f3 == -1.0f) {
                    f3 = 1.0f;
                }
                m1802(f, f2, f3);
            }
            m1793();
        }
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1817(int[] iArr, int i) throws IllegalArgumentException {
        if (m1795()) {
            int length = iArr.length;
            if (length > 0) {
                int[] iArr2 = new int[length];
                if (i == 0) {
                    iArr2 = Arrays.copyOf(iArr, length);
                } else {
                    DisplayMetrics displayMetrics = this.f1535.getResources().getDisplayMetrics();
                    for (int i2 = 0; i2 < length; i2++) {
                        iArr2[i2] = Math.round(TypedValue.applyDimension(i, (float) iArr[i2], displayMetrics));
                    }
                }
                this.f1534 = m1805(iArr2);
                if (!m1792()) {
                    throw new IllegalArgumentException("None of the preset sizes is valid: " + Arrays.toString(iArr));
                }
            } else {
                this.f1536 = false;
            }
            if (m1793()) {
                m1806();
            }
        }
    }
}
