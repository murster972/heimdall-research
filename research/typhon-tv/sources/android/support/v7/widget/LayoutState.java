package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

class LayoutState {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f1644 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f1645 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f1646;

    /* renamed from: ˑ  reason: contains not printable characters */
    boolean f1647;

    /* renamed from: 连任  reason: contains not printable characters */
    int f1648;

    /* renamed from: 靐  reason: contains not printable characters */
    int f1649;

    /* renamed from: 麤  reason: contains not printable characters */
    int f1650;

    /* renamed from: 齉  reason: contains not printable characters */
    int f1651;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean f1652 = true;

    LayoutState() {
    }

    public String toString() {
        return "LayoutState{mAvailable=" + this.f1649 + ", mCurrentPosition=" + this.f1651 + ", mItemDirection=" + this.f1650 + ", mLayoutDirection=" + this.f1648 + ", mStartLine=" + this.f1644 + ", mEndLine=" + this.f1645 + '}';
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public View m1965(RecyclerView.Recycler recycler) {
        View viewForPosition = recycler.getViewForPosition(this.f1651);
        this.f1651 += this.f1650;
        return viewForPosition;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1966(RecyclerView.State state) {
        return this.f1651 >= 0 && this.f1651 < state.getItemCount();
    }
}
