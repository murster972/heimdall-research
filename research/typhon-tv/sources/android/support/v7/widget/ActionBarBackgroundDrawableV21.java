package android.support.v7.widget;

import android.graphics.Outline;

class ActionBarBackgroundDrawableV21 extends ActionBarBackgroundDrawable {
    public ActionBarBackgroundDrawableV21(ActionBarContainer actionBarContainer) {
        super(actionBarContainer);
    }

    public void getOutline(Outline outline) {
        if (this.f1400.mIsSplit) {
            if (this.f1400.mSplitBackground != null) {
                this.f1400.mSplitBackground.getOutline(outline);
            }
        } else if (this.f1400.mBackground != null) {
            this.f1400.mBackground.getOutline(outline);
        }
    }
}
