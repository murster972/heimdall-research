package android.support.v7.widget;

import android.support.v4.os.TraceCompat;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

final class GapWorker implements Runnable {

    /* renamed from: 连任  reason: contains not printable characters */
    static Comparator<Task> f1629 = new Comparator<Task>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(Task task, Task task2) {
            int i = -1;
            if ((task.f1641 == null) != (task2.f1641 == null)) {
                return task.f1641 == null ? 1 : -1;
            }
            if (task.f1643 != task2.f1643) {
                if (!task.f1643) {
                    i = 1;
                }
                return i;
            }
            int i2 = task2.f1640 - task.f1640;
            if (i2 != 0) {
                return i2;
            }
            int i3 = task.f1642 - task2.f1642;
            if (i3 != 0) {
                return i3;
            }
            return 0;
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    static final ThreadLocal<GapWorker> f1630 = new ThreadLocal<>();

    /* renamed from: ʻ  reason: contains not printable characters */
    private ArrayList<Task> f1631 = new ArrayList<>();

    /* renamed from: 靐  reason: contains not printable characters */
    ArrayList<RecyclerView> f1632 = new ArrayList<>();

    /* renamed from: 麤  reason: contains not printable characters */
    long f1633;

    /* renamed from: 齉  reason: contains not printable characters */
    long f1634;

    static class LayoutPrefetchRegistryImpl implements RecyclerView.LayoutManager.LayoutPrefetchRegistry {

        /* renamed from: 靐  reason: contains not printable characters */
        int f1635;

        /* renamed from: 麤  reason: contains not printable characters */
        int f1636;

        /* renamed from: 齉  reason: contains not printable characters */
        int[] f1637;

        /* renamed from: 龘  reason: contains not printable characters */
        int f1638;

        LayoutPrefetchRegistryImpl() {
        }

        public void addPosition(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i2 < 0) {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            } else {
                int i3 = this.f1636 * 2;
                if (this.f1637 == null) {
                    this.f1637 = new int[4];
                    Arrays.fill(this.f1637, -1);
                } else if (i3 >= this.f1637.length) {
                    int[] iArr = this.f1637;
                    this.f1637 = new int[(i3 * 2)];
                    System.arraycopy(iArr, 0, this.f1637, 0, iArr.length);
                }
                this.f1637[i3] = i;
                this.f1637[i3 + 1] = i2;
                this.f1636++;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1960() {
            if (this.f1637 != null) {
                Arrays.fill(this.f1637, -1);
            }
            this.f1636 = 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1961(int i, int i2) {
            this.f1638 = i;
            this.f1635 = i2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1962(RecyclerView recyclerView, boolean z) {
            this.f1636 = 0;
            if (this.f1637 != null) {
                Arrays.fill(this.f1637, -1);
            }
            RecyclerView.LayoutManager layoutManager = recyclerView.mLayout;
            if (recyclerView.mAdapter != null && layoutManager != null && layoutManager.isItemPrefetchEnabled()) {
                if (z) {
                    if (!recyclerView.mAdapterHelper.m1701()) {
                        layoutManager.collectInitialPrefetchPositions(recyclerView.mAdapter.getItemCount(), this);
                    }
                } else if (!recyclerView.hasPendingAdapterUpdates()) {
                    layoutManager.collectAdjacentPrefetchPositions(this.f1638, this.f1635, recyclerView.mState, this);
                }
                if (this.f1636 > layoutManager.mPrefetchMaxCountObserved) {
                    layoutManager.mPrefetchMaxCountObserved = this.f1636;
                    layoutManager.mPrefetchMaxObservedInInitialPrefetch = z;
                    recyclerView.mRecycler.updateViewCacheSize();
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1963(int i) {
            if (this.f1637 != null) {
                int i2 = this.f1636 * 2;
                for (int i3 = 0; i3 < i2; i3 += 2) {
                    if (this.f1637[i3] == i) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    static class Task {

        /* renamed from: 连任  reason: contains not printable characters */
        public int f1639;

        /* renamed from: 靐  reason: contains not printable characters */
        public int f1640;

        /* renamed from: 麤  reason: contains not printable characters */
        public RecyclerView f1641;

        /* renamed from: 齉  reason: contains not printable characters */
        public int f1642;

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean f1643;

        Task() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1964() {
            this.f1643 = false;
            this.f1640 = 0;
            this.f1642 = 0;
            this.f1641 = null;
            this.f1639 = 0;
        }
    }

    GapWorker() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m1949(long j) {
        int i = 0;
        while (i < this.f1631.size()) {
            Task task = this.f1631.get(i);
            if (task.f1641 != null) {
                m1952(task, j);
                task.m1964();
                i++;
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private RecyclerView.ViewHolder m1950(RecyclerView recyclerView, int i, long j) {
        if (m1954(recyclerView, i)) {
            return null;
        }
        RecyclerView.Recycler recycler = recyclerView.mRecycler;
        try {
            recyclerView.onEnterLayoutOrScroll();
            RecyclerView.ViewHolder tryGetViewHolderForPositionByDeadline = recycler.tryGetViewHolderForPositionByDeadline(i, false, j);
            if (tryGetViewHolderForPositionByDeadline != null) {
                if (!tryGetViewHolderForPositionByDeadline.isBound() || tryGetViewHolderForPositionByDeadline.isInvalid()) {
                    recycler.addViewHolderToRecycledViewPool(tryGetViewHolderForPositionByDeadline, false);
                } else {
                    recycler.recycleView(tryGetViewHolderForPositionByDeadline.itemView);
                }
            }
            return tryGetViewHolderForPositionByDeadline;
        } finally {
            recyclerView.onExitLayoutOrScroll(false);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1951() {
        Task task;
        int size = this.f1632.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            RecyclerView recyclerView = this.f1632.get(i2);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.mPrefetchRegistry.m1962(recyclerView, false);
                i += recyclerView.mPrefetchRegistry.f1636;
            }
        }
        this.f1631.ensureCapacity(i);
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            RecyclerView recyclerView2 = this.f1632.get(i4);
            if (recyclerView2.getWindowVisibility() == 0) {
                LayoutPrefetchRegistryImpl layoutPrefetchRegistryImpl = recyclerView2.mPrefetchRegistry;
                int abs = Math.abs(layoutPrefetchRegistryImpl.f1638) + Math.abs(layoutPrefetchRegistryImpl.f1635);
                for (int i5 = 0; i5 < layoutPrefetchRegistryImpl.f1636 * 2; i5 += 2) {
                    if (i3 >= this.f1631.size()) {
                        task = new Task();
                        this.f1631.add(task);
                    } else {
                        task = this.f1631.get(i3);
                    }
                    int i6 = layoutPrefetchRegistryImpl.f1637[i5 + 1];
                    task.f1643 = i6 <= abs;
                    task.f1640 = abs;
                    task.f1642 = i6;
                    task.f1641 = recyclerView2;
                    task.f1639 = layoutPrefetchRegistryImpl.f1637[i5];
                    i3++;
                }
            }
        }
        Collections.sort(this.f1631, f1629);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1952(Task task, long j) {
        RecyclerView.ViewHolder r0 = m1950(task.f1641, task.f1639, task.f1643 ? Long.MAX_VALUE : j);
        if (r0 != null && r0.mNestedRecyclerView != null && r0.isBound() && !r0.isInvalid()) {
            m1953((RecyclerView) r0.mNestedRecyclerView.get(), j);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1953(RecyclerView recyclerView, long j) {
        if (recyclerView != null) {
            if (recyclerView.mDataSetHasChangedAfterLayout && recyclerView.mChildHelper.m1895() != 0) {
                recyclerView.removeAndRecycleViews();
            }
            LayoutPrefetchRegistryImpl layoutPrefetchRegistryImpl = recyclerView.mPrefetchRegistry;
            layoutPrefetchRegistryImpl.m1962(recyclerView, true);
            if (layoutPrefetchRegistryImpl.f1636 != 0) {
                try {
                    TraceCompat.beginSection("RV Nested Prefetch");
                    recyclerView.mState.prepareForNestedPrefetch(recyclerView.mAdapter);
                    for (int i = 0; i < layoutPrefetchRegistryImpl.f1636 * 2; i += 2) {
                        m1950(recyclerView, layoutPrefetchRegistryImpl.f1637[i], j);
                    }
                } finally {
                    TraceCompat.endSection();
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m1954(RecyclerView recyclerView, int i) {
        int r1 = recyclerView.mChildHelper.m1895();
        for (int i2 = 0; i2 < r1; i2++) {
            RecyclerView.ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(recyclerView.mChildHelper.m1893(i2));
            if (childViewHolderInt.mPosition == i && !childViewHolderInt.isInvalid()) {
                return true;
            }
        }
        return false;
    }

    public void run() {
        try {
            TraceCompat.beginSection("RV Prefetch");
            if (!this.f1632.isEmpty()) {
                int size = this.f1632.size();
                long j = 0;
                for (int i = 0; i < size; i++) {
                    RecyclerView recyclerView = this.f1632.get(i);
                    if (recyclerView.getWindowVisibility() == 0) {
                        j = Math.max(recyclerView.getDrawingTime(), j);
                    }
                }
                if (j == 0) {
                    this.f1634 = 0;
                    TraceCompat.endSection();
                    return;
                }
                m1956(TimeUnit.MILLISECONDS.toNanos(j) + this.f1633);
                this.f1634 = 0;
                TraceCompat.endSection();
            }
        } finally {
            this.f1634 = 0;
            TraceCompat.endSection();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1955(RecyclerView recyclerView) {
        boolean remove = this.f1632.remove(recyclerView);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1956(long j) {
        m1951();
        m1949(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1957(RecyclerView recyclerView) {
        this.f1632.add(recyclerView);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1958(RecyclerView recyclerView, int i, int i2) {
        if (recyclerView.isAttachedToWindow() && this.f1634 == 0) {
            this.f1634 = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.mPrefetchRegistry.m1961(i, i2);
    }
}
