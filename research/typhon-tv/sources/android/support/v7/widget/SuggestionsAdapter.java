package android.support.v7.widget;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ResourceCursorAdapter;
import android.support.v7.appcompat.R;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.WeakHashMap;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

class SuggestionsAdapter extends ResourceCursorAdapter implements View.OnClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f1787;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f1788 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f1789 = 1;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f1790 = -1;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f1791 = -1;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f1792 = -1;

    /* renamed from: ˑ  reason: contains not printable characters */
    private ColorStateList f1793;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f1794 = -1;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f1795 = -1;

    /* renamed from: 连任  reason: contains not printable characters */
    private final WeakHashMap<String, Drawable.ConstantState> f1796;

    /* renamed from: 靐  reason: contains not printable characters */
    private final SearchView f1797;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Context f1798;

    /* renamed from: 齉  reason: contains not printable characters */
    private final SearchableInfo f1799;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SearchManager f1800 = ((SearchManager) this.mContext.getSystemService("search"));

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f1801 = -1;

    private static final class ChildViewCache {

        /* renamed from: 连任  reason: contains not printable characters */
        public final ImageView f1802;

        /* renamed from: 靐  reason: contains not printable characters */
        public final TextView f1803;

        /* renamed from: 麤  reason: contains not printable characters */
        public final ImageView f1804;

        /* renamed from: 齉  reason: contains not printable characters */
        public final ImageView f1805;

        /* renamed from: 龘  reason: contains not printable characters */
        public final TextView f1806;

        public ChildViewCache(View view) {
            this.f1806 = (TextView) view.findViewById(16908308);
            this.f1803 = (TextView) view.findViewById(16908309);
            this.f1805 = (ImageView) view.findViewById(16908295);
            this.f1804 = (ImageView) view.findViewById(16908296);
            this.f1802 = (ImageView) view.findViewById(R.id.edit_query);
        }
    }

    public SuggestionsAdapter(Context context, SearchView searchView, SearchableInfo searchableInfo, WeakHashMap<String, Drawable.ConstantState> weakHashMap) {
        super(context, searchView.getSuggestionRowLayout(), (Cursor) null, true);
        this.f1797 = searchView;
        this.f1799 = searchableInfo;
        this.f1787 = searchView.getSuggestionCommitIconResId();
        this.f1798 = context;
        this.f1796 = weakHashMap;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Drawable m2142(ComponentName componentName) {
        PackageManager packageManager = this.mContext.getPackageManager();
        try {
            ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
            int iconResource = activityInfo.getIconResource();
            if (iconResource == 0) {
                return null;
            }
            Drawable drawable = packageManager.getDrawable(componentName.getPackageName(), iconResource, activityInfo.applicationInfo);
            if (drawable != null) {
                return drawable;
            }
            Log.w("SuggestionsAdapter", "Invalid icon resource " + iconResource + " for " + componentName.flattenToShortString());
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("SuggestionsAdapter", e.toString());
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Drawable m2143(Cursor cursor) {
        if (this.f1790 == -1) {
            return null;
        }
        Drawable r0 = m2149(cursor.getString(this.f1790));
        return r0 == null ? m2146(cursor) : r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Drawable m2144(Uri uri) {
        InputStream openInputStream;
        try {
            if ("android.resource".equals(uri.getScheme())) {
                return m2158(uri);
            }
            openInputStream = this.f1798.getContentResolver().openInputStream(uri);
            if (openInputStream == null) {
                throw new FileNotFoundException("Failed to open " + uri);
            }
            Drawable createFromStream = Drawable.createFromStream(openInputStream, (String) null);
            try {
                openInputStream.close();
                return createFromStream;
            } catch (IOException e) {
                Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e);
                return createFromStream;
            }
        } catch (Resources.NotFoundException e2) {
            throw new FileNotFoundException("Resource does not exist: " + uri);
        } catch (FileNotFoundException e3) {
            Log.w("SuggestionsAdapter", "Icon not found: " + uri + ", " + e3.getMessage());
            return null;
        } catch (Throwable th) {
            try {
                openInputStream.close();
            } catch (IOException e4) {
                Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e4);
            }
            throw th;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Drawable m2145(String str) {
        Drawable.ConstantState constantState = this.f1796.get(str);
        if (constantState == null) {
            return null;
        }
        return constantState.newDrawable();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private Drawable m2146(Cursor cursor) {
        Drawable r0 = m2148(this.f1799.getSearchActivity());
        return r0 != null ? r0 : this.mContext.getPackageManager().getDefaultActivityIcon();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private Drawable m2147(Cursor cursor) {
        if (this.f1791 == -1) {
            return null;
        }
        return m2149(cursor.getString(this.f1791));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Drawable m2148(ComponentName componentName) {
        String flattenToShortString = componentName.flattenToShortString();
        if (this.f1796.containsKey(flattenToShortString)) {
            Drawable.ConstantState constantState = this.f1796.get(flattenToShortString);
            if (constantState == null) {
                return null;
            }
            return constantState.newDrawable(this.f1798.getResources());
        }
        Drawable r2 = m2142(componentName);
        this.f1796.put(flattenToShortString, r2 == null ? null : r2.getConstantState());
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Drawable m2149(String str) {
        if (str == null || str.isEmpty() || "0".equals(str)) {
            return null;
        }
        try {
            int parseInt = Integer.parseInt(str);
            String str2 = "android.resource://" + this.f1798.getPackageName() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + parseInt;
            Drawable r0 = m2145(str2);
            if (r0 != null) {
                return r0;
            }
            Drawable drawable = ContextCompat.getDrawable(this.f1798, parseInt);
            m2156(str2, drawable);
            return drawable;
        } catch (NumberFormatException e) {
            Drawable r02 = m2145(str);
            if (r02 != null) {
                return r02;
            }
            Drawable r03 = m2144(Uri.parse(str));
            m2156(str, r03);
            return r03;
        } catch (Resources.NotFoundException e2) {
            Log.w("SuggestionsAdapter", "Icon resource not found: " + str);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private CharSequence m2150(CharSequence charSequence) {
        if (this.f1793 == null) {
            TypedValue typedValue = new TypedValue();
            this.mContext.getTheme().resolveAttribute(R.attr.textColorSearchUrl, typedValue, true);
            this.f1793 = this.mContext.getResources().getColorStateList(typedValue.resourceId);
        }
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new TextAppearanceSpan((String) null, 0, 0, this.f1793, (ColorStateList) null), 0, charSequence.length(), 33);
        return spannableString;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m2151(Cursor cursor, int i) {
        if (i == -1) {
            return null;
        }
        try {
            return cursor.getString(i);
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m2152(Cursor cursor, String str) {
        return m2151(cursor, cursor.getColumnIndex(str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2153(Cursor cursor) {
        Bundle extras = cursor != null ? cursor.getExtras() : null;
        if (extras == null || extras.getBoolean("in_progress")) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2154(ImageView imageView, Drawable drawable, int i) {
        imageView.setImageDrawable(drawable);
        if (drawable == null) {
            imageView.setVisibility(i);
            return;
        }
        imageView.setVisibility(0);
        drawable.setVisible(false, false);
        drawable.setVisible(true, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2155(TextView textView, CharSequence charSequence) {
        textView.setText(charSequence);
        if (TextUtils.isEmpty(charSequence)) {
            textView.setVisibility(8);
        } else {
            textView.setVisibility(0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2156(String str, Drawable drawable) {
        if (drawable != null) {
            this.f1796.put(str, drawable.getConstantState());
        }
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ChildViewCache childViewCache = (ChildViewCache) view.getTag();
        int i = 0;
        if (this.f1801 != -1) {
            i = cursor.getInt(this.f1801);
        }
        if (childViewCache.f1806 != null) {
            m2155(childViewCache.f1806, (CharSequence) m2151(cursor, this.f1794));
        }
        if (childViewCache.f1803 != null) {
            String r2 = m2151(cursor, this.f1792);
            CharSequence r22 = r2 != null ? m2150((CharSequence) r2) : m2151(cursor, this.f1795);
            if (TextUtils.isEmpty(r22)) {
                if (childViewCache.f1806 != null) {
                    childViewCache.f1806.setSingleLine(false);
                    childViewCache.f1806.setMaxLines(2);
                }
            } else if (childViewCache.f1806 != null) {
                childViewCache.f1806.setSingleLine(true);
                childViewCache.f1806.setMaxLines(1);
            }
            m2155(childViewCache.f1803, r22);
        }
        if (childViewCache.f1805 != null) {
            m2154(childViewCache.f1805, m2143(cursor), 4);
        }
        if (childViewCache.f1804 != null) {
            m2154(childViewCache.f1804, m2147(cursor), 8);
        }
        if (this.f1789 == 2 || (this.f1789 == 1 && (i & 1) != 0)) {
            childViewCache.f1802.setVisibility(0);
            childViewCache.f1802.setTag(childViewCache.f1806.getText());
            childViewCache.f1802.setOnClickListener(this);
            return;
        }
        childViewCache.f1802.setVisibility(8);
    }

    public void changeCursor(Cursor cursor) {
        if (this.f1788) {
            Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (cursor != null) {
                cursor.close();
                return;
            }
            return;
        }
        try {
            super.changeCursor(cursor);
            if (cursor != null) {
                this.f1794 = cursor.getColumnIndex("suggest_text_1");
                this.f1795 = cursor.getColumnIndex("suggest_text_2");
                this.f1792 = cursor.getColumnIndex("suggest_text_2_url");
                this.f1790 = cursor.getColumnIndex("suggest_icon_1");
                this.f1791 = cursor.getColumnIndex("suggest_icon_2");
                this.f1801 = cursor.getColumnIndex("suggest_flags");
            }
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "error changing cursor and caching columns", e);
        }
    }

    public CharSequence convertToString(Cursor cursor) {
        String r2;
        String r0;
        if (cursor == null) {
            return null;
        }
        String r1 = m2152(cursor, "suggest_intent_query");
        if (r1 != null) {
            return r1;
        }
        if (this.f1799.shouldRewriteQueryFromData() && (r0 = m2152(cursor, "suggest_intent_data")) != null) {
            return r0;
        }
        if (!this.f1799.shouldRewriteQueryFromText() || (r2 = m2152(cursor, "suggest_text_1")) == null) {
            return null;
        }
        return r2;
    }

    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getDropDownView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View newDropDownView = newDropDownView(this.mContext, this.mCursor, viewGroup);
            if (newDropDownView == null) {
                return newDropDownView;
            }
            ((ChildViewCache) newDropDownView.getTag()).f1806.setText(e.toString());
            return newDropDownView;
        }
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View newView = newView(this.mContext, this.mCursor, viewGroup);
            if (newView == null) {
                return newView;
            }
            ((ChildViewCache) newView.getTag()).f1806.setText(e.toString());
            return newView;
        }
    }

    public boolean hasStableIds() {
        return false;
    }

    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View newView = super.newView(context, cursor, viewGroup);
        newView.setTag(new ChildViewCache(newView));
        ((ImageView) newView.findViewById(R.id.edit_query)).setImageResource(this.f1787);
        return newView;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        m2153(getCursor());
    }

    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        m2153(getCursor());
    }

    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.f1797.onQueryRefine((CharSequence) tag);
        }
    }

    public Cursor runQueryOnBackgroundThread(CharSequence charSequence) {
        String charSequence2 = charSequence == null ? "" : charSequence.toString();
        Cursor cursor = null;
        if (this.f1797.getVisibility() != 0 || this.f1797.getWindowVisibility() != 0) {
            return null;
        }
        try {
            cursor = m2157(this.f1799, charSequence2, 50);
            if (cursor != null) {
                cursor.getCount();
                Cursor cursor2 = cursor;
                return cursor;
            }
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", e);
        }
        Cursor cursor3 = cursor;
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Cursor m2157(SearchableInfo searchableInfo, String str, int i) {
        String suggestAuthority;
        if (searchableInfo == null || (suggestAuthority = searchableInfo.getSuggestAuthority()) == null) {
            return null;
        }
        Uri.Builder fragment = new Uri.Builder().scheme("content").authority(suggestAuthority).query("").fragment("");
        String suggestPath = searchableInfo.getSuggestPath();
        if (suggestPath != null) {
            fragment.appendEncodedPath(suggestPath);
        }
        fragment.appendPath("search_suggest_query");
        String suggestSelection = searchableInfo.getSuggestSelection();
        String[] strArr = null;
        if (suggestSelection != null) {
            strArr = new String[]{str};
        } else {
            fragment.appendPath(str);
        }
        if (i > 0) {
            fragment.appendQueryParameter("limit", String.valueOf(i));
        }
        return this.mContext.getContentResolver().query(fragment.build(), (String[]) null, suggestSelection, strArr, (String) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Drawable m2158(Uri uri) throws FileNotFoundException {
        int identifier;
        String authority = uri.getAuthority();
        if (TextUtils.isEmpty(authority)) {
            throw new FileNotFoundException("No authority: " + uri);
        }
        try {
            Resources resourcesForApplication = this.mContext.getPackageManager().getResourcesForApplication(authority);
            List<String> pathSegments = uri.getPathSegments();
            if (pathSegments == null) {
                throw new FileNotFoundException("No path: " + uri);
            }
            int size = pathSegments.size();
            if (size == 1) {
                try {
                    identifier = Integer.parseInt(pathSegments.get(0));
                } catch (NumberFormatException e) {
                    throw new FileNotFoundException("Single path segment is not a resource ID: " + uri);
                }
            } else if (size == 2) {
                identifier = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
            } else {
                throw new FileNotFoundException("More than two path segments: " + uri);
            }
            if (identifier != 0) {
                return resourcesForApplication.getDrawable(identifier);
            }
            throw new FileNotFoundException("No resource found for: " + uri);
        } catch (PackageManager.NameNotFoundException e2) {
            throw new FileNotFoundException("No package found for authority: " + uri);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2159(int i) {
        this.f1789 = i;
    }
}
