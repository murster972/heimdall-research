package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v7.widget.RoundRectDrawableWithShadow;

class CardViewBaseImpl implements CardViewImpl {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final RectF f1545 = new RectF();

    CardViewBaseImpl() {
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private RoundRectDrawableWithShadow m1843(CardViewDelegate cardViewDelegate) {
        return (RoundRectDrawableWithShadow) cardViewDelegate.m1864();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private RoundRectDrawableWithShadow m1845(Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        return new RoundRectDrawableWithShadow(context.getResources(), colorStateList, f, f2, f3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1846(CardViewDelegate cardViewDelegate) {
        Rect rect = new Rect();
        m1843(cardViewDelegate).m2068(rect);
        cardViewDelegate.m1865((int) Math.ceil((double) m1851(cardViewDelegate)), (int) Math.ceil((double) m1854(cardViewDelegate)));
        cardViewDelegate.m1866(rect.left, rect.top, rect.right, rect.bottom);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1847(CardViewDelegate cardViewDelegate) {
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1848(CardViewDelegate cardViewDelegate) {
        m1843(cardViewDelegate).m2069(cardViewDelegate.m1862());
        m1846(cardViewDelegate);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public ColorStateList m1849(CardViewDelegate cardViewDelegate) {
        return m1843(cardViewDelegate).m2058();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public float m1850(CardViewDelegate cardViewDelegate) {
        return m1843(cardViewDelegate).m2060();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public float m1851(CardViewDelegate cardViewDelegate) {
        return m1843(cardViewDelegate).m2062();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1852(CardViewDelegate cardViewDelegate, float f) {
        m1843(cardViewDelegate).m2064(f);
        m1846(cardViewDelegate);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public float m1853(CardViewDelegate cardViewDelegate) {
        return m1843(cardViewDelegate).m2065();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public float m1854(CardViewDelegate cardViewDelegate) {
        return m1843(cardViewDelegate).m2059();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1855(CardViewDelegate cardViewDelegate, float f) {
        m1843(cardViewDelegate).m2061(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public float m1856(CardViewDelegate cardViewDelegate) {
        return m1843(cardViewDelegate).m2063();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1857() {
        RoundRectDrawableWithShadow.f1706 = new RoundRectDrawableWithShadow.RoundRectHelper() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m1861(Canvas canvas, RectF rectF, float f, Paint paint) {
                float f2 = f * 2.0f;
                float width = (rectF.width() - f2) - 1.0f;
                float height = (rectF.height() - f2) - 1.0f;
                if (f >= 1.0f) {
                    float f3 = f + 0.5f;
                    CardViewBaseImpl.this.f1545.set(-f3, -f3, f3, f3);
                    int save = canvas.save();
                    canvas.translate(rectF.left + f3, rectF.top + f3);
                    canvas.drawArc(CardViewBaseImpl.this.f1545, 180.0f, 90.0f, true, paint);
                    canvas.translate(width, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(CardViewBaseImpl.this.f1545, 180.0f, 90.0f, true, paint);
                    canvas.translate(height, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(CardViewBaseImpl.this.f1545, 180.0f, 90.0f, true, paint);
                    canvas.translate(width, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(CardViewBaseImpl.this.f1545, 180.0f, 90.0f, true, paint);
                    canvas.restoreToCount(save);
                    canvas.drawRect((rectF.left + f3) - 1.0f, rectF.top, 1.0f + (rectF.right - f3), rectF.top + f3, paint);
                    canvas.drawRect((rectF.left + f3) - 1.0f, rectF.bottom - f3, 1.0f + (rectF.right - f3), rectF.bottom, paint);
                }
                canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom - f, paint);
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1858(CardViewDelegate cardViewDelegate, float f) {
        m1843(cardViewDelegate).m2066(f);
        m1846(cardViewDelegate);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1859(CardViewDelegate cardViewDelegate, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        RoundRectDrawableWithShadow r6 = m1845(context, colorStateList, f, f2, f3);
        r6.m2069(cardViewDelegate.m1862());
        cardViewDelegate.m1867(r6);
        m1846(cardViewDelegate);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1860(CardViewDelegate cardViewDelegate, ColorStateList colorStateList) {
        m1843(cardViewDelegate).m2067(colorStateList);
    }
}
