package android.support.v7.widget;

class RtlSpacingHelper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f1723 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f1724 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f1725 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f1726 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f1727 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f1728 = Integer.MIN_VALUE;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f1729 = Integer.MIN_VALUE;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f1730 = 0;

    RtlSpacingHelper() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m2071() {
        return this.f1727;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m2072(int i, int i2) {
        this.f1725 = false;
        if (i != Integer.MIN_VALUE) {
            this.f1726 = i;
            this.f1730 = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f1723 = i2;
            this.f1727 = i2;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m2073() {
        return this.f1724 ? this.f1730 : this.f1727;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m2074() {
        return this.f1724 ? this.f1727 : this.f1730;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m2075() {
        return this.f1730;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2076(int i, int i2) {
        this.f1729 = i;
        this.f1728 = i2;
        this.f1725 = true;
        if (this.f1724) {
            if (i2 != Integer.MIN_VALUE) {
                this.f1730 = i2;
            }
            if (i != Integer.MIN_VALUE) {
                this.f1727 = i;
                return;
            }
            return;
        }
        if (i != Integer.MIN_VALUE) {
            this.f1730 = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f1727 = i2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2077(boolean z) {
        if (z != this.f1724) {
            this.f1724 = z;
            if (!this.f1725) {
                this.f1730 = this.f1726;
                this.f1727 = this.f1723;
            } else if (z) {
                this.f1730 = this.f1728 != Integer.MIN_VALUE ? this.f1728 : this.f1726;
                this.f1727 = this.f1729 != Integer.MIN_VALUE ? this.f1729 : this.f1723;
            } else {
                this.f1730 = this.f1729 != Integer.MIN_VALUE ? this.f1729 : this.f1726;
                this.f1727 = this.f1728 != Integer.MIN_VALUE ? this.f1728 : this.f1723;
            }
        }
    }
}
