package android.support.v7.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RestrictTo;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.AutoSizeableTextView;
import android.support.v7.appcompat.R;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.widget.TextView;
import java.lang.ref.WeakReference;

class AppCompatTextHelper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final AppCompatTextViewAutoSizeHelper f1517;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f1518 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Typeface f1519;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f1520;

    /* renamed from: 连任  reason: contains not printable characters */
    private TintInfo f1521;

    /* renamed from: 靐  reason: contains not printable characters */
    private TintInfo f1522;

    /* renamed from: 麤  reason: contains not printable characters */
    private TintInfo f1523;

    /* renamed from: 齉  reason: contains not printable characters */
    private TintInfo f1524;

    /* renamed from: 龘  reason: contains not printable characters */
    final TextView f1525;

    AppCompatTextHelper(TextView textView) {
        this.f1525 = textView;
        this.f1517 = new AppCompatTextViewAutoSizeHelper(this.f1525);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m1767(int i, float f) {
        this.f1517.m1814(i, f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static AppCompatTextHelper m1768(TextView textView) {
        return Build.VERSION.SDK_INT >= 17 ? new AppCompatTextHelperV17(textView) : new AppCompatTextHelper(textView);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static TintInfo m1769(Context context, AppCompatDrawableManager appCompatDrawableManager, int i) {
        ColorStateList tintList = appCompatDrawableManager.getTintList(context, i);
        if (tintList == null) {
            return null;
        }
        TintInfo tintInfo = new TintInfo();
        tintInfo.f1818 = true;
        tintInfo.f1820 = tintList;
        return tintInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1770(Context context, TintTypedArray tintTypedArray) {
        String string;
        boolean z = true;
        this.f1518 = tintTypedArray.getInt(R.styleable.TextAppearance_android_textStyle, this.f1518);
        if (tintTypedArray.hasValue(R.styleable.TextAppearance_android_fontFamily) || tintTypedArray.hasValue(R.styleable.TextAppearance_fontFamily)) {
            this.f1519 = null;
            int i = tintTypedArray.hasValue(R.styleable.TextAppearance_fontFamily) ? R.styleable.TextAppearance_fontFamily : R.styleable.TextAppearance_android_fontFamily;
            if (!context.isRestricted()) {
                final WeakReference weakReference = new WeakReference(this.f1525);
                try {
                    this.f1519 = tintTypedArray.getFont(i, this.f1518, new ResourcesCompat.FontCallback() {
                        public void onFontRetrievalFailed(int i) {
                        }

                        public void onFontRetrieved(Typeface typeface) {
                            AppCompatTextHelper.this.m1772((WeakReference<TextView>) weakReference, typeface);
                        }
                    });
                    if (this.f1519 != null) {
                        z = false;
                    }
                    this.f1520 = z;
                } catch (Resources.NotFoundException | UnsupportedOperationException e) {
                }
            }
            if (this.f1519 == null && (string = tintTypedArray.getString(i)) != null) {
                this.f1519 = Typeface.create(string, this.f1518);
            }
        } else if (tintTypedArray.hasValue(R.styleable.TextAppearance_android_typeface)) {
            this.f1520 = false;
            switch (tintTypedArray.getInt(R.styleable.TextAppearance_android_typeface, 1)) {
                case 1:
                    this.f1519 = Typeface.SANS_SERIF;
                    return;
                case 2:
                    this.f1519 = Typeface.SERIF;
                    return;
                case 3:
                    this.f1519 = Typeface.MONOSPACE;
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1772(WeakReference<TextView> weakReference, Typeface typeface) {
        if (this.f1520) {
            this.f1519 = typeface;
            TextView textView = (TextView) weakReference.get();
            if (textView != null) {
                textView.setTypeface(typeface, this.f1518);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m1773() {
        return this.f1517.m1811();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m1774() {
        return this.f1517.m1810();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int[] m1775() {
        return this.f1517.m1808();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m1776() {
        return this.f1517.m1809();
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1777() {
        this.f1517.m1806();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m1778() {
        return this.f1517.m1812();
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m1779() {
        return this.f1517.m1807();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1780() {
        if (this.f1522 != null || this.f1524 != null || this.f1523 != null || this.f1521 != null) {
            Drawable[] compoundDrawables = this.f1525.getCompoundDrawables();
            m1785(compoundDrawables[0], this.f1522);
            m1785(compoundDrawables[1], this.f1524);
            m1785(compoundDrawables[2], this.f1523);
            m1785(compoundDrawables[3], this.f1521);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1781(int i) {
        this.f1517.m1813(i);
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1782(int i, float f) {
        if (!AutoSizeableTextView.PLATFORM_SUPPORTS_AUTOSIZE && !m1779()) {
            m1767(i, f);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1783(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        this.f1517.m1815(i, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1784(Context context, int i) {
        ColorStateList colorStateList;
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, i, R.styleable.TextAppearance);
        if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_textAllCaps)) {
            m1787(obtainStyledAttributes.getBoolean(R.styleable.TextAppearance_textAllCaps, false));
        }
        if (Build.VERSION.SDK_INT < 23 && obtainStyledAttributes.hasValue(R.styleable.TextAppearance_android_textColor) && (colorStateList = obtainStyledAttributes.getColorStateList(R.styleable.TextAppearance_android_textColor)) != null) {
            this.f1525.setTextColor(colorStateList);
        }
        m1770(context, obtainStyledAttributes);
        obtainStyledAttributes.recycle();
        if (this.f1519 != null) {
            this.f1525.setTypeface(this.f1519, this.f1518);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m1785(Drawable drawable, TintInfo tintInfo) {
        if (drawable != null && tintInfo != null) {
            AppCompatDrawableManager.tintDrawable(drawable, tintInfo, this.f1525.getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1786(AttributeSet attributeSet, int i) {
        Context context = this.f1525.getContext();
        AppCompatDrawableManager appCompatDrawableManager = AppCompatDrawableManager.get();
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, attributeSet, R.styleable.AppCompatTextHelper, i, 0);
        int resourceId = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_textAppearance, -1);
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableLeft)) {
            this.f1522 = m1769(context, appCompatDrawableManager, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableLeft, 0));
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableTop)) {
            this.f1524 = m1769(context, appCompatDrawableManager, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableTop, 0));
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableRight)) {
            this.f1523 = m1769(context, appCompatDrawableManager, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableRight, 0));
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableBottom)) {
            this.f1521 = m1769(context, appCompatDrawableManager, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableBottom, 0));
        }
        obtainStyledAttributes.recycle();
        boolean z = this.f1525.getTransformationMethod() instanceof PasswordTransformationMethod;
        boolean z2 = false;
        boolean z3 = false;
        ColorStateList colorStateList = null;
        ColorStateList colorStateList2 = null;
        ColorStateList colorStateList3 = null;
        if (resourceId != -1) {
            TintTypedArray obtainStyledAttributes2 = TintTypedArray.obtainStyledAttributes(context, resourceId, R.styleable.TextAppearance);
            if (!z && obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_textAllCaps)) {
                z3 = true;
                z2 = obtainStyledAttributes2.getBoolean(R.styleable.TextAppearance_textAllCaps, false);
            }
            m1770(context, obtainStyledAttributes2);
            if (Build.VERSION.SDK_INT < 23) {
                if (obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_android_textColor)) {
                    colorStateList = obtainStyledAttributes2.getColorStateList(R.styleable.TextAppearance_android_textColor);
                }
                if (obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_android_textColorHint)) {
                    colorStateList2 = obtainStyledAttributes2.getColorStateList(R.styleable.TextAppearance_android_textColorHint);
                }
                if (obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_android_textColorLink)) {
                    colorStateList3 = obtainStyledAttributes2.getColorStateList(R.styleable.TextAppearance_android_textColorLink);
                }
            }
            obtainStyledAttributes2.recycle();
        }
        TintTypedArray obtainStyledAttributes3 = TintTypedArray.obtainStyledAttributes(context, attributeSet, R.styleable.TextAppearance, i, 0);
        if (!z && obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_textAllCaps)) {
            z3 = true;
            z2 = obtainStyledAttributes3.getBoolean(R.styleable.TextAppearance_textAllCaps, false);
        }
        if (Build.VERSION.SDK_INT < 23) {
            if (obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_android_textColor)) {
                colorStateList = obtainStyledAttributes3.getColorStateList(R.styleable.TextAppearance_android_textColor);
            }
            if (obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_android_textColorHint)) {
                colorStateList2 = obtainStyledAttributes3.getColorStateList(R.styleable.TextAppearance_android_textColorHint);
            }
            if (obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_android_textColorLink)) {
                colorStateList3 = obtainStyledAttributes3.getColorStateList(R.styleable.TextAppearance_android_textColorLink);
            }
        }
        m1770(context, obtainStyledAttributes3);
        obtainStyledAttributes3.recycle();
        if (colorStateList != null) {
            this.f1525.setTextColor(colorStateList);
        }
        if (colorStateList2 != null) {
            this.f1525.setHintTextColor(colorStateList2);
        }
        if (colorStateList3 != null) {
            this.f1525.setLinkTextColor(colorStateList3);
        }
        if (!z && z3) {
            m1787(z2);
        }
        if (this.f1519 != null) {
            this.f1525.setTypeface(this.f1519, this.f1518);
        }
        this.f1517.m1816(attributeSet, i);
        if (AutoSizeableTextView.PLATFORM_SUPPORTS_AUTOSIZE && this.f1517.m1812() != 0) {
            int[] r6 = this.f1517.m1808();
            if (r6.length <= 0) {
                return;
            }
            if (((float) this.f1525.getAutoSizeStepGranularity()) != -1.0f) {
                this.f1525.setAutoSizeTextTypeUniformWithConfiguration(this.f1517.m1811(), this.f1517.m1810(), this.f1517.m1809(), 0);
            } else {
                this.f1525.setAutoSizeTextTypeUniformWithPresetSizes(r6, 0);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1787(boolean z) {
        this.f1525.setAllCaps(z);
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1788(boolean z, int i, int i2, int i3, int i4) {
        if (!AutoSizeableTextView.PLATFORM_SUPPORTS_AUTOSIZE) {
            m1777();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1789(int[] iArr, int i) throws IllegalArgumentException {
        this.f1517.m1817(iArr, i);
    }
}
