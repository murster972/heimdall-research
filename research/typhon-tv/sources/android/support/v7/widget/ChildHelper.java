package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

class ChildHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    final Bucket f1547 = new Bucket();

    /* renamed from: 齉  reason: contains not printable characters */
    final List<View> f1548 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    final Callback f1549;

    static class Bucket {

        /* renamed from: 靐  reason: contains not printable characters */
        Bucket f1550;

        /* renamed from: 龘  reason: contains not printable characters */
        long f1551 = 0;

        Bucket() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m1904() {
            if (this.f1550 == null) {
                this.f1550 = new Bucket();
            }
        }

        public String toString() {
            return this.f1550 == null ? Long.toBinaryString(this.f1551) : this.f1550.toString() + "xx" + Long.toBinaryString(this.f1551);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 连任  reason: contains not printable characters */
        public int m1905(int i) {
            return this.f1550 == null ? i >= 64 ? Long.bitCount(this.f1551) : Long.bitCount(this.f1551 & ((1 << i) - 1)) : i < 64 ? Long.bitCount(this.f1551 & ((1 << i) - 1)) : this.f1550.m1905(i - 64) + Long.bitCount(this.f1551);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m1906(int i) {
            if (i < 64) {
                this.f1551 &= (1 << i) ^ -1;
            } else if (this.f1550 != null) {
                this.f1550.m1906(i - 64);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public boolean m1907(int i) {
            if (i >= 64) {
                m1904();
                return this.f1550.m1907(i - 64);
            }
            long j = 1 << i;
            boolean z = (this.f1551 & j) != 0;
            this.f1551 &= -1 ^ j;
            long j2 = j - 1;
            this.f1551 = (this.f1551 & j2) | Long.rotateRight(this.f1551 & (-1 ^ j2), 1);
            if (this.f1550 == null) {
                return z;
            }
            if (this.f1550.m1908(0)) {
                m1910(63);
            }
            this.f1550.m1907(0);
            return z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m1908(int i) {
            if (i < 64) {
                return (this.f1551 & (1 << i)) != 0;
            }
            m1904();
            return this.f1550.m1908(i - 64);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1909() {
            this.f1551 = 0;
            if (this.f1550 != null) {
                this.f1550.m1909();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1910(int i) {
            if (i >= 64) {
                m1904();
                this.f1550.m1910(i - 64);
                return;
            }
            this.f1551 |= 1 << i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1911(int i, boolean z) {
            if (i >= 64) {
                m1904();
                this.f1550.m1911(i - 64, z);
                return;
            }
            boolean z2 = (this.f1551 & Long.MIN_VALUE) != 0;
            long j = (1 << i) - 1;
            this.f1551 = (this.f1551 & j) | ((this.f1551 & (-1 ^ j)) << 1);
            if (z) {
                m1910(i);
            } else {
                m1906(i);
            }
            if (z2 || this.f1550 != null) {
                m1904();
                this.f1550.m1911(0, z2);
            }
        }
    }

    interface Callback {
        /* renamed from: 靐  reason: contains not printable characters */
        RecyclerView.ViewHolder m1912(View view);

        /* renamed from: 靐  reason: contains not printable characters */
        View m1913(int i);

        /* renamed from: 靐  reason: contains not printable characters */
        void m1914();

        /* renamed from: 麤  reason: contains not printable characters */
        void m1915(View view);

        /* renamed from: 齉  reason: contains not printable characters */
        void m1916(int i);

        /* renamed from: 齉  reason: contains not printable characters */
        void m1917(View view);

        /* renamed from: 龘  reason: contains not printable characters */
        int m1918();

        /* renamed from: 龘  reason: contains not printable characters */
        int m1919(View view);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1920(int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1921(View view, int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1922(View view, int i, ViewGroup.LayoutParams layoutParams);
    }

    ChildHelper(Callback callback) {
        this.f1549 = callback;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m1884(int i) {
        if (i < 0) {
            return -1;
        }
        int r1 = this.f1549.m1918();
        int i2 = i;
        while (i2 < r1) {
            int r0 = i - (i2 - this.f1547.m1905(i2));
            if (r0 == 0) {
                while (this.f1547.m1908(i2)) {
                    i2++;
                }
                return i2;
            }
            i2 += r0;
        }
        return -1;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m1885(View view) {
        this.f1548.add(view);
        this.f1549.m1917(view);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m1886(View view) {
        if (!this.f1548.remove(view)) {
            return false;
        }
        this.f1549.m1915(view);
        return true;
    }

    public String toString() {
        return this.f1547.toString() + ", hidden list:" + this.f1548.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1887(View view) {
        int r0 = this.f1549.m1919(view);
        if (r0 == -1) {
            if (m1886(view)) {
            }
            return true;
        } else if (!this.f1547.m1908(r0)) {
            return false;
        } else {
            this.f1547.m1907(r0);
            if (!m1886(view)) {
            }
            this.f1549.m1920(r0);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m1888(int i) {
        int r0 = m1884(i);
        this.f1547.m1907(r0);
        this.f1549.m1916(r0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m1889(View view) {
        int r0 = this.f1549.m1919(view);
        if (r0 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (!this.f1547.m1908(r0)) {
            throw new RuntimeException("trying to unhide a view that was not hidden" + view);
        } else {
            this.f1547.m1906(r0);
            m1886(view);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m1890() {
        return this.f1549.m1918() - this.f1548.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m1891(View view) {
        int r0 = this.f1549.m1919(view);
        if (r0 != -1 && !this.f1547.m1908(r0)) {
            return r0 - this.f1547.m1905(r0);
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public View m1892(int i) {
        return this.f1549.m1913(m1884(i));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public View m1893(int i) {
        return this.f1549.m1913(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m1894(View view) {
        int r0 = this.f1549.m1919(view);
        if (r0 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        }
        this.f1547.m1910(r0);
        m1885(view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m1895() {
        return this.f1549.m1918();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public View m1896(int i) {
        int size = this.f1548.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = this.f1548.get(i2);
            RecyclerView.ViewHolder r1 = this.f1549.m1912(view);
            if (r1.getLayoutPosition() == i && !r1.isInvalid() && !r1.isRemoved()) {
                return view;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m1897(View view) {
        return this.f1548.contains(view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1898() {
        this.f1547.m1909();
        for (int size = this.f1548.size() - 1; size >= 0; size--) {
            this.f1549.m1915(this.f1548.get(size));
            this.f1548.remove(size);
        }
        this.f1549.m1914();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1899(int i) {
        int r0 = m1884(i);
        View r1 = this.f1549.m1913(r0);
        if (r1 != null) {
            if (this.f1547.m1907(r0)) {
                m1886(r1);
            }
            this.f1549.m1920(r0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1900(View view) {
        int r0 = this.f1549.m1919(view);
        if (r0 >= 0) {
            if (this.f1547.m1907(r0)) {
                m1886(view);
            }
            this.f1549.m1920(r0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1901(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        int r0 = i < 0 ? this.f1549.m1918() : m1884(i);
        this.f1547.m1911(r0, z);
        if (z) {
            m1885(view);
        }
        this.f1549.m1922(view, r0, layoutParams);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1902(View view, int i, boolean z) {
        int r0 = i < 0 ? this.f1549.m1918() : m1884(i);
        this.f1547.m1911(r0, z);
        if (z) {
            m1885(view);
        }
        this.f1549.m1921(view, r0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1903(View view, boolean z) {
        m1902(view, -1, z);
    }
}
