package android.support.v7.widget;

import android.support.annotation.RestrictTo;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;

@RestrictTo
class TooltipCompatHandler implements View.OnAttachStateChangeListener, View.OnHoverListener, View.OnLongClickListener {

    /* renamed from: ˑ  reason: contains not printable characters */
    private static TooltipCompatHandler f1833;

    /* renamed from: ٴ  reason: contains not printable characters */
    private static TooltipCompatHandler f1834;

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f1835;

    /* renamed from: ʼ  reason: contains not printable characters */
    private TooltipPopup f1836;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f1837;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f1838;

    /* renamed from: 靐  reason: contains not printable characters */
    private final CharSequence f1839;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Runnable f1840 = new Runnable() {
        public void run() {
            TooltipCompatHandler.this.m2177();
        }
    };

    /* renamed from: 齉  reason: contains not printable characters */
    private final Runnable f1841 = new Runnable() {
        public void run() {
            TooltipCompatHandler.this.m2181(false);
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f1842;

    private TooltipCompatHandler(View view, CharSequence charSequence) {
        this.f1842 = view;
        this.f1839 = charSequence;
        this.f1842.setOnLongClickListener(this);
        this.f1842.setOnHoverListener(this);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m2174() {
        this.f1842.postDelayed(this.f1841, (long) ViewConfiguration.getLongPressTimeout());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m2175(TooltipCompatHandler tooltipCompatHandler) {
        if (f1833 != null) {
            f1833.m2176();
        }
        f1833 = tooltipCompatHandler;
        if (f1833 != null) {
            f1833.m2174();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m2176() {
        this.f1842.removeCallbacks(this.f1841);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2177() {
        if (f1834 == this) {
            f1834 = null;
            if (this.f1836 != null) {
                this.f1836.m2185();
                this.f1836 = null;
                this.f1842.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (f1833 == this) {
            m2175((TooltipCompatHandler) null);
        }
        this.f1842.removeCallbacks(this.f1840);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m2180(View view, CharSequence charSequence) {
        if (f1833 != null && f1833.f1842 == view) {
            m2175((TooltipCompatHandler) null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            if (f1834 != null && f1834.f1842 == view) {
                f1834.m2177();
            }
            view.setOnLongClickListener((View.OnLongClickListener) null);
            view.setLongClickable(false);
            view.setOnHoverListener((View.OnHoverListener) null);
            return;
        }
        new TooltipCompatHandler(view, charSequence);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2181(boolean z) {
        if (ViewCompat.isAttachedToWindow(this.f1842)) {
            m2175((TooltipCompatHandler) null);
            if (f1834 != null) {
                f1834.m2177();
            }
            f1834 = this;
            this.f1837 = z;
            this.f1836 = new TooltipPopup(this.f1842.getContext());
            this.f1836.m2186(this.f1842, this.f1838, this.f1835, this.f1837, this.f1839);
            this.f1842.addOnAttachStateChangeListener(this);
            long longPressTimeout = this.f1837 ? 2500 : (ViewCompat.getWindowSystemUiVisibility(this.f1842) & 1) == 1 ? 3000 - ((long) ViewConfiguration.getLongPressTimeout()) : 15000 - ((long) ViewConfiguration.getLongPressTimeout());
            this.f1842.removeCallbacks(this.f1840);
            this.f1842.postDelayed(this.f1840, longPressTimeout);
        }
    }

    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.f1836 == null || !this.f1837) {
            AccessibilityManager accessibilityManager = (AccessibilityManager) this.f1842.getContext().getSystemService("accessibility");
            if (!accessibilityManager.isEnabled() || !accessibilityManager.isTouchExplorationEnabled()) {
                switch (motionEvent.getAction()) {
                    case 7:
                        if (this.f1842.isEnabled() && this.f1836 == null) {
                            this.f1838 = (int) motionEvent.getX();
                            this.f1835 = (int) motionEvent.getY();
                            m2175(this);
                            break;
                        }
                    case 10:
                        m2177();
                        break;
                }
            }
        }
        return false;
    }

    public boolean onLongClick(View view) {
        this.f1838 = view.getWidth() / 2;
        this.f1835 = view.getHeight() / 2;
        m2181(true);
        return true;
    }

    public void onViewAttachedToWindow(View view) {
    }

    public void onViewDetachedFromWindow(View view) {
        m2177();
    }
}
