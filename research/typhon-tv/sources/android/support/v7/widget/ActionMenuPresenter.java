package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.GravityCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.BaseMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.view.menu.ShowableListMenu;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.widget.ActionMenuView;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

class ActionMenuPresenter extends BaseMenuPresenter implements ActionProvider.SubUiVisibilityListener {

    /* renamed from: ʼ  reason: contains not printable characters */
    OverflowMenuButton f1406;

    /* renamed from: ʽ  reason: contains not printable characters */
    OverflowPopup f1407;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Drawable f1408;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f1409;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f1410;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f1411;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f1412;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f1413;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f1414;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f1415;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f1416;

    /* renamed from: ˑ  reason: contains not printable characters */
    ActionButtonSubmenu f1417;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f1418;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f1419;

    /* renamed from: ٴ  reason: contains not printable characters */
    OpenOverflowRunnable f1420;

    /* renamed from: ᐧ  reason: contains not printable characters */
    final PopupPresenterCallback f1421 = new PopupPresenterCallback();

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final SparseBooleanArray f1422 = new SparseBooleanArray();

    /* renamed from: ᵎ  reason: contains not printable characters */
    private View f1423;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private ActionMenuPopupCallback f1424;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f1425;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f1426;

    private class ActionButtonSubmenu extends MenuPopupHelper {
        public ActionButtonSubmenu(Context context, SubMenuBuilder subMenuBuilder, View view) {
            super(context, subMenuBuilder, view, false, R.attr.actionOverflowMenuStyle);
            if (!((MenuItemImpl) subMenuBuilder.getItem()).m1554()) {
                m1602(ActionMenuPresenter.this.f1406 == null ? (View) ActionMenuPresenter.this.f1229 : ActionMenuPresenter.this.f1406);
            }
            m1601((MenuPresenter.Callback) ActionMenuPresenter.this.f1421);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1649() {
            ActionMenuPresenter.this.f1417 = null;
            ActionMenuPresenter.this.f1411 = 0;
            super.m1593();
        }
    }

    private class ActionMenuPopupCallback extends ActionMenuItemView.PopupCallback {
        ActionMenuPopupCallback() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ShowableListMenu m1650() {
            if (ActionMenuPresenter.this.f1417 != null) {
                return ActionMenuPresenter.this.f1417.m1598();
            }
            return null;
        }
    }

    private class OpenOverflowRunnable implements Runnable {

        /* renamed from: 靐  reason: contains not printable characters */
        private OverflowPopup f1429;

        public OpenOverflowRunnable(OverflowPopup overflowPopup) {
            this.f1429 = overflowPopup;
        }

        public void run() {
            if (ActionMenuPresenter.this.f1237 != null) {
                ActionMenuPresenter.this.f1237.changeMenuMode();
            }
            View view = (View) ActionMenuPresenter.this.f1229;
            if (!(view == null || view.getWindowToken() == null || !this.f1429.m1597())) {
                ActionMenuPresenter.this.f1407 = this.f1429;
            }
            ActionMenuPresenter.this.f1420 = null;
        }
    }

    private class OverflowMenuButton extends AppCompatImageView implements ActionMenuView.ActionMenuChildView {

        /* renamed from: 靐  reason: contains not printable characters */
        private final float[] f1431 = new float[2];

        public OverflowMenuButton(Context context) {
            super(context, (AttributeSet) null, R.attr.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            TooltipCompat.setTooltipText(this, getContentDescription());
            setOnTouchListener(new ForwardingListener(this, ActionMenuPresenter.this) {
                public ShowableListMenu getPopup() {
                    if (ActionMenuPresenter.this.f1407 == null) {
                        return null;
                    }
                    return ActionMenuPresenter.this.f1407.m1598();
                }

                public boolean onForwardingStarted() {
                    ActionMenuPresenter.this.m1639();
                    return true;
                }

                public boolean onForwardingStopped() {
                    if (ActionMenuPresenter.this.f1420 != null) {
                        return false;
                    }
                    ActionMenuPresenter.this.m1638();
                    return true;
                }
            });
        }

        public boolean needsDividerAfter() {
            return false;
        }

        public boolean needsDividerBefore() {
            return false;
        }

        public boolean performClick() {
            if (!super.performClick()) {
                playSoundEffect(0);
                ActionMenuPresenter.this.m1639();
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = getPaddingLeft() - getPaddingRight();
                int i5 = (width + paddingLeft) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                DrawableCompat.setHotspotBounds(background, i5 - max, paddingTop - max, i5 + max, paddingTop + max);
            }
            return frame;
        }
    }

    private class OverflowPopup extends MenuPopupHelper {
        public OverflowPopup(Context context, MenuBuilder menuBuilder, View view, boolean z) {
            super(context, menuBuilder, view, z, R.attr.actionOverflowMenuStyle);
            m1600((int) GravityCompat.END);
            m1601((MenuPresenter.Callback) ActionMenuPresenter.this.f1421);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1651() {
            if (ActionMenuPresenter.this.f1237 != null) {
                ActionMenuPresenter.this.f1237.close();
            }
            ActionMenuPresenter.this.f1407 = null;
            super.m1593();
        }
    }

    private class PopupPresenterCallback implements MenuPresenter.Callback {
        PopupPresenterCallback() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1652(MenuBuilder menuBuilder, boolean z) {
            if (menuBuilder instanceof SubMenuBuilder) {
                menuBuilder.getRootMenu().close(false);
            }
            MenuPresenter.Callback r0 = ActionMenuPresenter.this.m1498();
            if (r0 != null) {
                r0.m1606(menuBuilder, z);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1653(MenuBuilder menuBuilder) {
            if (menuBuilder == null) {
                return false;
            }
            ActionMenuPresenter.this.f1411 = ((SubMenuBuilder) menuBuilder).getItem().getItemId();
            MenuPresenter.Callback r0 = ActionMenuPresenter.this.m1498();
            return r0 != null ? r0.m1607(menuBuilder) : false;
        }
    }

    private static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: 龘  reason: contains not printable characters */
        public int f1437;

        SavedState() {
        }

        SavedState(Parcel parcel) {
            this.f1437 = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f1437);
        }
    }

    public ActionMenuPresenter(Context context) {
        super(context, R.layout.abc_action_menu_layout, R.layout.abc_action_menu_item_layout);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private View m1630(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.f1229;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof MenuView.ItemView) && ((MenuView.ItemView) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public boolean flagActionItems() {
        ArrayList<MenuItemImpl> arrayList;
        int i;
        if (this.f1237 != null) {
            arrayList = this.f1237.getVisibleItems();
            i = arrayList.size();
        } else {
            arrayList = null;
            i = 0;
        }
        int i2 = this.f1415;
        int i3 = this.f1414;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.f1229;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        boolean z = false;
        for (int i7 = 0; i7 < i; i7++) {
            MenuItemImpl menuItemImpl = arrayList.get(i7);
            if (menuItemImpl.m1556()) {
                i4++;
            } else if (menuItemImpl.m1555()) {
                i5++;
            } else {
                z = true;
            }
            if (this.f1418 && menuItemImpl.isActionViewExpanded()) {
                i2 = 0;
            }
        }
        if (this.f1425 && (z || i4 + i5 > i2)) {
            i2--;
        }
        int i8 = i2 - i4;
        SparseBooleanArray sparseBooleanArray = this.f1422;
        sparseBooleanArray.clear();
        int i9 = 0;
        int i10 = 0;
        if (this.f1412) {
            i10 = i3 / this.f1419;
            i9 = this.f1419 + ((i3 % this.f1419) / i10);
        }
        for (int i11 = 0; i11 < i; i11++) {
            MenuItemImpl menuItemImpl2 = arrayList.get(i11);
            if (menuItemImpl2.m1556()) {
                View r24 = m1641(menuItemImpl2, this.f1423, viewGroup);
                if (this.f1423 == null) {
                    this.f1423 = r24;
                }
                if (this.f1412) {
                    i10 -= ActionMenuView.measureChildForCells(r24, i9, i10, makeMeasureSpec, 0);
                } else {
                    r24.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = r24.getMeasuredWidth();
                i3 -= measuredWidth;
                if (i6 == 0) {
                    i6 = measuredWidth;
                }
                int groupId = menuItemImpl2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                menuItemImpl2.m1563(true);
            } else if (menuItemImpl2.m1555()) {
                int groupId2 = menuItemImpl2.getGroupId();
                boolean z2 = sparseBooleanArray.get(groupId2);
                boolean z3 = (i8 > 0 || z2) && i3 > 0 && (!this.f1412 || i10 > 0);
                if (z3) {
                    View r242 = m1641(menuItemImpl2, this.f1423, viewGroup);
                    if (this.f1423 == null) {
                        this.f1423 = r242;
                    }
                    if (this.f1412) {
                        int measureChildForCells = ActionMenuView.measureChildForCells(r242, i9, i10, makeMeasureSpec, 0);
                        i10 -= measureChildForCells;
                        if (measureChildForCells == 0) {
                            z3 = false;
                        }
                    } else {
                        r242.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth2 = r242.getMeasuredWidth();
                    i3 -= measuredWidth2;
                    if (i6 == 0) {
                        i6 = measuredWidth2;
                    }
                    if (this.f1412) {
                        z3 &= i3 >= 0;
                    } else {
                        z3 &= i3 + i6 > 0;
                    }
                }
                if (z3 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z2) {
                    sparseBooleanArray.put(groupId2, false);
                    for (int i12 = 0; i12 < i11; i12++) {
                        MenuItemImpl menuItemImpl3 = arrayList.get(i12);
                        if (menuItemImpl3.getGroupId() == groupId2) {
                            if (menuItemImpl3.m1554()) {
                                i8++;
                            }
                            menuItemImpl3.m1563(false);
                        }
                    }
                }
                if (z3) {
                    i8--;
                }
                menuItemImpl2.m1563(z3);
            } else {
                menuItemImpl2.m1563(false);
            }
        }
        return true;
    }

    public void initForMenu(Context context, MenuBuilder menuBuilder) {
        super.initForMenu(context, menuBuilder);
        Resources resources = context.getResources();
        ActionBarPolicy r0 = ActionBarPolicy.m1415(context);
        if (!this.f1426) {
            this.f1425 = r0.m1419();
        }
        if (!this.f1416) {
            this.f1413 = r0.m1421();
        }
        if (!this.f1410) {
            this.f1415 = r0.m1422();
        }
        int i = this.f1413;
        if (this.f1425) {
            if (this.f1406 == null) {
                this.f1406 = new OverflowMenuButton(this.f1238);
                if (this.f1409) {
                    this.f1406.setImageDrawable(this.f1408);
                    this.f1408 = null;
                    this.f1409 = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.f1406.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.f1406.getMeasuredWidth();
        } else {
            this.f1406 = null;
        }
        this.f1414 = i;
        this.f1419 = (int) (56.0f * resources.getDisplayMetrics().density);
        this.f1423 = null;
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        m1635();
        super.onCloseMenu(menuBuilder, z);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        if (parcelable instanceof SavedState) {
            SavedState savedState = (SavedState) parcelable;
            if (savedState.f1437 > 0 && (findItem = this.f1237.findItem(savedState.f1437)) != null) {
                onSubMenuSelected((SubMenuBuilder) findItem.getSubMenu());
            }
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState();
        savedState.f1437 = this.f1411;
        return savedState;
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        SubMenuBuilder subMenuBuilder2 = subMenuBuilder;
        while (subMenuBuilder2.getParentMenu() != this.f1237) {
            subMenuBuilder2 = (SubMenuBuilder) subMenuBuilder2.getParentMenu();
        }
        View r0 = m1630(subMenuBuilder2.getItem());
        if (r0 == null) {
            return false;
        }
        this.f1411 = subMenuBuilder.getItem().getItemId();
        boolean z = false;
        int size = subMenuBuilder.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            MenuItem item = subMenuBuilder.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z = true;
                break;
            }
            i++;
        }
        this.f1417 = new ActionButtonSubmenu(this.f1235, subMenuBuilder, r0);
        this.f1417.m1604(z);
        this.f1417.m1596();
        super.onSubMenuSelected(subMenuBuilder);
        return true;
    }

    public void onSubUiVisibilityChanged(boolean z) {
        if (z) {
            super.onSubMenuSelected((SubMenuBuilder) null);
        } else if (this.f1237 != null) {
            this.f1237.close(false);
        }
    }

    public void updateMenuView(boolean z) {
        super.updateMenuView(z);
        ((View) this.f1229).requestLayout();
        if (this.f1237 != null) {
            ArrayList<MenuItemImpl> actionItems = this.f1237.getActionItems();
            int size = actionItems.size();
            for (int i = 0; i < size; i++) {
                ActionProvider supportActionProvider = actionItems.get(i).getSupportActionProvider();
                if (supportActionProvider != null) {
                    supportActionProvider.setSubUiVisibilityListener(this);
                }
            }
        }
        ArrayList<MenuItemImpl> nonActionItems = this.f1237 != null ? this.f1237.getNonActionItems() : null;
        boolean z2 = false;
        if (this.f1425 && nonActionItems != null) {
            int size2 = nonActionItems.size();
            z2 = size2 == 1 ? !nonActionItems.get(0).isActionViewExpanded() : size2 > 0;
        }
        if (z2) {
            if (this.f1406 == null) {
                this.f1406 = new OverflowMenuButton(this.f1238);
            }
            ViewGroup viewGroup = (ViewGroup) this.f1406.getParent();
            if (viewGroup != this.f1229) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.f1406);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.f1229;
                actionMenuView.addView(this.f1406, actionMenuView.generateOverflowButtonLayoutParams());
            }
        } else if (this.f1406 != null && this.f1406.getParent() == this.f1229) {
            ((ViewGroup) this.f1229).removeView(this.f1406);
        }
        ((ActionMenuView) this.f1229).setOverflowReserved(this.f1425);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1631() {
        if (this.f1417 == null) {
            return false;
        }
        this.f1417.m1595();
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1632() {
        return this.f1407 != null && this.f1407.m1594();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1633() {
        return this.f1420 != null || m1632();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m1634() {
        return this.f1425;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m1635() {
        return m1638() | m1631();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Drawable m1636() {
        if (this.f1406 != null) {
            return this.f1406.getDrawable();
        }
        if (this.f1409) {
            return this.f1408;
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1637(boolean z) {
        this.f1418 = z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m1638() {
        if (this.f1420 == null || this.f1229 == null) {
            OverflowPopup overflowPopup = this.f1407;
            if (overflowPopup == null) {
                return false;
            }
            overflowPopup.m1595();
            return true;
        }
        ((View) this.f1229).removeCallbacks(this.f1420);
        this.f1420 = null;
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m1639() {
        if (!this.f1425 || m1632() || this.f1237 == null || this.f1229 == null || this.f1420 != null || this.f1237.getNonActionItems().isEmpty()) {
            return false;
        }
        this.f1420 = new OpenOverflowRunnable(new OverflowPopup(this.f1235, this.f1237, this.f1406, true));
        ((View) this.f1229).post(this.f1420);
        super.onSubMenuSelected((SubMenuBuilder) null);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MenuView m1640(ViewGroup viewGroup) {
        MenuView menuView = this.f1229;
        MenuView r1 = super.m1499(viewGroup);
        if (menuView != r1) {
            ((ActionMenuView) r1).setPresenter(this);
        }
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public View m1641(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        View actionView = menuItemImpl.getActionView();
        if (actionView == null || menuItemImpl.m1552()) {
            actionView = super.m1500(menuItemImpl, view, viewGroup);
        }
        actionView.setVisibility(menuItemImpl.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1642(Configuration configuration) {
        if (!this.f1410) {
            this.f1415 = ActionBarPolicy.m1415(this.f1235).m1422();
        }
        if (this.f1237 != null) {
            this.f1237.onItemsChanged(true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1643(Drawable drawable) {
        if (this.f1406 != null) {
            this.f1406.setImageDrawable(drawable);
            return;
        }
        this.f1409 = true;
        this.f1408 = drawable;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1644(MenuItemImpl menuItemImpl, MenuView.ItemView itemView) {
        itemView.initialize(menuItemImpl, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) itemView;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.f1229);
        if (this.f1424 == null) {
            this.f1424 = new ActionMenuPopupCallback();
        }
        actionMenuItemView.setPopupCallback(this.f1424);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1645(ActionMenuView actionMenuView) {
        this.f1229 = actionMenuView;
        actionMenuView.initialize(this.f1237);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1646(boolean z) {
        this.f1425 = z;
        this.f1426 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1647(int i, MenuItemImpl menuItemImpl) {
        return menuItemImpl.m1554();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1648(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.f1406) {
            return false;
        }
        return super.m1505(viewGroup, i);
    }
}
