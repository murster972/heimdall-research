package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;

class CardViewApi21Impl implements CardViewImpl {
    CardViewApi21Impl() {
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private RoundRectDrawable m1827(CardViewDelegate cardViewDelegate) {
        return (RoundRectDrawable) cardViewDelegate.m1864();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1828(CardViewDelegate cardViewDelegate) {
        if (!cardViewDelegate.m1868()) {
            cardViewDelegate.m1866(0, 0, 0, 0);
            return;
        }
        float r0 = m1838(cardViewDelegate);
        float r2 = m1835(cardViewDelegate);
        int ceil = (int) Math.ceil((double) RoundRectDrawableWithShadow.m2051(r0, r2, cardViewDelegate.m1862()));
        int ceil2 = (int) Math.ceil((double) RoundRectDrawableWithShadow.m2055(r0, r2, cardViewDelegate.m1862()));
        cardViewDelegate.m1866(ceil, ceil2, ceil, ceil2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1829(CardViewDelegate cardViewDelegate) {
        m1834(cardViewDelegate, m1838(cardViewDelegate));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1830(CardViewDelegate cardViewDelegate) {
        m1834(cardViewDelegate, m1838(cardViewDelegate));
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public ColorStateList m1831(CardViewDelegate cardViewDelegate) {
        return m1827(cardViewDelegate).getColor();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public float m1832(CardViewDelegate cardViewDelegate) {
        return cardViewDelegate.m1863().getElevation();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public float m1833(CardViewDelegate cardViewDelegate) {
        return m1835(cardViewDelegate) * 2.0f;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1834(CardViewDelegate cardViewDelegate, float f) {
        m1827(cardViewDelegate).setPadding(f, cardViewDelegate.m1868(), cardViewDelegate.m1862());
        m1828(cardViewDelegate);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public float m1835(CardViewDelegate cardViewDelegate) {
        return m1827(cardViewDelegate).getRadius();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public float m1836(CardViewDelegate cardViewDelegate) {
        return m1835(cardViewDelegate) * 2.0f;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1837(CardViewDelegate cardViewDelegate, float f) {
        cardViewDelegate.m1863().setElevation(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public float m1838(CardViewDelegate cardViewDelegate) {
        return m1827(cardViewDelegate).getPadding();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1839() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1840(CardViewDelegate cardViewDelegate, float f) {
        m1827(cardViewDelegate).setRadius(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1841(CardViewDelegate cardViewDelegate, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        cardViewDelegate.m1867(new RoundRectDrawable(colorStateList, f));
        View r1 = cardViewDelegate.m1863();
        r1.setClipToOutline(true);
        r1.setElevation(f2);
        m1834(cardViewDelegate, f3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1842(CardViewDelegate cardViewDelegate, ColorStateList colorStateList) {
        m1827(cardViewDelegate).setColor(colorStateList);
    }
}
