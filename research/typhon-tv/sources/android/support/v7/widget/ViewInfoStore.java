package android.support.v7.widget;

import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.support.v4.util.Pools;
import android.support.v7.widget.RecyclerView;

class ViewInfoStore {

    /* renamed from: 靐  reason: contains not printable characters */
    final LongSparseArray<RecyclerView.ViewHolder> f1859 = new LongSparseArray<>();

    /* renamed from: 龘  reason: contains not printable characters */
    final ArrayMap<RecyclerView.ViewHolder, InfoRecord> f1860 = new ArrayMap<>();

    static class InfoRecord {

        /* renamed from: 麤  reason: contains not printable characters */
        static Pools.Pool<InfoRecord> f1861 = new Pools.SimplePool(20);

        /* renamed from: 靐  reason: contains not printable characters */
        RecyclerView.ItemAnimator.ItemHolderInfo f1862;

        /* renamed from: 齉  reason: contains not printable characters */
        RecyclerView.ItemAnimator.ItemHolderInfo f1863;

        /* renamed from: 龘  reason: contains not printable characters */
        int f1864;

        private InfoRecord() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        static void m2216() {
            do {
            } while (f1861.acquire() != null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static InfoRecord m2217() {
            InfoRecord acquire = f1861.acquire();
            return acquire == null ? new InfoRecord() : acquire;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static void m2218(InfoRecord infoRecord) {
            infoRecord.f1864 = 0;
            infoRecord.f1862 = null;
            infoRecord.f1863 = null;
            f1861.release(infoRecord);
        }
    }

    interface ProcessCallback {
        /* renamed from: 靐  reason: contains not printable characters */
        void m2219(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2);

        /* renamed from: 齉  reason: contains not printable characters */
        void m2220(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2);

        /* renamed from: 龘  reason: contains not printable characters */
        void m2221(RecyclerView.ViewHolder viewHolder);

        /* renamed from: 龘  reason: contains not printable characters */
        void m2222(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2);
    }

    ViewInfoStore() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private RecyclerView.ItemAnimator.ItemHolderInfo m2199(RecyclerView.ViewHolder viewHolder, int i) {
        InfoRecord valueAt;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo = null;
        int indexOfKey = this.f1860.indexOfKey(viewHolder);
        if (!(indexOfKey < 0 || (valueAt = this.f1860.valueAt(indexOfKey)) == null || (valueAt.f1864 & i) == 0)) {
            valueAt.f1864 &= i ^ -1;
            if (i == 4) {
                itemHolderInfo = valueAt.f1862;
            } else if (i == 8) {
                itemHolderInfo = valueAt.f1863;
            } else {
                throw new IllegalArgumentException("Must provide flag PRE or POST");
            }
            if ((valueAt.f1864 & 12) == 0) {
                this.f1860.removeAt(indexOfKey);
                InfoRecord.m2218(valueAt);
            }
        }
        return itemHolderInfo;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2200(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.f1860.get(viewHolder);
        if (infoRecord != null) {
            infoRecord.f1864 &= -2;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2201(RecyclerView.ViewHolder viewHolder) {
        int size = this.f1859.size() - 1;
        while (true) {
            if (size < 0) {
                break;
            } else if (viewHolder == this.f1859.valueAt(size)) {
                this.f1859.removeAt(size);
                break;
            } else {
                size--;
            }
        }
        InfoRecord remove = this.f1860.remove(viewHolder);
        if (remove != null) {
            InfoRecord.m2218(remove);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2202(RecyclerView.ViewHolder viewHolder) {
        m2200(viewHolder);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m2203(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.f1860.get(viewHolder);
        if (infoRecord == null) {
            infoRecord = InfoRecord.m2217();
            this.f1860.put(viewHolder, infoRecord);
        }
        infoRecord.f1864 |= 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public RecyclerView.ItemAnimator.ItemHolderInfo m2204(RecyclerView.ViewHolder viewHolder) {
        return m2199(viewHolder, 4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m2205() {
        InfoRecord.m2216();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m2206(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo) {
        InfoRecord infoRecord = this.f1860.get(viewHolder);
        if (infoRecord == null) {
            infoRecord = InfoRecord.m2217();
            this.f1860.put(viewHolder, infoRecord);
        }
        infoRecord.f1864 |= 2;
        infoRecord.f1862 = itemHolderInfo;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m2207(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.f1860.get(viewHolder);
        return (infoRecord == null || (infoRecord.f1864 & 4) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public RecyclerView.ItemAnimator.ItemHolderInfo m2208(RecyclerView.ViewHolder viewHolder) {
        return m2199(viewHolder, 8);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m2209(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo) {
        InfoRecord infoRecord = this.f1860.get(viewHolder);
        if (infoRecord == null) {
            infoRecord = InfoRecord.m2217();
            this.f1860.put(viewHolder, infoRecord);
        }
        infoRecord.f1863 = itemHolderInfo;
        infoRecord.f1864 |= 8;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public RecyclerView.ViewHolder m2210(long j) {
        return this.f1859.get(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2211() {
        this.f1860.clear();
        this.f1859.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2212(long j, RecyclerView.ViewHolder viewHolder) {
        this.f1859.put(j, viewHolder);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2213(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo) {
        InfoRecord infoRecord = this.f1860.get(viewHolder);
        if (infoRecord == null) {
            infoRecord = InfoRecord.m2217();
            this.f1860.put(viewHolder, infoRecord);
        }
        infoRecord.f1862 = itemHolderInfo;
        infoRecord.f1864 |= 4;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2214(ProcessCallback processCallback) {
        for (int size = this.f1860.size() - 1; size >= 0; size--) {
            RecyclerView.ViewHolder keyAt = this.f1860.keyAt(size);
            InfoRecord removeAt = this.f1860.removeAt(size);
            if ((removeAt.f1864 & 3) == 3) {
                processCallback.m2221(keyAt);
            } else if ((removeAt.f1864 & 1) != 0) {
                if (removeAt.f1862 == null) {
                    processCallback.m2221(keyAt);
                } else {
                    processCallback.m2222(keyAt, removeAt.f1862, removeAt.f1863);
                }
            } else if ((removeAt.f1864 & 14) == 14) {
                processCallback.m2219(keyAt, removeAt.f1862, removeAt.f1863);
            } else if ((removeAt.f1864 & 12) == 12) {
                processCallback.m2220(keyAt, removeAt.f1862, removeAt.f1863);
            } else if ((removeAt.f1864 & 4) != 0) {
                processCallback.m2222(keyAt, removeAt.f1862, (RecyclerView.ItemAnimator.ItemHolderInfo) null);
            } else if ((removeAt.f1864 & 8) != 0) {
                processCallback.m2219(keyAt, removeAt.f1862, removeAt.f1863);
            } else if ((removeAt.f1864 & 2) != 0) {
            }
            InfoRecord.m2218(removeAt);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m2215(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.f1860.get(viewHolder);
        return (infoRecord == null || (infoRecord.f1864 & 1) == 0) ? false : true;
    }
}
