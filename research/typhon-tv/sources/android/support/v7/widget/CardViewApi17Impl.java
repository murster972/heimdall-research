package android.support.v7.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.widget.RoundRectDrawableWithShadow;

class CardViewApi17Impl extends CardViewBaseImpl {
    CardViewApi17Impl() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1825() {
        RoundRectDrawableWithShadow.f1706 = new RoundRectDrawableWithShadow.RoundRectHelper() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m1826(Canvas canvas, RectF rectF, float f, Paint paint) {
                canvas.drawRoundRect(rectF, f, f, paint);
            }
        };
    }
}
