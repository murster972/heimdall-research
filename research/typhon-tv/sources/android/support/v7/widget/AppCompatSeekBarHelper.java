package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.widget.SeekBar;

class AppCompatSeekBarHelper extends AppCompatProgressBarHelper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f1498 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f1499 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private Drawable f1500;

    /* renamed from: 麤  reason: contains not printable characters */
    private PorterDuff.Mode f1501 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private ColorStateList f1502 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SeekBar f1503;

    AppCompatSeekBarHelper(SeekBar seekBar) {
        super(seekBar);
        this.f1503 = seekBar;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m1756() {
        if (this.f1500 == null) {
            return;
        }
        if (this.f1499 || this.f1498) {
            this.f1500 = DrawableCompat.wrap(this.f1500.mutate());
            if (this.f1499) {
                DrawableCompat.setTintList(this.f1500, this.f1502);
            }
            if (this.f1498) {
                DrawableCompat.setTintMode(this.f1500, this.f1501);
            }
            if (this.f1500.isStateful()) {
                this.f1500.setState(this.f1503.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1757() {
        if (this.f1500 != null) {
            this.f1500.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m1758() {
        Drawable drawable = this.f1500;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.f1503.getDrawableState())) {
            this.f1503.invalidateDrawable(drawable);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1759(Canvas canvas) {
        int max;
        int i = 1;
        if (this.f1500 != null && (max = this.f1503.getMax()) > 1) {
            int intrinsicWidth = this.f1500.getIntrinsicWidth();
            int intrinsicHeight = this.f1500.getIntrinsicHeight();
            int i2 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
            if (intrinsicHeight >= 0) {
                i = intrinsicHeight / 2;
            }
            this.f1500.setBounds(-i2, -i, i2, i);
            float width = ((float) ((this.f1503.getWidth() - this.f1503.getPaddingLeft()) - this.f1503.getPaddingRight())) / ((float) max);
            int save = canvas.save();
            canvas.translate((float) this.f1503.getPaddingLeft(), (float) (this.f1503.getHeight() / 2));
            for (int i3 = 0; i3 <= max; i3++) {
                this.f1500.draw(canvas);
                canvas.translate(width, 0.0f);
            }
            canvas.restoreToCount(save);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1760(Drawable drawable) {
        if (this.f1500 != null) {
            this.f1500.setCallback((Drawable.Callback) null);
        }
        this.f1500 = drawable;
        if (drawable != null) {
            drawable.setCallback(this.f1503);
            DrawableCompat.setLayoutDirection(drawable, ViewCompat.getLayoutDirection(this.f1503));
            if (drawable.isStateful()) {
                drawable.setState(this.f1503.getDrawableState());
            }
            m1756();
        }
        this.f1503.invalidate();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1761(AttributeSet attributeSet, int i) {
        super.m1755(attributeSet, i);
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(this.f1503.getContext(), attributeSet, R.styleable.AppCompatSeekBar, i, 0);
        Drawable drawableIfKnown = obtainStyledAttributes.getDrawableIfKnown(R.styleable.AppCompatSeekBar_android_thumb);
        if (drawableIfKnown != null) {
            this.f1503.setThumb(drawableIfKnown);
        }
        m1760(obtainStyledAttributes.getDrawable(R.styleable.AppCompatSeekBar_tickMark));
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatSeekBar_tickMarkTintMode)) {
            this.f1501 = DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(R.styleable.AppCompatSeekBar_tickMarkTintMode, -1), this.f1501);
            this.f1498 = true;
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatSeekBar_tickMarkTint)) {
            this.f1502 = obtainStyledAttributes.getColorStateList(R.styleable.AppCompatSeekBar_tickMarkTint);
            this.f1499 = true;
        }
        obtainStyledAttributes.recycle();
        m1756();
    }
}
