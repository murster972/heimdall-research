package android.support.v7.widget;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;

public class TooltipCompat {
    private static final ViewCompatImpl IMPL;

    @TargetApi(26)
    private static class Api26ViewCompatImpl implements ViewCompatImpl {
        private Api26ViewCompatImpl() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m2171(View view, CharSequence charSequence) {
            view.setTooltipText(charSequence);
        }
    }

    private static class BaseViewCompatImpl implements ViewCompatImpl {
        private BaseViewCompatImpl() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m2172(View view, CharSequence charSequence) {
            TooltipCompatHandler.m2180(view, charSequence);
        }
    }

    private interface ViewCompatImpl {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2173(View view, CharSequence charSequence);
    }

    static {
        if (Build.VERSION.SDK_INT >= 26) {
            IMPL = new Api26ViewCompatImpl();
        } else {
            IMPL = new BaseViewCompatImpl();
        }
    }

    private TooltipCompat() {
    }

    public static void setTooltipText(View view, CharSequence charSequence) {
        IMPL.m2173(view, charSequence);
    }
}
