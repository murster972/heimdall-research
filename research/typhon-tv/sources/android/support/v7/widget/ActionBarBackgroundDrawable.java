package android.support.v7.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

class ActionBarBackgroundDrawable extends Drawable {

    /* renamed from: 龘  reason: contains not printable characters */
    final ActionBarContainer f1400;

    public ActionBarBackgroundDrawable(ActionBarContainer actionBarContainer) {
        this.f1400 = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        if (!this.f1400.mIsSplit) {
            if (this.f1400.mBackground != null) {
                this.f1400.mBackground.draw(canvas);
            }
            if (this.f1400.mStackedBackground != null && this.f1400.mIsStacked) {
                this.f1400.mStackedBackground.draw(canvas);
            }
        } else if (this.f1400.mSplitBackground != null) {
            this.f1400.mSplitBackground.draw(canvas);
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
