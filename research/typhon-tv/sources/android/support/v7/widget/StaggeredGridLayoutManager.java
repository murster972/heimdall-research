package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class StaggeredGridLayoutManager extends RecyclerView.LayoutManager implements RecyclerView.SmoothScroller.ScrollVectorProvider {
    static final boolean DEBUG = false;
    @Deprecated
    public static final int GAP_HANDLING_LAZY = 1;
    public static final int GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS = 2;
    public static final int GAP_HANDLING_NONE = 0;
    public static final int HORIZONTAL = 0;
    static final int INVALID_OFFSET = Integer.MIN_VALUE;
    private static final float MAX_SCROLL_FACTOR = 0.33333334f;
    private static final String TAG = "StaggeredGridLManager";
    public static final int VERTICAL = 1;
    private final AnchorInfo mAnchorInfo = new AnchorInfo();
    private final Runnable mCheckForGapsRunnable = new Runnable() {
        public void run() {
            StaggeredGridLayoutManager.this.checkForGaps();
        }
    };
    private int mFullSizeSpec;
    private int mGapStrategy = 2;
    private boolean mLaidOutInvalidFullSpan = false;
    private boolean mLastLayoutFromEnd;
    private boolean mLastLayoutRTL;
    private final LayoutState mLayoutState;
    LazySpanLookup mLazySpanLookup = new LazySpanLookup();
    private int mOrientation;
    private SavedState mPendingSavedState;
    int mPendingScrollPosition = -1;
    int mPendingScrollPositionOffset = Integer.MIN_VALUE;
    private int[] mPrefetchDistances;
    OrientationHelper mPrimaryOrientation;
    private BitSet mRemainingSpans;
    boolean mReverseLayout = false;
    OrientationHelper mSecondaryOrientation;
    boolean mShouldReverseLayout = false;
    private int mSizePerSpan;
    private boolean mSmoothScrollbarEnabled = true;
    private int mSpanCount = -1;
    Span[] mSpans;
    private final Rect mTmpRect = new Rect();

    class AnchorInfo {

        /* renamed from: ʻ  reason: contains not printable characters */
        int[] f1768;

        /* renamed from: 连任  reason: contains not printable characters */
        boolean f1770;

        /* renamed from: 靐  reason: contains not printable characters */
        int f1771;

        /* renamed from: 麤  reason: contains not printable characters */
        boolean f1772;

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f1773;

        /* renamed from: 龘  reason: contains not printable characters */
        int f1774;

        AnchorInfo() {
            m2092();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m2091() {
            this.f1771 = this.f1773 ? StaggeredGridLayoutManager.this.mPrimaryOrientation.getEndAfterPadding() : StaggeredGridLayoutManager.this.mPrimaryOrientation.getStartAfterPadding();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2092() {
            this.f1774 = -1;
            this.f1771 = Integer.MIN_VALUE;
            this.f1773 = false;
            this.f1772 = false;
            this.f1770 = false;
            if (this.f1768 != null) {
                Arrays.fill(this.f1768, -1);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2093(int i) {
            if (this.f1773) {
                this.f1771 = StaggeredGridLayoutManager.this.mPrimaryOrientation.getEndAfterPadding() - i;
            } else {
                this.f1771 = StaggeredGridLayoutManager.this.mPrimaryOrientation.getStartAfterPadding() + i;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2094(Span[] spanArr) {
            int length = spanArr.length;
            if (this.f1768 == null || this.f1768.length < length) {
                this.f1768 = new int[StaggeredGridLayoutManager.this.mSpans.length];
            }
            for (int i = 0; i < length; i++) {
                this.f1768[i] = spanArr[i].m2134(Integer.MIN_VALUE);
            }
        }
    }

    public static class LayoutParams extends RecyclerView.LayoutParams {
        public static final int INVALID_SPAN_ID = -1;
        boolean mFullSpan;
        Span mSpan;

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(RecyclerView.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public final int getSpanIndex() {
            if (this.mSpan == null) {
                return -1;
            }
            return this.mSpan.f1782;
        }

        public boolean isFullSpan() {
            return this.mFullSpan;
        }

        public void setFullSpan(boolean z) {
            this.mFullSpan = z;
        }
    }

    static class LazySpanLookup {

        /* renamed from: 靐  reason: contains not printable characters */
        List<FullSpanItem> f1775;

        /* renamed from: 龘  reason: contains not printable characters */
        int[] f1776;

        static class FullSpanItem implements Parcelable {
            public static final Parcelable.Creator<FullSpanItem> CREATOR = new Parcelable.Creator<FullSpanItem>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public FullSpanItem createFromParcel(Parcel parcel) {
                    return new FullSpanItem(parcel);
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public FullSpanItem[] newArray(int i) {
                    return new FullSpanItem[i];
                }
            };

            /* renamed from: 靐  reason: contains not printable characters */
            int f1777;

            /* renamed from: 麤  reason: contains not printable characters */
            boolean f1778;

            /* renamed from: 齉  reason: contains not printable characters */
            int[] f1779;

            /* renamed from: 龘  reason: contains not printable characters */
            int f1780;

            FullSpanItem() {
            }

            FullSpanItem(Parcel parcel) {
                boolean z = true;
                this.f1780 = parcel.readInt();
                this.f1777 = parcel.readInt();
                this.f1778 = parcel.readInt() != 1 ? false : z;
                int readInt = parcel.readInt();
                if (readInt > 0) {
                    this.f1779 = new int[readInt];
                    parcel.readIntArray(this.f1779);
                }
            }

            public int describeContents() {
                return 0;
            }

            public String toString() {
                return "FullSpanItem{mPosition=" + this.f1780 + ", mGapDir=" + this.f1777 + ", mHasUnwantedGapAfter=" + this.f1778 + ", mGapPerSpan=" + Arrays.toString(this.f1779) + '}';
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f1780);
                parcel.writeInt(this.f1777);
                parcel.writeInt(this.f1778 ? 1 : 0);
                if (this.f1779 == null || this.f1779.length <= 0) {
                    parcel.writeInt(0);
                    return;
                }
                parcel.writeInt(this.f1779.length);
                parcel.writeIntArray(this.f1779);
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public int m2110(int i) {
                if (this.f1779 == null) {
                    return 0;
                }
                return this.f1779[i];
            }
        }

        LazySpanLookup() {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private int m2095(int i) {
            if (this.f1775 == null) {
                return -1;
            }
            FullSpanItem r3 = m2098(i);
            if (r3 != null) {
                this.f1775.remove(r3);
            }
            int i2 = -1;
            int size = this.f1775.size();
            int i3 = 0;
            while (true) {
                if (i3 >= size) {
                    break;
                } else if (this.f1775.get(i3).f1780 >= i) {
                    i2 = i3;
                    break;
                } else {
                    i3++;
                }
            }
            if (i2 == -1) {
                return -1;
            }
            this.f1775.remove(i2);
            return this.f1775.get(i2).f1780;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m2096(int i, int i2) {
            if (this.f1775 != null) {
                for (int size = this.f1775.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.f1775.get(size);
                    if (fullSpanItem.f1780 >= i) {
                        fullSpanItem.f1780 += i2;
                    }
                }
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private void m2097(int i, int i2) {
            if (this.f1775 != null) {
                int i3 = i + i2;
                for (int size = this.f1775.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.f1775.get(size);
                    if (fullSpanItem.f1780 >= i) {
                        if (fullSpanItem.f1780 < i3) {
                            this.f1775.remove(size);
                        } else {
                            fullSpanItem.f1780 -= i2;
                        }
                    }
                }
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public FullSpanItem m2098(int i) {
            if (this.f1775 == null) {
                return null;
            }
            for (int size = this.f1775.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = this.f1775.get(size);
                if (fullSpanItem.f1780 == i) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 连任  reason: contains not printable characters */
        public void m2099(int i) {
            if (this.f1776 == null) {
                this.f1776 = new int[(Math.max(i, 10) + 1)];
                Arrays.fill(this.f1776, -1);
            } else if (i >= this.f1776.length) {
                int[] iArr = this.f1776;
                this.f1776 = new int[m2102(i)];
                System.arraycopy(iArr, 0, this.f1776, 0, iArr.length);
                Arrays.fill(this.f1776, iArr.length, this.f1776.length, -1);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m2100(int i) {
            if (this.f1776 == null || i >= this.f1776.length) {
                return -1;
            }
            int r0 = m2095(i);
            if (r0 == -1) {
                Arrays.fill(this.f1776, i, this.f1776.length, -1);
                return this.f1776.length;
            }
            Arrays.fill(this.f1776, i, r0 + 1, -1);
            return r0 + 1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m2101(int i, int i2) {
            if (this.f1776 != null && i < this.f1776.length) {
                m2099(i + i2);
                System.arraycopy(this.f1776, i, this.f1776, i + i2, (this.f1776.length - i) - i2);
                Arrays.fill(this.f1776, i, i + i2, -1);
                m2096(i, i2);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public int m2102(int i) {
            int length = this.f1776.length;
            while (length <= i) {
                length *= 2;
            }
            return length;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public int m2103(int i) {
            if (this.f1776 == null || i >= this.f1776.length) {
                return -1;
            }
            return this.f1776[i];
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m2104(int i) {
            if (this.f1775 != null) {
                for (int size = this.f1775.size() - 1; size >= 0; size--) {
                    if (this.f1775.get(size).f1780 >= i) {
                        this.f1775.remove(size);
                    }
                }
            }
            return m2100(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public FullSpanItem m2105(int i, int i2, int i3, boolean z) {
            if (this.f1775 == null) {
                return null;
            }
            int size = this.f1775.size();
            for (int i4 = 0; i4 < size; i4++) {
                FullSpanItem fullSpanItem = this.f1775.get(i4);
                if (fullSpanItem.f1780 >= i2) {
                    return null;
                }
                if (fullSpanItem.f1780 >= i) {
                    if (i3 == 0 || fullSpanItem.f1777 == i3) {
                        return fullSpanItem;
                    }
                    if (z && fullSpanItem.f1778) {
                        return fullSpanItem;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2106() {
            if (this.f1776 != null) {
                Arrays.fill(this.f1776, -1);
            }
            this.f1775 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2107(int i, int i2) {
            if (this.f1776 != null && i < this.f1776.length) {
                m2099(i + i2);
                System.arraycopy(this.f1776, i + i2, this.f1776, i, (this.f1776.length - i) - i2);
                Arrays.fill(this.f1776, this.f1776.length - i2, this.f1776.length, -1);
                m2097(i, i2);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2108(int i, Span span) {
            m2099(i);
            this.f1776[i] = span.f1782;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m2109(FullSpanItem fullSpanItem) {
            if (this.f1775 == null) {
                this.f1775 = new ArrayList();
            }
            int size = this.f1775.size();
            for (int i = 0; i < size; i++) {
                FullSpanItem fullSpanItem2 = this.f1775.get(i);
                if (fullSpanItem2.f1780 == fullSpanItem.f1780) {
                    this.f1775.remove(i);
                }
                if (fullSpanItem2.f1780 >= fullSpanItem.f1780) {
                    this.f1775.add(i, fullSpanItem);
                    return;
                }
            }
            this.f1775.add(fullSpanItem);
        }
    }

    @RestrictTo
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        boolean mAnchorLayoutFromEnd;
        int mAnchorPosition;
        List<LazySpanLookup.FullSpanItem> mFullSpanItems;
        boolean mLastLayoutRTL;
        boolean mReverseLayout;
        int[] mSpanLookup;
        int mSpanLookupSize;
        int[] mSpanOffsets;
        int mSpanOffsetsSize;
        int mVisibleAnchorPosition;

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            boolean z = true;
            this.mAnchorPosition = parcel.readInt();
            this.mVisibleAnchorPosition = parcel.readInt();
            this.mSpanOffsetsSize = parcel.readInt();
            if (this.mSpanOffsetsSize > 0) {
                this.mSpanOffsets = new int[this.mSpanOffsetsSize];
                parcel.readIntArray(this.mSpanOffsets);
            }
            this.mSpanLookupSize = parcel.readInt();
            if (this.mSpanLookupSize > 0) {
                this.mSpanLookup = new int[this.mSpanLookupSize];
                parcel.readIntArray(this.mSpanLookup);
            }
            this.mReverseLayout = parcel.readInt() == 1;
            this.mAnchorLayoutFromEnd = parcel.readInt() == 1;
            this.mLastLayoutRTL = parcel.readInt() != 1 ? false : z;
            this.mFullSpanItems = parcel.readArrayList(LazySpanLookup.FullSpanItem.class.getClassLoader());
        }

        public SavedState(SavedState savedState) {
            this.mSpanOffsetsSize = savedState.mSpanOffsetsSize;
            this.mAnchorPosition = savedState.mAnchorPosition;
            this.mVisibleAnchorPosition = savedState.mVisibleAnchorPosition;
            this.mSpanOffsets = savedState.mSpanOffsets;
            this.mSpanLookupSize = savedState.mSpanLookupSize;
            this.mSpanLookup = savedState.mSpanLookup;
            this.mReverseLayout = savedState.mReverseLayout;
            this.mAnchorLayoutFromEnd = savedState.mAnchorLayoutFromEnd;
            this.mLastLayoutRTL = savedState.mLastLayoutRTL;
            this.mFullSpanItems = savedState.mFullSpanItems;
        }

        public int describeContents() {
            return 0;
        }

        /* access modifiers changed from: package-private */
        public void invalidateAnchorPositionInfo() {
            this.mSpanOffsets = null;
            this.mSpanOffsetsSize = 0;
            this.mAnchorPosition = -1;
            this.mVisibleAnchorPosition = -1;
        }

        /* access modifiers changed from: package-private */
        public void invalidateSpanInfo() {
            this.mSpanOffsets = null;
            this.mSpanOffsetsSize = 0;
            this.mSpanLookupSize = 0;
            this.mSpanLookup = null;
            this.mFullSpanItems = null;
        }

        public void writeToParcel(Parcel parcel, int i) {
            int i2 = 1;
            parcel.writeInt(this.mAnchorPosition);
            parcel.writeInt(this.mVisibleAnchorPosition);
            parcel.writeInt(this.mSpanOffsetsSize);
            if (this.mSpanOffsetsSize > 0) {
                parcel.writeIntArray(this.mSpanOffsets);
            }
            parcel.writeInt(this.mSpanLookupSize);
            if (this.mSpanLookupSize > 0) {
                parcel.writeIntArray(this.mSpanLookup);
            }
            parcel.writeInt(this.mReverseLayout ? 1 : 0);
            parcel.writeInt(this.mAnchorLayoutFromEnd ? 1 : 0);
            if (!this.mLastLayoutRTL) {
                i2 = 0;
            }
            parcel.writeInt(i2);
            parcel.writeList(this.mFullSpanItems);
        }
    }

    class Span {

        /* renamed from: 连任  reason: contains not printable characters */
        final int f1782;

        /* renamed from: 靐  reason: contains not printable characters */
        int f1783 = Integer.MIN_VALUE;

        /* renamed from: 麤  reason: contains not printable characters */
        int f1784 = 0;

        /* renamed from: 齉  reason: contains not printable characters */
        int f1785 = Integer.MIN_VALUE;

        /* renamed from: 龘  reason: contains not printable characters */
        ArrayList<View> f1786 = new ArrayList<>();

        Span(int i) {
            this.f1782 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m2115() {
            this.f1783 = Integer.MIN_VALUE;
            this.f1785 = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m2116() {
            int size = this.f1786.size();
            View remove = this.f1786.remove(size - 1);
            LayoutParams r1 = m2131(remove);
            r1.mSpan = null;
            if (r1.isItemRemoved() || r1.isItemChanged()) {
                this.f1784 -= StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedMeasurement(remove);
            }
            if (size == 1) {
                this.f1783 = Integer.MIN_VALUE;
            }
            this.f1785 = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m2117() {
            View remove = this.f1786.remove(0);
            LayoutParams r0 = m2131(remove);
            r0.mSpan = null;
            if (this.f1786.size() == 0) {
                this.f1785 = Integer.MIN_VALUE;
            }
            if (r0.isItemRemoved() || r0.isItemChanged()) {
                this.f1784 -= StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedMeasurement(remove);
            }
            this.f1783 = Integer.MIN_VALUE;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m2118() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? m2135(0, this.f1786.size(), false) : m2135(this.f1786.size() - 1, -1, false);
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public int m2119() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? m2127(0, this.f1786.size(), true) : m2127(this.f1786.size() - 1, -1, true);
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public int m2120() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? m2135(this.f1786.size() - 1, -1, true) : m2135(0, this.f1786.size(), true);
        }

        /* renamed from: ˑ  reason: contains not printable characters */
        public int m2121() {
            return this.f1784;
        }

        /* renamed from: ٴ  reason: contains not printable characters */
        public int m2122() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? m2135(this.f1786.size() - 1, -1, false) : m2135(0, this.f1786.size(), false);
        }

        /* renamed from: ᐧ  reason: contains not printable characters */
        public int m2123() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? m2127(this.f1786.size() - 1, -1, true) : m2127(0, this.f1786.size(), true);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 连任  reason: contains not printable characters */
        public void m2124() {
            this.f1786.clear();
            m2115();
            this.f1784 = 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m2125() {
            if (this.f1783 != Integer.MIN_VALUE) {
                return this.f1783;
            }
            m2138();
            return this.f1783;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m2126(int i) {
            if (this.f1785 != Integer.MIN_VALUE) {
                return this.f1785;
            }
            if (this.f1786.size() == 0) {
                return i;
            }
            m2132();
            return this.f1785;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m2127(int i, int i2, boolean z) {
            return m2136(i, i2, false, false, z);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m2128(View view) {
            LayoutParams r0 = m2131(view);
            r0.mSpan = this;
            this.f1786.add(view);
            this.f1785 = Integer.MIN_VALUE;
            if (this.f1786.size() == 1) {
                this.f1783 = Integer.MIN_VALUE;
            }
            if (r0.isItemRemoved() || r0.isItemChanged()) {
                this.f1784 += StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedMeasurement(view);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public int m2129() {
            if (this.f1785 != Integer.MIN_VALUE) {
                return this.f1785;
            }
            m2132();
            return this.f1785;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public void m2130(int i) {
            if (this.f1783 != Integer.MIN_VALUE) {
                this.f1783 += i;
            }
            if (this.f1785 != Integer.MIN_VALUE) {
                this.f1785 += i;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public LayoutParams m2131(View view) {
            return (LayoutParams) view.getLayoutParams();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m2132() {
            LazySpanLookup.FullSpanItem r1;
            View view = this.f1786.get(this.f1786.size() - 1);
            LayoutParams r2 = m2131(view);
            this.f1785 = StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedEnd(view);
            if (r2.mFullSpan && (r1 = StaggeredGridLayoutManager.this.mLazySpanLookup.m2098(r2.getViewLayoutPosition())) != null && r1.f1777 == 1) {
                this.f1785 += r1.m2110(this.f1782);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m2133(int i) {
            this.f1783 = i;
            this.f1785 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m2134(int i) {
            if (this.f1783 != Integer.MIN_VALUE) {
                return this.f1783;
            }
            if (this.f1786.size() == 0) {
                return i;
            }
            m2138();
            return this.f1783;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m2135(int i, int i2, boolean z) {
            return m2136(i, i2, z, true, false);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m2136(int i, int i2, boolean z, boolean z2, boolean z3) {
            int startAfterPadding = StaggeredGridLayoutManager.this.mPrimaryOrientation.getStartAfterPadding();
            int endAfterPadding = StaggeredGridLayoutManager.this.mPrimaryOrientation.getEndAfterPadding();
            int i3 = i2 > i ? 1 : -1;
            for (int i4 = i; i4 != i2; i4 += i3) {
                View view = this.f1786.get(i4);
                int decoratedStart = StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedStart(view);
                int decoratedEnd = StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedEnd(view);
                boolean z4 = z3 ? decoratedStart <= endAfterPadding : decoratedStart < endAfterPadding;
                boolean z5 = z3 ? decoratedEnd >= startAfterPadding : decoratedEnd > startAfterPadding;
                if (z4 && z5) {
                    if (!z || !z2) {
                        if (z2) {
                            return StaggeredGridLayoutManager.this.getPosition(view);
                        }
                        if (decoratedStart < startAfterPadding || decoratedEnd > endAfterPadding) {
                            return StaggeredGridLayoutManager.this.getPosition(view);
                        }
                    } else if (decoratedStart >= startAfterPadding && decoratedEnd <= endAfterPadding) {
                        return StaggeredGridLayoutManager.this.getPosition(view);
                    }
                }
            }
            return -1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public View m2137(int i, int i2) {
            View view = null;
            if (i2 != -1) {
                for (int size = this.f1786.size() - 1; size >= 0; size--) {
                    View view2 = this.f1786.get(size);
                    if ((StaggeredGridLayoutManager.this.mReverseLayout && StaggeredGridLayoutManager.this.getPosition(view2) >= i) || ((!StaggeredGridLayoutManager.this.mReverseLayout && StaggeredGridLayoutManager.this.getPosition(view2) <= i) || !view2.hasFocusable())) {
                        break;
                    }
                    view = view2;
                }
            } else {
                int size2 = this.f1786.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    View view3 = this.f1786.get(i3);
                    if ((StaggeredGridLayoutManager.this.mReverseLayout && StaggeredGridLayoutManager.this.getPosition(view3) <= i) || ((!StaggeredGridLayoutManager.this.mReverseLayout && StaggeredGridLayoutManager.this.getPosition(view3) >= i) || !view3.hasFocusable())) {
                        break;
                    }
                    view = view3;
                }
            }
            return view;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2138() {
            LazySpanLookup.FullSpanItem r0;
            View view = this.f1786.get(0);
            LayoutParams r1 = m2131(view);
            this.f1783 = StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedStart(view);
            if (r1.mFullSpan && (r0 = StaggeredGridLayoutManager.this.mLazySpanLookup.m2098(r1.getViewLayoutPosition())) != null && r0.f1777 == -1) {
                this.f1783 -= r0.m2110(this.f1782);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2139(View view) {
            LayoutParams r0 = m2131(view);
            r0.mSpan = this;
            this.f1786.add(0, view);
            this.f1783 = Integer.MIN_VALUE;
            if (this.f1786.size() == 1) {
                this.f1785 = Integer.MIN_VALUE;
            }
            if (r0.isItemRemoved() || r0.isItemChanged()) {
                this.f1784 += StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedMeasurement(view);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2140(boolean z, int i) {
            int r0 = z ? m2126(Integer.MIN_VALUE) : m2134(Integer.MIN_VALUE);
            m2124();
            if (r0 != Integer.MIN_VALUE) {
                if (z && r0 < StaggeredGridLayoutManager.this.mPrimaryOrientation.getEndAfterPadding()) {
                    return;
                }
                if (z || r0 <= StaggeredGridLayoutManager.this.mPrimaryOrientation.getStartAfterPadding()) {
                    if (i != Integer.MIN_VALUE) {
                        r0 += i;
                    }
                    this.f1785 = r0;
                    this.f1783 = r0;
                }
            }
        }

        /* renamed from: ﹶ  reason: contains not printable characters */
        public int m2141() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? m2135(0, this.f1786.size(), true) : m2135(this.f1786.size() - 1, -1, true);
        }
    }

    public StaggeredGridLayoutManager(int i, int i2) {
        boolean z = true;
        this.mOrientation = i2;
        setSpanCount(i);
        setAutoMeasureEnabled(this.mGapStrategy == 0 ? false : z);
        this.mLayoutState = new LayoutState();
        createOrientationHelpers();
    }

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        boolean z = true;
        RecyclerView.LayoutManager.Properties properties = getProperties(context, attributeSet, i, i2);
        setOrientation(properties.orientation);
        setSpanCount(properties.spanCount);
        setReverseLayout(properties.reverseLayout);
        setAutoMeasureEnabled(this.mGapStrategy == 0 ? false : z);
        this.mLayoutState = new LayoutState();
        createOrientationHelpers();
    }

    private void appendViewToAllSpans(View view) {
        for (int i = this.mSpanCount - 1; i >= 0; i--) {
            this.mSpans[i].m2128(view);
        }
    }

    private void applyPendingSavedState(AnchorInfo anchorInfo) {
        if (this.mPendingSavedState.mSpanOffsetsSize > 0) {
            if (this.mPendingSavedState.mSpanOffsetsSize == this.mSpanCount) {
                for (int i = 0; i < this.mSpanCount; i++) {
                    this.mSpans[i].m2124();
                    int i2 = this.mPendingSavedState.mSpanOffsets[i];
                    if (i2 != Integer.MIN_VALUE) {
                        i2 = this.mPendingSavedState.mAnchorLayoutFromEnd ? i2 + this.mPrimaryOrientation.getEndAfterPadding() : i2 + this.mPrimaryOrientation.getStartAfterPadding();
                    }
                    this.mSpans[i].m2133(i2);
                }
            } else {
                this.mPendingSavedState.invalidateSpanInfo();
                this.mPendingSavedState.mAnchorPosition = this.mPendingSavedState.mVisibleAnchorPosition;
            }
        }
        this.mLastLayoutRTL = this.mPendingSavedState.mLastLayoutRTL;
        setReverseLayout(this.mPendingSavedState.mReverseLayout);
        resolveShouldLayoutReverse();
        if (this.mPendingSavedState.mAnchorPosition != -1) {
            this.mPendingScrollPosition = this.mPendingSavedState.mAnchorPosition;
            anchorInfo.f1773 = this.mPendingSavedState.mAnchorLayoutFromEnd;
        } else {
            anchorInfo.f1773 = this.mShouldReverseLayout;
        }
        if (this.mPendingSavedState.mSpanLookupSize > 1) {
            this.mLazySpanLookup.f1776 = this.mPendingSavedState.mSpanLookup;
            this.mLazySpanLookup.f1775 = this.mPendingSavedState.mFullSpanItems;
        }
    }

    private void attachViewToSpans(View view, LayoutParams layoutParams, LayoutState layoutState) {
        if (layoutState.f1648 == 1) {
            if (layoutParams.mFullSpan) {
                appendViewToAllSpans(view);
            } else {
                layoutParams.mSpan.m2128(view);
            }
        } else if (layoutParams.mFullSpan) {
            prependViewToAllSpans(view);
        } else {
            layoutParams.mSpan.m2139(view);
        }
    }

    private int calculateScrollDirectionForPosition(int i) {
        int i2 = -1;
        if (getChildCount() == 0) {
            return this.mShouldReverseLayout ? 1 : -1;
        }
        if ((i < getFirstChildPosition()) == this.mShouldReverseLayout) {
            i2 = 1;
        }
        return i2;
    }

    private boolean checkSpanForGap(Span span) {
        if (this.mShouldReverseLayout) {
            if (span.m2129() < this.mPrimaryOrientation.getEndAfterPadding()) {
                return !span.m2131(span.f1786.get(span.f1786.size() + -1)).mFullSpan;
            }
        } else if (span.m2125() > this.mPrimaryOrientation.getStartAfterPadding()) {
            return !span.m2131(span.f1786.get(0)).mFullSpan;
        }
        return false;
    }

    private int computeScrollExtent(RecyclerView.State state) {
        boolean z = true;
        if (getChildCount() == 0) {
            return 0;
        }
        OrientationHelper orientationHelper = this.mPrimaryOrientation;
        View findFirstVisibleItemClosestToStart = findFirstVisibleItemClosestToStart(!this.mSmoothScrollbarEnabled);
        if (this.mSmoothScrollbarEnabled) {
            z = false;
        }
        return ScrollbarHelper.m2079(state, orientationHelper, findFirstVisibleItemClosestToStart, findFirstVisibleItemClosestToEnd(z), this, this.mSmoothScrollbarEnabled);
    }

    private int computeScrollOffset(RecyclerView.State state) {
        boolean z = true;
        if (getChildCount() == 0) {
            return 0;
        }
        OrientationHelper orientationHelper = this.mPrimaryOrientation;
        View findFirstVisibleItemClosestToStart = findFirstVisibleItemClosestToStart(!this.mSmoothScrollbarEnabled);
        if (this.mSmoothScrollbarEnabled) {
            z = false;
        }
        return ScrollbarHelper.m2080(state, orientationHelper, findFirstVisibleItemClosestToStart, findFirstVisibleItemClosestToEnd(z), this, this.mSmoothScrollbarEnabled, this.mShouldReverseLayout);
    }

    private int computeScrollRange(RecyclerView.State state) {
        boolean z = true;
        if (getChildCount() == 0) {
            return 0;
        }
        OrientationHelper orientationHelper = this.mPrimaryOrientation;
        View findFirstVisibleItemClosestToStart = findFirstVisibleItemClosestToStart(!this.mSmoothScrollbarEnabled);
        if (this.mSmoothScrollbarEnabled) {
            z = false;
        }
        return ScrollbarHelper.m2078(state, orientationHelper, findFirstVisibleItemClosestToStart, findFirstVisibleItemClosestToEnd(z), this, this.mSmoothScrollbarEnabled);
    }

    private int convertFocusDirectionToLayoutDirection(int i) {
        int i2 = Integer.MIN_VALUE;
        int i3 = 1;
        switch (i) {
            case 1:
                return (this.mOrientation == 1 || !isLayoutRTL()) ? -1 : 1;
            case 2:
                if (this.mOrientation == 1) {
                    return 1;
                }
                return !isLayoutRTL() ? 1 : -1;
            case 17:
                return this.mOrientation != 0 ? Integer.MIN_VALUE : -1;
            case 33:
                return this.mOrientation != 1 ? Integer.MIN_VALUE : -1;
            case 66:
                if (this.mOrientation != 0) {
                    i3 = Integer.MIN_VALUE;
                }
                return i3;
            case 130:
                if (this.mOrientation == 1) {
                    i2 = 1;
                }
                return i2;
            default:
                return Integer.MIN_VALUE;
        }
    }

    private LazySpanLookup.FullSpanItem createFullSpanItemFromEnd(int i) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.f1779 = new int[this.mSpanCount];
        for (int i2 = 0; i2 < this.mSpanCount; i2++) {
            fullSpanItem.f1779[i2] = i - this.mSpans[i2].m2126(i);
        }
        return fullSpanItem;
    }

    private LazySpanLookup.FullSpanItem createFullSpanItemFromStart(int i) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.f1779 = new int[this.mSpanCount];
        for (int i2 = 0; i2 < this.mSpanCount; i2++) {
            fullSpanItem.f1779[i2] = this.mSpans[i2].m2134(i) - i;
        }
        return fullSpanItem;
    }

    private void createOrientationHelpers() {
        this.mPrimaryOrientation = OrientationHelper.createOrientationHelper(this, this.mOrientation);
        this.mSecondaryOrientation = OrientationHelper.createOrientationHelper(this, 1 - this.mOrientation);
    }

    private int fill(RecyclerView.Recycler recycler, LayoutState layoutState, RecyclerView.State state) {
        Span span;
        int minStart;
        int decoratedMeasurement;
        int startAfterPadding;
        int decoratedMeasurement2;
        this.mRemainingSpans.set(0, this.mSpanCount, true);
        int i = this.mLayoutState.f1647 ? layoutState.f1648 == 1 ? MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT : Integer.MIN_VALUE : layoutState.f1648 == 1 ? layoutState.f1645 + layoutState.f1649 : layoutState.f1644 - layoutState.f1649;
        updateAllRemainingSpans(layoutState.f1648, i);
        int endAfterPadding = this.mShouldReverseLayout ? this.mPrimaryOrientation.getEndAfterPadding() : this.mPrimaryOrientation.getStartAfterPadding();
        boolean z = false;
        while (layoutState.m1966(state) && (this.mLayoutState.f1647 || !this.mRemainingSpans.isEmpty())) {
            View r4 = layoutState.m1965(recycler);
            LayoutParams layoutParams = (LayoutParams) r4.getLayoutParams();
            int viewLayoutPosition = layoutParams.getViewLayoutPosition();
            int r26 = this.mLazySpanLookup.m2103(viewLayoutPosition);
            boolean z2 = r26 == -1;
            if (z2) {
                span = layoutParams.mFullSpan ? this.mSpans[0] : getNextSpan(layoutState);
                this.mLazySpanLookup.m2108(viewLayoutPosition, span);
            } else {
                span = this.mSpans[r26];
            }
            layoutParams.mSpan = span;
            if (layoutState.f1648 == 1) {
                addView(r4);
            } else {
                addView(r4, 0);
            }
            measureChildWithDecorationsAndMargin(r4, layoutParams, false);
            if (layoutState.f1648 == 1) {
                decoratedMeasurement = layoutParams.mFullSpan ? getMaxEnd(endAfterPadding) : span.m2126(endAfterPadding);
                minStart = decoratedMeasurement + this.mPrimaryOrientation.getDecoratedMeasurement(r4);
                if (z2 && layoutParams.mFullSpan) {
                    LazySpanLookup.FullSpanItem createFullSpanItemFromEnd = createFullSpanItemFromEnd(decoratedMeasurement);
                    createFullSpanItemFromEnd.f1777 = -1;
                    createFullSpanItemFromEnd.f1780 = viewLayoutPosition;
                    this.mLazySpanLookup.m2109(createFullSpanItemFromEnd);
                }
            } else {
                minStart = layoutParams.mFullSpan ? getMinStart(endAfterPadding) : span.m2134(endAfterPadding);
                decoratedMeasurement = minStart - this.mPrimaryOrientation.getDecoratedMeasurement(r4);
                if (z2 && layoutParams.mFullSpan) {
                    LazySpanLookup.FullSpanItem createFullSpanItemFromStart = createFullSpanItemFromStart(minStart);
                    createFullSpanItemFromStart.f1777 = 1;
                    createFullSpanItemFromStart.f1780 = viewLayoutPosition;
                    this.mLazySpanLookup.m2109(createFullSpanItemFromStart);
                }
            }
            if (layoutParams.mFullSpan && layoutState.f1650 == -1) {
                if (z2) {
                    this.mLaidOutInvalidFullSpan = true;
                } else {
                    if (layoutState.f1648 == 1 ? !areAllEndsEqual() : !areAllStartsEqual()) {
                        LazySpanLookup.FullSpanItem r20 = this.mLazySpanLookup.m2098(viewLayoutPosition);
                        if (r20 != null) {
                            r20.f1778 = true;
                        }
                        this.mLaidOutInvalidFullSpan = true;
                    }
                }
            }
            attachViewToSpans(r4, layoutParams, layoutState);
            if (!isLayoutRTL() || this.mOrientation != 1) {
                startAfterPadding = layoutParams.mFullSpan ? this.mSecondaryOrientation.getStartAfterPadding() : (span.f1782 * this.mSizePerSpan) + this.mSecondaryOrientation.getStartAfterPadding();
                decoratedMeasurement2 = startAfterPadding + this.mSecondaryOrientation.getDecoratedMeasurement(r4);
            } else {
                decoratedMeasurement2 = layoutParams.mFullSpan ? this.mSecondaryOrientation.getEndAfterPadding() : this.mSecondaryOrientation.getEndAfterPadding() - (((this.mSpanCount - 1) - span.f1782) * this.mSizePerSpan);
                startAfterPadding = decoratedMeasurement2 - this.mSecondaryOrientation.getDecoratedMeasurement(r4);
            }
            if (this.mOrientation == 1) {
                layoutDecoratedWithMargins(r4, startAfterPadding, decoratedMeasurement, decoratedMeasurement2, minStart);
            } else {
                layoutDecoratedWithMargins(r4, decoratedMeasurement, startAfterPadding, minStart, decoratedMeasurement2);
            }
            if (layoutParams.mFullSpan) {
                updateAllRemainingSpans(this.mLayoutState.f1648, i);
            } else {
                updateRemainingSpans(span, this.mLayoutState.f1648, i);
            }
            recycle(recycler, this.mLayoutState);
            if (this.mLayoutState.f1646 && r4.hasFocusable()) {
                if (layoutParams.mFullSpan) {
                    this.mRemainingSpans.clear();
                } else {
                    this.mRemainingSpans.set(span.f1782, false);
                }
            }
            z = true;
        }
        if (!z) {
            recycle(recycler, this.mLayoutState);
        }
        int startAfterPadding2 = this.mLayoutState.f1648 == -1 ? this.mPrimaryOrientation.getStartAfterPadding() - getMinStart(this.mPrimaryOrientation.getStartAfterPadding()) : getMaxEnd(this.mPrimaryOrientation.getEndAfterPadding()) - this.mPrimaryOrientation.getEndAfterPadding();
        if (startAfterPadding2 > 0) {
            return Math.min(layoutState.f1649, startAfterPadding2);
        }
        return 0;
    }

    private int findFirstReferenceChildPosition(int i) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            int position = getPosition(getChildAt(i2));
            if (position >= 0 && position < i) {
                return position;
            }
        }
        return 0;
    }

    private int findLastReferenceChildPosition(int i) {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            int position = getPosition(getChildAt(childCount));
            if (position >= 0 && position < i) {
                return position;
            }
        }
        return 0;
    }

    private void fixEndGap(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        int endAfterPadding;
        int maxEnd = getMaxEnd(Integer.MIN_VALUE);
        if (maxEnd != Integer.MIN_VALUE && (endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding() - maxEnd) > 0) {
            int i = endAfterPadding - (-scrollBy(-endAfterPadding, recycler, state));
            if (z && i > 0) {
                this.mPrimaryOrientation.offsetChildren(i);
            }
        }
    }

    private void fixStartGap(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        int startAfterPadding;
        int minStart = getMinStart(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
        if (minStart != Integer.MAX_VALUE && (startAfterPadding = minStart - this.mPrimaryOrientation.getStartAfterPadding()) > 0) {
            int scrollBy = startAfterPadding - scrollBy(startAfterPadding, recycler, state);
            if (z && scrollBy > 0) {
                this.mPrimaryOrientation.offsetChildren(-scrollBy);
            }
        }
    }

    private int getMaxEnd(int i) {
        int r1 = this.mSpans[0].m2126(i);
        for (int i2 = 1; i2 < this.mSpanCount; i2++) {
            int r2 = this.mSpans[i2].m2126(i);
            if (r2 > r1) {
                r1 = r2;
            }
        }
        return r1;
    }

    private int getMaxStart(int i) {
        int r1 = this.mSpans[0].m2134(i);
        for (int i2 = 1; i2 < this.mSpanCount; i2++) {
            int r2 = this.mSpans[i2].m2134(i);
            if (r2 > r1) {
                r1 = r2;
            }
        }
        return r1;
    }

    private int getMinEnd(int i) {
        int r1 = this.mSpans[0].m2126(i);
        for (int i2 = 1; i2 < this.mSpanCount; i2++) {
            int r2 = this.mSpans[i2].m2126(i);
            if (r2 < r1) {
                r1 = r2;
            }
        }
        return r1;
    }

    private int getMinStart(int i) {
        int r1 = this.mSpans[0].m2134(i);
        for (int i2 = 1; i2 < this.mSpanCount; i2++) {
            int r2 = this.mSpans[i2].m2134(i);
            if (r2 < r1) {
                r1 = r2;
            }
        }
        return r1;
    }

    private Span getNextSpan(LayoutState layoutState) {
        int i;
        int i2;
        int i3;
        if (preferLastSpan(layoutState.f1648)) {
            i = this.mSpanCount - 1;
            i2 = -1;
            i3 = -1;
        } else {
            i = 0;
            i2 = this.mSpanCount;
            i3 = 1;
        }
        if (layoutState.f1648 == 1) {
            Span span = null;
            int i4 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
            int startAfterPadding = this.mPrimaryOrientation.getStartAfterPadding();
            for (int i5 = i; i5 != i2; i5 += i3) {
                Span span2 = this.mSpans[i5];
                int r9 = span2.m2126(startAfterPadding);
                if (r9 < i4) {
                    span = span2;
                    i4 = r9;
                }
            }
            return span;
        }
        Span span3 = null;
        int i6 = Integer.MIN_VALUE;
        int endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding();
        for (int i7 = i; i7 != i2; i7 += i3) {
            Span span4 = this.mSpans[i7];
            int r92 = span4.m2134(endAfterPadding);
            if (r92 > i6) {
                span3 = span4;
                i6 = r92;
            }
        }
        return span3;
    }

    private void handleUpdate(int i, int i2, int i3) {
        int i4;
        int i5;
        int lastChildPosition = this.mShouldReverseLayout ? getLastChildPosition() : getFirstChildPosition();
        if (i3 != 8) {
            i4 = i;
            i5 = i + i2;
        } else if (i < i2) {
            i5 = i2 + 1;
            i4 = i;
        } else {
            i5 = i + 1;
            i4 = i2;
        }
        this.mLazySpanLookup.m2100(i4);
        switch (i3) {
            case 1:
                this.mLazySpanLookup.m2101(i, i2);
                break;
            case 2:
                this.mLazySpanLookup.m2107(i, i2);
                break;
            case 8:
                this.mLazySpanLookup.m2107(i, 1);
                this.mLazySpanLookup.m2101(i2, 1);
                break;
        }
        if (i5 > lastChildPosition) {
            if (i4 <= (this.mShouldReverseLayout ? getFirstChildPosition() : getLastChildPosition())) {
                requestLayout();
            }
        }
    }

    private void measureChildWithDecorationsAndMargin(View view, int i, int i2, boolean z) {
        calculateItemDecorationsForChild(view, this.mTmpRect);
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int updateSpecWithExtra = updateSpecWithExtra(i, layoutParams.leftMargin + this.mTmpRect.left, layoutParams.rightMargin + this.mTmpRect.right);
        int updateSpecWithExtra2 = updateSpecWithExtra(i2, layoutParams.topMargin + this.mTmpRect.top, layoutParams.bottomMargin + this.mTmpRect.bottom);
        if (z ? shouldReMeasureChild(view, updateSpecWithExtra, updateSpecWithExtra2, layoutParams) : shouldMeasureChild(view, updateSpecWithExtra, updateSpecWithExtra2, layoutParams)) {
            view.measure(updateSpecWithExtra, updateSpecWithExtra2);
        }
    }

    private void measureChildWithDecorationsAndMargin(View view, LayoutParams layoutParams, boolean z) {
        if (layoutParams.mFullSpan) {
            if (this.mOrientation == 1) {
                measureChildWithDecorationsAndMargin(view, this.mFullSizeSpec, getChildMeasureSpec(getHeight(), getHeightMode(), 0, layoutParams.height, true), z);
            } else {
                measureChildWithDecorationsAndMargin(view, getChildMeasureSpec(getWidth(), getWidthMode(), 0, layoutParams.width, true), this.mFullSizeSpec, z);
            }
        } else if (this.mOrientation == 1) {
            measureChildWithDecorationsAndMargin(view, getChildMeasureSpec(this.mSizePerSpan, getWidthMode(), 0, layoutParams.width, false), getChildMeasureSpec(getHeight(), getHeightMode(), 0, layoutParams.height, true), z);
        } else {
            measureChildWithDecorationsAndMargin(view, getChildMeasureSpec(getWidth(), getWidthMode(), 0, layoutParams.width, true), getChildMeasureSpec(this.mSizePerSpan, getHeightMode(), 0, layoutParams.height, false), z);
        }
    }

    private void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        boolean z2 = true;
        AnchorInfo anchorInfo = this.mAnchorInfo;
        if (!(this.mPendingSavedState == null && this.mPendingScrollPosition == -1) && state.getItemCount() == 0) {
            removeAndRecycleAllViews(recycler);
            anchorInfo.m2092();
            return;
        }
        boolean z3 = (anchorInfo.f1770 && this.mPendingScrollPosition == -1 && this.mPendingSavedState == null) ? false : true;
        if (z3) {
            anchorInfo.m2092();
            if (this.mPendingSavedState != null) {
                applyPendingSavedState(anchorInfo);
            } else {
                resolveShouldLayoutReverse();
                anchorInfo.f1773 = this.mShouldReverseLayout;
            }
            updateAnchorInfoForLayout(state, anchorInfo);
            anchorInfo.f1770 = true;
        }
        if (this.mPendingSavedState == null && this.mPendingScrollPosition == -1 && !(anchorInfo.f1773 == this.mLastLayoutFromEnd && isLayoutRTL() == this.mLastLayoutRTL)) {
            this.mLazySpanLookup.m2106();
            anchorInfo.f1772 = true;
        }
        if (getChildCount() > 0 && (this.mPendingSavedState == null || this.mPendingSavedState.mSpanOffsetsSize < 1)) {
            if (anchorInfo.f1772) {
                for (int i = 0; i < this.mSpanCount; i++) {
                    this.mSpans[i].m2124();
                    if (anchorInfo.f1771 != Integer.MIN_VALUE) {
                        this.mSpans[i].m2133(anchorInfo.f1771);
                    }
                }
            } else if (z3 || this.mAnchorInfo.f1768 == null) {
                for (int i2 = 0; i2 < this.mSpanCount; i2++) {
                    this.mSpans[i2].m2140(this.mShouldReverseLayout, anchorInfo.f1771);
                }
                this.mAnchorInfo.m2094(this.mSpans);
            } else {
                for (int i3 = 0; i3 < this.mSpanCount; i3++) {
                    Span span = this.mSpans[i3];
                    span.m2124();
                    span.m2133(this.mAnchorInfo.f1768[i3]);
                }
            }
        }
        detachAndScrapAttachedViews(recycler);
        this.mLayoutState.f1652 = false;
        this.mLaidOutInvalidFullSpan = false;
        updateMeasureSpecs(this.mSecondaryOrientation.getTotalSpace());
        updateLayoutState(anchorInfo.f1774, state);
        if (anchorInfo.f1773) {
            setLayoutStateDirection(-1);
            fill(recycler, this.mLayoutState, state);
            setLayoutStateDirection(1);
            this.mLayoutState.f1651 = anchorInfo.f1774 + this.mLayoutState.f1650;
            fill(recycler, this.mLayoutState, state);
        } else {
            setLayoutStateDirection(1);
            fill(recycler, this.mLayoutState, state);
            setLayoutStateDirection(-1);
            this.mLayoutState.f1651 = anchorInfo.f1774 + this.mLayoutState.f1650;
            fill(recycler, this.mLayoutState, state);
        }
        repositionToWrapContentIfNecessary();
        if (getChildCount() > 0) {
            if (this.mShouldReverseLayout) {
                fixEndGap(recycler, state, true);
                fixStartGap(recycler, state, false);
            } else {
                fixStartGap(recycler, state, true);
                fixEndGap(recycler, state, false);
            }
        }
        boolean z4 = false;
        if (z && !state.isPreLayout()) {
            if (this.mGapStrategy == 0 || getChildCount() <= 0 || (!this.mLaidOutInvalidFullSpan && hasGapsToFix() == null)) {
                z2 = false;
            }
            if (z2) {
                removeCallbacks(this.mCheckForGapsRunnable);
                if (checkForGaps()) {
                    z4 = true;
                }
            }
        }
        if (state.isPreLayout()) {
            this.mAnchorInfo.m2092();
        }
        this.mLastLayoutFromEnd = anchorInfo.f1773;
        this.mLastLayoutRTL = isLayoutRTL();
        if (z4) {
            this.mAnchorInfo.m2092();
            onLayoutChildren(recycler, state, false);
        }
    }

    private boolean preferLastSpan(int i) {
        if (this.mOrientation == 0) {
            return (i == -1) != this.mShouldReverseLayout;
        }
        return ((i == -1) == this.mShouldReverseLayout) == isLayoutRTL();
    }

    private void prependViewToAllSpans(View view) {
        for (int i = this.mSpanCount - 1; i >= 0; i--) {
            this.mSpans[i].m2139(view);
        }
    }

    private void recycle(RecyclerView.Recycler recycler, LayoutState layoutState) {
        if (layoutState.f1652 && !layoutState.f1647) {
            if (layoutState.f1649 == 0) {
                if (layoutState.f1648 == -1) {
                    recycleFromEnd(recycler, layoutState.f1645);
                } else {
                    recycleFromStart(recycler, layoutState.f1644);
                }
            } else if (layoutState.f1648 == -1) {
                int maxStart = layoutState.f1644 - getMaxStart(layoutState.f1644);
                recycleFromEnd(recycler, maxStart < 0 ? layoutState.f1645 : layoutState.f1645 - Math.min(maxStart, layoutState.f1649));
            } else {
                int minEnd = getMinEnd(layoutState.f1645) - layoutState.f1645;
                recycleFromStart(recycler, minEnd < 0 ? layoutState.f1644 : layoutState.f1644 + Math.min(minEnd, layoutState.f1649));
            }
        }
    }

    private void recycleFromEnd(RecyclerView.Recycler recycler, int i) {
        int childCount = getChildCount() - 1;
        while (childCount >= 0) {
            View childAt = getChildAt(childCount);
            if (this.mPrimaryOrientation.getDecoratedStart(childAt) >= i && this.mPrimaryOrientation.getTransformedStartWithDecoration(childAt) >= i) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.mFullSpan) {
                    int i2 = 0;
                    while (i2 < this.mSpanCount) {
                        if (this.mSpans[i2].f1786.size() != 1) {
                            i2++;
                        } else {
                            return;
                        }
                    }
                    for (int i3 = 0; i3 < this.mSpanCount; i3++) {
                        this.mSpans[i3].m2116();
                    }
                } else if (layoutParams.mSpan.f1786.size() != 1) {
                    layoutParams.mSpan.m2116();
                } else {
                    return;
                }
                removeAndRecycleView(childAt, recycler);
                childCount--;
            } else {
                return;
            }
        }
    }

    private void recycleFromStart(RecyclerView.Recycler recycler, int i) {
        while (getChildCount() > 0) {
            View childAt = getChildAt(0);
            if (this.mPrimaryOrientation.getDecoratedEnd(childAt) <= i && this.mPrimaryOrientation.getTransformedEndWithDecoration(childAt) <= i) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.mFullSpan) {
                    int i2 = 0;
                    while (i2 < this.mSpanCount) {
                        if (this.mSpans[i2].f1786.size() != 1) {
                            i2++;
                        } else {
                            return;
                        }
                    }
                    for (int i3 = 0; i3 < this.mSpanCount; i3++) {
                        this.mSpans[i3].m2117();
                    }
                } else if (layoutParams.mSpan.f1786.size() != 1) {
                    layoutParams.mSpan.m2117();
                } else {
                    return;
                }
                removeAndRecycleView(childAt, recycler);
            } else {
                return;
            }
        }
    }

    private void repositionToWrapContentIfNecessary() {
        if (this.mSecondaryOrientation.getMode() != 1073741824) {
            float f = 0.0f;
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = getChildAt(i);
                float decoratedMeasurement = (float) this.mSecondaryOrientation.getDecoratedMeasurement(childAt);
                if (decoratedMeasurement >= f) {
                    if (((LayoutParams) childAt.getLayoutParams()).isFullSpan()) {
                        decoratedMeasurement = (1.0f * decoratedMeasurement) / ((float) this.mSpanCount);
                    }
                    f = Math.max(f, decoratedMeasurement);
                }
            }
            int i2 = this.mSizePerSpan;
            int round = Math.round(((float) this.mSpanCount) * f);
            if (this.mSecondaryOrientation.getMode() == Integer.MIN_VALUE) {
                round = Math.min(round, this.mSecondaryOrientation.getTotalSpace());
            }
            updateMeasureSpecs(round);
            if (this.mSizePerSpan != i2) {
                for (int i3 = 0; i3 < childCount; i3++) {
                    View childAt2 = getChildAt(i3);
                    LayoutParams layoutParams = (LayoutParams) childAt2.getLayoutParams();
                    if (!layoutParams.mFullSpan) {
                        if (!isLayoutRTL() || this.mOrientation != 1) {
                            int i4 = layoutParams.mSpan.f1782 * this.mSizePerSpan;
                            int i5 = layoutParams.mSpan.f1782 * i2;
                            if (this.mOrientation == 1) {
                                childAt2.offsetLeftAndRight(i4 - i5);
                            } else {
                                childAt2.offsetTopAndBottom(i4 - i5);
                            }
                        } else {
                            childAt2.offsetLeftAndRight(((-((this.mSpanCount - 1) - layoutParams.mSpan.f1782)) * this.mSizePerSpan) - ((-((this.mSpanCount - 1) - layoutParams.mSpan.f1782)) * i2));
                        }
                    }
                }
            }
        }
    }

    private void resolveShouldLayoutReverse() {
        boolean z = true;
        if (this.mOrientation == 1 || !isLayoutRTL()) {
            this.mShouldReverseLayout = this.mReverseLayout;
            return;
        }
        if (this.mReverseLayout) {
            z = false;
        }
        this.mShouldReverseLayout = z;
    }

    private void setLayoutStateDirection(int i) {
        int i2 = 1;
        this.mLayoutState.f1648 = i;
        LayoutState layoutState = this.mLayoutState;
        if (this.mShouldReverseLayout != (i == -1)) {
            i2 = -1;
        }
        layoutState.f1650 = i2;
    }

    private void updateAllRemainingSpans(int i, int i2) {
        for (int i3 = 0; i3 < this.mSpanCount; i3++) {
            if (!this.mSpans[i3].f1786.isEmpty()) {
                updateRemainingSpans(this.mSpans[i3], i, i2);
            }
        }
    }

    private boolean updateAnchorFromChildren(RecyclerView.State state, AnchorInfo anchorInfo) {
        anchorInfo.f1774 = this.mLastLayoutFromEnd ? findLastReferenceChildPosition(state.getItemCount()) : findFirstReferenceChildPosition(state.getItemCount());
        anchorInfo.f1771 = Integer.MIN_VALUE;
        return true;
    }

    private void updateLayoutState(int i, RecyclerView.State state) {
        int targetScrollPosition;
        boolean z = true;
        this.mLayoutState.f1649 = 0;
        this.mLayoutState.f1651 = i;
        int i2 = 0;
        int i3 = 0;
        if (isSmoothScrolling() && (targetScrollPosition = state.getTargetScrollPosition()) != -1) {
            if (this.mShouldReverseLayout == (targetScrollPosition < i)) {
                i3 = this.mPrimaryOrientation.getTotalSpace();
            } else {
                i2 = this.mPrimaryOrientation.getTotalSpace();
            }
        }
        if (getClipToPadding()) {
            this.mLayoutState.f1644 = this.mPrimaryOrientation.getStartAfterPadding() - i2;
            this.mLayoutState.f1645 = this.mPrimaryOrientation.getEndAfterPadding() + i3;
        } else {
            this.mLayoutState.f1645 = this.mPrimaryOrientation.getEnd() + i3;
            this.mLayoutState.f1644 = -i2;
        }
        this.mLayoutState.f1646 = false;
        this.mLayoutState.f1652 = true;
        LayoutState layoutState = this.mLayoutState;
        if (!(this.mPrimaryOrientation.getMode() == 0 && this.mPrimaryOrientation.getEnd() == 0)) {
            z = false;
        }
        layoutState.f1647 = z;
    }

    private void updateRemainingSpans(Span span, int i, int i2) {
        int r0 = span.m2121();
        if (i == -1) {
            if (span.m2125() + r0 <= i2) {
                this.mRemainingSpans.set(span.f1782, false);
            }
        } else if (span.m2129() - r0 >= i2) {
            this.mRemainingSpans.set(span.f1782, false);
        }
    }

    private int updateSpecWithExtra(int i, int i2, int i3) {
        if (i2 == 0 && i3 == 0) {
            return i;
        }
        int mode = View.MeasureSpec.getMode(i);
        return (mode == Integer.MIN_VALUE || mode == 1073741824) ? View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i) - i2) - i3), mode) : i;
    }

    /* access modifiers changed from: package-private */
    public boolean areAllEndsEqual() {
        int r0 = this.mSpans[0].m2126(Integer.MIN_VALUE);
        for (int i = 1; i < this.mSpanCount; i++) {
            if (this.mSpans[i].m2126(Integer.MIN_VALUE) != r0) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean areAllStartsEqual() {
        int r1 = this.mSpans[0].m2134(Integer.MIN_VALUE);
        for (int i = 1; i < this.mSpanCount; i++) {
            if (this.mSpans[i].m2134(Integer.MIN_VALUE) != r1) {
                return false;
            }
        }
        return true;
    }

    public void assertNotInLayoutOrScroll(String str) {
        if (this.mPendingSavedState == null) {
            super.assertNotInLayoutOrScroll(str);
        }
    }

    public boolean canScrollHorizontally() {
        return this.mOrientation == 0;
    }

    public boolean canScrollVertically() {
        return this.mOrientation == 1;
    }

    /* access modifiers changed from: package-private */
    public boolean checkForGaps() {
        int firstChildPosition;
        int lastChildPosition;
        if (getChildCount() == 0 || this.mGapStrategy == 0 || !isAttachedToWindow()) {
            return false;
        }
        if (this.mShouldReverseLayout) {
            firstChildPosition = getLastChildPosition();
            lastChildPosition = getFirstChildPosition();
        } else {
            firstChildPosition = getFirstChildPosition();
            lastChildPosition = getLastChildPosition();
        }
        if (firstChildPosition == 0 && hasGapsToFix() != null) {
            this.mLazySpanLookup.m2106();
            requestSimpleAnimationsInNextLayout();
            requestLayout();
            return true;
        } else if (!this.mLaidOutInvalidFullSpan) {
            return false;
        } else {
            int i = this.mShouldReverseLayout ? -1 : 1;
            LazySpanLookup.FullSpanItem r1 = this.mLazySpanLookup.m2105(firstChildPosition, lastChildPosition + 1, i, true);
            if (r1 == null) {
                this.mLaidOutInvalidFullSpan = false;
                this.mLazySpanLookup.m2104(lastChildPosition + 1);
                return false;
            }
            LazySpanLookup.FullSpanItem r5 = this.mLazySpanLookup.m2105(firstChildPosition, r1.f1780, i * -1, true);
            if (r5 == null) {
                this.mLazySpanLookup.m2104(r1.f1780);
            } else {
                this.mLazySpanLookup.m2104(r5.f1780 + 1);
            }
            requestSimpleAnimationsInNextLayout();
            requestLayout();
            return true;
        }
    }

    public boolean checkLayoutParams(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void collectAdjacentPrefetchPositions(int i, int i2, RecyclerView.State state, RecyclerView.LayoutManager.LayoutPrefetchRegistry layoutPrefetchRegistry) {
        int i3 = this.mOrientation == 0 ? i : i2;
        if (getChildCount() != 0 && i3 != 0) {
            prepareLayoutStateForDelta(i3, state);
            if (this.mPrefetchDistances == null || this.mPrefetchDistances.length < this.mSpanCount) {
                this.mPrefetchDistances = new int[this.mSpanCount];
            }
            int i4 = 0;
            for (int i5 = 0; i5 < this.mSpanCount; i5++) {
                int r1 = this.mLayoutState.f1650 == -1 ? this.mLayoutState.f1644 - this.mSpans[i5].m2134(this.mLayoutState.f1644) : this.mSpans[i5].m2126(this.mLayoutState.f1645) - this.mLayoutState.f1645;
                if (r1 >= 0) {
                    this.mPrefetchDistances[i4] = r1;
                    i4++;
                }
            }
            Arrays.sort(this.mPrefetchDistances, 0, i4);
            for (int i6 = 0; i6 < i4 && this.mLayoutState.m1966(state); i6++) {
                layoutPrefetchRegistry.addPosition(this.mLayoutState.f1651, this.mPrefetchDistances[i6]);
                this.mLayoutState.f1651 += this.mLayoutState.f1650;
            }
        }
    }

    public int computeHorizontalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    public int computeHorizontalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    public int computeHorizontalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    public PointF computeScrollVectorForPosition(int i) {
        int calculateScrollDirectionForPosition = calculateScrollDirectionForPosition(i);
        PointF pointF = new PointF();
        if (calculateScrollDirectionForPosition == 0) {
            return null;
        }
        if (this.mOrientation == 0) {
            pointF.x = (float) calculateScrollDirectionForPosition;
            pointF.y = 0.0f;
            return pointF;
        }
        pointF.x = 0.0f;
        pointF.y = (float) calculateScrollDirectionForPosition;
        return pointF;
    }

    public int computeVerticalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    public int computeVerticalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    public int computeVerticalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    public int[] findFirstCompletelyVisibleItemPositions(int[] iArr) {
        if (iArr == null) {
            iArr = new int[this.mSpanCount];
        } else if (iArr.length < this.mSpanCount) {
            throw new IllegalArgumentException("Provided int[]'s size must be more than or equal to span count. Expected:" + this.mSpanCount + ", array size:" + iArr.length);
        }
        for (int i = 0; i < this.mSpanCount; i++) {
            iArr[i] = this.mSpans[i].m2120();
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public View findFirstVisibleItemClosestToEnd(boolean z) {
        int startAfterPadding = this.mPrimaryOrientation.getStartAfterPadding();
        int endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding();
        View view = null;
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            int decoratedStart = this.mPrimaryOrientation.getDecoratedStart(childAt);
            int decoratedEnd = this.mPrimaryOrientation.getDecoratedEnd(childAt);
            if (decoratedEnd > startAfterPadding && decoratedStart < endAfterPadding) {
                if (decoratedEnd <= endAfterPadding || !z) {
                    return childAt;
                }
                if (view == null) {
                    view = childAt;
                }
            }
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    public View findFirstVisibleItemClosestToStart(boolean z) {
        int startAfterPadding = this.mPrimaryOrientation.getStartAfterPadding();
        int endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding();
        int childCount = getChildCount();
        View view = null;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            int decoratedStart = this.mPrimaryOrientation.getDecoratedStart(childAt);
            if (this.mPrimaryOrientation.getDecoratedEnd(childAt) > startAfterPadding && decoratedStart < endAfterPadding) {
                if (decoratedStart >= startAfterPadding || !z) {
                    return childAt;
                }
                if (view == null) {
                    view = childAt;
                }
            }
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    public int findFirstVisibleItemPositionInt() {
        View findFirstVisibleItemClosestToEnd = this.mShouldReverseLayout ? findFirstVisibleItemClosestToEnd(true) : findFirstVisibleItemClosestToStart(true);
        if (findFirstVisibleItemClosestToEnd == null) {
            return -1;
        }
        return getPosition(findFirstVisibleItemClosestToEnd);
    }

    public int[] findFirstVisibleItemPositions(int[] iArr) {
        if (iArr == null) {
            iArr = new int[this.mSpanCount];
        } else if (iArr.length < this.mSpanCount) {
            throw new IllegalArgumentException("Provided int[]'s size must be more than or equal to span count. Expected:" + this.mSpanCount + ", array size:" + iArr.length);
        }
        for (int i = 0; i < this.mSpanCount; i++) {
            iArr[i] = this.mSpans[i].m2122();
        }
        return iArr;
    }

    public int[] findLastCompletelyVisibleItemPositions(int[] iArr) {
        if (iArr == null) {
            iArr = new int[this.mSpanCount];
        } else if (iArr.length < this.mSpanCount) {
            throw new IllegalArgumentException("Provided int[]'s size must be more than or equal to span count. Expected:" + this.mSpanCount + ", array size:" + iArr.length);
        }
        for (int i = 0; i < this.mSpanCount; i++) {
            iArr[i] = this.mSpans[i].m2141();
        }
        return iArr;
    }

    public int[] findLastVisibleItemPositions(int[] iArr) {
        if (iArr == null) {
            iArr = new int[this.mSpanCount];
        } else if (iArr.length < this.mSpanCount) {
            throw new IllegalArgumentException("Provided int[]'s size must be more than or equal to span count. Expected:" + this.mSpanCount + ", array size:" + iArr.length);
        }
        for (int i = 0; i < this.mSpanCount; i++) {
            iArr[i] = this.mSpans[i].m2118();
        }
        return iArr;
    }

    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return this.mOrientation == 0 ? new LayoutParams(-2, -1) : new LayoutParams(-1, -2);
    }

    public RecyclerView.LayoutParams generateLayoutParams(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    public int getColumnCountForAccessibility(RecyclerView.Recycler recycler, RecyclerView.State state) {
        return this.mOrientation == 1 ? this.mSpanCount : super.getColumnCountForAccessibility(recycler, state);
    }

    /* access modifiers changed from: package-private */
    public int getFirstChildPosition() {
        if (getChildCount() == 0) {
            return 0;
        }
        return getPosition(getChildAt(0));
    }

    public int getGapStrategy() {
        return this.mGapStrategy;
    }

    /* access modifiers changed from: package-private */
    public int getLastChildPosition() {
        int childCount = getChildCount();
        if (childCount == 0) {
            return 0;
        }
        return getPosition(getChildAt(childCount - 1));
    }

    public int getOrientation() {
        return this.mOrientation;
    }

    public boolean getReverseLayout() {
        return this.mReverseLayout;
    }

    public int getRowCountForAccessibility(RecyclerView.Recycler recycler, RecyclerView.State state) {
        return this.mOrientation == 0 ? this.mSpanCount : super.getRowCountForAccessibility(recycler, state);
    }

    public int getSpanCount() {
        return this.mSpanCount;
    }

    /* access modifiers changed from: package-private */
    public View hasGapsToFix() {
        int i;
        int i2;
        int childCount = getChildCount() - 1;
        BitSet bitSet = new BitSet(this.mSpanCount);
        bitSet.set(0, this.mSpanCount, true);
        char c = (this.mOrientation != 1 || !isLayoutRTL()) ? (char) 65535 : 1;
        if (this.mShouldReverseLayout) {
            i = childCount;
            i2 = 0 - 1;
        } else {
            i = 0;
            i2 = childCount + 1;
        }
        int i3 = i < i2 ? 1 : -1;
        for (int i4 = i; i4 != i2; i4 += i3) {
            View childAt = getChildAt(i4);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (bitSet.get(layoutParams.mSpan.f1782)) {
                if (checkSpanForGap(layoutParams.mSpan)) {
                    return childAt;
                }
                bitSet.clear(layoutParams.mSpan.f1782);
            }
            if (!layoutParams.mFullSpan && i4 + i3 != i2) {
                View childAt2 = getChildAt(i4 + i3);
                boolean z = false;
                if (this.mShouldReverseLayout) {
                    int decoratedEnd = this.mPrimaryOrientation.getDecoratedEnd(childAt);
                    int decoratedEnd2 = this.mPrimaryOrientation.getDecoratedEnd(childAt2);
                    if (decoratedEnd < decoratedEnd2) {
                        return childAt;
                    }
                    if (decoratedEnd == decoratedEnd2) {
                        z = true;
                    }
                } else {
                    int decoratedStart = this.mPrimaryOrientation.getDecoratedStart(childAt);
                    int decoratedStart2 = this.mPrimaryOrientation.getDecoratedStart(childAt2);
                    if (decoratedStart > decoratedStart2) {
                        return childAt;
                    }
                    if (decoratedStart == decoratedStart2) {
                        z = true;
                    }
                }
                if (!z) {
                    continue;
                } else {
                    if ((layoutParams.mSpan.f1782 - ((LayoutParams) childAt2.getLayoutParams()).mSpan.f1782 < 0) != (c < 0)) {
                        return childAt;
                    }
                }
            }
        }
        return null;
    }

    public void invalidateSpanAssignments() {
        this.mLazySpanLookup.m2106();
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public boolean isLayoutRTL() {
        return getLayoutDirection() == 1;
    }

    public void offsetChildrenHorizontal(int i) {
        super.offsetChildrenHorizontal(i);
        for (int i2 = 0; i2 < this.mSpanCount; i2++) {
            this.mSpans[i2].m2130(i);
        }
    }

    public void offsetChildrenVertical(int i) {
        super.offsetChildrenVertical(i);
        for (int i2 = 0; i2 < this.mSpanCount; i2++) {
            this.mSpans[i2].m2130(i);
        }
    }

    public void onDetachedFromWindow(RecyclerView recyclerView, RecyclerView.Recycler recycler) {
        removeCallbacks(this.mCheckForGapsRunnable);
        for (int i = 0; i < this.mSpanCount; i++) {
            this.mSpans[i].m2124();
        }
        recyclerView.requestLayout();
    }

    public View onFocusSearchFailed(View view, int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        View findContainingItemView;
        View r11;
        if (getChildCount() == 0 || (findContainingItemView = findContainingItemView(view)) == null) {
            return null;
        }
        resolveShouldLayoutReverse();
        int convertFocusDirectionToLayoutDirection = convertFocusDirectionToLayoutDirection(i);
        if (convertFocusDirectionToLayoutDirection == Integer.MIN_VALUE) {
            return null;
        }
        LayoutParams layoutParams = (LayoutParams) findContainingItemView.getLayoutParams();
        boolean z = layoutParams.mFullSpan;
        Span span = layoutParams.mSpan;
        int lastChildPosition = convertFocusDirectionToLayoutDirection == 1 ? getLastChildPosition() : getFirstChildPosition();
        updateLayoutState(lastChildPosition, state);
        setLayoutStateDirection(convertFocusDirectionToLayoutDirection);
        this.mLayoutState.f1651 = this.mLayoutState.f1650 + lastChildPosition;
        this.mLayoutState.f1649 = (int) (MAX_SCROLL_FACTOR * ((float) this.mPrimaryOrientation.getTotalSpace()));
        this.mLayoutState.f1646 = true;
        this.mLayoutState.f1652 = false;
        fill(recycler, this.mLayoutState, state);
        this.mLastLayoutFromEnd = this.mShouldReverseLayout;
        if (!z && (r11 = span.m2137(lastChildPosition, convertFocusDirectionToLayoutDirection)) != null && r11 != findContainingItemView) {
            return r11;
        }
        if (preferLastSpan(convertFocusDirectionToLayoutDirection)) {
            for (int i2 = this.mSpanCount - 1; i2 >= 0; i2--) {
                View r112 = this.mSpans[i2].m2137(lastChildPosition, convertFocusDirectionToLayoutDirection);
                if (r112 != null && r112 != findContainingItemView) {
                    return r112;
                }
            }
        } else {
            for (int i3 = 0; i3 < this.mSpanCount; i3++) {
                View r113 = this.mSpans[i3].m2137(lastChildPosition, convertFocusDirectionToLayoutDirection);
                if (r113 != null && r113 != findContainingItemView) {
                    return r113;
                }
            }
        }
        boolean z2 = (!this.mReverseLayout) == (convertFocusDirectionToLayoutDirection == -1);
        if (!z) {
            View findViewByPosition = findViewByPosition(z2 ? span.m2123() : span.m2119());
            if (!(findViewByPosition == null || findViewByPosition == findContainingItemView)) {
                return findViewByPosition;
            }
        }
        if (preferLastSpan(convertFocusDirectionToLayoutDirection)) {
            for (int i4 = this.mSpanCount - 1; i4 >= 0; i4--) {
                if (i4 != span.f1782) {
                    View findViewByPosition2 = findViewByPosition(z2 ? this.mSpans[i4].m2123() : this.mSpans[i4].m2119());
                    if (!(findViewByPosition2 == null || findViewByPosition2 == findContainingItemView)) {
                        return findViewByPosition2;
                    }
                }
            }
        } else {
            for (int i5 = 0; i5 < this.mSpanCount; i5++) {
                View findViewByPosition3 = findViewByPosition(z2 ? this.mSpans[i5].m2123() : this.mSpans[i5].m2119());
                if (findViewByPosition3 != null && findViewByPosition3 != findContainingItemView) {
                    return findViewByPosition3;
                }
            }
        }
        return null;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        if (getChildCount() > 0) {
            View findFirstVisibleItemClosestToStart = findFirstVisibleItemClosestToStart(false);
            View findFirstVisibleItemClosestToEnd = findFirstVisibleItemClosestToEnd(false);
            if (findFirstVisibleItemClosestToStart != null && findFirstVisibleItemClosestToEnd != null) {
                int position = getPosition(findFirstVisibleItemClosestToStart);
                int position2 = getPosition(findFirstVisibleItemClosestToEnd);
                if (position < position2) {
                    accessibilityEvent.setFromIndex(position);
                    accessibilityEvent.setToIndex(position2);
                    return;
                }
                accessibilityEvent.setFromIndex(position2);
                accessibilityEvent.setToIndex(position);
            }
        }
    }

    public void onInitializeAccessibilityNodeInfoForItem(RecyclerView.Recycler recycler, RecyclerView.State state, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            super.onInitializeAccessibilityNodeInfoForItem(view, accessibilityNodeInfoCompat);
            return;
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        if (this.mOrientation == 0) {
            accessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(layoutParams2.getSpanIndex(), layoutParams2.mFullSpan ? this.mSpanCount : 1, -1, -1, layoutParams2.mFullSpan, false));
        } else {
            accessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(-1, -1, layoutParams2.getSpanIndex(), layoutParams2.mFullSpan ? this.mSpanCount : 1, layoutParams2.mFullSpan, false));
        }
    }

    public void onItemsAdded(RecyclerView recyclerView, int i, int i2) {
        handleUpdate(i, i2, 1);
    }

    public void onItemsChanged(RecyclerView recyclerView) {
        this.mLazySpanLookup.m2106();
        requestLayout();
    }

    public void onItemsMoved(RecyclerView recyclerView, int i, int i2, int i3) {
        handleUpdate(i, i2, 8);
    }

    public void onItemsRemoved(RecyclerView recyclerView, int i, int i2) {
        handleUpdate(i, i2, 2);
    }

    public void onItemsUpdated(RecyclerView recyclerView, int i, int i2, Object obj) {
        handleUpdate(i, i2, 4);
    }

    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        onLayoutChildren(recycler, state, true);
    }

    public void onLayoutCompleted(RecyclerView.State state) {
        super.onLayoutCompleted(state);
        this.mPendingScrollPosition = -1;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        this.mPendingSavedState = null;
        this.mAnchorInfo.m2092();
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.mPendingSavedState = (SavedState) parcelable;
            requestLayout();
        }
    }

    public Parcelable onSaveInstanceState() {
        int r1;
        if (this.mPendingSavedState != null) {
            return new SavedState(this.mPendingSavedState);
        }
        SavedState savedState = new SavedState();
        savedState.mReverseLayout = this.mReverseLayout;
        savedState.mAnchorLayoutFromEnd = this.mLastLayoutFromEnd;
        savedState.mLastLayoutRTL = this.mLastLayoutRTL;
        if (this.mLazySpanLookup == null || this.mLazySpanLookup.f1776 == null) {
            savedState.mSpanLookupSize = 0;
        } else {
            savedState.mSpanLookup = this.mLazySpanLookup.f1776;
            savedState.mSpanLookupSize = savedState.mSpanLookup.length;
            savedState.mFullSpanItems = this.mLazySpanLookup.f1775;
        }
        if (getChildCount() > 0) {
            savedState.mAnchorPosition = this.mLastLayoutFromEnd ? getLastChildPosition() : getFirstChildPosition();
            savedState.mVisibleAnchorPosition = findFirstVisibleItemPositionInt();
            savedState.mSpanOffsetsSize = this.mSpanCount;
            savedState.mSpanOffsets = new int[this.mSpanCount];
            for (int i = 0; i < this.mSpanCount; i++) {
                if (this.mLastLayoutFromEnd) {
                    r1 = this.mSpans[i].m2126(Integer.MIN_VALUE);
                    if (r1 != Integer.MIN_VALUE) {
                        r1 -= this.mPrimaryOrientation.getEndAfterPadding();
                    }
                } else {
                    r1 = this.mSpans[i].m2134(Integer.MIN_VALUE);
                    if (r1 != Integer.MIN_VALUE) {
                        r1 -= this.mPrimaryOrientation.getStartAfterPadding();
                    }
                }
                savedState.mSpanOffsets[i] = r1;
            }
            return savedState;
        }
        savedState.mAnchorPosition = -1;
        savedState.mVisibleAnchorPosition = -1;
        savedState.mSpanOffsetsSize = 0;
        return savedState;
    }

    public void onScrollStateChanged(int i) {
        if (i == 0) {
            checkForGaps();
        }
    }

    /* access modifiers changed from: package-private */
    public void prepareLayoutStateForDelta(int i, RecyclerView.State state) {
        int i2;
        int firstChildPosition;
        if (i > 0) {
            i2 = 1;
            firstChildPosition = getLastChildPosition();
        } else {
            i2 = -1;
            firstChildPosition = getFirstChildPosition();
        }
        this.mLayoutState.f1652 = true;
        updateLayoutState(firstChildPosition, state);
        setLayoutStateDirection(i2);
        this.mLayoutState.f1651 = this.mLayoutState.f1650 + firstChildPosition;
        this.mLayoutState.f1649 = Math.abs(i);
    }

    /* access modifiers changed from: package-private */
    public int scrollBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        if (getChildCount() == 0 || i == 0) {
            return 0;
        }
        prepareLayoutStateForDelta(i, state);
        int fill = fill(recycler, this.mLayoutState, state);
        int i2 = this.mLayoutState.f1649 < fill ? i : i < 0 ? -fill : fill;
        this.mPrimaryOrientation.offsetChildren(-i2);
        this.mLastLayoutFromEnd = this.mShouldReverseLayout;
        this.mLayoutState.f1649 = 0;
        recycle(recycler, this.mLayoutState);
        return i2;
    }

    public int scrollHorizontallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        return scrollBy(i, recycler, state);
    }

    public void scrollToPosition(int i) {
        if (!(this.mPendingSavedState == null || this.mPendingSavedState.mAnchorPosition == i)) {
            this.mPendingSavedState.invalidateAnchorPositionInfo();
        }
        this.mPendingScrollPosition = i;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        requestLayout();
    }

    public void scrollToPositionWithOffset(int i, int i2) {
        if (this.mPendingSavedState != null) {
            this.mPendingSavedState.invalidateAnchorPositionInfo();
        }
        this.mPendingScrollPosition = i;
        this.mPendingScrollPositionOffset = i2;
        requestLayout();
    }

    public int scrollVerticallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        return scrollBy(i, recycler, state);
    }

    public void setGapStrategy(int i) {
        assertNotInLayoutOrScroll((String) null);
        if (i != this.mGapStrategy) {
            if (i == 0 || i == 2) {
                this.mGapStrategy = i;
                setAutoMeasureEnabled(this.mGapStrategy != 0);
                requestLayout();
                return;
            }
            throw new IllegalArgumentException("invalid gap strategy. Must be GAP_HANDLING_NONE or GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS");
        }
    }

    public void setMeasuredDimension(Rect rect, int i, int i2) {
        int chooseSize;
        int chooseSize2;
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        if (this.mOrientation == 1) {
            chooseSize2 = chooseSize(i2, rect.height() + paddingTop, getMinimumHeight());
            chooseSize = chooseSize(i, (this.mSizePerSpan * this.mSpanCount) + paddingLeft, getMinimumWidth());
        } else {
            chooseSize = chooseSize(i, rect.width() + paddingLeft, getMinimumWidth());
            chooseSize2 = chooseSize(i2, (this.mSizePerSpan * this.mSpanCount) + paddingTop, getMinimumHeight());
        }
        setMeasuredDimension(chooseSize, chooseSize2);
    }

    public void setOrientation(int i) {
        if (i == 0 || i == 1) {
            assertNotInLayoutOrScroll((String) null);
            if (i != this.mOrientation) {
                this.mOrientation = i;
                OrientationHelper orientationHelper = this.mPrimaryOrientation;
                this.mPrimaryOrientation = this.mSecondaryOrientation;
                this.mSecondaryOrientation = orientationHelper;
                requestLayout();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation.");
    }

    public void setReverseLayout(boolean z) {
        assertNotInLayoutOrScroll((String) null);
        if (!(this.mPendingSavedState == null || this.mPendingSavedState.mReverseLayout == z)) {
            this.mPendingSavedState.mReverseLayout = z;
        }
        this.mReverseLayout = z;
        requestLayout();
    }

    public void setSpanCount(int i) {
        assertNotInLayoutOrScroll((String) null);
        if (i != this.mSpanCount) {
            invalidateSpanAssignments();
            this.mSpanCount = i;
            this.mRemainingSpans = new BitSet(this.mSpanCount);
            this.mSpans = new Span[this.mSpanCount];
            for (int i2 = 0; i2 < this.mSpanCount; i2++) {
                this.mSpans[i2] = new Span(i2);
            }
            requestLayout();
        }
    }

    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int i) {
        LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext());
        linearSmoothScroller.setTargetPosition(i);
        startSmoothScroll(linearSmoothScroller);
    }

    public boolean supportsPredictiveItemAnimations() {
        return this.mPendingSavedState == null;
    }

    /* access modifiers changed from: package-private */
    public boolean updateAnchorFromPendingData(RecyclerView.State state, AnchorInfo anchorInfo) {
        boolean z = false;
        if (state.isPreLayout() || this.mPendingScrollPosition == -1) {
            return false;
        }
        if (this.mPendingScrollPosition < 0 || this.mPendingScrollPosition >= state.getItemCount()) {
            this.mPendingScrollPosition = -1;
            this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
            return false;
        } else if (this.mPendingSavedState == null || this.mPendingSavedState.mAnchorPosition == -1 || this.mPendingSavedState.mSpanOffsetsSize < 1) {
            View findViewByPosition = findViewByPosition(this.mPendingScrollPosition);
            if (findViewByPosition != null) {
                anchorInfo.f1774 = this.mShouldReverseLayout ? getLastChildPosition() : getFirstChildPosition();
                if (this.mPendingScrollPositionOffset != Integer.MIN_VALUE) {
                    if (anchorInfo.f1773) {
                        anchorInfo.f1771 = (this.mPrimaryOrientation.getEndAfterPadding() - this.mPendingScrollPositionOffset) - this.mPrimaryOrientation.getDecoratedEnd(findViewByPosition);
                        return true;
                    }
                    anchorInfo.f1771 = (this.mPrimaryOrientation.getStartAfterPadding() + this.mPendingScrollPositionOffset) - this.mPrimaryOrientation.getDecoratedStart(findViewByPosition);
                    return true;
                } else if (this.mPrimaryOrientation.getDecoratedMeasurement(findViewByPosition) > this.mPrimaryOrientation.getTotalSpace()) {
                    anchorInfo.f1771 = anchorInfo.f1773 ? this.mPrimaryOrientation.getEndAfterPadding() : this.mPrimaryOrientation.getStartAfterPadding();
                    return true;
                } else {
                    int decoratedStart = this.mPrimaryOrientation.getDecoratedStart(findViewByPosition) - this.mPrimaryOrientation.getStartAfterPadding();
                    if (decoratedStart < 0) {
                        anchorInfo.f1771 = -decoratedStart;
                        return true;
                    }
                    int endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding() - this.mPrimaryOrientation.getDecoratedEnd(findViewByPosition);
                    if (endAfterPadding < 0) {
                        anchorInfo.f1771 = endAfterPadding;
                        return true;
                    }
                    anchorInfo.f1771 = Integer.MIN_VALUE;
                    return true;
                }
            } else {
                anchorInfo.f1774 = this.mPendingScrollPosition;
                if (this.mPendingScrollPositionOffset == Integer.MIN_VALUE) {
                    if (calculateScrollDirectionForPosition(anchorInfo.f1774) == 1) {
                        z = true;
                    }
                    anchorInfo.f1773 = z;
                    anchorInfo.m2091();
                } else {
                    anchorInfo.m2093(this.mPendingScrollPositionOffset);
                }
                anchorInfo.f1772 = true;
                return true;
            }
        } else {
            anchorInfo.f1771 = Integer.MIN_VALUE;
            anchorInfo.f1774 = this.mPendingScrollPosition;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void updateAnchorInfoForLayout(RecyclerView.State state, AnchorInfo anchorInfo) {
        if (!updateAnchorFromPendingData(state, anchorInfo) && !updateAnchorFromChildren(state, anchorInfo)) {
            anchorInfo.m2091();
            anchorInfo.f1774 = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public void updateMeasureSpecs(int i) {
        this.mSizePerSpan = i / this.mSpanCount;
        this.mFullSizeSpec = View.MeasureSpec.makeMeasureSpec(i, this.mSecondaryOrientation.getMode());
    }
}
