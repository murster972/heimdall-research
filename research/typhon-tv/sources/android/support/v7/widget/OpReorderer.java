package android.support.v7.widget;

import android.support.v7.widget.AdapterHelper;
import java.util.List;

class OpReorderer {

    /* renamed from: 龘  reason: contains not printable characters */
    final Callback f1679;

    interface Callback {
        /* renamed from: 龘  reason: contains not printable characters */
        AdapterHelper.UpdateOp m1988(int i, int i2, int i3, Object obj);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1989(AdapterHelper.UpdateOp updateOp);
    }

    OpReorderer(Callback callback) {
        this.f1679 = callback;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m1982(List<AdapterHelper.UpdateOp> list) {
        boolean z = false;
        for (int size = list.size() - 1; size >= 0; size--) {
            if (list.get(size).f1480 != 8) {
                z = true;
            } else if (z) {
                return size;
            }
        }
        return -1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m1983(List<AdapterHelper.UpdateOp> list, int i, AdapterHelper.UpdateOp updateOp, int i2, AdapterHelper.UpdateOp updateOp2) {
        int i3 = 0;
        if (updateOp.f1478 < updateOp2.f1477) {
            i3 = 0 - 1;
        }
        if (updateOp.f1477 < updateOp2.f1477) {
            i3++;
        }
        if (updateOp2.f1477 <= updateOp.f1477) {
            updateOp.f1477 += updateOp2.f1478;
        }
        if (updateOp2.f1477 <= updateOp.f1478) {
            updateOp.f1478 += updateOp2.f1478;
        }
        updateOp2.f1477 += i3;
        list.set(i, updateOp2);
        list.set(i2, updateOp);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1984(List<AdapterHelper.UpdateOp> list, int i, int i2) {
        AdapterHelper.UpdateOp updateOp = list.get(i);
        AdapterHelper.UpdateOp updateOp2 = list.get(i2);
        switch (updateOp2.f1480) {
            case 1:
                m1983(list, i, updateOp, i2, updateOp2);
                return;
            case 2:
                m1987(list, i, updateOp, i2, updateOp2);
                return;
            case 4:
                m1985(list, i, updateOp, i2, updateOp2);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1985(List<AdapterHelper.UpdateOp> list, int i, AdapterHelper.UpdateOp updateOp, int i2, AdapterHelper.UpdateOp updateOp2) {
        AdapterHelper.UpdateOp updateOp3 = null;
        AdapterHelper.UpdateOp updateOp4 = null;
        if (updateOp.f1478 < updateOp2.f1477) {
            updateOp2.f1477--;
        } else if (updateOp.f1478 < updateOp2.f1477 + updateOp2.f1478) {
            updateOp2.f1478--;
            updateOp3 = this.f1679.m1988(4, updateOp.f1477, 1, updateOp2.f1479);
        }
        if (updateOp.f1477 <= updateOp2.f1477) {
            updateOp2.f1477++;
        } else if (updateOp.f1477 < updateOp2.f1477 + updateOp2.f1478) {
            int i3 = (updateOp2.f1477 + updateOp2.f1478) - updateOp.f1477;
            updateOp4 = this.f1679.m1988(4, updateOp.f1477 + 1, i3, updateOp2.f1479);
            updateOp2.f1478 -= i3;
        }
        list.set(i2, updateOp);
        if (updateOp2.f1478 > 0) {
            list.set(i, updateOp2);
        } else {
            list.remove(i);
            this.f1679.m1989(updateOp2);
        }
        if (updateOp3 != null) {
            list.add(i, updateOp3);
        }
        if (updateOp4 != null) {
            list.add(i, updateOp4);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1986(List<AdapterHelper.UpdateOp> list) {
        while (true) {
            int r0 = m1982(list);
            if (r0 != -1) {
                m1984(list, r0, r0 + 1);
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1987(List<AdapterHelper.UpdateOp> list, int i, AdapterHelper.UpdateOp updateOp, int i2, AdapterHelper.UpdateOp updateOp2) {
        boolean z;
        AdapterHelper.UpdateOp updateOp3 = null;
        boolean z2 = false;
        if (updateOp.f1477 < updateOp.f1478) {
            z = false;
            if (updateOp2.f1477 == updateOp.f1477 && updateOp2.f1478 == updateOp.f1478 - updateOp.f1477) {
                z2 = true;
            }
        } else {
            z = true;
            if (updateOp2.f1477 == updateOp.f1478 + 1 && updateOp2.f1478 == updateOp.f1477 - updateOp.f1478) {
                z2 = true;
            }
        }
        if (updateOp.f1478 < updateOp2.f1477) {
            updateOp2.f1477--;
        } else if (updateOp.f1478 < updateOp2.f1477 + updateOp2.f1478) {
            updateOp2.f1478--;
            updateOp.f1480 = 2;
            updateOp.f1478 = 1;
            if (updateOp2.f1478 == 0) {
                list.remove(i2);
                this.f1679.m1989(updateOp2);
                return;
            }
            return;
        }
        if (updateOp.f1477 <= updateOp2.f1477) {
            updateOp2.f1477++;
        } else if (updateOp.f1477 < updateOp2.f1477 + updateOp2.f1478) {
            updateOp3 = this.f1679.m1988(2, updateOp.f1477 + 1, (updateOp2.f1477 + updateOp2.f1478) - updateOp.f1477, (Object) null);
            updateOp2.f1478 = updateOp.f1477 - updateOp2.f1477;
        }
        if (z2) {
            list.set(i, updateOp2);
            list.remove(i2);
            this.f1679.m1989(updateOp);
            return;
        }
        if (z) {
            if (updateOp3 != null) {
                if (updateOp.f1477 > updateOp3.f1477) {
                    updateOp.f1477 -= updateOp3.f1478;
                }
                if (updateOp.f1478 > updateOp3.f1477) {
                    updateOp.f1478 -= updateOp3.f1478;
                }
            }
            if (updateOp.f1477 > updateOp2.f1477) {
                updateOp.f1477 -= updateOp2.f1478;
            }
            if (updateOp.f1478 > updateOp2.f1477) {
                updateOp.f1478 -= updateOp2.f1478;
            }
        } else {
            if (updateOp3 != null) {
                if (updateOp.f1477 >= updateOp3.f1477) {
                    updateOp.f1477 -= updateOp3.f1478;
                }
                if (updateOp.f1478 >= updateOp3.f1477) {
                    updateOp.f1478 -= updateOp3.f1478;
                }
            }
            if (updateOp.f1477 >= updateOp2.f1477) {
                updateOp.f1477 -= updateOp2.f1478;
            }
            if (updateOp.f1478 >= updateOp2.f1477) {
                updateOp.f1478 -= updateOp2.f1478;
            }
        }
        list.set(i, updateOp2);
        if (updateOp.f1477 != updateOp.f1478) {
            list.set(i2, updateOp);
        } else {
            list.remove(i2);
        }
        if (updateOp3 != null) {
            list.add(i, updateOp3);
        }
    }
}
