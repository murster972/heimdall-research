package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.v7.cardview.R;

class RoundRectDrawableWithShadow extends Drawable {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final double f1705 = Math.cos(Math.toRadians(45.0d));

    /* renamed from: 龘  reason: contains not printable characters */
    static RoundRectHelper f1706;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Paint f1707;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final RectF f1708;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f1709;

    /* renamed from: ʾ  reason: contains not printable characters */
    private ColorStateList f1710;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f1711 = true;

    /* renamed from: ˈ  reason: contains not printable characters */
    private float f1712;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f1713 = true;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f1714 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Path f1715;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f1716;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f1717;

    /* renamed from: 连任  reason: contains not printable characters */
    private Paint f1718;

    /* renamed from: 麤  reason: contains not printable characters */
    private Paint f1719;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f1720;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final int f1721;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final int f1722;

    interface RoundRectHelper {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2070(Canvas canvas, RectF rectF, float f, Paint paint);
    }

    RoundRectDrawableWithShadow(Resources resources, ColorStateList colorStateList, float f, float f2, float f3) {
        this.f1721 = resources.getColor(R.color.cardview_shadow_start_color);
        this.f1722 = resources.getColor(R.color.cardview_shadow_end_color);
        this.f1720 = resources.getDimensionPixelSize(R.dimen.cardview_compat_inset_shadow);
        this.f1719 = new Paint(5);
        m2052(colorStateList);
        this.f1718 = new Paint(5);
        this.f1718.setStyle(Paint.Style.FILL);
        this.f1709 = (float) ((int) (0.5f + f));
        this.f1708 = new RectF();
        this.f1707 = new Paint(this.f1718);
        this.f1707.setAntiAlias(false);
        m2056(f2, f3);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m2050() {
        RectF rectF = new RectF(-this.f1709, -this.f1709, this.f1709, this.f1709);
        RectF rectF2 = new RectF(rectF);
        rectF2.inset(-this.f1717, -this.f1717);
        if (this.f1715 == null) {
            this.f1715 = new Path();
        } else {
            this.f1715.reset();
        }
        this.f1715.setFillType(Path.FillType.EVEN_ODD);
        this.f1715.moveTo(-this.f1709, 0.0f);
        this.f1715.rLineTo(-this.f1717, 0.0f);
        this.f1715.arcTo(rectF2, 180.0f, 90.0f, false);
        this.f1715.arcTo(rectF, 270.0f, -90.0f, false);
        this.f1715.close();
        this.f1718.setShader(new RadialGradient(0.0f, 0.0f, this.f1709 + this.f1717, new int[]{this.f1721, this.f1721, this.f1722}, new float[]{0.0f, this.f1709 / (this.f1709 + this.f1717), 1.0f}, Shader.TileMode.CLAMP));
        this.f1707.setShader(new LinearGradient(0.0f, (-this.f1709) + this.f1717, 0.0f, (-this.f1709) - this.f1717, new int[]{this.f1721, this.f1721, this.f1722}, new float[]{0.0f, 0.5f, 1.0f}, Shader.TileMode.CLAMP));
        this.f1707.setAntiAlias(false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static float m2051(float f, float f2, boolean z) {
        return z ? (float) (((double) f) + ((1.0d - f1705) * ((double) f2))) : f;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m2052(ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        this.f1710 = colorStateList;
        this.f1719.setColor(this.f1710.getColorForState(getState(), this.f1710.getDefaultColor()));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m2053(Rect rect) {
        float f = this.f1716 * 1.5f;
        this.f1708.set(((float) rect.left) + this.f1716, ((float) rect.top) + f, ((float) rect.right) - this.f1716, ((float) rect.bottom) - f);
        m2050();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private int m2054(float f) {
        int i = (int) (0.5f + f);
        return i % 2 == 1 ? i - 1 : i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static float m2055(float f, float f2, boolean z) {
        return z ? (float) (((double) (1.5f * f)) + ((1.0d - f1705) * ((double) f2))) : 1.5f * f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2056(float f, float f2) {
        if (f < 0.0f) {
            throw new IllegalArgumentException("Invalid shadow size " + f + ". Must be >= 0");
        } else if (f2 < 0.0f) {
            throw new IllegalArgumentException("Invalid max shadow size " + f2 + ". Must be >= 0");
        } else {
            float r4 = (float) m2054(f);
            float r5 = (float) m2054(f2);
            if (r4 > r5) {
                r4 = r5;
                if (!this.f1714) {
                    this.f1714 = true;
                }
            }
            if (this.f1712 != r4 || this.f1716 != r5) {
                this.f1712 = r4;
                this.f1716 = r5;
                this.f1717 = (float) ((int) ((1.5f * r4) + ((float) this.f1720) + 0.5f));
                this.f1711 = true;
                invalidateSelf();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2057(Canvas canvas) {
        float f = (-this.f1709) - this.f1717;
        float f2 = this.f1709 + ((float) this.f1720) + (this.f1712 / 2.0f);
        boolean z = this.f1708.width() - (2.0f * f2) > 0.0f;
        boolean z2 = this.f1708.height() - (2.0f * f2) > 0.0f;
        int save = canvas.save();
        canvas.translate(this.f1708.left + f2, this.f1708.top + f2);
        canvas.drawPath(this.f1715, this.f1718);
        if (z) {
            canvas.drawRect(0.0f, f, this.f1708.width() - (2.0f * f2), -this.f1709, this.f1707);
        }
        canvas.restoreToCount(save);
        int save2 = canvas.save();
        canvas.translate(this.f1708.right - f2, this.f1708.bottom - f2);
        canvas.rotate(180.0f);
        canvas.drawPath(this.f1715, this.f1718);
        if (z) {
            canvas.drawRect(0.0f, f, this.f1708.width() - (2.0f * f2), this.f1717 + (-this.f1709), this.f1707);
        }
        canvas.restoreToCount(save2);
        int save3 = canvas.save();
        canvas.translate(this.f1708.left + f2, this.f1708.bottom - f2);
        canvas.rotate(270.0f);
        canvas.drawPath(this.f1715, this.f1718);
        if (z2) {
            canvas.drawRect(0.0f, f, this.f1708.height() - (2.0f * f2), -this.f1709, this.f1707);
        }
        canvas.restoreToCount(save3);
        int save4 = canvas.save();
        canvas.translate(this.f1708.right - f2, this.f1708.top + f2);
        canvas.rotate(90.0f);
        canvas.drawPath(this.f1715, this.f1718);
        if (z2) {
            canvas.drawRect(0.0f, f, this.f1708.height() - (2.0f * f2), -this.f1709, this.f1707);
        }
        canvas.restoreToCount(save4);
    }

    public void draw(Canvas canvas) {
        if (this.f1711) {
            m2053(getBounds());
            this.f1711 = false;
        }
        canvas.translate(0.0f, this.f1712 / 2.0f);
        m2057(canvas);
        canvas.translate(0.0f, (-this.f1712) / 2.0f);
        f1706.m2070(canvas, this.f1708, this.f1709, this.f1719);
    }

    public int getOpacity() {
        return -3;
    }

    public boolean getPadding(Rect rect) {
        int ceil = (int) Math.ceil((double) m2055(this.f1716, this.f1709, this.f1713));
        int ceil2 = (int) Math.ceil((double) m2051(this.f1716, this.f1709, this.f1713));
        rect.set(ceil2, ceil, ceil2, ceil);
        return true;
    }

    public boolean isStateful() {
        return (this.f1710 != null && this.f1710.isStateful()) || super.isStateful();
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.f1711 = true;
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        int colorForState = this.f1710.getColorForState(iArr, this.f1710.getDefaultColor());
        if (this.f1719.getColor() == colorForState) {
            return false;
        }
        this.f1719.setColor(colorForState);
        this.f1711 = true;
        invalidateSelf();
        return true;
    }

    public void setAlpha(int i) {
        this.f1719.setAlpha(i);
        this.f1718.setAlpha(i);
        this.f1707.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f1719.setColorFilter(colorFilter);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public ColorStateList m2058() {
        return this.f1710;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public float m2059() {
        return (((this.f1716 * 1.5f) + ((float) this.f1720)) * 2.0f) + (2.0f * Math.max(this.f1716, this.f1709 + ((float) this.f1720) + ((this.f1716 * 1.5f) / 2.0f)));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public float m2060() {
        return this.f1712;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m2061(float f) {
        m2056(f, this.f1716);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public float m2062() {
        return ((this.f1716 + ((float) this.f1720)) * 2.0f) + (2.0f * Math.max(this.f1716, this.f1709 + ((float) this.f1720) + (this.f1716 / 2.0f)));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public float m2063() {
        return this.f1716;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m2064(float f) {
        m2056(this.f1712, f);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public float m2065() {
        return this.f1709;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2066(float f) {
        if (f < 0.0f) {
            throw new IllegalArgumentException("Invalid radius " + f + ". Must be >= 0");
        }
        float f2 = (float) ((int) (0.5f + f));
        if (this.f1709 != f2) {
            this.f1709 = f2;
            this.f1711 = true;
            invalidateSelf();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2067(ColorStateList colorStateList) {
        m2052(colorStateList);
        invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2068(Rect rect) {
        getPadding(rect);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2069(boolean z) {
        this.f1713 = z;
        invalidateSelf();
    }
}
