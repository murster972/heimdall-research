package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.appcompat.R;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;
import android.widget.CompoundButton;

class AppCompatCompoundButtonHelper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f1487;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f1488 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private ColorStateList f1489 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f1490 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private PorterDuff.Mode f1491 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private final CompoundButton f1492;

    AppCompatCompoundButtonHelper(CompoundButton compoundButton) {
        this.f1492 = compoundButton;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public PorterDuff.Mode m1734() {
        return this.f1491;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m1735() {
        Drawable buttonDrawable = CompoundButtonCompat.getButtonDrawable(this.f1492);
        if (buttonDrawable == null) {
            return;
        }
        if (this.f1490 || this.f1488) {
            Drawable mutate = DrawableCompat.wrap(buttonDrawable).mutate();
            if (this.f1490) {
                DrawableCompat.setTintList(mutate, this.f1489);
            }
            if (this.f1488) {
                DrawableCompat.setTintMode(mutate, this.f1491);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.f1492.getDrawableState());
            }
            this.f1492.setButtonDrawable(mutate);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m1736() {
        if (this.f1487) {
            this.f1487 = false;
            return;
        }
        this.f1487 = true;
        m1735();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r0 = android.support.v4.widget.CompoundButtonCompat.getButtonDrawable(r3.f1492);
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int m1737(int r4) {
        /*
            r3 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 17
            if (r1 >= r2) goto L_0x0013
            android.widget.CompoundButton r1 = r3.f1492
            android.graphics.drawable.Drawable r0 = android.support.v4.widget.CompoundButtonCompat.getButtonDrawable(r1)
            if (r0 == 0) goto L_0x0013
            int r1 = r0.getIntrinsicWidth()
            int r4 = r4 + r1
        L_0x0013:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatCompoundButtonHelper.m1737(int):int");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ColorStateList m1738() {
        return this.f1489;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1739(ColorStateList colorStateList) {
        this.f1489 = colorStateList;
        this.f1490 = true;
        m1735();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1740(PorterDuff.Mode mode) {
        this.f1491 = mode;
        this.f1488 = true;
        m1735();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1741(AttributeSet attributeSet, int i) {
        int resourceId;
        TypedArray obtainStyledAttributes = this.f1492.getContext().obtainStyledAttributes(attributeSet, R.styleable.CompoundButton, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(R.styleable.CompoundButton_android_button) && (resourceId = obtainStyledAttributes.getResourceId(R.styleable.CompoundButton_android_button, 0)) != 0) {
                this.f1492.setButtonDrawable(AppCompatResources.m941(this.f1492.getContext(), resourceId));
            }
            if (obtainStyledAttributes.hasValue(R.styleable.CompoundButton_buttonTint)) {
                CompoundButtonCompat.setButtonTintList(this.f1492, obtainStyledAttributes.getColorStateList(R.styleable.CompoundButton_buttonTint));
            }
            if (obtainStyledAttributes.hasValue(R.styleable.CompoundButton_buttonTintMode)) {
                CompoundButtonCompat.setButtonTintMode(this.f1492, DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(R.styleable.CompoundButton_buttonTintMode, -1), (PorterDuff.Mode) null));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }
}
