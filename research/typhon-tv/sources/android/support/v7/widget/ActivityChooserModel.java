package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class ActivityChooserModel extends DataSetObservable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Map<String, ActivityChooserModel> f1439 = new HashMap();

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Object f1440 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    static final String f1441 = ActivityChooserModel.class.getSimpleName();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Object f1442 = new Object();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final List<ActivityResolveInfo> f1443 = new ArrayList();

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f1444 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f1445 = true;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f1446 = 50;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final List<HistoricalRecord> f1447 = new ArrayList();

    /* renamed from: ٴ  reason: contains not printable characters */
    private Intent f1448;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private ActivitySorter f1449 = new DefaultSorter();

    /* renamed from: 靐  reason: contains not printable characters */
    final Context f1450;

    /* renamed from: 麤  reason: contains not printable characters */
    boolean f1451 = true;

    /* renamed from: 齉  reason: contains not printable characters */
    final String f1452;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f1453 = false;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private OnChooseActivityListener f1454;

    public interface ActivityChooserModelClient {
        void setActivityChooserModel(ActivityChooserModel activityChooserModel);
    }

    public static final class ActivityResolveInfo implements Comparable<ActivityResolveInfo> {
        public final ResolveInfo resolveInfo;
        public float weight;

        public ActivityResolveInfo(ResolveInfo resolveInfo2) {
            this.resolveInfo = resolveInfo2;
        }

        public int compareTo(ActivityResolveInfo activityResolveInfo) {
            return Float.floatToIntBits(activityResolveInfo.weight) - Float.floatToIntBits(this.weight);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            return Float.floatToIntBits(this.weight) == Float.floatToIntBits(((ActivityResolveInfo) obj).weight);
        }

        public int hashCode() {
            return Float.floatToIntBits(this.weight) + 31;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("resolveInfo:").append(this.resolveInfo.toString());
            sb.append("; weight:").append(new BigDecimal((double) this.weight));
            sb.append("]");
            return sb.toString();
        }
    }

    public interface ActivitySorter {
        void sort(Intent intent, List<ActivityResolveInfo> list, List<HistoricalRecord> list2);
    }

    private static final class DefaultSorter implements ActivitySorter {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<ComponentName, ActivityResolveInfo> f1455 = new HashMap();

        DefaultSorter() {
        }

        public void sort(Intent intent, List<ActivityResolveInfo> list, List<HistoricalRecord> list2) {
            Map<ComponentName, ActivityResolveInfo> map = this.f1455;
            map.clear();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                ActivityResolveInfo activityResolveInfo = list.get(i);
                activityResolveInfo.weight = 0.0f;
                map.put(new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name), activityResolveInfo);
            }
            float f = 1.0f;
            for (int size2 = list2.size() - 1; size2 >= 0; size2--) {
                HistoricalRecord historicalRecord = list2.get(size2);
                ActivityResolveInfo activityResolveInfo2 = map.get(historicalRecord.activity);
                if (activityResolveInfo2 != null) {
                    activityResolveInfo2.weight += historicalRecord.weight * f;
                    f *= 0.95f;
                }
            }
            Collections.sort(list);
        }
    }

    public static final class HistoricalRecord {
        public final ComponentName activity;
        public final long time;
        public final float weight;

        public HistoricalRecord(ComponentName componentName, long j, float f) {
            this.activity = componentName;
            this.time = j;
            this.weight = f;
        }

        public HistoricalRecord(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            HistoricalRecord historicalRecord = (HistoricalRecord) obj;
            if (this.activity == null) {
                if (historicalRecord.activity != null) {
                    return false;
                }
            } else if (!this.activity.equals(historicalRecord.activity)) {
                return false;
            }
            if (this.time != historicalRecord.time) {
                return false;
            }
            return Float.floatToIntBits(this.weight) == Float.floatToIntBits(historicalRecord.weight);
        }

        public int hashCode() {
            return (((((this.activity == null ? 0 : this.activity.hashCode()) + 31) * 31) + ((int) (this.time ^ (this.time >>> 32)))) * 31) + Float.floatToIntBits(this.weight);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("; activity:").append(this.activity);
            sb.append("; time:").append(this.time);
            sb.append("; weight:").append(new BigDecimal((double) this.weight));
            sb.append("]");
            return sb.toString();
        }
    }

    public interface OnChooseActivityListener {
        boolean onChooseActivity(ActivityChooserModel activityChooserModel, Intent intent);
    }

    private final class PersistHistoryAsyncTask extends AsyncTask<Object, Void, Void> {
        PersistHistoryAsyncTask() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Void doInBackground(Object... objArr) {
            List list = objArr[0];
            String str = objArr[1];
            try {
                FileOutputStream openFileOutput = ActivityChooserModel.this.f1450.openFileOutput(str, 0);
                XmlSerializer newSerializer = Xml.newSerializer();
                try {
                    newSerializer.setOutput(openFileOutput, (String) null);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag((String) null, "historical-records");
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        HistoricalRecord historicalRecord = (HistoricalRecord) list.remove(0);
                        newSerializer.startTag((String) null, "historical-record");
                        newSerializer.attribute((String) null, "activity", historicalRecord.activity.flattenToString());
                        newSerializer.attribute((String) null, "time", String.valueOf(historicalRecord.time));
                        newSerializer.attribute((String) null, "weight", String.valueOf(historicalRecord.weight));
                        newSerializer.endTag((String) null, "historical-record");
                    }
                    newSerializer.endTag((String) null, "historical-records");
                    newSerializer.endDocument();
                    ActivityChooserModel.this.f1451 = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (IllegalArgumentException e2) {
                    Log.e(ActivityChooserModel.f1441, "Error writing historical record file: " + ActivityChooserModel.this.f1452, e2);
                    ActivityChooserModel.this.f1451 = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e3) {
                        }
                    }
                } catch (IllegalStateException e4) {
                    Log.e(ActivityChooserModel.f1441, "Error writing historical record file: " + ActivityChooserModel.this.f1452, e4);
                    ActivityChooserModel.this.f1451 = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e5) {
                        }
                    }
                } catch (IOException e6) {
                    Log.e(ActivityChooserModel.f1441, "Error writing historical record file: " + ActivityChooserModel.this.f1452, e6);
                    ActivityChooserModel.this.f1451 = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e7) {
                        }
                    }
                } catch (Throwable th) {
                    ActivityChooserModel.this.f1451 = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e8) {
                        }
                    }
                    throw th;
                }
                return null;
            } catch (FileNotFoundException e9) {
                Log.e(ActivityChooserModel.f1441, "Error writing historical record file: " + str, e9);
                return null;
            }
        }
    }

    private ActivityChooserModel(Context context, String str) {
        this.f1450 = context.getApplicationContext();
        if (TextUtils.isEmpty(str) || str.endsWith(".xml")) {
            this.f1452 = str;
        } else {
            this.f1452 = str + ".xml";
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m1658() {
        if (this.f1449 == null || this.f1448 == null || this.f1443.isEmpty() || this.f1447.isEmpty()) {
            return false;
        }
        this.f1449.sort(this.f1448, this.f1443, Collections.unmodifiableList(this.f1447));
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m1659() {
        if (!this.f1453 || this.f1448 == null) {
            return false;
        }
        this.f1453 = false;
        this.f1443.clear();
        List<ResolveInfo> queryIntentActivities = this.f1450.getPackageManager().queryIntentActivities(this.f1448, 0);
        int size = queryIntentActivities.size();
        for (int i = 0; i < size; i++) {
            this.f1443.add(new ActivityResolveInfo(queryIntentActivities.get(i)));
        }
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m1660() {
        if (!this.f1451 || !this.f1445 || TextUtils.isEmpty(this.f1452)) {
            return false;
        }
        this.f1451 = false;
        this.f1444 = true;
        m1662();
        return true;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m1661() {
        int size = this.f1447.size() - this.f1446;
        if (size > 0) {
            this.f1445 = true;
            for (int i = 0; i < size; i++) {
                HistoricalRecord remove = this.f1447.remove(0);
            }
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m1662() {
        FileInputStream fileInputStream = null;
        try {
            FileInputStream openFileInput = this.f1450.openFileInput(this.f1452);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i = 0;
                while (i != 1 && i != 2) {
                    i = newPullParser.next();
                }
                if (!"historical-records".equals(newPullParser.getName())) {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
                List<HistoricalRecord> list = this.f1447;
                list.clear();
                while (true) {
                    int next = newPullParser.next();
                    if (next == 1) {
                        if (openFileInput != null) {
                            try {
                                openFileInput.close();
                                return;
                            } catch (IOException e) {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else if (!(next == 3 || next == 4)) {
                        if (!"historical-record".equals(newPullParser.getName())) {
                            throw new XmlPullParserException("Share records file not well-formed.");
                        }
                        list.add(new HistoricalRecord(newPullParser.getAttributeValue((String) null, "activity"), Long.parseLong(newPullParser.getAttributeValue((String) null, "time")), Float.parseFloat(newPullParser.getAttributeValue((String) null, "weight"))));
                    }
                }
            } catch (XmlPullParserException e2) {
                Log.e(f1441, "Error reading historical recrod file: " + this.f1452, e2);
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e3) {
                    }
                }
            } catch (IOException e4) {
                Log.e(f1441, "Error reading historical recrod file: " + this.f1452, e4);
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e5) {
                    }
                }
            } catch (Throwable th) {
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e6) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException e7) {
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m1663() {
        boolean r0 = m1659() | m1660();
        m1661();
        if (r0) {
            m1658();
            notifyChanged();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m1664() {
        if (!this.f1444) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.f1445) {
            this.f1445 = false;
            if (!TextUtils.isEmpty(this.f1452)) {
                new PersistHistoryAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Object[]{new ArrayList(this.f1447), this.f1452});
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ActivityChooserModel m1665(Context context, String str) {
        ActivityChooserModel activityChooserModel;
        synchronized (f1440) {
            activityChooserModel = f1439.get(str);
            if (activityChooserModel == null) {
                activityChooserModel = new ActivityChooserModel(context, str);
                f1439.put(str, activityChooserModel);
            }
        }
        return activityChooserModel;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m1666(HistoricalRecord historicalRecord) {
        boolean add = this.f1447.add(historicalRecord);
        if (add) {
            this.f1445 = true;
            m1661();
            m1664();
            m1658();
            notifyChanged();
        }
        return add;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Intent m1667(int i) {
        synchronized (this.f1442) {
            if (this.f1448 == null) {
                return null;
            }
            m1663();
            ActivityResolveInfo activityResolveInfo = this.f1443.get(i);
            ComponentName componentName = new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
            Intent intent = new Intent(this.f1448);
            intent.setComponent(componentName);
            if (this.f1454 != null) {
                if (this.f1454.onChooseActivity(this, new Intent(intent))) {
                    return null;
                }
            }
            m1666(new HistoricalRecord(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ResolveInfo m1668() {
        synchronized (this.f1442) {
            m1663();
            if (this.f1443.isEmpty()) {
                return null;
            }
            ResolveInfo resolveInfo = this.f1443.get(0).resolveInfo;
            return resolveInfo;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m1669() {
        int size;
        synchronized (this.f1442) {
            m1663();
            size = this.f1447.size();
        }
        return size;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1670(int i) {
        synchronized (this.f1442) {
            m1663();
            ActivityResolveInfo activityResolveInfo = this.f1443.get(i);
            ActivityResolveInfo activityResolveInfo2 = this.f1443.get(0);
            m1666(new HistoricalRecord(new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name), System.currentTimeMillis(), activityResolveInfo2 != null ? (activityResolveInfo2.weight - activityResolveInfo.weight) + 5.0f : 1.0f));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m1671() {
        int size;
        synchronized (this.f1442) {
            m1663();
            size = this.f1443.size();
        }
        return size;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m1672(ResolveInfo resolveInfo) {
        int i;
        synchronized (this.f1442) {
            m1663();
            List<ActivityResolveInfo> list = this.f1443;
            int size = list.size();
            i = 0;
            while (true) {
                if (i >= size) {
                    i = -1;
                    break;
                } else if (list.get(i).resolveInfo == resolveInfo) {
                    break;
                } else {
                    i++;
                }
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ResolveInfo m1673(int i) {
        ResolveInfo resolveInfo;
        synchronized (this.f1442) {
            m1663();
            resolveInfo = this.f1443.get(i).resolveInfo;
        }
        return resolveInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1674(Intent intent) {
        synchronized (this.f1442) {
            if (this.f1448 != intent) {
                this.f1448 = intent;
                this.f1453 = true;
                m1663();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1675(OnChooseActivityListener onChooseActivityListener) {
        synchronized (this.f1442) {
            this.f1454 = onChooseActivityListener;
        }
    }
}
