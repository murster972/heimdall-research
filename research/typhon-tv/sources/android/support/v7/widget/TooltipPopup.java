package android.support.v7.widget;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.annotation.RestrictTo;
import android.support.v4.view.PointerIconCompat;
import android.support.v7.appcompat.R;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import io.fabric.sdk.android.services.common.AbstractSpiCall;

@RestrictTo
class TooltipPopup {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int[] f1845 = new int[2];

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int[] f1846 = new int[2];

    /* renamed from: 连任  reason: contains not printable characters */
    private final Rect f1847 = new Rect();

    /* renamed from: 靐  reason: contains not printable characters */
    private final View f1848;

    /* renamed from: 麤  reason: contains not printable characters */
    private final WindowManager.LayoutParams f1849 = new WindowManager.LayoutParams();

    /* renamed from: 齉  reason: contains not printable characters */
    private final TextView f1850;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f1851;

    TooltipPopup(Context context) {
        this.f1851 = context;
        this.f1848 = LayoutInflater.from(this.f1851).inflate(R.layout.tooltip, (ViewGroup) null);
        this.f1850 = (TextView) this.f1848.findViewById(R.id.message);
        this.f1849.setTitle(getClass().getSimpleName());
        this.f1849.packageName = this.f1851.getPackageName();
        this.f1849.type = PointerIconCompat.TYPE_HAND;
        this.f1849.width = -2;
        this.f1849.height = -2;
        this.f1849.format = -3;
        this.f1849.windowAnimations = R.style.Animation_AppCompat_Tooltip;
        this.f1849.flags = 24;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static View m2182(View view) {
        for (Context context = view.getContext(); context instanceof ContextWrapper; context = ((ContextWrapper) context).getBaseContext()) {
            if (context instanceof Activity) {
                return ((Activity) context).getWindow().getDecorView();
            }
        }
        return view.getRootView();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2183(View view, int i, int i2, boolean z, WindowManager.LayoutParams layoutParams) {
        int height;
        int i3;
        int dimensionPixelOffset = this.f1851.getResources().getDimensionPixelOffset(R.dimen.tooltip_precise_anchor_threshold);
        int width = view.getWidth() >= dimensionPixelOffset ? i : view.getWidth() / 2;
        if (view.getHeight() >= dimensionPixelOffset) {
            int dimensionPixelOffset2 = this.f1851.getResources().getDimensionPixelOffset(R.dimen.tooltip_precise_anchor_extra_offset);
            height = i2 + dimensionPixelOffset2;
            i3 = i2 - dimensionPixelOffset2;
        } else {
            height = view.getHeight();
            i3 = 0;
        }
        layoutParams.gravity = 49;
        int dimensionPixelOffset3 = this.f1851.getResources().getDimensionPixelOffset(z ? R.dimen.tooltip_y_offset_touch : R.dimen.tooltip_y_offset_non_touch);
        View r4 = m2182(view);
        if (r4 == null) {
            Log.e("TooltipPopup", "Cannot find app view");
            return;
        }
        r4.getWindowVisibleDisplayFrame(this.f1847);
        if (this.f1847.left < 0 && this.f1847.top < 0) {
            Resources resources = this.f1851.getResources();
            int identifier = resources.getIdentifier("status_bar_height", "dimen", AbstractSpiCall.ANDROID_CLIENT_TYPE);
            int dimensionPixelSize = identifier != 0 ? resources.getDimensionPixelSize(identifier) : 0;
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            this.f1847.set(0, dimensionPixelSize, displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
        r4.getLocationOnScreen(this.f1846);
        view.getLocationOnScreen(this.f1845);
        int[] iArr = this.f1845;
        iArr[0] = iArr[0] - this.f1846[0];
        int[] iArr2 = this.f1845;
        iArr2[1] = iArr2[1] - this.f1846[1];
        layoutParams.x = (this.f1845[0] + width) - (this.f1847.width() / 2);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.f1848.measure(makeMeasureSpec, makeMeasureSpec);
        int measuredHeight = this.f1848.getMeasuredHeight();
        int i4 = ((this.f1845[1] + i3) - dimensionPixelOffset3) - measuredHeight;
        int i5 = this.f1845[1] + height + dimensionPixelOffset3;
        if (z) {
            if (i4 >= 0) {
                layoutParams.y = i4;
            } else {
                layoutParams.y = i5;
            }
        } else if (i5 + measuredHeight <= this.f1847.height()) {
            layoutParams.y = i5;
        } else {
            layoutParams.y = i4;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m2184() {
        return this.f1848.getParent() != null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2185() {
        if (m2184()) {
            ((WindowManager) this.f1851.getSystemService("window")).removeView(this.f1848);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2186(View view, int i, int i2, boolean z, CharSequence charSequence) {
        if (m2184()) {
            m2185();
        }
        this.f1850.setText(charSequence);
        m2183(view, i, i2, z, this.f1849);
        ((WindowManager) this.f1851.getSystemService("window")).addView(this.f1848, this.f1849);
    }
}
