package android.support.v7.widget;

import android.view.View;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

class ViewBoundsCheck {

    /* renamed from: 靐  reason: contains not printable characters */
    BoundFlags f1852 = new BoundFlags();

    /* renamed from: 龘  reason: contains not printable characters */
    final Callback f1853;

    static class BoundFlags {

        /* renamed from: 连任  reason: contains not printable characters */
        int f1854;

        /* renamed from: 靐  reason: contains not printable characters */
        int f1855;

        /* renamed from: 麤  reason: contains not printable characters */
        int f1856;

        /* renamed from: 齉  reason: contains not printable characters */
        int f1857;

        /* renamed from: 龘  reason: contains not printable characters */
        int f1858 = 0;

        BoundFlags() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m2189() {
            if ((this.f1858 & 7) != 0 && (this.f1858 & (m2190(this.f1856, this.f1855) << 0)) == 0) {
                return false;
            }
            if ((this.f1858 & 112) != 0 && (this.f1858 & (m2190(this.f1856, this.f1857) << 4)) == 0) {
                return false;
            }
            if ((this.f1858 & 1792) == 0 || (this.f1858 & (m2190(this.f1854, this.f1855) << 8)) != 0) {
                return (this.f1858 & 28672) == 0 || (this.f1858 & (m2190(this.f1854, this.f1857) << 12)) != 0;
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m2190(int i, int i2) {
            if (i > i2) {
                return 1;
            }
            return i == i2 ? 2 : 4;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2191() {
            this.f1858 = 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2192(int i) {
            this.f1858 |= i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m2193(int i, int i2, int i3, int i4) {
            this.f1855 = i;
            this.f1857 = i2;
            this.f1856 = i3;
            this.f1854 = i4;
        }
    }

    interface Callback {
        /* renamed from: 靐  reason: contains not printable characters */
        int m2194();

        /* renamed from: 靐  reason: contains not printable characters */
        int m2195(View view);

        /* renamed from: 龘  reason: contains not printable characters */
        int m2196();

        /* renamed from: 龘  reason: contains not printable characters */
        int m2197(View view);

        /* renamed from: 龘  reason: contains not printable characters */
        View m2198(int i);
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface ViewBounds {
    }

    ViewBoundsCheck(Callback callback) {
        this.f1853 = callback;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public View m2187(int i, int i2, int i3, int i4) {
        int r7 = this.f1853.m2196();
        int r4 = this.f1853.m2194();
        int i5 = i2 > i ? 1 : -1;
        View view = null;
        for (int i6 = i; i6 != i2; i6 += i5) {
            View r1 = this.f1853.m2198(i6);
            this.f1852.m2193(r7, r4, this.f1853.m2197(r1), this.f1853.m2195(r1));
            if (i3 != 0) {
                this.f1852.m2191();
                this.f1852.m2192(i3);
                if (this.f1852.m2189()) {
                    return r1;
                }
            }
            if (i4 != 0) {
                this.f1852.m2191();
                this.f1852.m2192(i4);
                if (this.f1852.m2189()) {
                    view = r1;
                }
            }
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m2188(View view, int i) {
        this.f1852.m2193(this.f1853.m2196(), this.f1853.m2194(), this.f1853.m2197(view), this.f1853.m2195(view));
        if (i == 0) {
            return false;
        }
        this.f1852.m2191();
        this.f1852.m2192(i);
        return this.f1852.m2189();
    }
}
