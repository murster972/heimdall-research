package android.support.v7.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;

class FastScroller extends RecyclerView.ItemDecoration implements RecyclerView.OnItemTouchListener {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final int[] f1591 = {16842919};

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final int[] f1592 = new int[0];

    /* renamed from: ʻ  reason: contains not printable characters */
    float f1593;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f1594;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f1595;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f1596 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public final Drawable f1597;

    /* renamed from: ˉ  reason: contains not printable characters */
    private RecyclerView f1598;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int f1599;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final int f1600;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f1601 = 0;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f1602 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f1603;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f1604 = false;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f1605 = 0;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final int f1606;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final StateListDrawable f1607;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f1608 = 0;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final int[] f1609 = new int[2];

    /* renamed from: ᵔ  reason: contains not printable characters */
    private final int[] f1610 = new int[2];
    /* access modifiers changed from: private */

    /* renamed from: ᵢ  reason: contains not printable characters */
    public final ValueAnimator f1611 = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
    /* access modifiers changed from: private */

    /* renamed from: ⁱ  reason: contains not printable characters */
    public int f1612 = 0;

    /* renamed from: 连任  reason: contains not printable characters */
    int f1613;

    /* renamed from: 靐  reason: contains not printable characters */
    int f1614;

    /* renamed from: 麤  reason: contains not printable characters */
    int f1615;

    /* renamed from: 齉  reason: contains not printable characters */
    float f1616;

    /* renamed from: 龘  reason: contains not printable characters */
    int f1617;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final Runnable f1618 = new Runnable() {
        public void run() {
            FastScroller.this.m1945(500);
        }
    };

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final StateListDrawable f1619;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final Drawable f1620;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private final RecyclerView.OnScrollListener f1621 = new RecyclerView.OnScrollListener() {
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            FastScroller.this.m1946(recyclerView.computeHorizontalScrollOffset(), recyclerView.computeVerticalScrollOffset());
        }
    };

    private class AnimatorListener extends AnimatorListenerAdapter {

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f1624;

        private AnimatorListener() {
            this.f1624 = false;
        }

        public void onAnimationCancel(Animator animator) {
            this.f1624 = true;
        }

        public void onAnimationEnd(Animator animator) {
            if (this.f1624) {
                this.f1624 = false;
            } else if (((Float) FastScroller.this.f1611.getAnimatedValue()).floatValue() == 0.0f) {
                int unused = FastScroller.this.f1612 = 0;
                FastScroller.this.m1929(0);
            } else {
                int unused2 = FastScroller.this.f1612 = 2;
                FastScroller.this.m1934();
            }
        }
    }

    private class AnimatorUpdater implements ValueAnimator.AnimatorUpdateListener {
        private AnimatorUpdater() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int floatValue = (int) (((Float) valueAnimator.getAnimatedValue()).floatValue() * 255.0f);
            FastScroller.this.f1607.setAlpha(floatValue);
            FastScroller.this.f1597.setAlpha(floatValue);
            FastScroller.this.m1934();
        }
    }

    FastScroller(RecyclerView recyclerView, StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2, int i, int i2, int i3) {
        this.f1607 = stateListDrawable;
        this.f1597 = drawable;
        this.f1619 = stateListDrawable2;
        this.f1620 = drawable2;
        this.f1594 = Math.max(i, stateListDrawable.getIntrinsicWidth());
        this.f1595 = Math.max(i, drawable.getIntrinsicWidth());
        this.f1599 = Math.max(i, stateListDrawable2.getIntrinsicWidth());
        this.f1600 = Math.max(i, drawable2.getIntrinsicWidth());
        this.f1603 = i2;
        this.f1606 = i3;
        this.f1607.setAlpha(255);
        this.f1597.setAlpha(255);
        this.f1611.addListener(new AnimatorListener());
        this.f1611.addUpdateListener(new AnimatorUpdater());
        m1947(recyclerView);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1923() {
        this.f1598.removeCallbacks(this.f1618);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int[] m1924() {
        this.f1609[0] = this.f1606;
        this.f1609[1] = this.f1596 - this.f1606;
        return this.f1609;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private int[] m1925() {
        this.f1610[0] = this.f1606;
        this.f1610[1] = this.f1601 - this.f1606;
        return this.f1610;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean m1926() {
        return ViewCompat.getLayoutDirection(this.f1598) == 1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m1927() {
        this.f1598.addItemDecoration(this);
        this.f1598.addOnItemTouchListener(this);
        this.f1598.addOnScrollListener(this.f1621);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m1928(float f) {
        int[] r3 = m1925();
        float max = Math.max((float) r3[0], Math.min((float) r3[1], f));
        if (Math.abs(((float) this.f1613) - max) >= 2.0f) {
            int r7 = m1938(this.f1593, max, r3, this.f1598.computeHorizontalScrollRange(), this.f1598.computeHorizontalScrollOffset(), this.f1601);
            if (r7 != 0) {
                this.f1598.scrollBy(r7, 0);
            }
            this.f1593 = max;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1929(int i) {
        if (i == 2 && this.f1605 != 2) {
            this.f1607.setState(f1591);
            m1923();
        }
        if (i == 0) {
            m1934();
        } else {
            m1944();
        }
        if (this.f1605 == 2 && i != 2) {
            this.f1607.setState(f1592);
            m1937(1200);
        } else if (i == 1) {
            m1937(1500);
        }
        this.f1605 = i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m1930(Canvas canvas) {
        int i = this.f1596 - this.f1599;
        int i2 = this.f1613 - (this.f1615 / 2);
        this.f1619.setBounds(0, 0, this.f1615, this.f1599);
        this.f1620.setBounds(0, 0, this.f1601, this.f1600);
        canvas.translate(0.0f, (float) i);
        this.f1620.draw(canvas);
        canvas.translate((float) i2, 0.0f);
        this.f1619.draw(canvas);
        canvas.translate((float) (-i2), (float) (-i));
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m1934() {
        this.f1598.invalidate();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m1936() {
        this.f1598.removeItemDecoration(this);
        this.f1598.removeOnItemTouchListener(this);
        this.f1598.removeOnScrollListener(this.f1621);
        m1923();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m1937(int i) {
        m1923();
        this.f1598.postDelayed(this.f1618, (long) i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m1938(float f, float f2, int[] iArr, int i, int i2, int i3) {
        int i4 = iArr[1] - iArr[0];
        if (i4 == 0) {
            return 0;
        }
        int i5 = i - i3;
        int i6 = (int) (((float) i5) * ((f2 - f) / ((float) i4)));
        int i7 = i2 + i6;
        if (i7 >= i5 || i7 < 0) {
            return 0;
        }
        return i6;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1941(float f) {
        int[] r3 = m1924();
        float max = Math.max((float) r3[0], Math.min((float) r3[1], f));
        if (Math.abs(((float) this.f1614) - max) >= 2.0f) {
            int r7 = m1938(this.f1616, max, r3, this.f1598.computeVerticalScrollRange(), this.f1598.computeVerticalScrollOffset(), this.f1596);
            if (r7 != 0) {
                this.f1598.scrollBy(0, r7);
            }
            this.f1616 = max;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1942(Canvas canvas) {
        int i = this.f1601 - this.f1594;
        int i2 = this.f1614 - (this.f1617 / 2);
        this.f1607.setBounds(0, 0, this.f1594, this.f1617);
        this.f1597.setBounds(0, 0, this.f1595, this.f1596);
        if (m1926()) {
            this.f1597.draw(canvas);
            canvas.translate((float) this.f1594, (float) i2);
            canvas.scale(-1.0f, 1.0f);
            this.f1607.draw(canvas);
            canvas.scale(1.0f, 1.0f);
            canvas.translate((float) (-this.f1594), (float) (-i2));
            return;
        }
        canvas.translate((float) i, 0.0f);
        this.f1597.draw(canvas);
        canvas.translate(0.0f, (float) i2);
        this.f1607.draw(canvas);
        canvas.translate((float) (-i), (float) (-i2));
    }

    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        if (this.f1601 != this.f1598.getWidth() || this.f1596 != this.f1598.getHeight()) {
            this.f1601 = this.f1598.getWidth();
            this.f1596 = this.f1598.getHeight();
            m1929(0);
        } else if (this.f1612 != 0) {
            if (this.f1602) {
                m1942(canvas);
            }
            if (this.f1604) {
                m1930(canvas);
            }
        }
    }

    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        if (this.f1605 != 1) {
            return this.f1605 == 2;
        }
        boolean r2 = m1948(motionEvent.getX(), motionEvent.getY());
        boolean r1 = m1943(motionEvent.getX(), motionEvent.getY());
        if (motionEvent.getAction() != 0 || (!r2 && !r1)) {
            return false;
        }
        if (r1) {
            this.f1608 = 1;
            this.f1593 = (float) ((int) motionEvent.getX());
        } else if (r2) {
            this.f1608 = 2;
            this.f1616 = (float) ((int) motionEvent.getY());
        }
        m1929(2);
        return true;
    }

    public void onRequestDisallowInterceptTouchEvent(boolean z) {
    }

    public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        if (this.f1605 != 0) {
            if (motionEvent.getAction() == 0) {
                boolean r1 = m1948(motionEvent.getX(), motionEvent.getY());
                boolean r0 = m1943(motionEvent.getX(), motionEvent.getY());
                if (r1 || r0) {
                    if (r0) {
                        this.f1608 = 1;
                        this.f1593 = (float) ((int) motionEvent.getX());
                    } else if (r1) {
                        this.f1608 = 2;
                        this.f1616 = (float) ((int) motionEvent.getY());
                    }
                    m1929(2);
                }
            } else if (motionEvent.getAction() == 1 && this.f1605 == 2) {
                this.f1616 = 0.0f;
                this.f1593 = 0.0f;
                m1929(1);
                this.f1608 = 0;
            } else if (motionEvent.getAction() == 2 && this.f1605 == 2) {
                m1944();
                if (this.f1608 == 1) {
                    m1928(motionEvent.getX());
                }
                if (this.f1608 == 2) {
                    m1941(motionEvent.getY());
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m1943(float f, float f2) {
        return f2 >= ((float) (this.f1596 - this.f1599)) && f >= ((float) (this.f1613 - (this.f1615 / 2))) && f <= ((float) (this.f1613 + (this.f1615 / 2)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1944() {
        switch (this.f1612) {
            case 0:
                break;
            case 3:
                this.f1611.cancel();
                break;
            default:
                return;
        }
        this.f1612 = 1;
        this.f1611.setFloatValues(new float[]{((Float) this.f1611.getAnimatedValue()).floatValue(), 1.0f});
        this.f1611.setDuration(500);
        this.f1611.setStartDelay(0);
        this.f1611.start();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1945(int i) {
        switch (this.f1612) {
            case 1:
                this.f1611.cancel();
                break;
            case 2:
                break;
            default:
                return;
        }
        this.f1612 = 3;
        this.f1611.setFloatValues(new float[]{((Float) this.f1611.getAnimatedValue()).floatValue(), 0.0f});
        this.f1611.setDuration((long) i);
        this.f1611.start();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1946(int i, int i2) {
        int computeVerticalScrollRange = this.f1598.computeVerticalScrollRange();
        int i3 = this.f1596;
        this.f1602 = computeVerticalScrollRange - i3 > 0 && this.f1596 >= this.f1603;
        int computeHorizontalScrollRange = this.f1598.computeHorizontalScrollRange();
        int i4 = this.f1601;
        this.f1604 = computeHorizontalScrollRange - i4 > 0 && this.f1601 >= this.f1603;
        if (this.f1602 || this.f1604) {
            if (this.f1602) {
                this.f1614 = (int) ((((float) i3) * (((float) i2) + (((float) i3) / 2.0f))) / ((float) computeVerticalScrollRange));
                this.f1617 = Math.min(i3, (i3 * i3) / computeVerticalScrollRange);
            }
            if (this.f1604) {
                this.f1613 = (int) ((((float) i4) * (((float) i) + (((float) i4) / 2.0f))) / ((float) computeHorizontalScrollRange));
                this.f1615 = Math.min(i4, (i4 * i4) / computeHorizontalScrollRange);
            }
            if (this.f1605 == 0 || this.f1605 == 1) {
                m1929(1);
            }
        } else if (this.f1605 != 0) {
            m1929(0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1947(RecyclerView recyclerView) {
        if (this.f1598 != recyclerView) {
            if (this.f1598 != null) {
                m1936();
            }
            this.f1598 = recyclerView;
            if (this.f1598 != null) {
                m1927();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1948(float f, float f2) {
        if (!m1926() ? f >= ((float) (this.f1601 - this.f1594)) : f <= ((float) (this.f1594 / 2))) {
            return f2 >= ((float) (this.f1614 - (this.f1617 / 2))) && f2 <= ((float) (this.f1614 + (this.f1617 / 2)));
        }
    }
}
