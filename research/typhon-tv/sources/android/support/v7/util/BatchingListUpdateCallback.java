package android.support.v7.util;

public class BatchingListUpdateCallback implements ListUpdateCallback {

    /* renamed from: 连任  reason: contains not printable characters */
    Object f1108 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    int f1109 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    int f1110 = -1;

    /* renamed from: 齉  reason: contains not printable characters */
    int f1111 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    final ListUpdateCallback f1112;

    public BatchingListUpdateCallback(ListUpdateCallback listUpdateCallback) {
        this.f1112 = listUpdateCallback;
    }

    public void onChanged(int i, int i2, Object obj) {
        if (this.f1109 != 3 || i > this.f1111 + this.f1110 || i + i2 < this.f1111 || this.f1108 != obj) {
            m1403();
            this.f1111 = i;
            this.f1110 = i2;
            this.f1108 = obj;
            this.f1109 = 3;
            return;
        }
        int i3 = this.f1111 + this.f1110;
        this.f1111 = Math.min(i, this.f1111);
        this.f1110 = Math.max(i3, i + i2) - this.f1111;
    }

    public void onInserted(int i, int i2) {
        if (this.f1109 != 1 || i < this.f1111 || i > this.f1111 + this.f1110) {
            m1403();
            this.f1111 = i;
            this.f1110 = i2;
            this.f1109 = 1;
            return;
        }
        this.f1110 += i2;
        this.f1111 = Math.min(i, this.f1111);
    }

    public void onRemoved(int i, int i2) {
        if (this.f1109 != 2 || this.f1111 < i || this.f1111 > i + i2) {
            m1403();
            this.f1111 = i;
            this.f1110 = i2;
            this.f1109 = 2;
            return;
        }
        this.f1110 += i2;
        this.f1111 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1403() {
        if (this.f1109 != 0) {
            switch (this.f1109) {
                case 1:
                    this.f1112.onInserted(this.f1111, this.f1110);
                    break;
                case 2:
                    this.f1112.onRemoved(this.f1111, this.f1110);
                    break;
                case 3:
                    this.f1112.onChanged(this.f1111, this.f1110, this.f1108);
                    break;
            }
            this.f1108 = null;
            this.f1109 = 0;
        }
    }
}
