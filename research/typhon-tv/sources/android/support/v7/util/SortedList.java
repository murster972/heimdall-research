package android.support.v7.util;

import java.lang.reflect.Array;
import java.util.Comparator;

public class SortedList<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private BatchedCallback f1113;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f1114 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Class<T> f1115;

    /* renamed from: 连任  reason: contains not printable characters */
    private Callback f1116;

    /* renamed from: 靐  reason: contains not printable characters */
    private T[] f1117;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f1118;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f1119;

    /* renamed from: 龘  reason: contains not printable characters */
    T[] f1120;

    public static class BatchedCallback<T2> extends Callback<T2> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final BatchingListUpdateCallback f1121 = new BatchingListUpdateCallback(this.f1122);

        /* renamed from: 龘  reason: contains not printable characters */
        final Callback<T2> f1122;

        public BatchedCallback(Callback<T2> callback) {
            this.f1122 = callback;
        }

        public boolean areContentsTheSame(T2 t2, T2 t22) {
            return this.f1122.areContentsTheSame(t2, t22);
        }

        public boolean areItemsTheSame(T2 t2, T2 t22) {
            return this.f1122.areItemsTheSame(t2, t22);
        }

        public int compare(T2 t2, T2 t22) {
            return this.f1122.compare(t2, t22);
        }

        public void onChanged(int i, int i2) {
            this.f1121.onChanged(i, i2, (Object) null);
        }

        public void onInserted(int i, int i2) {
            this.f1121.onInserted(i, i2);
        }

        public void onRemoved(int i, int i2) {
            this.f1121.onRemoved(i, i2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1414() {
            this.f1121.m1403();
        }
    }

    public static abstract class Callback<T2> implements ListUpdateCallback, Comparator<T2> {
        public abstract boolean areContentsTheSame(T2 t2, T2 t22);

        public abstract boolean areItemsTheSame(T2 t2, T2 t22);

        public abstract int compare(T2 t2, T2 t22);

        public abstract void onChanged(int i, int i2);

        public void onChanged(int i, int i2, Object obj) {
            onChanged(i, i2);
        }
    }

    public SortedList(Class<T> cls, Callback<T> callback, int i) {
        this.f1115 = cls;
        this.f1120 = (Object[]) Array.newInstance(cls, i);
        this.f1116 = callback;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m1404() {
        if (this.f1117 != null) {
            throw new IllegalStateException("Cannot call this method from within addAll");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m1405(T t, int i, int i2, int i3) {
        int i4 = i - 1;
        while (i4 >= i2) {
            T t2 = this.f1120[i4];
            if (this.f1116.compare(t2, t) != 0) {
                break;
            } else if (this.f1116.areItemsTheSame(t2, t)) {
                int i5 = i4;
                return i4;
            } else {
                i4--;
            }
        }
        int i6 = i + 1;
        while (i6 < i3) {
            T t3 = this.f1120[i6];
            if (this.f1116.compare(t3, t) != 0) {
                break;
            } else if (this.f1116.areItemsTheSame(t3, t)) {
                int i7 = i6;
                return i6;
            } else {
                i6++;
            }
        }
        int i8 = i6;
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m1406(T t, boolean z) {
        int r7 = m1407(t, this.f1120, 0, this.f1114, 1);
        if (r7 == -1) {
            r7 = 0;
        } else if (r7 < this.f1114) {
            T t2 = this.f1120[r7];
            if (this.f1116.areItemsTheSame(t2, t)) {
                if (this.f1116.areContentsTheSame(t2, t)) {
                    this.f1120[r7] = t;
                    int i = r7;
                    return r7;
                }
                this.f1120[r7] = t;
                this.f1116.onChanged(r7, 1);
                int i2 = r7;
                return r7;
            }
        }
        m1408(r7, t);
        if (z) {
            this.f1116.onInserted(r7, 1);
        }
        int i3 = r7;
        return r7;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m1407(T t, T[] tArr, int i, int i2, int i3) {
        while (i < i2) {
            int i4 = (i + i2) / 2;
            T t2 = tArr[i4];
            int compare = this.f1116.compare(t2, t);
            if (compare < 0) {
                i = i4 + 1;
            } else if (compare != 0) {
                i2 = i4;
            } else if (this.f1116.areItemsTheSame(t2, t)) {
                return i4;
            } else {
                int r1 = m1405(t, i4, i, i2);
                return i3 == 1 ? r1 != -1 ? r1 : i4 : r1;
            }
        }
        if (i3 != 1) {
            i = -1;
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1408(int i, T t) {
        if (i > this.f1114) {
            throw new IndexOutOfBoundsException("cannot add item to " + i + " because size is " + this.f1114);
        }
        if (this.f1114 == this.f1120.length) {
            T[] tArr = (Object[]) ((Object[]) Array.newInstance(this.f1115, this.f1120.length + 10));
            System.arraycopy(this.f1120, 0, tArr, 0, i);
            tArr[i] = t;
            System.arraycopy(this.f1120, i, tArr, i + 1, this.f1114 - i);
            this.f1120 = tArr;
        } else {
            System.arraycopy(this.f1120, i, this.f1120, i + 1, this.f1114 - i);
            this.f1120[i] = t;
        }
        this.f1114++;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1409() {
        m1404();
        if (!(this.f1116 instanceof BatchedCallback)) {
            if (this.f1113 == null) {
                this.f1113 = new BatchedCallback(this.f1116);
            }
            this.f1116 = this.f1113;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1410() {
        m1404();
        if (this.f1116 instanceof BatchedCallback) {
            ((BatchedCallback) this.f1116).m1414();
        }
        if (this.f1116 == this.f1113) {
            this.f1116 = this.f1113.f1122;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m1411() {
        return this.f1114;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m1412(T t) {
        m1404();
        return m1406(t, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public T m1413(int i) throws IndexOutOfBoundsException {
        if (i < this.f1114 && i >= 0) {
            return (this.f1117 == null || i < this.f1118) ? this.f1120[i] : this.f1117[(i - this.f1118) + this.f1119];
        }
        throw new IndexOutOfBoundsException("Asked to get item at " + i + " but size is " + this.f1114);
    }
}
