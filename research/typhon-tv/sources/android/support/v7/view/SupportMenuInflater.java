package android.support.v7.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.support.annotation.RestrictTo;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuItemWrapperICS;
import android.support.v7.widget.DrawableUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@RestrictTo
public class SupportMenuInflater extends MenuInflater {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Class<?>[] f1144 = f1145;

    /* renamed from: 龘  reason: contains not printable characters */
    static final Class<?>[] f1145 = {Context.class};

    /* renamed from: ʻ  reason: contains not printable characters */
    private Object f1146;

    /* renamed from: 连任  reason: contains not printable characters */
    Context f1147;

    /* renamed from: 麤  reason: contains not printable characters */
    final Object[] f1148 = this.f1149;

    /* renamed from: 齉  reason: contains not printable characters */
    final Object[] f1149;

    private static class InflatedOnMenuItemClickListener implements MenuItem.OnMenuItemClickListener {

        /* renamed from: 龘  reason: contains not printable characters */
        private static final Class<?>[] f1150 = {MenuItem.class};

        /* renamed from: 靐  reason: contains not printable characters */
        private Object f1151;

        /* renamed from: 齉  reason: contains not printable characters */
        private Method f1152;

        public InflatedOnMenuItemClickListener(Object obj, String str) {
            this.f1151 = obj;
            Class<?> cls = obj.getClass();
            try {
                this.f1152 = cls.getMethod(str, f1150);
            } catch (Exception e) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e);
                throw inflateException;
            }
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.f1152.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.f1152.invoke(this.f1151, new Object[]{menuItem})).booleanValue();
                }
                this.f1152.invoke(this.f1151, new Object[]{menuItem});
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private class MenuState {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f1153;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f1154;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f1155;

        /* renamed from: ʾ  reason: contains not printable characters */
        private CharSequence f1156;

        /* renamed from: ʿ  reason: contains not printable characters */
        private CharSequence f1157;

        /* renamed from: ˆ  reason: contains not printable characters */
        private int f1158;

        /* renamed from: ˈ  reason: contains not printable characters */
        private int f1159;

        /* renamed from: ˉ  reason: contains not printable characters */
        private boolean f1160;

        /* renamed from: ˊ  reason: contains not printable characters */
        private int f1161;

        /* renamed from: ˋ  reason: contains not printable characters */
        private char f1162;

        /* renamed from: ˎ  reason: contains not printable characters */
        private int f1163;

        /* renamed from: ˏ  reason: contains not printable characters */
        private boolean f1164;

        /* renamed from: ˑ  reason: contains not printable characters */
        private boolean f1165;

        /* renamed from: י  reason: contains not printable characters */
        private boolean f1166;

        /* renamed from: ـ  reason: contains not printable characters */
        private int f1167;

        /* renamed from: ٴ  reason: contains not printable characters */
        private boolean f1168;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private int f1169;

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        private PorterDuff.Mode f1170 = null;

        /* renamed from: ᴵ  reason: contains not printable characters */
        private int f1171;

        /* renamed from: ᵎ  reason: contains not printable characters */
        private String f1172;

        /* renamed from: ᵔ  reason: contains not printable characters */
        private String f1173;

        /* renamed from: ᵢ  reason: contains not printable characters */
        private String f1174;

        /* renamed from: ⁱ  reason: contains not printable characters */
        private CharSequence f1175;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f1176;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f1178;

        /* renamed from: 齉  reason: contains not printable characters */
        private Menu f1179;

        /* renamed from: 龘  reason: contains not printable characters */
        ActionProvider f1180;

        /* renamed from: ﹳ  reason: contains not printable characters */
        private CharSequence f1181;

        /* renamed from: ﹶ  reason: contains not printable characters */
        private int f1182;

        /* renamed from: ﾞ  reason: contains not printable characters */
        private char f1183;

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        private ColorStateList f1184 = null;

        public MenuState(Menu menu) {
            this.f1179 = menu;
            m1478();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private char m1471(String str) {
            if (str == null) {
                return 0;
            }
            return str.charAt(0);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private <T> T m1472(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor<?> constructor = SupportMenuInflater.this.f1147.getClassLoader().loadClass(str).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (Exception e) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e);
                return null;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m1473(MenuItem menuItem) {
            menuItem.setChecked(this.f1160).setVisible(this.f1164).setEnabled(this.f1166).setCheckable(this.f1158 >= 1).setTitleCondensed(this.f1157).setIcon(this.f1182);
            if (this.f1167 >= 0) {
                menuItem.setShowAsAction(this.f1167);
            }
            if (this.f1174 != null) {
                if (SupportMenuInflater.this.f1147.isRestricted()) {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
                menuItem.setOnMenuItemClickListener(new InflatedOnMenuItemClickListener(SupportMenuInflater.this.m1470(), this.f1174));
            }
            if (menuItem instanceof MenuItemImpl) {
                MenuItemImpl menuItemImpl = (MenuItemImpl) menuItem;
            }
            if (this.f1158 >= 2) {
                if (menuItem instanceof MenuItemImpl) {
                    ((MenuItemImpl) menuItem).m1571(true);
                } else if (menuItem instanceof MenuItemWrapperICS) {
                    ((MenuItemWrapperICS) menuItem).m1574(true);
                }
            }
            boolean z = false;
            if (this.f1172 != null) {
                menuItem.setActionView((View) m1472(this.f1172, SupportMenuInflater.f1145, SupportMenuInflater.this.f1149));
                z = true;
            }
            if (this.f1171 > 0) {
                if (!z) {
                    menuItem.setActionView(this.f1171);
                } else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            if (this.f1180 != null) {
                MenuItemCompat.setActionProvider(menuItem, this.f1180);
            }
            MenuItemCompat.setContentDescription(menuItem, this.f1175);
            MenuItemCompat.setTooltipText(menuItem, this.f1181);
            MenuItemCompat.setAlphabeticShortcut(menuItem, this.f1183, this.f1161);
            MenuItemCompat.setNumericShortcut(menuItem, this.f1162, this.f1163);
            if (this.f1170 != null) {
                MenuItemCompat.setIconTintMode(menuItem, this.f1170);
            }
            if (this.f1184 != null) {
                MenuItemCompat.setIconTintList(menuItem, this.f1184);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1474() {
            this.f1168 = true;
            m1473(this.f1179.add(this.f1178, this.f1169, this.f1159, this.f1156));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1475(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = SupportMenuInflater.this.f1147.obtainStyledAttributes(attributeSet, R.styleable.MenuItem);
            this.f1169 = obtainStyledAttributes.getResourceId(R.styleable.MenuItem_android_id, 0);
            this.f1159 = (-65536 & obtainStyledAttributes.getInt(R.styleable.MenuItem_android_menuCategory, this.f1176)) | (65535 & obtainStyledAttributes.getInt(R.styleable.MenuItem_android_orderInCategory, this.f1153));
            this.f1156 = obtainStyledAttributes.getText(R.styleable.MenuItem_android_title);
            this.f1157 = obtainStyledAttributes.getText(R.styleable.MenuItem_android_titleCondensed);
            this.f1182 = obtainStyledAttributes.getResourceId(R.styleable.MenuItem_android_icon, 0);
            this.f1183 = m1471(obtainStyledAttributes.getString(R.styleable.MenuItem_android_alphabeticShortcut));
            this.f1161 = obtainStyledAttributes.getInt(R.styleable.MenuItem_alphabeticModifiers, 4096);
            this.f1162 = m1471(obtainStyledAttributes.getString(R.styleable.MenuItem_android_numericShortcut));
            this.f1163 = obtainStyledAttributes.getInt(R.styleable.MenuItem_numericModifiers, 4096);
            if (obtainStyledAttributes.hasValue(R.styleable.MenuItem_android_checkable)) {
                this.f1158 = obtainStyledAttributes.getBoolean(R.styleable.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.f1158 = this.f1154;
            }
            this.f1160 = obtainStyledAttributes.getBoolean(R.styleable.MenuItem_android_checked, false);
            this.f1164 = obtainStyledAttributes.getBoolean(R.styleable.MenuItem_android_visible, this.f1155);
            this.f1166 = obtainStyledAttributes.getBoolean(R.styleable.MenuItem_android_enabled, this.f1165);
            this.f1167 = obtainStyledAttributes.getInt(R.styleable.MenuItem_showAsAction, -1);
            this.f1174 = obtainStyledAttributes.getString(R.styleable.MenuItem_android_onClick);
            this.f1171 = obtainStyledAttributes.getResourceId(R.styleable.MenuItem_actionLayout, 0);
            this.f1172 = obtainStyledAttributes.getString(R.styleable.MenuItem_actionViewClass);
            this.f1173 = obtainStyledAttributes.getString(R.styleable.MenuItem_actionProviderClass);
            boolean z = this.f1173 != null;
            if (z && this.f1171 == 0 && this.f1172 == null) {
                this.f1180 = (ActionProvider) m1472(this.f1173, SupportMenuInflater.f1144, SupportMenuInflater.this.f1148);
            } else {
                if (z) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.f1180 = null;
            }
            this.f1175 = obtainStyledAttributes.getText(R.styleable.MenuItem_contentDescription);
            this.f1181 = obtainStyledAttributes.getText(R.styleable.MenuItem_tooltipText);
            if (obtainStyledAttributes.hasValue(R.styleable.MenuItem_iconTintMode)) {
                this.f1170 = DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(R.styleable.MenuItem_iconTintMode, -1), this.f1170);
            } else {
                this.f1170 = null;
            }
            if (obtainStyledAttributes.hasValue(R.styleable.MenuItem_iconTint)) {
                this.f1184 = obtainStyledAttributes.getColorStateList(R.styleable.MenuItem_iconTint);
            } else {
                this.f1184 = null;
            }
            obtainStyledAttributes.recycle();
            this.f1168 = false;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean m1476() {
            return this.f1168;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public SubMenu m1477() {
            this.f1168 = true;
            SubMenu addSubMenu = this.f1179.addSubMenu(this.f1178, this.f1169, this.f1159, this.f1156);
            m1473(addSubMenu.getItem());
            return addSubMenu;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1478() {
            this.f1178 = 0;
            this.f1176 = 0;
            this.f1153 = 0;
            this.f1154 = 0;
            this.f1155 = true;
            this.f1165 = true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1479(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = SupportMenuInflater.this.f1147.obtainStyledAttributes(attributeSet, R.styleable.MenuGroup);
            this.f1178 = obtainStyledAttributes.getResourceId(R.styleable.MenuGroup_android_id, 0);
            this.f1176 = obtainStyledAttributes.getInt(R.styleable.MenuGroup_android_menuCategory, 0);
            this.f1153 = obtainStyledAttributes.getInt(R.styleable.MenuGroup_android_orderInCategory, 0);
            this.f1154 = obtainStyledAttributes.getInt(R.styleable.MenuGroup_android_checkableBehavior, 0);
            this.f1155 = obtainStyledAttributes.getBoolean(R.styleable.MenuGroup_android_visible, true);
            this.f1165 = obtainStyledAttributes.getBoolean(R.styleable.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }
    }

    public SupportMenuInflater(Context context) {
        super(context);
        this.f1147 = context;
        this.f1149 = new Object[]{context};
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Object m1468(Object obj) {
        return (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) ? m1468(((ContextWrapper) obj).getBaseContext()) : obj;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1469(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) throws XmlPullParserException, IOException {
        MenuState menuState = new MenuState(menu);
        int eventType = xmlPullParser.getEventType();
        boolean z = false;
        String str = null;
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        boolean z2 = false;
        while (!z2) {
            switch (eventType) {
                case 1:
                    throw new RuntimeException("Unexpected end of document");
                case 2:
                    if (z) {
                        break;
                    } else {
                        String name2 = xmlPullParser.getName();
                        if (!name2.equals("group")) {
                            if (!name2.equals("item")) {
                                if (!name2.equals("menu")) {
                                    z = true;
                                    str = name2;
                                    break;
                                } else {
                                    m1469(xmlPullParser, attributeSet, menuState.m1477());
                                    break;
                                }
                            } else {
                                menuState.m1475(attributeSet);
                                break;
                            }
                        } else {
                            menuState.m1479(attributeSet);
                            break;
                        }
                    }
                case 3:
                    String name3 = xmlPullParser.getName();
                    if (!z || !name3.equals(str)) {
                        if (!name3.equals("group")) {
                            if (!name3.equals("item")) {
                                if (!name3.equals("menu")) {
                                    break;
                                } else {
                                    z2 = true;
                                    break;
                                }
                            } else if (!menuState.m1476()) {
                                if (menuState.f1180 != null && menuState.f1180.hasSubMenu()) {
                                    menuState.m1477();
                                    break;
                                } else {
                                    menuState.m1474();
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            menuState.m1478();
                            break;
                        }
                    } else {
                        z = false;
                        str = null;
                        break;
                    }
                    break;
            }
            eventType = xmlPullParser.next();
        }
    }

    public void inflate(int i, Menu menu) {
        if (!(menu instanceof SupportMenu)) {
            super.inflate(i, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.f1147.getResources().getLayout(i);
            m1469(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e) {
            throw new InflateException("Error inflating menu XML", e);
        } catch (IOException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Object m1470() {
        if (this.f1146 == null) {
            this.f1146 = m1468(this.f1147);
        }
        return this.f1146;
    }
}
