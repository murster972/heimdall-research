package android.support.v7.view;

import android.content.Context;
import android.support.annotation.RestrictTo;
import android.support.v7.view.ActionMode;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

@RestrictTo
public class StandaloneActionMode extends ActionMode implements MenuBuilder.Callback {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f1131;

    /* renamed from: ʼ  reason: contains not printable characters */
    private MenuBuilder f1132;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f1133;

    /* renamed from: 靐  reason: contains not printable characters */
    private ActionBarContextView f1134;

    /* renamed from: 麤  reason: contains not printable characters */
    private WeakReference<View> f1135;

    /* renamed from: 齉  reason: contains not printable characters */
    private ActionMode.Callback f1136;

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f1137;

    public StandaloneActionMode(Context context, ActionBarContextView actionBarContextView, ActionMode.Callback callback, boolean z) {
        this.f1137 = context;
        this.f1134 = actionBarContextView;
        this.f1136 = callback;
        this.f1132 = new MenuBuilder(actionBarContextView.getContext()).setDefaultShowAsAction(1);
        this.f1132.setCallback(this);
        this.f1131 = z;
    }

    public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
        return this.f1136.m1443((ActionMode) this, menuItem);
    }

    public void onMenuModeChange(MenuBuilder menuBuilder) {
        m1455();
        this.f1134.showOverflowMenu();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public CharSequence m1448() {
        return this.f1134.getTitle();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public CharSequence m1449() {
        return this.f1134.getSubtitle();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1450() {
        return this.f1134.isTitleOptional();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public View m1451() {
        if (this.f1135 != null) {
            return (View) this.f1135.get();
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Menu m1452() {
        return this.f1132;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1453(int i) {
        m1460((CharSequence) this.f1137.getString(i));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1454(CharSequence charSequence) {
        this.f1134.setTitle(charSequence);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m1455() {
        this.f1136.m1440(this, this.f1132);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1456() {
        if (!this.f1133) {
            this.f1133 = true;
            this.f1134.sendAccessibilityEvent(32);
            this.f1136.m1441(this);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MenuInflater m1457() {
        return new SupportMenuInflater(this.f1134.getContext());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1458(int i) {
        m1454((CharSequence) this.f1137.getString(i));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1459(View view) {
        this.f1134.setCustomView(view);
        this.f1135 = view != null ? new WeakReference<>(view) : null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1460(CharSequence charSequence) {
        this.f1134.setSubtitle(charSequence);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1461(boolean z) {
        super.m1439(z);
        this.f1134.setTitleOptional(z);
    }
}
