package android.support.v7.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.RestrictTo;
import android.support.v7.appcompat.R;
import android.view.LayoutInflater;

@RestrictTo
public class ContextThemeWrapper extends ContextWrapper {

    /* renamed from: 连任  reason: contains not printable characters */
    private Resources f1126;

    /* renamed from: 靐  reason: contains not printable characters */
    private Resources.Theme f1127;

    /* renamed from: 麤  reason: contains not printable characters */
    private Configuration f1128;

    /* renamed from: 齉  reason: contains not printable characters */
    private LayoutInflater f1129;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f1130;

    public ContextThemeWrapper() {
        super((Context) null);
    }

    public ContextThemeWrapper(Context context, int i) {
        super(context);
        this.f1130 = i;
    }

    public ContextThemeWrapper(Context context, Resources.Theme theme) {
        super(context);
        this.f1127 = theme;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Resources m1444() {
        if (this.f1126 == null) {
            if (this.f1128 == null) {
                this.f1126 = super.getResources();
            } else if (Build.VERSION.SDK_INT >= 17) {
                this.f1126 = createConfigurationContext(this.f1128).getResources();
            }
        }
        return this.f1126;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m1445() {
        boolean z = this.f1127 == null;
        if (z) {
            this.f1127 = getResources().newTheme();
            Resources.Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.f1127.setTo(theme);
            }
        }
        m1447(this.f1127, this.f1130, z);
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    public AssetManager getAssets() {
        return getResources().getAssets();
    }

    public Resources getResources() {
        return m1444();
    }

    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.f1129 == null) {
            this.f1129 = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.f1129;
    }

    public Resources.Theme getTheme() {
        if (this.f1127 != null) {
            return this.f1127;
        }
        if (this.f1130 == 0) {
            this.f1130 = R.style.Theme_AppCompat_Light;
        }
        m1445();
        return this.f1127;
    }

    public void setTheme(int i) {
        if (this.f1130 != i) {
            this.f1130 = i;
            m1445();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m1446() {
        return this.f1130;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1447(Resources.Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }
}
