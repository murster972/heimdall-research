package android.support.v7.view;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public abstract class ActionMode {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f1124;

    /* renamed from: 龘  reason: contains not printable characters */
    private Object f1125;

    public interface Callback {
        /* renamed from: 靐  reason: contains not printable characters */
        boolean m1440(ActionMode actionMode, Menu menu);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1441(ActionMode actionMode);

        /* renamed from: 龘  reason: contains not printable characters */
        boolean m1442(ActionMode actionMode, Menu menu);

        /* renamed from: 龘  reason: contains not printable characters */
        boolean m1443(ActionMode actionMode, MenuItem menuItem);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract CharSequence m1423();

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract CharSequence m1424();

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1425() {
        return false;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public abstract View m1426();

    /* renamed from: ٴ  reason: contains not printable characters */
    public Object m1427() {
        return this.f1125;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m1428() {
        return this.f1124;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract Menu m1429();

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m1430(int i);

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m1431(CharSequence charSequence);

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract void m1432();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m1433();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract MenuInflater m1434();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m1435(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m1436(View view);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m1437(CharSequence charSequence);

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1438(Object obj) {
        this.f1125 = obj;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1439(boolean z) {
        this.f1124 = z;
    }
}
