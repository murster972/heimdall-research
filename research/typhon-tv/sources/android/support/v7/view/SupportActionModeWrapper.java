package android.support.v7.view;

import android.content.Context;
import android.support.annotation.RestrictTo;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.view.ActionMode;
import android.support.v7.view.menu.MenuWrapperFactory;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;

@RestrictTo
public class SupportActionModeWrapper extends ActionMode {

    /* renamed from: 靐  reason: contains not printable characters */
    final ActionMode f1138;

    /* renamed from: 龘  reason: contains not printable characters */
    final Context f1139;

    @RestrictTo
    public static class CallbackWrapper implements ActionMode.Callback {

        /* renamed from: 靐  reason: contains not printable characters */
        final Context f1140;

        /* renamed from: 麤  reason: contains not printable characters */
        final SimpleArrayMap<Menu, Menu> f1141 = new SimpleArrayMap<>();

        /* renamed from: 齉  reason: contains not printable characters */
        final ArrayList<SupportActionModeWrapper> f1142 = new ArrayList<>();

        /* renamed from: 龘  reason: contains not printable characters */
        final ActionMode.Callback f1143;

        public CallbackWrapper(Context context, ActionMode.Callback callback) {
            this.f1140 = context;
            this.f1143 = callback;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Menu m1462(Menu menu) {
            Menu menu2 = this.f1141.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            Menu r0 = MenuWrapperFactory.m1608(this.f1140, (SupportMenu) menu);
            this.f1141.put(menu, r0);
            return r0;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public android.view.ActionMode m1463(ActionMode actionMode) {
            int size = this.f1142.size();
            for (int i = 0; i < size; i++) {
                SupportActionModeWrapper supportActionModeWrapper = this.f1142.get(i);
                if (supportActionModeWrapper != null && supportActionModeWrapper.f1138 == actionMode) {
                    return supportActionModeWrapper;
                }
            }
            SupportActionModeWrapper supportActionModeWrapper2 = new SupportActionModeWrapper(this.f1140, actionMode);
            this.f1142.add(supportActionModeWrapper2);
            return supportActionModeWrapper2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m1464(ActionMode actionMode, Menu menu) {
            return this.f1143.onPrepareActionMode(m1463(actionMode), m1462(menu));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1465(ActionMode actionMode) {
            this.f1143.onDestroyActionMode(m1463(actionMode));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1466(ActionMode actionMode, Menu menu) {
            return this.f1143.onCreateActionMode(m1463(actionMode), m1462(menu));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1467(ActionMode actionMode, MenuItem menuItem) {
            return this.f1143.onActionItemClicked(m1463(actionMode), MenuWrapperFactory.m1609(this.f1140, (SupportMenuItem) menuItem));
        }
    }

    public SupportActionModeWrapper(Context context, ActionMode actionMode) {
        this.f1139 = context;
        this.f1138 = actionMode;
    }

    public void finish() {
        this.f1138.m1433();
    }

    public View getCustomView() {
        return this.f1138.m1426();
    }

    public Menu getMenu() {
        return MenuWrapperFactory.m1608(this.f1139, (SupportMenu) this.f1138.m1429());
    }

    public MenuInflater getMenuInflater() {
        return this.f1138.m1434();
    }

    public CharSequence getSubtitle() {
        return this.f1138.m1424();
    }

    public Object getTag() {
        return this.f1138.m1427();
    }

    public CharSequence getTitle() {
        return this.f1138.m1423();
    }

    public boolean getTitleOptionalHint() {
        return this.f1138.m1428();
    }

    public void invalidate() {
        this.f1138.m1432();
    }

    public boolean isTitleOptional() {
        return this.f1138.m1425();
    }

    public void setCustomView(View view) {
        this.f1138.m1436(view);
    }

    public void setSubtitle(int i) {
        this.f1138.m1430(i);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f1138.m1437(charSequence);
    }

    public void setTag(Object obj) {
        this.f1138.m1438(obj);
    }

    public void setTitle(int i) {
        this.f1138.m1435(i);
    }

    public void setTitle(CharSequence charSequence) {
        this.f1138.m1431(charSequence);
    }

    public void setTitleOptionalHint(boolean z) {
        this.f1138.m1439(z);
    }
}
