package android.support.v7.view;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.RestrictTo;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.view.ViewConfiguration;

@RestrictTo
public class ActionBarPolicy {

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f1123;

    private ActionBarPolicy(Context context) {
        this.f1123 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ActionBarPolicy m1415(Context context) {
        return new ActionBarPolicy(context);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1416() {
        return this.f1123.getApplicationInfo().targetSdkVersion < 14;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m1417() {
        return this.f1123.getResources().getDimensionPixelSize(R.dimen.abc_action_bar_stacked_tab_max_width);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m1418() {
        TypedArray obtainStyledAttributes = this.f1123.obtainStyledAttributes((AttributeSet) null, R.styleable.ActionBar, R.attr.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(R.styleable.ActionBar_height, 0);
        Resources resources = this.f1123.getResources();
        if (!m1420()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(R.dimen.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m1419() {
        return Build.VERSION.SDK_INT >= 19 || !ViewConfiguration.get(this.f1123).hasPermanentMenuKey();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m1420() {
        return this.f1123.getResources().getBoolean(R.bool.abc_action_bar_embed_tabs);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m1421() {
        return this.f1123.getResources().getDisplayMetrics().widthPixels / 2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m1422() {
        Configuration configuration = this.f1123.getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        int i2 = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || i > 600 || ((i > 960 && i2 > 720) || (i > 720 && i2 > 960))) {
            return 5;
        }
        if (i >= 500 || ((i > 640 && i2 > 480) || (i > 480 && i2 > 640))) {
            return 4;
        }
        return i >= 360 ? 3 : 2;
    }
}
