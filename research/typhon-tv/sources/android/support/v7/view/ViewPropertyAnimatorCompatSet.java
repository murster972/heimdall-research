package android.support.v7.view;

import android.support.annotation.RestrictTo;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

@RestrictTo
public class ViewPropertyAnimatorCompatSet {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final ViewPropertyAnimatorListenerAdapter f1185 = new ViewPropertyAnimatorListenerAdapter() {

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f1191 = false;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f1192 = 0;

        public void onAnimationEnd(View view) {
            int i = this.f1192 + 1;
            this.f1192 = i;
            if (i == ViewPropertyAnimatorCompatSet.this.f1190.size()) {
                if (ViewPropertyAnimatorCompatSet.this.f1187 != null) {
                    ViewPropertyAnimatorCompatSet.this.f1187.onAnimationEnd((View) null);
                }
                m1488();
            }
        }

        public void onAnimationStart(View view) {
            if (!this.f1191) {
                this.f1191 = true;
                if (ViewPropertyAnimatorCompatSet.this.f1187 != null) {
                    ViewPropertyAnimatorCompatSet.this.f1187.onAnimationStart((View) null);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1488() {
            this.f1192 = 0;
            this.f1191 = false;
            ViewPropertyAnimatorCompatSet.this.m1480();
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f1186;

    /* renamed from: 靐  reason: contains not printable characters */
    ViewPropertyAnimatorListener f1187;

    /* renamed from: 麤  reason: contains not printable characters */
    private Interpolator f1188;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f1189 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    final ArrayList<ViewPropertyAnimatorCompat> f1190 = new ArrayList<>();

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1480() {
        this.f1186 = false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1481() {
        if (this.f1186) {
            Iterator<ViewPropertyAnimatorCompat> it2 = this.f1190.iterator();
            while (it2.hasNext()) {
                it2.next().cancel();
            }
            this.f1186 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimatorCompatSet m1482(long j) {
        if (!this.f1186) {
            this.f1189 = j;
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimatorCompatSet m1483(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat) {
        if (!this.f1186) {
            this.f1190.add(viewPropertyAnimatorCompat);
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimatorCompatSet m1484(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2) {
        this.f1190.add(viewPropertyAnimatorCompat);
        viewPropertyAnimatorCompat2.setStartDelay(viewPropertyAnimatorCompat.getDuration());
        this.f1190.add(viewPropertyAnimatorCompat2);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimatorCompatSet m1485(ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        if (!this.f1186) {
            this.f1187 = viewPropertyAnimatorListener;
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimatorCompatSet m1486(Interpolator interpolator) {
        if (!this.f1186) {
            this.f1188 = interpolator;
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1487() {
        if (!this.f1186) {
            Iterator<ViewPropertyAnimatorCompat> it2 = this.f1190.iterator();
            while (it2.hasNext()) {
                ViewPropertyAnimatorCompat next = it2.next();
                if (this.f1189 >= 0) {
                    next.setDuration(this.f1189);
                }
                if (this.f1188 != null) {
                    next.setInterpolator(this.f1188);
                }
                if (this.f1187 != null) {
                    next.setListener(this.f1185);
                }
                next.start();
            }
            this.f1186 = true;
        }
    }
}
