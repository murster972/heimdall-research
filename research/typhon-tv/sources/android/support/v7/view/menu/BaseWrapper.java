package android.support.v7.view.menu;

class BaseWrapper<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final T f1242;

    BaseWrapper(T t) {
        if (t == null) {
            throw new IllegalArgumentException("Wrapped Object can not be null.");
        }
        this.f1242 = t;
    }
}
