package android.support.v7.view.menu;

import android.content.Context;
import android.support.annotation.RestrictTo;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.view.ActionProvider;
import android.support.v7.view.menu.MenuItemWrapperICS;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;

@RestrictTo
class MenuItemWrapperJB extends MenuItemWrapperICS {

    class ActionProviderWrapperJB extends MenuItemWrapperICS.ActionProviderWrapper implements ActionProvider.VisibilityListener {

        /* renamed from: 齉  reason: contains not printable characters */
        ActionProvider.VisibilityListener f1362;

        public ActionProviderWrapperJB(Context context, android.view.ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        public boolean isVisible() {
            return this.f1357.isVisible();
        }

        public void onActionProviderVisibilityChanged(boolean z) {
            if (this.f1362 != null) {
                this.f1362.onActionProviderVisibilityChanged(z);
            }
        }

        public View onCreateActionView(MenuItem menuItem) {
            return this.f1357.onCreateActionView(menuItem);
        }

        public boolean overridesItemVisibility() {
            return this.f1357.overridesItemVisibility();
        }

        public void refreshVisibility() {
            this.f1357.refreshVisibility();
        }

        public void setVisibilityListener(ActionProvider.VisibilityListener visibilityListener) {
            this.f1362 = visibilityListener;
            android.view.ActionProvider actionProvider = this.f1357;
            if (visibilityListener == null) {
                this = null;
            }
            actionProvider.setVisibilityListener(this);
        }
    }

    MenuItemWrapperJB(Context context, SupportMenuItem supportMenuItem) {
        super(context, supportMenuItem);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public MenuItemWrapperICS.ActionProviderWrapper m1576(android.view.ActionProvider actionProvider) {
        return new ActionProviderWrapperJB(this.f1241, actionProvider);
    }
}
