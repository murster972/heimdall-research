package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.widget.MenuItemHoverListener;
import android.support.v7.widget.MenuPopupWindow;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

final class CascadingMenuPopup extends MenuPopup implements MenuPresenter, View.OnKeyListener, PopupWindow.OnDismissListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f1243;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f1244;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f1245;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final MenuItemHoverListener f1246 = new MenuItemHoverListener() {
        public void onItemHoverEnter(final MenuBuilder menuBuilder, final MenuItem menuItem) {
            CascadingMenuPopup.this.f1266.removeCallbacksAndMessages((Object) null);
            int i = -1;
            int i2 = 0;
            int size = CascadingMenuPopup.this.f1263.size();
            while (true) {
                if (i2 >= size) {
                    break;
                } else if (menuBuilder == CascadingMenuPopup.this.f1263.get(i2).f1276) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i != -1) {
                int i3 = i + 1;
                final CascadingMenuInfo cascadingMenuInfo = i3 < CascadingMenuPopup.this.f1263.size() ? CascadingMenuPopup.this.f1263.get(i3) : null;
                CascadingMenuPopup.this.f1266.postAtTime(new Runnable() {
                    public void run() {
                        if (cascadingMenuInfo != null) {
                            CascadingMenuPopup.this.f1264 = true;
                            cascadingMenuInfo.f1276.close(false);
                            CascadingMenuPopup.this.f1264 = false;
                        }
                        if (menuItem.isEnabled() && menuItem.hasSubMenu()) {
                            menuBuilder.performItemAction(menuItem, 4);
                        }
                    }
                }, menuBuilder, SystemClock.uptimeMillis() + 200);
            }
        }

        public void onItemHoverExit(MenuBuilder menuBuilder, MenuItem menuItem) {
            CascadingMenuPopup.this.f1266.removeCallbacksAndMessages(menuBuilder);
        }
    };

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f1247 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f1248;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final View.OnAttachStateChangeListener f1249 = new View.OnAttachStateChangeListener() {
        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            if (CascadingMenuPopup.this.f1260 != null) {
                if (!CascadingMenuPopup.this.f1260.isAlive()) {
                    ViewTreeObserver unused = CascadingMenuPopup.this.f1260 = view.getViewTreeObserver();
                }
                CascadingMenuPopup.this.f1260.removeGlobalOnLayoutListener(CascadingMenuPopup.this.f1259);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f1250;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f1251;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f1252;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f1253;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f1254;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final boolean f1255;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f1256;

    /* renamed from: ـ  reason: contains not printable characters */
    private MenuPresenter.Callback f1257;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final List<MenuBuilder> f1258 = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final ViewTreeObserver.OnGlobalLayoutListener f1259 = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (CascadingMenuPopup.this.isShowing() && CascadingMenuPopup.this.f1263.size() > 0 && !CascadingMenuPopup.this.f1263.get(0).f1278.isModal()) {
                View view = CascadingMenuPopup.this.f1265;
                if (view == null || !view.isShown()) {
                    CascadingMenuPopup.this.dismiss();
                    return;
                }
                for (CascadingMenuInfo cascadingMenuInfo : CascadingMenuPopup.this.f1263) {
                    cascadingMenuInfo.f1278.show();
                }
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ᴵ  reason: contains not printable characters */
    public ViewTreeObserver f1260;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private PopupWindow.OnDismissListener f1261;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Context f1262;

    /* renamed from: 靐  reason: contains not printable characters */
    final List<CascadingMenuInfo> f1263 = new ArrayList();

    /* renamed from: 麤  reason: contains not printable characters */
    boolean f1264;

    /* renamed from: 齉  reason: contains not printable characters */
    View f1265;

    /* renamed from: 龘  reason: contains not printable characters */
    final Handler f1266;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f1267 = 0;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private View f1268;

    private static class CascadingMenuInfo {

        /* renamed from: 靐  reason: contains not printable characters */
        public final MenuBuilder f1276;

        /* renamed from: 齉  reason: contains not printable characters */
        public final int f1277;

        /* renamed from: 龘  reason: contains not printable characters */
        public final MenuPopupWindow f1278;

        public CascadingMenuInfo(MenuPopupWindow menuPopupWindow, MenuBuilder menuBuilder, int i) {
            this.f1278 = menuPopupWindow;
            this.f1276 = menuBuilder;
            this.f1277 = i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ListView m1530() {
            return this.f1278.getListView();
        }
    }

    public CascadingMenuPopup(Context context, View view, int i, int i2, boolean z) {
        this.f1262 = context;
        this.f1268 = view;
        this.f1244 = i;
        this.f1245 = i2;
        this.f1255 = z;
        this.f1254 = false;
        this.f1251 = m1512();
        Resources resources = context.getResources();
        this.f1243 = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(R.dimen.abc_config_prefDialogWidth));
        this.f1266 = new Handler();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private int m1512() {
        return ViewCompat.getLayoutDirection(this.f1268) == 1 ? 0 : 1;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private int m1513(int i) {
        ListView r1 = this.f1263.get(this.f1263.size() - 1).m1530();
        int[] iArr = new int[2];
        r1.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.f1265.getWindowVisibleDisplayFrame(rect);
        return this.f1251 == 1 ? (iArr[0] + r1.getWidth()) + i > rect.right ? 0 : 1 : iArr[0] - i < 0 ? 1 : 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private int m1514(MenuBuilder menuBuilder) {
        int size = this.f1263.size();
        for (int i = 0; i < size; i++) {
            if (menuBuilder == this.f1263.get(i).f1276) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private MenuPopupWindow m1515() {
        MenuPopupWindow menuPopupWindow = new MenuPopupWindow(this.f1262, (AttributeSet) null, this.f1244, this.f1245);
        menuPopupWindow.setHoverListener(this.f1246);
        menuPopupWindow.setOnItemClickListener(this);
        menuPopupWindow.setOnDismissListener(this);
        menuPopupWindow.setAnchorView(this.f1268);
        menuPopupWindow.setDropDownGravity(this.f1267);
        menuPopupWindow.setModal(true);
        menuPopupWindow.setInputMethodMode(2);
        return menuPopupWindow;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m1516(MenuBuilder menuBuilder) {
        CascadingMenuInfo cascadingMenuInfo;
        View view;
        int i;
        int i2;
        LayoutInflater from = LayoutInflater.from(this.f1262);
        MenuAdapter menuAdapter = new MenuAdapter(menuBuilder, from, this.f1255);
        if (!isShowing() && this.f1254) {
            menuAdapter.m1543(true);
        } else if (isShowing()) {
            menuAdapter.m1543(MenuPopup.m1577(menuBuilder));
        }
        int r9 = m1578(menuAdapter, (ViewGroup) null, this.f1262, this.f1243);
        MenuPopupWindow r16 = m1515();
        r16.setAdapter(menuAdapter);
        r16.setContentWidth(r9);
        r16.setDropDownGravity(this.f1267);
        if (this.f1263.size() > 0) {
            cascadingMenuInfo = this.f1263.get(this.f1263.size() - 1);
            view = m1518(cascadingMenuInfo, menuBuilder);
        } else {
            cascadingMenuInfo = null;
            view = null;
        }
        if (view != null) {
            r16.setTouchModal(false);
            r16.setEnterTransition((Object) null);
            int r10 = m1513(r9);
            boolean z = r10 == 1;
            this.f1251 = r10;
            if (Build.VERSION.SDK_INT >= 26) {
                r16.setAnchorView(view);
                i = 0;
                i2 = 0;
            } else {
                int[] iArr = new int[2];
                this.f1268.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                i = iArr2[0] - iArr[0];
                i2 = iArr2[1] - iArr[1];
            }
            r16.setHorizontalOffset((this.f1267 & 5) == 5 ? z ? i + r9 : i - view.getWidth() : z ? i + view.getWidth() : i - r9);
            r16.setOverlapAnchor(true);
            r16.setVerticalOffset(i2);
        } else {
            if (this.f1252) {
                r16.setHorizontalOffset(this.f1248);
            }
            if (this.f1253) {
                r16.setVerticalOffset(this.f1250);
            }
            r16.setEpicenterBounds(m1580());
        }
        this.f1263.add(new CascadingMenuInfo(r16, menuBuilder, this.f1251));
        r16.show();
        ListView listView = r16.getListView();
        listView.setOnKeyListener(this);
        if (cascadingMenuInfo == null && this.f1256 && menuBuilder.getHeaderTitle() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(R.layout.abc_popup_menu_header_item_layout, listView, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(menuBuilder.getHeaderTitle());
            listView.addHeaderView(frameLayout, (Object) null, false);
            r16.show();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private MenuItem m1517(MenuBuilder menuBuilder, MenuBuilder menuBuilder2) {
        int size = menuBuilder.size();
        for (int i = 0; i < size; i++) {
            MenuItem item = menuBuilder.getItem(i);
            if (item.hasSubMenu() && menuBuilder2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private View m1518(CascadingMenuInfo cascadingMenuInfo, MenuBuilder menuBuilder) {
        int i;
        MenuAdapter menuAdapter;
        int firstVisiblePosition;
        MenuItem r7 = m1517(cascadingMenuInfo.f1276, menuBuilder);
        if (r7 == null) {
            return null;
        }
        ListView r5 = cascadingMenuInfo.m1530();
        ListAdapter adapter = r5.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i = headerViewListAdapter.getHeadersCount();
            menuAdapter = (MenuAdapter) headerViewListAdapter.getWrappedAdapter();
        } else {
            i = 0;
            menuAdapter = (MenuAdapter) adapter;
        }
        int i2 = -1;
        int i3 = 0;
        int count = menuAdapter.getCount();
        while (true) {
            if (i3 >= count) {
                break;
            } else if (r7 == menuAdapter.getItem(i3)) {
                i2 = i3;
                break;
            } else {
                i3++;
            }
        }
        if (i2 == -1 || (firstVisiblePosition = (i2 + i) - r5.getFirstVisiblePosition()) < 0 || firstVisiblePosition >= r5.getChildCount()) {
            return null;
        }
        return r5.getChildAt(firstVisiblePosition);
    }

    public void dismiss() {
        int size = this.f1263.size();
        if (size > 0) {
            CascadingMenuInfo[] cascadingMenuInfoArr = (CascadingMenuInfo[]) this.f1263.toArray(new CascadingMenuInfo[size]);
            for (int i = size - 1; i >= 0; i--) {
                CascadingMenuInfo cascadingMenuInfo = cascadingMenuInfoArr[i];
                if (cascadingMenuInfo.f1278.isShowing()) {
                    cascadingMenuInfo.f1278.dismiss();
                }
            }
        }
    }

    public boolean flagActionItems() {
        return false;
    }

    public ListView getListView() {
        if (this.f1263.isEmpty()) {
            return null;
        }
        return this.f1263.get(this.f1263.size() - 1).m1530();
    }

    public boolean isShowing() {
        return this.f1263.size() > 0 && this.f1263.get(0).f1278.isShowing();
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        int r3 = m1514(menuBuilder);
        if (r3 >= 0) {
            int i = r3 + 1;
            if (i < this.f1263.size()) {
                this.f1263.get(i).f1276.close(false);
            }
            CascadingMenuInfo remove = this.f1263.remove(r3);
            remove.f1276.removeMenuPresenter(this);
            if (this.f1264) {
                remove.f1278.setExitTransition((Object) null);
                remove.f1278.setAnimationStyle(0);
            }
            remove.f1278.dismiss();
            int size = this.f1263.size();
            if (size > 0) {
                this.f1251 = this.f1263.get(size - 1).f1277;
            } else {
                this.f1251 = m1512();
            }
            if (size == 0) {
                dismiss();
                if (this.f1257 != null) {
                    this.f1257.m1606(menuBuilder, true);
                }
                if (this.f1260 != null) {
                    if (this.f1260.isAlive()) {
                        this.f1260.removeGlobalOnLayoutListener(this.f1259);
                    }
                    this.f1260 = null;
                }
                this.f1265.removeOnAttachStateChangeListener(this.f1249);
                this.f1261.onDismiss();
            } else if (z) {
                this.f1263.get(0).f1276.close(false);
            }
        }
    }

    public void onDismiss() {
        CascadingMenuInfo cascadingMenuInfo = null;
        int i = 0;
        int size = this.f1263.size();
        while (true) {
            if (i >= size) {
                break;
            }
            CascadingMenuInfo cascadingMenuInfo2 = this.f1263.get(i);
            if (!cascadingMenuInfo2.f1278.isShowing()) {
                cascadingMenuInfo = cascadingMenuInfo2;
                break;
            }
            i++;
        }
        if (cascadingMenuInfo != null) {
            cascadingMenuInfo.f1276.close(false);
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
    }

    public Parcelable onSaveInstanceState() {
        return null;
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        for (CascadingMenuInfo next : this.f1263) {
            if (subMenuBuilder == next.f1276) {
                next.m1530().requestFocus();
                return true;
            }
        }
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        m1525((MenuBuilder) subMenuBuilder);
        if (this.f1257 == null) {
            return true;
        }
        this.f1257.m1607(subMenuBuilder);
        return true;
    }

    public void setCallback(MenuPresenter.Callback callback) {
        this.f1257 = callback;
    }

    public void show() {
        if (!isShowing()) {
            for (MenuBuilder r1 : this.f1258) {
                m1516(r1);
            }
            this.f1258.clear();
            this.f1265 = this.f1268;
            if (this.f1265 != null) {
                boolean z = this.f1260 == null;
                this.f1260 = this.f1265.getViewTreeObserver();
                if (z) {
                    this.f1260.addOnGlobalLayoutListener(this.f1259);
                }
                this.f1265.addOnAttachStateChangeListener(this.f1249);
            }
        }
    }

    public void updateMenuView(boolean z) {
        for (CascadingMenuInfo r0 : this.f1263) {
            m1579(r0.m1530().getAdapter()).notifyDataSetChanged();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1521(int i) {
        this.f1252 = true;
        this.f1248 = i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1522(boolean z) {
        this.f1256 = z;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1523(int i) {
        this.f1253 = true;
        this.f1250 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1524(int i) {
        if (this.f1247 != i) {
            this.f1247 = i;
            this.f1267 = GravityCompat.getAbsoluteGravity(i, ViewCompat.getLayoutDirection(this.f1268));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1525(MenuBuilder menuBuilder) {
        menuBuilder.addMenuPresenter(this, this.f1262);
        if (isShowing()) {
            m1516(menuBuilder);
        } else {
            this.f1258.add(menuBuilder);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1526(View view) {
        if (this.f1268 != view) {
            this.f1268 = view;
            this.f1267 = GravityCompat.getAbsoluteGravity(this.f1247, ViewCompat.getLayoutDirection(this.f1268));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1527(PopupWindow.OnDismissListener onDismissListener) {
        this.f1261 = onDismissListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1528(boolean z) {
        this.f1254 = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1529() {
        return false;
    }
}
