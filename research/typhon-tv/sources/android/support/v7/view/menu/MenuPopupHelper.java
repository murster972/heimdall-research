package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.RestrictTo;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuPresenter;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

@RestrictTo
public class MenuPopupHelper {

    /* renamed from: ʻ  reason: contains not printable characters */
    private View f1364;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f1365;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f1366;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final PopupWindow.OnDismissListener f1367;

    /* renamed from: ˑ  reason: contains not printable characters */
    private MenuPresenter.Callback f1368;

    /* renamed from: ٴ  reason: contains not printable characters */
    private MenuPopup f1369;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private PopupWindow.OnDismissListener f1370;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f1371;

    /* renamed from: 靐  reason: contains not printable characters */
    private final MenuBuilder f1372;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f1373;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f1374;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f1375;

    public MenuPopupHelper(Context context, MenuBuilder menuBuilder, View view, boolean z, int i) {
        this(context, menuBuilder, view, z, i, 0);
    }

    public MenuPopupHelper(Context context, MenuBuilder menuBuilder, View view, boolean z, int i, int i2) {
        this.f1365 = GravityCompat.START;
        this.f1367 = new PopupWindow.OnDismissListener() {
            public void onDismiss() {
                MenuPopupHelper.this.m1593();
            }
        };
        this.f1375 = context;
        this.f1372 = menuBuilder;
        this.f1364 = view;
        this.f1374 = z;
        this.f1373 = i;
        this.f1371 = i2;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private MenuPopup m1591() {
        Display defaultDisplay = ((WindowManager) this.f1375.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else {
            defaultDisplay.getSize(point);
        }
        MenuPopup cascadingMenuPopup = Math.min(point.x, point.y) >= this.f1375.getResources().getDimensionPixelSize(R.dimen.abc_cascading_menus_min_smallest_width) ? new CascadingMenuPopup(this.f1375, this.f1364, this.f1373, this.f1371, this.f1374) : new StandardMenuPopup(this.f1375, this.f1372, this.f1364, this.f1373, this.f1371, this.f1374);
        cascadingMenuPopup.m1586(this.f1372);
        cascadingMenuPopup.m1588(this.f1367);
        cascadingMenuPopup.m1587(this.f1364);
        cascadingMenuPopup.setCallback(this.f1368);
        cascadingMenuPopup.m1589(this.f1366);
        cascadingMenuPopup.m1584(this.f1365);
        return cascadingMenuPopup;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1592(int i, int i2, boolean z, boolean z2) {
        MenuPopup r4 = m1598();
        r4.m1582(z2);
        if (z) {
            if ((GravityCompat.getAbsoluteGravity(this.f1365, ViewCompat.getLayoutDirection(this.f1364)) & 7) == 5) {
                i += this.f1364.getWidth();
            }
            r4.m1581(i);
            r4.m1583(i2);
            int i3 = (int) ((48.0f * this.f1375.getResources().getDisplayMetrics().density) / 2.0f);
            r4.m1585(new Rect(i - i3, i2 - i3, i + i3, i2 + i3));
        }
        r4.show();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m1593() {
        this.f1369 = null;
        if (this.f1370 != null) {
            this.f1370.onDismiss();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1594() {
        return this.f1369 != null && this.f1369.isShowing();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m1595() {
        if (m1594()) {
            this.f1369.dismiss();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1596() {
        if (!m1597()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m1597() {
        if (m1594()) {
            return true;
        }
        if (this.f1364 == null) {
            return false;
        }
        m1592(0, 0, false, false);
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public MenuPopup m1598() {
        if (this.f1369 == null) {
            this.f1369 = m1591();
        }
        return this.f1369;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m1599() {
        return this.f1365;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1600(int i) {
        this.f1365 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1601(MenuPresenter.Callback callback) {
        this.f1368 = callback;
        if (this.f1369 != null) {
            this.f1369.setCallback(callback);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1602(View view) {
        this.f1364 = view;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1603(PopupWindow.OnDismissListener onDismissListener) {
        this.f1370 = onDismissListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1604(boolean z) {
        this.f1366 = z;
        if (this.f1369 != null) {
            this.f1369.m1589(z);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1605(int i, int i2) {
        if (m1594()) {
            return true;
        }
        if (this.f1364 == null) {
            return false;
        }
        m1592(i, i2, true, true);
        return true;
    }
}
