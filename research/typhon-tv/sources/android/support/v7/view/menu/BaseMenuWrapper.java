package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.internal.view.SupportSubMenu;
import android.support.v4.util.ArrayMap;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.Iterator;
import java.util.Map;

abstract class BaseMenuWrapper<T> extends BaseWrapper<T> {

    /* renamed from: 麤  reason: contains not printable characters */
    private Map<SupportSubMenu, SubMenu> f1239;

    /* renamed from: 齉  reason: contains not printable characters */
    private Map<SupportMenuItem, MenuItem> f1240;

    /* renamed from: 龘  reason: contains not printable characters */
    final Context f1241;

    BaseMenuWrapper(Context context, T t) {
        super(t);
        this.f1241 = context;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m1506(int i) {
        if (this.f1240 != null) {
            Iterator<SupportMenuItem> it2 = this.f1240.keySet().iterator();
            while (it2.hasNext()) {
                if (i == it2.next().getItemId()) {
                    it2.remove();
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final MenuItem m1507(MenuItem menuItem) {
        if (!(menuItem instanceof SupportMenuItem)) {
            return menuItem;
        }
        SupportMenuItem supportMenuItem = (SupportMenuItem) menuItem;
        if (this.f1240 == null) {
            this.f1240 = new ArrayMap();
        }
        MenuItem menuItem2 = this.f1240.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        MenuItem r1 = MenuWrapperFactory.m1609(this.f1241, supportMenuItem);
        this.f1240.put(supportMenuItem, r1);
        return r1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final SubMenu m1508(SubMenu subMenu) {
        if (!(subMenu instanceof SupportSubMenu)) {
            return subMenu;
        }
        SupportSubMenu supportSubMenu = (SupportSubMenu) subMenu;
        if (this.f1239 == null) {
            this.f1239 = new ArrayMap();
        }
        SubMenu subMenu2 = this.f1239.get(supportSubMenu);
        if (subMenu2 != null) {
            return subMenu2;
        }
        SubMenu r1 = MenuWrapperFactory.m1610(this.f1241, supportSubMenu);
        this.f1239.put(supportSubMenu, r1);
        return r1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m1509() {
        if (this.f1240 != null) {
            this.f1240.clear();
        }
        if (this.f1239 != null) {
            this.f1239.clear();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m1510(int i) {
        if (this.f1240 != null) {
            Iterator<SupportMenuItem> it2 = this.f1240.keySet().iterator();
            while (it2.hasNext()) {
                if (i == it2.next().getGroupId()) {
                    it2.remove();
                }
            }
        }
    }
}
