package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.widget.MenuPopupWindow;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

final class StandardMenuPopup extends MenuPopup implements MenuPresenter, View.OnKeyListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean f1377;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f1378;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f1379;

    /* renamed from: ʾ  reason: contains not printable characters */
    private View f1380;

    /* renamed from: ʿ  reason: contains not printable characters */
    private MenuPresenter.Callback f1381;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f1382;

    /* renamed from: ˈ  reason: contains not printable characters */
    private PopupWindow.OnDismissListener f1383;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f1384;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f1385;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f1386 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f1387;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public final ViewTreeObserver.OnGlobalLayoutListener f1388 = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (StandardMenuPopup.this.isShowing() && !StandardMenuPopup.this.f1394.isModal()) {
                View view = StandardMenuPopup.this.f1391;
                if (view == null || !view.isShown()) {
                    StandardMenuPopup.this.dismiss();
                } else {
                    StandardMenuPopup.this.f1394.show();
                }
            }
        }
    };

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final View.OnAttachStateChangeListener f1389 = new View.OnAttachStateChangeListener() {
        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            if (StandardMenuPopup.this.f1395 != null) {
                if (!StandardMenuPopup.this.f1395.isAlive()) {
                    ViewTreeObserver unused = StandardMenuPopup.this.f1395 = view.getViewTreeObserver();
                }
                StandardMenuPopup.this.f1395.removeGlobalOnLayoutListener(StandardMenuPopup.this.f1388);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private final MenuAdapter f1390;

    /* renamed from: 靐  reason: contains not printable characters */
    View f1391;

    /* renamed from: 麤  reason: contains not printable characters */
    private final MenuBuilder f1392;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f1393;

    /* renamed from: 龘  reason: contains not printable characters */
    final MenuPopupWindow f1394;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public ViewTreeObserver f1395;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f1396;

    public StandardMenuPopup(Context context, MenuBuilder menuBuilder, View view, int i, int i2, boolean z) {
        this.f1393 = context;
        this.f1392 = menuBuilder;
        this.f1377 = z;
        this.f1390 = new MenuAdapter(menuBuilder, LayoutInflater.from(context), this.f1377);
        this.f1379 = i;
        this.f1387 = i2;
        Resources resources = context.getResources();
        this.f1378 = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(R.dimen.abc_config_prefDialogWidth));
        this.f1380 = view;
        this.f1394 = new MenuPopupWindow(this.f1393, (AttributeSet) null, this.f1379, this.f1387);
        menuBuilder.addMenuPresenter(this, context);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m1612() {
        if (isShowing()) {
            return true;
        }
        if (this.f1396 || this.f1380 == null) {
            return false;
        }
        this.f1391 = this.f1380;
        this.f1394.setOnDismissListener(this);
        this.f1394.setOnItemClickListener(this);
        this.f1394.setModal(true);
        View view = this.f1391;
        boolean z = this.f1395 == null;
        this.f1395 = view.getViewTreeObserver();
        if (z) {
            this.f1395.addOnGlobalLayoutListener(this.f1388);
        }
        view.addOnAttachStateChangeListener(this.f1389);
        this.f1394.setAnchorView(view);
        this.f1394.setDropDownGravity(this.f1386);
        if (!this.f1384) {
            this.f1385 = m1578(this.f1390, (ViewGroup) null, this.f1393, this.f1378);
            this.f1384 = true;
        }
        this.f1394.setContentWidth(this.f1385);
        this.f1394.setInputMethodMode(2);
        this.f1394.setEpicenterBounds(m1580());
        this.f1394.show();
        ListView listView = this.f1394.getListView();
        listView.setOnKeyListener(this);
        if (this.f1382 && this.f1392.getHeaderTitle() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.f1393).inflate(R.layout.abc_popup_menu_header_item_layout, listView, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.f1392.getHeaderTitle());
            }
            frameLayout.setEnabled(false);
            listView.addHeaderView(frameLayout, (Object) null, false);
        }
        this.f1394.setAdapter(this.f1390);
        this.f1394.show();
        return true;
    }

    public void dismiss() {
        if (isShowing()) {
            this.f1394.dismiss();
        }
    }

    public boolean flagActionItems() {
        return false;
    }

    public ListView getListView() {
        return this.f1394.getListView();
    }

    public boolean isShowing() {
        return !this.f1396 && this.f1394.isShowing();
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        if (menuBuilder == this.f1392) {
            dismiss();
            if (this.f1381 != null) {
                this.f1381.m1606(menuBuilder, z);
            }
        }
    }

    public void onDismiss() {
        this.f1396 = true;
        this.f1392.close();
        if (this.f1395 != null) {
            if (!this.f1395.isAlive()) {
                this.f1395 = this.f1391.getViewTreeObserver();
            }
            this.f1395.removeGlobalOnLayoutListener(this.f1388);
            this.f1395 = null;
        }
        this.f1391.removeOnAttachStateChangeListener(this.f1389);
        if (this.f1383 != null) {
            this.f1383.onDismiss();
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
    }

    public Parcelable onSaveInstanceState() {
        return null;
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        if (subMenuBuilder.hasVisibleItems()) {
            MenuPopupHelper menuPopupHelper = new MenuPopupHelper(this.f1393, subMenuBuilder, this.f1391, this.f1377, this.f1379, this.f1387);
            menuPopupHelper.m1601(this.f1381);
            menuPopupHelper.m1604(MenuPopup.m1577((MenuBuilder) subMenuBuilder));
            menuPopupHelper.m1600(this.f1386);
            menuPopupHelper.m1603(this.f1383);
            this.f1383 = null;
            this.f1392.close(false);
            if (menuPopupHelper.m1605(this.f1394.getHorizontalOffset(), this.f1394.getVerticalOffset())) {
                if (this.f1381 != null) {
                    this.f1381.m1607(subMenuBuilder);
                }
                return true;
            }
        }
        return false;
    }

    public void setCallback(MenuPresenter.Callback callback) {
        this.f1381 = callback;
    }

    public void show() {
        if (!m1612()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    public void updateMenuView(boolean z) {
        this.f1384 = false;
        if (this.f1390 != null) {
            this.f1390.notifyDataSetChanged();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1615(int i) {
        this.f1394.setHorizontalOffset(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1616(boolean z) {
        this.f1382 = z;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1617(int i) {
        this.f1394.setVerticalOffset(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1618(int i) {
        this.f1386 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1619(MenuBuilder menuBuilder) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1620(View view) {
        this.f1380 = view;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1621(PopupWindow.OnDismissListener onDismissListener) {
        this.f1383 = onDismissListener;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1622(boolean z) {
        this.f1390.m1543(z);
    }
}
