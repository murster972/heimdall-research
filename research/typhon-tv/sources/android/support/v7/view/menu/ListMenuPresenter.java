package android.support.v7.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.annotation.RestrictTo;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import java.util.ArrayList;

@RestrictTo
public class ListMenuPresenter implements MenuPresenter, AdapterView.OnItemClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f1297;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f1298;

    /* renamed from: ʽ  reason: contains not printable characters */
    MenuAdapter f1299;

    /* renamed from: ˑ  reason: contains not printable characters */
    private MenuPresenter.Callback f1300;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f1301;

    /* renamed from: 连任  reason: contains not printable characters */
    int f1302;

    /* renamed from: 靐  reason: contains not printable characters */
    LayoutInflater f1303;

    /* renamed from: 麤  reason: contains not printable characters */
    ExpandedMenuView f1304;

    /* renamed from: 齉  reason: contains not printable characters */
    MenuBuilder f1305;

    /* renamed from: 龘  reason: contains not printable characters */
    Context f1306;

    private class MenuAdapter extends BaseAdapter {

        /* renamed from: 靐  reason: contains not printable characters */
        private int f1307 = -1;

        public MenuAdapter() {
            m1539();
        }

        public int getCount() {
            int size = ListMenuPresenter.this.f1305.getNonActionItems().size() - ListMenuPresenter.this.f1302;
            return this.f1307 < 0 ? size : size - 1;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = ListMenuPresenter.this.f1303.inflate(ListMenuPresenter.this.f1298, viewGroup, false);
            }
            ((MenuView.ItemView) view).initialize(getItem(i), 0);
            return view;
        }

        public void notifyDataSetChanged() {
            m1539();
            super.notifyDataSetChanged();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MenuItemImpl getItem(int i) {
            ArrayList<MenuItemImpl> nonActionItems = ListMenuPresenter.this.f1305.getNonActionItems();
            int i2 = i + ListMenuPresenter.this.f1302;
            if (this.f1307 >= 0 && i2 >= this.f1307) {
                i2++;
            }
            return nonActionItems.get(i2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1539() {
            MenuItemImpl expandedItem = ListMenuPresenter.this.f1305.getExpandedItem();
            if (expandedItem != null) {
                ArrayList<MenuItemImpl> nonActionItems = ListMenuPresenter.this.f1305.getNonActionItems();
                int size = nonActionItems.size();
                for (int i = 0; i < size; i++) {
                    if (nonActionItems.get(i) == expandedItem) {
                        this.f1307 = i;
                        return;
                    }
                }
            }
            this.f1307 = -1;
        }
    }

    public ListMenuPresenter(int i, int i2) {
        this.f1298 = i;
        this.f1297 = i2;
    }

    public ListMenuPresenter(Context context, int i) {
        this(i, 0);
        this.f1306 = context;
        this.f1303 = LayoutInflater.from(this.f1306);
    }

    public boolean collapseItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean expandItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean flagActionItems() {
        return false;
    }

    public int getId() {
        return this.f1301;
    }

    public void initForMenu(Context context, MenuBuilder menuBuilder) {
        if (this.f1297 != 0) {
            this.f1306 = new ContextThemeWrapper(context, this.f1297);
            this.f1303 = LayoutInflater.from(this.f1306);
        } else if (this.f1306 != null) {
            this.f1306 = context;
            if (this.f1303 == null) {
                this.f1303 = LayoutInflater.from(this.f1306);
            }
        }
        this.f1305 = menuBuilder;
        if (this.f1299 != null) {
            this.f1299.notifyDataSetChanged();
        }
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        if (this.f1300 != null) {
            this.f1300.m1606(menuBuilder, z);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.f1305.performItemAction(this.f1299.getItem(i), this, 0);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        m1534((Bundle) parcelable);
    }

    public Parcelable onSaveInstanceState() {
        if (this.f1304 == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        m1537(bundle);
        return bundle;
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        new MenuDialogHelper(subMenuBuilder).m1545((IBinder) null);
        if (this.f1300 != null) {
            this.f1300.m1607(subMenuBuilder);
        }
        return true;
    }

    public void setCallback(MenuPresenter.Callback callback) {
        this.f1300 = callback;
    }

    public void updateMenuView(boolean z) {
        if (this.f1299 != null) {
            this.f1299.notifyDataSetChanged();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1534(Bundle bundle) {
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.f1304.restoreHierarchyState(sparseParcelableArray);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MenuView m1535(ViewGroup viewGroup) {
        if (this.f1304 == null) {
            this.f1304 = (ExpandedMenuView) this.f1303.inflate(R.layout.abc_expanded_menu_layout, viewGroup, false);
            if (this.f1299 == null) {
                this.f1299 = new MenuAdapter();
            }
            this.f1304.setAdapter(this.f1299);
            this.f1304.setOnItemClickListener(this);
        }
        return this.f1304;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ListAdapter m1536() {
        if (this.f1299 == null) {
            this.f1299 = new MenuAdapter();
        }
        return this.f1299;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1537(Bundle bundle) {
        SparseArray sparseArray = new SparseArray();
        if (this.f1304 != null) {
            this.f1304.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }
}
