package android.support.v7.view.menu;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.internal.view.SupportMenuItem;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

@RestrictTo
public class ActionMenuItem implements SupportMenuItem {

    /* renamed from: ʻ  reason: contains not printable characters */
    private CharSequence f1195;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Intent f1196;

    /* renamed from: ʽ  reason: contains not printable characters */
    private char f1197;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f1198 = 0;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Context f1199;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f1200 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Drawable f1201;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f1202 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    private CharSequence f1203;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ColorStateList f1204 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private PorterDuff.Mode f1205 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f1206 = 16;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f1207 = 4096;

    /* renamed from: ٴ  reason: contains not printable characters */
    private char f1208;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f1209 = 4096;

    /* renamed from: 连任  reason: contains not printable characters */
    private CharSequence f1210;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f1211;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f1212;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f1213;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f1214;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private MenuItem.OnMenuItemClickListener f1215;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private CharSequence f1216;

    public ActionMenuItem(Context context, int i, int i2, int i3, int i4, CharSequence charSequence) {
        this.f1199 = context;
        this.f1214 = i2;
        this.f1211 = i;
        this.f1213 = i3;
        this.f1212 = i4;
        this.f1210 = charSequence;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1489() {
        if (this.f1201 == null) {
            return;
        }
        if (this.f1200 || this.f1202) {
            this.f1201 = DrawableCompat.wrap(this.f1201);
            this.f1201 = this.f1201.mutate();
            if (this.f1200) {
                DrawableCompat.setTintList(this.f1201, this.f1204);
            }
            if (this.f1202) {
                DrawableCompat.setTintMode(this.f1201, this.f1205);
            }
        }
    }

    public boolean collapseActionView() {
        return false;
    }

    public boolean expandActionView() {
        return false;
    }

    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException();
    }

    public View getActionView() {
        return null;
    }

    public int getAlphabeticModifiers() {
        return this.f1209;
    }

    public char getAlphabeticShortcut() {
        return this.f1208;
    }

    public CharSequence getContentDescription() {
        return this.f1216;
    }

    public int getGroupId() {
        return this.f1211;
    }

    public Drawable getIcon() {
        return this.f1201;
    }

    public ColorStateList getIconTintList() {
        return this.f1204;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.f1205;
    }

    public Intent getIntent() {
        return this.f1196;
    }

    public int getItemId() {
        return this.f1214;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    public int getNumericModifiers() {
        return this.f1207;
    }

    public char getNumericShortcut() {
        return this.f1197;
    }

    public int getOrder() {
        return this.f1212;
    }

    public SubMenu getSubMenu() {
        return null;
    }

    public android.support.v4.view.ActionProvider getSupportActionProvider() {
        return null;
    }

    public CharSequence getTitle() {
        return this.f1210;
    }

    public CharSequence getTitleCondensed() {
        return this.f1195 != null ? this.f1195 : this.f1210;
    }

    public CharSequence getTooltipText() {
        return this.f1203;
    }

    public boolean hasSubMenu() {
        return false;
    }

    public boolean isActionViewExpanded() {
        return false;
    }

    public boolean isCheckable() {
        return (this.f1206 & 1) != 0;
    }

    public boolean isChecked() {
        return (this.f1206 & 2) != 0;
    }

    public boolean isEnabled() {
        return (this.f1206 & 16) != 0;
    }

    public boolean isVisible() {
        return (this.f1206 & 8) == 0;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setAlphabeticShortcut(char c) {
        this.f1208 = Character.toLowerCase(c);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c, int i) {
        this.f1208 = Character.toLowerCase(c);
        this.f1209 = KeyEvent.normalizeMetaState(i);
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        this.f1206 = (z ? 1 : 0) | (this.f1206 & -2);
        return this;
    }

    public MenuItem setChecked(boolean z) {
        this.f1206 = (z ? 2 : 0) | (this.f1206 & -3);
        return this;
    }

    public SupportMenuItem setContentDescription(CharSequence charSequence) {
        this.f1216 = charSequence;
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        this.f1206 = (z ? 16 : 0) | (this.f1206 & -17);
        return this;
    }

    public MenuItem setIcon(int i) {
        this.f1198 = i;
        this.f1201 = ContextCompat.getDrawable(this.f1199, i);
        m1489();
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f1201 = drawable;
        this.f1198 = 0;
        m1489();
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.f1204 = colorStateList;
        this.f1200 = true;
        m1489();
        return this;
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.f1205 = mode;
        this.f1202 = true;
        m1489();
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.f1196 = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c) {
        this.f1197 = c;
        return this;
    }

    public MenuItem setNumericShortcut(char c, int i) {
        this.f1197 = c;
        this.f1207 = KeyEvent.normalizeMetaState(i);
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.f1215 = onMenuItemClickListener;
        return this;
    }

    public MenuItem setShortcut(char c, char c2) {
        this.f1197 = c;
        this.f1208 = Character.toLowerCase(c2);
        return this;
    }

    public MenuItem setShortcut(char c, char c2, int i, int i2) {
        this.f1197 = c;
        this.f1207 = KeyEvent.normalizeMetaState(i);
        this.f1208 = Character.toLowerCase(c2);
        this.f1209 = KeyEvent.normalizeMetaState(i2);
        return this;
    }

    public void setShowAsAction(int i) {
    }

    public SupportMenuItem setSupportActionProvider(android.support.v4.view.ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setTitle(int i) {
        this.f1210 = this.f1199.getResources().getString(i);
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f1210 = charSequence;
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f1195 = charSequence;
        return this;
    }

    public SupportMenuItem setTooltipText(CharSequence charSequence) {
        this.f1203 = charSequence;
        return this;
    }

    public MenuItem setVisible(boolean z) {
        this.f1206 = (z ? 0 : 8) | (this.f1206 & 8);
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public SupportMenuItem setShowAsActionFlags(int i) {
        setShowAsAction(i);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SupportMenuItem setActionView(int i) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SupportMenuItem setActionView(View view) {
        throw new UnsupportedOperationException();
    }
}
