package android.support.v7.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RestrictTo;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.view.ActionProvider;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.view.menu.MenuView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;
import android.widget.LinearLayout;

@RestrictTo
public final class MenuItemImpl implements SupportMenuItem {

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private static String f1319;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private static String f1320;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private static String f1321;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private static String f1322;

    /* renamed from: ʻ  reason: contains not printable characters */
    private CharSequence f1323;

    /* renamed from: ʼ  reason: contains not printable characters */
    private CharSequence f1324;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Intent f1325;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Drawable f1326;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f1327 = 0;

    /* renamed from: ˆ  reason: contains not printable characters */
    private ColorStateList f1328 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f1329 = 4096;

    /* renamed from: ˉ  reason: contains not printable characters */
    private PorterDuff.Mode f1330 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private MenuItem.OnMenuItemClickListener f1331;

    /* renamed from: ˋ  reason: contains not printable characters */
    private CharSequence f1332;

    /* renamed from: ˎ  reason: contains not printable characters */
    private CharSequence f1333;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f1334 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private char f1335;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f1336 = false;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f1337 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f1338 = 4096;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private char f1339;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f1340 = 16;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f1341 = 0;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private View f1342;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private ActionProvider f1343;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private MenuItem.OnActionExpandListener f1344;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f1345;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f1346;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f1347;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f1348;

    /* renamed from: 龘  reason: contains not printable characters */
    MenuBuilder f1349;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f1350 = false;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private SubMenuBuilder f1351;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private Runnable f1352;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private ContextMenu.ContextMenuInfo f1353;

    MenuItemImpl(MenuBuilder menuBuilder, int i, int i2, int i3, int i4, CharSequence charSequence, int i5) {
        this.f1349 = menuBuilder;
        this.f1346 = i2;
        this.f1348 = i;
        this.f1347 = i3;
        this.f1345 = i4;
        this.f1323 = charSequence;
        this.f1341 = i5;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Drawable m1548(Drawable drawable) {
        if (drawable != null && this.f1337 && (this.f1334 || this.f1336)) {
            drawable = DrawableCompat.wrap(drawable).mutate();
            if (this.f1334) {
                DrawableCompat.setTintList(drawable, this.f1328);
            }
            if (this.f1336) {
                DrawableCompat.setTintMode(drawable, this.f1330);
            }
            this.f1337 = false;
        }
        return drawable;
    }

    public boolean collapseActionView() {
        if ((this.f1341 & 8) == 0) {
            return false;
        }
        if (this.f1342 == null) {
            return true;
        }
        if (this.f1344 == null || this.f1344.onMenuItemActionCollapse(this)) {
            return this.f1349.collapseItemActionView(this);
        }
        return false;
    }

    public boolean expandActionView() {
        if (!m1552()) {
            return false;
        }
        if (this.f1344 == null || this.f1344.onMenuItemActionExpand(this)) {
            return this.f1349.expandItemActionView(this);
        }
        return false;
    }

    public android.view.ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    public View getActionView() {
        if (this.f1342 != null) {
            return this.f1342;
        }
        if (this.f1343 == null) {
            return null;
        }
        this.f1342 = this.f1343.onCreateActionView(this);
        return this.f1342;
    }

    public int getAlphabeticModifiers() {
        return this.f1329;
    }

    public char getAlphabeticShortcut() {
        return this.f1339;
    }

    public CharSequence getContentDescription() {
        return this.f1332;
    }

    public int getGroupId() {
        return this.f1348;
    }

    public Drawable getIcon() {
        if (this.f1326 != null) {
            return m1548(this.f1326);
        }
        if (this.f1327 == 0) {
            return null;
        }
        Drawable r0 = AppCompatResources.m941(this.f1349.getContext(), this.f1327);
        this.f1327 = 0;
        this.f1326 = r0;
        return m1548(r0);
    }

    public ColorStateList getIconTintList() {
        return this.f1328;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.f1330;
    }

    public Intent getIntent() {
        return this.f1325;
    }

    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.f1346;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.f1353;
    }

    public int getNumericModifiers() {
        return this.f1338;
    }

    public char getNumericShortcut() {
        return this.f1335;
    }

    public int getOrder() {
        return this.f1347;
    }

    public SubMenu getSubMenu() {
        return this.f1351;
    }

    public ActionProvider getSupportActionProvider() {
        return this.f1343;
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.f1323;
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f1324 != null ? this.f1324 : this.f1323;
        return (Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) ? charSequence : charSequence.toString();
    }

    public CharSequence getTooltipText() {
        return this.f1333;
    }

    public boolean hasSubMenu() {
        return this.f1351 != null;
    }

    public boolean isActionViewExpanded() {
        return this.f1350;
    }

    public boolean isCheckable() {
        return (this.f1340 & 1) == 1;
    }

    public boolean isChecked() {
        return (this.f1340 & 2) == 2;
    }

    public boolean isEnabled() {
        return (this.f1340 & 16) != 0;
    }

    public boolean isVisible() {
        return (this.f1343 == null || !this.f1343.overridesItemVisibility()) ? (this.f1340 & 8) == 0 : (this.f1340 & 8) == 0 && this.f1343.isVisible();
    }

    public MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    public MenuItem setAlphabeticShortcut(char c) {
        if (this.f1339 != c) {
            this.f1339 = Character.toLowerCase(c);
            this.f1349.onItemsChanged(false);
        }
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c, int i) {
        if (!(this.f1339 == c && this.f1329 == i)) {
            this.f1339 = Character.toLowerCase(c);
            this.f1329 = KeyEvent.normalizeMetaState(i);
            this.f1349.onItemsChanged(false);
        }
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        int i = this.f1340;
        this.f1340 = (z ? 1 : 0) | (this.f1340 & -2);
        if (i != this.f1340) {
            this.f1349.onItemsChanged(false);
        }
        return this;
    }

    public MenuItem setChecked(boolean z) {
        if ((this.f1340 & 4) != 0) {
            this.f1349.setExclusiveItemChecked(this);
        } else {
            m1561(z);
        }
        return this;
    }

    public SupportMenuItem setContentDescription(CharSequence charSequence) {
        this.f1332 = charSequence;
        this.f1349.onItemsChanged(false);
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        if (z) {
            this.f1340 |= 16;
        } else {
            this.f1340 &= -17;
        }
        this.f1349.onItemsChanged(false);
        return this;
    }

    public MenuItem setIcon(int i) {
        this.f1326 = null;
        this.f1327 = i;
        this.f1337 = true;
        this.f1349.onItemsChanged(false);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f1327 = 0;
        this.f1326 = drawable;
        this.f1337 = true;
        this.f1349.onItemsChanged(false);
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.f1328 = colorStateList;
        this.f1334 = true;
        this.f1337 = true;
        this.f1349.onItemsChanged(false);
        return this;
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.f1330 = mode;
        this.f1336 = true;
        this.f1337 = true;
        this.f1349.onItemsChanged(false);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.f1325 = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c) {
        if (this.f1335 != c) {
            this.f1335 = c;
            this.f1349.onItemsChanged(false);
        }
        return this;
    }

    public MenuItem setNumericShortcut(char c, int i) {
        if (!(this.f1335 == c && this.f1338 == i)) {
            this.f1335 = c;
            this.f1338 = KeyEvent.normalizeMetaState(i);
            this.f1349.onItemsChanged(false);
        }
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        this.f1344 = onActionExpandListener;
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.f1331 = onMenuItemClickListener;
        return this;
    }

    public MenuItem setShortcut(char c, char c2) {
        this.f1335 = c;
        this.f1339 = Character.toLowerCase(c2);
        this.f1349.onItemsChanged(false);
        return this;
    }

    public MenuItem setShortcut(char c, char c2, int i, int i2) {
        this.f1335 = c;
        this.f1338 = KeyEvent.normalizeMetaState(i);
        this.f1339 = Character.toLowerCase(c2);
        this.f1329 = KeyEvent.normalizeMetaState(i2);
        this.f1349.onItemsChanged(false);
        return this;
    }

    public void setShowAsAction(int i) {
        switch (i & 3) {
            case 0:
            case 1:
            case 2:
                this.f1341 = i;
                this.f1349.onItemActionRequestChanged(this);
                return;
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
    }

    public SupportMenuItem setSupportActionProvider(ActionProvider actionProvider) {
        if (this.f1343 != null) {
            this.f1343.reset();
        }
        this.f1342 = null;
        this.f1343 = actionProvider;
        this.f1349.onItemsChanged(true);
        if (this.f1343 != null) {
            this.f1343.setVisibilityListener(new ActionProvider.VisibilityListener() {
                public void onActionProviderVisibilityChanged(boolean z) {
                    MenuItemImpl.this.f1349.onItemVisibleChanged(MenuItemImpl.this);
                }
            });
        }
        return this;
    }

    public MenuItem setTitle(int i) {
        return setTitle((CharSequence) this.f1349.getContext().getString(i));
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f1323 = charSequence;
        this.f1349.onItemsChanged(false);
        if (this.f1351 != null) {
            this.f1351.setHeaderTitle(charSequence);
        }
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f1324 = charSequence;
        if (charSequence == null) {
            CharSequence charSequence2 = this.f1323;
        }
        this.f1349.onItemsChanged(false);
        return this;
    }

    public SupportMenuItem setTooltipText(CharSequence charSequence) {
        this.f1333 = charSequence;
        this.f1349.onItemsChanged(false);
        return this;
    }

    public MenuItem setVisible(boolean z) {
        if (m1565(z)) {
            this.f1349.onItemVisibleChanged(this);
        }
        return this;
    }

    public String toString() {
        if (this.f1323 != null) {
            return this.f1323.toString();
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1549() {
        return (this.f1340 & 4) != 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1550() {
        this.f1349.onItemActionRequestChanged(this);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1551() {
        return this.f1349.getOptionalIconsVisible();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m1552() {
        if ((this.f1341 & 8) == 0) {
            return false;
        }
        if (this.f1342 == null && this.f1343 != null) {
            this.f1342 = this.f1343.onCreateActionView(this);
        }
        return this.f1342 != null;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m1553() {
        return (this.f1341 & 4) == 4;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m1554() {
        return (this.f1340 & 32) == 32;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m1555() {
        return (this.f1341 & 1) == 1;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m1556() {
        return (this.f1341 & 2) == 2;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m1557(boolean z) {
        this.f1350 = z;
        this.f1349.onItemsChanged(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m1558() {
        return this.f1349.isShortcutsVisible() && m1564() != 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m1559() {
        return this.f1345;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public SupportMenuItem setShowAsActionFlags(int i) {
        setShowAsAction(i);
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1561(boolean z) {
        int i = this.f1340;
        this.f1340 = (z ? 2 : 0) | (this.f1340 & -3);
        if (i != this.f1340) {
            this.f1349.onItemsChanged(false);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m1562() {
        char r1 = m1564();
        if (r1 == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(f1321);
        switch (r1) {
            case 8:
                sb.append(f1319);
                break;
            case 10:
                sb.append(f1322);
                break;
            case ' ':
                sb.append(f1320);
                break;
            default:
                sb.append(r1);
                break;
        }
        return sb.toString();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m1563(boolean z) {
        if (z) {
            this.f1340 |= 32;
        } else {
            this.f1340 &= -33;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public char m1564() {
        return this.f1349.isQwertyMode() ? this.f1339 : this.f1335;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m1565(boolean z) {
        int i = this.f1340;
        this.f1340 = (z ? 0 : 8) | (this.f1340 & -9);
        return i != this.f1340;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SupportMenuItem setActionView(int i) {
        Context context = this.f1349.getContext();
        setActionView(LayoutInflater.from(context).inflate(i, new LinearLayout(context), false));
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SupportMenuItem setActionView(View view) {
        this.f1342 = view;
        this.f1343 = null;
        if (view != null && view.getId() == -1 && this.f1346 > 0) {
            view.setId(this.f1346);
        }
        this.f1349.onItemActionRequestChanged(this);
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public CharSequence m1568(MenuView.ItemView itemView) {
        return (itemView == null || !itemView.prefersCondensedTitle()) ? getTitle() : getTitleCondensed();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1569(SubMenuBuilder subMenuBuilder) {
        this.f1351 = subMenuBuilder;
        subMenuBuilder.setHeaderTitle(getTitle());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1570(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.f1353 = contextMenuInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1571(boolean z) {
        this.f1340 = (z ? 4 : 0) | (this.f1340 & -5);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1572() {
        if ((this.f1331 != null && this.f1331.onMenuItemClick(this)) || this.f1349.dispatchMenuItemSelected(this.f1349, this)) {
            return true;
        }
        if (this.f1352 != null) {
            this.f1352.run();
            return true;
        }
        if (this.f1325 != null) {
            try {
                this.f1349.getContext().startActivity(this.f1325);
                return true;
            } catch (ActivityNotFoundException e) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e);
            }
        }
        return this.f1343 != null && this.f1343.onPerformDefaultAction();
    }
}
