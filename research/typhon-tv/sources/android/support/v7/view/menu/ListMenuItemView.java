package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.TintTypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

@RestrictTo
public class ListMenuItemView extends LinearLayout implements MenuView.ItemView {

    /* renamed from: ʻ  reason: contains not printable characters */
    private TextView f1282;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ImageView f1283;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Drawable f1284;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f1285;

    /* renamed from: ʿ  reason: contains not printable characters */
    private LayoutInflater f1286;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Drawable f1287;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f1288;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Context f1289;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f1290;

    /* renamed from: 连任  reason: contains not printable characters */
    private CheckBox f1291;

    /* renamed from: 靐  reason: contains not printable characters */
    private ImageView f1292;

    /* renamed from: 麤  reason: contains not printable characters */
    private TextView f1293;

    /* renamed from: 齉  reason: contains not printable characters */
    private RadioButton f1294;

    /* renamed from: 龘  reason: contains not printable characters */
    private MenuItemImpl f1295;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f1296;

    public ListMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.listMenuViewStyle);
    }

    public ListMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(getContext(), attributeSet, R.styleable.MenuView, i, 0);
        this.f1284 = obtainStyledAttributes.getDrawable(R.styleable.MenuView_android_itemBackground);
        this.f1288 = obtainStyledAttributes.getResourceId(R.styleable.MenuView_android_itemTextAppearance, -1);
        this.f1290 = obtainStyledAttributes.getBoolean(R.styleable.MenuView_preserveIconSpacing, false);
        this.f1289 = context;
        this.f1287 = obtainStyledAttributes.getDrawable(R.styleable.MenuView_subMenuArrow);
        obtainStyledAttributes.recycle();
    }

    private LayoutInflater getInflater() {
        if (this.f1286 == null) {
            this.f1286 = LayoutInflater.from(getContext());
        }
        return this.f1286;
    }

    private void setSubMenuArrowVisible(boolean z) {
        if (this.f1283 != null) {
            this.f1283.setVisibility(z ? 0 : 8);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m1531() {
        this.f1294 = (RadioButton) getInflater().inflate(R.layout.abc_list_menu_item_radio, this, false);
        addView(this.f1294);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m1532() {
        this.f1291 = (CheckBox) getInflater().inflate(R.layout.abc_list_menu_item_checkbox, this, false);
        addView(this.f1291);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m1533() {
        this.f1292 = (ImageView) getInflater().inflate(R.layout.abc_list_menu_item_icon, this, false);
        addView(this.f1292, 0);
    }

    public MenuItemImpl getItemData() {
        return this.f1295;
    }

    public void initialize(MenuItemImpl menuItemImpl, int i) {
        this.f1295 = menuItemImpl;
        this.f1285 = i;
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
        setTitle(menuItemImpl.m1568((MenuView.ItemView) this));
        setCheckable(menuItemImpl.isCheckable());
        setShortcut(menuItemImpl.m1558(), menuItemImpl.m1564());
        setIcon(menuItemImpl.getIcon());
        setEnabled(menuItemImpl.isEnabled());
        setSubMenuArrowVisible(menuItemImpl.hasSubMenu());
        setContentDescription(menuItemImpl.getContentDescription());
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        ViewCompat.setBackground(this, this.f1284);
        this.f1293 = (TextView) findViewById(R.id.title);
        if (this.f1288 != -1) {
            this.f1293.setTextAppearance(this.f1289, this.f1288);
        }
        this.f1282 = (TextView) findViewById(R.id.shortcut);
        this.f1283 = (ImageView) findViewById(R.id.submenuarrow);
        if (this.f1283 != null) {
            this.f1283.setImageDrawable(this.f1287);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.f1292 != null && this.f1290) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.f1292.getLayoutParams();
            if (layoutParams.height > 0 && layoutParams2.width <= 0) {
                layoutParams2.width = layoutParams.height;
            }
        }
        super.onMeasure(i, i2);
    }

    public boolean prefersCondensedTitle() {
        return false;
    }

    public void setCheckable(boolean z) {
        CompoundButton compoundButton;
        CompoundButton compoundButton2;
        if (z || this.f1294 != null || this.f1291 != null) {
            if (this.f1295.m1549()) {
                if (this.f1294 == null) {
                    m1531();
                }
                compoundButton = this.f1294;
                compoundButton2 = this.f1291;
            } else {
                if (this.f1291 == null) {
                    m1532();
                }
                compoundButton = this.f1291;
                compoundButton2 = this.f1294;
            }
            if (z) {
                compoundButton.setChecked(this.f1295.isChecked());
                int i = z ? 0 : 8;
                if (compoundButton.getVisibility() != i) {
                    compoundButton.setVisibility(i);
                }
                if (compoundButton2 != null && compoundButton2.getVisibility() != 8) {
                    compoundButton2.setVisibility(8);
                    return;
                }
                return;
            }
            if (this.f1291 != null) {
                this.f1291.setVisibility(8);
            }
            if (this.f1294 != null) {
                this.f1294.setVisibility(8);
            }
        }
    }

    public void setChecked(boolean z) {
        CompoundButton compoundButton;
        if (this.f1295.m1549()) {
            if (this.f1294 == null) {
                m1531();
            }
            compoundButton = this.f1294;
        } else {
            if (this.f1291 == null) {
                m1532();
            }
            compoundButton = this.f1291;
        }
        compoundButton.setChecked(z);
    }

    public void setForceShowIcon(boolean z) {
        this.f1296 = z;
        this.f1290 = z;
    }

    public void setIcon(Drawable drawable) {
        boolean z = this.f1295.m1551() || this.f1296;
        if (!z && !this.f1290) {
            return;
        }
        if (this.f1292 != null || drawable != null || this.f1290) {
            if (this.f1292 == null) {
                m1533();
            }
            if (drawable != null || this.f1290) {
                ImageView imageView = this.f1292;
                if (!z) {
                    drawable = null;
                }
                imageView.setImageDrawable(drawable);
                if (this.f1292.getVisibility() != 0) {
                    this.f1292.setVisibility(0);
                    return;
                }
                return;
            }
            this.f1292.setVisibility(8);
        }
    }

    public void setShortcut(boolean z, char c) {
        int i = (!z || !this.f1295.m1558()) ? 8 : 0;
        if (i == 0) {
            this.f1282.setText(this.f1295.m1562());
        }
        if (this.f1282.getVisibility() != i) {
            this.f1282.setVisibility(i);
        }
    }

    public void setTitle(CharSequence charSequence) {
        if (charSequence != null) {
            this.f1293.setText(charSequence);
            if (this.f1293.getVisibility() != 0) {
                this.f1293.setVisibility(0);
            }
        } else if (this.f1293.getVisibility() != 8) {
            this.f1293.setVisibility(8);
        }
    }
}
