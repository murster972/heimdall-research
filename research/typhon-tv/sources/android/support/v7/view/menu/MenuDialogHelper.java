package android.support.v7.view.menu;

import android.content.DialogInterface;
import android.os.IBinder;
import android.support.v4.view.PointerIconCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuPresenter;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

class MenuDialogHelper implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, DialogInterface.OnKeyListener, MenuPresenter.Callback {

    /* renamed from: 靐  reason: contains not printable characters */
    private MenuBuilder f1315;

    /* renamed from: 麤  reason: contains not printable characters */
    private MenuPresenter.Callback f1316;

    /* renamed from: 齉  reason: contains not printable characters */
    private AlertDialog f1317;

    /* renamed from: 龘  reason: contains not printable characters */
    ListMenuPresenter f1318;

    public MenuDialogHelper(MenuBuilder menuBuilder) {
        this.f1315 = menuBuilder;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f1315.performItemAction((MenuItemImpl) this.f1318.m1536().getItem(i), 0);
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.f1318.onCloseMenu(this.f1315, true);
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.f1317.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.f1317.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.f1315.close(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.f1315.performShortcut(i, keyEvent, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1544() {
        if (this.f1317 != null) {
            this.f1317.dismiss();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1545(IBinder iBinder) {
        MenuBuilder menuBuilder = this.f1315;
        AlertDialog.Builder builder = new AlertDialog.Builder(menuBuilder.getContext());
        this.f1318 = new ListMenuPresenter(builder.m529(), R.layout.abc_list_menu_item_layout);
        this.f1318.setCallback(this);
        this.f1315.addMenuPresenter(this.f1318);
        builder.m535(this.f1318.m1536(), (DialogInterface.OnClickListener) this);
        View headerView = menuBuilder.getHeaderView();
        if (headerView != null) {
            builder.m534(headerView);
        } else {
            builder.m533(menuBuilder.getHeaderIcon()).m536(menuBuilder.getHeaderTitle());
        }
        builder.m532((DialogInterface.OnKeyListener) this);
        this.f1317 = builder.m525();
        this.f1317.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.f1317.getWindow().getAttributes();
        attributes.type = PointerIconCompat.TYPE_HELP;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.f1317.show();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1546(MenuBuilder menuBuilder, boolean z) {
        if (z || menuBuilder == this.f1315) {
            m1544();
        }
        if (this.f1316 != null) {
            this.f1316.m1606(menuBuilder, z);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1547(MenuBuilder menuBuilder) {
        if (this.f1316 != null) {
            return this.f1316.m1607(menuBuilder);
        }
        return false;
    }
}
