package android.support.v7.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.internal.view.SupportMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

class MenuWrapperICS extends BaseMenuWrapper<SupportMenu> implements Menu {
    MenuWrapperICS(Context context, SupportMenu supportMenu) {
        super(context, supportMenu);
    }

    public MenuItem add(int i) {
        return m1507(((SupportMenu) this.f1242).add(i));
    }

    public MenuItem add(int i, int i2, int i3, int i4) {
        return m1507(((SupportMenu) this.f1242).add(i, i2, i3, i4));
    }

    public MenuItem add(int i, int i2, int i3, CharSequence charSequence) {
        return m1507(((SupportMenu) this.f1242).add(i, i2, i3, charSequence));
    }

    public MenuItem add(CharSequence charSequence) {
        return m1507(((SupportMenu) this.f1242).add(charSequence));
    }

    public int addIntentOptions(int i, int i2, int i3, ComponentName componentName, Intent[] intentArr, Intent intent, int i4, MenuItem[] menuItemArr) {
        MenuItem[] menuItemArr2 = null;
        if (menuItemArr != null) {
            menuItemArr2 = new MenuItem[menuItemArr.length];
        }
        int addIntentOptions = ((SupportMenu) this.f1242).addIntentOptions(i, i2, i3, componentName, intentArr, intent, i4, menuItemArr2);
        if (menuItemArr2 != null) {
            int length = menuItemArr2.length;
            for (int i5 = 0; i5 < length; i5++) {
                menuItemArr[i5] = m1507(menuItemArr2[i5]);
            }
        }
        return addIntentOptions;
    }

    public SubMenu addSubMenu(int i) {
        return m1508(((SupportMenu) this.f1242).addSubMenu(i));
    }

    public SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return m1508(((SupportMenu) this.f1242).addSubMenu(i, i2, i3, i4));
    }

    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        return m1508(((SupportMenu) this.f1242).addSubMenu(i, i2, i3, charSequence));
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return m1508(((SupportMenu) this.f1242).addSubMenu(charSequence));
    }

    public void clear() {
        m1509();
        ((SupportMenu) this.f1242).clear();
    }

    public void close() {
        ((SupportMenu) this.f1242).close();
    }

    public MenuItem findItem(int i) {
        return m1507(((SupportMenu) this.f1242).findItem(i));
    }

    public MenuItem getItem(int i) {
        return m1507(((SupportMenu) this.f1242).getItem(i));
    }

    public boolean hasVisibleItems() {
        return ((SupportMenu) this.f1242).hasVisibleItems();
    }

    public boolean isShortcutKey(int i, KeyEvent keyEvent) {
        return ((SupportMenu) this.f1242).isShortcutKey(i, keyEvent);
    }

    public boolean performIdentifierAction(int i, int i2) {
        return ((SupportMenu) this.f1242).performIdentifierAction(i, i2);
    }

    public boolean performShortcut(int i, KeyEvent keyEvent, int i2) {
        return ((SupportMenu) this.f1242).performShortcut(i, keyEvent, i2);
    }

    public void removeGroup(int i) {
        m1510(i);
        ((SupportMenu) this.f1242).removeGroup(i);
    }

    public void removeItem(int i) {
        m1506(i);
        ((SupportMenu) this.f1242).removeItem(i);
    }

    public void setGroupCheckable(int i, boolean z, boolean z2) {
        ((SupportMenu) this.f1242).setGroupCheckable(i, z, z2);
    }

    public void setGroupEnabled(int i, boolean z) {
        ((SupportMenu) this.f1242).setGroupEnabled(i, z);
    }

    public void setGroupVisible(int i, boolean z) {
        ((SupportMenu) this.f1242).setGroupVisible(i, z);
    }

    public void setQwertyMode(boolean z) {
        ((SupportMenu) this.f1242).setQwertyMode(z);
    }

    public int size() {
        return ((SupportMenu) this.f1242).size();
    }
}
