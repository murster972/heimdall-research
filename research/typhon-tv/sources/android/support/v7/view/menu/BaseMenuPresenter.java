package android.support.v7.view.menu;

import android.content.Context;
import android.support.annotation.RestrictTo;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

@RestrictTo
public abstract class BaseMenuPresenter implements MenuPresenter {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected MenuView f1229;

    /* renamed from: ʼ  reason: contains not printable characters */
    private MenuPresenter.Callback f1230;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f1231;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f1232;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f1233;

    /* renamed from: 连任  reason: contains not printable characters */
    protected LayoutInflater f1234;

    /* renamed from: 靐  reason: contains not printable characters */
    protected Context f1235;

    /* renamed from: 麤  reason: contains not printable characters */
    protected LayoutInflater f1236;

    /* renamed from: 齉  reason: contains not printable characters */
    protected MenuBuilder f1237;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Context f1238;

    public BaseMenuPresenter(Context context, int i, int i2) {
        this.f1238 = context;
        this.f1236 = LayoutInflater.from(context);
        this.f1231 = i;
        this.f1232 = i2;
    }

    public boolean collapseItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean expandItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean flagActionItems() {
        return false;
    }

    public int getId() {
        return this.f1233;
    }

    public void initForMenu(Context context, MenuBuilder menuBuilder) {
        this.f1235 = context;
        this.f1234 = LayoutInflater.from(this.f1235);
        this.f1237 = menuBuilder;
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        if (this.f1230 != null) {
            this.f1230.m1606(menuBuilder, z);
        }
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        if (this.f1230 != null) {
            return this.f1230.m1607(subMenuBuilder);
        }
        return false;
    }

    public void setCallback(MenuPresenter.Callback callback) {
        this.f1230 = callback;
    }

    public void updateMenuView(boolean z) {
        ViewGroup viewGroup = (ViewGroup) this.f1229;
        if (viewGroup != null) {
            int i = 0;
            if (this.f1237 != null) {
                this.f1237.flagActionItems();
                ArrayList<MenuItemImpl> visibleItems = this.f1237.getVisibleItems();
                int size = visibleItems.size();
                for (int i2 = 0; i2 < size; i2++) {
                    MenuItemImpl menuItemImpl = visibleItems.get(i2);
                    if (m1504(i, menuItemImpl)) {
                        View childAt = viewGroup.getChildAt(i);
                        MenuItemImpl itemData = childAt instanceof MenuView.ItemView ? ((MenuView.ItemView) childAt).getItemData() : null;
                        View r5 = m1500(menuItemImpl, childAt, viewGroup);
                        if (menuItemImpl != itemData) {
                            r5.setPressed(false);
                            r5.jumpDrawablesToCurrentState();
                        }
                        if (r5 != childAt) {
                            m1503(r5, i);
                        }
                        i++;
                    }
                }
            }
            while (i < viewGroup.getChildCount()) {
                if (!m1505(viewGroup, i)) {
                    i++;
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public MenuView.ItemView m1497(ViewGroup viewGroup) {
        return (MenuView.ItemView) this.f1236.inflate(this.f1232, viewGroup, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MenuPresenter.Callback m1498() {
        return this.f1230;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MenuView m1499(ViewGroup viewGroup) {
        if (this.f1229 == null) {
            this.f1229 = (MenuView) this.f1236.inflate(this.f1231, viewGroup, false);
            this.f1229.initialize(this.f1237);
            updateMenuView(true);
        }
        return this.f1229;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public View m1500(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        MenuView.ItemView r0 = view instanceof MenuView.ItemView ? (MenuView.ItemView) view : m1497(viewGroup);
        m1502(menuItemImpl, r0);
        return (View) r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1501(int i) {
        this.f1233 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m1502(MenuItemImpl menuItemImpl, MenuView.ItemView itemView);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1503(View view, int i) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.f1229).addView(view, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1504(int i, MenuItemImpl menuItemImpl) {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1505(ViewGroup viewGroup, int i) {
        viewGroup.removeViewAt(i);
        return true;
    }
}
