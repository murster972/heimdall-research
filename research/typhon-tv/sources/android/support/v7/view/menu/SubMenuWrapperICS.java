package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.internal.view.SupportSubMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

@RestrictTo
class SubMenuWrapperICS extends MenuWrapperICS implements SubMenu {
    SubMenuWrapperICS(Context context, SupportSubMenu supportSubMenu) {
        super(context, supportSubMenu);
    }

    public void clearHeader() {
        m1623().clearHeader();
    }

    public MenuItem getItem() {
        return m1507(m1623().getItem());
    }

    public SubMenu setHeaderIcon(int i) {
        m1623().setHeaderIcon(i);
        return this;
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        m1623().setHeaderIcon(drawable);
        return this;
    }

    public SubMenu setHeaderTitle(int i) {
        m1623().setHeaderTitle(i);
        return this;
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        m1623().setHeaderTitle(charSequence);
        return this;
    }

    public SubMenu setHeaderView(View view) {
        m1623().setHeaderView(view);
        return this;
    }

    public SubMenu setIcon(int i) {
        m1623().setIcon(i);
        return this;
    }

    public SubMenu setIcon(Drawable drawable) {
        m1623().setIcon(drawable);
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public SupportSubMenu m1623() {
        return (SupportSubMenu) this.f1242;
    }
}
