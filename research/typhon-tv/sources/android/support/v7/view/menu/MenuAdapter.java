package android.support.v7.view.menu;

import android.support.annotation.RestrictTo;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

@RestrictTo
public class MenuAdapter extends BaseAdapter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final int f1309 = R.layout.abc_popup_menu_item_layout;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final LayoutInflater f1310;

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f1311;

    /* renamed from: 靐  reason: contains not printable characters */
    MenuBuilder f1312;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f1313;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f1314 = -1;

    public MenuAdapter(MenuBuilder menuBuilder, LayoutInflater layoutInflater, boolean z) {
        this.f1311 = z;
        this.f1310 = layoutInflater;
        this.f1312 = menuBuilder;
        m1540();
    }

    public int getCount() {
        ArrayList<MenuItemImpl> nonActionItems = this.f1311 ? this.f1312.getNonActionItems() : this.f1312.getVisibleItems();
        return this.f1314 < 0 ? nonActionItems.size() : nonActionItems.size() - 1;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.f1310.inflate(f1309, viewGroup, false);
        }
        MenuView.ItemView itemView = (MenuView.ItemView) view;
        if (this.f1313) {
            ((ListMenuItemView) view).setForceShowIcon(true);
        }
        itemView.initialize(getItem(i), 0);
        return view;
    }

    public void notifyDataSetChanged() {
        m1540();
        super.notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1540() {
        MenuItemImpl expandedItem = this.f1312.getExpandedItem();
        if (expandedItem != null) {
            ArrayList<MenuItemImpl> nonActionItems = this.f1312.getNonActionItems();
            int size = nonActionItems.size();
            for (int i = 0; i < size; i++) {
                if (nonActionItems.get(i) == expandedItem) {
                    this.f1314 = i;
                    return;
                }
            }
        }
        this.f1314 = -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MenuBuilder m1541() {
        return this.f1312;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MenuItemImpl getItem(int i) {
        ArrayList<MenuItemImpl> nonActionItems = this.f1311 ? this.f1312.getNonActionItems() : this.f1312.getVisibleItems();
        if (this.f1314 >= 0 && i >= this.f1314) {
            i++;
        }
        return nonActionItems.get(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1543(boolean z) {
        this.f1313 = z;
    }
}
