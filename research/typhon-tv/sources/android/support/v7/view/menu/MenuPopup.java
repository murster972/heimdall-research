package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.Rect;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;

abstract class MenuPopup implements MenuPresenter, ShowableListMenu, AdapterView.OnItemClickListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private Rect f1363;

    MenuPopup() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    protected static boolean m1577(MenuBuilder menuBuilder) {
        int size = menuBuilder.size();
        for (int i = 0; i < size; i++) {
            MenuItem item = menuBuilder.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static int m1578(ListAdapter listAdapter, ViewGroup viewGroup, Context context, int i) {
        int i2 = 0;
        View view = null;
        int i3 = 0;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = listAdapter.getCount();
        for (int i4 = 0; i4 < count; i4++) {
            int itemViewType = listAdapter.getItemViewType(i4);
            if (itemViewType != i3) {
                i3 = itemViewType;
                view = null;
            }
            if (viewGroup == null) {
                viewGroup = new FrameLayout(context);
            }
            view = listAdapter.getView(i4, view, viewGroup);
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            int measuredWidth = view.getMeasuredWidth();
            if (measuredWidth >= i) {
                return i;
            }
            if (measuredWidth > i2) {
                i2 = measuredWidth;
            }
        }
        return i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static MenuAdapter m1579(ListAdapter listAdapter) {
        return listAdapter instanceof HeaderViewListAdapter ? (MenuAdapter) ((HeaderViewListAdapter) listAdapter).getWrappedAdapter() : (MenuAdapter) listAdapter;
    }

    public boolean collapseItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean expandItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public int getId() {
        return 0;
    }

    public void initForMenu(Context context, MenuBuilder menuBuilder) {
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        ListAdapter listAdapter = (ListAdapter) adapterView.getAdapter();
        m1579(listAdapter).f1312.performItemAction((MenuItem) listAdapter.getItem(i), this, m1590() ? 0 : 4);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Rect m1580() {
        return this.f1363;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m1581(int i);

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m1582(boolean z);

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m1583(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m1584(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1585(Rect rect) {
        this.f1363 = rect;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m1586(MenuBuilder menuBuilder);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m1587(View view);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m1588(PopupWindow.OnDismissListener onDismissListener);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m1589(boolean z);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1590() {
        return true;
    }
}
