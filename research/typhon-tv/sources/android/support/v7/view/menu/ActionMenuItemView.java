package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.annotation.RestrictTo;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.ForwardingListener;
import android.support.v7.widget.TooltipCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

@RestrictTo
public class ActionMenuItemView extends AppCompatTextView implements MenuView.ItemView, ActionMenuView.ActionMenuChildView, View.OnClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private ForwardingListener f1217;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f1218;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f1219;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f1220;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f1221;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f1222;

    /* renamed from: 连任  reason: contains not printable characters */
    private Drawable f1223;

    /* renamed from: 靐  reason: contains not printable characters */
    MenuBuilder.ItemInvoker f1224;

    /* renamed from: 麤  reason: contains not printable characters */
    private CharSequence f1225;

    /* renamed from: 齉  reason: contains not printable characters */
    PopupCallback f1226;

    /* renamed from: 龘  reason: contains not printable characters */
    MenuItemImpl f1227;

    private class ActionMenuItemForwardingListener extends ForwardingListener {
        public ActionMenuItemForwardingListener() {
            super(ActionMenuItemView.this);
        }

        public ShowableListMenu getPopup() {
            if (ActionMenuItemView.this.f1226 != null) {
                return ActionMenuItemView.this.f1226.m1496();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:4:0x0015, code lost:
            r0 = getPopup();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onForwardingStarted() {
            /*
                r4 = this;
                r1 = 0
                android.support.v7.view.menu.ActionMenuItemView r2 = android.support.v7.view.menu.ActionMenuItemView.this
                android.support.v7.view.menu.MenuBuilder$ItemInvoker r2 = r2.f1224
                if (r2 == 0) goto L_0x0022
                android.support.v7.view.menu.ActionMenuItemView r2 = android.support.v7.view.menu.ActionMenuItemView.this
                android.support.v7.view.menu.MenuBuilder$ItemInvoker r2 = r2.f1224
                android.support.v7.view.menu.ActionMenuItemView r3 = android.support.v7.view.menu.ActionMenuItemView.this
                android.support.v7.view.menu.MenuItemImpl r3 = r3.f1227
                boolean r2 = r2.invokeItem(r3)
                if (r2 == 0) goto L_0x0022
                android.support.v7.view.menu.ShowableListMenu r0 = r4.getPopup()
                if (r0 == 0) goto L_0x0022
                boolean r2 = r0.isShowing()
                if (r2 == 0) goto L_0x0022
                r1 = 1
            L_0x0022:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.view.menu.ActionMenuItemView.ActionMenuItemForwardingListener.onForwardingStarted():boolean");
        }
    }

    public static abstract class PopupCallback {
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract ShowableListMenu m1496();
    }

    public ActionMenuItemView(Context context) {
        this(context, (AttributeSet) null);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Resources resources = context.getResources();
        this.f1218 = m1493();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ActionMenuItemView, i, 0);
        this.f1220 = obtainStyledAttributes.getDimensionPixelSize(R.styleable.ActionMenuItemView_android_minWidth, 0);
        obtainStyledAttributes.recycle();
        this.f1222 = (int) ((32.0f * resources.getDisplayMetrics().density) + 0.5f);
        setOnClickListener(this);
        this.f1221 = -1;
        setSaveEnabled(false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m1493() {
        Configuration configuration = getContext().getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        return i >= 480 || (i >= 640 && configuration.screenHeightDp >= 480) || configuration.orientation == 2;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m1494() {
        boolean z = false;
        CharSequence charSequence = null;
        boolean z2 = !TextUtils.isEmpty(this.f1225);
        if (this.f1223 == null || (this.f1227.m1553() && (this.f1218 || this.f1219))) {
            z = true;
        }
        boolean z3 = z2 & z;
        setText(z3 ? this.f1225 : null);
        CharSequence contentDescription = this.f1227.getContentDescription();
        if (TextUtils.isEmpty(contentDescription)) {
            setContentDescription(z3 ? null : this.f1227.getTitle());
        } else {
            setContentDescription(contentDescription);
        }
        CharSequence tooltipText = this.f1227.getTooltipText();
        if (TextUtils.isEmpty(tooltipText)) {
            if (!z3) {
                charSequence = this.f1227.getTitle();
            }
            TooltipCompat.setTooltipText(this, charSequence);
            return;
        }
        TooltipCompat.setTooltipText(this, tooltipText);
    }

    public MenuItemImpl getItemData() {
        return this.f1227;
    }

    public void initialize(MenuItemImpl menuItemImpl, int i) {
        this.f1227 = menuItemImpl;
        setIcon(menuItemImpl.getIcon());
        setTitle(menuItemImpl.m1568((MenuView.ItemView) this));
        setId(menuItemImpl.getItemId());
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
        setEnabled(menuItemImpl.isEnabled());
        if (menuItemImpl.hasSubMenu() && this.f1217 == null) {
            this.f1217 = new ActionMenuItemForwardingListener();
        }
    }

    public boolean needsDividerAfter() {
        return m1495();
    }

    public boolean needsDividerBefore() {
        return m1495() && this.f1227.getIcon() == null;
    }

    public void onClick(View view) {
        if (this.f1224 != null) {
            this.f1224.invokeItem(this.f1227);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.f1218 = m1493();
        m1494();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        boolean r3 = m1495();
        if (r3 && this.f1221 >= 0) {
            super.setPadding(this.f1221, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int measuredWidth = getMeasuredWidth();
        int min = mode == Integer.MIN_VALUE ? Math.min(size, this.f1220) : this.f1220;
        if (mode != 1073741824 && this.f1220 > 0 && measuredWidth < min) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(min, 1073741824), i2);
        }
        if (!r3 && this.f1223 != null) {
            super.setPadding((getMeasuredWidth() - this.f1223.getBounds().width()) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        super.onRestoreInstanceState((Parcelable) null);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f1227.hasSubMenu() || this.f1217 == null || !this.f1217.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public boolean prefersCondensedTitle() {
        return true;
    }

    public void setCheckable(boolean z) {
    }

    public void setChecked(boolean z) {
    }

    public void setExpandedFormat(boolean z) {
        if (this.f1219 != z) {
            this.f1219 = z;
            if (this.f1227 != null) {
                this.f1227.m1550();
            }
        }
    }

    public void setIcon(Drawable drawable) {
        this.f1223 = drawable;
        if (drawable != null) {
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (intrinsicWidth > this.f1222) {
                float f = ((float) this.f1222) / ((float) intrinsicWidth);
                intrinsicWidth = this.f1222;
                intrinsicHeight = (int) (((float) intrinsicHeight) * f);
            }
            if (intrinsicHeight > this.f1222) {
                float f2 = ((float) this.f1222) / ((float) intrinsicHeight);
                intrinsicHeight = this.f1222;
                intrinsicWidth = (int) (((float) intrinsicWidth) * f2);
            }
            drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        }
        setCompoundDrawables(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        m1494();
    }

    public void setItemInvoker(MenuBuilder.ItemInvoker itemInvoker) {
        this.f1224 = itemInvoker;
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.f1221 = i;
        super.setPadding(i, i2, i3, i4);
    }

    public void setPopupCallback(PopupCallback popupCallback) {
        this.f1226 = popupCallback;
    }

    public void setShortcut(boolean z, char c) {
    }

    public void setTitle(CharSequence charSequence) {
        this.f1225 = charSequence;
        m1494();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1495() {
        return !TextUtils.isEmpty(getText());
    }
}
