package android.support.v7.view.menu;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.view.ActionProvider;
import android.support.v7.view.CollapsibleActionView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import java.lang.reflect.Method;

@RestrictTo
public class MenuItemWrapperICS extends BaseMenuWrapper<SupportMenuItem> implements MenuItem {

    /* renamed from: 齉  reason: contains not printable characters */
    private Method f1355;

    class ActionProviderWrapper extends ActionProvider {

        /* renamed from: 龘  reason: contains not printable characters */
        final android.view.ActionProvider f1357;

        public ActionProviderWrapper(Context context, android.view.ActionProvider actionProvider) {
            super(context);
            this.f1357 = actionProvider;
        }

        public boolean hasSubMenu() {
            return this.f1357.hasSubMenu();
        }

        public View onCreateActionView() {
            return this.f1357.onCreateActionView();
        }

        public boolean onPerformDefaultAction() {
            return this.f1357.onPerformDefaultAction();
        }

        public void onPrepareSubMenu(SubMenu subMenu) {
            this.f1357.onPrepareSubMenu(MenuItemWrapperICS.this.m1508(subMenu));
        }
    }

    static class CollapsibleActionViewWrapper extends FrameLayout implements CollapsibleActionView {

        /* renamed from: 龘  reason: contains not printable characters */
        final android.view.CollapsibleActionView f1358;

        CollapsibleActionViewWrapper(View view) {
            super(view.getContext());
            this.f1358 = (android.view.CollapsibleActionView) view;
            addView(view);
        }

        public void onActionViewCollapsed() {
            this.f1358.onActionViewCollapsed();
        }

        public void onActionViewExpanded() {
            this.f1358.onActionViewExpanded();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public View m1575() {
            return (View) this.f1358;
        }
    }

    private class OnActionExpandListenerWrapper extends BaseWrapper<MenuItem.OnActionExpandListener> implements MenuItem.OnActionExpandListener {
        OnActionExpandListenerWrapper(MenuItem.OnActionExpandListener onActionExpandListener) {
            super(onActionExpandListener);
        }

        public boolean onMenuItemActionCollapse(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.f1242).onMenuItemActionCollapse(MenuItemWrapperICS.this.m1507(menuItem));
        }

        public boolean onMenuItemActionExpand(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.f1242).onMenuItemActionExpand(MenuItemWrapperICS.this.m1507(menuItem));
        }
    }

    private class OnMenuItemClickListenerWrapper extends BaseWrapper<MenuItem.OnMenuItemClickListener> implements MenuItem.OnMenuItemClickListener {
        OnMenuItemClickListenerWrapper(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
            super(onMenuItemClickListener);
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            return ((MenuItem.OnMenuItemClickListener) this.f1242).onMenuItemClick(MenuItemWrapperICS.this.m1507(menuItem));
        }
    }

    MenuItemWrapperICS(Context context, SupportMenuItem supportMenuItem) {
        super(context, supportMenuItem);
    }

    public boolean collapseActionView() {
        return ((SupportMenuItem) this.f1242).collapseActionView();
    }

    public boolean expandActionView() {
        return ((SupportMenuItem) this.f1242).expandActionView();
    }

    public android.view.ActionProvider getActionProvider() {
        ActionProvider supportActionProvider = ((SupportMenuItem) this.f1242).getSupportActionProvider();
        if (supportActionProvider instanceof ActionProviderWrapper) {
            return ((ActionProviderWrapper) supportActionProvider).f1357;
        }
        return null;
    }

    public View getActionView() {
        View actionView = ((SupportMenuItem) this.f1242).getActionView();
        return actionView instanceof CollapsibleActionViewWrapper ? ((CollapsibleActionViewWrapper) actionView).m1575() : actionView;
    }

    public int getAlphabeticModifiers() {
        return ((SupportMenuItem) this.f1242).getAlphabeticModifiers();
    }

    public char getAlphabeticShortcut() {
        return ((SupportMenuItem) this.f1242).getAlphabeticShortcut();
    }

    public CharSequence getContentDescription() {
        return ((SupportMenuItem) this.f1242).getContentDescription();
    }

    public int getGroupId() {
        return ((SupportMenuItem) this.f1242).getGroupId();
    }

    public Drawable getIcon() {
        return ((SupportMenuItem) this.f1242).getIcon();
    }

    public ColorStateList getIconTintList() {
        return ((SupportMenuItem) this.f1242).getIconTintList();
    }

    public PorterDuff.Mode getIconTintMode() {
        return ((SupportMenuItem) this.f1242).getIconTintMode();
    }

    public Intent getIntent() {
        return ((SupportMenuItem) this.f1242).getIntent();
    }

    public int getItemId() {
        return ((SupportMenuItem) this.f1242).getItemId();
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return ((SupportMenuItem) this.f1242).getMenuInfo();
    }

    public int getNumericModifiers() {
        return ((SupportMenuItem) this.f1242).getNumericModifiers();
    }

    public char getNumericShortcut() {
        return ((SupportMenuItem) this.f1242).getNumericShortcut();
    }

    public int getOrder() {
        return ((SupportMenuItem) this.f1242).getOrder();
    }

    public SubMenu getSubMenu() {
        return m1508(((SupportMenuItem) this.f1242).getSubMenu());
    }

    public CharSequence getTitle() {
        return ((SupportMenuItem) this.f1242).getTitle();
    }

    public CharSequence getTitleCondensed() {
        return ((SupportMenuItem) this.f1242).getTitleCondensed();
    }

    public CharSequence getTooltipText() {
        return ((SupportMenuItem) this.f1242).getTooltipText();
    }

    public boolean hasSubMenu() {
        return ((SupportMenuItem) this.f1242).hasSubMenu();
    }

    public boolean isActionViewExpanded() {
        return ((SupportMenuItem) this.f1242).isActionViewExpanded();
    }

    public boolean isCheckable() {
        return ((SupportMenuItem) this.f1242).isCheckable();
    }

    public boolean isChecked() {
        return ((SupportMenuItem) this.f1242).isChecked();
    }

    public boolean isEnabled() {
        return ((SupportMenuItem) this.f1242).isEnabled();
    }

    public boolean isVisible() {
        return ((SupportMenuItem) this.f1242).isVisible();
    }

    public MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        ((SupportMenuItem) this.f1242).setSupportActionProvider(actionProvider != null ? m1573(actionProvider) : null);
        return this;
    }

    public MenuItem setActionView(int i) {
        ((SupportMenuItem) this.f1242).setActionView(i);
        View actionView = ((SupportMenuItem) this.f1242).getActionView();
        if (actionView instanceof android.view.CollapsibleActionView) {
            ((SupportMenuItem) this.f1242).setActionView((View) new CollapsibleActionViewWrapper(actionView));
        }
        return this;
    }

    public MenuItem setActionView(View view) {
        if (view instanceof android.view.CollapsibleActionView) {
            view = new CollapsibleActionViewWrapper(view);
        }
        ((SupportMenuItem) this.f1242).setActionView(view);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c) {
        ((SupportMenuItem) this.f1242).setAlphabeticShortcut(c);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c, int i) {
        ((SupportMenuItem) this.f1242).setAlphabeticShortcut(c, i);
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        ((SupportMenuItem) this.f1242).setCheckable(z);
        return this;
    }

    public MenuItem setChecked(boolean z) {
        ((SupportMenuItem) this.f1242).setChecked(z);
        return this;
    }

    public MenuItem setContentDescription(CharSequence charSequence) {
        ((SupportMenuItem) this.f1242).setContentDescription(charSequence);
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        ((SupportMenuItem) this.f1242).setEnabled(z);
        return this;
    }

    public MenuItem setIcon(int i) {
        ((SupportMenuItem) this.f1242).setIcon(i);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        ((SupportMenuItem) this.f1242).setIcon(drawable);
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        ((SupportMenuItem) this.f1242).setIconTintList(colorStateList);
        return this;
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        ((SupportMenuItem) this.f1242).setIconTintMode(mode);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        ((SupportMenuItem) this.f1242).setIntent(intent);
        return this;
    }

    public MenuItem setNumericShortcut(char c) {
        ((SupportMenuItem) this.f1242).setNumericShortcut(c);
        return this;
    }

    public MenuItem setNumericShortcut(char c, int i) {
        ((SupportMenuItem) this.f1242).setNumericShortcut(c, i);
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        ((SupportMenuItem) this.f1242).setOnActionExpandListener(onActionExpandListener != null ? new OnActionExpandListenerWrapper(onActionExpandListener) : null);
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        ((SupportMenuItem) this.f1242).setOnMenuItemClickListener(onMenuItemClickListener != null ? new OnMenuItemClickListenerWrapper(onMenuItemClickListener) : null);
        return this;
    }

    public MenuItem setShortcut(char c, char c2) {
        ((SupportMenuItem) this.f1242).setShortcut(c, c2);
        return this;
    }

    public MenuItem setShortcut(char c, char c2, int i, int i2) {
        ((SupportMenuItem) this.f1242).setShortcut(c, c2, i, i2);
        return this;
    }

    public void setShowAsAction(int i) {
        ((SupportMenuItem) this.f1242).setShowAsAction(i);
    }

    public MenuItem setShowAsActionFlags(int i) {
        ((SupportMenuItem) this.f1242).setShowAsActionFlags(i);
        return this;
    }

    public MenuItem setTitle(int i) {
        ((SupportMenuItem) this.f1242).setTitle(i);
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        ((SupportMenuItem) this.f1242).setTitle(charSequence);
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        ((SupportMenuItem) this.f1242).setTitleCondensed(charSequence);
        return this;
    }

    public MenuItem setTooltipText(CharSequence charSequence) {
        ((SupportMenuItem) this.f1242).setTooltipText(charSequence);
        return this;
    }

    public MenuItem setVisible(boolean z) {
        return ((SupportMenuItem) this.f1242).setVisible(z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ActionProviderWrapper m1573(android.view.ActionProvider actionProvider) {
        return new ActionProviderWrapper(this.f1241, actionProvider);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1574(boolean z) {
        try {
            if (this.f1355 == null) {
                this.f1355 = ((SupportMenuItem) this.f1242).getClass().getDeclaredMethod("setExclusiveCheckable", new Class[]{Boolean.TYPE});
            }
            this.f1355.invoke(this.f1242, new Object[]{Boolean.valueOf(z)});
        } catch (Exception e) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e);
        }
    }
}
