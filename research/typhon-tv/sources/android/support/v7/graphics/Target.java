package android.support.v7.graphics;

public final class Target {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final Target f895 = new Target();

    /* renamed from: 连任  reason: contains not printable characters */
    public static final Target f896 = new Target();

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Target f897 = new Target();

    /* renamed from: 麤  reason: contains not printable characters */
    public static final Target f898 = new Target();

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Target f899 = new Target();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Target f900 = new Target();

    /* renamed from: ʼ  reason: contains not printable characters */
    final float[] f901 = new float[3];

    /* renamed from: ʽ  reason: contains not printable characters */
    final float[] f902 = new float[3];

    /* renamed from: ˑ  reason: contains not printable characters */
    final float[] f903 = new float[3];

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean f904 = true;

    static {
        m1000(f900);
        m999(f900);
        m998(f897);
        m999(f897);
        m1001(f899);
        m999(f899);
        m1000(f898);
        m997(f898);
        m998(f896);
        m997(f896);
        m1001(f895);
        m997(f895);
    }

    Target() {
        m1002(this.f901);
        m1002(this.f902);
        m996();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m996() {
        this.f903[0] = 0.24f;
        this.f903[1] = 0.52f;
        this.f903[2] = 0.24f;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static void m997(Target target) {
        target.f901[1] = 0.3f;
        target.f901[2] = 0.4f;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m998(Target target) {
        target.f902[0] = 0.3f;
        target.f902[1] = 0.5f;
        target.f902[2] = 0.7f;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static void m999(Target target) {
        target.f901[0] = 0.35f;
        target.f901[1] = 1.0f;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static void m1000(Target target) {
        target.f902[0] = 0.55f;
        target.f902[1] = 0.74f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m1001(Target target) {
        target.f902[1] = 0.26f;
        target.f902[2] = 0.45f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m1002(float[] fArr) {
        fArr[0] = 0.0f;
        fArr[1] = 0.5f;
        fArr[2] = 1.0f;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public float m1003() {
        return this.f902[2];
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public float m1004() {
        return this.f903[0];
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public float m1005() {
        return this.f903[1];
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public float m1006() {
        return this.f903[2];
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m1007() {
        return this.f904;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m1008() {
        float f = 0.0f;
        for (float f2 : this.f903) {
            if (f2 > 0.0f) {
                f += f2;
            }
        }
        if (f != 0.0f) {
            int length = this.f903.length;
            for (int i = 0; i < length; i++) {
                if (this.f903[i] > 0.0f) {
                    float[] fArr = this.f903;
                    fArr[i] = fArr[i] / f;
                }
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public float m1009() {
        return this.f902[1];
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public float m1010() {
        return this.f901[1];
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public float m1011() {
        return this.f902[0];
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public float m1012() {
        return this.f901[2];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public float m1013() {
        return this.f901[0];
    }
}
