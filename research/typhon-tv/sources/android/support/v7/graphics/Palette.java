package android.support.v7.graphics;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.util.ArrayMap;
import android.util.SparseBooleanArray;
import android.util.TimingLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class Palette {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Filter f872 = new Filter() {
        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m981(float[] fArr) {
            return fArr[2] >= 0.95f;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean m982(float[] fArr) {
            return fArr[0] >= 10.0f && fArr[0] <= 37.0f && fArr[1] <= 0.82f;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m983(float[] fArr) {
            return fArr[2] <= 0.05f;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m984(int i, float[] fArr) {
            return !m981(fArr) && !m983(fArr) && !m982(fArr);
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Swatch f873 = m976();

    /* renamed from: 连任  reason: contains not printable characters */
    private final SparseBooleanArray f874 = new SparseBooleanArray();

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<Swatch> f875;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<Target, Swatch> f876 = new ArrayMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<Target> f877;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f878 = -1;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final List<Filter> f879 = new ArrayList();

        /* renamed from: ʽ  reason: contains not printable characters */
        private Rect f880;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f881 = 12544;

        /* renamed from: 靐  reason: contains not printable characters */
        private final Bitmap f882;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f883 = 16;

        /* renamed from: 齉  reason: contains not printable characters */
        private final List<Target> f884 = new ArrayList();

        /* renamed from: 龘  reason: contains not printable characters */
        private final List<Swatch> f885;

        public Builder(Bitmap bitmap) {
            if (bitmap == null || bitmap.isRecycled()) {
                throw new IllegalArgumentException("Bitmap is not valid");
            }
            this.f879.add(Palette.f872);
            this.f882 = bitmap;
            this.f885 = null;
            this.f884.add(Target.f900);
            this.f884.add(Target.f897);
            this.f884.add(Target.f899);
            this.f884.add(Target.f898);
            this.f884.add(Target.f896);
            this.f884.add(Target.f895);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private Bitmap m985(Bitmap bitmap) {
            int max;
            double d = -1.0d;
            if (this.f881 > 0) {
                int width = bitmap.getWidth() * bitmap.getHeight();
                if (width > this.f881) {
                    d = Math.sqrt(((double) this.f881) / ((double) width));
                }
            } else if (this.f878 > 0 && (max = Math.max(bitmap.getWidth(), bitmap.getHeight())) > this.f878) {
                d = ((double) this.f878) / ((double) max);
            }
            return d <= 0.0d ? bitmap : Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * d), (int) Math.ceil(((double) bitmap.getHeight()) * d), false);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int[] m986(Bitmap bitmap) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int[] iArr = new int[(width * height)];
            bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
            if (this.f880 == null) {
                return iArr;
            }
            int width2 = this.f880.width();
            int height2 = this.f880.height();
            int[] iArr2 = new int[(width2 * height2)];
            for (int i = 0; i < height2; i++) {
                System.arraycopy(iArr, ((this.f880.top + i) * width) + this.f880.left, iArr2, i * width2, width2);
            }
            return iArr2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m987(int i) {
            this.f883 = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Palette m988() {
            List<Swatch> list;
            TimingLogger timingLogger = null;
            if (this.f882 != null) {
                Bitmap r0 = m985(this.f882);
                if (timingLogger != null) {
                    timingLogger.addSplit("Processed Bitmap");
                }
                Rect rect = this.f880;
                if (!(r0 == this.f882 || rect == null)) {
                    double width = ((double) r0.getWidth()) / ((double) this.f882.getWidth());
                    rect.left = (int) Math.floor(((double) rect.left) * width);
                    rect.top = (int) Math.floor(((double) rect.top) * width);
                    rect.right = Math.min((int) Math.ceil(((double) rect.right) * width), r0.getWidth());
                    rect.bottom = Math.min((int) Math.ceil(((double) rect.bottom) * width), r0.getHeight());
                }
                ColorCutQuantizer colorCutQuantizer = new ColorCutQuantizer(m986(r0), this.f883, this.f879.isEmpty() ? null : (Filter[]) this.f879.toArray(new Filter[this.f879.size()]));
                if (r0 != this.f882) {
                    r0.recycle();
                }
                list = colorCutQuantizer.m964();
                if (timingLogger != null) {
                    timingLogger.addSplit("Color quantization completed");
                }
            } else {
                list = this.f885;
            }
            Palette palette = new Palette(list, this.f884);
            palette.m979();
            if (timingLogger != null) {
                timingLogger.addSplit("Created Palette");
                timingLogger.dumpToLog();
            }
            return palette;
        }
    }

    public interface Filter {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m989(int i, float[] fArr);
    }

    public static final class Swatch {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f886;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f887;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f888;

        /* renamed from: ˑ  reason: contains not printable characters */
        private float[] f889;

        /* renamed from: 连任  reason: contains not printable characters */
        private final int f890;

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f891;

        /* renamed from: 麤  reason: contains not printable characters */
        private final int f892;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f893;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f894;

        public Swatch(int i, int i2) {
            this.f894 = Color.red(i);
            this.f891 = Color.green(i);
            this.f893 = Color.blue(i);
            this.f892 = i;
            this.f890 = i2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m990() {
            if (!this.f886) {
                int calculateMinimumAlpha = ColorUtils.calculateMinimumAlpha(-1, this.f892, 4.5f);
                int calculateMinimumAlpha2 = ColorUtils.calculateMinimumAlpha(-1, this.f892, 3.0f);
                if (calculateMinimumAlpha == -1 || calculateMinimumAlpha2 == -1) {
                    int calculateMinimumAlpha3 = ColorUtils.calculateMinimumAlpha(-16777216, this.f892, 4.5f);
                    int calculateMinimumAlpha4 = ColorUtils.calculateMinimumAlpha(-16777216, this.f892, 3.0f);
                    if (calculateMinimumAlpha3 == -1 || calculateMinimumAlpha4 == -1) {
                        this.f888 = calculateMinimumAlpha != -1 ? ColorUtils.setAlphaComponent(-1, calculateMinimumAlpha) : ColorUtils.setAlphaComponent(-16777216, calculateMinimumAlpha3);
                        this.f887 = calculateMinimumAlpha2 != -1 ? ColorUtils.setAlphaComponent(-1, calculateMinimumAlpha2) : ColorUtils.setAlphaComponent(-16777216, calculateMinimumAlpha4);
                        this.f886 = true;
                        return;
                    }
                    this.f888 = ColorUtils.setAlphaComponent(-16777216, calculateMinimumAlpha3);
                    this.f887 = ColorUtils.setAlphaComponent(-16777216, calculateMinimumAlpha4);
                    this.f886 = true;
                    return;
                }
                this.f888 = ColorUtils.setAlphaComponent(-1, calculateMinimumAlpha);
                this.f887 = ColorUtils.setAlphaComponent(-1, calculateMinimumAlpha2);
                this.f886 = true;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Swatch swatch = (Swatch) obj;
            return this.f890 == swatch.f890 && this.f892 == swatch.f892;
        }

        public int hashCode() {
            return (this.f892 * 31) + this.f890;
        }

        public String toString() {
            return getClass().getSimpleName() + " [RGB: #" + Integer.toHexString(m995()) + ']' + " [HSL: " + Arrays.toString(m992()) + ']' + " [Population: " + this.f890 + ']' + " [Title Text: #" + Integer.toHexString(m993()) + ']' + " [Body Text: #" + Integer.toHexString(m991()) + ']';
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public int m991() {
            m990();
            return this.f888;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public float[] m992() {
            if (this.f889 == null) {
                this.f889 = new float[3];
            }
            ColorUtils.RGBToHSL(this.f894, this.f891, this.f893, this.f889);
            return this.f889;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public int m993() {
            m990();
            return this.f887;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public int m994() {
            return this.f890;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m995() {
            return this.f892;
        }
    }

    Palette(List<Swatch> list, List<Target> list2) {
        this.f875 = list;
        this.f877 = list2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private float m974(Swatch swatch, Target target) {
        float[] r0 = swatch.m992();
        float f = 0.0f;
        float f2 = 0.0f;
        float f3 = 0.0f;
        int r2 = this.f873 != null ? this.f873.m994() : 1;
        if (target.m1004() > 0.0f) {
            f = target.m1004() * (1.0f - Math.abs(r0[1] - target.m1010()));
        }
        if (target.m1005() > 0.0f) {
            f2 = target.m1005() * (1.0f - Math.abs(r0[2] - target.m1009()));
        }
        if (target.m1006() > 0.0f) {
            f3 = target.m1006() * (((float) swatch.m994()) / ((float) r2));
        }
        return f + f2 + f3;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Swatch m975(Target target) {
        float f = 0.0f;
        Swatch swatch = null;
        int size = this.f875.size();
        for (int i = 0; i < size; i++) {
            Swatch swatch2 = this.f875.get(i);
            if (m978(swatch2, target)) {
                float r4 = m974(swatch2, target);
                if (swatch == null || r4 > f) {
                    swatch = swatch2;
                    f = r4;
                }
            }
        }
        return swatch;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private Swatch m976() {
        int i = Integer.MIN_VALUE;
        Swatch swatch = null;
        int size = this.f875.size();
        for (int i2 = 0; i2 < size; i2++) {
            Swatch swatch2 = this.f875.get(i2);
            if (swatch2.m994() > i) {
                swatch = swatch2;
                i = swatch2.m994();
            }
        }
        return swatch;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Swatch m977(Target target) {
        Swatch r0 = m975(target);
        if (r0 != null && target.m1007()) {
            this.f874.append(r0.m995(), true);
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m978(Swatch swatch, Target target) {
        float[] r0 = swatch.m992();
        return r0[1] >= target.m1013() && r0[1] <= target.m1012() && r0[2] >= target.m1011() && r0[2] <= target.m1003() && !this.f874.get(swatch.m995());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m979() {
        int size = this.f877.size();
        for (int i = 0; i < size; i++) {
            Target target = this.f877.get(i);
            target.m1008();
            this.f876.put(target, m977(target));
        }
        this.f874.clear();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<Swatch> m980() {
        return Collections.unmodifiableList(this.f875);
    }
}
