package android.support.v7.graphics;

import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.graphics.Palette;
import android.util.TimingLogger;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

final class ColorCutQuantizer {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final Comparator<Vbox> f855 = new Comparator<Vbox>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(Vbox vbox, Vbox vbox2) {
            return vbox2.m973() - vbox.m973();
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private final float[] f856 = new float[3];

    /* renamed from: 连任  reason: contains not printable characters */
    final Palette.Filter[] f857;

    /* renamed from: 靐  reason: contains not printable characters */
    final int[] f858;

    /* renamed from: 麤  reason: contains not printable characters */
    final TimingLogger f859 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    final List<Palette.Swatch> f860;

    /* renamed from: 龘  reason: contains not printable characters */
    final int[] f861;

    private class Vbox {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f862;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f863;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f864;

        /* renamed from: ˑ  reason: contains not printable characters */
        private int f865;

        /* renamed from: ٴ  reason: contains not printable characters */
        private int f866;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f867;

        /* renamed from: 靐  reason: contains not printable characters */
        private int f868;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f869;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f870;

        Vbox(int i, int i2) {
            this.f868 = i;
            this.f870 = i2;
            m971();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public final int m966() {
            int i = this.f862 - this.f867;
            int i2 = this.f864 - this.f863;
            int i3 = this.f866 - this.f865;
            if (i < i2 || i < i3) {
                return (i2 < i || i2 < i3) ? -1 : -2;
            }
            return -3;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public final int m967() {
            int r4 = m966();
            int[] iArr = ColorCutQuantizer.this.f861;
            int[] iArr2 = ColorCutQuantizer.this.f858;
            ColorCutQuantizer.m961(iArr, r4, this.f868, this.f870);
            Arrays.sort(iArr, this.f868, this.f870 + 1);
            ColorCutQuantizer.m961(iArr, r4, this.f868, this.f870);
            int i = this.f869 / 2;
            int i2 = 0;
            for (int i3 = this.f868; i3 <= this.f870; i3++) {
                i2 += iArr2[iArr[i3]];
                if (i2 >= i) {
                    return Math.min(this.f870 - 1, i3);
                }
            }
            return this.f868;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public final Palette.Swatch m968() {
            int[] iArr = ColorCutQuantizer.this.f861;
            int[] iArr2 = ColorCutQuantizer.this.f858;
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            for (int i5 = this.f868; i5 <= this.f870; i5++) {
                int i6 = iArr[i5];
                int i7 = iArr2[i6];
                i4 += i7;
                i += ColorCutQuantizer.m957(i6) * i7;
                i2 += ColorCutQuantizer.m953(i6) * i7;
                i3 += ColorCutQuantizer.m956(i6) * i7;
            }
            return new Palette.Swatch(ColorCutQuantizer.m958(Math.round(((float) i) / ((float) i4)), Math.round(((float) i2) / ((float) i4)), Math.round(((float) i3) / ((float) i4))), i4);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 连任  reason: contains not printable characters */
        public final Vbox m969() {
            if (!m970()) {
                throw new IllegalStateException("Can not split a box with only 1 color");
            }
            int r1 = m967();
            Vbox vbox = new Vbox(r1 + 1, this.f870);
            this.f870 = r1;
            m971();
            return vbox;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public final boolean m970() {
            return m972() > 1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public final void m971() {
            int[] iArr = ColorCutQuantizer.this.f861;
            int[] iArr2 = ColorCutQuantizer.this.f858;
            int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
            int i2 = Integer.MAX_VALUE;
            int i3 = Integer.MAX_VALUE;
            int i4 = Integer.MIN_VALUE;
            int i5 = Integer.MIN_VALUE;
            int i6 = Integer.MIN_VALUE;
            int i7 = 0;
            for (int i8 = this.f868; i8 <= this.f870; i8++) {
                int i9 = iArr[i8];
                i7 += iArr2[i9];
                int r13 = ColorCutQuantizer.m957(i9);
                int r4 = ColorCutQuantizer.m953(i9);
                int r0 = ColorCutQuantizer.m956(i9);
                if (r13 > i6) {
                    i6 = r13;
                }
                if (r13 < i3) {
                    i3 = r13;
                }
                if (r4 > i5) {
                    i5 = r4;
                }
                if (r4 < i2) {
                    i2 = r4;
                }
                if (r0 > i4) {
                    i4 = r0;
                }
                if (r0 < i) {
                    i = r0;
                }
            }
            this.f867 = i3;
            this.f862 = i6;
            this.f863 = i2;
            this.f864 = i5;
            this.f865 = i;
            this.f866 = i4;
            this.f869 = i7;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public final int m972() {
            return (this.f870 + 1) - this.f868;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final int m973() {
            return ((this.f862 - this.f867) + 1) * ((this.f864 - this.f863) + 1) * ((this.f866 - this.f865) + 1);
        }
    }

    ColorCutQuantizer(int[] iArr, int i, Palette.Filter[] filterArr) {
        this.f857 = filterArr;
        int[] iArr2 = new int[32768];
        this.f858 = iArr2;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            int r8 = m950(iArr[i2]);
            iArr[i2] = r8;
            iArr2[r8] = iArr2[r8] + 1;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < iArr2.length; i4++) {
            if (iArr2[i4] > 0 && m952(i4)) {
                iArr2[i4] = 0;
            }
            if (iArr2[i4] > 0) {
                i3++;
            }
        }
        int[] iArr3 = new int[i3];
        this.f861 = iArr3;
        int i5 = 0;
        for (int i6 = 0; i6 < iArr2.length; i6++) {
            if (iArr2[i6] > 0) {
                iArr3[i5] = i6;
                i5++;
            }
        }
        if (i3 <= i) {
            this.f860 = new ArrayList();
            for (int i7 : iArr3) {
                this.f860.add(new Palette.Swatch(m951(i7), iArr2[i7]));
            }
            return;
        }
        this.f860 = m955(i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int m950(int i) {
        int r2 = m954(Color.red(i), 8, 5);
        int r1 = m954(Color.green(i), 8, 5);
        return (r2 << 10) | (r1 << 5) | m954(Color.blue(i), 8, 5);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int m951(int i) {
        return m958(m957(i), m953(i), m956(i));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean m952(int i) {
        int r0 = m951(i);
        ColorUtils.colorToHSL(r0, this.f856);
        return m962(r0, this.f856);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static int m953(int i) {
        return (i >> 5) & 31;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m954(int i, int i2, int i3) {
        return ((1 << i3) - 1) & (i3 > i2 ? i << (i3 - i2) : i >> (i2 - i3));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private List<Palette.Swatch> m955(int i) {
        PriorityQueue priorityQueue = new PriorityQueue(i, f855);
        priorityQueue.offer(new Vbox(0, this.f861.length - 1));
        m960((PriorityQueue<Vbox>) priorityQueue, i);
        return m959((Collection<Vbox>) priorityQueue);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static int m956(int i) {
        return i & 31;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m957(int i) {
        return (i >> 10) & 31;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m958(int i, int i2, int i3) {
        return Color.rgb(m954(i, 5, 8), m954(i2, 5, 8), m954(i3, 5, 8));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<Palette.Swatch> m959(Collection<Vbox> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (Vbox r2 : collection) {
            Palette.Swatch r1 = r2.m968();
            if (!m963(r1)) {
                arrayList.add(r1);
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m960(PriorityQueue<Vbox> priorityQueue, int i) {
        Vbox poll;
        while (priorityQueue.size() < i && (poll = priorityQueue.poll()) != null && poll.m970()) {
            priorityQueue.offer(poll.m969());
            priorityQueue.offer(poll);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m961(int[] iArr, int i, int i2, int i3) {
        switch (i) {
            case -2:
                for (int i4 = i2; i4 <= i3; i4++) {
                    int i5 = iArr[i4];
                    iArr[i4] = (m953(i5) << 10) | (m957(i5) << 5) | m956(i5);
                }
                return;
            case -1:
                for (int i6 = i2; i6 <= i3; i6++) {
                    int i7 = iArr[i6];
                    iArr[i6] = (m956(i7) << 10) | (m953(i7) << 5) | m957(i7);
                }
                return;
            default:
                return;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m962(int i, float[] fArr) {
        if (this.f857 != null && this.f857.length > 0) {
            for (Palette.Filter r2 : this.f857) {
                if (!r2.m989(i, fArr)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m963(Palette.Swatch swatch) {
        return m962(swatch.m995(), swatch.m992());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<Palette.Swatch> m964() {
        return this.f860;
    }
}
