package android.support.v7.graphics.drawable;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;

public class DrawerArrowDrawable extends Drawable {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final float f905 = ((float) Math.toRadians(45.0d));

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f906;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f907;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Path f908 = new Path();

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f909 = 2;

    /* renamed from: ˈ  reason: contains not printable characters */
    private float f910;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f911;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f912 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f913;

    /* renamed from: 连任  reason: contains not printable characters */
    private float f914;

    /* renamed from: 麤  reason: contains not printable characters */
    private float f915;

    /* renamed from: 齉  reason: contains not printable characters */
    private float f916;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f917 = new Paint();

    public DrawerArrowDrawable(Context context) {
        this.f917.setStyle(Paint.Style.STROKE);
        this.f917.setStrokeJoin(Paint.Join.MITER);
        this.f917.setStrokeCap(Paint.Cap.BUTT);
        this.f917.setAntiAlias(true);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes((AttributeSet) null, R.styleable.DrawerArrowToggle, R.attr.drawerArrowStyle, R.style.Base_Widget_AppCompat_DrawerArrowToggle);
        m1019(obtainStyledAttributes.getColor(R.styleable.DrawerArrowToggle_color, 0));
        m1018(obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_thickness, 0.0f));
        m1020(obtainStyledAttributes.getBoolean(R.styleable.DrawerArrowToggle_spinBars, true));
        m1015((float) Math.round(obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_gapBetweenBars, 0.0f)));
        this.f911 = obtainStyledAttributes.getDimensionPixelSize(R.styleable.DrawerArrowToggle_drawableSize, 0);
        this.f915 = (float) Math.round(obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_barLength, 0.0f));
        this.f916 = (float) Math.round(obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_arrowHeadLength, 0.0f));
        this.f914 = obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_arrowShaftLength, 0.0f);
        obtainStyledAttributes.recycle();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static float m1014(float f, float f2, float f3) {
        return ((f2 - f) * f3) + f;
    }

    public void draw(Canvas canvas) {
        boolean z;
        Rect bounds = getBounds();
        switch (this.f909) {
            case 0:
                z = false;
                break;
            case 1:
                z = true;
                break;
            case 3:
                if (DrawableCompat.getLayoutDirection(this) != 0) {
                    z = false;
                    break;
                } else {
                    z = true;
                    break;
                }
            default:
                if (DrawableCompat.getLayoutDirection(this) != 1) {
                    z = false;
                    break;
                } else {
                    z = true;
                    break;
                }
        }
        float r5 = m1014(this.f915, (float) Math.sqrt((double) (this.f916 * this.f916 * 2.0f)), this.f913);
        float r8 = m1014(this.f915, this.f914, this.f913);
        float round = (float) Math.round(m1014(0.0f, this.f910, this.f913));
        float r15 = m1014(0.0f, f905, this.f913);
        float r12 = m1014(z ? 0.0f : -180.0f, z ? 180.0f : 0.0f, this.f913);
        float round2 = (float) Math.round(((double) r5) * Math.cos((double) r15));
        float round3 = (float) Math.round(((double) r5) * Math.sin((double) r15));
        this.f908.rewind();
        float r16 = m1014(this.f906 + this.f917.getStrokeWidth(), -this.f910, this.f913);
        float f = (-r8) / 2.0f;
        this.f908.moveTo(f + round, 0.0f);
        this.f908.rLineTo(r8 - (2.0f * round), 0.0f);
        this.f908.moveTo(f, r16);
        this.f908.rLineTo(round2, round3);
        this.f908.moveTo(f, -r16);
        this.f908.rLineTo(round2, -round3);
        this.f908.close();
        canvas.save();
        float strokeWidth = this.f917.getStrokeWidth();
        canvas.translate((float) bounds.centerX(), ((float) ((((int) ((((float) bounds.height()) - (3.0f * strokeWidth)) - (this.f906 * 2.0f))) / 4) * 2)) + (1.5f * strokeWidth) + this.f906);
        if (this.f907) {
            canvas.rotate(((float) (this.f912 ^ z ? -1 : 1)) * r12);
        } else if (z) {
            canvas.rotate(180.0f);
        }
        canvas.drawPath(this.f908, this.f917);
        canvas.restore();
    }

    public int getIntrinsicHeight() {
        return this.f911;
    }

    public int getIntrinsicWidth() {
        return this.f911;
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int i) {
        if (i != this.f917.getAlpha()) {
            this.f917.setAlpha(i);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f917.setColorFilter(colorFilter);
        invalidateSelf();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1015(float f) {
        if (f != this.f906) {
            this.f906 = f;
            invalidateSelf();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1016(boolean z) {
        if (this.f912 != z) {
            this.f912 = z;
            invalidateSelf();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1017(float f) {
        if (this.f913 != f) {
            this.f913 = f;
            invalidateSelf();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1018(float f) {
        if (this.f917.getStrokeWidth() != f) {
            this.f917.setStrokeWidth(f);
            this.f910 = (float) (((double) (f / 2.0f)) * Math.cos((double) f905));
            invalidateSelf();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1019(int i) {
        if (i != this.f917.getColor()) {
            this.f917.setColor(i);
            invalidateSelf();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1020(boolean z) {
        if (this.f907 != z) {
            this.f907 = z;
            invalidateSelf();
        }
    }
}
