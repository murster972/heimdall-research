package android.support.v7.content.res;

import java.lang.reflect.Array;

final class GrowingArrayUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ boolean f854 = (!GrowingArrayUtils.class.desiredAssertionStatus());

    private GrowingArrayUtils() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m947(int i) {
        if (i <= 4) {
            return 8;
        }
        return i * 2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int[] m948(int[] iArr, int i, int i2) {
        if (f854 || i <= iArr.length) {
            if (i + 1 > iArr.length) {
                int[] iArr2 = new int[m947(i)];
                System.arraycopy(iArr, 0, iArr2, 0, i);
                iArr = iArr2;
            }
            iArr[i] = i2;
            return iArr;
        }
        throw new AssertionError();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T[] m949(T[] tArr, int i, T t) {
        if (f854 || i <= tArr.length) {
            if (i + 1 > tArr.length) {
                T[] tArr2 = (Object[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), m947(i)));
                System.arraycopy(tArr, 0, tArr2, 0, i);
                tArr = tArr2;
            }
            tArr[i] = t;
            return tArr;
        }
        throw new AssertionError();
    }
}
