package android.support.v7.content.res;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import java.util.WeakHashMap;

public final class AppCompatResources {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final WeakHashMap<Context, SparseArray<ColorStateListCacheEntry>> f849 = new WeakHashMap<>(0);

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Object f850 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ThreadLocal<TypedValue> f851 = new ThreadLocal<>();

    private static class ColorStateListCacheEntry {

        /* renamed from: 靐  reason: contains not printable characters */
        final Configuration f852;

        /* renamed from: 龘  reason: contains not printable characters */
        final ColorStateList f853;

        ColorStateListCacheEntry(ColorStateList colorStateList, Configuration configuration) {
            this.f853 = colorStateList;
            this.f852 = configuration;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static boolean m940(Context context, int i) {
        Resources resources = context.getResources();
        TypedValue r1 = m945();
        resources.getValue(i, r1, true);
        return r1.type >= 28 && r1.type <= 31;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Drawable m941(Context context, int i) {
        return AppCompatDrawableManager.get().getDrawable(context, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return null;
     */
    /* renamed from: 麤  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.content.res.ColorStateList m942(android.content.Context r5, int r6) {
        /*
            java.lang.Object r3 = f850
            monitor-enter(r3)
            java.util.WeakHashMap<android.content.Context, android.util.SparseArray<android.support.v7.content.res.AppCompatResources$ColorStateListCacheEntry>> r2 = f849     // Catch:{ all -> 0x0035 }
            java.lang.Object r0 = r2.get(r5)     // Catch:{ all -> 0x0035 }
            android.util.SparseArray r0 = (android.util.SparseArray) r0     // Catch:{ all -> 0x0035 }
            if (r0 == 0) goto L_0x0032
            int r2 = r0.size()     // Catch:{ all -> 0x0035 }
            if (r2 <= 0) goto L_0x0032
            java.lang.Object r1 = r0.get(r6)     // Catch:{ all -> 0x0035 }
            android.support.v7.content.res.AppCompatResources$ColorStateListCacheEntry r1 = (android.support.v7.content.res.AppCompatResources.ColorStateListCacheEntry) r1     // Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0032
            android.content.res.Configuration r2 = r1.f852     // Catch:{ all -> 0x0035 }
            android.content.res.Resources r4 = r5.getResources()     // Catch:{ all -> 0x0035 }
            android.content.res.Configuration r4 = r4.getConfiguration()     // Catch:{ all -> 0x0035 }
            boolean r2 = r2.equals(r4)     // Catch:{ all -> 0x0035 }
            if (r2 == 0) goto L_0x002f
            android.content.res.ColorStateList r2 = r1.f853     // Catch:{ all -> 0x0035 }
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
        L_0x002e:
            return r2
        L_0x002f:
            r0.remove(r6)     // Catch:{ all -> 0x0035 }
        L_0x0032:
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
            r2 = 0
            goto L_0x002e
        L_0x0035:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.content.res.AppCompatResources.m942(android.content.Context, int):android.content.res.ColorStateList");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static ColorStateList m943(Context context, int i) {
        if (m940(context, i)) {
            return null;
        }
        Resources resources = context.getResources();
        try {
            return AppCompatColorStateListInflater.m937(resources, resources.getXml(i), context.getTheme());
        } catch (Exception e) {
            Log.e("AppCompatResources", "Failed to inflate ColorStateList, leaving it to the framework", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ColorStateList m944(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return context.getColorStateList(i);
        }
        ColorStateList r0 = m942(context, i);
        if (r0 != null) {
            return r0;
        }
        ColorStateList r02 = m943(context, i);
        if (r02 == null) {
            return ContextCompat.getColorStateList(context, i);
        }
        m946(context, i, r02);
        return r02;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static TypedValue m945() {
        TypedValue typedValue = f851.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        f851.set(typedValue2);
        return typedValue2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m946(Context context, int i, ColorStateList colorStateList) {
        synchronized (f850) {
            SparseArray sparseArray = f849.get(context);
            if (sparseArray == null) {
                sparseArray = new SparseArray();
                f849.put(context, sparseArray);
            }
            sparseArray.append(i, new ColorStateListCacheEntry(colorStateList, context.getResources().getConfiguration()));
        }
    }
}
