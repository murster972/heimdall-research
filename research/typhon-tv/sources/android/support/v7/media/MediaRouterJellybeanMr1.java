package android.support.v7.media;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.media.MediaRouter;
import android.os.Build;
import android.os.Handler;
import android.support.v7.media.MediaRouterJellybean;
import android.util.Log;
import android.view.Display;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

final class MediaRouterJellybeanMr1 {

    public static final class ActiveScanWorkaround implements Runnable {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Handler f1022;

        /* renamed from: 麤  reason: contains not printable characters */
        private boolean f1023;

        /* renamed from: 齉  reason: contains not printable characters */
        private Method f1024;

        /* renamed from: 龘  reason: contains not printable characters */
        private final DisplayManager f1025;

        public ActiveScanWorkaround(Context context, Handler handler) {
            if (Build.VERSION.SDK_INT != 17) {
                throw new UnsupportedOperationException();
            }
            this.f1025 = (DisplayManager) context.getSystemService("display");
            this.f1022 = handler;
            try {
                this.f1024 = DisplayManager.class.getMethod("scanWifiDisplays", new Class[0]);
            } catch (NoSuchMethodException e) {
            }
        }

        public void run() {
            if (this.f1023) {
                try {
                    this.f1024.invoke(this.f1025, new Object[0]);
                } catch (IllegalAccessException e) {
                    Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays.", e);
                } catch (InvocationTargetException e2) {
                    Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays.", e2);
                }
                this.f1022.postDelayed(this, 15000);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1274(int i) {
            if ((i & 2) != 0) {
                if (this.f1023) {
                    return;
                }
                if (this.f1024 != null) {
                    this.f1023 = true;
                    this.f1022.post(this);
                    return;
                }
                Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays because the DisplayManager.scanWifiDisplays() method is not available on this device.");
            } else if (this.f1023) {
                this.f1023 = false;
                this.f1022.removeCallbacks(this);
            }
        }
    }

    public interface Callback extends MediaRouterJellybean.Callback {
        /* renamed from: 连任  reason: contains not printable characters */
        void m1275(Object obj);
    }

    static class CallbackProxy<T extends Callback> extends MediaRouterJellybean.CallbackProxy<T> {
        public CallbackProxy(T t) {
            super(t);
        }

        public void onRoutePresentationDisplayChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            ((Callback) this.f1018).m1275(routeInfo);
        }
    }

    public static final class IsConnectingWorkaround {

        /* renamed from: 靐  reason: contains not printable characters */
        private int f1026;

        /* renamed from: 龘  reason: contains not printable characters */
        private Method f1027;

        public IsConnectingWorkaround() {
            if (Build.VERSION.SDK_INT != 17) {
                throw new UnsupportedOperationException();
            }
            try {
                this.f1026 = MediaRouter.RouteInfo.class.getField("STATUS_CONNECTING").getInt((Object) null);
                this.f1027 = MediaRouter.RouteInfo.class.getMethod("getStatusCode", new Class[0]);
            } catch (IllegalAccessException | NoSuchFieldException | NoSuchMethodException e) {
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1276(Object obj) {
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) obj;
            if (this.f1027 != null) {
                try {
                    return ((Integer) this.f1027.invoke(routeInfo, new Object[0])).intValue() == this.f1026;
                } catch (IllegalAccessException | InvocationTargetException e) {
                }
            }
            return false;
        }
    }

    public static final class RouteInfo {
        /* renamed from: 靐  reason: contains not printable characters */
        public static Display m1277(Object obj) {
            try {
                return ((MediaRouter.RouteInfo) obj).getPresentationDisplay();
            } catch (NoSuchMethodError e) {
                Log.w("MediaRouterJellybeanMr1", "Cannot get presentation display for the route.", e);
                return null;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static boolean m1278(Object obj) {
            return ((MediaRouter.RouteInfo) obj).isEnabled();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m1273(Callback callback) {
        return new CallbackProxy(callback);
    }
}
