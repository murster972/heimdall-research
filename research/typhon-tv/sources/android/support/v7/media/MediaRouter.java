package android.support.v7.media;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.RestrictTo;
import android.support.v4.app.ActivityManagerCompat;
import android.support.v4.hardware.display.DisplayManagerCompat;
import android.support.v4.media.VolumeProviderCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.InputDeviceCompat;
import android.support.v7.media.MediaRouteProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.RegisteredMediaRouteProviderWatcher;
import android.support.v7.media.RemoteControlClientCompat;
import android.support.v7.media.SystemMediaRouteProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class MediaRouter {

    /* renamed from: 靐  reason: contains not printable characters */
    static GlobalMediaRouter f943;

    /* renamed from: 龘  reason: contains not printable characters */
    static final boolean f944 = Log.isLoggable("MediaRouter", 3);

    /* renamed from: 麤  reason: contains not printable characters */
    final ArrayList<CallbackRecord> f945;

    /* renamed from: 齉  reason: contains not printable characters */
    final Context f946;

    public static abstract class Callback {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1122(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1123(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public void m1124(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1125(MediaRouter mediaRouter, ProviderInfo providerInfo) {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1126(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m1127(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1128(MediaRouter mediaRouter, ProviderInfo providerInfo) {
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1129(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1130(MediaRouter mediaRouter, ProviderInfo providerInfo) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1131(MediaRouter mediaRouter, RouteInfo routeInfo) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1132(MediaRouter mediaRouter, RouteInfo routeInfo, int i) {
            m1124(mediaRouter, routeInfo);
        }
    }

    private static final class CallbackRecord {

        /* renamed from: 靐  reason: contains not printable characters */
        public final Callback f947;

        /* renamed from: 麤  reason: contains not printable characters */
        public int f948;

        /* renamed from: 齉  reason: contains not printable characters */
        public MediaRouteSelector f949 = MediaRouteSelector.f939;

        /* renamed from: 龘  reason: contains not printable characters */
        public final MediaRouter f950;

        public CallbackRecord(MediaRouter mediaRouter, Callback callback) {
            this.f950 = mediaRouter;
            this.f947 = callback;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1133(RouteInfo routeInfo) {
            return (this.f948 & 2) != 0 || routeInfo.m1225(this.f949);
        }
    }

    public static abstract class ControlRequestCallback {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1134(Bundle bundle) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1135(String str, Bundle bundle) {
        }
    }

    private static final class GlobalMediaRouter implements RegisteredMediaRouteProviderWatcher.Callback, SystemMediaRouteProvider.SyncCallback {

        /* renamed from: ʻ  reason: contains not printable characters */
        RouteInfo f951;

        /* renamed from: ʼ  reason: contains not printable characters */
        MediaSessionCompat f952;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final ArrayList<RouteInfo> f953 = new ArrayList<>();

        /* renamed from: ʾ  reason: contains not printable characters */
        private final DisplayManagerCompat f954;

        /* renamed from: ʿ  reason: contains not printable characters */
        private final boolean f955;

        /* renamed from: ˆ  reason: contains not printable characters */
        private MediaRouteDiscoveryRequest f956;

        /* renamed from: ˈ  reason: contains not printable characters */
        private final ProviderCallback f957 = new ProviderCallback();

        /* renamed from: ˉ  reason: contains not printable characters */
        private MediaSessionRecord f958;

        /* renamed from: ˊ  reason: contains not printable characters */
        private RouteInfo f959;

        /* renamed from: ˋ  reason: contains not printable characters */
        private MediaRouteProvider.RouteController f960;

        /* renamed from: ˎ  reason: contains not printable characters */
        private final Map<String, MediaRouteProvider.RouteController> f961 = new HashMap();

        /* renamed from: ˏ  reason: contains not printable characters */
        private MediaSessionCompat f962;

        /* renamed from: ˑ  reason: contains not printable characters */
        private final Map<Pair<String, String>, String> f963 = new HashMap();

        /* renamed from: י  reason: contains not printable characters */
        private MediaSessionCompat.OnActiveChangeListener f964 = new MediaSessionCompat.OnActiveChangeListener() {
            public void onActiveChanged() {
                if (GlobalMediaRouter.this.f952 == null) {
                    return;
                }
                if (GlobalMediaRouter.this.f952.isActive()) {
                    GlobalMediaRouter.this.m1171(GlobalMediaRouter.this.f952.getRemoteControlClient());
                } else {
                    GlobalMediaRouter.this.m1158(GlobalMediaRouter.this.f952.getRemoteControlClient());
                }
            }
        };

        /* renamed from: ٴ  reason: contains not printable characters */
        private final ArrayList<ProviderInfo> f965 = new ArrayList<>();

        /* renamed from: ᐧ  reason: contains not printable characters */
        private final ArrayList<RemoteControlClientRecord> f966 = new ArrayList<>();

        /* renamed from: 连任  reason: contains not printable characters */
        final SystemMediaRouteProvider f967;

        /* renamed from: 靐  reason: contains not printable characters */
        final ArrayList<WeakReference<MediaRouter>> f968 = new ArrayList<>();

        /* renamed from: 麤  reason: contains not printable characters */
        final CallbackHandler f969 = new CallbackHandler();

        /* renamed from: 齉  reason: contains not printable characters */
        final RemoteControlClientCompat.PlaybackInfo f970 = new RemoteControlClientCompat.PlaybackInfo();

        /* renamed from: 龘  reason: contains not printable characters */
        final Context f971;

        /* renamed from: ﹶ  reason: contains not printable characters */
        private RegisteredMediaRouteProviderWatcher f972;

        /* renamed from: ﾞ  reason: contains not printable characters */
        private RouteInfo f973;

        private final class CallbackHandler extends Handler {

            /* renamed from: 靐  reason: contains not printable characters */
            private final ArrayList<CallbackRecord> f975 = new ArrayList<>();

            CallbackHandler() {
            }

            /* renamed from: 靐  reason: contains not printable characters */
            private void m1173(int i, Object obj) {
                switch (i) {
                    case InputDeviceCompat.SOURCE_KEYBOARD:
                        GlobalMediaRouter.this.f967.m1350((RouteInfo) obj);
                        return;
                    case 258:
                        GlobalMediaRouter.this.f967.m1347((RouteInfo) obj);
                        return;
                    case 259:
                        GlobalMediaRouter.this.f967.m1349((RouteInfo) obj);
                        return;
                    case 262:
                        GlobalMediaRouter.this.f967.m1348((RouteInfo) obj);
                        return;
                    default:
                        return;
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            private void m1174(CallbackRecord callbackRecord, int i, Object obj, int i2) {
                MediaRouter mediaRouter = callbackRecord.f950;
                Callback callback = callbackRecord.f947;
                switch (65280 & i) {
                    case 256:
                        RouteInfo routeInfo = (RouteInfo) obj;
                        if (callbackRecord.m1133(routeInfo)) {
                            switch (i) {
                                case InputDeviceCompat.SOURCE_KEYBOARD:
                                    callback.m1131(mediaRouter, routeInfo);
                                    return;
                                case 258:
                                    callback.m1126(mediaRouter, routeInfo);
                                    return;
                                case 259:
                                    callback.m1129(mediaRouter, routeInfo);
                                    return;
                                case 260:
                                    callback.m1122(mediaRouter, routeInfo);
                                    return;
                                case 261:
                                    callback.m1123(mediaRouter, routeInfo);
                                    return;
                                case 262:
                                    callback.m1127(mediaRouter, routeInfo);
                                    return;
                                case 263:
                                    callback.m1132(mediaRouter, routeInfo, i2);
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            return;
                        }
                    case 512:
                        ProviderInfo providerInfo = (ProviderInfo) obj;
                        switch (i) {
                            case InputDeviceCompat.SOURCE_DPAD:
                                callback.m1130(mediaRouter, providerInfo);
                                return;
                            case 514:
                                callback.m1125(mediaRouter, providerInfo);
                                return;
                            case 515:
                                callback.m1128(mediaRouter, providerInfo);
                                return;
                            default:
                                return;
                        }
                    default:
                        return;
                }
            }

            public void handleMessage(Message message) {
                int i = message.what;
                Object obj = message.obj;
                int i2 = message.arg1;
                if (i == 259 && GlobalMediaRouter.this.m1154().m1222().equals(((RouteInfo) obj).m1222())) {
                    GlobalMediaRouter.this.m1150(true);
                }
                m1173(i, obj);
                try {
                    int size = GlobalMediaRouter.this.f968.size();
                    while (true) {
                        size--;
                        if (size < 0) {
                            break;
                        }
                        MediaRouter mediaRouter = (MediaRouter) GlobalMediaRouter.this.f968.get(size).get();
                        if (mediaRouter == null) {
                            GlobalMediaRouter.this.f968.remove(size);
                        } else {
                            this.f975.addAll(mediaRouter.f945);
                        }
                    }
                    int size2 = this.f975.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        m1174(this.f975.get(i3), i, obj, i2);
                    }
                } finally {
                    this.f975.clear();
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m1175(int i, Object obj) {
                obtainMessage(i, obj).sendToTarget();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m1176(int i, Object obj, int i2) {
                Message obtainMessage = obtainMessage(i, obj);
                obtainMessage.arg1 = i2;
                obtainMessage.sendToTarget();
            }
        }

        private final class MediaSessionRecord {

            /* renamed from: 连任  reason: contains not printable characters */
            private VolumeProviderCompat f977;

            /* renamed from: 靐  reason: contains not printable characters */
            private final MediaSessionCompat f978;

            /* renamed from: 麤  reason: contains not printable characters */
            private int f979;

            /* renamed from: 齉  reason: contains not printable characters */
            private int f980;

            public MediaSessionRecord(MediaSessionCompat mediaSessionCompat) {
                this.f978 = mediaSessionCompat;
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public MediaSessionCompat.Token m1177() {
                return this.f978.getSessionToken();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m1178() {
                this.f978.setPlaybackToLocal(GlobalMediaRouter.this.f970.f1076);
                this.f977 = null;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m1179(int i, int i2, int i3) {
                if (this.f977 != null && i == this.f980 && i2 == this.f979) {
                    this.f977.setCurrentVolume(i3);
                    return;
                }
                this.f977 = new VolumeProviderCompat(i, i2, i3) {
                    public void onAdjustVolume(final int i) {
                        GlobalMediaRouter.this.f969.post(new Runnable() {
                            public void run() {
                                if (GlobalMediaRouter.this.f951 != null) {
                                    GlobalMediaRouter.this.f951.m1220(i);
                                }
                            }
                        });
                    }

                    public void onSetVolumeTo(final int i) {
                        GlobalMediaRouter.this.f969.post(new Runnable() {
                            public void run() {
                                if (GlobalMediaRouter.this.f951 != null) {
                                    GlobalMediaRouter.this.f951.m1224(i);
                                }
                            }
                        });
                    }
                };
                this.f978.setPlaybackToRemote(this.f977);
            }
        }

        private final class ProviderCallback extends MediaRouteProvider.Callback {
            ProviderCallback() {
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m1180(MediaRouteProvider mediaRouteProvider, MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
                GlobalMediaRouter.this.m1168(mediaRouteProvider, mediaRouteProviderDescriptor);
            }
        }

        private final class RemoteControlClientRecord implements RemoteControlClientCompat.VolumeCallback {

            /* renamed from: 靐  reason: contains not printable characters */
            private final RemoteControlClientCompat f988;

            /* renamed from: 齉  reason: contains not printable characters */
            private boolean f989;

            public RemoteControlClientRecord(Object obj) {
                this.f988 = RemoteControlClientCompat.m1336(GlobalMediaRouter.this.f971, obj);
                this.f988.m1339((RemoteControlClientCompat.VolumeCallback) this);
                m1183();
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m1181() {
                this.f989 = true;
                this.f988.m1339((RemoteControlClientCompat.VolumeCallback) null);
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m1182(int i) {
                if (!this.f989 && GlobalMediaRouter.this.f951 != null) {
                    GlobalMediaRouter.this.f951.m1220(i);
                }
            }

            /* renamed from: 齉  reason: contains not printable characters */
            public void m1183() {
                this.f988.m1338(GlobalMediaRouter.this.f970);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Object m1184() {
                return this.f988.m1337();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m1185(int i) {
                if (!this.f989 && GlobalMediaRouter.this.f951 != null) {
                    GlobalMediaRouter.this.f951.m1224(i);
                }
            }
        }

        GlobalMediaRouter(Context context) {
            this.f971 = context;
            this.f954 = DisplayManagerCompat.getInstance(context);
            this.f955 = ActivityManagerCompat.isLowRamDevice((ActivityManager) context.getSystemService("activity"));
            this.f967 = SystemMediaRouteProvider.m1345(context, this);
        }

        /* renamed from: ˑ  reason: contains not printable characters */
        private void m1136() {
            if (this.f951 != null) {
                this.f970.f1078 = this.f951.m1208();
                this.f970.f1075 = this.f951.m1209();
                this.f970.f1077 = this.f951.m1207();
                this.f970.f1076 = this.f951.m1202();
                this.f970.f1074 = this.f951.m1205();
                int size = this.f966.size();
                for (int i = 0; i < size; i++) {
                    this.f966.get(i).m1183();
                }
                if (this.f958 == null) {
                    return;
                }
                if (this.f951 == m1161() || this.f951 == m1160()) {
                    this.f958.m1178();
                    return;
                }
                int i2 = 0;
                if (this.f970.f1077 == 1) {
                    i2 = 2;
                }
                this.f958.m1179(i2, this.f970.f1075, this.f970.f1078);
            } else if (this.f958 != null) {
                this.f958.m1178();
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: 靐  reason: contains not printable characters */
        public String m1137(ProviderInfo providerInfo, String str) {
            return this.f963.get(new Pair(providerInfo.m1189().flattenToShortString(), str));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m1138(RouteInfo routeInfo) {
            return routeInfo.m1216() == this.f967 && routeInfo.m1226("android.media.intent.category.LIVE_AUDIO") && !routeInfo.m1226("android.media.intent.category.LIVE_VIDEO");
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m1139(RouteInfo routeInfo, int i) {
            if (MediaRouter.f943 == null || (this.f959 != null && routeInfo.m1215())) {
                StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
                StringBuffer stringBuffer = new StringBuffer();
                for (int i2 = 3; i2 < stackTrace.length; i2++) {
                    StackTraceElement stackTraceElement = stackTrace[i2];
                    stringBuffer.append(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + ":" + stackTraceElement.getLineNumber()).append("  ");
                }
                if (MediaRouter.f943 == null) {
                    Log.w("MediaRouter", "setSelectedRouteInternal is called while sGlobal is null: pkgName=" + this.f971.getPackageName() + ", callers=" + stringBuffer.toString());
                } else {
                    Log.w("MediaRouter", "Default route is selected while a BT route is available: pkgName=" + this.f971.getPackageName() + ", callers=" + stringBuffer.toString());
                }
            }
            if (this.f951 != routeInfo) {
                if (this.f951 != null) {
                    if (MediaRouter.f944) {
                        Log.d("MediaRouter", "Route unselected: " + this.f951 + " reason: " + i);
                    }
                    this.f969.m1176(263, this.f951, i);
                    if (this.f960 != null) {
                        this.f960.m1087(i);
                        this.f960.m1086();
                        this.f960 = null;
                    }
                    if (!this.f961.isEmpty()) {
                        for (MediaRouteProvider.RouteController next : this.f961.values()) {
                            next.m1087(i);
                            next.m1086();
                        }
                        this.f961.clear();
                    }
                }
                this.f951 = routeInfo;
                this.f960 = routeInfo.m1216().m1074(routeInfo.f1014);
                if (this.f960 != null) {
                    this.f960.m1082();
                }
                if (MediaRouter.f944) {
                    Log.d("MediaRouter", "Route selected: " + this.f951);
                }
                this.f969.m1175(262, this.f951);
                if (this.f951 instanceof RouteGroup) {
                    List<RouteInfo> r5 = ((RouteGroup) this.f951).m1194();
                    this.f961.clear();
                    for (RouteInfo next2 : r5) {
                        MediaRouteProvider.RouteController r2 = next2.m1216().m1075(next2.f1014, this.f951.f1014);
                        r2.m1082();
                        this.f961.put(next2.f1014, r2);
                    }
                }
                m1136();
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private int m1140(MediaRouteProvider mediaRouteProvider) {
            int size = this.f965.size();
            for (int i = 0; i < size; i++) {
                if (this.f965.get(i).f994 == mediaRouteProvider) {
                    return i;
                }
            }
            return -1;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private int m1141(Object obj) {
            int size = this.f966.size();
            for (int i = 0; i < size; i++) {
                if (this.f966.get(i).m1184() == obj) {
                    return i;
                }
            }
            return -1;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private int m1142(String str) {
            int size = this.f953.size();
            for (int i = 0; i < size; i++) {
                if (this.f953.get(i).f1013.equals(str)) {
                    return i;
                }
            }
            return -1;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean m1143(RouteInfo routeInfo) {
            return routeInfo.m1216() == this.f967 && routeInfo.f1014.equals("DEFAULT_ROUTE");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m1144(RouteInfo routeInfo, MediaRouteDescriptor mediaRouteDescriptor) {
            int r0 = routeInfo.m1223(mediaRouteDescriptor);
            if (r0 != 0) {
                if ((r0 & 1) != 0) {
                    if (MediaRouter.f944) {
                        Log.d("MediaRouter", "Route changed: " + routeInfo);
                    }
                    this.f969.m1175(259, routeInfo);
                }
                if ((r0 & 2) != 0) {
                    if (MediaRouter.f944) {
                        Log.d("MediaRouter", "Route volume changed: " + routeInfo);
                    }
                    this.f969.m1175(260, routeInfo);
                }
                if ((r0 & 4) != 0) {
                    if (MediaRouter.f944) {
                        Log.d("MediaRouter", "Route presentation display changed: " + routeInfo);
                    }
                    this.f969.m1175(261, routeInfo);
                }
            }
            return r0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m1146(ProviderInfo providerInfo, String str) {
            String flattenToShortString = providerInfo.m1189().flattenToShortString();
            String str2 = flattenToShortString + ":" + str;
            if (m1142(str2) < 0) {
                this.f963.put(new Pair(flattenToShortString, str), str2);
                return str2;
            }
            Log.w("MediaRouter", "Either " + str + " isn't unique in " + flattenToShortString + " or we're trying to assign a unique ID for an already added route");
            int i = 2;
            while (true) {
                String format = String.format(Locale.US, "%s_%d", new Object[]{str2, Integer.valueOf(i)});
                if (m1142(format) < 0) {
                    this.f963.put(new Pair(flattenToShortString, str), format);
                    return format;
                }
                i++;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m1147(MediaSessionRecord mediaSessionRecord) {
            if (this.f958 != null) {
                this.f958.m1178();
            }
            this.f958 = mediaSessionRecord;
            if (mediaSessionRecord != null) {
                m1136();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m1149(ProviderInfo providerInfo, MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
            int i;
            if (providerInfo.m1192(mediaRouteProviderDescriptor)) {
                int i2 = 0;
                boolean z = false;
                if (mediaRouteProviderDescriptor != null) {
                    if (mediaRouteProviderDescriptor.m1090()) {
                        List<MediaRouteDescriptor> r11 = mediaRouteProviderDescriptor.m1091();
                        int size = r11.size();
                        ArrayList<Pair> arrayList = new ArrayList<>();
                        ArrayList<Pair> arrayList2 = new ArrayList<>();
                        int i3 = 0;
                        int i4 = 0;
                        while (i3 < size) {
                            MediaRouteDescriptor mediaRouteDescriptor = r11.get(i3);
                            String r5 = mediaRouteDescriptor.m1043();
                            int r13 = providerInfo.m1190(r5);
                            if (r13 < 0) {
                                String r16 = m1146(providerInfo, r5);
                                boolean z2 = mediaRouteDescriptor.m1040() != null;
                                RouteInfo routeGroup = z2 ? new RouteGroup(providerInfo, r5, r16) : new RouteInfo(providerInfo, r5, r16);
                                i = i4 + 1;
                                providerInfo.f991.add(i4, routeGroup);
                                this.f953.add(routeGroup);
                                if (z2) {
                                    arrayList.add(new Pair(routeGroup, mediaRouteDescriptor));
                                } else {
                                    routeGroup.m1223(mediaRouteDescriptor);
                                    if (MediaRouter.f944) {
                                        Log.d("MediaRouter", "Route added: " + routeGroup);
                                    }
                                    this.f969.m1175(InputDeviceCompat.SOURCE_KEYBOARD, routeGroup);
                                }
                            } else if (r13 < i4) {
                                Log.w("MediaRouter", "Ignoring route descriptor with duplicate id: " + mediaRouteDescriptor);
                                i = i4;
                            } else {
                                RouteInfo routeInfo = (RouteInfo) providerInfo.f991.get(r13);
                                i = i4 + 1;
                                Collections.swap(providerInfo.f991, r13, i4);
                                if (routeInfo instanceof RouteGroup) {
                                    arrayList2.add(new Pair(routeInfo, mediaRouteDescriptor));
                                } else if (m1144(routeInfo, mediaRouteDescriptor) != 0 && routeInfo == this.f951) {
                                    z = true;
                                }
                            }
                            i3++;
                            i4 = i;
                        }
                        for (Pair pair : arrayList) {
                            RouteInfo routeInfo2 = (RouteInfo) pair.first;
                            routeInfo2.m1223((MediaRouteDescriptor) pair.second);
                            if (MediaRouter.f944) {
                                Log.d("MediaRouter", "Route added: " + routeInfo2);
                            }
                            this.f969.m1175(InputDeviceCompat.SOURCE_KEYBOARD, routeInfo2);
                        }
                        for (Pair pair2 : arrayList2) {
                            RouteInfo routeInfo3 = (RouteInfo) pair2.first;
                            if (m1144(routeInfo3, (MediaRouteDescriptor) pair2.second) != 0 && routeInfo3 == this.f951) {
                                z = true;
                            }
                        }
                        i2 = i4;
                    } else {
                        Log.w("MediaRouter", "Ignoring invalid provider descriptor: " + mediaRouteProviderDescriptor);
                    }
                }
                for (int size2 = providerInfo.f991.size() - 1; size2 >= i2; size2--) {
                    RouteInfo routeInfo4 = (RouteInfo) providerInfo.f991.get(size2);
                    routeInfo4.m1223((MediaRouteDescriptor) null);
                    this.f953.remove(routeInfo4);
                }
                m1150(z);
                for (int size3 = providerInfo.f991.size() - 1; size3 >= i2; size3--) {
                    RouteInfo routeInfo5 = (RouteInfo) providerInfo.f991.remove(size3);
                    if (MediaRouter.f944) {
                        Log.d("MediaRouter", "Route removed: " + routeInfo5);
                    }
                    this.f969.m1175(258, routeInfo5);
                }
                if (MediaRouter.f944) {
                    Log.d("MediaRouter", "Provider changed: " + providerInfo);
                }
                this.f969.m1175(515, providerInfo);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1150(boolean z) {
            if (this.f973 != null && !this.f973.m1228()) {
                Log.i("MediaRouter", "Clearing the default route because it is no longer selectable: " + this.f973);
                this.f973 = null;
            }
            if (this.f973 == null && !this.f953.isEmpty()) {
                Iterator<RouteInfo> it2 = this.f953.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    RouteInfo next = it2.next();
                    if (m1143(next) && next.m1228()) {
                        this.f973 = next;
                        Log.i("MediaRouter", "Found default route: " + this.f973);
                        break;
                    }
                }
            }
            if (this.f959 != null && !this.f959.m1228()) {
                Log.i("MediaRouter", "Clearing the bluetooth route because it is no longer selectable: " + this.f959);
                this.f959 = null;
            }
            if (this.f959 == null && !this.f953.isEmpty()) {
                Iterator<RouteInfo> it3 = this.f953.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    RouteInfo next2 = it3.next();
                    if (m1138(next2) && next2.m1228()) {
                        this.f959 = next2;
                        Log.i("MediaRouter", "Found bluetooth route: " + this.f959);
                        break;
                    }
                }
            }
            if (this.f951 == null || !this.f951.m1228()) {
                Log.i("MediaRouter", "Unselecting the current route because it is no longer selectable: " + this.f951);
                m1139(m1152(), 0);
            } else if (z) {
                if (this.f951 instanceof RouteGroup) {
                    List<RouteInfo> r5 = ((RouteGroup) this.f951).m1194();
                    HashSet hashSet = new HashSet();
                    for (RouteInfo r4 : r5) {
                        hashSet.add(r4.f1014);
                    }
                    Iterator<Map.Entry<String, MediaRouteProvider.RouteController>> it4 = this.f961.entrySet().iterator();
                    while (it4.hasNext()) {
                        Map.Entry next3 = it4.next();
                        if (!hashSet.contains(next3.getKey())) {
                            MediaRouteProvider.RouteController routeController = (MediaRouteProvider.RouteController) next3.getValue();
                            routeController.m1084();
                            routeController.m1086();
                            it4.remove();
                        }
                    }
                    for (RouteInfo next4 : r5) {
                        if (!this.f961.containsKey(next4.f1014)) {
                            MediaRouteProvider.RouteController r0 = next4.m1216().m1075(next4.f1014, this.f951.f1014);
                            r0.m1082();
                            this.f961.put(next4.f1014, r0);
                        }
                    }
                }
                m1136();
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1151() {
            boolean z = false;
            boolean z2 = false;
            MediaRouteSelector.Builder builder = new MediaRouteSelector.Builder();
            int size = this.f968.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                MediaRouter mediaRouter = (MediaRouter) this.f968.get(size).get();
                if (mediaRouter == null) {
                    this.f968.remove(size);
                } else {
                    int size2 = mediaRouter.f945.size();
                    for (int i = 0; i < size2; i++) {
                        CallbackRecord callbackRecord = mediaRouter.f945.get(i);
                        builder.m1103(callbackRecord.f949);
                        if ((callbackRecord.f948 & 1) != 0) {
                            z2 = true;
                            z = true;
                        }
                        if ((callbackRecord.f948 & 4) != 0 && !this.f955) {
                            z = true;
                        }
                        if ((callbackRecord.f948 & 8) != 0) {
                            z = true;
                        }
                    }
                }
            }
            MediaRouteSelector r9 = z ? builder.m1106() : MediaRouteSelector.f939;
            if (this.f956 == null || !this.f956.m1065().equals(r9) || this.f956.m1062() != z2) {
                if (!r9.m1099() || z2) {
                    this.f956 = new MediaRouteDiscoveryRequest(r9, z2);
                } else if (this.f956 != null) {
                    this.f956 = null;
                } else {
                    return;
                }
                if (MediaRouter.f944) {
                    Log.d("MediaRouter", "Updated discovery request: " + this.f956);
                }
                if (z && !z2 && this.f955) {
                    Log.i("MediaRouter", "Forcing passive route discovery on a low-RAM device, system performance may be affected.  Please consider using CALLBACK_FLAG_REQUEST_DISCOVERY instead of CALLBACK_FLAG_FORCE_DISCOVERY.");
                }
                int size3 = this.f965.size();
                for (int i2 = 0; i2 < size3; i2++) {
                    this.f965.get(i2).f994.m1076(this.f956);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public RouteInfo m1152() {
            Iterator<RouteInfo> it2 = this.f953.iterator();
            while (it2.hasNext()) {
                RouteInfo next = it2.next();
                if (next != this.f973 && m1138(next) && next.m1228()) {
                    return next;
                }
            }
            return this.f973;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public MediaSessionCompat.Token m1153() {
            if (this.f958 != null) {
                return this.f958.m1177();
            }
            if (this.f962 != null) {
                return this.f962.getSessionToken();
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 连任  reason: contains not printable characters */
        public RouteInfo m1154() {
            if (this.f951 != null) {
                return this.f951;
            }
            throw new IllegalStateException("There is no currently selected route.  The media router has not yet been fully initialized.");
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public List<RouteInfo> m1155() {
            return this.f953;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1156(MediaRouteProvider mediaRouteProvider) {
            int r0 = m1140(mediaRouteProvider);
            if (r0 >= 0) {
                mediaRouteProvider.m1077((MediaRouteProvider.Callback) null);
                mediaRouteProvider.m1076((MediaRouteDiscoveryRequest) null);
                ProviderInfo providerInfo = this.f965.get(r0);
                m1149(providerInfo, (MediaRouteProviderDescriptor) null);
                if (MediaRouter.f944) {
                    Log.d("MediaRouter", "Provider removed: " + providerInfo);
                }
                this.f969.m1175(514, providerInfo);
                this.f965.remove(r0);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1157(RouteInfo routeInfo, int i) {
            if (routeInfo == this.f951 && this.f960 != null) {
                this.f960.m1085(i);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1158(Object obj) {
            int r0 = m1141(obj);
            if (r0 >= 0) {
                this.f966.remove(r0).m1181();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:2:0x000f, code lost:
            r0 = r5.f965.get(r1);
         */
        /* renamed from: 靐  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m1159(java.lang.String r6) {
            /*
                r5 = this;
                android.support.v7.media.MediaRouter$GlobalMediaRouter$CallbackHandler r3 = r5.f969
                r4 = 262(0x106, float:3.67E-43)
                r3.removeMessages(r4)
                android.support.v7.media.SystemMediaRouteProvider r3 = r5.f967
                int r1 = r5.m1140((android.support.v7.media.MediaRouteProvider) r3)
                if (r1 < 0) goto L_0x002a
                java.util.ArrayList<android.support.v7.media.MediaRouter$ProviderInfo> r3 = r5.f965
                java.lang.Object r0 = r3.get(r1)
                android.support.v7.media.MediaRouter$ProviderInfo r0 = (android.support.v7.media.MediaRouter.ProviderInfo) r0
                int r2 = r0.m1190((java.lang.String) r6)
                if (r2 < 0) goto L_0x002a
                java.util.List r3 = r0.f991
                java.lang.Object r3 = r3.get(r2)
                android.support.v7.media.MediaRouter$RouteInfo r3 = (android.support.v7.media.MediaRouter.RouteInfo) r3
                r3.m1212()
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.media.MediaRouter.GlobalMediaRouter.m1159(java.lang.String):void");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public RouteInfo m1160() {
            return this.f959;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public RouteInfo m1161() {
            if (this.f973 != null) {
                return this.f973;
            }
            throw new IllegalStateException("There is no default route.  The media router has not yet been fully initialized.");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m1162(RouteInfo routeInfo, int i) {
            if (!this.f953.contains(routeInfo)) {
                Log.w("MediaRouter", "Ignoring attempt to select removed route: " + routeInfo);
            } else if (!routeInfo.f998) {
                Log.w("MediaRouter", "Ignoring attempt to select disabled route: " + routeInfo);
            } else {
                m1139(routeInfo, i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public RouteInfo m1163(String str) {
            Iterator<RouteInfo> it2 = this.f953.iterator();
            while (it2.hasNext()) {
                RouteInfo next = it2.next();
                if (next.f1013.equals(str)) {
                    return next;
                }
            }
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaRouter m1164(Context context) {
            int size = this.f968.size();
            while (true) {
                size--;
                if (size >= 0) {
                    MediaRouter mediaRouter = (MediaRouter) this.f968.get(size).get();
                    if (mediaRouter == null) {
                        this.f968.remove(size);
                    } else if (mediaRouter.f946 == context) {
                        MediaRouter mediaRouter2 = mediaRouter;
                        return mediaRouter;
                    }
                } else {
                    MediaRouter mediaRouter3 = new MediaRouter(context);
                    this.f968.add(new WeakReference(mediaRouter3));
                    MediaRouter mediaRouter4 = mediaRouter3;
                    return mediaRouter3;
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1165() {
            m1167((MediaRouteProvider) this.f967);
            this.f972 = new RegisteredMediaRouteProviderWatcher(this.f971, this);
            this.f972.m1333();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1166(MediaSessionCompat mediaSessionCompat) {
            this.f962 = mediaSessionCompat;
            if (Build.VERSION.SDK_INT >= 21) {
                m1147(mediaSessionCompat != null ? new MediaSessionRecord(mediaSessionCompat) : null);
            } else if (Build.VERSION.SDK_INT >= 14) {
                if (this.f952 != null) {
                    m1158(this.f952.getRemoteControlClient());
                    this.f952.removeOnActiveChangeListener(this.f964);
                }
                this.f952 = mediaSessionCompat;
                if (mediaSessionCompat != null) {
                    mediaSessionCompat.addOnActiveChangeListener(this.f964);
                    if (mediaSessionCompat.isActive()) {
                        m1171(mediaSessionCompat.getRemoteControlClient());
                    }
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1167(MediaRouteProvider mediaRouteProvider) {
            if (m1140(mediaRouteProvider) < 0) {
                ProviderInfo providerInfo = new ProviderInfo(mediaRouteProvider);
                this.f965.add(providerInfo);
                if (MediaRouter.f944) {
                    Log.d("MediaRouter", "Provider added: " + providerInfo);
                }
                this.f969.m1175(InputDeviceCompat.SOURCE_DPAD, providerInfo);
                m1149(providerInfo, mediaRouteProvider.m1066());
                mediaRouteProvider.m1077((MediaRouteProvider.Callback) this.f957);
                mediaRouteProvider.m1076(this.f956);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1168(MediaRouteProvider mediaRouteProvider, MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
            int r0 = m1140(mediaRouteProvider);
            if (r0 >= 0) {
                m1149(this.f965.get(r0), mediaRouteProviderDescriptor);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1169(RouteInfo routeInfo) {
            m1162(routeInfo, 3);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1170(RouteInfo routeInfo, int i) {
            MediaRouteProvider.RouteController routeController;
            if (routeInfo == this.f951 && this.f960 != null) {
                this.f960.m1083(i);
            } else if (!this.f961.isEmpty() && (routeController = this.f961.get(routeInfo.f1014)) != null) {
                routeController.m1083(i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1171(Object obj) {
            if (m1141(obj) < 0) {
                this.f966.add(new RemoteControlClientRecord(obj));
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1172(MediaRouteSelector mediaRouteSelector, int i) {
            if (mediaRouteSelector.m1099()) {
                return false;
            }
            if ((i & 2) == 0 && this.f955) {
                return true;
            }
            int size = this.f953.size();
            for (int i2 = 0; i2 < size; i2++) {
                RouteInfo routeInfo = this.f953.get(i2);
                if (((i & 1) == 0 || !routeInfo.m1227()) && routeInfo.m1225(mediaRouteSelector)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static final class ProviderInfo {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final List<RouteInfo> f991 = new ArrayList();

        /* renamed from: 麤  reason: contains not printable characters */
        private MediaRouteProviderDescriptor f992;

        /* renamed from: 齉  reason: contains not printable characters */
        private final MediaRouteProvider.ProviderMetadata f993;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final MediaRouteProvider f994;

        ProviderInfo(MediaRouteProvider mediaRouteProvider) {
            this.f994 = mediaRouteProvider;
            this.f993 = mediaRouteProvider.m1072();
        }

        public String toString() {
            return "MediaRouter.RouteProviderInfo{ packageName=" + m1188() + " }";
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m1188() {
            return this.f993.m1081();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public ComponentName m1189() {
            return this.f993.m1080();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m1190(String str) {
            int size = this.f991.size();
            for (int i = 0; i < size; i++) {
                if (this.f991.get(i).f1014.equals(str)) {
                    return i;
                }
            }
            return -1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaRouteProvider m1191() {
            MediaRouter.m1107();
            return this.f994;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1192(MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
            if (this.f992 == mediaRouteProviderDescriptor) {
                return false;
            }
            this.f992 = mediaRouteProviderDescriptor;
            return true;
        }
    }

    @RestrictTo
    public static class RouteGroup extends RouteInfo {

        /* renamed from: 靐  reason: contains not printable characters */
        private List<RouteInfo> f995 = new ArrayList();

        RouteGroup(ProviderInfo providerInfo, String str, String str2) {
            super(providerInfo, str, str2);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(super.toString());
            sb.append('[');
            int size = this.f995.size();
            for (int i = 0; i < size; i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(this.f995.get(i));
            }
            sb.append(']');
            return sb.toString();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m1193(MediaRouteDescriptor mediaRouteDescriptor) {
            int i = 1;
            boolean z = false;
            if (this.f1015 != mediaRouteDescriptor) {
                this.f1015 = mediaRouteDescriptor;
                if (mediaRouteDescriptor != null) {
                    List<String> r3 = mediaRouteDescriptor.m1040();
                    ArrayList arrayList = new ArrayList();
                    z = r3.size() != this.f995.size();
                    for (String r2 : r3) {
                        RouteInfo r1 = MediaRouter.f943.m1163(MediaRouter.f943.m1137(m1219(), r2));
                        if (r1 != null) {
                            arrayList.add(r1);
                            if (!z && !this.f995.contains(r1)) {
                                z = true;
                            }
                        }
                    }
                    if (z) {
                        this.f995 = arrayList;
                    }
                }
            }
            if (!z) {
                i = 0;
            }
            return i | super.m1218(mediaRouteDescriptor);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public List<RouteInfo> m1194() {
            return this.f995;
        }
    }

    public static class RouteInfo {

        /* renamed from: ʻ  reason: contains not printable characters */
        private String f996;

        /* renamed from: ʼ  reason: contains not printable characters */
        private Uri f997;
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean f998;

        /* renamed from: ʾ  reason: contains not printable characters */
        private int f999;

        /* renamed from: ʿ  reason: contains not printable characters */
        private int f1000;

        /* renamed from: ˆ  reason: contains not printable characters */
        private int f1001 = -1;

        /* renamed from: ˈ  reason: contains not printable characters */
        private final ArrayList<IntentFilter> f1002 = new ArrayList<>();

        /* renamed from: ˉ  reason: contains not printable characters */
        private Bundle f1003;

        /* renamed from: ˊ  reason: contains not printable characters */
        private int f1004;

        /* renamed from: ˋ  reason: contains not printable characters */
        private int f1005;

        /* renamed from: ˎ  reason: contains not printable characters */
        private Display f1006;

        /* renamed from: ˏ  reason: contains not printable characters */
        private IntentSender f1007;

        /* renamed from: ˑ  reason: contains not printable characters */
        private boolean f1008;

        /* renamed from: ٴ  reason: contains not printable characters */
        private int f1009;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private boolean f1010;

        /* renamed from: 连任  reason: contains not printable characters */
        private String f1011;

        /* renamed from: 靐  reason: contains not printable characters */
        private final ProviderInfo f1012;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public final String f1013;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final String f1014;

        /* renamed from: 龘  reason: contains not printable characters */
        MediaRouteDescriptor f1015;

        /* renamed from: ﹶ  reason: contains not printable characters */
        private int f1016;

        /* renamed from: ﾞ  reason: contains not printable characters */
        private int f1017;

        RouteInfo(ProviderInfo providerInfo, String str, String str2) {
            this.f1012 = providerInfo;
            this.f1014 = str;
            this.f1013 = str2;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private static boolean m1196(RouteInfo routeInfo) {
            return TextUtils.equals(routeInfo.m1216().m1072().m1081(), AbstractSpiCall.ANDROID_CLIENT_TYPE);
        }

        public String toString() {
            return "MediaRouter.RouteInfo{ uniqueId=" + this.f1013 + ", name=" + this.f1011 + ", description=" + this.f996 + ", iconUri=" + this.f997 + ", enabled=" + this.f998 + ", connecting=" + this.f1008 + ", connectionState=" + this.f1009 + ", canDisconnect=" + this.f1010 + ", playbackType=" + this.f999 + ", playbackStream=" + this.f1000 + ", deviceType=" + this.f1016 + ", volumeHandling=" + this.f1017 + ", volume=" + this.f1004 + ", volumeMax=" + this.f1005 + ", presentationDisplayId=" + this.f1001 + ", extras=" + this.f1003 + ", settingsIntent=" + this.f1007 + ", providerPackageName=" + this.f1012.m1188() + " }";
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Uri m1199() {
            return this.f997;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m1200() {
            return this.f998;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m1201() {
            return this.f1008;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m1202() {
            return this.f1000;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public int m1203() {
            return this.f1016;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public boolean m1204() {
            return this.f1010;
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public int m1205() {
            return this.f999;
        }

        @RestrictTo
        /* renamed from: ˉ  reason: contains not printable characters */
        public int m1206() {
            return this.f1001;
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public int m1207() {
            return this.f1017;
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public int m1208() {
            return this.f1004;
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        public int m1209() {
            return this.f1005;
        }

        /* renamed from: ˏ  reason: contains not printable characters */
        public Bundle m1210() {
            return this.f1003;
        }

        /* renamed from: ˑ  reason: contains not printable characters */
        public int m1211() {
            return this.f1009;
        }

        /* renamed from: י  reason: contains not printable characters */
        public void m1212() {
            MediaRouter.m1107();
            MediaRouter.f943.m1169(this);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ـ  reason: contains not printable characters */
        public String m1213() {
            return this.f1014;
        }

        /* renamed from: ٴ  reason: contains not printable characters */
        public boolean m1214() {
            MediaRouter.m1107();
            return MediaRouter.f943.m1154() == this;
        }

        /* renamed from: ᐧ  reason: contains not printable characters */
        public boolean m1215() {
            MediaRouter.m1107();
            return MediaRouter.f943.m1161() == this;
        }

        @RestrictTo
        /* renamed from: ᴵ  reason: contains not printable characters */
        public MediaRouteProvider m1216() {
            return this.f1012.m1191();
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public String m1217() {
            return this.f996;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m1218(MediaRouteDescriptor mediaRouteDescriptor) {
            int i = 0;
            this.f1015 = mediaRouteDescriptor;
            if (mediaRouteDescriptor == null) {
                return 0;
            }
            if (!MediaRouter.m1110(this.f1011, mediaRouteDescriptor.m1042())) {
                this.f1011 = mediaRouteDescriptor.m1042();
                i = 0 | 1;
            }
            if (!MediaRouter.m1110(this.f996, mediaRouteDescriptor.m1041())) {
                this.f996 = mediaRouteDescriptor.m1041();
                i |= 1;
            }
            if (!MediaRouter.m1110(this.f997, mediaRouteDescriptor.m1039())) {
                this.f997 = mediaRouteDescriptor.m1039();
                i |= 1;
            }
            if (this.f998 != mediaRouteDescriptor.m1022()) {
                this.f998 = mediaRouteDescriptor.m1022();
                i |= 1;
            }
            if (this.f1008 != mediaRouteDescriptor.m1023()) {
                this.f1008 = mediaRouteDescriptor.m1023();
                i |= 1;
            }
            if (this.f1009 != mediaRouteDescriptor.m1024()) {
                this.f1009 = mediaRouteDescriptor.m1024();
                i |= 1;
            }
            if (!this.f1002.equals(mediaRouteDescriptor.m1038())) {
                this.f1002.clear();
                this.f1002.addAll(mediaRouteDescriptor.m1038());
                i |= 1;
            }
            if (this.f999 != mediaRouteDescriptor.m1025()) {
                this.f999 = mediaRouteDescriptor.m1025();
                i |= 1;
            }
            if (this.f1000 != mediaRouteDescriptor.m1026()) {
                this.f1000 = mediaRouteDescriptor.m1026();
                i |= 1;
            }
            if (this.f1016 != mediaRouteDescriptor.m1044()) {
                this.f1016 = mediaRouteDescriptor.m1044();
                i |= 1;
            }
            if (this.f1017 != mediaRouteDescriptor.m1031()) {
                this.f1017 = mediaRouteDescriptor.m1031();
                i |= 3;
            }
            if (this.f1004 != mediaRouteDescriptor.m1045()) {
                this.f1004 = mediaRouteDescriptor.m1045();
                i |= 3;
            }
            if (this.f1005 != mediaRouteDescriptor.m1030()) {
                this.f1005 = mediaRouteDescriptor.m1030();
                i |= 3;
            }
            if (this.f1001 != mediaRouteDescriptor.m1032()) {
                this.f1001 = mediaRouteDescriptor.m1032();
                this.f1006 = null;
                i |= 5;
            }
            if (!MediaRouter.m1110(this.f1003, mediaRouteDescriptor.m1027())) {
                this.f1003 = mediaRouteDescriptor.m1027();
                i |= 1;
            }
            if (!MediaRouter.m1110(this.f1007, mediaRouteDescriptor.m1037())) {
                this.f1007 = mediaRouteDescriptor.m1037();
                i |= 1;
            }
            if (this.f1010 == mediaRouteDescriptor.m1034()) {
                return i;
            }
            this.f1010 = mediaRouteDescriptor.m1034();
            return i | 5;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public ProviderInfo m1219() {
            return this.f1012;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1220(int i) {
            MediaRouter.m1107();
            if (i != 0) {
                MediaRouter.f943.m1157(this, i);
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public String m1221() {
            return this.f1011;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public String m1222() {
            return this.f1013;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m1223(MediaRouteDescriptor mediaRouteDescriptor) {
            if (this.f1015 != mediaRouteDescriptor) {
                return m1218(mediaRouteDescriptor);
            }
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1224(int i) {
            MediaRouter.m1107();
            MediaRouter.f943.m1170(this, Math.min(this.f1005, Math.max(0, i)));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1225(MediaRouteSelector mediaRouteSelector) {
            if (mediaRouteSelector == null) {
                throw new IllegalArgumentException("selector must not be null");
            }
            MediaRouter.m1107();
            return mediaRouteSelector.m1102((List<IntentFilter>) this.f1002);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1226(String str) {
            if (str == null) {
                throw new IllegalArgumentException("category must not be null");
            }
            MediaRouter.m1107();
            int size = this.f1002.size();
            for (int i = 0; i < size; i++) {
                if (this.f1002.get(i).hasCategory(str)) {
                    return true;
                }
            }
            return false;
        }

        @RestrictTo
        /* renamed from: ﹶ  reason: contains not printable characters */
        public boolean m1227() {
            if (m1215() || this.f1016 == 3) {
                return true;
            }
            return m1196(this) && m1226("android.media.intent.category.LIVE_AUDIO") && !m1226("android.media.intent.category.LIVE_VIDEO");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﾞ  reason: contains not printable characters */
        public boolean m1228() {
            return this.f1015 != null && this.f998;
        }
    }

    private MediaRouter(Context context) {
        this.f945 = new ArrayList<>();
        this.f946 = context;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    static void m1107() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("The media router service must only be accessed on the application's main thread.");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m1108(Callback callback) {
        int size = this.f945.size();
        for (int i = 0; i < size; i++) {
            if (this.f945.get(i).f947 == callback) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaRouter m1109(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("context must not be null");
        }
        m1107();
        if (f943 == null) {
            f943 = new GlobalMediaRouter(context.getApplicationContext());
            f943.m1165();
        }
        return f943.m1164(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> boolean m1110(T t, T t2) {
        return t == t2 || !(t == null || t2 == null || !t.equals(t2));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public RouteInfo m1111() {
        m1107();
        return f943.m1161();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public MediaSessionCompat.Token m1112() {
        return f943.m1153();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public RouteInfo m1113() {
        m1107();
        return f943.m1154();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<RouteInfo> m1114() {
        m1107();
        return f943.m1155();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1115(int i) {
        if (i < 0 || i > 3) {
            throw new IllegalArgumentException("Unsupported reason to unselect route");
        }
        m1107();
        RouteInfo r0 = f943.m1152();
        if (f943.m1154() != r0) {
            f943.m1162(r0, i);
        } else {
            f943.m1162(f943.m1161(), i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1116(MediaSessionCompat mediaSessionCompat) {
        if (f944) {
            Log.d("MediaRouter", "addMediaSessionCompat: " + mediaSessionCompat);
        }
        f943.m1166(mediaSessionCompat);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1117(MediaRouteSelector mediaRouteSelector, Callback callback) {
        m1118(mediaRouteSelector, callback, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1118(MediaRouteSelector mediaRouteSelector, Callback callback, int i) {
        CallbackRecord callbackRecord;
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        } else if (callback == null) {
            throw new IllegalArgumentException("callback must not be null");
        } else {
            m1107();
            if (f944) {
                Log.d("MediaRouter", "addCallback: selector=" + mediaRouteSelector + ", callback=" + callback + ", flags=" + Integer.toHexString(i));
            }
            int r0 = m1108(callback);
            if (r0 < 0) {
                callbackRecord = new CallbackRecord(this, callback);
                this.f945.add(callbackRecord);
            } else {
                callbackRecord = this.f945.get(r0);
            }
            boolean z = false;
            if (((callbackRecord.f948 ^ -1) & i) != 0) {
                callbackRecord.f948 |= i;
                z = true;
            }
            if (!callbackRecord.f949.m1101(mediaRouteSelector)) {
                callbackRecord.f949 = new MediaRouteSelector.Builder(callbackRecord.f949).m1103(mediaRouteSelector).m1106();
                z = true;
            }
            if (z) {
                f943.m1151();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1119(Callback callback) {
        if (callback == null) {
            throw new IllegalArgumentException("callback must not be null");
        }
        m1107();
        if (f944) {
            Log.d("MediaRouter", "removeCallback: callback=" + callback);
        }
        int r0 = m1108(callback);
        if (r0 >= 0) {
            this.f945.remove(r0);
            f943.m1151();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1120(RouteInfo routeInfo) {
        if (routeInfo == null) {
            throw new IllegalArgumentException("route must not be null");
        }
        m1107();
        if (f944) {
            Log.d("MediaRouter", "selectRoute: " + routeInfo);
        }
        f943.m1169(routeInfo);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1121(MediaRouteSelector mediaRouteSelector, int i) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        }
        m1107();
        return f943.m1172(mediaRouteSelector, i);
    }
}
