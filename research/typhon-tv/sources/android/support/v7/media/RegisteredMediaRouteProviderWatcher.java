package android.support.v7.media;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Handler;
import java.util.ArrayList;
import java.util.Collections;

final class RegisteredMediaRouteProviderWatcher {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f1056;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final BroadcastReceiver f1057 = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            RegisteredMediaRouteProviderWatcher.this.m1332();
        }
    };

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Runnable f1058 = new Runnable() {
        public void run() {
            RegisteredMediaRouteProviderWatcher.this.m1332();
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private final ArrayList<RegisteredMediaRouteProvider> f1059 = new ArrayList<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Callback f1060;

    /* renamed from: 麤  reason: contains not printable characters */
    private final PackageManager f1061;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Handler f1062;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f1063;

    public interface Callback {
        /* renamed from: 靐  reason: contains not printable characters */
        void m1334(MediaRouteProvider mediaRouteProvider);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1335(MediaRouteProvider mediaRouteProvider);
    }

    public RegisteredMediaRouteProviderWatcher(Context context, Callback callback) {
        this.f1063 = context;
        this.f1060 = callback;
        this.f1062 = new Handler();
        this.f1061 = context.getPackageManager();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m1331(String str, String str2) {
        int size = this.f1059.size();
        for (int i = 0; i < size; i++) {
            if (this.f1059.get(i).m1297(str, str2)) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1332() {
        if (this.f1056) {
            int i = 0;
            for (ResolveInfo resolveInfo : this.f1061.queryIntentServices(new Intent("android.media.MediaRouteProviderService"), 0)) {
                ServiceInfo serviceInfo = resolveInfo.serviceInfo;
                if (serviceInfo != null) {
                    int r5 = m1331(serviceInfo.packageName, serviceInfo.name);
                    if (r5 < 0) {
                        RegisteredMediaRouteProvider registeredMediaRouteProvider = new RegisteredMediaRouteProvider(this.f1063, new ComponentName(serviceInfo.packageName, serviceInfo.name));
                        registeredMediaRouteProvider.m1292();
                        this.f1059.add(i, registeredMediaRouteProvider);
                        this.f1060.m1335(registeredMediaRouteProvider);
                        i++;
                    } else if (r5 >= i) {
                        RegisteredMediaRouteProvider registeredMediaRouteProvider2 = this.f1059.get(r5);
                        registeredMediaRouteProvider2.m1292();
                        registeredMediaRouteProvider2.m1294();
                        Collections.swap(this.f1059, r5, i);
                        i++;
                    }
                }
            }
            if (i < this.f1059.size()) {
                for (int size = this.f1059.size() - 1; size >= i; size--) {
                    RegisteredMediaRouteProvider registeredMediaRouteProvider3 = this.f1059.get(size);
                    this.f1060.m1334(registeredMediaRouteProvider3);
                    this.f1059.remove(registeredMediaRouteProvider3);
                    registeredMediaRouteProvider3.m1293();
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1333() {
        if (!this.f1056) {
            this.f1056 = true;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
            intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
            intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
            intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
            intentFilter.addAction("android.intent.action.PACKAGE_RESTARTED");
            intentFilter.addDataScheme("package");
            this.f1063.registerReceiver(this.f1057, intentFilter, (String) null, this.f1062);
            this.f1062.post(this.f1058);
        }
    }
}
