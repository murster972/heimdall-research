package android.support.v7.media;

import android.content.IntentFilter;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.RestrictTo;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class MediaRouteDescriptor {

    /* renamed from: 靐  reason: contains not printable characters */
    List<IntentFilter> f918;

    /* renamed from: 龘  reason: contains not printable characters */
    final Bundle f919;

    public static final class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private ArrayList<String> f920;

        /* renamed from: 齉  reason: contains not printable characters */
        private ArrayList<IntentFilter> f921;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Bundle f922;

        public Builder(MediaRouteDescriptor mediaRouteDescriptor) {
            if (mediaRouteDescriptor == null) {
                throw new IllegalArgumentException("descriptor must not be null");
            }
            this.f922 = new Bundle(mediaRouteDescriptor.f919);
            mediaRouteDescriptor.m1028();
            if (!mediaRouteDescriptor.f918.isEmpty()) {
                this.f921 = new ArrayList<>(mediaRouteDescriptor.f918);
            }
        }

        public Builder(String str, String str2) {
            this.f922 = new Bundle();
            m1057(str);
            m1050(str2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Builder m1046(int i) {
            this.f922.putInt("volumeHandling", i);
            return this;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Builder m1047(int i) {
            this.f922.putInt("presentationDisplayId", i);
            return this;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public Builder m1048(int i) {
            this.f922.putInt("volumeMax", i);
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m1049(int i) {
            this.f922.putInt("playbackStream", i);
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m1050(String str) {
            this.f922.putString("name", str);
            return this;
        }

        @Deprecated
        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m1051(boolean z) {
            this.f922.putBoolean("connecting", z);
            return this;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Builder m1052(int i) {
            this.f922.putInt("volume", i);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m1053(int i) {
            this.f922.putInt("deviceType", i);
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m1054(String str) {
            this.f922.putString(NotificationCompat.CATEGORY_STATUS, str);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m1055(int i) {
            this.f922.putInt("playbackType", i);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m1056(IntentFilter intentFilter) {
            if (intentFilter == null) {
                throw new IllegalArgumentException("filter must not be null");
            }
            if (this.f921 == null) {
                this.f921 = new ArrayList<>();
            }
            if (!this.f921.contains(intentFilter)) {
                this.f921.add(intentFilter);
            }
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m1057(String str) {
            this.f922.putString("id", str);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m1058(Collection<IntentFilter> collection) {
            if (collection == null) {
                throw new IllegalArgumentException("filters must not be null");
            }
            if (!collection.isEmpty()) {
                for (IntentFilter r0 : collection) {
                    m1056(r0);
                }
            }
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m1059(boolean z) {
            this.f922.putBoolean("enabled", z);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaRouteDescriptor m1060() {
            if (this.f921 != null) {
                this.f922.putParcelableArrayList("controlFilters", this.f921);
            }
            if (this.f920 != null) {
                this.f922.putStringArrayList("groupMemberIds", this.f920);
            }
            return new MediaRouteDescriptor(this.f922, this.f921);
        }
    }

    MediaRouteDescriptor(Bundle bundle, List<IntentFilter> list) {
        this.f919 = bundle;
        this.f918 = list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaRouteDescriptor m1021(Bundle bundle) {
        if (bundle != null) {
            return new MediaRouteDescriptor(bundle, (List<IntentFilter>) null);
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediaRouteDescriptor{ ");
        sb.append("id=").append(m1043());
        sb.append(", groupMemberIds=").append(m1040());
        sb.append(", name=").append(m1042());
        sb.append(", description=").append(m1041());
        sb.append(", iconUri=").append(m1039());
        sb.append(", isEnabled=").append(m1022());
        sb.append(", isConnecting=").append(m1023());
        sb.append(", connectionState=").append(m1024());
        sb.append(", controlFilters=").append(Arrays.toString(m1038().toArray()));
        sb.append(", playbackType=").append(m1025());
        sb.append(", playbackStream=").append(m1026());
        sb.append(", deviceType=").append(m1044());
        sb.append(", volume=").append(m1045());
        sb.append(", volumeMax=").append(m1030());
        sb.append(", volumeHandling=").append(m1031());
        sb.append(", presentationDisplayId=").append(m1032());
        sb.append(", extras=").append(m1027());
        sb.append(", isValid=").append(m1035());
        sb.append(", minClientVersion=").append(m1029());
        sb.append(", maxClientVersion=").append(m1033());
        sb.append(" }");
        return sb.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m1022() {
        return this.f919.getBoolean("enabled", true);
    }

    @Deprecated
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m1023() {
        return this.f919.getBoolean("connecting", false);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m1024() {
        return this.f919.getInt("connectionState", 0);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m1025() {
        return this.f919.getInt("playbackType", 1);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m1026() {
        return this.f919.getInt("playbackStream", -1);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Bundle m1027() {
        return this.f919.getBundle("extras");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m1028() {
        if (this.f918 == null) {
            this.f918 = this.f919.getParcelableArrayList("controlFilters");
            if (this.f918 == null) {
                this.f918 = Collections.emptyList();
            }
        }
    }

    @RestrictTo
    /* renamed from: ˉ  reason: contains not printable characters */
    public int m1029() {
        return this.f919.getInt("minClientVersion", 1);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public int m1030() {
        return this.f919.getInt("volumeMax");
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public int m1031() {
        return this.f919.getInt("volumeHandling", 0);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m1032() {
        return this.f919.getInt("presentationDisplayId", -1);
    }

    @RestrictTo
    /* renamed from: ˏ  reason: contains not printable characters */
    public int m1033() {
        return this.f919.getInt("maxClientVersion", MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m1034() {
        return this.f919.getBoolean("canDisconnect", false);
    }

    /* renamed from: י  reason: contains not printable characters */
    public boolean m1035() {
        m1028();
        return !TextUtils.isEmpty(m1043()) && !TextUtils.isEmpty(m1042()) && !this.f918.contains((Object) null);
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public Bundle m1036() {
        return this.f919;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public IntentSender m1037() {
        return (IntentSender) this.f919.getParcelable("settingsIntent");
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public List<IntentFilter> m1038() {
        m1028();
        return this.f918;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Uri m1039() {
        String string = this.f919.getString("iconUri");
        if (string == null) {
            return null;
        }
        return Uri.parse(string);
    }

    @RestrictTo
    /* renamed from: 靐  reason: contains not printable characters */
    public List<String> m1040() {
        return this.f919.getStringArrayList("groupMemberIds");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m1041() {
        return this.f919.getString(NotificationCompat.CATEGORY_STATUS);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m1042() {
        return this.f919.getString("name");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m1043() {
        return this.f919.getString("id");
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public int m1044() {
        return this.f919.getInt("deviceType");
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public int m1045() {
        return this.f919.getInt("volume");
    }
}
