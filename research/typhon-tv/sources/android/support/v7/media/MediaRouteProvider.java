package android.support.v7.media;

import android.content.ComponentName;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RestrictTo;
import android.support.v4.util.ObjectsCompat;

public abstract class MediaRouteProvider {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f925;

    /* renamed from: ʼ  reason: contains not printable characters */
    private MediaRouteProviderDescriptor f926;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f927;

    /* renamed from: 连任  reason: contains not printable characters */
    private MediaRouteDiscoveryRequest f928;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ProviderMetadata f929;

    /* renamed from: 麤  reason: contains not printable characters */
    private Callback f930;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ProviderHandler f931 = new ProviderHandler();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f932;

    public static abstract class Callback {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1079(MediaRouteProvider mediaRouteProvider, MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
        }
    }

    private final class ProviderHandler extends Handler {
        ProviderHandler() {
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    MediaRouteProvider.this.m1067();
                    return;
                case 2:
                    MediaRouteProvider.this.m1068();
                    return;
                default:
                    return;
            }
        }
    }

    public static final class ProviderMetadata {

        /* renamed from: 龘  reason: contains not printable characters */
        private final ComponentName f934;

        ProviderMetadata(ComponentName componentName) {
            if (componentName == null) {
                throw new IllegalArgumentException("componentName must not be null");
            }
            this.f934 = componentName;
        }

        public String toString() {
            return "ProviderMetadata{ componentName=" + this.f934.flattenToShortString() + " }";
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public ComponentName m1080() {
            return this.f934;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m1081() {
            return this.f934.getPackageName();
        }
    }

    public static abstract class RouteController {
        /* renamed from: 靐  reason: contains not printable characters */
        public void m1082() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1083(int i) {
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1084() {
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1085(int i) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1086() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1087(int i) {
            m1084();
        }
    }

    MediaRouteProvider(Context context, ProviderMetadata providerMetadata) {
        if (context == null) {
            throw new IllegalArgumentException("context must not be null");
        }
        this.f932 = context;
        if (providerMetadata == null) {
            this.f929 = new ProviderMetadata(new ComponentName(context, getClass()));
        } else {
            this.f929 = providerMetadata;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final MediaRouteProviderDescriptor m1066() {
        return this.f926;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m1067() {
        this.f927 = false;
        if (this.f930 != null) {
            this.f930.m1079(this, this.f926);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m1068() {
        this.f925 = false;
        m1070(this.f928);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Handler m1069() {
        return this.f931;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1070(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final MediaRouteDiscoveryRequest m1071() {
        return this.f928;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final ProviderMetadata m1072() {
        return this.f929;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Context m1073() {
        return this.f932;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public RouteController m1074(String str) {
        if (str != null) {
            return null;
        }
        throw new IllegalArgumentException("routeId cannot be null");
    }

    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public RouteController m1075(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("routeId cannot be null");
        } else if (str2 != null) {
            return m1074(str);
        } else {
            throw new IllegalArgumentException("routeGroupId cannot be null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m1076(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
        MediaRouter.m1107();
        if (!ObjectsCompat.equals(this.f928, mediaRouteDiscoveryRequest)) {
            this.f928 = mediaRouteDiscoveryRequest;
            if (!this.f925) {
                this.f925 = true;
                this.f931.sendEmptyMessage(2);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m1077(Callback callback) {
        MediaRouter.m1107();
        this.f930 = callback;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m1078(MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
        MediaRouter.m1107();
        if (this.f926 != mediaRouteProviderDescriptor) {
            this.f926 = mediaRouteProviderDescriptor;
            if (!this.f927) {
                this.f927 = true;
                this.f931.sendEmptyMessage(1);
            }
        }
    }
}
