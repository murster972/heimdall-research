package android.support.v7.media;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class MediaRouteProviderDescriptor {

    /* renamed from: 靐  reason: contains not printable characters */
    private List<MediaRouteDescriptor> f935;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Bundle f936;

    public static final class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private ArrayList<MediaRouteDescriptor> f937;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Bundle f938 = new Bundle();

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m1092(MediaRouteDescriptor mediaRouteDescriptor) {
            if (mediaRouteDescriptor == null) {
                throw new IllegalArgumentException("route must not be null");
            }
            if (this.f937 == null) {
                this.f937 = new ArrayList<>();
            } else if (this.f937.contains(mediaRouteDescriptor)) {
                throw new IllegalArgumentException("route descriptor already added");
            }
            this.f937.add(mediaRouteDescriptor);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaRouteProviderDescriptor m1093() {
            if (this.f937 != null) {
                int size = this.f937.size();
                ArrayList arrayList = new ArrayList(size);
                for (int i = 0; i < size; i++) {
                    arrayList.add(this.f937.get(i).m1036());
                }
                this.f938.putParcelableArrayList("routes", arrayList);
            }
            return new MediaRouteProviderDescriptor(this.f938, this.f937);
        }
    }

    private MediaRouteProviderDescriptor(Bundle bundle, List<MediaRouteDescriptor> list) {
        this.f936 = bundle;
        this.f935 = list;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m1088() {
        if (this.f935 == null) {
            ArrayList parcelableArrayList = this.f936.getParcelableArrayList("routes");
            if (parcelableArrayList == null || parcelableArrayList.isEmpty()) {
                this.f935 = Collections.emptyList();
                return;
            }
            int size = parcelableArrayList.size();
            this.f935 = new ArrayList(size);
            for (int i = 0; i < size; i++) {
                this.f935.add(MediaRouteDescriptor.m1021((Bundle) parcelableArrayList.get(i)));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaRouteProviderDescriptor m1089(Bundle bundle) {
        if (bundle != null) {
            return new MediaRouteProviderDescriptor(bundle, (List<MediaRouteDescriptor>) null);
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediaRouteProviderDescriptor{ ");
        sb.append("routes=").append(Arrays.toString(m1091().toArray()));
        sb.append(", isValid=").append(m1090());
        sb.append(" }");
        return sb.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m1090() {
        m1088();
        int size = this.f935.size();
        for (int i = 0; i < size; i++) {
            MediaRouteDescriptor mediaRouteDescriptor = this.f935.get(i);
            if (mediaRouteDescriptor == null || !mediaRouteDescriptor.m1035()) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<MediaRouteDescriptor> m1091() {
        m1088();
        return this.f935;
    }
}
