package android.support.v7.media;

import android.os.Bundle;

public final class MediaRouteDiscoveryRequest {

    /* renamed from: 靐  reason: contains not printable characters */
    private MediaRouteSelector f923;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Bundle f924;

    public MediaRouteDiscoveryRequest(MediaRouteSelector mediaRouteSelector, boolean z) {
        if (mediaRouteSelector == null) {
            throw new IllegalArgumentException("selector must not be null");
        }
        this.f924 = new Bundle();
        this.f923 = mediaRouteSelector;
        this.f924.putBundle("selector", mediaRouteSelector.m1096());
        this.f924.putBoolean("activeScan", z);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m1061() {
        if (this.f923 == null) {
            this.f923 = MediaRouteSelector.m1095(this.f924.getBundle("selector"));
            if (this.f923 == null) {
                this.f923 = MediaRouteSelector.f939;
            }
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof MediaRouteDiscoveryRequest)) {
            return false;
        }
        MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest = (MediaRouteDiscoveryRequest) obj;
        return m1065().equals(mediaRouteDiscoveryRequest.m1065()) && m1062() == mediaRouteDiscoveryRequest.m1062();
    }

    public int hashCode() {
        return (m1062() ? 1 : 0) ^ m1065().hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DiscoveryRequest{ selector=").append(m1065());
        sb.append(", activeScan=").append(m1062());
        sb.append(", isValid=").append(m1064());
        sb.append(" }");
        return sb.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m1062() {
        return this.f924.getBoolean("activeScan");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Bundle m1063() {
        return this.f924;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m1064() {
        m1061();
        return this.f923.m1098();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaRouteSelector m1065() {
        m1061();
        return this.f923;
    }
}
