package android.support.v7.media;

import android.os.Messenger;

abstract class MediaRouteProviderProtocol {
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m1094(Messenger messenger) {
        if (messenger == null) {
            return false;
        }
        try {
            return messenger.getBinder() != null;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
