package android.support.v7.media;

import android.media.MediaRouter;

final class MediaRouterJellybeanMr2 {

    public static final class RouteInfo {
        /* renamed from: 靐  reason: contains not printable characters */
        public static boolean m1281(Object obj) {
            return ((MediaRouter.RouteInfo) obj).isConnecting();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static CharSequence m1282(Object obj) {
            return ((MediaRouter.RouteInfo) obj).getDescription();
        }
    }

    public static final class UserRouteInfo {
        /* renamed from: 龘  reason: contains not printable characters */
        public static void m1283(Object obj, CharSequence charSequence) {
            ((MediaRouter.UserRouteInfo) obj).setDescription(charSequence);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m1279(Object obj) {
        return ((MediaRouter) obj).getDefaultRoute();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m1280(Object obj, int i, Object obj2, int i2) {
        ((MediaRouter) obj).addCallback(i, (MediaRouter.Callback) obj2, i2);
    }
}
