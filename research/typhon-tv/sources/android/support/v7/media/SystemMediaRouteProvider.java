package android.support.v7.media;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v7.media.MediaRouteDescriptor;
import android.support.v7.media.MediaRouteProvider;
import android.support.v7.media.MediaRouteProviderDescriptor;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouterApi24;
import android.support.v7.media.MediaRouterJellybean;
import android.support.v7.media.MediaRouterJellybeanMr1;
import android.support.v7.media.MediaRouterJellybeanMr2;
import android.support.v7.mediarouter.R;
import android.view.Display;
import com.google.android.exoplayer2.util.MimeTypes;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

abstract class SystemMediaRouteProvider extends MediaRouteProvider {

    private static class Api24Impl extends JellybeanMr2Impl {
        public Api24Impl(Context context, SyncCallback syncCallback) {
            super(context, syncCallback);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1351(JellybeanImpl.SystemRouteRecord systemRouteRecord, MediaRouteDescriptor.Builder builder) {
            super.m1396(systemRouteRecord, builder);
            builder.m1053(MediaRouterApi24.RouteInfo.m1229(systemRouteRecord.f1096));
        }
    }

    static class JellybeanImpl extends SystemMediaRouteProvider implements MediaRouterJellybean.Callback, MediaRouterJellybean.VolumeCallback {

        /* renamed from: ٴ  reason: contains not printable characters */
        private static final ArrayList<IntentFilter> f1079 = new ArrayList<>();

        /* renamed from: ᐧ  reason: contains not printable characters */
        private static final ArrayList<IntentFilter> f1080 = new ArrayList<>();

        /* renamed from: ʻ  reason: contains not printable characters */
        protected boolean f1081;

        /* renamed from: ʼ  reason: contains not printable characters */
        protected boolean f1082;

        /* renamed from: ʽ  reason: contains not printable characters */
        protected final ArrayList<SystemRouteRecord> f1083 = new ArrayList<>();

        /* renamed from: ʾ  reason: contains not printable characters */
        private MediaRouterJellybean.SelectRouteWorkaround f1084;

        /* renamed from: ʿ  reason: contains not printable characters */
        private MediaRouterJellybean.GetDefaultRouteWorkaround f1085;

        /* renamed from: ˈ  reason: contains not printable characters */
        private final SyncCallback f1086;

        /* renamed from: ˑ  reason: contains not printable characters */
        protected final ArrayList<UserRouteRecord> f1087 = new ArrayList<>();

        /* renamed from: 连任  reason: contains not printable characters */
        protected int f1088;

        /* renamed from: 靐  reason: contains not printable characters */
        protected final Object f1089;

        /* renamed from: 麤  reason: contains not printable characters */
        protected final Object f1090;

        /* renamed from: 齉  reason: contains not printable characters */
        protected final Object f1091;

        /* renamed from: 龘  reason: contains not printable characters */
        protected final Object f1092;

        protected static final class SystemRouteController extends MediaRouteProvider.RouteController {

            /* renamed from: 龘  reason: contains not printable characters */
            private final Object f1093;

            public SystemRouteController(Object obj) {
                this.f1093 = obj;
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m1385(int i) {
                MediaRouterJellybean.RouteInfo.m1260(this.f1093, i);
            }

            /* renamed from: 齉  reason: contains not printable characters */
            public void m1386(int i) {
                MediaRouterJellybean.RouteInfo.m1255(this.f1093, i);
            }
        }

        protected static final class SystemRouteRecord {

            /* renamed from: 靐  reason: contains not printable characters */
            public final String f1094;

            /* renamed from: 齉  reason: contains not printable characters */
            public MediaRouteDescriptor f1095;

            /* renamed from: 龘  reason: contains not printable characters */
            public final Object f1096;

            public SystemRouteRecord(Object obj, String str) {
                this.f1096 = obj;
                this.f1094 = str;
            }
        }

        protected static final class UserRouteRecord {

            /* renamed from: 靐  reason: contains not printable characters */
            public final Object f1097;

            /* renamed from: 龘  reason: contains not printable characters */
            public final MediaRouter.RouteInfo f1098;

            public UserRouteRecord(MediaRouter.RouteInfo routeInfo, Object obj) {
                this.f1098 = routeInfo;
                this.f1097 = obj;
            }
        }

        static {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addCategory("android.media.intent.category.LIVE_AUDIO");
            f1079.add(intentFilter);
            IntentFilter intentFilter2 = new IntentFilter();
            intentFilter2.addCategory("android.media.intent.category.LIVE_VIDEO");
            f1080.add(intentFilter2);
        }

        public JellybeanImpl(Context context, SyncCallback syncCallback) {
            super(context);
            this.f1086 = syncCallback;
            this.f1092 = MediaRouterJellybean.m1234(context);
            this.f1089 = m1363();
            this.f1091 = m1359();
            this.f1090 = MediaRouterJellybean.m1238(this.f1092, context.getResources().getString(R.string.mr_user_route_category_name), false);
            m1352();
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        private void m1352() {
            m1362();
            boolean z = false;
            for (Object r1 : MediaRouterJellybean.m1239(this.f1092)) {
                z |= m1354(r1);
            }
            if (z) {
                m1360();
            }
        }

        /* renamed from: ٴ  reason: contains not printable characters */
        private String m1353(Object obj) {
            String format = m1357() == obj ? "DEFAULT_ROUTE" : String.format(Locale.US, "ROUTE_%08x", new Object[]{Integer.valueOf(m1358(obj).hashCode())});
            if (m1365(format) < 0) {
                return format;
            }
            int i = 2;
            while (true) {
                String format2 = String.format(Locale.US, "%s_%d", new Object[]{format, Integer.valueOf(i)});
                if (m1365(format2) < 0) {
                    return format2;
                }
                i++;
            }
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean m1354(Object obj) {
            if (m1356(obj) != null || m1355(obj) >= 0) {
                return false;
            }
            SystemRouteRecord systemRouteRecord = new SystemRouteRecord(obj, m1353(obj));
            m1378(systemRouteRecord);
            this.f1083.add(systemRouteRecord);
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m1355(Object obj) {
            int size = this.f1083.size();
            for (int i = 0; i < size; i++) {
                if (this.f1083.get(i).f1096 == obj) {
                    return i;
                }
            }
            return -1;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʼ  reason: contains not printable characters */
        public UserRouteRecord m1356(Object obj) {
            Object r0 = MediaRouterJellybean.RouteInfo.m1252(obj);
            if (r0 instanceof UserRouteRecord) {
                return (UserRouteRecord) r0;
            }
            return null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʽ  reason: contains not printable characters */
        public Object m1357() {
            if (this.f1085 == null) {
                this.f1085 = new MediaRouterJellybean.GetDefaultRouteWorkaround();
            }
            return this.f1085.m1250(this.f1092);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʽ  reason: contains not printable characters */
        public String m1358(Object obj) {
            CharSequence r0 = MediaRouterJellybean.RouteInfo.m1259(obj, m1073());
            return r0 != null ? r0.toString() : "";
        }

        /* access modifiers changed from: protected */
        /* renamed from: ˈ  reason: contains not printable characters */
        public Object m1359() {
            return MediaRouterJellybean.m1236((MediaRouterJellybean.VolumeCallback) this);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ˑ  reason: contains not printable characters */
        public void m1360() {
            MediaRouteProviderDescriptor.Builder builder = new MediaRouteProviderDescriptor.Builder();
            int size = this.f1083.size();
            for (int i = 0; i < size; i++) {
                builder.m1092(this.f1083.get(i).f1095);
            }
            m1078(builder.m1093());
        }

        /* access modifiers changed from: protected */
        /* renamed from: ˑ  reason: contains not printable characters */
        public void m1361(Object obj) {
            if (this.f1084 == null) {
                this.f1084 = new MediaRouterJellybean.SelectRouteWorkaround();
            }
            this.f1084.m1262(this.f1092, GravityCompat.START, obj);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ٴ  reason: contains not printable characters */
        public void m1362() {
            if (this.f1082) {
                this.f1082 = false;
                MediaRouterJellybean.m1241(this.f1092, this.f1089);
            }
            if (this.f1088 != 0) {
                this.f1082 = true;
                MediaRouterJellybean.m1231(this.f1092, this.f1088, this.f1089);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: ᐧ  reason: contains not printable characters */
        public Object m1363() {
            return MediaRouterJellybean.m1235((MediaRouterJellybean.Callback) this);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 连任  reason: contains not printable characters */
        public int m1364(MediaRouter.RouteInfo routeInfo) {
            int size = this.f1087.size();
            for (int i = 0; i < size; i++) {
                if (this.f1087.get(i).f1098 == routeInfo) {
                    return i;
                }
            }
            return -1;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m1365(String str) {
            int size = this.f1083.size();
            for (int i = 0; i < size; i++) {
                if (this.f1083.get(i).f1094.equals(str)) {
                    return i;
                }
            }
            return -1;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1366(int i, Object obj) {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1367(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
            int i = 0;
            boolean z = false;
            if (mediaRouteDiscoveryRequest != null) {
                List<String> r0 = mediaRouteDiscoveryRequest.m1065().m1100();
                int size = r0.size();
                for (int i2 = 0; i2 < size; i2++) {
                    String str = r0.get(i2);
                    i = str.equals("android.media.intent.category.LIVE_AUDIO") ? i | 1 : str.equals("android.media.intent.category.LIVE_VIDEO") ? i | 2 : i | 8388608;
                }
                z = mediaRouteDiscoveryRequest.m1062();
            }
            if (this.f1088 != i || this.f1081 != z) {
                this.f1088 = i;
                this.f1081 = z;
                m1352();
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1368(MediaRouter.RouteInfo routeInfo) {
            int r0;
            if (routeInfo.m1216() != this && (r0 = m1364(routeInfo)) >= 0) {
                UserRouteRecord remove = this.f1087.remove(r0);
                MediaRouterJellybean.RouteInfo.m1261(remove.f1097, (Object) null);
                MediaRouterJellybean.UserRouteInfo.m1270(remove.f1097, (Object) null);
                MediaRouterJellybean.m1232(this.f1092, remove.f1097);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1369(Object obj) {
            int r0;
            if (m1356(obj) == null && (r0 = m1355(obj)) >= 0) {
                this.f1083.remove(r0);
                m1360();
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1370(Object obj, int i) {
            UserRouteRecord r0 = m1356(obj);
            if (r0 != null) {
                r0.f1098.m1220(i);
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m1371(MediaRouter.RouteInfo routeInfo) {
            if (routeInfo.m1214()) {
                if (routeInfo.m1216() != this) {
                    int r0 = m1364(routeInfo);
                    if (r0 >= 0) {
                        m1361(this.f1087.get(r0).f1097);
                        return;
                    }
                    return;
                }
                int r02 = m1365(routeInfo.m1213());
                if (r02 >= 0) {
                    m1361(this.f1083.get(r02).f1096);
                }
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m1372(Object obj) {
            int r0;
            if (m1356(obj) == null && (r0 = m1355(obj)) >= 0) {
                SystemRouteRecord systemRouteRecord = this.f1083.get(r0);
                int r1 = MediaRouterJellybean.RouteInfo.m1256(obj);
                if (r1 != systemRouteRecord.f1095.m1045()) {
                    systemRouteRecord.f1095 = new MediaRouteDescriptor.Builder(systemRouteRecord.f1095).m1052(r1).m1060();
                    m1360();
                }
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1373(MediaRouter.RouteInfo routeInfo) {
            int r0;
            if (routeInfo.m1216() != this && (r0 = m1364(routeInfo)) >= 0) {
                m1380(this.f1087.get(r0));
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1374(Object obj) {
            int r0;
            if (m1356(obj) == null && (r0 = m1355(obj)) >= 0) {
                m1378(this.f1083.get(r0));
                m1360();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaRouteProvider.RouteController m1375(String str) {
            int r0 = m1365(str);
            if (r0 >= 0) {
                return new SystemRouteController(this.f1083.get(r0).f1096);
            }
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1376(int i, Object obj) {
            if (obj == MediaRouterJellybean.m1237(this.f1092, (int) GravityCompat.START)) {
                UserRouteRecord r2 = m1356(obj);
                if (r2 != null) {
                    r2.f1098.m1212();
                    return;
                }
                int r0 = m1355(obj);
                if (r0 >= 0) {
                    this.f1086.m1402(this.f1083.get(r0).f1094);
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1377(MediaRouter.RouteInfo routeInfo) {
            if (routeInfo.m1216() != this) {
                Object r2 = MediaRouterJellybean.m1230(this.f1092, this.f1090);
                UserRouteRecord userRouteRecord = new UserRouteRecord(routeInfo, r2);
                MediaRouterJellybean.RouteInfo.m1261(r2, (Object) userRouteRecord);
                MediaRouterJellybean.UserRouteInfo.m1270(r2, this.f1091);
                m1380(userRouteRecord);
                this.f1087.add(userRouteRecord);
                MediaRouterJellybean.m1233(this.f1092, r2);
                return;
            }
            int r0 = m1355(MediaRouterJellybean.m1237(this.f1092, (int) GravityCompat.START));
            if (r0 >= 0 && this.f1083.get(r0).f1094.equals(routeInfo.m1213())) {
                routeInfo.m1212();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1378(SystemRouteRecord systemRouteRecord) {
            MediaRouteDescriptor.Builder builder = new MediaRouteDescriptor.Builder(systemRouteRecord.f1094, m1358(systemRouteRecord.f1096));
            m1379(systemRouteRecord, builder);
            systemRouteRecord.f1095 = builder.m1060();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1379(SystemRouteRecord systemRouteRecord, MediaRouteDescriptor.Builder builder) {
            int r0 = MediaRouterJellybean.RouteInfo.m1258(systemRouteRecord.f1096);
            if ((r0 & 1) != 0) {
                builder.m1058((Collection<IntentFilter>) f1079);
            }
            if ((r0 & 2) != 0) {
                builder.m1058((Collection<IntentFilter>) f1080);
            }
            builder.m1055(MediaRouterJellybean.RouteInfo.m1254(systemRouteRecord.f1096));
            builder.m1049(MediaRouterJellybean.RouteInfo.m1257(systemRouteRecord.f1096));
            builder.m1052(MediaRouterJellybean.RouteInfo.m1256(systemRouteRecord.f1096));
            builder.m1048(MediaRouterJellybean.RouteInfo.m1253(systemRouteRecord.f1096));
            builder.m1046(MediaRouterJellybean.RouteInfo.m1251(systemRouteRecord.f1096));
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1380(UserRouteRecord userRouteRecord) {
            MediaRouterJellybean.UserRouteInfo.m1269(userRouteRecord.f1097, (CharSequence) userRouteRecord.f1098.m1221());
            MediaRouterJellybean.UserRouteInfo.m1268(userRouteRecord.f1097, userRouteRecord.f1098.m1205());
            MediaRouterJellybean.UserRouteInfo.m1264(userRouteRecord.f1097, userRouteRecord.f1098.m1202());
            MediaRouterJellybean.UserRouteInfo.m1267(userRouteRecord.f1097, userRouteRecord.f1098.m1208());
            MediaRouterJellybean.UserRouteInfo.m1266(userRouteRecord.f1097, userRouteRecord.f1098.m1209());
            MediaRouterJellybean.UserRouteInfo.m1263(userRouteRecord.f1097, userRouteRecord.f1098.m1207());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1381(Object obj) {
            if (m1354(obj)) {
                m1360();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1382(Object obj, int i) {
            UserRouteRecord r0 = m1356(obj);
            if (r0 != null) {
                r0.f1098.m1224(i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1383(Object obj, Object obj2) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1384(Object obj, Object obj2, int i) {
        }
    }

    private static class JellybeanMr1Impl extends JellybeanImpl implements MediaRouterJellybeanMr1.Callback {

        /* renamed from: ٴ  reason: contains not printable characters */
        private MediaRouterJellybeanMr1.ActiveScanWorkaround f1099;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private MediaRouterJellybeanMr1.IsConnectingWorkaround f1100;

        public JellybeanMr1Impl(Context context, SyncCallback syncCallback) {
            super(context, syncCallback);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ٴ  reason: contains not printable characters */
        public void m1387() {
            super.m1362();
            if (this.f1099 == null) {
                this.f1099 = new MediaRouterJellybeanMr1.ActiveScanWorkaround(m1073(), m1069());
            }
            this.f1099.m1274(this.f1081 ? this.f1088 : 0);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ᐧ  reason: contains not printable characters */
        public Object m1388() {
            return MediaRouterJellybeanMr1.m1273(this);
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public void m1389(Object obj) {
            int r0 = m1355(obj);
            if (r0 >= 0) {
                JellybeanImpl.SystemRouteRecord systemRouteRecord = (JellybeanImpl.SystemRouteRecord) this.f1083.get(r0);
                Display r1 = MediaRouterJellybeanMr1.RouteInfo.m1277(obj);
                int displayId = r1 != null ? r1.getDisplayId() : -1;
                if (displayId != systemRouteRecord.f1095.m1032()) {
                    systemRouteRecord.f1095 = new MediaRouteDescriptor.Builder(systemRouteRecord.f1095).m1047(displayId).m1060();
                    m1360();
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m1390(JellybeanImpl.SystemRouteRecord systemRouteRecord) {
            if (this.f1100 == null) {
                this.f1100 = new MediaRouterJellybeanMr1.IsConnectingWorkaround();
            }
            return this.f1100.m1276(systemRouteRecord.f1096);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1391(JellybeanImpl.SystemRouteRecord systemRouteRecord, MediaRouteDescriptor.Builder builder) {
            super.m1379(systemRouteRecord, builder);
            if (!MediaRouterJellybeanMr1.RouteInfo.m1278(systemRouteRecord.f1096)) {
                builder.m1059(false);
            }
            if (m1390(systemRouteRecord)) {
                builder.m1051(true);
            }
            Display r0 = MediaRouterJellybeanMr1.RouteInfo.m1277(systemRouteRecord.f1096);
            if (r0 != null) {
                builder.m1047(r0.getDisplayId());
            }
        }
    }

    private static class JellybeanMr2Impl extends JellybeanMr1Impl {
        public JellybeanMr2Impl(Context context, SyncCallback syncCallback) {
            super(context, syncCallback);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʽ  reason: contains not printable characters */
        public Object m1392() {
            return MediaRouterJellybeanMr2.m1279(this.f1092);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ˑ  reason: contains not printable characters */
        public void m1393(Object obj) {
            MediaRouterJellybean.m1240(this.f1092, (int) GravityCompat.START, obj);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ٴ  reason: contains not printable characters */
        public void m1394() {
            int i = 1;
            if (this.f1082) {
                MediaRouterJellybean.m1241(this.f1092, this.f1089);
            }
            this.f1082 = true;
            Object obj = this.f1092;
            int i2 = this.f1088;
            Object obj2 = this.f1089;
            if (!this.f1081) {
                i = 0;
            }
            MediaRouterJellybeanMr2.m1280(obj, i2, obj2, i | 2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m1395(JellybeanImpl.SystemRouteRecord systemRouteRecord) {
            return MediaRouterJellybeanMr2.RouteInfo.m1281(systemRouteRecord.f1096);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1396(JellybeanImpl.SystemRouteRecord systemRouteRecord, MediaRouteDescriptor.Builder builder) {
            super.m1391(systemRouteRecord, builder);
            CharSequence r0 = MediaRouterJellybeanMr2.RouteInfo.m1282(systemRouteRecord.f1096);
            if (r0 != null) {
                builder.m1054(r0.toString());
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m1397(JellybeanImpl.UserRouteRecord userRouteRecord) {
            super.m1380(userRouteRecord);
            MediaRouterJellybeanMr2.UserRouteInfo.m1283(userRouteRecord.f1097, userRouteRecord.f1098.m1217());
        }
    }

    static class LegacyImpl extends SystemMediaRouteProvider {

        /* renamed from: 齉  reason: contains not printable characters */
        private static final ArrayList<IntentFilter> f1101 = new ArrayList<>();

        /* renamed from: 靐  reason: contains not printable characters */
        int f1102 = -1;

        /* renamed from: 麤  reason: contains not printable characters */
        private final VolumeChangeReceiver f1103;

        /* renamed from: 龘  reason: contains not printable characters */
        final AudioManager f1104;

        final class DefaultRouteController extends MediaRouteProvider.RouteController {
            DefaultRouteController() {
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m1400(int i) {
                LegacyImpl.this.f1104.setStreamVolume(3, i, 0);
                LegacyImpl.this.m1398();
            }

            /* renamed from: 齉  reason: contains not printable characters */
            public void m1401(int i) {
                int streamVolume = LegacyImpl.this.f1104.getStreamVolume(3);
                if (Math.min(LegacyImpl.this.f1104.getStreamMaxVolume(3), Math.max(0, streamVolume + i)) != streamVolume) {
                    LegacyImpl.this.f1104.setStreamVolume(3, streamVolume, 0);
                }
                LegacyImpl.this.m1398();
            }
        }

        final class VolumeChangeReceiver extends BroadcastReceiver {
            VolumeChangeReceiver() {
            }

            public void onReceive(Context context, Intent intent) {
                int intExtra;
                if (intent.getAction().equals("android.media.VOLUME_CHANGED_ACTION") && intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1) == 3 && (intExtra = intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", -1)) >= 0 && intExtra != LegacyImpl.this.f1102) {
                    LegacyImpl.this.m1398();
                }
            }
        }

        static {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addCategory("android.media.intent.category.LIVE_AUDIO");
            intentFilter.addCategory("android.media.intent.category.LIVE_VIDEO");
            f1101.add(intentFilter);
        }

        public LegacyImpl(Context context) {
            super(context);
            this.f1104 = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            this.f1103 = new VolumeChangeReceiver();
            context.registerReceiver(this.f1103, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
            m1398();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˑ  reason: contains not printable characters */
        public void m1398() {
            Resources resources = m1073().getResources();
            int streamMaxVolume = this.f1104.getStreamMaxVolume(3);
            this.f1102 = this.f1104.getStreamVolume(3);
            m1078(new MediaRouteProviderDescriptor.Builder().m1092(new MediaRouteDescriptor.Builder("DEFAULT_ROUTE", resources.getString(R.string.mr_system_route_name)).m1058((Collection<IntentFilter>) f1101).m1049(3).m1055(0).m1046(1).m1048(streamMaxVolume).m1052(this.f1102).m1060()).m1093());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaRouteProvider.RouteController m1399(String str) {
            if (str.equals("DEFAULT_ROUTE")) {
                return new DefaultRouteController();
            }
            return null;
        }
    }

    public interface SyncCallback {
        /* renamed from: 靐  reason: contains not printable characters */
        void m1402(String str);
    }

    protected SystemMediaRouteProvider(Context context) {
        super(context, new MediaRouteProvider.ProviderMetadata(new ComponentName(AbstractSpiCall.ANDROID_CLIENT_TYPE, SystemMediaRouteProvider.class.getName())));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static SystemMediaRouteProvider m1345(Context context, SyncCallback syncCallback) {
        return Build.VERSION.SDK_INT >= 24 ? new Api24Impl(context, syncCallback) : Build.VERSION.SDK_INT >= 18 ? new JellybeanMr2Impl(context, syncCallback) : Build.VERSION.SDK_INT >= 17 ? new JellybeanMr1Impl(context, syncCallback) : Build.VERSION.SDK_INT >= 16 ? new JellybeanImpl(context, syncCallback) : new LegacyImpl(context);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public Object m1346() {
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1347(MediaRouter.RouteInfo routeInfo) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m1348(MediaRouter.RouteInfo routeInfo) {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m1349(MediaRouter.RouteInfo routeInfo) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1350(MediaRouter.RouteInfo routeInfo) {
    }
}
