package android.support.v7.media;

import android.content.Context;
import android.media.MediaRouter;
import android.media.RemoteControlClient;
import android.os.Build;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

final class MediaRouterJellybean {

    public interface Callback {
        /* renamed from: 靐  reason: contains not printable characters */
        void m1242(int i, Object obj);

        /* renamed from: 靐  reason: contains not printable characters */
        void m1243(Object obj);

        /* renamed from: 麤  reason: contains not printable characters */
        void m1244(Object obj);

        /* renamed from: 齉  reason: contains not printable characters */
        void m1245(Object obj);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1246(int i, Object obj);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1247(Object obj);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1248(Object obj, Object obj2);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1249(Object obj, Object obj2, int i);
    }

    static class CallbackProxy<T extends Callback> extends MediaRouter.Callback {

        /* renamed from: 龘  reason: contains not printable characters */
        protected final T f1018;

        public CallbackProxy(T t) {
            this.f1018 = t;
        }

        public void onRouteAdded(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            this.f1018.m1247(routeInfo);
        }

        public void onRouteChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            this.f1018.m1245(routeInfo);
        }

        public void onRouteGrouped(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo, MediaRouter.RouteGroup routeGroup, int i) {
            this.f1018.m1249(routeInfo, routeGroup, i);
        }

        public void onRouteRemoved(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            this.f1018.m1243(routeInfo);
        }

        public void onRouteSelected(MediaRouter mediaRouter, int i, MediaRouter.RouteInfo routeInfo) {
            this.f1018.m1246(i, (Object) routeInfo);
        }

        public void onRouteUngrouped(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo, MediaRouter.RouteGroup routeGroup) {
            this.f1018.m1248((Object) routeInfo, (Object) routeGroup);
        }

        public void onRouteUnselected(MediaRouter mediaRouter, int i, MediaRouter.RouteInfo routeInfo) {
            this.f1018.m1242(i, routeInfo);
        }

        public void onRouteVolumeChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
            this.f1018.m1244(routeInfo);
        }
    }

    public static final class GetDefaultRouteWorkaround {

        /* renamed from: 龘  reason: contains not printable characters */
        private Method f1019;

        public GetDefaultRouteWorkaround() {
            if (Build.VERSION.SDK_INT < 16 || Build.VERSION.SDK_INT > 17) {
                throw new UnsupportedOperationException();
            }
            try {
                this.f1019 = MediaRouter.class.getMethod("getSystemAudioRoute", new Class[0]);
            } catch (NoSuchMethodException e) {
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Object m1250(Object obj) {
            MediaRouter mediaRouter = (MediaRouter) obj;
            if (this.f1019 != null) {
                try {
                    return this.f1019.invoke(mediaRouter, new Object[0]);
                } catch (IllegalAccessException | InvocationTargetException e) {
                }
            }
            return mediaRouter.getRouteAt(0);
        }
    }

    public static final class RouteInfo {
        /* renamed from: ʻ  reason: contains not printable characters */
        public static int m1251(Object obj) {
            return ((MediaRouter.RouteInfo) obj).getVolumeHandling();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public static Object m1252(Object obj) {
            return ((MediaRouter.RouteInfo) obj).getTag();
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public static int m1253(Object obj) {
            return ((MediaRouter.RouteInfo) obj).getVolumeMax();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public static int m1254(Object obj) {
            return ((MediaRouter.RouteInfo) obj).getPlaybackType();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public static void m1255(Object obj, int i) {
            ((MediaRouter.RouteInfo) obj).requestUpdateVolume(i);
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public static int m1256(Object obj) {
            return ((MediaRouter.RouteInfo) obj).getVolume();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public static int m1257(Object obj) {
            return ((MediaRouter.RouteInfo) obj).getPlaybackStream();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static int m1258(Object obj) {
            return ((MediaRouter.RouteInfo) obj).getSupportedTypes();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static CharSequence m1259(Object obj, Context context) {
            return ((MediaRouter.RouteInfo) obj).getName(context);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m1260(Object obj, int i) {
            ((MediaRouter.RouteInfo) obj).requestSetVolume(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m1261(Object obj, Object obj2) {
            ((MediaRouter.RouteInfo) obj).setTag(obj2);
        }
    }

    public static final class SelectRouteWorkaround {

        /* renamed from: 龘  reason: contains not printable characters */
        private Method f1020;

        public SelectRouteWorkaround() {
            if (Build.VERSION.SDK_INT < 16 || Build.VERSION.SDK_INT > 17) {
                throw new UnsupportedOperationException();
            }
            Class<MediaRouter> cls = MediaRouter.class;
            try {
                this.f1020 = cls.getMethod("selectRouteInt", new Class[]{Integer.TYPE, MediaRouter.RouteInfo.class});
            } catch (NoSuchMethodException e) {
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1262(Object obj, int i, Object obj2) {
            MediaRouter mediaRouter = (MediaRouter) obj;
            MediaRouter.RouteInfo routeInfo = (MediaRouter.RouteInfo) obj2;
            if ((8388608 & routeInfo.getSupportedTypes()) == 0) {
                if (this.f1020 != null) {
                    try {
                        this.f1020.invoke(mediaRouter, new Object[]{Integer.valueOf(i), routeInfo});
                        return;
                    } catch (IllegalAccessException e) {
                        Log.w("MediaRouterJellybean", "Cannot programmatically select non-user route.  Media routing may not work.", e);
                    } catch (InvocationTargetException e2) {
                        Log.w("MediaRouterJellybean", "Cannot programmatically select non-user route.  Media routing may not work.", e2);
                    }
                } else {
                    Log.w("MediaRouterJellybean", "Cannot programmatically select non-user route because the platform is missing the selectRouteInt() method.  Media routing may not work.");
                }
            }
            mediaRouter.selectRoute(i, routeInfo);
        }
    }

    public static final class UserRouteInfo {
        /* renamed from: 连任  reason: contains not printable characters */
        public static void m1263(Object obj, int i) {
            ((MediaRouter.UserRouteInfo) obj).setVolumeHandling(i);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public static void m1264(Object obj, int i) {
            ((MediaRouter.UserRouteInfo) obj).setPlaybackStream(i);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public static void m1265(Object obj, Object obj2) {
            ((MediaRouter.UserRouteInfo) obj).setRemoteControlClient((RemoteControlClient) obj2);
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public static void m1266(Object obj, int i) {
            ((MediaRouter.UserRouteInfo) obj).setVolumeMax(i);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public static void m1267(Object obj, int i) {
            ((MediaRouter.UserRouteInfo) obj).setVolume(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m1268(Object obj, int i) {
            ((MediaRouter.UserRouteInfo) obj).setPlaybackType(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m1269(Object obj, CharSequence charSequence) {
            ((MediaRouter.UserRouteInfo) obj).setName(charSequence);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m1270(Object obj, Object obj2) {
            ((MediaRouter.UserRouteInfo) obj).setVolumeCallback((MediaRouter.VolumeCallback) obj2);
        }
    }

    public interface VolumeCallback {
        /* renamed from: 靐  reason: contains not printable characters */
        void m1271(Object obj, int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1272(Object obj, int i);
    }

    static class VolumeCallbackProxy<T extends VolumeCallback> extends MediaRouter.VolumeCallback {

        /* renamed from: 龘  reason: contains not printable characters */
        protected final T f1021;

        public VolumeCallbackProxy(T t) {
            this.f1021 = t;
        }

        public void onVolumeSetRequest(MediaRouter.RouteInfo routeInfo, int i) {
            this.f1021.m1272(routeInfo, i);
        }

        public void onVolumeUpdateRequest(MediaRouter.RouteInfo routeInfo, int i) {
            this.f1021.m1271(routeInfo, i);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Object m1230(Object obj, Object obj2) {
        return ((MediaRouter) obj).createUserRoute((MediaRouter.RouteCategory) obj2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m1231(Object obj, int i, Object obj2) {
        ((MediaRouter) obj).addCallback(i, (MediaRouter.Callback) obj2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static void m1232(Object obj, Object obj2) {
        ((MediaRouter) obj).removeUserRoute((MediaRouter.UserRouteInfo) obj2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static void m1233(Object obj, Object obj2) {
        ((MediaRouter) obj).addUserRoute((MediaRouter.UserRouteInfo) obj2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m1234(Context context) {
        return context.getSystemService("media_router");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m1235(Callback callback) {
        return new CallbackProxy(callback);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m1236(VolumeCallback volumeCallback) {
        return new VolumeCallbackProxy(volumeCallback);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m1237(Object obj, int i) {
        return ((MediaRouter) obj).getSelectedRoute(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m1238(Object obj, String str, boolean z) {
        return ((MediaRouter) obj).createRouteCategory(str, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List m1239(Object obj) {
        MediaRouter mediaRouter = (MediaRouter) obj;
        int routeCount = mediaRouter.getRouteCount();
        ArrayList arrayList = new ArrayList(routeCount);
        for (int i = 0; i < routeCount; i++) {
            arrayList.add(mediaRouter.getRouteAt(i));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m1240(Object obj, int i, Object obj2) {
        ((MediaRouter) obj).selectRoute(i, (MediaRouter.RouteInfo) obj2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m1241(Object obj, Object obj2) {
        ((MediaRouter) obj).removeCallback((MediaRouter.Callback) obj2);
    }
}
