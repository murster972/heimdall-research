package android.support.v7.media;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.media.MediaRouteProvider;
import android.support.v7.media.MediaRouter;
import android.util.Log;
import android.util.SparseArray;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

final class RegisteredMediaRouteProvider extends MediaRouteProvider implements ServiceConnection {

    /* renamed from: 龘  reason: contains not printable characters */
    static final boolean f1028 = Log.isLoggable("MediaRouteProviderProxy", 3);

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f1029;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Connection f1030;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f1031;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f1032;

    /* renamed from: 靐  reason: contains not printable characters */
    final PrivateHandler f1033;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ArrayList<Controller> f1034 = new ArrayList<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private final ComponentName f1035;

    private final class Connection implements IBinder.DeathRecipient {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f1036 = 1;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f1037;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f1038;

        /* renamed from: ˑ  reason: contains not printable characters */
        private final SparseArray<MediaRouter.ControlRequestCallback> f1039 = new SparseArray<>();

        /* renamed from: 连任  reason: contains not printable characters */
        private int f1040 = 1;

        /* renamed from: 靐  reason: contains not printable characters */
        private final Messenger f1041;

        /* renamed from: 麤  reason: contains not printable characters */
        private final Messenger f1042;

        /* renamed from: 齉  reason: contains not printable characters */
        private final ReceiveHandler f1043;

        public Connection(Messenger messenger) {
            this.f1041 = messenger;
            this.f1043 = new ReceiveHandler(this);
            this.f1042 = new Messenger(this.f1043);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m1304(int i, int i2, int i3, Object obj, Bundle bundle) {
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.arg1 = i2;
            obtain.arg2 = i3;
            obtain.obj = obj;
            obtain.setData(bundle);
            obtain.replyTo = this.f1042;
            try {
                this.f1041.send(obtain);
                return true;
            } catch (DeadObjectException e) {
                return false;
            } catch (RemoteException e2) {
                if (i != 2) {
                    Log.e("MediaRouteProviderProxy", "Could not send message to service.", e2);
                }
                return false;
            }
        }

        public void binderDied() {
            RegisteredMediaRouteProvider.this.f1033.post(new Runnable() {
                public void run() {
                    RegisteredMediaRouteProvider.this.m1296(Connection.this);
                }
            });
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1305() {
            m1304(2, 0, 0, (Object) null, (Bundle) null);
            this.f1043.m1330();
            this.f1041.getBinder().unlinkToDeath(this, 0);
            RegisteredMediaRouteProvider.this.f1033.post(new Runnable() {
                public void run() {
                    Connection.this.m1309();
                }
            });
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1306(int i, int i2) {
            Bundle bundle = new Bundle();
            bundle.putInt("volume", i2);
            int i3 = this.f1040;
            this.f1040 = i3 + 1;
            m1304(7, i3, i, (Object) null, bundle);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m1307(int i) {
            return true;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m1308(int i) {
            int i2 = this.f1040;
            this.f1040 = i2 + 1;
            m1304(5, i2, i, (Object) null, (Bundle) null);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m1309() {
            for (int i = 0; i < this.f1039.size(); i++) {
                this.f1039.valueAt(i).m1135((String) null, (Bundle) null);
            }
            this.f1039.clear();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1310(int i) {
            int i2 = this.f1040;
            this.f1040 = i2 + 1;
            m1304(4, i2, i, (Object) null, (Bundle) null);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1311(int i, int i2) {
            Bundle bundle = new Bundle();
            bundle.putInt("volume", i2);
            int i3 = this.f1040;
            this.f1040 = i3 + 1;
            m1304(8, i3, i, (Object) null, bundle);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m1312(String str, String str2) {
            int i = this.f1036;
            this.f1036 = i + 1;
            Bundle bundle = new Bundle();
            bundle.putString("routeId", str);
            bundle.putString("routeGroupId", str2);
            int i2 = this.f1040;
            this.f1040 = i2 + 1;
            m1304(3, i2, i, (Object) null, bundle);
            return i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1313(int i, int i2) {
            Bundle bundle = new Bundle();
            bundle.putInt("unselectReason", i2);
            int i3 = this.f1040;
            this.f1040 = i3 + 1;
            m1304(6, i3, i, (Object) null, bundle);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1314(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
            int i = this.f1040;
            this.f1040 = i + 1;
            m1304(10, i, 0, mediaRouteDiscoveryRequest != null ? mediaRouteDiscoveryRequest.m1063() : null, (Bundle) null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1315() {
            int i = this.f1040;
            this.f1040 = i + 1;
            this.f1038 = i;
            if (!m1304(1, this.f1038, 2, (Object) null, (Bundle) null)) {
                return false;
            }
            try {
                this.f1041.getBinder().linkToDeath(this, 0);
                return true;
            } catch (RemoteException e) {
                binderDied();
                return false;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1316(int i) {
            if (i == this.f1038) {
                this.f1038 = 0;
                RegisteredMediaRouteProvider.this.m1302(this, "Registration failed");
            }
            MediaRouter.ControlRequestCallback controlRequestCallback = this.f1039.get(i);
            if (controlRequestCallback == null) {
                return true;
            }
            this.f1039.remove(i);
            controlRequestCallback.m1135((String) null, (Bundle) null);
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1317(int i, int i2, Bundle bundle) {
            if (this.f1037 != 0 || i != this.f1038 || i2 < 1) {
                return false;
            }
            this.f1038 = 0;
            this.f1037 = i2;
            RegisteredMediaRouteProvider.this.m1301(this, MediaRouteProviderDescriptor.m1089(bundle));
            RegisteredMediaRouteProvider.this.m1300(this);
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1318(int i, Bundle bundle) {
            MediaRouter.ControlRequestCallback controlRequestCallback = this.f1039.get(i);
            if (controlRequestCallback == null) {
                return false;
            }
            this.f1039.remove(i);
            controlRequestCallback.m1134(bundle);
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1319(int i, String str, Bundle bundle) {
            MediaRouter.ControlRequestCallback controlRequestCallback = this.f1039.get(i);
            if (controlRequestCallback == null) {
                return false;
            }
            this.f1039.remove(i);
            controlRequestCallback.m1135(str, bundle);
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m1320(Bundle bundle) {
            if (this.f1037 == 0) {
                return false;
            }
            RegisteredMediaRouteProvider.this.m1301(this, MediaRouteProviderDescriptor.m1089(bundle));
            return true;
        }
    }

    private final class Controller extends MediaRouteProvider.RouteController {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f1047;

        /* renamed from: ʼ  reason: contains not printable characters */
        private Connection f1048;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f1049;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f1050 = -1;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f1051;

        /* renamed from: 麤  reason: contains not printable characters */
        private boolean f1052;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String f1053;

        public Controller(String str, String str2) {
            this.f1051 = str;
            this.f1053 = str2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1321() {
            this.f1052 = true;
            if (this.f1048 != null) {
                this.f1048.m1308(this.f1049);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m1322(int i) {
            if (this.f1048 != null) {
                this.f1048.m1306(this.f1049, i);
                return;
            }
            this.f1050 = i;
            this.f1047 = 0;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m1323() {
            if (this.f1048 != null) {
                this.f1048.m1310(this.f1049);
                this.f1048 = null;
                this.f1049 = 0;
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1324() {
            m1327(0);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m1325(int i) {
            if (this.f1048 != null) {
                this.f1048.m1311(this.f1049, i);
            } else {
                this.f1047 += i;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1326() {
            RegisteredMediaRouteProvider.this.m1303(this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1327(int i) {
            this.f1052 = false;
            if (this.f1048 != null) {
                this.f1048.m1313(this.f1049, i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1328(Connection connection) {
            this.f1048 = connection;
            this.f1049 = connection.m1312(this.f1051, this.f1053);
            if (this.f1052) {
                connection.m1308(this.f1049);
                if (this.f1050 >= 0) {
                    connection.m1306(this.f1049, this.f1050);
                    this.f1050 = -1;
                }
                if (this.f1047 != 0) {
                    connection.m1311(this.f1049, this.f1047);
                    this.f1047 = 0;
                }
            }
        }
    }

    private static final class PrivateHandler extends Handler {
        PrivateHandler() {
        }
    }

    private static final class ReceiveHandler extends Handler {

        /* renamed from: 龘  reason: contains not printable characters */
        private final WeakReference<Connection> f1055;

        public ReceiveHandler(Connection connection) {
            this.f1055 = new WeakReference<>(connection);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m1329(Connection connection, int i, int i2, int i3, Object obj, Bundle bundle) {
            switch (i) {
                case 0:
                    connection.m1316(i2);
                    return true;
                case 1:
                    connection.m1307(i2);
                    return true;
                case 2:
                    if (obj == null || (obj instanceof Bundle)) {
                        return connection.m1317(i2, i3, (Bundle) obj);
                    }
                case 3:
                    if (obj == null || (obj instanceof Bundle)) {
                        return connection.m1318(i2, (Bundle) obj);
                    }
                case 4:
                    if (obj == null || (obj instanceof Bundle)) {
                        return connection.m1319(i2, bundle == null ? null : bundle.getString("error"), (Bundle) obj);
                    }
                case 5:
                    if (obj == null || (obj instanceof Bundle)) {
                        return connection.m1320((Bundle) obj);
                    }
            }
            return false;
        }

        public void handleMessage(Message message) {
            Connection connection = (Connection) this.f1055.get();
            if (connection != null) {
                if (!m1329(connection, message.what, message.arg1, message.arg2, message.obj, message.peekData()) && RegisteredMediaRouteProvider.f1028) {
                    Log.d("MediaRouteProviderProxy", "Unhandled message from server: " + message);
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1330() {
            this.f1055.clear();
        }
    }

    public RegisteredMediaRouteProvider(Context context, ComponentName componentName) {
        super(context, new MediaRouteProvider.ProviderMetadata(componentName));
        this.f1035 = componentName;
        this.f1033 = new PrivateHandler();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m1284() {
        if (!this.f1029) {
            if (f1028) {
                Log.d("MediaRouteProviderProxy", this + ": Binding");
            }
            Intent intent = new Intent("android.media.MediaRouteProviderService");
            intent.setComponent(this.f1035);
            try {
                this.f1029 = m1073().bindService(intent, this, 1);
                if (!this.f1029 && f1028) {
                    Log.d("MediaRouteProviderProxy", this + ": Bind failed");
                }
            } catch (SecurityException e) {
                if (f1028) {
                    Log.d("MediaRouteProviderProxy", this + ": Bind failed", e);
                }
            }
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m1285() {
        if (this.f1029) {
            if (f1028) {
                Log.d("MediaRouteProviderProxy", this + ": Unbinding");
            }
            this.f1029 = false;
            m1290();
            m1073().unbindService(this);
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean m1286() {
        return this.f1032 && (m1071() != null || !this.f1034.isEmpty());
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m1287() {
        int size = this.f1034.size();
        for (int i = 0; i < size; i++) {
            this.f1034.get(i).m1323();
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m1288() {
        if (m1286()) {
            m1284();
        } else {
            m1285();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private MediaRouteProvider.RouteController m1289(String str, String str2) {
        MediaRouteProviderDescriptor r2 = m1066();
        if (r2 != null) {
            List<MediaRouteDescriptor> r5 = r2.m1091();
            int size = r5.size();
            for (int i = 0; i < size; i++) {
                if (r5.get(i).m1043().equals(str)) {
                    Controller controller = new Controller(str, str2);
                    this.f1034.add(controller);
                    if (this.f1031) {
                        controller.m1328(this.f1030);
                    }
                    m1288();
                    return controller;
                }
            }
        }
        return null;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private void m1290() {
        if (this.f1030 != null) {
            m1078((MediaRouteProviderDescriptor) null);
            this.f1031 = false;
            m1287();
            this.f1030.m1305();
            this.f1030 = null;
        }
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private void m1291() {
        int size = this.f1034.size();
        for (int i = 0; i < size; i++) {
            this.f1034.get(i).m1328(this.f1030);
        }
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (f1028) {
            Log.d("MediaRouteProviderProxy", this + ": Connected");
        }
        if (this.f1029) {
            m1290();
            Messenger messenger = iBinder != null ? new Messenger(iBinder) : null;
            if (MediaRouteProviderProtocol.m1094(messenger)) {
                Connection connection = new Connection(messenger);
                if (connection.m1315()) {
                    this.f1030 = connection;
                } else if (f1028) {
                    Log.d("MediaRouteProviderProxy", this + ": Registration failed");
                }
            } else {
                Log.e("MediaRouteProviderProxy", this + ": Service returned invalid messenger binder");
            }
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (f1028) {
            Log.d("MediaRouteProviderProxy", this + ": Service disconnected");
        }
        m1290();
    }

    public String toString() {
        return "Service connection " + this.f1035.flattenToShortString();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m1292() {
        if (!this.f1032) {
            if (f1028) {
                Log.d("MediaRouteProviderProxy", this + ": Starting");
            }
            this.f1032 = true;
            m1288();
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m1293() {
        if (this.f1032) {
            if (f1028) {
                Log.d("MediaRouteProviderProxy", this + ": Stopping");
            }
            this.f1032 = false;
            m1288();
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m1294() {
        if (this.f1030 == null && m1286()) {
            m1285();
            m1284();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m1295(MediaRouteDiscoveryRequest mediaRouteDiscoveryRequest) {
        if (this.f1031) {
            this.f1030.m1314(mediaRouteDiscoveryRequest);
        }
        m1288();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1296(Connection connection) {
        if (this.f1030 == connection) {
            if (f1028) {
                Log.d("MediaRouteProviderProxy", this + ": Service connection died");
            }
            m1290();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m1297(String str, String str2) {
        return this.f1035.getPackageName().equals(str) && this.f1035.getClassName().equals(str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaRouteProvider.RouteController m1298(String str) {
        if (str != null) {
            return m1289(str, (String) null);
        }
        throw new IllegalArgumentException("routeId cannot be null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MediaRouteProvider.RouteController m1299(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("routeId cannot be null");
        } else if (str2 != null) {
            return m1289(str, str2);
        } else {
            throw new IllegalArgumentException("routeGroupId cannot be null");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1300(Connection connection) {
        if (this.f1030 == connection) {
            this.f1031 = true;
            m1291();
            MediaRouteDiscoveryRequest r0 = m1071();
            if (r0 != null) {
                this.f1030.m1314(r0);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1301(Connection connection, MediaRouteProviderDescriptor mediaRouteProviderDescriptor) {
        if (this.f1030 == connection) {
            if (f1028) {
                Log.d("MediaRouteProviderProxy", this + ": Descriptor changed, descriptor=" + mediaRouteProviderDescriptor);
            }
            m1078(mediaRouteProviderDescriptor);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1302(Connection connection, String str) {
        if (this.f1030 == connection) {
            if (f1028) {
                Log.d("MediaRouteProviderProxy", this + ": Service connection error - " + str);
            }
            m1285();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m1303(Controller controller) {
        this.f1034.remove(controller);
        controller.m1323();
        m1288();
    }
}
