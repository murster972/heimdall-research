package android.support.v7.media;

import android.content.Context;
import android.os.Build;
import android.support.v7.media.MediaRouterJellybean;
import java.lang.ref.WeakReference;

abstract class RemoteControlClientCompat {

    /* renamed from: 靐  reason: contains not printable characters */
    protected final Object f1066;

    /* renamed from: 齉  reason: contains not printable characters */
    protected VolumeCallback f1067;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final Context f1068;

    static class JellybeanImpl extends RemoteControlClientCompat {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Object f1069 = MediaRouterJellybean.m1230(this.f1072, this.f1071);

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f1070;

        /* renamed from: 连任  reason: contains not printable characters */
        private final Object f1071 = MediaRouterJellybean.m1238(this.f1072, "", false);

        /* renamed from: 麤  reason: contains not printable characters */
        private final Object f1072;

        private static final class VolumeCallbackWrapper implements MediaRouterJellybean.VolumeCallback {

            /* renamed from: 龘  reason: contains not printable characters */
            private final WeakReference<JellybeanImpl> f1073;

            public VolumeCallbackWrapper(JellybeanImpl jellybeanImpl) {
                this.f1073 = new WeakReference<>(jellybeanImpl);
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m1341(Object obj, int i) {
                JellybeanImpl jellybeanImpl = (JellybeanImpl) this.f1073.get();
                if (jellybeanImpl != null && jellybeanImpl.f1067 != null) {
                    jellybeanImpl.f1067.m1343(i);
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m1342(Object obj, int i) {
                JellybeanImpl jellybeanImpl = (JellybeanImpl) this.f1073.get();
                if (jellybeanImpl != null && jellybeanImpl.f1067 != null) {
                    jellybeanImpl.f1067.m1344(i);
                }
            }
        }

        public JellybeanImpl(Context context, Object obj) {
            super(context, obj);
            this.f1072 = MediaRouterJellybean.m1234(context);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m1340(PlaybackInfo playbackInfo) {
            MediaRouterJellybean.UserRouteInfo.m1267(this.f1069, playbackInfo.f1078);
            MediaRouterJellybean.UserRouteInfo.m1266(this.f1069, playbackInfo.f1075);
            MediaRouterJellybean.UserRouteInfo.m1263(this.f1069, playbackInfo.f1077);
            MediaRouterJellybean.UserRouteInfo.m1264(this.f1069, playbackInfo.f1076);
            MediaRouterJellybean.UserRouteInfo.m1268(this.f1069, playbackInfo.f1074);
            if (!this.f1070) {
                this.f1070 = true;
                MediaRouterJellybean.UserRouteInfo.m1270(this.f1069, MediaRouterJellybean.m1236((MediaRouterJellybean.VolumeCallback) new VolumeCallbackWrapper(this)));
                MediaRouterJellybean.UserRouteInfo.m1265(this.f1069, this.f1066);
            }
        }
    }

    static class LegacyImpl extends RemoteControlClientCompat {
        public LegacyImpl(Context context, Object obj) {
            super(context, obj);
        }
    }

    public static final class PlaybackInfo {

        /* renamed from: 连任  reason: contains not printable characters */
        public int f1074 = 1;

        /* renamed from: 靐  reason: contains not printable characters */
        public int f1075;

        /* renamed from: 麤  reason: contains not printable characters */
        public int f1076 = 3;

        /* renamed from: 齉  reason: contains not printable characters */
        public int f1077 = 0;

        /* renamed from: 龘  reason: contains not printable characters */
        public int f1078;
    }

    public interface VolumeCallback {
        /* renamed from: 靐  reason: contains not printable characters */
        void m1343(int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m1344(int i);
    }

    protected RemoteControlClientCompat(Context context, Object obj) {
        this.f1068 = context;
        this.f1066 = obj;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static RemoteControlClientCompat m1336(Context context, Object obj) {
        return Build.VERSION.SDK_INT >= 16 ? new JellybeanImpl(context, obj) : new LegacyImpl(context, obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m1337() {
        return this.f1066;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1338(PlaybackInfo playbackInfo) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m1339(VolumeCallback volumeCallback) {
        this.f1067 = volumeCallback;
    }
}
