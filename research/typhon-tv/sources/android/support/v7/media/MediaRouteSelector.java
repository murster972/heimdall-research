package android.support.v7.media;

import android.content.IntentFilter;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class MediaRouteSelector {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final MediaRouteSelector f939 = new MediaRouteSelector(new Bundle(), (List<String>) null);

    /* renamed from: 齉  reason: contains not printable characters */
    private final Bundle f940;

    /* renamed from: 龘  reason: contains not printable characters */
    List<String> f941;

    public static final class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private ArrayList<String> f942;

        public Builder() {
        }

        public Builder(MediaRouteSelector mediaRouteSelector) {
            if (mediaRouteSelector == null) {
                throw new IllegalArgumentException("selector must not be null");
            }
            mediaRouteSelector.m1097();
            if (!mediaRouteSelector.f941.isEmpty()) {
                this.f942 = new ArrayList<>(mediaRouteSelector.f941);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m1103(MediaRouteSelector mediaRouteSelector) {
            if (mediaRouteSelector == null) {
                throw new IllegalArgumentException("selector must not be null");
            }
            m1105((Collection<String>) mediaRouteSelector.m1100());
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m1104(String str) {
            if (str == null) {
                throw new IllegalArgumentException("category must not be null");
            }
            if (this.f942 == null) {
                this.f942 = new ArrayList<>();
            }
            if (!this.f942.contains(str)) {
                this.f942.add(str);
            }
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m1105(Collection<String> collection) {
            if (collection == null) {
                throw new IllegalArgumentException("categories must not be null");
            }
            if (!collection.isEmpty()) {
                for (String r0 : collection) {
                    m1104(r0);
                }
            }
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public MediaRouteSelector m1106() {
            if (this.f942 == null) {
                return MediaRouteSelector.f939;
            }
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("controlCategories", this.f942);
            return new MediaRouteSelector(bundle, this.f942);
        }
    }

    MediaRouteSelector(Bundle bundle, List<String> list) {
        this.f940 = bundle;
        this.f941 = list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static MediaRouteSelector m1095(Bundle bundle) {
        if (bundle != null) {
            return new MediaRouteSelector(bundle, (List<String>) null);
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof MediaRouteSelector)) {
            return false;
        }
        MediaRouteSelector mediaRouteSelector = (MediaRouteSelector) obj;
        m1097();
        mediaRouteSelector.m1097();
        return this.f941.equals(mediaRouteSelector.f941);
    }

    public int hashCode() {
        m1097();
        return this.f941.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediaRouteSelector{ ");
        sb.append("controlCategories=").append(Arrays.toString(m1100().toArray()));
        sb.append(" }");
        return sb.toString();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Bundle m1096() {
        return this.f940;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m1097() {
        if (this.f941 == null) {
            this.f941 = this.f940.getStringArrayList("controlCategories");
            if (this.f941 == null || this.f941.isEmpty()) {
                this.f941 = Collections.emptyList();
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m1098() {
        m1097();
        return !this.f941.contains((Object) null);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m1099() {
        m1097();
        return this.f941.isEmpty();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<String> m1100() {
        m1097();
        return this.f941;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1101(MediaRouteSelector mediaRouteSelector) {
        if (mediaRouteSelector == null) {
            return false;
        }
        m1097();
        mediaRouteSelector.m1097();
        return this.f941.containsAll(mediaRouteSelector.f941);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m1102(List<IntentFilter> list) {
        if (list != null) {
            m1097();
            int size = this.f941.size();
            if (size != 0) {
                int size2 = list.size();
                for (int i = 0; i < size2; i++) {
                    IntentFilter intentFilter = list.get(i);
                    if (intentFilter != null) {
                        for (int i2 = 0; i2 < size; i2++) {
                            if (intentFilter.hasCategory(this.f941.get(i2))) {
                                return true;
                            }
                        }
                        continue;
                    }
                }
            }
        }
        return false;
    }
}
