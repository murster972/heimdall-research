package android.support.v7.cardview;

public final class R {

    public static final class attr {
        public static final int cardBackgroundColor = 2130968701;
        public static final int cardCornerRadius = 2130968702;
        public static final int cardElevation = 2130968703;
        public static final int cardMaxElevation = 2130968704;
        public static final int cardPreventCornerOverlap = 2130968705;
        public static final int cardUseCompatPadding = 2130968706;
        public static final int contentPadding = 2130968768;
        public static final int contentPaddingBottom = 2130968769;
        public static final int contentPaddingLeft = 2130968770;
        public static final int contentPaddingRight = 2130968771;
        public static final int contentPaddingTop = 2130968772;
    }

    public static final class color {
        public static final int cardview_dark_background = 2131099696;
        public static final int cardview_light_background = 2131099697;
        public static final int cardview_shadow_end_color = 2131099698;
        public static final int cardview_shadow_start_color = 2131099699;
    }

    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131165270;
        public static final int cardview_default_elevation = 2131165271;
        public static final int cardview_default_radius = 2131165272;
    }

    public static final class style {
        public static final int Base_CardView = 2131886091;
        public static final int CardView = 2131886250;
        public static final int CardView_Dark = 2131886251;
        public static final int CardView_Light = 2131886252;
    }

    public static final class styleable {
        public static final int[] CardView = {16843071, 16843072, com.typhoon.tv.R.attr.cardBackgroundColor, com.typhoon.tv.R.attr.cardCornerRadius, com.typhoon.tv.R.attr.cardElevation, com.typhoon.tv.R.attr.cardMaxElevation, com.typhoon.tv.R.attr.cardPreventCornerOverlap, com.typhoon.tv.R.attr.cardUseCompatPadding, com.typhoon.tv.R.attr.contentPadding, com.typhoon.tv.R.attr.contentPaddingBottom, com.typhoon.tv.R.attr.contentPaddingLeft, com.typhoon.tv.R.attr.contentPaddingRight, com.typhoon.tv.R.attr.contentPaddingTop};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;
    }
}
