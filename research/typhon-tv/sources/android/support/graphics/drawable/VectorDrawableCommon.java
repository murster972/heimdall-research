package android.support.graphics.drawable;

import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.graphics.drawable.TintAwareDrawable;

abstract class VectorDrawableCommon extends Drawable implements TintAwareDrawable {

    /* renamed from: 靐  reason: contains not printable characters */
    Drawable f83;

    VectorDrawableCommon() {
    }

    public void applyTheme(Resources.Theme theme) {
        if (this.f83 != null) {
            DrawableCompat.applyTheme(this.f83, theme);
        }
    }

    public void clearColorFilter() {
        if (this.f83 != null) {
            this.f83.clearColorFilter();
        } else {
            super.clearColorFilter();
        }
    }

    public ColorFilter getColorFilter() {
        if (this.f83 != null) {
            return DrawableCompat.getColorFilter(this.f83);
        }
        return null;
    }

    public Drawable getCurrent() {
        return this.f83 != null ? this.f83.getCurrent() : super.getCurrent();
    }

    public int getMinimumHeight() {
        return this.f83 != null ? this.f83.getMinimumHeight() : super.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.f83 != null ? this.f83.getMinimumWidth() : super.getMinimumWidth();
    }

    public boolean getPadding(Rect rect) {
        return this.f83 != null ? this.f83.getPadding(rect) : super.getPadding(rect);
    }

    public int[] getState() {
        return this.f83 != null ? this.f83.getState() : super.getState();
    }

    public Region getTransparentRegion() {
        return this.f83 != null ? this.f83.getTransparentRegion() : super.getTransparentRegion();
    }

    public void jumpToCurrentState() {
        if (this.f83 != null) {
            DrawableCompat.jumpToCurrentState(this.f83);
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f83 != null) {
            this.f83.setBounds(rect);
        } else {
            super.onBoundsChange(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f83 != null ? this.f83.setLevel(i) : super.onLevelChange(i);
    }

    public void setChangingConfigurations(int i) {
        if (this.f83 != null) {
            this.f83.setChangingConfigurations(i);
        } else {
            super.setChangingConfigurations(i);
        }
    }

    public void setColorFilter(int i, PorterDuff.Mode mode) {
        if (this.f83 != null) {
            this.f83.setColorFilter(i, mode);
        } else {
            super.setColorFilter(i, mode);
        }
    }

    public void setFilterBitmap(boolean z) {
        if (this.f83 != null) {
            this.f83.setFilterBitmap(z);
        }
    }

    public void setHotspot(float f, float f2) {
        if (this.f83 != null) {
            DrawableCompat.setHotspot(this.f83, f, f2);
        }
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        if (this.f83 != null) {
            DrawableCompat.setHotspotBounds(this.f83, i, i2, i3, i4);
        }
    }

    public boolean setState(int[] iArr) {
        return this.f83 != null ? this.f83.setState(iArr) : super.setState(iArr);
    }
}
