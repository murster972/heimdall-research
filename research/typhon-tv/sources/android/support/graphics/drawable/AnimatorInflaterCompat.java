package android.support.graphics.drawable;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Build;
import android.support.annotation.RestrictTo;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v4.graphics.PathParser;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.util.Xml;
import android.view.InflateException;
import com.mopub.mobileads.VastIconXmlManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@RestrictTo
public class AnimatorInflaterCompat {

    private static class PathDataEvaluator implements TypeEvaluator<PathParser.PathDataNode[]> {

        /* renamed from: 龘  reason: contains not printable characters */
        private PathParser.PathDataNode[] f79;

        private PathDataEvaluator() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PathParser.PathDataNode[] evaluate(float f, PathParser.PathDataNode[] pathDataNodeArr, PathParser.PathDataNode[] pathDataNodeArr2) {
            if (!PathParser.canMorph(pathDataNodeArr, pathDataNodeArr2)) {
                throw new IllegalArgumentException("Can't interpolate between two incompatible pathData");
            }
            if (this.f79 == null || !PathParser.canMorph(this.f79, pathDataNodeArr)) {
                this.f79 = PathParser.deepCopyNodes(pathDataNodeArr);
            }
            for (int i = 0; i < pathDataNodeArr.length; i++) {
                this.f79[i].interpolatePathDataNode(pathDataNodeArr[i], pathDataNodeArr2[i], f);
            }
            return this.f79;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m69(Resources resources, Resources.Theme theme, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        boolean z = false;
        TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f59);
        TypedValue peekNamedValue = TypedArrayUtils.peekNamedValue(obtainAttributes, xmlPullParser, "value", 0);
        if (peekNamedValue != null) {
            z = true;
        }
        int i = (!z || !m86(peekNamedValue.type)) ? 0 : 3;
        obtainAttributes.recycle();
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m70(TypedArray typedArray, int i, int i2) {
        boolean z = true;
        TypedValue peekValue = typedArray.peekValue(i);
        boolean z2 = peekValue != null;
        int i3 = z2 ? peekValue.type : 0;
        TypedValue peekValue2 = typedArray.peekValue(i2);
        if (peekValue2 == null) {
            z = false;
        }
        return ((!z2 || !m86(i3)) && (!z || !m86(z ? peekValue2.type : 0))) ? 0 : 3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Animator m71(Context context, int i) throws Resources.NotFoundException {
        return Build.VERSION.SDK_INT >= 24 ? AnimatorInflater.loadAnimator(context, i) : m72(context, context.getResources(), context.getTheme(), i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Animator m72(Context context, Resources resources, Resources.Theme theme, int i) throws Resources.NotFoundException {
        return m73(context, resources, theme, i, 1.0f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Animator m73(Context context, Resources resources, Resources.Theme theme, int i, float f) throws Resources.NotFoundException {
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = resources.getAnimation(i);
            Animator r0 = m74(context, resources, theme, (XmlPullParser) xmlResourceParser, f);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            return r0;
        } catch (XmlPullParserException e) {
            Resources.NotFoundException notFoundException = new Resources.NotFoundException("Can't load animation resource ID #0x" + Integer.toHexString(i));
            notFoundException.initCause(e);
            throw notFoundException;
        } catch (IOException e2) {
            Resources.NotFoundException notFoundException2 = new Resources.NotFoundException("Can't load animation resource ID #0x" + Integer.toHexString(i));
            notFoundException2.initCause(e2);
            throw notFoundException2;
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Animator m74(Context context, Resources resources, Resources.Theme theme, XmlPullParser xmlPullParser, float f) throws XmlPullParserException, IOException {
        return m75(context, resources, theme, xmlPullParser, Xml.asAttributeSet(xmlPullParser), (AnimatorSet) null, 0, f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Animator m75(Context context, Resources resources, Resources.Theme theme, XmlPullParser xmlPullParser, AttributeSet attributeSet, AnimatorSet animatorSet, int i, float f) throws XmlPullParserException, IOException {
        Animator animator = null;
        ArrayList arrayList = null;
        int depth = xmlPullParser.getDepth();
        while (true) {
            int next = xmlPullParser.next();
            if ((next != 3 || xmlPullParser.getDepth() > depth) && next != 1) {
                if (next == 2) {
                    String name = xmlPullParser.getName();
                    boolean z = false;
                    if (name.equals("objectAnimator")) {
                        animator = m78(context, resources, theme, attributeSet, f, xmlPullParser);
                    } else if (name.equals("animator")) {
                        animator = m81(context, resources, theme, attributeSet, (ValueAnimator) null, f, xmlPullParser);
                    } else if (name.equals("set")) {
                        animator = new AnimatorSet();
                        TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f56);
                        Context context2 = context;
                        Resources resources2 = resources;
                        Resources.Theme theme2 = theme;
                        XmlPullParser xmlPullParser2 = xmlPullParser;
                        AttributeSet attributeSet2 = attributeSet;
                        m75(context2, resources2, theme2, xmlPullParser2, attributeSet2, (AnimatorSet) animator, TypedArrayUtils.getNamedInt(obtainAttributes, xmlPullParser, "ordering", 0, 0), f);
                        obtainAttributes.recycle();
                    } else if (name.equals("propertyValuesHolder")) {
                        PropertyValuesHolder[] r22 = m87(context, resources, theme, xmlPullParser, Xml.asAttributeSet(xmlPullParser));
                        if (!(r22 == null || animator == null || !(animator instanceof ValueAnimator))) {
                            ((ValueAnimator) animator).setValues(r22);
                        }
                        z = true;
                    } else {
                        throw new RuntimeException("Unknown animator name: " + xmlPullParser.getName());
                    }
                    if (animatorSet != null && !z) {
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        arrayList.add(animator);
                    }
                }
            }
        }
        if (!(animatorSet == null || arrayList == null)) {
            Animator[] animatorArr = new Animator[arrayList.size()];
            int i2 = 0;
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                animatorArr[i2] = (Animator) it2.next();
                i2++;
            }
            if (i == 0) {
                animatorSet.playTogether(animatorArr);
            } else {
                animatorSet.playSequentially(animatorArr);
            }
        }
        return animator;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Keyframe m76(Keyframe keyframe, float f) {
        return keyframe.getType() == Float.TYPE ? Keyframe.ofFloat(f) : keyframe.getType() == Integer.TYPE ? Keyframe.ofInt(f) : Keyframe.ofObject(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Keyframe m77(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, int i, XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f59);
        Keyframe keyframe = null;
        float namedFloat = TypedArrayUtils.getNamedFloat(obtainAttributes, xmlPullParser, "fraction", 3, -1.0f);
        TypedValue peekNamedValue = TypedArrayUtils.peekNamedValue(obtainAttributes, xmlPullParser, "value", 0);
        boolean z = peekNamedValue != null;
        if (i == 4) {
            i = (!z || !m86(peekNamedValue.type)) ? 0 : 3;
        }
        if (z) {
            switch (i) {
                case 0:
                    keyframe = Keyframe.ofFloat(namedFloat, TypedArrayUtils.getNamedFloat(obtainAttributes, xmlPullParser, "value", 0, 0.0f));
                    break;
                case 1:
                case 3:
                    keyframe = Keyframe.ofInt(namedFloat, TypedArrayUtils.getNamedInt(obtainAttributes, xmlPullParser, "value", 0, 0));
                    break;
            }
        } else {
            keyframe = i == 0 ? Keyframe.ofFloat(namedFloat) : Keyframe.ofInt(namedFloat);
        }
        int namedResourceId = TypedArrayUtils.getNamedResourceId(obtainAttributes, xmlPullParser, "interpolator", 1, 0);
        if (namedResourceId > 0) {
            keyframe.setInterpolator(AnimationUtilsCompat.m67(context, namedResourceId));
        }
        obtainAttributes.recycle();
        return keyframe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ObjectAnimator m78(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, float f, XmlPullParser xmlPullParser) throws Resources.NotFoundException {
        ObjectAnimator objectAnimator = new ObjectAnimator();
        m81(context, resources, theme, attributeSet, objectAnimator, f, xmlPullParser);
        return objectAnimator;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x012e  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.animation.PropertyValuesHolder m79(android.content.Context r25, android.content.res.Resources r26, android.content.res.Resources.Theme r27, org.xmlpull.v1.XmlPullParser r28, java.lang.String r29, int r30) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            r24 = 0
            r18 = 0
        L_0x0004:
            int r23 = r28.next()
            r3 = 3
            r0 = r23
            if (r0 == r3) goto L_0x005a
            r3 = 1
            r0 = r23
            if (r0 == r3) goto L_0x005a
            java.lang.String r20 = r28.getName()
            java.lang.String r3 = "keyframe"
            r0 = r20
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x0004
            r3 = 4
            r0 = r30
            if (r0 != r3) goto L_0x0034
            android.util.AttributeSet r3 = android.util.Xml.asAttributeSet(r28)
            r0 = r26
            r1 = r27
            r2 = r28
            int r30 = m69((android.content.res.Resources) r0, (android.content.res.Resources.Theme) r1, (android.util.AttributeSet) r3, (org.xmlpull.v1.XmlPullParser) r2)
        L_0x0034:
            android.util.AttributeSet r6 = android.util.Xml.asAttributeSet(r28)
            r3 = r25
            r4 = r26
            r5 = r27
            r7 = r30
            r8 = r28
            android.animation.Keyframe r16 = m77((android.content.Context) r3, (android.content.res.Resources) r4, (android.content.res.Resources.Theme) r5, (android.util.AttributeSet) r6, (int) r7, (org.xmlpull.v1.XmlPullParser) r8)
            if (r16 == 0) goto L_0x0056
            if (r18 != 0) goto L_0x004f
            java.util.ArrayList r18 = new java.util.ArrayList
            r18.<init>()
        L_0x004f:
            r0 = r18
            r1 = r16
            r0.add(r1)
        L_0x0056:
            r28.next()
            goto L_0x0004
        L_0x005a:
            if (r18 == 0) goto L_0x0137
            int r9 = r18.size()
            if (r9 <= 0) goto L_0x0137
            r3 = 0
            r0 = r18
            java.lang.Object r12 = r0.get(r3)
            android.animation.Keyframe r12 = (android.animation.Keyframe) r12
            int r3 = r9 + -1
            r0 = r18
            java.lang.Object r19 = r0.get(r3)
            android.animation.Keyframe r19 = (android.animation.Keyframe) r19
            float r10 = r19.getFraction()
            r3 = 1065353216(0x3f800000, float:1.0)
            int r3 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r3 >= 0) goto L_0x008b
            r3 = 0
            int r3 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r3 >= 0) goto L_0x00c1
            r3 = 1065353216(0x3f800000, float:1.0)
            r0 = r19
            r0.setFraction(r3)
        L_0x008b:
            float r21 = r12.getFraction()
            r3 = 0
            int r3 = (r21 > r3 ? 1 : (r21 == r3 ? 0 : -1))
            if (r3 == 0) goto L_0x009d
            r3 = 0
            int r3 = (r21 > r3 ? 1 : (r21 == r3 ? 0 : -1))
            if (r3 >= 0) goto L_0x00d5
            r3 = 0
            r12.setFraction(r3)
        L_0x009d:
            android.animation.Keyframe[] r0 = new android.animation.Keyframe[r9]
            r17 = r0
            r0 = r18
            r1 = r17
            r0.toArray(r1)
            r14 = 0
        L_0x00a9:
            if (r14 >= r9) goto L_0x0121
            r16 = r17[r14]
            float r3 = r16.getFraction()
            r4 = 0
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 >= 0) goto L_0x00be
            if (r14 != 0) goto L_0x00e3
            r3 = 0
            r0 = r16
            r0.setFraction(r3)
        L_0x00be:
            int r14 = r14 + 1
            goto L_0x00a9
        L_0x00c1:
            int r3 = r18.size()
            r4 = 1065353216(0x3f800000, float:1.0)
            r0 = r19
            android.animation.Keyframe r4 = m76((android.animation.Keyframe) r0, (float) r4)
            r0 = r18
            r0.add(r3, r4)
            int r9 = r9 + 1
            goto L_0x008b
        L_0x00d5:
            r3 = 0
            r4 = 0
            android.animation.Keyframe r4 = m76((android.animation.Keyframe) r12, (float) r4)
            r0 = r18
            r0.add(r3, r4)
            int r9 = r9 + 1
            goto L_0x009d
        L_0x00e3:
            int r3 = r9 + -1
            if (r14 != r3) goto L_0x00ef
            r3 = 1065353216(0x3f800000, float:1.0)
            r0 = r16
            r0.setFraction(r3)
            goto L_0x00be
        L_0x00ef:
            r22 = r14
            r11 = r14
            int r15 = r22 + 1
        L_0x00f4:
            int r3 = r9 + -1
            if (r15 >= r3) goto L_0x0103
            r3 = r17[r15]
            float r3 = r3.getFraction()
            r4 = 0
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 < 0) goto L_0x011d
        L_0x0103:
            int r3 = r11 + 1
            r3 = r17[r3]
            float r3 = r3.getFraction()
            int r4 = r22 + -1
            r4 = r17[r4]
            float r4 = r4.getFraction()
            float r13 = r3 - r4
            r0 = r17
            r1 = r22
            m85((android.animation.Keyframe[]) r0, (float) r13, (int) r1, (int) r11)
            goto L_0x00be
        L_0x011d:
            r11 = r15
            int r15 = r15 + 1
            goto L_0x00f4
        L_0x0121:
            r0 = r29
            r1 = r17
            android.animation.PropertyValuesHolder r24 = android.animation.PropertyValuesHolder.ofKeyframe(r0, r1)
            r3 = 3
            r0 = r30
            if (r0 != r3) goto L_0x0137
            android.support.graphics.drawable.ArgbEvaluator r3 = android.support.graphics.drawable.ArgbEvaluator.m89()
            r0 = r24
            r0.setEvaluator(r3)
        L_0x0137:
            return r24
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.graphics.drawable.AnimatorInflaterCompat.m79(android.content.Context, android.content.res.Resources, android.content.res.Resources$Theme, org.xmlpull.v1.XmlPullParser, java.lang.String, int):android.animation.PropertyValuesHolder");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static PropertyValuesHolder m80(TypedArray typedArray, int i, int i2, int i3, String str) {
        TypedValue peekValue = typedArray.peekValue(i2);
        boolean z = peekValue != null;
        int i4 = z ? peekValue.type : 0;
        TypedValue peekValue2 = typedArray.peekValue(i3);
        boolean z2 = peekValue2 != null;
        int i5 = z2 ? peekValue2.type : 0;
        if (i == 4) {
            i = ((!z || !m86(i4)) && (!z2 || !m86(i5))) ? 0 : 3;
        }
        boolean z3 = i == 0;
        PropertyValuesHolder propertyValuesHolder = null;
        if (i == 2) {
            String string = typedArray.getString(i2);
            String string2 = typedArray.getString(i3);
            PathParser.PathDataNode[] createNodesFromPathData = PathParser.createNodesFromPathData(string);
            PathParser.PathDataNode[] createNodesFromPathData2 = PathParser.createNodesFromPathData(string2);
            if (createNodesFromPathData == null && createNodesFromPathData2 == null) {
                return null;
            }
            if (createNodesFromPathData != null) {
                PathDataEvaluator pathDataEvaluator = new PathDataEvaluator();
                if (createNodesFromPathData2 == null) {
                    return PropertyValuesHolder.ofObject(str, pathDataEvaluator, new Object[]{createNodesFromPathData});
                } else if (!PathParser.canMorph(createNodesFromPathData, createNodesFromPathData2)) {
                    throw new InflateException(" Can't morph from " + string + " to " + string2);
                } else {
                    return PropertyValuesHolder.ofObject(str, pathDataEvaluator, new Object[]{createNodesFromPathData, createNodesFromPathData2});
                }
            } else if (createNodesFromPathData2 == null) {
                return null;
            } else {
                return PropertyValuesHolder.ofObject(str, new PathDataEvaluator(), new Object[]{createNodesFromPathData2});
            }
        } else {
            ArgbEvaluator argbEvaluator = null;
            if (i == 3) {
                argbEvaluator = ArgbEvaluator.m89();
            }
            if (z3) {
                if (z) {
                    float dimension = i4 == 5 ? typedArray.getDimension(i2, 0.0f) : typedArray.getFloat(i2, 0.0f);
                    if (z2) {
                        propertyValuesHolder = PropertyValuesHolder.ofFloat(str, new float[]{dimension, i5 == 5 ? typedArray.getDimension(i3, 0.0f) : typedArray.getFloat(i3, 0.0f)});
                    } else {
                        propertyValuesHolder = PropertyValuesHolder.ofFloat(str, new float[]{dimension});
                    }
                } else {
                    propertyValuesHolder = PropertyValuesHolder.ofFloat(str, new float[]{i5 == 5 ? typedArray.getDimension(i3, 0.0f) : typedArray.getFloat(i3, 0.0f)});
                }
            } else if (z) {
                int dimension2 = i4 == 5 ? (int) typedArray.getDimension(i2, 0.0f) : m86(i4) ? typedArray.getColor(i2, 0) : typedArray.getInt(i2, 0);
                if (z2) {
                    propertyValuesHolder = PropertyValuesHolder.ofInt(str, new int[]{dimension2, i5 == 5 ? (int) typedArray.getDimension(i3, 0.0f) : m86(i5) ? typedArray.getColor(i3, 0) : typedArray.getInt(i3, 0)});
                } else {
                    propertyValuesHolder = PropertyValuesHolder.ofInt(str, new int[]{dimension2});
                }
            } else if (z2) {
                propertyValuesHolder = PropertyValuesHolder.ofInt(str, new int[]{i5 == 5 ? (int) typedArray.getDimension(i3, 0.0f) : m86(i5) ? typedArray.getColor(i3, 0) : typedArray.getInt(i3, 0)});
            }
            if (propertyValuesHolder == null || argbEvaluator == null) {
                return propertyValuesHolder;
            }
            propertyValuesHolder.setEvaluator(argbEvaluator);
            return propertyValuesHolder;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ValueAnimator m81(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, ValueAnimator valueAnimator, float f, XmlPullParser xmlPullParser) throws Resources.NotFoundException {
        TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f55);
        TypedArray obtainAttributes2 = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f60);
        if (valueAnimator == null) {
            valueAnimator = new ValueAnimator();
        }
        m83(valueAnimator, obtainAttributes, obtainAttributes2, f, xmlPullParser);
        int namedResourceId = TypedArrayUtils.getNamedResourceId(obtainAttributes, xmlPullParser, "interpolator", 0, 0);
        if (namedResourceId > 0) {
            valueAnimator.setInterpolator(AnimationUtilsCompat.m67(context, namedResourceId));
        }
        obtainAttributes.recycle();
        if (obtainAttributes2 != null) {
            obtainAttributes2.recycle();
        }
        return valueAnimator;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m82(ValueAnimator valueAnimator, TypedArray typedArray, int i, float f, XmlPullParser xmlPullParser) {
        ObjectAnimator objectAnimator = (ObjectAnimator) valueAnimator;
        String namedString = TypedArrayUtils.getNamedString(typedArray, xmlPullParser, "pathData", 1);
        if (namedString != null) {
            String namedString2 = TypedArrayUtils.getNamedString(typedArray, xmlPullParser, "propertyXName", 2);
            String namedString3 = TypedArrayUtils.getNamedString(typedArray, xmlPullParser, "propertyYName", 3);
            if (i == 2 || i == 4) {
            }
            if (namedString2 == null && namedString3 == null) {
                throw new InflateException(typedArray.getPositionDescription() + " propertyXName or propertyYName is needed for PathData");
            }
            m84(PathParser.createPathFromPathData(namedString), objectAnimator, 0.5f * f, namedString2, namedString3);
            return;
        }
        objectAnimator.setPropertyName(TypedArrayUtils.getNamedString(typedArray, xmlPullParser, "propertyName", 0));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m83(ValueAnimator valueAnimator, TypedArray typedArray, TypedArray typedArray2, float f, XmlPullParser xmlPullParser) {
        long namedInt = (long) TypedArrayUtils.getNamedInt(typedArray, xmlPullParser, VastIconXmlManager.DURATION, 1, 300);
        long namedInt2 = (long) TypedArrayUtils.getNamedInt(typedArray, xmlPullParser, "startOffset", 2, 0);
        int namedInt3 = TypedArrayUtils.getNamedInt(typedArray, xmlPullParser, "valueType", 7, 4);
        if (TypedArrayUtils.hasAttribute(xmlPullParser, "valueFrom") && TypedArrayUtils.hasAttribute(xmlPullParser, "valueTo")) {
            if (namedInt3 == 4) {
                namedInt3 = m70(typedArray, 5, 6);
            }
            PropertyValuesHolder r2 = m80(typedArray, namedInt3, 5, 6, "");
            if (r2 != null) {
                valueAnimator.setValues(new PropertyValuesHolder[]{r2});
            }
        }
        valueAnimator.setDuration(namedInt);
        valueAnimator.setStartDelay(namedInt2);
        valueAnimator.setRepeatCount(TypedArrayUtils.getNamedInt(typedArray, xmlPullParser, "repeatCount", 3, 0));
        valueAnimator.setRepeatMode(TypedArrayUtils.getNamedInt(typedArray, xmlPullParser, "repeatMode", 4, 1));
        if (typedArray2 != null) {
            m82(valueAnimator, typedArray2, namedInt3, f, xmlPullParser);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m84(Path path, ObjectAnimator objectAnimator, float f, String str, String str2) {
        PathMeasure pathMeasure = new PathMeasure(path, false);
        float f2 = 0.0f;
        ArrayList arrayList = new ArrayList();
        arrayList.add(Float.valueOf(0.0f));
        do {
            f2 += pathMeasure.getLength();
            arrayList.add(Float.valueOf(f2));
        } while (pathMeasure.nextContour());
        PathMeasure pathMeasure2 = new PathMeasure(path, false);
        int min = Math.min(100, ((int) (f2 / f)) + 1);
        float[] fArr = new float[min];
        float[] fArr2 = new float[min];
        float[] fArr3 = new float[2];
        int i = 0;
        float f3 = f2 / ((float) (min - 1));
        float f4 = 0.0f;
        for (int i2 = 0; i2 < min; i2++) {
            pathMeasure2.getPosTan(f4, fArr3, (float[]) null);
            pathMeasure2.getPosTan(f4, fArr3, (float[]) null);
            fArr[i2] = fArr3[0];
            fArr2[i2] = fArr3[1];
            f4 += f3;
            if (i + 1 < arrayList.size() && f4 > ((Float) arrayList.get(i + 1)).floatValue()) {
                f4 -= ((Float) arrayList.get(i + 1)).floatValue();
                i++;
                pathMeasure2.nextContour();
            }
        }
        PropertyValuesHolder propertyValuesHolder = null;
        PropertyValuesHolder propertyValuesHolder2 = null;
        if (str != null) {
            propertyValuesHolder = PropertyValuesHolder.ofFloat(str, fArr);
        }
        if (str2 != null) {
            propertyValuesHolder2 = PropertyValuesHolder.ofFloat(str2, fArr2);
        }
        if (propertyValuesHolder == null) {
            objectAnimator.setValues(new PropertyValuesHolder[]{propertyValuesHolder2});
        } else if (propertyValuesHolder2 == null) {
            objectAnimator.setValues(new PropertyValuesHolder[]{propertyValuesHolder});
        } else {
            objectAnimator.setValues(new PropertyValuesHolder[]{propertyValuesHolder, propertyValuesHolder2});
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m85(Keyframe[] keyframeArr, float f, int i, int i2) {
        float f2 = f / ((float) ((i2 - i) + 2));
        for (int i3 = i; i3 <= i2; i3++) {
            keyframeArr[i3].setFraction(keyframeArr[i3 - 1].getFraction() + f2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m86(int i) {
        return i >= 28 && i <= 31;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static PropertyValuesHolder[] m87(Context context, Resources resources, Resources.Theme theme, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        ArrayList arrayList = null;
        while (true) {
            int eventType = xmlPullParser.getEventType();
            if (eventType == 3 || eventType == 1) {
                PropertyValuesHolder[] propertyValuesHolderArr = null;
            } else if (eventType != 2) {
                xmlPullParser.next();
            } else {
                if (xmlPullParser.getName().equals("propertyValuesHolder")) {
                    TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f58);
                    String namedString = TypedArrayUtils.getNamedString(obtainAttributes, xmlPullParser, "propertyName", 3);
                    int namedInt = TypedArrayUtils.getNamedInt(obtainAttributes, xmlPullParser, "valueType", 2, 4);
                    PropertyValuesHolder r13 = m79(context, resources, theme, xmlPullParser, namedString, namedInt);
                    if (r13 == null) {
                        r13 = m80(obtainAttributes, namedInt, 0, 1, namedString);
                    }
                    if (r13 != null) {
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        arrayList.add(r13);
                    }
                    obtainAttributes.recycle();
                }
                xmlPullParser.next();
            }
        }
        PropertyValuesHolder[] propertyValuesHolderArr2 = null;
        if (arrayList != null) {
            int size = arrayList.size();
            propertyValuesHolderArr2 = new PropertyValuesHolder[size];
            for (int i = 0; i < size; i++) {
                propertyValuesHolderArr2[i] = (PropertyValuesHolder) arrayList.get(i);
            }
        }
        return propertyValuesHolderArr2;
    }
}
