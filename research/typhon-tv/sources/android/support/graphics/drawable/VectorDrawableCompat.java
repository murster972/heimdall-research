package android.support.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v4.graphics.PathParser;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class VectorDrawableCompat extends VectorDrawableCommon {

    /* renamed from: 龘  reason: contains not printable characters */
    static final PorterDuff.Mode f84 = PorterDuff.Mode.SRC_IN;

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f85;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f86;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Drawable.ConstantState f87;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final float[] f88;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Matrix f89;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final Rect f90;

    /* renamed from: 连任  reason: contains not printable characters */
    private ColorFilter f91;

    /* renamed from: 麤  reason: contains not printable characters */
    private PorterDuffColorFilter f92;

    /* renamed from: 齉  reason: contains not printable characters */
    private VectorDrawableCompatState f93;

    private static class VClipPath extends VPath {
        public VClipPath() {
        }

        public VClipPath(VClipPath vClipPath) {
            super(vClipPath);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m104(TypedArray typedArray) {
            String string = typedArray.getString(0);
            if (string != null) {
                this.f121 = string;
            }
            String string2 = typedArray.getString(1);
            if (string2 != null) {
                this.f120 = PathParser.createNodesFromPathData(string2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m105(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            if (TypedArrayUtils.hasAttribute(xmlPullParser, "pathData")) {
                TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f63);
                m104(obtainAttributes);
                obtainAttributes.recycle();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m106() {
            return true;
        }
    }

    private static class VFullPath extends VPath {

        /* renamed from: ʻ  reason: contains not printable characters */
        float f94 = 1.0f;

        /* renamed from: ʼ  reason: contains not printable characters */
        float f95 = 0.0f;

        /* renamed from: ʽ  reason: contains not printable characters */
        float f96 = 1.0f;

        /* renamed from: ˈ  reason: contains not printable characters */
        float f97 = 4.0f;

        /* renamed from: ˑ  reason: contains not printable characters */
        float f98 = 0.0f;

        /* renamed from: ٴ  reason: contains not printable characters */
        Paint.Cap f99 = Paint.Cap.BUTT;

        /* renamed from: ᐧ  reason: contains not printable characters */
        Paint.Join f100 = Paint.Join.MITER;

        /* renamed from: 连任  reason: contains not printable characters */
        int f101 = 0;

        /* renamed from: 靐  reason: contains not printable characters */
        float f102 = 0.0f;

        /* renamed from: 麤  reason: contains not printable characters */
        float f103 = 1.0f;

        /* renamed from: 齉  reason: contains not printable characters */
        int f104 = 0;

        /* renamed from: 龘  reason: contains not printable characters */
        int f105 = 0;

        /* renamed from: ﾞ  reason: contains not printable characters */
        private int[] f106;

        public VFullPath() {
        }

        public VFullPath(VFullPath vFullPath) {
            super(vFullPath);
            this.f106 = vFullPath.f106;
            this.f105 = vFullPath.f105;
            this.f102 = vFullPath.f102;
            this.f103 = vFullPath.f103;
            this.f104 = vFullPath.f104;
            this.f101 = vFullPath.f101;
            this.f94 = vFullPath.f94;
            this.f95 = vFullPath.f95;
            this.f96 = vFullPath.f96;
            this.f98 = vFullPath.f98;
            this.f99 = vFullPath.f99;
            this.f100 = vFullPath.f100;
            this.f97 = vFullPath.f97;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Paint.Cap m107(int i, Paint.Cap cap) {
            switch (i) {
                case 0:
                    return Paint.Cap.BUTT;
                case 1:
                    return Paint.Cap.ROUND;
                case 2:
                    return Paint.Cap.SQUARE;
                default:
                    return cap;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Paint.Join m108(int i, Paint.Join join) {
            switch (i) {
                case 0:
                    return Paint.Join.MITER;
                case 1:
                    return Paint.Join.ROUND;
                case 2:
                    return Paint.Join.BEVEL;
                default:
                    return join;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m109(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.f106 = null;
            if (TypedArrayUtils.hasAttribute(xmlPullParser, "pathData")) {
                String string = typedArray.getString(0);
                if (string != null) {
                    this.f121 = string;
                }
                String string2 = typedArray.getString(2);
                if (string2 != null) {
                    this.f120 = PathParser.createNodesFromPathData(string2);
                }
                this.f104 = TypedArrayUtils.getNamedColor(typedArray, xmlPullParser, "fillColor", 1, this.f104);
                this.f94 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "fillAlpha", 12, this.f94);
                this.f99 = m107(TypedArrayUtils.getNamedInt(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.f99);
                this.f100 = m108(TypedArrayUtils.getNamedInt(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.f100);
                this.f97 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.f97);
                this.f105 = TypedArrayUtils.getNamedColor(typedArray, xmlPullParser, "strokeColor", 3, this.f105);
                this.f103 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "strokeAlpha", 11, this.f103);
                this.f102 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "strokeWidth", 4, this.f102);
                this.f96 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "trimPathEnd", 6, this.f96);
                this.f98 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "trimPathOffset", 7, this.f98);
                this.f95 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "trimPathStart", 5, this.f95);
                this.f101 = TypedArrayUtils.getNamedInt(typedArray, xmlPullParser, "fillType", 13, this.f101);
            }
        }

        /* access modifiers changed from: package-private */
        public float getFillAlpha() {
            return this.f94;
        }

        /* access modifiers changed from: package-private */
        public int getFillColor() {
            return this.f104;
        }

        /* access modifiers changed from: package-private */
        public float getStrokeAlpha() {
            return this.f103;
        }

        /* access modifiers changed from: package-private */
        public int getStrokeColor() {
            return this.f105;
        }

        /* access modifiers changed from: package-private */
        public float getStrokeWidth() {
            return this.f102;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathEnd() {
            return this.f96;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathOffset() {
            return this.f98;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathStart() {
            return this.f95;
        }

        /* access modifiers changed from: package-private */
        public void setFillAlpha(float f) {
            this.f94 = f;
        }

        /* access modifiers changed from: package-private */
        public void setFillColor(int i) {
            this.f104 = i;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeAlpha(float f) {
            this.f103 = f;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeColor(int i) {
            this.f105 = i;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeWidth(float f) {
            this.f102 = f;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathEnd(float f) {
            this.f96 = f;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathOffset(float f) {
            this.f98 = f;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathStart(float f) {
            this.f95 = f;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m110(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f64);
            m109(obtainAttributes, xmlPullParser);
            obtainAttributes.recycle();
        }
    }

    private static class VGroup {

        /* renamed from: ʻ  reason: contains not printable characters */
        private float f107 = 0.0f;

        /* renamed from: ʼ  reason: contains not printable characters */
        private float f108 = 1.0f;

        /* renamed from: ʽ  reason: contains not printable characters */
        private float f109 = 1.0f;

        /* renamed from: ʾ  reason: contains not printable characters */
        private String f110 = null;

        /* renamed from: ˈ  reason: contains not printable characters */
        private int[] f111;

        /* renamed from: ˑ  reason: contains not printable characters */
        private float f112 = 0.0f;

        /* renamed from: ٴ  reason: contains not printable characters */
        private float f113 = 0.0f;
        /* access modifiers changed from: private */

        /* renamed from: ᐧ  reason: contains not printable characters */
        public final Matrix f114 = new Matrix();

        /* renamed from: 连任  reason: contains not printable characters */
        private float f115 = 0.0f;

        /* renamed from: 靐  reason: contains not printable characters */
        float f116 = 0.0f;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public final Matrix f117 = new Matrix();

        /* renamed from: 齉  reason: contains not printable characters */
        int f118;

        /* renamed from: 龘  reason: contains not printable characters */
        final ArrayList<Object> f119 = new ArrayList<>();

        public VGroup() {
        }

        public VGroup(VGroup vGroup, ArrayMap<String, Object> arrayMap) {
            VPath vClipPath;
            this.f116 = vGroup.f116;
            this.f115 = vGroup.f115;
            this.f107 = vGroup.f107;
            this.f108 = vGroup.f108;
            this.f109 = vGroup.f109;
            this.f112 = vGroup.f112;
            this.f113 = vGroup.f113;
            this.f111 = vGroup.f111;
            this.f110 = vGroup.f110;
            this.f118 = vGroup.f118;
            if (this.f110 != null) {
                arrayMap.put(this.f110, this);
            }
            this.f114.set(vGroup.f114);
            ArrayList<Object> arrayList = vGroup.f119;
            for (int i = 0; i < arrayList.size(); i++) {
                Object obj = arrayList.get(i);
                if (obj instanceof VGroup) {
                    this.f119.add(new VGroup((VGroup) obj, arrayMap));
                } else {
                    if (obj instanceof VFullPath) {
                        vClipPath = new VFullPath((VFullPath) obj);
                    } else if (obj instanceof VClipPath) {
                        vClipPath = new VClipPath((VClipPath) obj);
                    } else {
                        throw new IllegalStateException("Unknown object in the tree!");
                    }
                    this.f119.add(vClipPath);
                    if (vClipPath.f121 != null) {
                        arrayMap.put(vClipPath.f121, vClipPath);
                    }
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m113() {
            this.f114.reset();
            this.f114.postTranslate(-this.f115, -this.f107);
            this.f114.postScale(this.f108, this.f109);
            this.f114.postRotate(this.f116, 0.0f, 0.0f);
            this.f114.postTranslate(this.f112 + this.f115, this.f113 + this.f107);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m114(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.f111 = null;
            this.f116 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "rotation", 5, this.f116);
            this.f115 = typedArray.getFloat(1, this.f115);
            this.f107 = typedArray.getFloat(2, this.f107);
            this.f108 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "scaleX", 3, this.f108);
            this.f109 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "scaleY", 4, this.f109);
            this.f112 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "translateX", 6, this.f112);
            this.f113 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "translateY", 7, this.f113);
            String string = typedArray.getString(0);
            if (string != null) {
                this.f110 = string;
            }
            m113();
        }

        public String getGroupName() {
            return this.f110;
        }

        public Matrix getLocalMatrix() {
            return this.f114;
        }

        public float getPivotX() {
            return this.f115;
        }

        public float getPivotY() {
            return this.f107;
        }

        public float getRotation() {
            return this.f116;
        }

        public float getScaleX() {
            return this.f108;
        }

        public float getScaleY() {
            return this.f109;
        }

        public float getTranslateX() {
            return this.f112;
        }

        public float getTranslateY() {
            return this.f113;
        }

        public void setPivotX(float f) {
            if (f != this.f115) {
                this.f115 = f;
                m113();
            }
        }

        public void setPivotY(float f) {
            if (f != this.f107) {
                this.f107 = f;
                m113();
            }
        }

        public void setRotation(float f) {
            if (f != this.f116) {
                this.f116 = f;
                m113();
            }
        }

        public void setScaleX(float f) {
            if (f != this.f108) {
                this.f108 = f;
                m113();
            }
        }

        public void setScaleY(float f) {
            if (f != this.f109) {
                this.f109 = f;
                m113();
            }
        }

        public void setTranslateX(float f) {
            if (f != this.f112) {
                this.f112 = f;
                m113();
            }
        }

        public void setTranslateY(float f) {
            if (f != this.f113) {
                this.f113 = f;
                m113();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m115(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f62);
            m114(obtainAttributes, xmlPullParser);
            obtainAttributes.recycle();
        }
    }

    private static class VPath {

        /* renamed from: ʾ  reason: contains not printable characters */
        protected PathParser.PathDataNode[] f120 = null;

        /* renamed from: ʿ  reason: contains not printable characters */
        String f121;

        /* renamed from: ﹶ  reason: contains not printable characters */
        int f122;

        public VPath() {
        }

        public VPath(VPath vPath) {
            this.f121 = vPath.f121;
            this.f122 = vPath.f122;
            this.f120 = PathParser.deepCopyNodes(vPath.f120);
        }

        public PathParser.PathDataNode[] getPathData() {
            return this.f120;
        }

        public String getPathName() {
            return this.f121;
        }

        public void setPathData(PathParser.PathDataNode[] pathDataNodeArr) {
            if (!PathParser.canMorph(this.f120, pathDataNodeArr)) {
                this.f120 = PathParser.deepCopyNodes(pathDataNodeArr);
            } else {
                PathParser.updateNodes(this.f120, pathDataNodeArr);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m116(Path path) {
            path.reset();
            if (this.f120 != null) {
                PathParser.PathDataNode.nodesToPath(this.f120, path);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m117() {
            return false;
        }
    }

    private static class VPathRenderer {

        /* renamed from: ᐧ  reason: contains not printable characters */
        private static final Matrix f123 = new Matrix();

        /* renamed from: ʻ  reason: contains not printable characters */
        int f124;

        /* renamed from: ʼ  reason: contains not printable characters */
        String f125;

        /* renamed from: ʽ  reason: contains not printable characters */
        final ArrayMap<String, Object> f126;
        /* access modifiers changed from: private */

        /* renamed from: ʾ  reason: contains not printable characters */
        public Paint f127;
        /* access modifiers changed from: private */

        /* renamed from: ʿ  reason: contains not printable characters */
        public Paint f128;

        /* renamed from: ˈ  reason: contains not printable characters */
        private final Matrix f129;

        /* renamed from: ˑ  reason: contains not printable characters */
        private final Path f130;

        /* renamed from: ٴ  reason: contains not printable characters */
        private final Path f131;

        /* renamed from: 连任  reason: contains not printable characters */
        float f132;

        /* renamed from: 靐  reason: contains not printable characters */
        float f133;

        /* renamed from: 麤  reason: contains not printable characters */
        float f134;

        /* renamed from: 齉  reason: contains not printable characters */
        float f135;

        /* renamed from: 龘  reason: contains not printable characters */
        final VGroup f136;

        /* renamed from: ﹶ  reason: contains not printable characters */
        private PathMeasure f137;

        /* renamed from: ﾞ  reason: contains not printable characters */
        private int f138;

        public VPathRenderer() {
            this.f129 = new Matrix();
            this.f133 = 0.0f;
            this.f135 = 0.0f;
            this.f134 = 0.0f;
            this.f132 = 0.0f;
            this.f124 = 255;
            this.f125 = null;
            this.f126 = new ArrayMap<>();
            this.f136 = new VGroup();
            this.f130 = new Path();
            this.f131 = new Path();
        }

        public VPathRenderer(VPathRenderer vPathRenderer) {
            this.f129 = new Matrix();
            this.f133 = 0.0f;
            this.f135 = 0.0f;
            this.f134 = 0.0f;
            this.f132 = 0.0f;
            this.f124 = 255;
            this.f125 = null;
            this.f126 = new ArrayMap<>();
            this.f136 = new VGroup(vPathRenderer.f136, this.f126);
            this.f130 = new Path(vPathRenderer.f130);
            this.f131 = new Path(vPathRenderer.f131);
            this.f133 = vPathRenderer.f133;
            this.f135 = vPathRenderer.f135;
            this.f134 = vPathRenderer.f134;
            this.f132 = vPathRenderer.f132;
            this.f138 = vPathRenderer.f138;
            this.f124 = vPathRenderer.f124;
            this.f125 = vPathRenderer.f125;
            if (vPathRenderer.f125 != null) {
                this.f126.put(vPathRenderer.f125, this);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static float m120(float f, float f2, float f3, float f4) {
            return (f * f4) - (f2 * f3);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private float m121(Matrix matrix) {
            float[] fArr = {0.0f, 1.0f, 1.0f, 0.0f};
            matrix.mapVectors(fArr);
            float hypot = (float) Math.hypot((double) fArr[0], (double) fArr[1]);
            float hypot2 = (float) Math.hypot((double) fArr[2], (double) fArr[3]);
            float r0 = m120(fArr[0], fArr[1], fArr[2], fArr[3]);
            float max = Math.max(hypot, hypot2);
            if (max > 0.0f) {
                return Math.abs(r0) / max;
            }
            return 0.0f;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m124(VGroup vGroup, Matrix matrix, Canvas canvas, int i, int i2, ColorFilter colorFilter) {
            vGroup.f117.set(matrix);
            vGroup.f117.preConcat(vGroup.f114);
            canvas.save();
            for (int i3 = 0; i3 < vGroup.f119.size(); i3++) {
                Object obj = vGroup.f119.get(i3);
                if (obj instanceof VGroup) {
                    m124((VGroup) obj, vGroup.f117, canvas, i, i2, colorFilter);
                } else if (obj instanceof VPath) {
                    m125(vGroup, (VPath) obj, canvas, i, i2, colorFilter);
                }
            }
            canvas.restore();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m125(VGroup vGroup, VPath vPath, Canvas canvas, int i, int i2, ColorFilter colorFilter) {
            float f = ((float) i) / this.f134;
            float f2 = ((float) i2) / this.f132;
            float min = Math.min(f, f2);
            Matrix r5 = vGroup.f117;
            this.f129.set(r5);
            this.f129.postScale(f, f2);
            float r7 = m121(r5);
            if (r7 != 0.0f) {
                vPath.m116(this.f130);
                Path path = this.f130;
                this.f131.reset();
                if (vPath.m117()) {
                    this.f131.addPath(path, this.f129);
                    canvas.clipPath(this.f131);
                    return;
                }
                VFullPath vFullPath = (VFullPath) vPath;
                if (!(vFullPath.f95 == 0.0f && vFullPath.f96 == 1.0f)) {
                    float f3 = (vFullPath.f95 + vFullPath.f98) % 1.0f;
                    float f4 = (vFullPath.f96 + vFullPath.f98) % 1.0f;
                    if (this.f137 == null) {
                        this.f137 = new PathMeasure();
                    }
                    this.f137.setPath(this.f130, false);
                    float length = this.f137.getLength();
                    float f5 = f3 * length;
                    float f6 = f4 * length;
                    path.reset();
                    if (f5 > f6) {
                        this.f137.getSegment(f5, length, path, true);
                        this.f137.getSegment(0.0f, f6, path, true);
                    } else {
                        this.f137.getSegment(f5, f6, path, true);
                    }
                    path.rLineTo(0.0f, 0.0f);
                }
                this.f131.addPath(path, this.f129);
                if (vFullPath.f104 != 0) {
                    if (this.f128 == null) {
                        this.f128 = new Paint();
                        this.f128.setStyle(Paint.Style.FILL);
                        this.f128.setAntiAlias(true);
                    }
                    Paint paint = this.f128;
                    paint.setColor(VectorDrawableCompat.m95(vFullPath.f104, vFullPath.f94));
                    paint.setColorFilter(colorFilter);
                    this.f131.setFillType(vFullPath.f101 == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    canvas.drawPath(this.f131, paint);
                }
                if (vFullPath.f105 != 0) {
                    if (this.f127 == null) {
                        this.f127 = new Paint();
                        this.f127.setStyle(Paint.Style.STROKE);
                        this.f127.setAntiAlias(true);
                    }
                    Paint paint2 = this.f127;
                    if (vFullPath.f100 != null) {
                        paint2.setStrokeJoin(vFullPath.f100);
                    }
                    if (vFullPath.f99 != null) {
                        paint2.setStrokeCap(vFullPath.f99);
                    }
                    paint2.setStrokeMiter(vFullPath.f97);
                    paint2.setColor(VectorDrawableCompat.m95(vFullPath.f105, vFullPath.f103));
                    paint2.setColorFilter(colorFilter);
                    paint2.setStrokeWidth(vFullPath.f102 * min * r7);
                    canvas.drawPath(this.f131, paint2);
                }
            }
        }

        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        public int getRootAlpha() {
            return this.f124;
        }

        public void setAlpha(float f) {
            setRootAlpha((int) (255.0f * f));
        }

        public void setRootAlpha(int i) {
            this.f124 = i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m126(Canvas canvas, int i, int i2, ColorFilter colorFilter) {
            m124(this.f136, f123, canvas, i, i2, colorFilter);
        }
    }

    private static class VectorDrawableCompatState extends Drawable.ConstantState {

        /* renamed from: ʻ  reason: contains not printable characters */
        Bitmap f139;

        /* renamed from: ʼ  reason: contains not printable characters */
        ColorStateList f140;

        /* renamed from: ʽ  reason: contains not printable characters */
        PorterDuff.Mode f141;

        /* renamed from: ˈ  reason: contains not printable characters */
        Paint f142;

        /* renamed from: ˑ  reason: contains not printable characters */
        int f143;

        /* renamed from: ٴ  reason: contains not printable characters */
        boolean f144;

        /* renamed from: ᐧ  reason: contains not printable characters */
        boolean f145;

        /* renamed from: 连任  reason: contains not printable characters */
        boolean f146;

        /* renamed from: 靐  reason: contains not printable characters */
        VPathRenderer f147;

        /* renamed from: 麤  reason: contains not printable characters */
        PorterDuff.Mode f148;

        /* renamed from: 齉  reason: contains not printable characters */
        ColorStateList f149;

        /* renamed from: 龘  reason: contains not printable characters */
        int f150;

        public VectorDrawableCompatState() {
            this.f149 = null;
            this.f148 = VectorDrawableCompat.f84;
            this.f147 = new VPathRenderer();
        }

        public VectorDrawableCompatState(VectorDrawableCompatState vectorDrawableCompatState) {
            this.f149 = null;
            this.f148 = VectorDrawableCompat.f84;
            if (vectorDrawableCompatState != null) {
                this.f150 = vectorDrawableCompatState.f150;
                this.f147 = new VPathRenderer(vectorDrawableCompatState.f147);
                if (vectorDrawableCompatState.f147.f128 != null) {
                    Paint unused = this.f147.f128 = new Paint(vectorDrawableCompatState.f147.f128);
                }
                if (vectorDrawableCompatState.f147.f127 != null) {
                    Paint unused2 = this.f147.f127 = new Paint(vectorDrawableCompatState.f147.f127);
                }
                this.f149 = vectorDrawableCompatState.f149;
                this.f148 = vectorDrawableCompatState.f148;
                this.f146 = vectorDrawableCompatState.f146;
            }
        }

        public int getChangingConfigurations() {
            return this.f150;
        }

        public Drawable newDrawable() {
            return new VectorDrawableCompat(this);
        }

        public Drawable newDrawable(Resources resources) {
            return new VectorDrawableCompat(this);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m127(int i, int i2) {
            if (this.f139 == null || !m130(i, i2)) {
                this.f139 = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
                this.f145 = true;
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m128() {
            return !this.f145 && this.f140 == this.f149 && this.f141 == this.f148 && this.f144 == this.f146 && this.f143 == this.f147.getRootAlpha();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m129() {
            this.f140 = this.f149;
            this.f141 = this.f148;
            this.f143 = this.f147.getRootAlpha();
            this.f144 = this.f146;
            this.f145 = false;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m130(int i, int i2) {
            return i == this.f139.getWidth() && i2 == this.f139.getHeight();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Paint m131(ColorFilter colorFilter) {
            if (!m134() && colorFilter == null) {
                return null;
            }
            if (this.f142 == null) {
                this.f142 = new Paint();
                this.f142.setFilterBitmap(true);
            }
            this.f142.setAlpha(this.f147.getRootAlpha());
            this.f142.setColorFilter(colorFilter);
            return this.f142;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m132(int i, int i2) {
            this.f139.eraseColor(0);
            this.f147.m126(new Canvas(this.f139), i, i2, (ColorFilter) null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m133(Canvas canvas, ColorFilter colorFilter, Rect rect) {
            canvas.drawBitmap(this.f139, (Rect) null, rect, m131(colorFilter));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m134() {
            return this.f147.getRootAlpha() < 255;
        }
    }

    private static class VectorDrawableDelegateState extends Drawable.ConstantState {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Drawable.ConstantState f151;

        public VectorDrawableDelegateState(Drawable.ConstantState constantState) {
            this.f151 = constantState;
        }

        public boolean canApplyTheme() {
            return this.f151.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f151.getChangingConfigurations();
        }

        public Drawable newDrawable() {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f83 = (VectorDrawable) this.f151.newDrawable();
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f83 = (VectorDrawable) this.f151.newDrawable(resources);
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f83 = (VectorDrawable) this.f151.newDrawable(resources, theme);
            return vectorDrawableCompat;
        }
    }

    VectorDrawableCompat() {
        this.f86 = true;
        this.f88 = new float[9];
        this.f89 = new Matrix();
        this.f90 = new Rect();
        this.f93 = new VectorDrawableCompatState();
    }

    VectorDrawableCompat(VectorDrawableCompatState vectorDrawableCompatState) {
        this.f86 = true;
        this.f88 = new float[9];
        this.f89 = new Matrix();
        this.f90 = new Rect();
        this.f93 = vectorDrawableCompatState;
        this.f92 = m101(this.f92, vectorDrawableCompatState.f149, vectorDrawableCompatState.f148);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m94(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        VectorDrawableCompatState vectorDrawableCompatState = this.f93;
        VPathRenderer vPathRenderer = vectorDrawableCompatState.f147;
        boolean z = true;
        Stack stack = new Stack();
        stack.push(vPathRenderer.f136);
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                VGroup vGroup = (VGroup) stack.peek();
                if ("path".equals(name)) {
                    VFullPath vFullPath = new VFullPath();
                    vFullPath.m110(resources, attributeSet, theme, xmlPullParser);
                    vGroup.f119.add(vFullPath);
                    if (vFullPath.getPathName() != null) {
                        vPathRenderer.f126.put(vFullPath.getPathName(), vFullPath);
                    }
                    z = false;
                    vectorDrawableCompatState.f150 |= vFullPath.f122;
                } else if ("clip-path".equals(name)) {
                    VClipPath vClipPath = new VClipPath();
                    vClipPath.m105(resources, attributeSet, theme, xmlPullParser);
                    vGroup.f119.add(vClipPath);
                    if (vClipPath.getPathName() != null) {
                        vPathRenderer.f126.put(vClipPath.getPathName(), vClipPath);
                    }
                    vectorDrawableCompatState.f150 |= vClipPath.f122;
                } else if ("group".equals(name)) {
                    VGroup vGroup2 = new VGroup();
                    vGroup2.m115(resources, attributeSet, theme, xmlPullParser);
                    vGroup.f119.add(vGroup2);
                    stack.push(vGroup2);
                    if (vGroup2.getGroupName() != null) {
                        vPathRenderer.f126.put(vGroup2.getGroupName(), vGroup2);
                    }
                    vectorDrawableCompatState.f150 |= vGroup2.f118;
                }
            } else if (eventType == 3 && "group".equals(xmlPullParser.getName())) {
                stack.pop();
            }
            eventType = xmlPullParser.next();
        }
        if (z) {
            StringBuffer stringBuffer = new StringBuffer();
            if (stringBuffer.length() > 0) {
                stringBuffer.append(" or ");
            }
            stringBuffer.append("path");
            throw new XmlPullParserException("no " + stringBuffer + " defined");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m95(int i, float f) {
        return (i & ViewCompat.MEASURED_SIZE_MASK) | (((int) (((float) Color.alpha(i)) * f)) << 24);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static PorterDuff.Mode m96(int i, PorterDuff.Mode mode) {
        switch (i) {
            case 3:
                return PorterDuff.Mode.SRC_OVER;
            case 5:
                return PorterDuff.Mode.SRC_IN;
            case 9:
                return PorterDuff.Mode.SRC_ATOP;
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return Build.VERSION.SDK_INT >= 11 ? PorterDuff.Mode.ADD : mode;
            default:
                return mode;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0033 A[Catch:{ XmlPullParserException -> 0x003c, IOException -> 0x004d }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0048 A[SYNTHETIC, Splitter:B:15:0x0048] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.support.graphics.drawable.VectorDrawableCompat m97(android.content.res.Resources r8, int r9, android.content.res.Resources.Theme r10) {
        /*
            r7 = 2
            int r5 = android.os.Build.VERSION.SDK_INT
            r6 = 24
            if (r5 < r6) goto L_0x0020
            android.support.graphics.drawable.VectorDrawableCompat r1 = new android.support.graphics.drawable.VectorDrawableCompat
            r1.<init>()
            android.graphics.drawable.Drawable r5 = android.support.v4.content.res.ResourcesCompat.getDrawable(r8, r9, r10)
            r1.f83 = r5
            android.support.graphics.drawable.VectorDrawableCompat$VectorDrawableDelegateState r5 = new android.support.graphics.drawable.VectorDrawableCompat$VectorDrawableDelegateState
            android.graphics.drawable.Drawable r6 = r1.f83
            android.graphics.drawable.Drawable$ConstantState r6 = r6.getConstantState()
            r5.<init>(r6)
            r1.f87 = r5
        L_0x001f:
            return r1
        L_0x0020:
            android.content.res.XmlResourceParser r3 = r8.getXml(r9)     // Catch:{ XmlPullParserException -> 0x003c, IOException -> 0x004d }
            android.util.AttributeSet r0 = android.util.Xml.asAttributeSet(r3)     // Catch:{ XmlPullParserException -> 0x003c, IOException -> 0x004d }
        L_0x0028:
            int r4 = r3.next()     // Catch:{ XmlPullParserException -> 0x003c, IOException -> 0x004d }
            if (r4 == r7) goto L_0x0031
            r5 = 1
            if (r4 != r5) goto L_0x0028
        L_0x0031:
            if (r4 == r7) goto L_0x0048
            org.xmlpull.v1.XmlPullParserException r5 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ XmlPullParserException -> 0x003c, IOException -> 0x004d }
            java.lang.String r6 = "No start tag found"
            r5.<init>(r6)     // Catch:{ XmlPullParserException -> 0x003c, IOException -> 0x004d }
            throw r5     // Catch:{ XmlPullParserException -> 0x003c, IOException -> 0x004d }
        L_0x003c:
            r2 = move-exception
            java.lang.String r5 = "VectorDrawableCompat"
            java.lang.String r6 = "parser error"
            android.util.Log.e(r5, r6, r2)
        L_0x0046:
            r1 = 0
            goto L_0x001f
        L_0x0048:
            android.support.graphics.drawable.VectorDrawableCompat r1 = m98(r8, r3, r0, r10)     // Catch:{ XmlPullParserException -> 0x003c, IOException -> 0x004d }
            goto L_0x001f
        L_0x004d:
            r2 = move-exception
            java.lang.String r5 = "VectorDrawableCompat"
            java.lang.String r6 = "parser error"
            android.util.Log.e(r5, r6, r2)
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.graphics.drawable.VectorDrawableCompat.m97(android.content.res.Resources, int, android.content.res.Resources$Theme):android.support.graphics.drawable.VectorDrawableCompat");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static VectorDrawableCompat m98(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
        vectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
        return vectorDrawableCompat;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m99(TypedArray typedArray, XmlPullParser xmlPullParser) throws XmlPullParserException {
        VectorDrawableCompatState vectorDrawableCompatState = this.f93;
        VPathRenderer vPathRenderer = vectorDrawableCompatState.f147;
        vectorDrawableCompatState.f148 = m96(TypedArrayUtils.getNamedInt(typedArray, xmlPullParser, "tintMode", 6, -1), PorterDuff.Mode.SRC_IN);
        ColorStateList colorStateList = typedArray.getColorStateList(1);
        if (colorStateList != null) {
            vectorDrawableCompatState.f149 = colorStateList;
        }
        vectorDrawableCompatState.f146 = TypedArrayUtils.getNamedBoolean(typedArray, xmlPullParser, "autoMirrored", 5, vectorDrawableCompatState.f146);
        vPathRenderer.f134 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "viewportWidth", 7, vPathRenderer.f134);
        vPathRenderer.f132 = TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "viewportHeight", 8, vPathRenderer.f132);
        if (vPathRenderer.f134 <= 0.0f) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (vPathRenderer.f132 <= 0.0f) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        } else {
            vPathRenderer.f133 = typedArray.getDimension(3, vPathRenderer.f133);
            vPathRenderer.f135 = typedArray.getDimension(2, vPathRenderer.f135);
            if (vPathRenderer.f133 <= 0.0f) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (vPathRenderer.f135 <= 0.0f) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            } else {
                vPathRenderer.setAlpha(TypedArrayUtils.getNamedFloat(typedArray, xmlPullParser, "alpha", 4, vPathRenderer.getAlpha()));
                String string = typedArray.getString(0);
                if (string != null) {
                    vPathRenderer.f125 = string;
                    vPathRenderer.f126.put(string, vPathRenderer);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m100() {
        if (Build.VERSION.SDK_INT >= 17) {
            return isAutoMirrored() && DrawableCompat.getLayoutDirection(this) == 1;
        }
        return false;
    }

    public /* bridge */ /* synthetic */ void applyTheme(Resources.Theme theme) {
        super.applyTheme(theme);
    }

    public boolean canApplyTheme() {
        if (this.f83 == null) {
            return false;
        }
        DrawableCompat.canApplyTheme(this.f83);
        return false;
    }

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public void draw(Canvas canvas) {
        if (this.f83 != null) {
            this.f83.draw(canvas);
            return;
        }
        copyBounds(this.f90);
        if (this.f90.width() > 0 && this.f90.height() > 0) {
            ColorFilter colorFilter = this.f91 == null ? this.f92 : this.f91;
            canvas.getMatrix(this.f89);
            this.f89.getValues(this.f88);
            float abs = Math.abs(this.f88[0]);
            float abs2 = Math.abs(this.f88[4]);
            float abs3 = Math.abs(this.f88[1]);
            float abs4 = Math.abs(this.f88[3]);
            if (!(abs3 == 0.0f && abs4 == 0.0f)) {
                abs = 1.0f;
                abs2 = 1.0f;
            }
            int min = Math.min(2048, (int) (((float) this.f90.width()) * abs));
            int min2 = Math.min(2048, (int) (((float) this.f90.height()) * abs2));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                canvas.translate((float) this.f90.left, (float) this.f90.top);
                if (m100()) {
                    canvas.translate((float) this.f90.width(), 0.0f);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.f90.offsetTo(0, 0);
                this.f93.m127(min, min2);
                if (!this.f86) {
                    this.f93.m132(min, min2);
                } else if (!this.f93.m128()) {
                    this.f93.m132(min, min2);
                    this.f93.m129();
                }
                this.f93.m133(canvas, colorFilter, this.f90);
                canvas.restoreToCount(save);
            }
        }
    }

    public int getAlpha() {
        return this.f83 != null ? DrawableCompat.getAlpha(this.f83) : this.f93.f147.getRootAlpha();
    }

    public int getChangingConfigurations() {
        return this.f83 != null ? this.f83.getChangingConfigurations() : super.getChangingConfigurations() | this.f93.getChangingConfigurations();
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public Drawable.ConstantState getConstantState() {
        if (this.f83 != null && Build.VERSION.SDK_INT >= 24) {
            return new VectorDrawableDelegateState(this.f83.getConstantState());
        }
        this.f93.f150 = getChangingConfigurations();
        return this.f93;
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.f83 != null ? this.f83.getIntrinsicHeight() : (int) this.f93.f147.f135;
    }

    public int getIntrinsicWidth() {
        return this.f83 != null ? this.f83.getIntrinsicWidth() : (int) this.f93.f147.f133;
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public int getOpacity() {
        if (this.f83 != null) {
            return this.f83.getOpacity();
        }
        return -3;
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        if (this.f83 != null) {
            this.f83.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, (Resources.Theme) null);
        }
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        if (this.f83 != null) {
            DrawableCompat.inflate(this.f83, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        VectorDrawableCompatState vectorDrawableCompatState = this.f93;
        vectorDrawableCompatState.f147 = new VPathRenderer();
        TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f65);
        m99(obtainAttributes, xmlPullParser);
        obtainAttributes.recycle();
        vectorDrawableCompatState.f150 = getChangingConfigurations();
        vectorDrawableCompatState.f145 = true;
        m94(resources, xmlPullParser, attributeSet, theme);
        this.f92 = m101(this.f92, vectorDrawableCompatState.f149, vectorDrawableCompatState.f148);
    }

    public void invalidateSelf() {
        if (this.f83 != null) {
            this.f83.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    public boolean isAutoMirrored() {
        return this.f83 != null ? DrawableCompat.isAutoMirrored(this.f83) : this.f93.f146;
    }

    public boolean isStateful() {
        return this.f83 != null ? this.f83.isStateful() : super.isStateful() || !(this.f93 == null || this.f93.f149 == null || !this.f93.f149.isStateful());
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public Drawable mutate() {
        if (this.f83 != null) {
            this.f83.mutate();
        } else if (!this.f85 && super.mutate() == this) {
            this.f93 = new VectorDrawableCompatState(this.f93);
            this.f85 = true;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f83 != null) {
            this.f83.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        if (this.f83 != null) {
            return this.f83.setState(iArr);
        }
        VectorDrawableCompatState vectorDrawableCompatState = this.f93;
        if (vectorDrawableCompatState.f149 == null || vectorDrawableCompatState.f148 == null) {
            return false;
        }
        this.f92 = m101(this.f92, vectorDrawableCompatState.f149, vectorDrawableCompatState.f148);
        invalidateSelf();
        return true;
    }

    public void scheduleSelf(Runnable runnable, long j) {
        if (this.f83 != null) {
            this.f83.scheduleSelf(runnable, j);
        } else {
            super.scheduleSelf(runnable, j);
        }
    }

    public void setAlpha(int i) {
        if (this.f83 != null) {
            this.f83.setAlpha(i);
        } else if (this.f93.f147.getRootAlpha() != i) {
            this.f93.f147.setRootAlpha(i);
            invalidateSelf();
        }
    }

    public void setAutoMirrored(boolean z) {
        if (this.f83 != null) {
            DrawableCompat.setAutoMirrored(this.f83, z);
        } else {
            this.f93.f146 = z;
        }
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i) {
        super.setChangingConfigurations(i);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i, PorterDuff.Mode mode) {
        super.setColorFilter(i, mode);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f83 != null) {
            this.f83.setColorFilter(colorFilter);
            return;
        }
        this.f91 = colorFilter;
        invalidateSelf();
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f, float f2) {
        super.setHotspot(f, f2);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    public void setTint(int i) {
        if (this.f83 != null) {
            DrawableCompat.setTint(this.f83, i);
        } else {
            setTintList(ColorStateList.valueOf(i));
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.f83 != null) {
            DrawableCompat.setTintList(this.f83, colorStateList);
            return;
        }
        VectorDrawableCompatState vectorDrawableCompatState = this.f93;
        if (vectorDrawableCompatState.f149 != colorStateList) {
            vectorDrawableCompatState.f149 = colorStateList;
            this.f92 = m101(this.f92, colorStateList, vectorDrawableCompatState.f148);
            invalidateSelf();
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (this.f83 != null) {
            DrawableCompat.setTintMode(this.f83, mode);
            return;
        }
        VectorDrawableCompatState vectorDrawableCompatState = this.f93;
        if (vectorDrawableCompatState.f148 != mode) {
            vectorDrawableCompatState.f148 = mode;
            this.f92 = m101(this.f92, vectorDrawableCompatState.f149, mode);
            invalidateSelf();
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        return this.f83 != null ? this.f83.setVisible(z, z2) : super.setVisible(z, z2);
    }

    public void unscheduleSelf(Runnable runnable) {
        if (this.f83 != null) {
            this.f83.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public PorterDuffColorFilter m101(PorterDuffColorFilter porterDuffColorFilter, ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Object m102(String str) {
        return this.f93.f147.f126.get(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m103(boolean z) {
        this.f86 = z;
    }
}
