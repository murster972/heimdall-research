package android.support.graphics.drawable;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ArrayMap;
import android.util.AttributeSet;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AnimatedVectorDrawableCompat extends VectorDrawableCommon implements Animatable2Compat {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Animator.AnimatorListener f66;

    /* renamed from: ʼ  reason: contains not printable characters */
    private ArrayList<Object> f67;

    /* renamed from: 连任  reason: contains not printable characters */
    private ArgbEvaluator f68;

    /* renamed from: 麤  reason: contains not printable characters */
    private Context f69;

    /* renamed from: 齉  reason: contains not printable characters */
    private AnimatedVectorDrawableCompatState f70;

    /* renamed from: 龘  reason: contains not printable characters */
    final Drawable.Callback f71;

    private static class AnimatedVectorDrawableCompatState extends Drawable.ConstantState {
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public ArrayList<Animator> f73;

        /* renamed from: 靐  reason: contains not printable characters */
        VectorDrawableCompat f74;

        /* renamed from: 麤  reason: contains not printable characters */
        ArrayMap<Animator, String> f75;

        /* renamed from: 齉  reason: contains not printable characters */
        AnimatorSet f76;

        /* renamed from: 龘  reason: contains not printable characters */
        int f77;

        public AnimatedVectorDrawableCompatState(Context context, AnimatedVectorDrawableCompatState animatedVectorDrawableCompatState, Drawable.Callback callback, Resources resources) {
            if (animatedVectorDrawableCompatState != null) {
                this.f77 = animatedVectorDrawableCompatState.f77;
                if (animatedVectorDrawableCompatState.f74 != null) {
                    Drawable.ConstantState constantState = animatedVectorDrawableCompatState.f74.getConstantState();
                    if (resources != null) {
                        this.f74 = (VectorDrawableCompat) constantState.newDrawable(resources);
                    } else {
                        this.f74 = (VectorDrawableCompat) constantState.newDrawable();
                    }
                    this.f74 = (VectorDrawableCompat) this.f74.mutate();
                    this.f74.setCallback(callback);
                    this.f74.setBounds(animatedVectorDrawableCompatState.f74.getBounds());
                    this.f74.m103(false);
                }
                if (animatedVectorDrawableCompatState.f73 != null) {
                    int size = animatedVectorDrawableCompatState.f73.size();
                    this.f73 = new ArrayList<>(size);
                    this.f75 = new ArrayMap<>(size);
                    for (int i = 0; i < size; i++) {
                        Animator animator = animatedVectorDrawableCompatState.f73.get(i);
                        Animator clone = animator.clone();
                        String str = animatedVectorDrawableCompatState.f75.get(animator);
                        clone.setTarget(this.f74.m102(str));
                        this.f73.add(clone);
                        this.f75.put(clone, str);
                    }
                    m66();
                }
            }
        }

        public int getChangingConfigurations() {
            return this.f77;
        }

        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m66() {
            if (this.f76 == null) {
                this.f76 = new AnimatorSet();
            }
            this.f76.playTogether(this.f73);
        }
    }

    private static class AnimatedVectorDrawableDelegateState extends Drawable.ConstantState {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Drawable.ConstantState f78;

        public AnimatedVectorDrawableDelegateState(Drawable.ConstantState constantState) {
            this.f78 = constantState;
        }

        public boolean canApplyTheme() {
            return this.f78.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f78.getChangingConfigurations();
        }

        public Drawable newDrawable() {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.f83 = this.f78.newDrawable();
            animatedVectorDrawableCompat.f83.setCallback(animatedVectorDrawableCompat.f71);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.f83 = this.f78.newDrawable(resources);
            animatedVectorDrawableCompat.f83.setCallback(animatedVectorDrawableCompat.f71);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.f83 = this.f78.newDrawable(resources, theme);
            animatedVectorDrawableCompat.f83.setCallback(animatedVectorDrawableCompat.f71);
            return animatedVectorDrawableCompat;
        }
    }

    AnimatedVectorDrawableCompat() {
        this((Context) null, (AnimatedVectorDrawableCompatState) null, (Resources) null);
    }

    private AnimatedVectorDrawableCompat(Context context) {
        this(context, (AnimatedVectorDrawableCompatState) null, (Resources) null);
    }

    private AnimatedVectorDrawableCompat(Context context, AnimatedVectorDrawableCompatState animatedVectorDrawableCompatState, Resources resources) {
        this.f68 = null;
        this.f66 = null;
        this.f67 = null;
        this.f71 = new Drawable.Callback() {
            public void invalidateDrawable(Drawable drawable) {
                AnimatedVectorDrawableCompat.this.invalidateSelf();
            }

            public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
                AnimatedVectorDrawableCompat.this.scheduleSelf(runnable, j);
            }

            public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
                AnimatedVectorDrawableCompat.this.unscheduleSelf(runnable);
            }
        };
        this.f69 = context;
        if (animatedVectorDrawableCompatState != null) {
            this.f70 = animatedVectorDrawableCompatState;
        } else {
            this.f70 = new AnimatedVectorDrawableCompatState(context, animatedVectorDrawableCompatState, this.f71, resources);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static AnimatedVectorDrawableCompat m61(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(context);
        animatedVectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
        return animatedVectorDrawableCompat;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m62(Animator animator) {
        ArrayList<Animator> childAnimations;
        if ((animator instanceof AnimatorSet) && (childAnimations = ((AnimatorSet) animator).getChildAnimations()) != null) {
            for (int i = 0; i < childAnimations.size(); i++) {
                m62(childAnimations.get(i));
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.f68 == null) {
                    this.f68 = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.f68);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m63(String str, Animator animator) {
        animator.setTarget(this.f70.f74.m102(str));
        if (Build.VERSION.SDK_INT < 21) {
            m62(animator);
        }
        if (this.f70.f73 == null) {
            ArrayList unused = this.f70.f73 = new ArrayList();
            this.f70.f75 = new ArrayMap<>();
        }
        this.f70.f73.add(animator);
        this.f70.f75.put(animator, str);
    }

    public void applyTheme(Resources.Theme theme) {
        if (this.f83 != null) {
            DrawableCompat.applyTheme(this.f83, theme);
        }
    }

    public boolean canApplyTheme() {
        if (this.f83 != null) {
            return DrawableCompat.canApplyTheme(this.f83);
        }
        return false;
    }

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public void draw(Canvas canvas) {
        if (this.f83 != null) {
            this.f83.draw(canvas);
            return;
        }
        this.f70.f74.draw(canvas);
        if (this.f70.f76.isStarted()) {
            invalidateSelf();
        }
    }

    public int getAlpha() {
        return this.f83 != null ? DrawableCompat.getAlpha(this.f83) : this.f70.f74.getAlpha();
    }

    public int getChangingConfigurations() {
        return this.f83 != null ? this.f83.getChangingConfigurations() : super.getChangingConfigurations() | this.f70.f77;
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public Drawable.ConstantState getConstantState() {
        if (this.f83 == null || Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new AnimatedVectorDrawableDelegateState(this.f83.getConstantState());
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.f83 != null ? this.f83.getIntrinsicHeight() : this.f70.f74.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        return this.f83 != null ? this.f83.getIntrinsicWidth() : this.f70.f74.getIntrinsicWidth();
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public int getOpacity() {
        return this.f83 != null ? this.f83.getOpacity() : this.f70.f74.getOpacity();
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        inflate(resources, xmlPullParser, attributeSet, (Resources.Theme) null);
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        if (this.f83 != null) {
            DrawableCompat.inflate(this.f83, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, AndroidResources.f61);
                    int resourceId = obtainAttributes.getResourceId(0, 0);
                    if (resourceId != 0) {
                        VectorDrawableCompat r8 = VectorDrawableCompat.m97(resources, resourceId, theme);
                        r8.m103(false);
                        r8.setCallback(this.f71);
                        if (this.f70.f74 != null) {
                            this.f70.f74.setCallback((Drawable.Callback) null);
                        }
                        this.f70.f74 = r8;
                    }
                    obtainAttributes.recycle();
                } else if ("target".equals(name)) {
                    TypedArray obtainAttributes2 = resources.obtainAttributes(attributeSet, AndroidResources.f54);
                    String string = obtainAttributes2.getString(0);
                    int resourceId2 = obtainAttributes2.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        if (this.f69 != null) {
                            m63(string, AnimatorInflaterCompat.m71(this.f69, resourceId2));
                        } else {
                            obtainAttributes2.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes2.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.f70.m66();
    }

    public boolean isAutoMirrored() {
        return this.f83 != null ? DrawableCompat.isAutoMirrored(this.f83) : this.f70.f74.isAutoMirrored();
    }

    public boolean isRunning() {
        return this.f83 != null ? ((AnimatedVectorDrawable) this.f83).isRunning() : this.f70.f76.isRunning();
    }

    public boolean isStateful() {
        return this.f83 != null ? this.f83.isStateful() : this.f70.f74.isStateful();
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public Drawable mutate() {
        if (this.f83 != null) {
            this.f83.mutate();
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f83 != null) {
            this.f83.setBounds(rect);
        } else {
            this.f70.f74.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f83 != null ? this.f83.setLevel(i) : this.f70.f74.setLevel(i);
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        return this.f83 != null ? this.f83.setState(iArr) : this.f70.f74.setState(iArr);
    }

    public void setAlpha(int i) {
        if (this.f83 != null) {
            this.f83.setAlpha(i);
        } else {
            this.f70.f74.setAlpha(i);
        }
    }

    public void setAutoMirrored(boolean z) {
        if (this.f83 != null) {
            DrawableCompat.setAutoMirrored(this.f83, z);
        } else {
            this.f70.f74.setAutoMirrored(z);
        }
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i) {
        super.setChangingConfigurations(i);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i, PorterDuff.Mode mode) {
        super.setColorFilter(i, mode);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f83 != null) {
            this.f83.setColorFilter(colorFilter);
        } else {
            this.f70.f74.setColorFilter(colorFilter);
        }
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f, float f2) {
        super.setHotspot(f, f2);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    public void setTint(int i) {
        if (this.f83 != null) {
            DrawableCompat.setTint(this.f83, i);
        } else {
            this.f70.f74.setTint(i);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.f83 != null) {
            DrawableCompat.setTintList(this.f83, colorStateList);
        } else {
            this.f70.f74.setTintList(colorStateList);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (this.f83 != null) {
            DrawableCompat.setTintMode(this.f83, mode);
        } else {
            this.f70.f74.setTintMode(mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.f83 != null) {
            return this.f83.setVisible(z, z2);
        }
        this.f70.f74.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    public void start() {
        if (this.f83 != null) {
            ((AnimatedVectorDrawable) this.f83).start();
        } else if (!this.f70.f76.isStarted()) {
            this.f70.f76.start();
            invalidateSelf();
        }
    }

    public void stop() {
        if (this.f83 != null) {
            ((AnimatedVectorDrawable) this.f83).stop();
        } else {
            this.f70.f76.end();
        }
    }
}
