package android.support.v4.graphics;

import android.content.Context;
import android.content.res.Resources;
import android.os.Process;
import android.support.annotation.RestrictTo;
import android.util.Log;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

@RestrictTo
public class TypefaceCompatUtil {
    private static final String CACHE_FILE_PREFIX = ".font";
    private static final String TAG = "TypefaceCompatUtil";

    private TypefaceCompatUtil() {
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    public static ByteBuffer copyToDirectBuffer(Context context, Resources resources, int i) {
        ByteBuffer byteBuffer = null;
        File tempFile = getTempFile(context);
        if (tempFile != null) {
            try {
                if (copyToFile(tempFile, resources, i)) {
                    byteBuffer = mmap(tempFile);
                    tempFile.delete();
                }
            } finally {
                tempFile.delete();
            }
        }
        return byteBuffer;
    }

    public static boolean copyToFile(File file, Resources resources, int i) {
        InputStream inputStream = null;
        try {
            inputStream = resources.openRawResource(i);
            return copyToFile(file, inputStream);
        } finally {
            closeQuietly(inputStream);
        }
    }

    public static boolean copyToFile(File file, InputStream inputStream) {
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream fileOutputStream2 = new FileOutputStream(file, false);
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read != -1) {
                        fileOutputStream2.write(bArr, 0, read);
                    } else {
                        closeQuietly(fileOutputStream2);
                        FileOutputStream fileOutputStream3 = fileOutputStream2;
                        return true;
                    }
                }
            } catch (IOException e) {
                e = e;
                fileOutputStream = fileOutputStream2;
                try {
                    Log.e(TAG, "Error copying resource contents to temp file: " + e.getMessage());
                    closeQuietly(fileOutputStream);
                    return false;
                } catch (Throwable th) {
                    th = th;
                    closeQuietly(fileOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = fileOutputStream2;
                closeQuietly(fileOutputStream);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            Log.e(TAG, "Error copying resource contents to temp file: " + e.getMessage());
            closeQuietly(fileOutputStream);
            return false;
        }
    }

    public static File getTempFile(Context context) {
        String str = CACHE_FILE_PREFIX + Process.myPid() + "-" + Process.myTid() + "-";
        int i = 0;
        while (i < 100) {
            File file = new File(context.getCacheDir(), str + i);
            try {
                if (file.createNewFile()) {
                    return file;
                }
                i++;
            } catch (IOException e) {
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x004e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x004f, code lost:
        r2 = r1;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x005d, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x005e, code lost:
        r3 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0079, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x007a, code lost:
        r2 = r1;
        r3 = null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x004e A[ExcHandler: all (r1v2 'th' java.lang.Throwable A[CUSTOM_DECLARE])] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.nio.ByteBuffer mmap(android.content.Context r13, android.os.CancellationSignal r14, android.net.Uri r15) {
        /*
            r10 = 0
            android.content.ContentResolver r9 = r13.getContentResolver()
            java.lang.String r1 = "r"
            android.os.ParcelFileDescriptor r8 = r9.openFileDescriptor(r15, r1, r14)     // Catch:{ IOException -> 0x0047 }
            r11 = 0
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x003b, all -> 0x004e }
            java.io.FileDescriptor r1 = r8.getFileDescriptor()     // Catch:{ Throwable -> 0x003b, all -> 0x004e }
            r7.<init>(r1)     // Catch:{ Throwable -> 0x003b, all -> 0x004e }
            r12 = 0
            java.nio.channels.FileChannel r0 = r7.getChannel()     // Catch:{ Throwable -> 0x005b, all -> 0x0079 }
            long r4 = r0.size()     // Catch:{ Throwable -> 0x005b, all -> 0x0079 }
            java.nio.channels.FileChannel$MapMode r1 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ Throwable -> 0x005b, all -> 0x0079 }
            r2 = 0
            java.nio.MappedByteBuffer r1 = r0.map(r1, r2, r4)     // Catch:{ Throwable -> 0x005b, all -> 0x0079 }
            if (r7 == 0) goto L_0x002e
            if (r10 == 0) goto L_0x004a
            r7.close()     // Catch:{ Throwable -> 0x0036, all -> 0x004e }
        L_0x002e:
            if (r8 == 0) goto L_0x0035
            if (r10 == 0) goto L_0x0057
            r8.close()     // Catch:{ Throwable -> 0x0052 }
        L_0x0035:
            return r1
        L_0x0036:
            r2 = move-exception
            r12.addSuppressed(r2)     // Catch:{ Throwable -> 0x003b, all -> 0x004e }
            goto L_0x002e
        L_0x003b:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x003d }
        L_0x003d:
            r2 = move-exception
            r3 = r1
        L_0x003f:
            if (r8 == 0) goto L_0x0046
            if (r3 == 0) goto L_0x0075
            r8.close()     // Catch:{ Throwable -> 0x0070 }
        L_0x0046:
            throw r2     // Catch:{ IOException -> 0x0047 }
        L_0x0047:
            r6 = move-exception
            r1 = r10
            goto L_0x0035
        L_0x004a:
            r7.close()     // Catch:{ Throwable -> 0x003b, all -> 0x004e }
            goto L_0x002e
        L_0x004e:
            r1 = move-exception
            r2 = r1
            r3 = r10
            goto L_0x003f
        L_0x0052:
            r2 = move-exception
            r11.addSuppressed(r2)     // Catch:{ IOException -> 0x0047 }
            goto L_0x0035
        L_0x0057:
            r8.close()     // Catch:{ IOException -> 0x0047 }
            goto L_0x0035
        L_0x005b:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x005d }
        L_0x005d:
            r2 = move-exception
            r3 = r1
        L_0x005f:
            if (r7 == 0) goto L_0x0066
            if (r3 == 0) goto L_0x006c
            r7.close()     // Catch:{ Throwable -> 0x0067, all -> 0x004e }
        L_0x0066:
            throw r2     // Catch:{ Throwable -> 0x003b, all -> 0x004e }
        L_0x0067:
            r1 = move-exception
            r3.addSuppressed(r1)     // Catch:{ Throwable -> 0x003b, all -> 0x004e }
            goto L_0x0066
        L_0x006c:
            r7.close()     // Catch:{ Throwable -> 0x003b, all -> 0x004e }
            goto L_0x0066
        L_0x0070:
            r1 = move-exception
            r3.addSuppressed(r1)     // Catch:{ IOException -> 0x0047 }
            goto L_0x0046
        L_0x0075:
            r8.close()     // Catch:{ IOException -> 0x0047 }
            goto L_0x0046
        L_0x0079:
            r1 = move-exception
            r2 = r1
            r3 = r10
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.graphics.TypefaceCompatUtil.mmap(android.content.Context, android.os.CancellationSignal, android.net.Uri):java.nio.ByteBuffer");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002d, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002e, code lost:
        r3 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0040, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0041, code lost:
        r2 = r1;
        r3 = null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.nio.ByteBuffer mmap(java.io.File r10) {
        /*
            r8 = 0
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0024 }
            r7.<init>(r10)     // Catch:{ IOException -> 0x0024 }
            r9 = 0
            java.nio.channels.FileChannel r0 = r7.getChannel()     // Catch:{ Throwable -> 0x002b, all -> 0x0040 }
            long r4 = r0.size()     // Catch:{ Throwable -> 0x002b, all -> 0x0040 }
            java.nio.channels.FileChannel$MapMode r1 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ Throwable -> 0x002b, all -> 0x0040 }
            r2 = 0
            java.nio.MappedByteBuffer r1 = r0.map(r1, r2, r4)     // Catch:{ Throwable -> 0x002b, all -> 0x0040 }
            if (r7 == 0) goto L_0x001e
            if (r8 == 0) goto L_0x0027
            r7.close()     // Catch:{ Throwable -> 0x001f }
        L_0x001e:
            return r1
        L_0x001f:
            r2 = move-exception
            r9.addSuppressed(r2)     // Catch:{ IOException -> 0x0024 }
            goto L_0x001e
        L_0x0024:
            r6 = move-exception
            r1 = r8
            goto L_0x001e
        L_0x0027:
            r7.close()     // Catch:{ IOException -> 0x0024 }
            goto L_0x001e
        L_0x002b:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x002d }
        L_0x002d:
            r2 = move-exception
            r3 = r1
        L_0x002f:
            if (r7 == 0) goto L_0x0036
            if (r3 == 0) goto L_0x003c
            r7.close()     // Catch:{ Throwable -> 0x0037 }
        L_0x0036:
            throw r2     // Catch:{ IOException -> 0x0024 }
        L_0x0037:
            r1 = move-exception
            r3.addSuppressed(r1)     // Catch:{ IOException -> 0x0024 }
            goto L_0x0036
        L_0x003c:
            r7.close()     // Catch:{ IOException -> 0x0024 }
            goto L_0x0036
        L_0x0040:
            r1 = move-exception
            r2 = r1
            r3 = r8
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.graphics.TypefaceCompatUtil.mmap(java.io.File):java.nio.ByteBuffer");
    }
}
