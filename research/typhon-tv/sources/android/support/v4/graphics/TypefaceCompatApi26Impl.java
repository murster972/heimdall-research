package android.support.v4.graphics;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.fonts.FontVariationAxis;
import android.support.annotation.RestrictTo;
import android.support.v4.content.res.FontResourcesParserCompat;
import android.util.Log;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

@RestrictTo
public class TypefaceCompatApi26Impl extends TypefaceCompatApi21Impl {
    private static final String ABORT_CREATION_METHOD = "abortCreation";
    private static final String ADD_FONT_FROM_ASSET_MANAGER_METHOD = "addFontFromAssetManager";
    private static final String ADD_FONT_FROM_BUFFER_METHOD = "addFontFromBuffer";
    private static final String CREATE_FROM_FAMILIES_WITH_DEFAULT_METHOD = "createFromFamiliesWithDefault";
    private static final String FONT_FAMILY_CLASS = "android.graphics.FontFamily";
    private static final String FREEZE_METHOD = "freeze";
    private static final int RESOLVE_BY_FONT_TABLE = -1;
    private static final String TAG = "TypefaceCompatApi26Impl";
    private static final Method sAbortCreation;
    private static final Method sAddFontFromAssetManager;
    private static final Method sAddFontFromBuffer;
    private static final Method sCreateFromFamiliesWithDefault;
    private static final Class sFontFamily;
    private static final Constructor sFontFamilyCtor;
    private static final Method sFreeze;

    static {
        Class<?> cls;
        Constructor<?> constructor;
        Method method;
        Method method2;
        Method method3;
        Method method4;
        Method method5;
        try {
            cls = Class.forName(FONT_FAMILY_CLASS);
            constructor = cls.getConstructor(new Class[0]);
            method = cls.getMethod(ADD_FONT_FROM_ASSET_MANAGER_METHOD, new Class[]{AssetManager.class, String.class, Integer.TYPE, Boolean.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, FontVariationAxis[].class});
            method2 = cls.getMethod(ADD_FONT_FROM_BUFFER_METHOD, new Class[]{ByteBuffer.class, Integer.TYPE, FontVariationAxis[].class, Integer.TYPE, Integer.TYPE});
            method3 = cls.getMethod(FREEZE_METHOD, new Class[0]);
            method4 = cls.getMethod(ABORT_CREATION_METHOD, new Class[0]);
            method5 = Typeface.class.getDeclaredMethod(CREATE_FROM_FAMILIES_WITH_DEFAULT_METHOD, new Class[]{Array.newInstance(cls, 1).getClass(), Integer.TYPE, Integer.TYPE});
            method5.setAccessible(true);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            Log.e(TAG, "Unable to collect necessary methods for class " + e.getClass().getName(), e);
            cls = null;
            constructor = null;
            method = null;
            method2 = null;
            method3 = null;
            method4 = null;
            method5 = null;
        }
        sFontFamilyCtor = constructor;
        sFontFamily = cls;
        sAddFontFromAssetManager = method;
        sAddFontFromBuffer = method2;
        sFreeze = method3;
        sAbortCreation = method4;
        sCreateFromFamiliesWithDefault = method5;
    }

    private static void abortCreation(Object obj) {
        try {
            sAbortCreation.invoke(obj, new Object[0]);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean addFontFromAssetManager(Context context, Object obj, String str, int i, int i2, int i3) {
        try {
            return ((Boolean) sAddFontFromAssetManager.invoke(obj, new Object[]{context.getAssets(), str, 0, false, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), null})).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean addFontFromBuffer(Object obj, ByteBuffer byteBuffer, int i, int i2, int i3) {
        try {
            return ((Boolean) sAddFontFromBuffer.invoke(obj, new Object[]{byteBuffer, Integer.valueOf(i), null, Integer.valueOf(i2), Integer.valueOf(i3)})).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static Typeface createFromFamiliesWithDefault(Object obj) {
        try {
            Object newInstance = Array.newInstance(sFontFamily, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) sCreateFromFamiliesWithDefault.invoke((Object) null, new Object[]{newInstance, -1, -1});
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean freeze(Object obj) {
        try {
            return ((Boolean) sFreeze.invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean isFontFamilyPrivateAPIAvailable() {
        if (sAddFontFromAssetManager == null) {
            Log.w(TAG, "Unable to collect necessary private methods. Fallback to legacy implementation.");
        }
        return sAddFontFromAssetManager != null;
    }

    private static Object newFamily() {
        try {
            return sFontFamilyCtor.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public Typeface createFromFontFamilyFilesResourceEntry(Context context, FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamilyFilesResourceEntry, Resources resources, int i) {
        if (!isFontFamilyPrivateAPIAvailable()) {
            return super.createFromFontFamilyFilesResourceEntry(context, fontFamilyFilesResourceEntry, resources, i);
        }
        Object newFamily = newFamily();
        for (FontResourcesParserCompat.FontFileResourceEntry fontFileResourceEntry : fontFamilyFilesResourceEntry.getEntries()) {
            if (!addFontFromAssetManager(context, newFamily, fontFileResourceEntry.getFileName(), 0, fontFileResourceEntry.getWeight(), fontFileResourceEntry.isItalic() ? 1 : 0)) {
                abortCreation(newFamily);
                return null;
            }
        }
        if (!freeze(newFamily)) {
            return null;
        }
        return createFromFamiliesWithDefault(newFamily);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0060, code lost:
        r15 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0061, code lost:
        r16 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00e2, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e3, code lost:
        r15 = r14;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Typeface createFromFontInfo(android.content.Context r20, android.os.CancellationSignal r21, android.support.v4.provider.FontsContractCompat.FontInfo[] r22, int r23) {
        /*
            r19 = this;
            r0 = r22
            int r14 = r0.length
            r15 = 1
            if (r14 >= r15) goto L_0x0008
            r14 = 0
        L_0x0007:
            return r14
        L_0x0008:
            boolean r14 = isFontFamilyPrivateAPIAvailable()
            if (r14 != 0) goto L_0x0076
            r0 = r19
            r1 = r22
            r2 = r23
            android.support.v4.provider.FontsContractCompat$FontInfo r4 = r0.findBestInfo(r1, r2)
            android.content.ContentResolver r10 = r20.getContentResolver()
            android.net.Uri r14 = r4.getUri()     // Catch:{ IOException -> 0x0057 }
            java.lang.String r15 = "r"
            r0 = r21
            android.os.ParcelFileDescriptor r9 = r10.openFileDescriptor(r14, r15, r0)     // Catch:{ IOException -> 0x0057 }
            r16 = 0
            android.graphics.Typeface$Builder r14 = new android.graphics.Typeface$Builder     // Catch:{ Throwable -> 0x005e, all -> 0x00e2 }
            java.io.FileDescriptor r15 = r9.getFileDescriptor()     // Catch:{ Throwable -> 0x005e, all -> 0x00e2 }
            r14.<init>(r15)     // Catch:{ Throwable -> 0x005e, all -> 0x00e2 }
            int r15 = r4.getWeight()     // Catch:{ Throwable -> 0x005e, all -> 0x00e2 }
            android.graphics.Typeface$Builder r14 = r14.setWeight(r15)     // Catch:{ Throwable -> 0x005e, all -> 0x00e2 }
            boolean r15 = r4.isItalic()     // Catch:{ Throwable -> 0x005e, all -> 0x00e2 }
            android.graphics.Typeface$Builder r14 = r14.setItalic(r15)     // Catch:{ Throwable -> 0x005e, all -> 0x00e2 }
            android.graphics.Typeface r14 = r14.build()     // Catch:{ Throwable -> 0x005e, all -> 0x00e2 }
            if (r9 == 0) goto L_0x0007
            if (r16 == 0) goto L_0x005a
            r9.close()     // Catch:{ Throwable -> 0x0050 }
            goto L_0x0007
        L_0x0050:
            r15 = move-exception
            r0 = r16
            r0.addSuppressed(r15)     // Catch:{ IOException -> 0x0057 }
            goto L_0x0007
        L_0x0057:
            r5 = move-exception
            r14 = 0
            goto L_0x0007
        L_0x005a:
            r9.close()     // Catch:{ IOException -> 0x0057 }
            goto L_0x0007
        L_0x005e:
            r14 = move-exception
            throw r14     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r15 = move-exception
            r16 = r14
        L_0x0063:
            if (r9 == 0) goto L_0x006a
            if (r16 == 0) goto L_0x0072
            r9.close()     // Catch:{ Throwable -> 0x006b }
        L_0x006a:
            throw r15     // Catch:{ IOException -> 0x0057 }
        L_0x006b:
            r14 = move-exception
            r0 = r16
            r0.addSuppressed(r14)     // Catch:{ IOException -> 0x0057 }
            goto L_0x006a
        L_0x0072:
            r9.close()     // Catch:{ IOException -> 0x0057 }
            goto L_0x006a
        L_0x0076:
            r0 = r20
            r1 = r22
            r2 = r21
            java.util.Map r13 = android.support.v4.provider.FontsContractCompat.prepareFontData(r0, r1, r2)
            java.lang.Object r8 = newFamily()
            r3 = 0
            r0 = r22
            int r0 = r0.length
            r16 = r0
            r14 = 0
            r15 = r14
        L_0x008c:
            r0 = r16
            if (r15 >= r0) goto L_0x00c5
            r6 = r22[r15]
            android.net.Uri r14 = r6.getUri()
            java.lang.Object r7 = r13.get(r14)
            java.nio.ByteBuffer r7 = (java.nio.ByteBuffer) r7
            if (r7 != 0) goto L_0x00a2
        L_0x009e:
            int r14 = r15 + 1
            r15 = r14
            goto L_0x008c
        L_0x00a2:
            int r17 = r6.getTtcIndex()
            int r18 = r6.getWeight()
            boolean r14 = r6.isItalic()
            if (r14 == 0) goto L_0x00c1
            r14 = 1
        L_0x00b1:
            r0 = r17
            r1 = r18
            boolean r11 = addFontFromBuffer(r8, r7, r0, r1, r14)
            if (r11 != 0) goto L_0x00c3
            abortCreation(r8)
            r14 = 0
            goto L_0x0007
        L_0x00c1:
            r14 = 0
            goto L_0x00b1
        L_0x00c3:
            r3 = 1
            goto L_0x009e
        L_0x00c5:
            if (r3 != 0) goto L_0x00cd
            abortCreation(r8)
            r14 = 0
            goto L_0x0007
        L_0x00cd:
            boolean r14 = freeze(r8)
            if (r14 != 0) goto L_0x00d6
            r14 = 0
            goto L_0x0007
        L_0x00d6:
            android.graphics.Typeface r12 = createFromFamiliesWithDefault(r8)
            r0 = r23
            android.graphics.Typeface r14 = android.graphics.Typeface.create(r12, r0)
            goto L_0x0007
        L_0x00e2:
            r14 = move-exception
            r15 = r14
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.graphics.TypefaceCompatApi26Impl.createFromFontInfo(android.content.Context, android.os.CancellationSignal, android.support.v4.provider.FontsContractCompat$FontInfo[], int):android.graphics.Typeface");
    }

    public Typeface createFromResourcesFontFile(Context context, Resources resources, int i, String str, int i2) {
        if (!isFontFamilyPrivateAPIAvailable()) {
            return super.createFromResourcesFontFile(context, resources, i, str, i2);
        }
        Object newFamily = newFamily();
        if (!addFontFromAssetManager(context, newFamily, str, 0, -1, -1)) {
            abortCreation(newFamily);
            return null;
        } else if (!freeze(newFamily)) {
            return null;
        } else {
            return createFromFamiliesWithDefault(newFamily);
        }
    }
}
