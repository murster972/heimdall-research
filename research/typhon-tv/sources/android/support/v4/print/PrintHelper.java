package android.support.v4.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.util.Log;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class PrintHelper {
    public static final int COLOR_MODE_COLOR = 2;
    public static final int COLOR_MODE_MONOCHROME = 1;
    public static final int ORIENTATION_LANDSCAPE = 1;
    public static final int ORIENTATION_PORTRAIT = 2;
    public static final int SCALE_MODE_FILL = 2;
    public static final int SCALE_MODE_FIT = 1;
    private final PrintHelperVersionImpl mImpl;

    @Retention(RetentionPolicy.SOURCE)
    private @interface ColorMode {
    }

    public interface OnPrintFinishCallback {
        void onFinish();
    }

    @Retention(RetentionPolicy.SOURCE)
    private @interface Orientation {
    }

    private static class PrintHelperApi19 implements PrintHelperVersionImpl {
        private static final String LOG_TAG = "PrintHelperApi19";
        private static final int MAX_PRINT_SIZE = 3500;
        int mColorMode = 2;
        final Context mContext;
        BitmapFactory.Options mDecodeOptions = null;
        protected boolean mIsMinMarginsHandlingCorrect = true;
        /* access modifiers changed from: private */
        public final Object mLock = new Object();
        int mOrientation;
        protected boolean mPrintActivityRespectsOrientation = true;
        int mScaleMode = 2;

        PrintHelperApi19(Context context) {
            this.mContext = context;
        }

        /* access modifiers changed from: private */
        public Bitmap convertBitmapForColorMode(Bitmap bitmap, int i) {
            if (i != 1) {
                return bitmap;
            }
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0.0f);
            paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
            canvas.setBitmap((Bitmap) null);
            return createBitmap;
        }

        /* access modifiers changed from: private */
        public Matrix getMatrix(int i, int i2, RectF rectF, int i3) {
            Matrix matrix = new Matrix();
            float width = rectF.width() / ((float) i);
            float max = i3 == 2 ? Math.max(width, rectF.height() / ((float) i2)) : Math.min(width, rectF.height() / ((float) i2));
            matrix.postScale(max, max);
            matrix.postTranslate((rectF.width() - (((float) i) * max)) / 2.0f, (rectF.height() - (((float) i2) * max)) / 2.0f);
            return matrix;
        }

        /* access modifiers changed from: private */
        public static boolean isPortrait(Bitmap bitmap) {
            return bitmap.getWidth() <= bitmap.getHeight();
        }

        private Bitmap loadBitmap(Uri uri, BitmapFactory.Options options) throws FileNotFoundException {
            if (uri == null || this.mContext == null) {
                throw new IllegalArgumentException("bad argument to loadBitmap");
            }
            InputStream inputStream = null;
            try {
                inputStream = this.mContext.getContentResolver().openInputStream(uri);
                Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, (Rect) null, options);
                if (inputStream != null) {
                    try {
                    } catch (IOException e) {
                        Log.w(LOG_TAG, "close fail ", e);
                    }
                }
                return decodeStream;
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                        Log.w(LOG_TAG, "close fail ", e2);
                    }
                }
            }
        }

        /* access modifiers changed from: private */
        public Bitmap loadConstrainedBitmap(Uri uri) throws FileNotFoundException {
            BitmapFactory.Options options;
            Bitmap bitmap = null;
            if (uri == null || this.mContext == null) {
                throw new IllegalArgumentException("bad argument to getScaledBitmap");
            }
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inJustDecodeBounds = true;
            loadBitmap(uri, options2);
            int i = options2.outWidth;
            int i2 = options2.outHeight;
            if (i > 0 && i2 > 0) {
                int max = Math.max(i, i2);
                int i3 = 1;
                while (max > MAX_PRINT_SIZE) {
                    max >>>= 1;
                    i3 <<= 1;
                }
                if (i3 > 0 && Math.min(i, i2) / i3 > 0) {
                    synchronized (this.mLock) {
                        this.mDecodeOptions = new BitmapFactory.Options();
                        this.mDecodeOptions.inMutable = true;
                        this.mDecodeOptions.inSampleSize = i3;
                        options = this.mDecodeOptions;
                    }
                    try {
                        bitmap = loadBitmap(uri, options);
                        synchronized (this.mLock) {
                            this.mDecodeOptions = null;
                        }
                    } catch (Throwable th) {
                        synchronized (this.mLock) {
                            this.mDecodeOptions = null;
                            throw th;
                        }
                    }
                }
            }
            return bitmap;
        }

        /* access modifiers changed from: private */
        public void writeBitmap(PrintAttributes printAttributes, int i, Bitmap bitmap, ParcelFileDescriptor parcelFileDescriptor, CancellationSignal cancellationSignal, PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
            final PrintAttributes build = this.mIsMinMarginsHandlingCorrect ? printAttributes : copyAttributes(printAttributes).setMinMargins(new PrintAttributes.Margins(0, 0, 0, 0)).build();
            final CancellationSignal cancellationSignal2 = cancellationSignal;
            final Bitmap bitmap2 = bitmap;
            final PrintAttributes printAttributes2 = printAttributes;
            final int i2 = i;
            final ParcelFileDescriptor parcelFileDescriptor2 = parcelFileDescriptor;
            final PrintDocumentAdapter.WriteResultCallback writeResultCallback2 = writeResultCallback;
            new AsyncTask<Void, Void, Throwable>() {
                /* access modifiers changed from: protected */
                /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
                /* JADX WARNING: Unknown top exception splitter block from list: {B:33:0x00b3=Splitter:B:33:0x00b3, B:46:0x00e4=Splitter:B:46:0x00e4, B:20:0x0078=Splitter:B:20:0x0078} */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public java.lang.Throwable doInBackground(java.lang.Void... r13) {
                    /*
                        r12 = this;
                        r7 = 0
                        android.os.CancellationSignal r8 = r2     // Catch:{ Throwable -> 0x0080 }
                        boolean r8 = r8.isCanceled()     // Catch:{ Throwable -> 0x0080 }
                        if (r8 == 0) goto L_0x000a
                    L_0x0009:
                        return r7
                    L_0x000a:
                        android.print.pdf.PrintedPdfDocument r6 = new android.print.pdf.PrintedPdfDocument     // Catch:{ Throwable -> 0x0080 }
                        android.support.v4.print.PrintHelper$PrintHelperApi19 r8 = android.support.v4.print.PrintHelper.PrintHelperApi19.this     // Catch:{ Throwable -> 0x0080 }
                        android.content.Context r8 = r8.mContext     // Catch:{ Throwable -> 0x0080 }
                        android.print.PrintAttributes r9 = r3     // Catch:{ Throwable -> 0x0080 }
                        r6.<init>(r8, r9)     // Catch:{ Throwable -> 0x0080 }
                        android.support.v4.print.PrintHelper$PrintHelperApi19 r8 = android.support.v4.print.PrintHelper.PrintHelperApi19.this     // Catch:{ Throwable -> 0x0080 }
                        android.graphics.Bitmap r9 = r4     // Catch:{ Throwable -> 0x0080 }
                        android.print.PrintAttributes r10 = r3     // Catch:{ Throwable -> 0x0080 }
                        int r10 = r10.getColorMode()     // Catch:{ Throwable -> 0x0080 }
                        android.graphics.Bitmap r4 = r8.convertBitmapForColorMode(r9, r10)     // Catch:{ Throwable -> 0x0080 }
                        android.os.CancellationSignal r8 = r2     // Catch:{ Throwable -> 0x0080 }
                        boolean r8 = r8.isCanceled()     // Catch:{ Throwable -> 0x0080 }
                        if (r8 != 0) goto L_0x0009
                        r8 = 1
                        android.graphics.pdf.PdfDocument$Page r5 = r6.startPage(r8)     // Catch:{ all -> 0x00a6 }
                        android.support.v4.print.PrintHelper$PrintHelperApi19 r8 = android.support.v4.print.PrintHelper.PrintHelperApi19.this     // Catch:{ all -> 0x00a6 }
                        boolean r8 = r8.mIsMinMarginsHandlingCorrect     // Catch:{ all -> 0x00a6 }
                        if (r8 == 0) goto L_0x0082
                        android.graphics.RectF r0 = new android.graphics.RectF     // Catch:{ all -> 0x00a6 }
                        android.graphics.pdf.PdfDocument$PageInfo r8 = r5.getInfo()     // Catch:{ all -> 0x00a6 }
                        android.graphics.Rect r8 = r8.getContentRect()     // Catch:{ all -> 0x00a6 }
                        r0.<init>(r8)     // Catch:{ all -> 0x00a6 }
                    L_0x0043:
                        android.support.v4.print.PrintHelper$PrintHelperApi19 r8 = android.support.v4.print.PrintHelper.PrintHelperApi19.this     // Catch:{ all -> 0x00a6 }
                        int r9 = r4.getWidth()     // Catch:{ all -> 0x00a6 }
                        int r10 = r4.getHeight()     // Catch:{ all -> 0x00a6 }
                        int r11 = r6     // Catch:{ all -> 0x00a6 }
                        android.graphics.Matrix r3 = r8.getMatrix(r9, r10, r0, r11)     // Catch:{ all -> 0x00a6 }
                        android.support.v4.print.PrintHelper$PrintHelperApi19 r8 = android.support.v4.print.PrintHelper.PrintHelperApi19.this     // Catch:{ all -> 0x00a6 }
                        boolean r8 = r8.mIsMinMarginsHandlingCorrect     // Catch:{ all -> 0x00a6 }
                        if (r8 == 0) goto L_0x00bb
                    L_0x0059:
                        android.graphics.Canvas r8 = r5.getCanvas()     // Catch:{ all -> 0x00a6 }
                        r9 = 0
                        r8.drawBitmap(r4, r3, r9)     // Catch:{ all -> 0x00a6 }
                        r6.finishPage(r5)     // Catch:{ all -> 0x00a6 }
                        android.os.CancellationSignal r8 = r2     // Catch:{ all -> 0x00a6 }
                        boolean r8 = r8.isCanceled()     // Catch:{ all -> 0x00a6 }
                        if (r8 == 0) goto L_0x00ca
                        r6.close()     // Catch:{ Throwable -> 0x0080 }
                        android.os.ParcelFileDescriptor r8 = r7     // Catch:{ Throwable -> 0x0080 }
                        if (r8 == 0) goto L_0x0078
                        android.os.ParcelFileDescriptor r8 = r7     // Catch:{ IOException -> 0x00f1 }
                        r8.close()     // Catch:{ IOException -> 0x00f1 }
                    L_0x0078:
                        android.graphics.Bitmap r8 = r4     // Catch:{ Throwable -> 0x0080 }
                        if (r4 == r8) goto L_0x0009
                        r4.recycle()     // Catch:{ Throwable -> 0x0080 }
                        goto L_0x0009
                    L_0x0080:
                        r7 = move-exception
                        goto L_0x0009
                    L_0x0082:
                        android.print.pdf.PrintedPdfDocument r1 = new android.print.pdf.PrintedPdfDocument     // Catch:{ all -> 0x00a6 }
                        android.support.v4.print.PrintHelper$PrintHelperApi19 r8 = android.support.v4.print.PrintHelper.PrintHelperApi19.this     // Catch:{ all -> 0x00a6 }
                        android.content.Context r8 = r8.mContext     // Catch:{ all -> 0x00a6 }
                        android.print.PrintAttributes r9 = r5     // Catch:{ all -> 0x00a6 }
                        r1.<init>(r8, r9)     // Catch:{ all -> 0x00a6 }
                        r8 = 1
                        android.graphics.pdf.PdfDocument$Page r2 = r1.startPage(r8)     // Catch:{ all -> 0x00a6 }
                        android.graphics.RectF r0 = new android.graphics.RectF     // Catch:{ all -> 0x00a6 }
                        android.graphics.pdf.PdfDocument$PageInfo r8 = r2.getInfo()     // Catch:{ all -> 0x00a6 }
                        android.graphics.Rect r8 = r8.getContentRect()     // Catch:{ all -> 0x00a6 }
                        r0.<init>(r8)     // Catch:{ all -> 0x00a6 }
                        r1.finishPage(r2)     // Catch:{ all -> 0x00a6 }
                        r1.close()     // Catch:{ all -> 0x00a6 }
                        goto L_0x0043
                    L_0x00a6:
                        r8 = move-exception
                        r6.close()     // Catch:{ Throwable -> 0x0080 }
                        android.os.ParcelFileDescriptor r9 = r7     // Catch:{ Throwable -> 0x0080 }
                        if (r9 == 0) goto L_0x00b3
                        android.os.ParcelFileDescriptor r9 = r7     // Catch:{ IOException -> 0x00ed }
                        r9.close()     // Catch:{ IOException -> 0x00ed }
                    L_0x00b3:
                        android.graphics.Bitmap r9 = r4     // Catch:{ Throwable -> 0x0080 }
                        if (r4 == r9) goto L_0x00ba
                        r4.recycle()     // Catch:{ Throwable -> 0x0080 }
                    L_0x00ba:
                        throw r8     // Catch:{ Throwable -> 0x0080 }
                    L_0x00bb:
                        float r8 = r0.left     // Catch:{ all -> 0x00a6 }
                        float r9 = r0.top     // Catch:{ all -> 0x00a6 }
                        r3.postTranslate(r8, r9)     // Catch:{ all -> 0x00a6 }
                        android.graphics.Canvas r8 = r5.getCanvas()     // Catch:{ all -> 0x00a6 }
                        r8.clipRect(r0)     // Catch:{ all -> 0x00a6 }
                        goto L_0x0059
                    L_0x00ca:
                        java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ all -> 0x00a6 }
                        android.os.ParcelFileDescriptor r9 = r7     // Catch:{ all -> 0x00a6 }
                        java.io.FileDescriptor r9 = r9.getFileDescriptor()     // Catch:{ all -> 0x00a6 }
                        r8.<init>(r9)     // Catch:{ all -> 0x00a6 }
                        r6.writeTo(r8)     // Catch:{ all -> 0x00a6 }
                        r6.close()     // Catch:{ Throwable -> 0x0080 }
                        android.os.ParcelFileDescriptor r8 = r7     // Catch:{ Throwable -> 0x0080 }
                        if (r8 == 0) goto L_0x00e4
                        android.os.ParcelFileDescriptor r8 = r7     // Catch:{ IOException -> 0x00ef }
                        r8.close()     // Catch:{ IOException -> 0x00ef }
                    L_0x00e4:
                        android.graphics.Bitmap r8 = r4     // Catch:{ Throwable -> 0x0080 }
                        if (r4 == r8) goto L_0x0009
                        r4.recycle()     // Catch:{ Throwable -> 0x0080 }
                        goto L_0x0009
                    L_0x00ed:
                        r9 = move-exception
                        goto L_0x00b3
                    L_0x00ef:
                        r8 = move-exception
                        goto L_0x00e4
                    L_0x00f1:
                        r8 = move-exception
                        goto L_0x0078
                    */
                    throw new UnsupportedOperationException("Method not decompiled: android.support.v4.print.PrintHelper.PrintHelperApi19.AnonymousClass2.doInBackground(java.lang.Void[]):java.lang.Throwable");
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Throwable th) {
                    if (cancellationSignal2.isCanceled()) {
                        writeResultCallback2.onWriteCancelled();
                    } else if (th == null) {
                        writeResultCallback2.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});
                    } else {
                        Log.e(PrintHelperApi19.LOG_TAG, "Error writing printed content", th);
                        writeResultCallback2.onWriteFailed((CharSequence) null);
                    }
                }
            }.execute(new Void[0]);
        }

        /* access modifiers changed from: protected */
        public PrintAttributes.Builder copyAttributes(PrintAttributes printAttributes) {
            PrintAttributes.Builder minMargins = new PrintAttributes.Builder().setMediaSize(printAttributes.getMediaSize()).setResolution(printAttributes.getResolution()).setMinMargins(printAttributes.getMinMargins());
            if (printAttributes.getColorMode() != 0) {
                minMargins.setColorMode(printAttributes.getColorMode());
            }
            return minMargins;
        }

        public int getColorMode() {
            return this.mColorMode;
        }

        public int getOrientation() {
            if (this.mOrientation == 0) {
                return 1;
            }
            return this.mOrientation;
        }

        public int getScaleMode() {
            return this.mScaleMode;
        }

        public void printBitmap(String str, Bitmap bitmap, OnPrintFinishCallback onPrintFinishCallback) {
            if (bitmap != null) {
                final int i = this.mScaleMode;
                final String str2 = str;
                final Bitmap bitmap2 = bitmap;
                final OnPrintFinishCallback onPrintFinishCallback2 = onPrintFinishCallback;
                ((PrintManager) this.mContext.getSystemService("print")).print(str, new PrintDocumentAdapter() {
                    private PrintAttributes mAttributes;

                    public void onFinish() {
                        if (onPrintFinishCallback2 != null) {
                            onPrintFinishCallback2.onFinish();
                        }
                    }

                    public void onLayout(PrintAttributes printAttributes, PrintAttributes printAttributes2, CancellationSignal cancellationSignal, PrintDocumentAdapter.LayoutResultCallback layoutResultCallback, Bundle bundle) {
                        boolean z = true;
                        this.mAttributes = printAttributes2;
                        PrintDocumentInfo build = new PrintDocumentInfo.Builder(str2).setContentType(1).setPageCount(1).build();
                        if (printAttributes2.equals(printAttributes)) {
                            z = false;
                        }
                        layoutResultCallback.onLayoutFinished(build, z);
                    }

                    public void onWrite(PageRange[] pageRangeArr, ParcelFileDescriptor parcelFileDescriptor, CancellationSignal cancellationSignal, PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
                        PrintHelperApi19.this.writeBitmap(this.mAttributes, i, bitmap2, parcelFileDescriptor, cancellationSignal, writeResultCallback);
                    }
                }, new PrintAttributes.Builder().setMediaSize(isPortrait(bitmap) ? PrintAttributes.MediaSize.UNKNOWN_PORTRAIT : PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE).setColorMode(this.mColorMode).build());
            }
        }

        public void printBitmap(String str, Uri uri, OnPrintFinishCallback onPrintFinishCallback) throws FileNotFoundException {
            final int i = this.mScaleMode;
            final String str2 = str;
            final Uri uri2 = uri;
            final OnPrintFinishCallback onPrintFinishCallback2 = onPrintFinishCallback;
            AnonymousClass3 r0 = new PrintDocumentAdapter() {
                /* access modifiers changed from: private */
                public PrintAttributes mAttributes;
                Bitmap mBitmap = null;
                AsyncTask<Uri, Boolean, Bitmap> mLoadBitmap;

                /* access modifiers changed from: private */
                public void cancelLoad() {
                    synchronized (PrintHelperApi19.this.mLock) {
                        if (PrintHelperApi19.this.mDecodeOptions != null) {
                            PrintHelperApi19.this.mDecodeOptions.requestCancelDecode();
                            PrintHelperApi19.this.mDecodeOptions = null;
                        }
                    }
                }

                public void onFinish() {
                    super.onFinish();
                    cancelLoad();
                    if (this.mLoadBitmap != null) {
                        this.mLoadBitmap.cancel(true);
                    }
                    if (onPrintFinishCallback2 != null) {
                        onPrintFinishCallback2.onFinish();
                    }
                    if (this.mBitmap != null) {
                        this.mBitmap.recycle();
                        this.mBitmap = null;
                    }
                }

                public void onLayout(PrintAttributes printAttributes, PrintAttributes printAttributes2, CancellationSignal cancellationSignal, PrintDocumentAdapter.LayoutResultCallback layoutResultCallback, Bundle bundle) {
                    boolean z = true;
                    synchronized (this) {
                        this.mAttributes = printAttributes2;
                    }
                    if (cancellationSignal.isCanceled()) {
                        layoutResultCallback.onLayoutCancelled();
                    } else if (this.mBitmap != null) {
                        PrintDocumentInfo build = new PrintDocumentInfo.Builder(str2).setContentType(1).setPageCount(1).build();
                        if (printAttributes2.equals(printAttributes)) {
                            z = false;
                        }
                        layoutResultCallback.onLayoutFinished(build, z);
                    } else {
                        final CancellationSignal cancellationSignal2 = cancellationSignal;
                        final PrintAttributes printAttributes3 = printAttributes2;
                        final PrintAttributes printAttributes4 = printAttributes;
                        final PrintDocumentAdapter.LayoutResultCallback layoutResultCallback2 = layoutResultCallback;
                        this.mLoadBitmap = new AsyncTask<Uri, Boolean, Bitmap>() {
                            /* access modifiers changed from: protected */
                            public Bitmap doInBackground(Uri... uriArr) {
                                try {
                                    return PrintHelperApi19.this.loadConstrainedBitmap(uri2);
                                } catch (FileNotFoundException e) {
                                    return null;
                                }
                            }

                            /* access modifiers changed from: protected */
                            public void onCancelled(Bitmap bitmap) {
                                layoutResultCallback2.onLayoutCancelled();
                                AnonymousClass3.this.mLoadBitmap = null;
                            }

                            /* access modifiers changed from: protected */
                            public void onPostExecute(Bitmap bitmap) {
                                PrintAttributes.MediaSize mediaSize;
                                super.onPostExecute(bitmap);
                                if (bitmap != null && (!PrintHelperApi19.this.mPrintActivityRespectsOrientation || PrintHelperApi19.this.mOrientation == 0)) {
                                    synchronized (this) {
                                        mediaSize = AnonymousClass3.this.mAttributes.getMediaSize();
                                    }
                                    if (!(mediaSize == null || mediaSize.isPortrait() == PrintHelperApi19.isPortrait(bitmap))) {
                                        Matrix matrix = new Matrix();
                                        matrix.postRotate(90.0f);
                                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                                    }
                                }
                                AnonymousClass3.this.mBitmap = bitmap;
                                if (bitmap != null) {
                                    layoutResultCallback2.onLayoutFinished(new PrintDocumentInfo.Builder(str2).setContentType(1).setPageCount(1).build(), !printAttributes3.equals(printAttributes4));
                                } else {
                                    layoutResultCallback2.onLayoutFailed((CharSequence) null);
                                }
                                AnonymousClass3.this.mLoadBitmap = null;
                            }

                            /* access modifiers changed from: protected */
                            public void onPreExecute() {
                                cancellationSignal2.setOnCancelListener(new CancellationSignal.OnCancelListener() {
                                    public void onCancel() {
                                        AnonymousClass3.this.cancelLoad();
                                        AnonymousClass1.this.cancel(false);
                                    }
                                });
                            }
                        }.execute(new Uri[0]);
                    }
                }

                public void onWrite(PageRange[] pageRangeArr, ParcelFileDescriptor parcelFileDescriptor, CancellationSignal cancellationSignal, PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
                    PrintHelperApi19.this.writeBitmap(this.mAttributes, i, this.mBitmap, parcelFileDescriptor, cancellationSignal, writeResultCallback);
                }
            };
            PrintManager printManager = (PrintManager) this.mContext.getSystemService("print");
            PrintAttributes.Builder builder = new PrintAttributes.Builder();
            builder.setColorMode(this.mColorMode);
            if (this.mOrientation == 1 || this.mOrientation == 0) {
                builder.setMediaSize(PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE);
            } else if (this.mOrientation == 2) {
                builder.setMediaSize(PrintAttributes.MediaSize.UNKNOWN_PORTRAIT);
            }
            printManager.print(str, r0, builder.build());
        }

        public void setColorMode(int i) {
            this.mColorMode = i;
        }

        public void setOrientation(int i) {
            this.mOrientation = i;
        }

        public void setScaleMode(int i) {
            this.mScaleMode = i;
        }
    }

    private static class PrintHelperApi20 extends PrintHelperApi19 {
        PrintHelperApi20(Context context) {
            super(context);
            this.mPrintActivityRespectsOrientation = false;
        }
    }

    private static class PrintHelperApi23 extends PrintHelperApi20 {
        PrintHelperApi23(Context context) {
            super(context);
            this.mIsMinMarginsHandlingCorrect = false;
        }

        /* access modifiers changed from: protected */
        public PrintAttributes.Builder copyAttributes(PrintAttributes printAttributes) {
            PrintAttributes.Builder copyAttributes = super.copyAttributes(printAttributes);
            if (printAttributes.getDuplexMode() != 0) {
                copyAttributes.setDuplexMode(printAttributes.getDuplexMode());
            }
            return copyAttributes;
        }
    }

    private static class PrintHelperApi24 extends PrintHelperApi23 {
        PrintHelperApi24(Context context) {
            super(context);
            this.mIsMinMarginsHandlingCorrect = true;
            this.mPrintActivityRespectsOrientation = true;
        }
    }

    private static final class PrintHelperStub implements PrintHelperVersionImpl {
        int mColorMode;
        int mOrientation;
        int mScaleMode;

        private PrintHelperStub() {
            this.mScaleMode = 2;
            this.mColorMode = 2;
            this.mOrientation = 1;
        }

        public int getColorMode() {
            return this.mColorMode;
        }

        public int getOrientation() {
            return this.mOrientation;
        }

        public int getScaleMode() {
            return this.mScaleMode;
        }

        public void printBitmap(String str, Bitmap bitmap, OnPrintFinishCallback onPrintFinishCallback) {
        }

        public void printBitmap(String str, Uri uri, OnPrintFinishCallback onPrintFinishCallback) {
        }

        public void setColorMode(int i) {
            this.mColorMode = i;
        }

        public void setOrientation(int i) {
            this.mOrientation = i;
        }

        public void setScaleMode(int i) {
            this.mScaleMode = i;
        }
    }

    interface PrintHelperVersionImpl {
        int getColorMode();

        int getOrientation();

        int getScaleMode();

        void printBitmap(String str, Bitmap bitmap, OnPrintFinishCallback onPrintFinishCallback);

        void printBitmap(String str, Uri uri, OnPrintFinishCallback onPrintFinishCallback) throws FileNotFoundException;

        void setColorMode(int i);

        void setOrientation(int i);

        void setScaleMode(int i);
    }

    @Retention(RetentionPolicy.SOURCE)
    private @interface ScaleMode {
    }

    public PrintHelper(Context context) {
        if (Build.VERSION.SDK_INT >= 24) {
            this.mImpl = new PrintHelperApi24(context);
        } else if (Build.VERSION.SDK_INT >= 23) {
            this.mImpl = new PrintHelperApi23(context);
        } else if (Build.VERSION.SDK_INT >= 20) {
            this.mImpl = new PrintHelperApi20(context);
        } else if (Build.VERSION.SDK_INT >= 19) {
            this.mImpl = new PrintHelperApi19(context);
        } else {
            this.mImpl = new PrintHelperStub();
        }
    }

    public static boolean systemSupportsPrint() {
        return Build.VERSION.SDK_INT >= 19;
    }

    public int getColorMode() {
        return this.mImpl.getColorMode();
    }

    public int getOrientation() {
        return this.mImpl.getOrientation();
    }

    public int getScaleMode() {
        return this.mImpl.getScaleMode();
    }

    public void printBitmap(String str, Bitmap bitmap) {
        this.mImpl.printBitmap(str, bitmap, (OnPrintFinishCallback) null);
    }

    public void printBitmap(String str, Bitmap bitmap, OnPrintFinishCallback onPrintFinishCallback) {
        this.mImpl.printBitmap(str, bitmap, onPrintFinishCallback);
    }

    public void printBitmap(String str, Uri uri) throws FileNotFoundException {
        this.mImpl.printBitmap(str, uri, (OnPrintFinishCallback) null);
    }

    public void printBitmap(String str, Uri uri, OnPrintFinishCallback onPrintFinishCallback) throws FileNotFoundException {
        this.mImpl.printBitmap(str, uri, onPrintFinishCallback);
    }

    public void setColorMode(int i) {
        this.mImpl.setColorMode(i);
    }

    public void setOrientation(int i) {
        this.mImpl.setOrientation(i);
    }

    public void setScaleMode(int i) {
        this.mImpl.setScaleMode(i);
    }
}
