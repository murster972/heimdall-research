package android.support.multidex;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.zip.CRC32;
import java.util.zip.ZipException;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

final class ZipUtil {

    static class CentralDirectory {

        /* renamed from: 靐  reason: contains not printable characters */
        long f166;

        /* renamed from: 龘  reason: contains not printable characters */
        long f167;

        CentralDirectory() {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static long m170(File file) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, InternalZipTyphoonApp.READ_MODE);
        try {
            return m171(randomAccessFile, m172(randomAccessFile));
        } finally {
            randomAccessFile.close();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static long m171(RandomAccessFile randomAccessFile, CentralDirectory centralDirectory) throws IOException {
        CRC32 crc32 = new CRC32();
        long j = centralDirectory.f166;
        randomAccessFile.seek(centralDirectory.f167);
        byte[] bArr = new byte[16384];
        int read = randomAccessFile.read(bArr, 0, (int) Math.min(16384, j));
        while (read != -1) {
            crc32.update(bArr, 0, read);
            j -= (long) read;
            if (j == 0) {
                break;
            }
            read = randomAccessFile.read(bArr, 0, (int) Math.min(16384, j));
        }
        return crc32.getValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static CentralDirectory m172(RandomAccessFile randomAccessFile) throws IOException, ZipException {
        long length = randomAccessFile.length() - 22;
        if (length < 0) {
            throw new ZipException("File too short to be a zip file: " + randomAccessFile.length());
        }
        long j = length - PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH;
        if (j < 0) {
            j = 0;
        }
        int reverseBytes = Integer.reverseBytes(101010256);
        do {
            randomAccessFile.seek(length);
            if (randomAccessFile.readInt() == reverseBytes) {
                randomAccessFile.skipBytes(2);
                randomAccessFile.skipBytes(2);
                randomAccessFile.skipBytes(2);
                randomAccessFile.skipBytes(2);
                CentralDirectory centralDirectory = new CentralDirectory();
                centralDirectory.f166 = ((long) Integer.reverseBytes(randomAccessFile.readInt())) & InternalZipTyphoonApp.ZIP_64_LIMIT;
                centralDirectory.f167 = ((long) Integer.reverseBytes(randomAccessFile.readInt())) & InternalZipTyphoonApp.ZIP_64_LIMIT;
                return centralDirectory;
            }
            length--;
        } while (length >= j);
        throw new ZipException("End Of Central Directory signature not found");
    }
}
