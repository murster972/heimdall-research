package android.support.multidex;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

final class MultiDexExtractor implements Closeable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final FileLock f159;

    /* renamed from: 连任  reason: contains not printable characters */
    private final FileChannel f160;

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f161;

    /* renamed from: 麤  reason: contains not printable characters */
    private final RandomAccessFile f162;

    /* renamed from: 齉  reason: contains not printable characters */
    private final File f163;

    /* renamed from: 龘  reason: contains not printable characters */
    private final File f164;

    private static class ExtractedDex extends File {
        public long crc = -1;

        public ExtractedDex(File file, String str) {
            super(file, str);
        }
    }

    MultiDexExtractor(File file, File file2) throws IOException {
        Throwable th;
        Log.i("MultiDex", "MultiDexExtractor(" + file.getPath() + ", " + file2.getPath() + ")");
        this.f164 = file;
        this.f163 = file2;
        this.f161 = m159(file);
        File file3 = new File(file2, "MultiDex.lock");
        this.f162 = new RandomAccessFile(file3, InternalZipTyphoonApp.WRITE_MODE);
        try {
            this.f160 = this.f162.getChannel();
            try {
                Log.i("MultiDex", "Blocking on lock " + file3.getPath());
                this.f159 = this.f160.lock();
                Log.i("MultiDex", file3.getPath() + " locked");
            } catch (IOException e) {
                th = e;
                m166((Closeable) this.f160);
                throw th;
            } catch (RuntimeException e2) {
                th = e2;
                m166((Closeable) this.f160);
                throw th;
            } catch (Error e3) {
                th = e3;
                m166((Closeable) this.f160);
                throw th;
            }
        } catch (IOException | Error | RuntimeException e4) {
            m166((Closeable) this.f162);
            throw e4;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static long m159(File file) throws IOException {
        long r0 = ZipUtil.m170(file);
        return r0 == -1 ? r0 - 1 : r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m160() {
        File[] listFiles = this.f163.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return !file.getName().equals("MultiDex.lock");
            }
        });
        if (listFiles == null) {
            Log.w("MultiDex", "Failed to list secondary dex dir content (" + this.f163.getPath() + ").");
            return;
        }
        for (File file : listFiles) {
            Log.i("MultiDex", "Trying to delete old file " + file.getPath() + " of size " + file.length());
            if (!file.delete()) {
                Log.w("MultiDex", "Failed to delete old file " + file.getPath());
            } else {
                Log.i("MultiDex", "Deleted old file " + file.getPath());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m161(File file) {
        long lastModified = file.lastModified();
        return lastModified == -1 ? lastModified - 1 : lastModified;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static SharedPreferences m162(Context context) {
        return context.getSharedPreferences("multidex.version", Build.VERSION.SDK_INT < 11 ? 0 : 4);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<ExtractedDex> m163() throws IOException {
        ExtractedDex extractedDex;
        boolean z;
        String str = this.f164.getName() + ".classes";
        m160();
        ArrayList arrayList = new ArrayList();
        ZipFile zipFile = new ZipFile(this.f164);
        int i = 2;
        try {
            ZipEntry entry = zipFile.getEntry("classes" + 2 + ".dex");
            while (entry != null) {
                extractedDex = new ExtractedDex(this.f163, str + i + ".zip");
                arrayList.add(extractedDex);
                Log.i("MultiDex", "Extraction is needed for file " + extractedDex);
                int i2 = 0;
                z = false;
                while (i2 < 3 && !z) {
                    i2++;
                    m167(zipFile, entry, (File) extractedDex, str);
                    extractedDex.crc = m159(extractedDex);
                    z = true;
                    Log.i("MultiDex", "Extraction " + (z ? "succeeded" : "failed") + " '" + extractedDex.getAbsolutePath() + "': length " + extractedDex.length() + " - crc: " + extractedDex.crc);
                    if (!z) {
                        extractedDex.delete();
                        if (extractedDex.exists()) {
                            Log.w("MultiDex", "Failed to delete corrupted secondary dex '" + extractedDex.getPath() + "'");
                        }
                    }
                }
                if (!z) {
                    throw new IOException("Could not create zip file " + extractedDex.getAbsolutePath() + " for secondary dex (" + i + ")");
                }
                i++;
                entry = zipFile.getEntry("classes" + i + ".dex");
            }
            try {
                zipFile.close();
            } catch (IOException e) {
                Log.w("MultiDex", "Failed to close resource", e);
            }
            return arrayList;
        } catch (IOException e2) {
            z = false;
            Log.w("MultiDex", "Failed to read crc from " + extractedDex.getAbsolutePath(), e2);
        } catch (Throwable th) {
            try {
                zipFile.close();
            } catch (IOException e3) {
                Log.w("MultiDex", "Failed to close resource", e3);
            }
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<ExtractedDex> m164(Context context, String str) throws IOException {
        Log.i("MultiDex", "loading existing secondary dex files");
        String str2 = this.f164.getName() + ".classes";
        SharedPreferences r14 = m162(context);
        int i = r14.getInt(str + "dex.number", 1);
        ArrayList arrayList = new ArrayList(i - 1);
        int i2 = 2;
        while (i2 <= i) {
            ExtractedDex extractedDex = new ExtractedDex(this.f163, str2 + i2 + ".zip");
            if (extractedDex.isFile()) {
                extractedDex.crc = m159(extractedDex);
                long j = r14.getLong(str + "dex.crc." + i2, -1);
                long j2 = r14.getLong(str + "dex.time." + i2, -1);
                long lastModified = extractedDex.lastModified();
                if (j2 == lastModified && j == extractedDex.crc) {
                    arrayList.add(extractedDex);
                    i2++;
                } else {
                    throw new IOException("Invalid extracted dex: " + extractedDex + " (key \"" + str + "\"), expected modification time: " + j2 + ", modification time: " + lastModified + ", expected crc: " + j + ", file crc: " + extractedDex.crc);
                }
            } else {
                throw new IOException("Missing extracted secondary dex file '" + extractedDex.getPath() + "'");
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m165(Context context, String str, long j, long j2, List<ExtractedDex> list) {
        SharedPreferences.Editor edit = m162(context).edit();
        edit.putLong(str + "timestamp", j);
        edit.putLong(str + "crc", j2);
        edit.putInt(str + "dex.number", list.size() + 1);
        int i = 2;
        for (ExtractedDex next : list) {
            edit.putLong(str + "dex.crc." + i, next.crc);
            edit.putLong(str + "dex.time." + i, next.lastModified());
            i++;
        }
        edit.commit();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m166(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
            Log.w("MultiDex", "Failed to close resource", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m167(ZipFile zipFile, ZipEntry zipEntry, File file, String str) throws IOException, FileNotFoundException {
        InputStream inputStream = zipFile.getInputStream(zipEntry);
        File createTempFile = File.createTempFile("tmp-" + str, ".zip", file.getParentFile());
        Log.i("MultiDex", "Extracting " + createTempFile.getPath());
        try {
            ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(createTempFile)));
            try {
                ZipEntry zipEntry2 = new ZipEntry("classes.dex");
                zipEntry2.setTime(zipEntry.getTime());
                zipOutputStream.putNextEntry(zipEntry2);
                byte[] bArr = new byte[16384];
                for (int read = inputStream.read(bArr); read != -1; read = inputStream.read(bArr)) {
                    zipOutputStream.write(bArr, 0, read);
                }
                zipOutputStream.closeEntry();
                zipOutputStream.close();
                if (!createTempFile.setReadOnly()) {
                    throw new IOException("Failed to mark readonly \"" + createTempFile.getAbsolutePath() + "\" (tmp of \"" + file.getAbsolutePath() + "\")");
                }
                Log.i("MultiDex", "Renaming to " + file.getPath());
                if (!createTempFile.renameTo(file)) {
                    throw new IOException("Failed to rename \"" + createTempFile.getAbsolutePath() + "\" to \"" + file.getAbsolutePath() + "\"");
                }
                m166((Closeable) inputStream);
                createTempFile.delete();
            } catch (Throwable th) {
                th = th;
                ZipOutputStream zipOutputStream2 = zipOutputStream;
                m166((Closeable) inputStream);
                createTempFile.delete();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            m166((Closeable) inputStream);
            createTempFile.delete();
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m168(Context context, File file, long j, String str) {
        SharedPreferences r0 = m162(context);
        return (r0.getLong(new StringBuilder().append(str).append("timestamp").toString(), -1) == m161(file) && r0.getLong(new StringBuilder().append(str).append("crc").toString(), -1) == j) ? false : true;
    }

    public void close() throws IOException {
        this.f159.release();
        this.f160.close();
        this.f162.close();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<? extends File> m169(Context context, String str, boolean z) throws IOException {
        List<ExtractedDex> r6;
        Log.i("MultiDex", "MultiDexExtractor.load(" + this.f164.getPath() + ", " + z + ", " + str + ")");
        if (!this.f159.isValid()) {
            throw new IllegalStateException("MultiDexExtractor was closed");
        }
        if (z || m168(context, this.f164, this.f161, str)) {
            if (z) {
                Log.i("MultiDex", "Forced extraction must be performed.");
            } else {
                Log.i("MultiDex", "Detected that extraction must be performed.");
            }
            r6 = m163();
            m165(context, str, m161(this.f164), this.f161, r6);
        } else {
            try {
                r6 = m164(context, str);
            } catch (IOException e) {
                Log.w("MultiDex", "Failed to reload existing extracted secondary dex files, falling back to fresh extraction", e);
                r6 = m163();
                m165(context, str, m161(this.f164), this.f161, r6);
            }
        }
        Log.i("MultiDex", "load found " + r6.size() + " secondary dex files");
        return r6;
    }
}
