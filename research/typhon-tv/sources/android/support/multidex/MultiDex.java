package android.support.multidex;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.util.Log;
import dalvik.system.DexFile;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipFile;

public final class MultiDex {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final boolean f152 = m148(System.getProperty("java.vm.version"));

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Set<File> f153 = new HashSet();

    private static final class V14 {

        /* renamed from: 龘  reason: contains not printable characters */
        private static final int f154 = ".zip".length();

        /* renamed from: 靐  reason: contains not printable characters */
        private final ElementConstructor f155;

        private interface ElementConstructor {
            /* renamed from: 龘  reason: contains not printable characters */
            Object m152(File file, DexFile dexFile) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException;
        }

        private static class ICSElementConstructor implements ElementConstructor {

            /* renamed from: 龘  reason: contains not printable characters */
            private final Constructor<?> f156;

            ICSElementConstructor(Class<?> cls) throws SecurityException, NoSuchMethodException {
                this.f156 = cls.getConstructor(new Class[]{File.class, ZipFile.class, DexFile.class});
                this.f156.setAccessible(true);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Object m153(File file, DexFile dexFile) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException {
                return this.f156.newInstance(new Object[]{file, new ZipFile(file), dexFile});
            }
        }

        private static class JBMR11ElementConstructor implements ElementConstructor {

            /* renamed from: 龘  reason: contains not printable characters */
            private final Constructor<?> f157;

            JBMR11ElementConstructor(Class<?> cls) throws SecurityException, NoSuchMethodException {
                this.f157 = cls.getConstructor(new Class[]{File.class, File.class, DexFile.class});
                this.f157.setAccessible(true);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Object m154(File file, DexFile dexFile) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
                return this.f157.newInstance(new Object[]{file, file, dexFile});
            }
        }

        private static class JBMR2ElementConstructor implements ElementConstructor {

            /* renamed from: 龘  reason: contains not printable characters */
            private final Constructor<?> f158;

            JBMR2ElementConstructor(Class<?> cls) throws SecurityException, NoSuchMethodException {
                this.f158 = cls.getConstructor(new Class[]{File.class, Boolean.TYPE, File.class, DexFile.class});
                this.f158.setAccessible(true);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Object m155(File file, DexFile dexFile) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
                return this.f158.newInstance(new Object[]{file, Boolean.FALSE, file, dexFile});
            }
        }

        private V14() throws ClassNotFoundException, SecurityException, NoSuchMethodException {
            ElementConstructor jBMR2ElementConstructor;
            Class<?> cls = Class.forName("dalvik.system.DexPathList$Element");
            try {
                jBMR2ElementConstructor = new ICSElementConstructor(cls);
            } catch (NoSuchMethodException e) {
                try {
                    jBMR2ElementConstructor = new JBMR11ElementConstructor(cls);
                } catch (NoSuchMethodException e2) {
                    jBMR2ElementConstructor = new JBMR2ElementConstructor(cls);
                }
            }
            this.f155 = jBMR2ElementConstructor;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static String m149(File file) {
            File parentFile = file.getParentFile();
            String name = file.getName();
            return new File(parentFile, name.substring(0, name.length() - f154) + ".dex").getPath();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static void m150(ClassLoader classLoader, List<? extends File> list) throws IOException, SecurityException, IllegalArgumentException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
            Object obj = MultiDex.m136(classLoader, "pathList").get(classLoader);
            Object[] r2 = new V14().m151(list);
            try {
                MultiDex.m138(obj, "dexElements", r2);
            } catch (NoSuchFieldException e) {
                Log.w("MultiDex", "Failed find field 'dexElements' attempting 'pathElements'", e);
                MultiDex.m138(obj, "pathElements", r2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Object[] m151(List<? extends File> list) throws IOException, SecurityException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
            Object[] objArr = new Object[list.size()];
            for (int i = 0; i < objArr.length; i++) {
                File file = (File) list.get(i);
                objArr[i] = this.f155.m152(file, DexFile.loadDex(file.getPath(), m149(file), 0));
            }
            return objArr;
        }
    }

    private static final class V19 {
        /* renamed from: 龘  reason: contains not printable characters */
        static void m156(ClassLoader classLoader, List<? extends File> list, File file) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, InvocationTargetException, NoSuchMethodException, IOException {
            IOException[] iOExceptionArr;
            Object obj = MultiDex.m136(classLoader, "pathList").get(classLoader);
            ArrayList arrayList = new ArrayList();
            MultiDex.m138(obj, "dexElements", m157(obj, new ArrayList(list), file, arrayList));
            if (arrayList.size() > 0) {
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    Log.w("MultiDex", "Exception in makeDexElement", (IOException) it2.next());
                }
                Field r7 = MultiDex.m136(obj, "dexElementsSuppressedExceptions");
                IOException[] iOExceptionArr2 = (IOException[]) r7.get(obj);
                if (iOExceptionArr2 == null) {
                    iOExceptionArr = (IOException[]) arrayList.toArray(new IOException[arrayList.size()]);
                } else {
                    IOException[] iOExceptionArr3 = new IOException[(arrayList.size() + iOExceptionArr2.length)];
                    arrayList.toArray(iOExceptionArr3);
                    System.arraycopy(iOExceptionArr2, 0, iOExceptionArr3, arrayList.size(), iOExceptionArr2.length);
                    iOExceptionArr = iOExceptionArr3;
                }
                r7.set(obj, iOExceptionArr);
                IOException iOException = new IOException("I/O exception during makeDexElement");
                iOException.initCause((Throwable) arrayList.get(0));
                throw iOException;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static Object[] m157(Object obj, ArrayList<File> arrayList, File file, ArrayList<IOException> arrayList2) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
            return (Object[]) MultiDex.m137(obj, "makeDexElements", (Class<?>[]) new Class[]{ArrayList.class, File.class, ArrayList.class}).invoke(obj, new Object[]{arrayList, file, arrayList2});
        }
    }

    private static final class V4 {
        /* renamed from: 龘  reason: contains not printable characters */
        static void m158(ClassLoader classLoader, List<? extends File> list) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, IOException {
            int size = list.size();
            Field r10 = MultiDex.m136(classLoader, "path");
            StringBuilder sb = new StringBuilder((String) r10.get(classLoader));
            String[] strArr = new String[size];
            File[] fileArr = new File[size];
            ZipFile[] zipFileArr = new ZipFile[size];
            DexFile[] dexFileArr = new DexFile[size];
            ListIterator<? extends File> listIterator = list.listIterator();
            while (listIterator.hasNext()) {
                File file = (File) listIterator.next();
                String absolutePath = file.getAbsolutePath();
                sb.append(':').append(absolutePath);
                int previousIndex = listIterator.previousIndex();
                strArr[previousIndex] = absolutePath;
                fileArr[previousIndex] = file;
                zipFileArr[previousIndex] = new ZipFile(file);
                dexFileArr[previousIndex] = DexFile.loadDex(absolutePath, absolutePath + ".dex", 0);
            }
            r10.set(classLoader, sb.toString());
            MultiDex.m138((Object) classLoader, "mPaths", (Object[]) strArr);
            MultiDex.m138((Object) classLoader, "mFiles", (Object[]) fileArr);
            MultiDex.m138((Object) classLoader, "mZips", (Object[]) zipFileArr);
            MultiDex.m138((Object) classLoader, "mDexs", (Object[]) dexFileArr);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static ApplicationInfo m135(Context context) {
        try {
            return context.getApplicationInfo();
        } catch (RuntimeException e) {
            Log.w("MultiDex", "Failure while trying to obtain ApplicationInfo from Context. Must be running in test mode. Skip patching.", e);
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static Field m136(Object obj, String str) throws NoSuchFieldException {
        Class cls = obj.getClass();
        while (cls != null) {
            try {
                Field declaredField = cls.getDeclaredField(str);
                if (!declaredField.isAccessible()) {
                    declaredField.setAccessible(true);
                }
                return declaredField;
            } catch (NoSuchFieldException e) {
                cls = cls.getSuperclass();
            }
        }
        throw new NoSuchFieldException("Field " + str + " not found in " + obj.getClass());
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static Method m137(Object obj, String str, Class<?>... clsArr) throws NoSuchMethodException {
        Class cls = obj.getClass();
        while (cls != null) {
            try {
                Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
                if (!declaredMethod.isAccessible()) {
                    declaredMethod.setAccessible(true);
                }
                return declaredMethod;
            } catch (NoSuchMethodException e) {
                cls = cls.getSuperclass();
            }
        }
        throw new NoSuchMethodException("Method " + str + " with parameters " + Arrays.asList(clsArr) + " not found in " + obj.getClass());
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m138(Object obj, String str, Object[] objArr) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Field r1 = m136(obj, str);
        Object[] objArr2 = (Object[]) r1.get(obj);
        Object[] objArr3 = (Object[]) Array.newInstance(objArr2.getClass().getComponentType(), objArr2.length + objArr.length);
        System.arraycopy(objArr2, 0, objArr3, 0, objArr2.length);
        System.arraycopy(objArr, 0, objArr3, objArr2.length, objArr.length);
        r1.set(obj, objArr3);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static void m139(Context context) throws Exception {
        File file = new File(context.getFilesDir(), "secondary-dexes");
        if (file.isDirectory()) {
            Log.i("MultiDex", "Clearing old secondary dex dir (" + file.getPath() + ").");
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                Log.w("MultiDex", "Failed to list secondary dex dir content (" + file.getPath() + ").");
                return;
            }
            for (File file2 : listFiles) {
                Log.i("MultiDex", "Trying to delete old file " + file2.getPath() + " of size " + file2.length());
                if (!file2.delete()) {
                    Log.w("MultiDex", "Failed to delete old file " + file2.getPath());
                } else {
                    Log.i("MultiDex", "Deleted old file " + file2.getPath());
                }
            }
            if (!file.delete()) {
                Log.w("MultiDex", "Failed to delete secondary dex dir " + file.getPath());
            } else {
                Log.i("MultiDex", "Deleted old secondary dex dir " + file.getPath());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static File m140(Context context, File file, String str) throws IOException {
        File file2 = new File(file, "code_cache");
        try {
            m145(file2);
        } catch (IOException e) {
            file2 = new File(context.getFilesDir(), "code_cache");
            m145(file2);
        }
        File file3 = new File(file2, str);
        m145(file3);
        return file3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m143(Context context) {
        Log.i("MultiDex", "Installing application");
        if (f152) {
            Log.i("MultiDex", "VM has multidex support, MultiDex support library is disabled.");
        } else if (Build.VERSION.SDK_INT < 4) {
            throw new RuntimeException("MultiDex installation failed. SDK " + Build.VERSION.SDK_INT + " is unsupported. Min SDK version is " + 4 + ".");
        } else {
            try {
                ApplicationInfo r6 = m135(context);
                if (r6 == null) {
                    Log.i("MultiDex", "No ApplicationInfo available, i.e. running on a test Context: MultiDex support library is disabled.");
                    return;
                }
                m144(context, new File(r6.sourceDir), new File(r6.dataDir), "secondary-dexes", "", true);
                Log.i("MultiDex", "install done");
            } catch (Exception e) {
                Log.e("MultiDex", "MultiDex installation failure", e);
                throw new RuntimeException("MultiDex installation failed (" + e.getMessage() + ").");
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m144(android.content.Context r11, java.io.File r12, java.io.File r13, java.lang.String r14, java.lang.String r15, boolean r16) throws java.io.IOException, java.lang.IllegalArgumentException, java.lang.IllegalAccessException, java.lang.NoSuchFieldException, java.lang.reflect.InvocationTargetException, java.lang.NoSuchMethodException, java.lang.SecurityException, java.lang.ClassNotFoundException, java.lang.InstantiationException {
        /*
            java.util.Set<java.io.File> r8 = f153
            monitor-enter(r8)
            java.util.Set<java.io.File> r7 = f153     // Catch:{ all -> 0x0079 }
            boolean r7 = r7.contains(r12)     // Catch:{ all -> 0x0079 }
            if (r7 == 0) goto L_0x000d
            monitor-exit(r8)     // Catch:{ all -> 0x0079 }
        L_0x000c:
            return
        L_0x000d:
            java.util.Set<java.io.File> r7 = f153     // Catch:{ all -> 0x0079 }
            r7.add(r12)     // Catch:{ all -> 0x0079 }
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0079 }
            r9 = 20
            if (r7 <= r9) goto L_0x0068
            java.lang.String r7 = "MultiDex"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            r9.<init>()     // Catch:{ all -> 0x0079 }
            java.lang.String r10 = "MultiDex is not guaranteed to work in SDK version "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0079 }
            int r10 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0079 }
            java.lang.String r10 = ": SDK version higher than "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0079 }
            r10 = 20
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0079 }
            java.lang.String r10 = " should be backed by "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0079 }
            java.lang.String r10 = "runtime with built-in multidex capabilty but it's not the "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0079 }
            java.lang.String r10 = "case here: java.vm.version=\""
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0079 }
            java.lang.String r10 = "java.vm.version"
            java.lang.String r10 = java.lang.System.getProperty(r10)     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0079 }
            java.lang.String r10 = "\""
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0079 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0079 }
            android.util.Log.w(r7, r9)     // Catch:{ all -> 0x0079 }
        L_0x0068:
            java.lang.ClassLoader r5 = r11.getClassLoader()     // Catch:{ RuntimeException -> 0x007c }
            if (r5 != 0) goto L_0x0088
            java.lang.String r7 = "MultiDex"
            java.lang.String r9 = "Context class loader is null. Must be running in test mode. Skip patching."
            android.util.Log.e(r7, r9)     // Catch:{ all -> 0x0079 }
            monitor-exit(r8)     // Catch:{ all -> 0x0079 }
            goto L_0x000c
        L_0x0079:
            r7 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0079 }
            throw r7
        L_0x007c:
            r2 = move-exception
            java.lang.String r7 = "MultiDex"
            java.lang.String r9 = "Failure while trying to obtain Context class loader. Must be running in test mode. Skip patching."
            android.util.Log.w(r7, r9, r2)     // Catch:{ all -> 0x0079 }
            monitor-exit(r8)     // Catch:{ all -> 0x0079 }
            goto L_0x000c
        L_0x0088:
            m139(r11)     // Catch:{ Throwable -> 0x00a3 }
        L_0x008b:
            java.io.File r1 = m140((android.content.Context) r11, (java.io.File) r13, (java.lang.String) r14)     // Catch:{ all -> 0x0079 }
            android.support.multidex.MultiDexExtractor r3 = new android.support.multidex.MultiDexExtractor     // Catch:{ all -> 0x0079 }
            r3.<init>(r12, r1)     // Catch:{ all -> 0x0079 }
            r0 = 0
            r7 = 0
            java.util.List r4 = r3.m169(r11, r15, r7)     // Catch:{ all -> 0x00b2 }
            m146((java.lang.ClassLoader) r5, (java.io.File) r1, (java.util.List<? extends java.io.File>) r4)     // Catch:{ IOException -> 0x00ae }
        L_0x009d:
            r3.close()     // Catch:{ IOException -> 0x00c9 }
        L_0x00a0:
            if (r0 == 0) goto L_0x00cf
            throw r0     // Catch:{ all -> 0x0079 }
        L_0x00a3:
            r6 = move-exception
            java.lang.String r7 = "MultiDex"
            java.lang.String r9 = "Something went wrong when trying to clear old MultiDex extraction, continuing without cleaning."
            android.util.Log.w(r7, r9, r6)     // Catch:{ all -> 0x0079 }
            goto L_0x008b
        L_0x00ae:
            r2 = move-exception
            if (r16 != 0) goto L_0x00b7
            throw r2     // Catch:{ all -> 0x00b2 }
        L_0x00b2:
            r7 = move-exception
            r3.close()     // Catch:{ IOException -> 0x00cc }
        L_0x00b6:
            throw r7     // Catch:{ all -> 0x0079 }
        L_0x00b7:
            java.lang.String r7 = "MultiDex"
            java.lang.String r9 = "Failed to install extracted secondary dex files, retrying with forced extraction"
            android.util.Log.w(r7, r9, r2)     // Catch:{ all -> 0x00b2 }
            r7 = 1
            java.util.List r4 = r3.m169(r11, r15, r7)     // Catch:{ all -> 0x00b2 }
            m146((java.lang.ClassLoader) r5, (java.io.File) r1, (java.util.List<? extends java.io.File>) r4)     // Catch:{ all -> 0x00b2 }
            goto L_0x009d
        L_0x00c9:
            r2 = move-exception
            r0 = r2
            goto L_0x00a0
        L_0x00cc:
            r2 = move-exception
            r0 = r2
            goto L_0x00b6
        L_0x00cf:
            monitor-exit(r8)     // Catch:{ all -> 0x0079 }
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.multidex.MultiDex.m144(android.content.Context, java.io.File, java.io.File, java.lang.String, java.lang.String, boolean):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m145(File file) throws IOException {
        file.mkdir();
        if (!file.isDirectory()) {
            File parentFile = file.getParentFile();
            if (parentFile == null) {
                Log.e("MultiDex", "Failed to create dir " + file.getPath() + ". Parent file is null.");
            } else {
                Log.e("MultiDex", "Failed to create dir " + file.getPath() + ". parent file is a dir " + parentFile.isDirectory() + ", a file " + parentFile.isFile() + ", exists " + parentFile.exists() + ", readable " + parentFile.canRead() + ", writable " + parentFile.canWrite());
            }
            throw new IOException("Failed to create directory " + file.getPath());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m146(ClassLoader classLoader, File file, List<? extends File> list) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, InvocationTargetException, NoSuchMethodException, IOException, SecurityException, ClassNotFoundException, InstantiationException {
        if (list.isEmpty()) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            V19.m156(classLoader, list, file);
        } else if (Build.VERSION.SDK_INT >= 14) {
            V14.m150(classLoader, list);
        } else {
            V4.m158(classLoader, list);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m148(String str) {
        boolean z = false;
        if (str != null) {
            Matcher matcher = Pattern.compile("(\\d+)\\.(\\d+)(\\.\\d+)?").matcher(str);
            if (matcher.matches()) {
                try {
                    int parseInt = Integer.parseInt(matcher.group(1));
                    z = parseInt > 2 || (parseInt == 2 && Integer.parseInt(matcher.group(2)) >= 1);
                } catch (NumberFormatException e) {
                }
            }
        }
        Log.i("MultiDex", "VM with version " + str + (z ? " has multidex support" : " does not have multidex support"));
        return z;
    }
}
