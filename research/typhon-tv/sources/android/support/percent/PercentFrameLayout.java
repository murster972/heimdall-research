package android.support.percent;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.percent.PercentLayoutHelper;
import android.util.AttributeSet;
import android.widget.FrameLayout;

@Deprecated
public class PercentFrameLayout extends FrameLayout {

    /* renamed from: 龘  reason: contains not printable characters */
    private final PercentLayoutHelper f168 = new PercentLayoutHelper(this);

    @Deprecated
    public static class LayoutParams extends FrameLayout.LayoutParams implements PercentLayoutHelper.PercentLayoutParams {

        /* renamed from: 龘  reason: contains not printable characters */
        private PercentLayoutHelper.PercentLayoutInfo f169;

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f169 = PercentLayoutHelper.m177(context, attributeSet);
        }

        /* access modifiers changed from: protected */
        public void setBaseAttributes(TypedArray typedArray, int i, int i2) {
            PercentLayoutHelper.m178(this, typedArray, i, i2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PercentLayoutHelper.PercentLayoutInfo m175() {
            if (this.f169 == null) {
                this.f169 = new PercentLayoutHelper.PercentLayoutInfo();
            }
            return this.f169;
        }
    }

    public PercentFrameLayout(Context context) {
        super(context);
    }

    public PercentFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PercentFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.f168.m181();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.f168.m182(i, i2);
        super.onMeasure(i, i2);
        if (this.f168.m180()) {
            super.onMeasure(i, i2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }
}
