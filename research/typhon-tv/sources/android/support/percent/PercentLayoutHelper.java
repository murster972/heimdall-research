package android.support.percent;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

@Deprecated
public class PercentLayoutHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ViewGroup f170;

    @Deprecated
    public static class PercentLayoutInfo {

        /* renamed from: ʻ  reason: contains not printable characters */
        public float f171 = -1.0f;

        /* renamed from: ʼ  reason: contains not printable characters */
        public float f172 = -1.0f;

        /* renamed from: ʽ  reason: contains not printable characters */
        public float f173 = -1.0f;

        /* renamed from: ˑ  reason: contains not printable characters */
        public float f174;

        /* renamed from: ٴ  reason: contains not printable characters */
        final PercentMarginLayoutParams f175 = new PercentMarginLayoutParams(0, 0);

        /* renamed from: 连任  reason: contains not printable characters */
        public float f176 = -1.0f;

        /* renamed from: 靐  reason: contains not printable characters */
        public float f177 = -1.0f;

        /* renamed from: 麤  reason: contains not printable characters */
        public float f178 = -1.0f;

        /* renamed from: 齉  reason: contains not printable characters */
        public float f179 = -1.0f;

        /* renamed from: 龘  reason: contains not printable characters */
        public float f180 = -1.0f;

        public String toString() {
            return String.format("PercentLayoutInformation width: %f height %f, margins (%f, %f,  %f, %f, %f, %f)", new Object[]{Float.valueOf(this.f180), Float.valueOf(this.f177), Float.valueOf(this.f179), Float.valueOf(this.f178), Float.valueOf(this.f176), Float.valueOf(this.f171), Float.valueOf(this.f172), Float.valueOf(this.f173)});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m183(View view, ViewGroup.MarginLayoutParams marginLayoutParams, int i, int i2) {
            m185(marginLayoutParams, i, i2);
            this.f175.leftMargin = marginLayoutParams.leftMargin;
            this.f175.topMargin = marginLayoutParams.topMargin;
            this.f175.rightMargin = marginLayoutParams.rightMargin;
            this.f175.bottomMargin = marginLayoutParams.bottomMargin;
            MarginLayoutParamsCompat.setMarginStart(this.f175, MarginLayoutParamsCompat.getMarginStart(marginLayoutParams));
            MarginLayoutParamsCompat.setMarginEnd(this.f175, MarginLayoutParamsCompat.getMarginEnd(marginLayoutParams));
            if (this.f179 >= 0.0f) {
                marginLayoutParams.leftMargin = Math.round(((float) i) * this.f179);
            }
            if (this.f178 >= 0.0f) {
                marginLayoutParams.topMargin = Math.round(((float) i2) * this.f178);
            }
            if (this.f176 >= 0.0f) {
                marginLayoutParams.rightMargin = Math.round(((float) i) * this.f176);
            }
            if (this.f171 >= 0.0f) {
                marginLayoutParams.bottomMargin = Math.round(((float) i2) * this.f171);
            }
            boolean z = false;
            if (this.f172 >= 0.0f) {
                MarginLayoutParamsCompat.setMarginStart(marginLayoutParams, Math.round(((float) i) * this.f172));
                z = true;
            }
            if (this.f173 >= 0.0f) {
                MarginLayoutParamsCompat.setMarginEnd(marginLayoutParams, Math.round(((float) i) * this.f173));
                z = true;
            }
            if (z && view != null) {
                MarginLayoutParamsCompat.resolveLayoutDirection(marginLayoutParams, ViewCompat.getLayoutDirection(view));
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m184(ViewGroup.LayoutParams layoutParams) {
            if (!this.f175.f181) {
                layoutParams.width = this.f175.width;
            }
            if (!this.f175.f182) {
                layoutParams.height = this.f175.height;
            }
            boolean unused = this.f175.f181 = false;
            boolean unused2 = this.f175.f182 = false;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m185(ViewGroup.LayoutParams layoutParams, int i, int i2) {
            this.f175.width = layoutParams.width;
            this.f175.height = layoutParams.height;
            boolean z = (this.f175.f181 || this.f175.width == 0) && this.f180 < 0.0f;
            boolean z2 = (this.f175.f182 || this.f175.height == 0) && this.f177 < 0.0f;
            if (this.f180 >= 0.0f) {
                layoutParams.width = Math.round(((float) i) * this.f180);
            }
            if (this.f177 >= 0.0f) {
                layoutParams.height = Math.round(((float) i2) * this.f177);
            }
            if (this.f174 >= 0.0f) {
                if (z) {
                    layoutParams.width = Math.round(((float) layoutParams.height) * this.f174);
                    boolean unused = this.f175.f181 = true;
                }
                if (z2) {
                    layoutParams.height = Math.round(((float) layoutParams.width) / this.f174);
                    boolean unused2 = this.f175.f182 = true;
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m186(ViewGroup.MarginLayoutParams marginLayoutParams) {
            m184((ViewGroup.LayoutParams) marginLayoutParams);
            marginLayoutParams.leftMargin = this.f175.leftMargin;
            marginLayoutParams.topMargin = this.f175.topMargin;
            marginLayoutParams.rightMargin = this.f175.rightMargin;
            marginLayoutParams.bottomMargin = this.f175.bottomMargin;
            MarginLayoutParamsCompat.setMarginStart(marginLayoutParams, MarginLayoutParamsCompat.getMarginStart(this.f175));
            MarginLayoutParamsCompat.setMarginEnd(marginLayoutParams, MarginLayoutParamsCompat.getMarginEnd(this.f175));
        }
    }

    @Deprecated
    public interface PercentLayoutParams {
        /* renamed from: 龘  reason: contains not printable characters */
        PercentLayoutInfo m187();
    }

    static class PercentMarginLayoutParams extends ViewGroup.MarginLayoutParams {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean f181;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean f182;

        public PercentMarginLayoutParams(int i, int i2) {
            super(i, i2);
        }
    }

    public PercentLayoutHelper(ViewGroup viewGroup) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("host must be non-null");
        }
        this.f170 = viewGroup;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m176(View view, PercentLayoutInfo percentLayoutInfo) {
        return (view.getMeasuredHeightAndState() & -16777216) == 16777216 && percentLayoutInfo.f177 >= 0.0f && percentLayoutInfo.f175.height == -2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static PercentLayoutInfo m177(Context context, AttributeSet attributeSet) {
        PercentLayoutInfo percentLayoutInfo = null;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.PercentLayout_Layout);
        float fraction = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_widthPercent, 1, 1, -1.0f);
        if (fraction != -1.0f) {
            if (0 == 0) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f180 = fraction;
        }
        float fraction2 = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_heightPercent, 1, 1, -1.0f);
        if (fraction2 != -1.0f) {
            if (percentLayoutInfo == null) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f177 = fraction2;
        }
        float fraction3 = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_marginPercent, 1, 1, -1.0f);
        if (fraction3 != -1.0f) {
            if (percentLayoutInfo == null) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f179 = fraction3;
            percentLayoutInfo.f178 = fraction3;
            percentLayoutInfo.f176 = fraction3;
            percentLayoutInfo.f171 = fraction3;
        }
        float fraction4 = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_marginLeftPercent, 1, 1, -1.0f);
        if (fraction4 != -1.0f) {
            if (percentLayoutInfo == null) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f179 = fraction4;
        }
        float fraction5 = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_marginTopPercent, 1, 1, -1.0f);
        if (fraction5 != -1.0f) {
            if (percentLayoutInfo == null) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f178 = fraction5;
        }
        float fraction6 = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_marginRightPercent, 1, 1, -1.0f);
        if (fraction6 != -1.0f) {
            if (percentLayoutInfo == null) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f176 = fraction6;
        }
        float fraction7 = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_marginBottomPercent, 1, 1, -1.0f);
        if (fraction7 != -1.0f) {
            if (percentLayoutInfo == null) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f171 = fraction7;
        }
        float fraction8 = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_marginStartPercent, 1, 1, -1.0f);
        if (fraction8 != -1.0f) {
            if (percentLayoutInfo == null) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f172 = fraction8;
        }
        float fraction9 = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_marginEndPercent, 1, 1, -1.0f);
        if (fraction9 != -1.0f) {
            if (percentLayoutInfo == null) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f173 = fraction9;
        }
        float fraction10 = obtainStyledAttributes.getFraction(R.styleable.PercentLayout_Layout_layout_aspectRatio, 1, 1, -1.0f);
        if (fraction10 != -1.0f) {
            if (percentLayoutInfo == null) {
                percentLayoutInfo = new PercentLayoutInfo();
            }
            percentLayoutInfo.f174 = fraction10;
        }
        obtainStyledAttributes.recycle();
        return percentLayoutInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m178(ViewGroup.LayoutParams layoutParams, TypedArray typedArray, int i, int i2) {
        layoutParams.width = typedArray.getLayoutDimension(i, 0);
        layoutParams.height = typedArray.getLayoutDimension(i2, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m179(View view, PercentLayoutInfo percentLayoutInfo) {
        return (view.getMeasuredWidthAndState() & -16777216) == 16777216 && percentLayoutInfo.f180 >= 0.0f && percentLayoutInfo.f175.width == -2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m180() {
        PercentLayoutInfo r2;
        boolean z = false;
        int childCount = this.f170.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.f170.getChildAt(i);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof PercentLayoutParams) && (r2 = ((PercentLayoutParams) layoutParams).m187()) != null) {
                if (m179(childAt, r2)) {
                    z = true;
                    layoutParams.width = -2;
                }
                if (m176(childAt, r2)) {
                    z = true;
                    layoutParams.height = -2;
                }
            }
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m181() {
        PercentLayoutInfo r2;
        int childCount = this.f170.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ViewGroup.LayoutParams layoutParams = this.f170.getChildAt(i).getLayoutParams();
            if ((layoutParams instanceof PercentLayoutParams) && (r2 = ((PercentLayoutParams) layoutParams).m187()) != null) {
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    r2.m186((ViewGroup.MarginLayoutParams) layoutParams);
                } else {
                    r2.m184(layoutParams);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m182(int i, int i2) {
        PercentLayoutInfo r3;
        int size = (View.MeasureSpec.getSize(i) - this.f170.getPaddingLeft()) - this.f170.getPaddingRight();
        int size2 = (View.MeasureSpec.getSize(i2) - this.f170.getPaddingTop()) - this.f170.getPaddingBottom();
        int childCount = this.f170.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.f170.getChildAt(i3);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof PercentLayoutParams) && (r3 = ((PercentLayoutParams) layoutParams).m187()) != null) {
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    r3.m183(childAt, (ViewGroup.MarginLayoutParams) layoutParams, size, size2);
                } else {
                    r3.m185(layoutParams, size, size2);
                }
            }
        }
    }
}
