package android.support.coreui;

public final class R {

    public static final class attr {
        public static final int font = 2130968819;
        public static final int fontProviderAuthority = 2130968821;
        public static final int fontProviderCerts = 2130968822;
        public static final int fontProviderFetchStrategy = 2130968823;
        public static final int fontProviderFetchTimeout = 2130968824;
        public static final int fontProviderPackage = 2130968825;
        public static final int fontProviderQuery = 2130968826;
        public static final int fontStyle = 2130968827;
        public static final int fontWeight = 2130968828;
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2131034112;
    }

    public static final class color {
        public static final int notification_action_color_filter = 2131099830;
        public static final int notification_icon_bg_color = 2131099831;
        public static final int ripple_material_light = 2131099842;
        public static final int secondary_text_default_material_light = 2131099845;
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131165308;
        public static final int compat_button_inset_vertical_material = 2131165309;
        public static final int compat_button_padding_horizontal_material = 2131165310;
        public static final int compat_button_padding_vertical_material = 2131165311;
        public static final int compat_control_corner_material = 2131165312;
        public static final int notification_action_icon_size = 2131165496;
        public static final int notification_action_text_size = 2131165497;
        public static final int notification_big_circle_margin = 2131165498;
        public static final int notification_content_margin_start = 2131165499;
        public static final int notification_large_icon_height = 2131165500;
        public static final int notification_large_icon_width = 2131165501;
        public static final int notification_main_column_padding_top = 2131165502;
        public static final int notification_media_narrow_margin = 2131165503;
        public static final int notification_right_icon_size = 2131165504;
        public static final int notification_right_side_padding_top = 2131165505;
        public static final int notification_small_icon_background_padding = 2131165506;
        public static final int notification_small_icon_size_as_large = 2131165507;
        public static final int notification_subtext_size = 2131165508;
        public static final int notification_top_pad = 2131165509;
        public static final int notification_top_pad_large_text = 2131165510;
    }

    public static final class drawable {
        public static final int notification_action_background = 2131231280;
        public static final int notification_bg = 2131231281;
        public static final int notification_bg_low = 2131231282;
        public static final int notification_bg_low_normal = 2131231283;
        public static final int notification_bg_low_pressed = 2131231284;
        public static final int notification_bg_normal = 2131231285;
        public static final int notification_bg_normal_pressed = 2131231286;
        public static final int notification_icon_background = 2131231287;
        public static final int notification_template_icon_bg = 2131231288;
        public static final int notification_template_icon_low_bg = 2131231289;
        public static final int notification_tile_bg = 2131231290;
        public static final int notify_panel_notification_icon_bg = 2131231291;
    }

    public static final class id {
        public static final int action_container = 2131296285;
        public static final int action_divider = 2131296287;
        public static final int action_image = 2131296290;
        public static final int action_text = 2131296313;
        public static final int actions = 2131296316;
        public static final int async = 2131296339;
        public static final int blocking = 2131296369;
        public static final int chronometer = 2131296411;
        public static final int forever = 2131296497;
        public static final int icon = 2131296505;
        public static final int icon_group = 2131296506;
        public static final int info = 2131296513;
        public static final int italic = 2131296517;
        public static final int line1 = 2131296557;
        public static final int line3 = 2131296558;
        public static final int normal = 2131296762;
        public static final int notification_background = 2131296763;
        public static final int notification_main_column = 2131296764;
        public static final int notification_main_column_container = 2131296765;
        public static final int right_icon = 2131296830;
        public static final int right_side = 2131296831;
        public static final int tag_transition_group = 2131296899;
        public static final int text = 2131296900;
        public static final int text2 = 2131296901;
        public static final int time = 2131296910;
        public static final int title = 2131296913;
    }

    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 2131361809;
    }

    public static final class layout {
        public static final int notification_action = 2131493047;
        public static final int notification_action_tombstone = 2131493048;
        public static final int notification_template_custom_big = 2131493055;
        public static final int notification_template_icon_group = 2131493056;
        public static final int notification_template_part_chronometer = 2131493060;
        public static final int notification_template_part_time = 2131493061;
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131821096;
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131886361;
        public static final int TextAppearance_Compat_Notification_Info = 2131886362;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131886364;
        public static final int TextAppearance_Compat_Notification_Time = 2131886367;
        public static final int TextAppearance_Compat_Notification_Title = 2131886369;
        public static final int Widget_Compat_NotificationActionContainer = 2131886497;
        public static final int Widget_Compat_NotificationActionText = 2131886498;
    }

    public static final class styleable {
        public static final int[] FontFamily = {com.typhoon.tv.R.attr.fontProviderAuthority, com.typhoon.tv.R.attr.fontProviderCerts, com.typhoon.tv.R.attr.fontProviderFetchStrategy, com.typhoon.tv.R.attr.fontProviderFetchTimeout, com.typhoon.tv.R.attr.fontProviderPackage, com.typhoon.tv.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, com.typhoon.tv.R.attr.font, com.typhoon.tv.R.attr.fontStyle, com.typhoon.tv.R.attr.fontWeight};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_font = 3;
        public static final int FontFamilyFont_fontStyle = 4;
        public static final int FontFamilyFont_fontWeight = 5;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
    }
}
