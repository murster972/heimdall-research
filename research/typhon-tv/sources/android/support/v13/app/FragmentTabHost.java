package android.support.v13.app;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private TabHost.OnTabChangeListener f343;

    /* renamed from: ʼ  reason: contains not printable characters */
    private TabInfo f344;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f345;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f346;

    /* renamed from: 靐  reason: contains not printable characters */
    private FrameLayout f347;

    /* renamed from: 麤  reason: contains not printable characters */
    private FragmentManager f348;

    /* renamed from: 齉  reason: contains not printable characters */
    private Context f349;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ArrayList<TabInfo> f350 = new ArrayList<>();

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: 龘  reason: contains not printable characters */
        String f351;

        SavedState(Parcel parcel) {
            super(parcel);
            this.f351 = parcel.readString();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.f351 + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.f351);
        }
    }

    static final class TabInfo {

        /* renamed from: 靐  reason: contains not printable characters */
        final Class<?> f352;

        /* renamed from: 麤  reason: contains not printable characters */
        Fragment f353;

        /* renamed from: 齉  reason: contains not printable characters */
        final Bundle f354;

        /* renamed from: 龘  reason: contains not printable characters */
        final String f355;
    }

    public FragmentTabHost(Context context) {
        super(context, (AttributeSet) null);
        m412(context, (AttributeSet) null);
    }

    public FragmentTabHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m412(context, attributeSet);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private FragmentTransaction m409(String str, FragmentTransaction fragmentTransaction) {
        TabInfo tabInfo = null;
        for (int i = 0; i < this.f350.size(); i++) {
            TabInfo tabInfo2 = this.f350.get(i);
            if (tabInfo2.f355.equals(str)) {
                tabInfo = tabInfo2;
            }
        }
        if (tabInfo == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.f344 != tabInfo) {
            if (fragmentTransaction == null) {
                fragmentTransaction = this.f348.beginTransaction();
            }
            if (!(this.f344 == null || this.f344.f353 == null)) {
                fragmentTransaction.detach(this.f344.f353);
            }
            if (tabInfo != null) {
                if (tabInfo.f353 == null) {
                    tabInfo.f353 = Fragment.instantiate(this.f349, tabInfo.f352.getName(), tabInfo.f354);
                    fragmentTransaction.add(this.f346, tabInfo.f353, tabInfo.f355);
                } else {
                    fragmentTransaction.attach(tabInfo.f353);
                }
            }
            this.f344 = tabInfo;
        }
        return fragmentTransaction;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m410() {
        if (this.f347 == null) {
            this.f347 = (FrameLayout) findViewById(this.f346);
            if (this.f347 == null) {
                throw new IllegalStateException("No tab content FrameLayout found for id " + this.f346);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m411(Context context) {
        if (findViewById(16908307) == null) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            addView(linearLayout, new FrameLayout.LayoutParams(-1, -1));
            TabWidget tabWidget = new TabWidget(context);
            tabWidget.setId(16908307);
            tabWidget.setOrientation(0);
            linearLayout.addView(tabWidget, new LinearLayout.LayoutParams(-1, -2, 0.0f));
            FrameLayout frameLayout = new FrameLayout(context);
            frameLayout.setId(16908305);
            linearLayout.addView(frameLayout, new LinearLayout.LayoutParams(0, 0, 0.0f));
            FrameLayout frameLayout2 = new FrameLayout(context);
            this.f347 = frameLayout2;
            this.f347.setId(this.f346);
            linearLayout.addView(frameLayout2, new LinearLayout.LayoutParams(-1, 0, 1.0f));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m412(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842995}, 0, 0);
        this.f346 = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener(this);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        FragmentTransaction fragmentTransaction = null;
        for (int i = 0; i < this.f350.size(); i++) {
            TabInfo tabInfo = this.f350.get(i);
            tabInfo.f353 = this.f348.findFragmentByTag(tabInfo.f355);
            if (tabInfo.f353 != null && !tabInfo.f353.isDetached()) {
                if (tabInfo.f355.equals(currentTabTag)) {
                    this.f344 = tabInfo;
                } else {
                    if (fragmentTransaction == null) {
                        fragmentTransaction = this.f348.beginTransaction();
                    }
                    fragmentTransaction.detach(tabInfo.f353);
                }
            }
        }
        this.f345 = true;
        FragmentTransaction r1 = m409(currentTabTag, fragmentTransaction);
        if (r1 != null) {
            r1.commit();
            this.f348.executePendingTransactions();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f345 = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.f351);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f351 = getCurrentTabTag();
        return savedState;
    }

    public void onTabChanged(String str) {
        FragmentTransaction r0;
        if (this.f345 && (r0 = m409(str, (FragmentTransaction) null)) != null) {
            r0.commit();
        }
        if (this.f343 != null) {
            this.f343.onTabChanged(str);
        }
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.f343 = onTabChangeListener;
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }

    public void setup(Context context, FragmentManager fragmentManager) {
        m411(context);
        super.setup();
        this.f349 = context;
        this.f348 = fragmentManager;
        m410();
    }

    public void setup(Context context, FragmentManager fragmentManager, int i) {
        m411(context);
        super.setup();
        this.f349 = context;
        this.f348 = fragmentManager;
        this.f346 = i;
        m410();
        this.f347.setId(i);
        if (getId() == -1) {
            setId(16908306);
        }
    }
}
