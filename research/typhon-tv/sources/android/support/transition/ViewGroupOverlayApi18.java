package android.support.transition;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;

class ViewGroupOverlayApi18 implements ViewGroupOverlayImpl {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ViewGroupOverlay f292;

    ViewGroupOverlayApi18(ViewGroup viewGroup) {
        this.f292 = viewGroup.getOverlay();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m318(Drawable drawable) {
        this.f292.remove(drawable);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m319(View view) {
        this.f292.remove(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m320(Drawable drawable) {
        this.f292.add(drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m321(View view) {
        this.f292.add(view);
    }
}
