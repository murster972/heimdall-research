package android.support.transition;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;

class ViewOverlayApi18 implements ViewOverlayImpl {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ViewOverlay f308;

    ViewOverlayApi18(View view) {
        this.f308 = view.getOverlay();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m344(Drawable drawable) {
        this.f308.remove(drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m345(Drawable drawable) {
        this.f308.add(drawable);
    }
}
