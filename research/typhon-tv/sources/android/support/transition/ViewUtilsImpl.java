package android.support.transition;

import android.graphics.Matrix;
import android.view.View;

interface ViewUtilsImpl {
    /* renamed from: 连任  reason: contains not printable characters */
    void m386(View view);

    /* renamed from: 靐  reason: contains not printable characters */
    WindowIdImpl m387(View view);

    /* renamed from: 靐  reason: contains not printable characters */
    void m388(View view, Matrix matrix);

    /* renamed from: 麤  reason: contains not printable characters */
    void m389(View view);

    /* renamed from: 齉  reason: contains not printable characters */
    float m390(View view);

    /* renamed from: 龘  reason: contains not printable characters */
    ViewOverlayImpl m391(View view);

    /* renamed from: 龘  reason: contains not printable characters */
    void m392(View view, float f);

    /* renamed from: 龘  reason: contains not printable characters */
    void m393(View view, int i, int i2, int i3, int i4);

    /* renamed from: 龘  reason: contains not printable characters */
    void m394(View view, Matrix matrix);
}
