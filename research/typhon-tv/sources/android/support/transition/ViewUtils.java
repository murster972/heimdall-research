package android.support.transition;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.util.Property;
import android.view.View;
import java.lang.reflect.Field;

class ViewUtils {

    /* renamed from: 连任  reason: contains not printable characters */
    private static boolean f309;

    /* renamed from: 靐  reason: contains not printable characters */
    static final Property<View, Rect> f310 = new Property<View, Rect>(Rect.class, "clipBounds") {
        /* renamed from: 龘  reason: contains not printable characters */
        public Rect get(View view) {
            return ViewCompat.getClipBounds(view);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void set(View view, Rect rect) {
            ViewCompat.setClipBounds(view, rect);
        }
    };

    /* renamed from: 麤  reason: contains not printable characters */
    private static Field f311;

    /* renamed from: 齉  reason: contains not printable characters */
    private static final ViewUtilsImpl f312;

    /* renamed from: 龘  reason: contains not printable characters */
    static final Property<View, Float> f313 = new Property<View, Float>(Float.class, "translationAlpha") {
        /* renamed from: 龘  reason: contains not printable characters */
        public Float get(View view) {
            return Float.valueOf(ViewUtils.m352(view));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void set(View view, Float f) {
            ViewUtils.m355(view, f.floatValue());
        }
    };

    static {
        if (Build.VERSION.SDK_INT >= 22) {
            f312 = new ViewUtilsApi22();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f312 = new ViewUtilsApi21();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f312 = new ViewUtilsApi19();
        } else if (Build.VERSION.SDK_INT >= 18) {
            f312 = new ViewUtilsApi18();
        } else {
            f312 = new ViewUtilsApi14();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    static void m348(View view) {
        f312.m386(view);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static WindowIdImpl m349(View view) {
        return f312.m387(view);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static void m350(View view, Matrix matrix) {
        f312.m388(view, matrix);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static void m351(View view) {
        f312.m389(view);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static float m352(View view) {
        return f312.m390(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ViewOverlayImpl m353(View view) {
        return f312.m391(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m354() {
        if (!f309) {
            try {
                f311 = View.class.getDeclaredField("mViewFlags");
                f311.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.i("ViewUtils", "fetchViewFlagsField: ");
            }
            f309 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m355(View view, float f) {
        f312.m392(view, f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m356(View view, int i) {
        m354();
        if (f311 != null) {
            try {
                f311.setInt(view, (f311.getInt(view) & -13) | i);
            } catch (IllegalAccessException e) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m357(View view, int i, int i2, int i3, int i4) {
        f312.m393(view, i, i2, i3, i4);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m358(View view, Matrix matrix) {
        f312.m394(view, matrix);
    }
}
