package android.support.transition;

import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class TransitionManager {

    /* renamed from: 靐  reason: contains not printable characters */
    private static ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>> f271 = new ThreadLocal<>();
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static ArrayList<ViewGroup> f272 = new ArrayList<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static Transition f273 = new AutoTransition();

    private static class MultiListener implements View.OnAttachStateChangeListener, ViewTreeObserver.OnPreDrawListener {

        /* renamed from: 靐  reason: contains not printable characters */
        ViewGroup f274;

        /* renamed from: 龘  reason: contains not printable characters */
        Transition f275;

        MultiListener(Transition transition, ViewGroup viewGroup) {
            this.f275 = transition;
            this.f274 = viewGroup;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m281() {
            this.f274.getViewTreeObserver().removeOnPreDrawListener(this);
            this.f274.removeOnAttachStateChangeListener(this);
        }

        public boolean onPreDraw() {
            m281();
            if (TransitionManager.f272.remove(this.f274)) {
                final ArrayMap<ViewGroup, ArrayList<Transition>> r3 = TransitionManager.m279();
                ArrayList arrayList = r3.get(this.f274);
                ArrayList arrayList2 = null;
                if (arrayList == null) {
                    arrayList = new ArrayList();
                    r3.put(this.f274, arrayList);
                } else if (arrayList.size() > 0) {
                    arrayList2 = new ArrayList(arrayList);
                }
                arrayList.add(this.f275);
                this.f275.addListener(new TransitionListenerAdapter() {
                    /* renamed from: 靐  reason: contains not printable characters */
                    public void m282(Transition transition) {
                        ((ArrayList) r3.get(MultiListener.this.f274)).remove(transition);
                    }
                });
                this.f275.captureValues(this.f274, false);
                if (arrayList2 != null) {
                    Iterator it2 = arrayList2.iterator();
                    while (it2.hasNext()) {
                        ((Transition) it2.next()).resume(this.f274);
                    }
                }
                this.f275.playTransition(this.f274);
            }
            return true;
        }

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            m281();
            TransitionManager.f272.remove(this.f274);
            ArrayList arrayList = TransitionManager.m279().get(this.f274);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    ((Transition) it2.next()).resume(this.f274);
                }
            }
            this.f275.clearValues(true);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m277(ViewGroup viewGroup, Transition transition) {
        if (transition != null && viewGroup != null) {
            MultiListener multiListener = new MultiListener(transition, viewGroup);
            viewGroup.addOnAttachStateChangeListener(multiListener);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(multiListener);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static void m278(ViewGroup viewGroup, Transition transition) {
        ArrayList arrayList = m279().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                ((Transition) it2.next()).pause(viewGroup);
            }
        }
        if (transition != null) {
            transition.captureValues(viewGroup, true);
        }
        Scene r0 = Scene.m259(viewGroup);
        if (r0 != null) {
            r0.m261();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ArrayMap<ViewGroup, ArrayList<Transition>> m279() {
        WeakReference weakReference = f271.get();
        if (weakReference == null || weakReference.get() == null) {
            weakReference = new WeakReference(new ArrayMap());
            f271.set(weakReference);
        }
        return (ArrayMap) weakReference.get();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m280(ViewGroup viewGroup, Transition transition) {
        if (!f272.contains(viewGroup) && ViewCompat.isLaidOut(viewGroup)) {
            f272.add(viewGroup);
            if (transition == null) {
                transition = f273;
            }
            Transition clone = transition.clone();
            m278(viewGroup, clone);
            Scene.m260(viewGroup, (Scene) null);
            m277(viewGroup, clone);
        }
    }
}
