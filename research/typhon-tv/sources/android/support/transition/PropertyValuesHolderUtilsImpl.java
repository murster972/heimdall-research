package android.support.transition;

import android.animation.PropertyValuesHolder;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.Property;

interface PropertyValuesHolderUtilsImpl {
    /* renamed from: 龘  reason: contains not printable characters */
    PropertyValuesHolder m257(Property<?, PointF> property, Path path);
}
