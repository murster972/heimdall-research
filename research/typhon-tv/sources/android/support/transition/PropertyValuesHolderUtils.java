package android.support.transition;

import android.animation.PropertyValuesHolder;
import android.graphics.Path;
import android.graphics.PointF;
import android.os.Build;
import android.util.Property;

class PropertyValuesHolderUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final PropertyValuesHolderUtilsImpl f248;

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f248 = new PropertyValuesHolderUtilsApi21();
        } else {
            f248 = new PropertyValuesHolderUtilsApi14();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static PropertyValuesHolder m254(Property<?, PointF> property, Path path) {
        return f248.m257(property, path);
    }
}
