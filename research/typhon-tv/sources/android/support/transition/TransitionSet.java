package android.support.transition;

import android.animation.TimeInterpolator;
import android.support.annotation.RestrictTo;
import android.support.transition.Transition;
import android.util.AndroidRuntimeException;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.commons.lang3.StringUtils;

public class TransitionSet extends Transition {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f278 = true;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f279 = false;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public int f280;

    /* renamed from: 龘  reason: contains not printable characters */
    private ArrayList<Transition> f281 = new ArrayList<>();

    static class TransitionSetListener extends TransitionListenerAdapter {

        /* renamed from: 龘  reason: contains not printable characters */
        TransitionSet f284;

        TransitionSetListener(TransitionSet transitionSet) {
            this.f284 = transitionSet;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public void m310(Transition transition) {
            if (!this.f284.f279) {
                this.f284.start();
                boolean unused = this.f284.f279 = true;
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m311(Transition transition) {
            TransitionSet.m286(this.f284);
            if (this.f284.f280 == 0) {
                boolean unused = this.f284.f279 = false;
                this.f284.end();
            }
            transition.removeListener(this);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static /* synthetic */ int m286(TransitionSet transitionSet) {
        int i = transitionSet.f280 - 1;
        transitionSet.f280 = i;
        return i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m287() {
        TransitionSetListener transitionSetListener = new TransitionSetListener(this);
        Iterator<Transition> it2 = this.f281.iterator();
        while (it2.hasNext()) {
            it2.next().addListener(transitionSetListener);
        }
        this.f280 = this.f281.size();
    }

    /* access modifiers changed from: protected */
    @RestrictTo
    public void cancel() {
        super.cancel();
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            this.f281.get(i).cancel();
        }
    }

    public void captureEndValues(TransitionValues transitionValues) {
        if (isValidTarget(transitionValues.f285)) {
            Iterator<Transition> it2 = this.f281.iterator();
            while (it2.hasNext()) {
                Transition next = it2.next();
                if (next.isValidTarget(transitionValues.f285)) {
                    next.captureEndValues(transitionValues);
                    transitionValues.f286.add(next);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void capturePropagationValues(TransitionValues transitionValues) {
        super.capturePropagationValues(transitionValues);
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            this.f281.get(i).capturePropagationValues(transitionValues);
        }
    }

    public void captureStartValues(TransitionValues transitionValues) {
        if (isValidTarget(transitionValues.f285)) {
            Iterator<Transition> it2 = this.f281.iterator();
            while (it2.hasNext()) {
                Transition next = it2.next();
                if (next.isValidTarget(transitionValues.f285)) {
                    next.captureStartValues(transitionValues);
                    transitionValues.f286.add(next);
                }
            }
        }
    }

    public Transition clone() {
        TransitionSet transitionSet = (TransitionSet) super.clone();
        transitionSet.f281 = new ArrayList<>();
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            transitionSet.m304(this.f281.get(i).clone());
        }
        return transitionSet;
    }

    /* access modifiers changed from: protected */
    @RestrictTo
    public void createAnimators(ViewGroup viewGroup, TransitionValuesMaps transitionValuesMaps, TransitionValuesMaps transitionValuesMaps2, ArrayList<TransitionValues> arrayList, ArrayList<TransitionValues> arrayList2) {
        long startDelay = getStartDelay();
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            Transition transition = this.f281.get(i);
            if (startDelay > 0 && (this.f278 || i == 0)) {
                long startDelay2 = transition.getStartDelay();
                if (startDelay2 > 0) {
                    transition.setStartDelay(startDelay + startDelay2);
                } else {
                    transition.setStartDelay(startDelay);
                }
            }
            transition.createAnimators(viewGroup, transitionValuesMaps, transitionValuesMaps2, arrayList, arrayList2);
        }
    }

    public Transition excludeTarget(int i, boolean z) {
        for (int i2 = 0; i2 < this.f281.size(); i2++) {
            this.f281.get(i2).excludeTarget(i, z);
        }
        return super.excludeTarget(i, z);
    }

    public Transition excludeTarget(View view, boolean z) {
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).excludeTarget(view, z);
        }
        return super.excludeTarget(view, z);
    }

    public Transition excludeTarget(Class cls, boolean z) {
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).excludeTarget(cls, z);
        }
        return super.excludeTarget(cls, z);
    }

    public Transition excludeTarget(String str, boolean z) {
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).excludeTarget(str, z);
        }
        return super.excludeTarget(str, z);
    }

    /* access modifiers changed from: package-private */
    @RestrictTo
    public void forceToEnd(ViewGroup viewGroup) {
        super.forceToEnd(viewGroup);
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            this.f281.get(i).forceToEnd(viewGroup);
        }
    }

    @RestrictTo
    public void pause(View view) {
        super.pause(view);
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            this.f281.get(i).pause(view);
        }
    }

    @RestrictTo
    public void resume(View view) {
        super.resume(view);
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            this.f281.get(i).resume(view);
        }
    }

    /* access modifiers changed from: protected */
    @RestrictTo
    public void runAnimators() {
        if (this.f281.isEmpty()) {
            start();
            end();
            return;
        }
        m287();
        if (!this.f278) {
            for (int i = 1; i < this.f281.size(); i++) {
                final Transition transition = this.f281.get(i);
                this.f281.get(i - 1).addListener(new TransitionListenerAdapter() {
                    /* renamed from: 靐  reason: contains not printable characters */
                    public void m309(Transition transition) {
                        transition.runAnimators();
                        transition.removeListener(this);
                    }
                });
            }
            Transition transition2 = this.f281.get(0);
            if (transition2 != null) {
                transition2.runAnimators();
                return;
            }
            return;
        }
        Iterator<Transition> it2 = this.f281.iterator();
        while (it2.hasNext()) {
            it2.next().runAnimators();
        }
    }

    /* access modifiers changed from: package-private */
    public void setCanRemoveViews(boolean z) {
        super.setCanRemoveViews(z);
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            this.f281.get(i).setCanRemoveViews(z);
        }
    }

    public void setEpicenterCallback(Transition.EpicenterCallback epicenterCallback) {
        super.setEpicenterCallback(epicenterCallback);
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            this.f281.get(i).setEpicenterCallback(epicenterCallback);
        }
    }

    public void setPathMotion(PathMotion pathMotion) {
        super.setPathMotion(pathMotion);
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).setPathMotion(pathMotion);
        }
    }

    /* access modifiers changed from: package-private */
    public String toString(String str) {
        String transition = super.toString(str);
        for (int i = 0; i < this.f281.size(); i++) {
            transition = transition + StringUtils.LF + this.f281.get(i).toString(str + "  ");
        }
        return transition;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Transition m291(int i) {
        if (i < 0 || i >= this.f281.size()) {
            return null;
        }
        return this.f281.get(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public TransitionSet setStartDelay(long j) {
        return (TransitionSet) super.setStartDelay(j);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public TransitionSet removeListener(Transition.TransitionListener transitionListener) {
        return (TransitionSet) super.removeListener(transitionListener);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public TransitionSet removeTarget(View view) {
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).removeTarget(view);
        }
        return (TransitionSet) super.removeTarget(view);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public TransitionSet removeTarget(Class cls) {
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).removeTarget(cls);
        }
        return (TransitionSet) super.removeTarget(cls);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public TransitionSet removeTarget(String str) {
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).removeTarget(str);
        }
        return (TransitionSet) super.removeTarget(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public TransitionSet removeTarget(int i) {
        for (int i2 = 0; i2 < this.f281.size(); i2++) {
            this.f281.get(i2).removeTarget(i);
        }
        return (TransitionSet) super.removeTarget(i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public TransitionSet addTarget(int i) {
        for (int i2 = 0; i2 < this.f281.size(); i2++) {
            this.f281.get(i2).addTarget(i);
        }
        return (TransitionSet) super.addTarget(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m299() {
        return this.f281.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TransitionSet m300(int i) {
        switch (i) {
            case 0:
                this.f278 = true;
                break;
            case 1:
                this.f278 = false;
                break;
            default:
                throw new AndroidRuntimeException("Invalid parameter for TransitionSet ordering: " + i);
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TransitionSet setDuration(long j) {
        super.setDuration(j);
        if (this.mDuration >= 0) {
            int size = this.f281.size();
            for (int i = 0; i < size; i++) {
                this.f281.get(i).setDuration(j);
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TransitionSet setInterpolator(TimeInterpolator timeInterpolator) {
        return (TransitionSet) super.setInterpolator(timeInterpolator);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TransitionSet addListener(Transition.TransitionListener transitionListener) {
        return (TransitionSet) super.addListener(transitionListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TransitionSet m304(Transition transition) {
        this.f281.add(transition);
        transition.mParent = this;
        if (this.mDuration >= 0) {
            transition.setDuration(this.mDuration);
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TransitionSet addTarget(View view) {
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).addTarget(view);
        }
        return (TransitionSet) super.addTarget(view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public TransitionSet setSceneRoot(ViewGroup viewGroup) {
        super.setSceneRoot(viewGroup);
        int size = this.f281.size();
        for (int i = 0; i < size; i++) {
            this.f281.get(i).setSceneRoot(viewGroup);
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TransitionSet addTarget(Class cls) {
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).addTarget(cls);
        }
        return (TransitionSet) super.addTarget(cls);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TransitionSet addTarget(String str) {
        for (int i = 0; i < this.f281.size(); i++) {
            this.f281.get(i).addTarget(str);
        }
        return (TransitionSet) super.addTarget(str);
    }
}
