package android.support.transition;

import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public class TransitionValues {

    /* renamed from: 靐  reason: contains not printable characters */
    public View f285;

    /* renamed from: 齉  reason: contains not printable characters */
    final ArrayList<Transition> f286 = new ArrayList<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<String, Object> f287 = new HashMap();

    public boolean equals(Object obj) {
        return (obj instanceof TransitionValues) && this.f285 == ((TransitionValues) obj).f285 && this.f287.equals(((TransitionValues) obj).f287);
    }

    public int hashCode() {
        return (this.f285.hashCode() * 31) + this.f287.hashCode();
    }

    public String toString() {
        String str = (("TransitionValues@" + Integer.toHexString(hashCode()) + ":\n") + "    view = " + this.f285 + StringUtils.LF) + "    values:";
        for (String next : this.f287.keySet()) {
            str = str + "    " + next + ": " + this.f287.get(next) + StringUtils.LF;
        }
        return str;
    }
}
