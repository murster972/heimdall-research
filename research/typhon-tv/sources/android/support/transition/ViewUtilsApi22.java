package android.support.transition;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ViewUtilsApi22 extends ViewUtilsApi21 {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f322;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Method f323;

    ViewUtilsApi22() {
    }

    @SuppressLint({"PrivateApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    private void m384() {
        if (!f322) {
            Class<View> cls = View.class;
            try {
                f323 = cls.getDeclaredMethod("setLeftTopRightBottom", new Class[]{Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE});
                f323.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi22", "Failed to retrieve setLeftTopRightBottom method", e);
            }
            f322 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m385(View view, int i, int i2, int i3, int i4) {
        m384();
        if (f323 != null) {
            try {
                f323.invoke(view, new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4)});
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }
}
