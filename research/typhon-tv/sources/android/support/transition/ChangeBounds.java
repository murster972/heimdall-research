package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import java.util.Map;

public class ChangeBounds extends Transition {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Property<View, PointF> f186 = new Property<View, PointF>(PointF.class, "topLeft") {
        /* renamed from: 龘  reason: contains not printable characters */
        public PointF get(View view) {
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void set(View view, PointF pointF) {
            ViewUtils.m357(view, Math.round(pointF.x), Math.round(pointF.y), view.getRight(), view.getBottom());
        }
    };

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final Property<View, PointF> f187 = new Property<View, PointF>(PointF.class, "position") {
        /* renamed from: 龘  reason: contains not printable characters */
        public PointF get(View view) {
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void set(View view, PointF pointF) {
            int round = Math.round(pointF.x);
            int round2 = Math.round(pointF.y);
            ViewUtils.m357(view, round, round2, round + view.getWidth(), round2 + view.getHeight());
        }
    };

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static RectEvaluator f188 = new RectEvaluator();

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Property<View, PointF> f189 = new Property<View, PointF>(PointF.class, "bottomRight") {
        /* renamed from: 龘  reason: contains not printable characters */
        public PointF get(View view) {
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void set(View view, PointF pointF) {
            ViewUtils.m357(view, view.getLeft(), view.getTop(), Math.round(pointF.x), Math.round(pointF.y));
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Property<Drawable, PointF> f190 = new Property<Drawable, PointF>(PointF.class, "boundsOrigin") {

        /* renamed from: 龘  reason: contains not printable characters */
        private Rect f197 = new Rect();

        /* renamed from: 龘  reason: contains not printable characters */
        public PointF get(Drawable drawable) {
            drawable.copyBounds(this.f197);
            return new PointF((float) this.f197.left, (float) this.f197.top);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void set(Drawable drawable, PointF pointF) {
            drawable.copyBounds(this.f197);
            this.f197.offsetTo(Math.round(pointF.x), Math.round(pointF.y));
            drawable.setBounds(this.f197);
        }
    };

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Property<ViewBounds, PointF> f191 = new Property<ViewBounds, PointF>(PointF.class, "bottomRight") {
        /* renamed from: 龘  reason: contains not printable characters */
        public PointF get(ViewBounds viewBounds) {
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void set(ViewBounds viewBounds, PointF pointF) {
            viewBounds.m227(pointF);
        }
    };

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Property<ViewBounds, PointF> f192 = new Property<ViewBounds, PointF>(PointF.class, "topLeft") {
        /* renamed from: 龘  reason: contains not printable characters */
        public PointF get(ViewBounds viewBounds) {
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void set(ViewBounds viewBounds, PointF pointF) {
            viewBounds.m228(pointF);
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f193 = {"android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY"};

    /* renamed from: ʽ  reason: contains not printable characters */
    private int[] f194 = new int[2];

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f195 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f196 = false;

    private static class ViewBounds {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f216;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f217;

        /* renamed from: 连任  reason: contains not printable characters */
        private View f218;

        /* renamed from: 靐  reason: contains not printable characters */
        private int f219;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f220;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f221;

        /* renamed from: 龘  reason: contains not printable characters */
        private int f222;

        ViewBounds(View view) {
            this.f218 = view;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m226() {
            ViewUtils.m357(this.f218, this.f222, this.f219, this.f221, this.f220);
            this.f216 = 0;
            this.f217 = 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m227(PointF pointF) {
            this.f221 = Math.round(pointF.x);
            this.f220 = Math.round(pointF.y);
            this.f217++;
            if (this.f216 == this.f217) {
                m226();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m228(PointF pointF) {
            this.f222 = Math.round(pointF.x);
            this.f219 = Math.round(pointF.y);
            this.f216++;
            if (this.f216 == this.f217) {
                m226();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m208(TransitionValues transitionValues) {
        View view = transitionValues.f285;
        if (ViewCompat.isLaidOut(view) || view.getWidth() != 0 || view.getHeight() != 0) {
            transitionValues.f287.put("android:changeBounds:bounds", new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()));
            transitionValues.f287.put("android:changeBounds:parent", transitionValues.f285.getParent());
            if (this.f196) {
                transitionValues.f285.getLocationInWindow(this.f194);
                transitionValues.f287.put("android:changeBounds:windowX", Integer.valueOf(this.f194[0]));
                transitionValues.f287.put("android:changeBounds:windowY", Integer.valueOf(this.f194[1]));
            }
            if (this.f195) {
                transitionValues.f287.put("android:changeBounds:clip", ViewCompat.getClipBounds(view));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m209(View view, View view2) {
        if (!this.f196) {
            return true;
        }
        TransitionValues matchedTransitionValues = getMatchedTransitionValues(view, true);
        return matchedTransitionValues == null ? view == view2 : view2 == matchedTransitionValues.f285;
    }

    public void captureEndValues(TransitionValues transitionValues) {
        m208(transitionValues);
    }

    public void captureStartValues(TransitionValues transitionValues) {
        m208(transitionValues);
    }

    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        Animator r18;
        if (transitionValues == null || transitionValues2 == null) {
            return null;
        }
        Map<String, Object> map = transitionValues.f287;
        Map<String, Object> map2 = transitionValues2.f287;
        ViewGroup viewGroup2 = (ViewGroup) map.get("android:changeBounds:parent");
        ViewGroup viewGroup3 = (ViewGroup) map2.get("android:changeBounds:parent");
        if (viewGroup2 == null || viewGroup3 == null) {
            return null;
        }
        final View view = transitionValues2.f285;
        if (m209(viewGroup2, viewGroup3)) {
            Rect rect = (Rect) transitionValues.f287.get("android:changeBounds:bounds");
            Rect rect2 = (Rect) transitionValues2.f287.get("android:changeBounds:bounds");
            int i = rect.left;
            final int i2 = rect2.left;
            int i3 = rect.top;
            final int i4 = rect2.top;
            int i5 = rect.right;
            final int i6 = rect2.right;
            int i7 = rect.bottom;
            final int i8 = rect2.bottom;
            int i9 = i5 - i;
            int i10 = i7 - i3;
            int i11 = i6 - i2;
            int i12 = i8 - i4;
            Rect rect3 = (Rect) transitionValues.f287.get("android:changeBounds:clip");
            Rect rect4 = (Rect) transitionValues2.f287.get("android:changeBounds:clip");
            int i13 = 0;
            if (!((i9 == 0 || i10 == 0) && (i11 == 0 || i12 == 0))) {
                if (!(i == i2 && i3 == i4)) {
                    i13 = 0 + 1;
                }
                if (!(i5 == i6 && i7 == i8)) {
                    i13++;
                }
            }
            if ((rect3 != null && !rect3.equals(rect4)) || (rect3 == null && rect4 != null)) {
                i13++;
            }
            if (i13 > 0) {
                if (!this.f195) {
                    ViewUtils.m357(view, i, i3, i5, i7);
                    if (i13 == 2) {
                        if (i9 == i11 && i10 == i12) {
                            r18 = ObjectAnimatorUtils.m247(view, f187, getPathMotion().m251((float) i, (float) i3, (float) i2, (float) i4));
                        } else {
                            ViewBounds viewBounds = new ViewBounds(view);
                            ViewBounds viewBounds2 = viewBounds;
                            ObjectAnimator r52 = ObjectAnimatorUtils.m247(viewBounds2, f192, getPathMotion().m251((float) i, (float) i3, (float) i2, (float) i4));
                            ViewBounds viewBounds3 = viewBounds;
                            ObjectAnimator r21 = ObjectAnimatorUtils.m247(viewBounds3, f191, getPathMotion().m251((float) i5, (float) i7, (float) i6, (float) i8));
                            AnimatorSet animatorSet = new AnimatorSet();
                            animatorSet.playTogether(new Animator[]{r52, r21});
                            r18 = animatorSet;
                            final ViewBounds viewBounds4 = viewBounds;
                            animatorSet.addListener(new AnimatorListenerAdapter() {
                                private ViewBounds mViewBounds = viewBounds4;
                            });
                        }
                    } else if (i == i2 && i3 == i4) {
                        r18 = ObjectAnimatorUtils.m247(view, f189, getPathMotion().m251((float) i5, (float) i7, (float) i6, (float) i8));
                    } else {
                        r18 = ObjectAnimatorUtils.m247(view, f186, getPathMotion().m251((float) i, (float) i3, (float) i2, (float) i4));
                    }
                } else {
                    int i14 = i;
                    int i15 = i3;
                    ViewUtils.m357(view, i14, i15, i + Math.max(i9, i11), i3 + Math.max(i10, i12));
                    ObjectAnimator objectAnimator = null;
                    if (!(i == i2 && i3 == i4)) {
                        objectAnimator = ObjectAnimatorUtils.m247(view, f187, getPathMotion().m251((float) i, (float) i3, (float) i2, (float) i4));
                    }
                    final Rect rect5 = rect4;
                    if (rect3 == null) {
                        rect3 = new Rect(0, 0, i9, i10);
                    }
                    if (rect4 == null) {
                        rect4 = new Rect(0, 0, i11, i12);
                    }
                    ObjectAnimator objectAnimator2 = null;
                    if (!rect3.equals(rect4)) {
                        ViewCompat.setClipBounds(view, rect3);
                        objectAnimator2 = ObjectAnimator.ofObject(view, "clipBounds", f188, new Object[]{rect3, rect4});
                        objectAnimator2.addListener(new AnimatorListenerAdapter() {

                            /* renamed from: ʽ  reason: contains not printable characters */
                            private boolean f207;

                            public void onAnimationCancel(Animator animator) {
                                this.f207 = true;
                            }

                            public void onAnimationEnd(Animator animator) {
                                if (!this.f207) {
                                    ViewCompat.setClipBounds(view, rect5);
                                    ViewUtils.m357(view, i2, i4, i6, i8);
                                }
                            }
                        });
                    }
                    r18 = TransitionUtils.m312(objectAnimator, objectAnimator2);
                }
                if (!(view.getParent() instanceof ViewGroup)) {
                    return r18;
                }
                ViewGroup viewGroup4 = (ViewGroup) view.getParent();
                ViewGroupUtils.m325(viewGroup4, true);
                final ViewGroup viewGroup5 = viewGroup4;
                addListener(new TransitionListenerAdapter() {

                    /* renamed from: 龘  reason: contains not printable characters */
                    boolean f215 = false;

                    /* renamed from: 靐  reason: contains not printable characters */
                    public void m222(Transition transition) {
                        if (!this.f215) {
                            ViewGroupUtils.m325(viewGroup5, false);
                        }
                        transition.removeListener(this);
                    }

                    /* renamed from: 麤  reason: contains not printable characters */
                    public void m223(Transition transition) {
                        ViewGroupUtils.m325(viewGroup5, true);
                    }

                    /* renamed from: 齉  reason: contains not printable characters */
                    public void m224(Transition transition) {
                        ViewGroupUtils.m325(viewGroup5, false);
                    }

                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m225(Transition transition) {
                        ViewGroupUtils.m325(viewGroup5, false);
                        this.f215 = true;
                    }
                });
                return r18;
            }
        } else {
            int intValue = ((Integer) transitionValues.f287.get("android:changeBounds:windowX")).intValue();
            int intValue2 = ((Integer) transitionValues.f287.get("android:changeBounds:windowY")).intValue();
            int intValue3 = ((Integer) transitionValues2.f287.get("android:changeBounds:windowX")).intValue();
            int intValue4 = ((Integer) transitionValues2.f287.get("android:changeBounds:windowY")).intValue();
            if (!(intValue == intValue3 && intValue2 == intValue4)) {
                viewGroup.getLocationInWindow(this.f194);
                Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                view.draw(new Canvas(createBitmap));
                final BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
                final float r17 = ViewUtils.m352(view);
                ViewUtils.m355(view, 0.0f);
                ViewUtils.m353(viewGroup).m347(bitmapDrawable);
                ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(bitmapDrawable, new PropertyValuesHolder[]{PropertyValuesHolderUtils.m254(f190, getPathMotion().m251((float) (intValue - this.f194[0]), (float) (intValue2 - this.f194[1]), (float) (intValue3 - this.f194[0]), (float) (intValue4 - this.f194[1])))});
                final ViewGroup viewGroup6 = viewGroup;
                final View view2 = view;
                ofPropertyValuesHolder.addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animator) {
                        ViewUtils.m353(viewGroup6).m346(bitmapDrawable);
                        ViewUtils.m355(view2, r17);
                    }
                });
                return ofPropertyValuesHolder;
            }
        }
        return null;
    }

    public String[] getTransitionProperties() {
        return f193;
    }
}
