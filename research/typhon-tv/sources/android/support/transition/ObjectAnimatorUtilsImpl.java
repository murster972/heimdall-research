package android.support.transition;

import android.animation.ObjectAnimator;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.Property;

interface ObjectAnimatorUtilsImpl {
    /* renamed from: 龘  reason: contains not printable characters */
    <T> ObjectAnimator m250(T t, Property<T, PointF> property, Path path);
}
