package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.transition.AnimatorUtilsApi14;
import android.support.transition.Transition;
import android.view.View;
import android.view.ViewGroup;

public abstract class Visibility extends Transition {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f324 = {"android:visibility:visibility", "android:visibility:parent"};

    /* renamed from: 靐  reason: contains not printable characters */
    private int f325 = 3;

    private static class DisappearListener extends AnimatorListenerAdapter implements AnimatorUtilsApi14.AnimatorPauseListenerCompat, Transition.TransitionListener {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f329;

        /* renamed from: 连任  reason: contains not printable characters */
        private final boolean f330;

        /* renamed from: 靐  reason: contains not printable characters */
        private final View f331;

        /* renamed from: 麤  reason: contains not printable characters */
        private final ViewGroup f332;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f333;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f334 = false;

        DisappearListener(View view, int i, boolean z) {
            this.f331 = view;
            this.f333 = i;
            this.f332 = (ViewGroup) view.getParent();
            this.f330 = z;
            m403(true);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m402() {
            if (!this.f334) {
                ViewUtils.m356(this.f331, this.f333);
                if (this.f332 != null) {
                    this.f332.invalidate();
                }
            }
            m403(false);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m403(boolean z) {
            if (this.f330 && this.f329 != z && this.f332 != null) {
                this.f329 = z;
                ViewGroupUtils.m325(this.f332, z);
            }
        }

        public void onAnimationCancel(Animator animator) {
            this.f334 = true;
        }

        public void onAnimationEnd(Animator animator) {
            m402();
        }

        public void onAnimationPause(Animator animator) {
            if (!this.f334) {
                ViewUtils.m356(this.f331, this.f333);
            }
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationResume(Animator animator) {
            if (!this.f334) {
                ViewUtils.m356(this.f331, 0);
            }
        }

        public void onAnimationStart(Animator animator) {
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public void m404(Transition transition) {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m405(Transition transition) {
            m402();
            transition.removeListener(this);
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m406(Transition transition) {
            m403(true);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m407(Transition transition) {
            m403(false);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m408(Transition transition) {
        }
    }

    private static class VisibilityInfo {

        /* renamed from: ʻ  reason: contains not printable characters */
        ViewGroup f335;

        /* renamed from: 连任  reason: contains not printable characters */
        ViewGroup f336;

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f337;

        /* renamed from: 麤  reason: contains not printable characters */
        int f338;

        /* renamed from: 齉  reason: contains not printable characters */
        int f339;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f340;

        private VisibilityInfo() {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private VisibilityInfo m395(TransitionValues transitionValues, TransitionValues transitionValues2) {
        VisibilityInfo visibilityInfo = new VisibilityInfo();
        visibilityInfo.f340 = false;
        visibilityInfo.f337 = false;
        if (transitionValues == null || !transitionValues.f287.containsKey("android:visibility:visibility")) {
            visibilityInfo.f339 = -1;
            visibilityInfo.f336 = null;
        } else {
            visibilityInfo.f339 = ((Integer) transitionValues.f287.get("android:visibility:visibility")).intValue();
            visibilityInfo.f336 = (ViewGroup) transitionValues.f287.get("android:visibility:parent");
        }
        if (transitionValues2 == null || !transitionValues2.f287.containsKey("android:visibility:visibility")) {
            visibilityInfo.f338 = -1;
            visibilityInfo.f335 = null;
        } else {
            visibilityInfo.f338 = ((Integer) transitionValues2.f287.get("android:visibility:visibility")).intValue();
            visibilityInfo.f335 = (ViewGroup) transitionValues2.f287.get("android:visibility:parent");
        }
        if (transitionValues == null || transitionValues2 == null) {
            if (transitionValues == null && visibilityInfo.f338 == 0) {
                visibilityInfo.f337 = true;
                visibilityInfo.f340 = true;
            } else if (transitionValues2 == null && visibilityInfo.f339 == 0) {
                visibilityInfo.f337 = false;
                visibilityInfo.f340 = true;
            }
        } else if (!(visibilityInfo.f339 == visibilityInfo.f338 && visibilityInfo.f336 == visibilityInfo.f335)) {
            if (visibilityInfo.f339 != visibilityInfo.f338) {
                if (visibilityInfo.f339 == 0) {
                    visibilityInfo.f337 = false;
                    visibilityInfo.f340 = true;
                } else if (visibilityInfo.f338 == 0) {
                    visibilityInfo.f337 = true;
                    visibilityInfo.f340 = true;
                }
            } else if (visibilityInfo.f335 == null) {
                visibilityInfo.f337 = false;
                visibilityInfo.f340 = true;
            } else if (visibilityInfo.f336 == null) {
                visibilityInfo.f337 = true;
                visibilityInfo.f340 = true;
            }
        }
        return visibilityInfo;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m396(TransitionValues transitionValues) {
        transitionValues.f287.put("android:visibility:visibility", Integer.valueOf(transitionValues.f285.getVisibility()));
        transitionValues.f287.put("android:visibility:parent", transitionValues.f285.getParent());
        int[] iArr = new int[2];
        transitionValues.f285.getLocationOnScreen(iArr);
        transitionValues.f287.put("android:visibility:screenLocation", iArr);
    }

    public void captureEndValues(TransitionValues transitionValues) {
        m396(transitionValues);
    }

    public void captureStartValues(TransitionValues transitionValues) {
        m396(transitionValues);
    }

    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        VisibilityInfo r6 = m395(transitionValues, transitionValues2);
        if (!r6.f340 || (r6.f336 == null && r6.f335 == null)) {
            return null;
        }
        if (r6.f337) {
            return m399(viewGroup, transitionValues, r6.f339, transitionValues2, r6.f338);
        }
        return m397(viewGroup, transitionValues, r6.f339, transitionValues2, r6.f338);
    }

    public String[] getTransitionProperties() {
        return f324;
    }

    public boolean isTransitionRequired(TransitionValues transitionValues, TransitionValues transitionValues2) {
        if (transitionValues == null && transitionValues2 == null) {
            return false;
        }
        if (transitionValues != null && transitionValues2 != null && transitionValues2.f287.containsKey("android:visibility:visibility") != transitionValues.f287.containsKey("android:visibility:visibility")) {
            return false;
        }
        VisibilityInfo r0 = m395(transitionValues, transitionValues2);
        if (r0.f340) {
            return r0.f339 == 0 || r0.f338 == 0;
        }
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Animator m397(ViewGroup viewGroup, TransitionValues transitionValues, int i, TransitionValues transitionValues2, int i2) {
        int id;
        if ((this.f325 & 2) != 2) {
            return null;
        }
        View view = transitionValues != null ? transitionValues.f285 : null;
        View view2 = transitionValues2 != null ? transitionValues2.f285 : null;
        View view3 = null;
        View view4 = null;
        if (view2 == null || view2.getParent() == null) {
            if (view2 != null) {
                view3 = view2;
            } else if (view != null) {
                if (view.getParent() == null) {
                    view3 = view;
                } else if (view.getParent() instanceof View) {
                    View view5 = (View) view.getParent();
                    if (!m395(getTransitionValues(view5, true), getMatchedTransitionValues(view5, true)).f340) {
                        view3 = TransitionUtils.m314(viewGroup, view, view5);
                    } else if (view5.getParent() == null && (id = view5.getId()) != -1 && viewGroup.findViewById(id) != null && this.mCanRemoveViews) {
                        view3 = view;
                    }
                }
            }
        } else if (i2 == 4) {
            view4 = view2;
        } else if (view == view2) {
            view4 = view2;
        } else {
            view3 = view;
        }
        int i3 = i2;
        if (view3 != null && transitionValues != null) {
            int[] iArr = (int[]) transitionValues.f287.get("android:visibility:screenLocation");
            int i4 = iArr[0];
            int i5 = iArr[1];
            int[] iArr2 = new int[2];
            viewGroup.getLocationOnScreen(iArr2);
            view3.offsetLeftAndRight((i4 - iArr2[0]) - view3.getLeft());
            view3.offsetTopAndBottom((i5 - iArr2[1]) - view3.getTop());
            final ViewGroupOverlayImpl r14 = ViewGroupUtils.m324(viewGroup);
            r14.m323(view3);
            Animator r5 = m398(viewGroup, view3, transitionValues, transitionValues2);
            if (r5 == null) {
                r14.m322(view3);
                return r5;
            }
            final View view6 = view3;
            r5.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    r14.m322(view6);
                }
            });
            return r5;
        } else if (view4 == null) {
            return null;
        } else {
            int visibility = view4.getVisibility();
            ViewUtils.m356(view4, 0);
            Animator r52 = m398(viewGroup, view4, transitionValues, transitionValues2);
            if (r52 != null) {
                DisappearListener disappearListener = new DisappearListener(view4, i3, true);
                r52.addListener(disappearListener);
                AnimatorUtils.m197(r52, disappearListener);
                addListener(disappearListener);
                return r52;
            }
            ViewUtils.m356(view4, visibility);
            return r52;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Animator m398(ViewGroup viewGroup, View view, TransitionValues transitionValues, TransitionValues transitionValues2) {
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Animator m399(ViewGroup viewGroup, TransitionValues transitionValues, int i, TransitionValues transitionValues2, int i2) {
        if ((this.f325 & 1) != 1 || transitionValues2 == null) {
            return null;
        }
        if (transitionValues == null) {
            View view = (View) transitionValues2.f285.getParent();
            if (m395(getMatchedTransitionValues(view, false), getTransitionValues(view, false)).f340) {
                return null;
            }
        }
        return m400(viewGroup, transitionValues2.f285, transitionValues, transitionValues2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Animator m400(ViewGroup viewGroup, View view, TransitionValues transitionValues, TransitionValues transitionValues2) {
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m401(int i) {
        if ((i & -4) != 0) {
            throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
        }
        this.f325 = i;
    }
}
