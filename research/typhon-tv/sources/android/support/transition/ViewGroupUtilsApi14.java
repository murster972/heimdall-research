package android.support.transition;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.util.Log;
import android.view.ViewGroup;
import io.presage.ads.NewAd;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ViewGroupUtilsApi14 implements ViewGroupUtilsImpl {

    /* renamed from: 连任  reason: contains not printable characters */
    private static boolean f294;

    /* renamed from: 靐  reason: contains not printable characters */
    private static Field f295;

    /* renamed from: 麤  reason: contains not printable characters */
    private static Method f296;

    /* renamed from: 齉  reason: contains not printable characters */
    private static boolean f297;

    /* renamed from: 龘  reason: contains not printable characters */
    private static LayoutTransition f298;

    ViewGroupUtilsApi14() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m326(LayoutTransition layoutTransition) {
        if (!f294) {
            try {
                f296 = LayoutTransition.class.getDeclaredMethod(NewAd.EVENT_CANCEL, new Class[0]);
                f296.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewGroupUtilsApi14", "Failed to access cancel method by reflection");
            }
            f294 = true;
        }
        if (f296 != null) {
            try {
                f296.invoke(layoutTransition, new Object[0]);
            } catch (IllegalAccessException e2) {
                Log.i("ViewGroupUtilsApi14", "Failed to access cancel method by reflection");
            } catch (InvocationTargetException e3) {
                Log.i("ViewGroupUtilsApi14", "Failed to invoke cancel method by reflection");
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewGroupOverlayImpl m327(ViewGroup viewGroup) {
        return ViewGroupOverlayApi14.m315(viewGroup);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m328(ViewGroup viewGroup, boolean z) {
        if (f298 == null) {
            f298 = new LayoutTransition() {
                public boolean isChangingLayout() {
                    return true;
                }
            };
            f298.setAnimator(2, (Animator) null);
            f298.setAnimator(0, (Animator) null);
            f298.setAnimator(1, (Animator) null);
            f298.setAnimator(3, (Animator) null);
            f298.setAnimator(4, (Animator) null);
        }
        if (z) {
            LayoutTransition layoutTransition = viewGroup.getLayoutTransition();
            if (layoutTransition != null) {
                if (layoutTransition.isRunning()) {
                    m326(layoutTransition);
                }
                if (layoutTransition != f298) {
                    viewGroup.setTag(R.id.transition_layout_save, layoutTransition);
                }
            }
            viewGroup.setLayoutTransition(f298);
            return;
        }
        viewGroup.setLayoutTransition((LayoutTransition) null);
        if (!f297) {
            try {
                f295 = ViewGroup.class.getDeclaredField("mLayoutSuppressed");
                f295.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.i("ViewGroupUtilsApi14", "Failed to access mLayoutSuppressed field by reflection");
            }
            f297 = true;
        }
        boolean z2 = false;
        if (f295 != null) {
            try {
                z2 = f295.getBoolean(viewGroup);
                if (z2) {
                    f295.setBoolean(viewGroup, false);
                }
            } catch (IllegalAccessException e2) {
                Log.i("ViewGroupUtilsApi14", "Failed to get mLayoutSuppressed field by reflection");
            }
        }
        if (z2) {
            viewGroup.requestLayout();
        }
        LayoutTransition layoutTransition2 = (LayoutTransition) viewGroup.getTag(R.id.transition_layout_save);
        if (layoutTransition2 != null) {
            viewGroup.setTag(R.id.transition_layout_save, (Object) null);
            viewGroup.setLayoutTransition(layoutTransition2);
        }
    }
}
