package android.support.transition;

import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ViewUtilsApi19 extends ViewUtilsApi18 {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f314;

    /* renamed from: 麤  reason: contains not printable characters */
    private static boolean f315;

    /* renamed from: 齉  reason: contains not printable characters */
    private static Method f316;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Method f317;

    ViewUtilsApi19() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m374() {
        if (!f315) {
            try {
                f316 = View.class.getDeclaredMethod("getTransitionAlpha", new Class[0]);
                f316.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi19", "Failed to retrieve getTransitionAlpha method", e);
            }
            f315 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m375() {
        if (!f314) {
            Class<View> cls = View.class;
            try {
                f317 = cls.getDeclaredMethod("setTransitionAlpha", new Class[]{Float.TYPE});
                f317.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi19", "Failed to retrieve setTransitionAlpha method", e);
            }
            f314 = true;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m376(View view) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m377(View view) {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public float m378(View view) {
        m374();
        if (f316 != null) {
            try {
                return ((Float) f316.invoke(view, new Object[0])).floatValue();
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
        return super.m367(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m379(View view, float f) {
        m375();
        if (f317 != null) {
            try {
                f317.invoke(view, new Object[]{Float.valueOf(f)});
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        } else {
            view.setAlpha(f);
        }
    }
}
