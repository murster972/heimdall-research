package android.support.transition;

import android.animation.ObjectAnimator;
import android.graphics.Path;
import android.graphics.PointF;
import android.os.Build;
import android.util.Property;

class ObjectAnimatorUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ObjectAnimatorUtilsImpl f241;

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f241 = new ObjectAnimatorUtilsApi21();
        } else {
            f241 = new ObjectAnimatorUtilsApi14();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> ObjectAnimator m247(T t, Property<T, PointF> property, Path path) {
        return f241.m250(t, property, path);
    }
}
