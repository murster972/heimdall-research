package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;

class AnimatorUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final AnimatorUtilsImpl f185;

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            f185 = new AnimatorUtilsApi19();
        } else {
            f185 = new AnimatorUtilsApi14();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static void m195(Animator animator) {
        f185.m204(animator);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m196(Animator animator) {
        f185.m205(animator);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m197(Animator animator, AnimatorListenerAdapter animatorListenerAdapter) {
        f185.m206(animator, animatorListenerAdapter);
    }
}
