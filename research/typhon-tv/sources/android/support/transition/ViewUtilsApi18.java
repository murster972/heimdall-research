package android.support.transition;

import android.view.View;

class ViewUtilsApi18 extends ViewUtilsApi14 {
    ViewUtilsApi18() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public WindowIdImpl m372(View view) {
        return new WindowIdApi18(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewOverlayImpl m373(View view) {
        return new ViewOverlayApi18(view);
    }
}
