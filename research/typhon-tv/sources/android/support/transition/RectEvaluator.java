package android.support.transition;

import android.animation.TypeEvaluator;
import android.graphics.Rect;

class RectEvaluator implements TypeEvaluator<Rect> {

    /* renamed from: 龘  reason: contains not printable characters */
    private Rect f249;

    RectEvaluator() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Rect evaluate(float f, Rect rect, Rect rect2) {
        int i = rect.left + ((int) (((float) (rect2.left - rect.left)) * f));
        int i2 = rect.top + ((int) (((float) (rect2.top - rect.top)) * f));
        int i3 = rect.right + ((int) (((float) (rect2.right - rect.right)) * f));
        int i4 = rect.bottom + ((int) (((float) (rect2.bottom - rect.bottom)) * f));
        if (this.f249 == null) {
            return new Rect(i, i2, i3, i4);
        }
        this.f249.set(i, i2, i3, i4);
        return this.f249;
    }
}
