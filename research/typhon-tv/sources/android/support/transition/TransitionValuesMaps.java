package android.support.transition;

import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.util.SparseArray;
import android.view.View;

class TransitionValuesMaps {

    /* renamed from: 靐  reason: contains not printable characters */
    final SparseArray<View> f288 = new SparseArray<>();

    /* renamed from: 麤  reason: contains not printable characters */
    final ArrayMap<String, View> f289 = new ArrayMap<>();

    /* renamed from: 齉  reason: contains not printable characters */
    final LongSparseArray<View> f290 = new LongSparseArray<>();

    /* renamed from: 龘  reason: contains not printable characters */
    final ArrayMap<View, TransitionValues> f291 = new ArrayMap<>();

    TransitionValuesMaps() {
    }
}
