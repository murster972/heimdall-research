package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.graphics.Paint;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;

public class Fade extends Visibility {

    private static class FadeAnimatorListener extends AnimatorListenerAdapter {

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f225 = false;

        /* renamed from: 龘  reason: contains not printable characters */
        private final View f226;

        FadeAnimatorListener(View view) {
            this.f226 = view;
        }

        public void onAnimationEnd(Animator animator) {
            ViewUtils.m355(this.f226, 1.0f);
            if (this.f225) {
                this.f226.setLayerType(0, (Paint) null);
            }
        }

        public void onAnimationStart(Animator animator) {
            if (ViewCompat.hasOverlappingRendering(this.f226) && this.f226.getLayerType() == 0) {
                this.f225 = true;
                this.f226.setLayerType(2, (Paint) null);
            }
        }
    }

    public Fade() {
    }

    public Fade(int i) {
        m401(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0003, code lost:
        r1 = (java.lang.Float) r4.f287.get("android:fade:transitionAlpha");
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static float m229(android.support.transition.TransitionValues r4, float r5) {
        /*
            r0 = r5
            if (r4 == 0) goto L_0x0014
            java.util.Map<java.lang.String, java.lang.Object> r2 = r4.f287
            java.lang.String r3 = "android:fade:transitionAlpha"
            java.lang.Object r1 = r2.get(r3)
            java.lang.Float r1 = (java.lang.Float) r1
            if (r1 == 0) goto L_0x0014
            float r0 = r1.floatValue()
        L_0x0014:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.transition.Fade.m229(android.support.transition.TransitionValues, float):float");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Animator m230(final View view, float f, float f2) {
        if (f == f2) {
            return null;
        }
        ViewUtils.m355(view, f);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, ViewUtils.f313, new float[]{f2});
        ofFloat.addListener(new FadeAnimatorListener(view));
        addListener(new TransitionListenerAdapter() {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m233(Transition transition) {
                ViewUtils.m355(view, 1.0f);
                ViewUtils.m348(view);
                transition.removeListener(this);
            }
        });
        return ofFloat;
    }

    public void captureStartValues(TransitionValues transitionValues) {
        super.captureStartValues(transitionValues);
        transitionValues.f287.put("android:fade:transitionAlpha", Float.valueOf(ViewUtils.m352(transitionValues.f285)));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Animator m231(ViewGroup viewGroup, View view, TransitionValues transitionValues, TransitionValues transitionValues2) {
        ViewUtils.m351(view);
        return m230(view, m229(transitionValues, 1.0f), 0.0f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Animator m232(ViewGroup viewGroup, View view, TransitionValues transitionValues, TransitionValues transitionValues2) {
        float r0 = m229(transitionValues, 0.0f);
        if (r0 == 1.0f) {
            r0 = 0.0f;
        }
        return m230(view, r0, 1.0f);
    }
}
