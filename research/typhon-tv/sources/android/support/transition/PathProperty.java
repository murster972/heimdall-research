package android.support.transition;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.util.Property;

class PathProperty<T> extends Property<T, Float> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f242;

    /* renamed from: 连任  reason: contains not printable characters */
    private final PointF f243 = new PointF();

    /* renamed from: 靐  reason: contains not printable characters */
    private final PathMeasure f244;

    /* renamed from: 麤  reason: contains not printable characters */
    private final float[] f245 = new float[2];

    /* renamed from: 齉  reason: contains not printable characters */
    private final float f246;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Property<T, PointF> f247;

    PathProperty(Property<T, PointF> property, Path path) {
        super(Float.class, property.getName());
        this.f247 = property;
        this.f244 = new PathMeasure(path, false);
        this.f246 = this.f244.getLength();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Float get(T t) {
        return Float.valueOf(this.f242);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void set(T t, Float f) {
        this.f242 = f.floatValue();
        this.f244.getPosTan(this.f246 * f.floatValue(), this.f245, (float[]) null);
        this.f243.x = this.f245[0];
        this.f243.y = this.f245[1];
        this.f247.set(t, this.f243);
    }
}
