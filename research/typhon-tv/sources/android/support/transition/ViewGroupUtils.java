package android.support.transition;

import android.os.Build;
import android.view.ViewGroup;

class ViewGroupUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ViewGroupUtilsImpl f293;

    static {
        if (Build.VERSION.SDK_INT >= 18) {
            f293 = new ViewGroupUtilsApi18();
        } else {
            f293 = new ViewGroupUtilsApi14();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ViewGroupOverlayImpl m324(ViewGroup viewGroup) {
        return f293.m332(viewGroup);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m325(ViewGroup viewGroup, boolean z) {
        f293.m333(viewGroup, z);
    }
}
