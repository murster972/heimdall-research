package android.support.transition;

import android.util.Log;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ViewGroupUtilsApi18 extends ViewGroupUtilsApi14 {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f300;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Method f301;

    ViewGroupUtilsApi18() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m329() {
        if (!f300) {
            Class<ViewGroup> cls = ViewGroup.class;
            try {
                f301 = cls.getDeclaredMethod("suppressLayout", new Class[]{Boolean.TYPE});
                f301.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi18", "Failed to retrieve suppressLayout method", e);
            }
            f300 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewGroupOverlayImpl m330(ViewGroup viewGroup) {
        return new ViewGroupOverlayApi18(viewGroup);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m331(ViewGroup viewGroup, boolean z) {
        m329();
        if (f301 != null) {
            try {
                f301.invoke(viewGroup, new Object[]{Boolean.valueOf(z)});
            } catch (IllegalAccessException e) {
                Log.i("ViewUtilsApi18", "Failed to invoke suppressLayout method", e);
            } catch (InvocationTargetException e2) {
                Log.i("ViewUtilsApi18", "Error invoking suppressLayout method", e2);
            }
        }
    }
}
