package android.support.transition;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.lang.reflect.Method;
import java.util.ArrayList;

class ViewOverlayApi14 implements ViewOverlayImpl {

    /* renamed from: 龘  reason: contains not printable characters */
    protected OverlayViewGroup f302;

    static class OverlayViewGroup extends ViewGroup {

        /* renamed from: 龘  reason: contains not printable characters */
        static Method f303;

        /* renamed from: 连任  reason: contains not printable characters */
        ViewOverlayApi14 f304;

        /* renamed from: 靐  reason: contains not printable characters */
        ViewGroup f305;

        /* renamed from: 麤  reason: contains not printable characters */
        ArrayList<Drawable> f306 = null;

        /* renamed from: 齉  reason: contains not printable characters */
        View f307;

        static {
            Class<ViewGroup> cls = ViewGroup.class;
            try {
                f303 = cls.getDeclaredMethod("invalidateChildInParentFast", new Class[]{Integer.TYPE, Integer.TYPE, Rect.class});
            } catch (NoSuchMethodException e) {
            }
        }

        OverlayViewGroup(Context context, ViewGroup viewGroup, View view, ViewOverlayApi14 viewOverlayApi14) {
            super(context);
            this.f305 = viewGroup;
            this.f307 = view;
            setRight(viewGroup.getWidth());
            setBottom(viewGroup.getHeight());
            viewGroup.addView(this);
            this.f304 = viewOverlayApi14;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m338(int[] iArr) {
            int[] iArr2 = new int[2];
            int[] iArr3 = new int[2];
            this.f305.getLocationOnScreen(iArr2);
            this.f307.getLocationOnScreen(iArr3);
            iArr[0] = iArr3[0] - iArr2[0];
            iArr[1] = iArr3[1] - iArr2[1];
        }

        /* access modifiers changed from: protected */
        public void dispatchDraw(Canvas canvas) {
            int i = 0;
            int[] iArr = new int[2];
            int[] iArr2 = new int[2];
            this.f305.getLocationOnScreen(iArr);
            this.f307.getLocationOnScreen(iArr2);
            canvas.translate((float) (iArr2[0] - iArr[0]), (float) (iArr2[1] - iArr[1]));
            canvas.clipRect(new Rect(0, 0, this.f307.getWidth(), this.f307.getHeight()));
            super.dispatchDraw(canvas);
            if (this.f306 != null) {
                i = this.f306.size();
            }
            for (int i2 = 0; i2 < i; i2++) {
                this.f306.get(i2).draw(canvas);
            }
        }

        public boolean dispatchTouchEvent(MotionEvent motionEvent) {
            return false;
        }

        public ViewParent invalidateChildInParent(int[] iArr, Rect rect) {
            if (this.f305 != null) {
                rect.offset(iArr[0], iArr[1]);
                if (this.f305 instanceof ViewGroup) {
                    iArr[0] = 0;
                    iArr[1] = 0;
                    int[] iArr2 = new int[2];
                    m338(iArr2);
                    rect.offset(iArr2[0], iArr2[1]);
                    return super.invalidateChildInParent(iArr, rect);
                }
                invalidate(rect);
            }
            return null;
        }

        public void invalidateDrawable(Drawable drawable) {
            invalidate(drawable.getBounds());
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        }

        /* access modifiers changed from: protected */
        public boolean verifyDrawable(Drawable drawable) {
            return super.verifyDrawable(drawable) || (this.f306 != null && this.f306.contains(drawable));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m339(Drawable drawable) {
            if (this.f306 != null) {
                this.f306.remove(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback((Drawable.Callback) null);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m340(View view) {
            super.removeView(view);
            if (m343()) {
                this.f305.removeView(this);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m341(Drawable drawable) {
            if (this.f306 == null) {
                this.f306 = new ArrayList<>();
            }
            if (!this.f306.contains(drawable)) {
                this.f306.add(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback(this);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m342(View view) {
            if (view.getParent() instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view.getParent();
                if (!(viewGroup == this.f305 || viewGroup.getParent() == null || !ViewCompat.isAttachedToWindow(viewGroup))) {
                    int[] iArr = new int[2];
                    int[] iArr2 = new int[2];
                    viewGroup.getLocationOnScreen(iArr);
                    this.f305.getLocationOnScreen(iArr2);
                    ViewCompat.offsetLeftAndRight(view, iArr[0] - iArr2[0]);
                    ViewCompat.offsetTopAndBottom(view, iArr[1] - iArr2[1]);
                }
                viewGroup.removeView(view);
                if (view.getParent() != null) {
                    viewGroup.removeView(view);
                }
            }
            super.addView(view, getChildCount() - 1);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m343() {
            return getChildCount() == 0 && (this.f306 == null || this.f306.size() == 0);
        }
    }

    ViewOverlayApi14(Context context, ViewGroup viewGroup, View view) {
        this.f302 = new OverlayViewGroup(context, viewGroup, view, this);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static ViewOverlayApi14 m334(View view) {
        ViewGroup r1 = m335(view);
        if (r1 == null) {
            return null;
        }
        int childCount = r1.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = r1.getChildAt(i);
            if (childAt instanceof OverlayViewGroup) {
                return ((OverlayViewGroup) childAt).f304;
            }
        }
        return new ViewGroupOverlayApi14(r1.getContext(), r1, view);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static ViewGroup m335(View view) {
        View view2 = view;
        while (view2 != null) {
            if (view2.getId() == 16908290 && (view2 instanceof ViewGroup)) {
                return (ViewGroup) view2;
            }
            if (view2.getParent() instanceof ViewGroup) {
                view2 = (ViewGroup) view2.getParent();
            }
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m336(Drawable drawable) {
        this.f302.m339(drawable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m337(Drawable drawable) {
        this.f302.m341(drawable);
    }
}
