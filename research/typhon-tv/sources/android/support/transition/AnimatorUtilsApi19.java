package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

class AnimatorUtilsApi19 implements AnimatorUtilsImpl {
    AnimatorUtilsApi19() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m201(Animator animator) {
        animator.resume();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m202(Animator animator) {
        animator.pause();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m203(Animator animator, AnimatorListenerAdapter animatorListenerAdapter) {
        animator.addPauseListener(animatorListenerAdapter);
    }
}
