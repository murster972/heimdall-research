package android.support.transition;

import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ViewUtilsApi21 extends ViewUtilsApi19 {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f318;

    /* renamed from: 麤  reason: contains not printable characters */
    private static boolean f319;

    /* renamed from: 齉  reason: contains not printable characters */
    private static Method f320;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Method f321;

    ViewUtilsApi21() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m380() {
        if (!f319) {
            try {
                f320 = View.class.getDeclaredMethod("transformMatrixToLocal", new Class[]{Matrix.class});
                f320.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToLocal method", e);
            }
            f319 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m381() {
        if (!f318) {
            try {
                f321 = View.class.getDeclaredMethod("transformMatrixToGlobal", new Class[]{Matrix.class});
                f321.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToGlobal method", e);
            }
            f318 = true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m382(View view, Matrix matrix) {
        m380();
        if (f320 != null) {
            try {
                f320.invoke(view, new Object[]{matrix});
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m383(View view, Matrix matrix) {
        m381();
        if (f321 != null) {
            try {
                f321.invoke(view, new Object[]{matrix});
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }
}
