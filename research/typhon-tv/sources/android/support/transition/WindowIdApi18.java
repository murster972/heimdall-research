package android.support.transition;

import android.view.View;
import android.view.WindowId;

class WindowIdApi18 implements WindowIdImpl {

    /* renamed from: 龘  reason: contains not printable characters */
    private final WindowId f342;

    WindowIdApi18(View view) {
        this.f342 = view.getWindowId();
    }

    public boolean equals(Object obj) {
        return (obj instanceof WindowIdApi18) && ((WindowIdApi18) obj).f342.equals(this.f342);
    }

    public int hashCode() {
        return this.f342.hashCode();
    }
}
