package android.support.transition;

import android.graphics.Rect;
import android.support.annotation.RestrictTo;
import android.support.transition.Transition;
import android.support.v4.app.FragmentTransitionImpl;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

@RestrictTo
public class FragmentTransitionSupport extends FragmentTransitionImpl {
    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m234(Transition transition) {
        return !isNullOrEmpty(transition.getTargetIds()) || !isNullOrEmpty(transition.getTargetNames()) || !isNullOrEmpty(transition.getTargetTypes());
    }

    public void addTarget(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).addTarget(view);
        }
    }

    public void addTargets(Object obj, ArrayList<View> arrayList) {
        Transition transition = (Transition) obj;
        if (transition != null) {
            if (transition instanceof TransitionSet) {
                TransitionSet transitionSet = (TransitionSet) transition;
                int r2 = transitionSet.m299();
                for (int i = 0; i < r2; i++) {
                    addTargets(transitionSet.m291(i), arrayList);
                }
            } else if (!m234(transition) && isNullOrEmpty(transition.getTargets())) {
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    transition.addTarget(arrayList.get(i2));
                }
            }
        }
    }

    public void beginDelayedTransition(ViewGroup viewGroup, Object obj) {
        TransitionManager.m280(viewGroup, (Transition) obj);
    }

    public boolean canHandle(Object obj) {
        return obj instanceof Transition;
    }

    public Object cloneTransition(Object obj) {
        if (obj != null) {
            return ((Transition) obj).clone();
        }
        return null;
    }

    public Object mergeTransitionsInSequence(Object obj, Object obj2, Object obj3) {
        Transition transition = null;
        Transition transition2 = (Transition) obj;
        Transition transition3 = (Transition) obj2;
        Transition transition4 = (Transition) obj3;
        if (transition2 != null && transition3 != null) {
            transition = new TransitionSet().m304(transition2).m304(transition3).m300(1);
        } else if (transition2 != null) {
            transition = transition2;
        } else if (transition3 != null) {
            transition = transition3;
        }
        if (transition4 == null) {
            return transition;
        }
        TransitionSet transitionSet = new TransitionSet();
        if (transition != null) {
            transitionSet.m304(transition);
        }
        transitionSet.m304(transition4);
        return transitionSet;
    }

    public Object mergeTransitionsTogether(Object obj, Object obj2, Object obj3) {
        TransitionSet transitionSet = new TransitionSet();
        if (obj != null) {
            transitionSet.m304((Transition) obj);
        }
        if (obj2 != null) {
            transitionSet.m304((Transition) obj2);
        }
        if (obj3 != null) {
            transitionSet.m304((Transition) obj3);
        }
        return transitionSet;
    }

    public void removeTarget(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).removeTarget(view);
        }
    }

    public void replaceTargets(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        Transition transition = (Transition) obj;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int r2 = transitionSet.m299();
            for (int i = 0; i < r2; i++) {
                replaceTargets(transitionSet.m291(i), arrayList, arrayList2);
            }
        } else if (!m234(transition)) {
            List<View> targets = transition.getTargets();
            if (targets.size() == arrayList.size() && targets.containsAll(arrayList)) {
                int size = arrayList2 == null ? 0 : arrayList2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    transition.addTarget(arrayList2.get(i2));
                }
                for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
                    transition.removeTarget(arrayList.get(size2));
                }
            }
        }
    }

    public void scheduleHideFragmentView(Object obj, final View view, final ArrayList<View> arrayList) {
        ((Transition) obj).addListener(new Transition.TransitionListener() {
            /* renamed from: 连任  reason: contains not printable characters */
            public void m236(Transition transition) {
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m237(Transition transition) {
                transition.removeListener(this);
                view.setVisibility(8);
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    ((View) arrayList.get(i)).setVisibility(0);
                }
            }

            /* renamed from: 麤  reason: contains not printable characters */
            public void m238(Transition transition) {
            }

            /* renamed from: 齉  reason: contains not printable characters */
            public void m239(Transition transition) {
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m240(Transition transition) {
            }
        });
    }

    public void scheduleRemoveTargets(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3) {
        final Object obj5 = obj2;
        final ArrayList<View> arrayList4 = arrayList;
        final Object obj6 = obj3;
        final ArrayList<View> arrayList5 = arrayList2;
        final Object obj7 = obj4;
        final ArrayList<View> arrayList6 = arrayList3;
        ((Transition) obj).addListener(new Transition.TransitionListener() {
            /* renamed from: 连任  reason: contains not printable characters */
            public void m241(Transition transition) {
                if (obj5 != null) {
                    FragmentTransitionSupport.this.replaceTargets(obj5, arrayList4, (ArrayList<View>) null);
                }
                if (obj6 != null) {
                    FragmentTransitionSupport.this.replaceTargets(obj6, arrayList5, (ArrayList<View>) null);
                }
                if (obj7 != null) {
                    FragmentTransitionSupport.this.replaceTargets(obj7, arrayList6, (ArrayList<View>) null);
                }
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m242(Transition transition) {
            }

            /* renamed from: 麤  reason: contains not printable characters */
            public void m243(Transition transition) {
            }

            /* renamed from: 齉  reason: contains not printable characters */
            public void m244(Transition transition) {
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m245(Transition transition) {
            }
        });
    }

    public void setEpicenter(Object obj, final Rect rect) {
        if (obj != null) {
            ((Transition) obj).setEpicenterCallback(new Transition.EpicenterCallback() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Rect m246(Transition transition) {
                    if (rect == null || rect.isEmpty()) {
                        return null;
                    }
                    return rect;
                }
            });
        }
    }

    public void setEpicenter(Object obj, View view) {
        if (view != null) {
            final Rect rect = new Rect();
            getBoundsOnScreen(view, rect);
            ((Transition) obj).setEpicenterCallback(new Transition.EpicenterCallback() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Rect m235(Transition transition) {
                    return rect;
                }
            });
        }
    }

    public void setSharedElementTargets(Object obj, View view, ArrayList<View> arrayList) {
        TransitionSet transitionSet = (TransitionSet) obj;
        List<View> targets = transitionSet.getTargets();
        targets.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            bfsAddViewChildren(targets, arrayList.get(i));
        }
        targets.add(view);
        arrayList.add(view);
        addTargets(transitionSet, arrayList);
    }

    public void swapSharedElementTargets(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        TransitionSet transitionSet = (TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.getTargets().clear();
            transitionSet.getTargets().addAll(arrayList2);
            replaceTargets(transitionSet, arrayList, arrayList2);
        }
    }

    public Object wrapTransitionInSet(Object obj) {
        if (obj == null) {
            return null;
        }
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.m304((Transition) obj);
        return transitionSet;
    }
}
