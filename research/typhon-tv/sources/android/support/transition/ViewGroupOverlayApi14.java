package android.support.transition;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

class ViewGroupOverlayApi14 extends ViewOverlayApi14 implements ViewGroupOverlayImpl {
    ViewGroupOverlayApi14(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ViewGroupOverlayApi14 m315(ViewGroup viewGroup) {
        return (ViewGroupOverlayApi14) ViewOverlayApi14.m334(viewGroup);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m316(View view) {
        this.f302.m340(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m317(View view) {
        this.f302.m342(view);
    }
}
