package android.support.transition;

import android.os.IBinder;

class WindowIdApi14 implements WindowIdImpl {

    /* renamed from: 龘  reason: contains not printable characters */
    private final IBinder f341;

    WindowIdApi14(IBinder iBinder) {
        this.f341 = iBinder;
    }

    public boolean equals(Object obj) {
        return (obj instanceof WindowIdApi14) && ((WindowIdApi14) obj).f341.equals(this.f341);
    }

    public int hashCode() {
        return this.f341.hashCode();
    }
}
