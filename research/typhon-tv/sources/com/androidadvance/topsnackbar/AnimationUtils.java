package com.androidadvance.topsnackbar;

import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

public class AnimationUtils {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Interpolator f3300 = new FastOutSlowInInterpolator();

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Interpolator f3301 = new DecelerateInterpolator();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Interpolator f3302 = new LinearInterpolator();
}
