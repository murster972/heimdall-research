package com.androidadvance.topsnackbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.androidadvance.topsnackbar.SnackbarManager;

public final class TSnackbar {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Handler f3311 = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ((TSnackbar) message.obj).m3913();
                    return true;
                case 1:
                    ((TSnackbar) message.obj).m3912(message.arg1);
                    return true;
                default:
                    return false;
            }
        }
    });
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public Callback f3312;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final SnackbarManager.Callback f3313 = new SnackbarManager.Callback() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m3918() {
            TSnackbar.f3311.sendMessage(TSnackbar.f3311.obtainMessage(0, TSnackbar.this));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m3919(int i) {
            TSnackbar.f3311.sendMessage(TSnackbar.f3311.obtainMessage(1, i, 0, TSnackbar.this));
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private int f3314;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ViewGroup f3315;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final SnackbarLayout f3316;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f3317;

    final class Behavior extends SwipeDismissBehavior<SnackbarLayout> {
        Behavior() {
        }

        public boolean canSwipeDismissView(View view) {
            return view instanceof SnackbarLayout;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, SnackbarLayout snackbarLayout, MotionEvent motionEvent) {
            if (coordinatorLayout.isPointInChildBounds(snackbarLayout, (int) motionEvent.getX(), (int) motionEvent.getY())) {
                switch (motionEvent.getActionMasked()) {
                    case 0:
                        SnackbarManager.m3880().m3887(TSnackbar.this.f3313);
                        break;
                    case 1:
                    case 3:
                        SnackbarManager.m3880().m3886(TSnackbar.this.f3313);
                        break;
                }
            }
            return super.onInterceptTouchEvent(coordinatorLayout, snackbarLayout, motionEvent);
        }
    }

    public static abstract class Callback {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m3924(TSnackbar tSnackbar) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m3925(TSnackbar tSnackbar, int i) {
        }
    }

    public static class SnackbarLayout extends LinearLayout {

        /* renamed from: ʻ  reason: contains not printable characters */
        private OnAttachStateChangeListener f3330;

        /* renamed from: 连任  reason: contains not printable characters */
        private OnLayoutChangeListener f3331;

        /* renamed from: 靐  reason: contains not printable characters */
        private Button f3332;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f3333;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f3334;

        /* renamed from: 龘  reason: contains not printable characters */
        private TextView f3335;

        interface OnAttachStateChangeListener {
            /* renamed from: 靐  reason: contains not printable characters */
            void m3930(View view);

            /* renamed from: 龘  reason: contains not printable characters */
            void m3931(View view);
        }

        interface OnLayoutChangeListener {
            /* renamed from: 龘  reason: contains not printable characters */
            void m3932(View view, int i, int i2, int i3, int i4);
        }

        public SnackbarLayout(Context context) {
            this(context, (AttributeSet) null);
        }

        public SnackbarLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SnackbarLayout);
            this.f3334 = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SnackbarLayout_android_maxWidth, -1);
            this.f3333 = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SnackbarLayout_maxActionInlineWidth, -1);
            if (obtainStyledAttributes.hasValue(R.styleable.SnackbarLayout_elevation)) {
                ViewCompat.setElevation(this, (float) obtainStyledAttributes.getDimensionPixelSize(R.styleable.SnackbarLayout_elevation, 0));
            }
            obtainStyledAttributes.recycle();
            setClickable(true);
            LayoutInflater.from(context).inflate(R.layout.tsnackbar_layout_include, this);
            ViewCompat.setAccessibilityLiveRegion(this, 1);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static void m3926(View view, int i, int i2) {
            if (ViewCompat.isPaddingRelative(view)) {
                ViewCompat.setPaddingRelative(view, ViewCompat.getPaddingStart(view), i, ViewCompat.getPaddingEnd(view), i2);
            } else {
                view.setPadding(view.getPaddingLeft(), i, view.getPaddingRight(), i2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m3927(int i, int i2, int i3) {
            boolean z = false;
            if (i != getOrientation()) {
                setOrientation(i);
                z = true;
            }
            if (this.f3335.getPaddingTop() == i2 && this.f3335.getPaddingBottom() == i3) {
                return z;
            }
            m3926((View) this.f3335, i2, i3);
            return true;
        }

        /* access modifiers changed from: package-private */
        public Button getActionView() {
            return this.f3332;
        }

        /* access modifiers changed from: package-private */
        public TextView getMessageView() {
            return this.f3335;
        }

        /* access modifiers changed from: protected */
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            if (this.f3330 != null) {
                this.f3330.m3931(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            if (this.f3330 != null) {
                this.f3330.m3930(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            super.onFinishInflate();
            this.f3335 = (TextView) findViewById(R.id.snackbar_text);
            this.f3332 = (Button) findViewById(R.id.snackbar_action);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            if (z && this.f3331 != null) {
                this.f3331.m3932(this, i, i2, i3, i4);
            }
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (this.f3334 > 0 && getMeasuredWidth() > this.f3334) {
                i = View.MeasureSpec.makeMeasureSpec(this.f3334, 1073741824);
                super.onMeasure(i, i2);
            }
            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.design_snackbar_padding_vertical_2lines);
            int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.design_snackbar_padding_vertical);
            boolean z = this.f3335.getLayout().getLineCount() > 1;
            boolean z2 = false;
            if (!z || this.f3333 <= 0 || this.f3332.getMeasuredWidth() <= this.f3333) {
                int i3 = z ? dimensionPixelSize : dimensionPixelSize2;
                if (m3927(0, i3, i3)) {
                    z2 = true;
                }
            } else if (m3927(1, dimensionPixelSize, dimensionPixelSize - dimensionPixelSize2)) {
                z2 = true;
            }
            if (z2) {
                super.onMeasure(i, i2);
            }
        }

        /* access modifiers changed from: package-private */
        public void setOnAttachStateChangeListener(OnAttachStateChangeListener onAttachStateChangeListener) {
            this.f3330 = onAttachStateChangeListener;
        }

        /* access modifiers changed from: package-private */
        public void setOnLayoutChangeListener(OnLayoutChangeListener onLayoutChangeListener) {
            this.f3331 = onLayoutChangeListener;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m3928(int i, int i2) {
            ViewCompat.setAlpha(this.f3335, 1.0f);
            ViewCompat.animate(this.f3335).alpha(0.0f).setDuration((long) i2).setStartDelay((long) i).start();
            if (this.f3332.getVisibility() == 0) {
                ViewCompat.setAlpha(this.f3332, 1.0f);
                ViewCompat.animate(this.f3332).alpha(0.0f).setDuration((long) i2).setStartDelay((long) i).start();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m3929(int i, int i2) {
            ViewCompat.setAlpha(this.f3335, 0.0f);
            ViewCompat.animate(this.f3335).alpha(1.0f).setDuration((long) i2).setStartDelay((long) i).start();
            if (this.f3332.getVisibility() == 0) {
                ViewCompat.setAlpha(this.f3332, 0.0f);
                ViewCompat.animate(this.f3332).alpha(1.0f).setDuration((long) i2).setStartDelay((long) i).start();
            }
        }
    }

    private TSnackbar(ViewGroup viewGroup) {
        this.f3315 = viewGroup;
        this.f3317 = viewGroup.getContext();
        this.f3316 = (SnackbarLayout) LayoutInflater.from(this.f3317).inflate(R.layout.tsnackbar_layout, this.f3315, false);
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m3897() {
        if (Build.VERSION.SDK_INT >= 14) {
            ViewCompat.setTranslationY(this.f3316, (float) (-this.f3316.getHeight()));
            ViewCompat.animate(this.f3316).translationY(0.0f).setInterpolator(AnimationUtils.f3300).setDuration(250).setListener(new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationEnd(View view) {
                    if (TSnackbar.this.f3312 != null) {
                        TSnackbar.this.f3312.m3924(TSnackbar.this);
                    }
                    SnackbarManager.m3880().m3885(TSnackbar.this.f3313);
                }

                public void onAnimationStart(View view) {
                    TSnackbar.this.f3316.m3929(70, 180);
                }
            }).start();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.f3316.getContext(), R.anim.top_in);
        loadAnimation.setInterpolator(AnimationUtils.f3300);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (TSnackbar.this.f3312 != null) {
                    TSnackbar.this.f3312.m3924(TSnackbar.this);
                }
                SnackbarManager.m3880().m3885(TSnackbar.this.f3313);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.f3316.startAnimation(loadAnimation);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m3898() {
        ViewGroup.LayoutParams layoutParams = this.f3316.getLayoutParams();
        if (!(layoutParams instanceof CoordinatorLayout.LayoutParams)) {
            return false;
        }
        CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) layoutParams).getBehavior();
        return (behavior instanceof SwipeDismissBehavior) && ((SwipeDismissBehavior) behavior).getDragState() != 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m3900(int i) {
        SnackbarManager.m3880().m3889(this.f3313);
        if (this.f3312 != null) {
            this.f3312.m3925(this, i);
        }
        ViewParent parent = this.f3316.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.f3316);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m3904(final int i) {
        if (Build.VERSION.SDK_INT >= 14) {
            ViewCompat.animate(this.f3316).translationY((float) (-this.f3316.getHeight())).setInterpolator(AnimationUtils.f3300).setDuration(250).setListener(new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationEnd(View view) {
                    TSnackbar.this.m3900(i);
                }

                public void onAnimationStart(View view) {
                    TSnackbar.this.f3316.m3928(0, 180);
                }
            }).start();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.f3316.getContext(), R.anim.top_out);
        loadAnimation.setInterpolator(AnimationUtils.f3300);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                TSnackbar.this.m3900(i);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.f3316.startAnimation(loadAnimation);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m3906(int i) {
        SnackbarManager.m3880().m3890(this.f3313, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ViewGroup m3907(View view) {
        ViewGroup viewGroup;
        ViewGroup viewGroup2 = null;
        while (!(view instanceof CoordinatorLayout)) {
            if (!(view instanceof FrameLayout)) {
                viewGroup = viewGroup2;
            } else if (view.getId() == 16908290) {
                return (ViewGroup) view;
            } else {
                viewGroup = (ViewGroup) view;
            }
            if (view != null) {
                ViewParent parent = view.getParent();
                view = parent instanceof View ? (View) parent : null;
            }
            if (view == null) {
                ViewGroup viewGroup3 = viewGroup;
                return viewGroup;
            }
            viewGroup2 = viewGroup;
        }
        return (ViewGroup) view;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static TSnackbar m3909(View view, CharSequence charSequence, int i) {
        TSnackbar tSnackbar = new TSnackbar(m3907(view));
        tSnackbar.m3916(charSequence);
        tSnackbar.m3915(i);
        return tSnackbar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m3911() {
        m3906(3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3912(int i) {
        if (this.f3316.getVisibility() != 0 || m3898()) {
            m3900(i);
        } else {
            m3904(i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m3913() {
        if (this.f3316.getParent() == null) {
            ViewGroup.LayoutParams layoutParams = this.f3316.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.LayoutParams) {
                Behavior behavior = new Behavior();
                behavior.setStartAlphaSwipeDistance(0.1f);
                behavior.setEndAlphaSwipeDistance(0.6f);
                behavior.setSwipeDirection(0);
                behavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
                    public void onDismiss(View view) {
                        TSnackbar.this.m3906(0);
                    }

                    public void onDragStateChanged(int i) {
                        switch (i) {
                            case 0:
                                SnackbarManager.m3880().m3886(TSnackbar.this.f3313);
                                return;
                            case 1:
                            case 2:
                                SnackbarManager.m3880().m3887(TSnackbar.this.f3313);
                                return;
                            default:
                                return;
                        }
                    }
                });
                ((CoordinatorLayout.LayoutParams) layoutParams).setBehavior(behavior);
            }
            this.f3315.addView(this.f3316);
        }
        this.f3316.setOnAttachStateChangeListener(new SnackbarLayout.OnAttachStateChangeListener() {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m3920(View view) {
                if (TSnackbar.this.m3914()) {
                    TSnackbar.f3311.post(new Runnable() {
                        public void run() {
                            TSnackbar.this.m3900(3);
                        }
                    });
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m3921(View view) {
            }
        });
        if (ViewCompat.isLaidOut(this.f3316)) {
            m3897();
        } else {
            this.f3316.setOnLayoutChangeListener(new SnackbarLayout.OnLayoutChangeListener() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m3922(View view, int i, int i2, int i3, int i4) {
                    TSnackbar.this.m3897();
                    TSnackbar.this.f3316.setOnLayoutChangeListener((SnackbarLayout.OnLayoutChangeListener) null);
                }
            });
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m3914() {
        return SnackbarManager.m3880().m3884(this.f3313);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TSnackbar m3915(int i) {
        this.f3314 = i;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TSnackbar m3916(CharSequence charSequence) {
        this.f3316.getMessageView().setText(charSequence);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3917() {
        SnackbarManager.m3880().m3888(this.f3314, this.f3313);
    }
}
