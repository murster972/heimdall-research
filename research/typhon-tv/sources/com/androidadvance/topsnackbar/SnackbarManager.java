package com.androidadvance.topsnackbar;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

class SnackbarManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private static SnackbarManager f3303;

    /* renamed from: 连任  reason: contains not printable characters */
    private SnackbarRecord f3304;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Object f3305 = new Object();

    /* renamed from: 麤  reason: contains not printable characters */
    private SnackbarRecord f3306;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Handler f3307 = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    SnackbarManager.this.m3879((SnackbarRecord) message.obj);
                    return true;
                default:
                    return false;
            }
        }
    });

    interface Callback {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3891();

        /* renamed from: 龘  reason: contains not printable characters */
        void m3892(int i);
    }

    private static class SnackbarRecord {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public int f3309;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final WeakReference<Callback> f3310;

        SnackbarRecord(int i, Callback callback) {
            this.f3310 = new WeakReference<>(callback);
            this.f3309 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m3896(Callback callback) {
            return callback != null && this.f3310.get() == callback;
        }
    }

    private SnackbarManager() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3876(Callback callback) {
        return this.f3306 != null && this.f3306.m3896(callback);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m3877(Callback callback) {
        return this.f3304 != null && this.f3304.m3896(callback);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m3878() {
        if (this.f3304 != null) {
            this.f3306 = this.f3304;
            this.f3304 = null;
            Callback callback = (Callback) this.f3306.f3310.get();
            if (callback != null) {
                callback.m3891();
            } else {
                this.f3306 = null;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m3879(SnackbarRecord snackbarRecord) {
        synchronized (this.f3305) {
            if (this.f3306 == snackbarRecord || this.f3304 == snackbarRecord) {
                m3883(snackbarRecord, 2);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static SnackbarManager m3880() {
        if (f3303 == null) {
            f3303 = new SnackbarManager();
        }
        return f3303;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3881(SnackbarRecord snackbarRecord) {
        if (snackbarRecord.f3309 != -2) {
            int i = 2750;
            if (snackbarRecord.f3309 > 0) {
                i = snackbarRecord.f3309;
            } else if (snackbarRecord.f3309 == -1) {
                i = 1500;
            }
            this.f3307.removeCallbacksAndMessages(snackbarRecord);
            this.f3307.sendMessageDelayed(Message.obtain(this.f3307, 0, snackbarRecord), (long) i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m3883(SnackbarRecord snackbarRecord, int i) {
        Callback callback = (Callback) snackbarRecord.f3310.get();
        if (callback == null) {
            return false;
        }
        callback.m3892(i);
        return true;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m3884(Callback callback) {
        boolean z;
        synchronized (this.f3305) {
            z = m3876(callback) || m3877(callback);
        }
        return z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m3885(Callback callback) {
        synchronized (this.f3305) {
            if (m3876(callback)) {
                m3881(this.f3306);
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m3886(Callback callback) {
        synchronized (this.f3305) {
            if (m3876(callback)) {
                m3881(this.f3306);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m3887(Callback callback) {
        synchronized (this.f3305) {
            if (m3876(callback)) {
                this.f3307.removeCallbacksAndMessages(this.f3306);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3888(int i, Callback callback) {
        synchronized (this.f3305) {
            if (m3876(callback)) {
                int unused = this.f3306.f3309 = i;
                this.f3307.removeCallbacksAndMessages(this.f3306);
                m3881(this.f3306);
                return;
            }
            if (m3877(callback)) {
                int unused2 = this.f3304.f3309 = i;
            } else {
                this.f3304 = new SnackbarRecord(i, callback);
            }
            if (this.f3306 == null || !m3883(this.f3306, 4)) {
                this.f3306 = null;
                m3878();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3889(Callback callback) {
        synchronized (this.f3305) {
            if (m3876(callback)) {
                this.f3306 = null;
                if (this.f3304 != null) {
                    m3878();
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3890(Callback callback, int i) {
        synchronized (this.f3305) {
            if (m3876(callback)) {
                m3883(this.f3306, i);
            } else if (m3877(callback)) {
                m3883(this.f3304, i);
            }
        }
    }
}
