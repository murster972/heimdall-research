package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TyphoonTV;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class MovieFileHD extends BaseProvider {
    private String b = "https://moviefiles.org";

    public String a() {
        return "MovieFileHD";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
        observableEmitter.a();
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo) {
        String name = movieInfo.getName();
        String valueOf = String.valueOf(movieInfo.getYear());
        TyphoonTV typhoonTV = new TyphoonTV();
        String a = Utils.a((name.replace("'", "") + StringUtils.SPACE + valueOf).replaceAll("(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)", StringUtils.SPACE).replace("  ", StringUtils.SPACE), new boolean[0]);
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36");
        HttpHelper.a().c(this.b, this.b);
        Iterator it2 = Jsoup.a(HttpHelper.a().a(this.b + "/index.php?search=" + a, new Map[]{hashMap})).e("tbody").b("td").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String c = element.c("href");
            String x = element.x();
            if (!c.isEmpty()) {
                String str = this.b + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + c;
                HttpHelper.a().c(str, str);
                hashMap.put("referer", str);
                try {
                    String c2 = Jsoup.a(HttpHelper.a().a(str + "&dl", new Map[]{hashMap})).f("input[id=copyTarget]").c("value");
                    if (!c2.isEmpty()) {
                        String str2 = "HD";
                        String a2 = a();
                        TyphoonTV.ParsedLinkModel b2 = typhoonTV.b(x);
                        if (b2 != null) {
                            if (!b2.a().equalsIgnoreCase("HQ")) {
                                str2 = b2.a();
                            }
                            a2 = b(b2.b());
                        }
                        if (GoogleVideoHelper.a(c2)) {
                            HashMap hashMap2 = new HashMap();
                            hashMap2.put("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36");
                            MediaSource mediaSource = new MediaSource(a2, "GoogleVideo", false);
                            mediaSource.setStreamLink(c2);
                            mediaSource.setPlayHeader(hashMap2);
                            mediaSource.setNeededToResolve(false);
                            mediaSource.setQuality(str2);
                            observableEmitter.a(mediaSource);
                        } else {
                            a(observableEmitter, c2, str2, a2);
                        }
                    }
                } catch (Throwable th) {
                    Logger.a(th, new boolean[0]);
                }
            }
        }
    }
}
