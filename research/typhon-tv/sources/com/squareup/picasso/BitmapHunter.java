package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.NetworkRequestHandler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

class BitmapHunter implements Runnable {

    /* renamed from: ˆ  reason: contains not printable characters */
    private static final RequestHandler f12327 = new RequestHandler() {
        /* renamed from: 龘  reason: contains not printable characters */
        public RequestHandler.Result m15581(Request request, int i) throws IOException {
            throw new IllegalStateException("Unrecognized type of request: " + request);
        }
    };

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final Object f12328 = new Object();

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final ThreadLocal<StringBuilder> f12329 = new ThreadLocal<StringBuilder>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public StringBuilder initialValue() {
            return new StringBuilder("Picasso-");
        }
    };

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final AtomicInteger f12330 = new AtomicInteger();

    /* renamed from: ʻ  reason: contains not printable characters */
    final Request f12331;

    /* renamed from: ʼ  reason: contains not printable characters */
    final int f12332;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f12333;

    /* renamed from: ʾ  reason: contains not printable characters */
    Picasso.LoadedFrom f12334;

    /* renamed from: ʿ  reason: contains not printable characters */
    Exception f12335;

    /* renamed from: ˈ  reason: contains not printable characters */
    Bitmap f12336;

    /* renamed from: ˑ  reason: contains not printable characters */
    final RequestHandler f12337;

    /* renamed from: ٴ  reason: contains not printable characters */
    Action f12338;

    /* renamed from: ᐧ  reason: contains not printable characters */
    List<Action> f12339;

    /* renamed from: 连任  reason: contains not printable characters */
    final String f12340;

    /* renamed from: 靐  reason: contains not printable characters */
    final Dispatcher f12341;

    /* renamed from: 麤  reason: contains not printable characters */
    final Stats f12342;

    /* renamed from: 齉  reason: contains not printable characters */
    final Cache f12343;

    /* renamed from: 龘  reason: contains not printable characters */
    final Picasso f12344;

    /* renamed from: ﹶ  reason: contains not printable characters */
    int f12345;

    /* renamed from: ﾞ  reason: contains not printable characters */
    int f12346;

    /* renamed from: 龘  reason: contains not printable characters */
    static Bitmap m15568(Request request, Bitmap bitmap, int i) {
        float f;
        float f2;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        boolean z = request.f12397;
        int i2 = 0;
        int i3 = 0;
        int i4 = width;
        int i5 = height;
        Matrix matrix = new Matrix();
        if (request.m15624()) {
            int i6 = request.f12394;
            int i7 = request.f12400;
            float f3 = request.f12395;
            if (f3 != 0.0f) {
                if (request.f12409) {
                    matrix.setRotate(f3, request.f12396, request.f12408);
                } else {
                    matrix.setRotate(f3);
                }
            }
            if (request.f12401) {
                float f4 = ((float) i6) / ((float) width);
                float f5 = ((float) i7) / ((float) height);
                if (f4 > f5) {
                    int ceil = (int) Math.ceil((double) (((float) height) * (f5 / f4)));
                    i3 = (height - ceil) / 2;
                    i5 = ceil;
                    f = f4;
                    f2 = ((float) i7) / ((float) i5);
                } else {
                    int ceil2 = (int) Math.ceil((double) (((float) width) * (f4 / f5)));
                    i2 = (width - ceil2) / 2;
                    i4 = ceil2;
                    f = ((float) i6) / ((float) i4);
                    f2 = f5;
                }
                if (m15572(z, width, height, i6, i7)) {
                    matrix.preScale(f, f2);
                }
            } else if (request.f12402) {
                float f6 = ((float) i6) / ((float) width);
                float f7 = ((float) i7) / ((float) height);
                float f8 = f6 < f7 ? f6 : f7;
                if (m15572(z, width, height, i6, i7)) {
                    matrix.preScale(f8, f8);
                }
            } else if (!((i6 == 0 && i7 == 0) || (i6 == width && i7 == height))) {
                float f9 = i6 != 0 ? ((float) i6) / ((float) width) : ((float) i7) / ((float) height);
                float f10 = i7 != 0 ? ((float) i7) / ((float) height) : ((float) i6) / ((float) width);
                if (m15572(z, width, height, i6, i7)) {
                    matrix.preScale(f9, f10);
                }
            }
        }
        if (i != 0) {
            matrix.preRotate((float) i);
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, i2, i3, i4, i5, matrix, true);
        if (createBitmap == bitmap) {
            return bitmap;
        }
        bitmap.recycle();
        return createBitmap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Bitmap m15569(InputStream inputStream, Request request) throws IOException {
        MarkableInputStream markableInputStream = new MarkableInputStream(inputStream);
        MarkableInputStream markableInputStream2 = markableInputStream;
        long r4 = markableInputStream.m15602(65536);
        BitmapFactory.Options r7 = RequestHandler.m15640(request);
        boolean r2 = RequestHandler.m15643(r7);
        boolean r3 = Utils.m15661(markableInputStream2);
        markableInputStream.m15603(r4);
        if (r3) {
            byte[] r1 = Utils.m15660(markableInputStream2);
            if (r2) {
                BitmapFactory.decodeByteArray(r1, 0, r1.length, r7);
                RequestHandler.m15642(request.f12394, request.f12400, r7, request);
            }
            return BitmapFactory.decodeByteArray(r1, 0, r1.length, r7);
        }
        if (r2) {
            BitmapFactory.decodeStream(markableInputStream2, (Rect) null, r7);
            RequestHandler.m15642(request.f12394, request.f12400, r7, request);
            markableInputStream.m15603(r4);
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(markableInputStream2, (Rect) null, r7);
        if (decodeStream != null) {
            return decodeStream;
        }
        throw new IOException("Failed to decode stream.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Bitmap m15570(List<Transformation> list, Bitmap bitmap) {
        int i = 0;
        int size = list.size();
        while (i < size) {
            final Transformation transformation = list.get(i);
            try {
                Bitmap r4 = transformation.m15657(bitmap);
                if (r4 == null) {
                    final StringBuilder append = new StringBuilder().append("Transformation ").append(transformation.m15658()).append(" returned null after ").append(i).append(" previous transformation(s).\n\nTransformation list:\n");
                    for (Transformation r5 : list) {
                        append.append(r5.m15658()).append(10);
                    }
                    Picasso.f12368.post(new Runnable() {
                        public void run() {
                            throw new NullPointerException(append.toString());
                        }
                    });
                    return null;
                } else if (r4 == bitmap && bitmap.isRecycled()) {
                    Picasso.f12368.post(new Runnable() {
                        public void run() {
                            throw new IllegalStateException("Transformation " + transformation.m15658() + " returned input Bitmap but recycled it.");
                        }
                    });
                    return null;
                } else if (r4 == bitmap || bitmap.isRecycled()) {
                    bitmap = r4;
                    i++;
                } else {
                    Picasso.f12368.post(new Runnable() {
                        public void run() {
                            throw new IllegalStateException("Transformation " + transformation.m15658() + " mutated input Bitmap but failed to recycle the original.");
                        }
                    });
                    return null;
                }
            } catch (RuntimeException e) {
                Picasso.f12368.post(new Runnable() {
                    public void run() {
                        throw new RuntimeException("Transformation " + transformation.m15658() + " crashed with exception.", e);
                    }
                });
                return null;
            }
        }
        return bitmap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m15571(Request request) {
        String r1 = request.m15629();
        StringBuilder sb = f12329.get();
        sb.ensureCapacity("Picasso-".length() + r1.length());
        sb.replace("Picasso-".length(), sb.length(), r1);
        Thread.currentThread().setName(sb.toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m15572(boolean z, int i, int i2, int i3, int i4) {
        return !z || i > i3 || i2 > i4;
    }

    public void run() {
        String str;
        try {
            m15571(this.f12331);
            if (this.f12344.f12376) {
                Utils.m15670("Hunter", "executing", Utils.m15664(this));
            }
            this.f12336 = m15579();
            if (this.f12336 == null) {
                this.f12341.m15593(this);
            } else {
                this.f12341.m15595(this);
            }
        } catch (Downloader.ResponseException e) {
            if (!e.localCacheOnly || e.responseCode != 504) {
                this.f12335 = e;
            }
            this.f12341.m15593(this);
        } catch (NetworkRequestHandler.ContentLengthException e2) {
            this.f12335 = e2;
            this.f12341.m15592(this);
        } catch (IOException e3) {
            this.f12335 = e3;
            this.f12341.m15592(this);
        } catch (OutOfMemoryError e4) {
            StringWriter stringWriter = new StringWriter();
            this.f12342.m15652().m15656(new PrintWriter(stringWriter));
            this.f12335 = new RuntimeException(stringWriter.toString(), e4);
            this.f12341.m15593(this);
        } catch (Exception e5) {
            this.f12335 = e5;
            this.f12341.m15593(this);
        } finally {
            str = "Picasso-Idle";
            Thread.currentThread().setName(str);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Exception m15573() {
        return this.f12335;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Picasso.LoadedFrom m15574() {
        return this.f12334;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public List<Action> m15575() {
        return this.f12339;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Bitmap m15576() {
        return this.f12336;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public Action m15577() {
        return this.f12338;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public Request m15578() {
        return this.f12331;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m15579() throws IOException {
        Bitmap bitmap = null;
        if (!MemoryPolicy.m15604(this.f12332) || (bitmap = this.f12343.m15584(this.f12340)) == null) {
            this.f12331.f12406 = this.f12346 == 0 ? NetworkPolicy.OFFLINE.index : this.f12333;
            RequestHandler.Result r4 = this.f12337.m15644(this.f12331, this.f12333);
            if (r4 != null) {
                this.f12334 = r4.m15647();
                this.f12345 = r4.m15646();
                bitmap = r4.m15648();
                if (bitmap == null) {
                    InputStream r3 = r4.m15645();
                    try {
                        bitmap = m15569(r3, this.f12331);
                    } finally {
                        Utils.m15669(r3);
                    }
                }
            }
            if (bitmap != null) {
                if (this.f12344.f12376) {
                    Utils.m15670("Hunter", "decoded", this.f12331.m15630());
                }
                this.f12342.m15655(bitmap);
                if (this.f12331.m15626() || this.f12345 != 0) {
                    synchronized (f12328) {
                        if (this.f12331.m15624() || this.f12345 != 0) {
                            bitmap = m15568(this.f12331, bitmap, this.f12345);
                            if (this.f12344.f12376) {
                                Utils.m15670("Hunter", "transformed", this.f12331.m15630());
                            }
                        }
                        if (this.f12331.m15625()) {
                            bitmap = m15570(this.f12331.f12393, bitmap);
                            if (this.f12344.f12376) {
                                Utils.m15671("Hunter", "transformed", this.f12331.m15630(), "from custom transformations");
                            }
                        }
                    }
                    if (bitmap != null) {
                        this.f12342.m15651(bitmap);
                    }
                }
            }
            Bitmap bitmap2 = bitmap;
            return bitmap;
        }
        this.f12342.m15653();
        this.f12334 = Picasso.LoadedFrom.MEMORY;
        if (this.f12344.f12376) {
            Utils.m15671("Hunter", "decoded", this.f12331.m15630(), "from cache");
        }
        Bitmap bitmap3 = bitmap;
        return bitmap;
    }
}
