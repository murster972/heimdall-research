package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import io.presage.ads.NewAd;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestCreator {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final AtomicInteger f12425 = new AtomicInteger();

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f12426;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f12427;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f12428;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Object f12429;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Drawable f12430;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f12431;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f12432;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Drawable f12433;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f12434;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Picasso f12435;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f12436;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Request.Builder f12437;

    /* renamed from: 靐  reason: contains not printable characters */
    private Drawable m15635() {
        return this.f12427 != 0 ? this.f12435.f12379.getResources().getDrawable(this.f12427) : this.f12433;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Request m15636(long j) {
        int andIncrement = f12425.getAndIncrement();
        Request r2 = this.f12437.m15632();
        r2.f12407 = andIncrement;
        r2.f12404 = j;
        boolean z = this.f12435.f12376;
        if (z) {
            Utils.m15671("Main", "created", r2.m15627(), r2.toString());
        }
        Request r3 = this.f12435.m15612(r2);
        if (r3 != r2) {
            r3.f12407 = andIncrement;
            r3.f12404 = j;
            if (z) {
                Utils.m15671("Main", "changed", r3.m15630(), "into " + r3);
            }
        }
        return r3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public RequestCreator m15637() {
        this.f12434 = false;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public RequestCreator m15638(int i, int i2) {
        this.f12437.m15633(i, i2);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15639(ImageView imageView, Callback callback) {
        Bitmap r6;
        long nanoTime = System.nanoTime();
        Utils.m15668();
        if (imageView == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (!this.f12437.m15634()) {
            this.f12435.m15613(imageView);
            if (this.f12426) {
                PicassoDrawable.m15623(imageView, m15635());
            }
        } else {
            if (this.f12434) {
                if (this.f12437.m15631()) {
                    throw new IllegalStateException("Fit cannot be used with resize.");
                }
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                if (width == 0 || height == 0) {
                    if (this.f12426) {
                        PicassoDrawable.m15623(imageView, m15635());
                    }
                    this.f12435.m15614(imageView, new DeferredRequestCreator(this, imageView, callback));
                    return;
                }
                this.f12437.m15633(width, height);
            }
            Request r10 = m15636(nanoTime);
            String r15 = Utils.m15666(r10);
            if (!MemoryPolicy.m15604(this.f12431) || (r6 = this.f12435.m15611(r15)) == null) {
                if (this.f12426) {
                    PicassoDrawable.m15623(imageView, m15635());
                }
                this.f12435.m15615((Action) new ImageViewAction(this.f12435, imageView, r10, this.f12431, this.f12432, this.f12428, this.f12430, r15, this.f12429, callback, this.f12436));
                return;
            }
            this.f12435.m15613(imageView);
            PicassoDrawable.m15622(imageView, this.f12435.f12379, r6, Picasso.LoadedFrom.MEMORY, this.f12436, this.f12435.f12375);
            if (this.f12435.f12376) {
                Utils.m15671("Main", NewAd.EVENT_COMPLETED, r10.m15627(), "from " + Picasso.LoadedFrom.MEMORY);
            }
            if (callback != null) {
                callback.m15589();
            }
        }
    }
}
