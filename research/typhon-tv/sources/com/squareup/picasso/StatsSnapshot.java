package com.squareup.picasso;

import java.io.PrintWriter;

public class StatsSnapshot {

    /* renamed from: ʻ  reason: contains not printable characters */
    public final long f12455;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final long f12456;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final long f12457;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int f12458;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final long f12459;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final int f12460;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final long f12461;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final long f12462;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final int f12463;

    /* renamed from: 连任  reason: contains not printable characters */
    public final long f12464;

    /* renamed from: 靐  reason: contains not printable characters */
    public final int f12465;

    /* renamed from: 麤  reason: contains not printable characters */
    public final long f12466;

    /* renamed from: 齉  reason: contains not printable characters */
    public final long f12467;

    /* renamed from: 龘  reason: contains not printable characters */
    public final int f12468;

    public StatsSnapshot(int i, int i2, long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, int i3, int i4, int i5, long j9) {
        this.f12468 = i;
        this.f12465 = i2;
        this.f12467 = j;
        this.f12466 = j2;
        this.f12464 = j3;
        this.f12455 = j4;
        this.f12456 = j5;
        this.f12457 = j6;
        this.f12461 = j7;
        this.f12462 = j8;
        this.f12463 = i3;
        this.f12460 = i4;
        this.f12458 = i5;
        this.f12459 = j9;
    }

    public String toString() {
        return "StatsSnapshot{maxSize=" + this.f12468 + ", size=" + this.f12465 + ", cacheHits=" + this.f12467 + ", cacheMisses=" + this.f12466 + ", downloadCount=" + this.f12463 + ", totalDownloadSize=" + this.f12464 + ", averageDownloadSize=" + this.f12457 + ", totalOriginalBitmapSize=" + this.f12455 + ", totalTransformedBitmapSize=" + this.f12456 + ", averageOriginalBitmapSize=" + this.f12461 + ", averageTransformedBitmapSize=" + this.f12462 + ", originalBitmapCount=" + this.f12460 + ", transformedBitmapCount=" + this.f12458 + ", timeStamp=" + this.f12459 + '}';
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15656(PrintWriter printWriter) {
        printWriter.println("===============BEGIN PICASSO STATS ===============");
        printWriter.println("Memory Cache Stats");
        printWriter.print("  Max Cache Size: ");
        printWriter.println(this.f12468);
        printWriter.print("  Cache Size: ");
        printWriter.println(this.f12465);
        printWriter.print("  Cache % Full: ");
        printWriter.println((int) Math.ceil((double) ((((float) this.f12465) / ((float) this.f12468)) * 100.0f)));
        printWriter.print("  Cache Hits: ");
        printWriter.println(this.f12467);
        printWriter.print("  Cache Misses: ");
        printWriter.println(this.f12466);
        printWriter.println("Network Stats");
        printWriter.print("  Download Count: ");
        printWriter.println(this.f12463);
        printWriter.print("  Total Download Size: ");
        printWriter.println(this.f12464);
        printWriter.print("  Average Download Size: ");
        printWriter.println(this.f12457);
        printWriter.println("Bitmap Stats");
        printWriter.print("  Total Bitmaps Decoded: ");
        printWriter.println(this.f12460);
        printWriter.print("  Total Bitmap Size: ");
        printWriter.println(this.f12455);
        printWriter.print("  Total Transformed Bitmaps: ");
        printWriter.println(this.f12458);
        printWriter.print("  Total Transformed Bitmap Size: ");
        printWriter.println(this.f12456);
        printWriter.print("  Average Bitmap Size: ");
        printWriter.println(this.f12461);
        printWriter.print("  Average Transformed Bitmap Size: ");
        printWriter.println(this.f12462);
        printWriter.println("===============END PICASSO STATS ===============");
        printWriter.flush();
    }
}
