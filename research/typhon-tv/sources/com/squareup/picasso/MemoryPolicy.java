package com.squareup.picasso;

public enum MemoryPolicy {
    NO_CACHE(1),
    NO_STORE(2);
    
    final int index;

    private MemoryPolicy(int i) {
        this.index = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m15604(int i) {
        return (NO_CACHE.index & i) == 0;
    }
}
