package com.squareup.picasso;

import android.graphics.Bitmap;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestHandler;
import java.io.IOException;
import java.io.InputStream;

class NetworkRequestHandler extends RequestHandler {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Stats f12365;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Downloader f12366;

    static class ContentLengthException extends IOException {
        public ContentLengthException(String str) {
            super(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public RequestHandler.Result m15605(Request request, int i) throws IOException {
        Downloader.Response r3 = this.f12366.m15596(request.f12405, request.f12406);
        if (r3 == null) {
            return null;
        }
        Picasso.LoadedFrom loadedFrom = r3.f5818 ? Picasso.LoadedFrom.DISK : Picasso.LoadedFrom.NETWORK;
        Bitmap r0 = r3.m6276();
        if (r0 != null) {
            return new RequestHandler.Result(r0, loadedFrom);
        }
        InputStream r1 = r3.m6278();
        if (r1 == null) {
            return null;
        }
        if (loadedFrom == Picasso.LoadedFrom.DISK && r3.m6277() == 0) {
            Utils.m15669(r1);
            throw new ContentLengthException("Received response with 0 content-length header.");
        }
        if (loadedFrom == Picasso.LoadedFrom.NETWORK && r3.m6277() > 0) {
            this.f12365.m15654(r3.m6277());
        }
        return new RequestHandler.Result(r1, loadedFrom);
    }
}
