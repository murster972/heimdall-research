package com.squareup.picasso;

import android.graphics.Bitmap;
import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;

public interface Downloader {

    public static class Response {

        /* renamed from: 靐  reason: contains not printable characters */
        final Bitmap f5816;

        /* renamed from: 麤  reason: contains not printable characters */
        final long f5817;

        /* renamed from: 齉  reason: contains not printable characters */
        final boolean f5818;

        /* renamed from: 龘  reason: contains not printable characters */
        final InputStream f5819;

        @Deprecated
        /* renamed from: 靐  reason: contains not printable characters */
        public Bitmap m6276() {
            return this.f5816;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public long m6277() {
            return this.f5817;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public InputStream m6278() {
            return this.f5819;
        }
    }

    public static class ResponseException extends IOException {
        final boolean localCacheOnly;
        final int responseCode;

        public ResponseException(String str, int i, int i2) {
            super(str);
            this.localCacheOnly = NetworkPolicy.isOfflineOnly(i);
            this.responseCode = i2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    Response m15596(Uri uri, int i) throws IOException;
}
