package com.squareup.picasso;

import android.os.Handler;

class Dispatcher {

    /* renamed from: 龘  reason: contains not printable characters */
    final Handler f12356;

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m15591(Action action) {
        this.f12356.sendMessage(this.f12356.obtainMessage(2, action));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m15592(BitmapHunter bitmapHunter) {
        this.f12356.sendMessageDelayed(this.f12356.obtainMessage(5, bitmapHunter), 500);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m15593(BitmapHunter bitmapHunter) {
        this.f12356.sendMessage(this.f12356.obtainMessage(6, bitmapHunter));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15594(Action action) {
        this.f12356.sendMessage(this.f12356.obtainMessage(1, action));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15595(BitmapHunter bitmapHunter) {
        this.f12356.sendMessage(this.f12356.obtainMessage(4, bitmapHunter));
    }
}
