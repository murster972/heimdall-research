package com.squareup.picasso;

import android.graphics.Bitmap;
import android.os.Handler;

class Stats {

    /* renamed from: ʻ  reason: contains not printable characters */
    long f12442;

    /* renamed from: ʼ  reason: contains not printable characters */
    long f12443;

    /* renamed from: ʽ  reason: contains not printable characters */
    long f12444;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f12445;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f12446;

    /* renamed from: ˑ  reason: contains not printable characters */
    long f12447;

    /* renamed from: ٴ  reason: contains not printable characters */
    long f12448;

    /* renamed from: ᐧ  reason: contains not printable characters */
    int f12449;

    /* renamed from: 连任  reason: contains not printable characters */
    long f12450;

    /* renamed from: 靐  reason: contains not printable characters */
    final Handler f12451;

    /* renamed from: 麤  reason: contains not printable characters */
    long f12452;

    /* renamed from: 齉  reason: contains not printable characters */
    long f12453;

    /* renamed from: 龘  reason: contains not printable characters */
    final Cache f12454;

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15649(Bitmap bitmap, int i) {
        this.f12451.sendMessage(this.f12451.obtainMessage(i, Utils.m15662(bitmap), 0));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m15650() {
        this.f12451.sendEmptyMessage(1);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m15651(Bitmap bitmap) {
        m15649(bitmap, 3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public StatsSnapshot m15652() {
        return new StatsSnapshot(this.f12454.m15582(), this.f12454.m15583(), this.f12453, this.f12452, this.f12450, this.f12442, this.f12443, this.f12444, this.f12447, this.f12448, this.f12449, this.f12446, this.f12445, System.currentTimeMillis());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15653() {
        this.f12451.sendEmptyMessage(0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15654(long j) {
        this.f12451.sendMessage(this.f12451.obtainMessage(4, Long.valueOf(j)));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15655(Bitmap bitmap) {
        m15649(bitmap, 2);
    }
}
