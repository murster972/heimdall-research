package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.squareup.picasso.Picasso;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

abstract class Action<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    final int f12314;

    /* renamed from: ʼ  reason: contains not printable characters */
    final int f12315;

    /* renamed from: ʽ  reason: contains not printable characters */
    final Drawable f12316;

    /* renamed from: ˈ  reason: contains not printable characters */
    boolean f12317;

    /* renamed from: ˑ  reason: contains not printable characters */
    final String f12318;

    /* renamed from: ٴ  reason: contains not printable characters */
    final Object f12319;

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean f12320;

    /* renamed from: 连任  reason: contains not printable characters */
    final int f12321;

    /* renamed from: 靐  reason: contains not printable characters */
    final Request f12322;

    /* renamed from: 麤  reason: contains not printable characters */
    final boolean f12323;

    /* renamed from: 齉  reason: contains not printable characters */
    final WeakReference<T> f12324;

    /* renamed from: 龘  reason: contains not printable characters */
    final Picasso f12325;

    static class RequestWeakReference<M> extends WeakReference<M> {

        /* renamed from: 龘  reason: contains not printable characters */
        final Action f12326;

        public RequestWeakReference(Action action, M m, ReferenceQueue<? super M> referenceQueue) {
            super(m, referenceQueue);
            this.f12326 = action;
        }
    }

    Action(Picasso picasso, T t, Request request, int i, int i2, int i3, Drawable drawable, String str, Object obj, boolean z) {
        this.f12325 = picasso;
        this.f12322 = request;
        this.f12324 = t == null ? null : new RequestWeakReference(this, t, picasso.f12374);
        this.f12321 = i;
        this.f12314 = i2;
        this.f12323 = z;
        this.f12315 = i3;
        this.f12316 = drawable;
        this.f12318 = str;
        this.f12319 = obj == null ? this : obj;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m15560() {
        return this.f12320;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Picasso m15561() {
        return this.f12325;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m15562() {
        return this.f12317;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m15563() {
        this.f12317 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m15564() {
        return this.f12318;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public T m15565() {
        if (this.f12324 == null) {
            return null;
        }
        return this.f12324.get();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m15566();

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m15567(Bitmap bitmap, Picasso.LoadedFrom loadedFrom);
}
