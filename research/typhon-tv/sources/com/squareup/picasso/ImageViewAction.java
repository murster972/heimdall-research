package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

class ImageViewAction extends Action<ImageView> {

    /* renamed from: ʾ  reason: contains not printable characters */
    Callback f12357;

    ImageViewAction(Picasso picasso, ImageView imageView, Request request, int i, int i2, int i3, Drawable drawable, String str, Object obj, Callback callback, boolean z) {
        super(picasso, imageView, request, i, i2, i3, drawable, str, obj, z);
        this.f12357 = callback;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m15597() {
        super.m15563();
        if (this.f12357 != null) {
            this.f12357 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15598() {
        ImageView imageView = (ImageView) this.f12324.get();
        if (imageView != null) {
            if (this.f12315 != 0) {
                imageView.setImageResource(this.f12315);
            } else if (this.f12316 != null) {
                imageView.setImageDrawable(this.f12316);
            }
            if (this.f12357 != null) {
                this.f12357.m15588();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15599(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap == null) {
            throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", new Object[]{this}));
        }
        ImageView imageView = (ImageView) this.f12324.get();
        if (imageView != null) {
            Bitmap bitmap2 = bitmap;
            Picasso.LoadedFrom loadedFrom2 = loadedFrom;
            PicassoDrawable.m15622(imageView, this.f12325.f12379, bitmap2, loadedFrom2, this.f12323, this.f12325.f12375);
            if (this.f12357 != null) {
                this.f12357.m15589();
            }
        }
    }
}
