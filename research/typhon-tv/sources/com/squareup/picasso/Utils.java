package com.squareup.picasso;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Looper;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

final class Utils {

    /* renamed from: 龘  reason: contains not printable characters */
    static final StringBuilder f12469 = new StringBuilder();

    @TargetApi(12)
    private static class BitmapHoneycombMR1 {
        /* renamed from: 龘  reason: contains not printable characters */
        static int m15672(Bitmap bitmap) {
            return bitmap.getByteCount();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static boolean m15659() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static byte[] m15660(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (-1 == read) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static boolean m15661(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[12];
        if (inputStream.read(bArr, 0, 12) == 12) {
            return "RIFF".equals(new String(bArr, 0, 4, "US-ASCII")) && "WEBP".equals(new String(bArr, 8, 4, "US-ASCII"));
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m15662(Bitmap bitmap) {
        int r0 = Build.VERSION.SDK_INT >= 12 ? BitmapHoneycombMR1.m15672(bitmap) : bitmap.getRowBytes() * bitmap.getHeight();
        if (r0 >= 0) {
            return r0;
        }
        throw new IllegalStateException("Negative size: " + bitmap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> T m15663(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m15664(BitmapHunter bitmapHunter) {
        return m15665(bitmapHunter, "");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m15665(BitmapHunter bitmapHunter, String str) {
        StringBuilder sb = new StringBuilder(str);
        Action r0 = bitmapHunter.m15577();
        if (r0 != null) {
            sb.append(r0.f12322.m15630());
        }
        List<Action> r1 = bitmapHunter.m15575();
        if (r1 != null) {
            int size = r1.size();
            for (int i = 0; i < size; i++) {
                if (i > 0 || r0 != null) {
                    sb.append(", ");
                }
                sb.append(r1.get(i).f12322.m15630());
            }
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m15666(Request request) {
        String r0 = m15667(request, f12469);
        f12469.setLength(0);
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m15667(Request request, StringBuilder sb) {
        if (request.f12392 != null) {
            sb.ensureCapacity(request.f12392.length() + 50);
            sb.append(request.f12392);
        } else if (request.f12405 != null) {
            String uri = request.f12405.toString();
            sb.ensureCapacity(uri.length() + 50);
            sb.append(uri);
        } else {
            sb.ensureCapacity(50);
            sb.append(request.f12403);
        }
        sb.append(10);
        if (request.f12395 != 0.0f) {
            sb.append("rotation:").append(request.f12395);
            if (request.f12409) {
                sb.append('@').append(request.f12396).append('x').append(request.f12408);
            }
            sb.append(10);
        }
        if (request.m15628()) {
            sb.append("resize:").append(request.f12394).append('x').append(request.f12400);
            sb.append(10);
        }
        if (request.f12401) {
            sb.append("centerCrop").append(10);
        } else if (request.f12402) {
            sb.append("centerInside").append(10);
        }
        if (request.f12393 != null) {
            int size = request.f12393.size();
            for (int i = 0; i < size; i++) {
                sb.append(request.f12393.get(i).m15658());
                sb.append(10);
            }
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m15668() {
        if (!m15659()) {
            throw new IllegalStateException("Method call should happen from the main thread.");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m15669(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m15670(String str, String str2, String str3) {
        m15671(str, str2, str3, "");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m15671(String str, String str2, String str3, String str4) {
        Log.d("Picasso", String.format("%1$-11s %2$-12s %3$s %4$s", new Object[]{str, str2, str3, str4}));
    }
}
