package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;

public abstract class RequestHandler {

    public static final class Result {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Bitmap f12438;

        /* renamed from: 麤  reason: contains not printable characters */
        private final int f12439;

        /* renamed from: 齉  reason: contains not printable characters */
        private final InputStream f12440;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Picasso.LoadedFrom f12441;

        public Result(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
            this((Bitmap) Utils.m15663(bitmap, "bitmap == null"), (InputStream) null, loadedFrom, 0);
        }

        Result(Bitmap bitmap, InputStream inputStream, Picasso.LoadedFrom loadedFrom, int i) {
            boolean z = true;
            if (!((inputStream == null ? false : z) ^ (bitmap != null))) {
                throw new AssertionError();
            }
            this.f12438 = bitmap;
            this.f12440 = inputStream;
            this.f12441 = (Picasso.LoadedFrom) Utils.m15663(loadedFrom, "loadedFrom == null");
            this.f12439 = i;
        }

        public Result(InputStream inputStream, Picasso.LoadedFrom loadedFrom) {
            this((Bitmap) null, (InputStream) Utils.m15663(inputStream, "stream == null"), loadedFrom, 0);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public InputStream m15645() {
            return this.f12440;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public int m15646() {
            return this.f12439;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Picasso.LoadedFrom m15647() {
            return this.f12441;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Bitmap m15648() {
            return this.f12438;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static BitmapFactory.Options m15640(Request request) {
        boolean r1 = request.m15628();
        boolean z = request.f12398 != null;
        BitmapFactory.Options options = null;
        if (r1 || z) {
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = r1;
            if (z) {
                options.inPreferredConfig = request.f12398;
            }
        }
        return options;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m15641(int i, int i2, int i3, int i4, BitmapFactory.Options options, Request request) {
        int i5 = 1;
        if (i4 > i2 || i3 > i) {
            if (i2 == 0) {
                i5 = (int) Math.floor((double) (((float) i3) / ((float) i)));
            } else if (i == 0) {
                i5 = (int) Math.floor((double) (((float) i4) / ((float) i2)));
            } else {
                int floor = (int) Math.floor((double) (((float) i4) / ((float) i2)));
                int floor2 = (int) Math.floor((double) (((float) i3) / ((float) i)));
                i5 = request.f12402 ? Math.max(floor, floor2) : Math.min(floor, floor2);
            }
        }
        options.inSampleSize = i5;
        options.inJustDecodeBounds = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m15642(int i, int i2, BitmapFactory.Options options, Request request) {
        m15641(i, i2, options.outWidth, options.outHeight, options, request);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m15643(BitmapFactory.Options options) {
        return options != null && options.inJustDecodeBounds;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Result m15644(Request request, int i) throws IOException;
}
