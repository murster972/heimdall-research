package com.squareup.picasso;

import android.graphics.Bitmap;
import android.net.Uri;
import com.squareup.picasso.Picasso;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class Request {

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final long f12391 = TimeUnit.SECONDS.toNanos(5);

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String f12392;

    /* renamed from: ʼ  reason: contains not printable characters */
    public final List<Transformation> f12393;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final int f12394;

    /* renamed from: ʾ  reason: contains not printable characters */
    public final float f12395;

    /* renamed from: ʿ  reason: contains not printable characters */
    public final float f12396;

    /* renamed from: ˈ  reason: contains not printable characters */
    public final boolean f12397;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final Bitmap.Config f12398;

    /* renamed from: ˋ  reason: contains not printable characters */
    public final Picasso.Priority f12399;

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int f12400;

    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean f12401;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean f12402;

    /* renamed from: 连任  reason: contains not printable characters */
    public final int f12403;

    /* renamed from: 靐  reason: contains not printable characters */
    long f12404;

    /* renamed from: 麤  reason: contains not printable characters */
    public final Uri f12405;

    /* renamed from: 齉  reason: contains not printable characters */
    int f12406;

    /* renamed from: 龘  reason: contains not printable characters */
    int f12407;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final float f12408;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final boolean f12409;

    public static final class Builder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f12410;

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f12411;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f12412;

        /* renamed from: ʾ  reason: contains not printable characters */
        private List<Transformation> f12413;

        /* renamed from: ʿ  reason: contains not printable characters */
        private Bitmap.Config f12414;

        /* renamed from: ˈ  reason: contains not printable characters */
        private boolean f12415;

        /* renamed from: ˑ  reason: contains not printable characters */
        private float f12416;

        /* renamed from: ٴ  reason: contains not printable characters */
        private float f12417;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private float f12418;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f12419;

        /* renamed from: 靐  reason: contains not printable characters */
        private int f12420;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f12421;

        /* renamed from: 齉  reason: contains not printable characters */
        private String f12422;

        /* renamed from: 龘  reason: contains not printable characters */
        private Uri f12423;

        /* renamed from: ﹶ  reason: contains not printable characters */
        private Picasso.Priority f12424;

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m15631() {
            return (this.f12421 == 0 && this.f12419 == 0) ? false : true;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Request m15632() {
            if (this.f12411 && this.f12410) {
                throw new IllegalStateException("Center crop and center inside can not be used together.");
            } else if (this.f12410 && this.f12421 == 0 && this.f12419 == 0) {
                throw new IllegalStateException("Center crop requires calling resize with positive width and height.");
            } else if (this.f12411 && this.f12421 == 0 && this.f12419 == 0) {
                throw new IllegalStateException("Center inside requires calling resize with positive width and height.");
            } else {
                if (this.f12424 == null) {
                    this.f12424 = Picasso.Priority.NORMAL;
                }
                return new Request(this.f12423, this.f12420, this.f12422, this.f12413, this.f12421, this.f12419, this.f12410, this.f12411, this.f12412, this.f12416, this.f12417, this.f12418, this.f12415, this.f12414, this.f12424);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m15633(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Width must be positive number or 0.");
            } else if (i2 < 0) {
                throw new IllegalArgumentException("Height must be positive number or 0.");
            } else if (i2 == 0 && i == 0) {
                throw new IllegalArgumentException("At least one dimension has to be positive number.");
            } else {
                this.f12421 = i;
                this.f12419 = i2;
                return this;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m15634() {
            return (this.f12423 == null && this.f12420 == 0) ? false : true;
        }
    }

    private Request(Uri uri, int i, String str, List<Transformation> list, int i2, int i3, boolean z, boolean z2, boolean z3, float f, float f2, float f3, boolean z4, Bitmap.Config config, Picasso.Priority priority) {
        this.f12405 = uri;
        this.f12403 = i;
        this.f12392 = str;
        if (list == null) {
            this.f12393 = null;
        } else {
            this.f12393 = Collections.unmodifiableList(list);
        }
        this.f12394 = i2;
        this.f12400 = i3;
        this.f12401 = z;
        this.f12402 = z2;
        this.f12397 = z3;
        this.f12395 = f;
        this.f12396 = f2;
        this.f12408 = f3;
        this.f12409 = z4;
        this.f12398 = config;
        this.f12399 = priority;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Request{");
        if (this.f12403 > 0) {
            sb.append(this.f12403);
        } else {
            sb.append(this.f12405);
        }
        if (this.f12393 != null && !this.f12393.isEmpty()) {
            for (Transformation r1 : this.f12393) {
                sb.append(' ').append(r1.m15658());
            }
        }
        if (this.f12392 != null) {
            sb.append(" stableKey(").append(this.f12392).append(')');
        }
        if (this.f12394 > 0) {
            sb.append(" resize(").append(this.f12394).append(',').append(this.f12400).append(')');
        }
        if (this.f12401) {
            sb.append(" centerCrop");
        }
        if (this.f12402) {
            sb.append(" centerInside");
        }
        if (this.f12395 != 0.0f) {
            sb.append(" rotation(").append(this.f12395);
            if (this.f12409) {
                sb.append(" @ ").append(this.f12396).append(',').append(this.f12408);
            }
            sb.append(')');
        }
        if (this.f12398 != null) {
            sb.append(' ').append(this.f12398);
        }
        sb.append('}');
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m15624() {
        return m15628() || this.f12395 != 0.0f;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m15625() {
        return this.f12393 != null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m15626() {
        return m15624() || m15625();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m15627() {
        return "[R" + this.f12407 + ']';
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m15628() {
        return (this.f12394 == 0 && this.f12400 == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m15629() {
        return this.f12405 != null ? String.valueOf(this.f12405.getPath()) : Integer.toHexString(this.f12403);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m15630() {
        long nanoTime = System.nanoTime() - this.f12404;
        return nanoTime > f12391 ? m15627() + '+' + TimeUnit.NANOSECONDS.toSeconds(nanoTime) + 's' : m15627() + '+' + TimeUnit.NANOSECONDS.toMillis(nanoTime) + "ms";
    }
}
