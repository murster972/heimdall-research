package com.squareup.picasso;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

final class MarkableInputStream extends InputStream {

    /* renamed from: 连任  reason: contains not printable characters */
    private long f12358;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f12359;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f12360;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f12361;

    /* renamed from: 龘  reason: contains not printable characters */
    private final InputStream f12362;

    public MarkableInputStream(InputStream inputStream) {
        this(inputStream, 4096);
    }

    public MarkableInputStream(InputStream inputStream, int i) {
        this.f12358 = -1;
        this.f12362 = !inputStream.markSupported() ? new BufferedInputStream(inputStream, i) : inputStream;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m15600(long j) {
        try {
            if (this.f12361 >= this.f12359 || this.f12359 > this.f12360) {
                this.f12361 = this.f12359;
                this.f12362.mark((int) (j - this.f12359));
            } else {
                this.f12362.reset();
                this.f12362.mark((int) (j - this.f12361));
                m15601(this.f12361, this.f12359);
            }
            this.f12360 = j;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to mark: " + e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15601(long j, long j2) throws IOException {
        while (j < j2) {
            long skip = this.f12362.skip(j2 - j);
            if (skip == 0) {
                if (read() != -1) {
                    skip = 1;
                } else {
                    return;
                }
            }
            j += skip;
        }
    }

    public int available() throws IOException {
        return this.f12362.available();
    }

    public void close() throws IOException {
        this.f12362.close();
    }

    public void mark(int i) {
        this.f12358 = m15602(i);
    }

    public boolean markSupported() {
        return this.f12362.markSupported();
    }

    public int read() throws IOException {
        int read = this.f12362.read();
        if (read != -1) {
            this.f12359++;
        }
        return read;
    }

    public int read(byte[] bArr) throws IOException {
        int read = this.f12362.read(bArr);
        if (read != -1) {
            this.f12359 += (long) read;
        }
        return read;
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        int read = this.f12362.read(bArr, i, i2);
        if (read != -1) {
            this.f12359 += (long) read;
        }
        return read;
    }

    public void reset() throws IOException {
        m15603(this.f12358);
    }

    public long skip(long j) throws IOException {
        long skip = this.f12362.skip(j);
        this.f12359 += skip;
        return skip;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m15602(int i) {
        long j = this.f12359 + ((long) i);
        if (this.f12360 < j) {
            m15600(j);
        }
        return this.f12359;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15603(long j) throws IOException {
        if (this.f12359 > this.f12360 || j < this.f12361) {
            throw new IOException("Cannot reset");
        }
        this.f12362.reset();
        m15601(this.f12361, j);
        this.f12359 = j;
    }
}
