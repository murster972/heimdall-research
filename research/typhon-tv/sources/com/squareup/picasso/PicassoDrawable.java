package com.squareup.picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.SystemClock;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

final class PicassoDrawable extends BitmapDrawable {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Paint f12383 = new Paint();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final boolean f12384;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final float f12385;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Picasso.LoadedFrom f12386;

    /* renamed from: 靐  reason: contains not printable characters */
    long f12387;

    /* renamed from: 麤  reason: contains not printable characters */
    int f12388 = 255;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean f12389;

    /* renamed from: 龘  reason: contains not printable characters */
    Drawable f12390;

    PicassoDrawable(Context context, Bitmap bitmap, Drawable drawable, Picasso.LoadedFrom loadedFrom, boolean z, boolean z2) {
        super(context.getResources(), bitmap);
        this.f12384 = z2;
        this.f12385 = context.getResources().getDisplayMetrics().density;
        this.f12386 = loadedFrom;
        if (loadedFrom != Picasso.LoadedFrom.MEMORY && !z) {
            this.f12390 = drawable;
            this.f12389 = true;
            this.f12387 = SystemClock.uptimeMillis();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Path m15620(Point point, int i) {
        Point point2 = new Point(point.x + i, point.y);
        Point point3 = new Point(point.x, point.y + i);
        Path path = new Path();
        path.moveTo((float) point.x, (float) point.y);
        path.lineTo((float) point2.x, (float) point2.y);
        path.lineTo((float) point3.x, (float) point3.y);
        return path;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15621(Canvas canvas) {
        f12383.setColor(-1);
        canvas.drawPath(m15620(new Point(0, 0), (int) (16.0f * this.f12385)), f12383);
        f12383.setColor(this.f12386.debugColor);
        canvas.drawPath(m15620(new Point(0, 0), (int) (15.0f * this.f12385)), f12383);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m15622(ImageView imageView, Context context, Bitmap bitmap, Picasso.LoadedFrom loadedFrom, boolean z, boolean z2) {
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof AnimationDrawable) {
            ((AnimationDrawable) drawable).stop();
        }
        imageView.setImageDrawable(new PicassoDrawable(context, bitmap, drawable, loadedFrom, z, z2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m15623(ImageView imageView, Drawable drawable) {
        imageView.setImageDrawable(drawable);
        if (imageView.getDrawable() instanceof AnimationDrawable) {
            ((AnimationDrawable) imageView.getDrawable()).start();
        }
    }

    public void draw(Canvas canvas) {
        if (!this.f12389) {
            super.draw(canvas);
        } else {
            float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.f12387)) / 200.0f;
            if (uptimeMillis >= 1.0f) {
                this.f12389 = false;
                this.f12390 = null;
                super.draw(canvas);
            } else {
                if (this.f12390 != null) {
                    this.f12390.draw(canvas);
                }
                super.setAlpha((int) (((float) this.f12388) * uptimeMillis));
                super.draw(canvas);
                super.setAlpha(this.f12388);
                if (Build.VERSION.SDK_INT <= 10) {
                    invalidateSelf();
                }
            }
        }
        if (this.f12384) {
            m15621(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f12390 != null) {
            this.f12390.setBounds(rect);
        }
        super.onBoundsChange(rect);
    }

    public void setAlpha(int i) {
        this.f12388 = i;
        if (this.f12390 != null) {
            this.f12390.setAlpha(i);
        }
        super.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f12390 != null) {
            this.f12390.setColorFilter(colorFilter);
        }
        super.setColorFilter(colorFilter);
    }
}
