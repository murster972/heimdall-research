package com.squareup.picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.internal.view.SupportMenu;
import android.widget.ImageView;
import io.presage.ads.NewAd;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Map;

public class Picasso {

    /* renamed from: 靐  reason: contains not printable characters */
    static volatile Picasso f12367 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    static final Handler f12368 = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 3:
                    Action action = (Action) message.obj;
                    if (action.m15561().f12376) {
                        Utils.m15671("Main", "canceled", action.f12322.m15630(), "target got garbage collected");
                    }
                    action.f12325.m15608(action.m15565());
                    return;
                case 8:
                    List list = (List) message.obj;
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        BitmapHunter bitmapHunter = (BitmapHunter) list.get(i);
                        bitmapHunter.f12344.m15616(bitmapHunter);
                    }
                    return;
                case 13:
                    List list2 = (List) message.obj;
                    int size2 = list2.size();
                    for (int i2 = 0; i2 < size2; i2++) {
                        Action action2 = (Action) list2.get(i2);
                        action2.f12325.m15610(action2);
                    }
                    return;
                default:
                    throw new AssertionError("Unknown handler message received: " + message.what);
            }
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    final Stats f12369;

    /* renamed from: ʼ  reason: contains not printable characters */
    final Map<Object, Action> f12370;

    /* renamed from: ʽ  reason: contains not printable characters */
    final Map<ImageView, DeferredRequestCreator> f12371;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final RequestTransformer f12372;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Listener f12373;

    /* renamed from: ˑ  reason: contains not printable characters */
    final ReferenceQueue<Object> f12374;

    /* renamed from: ٴ  reason: contains not printable characters */
    boolean f12375;

    /* renamed from: ᐧ  reason: contains not printable characters */
    volatile boolean f12376;

    /* renamed from: 连任  reason: contains not printable characters */
    final Cache f12377;

    /* renamed from: 麤  reason: contains not printable characters */
    final Dispatcher f12378;

    /* renamed from: 齉  reason: contains not printable characters */
    final Context f12379;

    public interface Listener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15617(Picasso picasso, Uri uri, Exception exc);
    }

    public enum LoadedFrom {
        MEMORY(-16711936),
        DISK(-16776961),
        NETWORK(SupportMenu.CATEGORY_MASK);
        
        final int debugColor;

        private LoadedFrom(int i) {
            this.debugColor = i;
        }
    }

    public enum Priority {
        LOW,
        NORMAL,
        HIGH
    }

    public interface RequestTransformer {

        /* renamed from: 龘  reason: contains not printable characters */
        public static final RequestTransformer f12382 = new RequestTransformer() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Request m15619(Request request) {
                return request;
            }
        };

        /* renamed from: 龘  reason: contains not printable characters */
        Request m15618(Request request);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15606(Bitmap bitmap, LoadedFrom loadedFrom, Action action) {
        if (!action.m15562()) {
            if (!action.m15560()) {
                this.f12370.remove(action.m15565());
            }
            if (bitmap == null) {
                action.m15566();
                if (this.f12376) {
                    Utils.m15670("Main", "errored", action.f12322.m15630());
                }
            } else if (loadedFrom == null) {
                throw new AssertionError("LoadedFrom cannot be null.");
            } else {
                action.m15567(bitmap, loadedFrom);
                if (this.f12376) {
                    Utils.m15671("Main", NewAd.EVENT_COMPLETED, action.f12322.m15630(), "from " + loadedFrom);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15608(Object obj) {
        DeferredRequestCreator remove;
        Utils.m15668();
        Action remove2 = this.f12370.remove(obj);
        if (remove2 != null) {
            remove2.m15563();
            this.f12378.m15591(remove2);
        }
        if ((obj instanceof ImageView) && (remove = this.f12371.remove((ImageView) obj)) != null) {
            remove.m15590();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m15609(Action action) {
        this.f12378.m15594(action);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m15610(Action action) {
        Bitmap bitmap = null;
        if (MemoryPolicy.m15604(action.f12321)) {
            bitmap = m15611(action.m15564());
        }
        if (bitmap != null) {
            m15606(bitmap, LoadedFrom.MEMORY, action);
            if (this.f12376) {
                Utils.m15671("Main", NewAd.EVENT_COMPLETED, action.f12322.m15630(), "from " + LoadedFrom.MEMORY);
                return;
            }
            return;
        }
        m15615(action);
        if (this.f12376) {
            Utils.m15670("Main", "resumed", action.f12322.m15630());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Bitmap m15611(String str) {
        Bitmap r0 = this.f12377.m15584(str);
        if (r0 != null) {
            this.f12369.m15653();
        } else {
            this.f12369.m15650();
        }
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Request m15612(Request request) {
        Request r0 = this.f12372.m15618(request);
        if (r0 != null) {
            return r0;
        }
        throw new IllegalStateException("Request transformer " + this.f12372.getClass().getCanonicalName() + " returned null for " + request);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15613(ImageView imageView) {
        m15608((Object) imageView);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15614(ImageView imageView, DeferredRequestCreator deferredRequestCreator) {
        this.f12371.put(imageView, deferredRequestCreator);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15615(Action action) {
        Object r0 = action.m15565();
        if (!(r0 == null || this.f12370.get(r0) == action)) {
            m15608(r0);
            this.f12370.put(r0, action);
        }
        m15609(action);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15616(BitmapHunter bitmapHunter) {
        boolean z = false;
        Action r9 = bitmapHunter.m15577();
        List<Action> r5 = bitmapHunter.m15575();
        boolean z2 = r5 != null && !r5.isEmpty();
        if (r9 != null || z2) {
            z = true;
        }
        if (z) {
            Uri uri = bitmapHunter.m15578().f12405;
            Exception r0 = bitmapHunter.m15573();
            Bitmap r7 = bitmapHunter.m15576();
            LoadedFrom r1 = bitmapHunter.m15574();
            if (r9 != null) {
                m15606(r7, r1, r9);
            }
            if (z2) {
                int size = r5.size();
                for (int i = 0; i < size; i++) {
                    m15606(r7, r1, r5.get(i));
                }
            }
            if (this.f12373 != null && r0 != null) {
                this.f12373.m15617(this, uri, r0);
            }
        }
    }
}
