package com.squareup.picasso;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

class DeferredRequestCreator implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: 靐  reason: contains not printable characters */
    final WeakReference<ImageView> f12353;

    /* renamed from: 齉  reason: contains not printable characters */
    Callback f12354;

    /* renamed from: 龘  reason: contains not printable characters */
    final RequestCreator f12355;

    DeferredRequestCreator(RequestCreator requestCreator, ImageView imageView, Callback callback) {
        this.f12355 = requestCreator;
        this.f12353 = new WeakReference<>(imageView);
        this.f12354 = callback;
        imageView.getViewTreeObserver().addOnPreDrawListener(this);
    }

    public boolean onPreDraw() {
        ImageView imageView = (ImageView) this.f12353.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                if (width > 0 && height > 0) {
                    viewTreeObserver.removeOnPreDrawListener(this);
                    this.f12355.m15637().m15638(width, height).m15639(imageView, this.f12354);
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15590() {
        this.f12354 = null;
        ImageView imageView = (ImageView) this.f12353.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this);
            }
        }
    }
}
