package com.squareup.duktape;

import android.support.annotation.Keep;
import java.io.Closeable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.LinkedHashMap;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public final class Duktape implements Closeable {
    /* access modifiers changed from: private */
    public long context;

    static {
        System.loadLibrary("duktape");
    }

    private Duktape(long j) {
        this.context = j;
    }

    /* access modifiers changed from: private */
    public static native Object call(long j, long j2, Object obj, Object[] objArr);

    public static Duktape create() {
        long createContext = createContext();
        if (createContext != 0) {
            return new Duktape(createContext);
        }
        throw new OutOfMemoryError("Cannot create Duktape instance");
    }

    private static native long createContext();

    private static native void destroyContext(long j);

    private static native Object evaluate(long j, String str, String str2);

    private static native long get(long j, String str, Object[] objArr);

    @Keep
    private static int getLocalTimeZoneOffset(double d) {
        return (int) TimeUnit.MILLISECONDS.toSeconds((long) TimeZone.getDefault().getOffset((long) d));
    }

    private static native void set(long j, String str, Object obj, Object[] objArr);

    public synchronized void close() {
        if (this.context != 0) {
            long j = this.context;
            this.context = 0;
            destroyContext(j);
        }
    }

    public synchronized Object evaluate(String str) {
        return evaluate(this.context, str, "?");
    }

    public synchronized Object evaluate(String str, String str2) {
        return evaluate(this.context, str, str2);
    }

    /* access modifiers changed from: protected */
    public synchronized void finalize() throws Throwable {
        if (this.context != 0) {
            Logger.getLogger(getClass().getName()).warning("Duktape instance leaked!");
        }
    }

    public synchronized <T> T get(String str, Class<T> cls) {
        T newProxyInstance;
        synchronized (this) {
            if (!cls.isInterface()) {
                throw new UnsupportedOperationException("Only interfaces can be proxied. Received: " + cls);
            } else if (cls.getInterfaces().length > 0) {
                throw new UnsupportedOperationException(cls + " must not extend other interfaces");
            } else {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Method method : cls.getMethods()) {
                    if (linkedHashMap.put(method.getName(), method) != null) {
                        throw new UnsupportedOperationException(method.getName() + " is overloaded in " + cls);
                    }
                }
                final long j = get(this.context, str, linkedHashMap.values().toArray());
                final String str2 = str;
                final Class<T> cls2 = cls;
                newProxyInstance = Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new InvocationHandler() {
                    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
                        Object access$100;
                        if (method.getDeclaringClass() == Object.class) {
                            return method.invoke(this, objArr);
                        }
                        synchronized (this) {
                            access$100 = Duktape.call(this.context, j, method, objArr);
                        }
                        return access$100;
                    }

                    public String toString() {
                        return String.format("DuktapeProxy{name=%s, type=%s}", new Object[]{str2, cls2.getName()});
                    }
                });
            }
        }
        return newProxyInstance;
    }

    public synchronized <T> void set(String str, Class<T> cls, T t) {
        if (!cls.isInterface()) {
            throw new UnsupportedOperationException("Only interfaces can be bound. Received: " + cls);
        } else if (cls.getInterfaces().length > 0) {
            throw new UnsupportedOperationException(cls + " must not extend other interfaces");
        } else if (!cls.isInstance(t)) {
            throw new IllegalArgumentException(t.getClass() + " is not an instance of " + cls);
        } else {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Method method : cls.getMethods()) {
                if (linkedHashMap.put(method.getName(), method) != null) {
                    throw new UnsupportedOperationException(method.getName() + " is overloaded in " + cls);
                }
            }
            set(this.context, str, t, linkedHashMap.values().toArray());
        }
    }
}
