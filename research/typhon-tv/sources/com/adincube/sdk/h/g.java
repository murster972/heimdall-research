package com.adincube.sdk.h;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.util.b.j;
import java.util.Locale;

public final class g {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f2749 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private a f2750;

    /* renamed from: 靐  reason: contains not printable characters */
    e f2751;

    /* renamed from: 麤  reason: contains not printable characters */
    boolean f2752 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    WebView f2753;

    /* renamed from: 龘  reason: contains not printable characters */
    Context f2754;

    public g(Context context, e eVar, WebView webView, a aVar) {
        this.f2754 = context;
        this.f2751 = eVar;
        this.f2753 = webView;
        this.f2750 = aVar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static boolean m3280() {
        return Build.VERSION.SDK_INT >= 26;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3281() {
        if (m3280() && this.f2753.getMeasuredHeight() != 0 && this.f2753.getMeasuredWidth() != 0 && this.f2752 && !this.f2749) {
            this.f2749 = true;
            String str = "device-width";
            if (this.f2751.f2587 != null) {
                str = this.f2751.f2587.toString();
            }
            double d = 1.0d;
            if (!(this.f2751.f2587 == null && this.f2751.f2586 == null)) {
                int r1 = j.m3542(this.f2754, this.f2751.f2587.intValue());
                int r4 = j.m3542(this.f2754, this.f2751.f2586.intValue());
                if (r1 > this.f2753.getMeasuredWidth() || r4 > this.f2753.getMeasuredHeight()) {
                    d = Math.min(((double) this.f2753.getMeasuredWidth()) / ((double) r1), ((double) this.f2753.getMeasuredHeight()) / ((double) r4));
                }
            }
            this.f2750.m3255("(function() {" + "var hasViewportMetaTag = document.querySelector('meta[name=\"viewport\"]');" + "if (hasViewportMetaTag) return;" + "var metaTag = document.createElement('meta');" + "metaTag.name = 'viewport';" + String.format(Locale.US, "metaTag.content = 'width=%s, initial-scale=%.2f, maximum-scale=%.2f, user-scalable=0';", new Object[]{str, Double.valueOf(d), Double.valueOf(d)}) + "document.head.appendChild(metaTag);" + "})();");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3282(int i, int i2) {
        if (this.f2751.f2586 != null && this.f2751.f2587 != null) {
            DisplayMetrics displayMetrics = this.f2754.getResources().getDisplayMetrics();
            this.f2753.setInitialScale((int) Math.round(Math.min(((double) this.f2753.getWidth()) / ((double) j.m3542(this.f2754, i)), ((double) this.f2753.getHeight()) / ((double) j.m3542(this.f2754, i2))) * ((double) displayMetrics.density) * 100.0d));
        }
    }
}
