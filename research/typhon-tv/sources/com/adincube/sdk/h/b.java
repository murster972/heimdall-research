package com.adincube.sdk.h;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import com.adincube.sdk.e.a;
import com.adincube.sdk.mediation.v.f;
import com.adincube.sdk.util.ErrorReportingHelper;

public final class b extends CountDownTimer {

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean f2715 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private a f2716;

    /* renamed from: 齉  reason: contains not printable characters */
    private f f2717;

    /* renamed from: 龘  reason: contains not printable characters */
    e f2718;

    private b(f fVar, a aVar, e eVar) {
        super(fVar.靐.longValue(), 500);
        this.f2717 = fVar;
        this.f2716 = aVar;
        this.f2718 = eVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m3258(f fVar, a aVar, e eVar) {
        return new b(fVar, aVar, eVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3259() {
        if (!this.f2715) {
            this.f2715 = true;
            this.f2716.m2605(true);
        }
    }

    public final void onFinish() {
        try {
            m3259();
        } catch (Throwable th) {
            ErrorReportingHelper.m3506("MRAIDCloseTimer.onFinish()", com.adincube.sdk.g.c.b.INTERSTITIAL, th);
            com.adincube.sdk.util.a.m3513("MRAIDCloseTimer.onFinish()", th);
        }
    }

    public final void onTick(long j) {
        try {
            if (this.f2717.齉 != null && this.f2717.靐.longValue() - j >= this.f2717.齉.longValue() && this.f2718.m3279(true)) {
                m3259();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            b.this.cancel();
                        } catch (Throwable th) {
                            ErrorReportingHelper.m3506("MRAIDCloseTimer.stopTimer()", com.adincube.sdk.g.c.b.INTERSTITIAL, th);
                            com.adincube.sdk.util.a.m3513("MRAIDCloseTimer.stopTimer()", th);
                        }
                    }
                });
            }
        } catch (Throwable th) {
            ErrorReportingHelper.m3506("MRAIDCloseTimer.onTick()", com.adincube.sdk.g.c.b.INTERSTITIAL, th);
            com.adincube.sdk.util.a.m3513("MRAIDCloseTimer.onFinish()", th);
        }
    }
}
