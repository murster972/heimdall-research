package com.adincube.sdk.h;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.mediation.v.f;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.b.j;
import com.adincube.sdk.util.n;
import com.adincube.sdk.util.u;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public final class c {

    /* renamed from: ʻ  reason: contains not printable characters */
    g f2720 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    a f2721 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f2722 = true;

    /* renamed from: ʾ  reason: contains not printable characters */
    public d f2723 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    private h f2724 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    public C0015c f2725 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f2726 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    String f2727 = "none";

    /* renamed from: ٴ  reason: contains not printable characters */
    public Point f2728 = new Point(0, 0);

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean f2729 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    public WebView f2730 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    Context f2731;

    /* renamed from: 麤  reason: contains not printable characters */
    f f2732;

    /* renamed from: 齉  reason: contains not printable characters */
    e f2733;

    /* renamed from: 龘  reason: contains not printable characters */
    List<String> f2734 = null;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private f f2735 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f2736 = false;

    private class a extends WebChromeClient {
        private a() {
        }

        /* synthetic */ a(c cVar, byte b) {
            this();
        }

        @SuppressLint({"NewApi"})
        public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            return true;
        }
    }

    private class b extends WebViewClient {
        private b() {
        }

        /* synthetic */ b(c cVar, byte b) {
            this();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private static WebResourceResponse m3270() {
            byte[] bArr = new byte[0];
            try {
                bArr = com.adincube.sdk.util.e.m3574("InVzZSBzdHJpY3QiO3dpbmRvdy5fbXJhaWQ9KGZ1bmN0aW9uKCl7dmFyIG1yYWlkU3RhdGVzPVsibG9hZGluZyIsImRlZmF1bHQiLCJleHBhbmRlZCIsInJlc2l6ZWQiLCJoaWRkZW4iXTt2YXIgbXJhaWRFdmVudHM9WyJyZWFkeSIsImVycm9yIiwic3RhdGVDaGFuZ2UiLCJ2aWV3YWJsZUNoYW5nZSIsInNpemVDaGFuZ2UiXTt2YXIgc3RhdGU9ImxvYWRpbmciO3ZhciB2aXNpYmxlPWZhbHNlO3ZhciBzY3JlZW5TaXplPXt9O3ZhciBtYXhTaXplPXt9O3ZhciBzdXBwb3J0cz17c21zOmZhbHNlLHRlbDpmYWxzZSxjYWxlbmRhcjpmYWxzZSxzdG9yZVBpY3R1cmU6ZmFsc2UsaW5saW5lVmlkZW86ZmFsc2V9O3ZhciBvcmllbnRhdGlvblByb3BlcnRpZXM9e2FsbG93T3JpZW50YXRpb25DaGFuZ2U6dHJ1ZSxmb3JjZU9yaWVudGF0aW9uOiJub25lIn07dmFyIHBsYWNlbWVudFR5cGU9bnVsbDt2YXIgZXZlbnRDYWxsYmFja3M9e307Zm9yKHZhciBpPTA7aTxtcmFpZEV2ZW50cy5sZW5ndGg7aSsrKXtldmVudENhbGxiYWNrc1ttcmFpZEV2ZW50c1tpXV09W119dmFyIHNlbGY9e19maXJlRXZlbnQ6ZnVuY3Rpb24oZXZlbnQpe2lmKG1yYWlkRXZlbnRzLmluZGV4T2YoZXZlbnQpPT0tMSl7cmV0dXJufXZhciBhcmdzPVtdO2Zvcih2YXIgaj0xO2o8YXJndW1lbnRzLmxlbmd0aDtqKyspe2FyZ3MucHVzaChhcmd1bWVudHNbal0pfXZhciBmdW5jdGlvbnM9ZXZlbnRDYWxsYmFja3NbZXZlbnRdO2Zvcih2YXIgaT0wO2k8ZnVuY3Rpb25zLmxlbmd0aDtpKyspe2Z1bmN0aW9uc1tpXS5hcHBseSh1bmRlZmluZWQsYXJncyl9fSxfY2FsbEFjdGlvbjpmdW5jdGlvbihhY3Rpb24scGFyYW1zKXt2YXIgdXJsPSJtcmFpZDovL2FkaW5jdWJlLyIrYWN0aW9uO2lmKHBhcmFtcyE9dW5kZWZpbmVkKXt2YXIgcXVlcnlQYXJhbWV0ZXJzPSIiO2Zvcih2YXIgcGFyYW1OYW1lIGluIHBhcmFtcyl7aWYocXVlcnlQYXJhbWV0ZXJzLmxlbmd0aD09MCl7cXVlcnlQYXJhbWV0ZXJzKz0iPyJ9ZWxzZXtxdWVyeVBhcmFtZXRlcnMrPSImIn1xdWVyeVBhcmFtZXRlcnMrPXBhcmFtTmFtZSsiPSIrZW5jb2RlVVJJQ29tcG9uZW50KHBhcmFtc1twYXJhbU5hbWVdKX11cmwrPXF1ZXJ5UGFyYW1ldGVyc312YXIgaWZyYW1lPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoImlmcmFtZSIpO2lmcmFtZS5zZXRBdHRyaWJ1dGUoInNyYyIsdXJsKTtpZnJhbWUuc2V0QXR0cmlidXRlKCJzdHlsZSIsImRpc3BsYXk6IG5vbmU7Iik7aWYoZG9jdW1lbnQuYm9keSE9bnVsbCl7ZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChpZnJhbWUpfWVsc2V7ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigiRE9NQ29udGVudExvYWRlZCIsZnVuY3Rpb24oKXtkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGlmcmFtZSl9LGZhbHNlKX19LF9zZXRTdXBwb3J0Rm9yOmZ1bmN0aW9uKGZlYXR1cmUsdmFsdWUpe2lmKHN1cHBvcnRzW2ZlYXR1cmVdPT11bmRlZmluZWQpe3JldHVybn1zdXBwb3J0c1tmZWF0dXJlXT0odmFsdWU9PSJ0cnVlIil9LF9zdXBwb3J0czpmdW5jdGlvbihmZWF0dXJlKXtpZihzdXBwb3J0c1tmZWF0dXJlXT09dW5kZWZpbmVkKXtyZXR1cm4gZmFsc2V9cmV0dXJuIHN1cHBvcnRzW2ZlYXR1cmVdfSxfc2V0U3RhdGU6ZnVuY3Rpb24obmV3U3RhdGUpe3N0YXRlPW5ld1N0YXRlfSxfZ2V0U3RhdGU6ZnVuY3Rpb24oKXtyZXR1cm4gc3RhdGV9LF9zZXRWaXNpYmxlOmZ1bmN0aW9uKHYpe3Zpc2libGU9KHY9PSJ0cnVlIil9LF9pc1ZpZXdhYmxlOmZ1bmN0aW9uKCl7cmV0dXJuKHZpc2libGU9PXRydWUpfSxfc2V0TWF4U2l6ZTpmdW5jdGlvbih3aWR0aCxoZWlnaHQpe21heFNpemU9e3dpZHRoOnBhcnNlSW50KHdpZHRoKSxoZWlnaHQ6cGFyc2VJbnQoaGVpZ2h0KX19LF9nZXRNYXhTaXplOmZ1bmN0aW9uKCl7cmV0dXJuIG1heFNpemV9LF9zZXRTY3JlZW5TaXplOmZ1bmN0aW9uKHdpZHRoLGhlaWdodCl7c2NyZWVuU2l6ZT17d2lkdGg6cGFyc2VJbnQod2lkdGgpLGhlaWdodDpwYXJzZUludChoZWlnaHQpfX0sX2dldFNjcmVlblNpemU6ZnVuY3Rpb24oKXtyZXR1cm4gc2NyZWVuU2l6ZX0sX3NldE9yaWVudGF0aW9uUHJvcGVydGllczpmdW5jdGlvbihhbGxvd09yaWVudGF0aW9uQ2hhbmdlLGZvcmNlT3JpZW50YXRpb24pe3NlbGYuX2NhbGxBY3Rpb24oInNldE9yaWVudGF0aW9uUHJvcGVydGllcyIse2FsbG93T3JpZW50YXRpb25DaGFuZ2U6YWxsb3dPcmllbnRhdGlvbkNoYW5nZXx8dHJ1ZSxmb3JjZU9yaWVudGF0aW9uOmZvcmNlT3JpZW50YXRpb258fCJub25lIn0pfSxfZ2V0T3JpZW50YXRpb25Qcm9wZXJ0aWVzOmZ1bmN0aW9uKCl7fSxfc2V0UGxhY2VtZW50VHlwZTpmdW5jdGlvbihwdCl7cGxhY2VtZW50VHlwZT1wdH0sX2dldFBsYWNlbWVudFR5cGU6ZnVuY3Rpb24oKXtyZXR1cm4gcGxhY2VtZW50VHlwZX0sX29wZW46ZnVuY3Rpb24odXJsKXtpZih1cmw9PXVuZGVmaW5lZCl7c2VsZi5fZmlyZUV2ZW50KCJlcnJvciIsInVybCBwYXJhbWF0ZXIgaXMgbWFuZGF0b3J5Iiwib3BlbiIpO3JldHVybn1zZWxmLl9jYWxsQWN0aW9uKCJvcGVuIix7dXJsOnVybH0pfSxfcGxheVZpZGVvOmZ1bmN0aW9uKHVyaSl7aWYodXJpPT11bmRlZmluZWQpe3NlbGYuX2ZpcmVFdmVudCgiZXJyb3IiLCJ1cmkgcGFyYW1hdGVyIGlzIG1hbmRhdG9yeSIsIm9wZW4iKTtyZXR1cm59c2VsZi5fY2FsbEFjdGlvbigicGxheVZpZGVvIix7dXJpOnVyaX0pfSxfY2xvc2U6ZnVuY3Rpb24oKXtzZWxmLl9jYWxsQWN0aW9uKCJjbG9zZSIpfSxfYWRkRXZlbnRMaXN0ZW5lcjpmdW5jdGlvbihldmVudCxjYWxsYmFjayl7aWYobXJhaWRFdmVudHMuaW5kZXhPZihldmVudCk9PS0xKXtzZWxmLl9maXJlRXZlbnQoImVycm9yIixldmVudCsiIGlzIG5vdCBhIHZhbGlkIE1SQUlEIDIuMCBldmVudC4iLCJhZGRFdmVudExpc3RlbmVyIik7cmV0dXJufWV2ZW50Q2FsbGJhY2tzW2V2ZW50XS5wdXNoKGNhbGxiYWNrKX0sX3JlbW92ZUV2ZW50TGlzdGVuZXI6ZnVuY3Rpb24oZXZlbnQsY2FsbGJhY2spe2lmKG1yYWlkRXZlbnRzLmluZGV4T2YoZXZlbnQpPT0tMSl7cmV0dXJufXZhciBpbmRleD1ldmVudENhbGxiYWNrc1tldmVudF0uaW5kZXhPZihjYWxsYmFjayk7aWYoaW5kZXghPS0xKXtldmVudENhbGxiYWNrc1tldmVudF0uc3BsaWNlKDEsMSl9fX07cmV0dXJuIHNlbGZ9KSgpO3ZhciBtcmFpZD0oZnVuY3Rpb24oKXtyZXR1cm57YWRkRXZlbnRMaXN0ZW5lcjp3aW5kb3cuX21yYWlkLl9hZGRFdmVudExpc3RlbmVyLGNyZWF0ZUNhbGVuZGFyRXZlbnQ6ZnVuY3Rpb24oKXt9LGNsb3NlOndpbmRvdy5fbXJhaWQuX2Nsb3NlLGV4cGFuZDpmdW5jdGlvbigpe30sZ2V0Q3VycmVudFBvc2l0aW9uOmZ1bmN0aW9uKCl7cmV0dXJue3g6MCx5OjAsd2lkdGg6d2luZG93Ll9tcmFpZC5fZ2V0TWF4U2l6ZSgpLndpZHRoLGhlaWdodDp3aW5kb3cuX21yYWlkLl9nZXRNYXhTaXplKCkuaGVpZ2h0fX0sZ2V0RGVmYXVsdFBvc2l0aW9uOmZ1bmN0aW9uKCl7cmV0dXJue3g6MCx5OjAsd2lkdGg6d2luZG93Ll9tcmFpZC5fZ2V0TWF4U2l6ZSgpLndpZHRoLGhlaWdodDp3aW5kb3cuX21yYWlkLl9nZXRNYXhTaXplKCkuaGVpZ2h0fX0sZ2V0RXhwYW5kUHJvcGVydGllczpmdW5jdGlvbigpe3JldHVybnt3aWR0aDp3aW5kb3cuX21yYWlkLl9nZXRNYXhTaXplKCkud2lkdGgsaGVpZ2h0OndpbmRvdy5fbXJhaWQuX2dldE1heFNpemUoKS5oZWlnaHQsdXNlQ3VzdG9tQ2xvc2U6ZmFsc2UsaXNNb2RhbDp0cnVlfX0sZ2V0TWF4U2l6ZTp3aW5kb3cuX21yYWlkLl9nZXRNYXhTaXplLGdldE9yaWVudGF0aW9uUHJvcGVydGllczp3aW5kb3cuX21yYWlkLl9nZXRPcmllbnRhdGlvblByb3BlcnRpZXMsZ2V0UGxhY2VtZW50VHlwZTp3aW5kb3cuX21yYWlkLl9nZXRQbGFjZW1lbnRUeXBlLGdldFJlc2l6ZVByb3BlcnRpZXM6ZnVuY3Rpb24oKXtyZXR1cm57d2lkdGg6d2luZG93Ll9tcmFpZC5fZ2V0TWF4U2l6ZSgpLndpZHRoLGhlaWdodDp3aW5kb3cuX21yYWlkLl9nZXRNYXhTaXplKCkuaGVpZ2h0LG9mZnNldFg6MCxvZmZzZXRZOjB9fSxnZXRTY3JlZW5TaXplOndpbmRvdy5fbXJhaWQuX2dldFNjcmVlblNpemUsZ2V0U3RhdGU6d2luZG93Ll9tcmFpZC5fZ2V0U3RhdGUsZ2V0VmVyc2lvbjpmdW5jdGlvbigpe3JldHVybiIyLjAifSxpc1ZpZXdhYmxlOndpbmRvdy5fbXJhaWQuX2lzVmlld2FibGUsb3Blbjp3aW5kb3cuX21yYWlkLl9vcGVuLHBsYXlWaWRlbzp3aW5kb3cuX21yYWlkLl9wbGF5VmlkZW8scmVtb3ZlRXZlbnRMaXN0ZW5lcjp3aW5kb3cuX21yYWlkLl9yZW1vdmVFdmVudExpc3RlbmVyLHJlc2l6ZTpmdW5jdGlvbigpe30sc2V0RXhwYW5kUHJvcGVydGllczpmdW5jdGlvbigpe30sc2V0T3JpZW50YXRpb25Qcm9wZXJ0aWVzOmZ1bmN0aW9uKG9wKXt3aW5kb3cuX21yYWlkLl9zZXRPcmllbnRhdGlvblByb3BlcnRpZXMob3AuYWxsb3dPcmllbnRhdGlvbkNoYW5nZSxvcC5mb3JjZU9yaWVudGF0aW9ufHwibm9uZSIpfSxzZXRSZXNpemVQcm9wZXJ0aWVzOmZ1bmN0aW9uKCl7fSxzdG9yZVBpY3R1cmU6ZnVuY3Rpb24oKXt9LHN1cHBvcnRzOndpbmRvdy5fbXJhaWQuX3N1cHBvcnRzLHVzZUN1c3RvbUNsb3NlOmZ1bmN0aW9uKCl7fX19KSgpOw==").getBytes("UTF-8");
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("MRAIDController.injectMraidJavascript", th);
                ErrorReportingHelper.m3511("MRAIDController.injectMraidJavascript", th);
            }
            return new WebResourceResponse("application/javascript", (String) null, new ByteArrayInputStream(bArr));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private com.adincube.sdk.g.a.a.a m3271(URL url) {
            for (com.adincube.sdk.g.a.a.a next : com.adincube.sdk.util.c.m3551(c.this.f2733, com.adincube.sdk.g.a.a.b.MEDIA, false)) {
                if (url.equals(next.m3178())) {
                    return next;
                }
            }
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m3272() {
            c.this.f2720.f2752 = true;
            c.this.f2720.m3281();
            c cVar = c.this;
            if (!cVar.f2729) {
                cVar.f2729 = true;
                String str = (cVar.f2733.f2581 == null || cVar.f2733.f2581.length() <= 0) ? cVar.f2732.ﹶ : cVar.f2733.f2581;
                if (str != null && str.length() != 0) {
                    cVar.f2721.m3255(str);
                }
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            try {
                m3272();
            } catch (Throwable th) {
                ErrorReportingHelper.m3506("MRAIDController#ABWebViewClient.onPageFinished", com.adincube.sdk.g.c.b.INTERSTITIAL, th);
                com.adincube.sdk.util.a.m3513("MRAIDController#ABWebViewClient.onPageFinished()", th);
            }
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        @android.annotation.SuppressLint({"NewApi"})
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final android.webkit.WebResourceResponse shouldInterceptRequest(android.webkit.WebView r13, java.lang.String r14) {
            /*
                r12 = this;
                r0 = 0
                r11 = 1
                r10 = 0
                java.lang.String r1 = "mraid://"
                boolean r1 = r14.startsWith(r1)     // Catch:{ Throwable -> 0x00c4 }
                if (r1 != 0) goto L_0x0015
                java.lang.String r1 = "adincube://"
                boolean r1 = r14.startsWith(r1)     // Catch:{ Throwable -> 0x00c4 }
                if (r1 == 0) goto L_0x0196
            L_0x0015:
                android.net.Uri r1 = android.net.Uri.parse(r14)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r2 = "mraid://"
                boolean r2 = r14.startsWith(r2)     // Catch:{ Throwable -> 0x00c4 }
                if (r2 == 0) goto L_0x003e
                com.adincube.sdk.h.c r2 = com.adincube.sdk.h.c.this     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r3 = "/close"
                java.lang.String r4 = r1.getPath()     // Catch:{ Throwable -> 0x00c4 }
                boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c4 }
                if (r3 == 0) goto L_0x006e
                java.lang.String r3 = "MRAIDController.handleCloseAction"
                com.adincube.sdk.h.c$c r4 = r2.f2725     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.c$1 r5 = new com.adincube.sdk.h.c$1     // Catch:{ Throwable -> 0x00c4 }
                r5.<init>()     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.util.o.m3700((java.lang.String) r3, r4, r5)     // Catch:{ Throwable -> 0x00c4 }
            L_0x003e:
                java.lang.String r2 = "adincube://"
                boolean r2 = r14.startsWith(r2)     // Catch:{ Throwable -> 0x00c4 }
                if (r2 == 0) goto L_0x0059
                java.lang.String r2 = r1.getLastPathSegment()     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r3 = "url"
                java.lang.String r1 = r1.getQueryParameter(r3)     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.c r3 = com.adincube.sdk.h.c.this     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.c$d r3 = r3.f2723     // Catch:{ Throwable -> 0x00c4 }
                r3.m3276(r2, r1)     // Catch:{ Throwable -> 0x00c4 }
            L_0x0059:
                r1 = 0
                byte[] r2 = new byte[r1]     // Catch:{ Throwable -> 0x00c4 }
                android.webkit.WebResourceResponse r1 = new android.webkit.WebResourceResponse     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r3 = "text/html"
                java.lang.String r4 = "UTF-8"
                java.io.ByteArrayInputStream r5 = new java.io.ByteArrayInputStream     // Catch:{ Throwable -> 0x00c4 }
                r5.<init>(r2)     // Catch:{ Throwable -> 0x00c4 }
                r1.<init>(r3, r4, r5)     // Catch:{ Throwable -> 0x00c4 }
                r0 = r1
            L_0x006d:
                return r0
            L_0x006e:
                java.lang.String r3 = "/open"
                java.lang.String r4 = r1.getPath()     // Catch:{ Throwable -> 0x00c4 }
                boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c4 }
                if (r3 == 0) goto L_0x00d8
                java.lang.String r3 = "url"
                java.lang.String r3 = r1.getQueryParameter(r3)     // Catch:{ Throwable -> 0x00c4 }
                if (r3 == 0) goto L_0x003e
                java.net.URI r4 = new java.net.URI     // Catch:{ URISyntaxException -> 0x0097 }
                r4.<init>(r3)     // Catch:{ URISyntaxException -> 0x0097 }
                java.lang.String r4 = "MRAIDController.handleOpenAction"
                com.adincube.sdk.h.c$c r5 = r2.f2725     // Catch:{ URISyntaxException -> 0x0097 }
                com.adincube.sdk.h.c$2 r6 = new com.adincube.sdk.h.c$2     // Catch:{ URISyntaxException -> 0x0097 }
                r6.<init>(r3)     // Catch:{ URISyntaxException -> 0x0097 }
                com.adincube.sdk.util.o.m3700((java.lang.String) r4, r5, r6)     // Catch:{ URISyntaxException -> 0x0097 }
                goto L_0x003e
            L_0x0097:
                r4 = move-exception
                com.adincube.sdk.h.a r2 = r2.f2721     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.d r4 = com.adincube.sdk.h.d.ERROR     // Catch:{ Throwable -> 0x00c4 }
                r5 = 2
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x00c4 }
                r6 = 0
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r8 = "'"
                r7.<init>(r8)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r7 = "' is not a valid URL. Will not be open"
                java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c4 }
                r5[r6] = r3     // Catch:{ Throwable -> 0x00c4 }
                r3 = 1
                java.lang.String r6 = "open"
                r5[r3] = r6     // Catch:{ Throwable -> 0x00c4 }
                r2.m3254((com.adincube.sdk.h.d) r4, (java.lang.Object[]) r5)     // Catch:{ Throwable -> 0x00c4 }
                goto L_0x003e
            L_0x00c4:
                r1 = move-exception
                java.lang.String r2 = "MRAIDController#ABWebViewClient.shouldInterceptRequest"
                com.adincube.sdk.g.c.b r3 = com.adincube.sdk.g.c.b.INTERSTITIAL
                com.adincube.sdk.util.ErrorReportingHelper.m3506((java.lang.String) r2, (com.adincube.sdk.g.c.b) r3, (java.lang.Throwable) r1)
                java.lang.String r2 = "MRAIDController#ABWebViewClient.shouldInterceptRequest"
                java.lang.Object[] r3 = new java.lang.Object[r11]
                r3[r10] = r1
                com.adincube.sdk.util.a.m3513(r2, r3)
                goto L_0x006d
            L_0x00d8:
                java.lang.String r3 = "/setOrientationProperties"
                java.lang.String r4 = r1.getPath()     // Catch:{ Throwable -> 0x00c4 }
                boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c4 }
                if (r3 == 0) goto L_0x013f
                java.lang.String r3 = "allowOrientationChange"
                java.lang.String r3 = r1.getQueryParameter(r3)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r4 = "forceOrientation"
                java.lang.String r4 = r1.getQueryParameter(r4)     // Catch:{ Throwable -> 0x00c4 }
                if (r3 == 0) goto L_0x003e
                if (r4 == 0) goto L_0x003e
                java.util.List<java.lang.String> r3 = r2.f2734     // Catch:{ Throwable -> 0x00c4 }
                boolean r3 = r3.contains(r4)     // Catch:{ Throwable -> 0x00c4 }
                if (r3 != 0) goto L_0x0135
                com.adincube.sdk.h.a r3 = r2.f2721     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.d r5 = com.adincube.sdk.h.d.ERROR     // Catch:{ Throwable -> 0x00c4 }
                r6 = 2
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x00c4 }
                r7 = 0
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r9 = "'"
                r8.<init>(r9)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r8 = "' is not one of the orientation defined in MRAID 2.0. Allowed values are "
                java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ Throwable -> 0x00c4 }
                java.util.List<java.lang.String> r2 = r2.f2734     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00c4 }
                java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00c4 }
                r6[r7] = r2     // Catch:{ Throwable -> 0x00c4 }
                r2 = 1
                java.lang.String r4 = "open"
                r6[r2] = r4     // Catch:{ Throwable -> 0x00c4 }
                r3.m3254((com.adincube.sdk.h.d) r5, (java.lang.Object[]) r6)     // Catch:{ Throwable -> 0x00c4 }
                goto L_0x003e
            L_0x0135:
                boolean r3 = java.lang.Boolean.parseBoolean(r4)     // Catch:{ Throwable -> 0x00c4 }
                r2.f2722 = r3     // Catch:{ Throwable -> 0x00c4 }
                r2.f2727 = r4     // Catch:{ Throwable -> 0x00c4 }
                goto L_0x003e
            L_0x013f:
                java.lang.String r3 = "/playVideo"
                java.lang.String r4 = r1.getPath()     // Catch:{ Throwable -> 0x00c4 }
                boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c4 }
                if (r3 == 0) goto L_0x003e
                java.lang.String r3 = "uri"
                java.lang.String r3 = r1.getQueryParameter(r3)     // Catch:{ Throwable -> 0x00c4 }
                if (r3 == 0) goto L_0x003e
                java.net.URI r4 = new java.net.URI     // Catch:{ URISyntaxException -> 0x0169 }
                r4.<init>(r3)     // Catch:{ URISyntaxException -> 0x0169 }
                java.lang.String r4 = "MRAIDController.handlePlayVideo"
                com.adincube.sdk.h.c$c r5 = r2.f2725     // Catch:{ URISyntaxException -> 0x0169 }
                com.adincube.sdk.h.c$3 r6 = new com.adincube.sdk.h.c$3     // Catch:{ URISyntaxException -> 0x0169 }
                r6.<init>(r3)     // Catch:{ URISyntaxException -> 0x0169 }
                com.adincube.sdk.util.o.m3700((java.lang.String) r4, r5, r6)     // Catch:{ URISyntaxException -> 0x0169 }
                goto L_0x003e
            L_0x0169:
                r4 = move-exception
                com.adincube.sdk.h.a r2 = r2.f2721     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.d r4 = com.adincube.sdk.h.d.ERROR     // Catch:{ Throwable -> 0x00c4 }
                r5 = 2
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x00c4 }
                r6 = 0
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r8 = "'"
                r7.<init>(r8)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r7 = "' is not a valid URI. Will not be played"
                java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c4 }
                r5[r6] = r3     // Catch:{ Throwable -> 0x00c4 }
                r3 = 1
                java.lang.String r6 = "playVideo"
                r5[r3] = r6     // Catch:{ Throwable -> 0x00c4 }
                r2.m3254((com.adincube.sdk.h.d) r4, (java.lang.Object[]) r5)     // Catch:{ Throwable -> 0x00c4 }
                goto L_0x003e
            L_0x0196:
                java.lang.String r1 = "http://"
                boolean r1 = r14.startsWith(r1)     // Catch:{ Throwable -> 0x00c4 }
                if (r1 != 0) goto L_0x01a8
                java.lang.String r1 = "https://"
                boolean r1 = r14.startsWith(r1)     // Catch:{ Throwable -> 0x00c4 }
                if (r1 == 0) goto L_0x006d
            L_0x01a8:
                com.adincube.sdk.h.c r1 = com.adincube.sdk.h.c.this     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.g r1 = r1.f2720     // Catch:{ Throwable -> 0x00c4 }
                r2 = 1
                r1.f2752 = r2     // Catch:{ Throwable -> 0x00c4 }
                r12.m3272()     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r1 = "="
                boolean r1 = r1.equals(r14)     // Catch:{ Throwable -> 0x00c4 }
                if (r1 == 0) goto L_0x01d8
                com.adincube.sdk.h.c r1 = com.adincube.sdk.h.c.this     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.a r1 = r1.f2721     // Catch:{ Throwable -> 0x00c4 }
                android.os.Handler r2 = new android.os.Handler     // Catch:{ Throwable -> 0x00c4 }
                android.os.Looper r3 = android.os.Looper.getMainLooper()     // Catch:{ Throwable -> 0x00c4 }
                r2.<init>(r3)     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.a$1 r3 = new com.adincube.sdk.h.a$1     // Catch:{ Throwable -> 0x00c4 }
                r3.<init>()     // Catch:{ Throwable -> 0x00c4 }
                r4 = 100
                r2.postDelayed(r3, r4)     // Catch:{ Throwable -> 0x00c4 }
                android.webkit.WebResourceResponse r0 = m3270()     // Catch:{ Throwable -> 0x00c4 }
                goto L_0x006d
            L_0x01d8:
                java.net.URL r1 = new java.net.URL     // Catch:{ Throwable -> 0x00c4 }
                r1.<init>(r14)     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.g.a.a.a r1 = r12.m3271(r1)     // Catch:{ Throwable -> 0x00c4 }
                if (r1 == 0) goto L_0x006d
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x00c4 }
                r3 = 0
                r2[r3] = r14     // Catch:{ Throwable -> 0x00c4 }
                com.adincube.sdk.h.c r2 = com.adincube.sdk.h.c.this     // Catch:{ Throwable -> 0x00c4 }
                android.content.Context r2 = r2.f2731     // Catch:{ Throwable -> 0x00c4 }
                java.io.InputStream r2 = com.adincube.sdk.util.n.m3694((android.content.Context) r2, (com.adincube.sdk.g.a.a.a) r1)     // Catch:{ Throwable -> 0x00c4 }
                android.webkit.WebResourceResponse r1 = new android.webkit.WebResourceResponse     // Catch:{ Throwable -> 0x00c4 }
                java.lang.String r3 = "application/octet-stream"
                r4 = 0
                r1.<init>(r3, r4, r2)     // Catch:{ Throwable -> 0x00c4 }
                r0 = r1
                goto L_0x006d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.h.c.b.shouldInterceptRequest(android.webkit.WebView, java.lang.String):android.webkit.WebResourceResponse");
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            try {
                if (!str.startsWith("data:") && !str.startsWith("adincube://") && !str.startsWith("mraid://")) {
                    c.this.f2725.m3275(Uri.parse(str));
                    return true;
                }
            } catch (Throwable th) {
                ErrorReportingHelper.m3506("MRAIDController#ABWebViewClient.shouldOverrideUrlLoading", com.adincube.sdk.g.c.b.INTERSTITIAL, th);
                com.adincube.sdk.util.a.m3513("MRAIDController#ABWebViewClient.shouldOverrideUrlLoading()", th);
            }
            return false;
        }
    }

    /* renamed from: com.adincube.sdk.h.c$c  reason: collision with other inner class name */
    public interface C0015c {
        /* renamed from: 靐  reason: contains not printable characters */
        void m3273(Uri uri);

        /* renamed from: 龘  reason: contains not printable characters */
        void m3274();

        /* renamed from: 龘  reason: contains not printable characters */
        void m3275(Uri uri);
    }

    public interface d {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3276(String str, String str2);
    }

    public c(Context context, e eVar, f fVar) {
        this.f2733 = eVar;
        this.f2732 = fVar;
        this.f2731 = context;
        this.f2734 = Arrays.asList(new String[]{"none", "portrait", "landscape"});
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3260() {
        if (!this.f2736) {
            this.f2736 = true;
            this.f2721.m3254(d.READY, new Object[0]);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m3261() {
        Point point = new Point();
        if (this.f2733.f2587 == null || this.f2733.f2586 == null) {
            point.x = j.m3545(this.f2731, this.f2730.getWidth());
            point.y = j.m3545(this.f2731, this.f2730.getHeight());
        } else {
            point.x = this.f2733.f2587.intValue();
            point.y = this.f2733.f2586.intValue();
        }
        if (point.x != this.f2728.x || point.y != this.f2728.y) {
            this.f2721.m3256("_setMaxSize", Integer.valueOf(point.x), Integer.valueOf(point.y));
            this.f2721.m3256("_setScreenSize", Integer.valueOf(point.x), Integer.valueOf(point.y));
            if (this.f2736) {
                this.f2721.m3254(d.SIZE_CHANGE, Integer.valueOf(this.f2728.x), Integer.valueOf(this.f2728.y));
            }
            this.f2728 = point;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3262() {
        g gVar = this.f2720;
        if (!g.m3280()) {
            if (gVar.f2751.f2586 == null || gVar.f2751.f2587 == null) {
                if (!(gVar.f2753.getWidth() == 0 && gVar.f2753.getHeight() == 0)) {
                    gVar.m3282(j.m3545(gVar.f2754, gVar.f2753.getWidth()), j.m3545(gVar.f2754, gVar.f2753.getHeight()));
                }
            } else if (!(gVar.f2751.f2586 == null || gVar.f2751.f2587 == null)) {
                gVar.m3282(gVar.f2751.f2587.intValue(), gVar.f2751.f2586.intValue());
            }
        }
        this.f2720.m3281();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3263() {
        InputStream inputStream = null;
        try {
            inputStream = n.m3694(this.f2731, com.adincube.sdk.util.c.m3550(this.f2733, com.adincube.sdk.g.a.a.b.MEDIA));
            String r0 = u.m3717(inputStream);
            if (r0 == null || r0.length() <= 0) {
                throw new IOException("Cannot read ad content from resource.");
            }
            this.f2724.m3284("=", r0, "text/html", "UTF-8");
            try {
                inputStream.close();
                return;
            } catch (Throwable th) {
                return;
            }
        } catch (Throwable th2) {
        }
        throw th;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3264(WebView webView) {
        this.f2735 = f.LOADING;
        this.f2736 = false;
        this.f2726 = false;
        this.f2730 = webView;
        this.f2724 = new h(webView);
        this.f2721 = new a(this.f2724);
        this.f2720 = new g(this.f2731, this.f2733, webView, this.f2721);
        webView.setWebViewClient(new b(this, (byte) 0));
        webView.setWebChromeClient(new a(this, (byte) 0));
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        WebSettings settings = webView.getSettings();
        if (g.m3280()) {
            settings.setUseWideViewPort(true);
            settings.setSupportZoom(false);
        }
        if (Build.VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            settings.setAllowContentAccess(false);
        }
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        webView.clearCache(true);
        settings.setCacheMode(2);
        if (Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        this.f2721.m3256("_setPlacementType", this.f2733.m3212().h);
        if (Build.VERSION.SDK_INT >= 18) {
            this.f2721.m3256("_setSupportFor", "inlineVideo", "true");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3265(f fVar) {
        if (this.f2735 != fVar) {
            this.f2735 = fVar;
            this.f2721.m3256("_setState", fVar);
            this.f2721.m3254(d.STATE_CHANGE, fVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3266(boolean z) {
        if (this.f2726 != z) {
            this.f2726 = z;
            this.f2721.m3256("_setVisible", Boolean.valueOf(z));
            if (this.f2736) {
                this.f2721.m3254(d.VIEWABLE_CHANGE, Boolean.valueOf(z));
            }
        }
    }
}
