package com.adincube.sdk.h;

import android.annotation.SuppressLint;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@SuppressLint({"NewApi"})
public final class e {

    /* renamed from: 龘  reason: contains not printable characters */
    private Map<String, a> f2745 = new HashMap();

    /* renamed from: com.adincube.sdk.h.e$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f2746 = new int[a.a().length];

        static {
            try {
                f2746[a.LOADING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f2746[a.LOADED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f2746[a.ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public enum a {
        LOADING("new"),
        LOADED("loaded"),
        ERROR("error");
        
        private String d;

        private a(String str) {
            this.d = str;
        }

        public static a a(String str) {
            for (a aVar : a()) {
                if (aVar.d.equals(str)) {
                    return aVar;
                }
            }
            return null;
        }

        public static a[] a() {
            return (a[]) f2747.clone();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m3277() {
        int i;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        for (String str : this.f2745.keySet()) {
            switch (AnonymousClass1.f2746[this.f2745.get(str).ordinal()]) {
                case 1:
                    i4++;
                    continue;
                case 2:
                    i3++;
                    continue;
                case 3:
                    i = i2 + 1;
                    break;
                default:
                    i = i2;
                    break;
            }
            i2 = i;
        }
        return String.format(Locale.US, "resources=[%d/%d] inError=[%d]", new Object[]{Integer.valueOf(i3), Integer.valueOf(i3 + i4 + i2), Integer.valueOf(i2)});
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3278(String str, String str2) {
        a a2 = a.a(str);
        synchronized (this.f2745) {
            this.f2745.put(str2, a2);
            Object[] objArr = {m3277(), a2.name(), str2.toString()};
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return false;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m3279(boolean r7) {
        /*
            r6 = this;
            r2 = 1
            r1 = 0
            java.util.Map<java.lang.String, com.adincube.sdk.h.e$a> r3 = r6.f2745
            monitor-enter(r3)
            java.util.Map<java.lang.String, com.adincube.sdk.h.e$a> r0 = r6.f2745     // Catch:{ all -> 0x0042 }
            int r0 = r0.size()     // Catch:{ all -> 0x0042 }
            if (r0 != 0) goto L_0x0010
            monitor-exit(r3)     // Catch:{ all -> 0x0042 }
            r0 = r1
        L_0x000f:
            return r0
        L_0x0010:
            java.util.Map<java.lang.String, com.adincube.sdk.h.e$a> r0 = r6.f2745     // Catch:{ all -> 0x0042 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x0042 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ all -> 0x0042 }
        L_0x001a:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x003f
            java.lang.Object r0 = r4.next()     // Catch:{ all -> 0x0042 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0042 }
            java.util.Map<java.lang.String, com.adincube.sdk.h.e$a> r5 = r6.f2745     // Catch:{ all -> 0x0042 }
            java.lang.Object r0 = r5.get(r0)     // Catch:{ all -> 0x0042 }
            com.adincube.sdk.h.e$a r5 = com.adincube.sdk.h.e.a.LOADED     // Catch:{ all -> 0x0042 }
            if (r0 == r5) goto L_0x001a
            if (r7 == 0) goto L_0x003c
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0042 }
            r2 = 0
            java.lang.String r4 = r6.m3277()     // Catch:{ all -> 0x0042 }
            r0[r2] = r4     // Catch:{ all -> 0x0042 }
        L_0x003c:
            monitor-exit(r3)     // Catch:{ all -> 0x0042 }
            r0 = r1
            goto L_0x000f
        L_0x003f:
            monitor-exit(r3)     // Catch:{ all -> 0x0042 }
            r0 = r2
            goto L_0x000f
        L_0x0042:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0042 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.h.e.m3279(boolean):boolean");
    }
}
