package com.adincube.sdk.h;

import android.os.Handler;
import android.os.Looper;
import android.webkit.WebView;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.v;
import java.util.concurrent.atomic.AtomicInteger;

public final class h {

    /* renamed from: 靐  reason: contains not printable characters */
    private AtomicInteger f2755 = new AtomicInteger(0);

    /* renamed from: 龘  reason: contains not printable characters */
    private WebView f2756;

    private static class a {

        /* renamed from: 靐  reason: contains not printable characters */
        public String f2759;

        /* renamed from: 麤  reason: contains not printable characters */
        public String f2760;

        /* renamed from: 齉  reason: contains not printable characters */
        public String f2761;

        /* renamed from: 龘  reason: contains not printable characters */
        public String f2762;

        private a() {
        }

        /* synthetic */ a(byte b) {
            this();
        }
    }

    public h(WebView webView) {
        this.f2756 = webView;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3283(a aVar) {
        try {
            Object[] objArr = {v.m3721(aVar.f2762), v.m3721(aVar.f2759)};
            this.f2755.incrementAndGet();
            if (aVar.f2759 == null) {
                this.f2756.loadUrl(aVar.f2762);
            } else {
                this.f2756.loadDataWithBaseURL(aVar.f2762, aVar.f2759, aVar.f2761, aVar.f2760, (String) null);
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("WebViewLoader.loadContentInUiThread", th);
            ErrorReportingHelper.m3511("WebViewLoader.loadContentInUiThread", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3284(String str, String str2, String str3, String str4) {
        final a aVar = new a((byte) 0);
        aVar.f2762 = str;
        aVar.f2759 = str2;
        aVar.f2761 = str3;
        aVar.f2760 = str4;
        if (Looper.myLooper() == Looper.getMainLooper()) {
            m3283(aVar);
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    h.this.m3283(aVar);
                }
            });
        }
    }
}
