package com.adincube.sdk.h;

public enum f {
    LOADING("loading"),
    DEFAULT("default"),
    EXPANDED("expanded"),
    RESIZED("resized"),
    HIDDEN("hidden");
    
    String f;

    private f(String str) {
        this.f = str;
    }

    public final String toString() {
        return this.f;
    }
}
