package com.adincube.sdk;

import android.content.Context;
import android.util.AttributeSet;
import com.adincube.sdk.k.a;
import com.adincube.sdk.mediation.r.b;
import com.adincube.sdk.util.ErrorReportingHelper;

public class AdChoicesView extends a {

    /* renamed from: 靐  reason: contains not printable characters */
    private b f1906 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f1907 = 8;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f1908 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private com.adincube.sdk.f.b.d.a.a f1909 = null;

    public AdChoicesView(Context context) {
        super(context);
        m2299();
    }

    public AdChoicesView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m2299();
    }

    public AdChoicesView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m2299();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m2299() {
        try {
            this.f1909 = new com.adincube.sdk.f.b.d.a.a();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AdChoicesView.init", th);
            ErrorReportingHelper.m3511("AdChoicesView.init", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2300(int i) {
        this.f1907 = i;
        if (i == 0) {
            super.setVisibility(this.f1908);
        } else {
            super.setVisibility(i);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setNativeAd(com.adincube.sdk.NativeAd r8) {
        /*
            r7 = this;
            r6 = 1
            r5 = 0
            com.adincube.sdk.mediation.r.b r0 = r7.f1906     // Catch:{ Throwable -> 0x0044 }
            if (r0 != r8) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            r0 = 0
            r7.f1906 = r0     // Catch:{ Throwable -> 0x0044 }
            r0 = 8
            r7.m2300(r0)     // Catch:{ Throwable -> 0x0044 }
            r7.removeAllViews()     // Catch:{ Throwable -> 0x0044 }
            if (r8 == 0) goto L_0x0006
            boolean r0 = r8 instanceof com.adincube.sdk.mediation.r.b     // Catch:{ Throwable -> 0x0044 }
            if (r0 == 0) goto L_0x0006
            com.adincube.sdk.mediation.r.b r8 = (com.adincube.sdk.mediation.r.b) r8     // Catch:{ Throwable -> 0x0044 }
            boolean r0 = r8.ˈ()     // Catch:{ Throwable -> 0x0044 }
            if (r0 == 0) goto L_0x0039
            android.content.Context r2 = r7.getContext()     // Catch:{ Throwable -> 0x0044 }
            com.adincube.sdk.mediation.r.c r0 = r8.齉     // Catch:{ Throwable -> 0x0044 }
            android.view.View r0 = r0.龘(r2, r8)     // Catch:{ Throwable -> 0x0044 }
            if (r0 != 0) goto L_0x0036
            com.adincube.sdk.k.a.a r1 = new com.adincube.sdk.k.a.a     // Catch:{ Throwable -> 0x0044 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0044 }
            com.adincube.sdk.mediation.r.b r0 = r1.f2896     // Catch:{ Throwable -> 0x0091 }
            if (r0 != r8) goto L_0x0056
            r0 = r1
        L_0x0036:
            r7.addView(r0)     // Catch:{ Throwable -> 0x0044 }
        L_0x0039:
            boolean r0 = r8.ˈ()     // Catch:{ Throwable -> 0x0044 }
            if (r0 == 0) goto L_0x00a8
            r0 = 0
            r7.m2300(r0)     // Catch:{ Throwable -> 0x0044 }
            goto L_0x0006
        L_0x0044:
            r0 = move-exception
            java.lang.String r1 = "AdChoicesView.setNativeAd"
            java.lang.Object[] r2 = new java.lang.Object[r6]
            r2[r5] = r0
            com.adincube.sdk.util.a.m3513(r1, r2)
            java.lang.String r1 = "AdChoicesView.setNativeAd"
            com.adincube.sdk.util.ErrorReportingHelper.m3511(r1, r0)
            goto L_0x0006
        L_0x0056:
            r0 = 0
            r1.f2896 = r0     // Catch:{ Throwable -> 0x0091 }
            r0 = 0
            r1.setImageDrawable(r0)     // Catch:{ Throwable -> 0x0091 }
            if (r8 == 0) goto L_0x008f
            boolean r0 = r8.ﹶ     // Catch:{ Throwable -> 0x0091 }
            if (r0 != 0) goto L_0x008f
            r1.f2896 = r8     // Catch:{ Throwable -> 0x0091 }
            boolean r0 = r8.ˈ()     // Catch:{ Throwable -> 0x0091 }
            if (r0 == 0) goto L_0x008f
            com.adincube.sdk.mediation.r.b$a r0 = r8.ʿ     // Catch:{ Throwable -> 0x0091 }
            boolean r2 = r0.龘()     // Catch:{ Throwable -> 0x0091 }
            if (r2 == 0) goto L_0x0086
            com.adincube.sdk.g.d.b r2 = new com.adincube.sdk.g.d.b     // Catch:{ Throwable -> 0x0091 }
            java.lang.String r0 = r0.龘     // Catch:{ Throwable -> 0x0091 }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x0091 }
            r2.f2688 = r1     // Catch:{ Throwable -> 0x0091 }
            com.adincube.sdk.f.d.b r0 = r1.f2895     // Catch:{ Throwable -> 0x0091 }
            com.adincube.sdk.g.d.b r0 = r0.m3094((com.adincube.sdk.g.d.b) r2)     // Catch:{ Throwable -> 0x0091 }
            r1.f2893 = r0     // Catch:{ Throwable -> 0x0091 }
            r0 = r1
            goto L_0x0036
        L_0x0086:
            com.adincube.sdk.mediation.r.c r0 = r8.齉     // Catch:{ Throwable -> 0x0091 }
            android.graphics.drawable.Drawable r0 = r1.m3404((com.adincube.sdk.mediation.r.c) r0)     // Catch:{ Throwable -> 0x0091 }
            r1.setImageDrawable(r0)     // Catch:{ Throwable -> 0x0091 }
        L_0x008f:
            r0 = r1
            goto L_0x0036
        L_0x0091:
            r0 = move-exception
            java.lang.String r2 = "NativeAdAdChoicesView.setNativeAd"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0044 }
            r4 = 0
            r3[r4] = r0     // Catch:{ Throwable -> 0x0044 }
            com.adincube.sdk.util.a.m3513(r2, r3)     // Catch:{ Throwable -> 0x0044 }
            java.lang.String r2 = "NativeAdAdChoicesView.setNativeAd"
            com.adincube.sdk.g.c.b r3 = com.adincube.sdk.g.c.b.NATIVE     // Catch:{ Throwable -> 0x0044 }
            com.adincube.sdk.util.ErrorReportingHelper.m3506((java.lang.String) r2, (com.adincube.sdk.g.c.b) r3, (java.lang.Throwable) r0)     // Catch:{ Throwable -> 0x0044 }
            r0 = r1
            goto L_0x0036
        L_0x00a8:
            r0 = 8
            r7.m2300(r0)     // Catch:{ Throwable -> 0x0044 }
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.AdChoicesView.setNativeAd(com.adincube.sdk.NativeAd):void");
    }

    public void setVisibility(int i) {
        try {
            this.f1908 = i;
            m2300(this.f1907);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AdChoicesView.setVisibility", th);
            ErrorReportingHelper.m3511("AdChoicesView.setVisibility", th);
        }
    }
}
