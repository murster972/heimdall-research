package com.adincube.sdk.f.g;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a.b;
import com.adincube.sdk.util.o;
import com.adincube.sdk.util.s;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class a {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static a f2532 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    private com.adincube.sdk.util.a.b f2533;

    /* renamed from: ʽ  reason: contains not printable characters */
    private b.a f2534 = new b.a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3169(boolean z) {
            if (!z) {
                try {
                    a.this.m3156();
                } catch (Throwable th) {
                    new Object[1][0] = th;
                    ErrorReportingHelper.m3506("ViewVisibilityTrackingManager#OnScreenStateChangedListener.onScreenStateChanged", com.adincube.sdk.g.c.b.NATIVE, th);
                }
            } else {
                a.this.m3152();
            }
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f2535 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private Set<C0010a> f2536 = new HashSet();

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f2537 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private Set<b> f2538 = new HashSet();

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f2539 = null;

    /* renamed from: com.adincube.sdk.f.g.a$a  reason: collision with other inner class name */
    private class C0010a {

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean f2542;

        /* renamed from: 齉  reason: contains not printable characters */
        public double f2544;

        /* renamed from: 龘  reason: contains not printable characters */
        public WeakReference<View> f2545;

        private C0010a() {
        }

        /* synthetic */ C0010a(a aVar, byte b) {
            this();
        }
    }

    public interface b {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3170(View view, boolean z);
    }

    private a(Context context) {
        this.f2539 = context.getApplicationContext();
        this.f2533 = new com.adincube.sdk.util.a.b(this.f2539, com.adincube.sdk.g.c.b.NATIVE, (com.adincube.sdk.g.c.a) null);
        this.f2533.f2975 = this.f2534;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private synchronized C0010a m3151(View view) {
        C0010a aVar;
        Iterator<C0010a> it2 = this.f2536.iterator();
        while (true) {
            if (!it2.hasNext()) {
                aVar = null;
                break;
            }
            aVar = it2.next();
            if (aVar.f2545.get() == view) {
                break;
            }
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m3152() {
        if (this.f2537) {
            if (this.f2535 && s.m3713(this.f2533.f2978)) {
                this.f2535 = false;
                m3162(false);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m3154(View view, double d) {
        return m3163(view, d, new Rect());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private synchronized void m3155() {
        if (this.f2537) {
            m3162(true);
            this.f2533.m3517();
            this.f2537 = false;
            this.f2535 = false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized void m3156() {
        if (!this.f2535) {
            m3162(true);
            this.f2535 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static a m3158(Context context) {
        if (f2532 == null) {
            synchronized (a.class) {
                if (f2532 == null) {
                    f2532 = new a(context);
                }
            }
        }
        return f2532;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m3159() {
        if (!this.f2537) {
            this.f2537 = true;
            this.f2535 = true;
            this.f2533.m3518();
        }
        m3152();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m3160(C0010a aVar, boolean z) {
        View view = (View) aVar.f2545.get();
        if (view != null) {
            for (b r1 : this.f2538) {
                try {
                    r1.m3170(view, z);
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("ViewVisibilityTrackingManager.notifyVisibilityChanged", th);
                    ErrorReportingHelper.m3511("ViewVisibilityTrackingManager.notifyVisibilityChanged", th);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m3162(boolean z) {
        try {
            if (this.f2537) {
                if (!this.f2535) {
                    Rect rect = new Rect();
                    Iterator<C0010a> it2 = this.f2536.iterator();
                    while (it2.hasNext()) {
                        if (!m3164(it2.next(), rect)) {
                            it2.remove();
                        }
                    }
                    if (!z) {
                        if (this.f2536.size() > 0) {
                            o.m3702("HandlerUtil.dispatchOnUiThreadDelayed", (Runnable) new Runnable() {
                                public final void run() {
                                    a.this.m3162(false);
                                }
                            }, 500);
                        } else {
                            m3155();
                        }
                    }
                }
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("ViewVisibilityTrackingManager.refreshTrackedViewsVisibility", th);
            ErrorReportingHelper.m3511("ViewVisibilityTrackingManager.refreshTrackedViewsVisibility", th);
        }
        return;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m3163(View view, double d, Rect rect) {
        try {
            if (view.getParent() == null || view.getWindowToken() == null || view.getVisibility() != 0 || view.getWindowVisibility() != 0) {
                return false;
            }
            if (Build.VERSION.SDK_INT >= 11 && view.getAlpha() < 0.9f) {
                return false;
            }
            if ((view.getMeasuredHeight() == 0 && view.getMeasuredWidth() == 0) || !view.getGlobalVisibleRect(rect)) {
                return false;
            }
            return ((long) (rect.width() * rect.height())) >= ((long) (((double) (view.getWidth() * view.getHeight())) * d));
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("ViewVisibilityTrackingManager.isVisible", th);
            ErrorReportingHelper.m3511("ViewVisibilityTrackingManager.isVisible", th);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized boolean m3164(C0010a aVar, Rect rect) {
        boolean z;
        View view = (View) aVar.f2545.get();
        if (view == null) {
            z = false;
        } else {
            boolean r0 = m3163(view, aVar.f2544, rect);
            if (r0 != aVar.f2542) {
                m3160(aVar, r0);
            }
            aVar.f2542 = r0;
            z = true;
        }
        return z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized void m3165(b bVar) {
        this.f2538.remove(bVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m3166(View view) {
        C0010a r0 = m3151(view);
        if (r0 != null) {
            this.f2536.remove(r0);
            if (this.f2536.size() == 0) {
                m3155();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m3167(View view, double d) {
        if (m3151(view) == null) {
            C0010a aVar = new C0010a(this, (byte) 0);
            aVar.f2545 = new WeakReference<>(view);
            aVar.f2544 = d;
            aVar.f2542 = m3163(view, d, new Rect());
            this.f2536.add(aVar);
            m3160(aVar, aVar.f2542);
            m3159();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m3168(b bVar) {
        this.f2538.add(bVar);
    }
}
