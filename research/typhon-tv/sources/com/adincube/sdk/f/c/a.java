package com.adincube.sdk.f.c;

import com.adincube.sdk.i.b;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class a {

    /* renamed from: 龘  reason: contains not printable characters */
    private static a f2446 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private Map<b.C0017b, ExecutorService> f2447 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    public static a m3062() {
        if (f2446 == null) {
            synchronized (a.class) {
                f2446 = new a();
            }
        }
        return f2446;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ExecutorService m3063(b.C0017b bVar) {
        ExecutorService executorService;
        synchronized (this.f2447) {
            executorService = this.f2447.get(bVar);
            if (executorService == null) {
                switch (bVar) {
                    case LOW:
                        executorService = Executors.newSingleThreadExecutor();
                        break;
                    case DEFAULT:
                        executorService = Executors.newFixedThreadPool(3);
                        break;
                    case HIGH:
                        executorService = Executors.newCachedThreadPool();
                        break;
                    default:
                        executorService = null;
                        break;
                }
                this.f2447.put(bVar, executorService);
            }
        }
        return executorService;
    }
}
