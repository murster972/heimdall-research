package com.adincube.sdk.f;

import android.content.Context;
import com.adincube.sdk.c.a.j;
import com.adincube.sdk.c.a.k;
import com.adincube.sdk.c.a.n;
import com.adincube.sdk.g.b.b;
import com.adincube.sdk.util.b.f;
import com.adincube.sdk.util.b.g;
import com.adincube.sdk.util.d;
import java.util.HashSet;
import java.util.Set;

public final class a {

    /* renamed from: 麤  reason: contains not printable characters */
    private static a f2128;

    /* renamed from: 齉  reason: contains not printable characters */
    private static b f2129;

    /* renamed from: 靐  reason: contains not printable characters */
    public Set<C0003a> f2130;

    /* renamed from: 龘  reason: contains not printable characters */
    public com.adincube.sdk.f.b.e.a.a f2131;

    /* renamed from: com.adincube.sdk.f.a$a  reason: collision with other inner class name */
    public interface C0003a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2613(b bVar);
    }

    private a() {
        this.f2131 = null;
        this.f2130 = new HashSet();
        this.f2131 = new com.adincube.sdk.f.b.e.a.b("cfg");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized a m2607() {
        a aVar;
        synchronized (a.class) {
            if (f2128 == null) {
                f2128 = new a();
            }
            aVar = f2128;
        }
        return aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m2608(Context context, boolean z) {
        boolean z2 = true;
        if (!f.m3536(context, "android.permission.INTERNET")) {
            throw new k();
        } else if (d.m3553() == null) {
            throw new j();
        } else if (z) {
            if (f.m3536(context, "android.permission.ACCESS_NETWORK_STATE") && g.m3537(context) == null) {
                z2 = false;
            }
            if (!z2) {
                throw new n();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2609() {
        if (m2610(true, true) != null) {
            f2129 = null;
            this.f2131.m3017();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized b m2610(boolean z, boolean z2) {
        b bVar = null;
        synchronized (this) {
            if (com.adincube.sdk.util.f.m3605() != null) {
                if (f2129 == null) {
                    f2129 = this.f2131.m3018();
                }
                if (f2129 != null && ((!f2129.f2633 || z) && (!f2129.f2635 || z2))) {
                    bVar = f2129;
                }
            }
        }
        return bVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Long m2611(boolean z) {
        b r0 = m2610(z, false);
        if (r0 == null) {
            return null;
        }
        return Long.valueOf(r0.f2636);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m2612(b bVar) {
        if (f2129 == null || f2129.f2636 < bVar.f2636) {
            f2129 = bVar;
            synchronized (this.f2130) {
                for (C0003a r0 : this.f2130) {
                    r0.m2613(bVar);
                }
            }
            this.f2131.m3019(bVar);
        }
    }
}
