package com.adincube.sdk.f.b.c;

import android.content.Context;
import com.adincube.sdk.f.b.c.a;
import com.adincube.sdk.f.b.c.h;
import com.adincube.sdk.f.b.c.k;
import com.adincube.sdk.g.c.d;
import com.adincube.sdk.g.c.e;
import com.adincube.sdk.util.ErrorReportingHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

abstract class b<T extends h> implements i {

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f2300;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Map<e, T> f2301 = new HashMap();

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f2302;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f2303 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private j f2304;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f2305 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f2306;

    /* renamed from: ٴ  reason: contains not printable characters */
    private long f2307;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private n f2308;

    /* renamed from: 连任  reason: contains not printable characters */
    protected final k.a f2309 = new k.a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2861(e eVar) {
            h r0 = b.this.m2855(eVar);
            r0.m2910(e.TIMEOUT);
            b.this.m2859(eVar, r0.m2906());
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    protected k f2310;

    /* renamed from: 麤  reason: contains not printable characters */
    protected final a.C0005a f2311 = new a.C0005a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2860(e eVar) {
            b.this.m2859(eVar, b.this.m2855(eVar).m2906());
        }
    };

    /* renamed from: 齉  reason: contains not printable characters */
    protected boolean f2312;

    /* renamed from: 龘  reason: contains not printable characters */
    protected d f2313;

    public b(d dVar, k kVar, j jVar, int i, int i2, long j) {
        this.f2310 = kVar;
        this.f2300 = System.currentTimeMillis();
        this.f2313 = dVar;
        this.f2304 = jVar;
        this.f2302 = i;
        this.f2306 = i2;
        this.f2307 = j;
        this.f2310 = kVar;
        this.f2310.m2927((i) this);
        this.f2310.m2928(this.f2309);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m2833() {
        if (!this.f2305 && !this.f2303) {
            this.f2305 = true;
            n nVar = this.f2308;
            if (nVar != null) {
                nVar.m2945(this.f2313);
            }
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m2834() {
        if (!this.f2305 && !this.f2303) {
            this.f2303 = true;
            n nVar = this.f2308;
            if (nVar != null) {
                nVar.m2944(this.f2313);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2835(e eVar, e eVar2, e eVar3) {
        Iterator<e> it2 = this.f2313.f2660.iterator();
        boolean z = false;
        while (it2.hasNext()) {
            e next = it2.next();
            if (!z) {
                z = next == eVar;
            } else if (next.f2667.equals(eVar.f2667)) {
                h r4 = m2855(next);
                if (r4.m2906() == eVar2) {
                    r4.m2910(eVar3);
                    m2859(next, r4.m2906());
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m2836() {
        int currentTimeMillis = (int) ((System.currentTimeMillis() - this.f2300) / this.f2307);
        if (currentTimeMillis < 0) {
            currentTimeMillis = 0;
        }
        return this.f2302 + currentTimeMillis > this.f2306 ? this.f2306 : currentTimeMillis + this.f2302;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m2837(e eVar) {
        n nVar = this.f2308;
        if (nVar != null) {
            nVar.m2946(this.f2313, eVar);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2838() {
        boolean z;
        for (e next : m2846()) {
            h r4 = m2855(next);
            if (r4.m2909() || r4.m2908()) {
                return false;
            }
            if (r4.m2907()) {
                try {
                    z = next.f2663.m3430();
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("Cannot check if network '" + next.f2667 + "' has been properly loaded. Mediation will continue", th);
                    ErrorReportingHelper.m3511("AbstractNetworkOrderLoader.isFirstNetworkBeenLoadedAndNoMoreLoaded", th);
                    z = false;
                }
                return !z;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean m2839() {
        ArrayList arrayList = new ArrayList();
        for (e next : m2846()) {
            h r4 = m2855(next);
            if (!arrayList.contains(next.f2667)) {
                if (r4.m2906() == e.LIAR) {
                    arrayList.add(next.f2667);
                }
                if (!r4.m2906().l) {
                    return false;
                }
                if (r4.m2906() == e.TIMEOUT) {
                    return false;
                }
            }
        }
        return true;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public synchronized void m2840() {
        this.f2312 = false;
        this.f2305 = false;
        this.f2303 = false;
        if (m2844()) {
            m2834();
        }
        if (m2850()) {
            m2833();
        }
        m2857();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Map<String, h> m2841() {
        HashMap hashMap = new HashMap();
        for (e next : m2846()) {
            h r4 = m2855(next);
            h hVar = (h) hashMap.get(next.f2667);
            if (hVar != null) {
                if (!(r4.m2906().m > hVar.m2906().m)) {
                }
            }
            hashMap.put(next.f2667, r4);
        }
        return hashMap;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m2842() {
        return this.f2312;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m2843() {
        ArrayList arrayList = new ArrayList();
        for (e next : m2846()) {
            h r3 = m2855(next);
            if (!arrayList.contains(next.f2667)) {
                if (r3.m2906() == e.LIAR) {
                    arrayList.add(next.f2667);
                }
                if (!r3.m2906().l) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m2844() {
        return m2839();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m2845() {
        this.f2312 = true;
        this.f2310.m2926();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public List<e> m2846() {
        return this.f2313.f2660;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m2847(e eVar) {
        Context r0;
        com.adincube.sdk.mediation.h r1;
        if (this.f2304 != null && (r0 = this.f2304.m2924()) != null && (r1 = eVar.f2663.m3429()) != null) {
            r1.m3481(r0);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract T m2848(e eVar);

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m2849(e eVar, e eVar2) {
        if (eVar2 == e.TIMEOUT) {
            m2835(eVar, e.WAITING, e.WAITING_FOR_RESPONSE);
        } else if (eVar2 != e.WAITING_FOR_RESPONSE) {
            m2835(eVar, e.WAITING_FOR_RESPONSE, e.WAITING);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract boolean m2850();

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public abstract List<e> m2851();

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m2852(e eVar) {
        n nVar = this.f2308;
        if (nVar != null) {
            nVar.m2947(eVar);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m2853() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m2854(e eVar);

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized T m2855(e eVar) {
        T t;
        t = (h) this.f2301.get(eVar);
        if (t == null) {
            t = m2848(eVar);
            this.f2301.put(eVar, t);
        }
        return t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public e m2856(e eVar) {
        e eVar2 = null;
        Iterator<e> it2 = m2846().iterator();
        while (it2.hasNext() && eVar2 == null) {
            e next = it2.next();
            if (m2855(next).m2906() != eVar) {
                next = eVar2;
            }
            eVar2 = next;
        }
        return eVar2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m2857() {
        if (m2846().isEmpty()) {
            m2834();
        } else if (!m2842() && !m2850()) {
            if (!this.f2310.m2931()) {
                this.f2310.m2925();
            }
            m2853();
            for (e r0 : m2851()) {
                m2854(r0);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2858(n nVar) {
        this.f2308 = nVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2859(e eVar, e eVar2) {
        n nVar;
        this.f2310.m2929(eVar);
        m2849(eVar, eVar2);
        if (!m2842() && (nVar = this.f2308) != null) {
            nVar.m2948(eVar, m2855(eVar).m2906());
            if (m2923()) {
                m2833();
            } else if (m2844()) {
                m2834();
            }
        }
        m2857();
    }
}
