package com.adincube.sdk.f.b.d;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import com.adincube.sdk.AdChoicesView;
import com.adincube.sdk.NativeAdMediaView;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.h.g;
import java.util.Arrays;
import java.util.List;

public final class a {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final List<Class<? extends View>> f2357 = Arrays.asList(new Class[]{NativeAdMediaView.class, AdChoicesView.class});

    /* renamed from: 靐  reason: contains not printable characters */
    public ViewGroup f2358;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public View.OnClickListener f2359 = new View.OnClickListener() {
        public final void onClick(View view) {
            try {
                a.this.m2957(view.getContext());
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("NativeAdClickManager#OnClickListener.onClick", th);
                ErrorReportingHelper.m3510("NativeAdClickManager#OnClickListener.onClick", a.this.f2360.ʼ(), b.NATIVE, th);
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public com.adincube.sdk.mediation.r.b f2360;

    public a(com.adincube.sdk.mediation.r.b bVar) {
        this.f2360 = bVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2956() {
        ViewGroup viewGroup;
        synchronized (this) {
            viewGroup = this.f2358;
            this.f2358 = null;
        }
        m2959(viewGroup);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2957(Context context) {
        if (!this.f2360.ﹶ) {
            this.f2360.齉.靐(context, this.f2360);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2958(Context context, String str) {
        if (context != null && str != null) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                if (!(context instanceof Activity)) {
                    intent.setFlags(268435456);
                }
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("NativeAdClickManager.openClickUrl", th);
                ErrorReportingHelper.m3510("NativeAdClickManager.openClickUrl", this.f2360.ʼ(), b.NATIVE, th);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2959(ViewGroup viewGroup) {
        if (viewGroup != null) {
            g.m3668(viewGroup, f2357, (g.a) new g.a() {
                /* renamed from: 龘  reason: contains not printable characters */
                public final void m2961(View view) {
                    view.setOnClickListener((View.OnClickListener) null);
                }
            });
        }
    }
}
