package com.adincube.sdk.f.b.e.b;

import android.content.Context;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.g.c.d;
import com.adincube.sdk.util.e;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.n;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import org.json.JSONObject;

public final class a implements b {

    /* renamed from: 靐  reason: contains not printable characters */
    private String f2424;

    /* renamed from: 龘  reason: contains not printable characters */
    private b f2425;

    public a(b bVar) {
        this(bVar, "no");
    }

    public a(b bVar, String str) {
        this.f2425 = bVar;
        this.f2424 = str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m3023(com.adincube.sdk.g.c.a aVar) {
        String str = this.f2424 + "-" + this.f2425.g;
        return aVar != null ? str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + aVar.e : str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized d m3024(com.adincube.sdk.g.c.a aVar) {
        d dVar = null;
        synchronized (this) {
            Context r1 = f.m3605();
            String r2 = n.m3695(r1, m3023(aVar));
            if (r2 != null) {
                try {
                    dVar = d.m3224(new JSONObject(e.m3574(r2)));
                } catch (Exception e) {
                    n.m3688(r1, m3023(aVar));
                }
            }
        }
        return dVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0005, code lost:
        if (r5.f2657 != false) goto L_0x0007;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void m3025(com.adincube.sdk.g.c.a r4, com.adincube.sdk.g.c.d r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            if (r5 == 0) goto L_0x0009
            boolean r0 = r5.f2657     // Catch:{ all -> 0x0023 }
            if (r0 == 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r3)
            return
        L_0x0009:
            android.content.Context r0 = com.adincube.sdk.util.f.m3605()     // Catch:{ all -> 0x0023 }
            if (r5 == 0) goto L_0x0026
            org.json.JSONObject r1 = r5.m3228()     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = com.adincube.sdk.util.e.m3575(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = r3.m3023(r4)     // Catch:{ all -> 0x0023 }
            com.adincube.sdk.util.n.m3697(r0, r2, r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0007
        L_0x0023:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0026:
            java.lang.String r1 = r3.m3023(r4)     // Catch:{ all -> 0x0023 }
            com.adincube.sdk.util.n.m3688(r0, r1)     // Catch:{ all -> 0x0023 }
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.f.b.e.b.a.m3025(com.adincube.sdk.g.c.a, com.adincube.sdk.g.c.d):void");
    }
}
