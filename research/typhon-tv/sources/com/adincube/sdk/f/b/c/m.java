package com.adincube.sdk.f.b.c;

import com.adincube.sdk.g.c.b;
import com.adincube.sdk.g.c.e;
import com.adincube.sdk.mediation.h;
import com.adincube.sdk.util.o;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class m {

    /* renamed from: 靐  reason: contains not printable characters */
    private static m f2348 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<String, b> f2349 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f2350 = true;

    /* renamed from: 龘  reason: contains not printable characters */
    final Set<i> f2351 = new HashSet();

    private m() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static m m2940() {
        if (f2348 == null) {
            synchronized (m.class) {
                f2348 = new m();
            }
        }
        return f2348;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized boolean m2941(b bVar, e eVar) {
        boolean z;
        h r0 = eVar.f2663.m3429();
        if (r0 == null || r0.m3476()) {
            z = false;
        } else {
            synchronized (this.f2349) {
                b bVar2 = this.f2349.get(eVar.f2667);
                z = (bVar2 == null || bVar2 == bVar) ? false : true;
            }
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2942(b bVar, String str) {
        synchronized (this.f2349) {
            if (this.f2349.get(str) == bVar) {
                this.f2349.remove(str);
                for (final i next : this.f2351) {
                    if (!this.f2350) {
                        next.m2920();
                    } else {
                        o.m3701("NetworkOrderLoadingConflictManager.load", (Runnable) new Runnable() {
                            public final void run() {
                                next.m2920();
                            }
                        });
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2943(b bVar, e eVar) {
        h r0 = eVar.f2663.m3429();
        if (r0 == null || r0.m3476()) {
            return true;
        }
        synchronized (this.f2349) {
            b bVar2 = this.f2349.get(eVar.f2667);
            if (bVar2 != null && bVar2 != bVar) {
                return false;
            }
            this.f2349.put(eVar.f2667, bVar);
            return true;
        }
    }
}
