package com.adincube.sdk.f.b.d;

import com.adincube.sdk.NativeAd;
import com.adincube.sdk.mediation.r.b;
import java.util.HashMap;
import java.util.Map;

public class f {

    /* renamed from: 龘  reason: contains not printable characters */
    private static f f2407 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private com.adincube.sdk.f.a f2408;

    /* renamed from: 齉  reason: contains not printable characters */
    private Map<a, Boolean> f2409;

    public enum a {
        ICON,
        COVER_IMAGE,
        COVER_VIDEO,
        AD_CHOICES_ICON;

        public static a a(NativeAd.Image.Type type) {
            switch (type) {
                case ICON:
                    return ICON;
                case COVER:
                    return COVER_IMAGE;
                default:
                    throw new IllegalStateException("Unknown image type '" + type + "'.");
            }
        }

        public static a[] a() {
            return (a[]) f2412.clone();
        }
    }

    private f() {
        this.f2408 = null;
        this.f2409 = new HashMap();
        this.f2408 = com.adincube.sdk.f.a.m2607();
        m3011(a.ICON, true);
        m3011(a.COVER_IMAGE, true);
        m3011(a.COVER_VIDEO, false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m3007(a aVar) {
        switch (aVar) {
            case ICON:
                return 25;
            case COVER_VIDEO:
                return 75;
            case AD_CHOICES_ICON:
                return 10;
            default:
                return 50;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static f m3008() {
        if (f2407 == null) {
            synchronized (f.class) {
                if (f2407 == null) {
                    f2407 = new f();
                }
            }
        }
        return f2407;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final com.adincube.sdk.g.d.a m3009(NativeAd.Image image) {
        a a2 = a.a(image.m2364());
        com.adincube.sdk.g.d.a aVar = new com.adincube.sdk.g.d.a(image.m2366());
        aVar.f2683 = m3007(a2);
        if (!(image.m2363() == null || image.m2365() == null)) {
            aVar.f2679 = image.m2363();
            aVar.f2678 = image.m2365();
        }
        if (image instanceof b.c) {
            aVar.f2692 = ((b.c) image).连任;
        }
        m3012((com.adincube.sdk.g.d.b) aVar);
        return aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final com.adincube.sdk.g.d.b m3010(b.d dVar) {
        com.adincube.sdk.g.d.b bVar = new com.adincube.sdk.g.d.b(dVar.龘);
        bVar.f2694 = Integer.valueOf(m3007(a.COVER_VIDEO));
        bVar.f2692 = dVar.靐;
        m3012(bVar);
        return bVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3011(a aVar, boolean z) {
        this.f2409.put(aVar, Boolean.valueOf(z));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3012(com.adincube.sdk.g.d.b bVar) {
        com.adincube.sdk.g.b.b r0 = this.f2408.m2610(true, true);
        bVar.f2687 = r0.f2627;
        Integer num = r0.f2629;
        if (num == null || num.intValue() <= 0) {
            bVar.f2685 = null;
        } else {
            bVar.f2685 = num;
        }
        Integer num2 = r0.f2618;
        if (num2 == null || num2.intValue() <= 0) {
            bVar.f2686 = null;
        } else {
            bVar.f2686 = num2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3013(a aVar) {
        Boolean bool = this.f2409.get(aVar);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }
}
