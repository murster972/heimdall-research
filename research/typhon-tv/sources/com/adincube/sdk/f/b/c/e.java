package com.adincube.sdk.f.b.c;

public enum e {
    WAITING("WAITING", false, 5),
    LOADING("LOADING", false, 6),
    LOADED("LOADED", false, 8),
    NO_MORE_INVENTORY("NO_MORE_INVENTORY", true, 1),
    ERROR("ERROR", true, 3),
    TIMEOUT("TIMEOUT", true, 2),
    WAITING_FOR_RESPONSE("WAITING_FOR_RESPONSE", true, 0),
    WAITING_FOR_OTHER_AD_TYPE("WAITING_FOR_OTHER_AD_TYPE", false, 7),
    EXPIRED("EXPIRED", true, 4),
    LIAR("LIAR", true, 9);
    
    public String k;
    boolean l;
    int m;

    private e(String str, boolean z, int i) {
        this.k = str;
        this.l = z;
        this.m = i;
    }

    public static e[] a() {
        return (e[]) f2324.clone();
    }
}
