package com.adincube.sdk.f.b.a.a;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.adincube.sdk.BannerView;
import com.adincube.sdk.c.a.c;
import com.adincube.sdk.c.a.p;
import com.adincube.sdk.f.a;
import com.adincube.sdk.f.b.a.b;
import com.adincube.sdk.util.ErrorReportingHelper;

public final class e extends a {

    /* renamed from: ʾ  reason: contains not printable characters */
    private b f2227;

    /* renamed from: ʿ  reason: contains not printable characters */
    private b f2228 = new b() {
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m2724(boolean z) {
            if (!z) {
                e.this.f2190.m2705((Integer) null, true);
                if (e.this.f2191 != null) {
                    e.this.f2190.m2705(e.this.f2191, false);
                } else {
                    e.this.f2190.m2705(0, false);
                }
                e.m2721(e.this);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2725(boolean z) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2726(boolean z, c cVar) {
        }
    };

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f2229 = true;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f2230 = false;

    public e(BannerView bannerView, c cVar, a aVar, com.adincube.sdk.f.a.b bVar, com.adincube.sdk.f.b.a.a aVar2, com.adincube.sdk.f.b.a.c cVar2, b bVar2) {
        super(bannerView, cVar, aVar, bVar, aVar2, cVar2);
        this.f2227 = bVar2;
        aVar2.m2661(this.f2228);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m2720() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                e.m2721(e.this);
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m2721(e eVar) {
        c e = null;
        try {
            if (eVar.f2192.getChildCount() > 0 || eVar.f2188.f2149) {
                return;
            }
            if (eVar.f2192.getVisibility() != 0) {
                eVar.f2229 = true;
            } else if (eVar.f2192.getMeasuredWidth() == 0) {
                eVar.f2230 = true;
            } else {
                eVar.m2676();
                View r1 = eVar.f2188.m2745();
                eVar.f2192.addView(r1, eVar.m2680(r1));
                eVar.f2189 = eVar.f2188.f2236;
                eVar.f2187.m2659(false);
                eVar.f2227.m2695();
                if (e != null) {
                    eVar.f2190.m2705(8, true);
                    eVar.m2683(e);
                }
            }
        } catch (c e2) {
            e = e2;
        } catch (Throwable th) {
            p pVar = new p(th);
            ErrorReportingHelper.m3506("BannerViewShowManager.show", com.adincube.sdk.g.c.b.BANNER, th);
            e = pVar;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m2722() {
        if (this.f2230) {
            this.f2230 = false;
            m2720();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2723(int i) {
        super.m2682(i);
        if (i == 0 && this.f2229) {
            this.f2229 = false;
            m2720();
        }
    }
}
