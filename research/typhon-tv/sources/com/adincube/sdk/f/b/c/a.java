package com.adincube.sdk.f.b.c;

import android.os.Handler;
import android.os.Looper;
import com.adincube.sdk.g.c.e;
import com.adincube.sdk.mediation.i;

public abstract class a implements com.adincube.sdk.mediation.a {

    /* renamed from: 靐  reason: contains not printable characters */
    C0005a f2293 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f2294;

    /* renamed from: 齉  reason: contains not printable characters */
    private i f2295;

    /* renamed from: 龘  reason: contains not printable characters */
    protected e f2296;

    /* renamed from: com.adincube.sdk.f.b.c.a$a  reason: collision with other inner class name */
    public interface C0005a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2832(e eVar);
    }

    public a(i iVar, e eVar, C0005a aVar, boolean z) {
        this.f2295 = iVar;
        this.f2296 = eVar;
        this.f2293 = aVar;
        this.f2294 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2830(final e eVar) {
        if (this.f2294) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        a.this.f2293.m2832(eVar);
                    } catch (Throwable th) {
                        com.adincube.sdk.util.a.m3513("DefaultAdLoadingStateListener.onStatusChangedForNetwork", th);
                    }
                }
            });
        } else {
            this.f2293.m2832(eVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2831(i iVar) {
        if (this.f2296.f2663 != null && !this.f2295.m2915()) {
            h r0 = this.f2295.m2918(this.f2296);
            switch (iVar.f2950) {
                case NO_MORE_INVENTORY:
                    r0.m2910(e.NO_MORE_INVENTORY);
                    break;
                case NETWORK:
                    if (iVar.f2952 != null) {
                        Object[] objArr = {iVar.f2952.m3473(), iVar.m3485()};
                    }
                    r0.m2910(e.ERROR);
                    break;
                case INTEGRATION:
                case UNKNOWN:
                    if (iVar.f2952 != null) {
                        com.adincube.sdk.util.a.m3513("Unexpected error occurred when loading ad for network '" + iVar.f2952.m3473() + "'. Category: " + iVar.f2950.e + ". Error code: " + iVar.m3485(), new Object[0]);
                    }
                    r0.m2910(e.ERROR);
                    break;
            }
            m2830(this.f2296);
        }
    }
}
