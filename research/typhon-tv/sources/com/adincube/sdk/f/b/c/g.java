package com.adincube.sdk.f.b.c;

import com.adincube.sdk.f.b.c.a;
import com.adincube.sdk.g.c.d;
import com.adincube.sdk.g.c.e;
import com.adincube.sdk.util.ErrorReportingHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class g extends b<f> {

    /* renamed from: ʻ  reason: contains not printable characters */
    d f2326;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f2327;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f2328;

    public class a extends a {
        public a(i iVar, e eVar, a.C0005a aVar, boolean z) {
            super(iVar, eVar, aVar, z);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2905() {
            if (this.f2296.f2663 != null && !g.this.f2312) {
                g.this.f2310.m2929(this.f2296);
                try {
                    f fVar = (f) g.this.m2855(this.f2296);
                    int size = this.f2296.f2663.ʽ().size();
                    fVar.f2325 = size;
                    if (size > 0) {
                        fVar.m2910(e.LOADED);
                    } else {
                        fVar.m2910(e.NO_MORE_INVENTORY);
                    }
                    m2830(this.f2296);
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("Error caught when retrieving native ad for '%s'. Mediation will continue.", this.f2296.f2667);
                    ErrorReportingHelper.m3506("NativeAdNetworkOrderLoader#NativeAdLoadingStateListener.onAdLoaded", g.this.f2326.f2662, th);
                }
            }
        }
    }

    public g(d dVar, int i, k kVar, j jVar, int i2, int i3, long j) {
        this(dVar, i, kVar, jVar, i2, i3, j, (byte) 0);
    }

    private g(d dVar, int i, k kVar, j jVar, int i2, int i3, long j, byte b) {
        super(dVar, kVar, jVar, i2, i3, j);
        this.f2326 = dVar;
        this.f2327 = i;
        this.f2328 = true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ int m2887() {
        return super.m2836();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ boolean m2888() {
        return super.m2838();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m2889() {
        super.m2840();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Map m2890() {
        return super.m2841();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ boolean m2891() {
        return super.m2842();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ boolean m2892() {
        return super.m2843();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m2893() {
        return m2839();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m2894() {
        super.m2845();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final /* synthetic */ h m2895(e eVar) {
        return new f(eVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m2896() {
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        for (e r0 : m2846()) {
            f fVar = (f) m2855(r0);
            if (fVar.m2908()) {
                i2++;
            }
            if (fVar.m2909()) {
                i++;
            }
            int r02 = fVar.m2886() + i3;
            if (r02 >= this.f2327) {
                return i == 0 && i2 == 0;
            }
            i3 = r02;
        }
        return i2 == 0 && i == 0 && i3 > 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final List<e> m2897() {
        ArrayList arrayList = new ArrayList(m2846());
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int r5 = super.m2836();
        Iterator it2 = arrayList.iterator();
        int i = 0;
        while (it2.hasNext()) {
            e eVar = (e) it2.next();
            h r7 = m2855(eVar);
            if (arrayList2.contains(eVar.f2667) || arrayList3.contains(eVar.f2667)) {
                it2.remove();
            } else if (eVar.f2665.m3466(this.f2326.f2662, arrayList2)) {
                it2.remove();
            } else if (r7.m2907()) {
                it2.remove();
            } else if (this.f2327 <= 0) {
                it2.remove();
            } else if (r7.m2909()) {
                i++;
                arrayList2.add(eVar.f2667);
                it2.remove();
            } else if (r7.m2906().l) {
                if (r7.m2906() == e.TIMEOUT) {
                    arrayList3.add(eVar.f2667);
                }
                it2.remove();
            } else if (i >= r5) {
                it2.remove();
            } else {
                i++;
                arrayList2.add(eVar.f2667);
            }
        }
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m2898() {
        super.m2853();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2899(e eVar) {
        e next;
        h r7 = m2855(eVar);
        r7.m2910(e.LOADING);
        Iterator<e> it2 = m2846().iterator();
        int i = 0;
        while (it2.hasNext() && i < this.f2327 && (next = it2.next()) != eVar) {
            i = ((f) m2855(next)).m2886() + i;
        }
        int i2 = this.f2327 - i;
        if (!(eVar.f2665 == null || eVar.f2665.f2944 == 0)) {
            i2 = eVar.f2665.f2944;
        }
        int min = Math.min(this.f2327 - i, i2);
        if (min > 0) {
            try {
                a aVar = new a(this, eVar, this.f2311, this.f2328);
                eVar.f2663.靐(min);
                eVar.f2663.m3434((com.adincube.sdk.mediation.a) aVar);
                m2847(eVar);
                eVar.f2663.m3432();
            } catch (com.adincube.sdk.c.b.a e) {
                com.adincube.sdk.util.a.m3513("Error caught when loading ad from network '%s'. Mediation will continue.", eVar.f2667, e);
                r7.m2910(e.ERROR);
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("Error caught when loading ad from network '%s'. Mediation will continue.", eVar.f2667, th);
                ErrorReportingHelper.m3510("NetworkOrderLoader.loadNetwork", eVar.f2667, this.f2326.f2662, th);
                r7.m2910(e.ERROR);
            }
            if (r7.m2906() == e.LOADING) {
                this.f2310.m2930(eVar, eVar.f2665.m3464(this.f2326.f2662));
            }
            m2852(eVar);
            if (r7.m2906() == e.ERROR) {
                m2859(eVar, r7.m2906());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ e m2900(e eVar) {
        return super.m2856(eVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m2901() {
        super.m2857();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m2902(n nVar) {
        super.m2858(nVar);
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final void m2903() {
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final boolean m2904() {
        return m2896();
    }
}
