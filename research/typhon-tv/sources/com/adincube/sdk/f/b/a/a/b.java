package com.adincube.sdk.f.b.a.a;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.adincube.sdk.BannerView;
import com.adincube.sdk.c.a.c;
import com.adincube.sdk.c.a.p;
import com.adincube.sdk.f.g.a;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a.a;
import com.adincube.sdk.util.o;
import com.adincube.sdk.util.s;

public final class b extends a {

    /* renamed from: ʾ  reason: contains not printable characters */
    boolean f2195 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    boolean f2196 = false;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f2197 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    boolean f2198 = false;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f2199 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f2200 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f2201 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    private long f2202 = 0;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f2203 = false;

    /* renamed from: י  reason: contains not printable characters */
    private a f2204 = null;

    /* renamed from: ـ  reason: contains not printable characters */
    private com.adincube.sdk.f.b.a.b f2205 = new com.adincube.sdk.f.b.a.b() {
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m2700(boolean z) {
            if (z) {
                b.m2691(b.this);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2701(boolean z) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2702(boolean z, c cVar) {
            if (z && "NO_INTERNET".equals(cVar.a)) {
                b.this.m2687();
            }
        }
    };

    /* renamed from: ᐧ  reason: contains not printable characters */
    com.adincube.sdk.f.g.a f2206 = null;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final a.C0020a f2207 = new a.C0020a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2703() {
            try {
                o.m3701("BannerView#OnAutoRefreshTimerFinishedListener.onAutoRefreshFinished", (Runnable) new Runnable() {
                    public final void run() {
                        b.m2690(b.this);
                    }
                });
            } catch (Throwable th) {
                new Object[1][0] = th;
                ErrorReportingHelper.m3504("BannerView#OnAutoRefreshTimerFinishedListener.onAutoRefreshFinished", com.adincube.sdk.g.c.b.BANNER, b.this.f2188.m2738(), (Boolean) true, th);
            }
        }
    };

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final a.b f2208 = new a.b() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2704(View view, boolean z) {
            try {
                if (view == b.this.f2192 && z) {
                    b.this.f2206.m3166((View) b.this.f2192);
                    if (b.this.f2198) {
                        b.this.f2198 = false;
                        b.this.m2687();
                    }
                    if (b.this.f2195) {
                        b.this.f2195 = false;
                        b.m2690(b.this);
                    }
                    if (b.this.f2196) {
                        b.this.f2196 = false;
                        b.m2691(b.this);
                    }
                }
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("BannerViewAutoRefreshManager#VisibilityChangeListener.onVisibilityChanged", th);
                ErrorReportingHelper.m3506("BannerViewAutoRefreshManager#VisibilityChangeListener.onVisibilityChanged", com.adincube.sdk.g.c.b.BANNER, th);
            }
        }
    };

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f2209 = false;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f2210 = false;

    public b(BannerView bannerView, c cVar, com.adincube.sdk.f.a aVar, com.adincube.sdk.f.a.b bVar, com.adincube.sdk.f.g.a aVar2, com.adincube.sdk.f.b.a.a aVar3, com.adincube.sdk.f.b.a.c cVar2) {
        super(bannerView, cVar, aVar, bVar, aVar3, cVar2);
        this.f2206 = aVar2;
        this.f2206.m3168(this.f2208);
        aVar3.m2661(this.f2205);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m2686() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                b.m2691(b.this);
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m2687() {
        com.adincube.sdk.g.b.b r0 = this.f2185.m2610(true, true);
        if (this.f2203) {
            if (this.f2192.getWindowToken() == null) {
                this.f2201 = true;
            } else if (!s.m3713(this.f2192.getContext())) {
                this.f2209 = true;
            } else if (this.f2193 != 0) {
                this.f2210 = true;
            } else if (this.f2192.getVisibility() != 0) {
                this.f2200 = true;
            } else if (this.f2202 != -1 || m2692(r0)) {
                long j = this.f2202;
                if (j <= 0) {
                    j = this.f2185.m2610(true, true).f2637;
                }
                if (j > 0) {
                    new Object[1][0] = Long.valueOf(j);
                    com.adincube.sdk.util.a.a aVar = new com.adincube.sdk.util.a.a(this.f2194, j);
                    aVar.f2970 = this.f2207;
                    aVar.start();
                    synchronized (this) {
                        this.f2204 = aVar;
                    }
                }
            } else {
                this.f2198 = true;
                this.f2206.m3167((View) this.f2192, r0.f2640);
            }
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m2688() {
        com.adincube.sdk.util.a.a aVar = this.f2204;
        if (aVar != null) {
            this.f2202 = aVar.f2972;
            new Object[1][0] = Long.valueOf(this.f2202);
            aVar.f2970 = null;
            aVar.cancel();
            synchronized (this) {
                if (aVar == this.f2204) {
                    this.f2204 = null;
                }
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static /* synthetic */ void m2690(b bVar) {
        try {
            com.adincube.sdk.g.b.b r0 = bVar.f2185.m2610(true, true);
            if (!bVar.f2188.f2149) {
                if (!bVar.m2692(r0)) {
                    bVar.f2195 = true;
                    bVar.f2206.m3167((View) bVar.f2192, r0.f2640);
                    return;
                }
                bVar.f2188.m2644();
            }
        } catch (c e) {
        } catch (Throwable th) {
            new Object[1][0] = th;
            ErrorReportingHelper.m3504("BannerView#OnAutoRefreshTimerFinishedListener.onAutoRefreshFinished", com.adincube.sdk.g.c.b.BANNER, bVar.f2188.m2738(), (Boolean) true, th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m2691(b bVar) {
        c e = null;
        com.adincube.sdk.g.b.b r1 = bVar.f2185.m2610(true, true);
        try {
            if (!bVar.f2188.f2149) {
                if (bVar.f2193 != 0) {
                    bVar.f2197 = true;
                } else if (bVar.f2192.getVisibility() != 0) {
                    bVar.f2199 = true;
                } else if (!bVar.m2692(r1)) {
                    bVar.f2196 = true;
                    bVar.f2206.m3167((View) bVar.f2192, r1.f2640);
                } else {
                    bVar.m2676();
                    bVar.f2192.removeAllViewsInLayout();
                    View r12 = bVar.f2188.m2745();
                    bVar.f2192.addView(r12, 0, bVar.m2680(r12));
                    bVar.f2189 = bVar.f2188.f2236;
                    bVar.f2187.m2659(true);
                    bVar.f2202 = -1;
                    if (!s.m3713(bVar.f2192.getContext()) || bVar.f2193 != 0) {
                        bVar.f2210 = true;
                    } else {
                        bVar.m2687();
                    }
                    if (e != null) {
                        bVar.f2190.m2705(8, true);
                        bVar.m2683(e);
                    }
                }
            }
        } catch (c e2) {
            e = e2;
        } catch (Throwable th) {
            p pVar = new p(th);
            ErrorReportingHelper.m3506("BannerView.show", com.adincube.sdk.g.c.b.BANNER, th);
            e = pVar;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m2692(com.adincube.sdk.g.b.b bVar) {
        Double valueOf = Double.valueOf(bVar.f2640);
        if (valueOf.doubleValue() <= 0.1d) {
            return true;
        }
        return com.adincube.sdk.f.g.a.m3154(this.f2192, valueOf.doubleValue());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m2693() {
        m2688();
        m2687();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m2694() {
        this.f2206.m3166((View) this.f2192);
        this.f2206.m3165(this.f2208);
        m2688();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m2695() {
        this.f2202 = -1;
        this.f2203 = true;
        m2687();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2696(int i) {
        super.m2677(i);
        if (i == 0) {
            if (this.f2210) {
                this.f2210 = false;
                m2687();
            }
            if (this.f2197) {
                this.f2197 = false;
                m2686();
                return;
            }
            return;
        }
        m2688();
        m2687();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m2697() {
        if (this.f2201) {
            this.f2201 = false;
            m2687();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2698(int i) {
        super.m2682(i);
        if (i == 0) {
            if (this.f2200) {
                this.f2200 = false;
                m2687();
            }
            if (this.f2199) {
                this.f2199 = false;
                m2686();
                return;
            }
            return;
        }
        m2688();
        m2687();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2699(boolean z) {
        if (!z) {
            m2688();
            m2687();
        } else if (this.f2209) {
            this.f2209 = false;
            m2687();
        }
    }
}
