package com.adincube.sdk.f.b;

import android.app.Activity;
import android.content.Context;
import com.adincube.sdk.c.a.h;
import com.adincube.sdk.c.a.o;
import com.adincube.sdk.c.b.g;
import com.adincube.sdk.f.b.c.m;
import com.adincube.sdk.g.b.c;
import com.adincube.sdk.g.c.d;
import com.adincube.sdk.g.c.e;
import com.adincube.sdk.mediation.k;
import com.adincube.sdk.mediation.l;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.f;

public abstract class b extends a implements k {

    /* renamed from: ʿ  reason: contains not printable characters */
    public String f2242 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private com.adincube.sdk.mediation.b f2243 = null;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private long f2244 = 0;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private String f2245 = null;

    public class a extends Exception {
        public a() {
        }
    }

    public b(com.adincube.sdk.f.a aVar, com.adincube.sdk.util.e.b bVar, d dVar, h hVar, com.adincube.sdk.f.b.e.b.b bVar2, j jVar, m mVar, f fVar, g gVar, k kVar) {
        super(aVar, bVar, dVar, hVar, bVar2, jVar, mVar, fVar, gVar, kVar);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m2747() {
        this.f2156.m2950(false, new com.adincube.sdk.c.a.m());
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m2748() {
        m2750();
        this.f2160.m3052(false, (String) null);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m2749() {
        try {
            super.m2628();
        } catch (o e) {
            com.adincube.sdk.util.a.m3512("Previous network is still on screen or did not finish properly. Requesting new ad.", new Object[0]);
            m2651((d) null, false);
            throw e;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m2750() {
        if (this.f2245 != null) {
            com.adincube.sdk.util.a.m3512("Previous ad from '" + this.f2245 + "' has not been properly hidden. Deactivating network until app is relaunched.", new Object[0]);
            this.f2157.m3033(this.f2245);
            this.f2245 = null;
            if (this.f2243 != null) {
                m2654(this.f2243);
                this.f2243 = null;
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public abstract c m2751();

    /* renamed from: 连任  reason: contains not printable characters */
    public final Context m2752() {
        return f.m3602();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m2753(d dVar) {
        super.m2636(dVar);
        try {
            m2747();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AbstractShowableNetworkMediationManager.onAllNetworksFailed", th);
            ErrorReportingHelper.m3505("AbstractShowableNetworkMediationManager.onAllNetworksFailed", m2646(), m2637(), th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public com.adincube.sdk.mediation.b m2754(e eVar) {
        boolean z;
        try {
            m2758(eVar.f2663);
            l lVar = (l) eVar.f2663;
            lVar.m3496(f.m3602());
            eVar.f2663.m3429().m3481((Context) f.m3602());
            lVar.m3495();
            this.f2245 = eVar.f2667;
            this.f2243 = eVar.f2663;
            this.f2242 = this.f2245;
            return eVar.f2663;
        } catch (g e) {
            e.a();
            ErrorReportingHelper.m3509("AbstractShowableNetworkMediationManager.showNetworkOrderElement", eVar.f2667, m2646(), m2637(), (Throwable) e);
            z = false;
        } catch (com.adincube.sdk.c.b.a e2) {
            e2.a();
            throw new a();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("Error caught while displaying ad from network '%s'. Impression has been dismissed.", eVar.f2667, th);
            ErrorReportingHelper.m3509("AbstractNetworkMediationManager.show", eVar.f2667, m2646(), m2637(), th);
            z = true;
        }
        throw new a();
        if (!z) {
            return null;
        }
        try {
            this.f2157.m3032(eVar.f2663);
            m2651((d) null, false);
        } catch (Throwable th2) {
            new Object[1][0] = th2;
            ErrorReportingHelper.m3509("AbstractShowableNetworkMediationManager.showNetworkOrderElement", eVar.f2667, m2646(), m2637(), th2);
        }
        throw new a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m2755() {
        m2747();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m2756(com.adincube.sdk.mediation.b bVar) {
        try {
            this.f2244 = System.currentTimeMillis();
            m2654(bVar);
            if (this.f2243 == bVar) {
                this.f2245 = null;
                this.f2243 = null;
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AbstractShowableNetworkMediationManager.onAdHidden", th);
            ErrorReportingHelper.m3505("AbstractShowableNetworkMediationManager.onAdHidden", m2646(), m2637(), th);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m2757() {
        if (this.f2245 == null) {
            super.m2644();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m2758(com.adincube.sdk.mediation.b bVar);

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2759(boolean z, boolean z2) {
        com.adincube.sdk.mediation.b bVar = null;
        Activity r1 = f.m3602();
        if (f.m3603(r1) || f.m3607(r1)) {
            com.adincube.sdk.util.a.m3512("Activity provided in show is finishing or destroyed. This may cause networks to fail to show an ad.", new Object[0]);
        }
        if (!z2) {
            m2762();
        }
        try {
            d r12 = m2632();
            if (this.f2148.m2610(true, true) == null) {
                throw new com.adincube.sdk.c.a.l();
            }
            com.adincube.sdk.g.c.f r2 = r12.m3227();
            while (bVar == null) {
                try {
                    if (!r2.hasNext()) {
                        break;
                    }
                    e r3 = r2.next();
                    bVar = m2754(r3);
                    if (bVar != null) {
                        this.f2154.m3059(r12, bVar, r3.f2664);
                        this.f2155.m3598(r3.f2667, m2646(), m2637());
                    }
                } catch (a e) {
                }
            }
            if (r2.f2672.size() > 0) {
                m2650(r12, r2.f2672);
            }
            if (bVar != null) {
                this.f2147.m2623(m2751(), r12);
                com.adincube.sdk.util.a.m3515(new com.adincube.sdk.util.d.c(m2646(), bVar).m3573(), new Object[0]);
                m2649(r12, bVar);
            } else if (r12.f2656.m2916()) {
                this.f2147.m2623(m2751(), r12);
                this.f2147.m2624(m2751(), r12);
                throw new com.adincube.sdk.c.a.m();
            } else {
                if (r2.f2672.size() > 0) {
                    r12.f2656.m2913();
                }
                if (z) {
                    this.f2147.m2623(m2751(), r12);
                }
                throw new h();
            }
        } catch (o e2) {
            com.adincube.sdk.util.a.m3512("Previous network is still on screen or did not finish properly. Requesting new ad.", new Object[0]);
            m2651((d) null, false);
            throw e2;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2760(d dVar) {
        boolean z;
        boolean z2 = true;
        if (!d.m3225(dVar)) {
            return false;
        }
        boolean r2 = dVar.f2656.m2923();
        com.adincube.sdk.g.b.b r3 = this.f2148.m2610(true, true);
        if (r3 == null || !r3.m3223(m2646())) {
            z = r2;
        } else {
            if (dVar.f2656.m2912()) {
                z2 = false;
            }
            z = z2 & r2;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2761(d dVar, com.adincube.sdk.g.b.b bVar) {
        boolean r0 = m2640(dVar, bVar);
        if (r0) {
            return r0;
        }
        if (!bVar.m3223(m2646()) || !dVar.f2656.m2912()) {
            return false;
        }
        m2638(dVar);
        return true;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public void m2762() {
        long currentTimeMillis = System.currentTimeMillis();
        com.adincube.sdk.g.b.b r2 = this.f2148.m2610(true, true);
        if (r2 != null) {
            if (currentTimeMillis < this.f2244) {
                this.f2244 = 0;
            }
            if (currentTimeMillis < r2.f2613 + this.f2244) {
                m2763();
            }
            this.f2244 = System.currentTimeMillis();
        }
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public abstract void m2763();
}
