package com.adincube.sdk.f.b.a;

import com.adincube.sdk.AdinCubeBannerEventListener;
import com.adincube.sdk.BannerView;
import com.adincube.sdk.c.a.c;
import com.adincube.sdk.f.b.c.c;
import com.adincube.sdk.f.b.d;
import com.adincube.sdk.util.o;
import java.util.HashSet;
import java.util.Set;

public final class a implements d {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f2167 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private Set<b> f2168 = new HashSet();

    /* renamed from: 靐  reason: contains not printable characters */
    public AdinCubeBannerEventListener f2169 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private com.adincube.sdk.f.a f2170 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    int f2171 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    BannerView f2172 = null;

    public a(BannerView bannerView, com.adincube.sdk.f.a aVar) {
        this.f2172 = bannerView;
        this.f2170 = aVar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2659(boolean z) {
        if (!z || this.f2170.m2610(true, true).f2623) {
            o.m3699(this.f2169, new com.adincube.sdk.util.c.a<AdinCubeBannerEventListener>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public final /* synthetic */ void m2670(Object obj) {
                    ((AdinCubeBannerEventListener) obj).m2329(a.this.f2172);
                }
            });
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2660(final boolean z, final c cVar) {
        new StringBuilder("onLoadError - ").append(cVar.a);
        o.m3703("BannerEventListenerManager.onLoadError", this.f2168, new com.adincube.sdk.util.c.a<b>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* bridge */ /* synthetic */ void m2667(Object obj) {
                ((b) obj).m2729(z, cVar);
            }
        });
        if (!z) {
            o.m3699(this.f2169, new com.adincube.sdk.util.c.a<AdinCubeBannerEventListener>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public final /* synthetic */ void m2668(Object obj) {
                    ((AdinCubeBannerEventListener) obj).m2333(a.this.f2172, cVar.a);
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2661(b bVar) {
        this.f2168.add(bVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2662(final boolean z) {
        o.m3703("BannerEventListenerManager.onAdCached", this.f2168, new com.adincube.sdk.util.c.a<b>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* synthetic */ void m2665(Object obj) {
                b bVar = (b) obj;
                if (a.this.f2171 == c.a.a) {
                    bVar.m2728(z);
                } else if (a.this.f2171 == c.a.b) {
                    bVar.m2727(z);
                }
            }
        });
        if (!this.f2167 && !z) {
            this.f2167 = true;
            o.m3699(this.f2169, new com.adincube.sdk.util.c.a<AdinCubeBannerEventListener>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public final /* synthetic */ void m2666(Object obj) {
                    ((AdinCubeBannerEventListener) obj).m2332(a.this.f2172);
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2663(boolean z, com.adincube.sdk.c.a.c cVar) {
        m2660(z, cVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2664() {
        return this.f2169 != null;
    }
}
