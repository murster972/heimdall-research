package com.adincube.sdk.f.b.b;

import android.app.Activity;
import android.content.Context;
import com.adincube.sdk.AdinCubeInterstitialEventListener;
import com.adincube.sdk.c.a.c;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.o;

public final class a {

    /* renamed from: 连任  reason: contains not printable characters */
    private static long f2246 = 0;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Activity f2247 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private com.adincube.sdk.f.a f2248 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    private c f2249 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f2250 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    public C0004a f2251 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public AdinCubeInterstitialEventListener f2252 = new AdinCubeInterstitialEventListener() {
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m2771() {
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final void m2772() {
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m2773() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2774() {
            a.this.m2767();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2775(String str) {
        }
    };

    /* renamed from: 齉  reason: contains not printable characters */
    public long f2253 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    public b f2254 = null;

    /* renamed from: com.adincube.sdk.f.b.b.a$a  reason: collision with other inner class name */
    public interface C0004a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2776();
    }

    public a(Activity activity, com.adincube.sdk.f.a aVar, b bVar, c cVar) {
        this.f2247 = activity;
        this.f2248 = aVar;
        this.f2254 = bVar;
        this.f2249 = cVar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static long m2764() {
        return f2246;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m2766() {
        try {
            this.f2250 = true;
            this.f2247 = null;
            b bVar = this.f2254;
            AdinCubeInterstitialEventListener adinCubeInterstitialEventListener = this.f2252;
            synchronized (bVar.f2258) {
                bVar.f2258.remove(adinCubeInterstitialEventListener);
            }
            if (this.f2251 != null) {
                this.f2251.m2776();
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("DeferredInterstitial.finish", th);
            ErrorReportingHelper.m3506("DeferredInterstitial.finish", b.INTERSTITIAL, th);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m2767() {
        c e = null;
        try {
            if (this.f2250) {
                m2766();
            } else if (f.m3604(this.f2247)) {
                m2766();
                m2766();
            } else {
                f.m3606((Context) this.f2247);
                this.f2249.m2759(true, true);
                f2246 = System.currentTimeMillis();
                m2766();
                if (e != null) {
                    m2768(e);
                }
            }
        } catch (c e2) {
            e = e2;
            m2766();
        } catch (Throwable th) {
            m2766();
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m2768(c cVar) {
        try {
            cVar.a();
            b.m2777().m2780(cVar);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("DeferredInterstitial.displayError", th);
            ErrorReportingHelper.m3506("DeferredInterstitial.displayError", b.INTERSTITIAL, th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2770() {
        try {
            if (!this.f2250) {
                if (f.m3604(this.f2247)) {
                    m2766();
                    return;
                }
                long r0 = com.adincube.sdk.g.b.b.m3216(this.f2248.m2610(true, true));
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis < this.f2253 || r0 + this.f2253 < currentTimeMillis) {
                    new Object[1][0] = Long.valueOf(currentTimeMillis - this.f2253);
                    m2767();
                    return;
                }
                o.m3702("HandlerUtil.dispatchOnUiThreadDelayed", (Runnable) new Runnable() {
                    public final void run() {
                        a.this.m2770();
                    }
                }, 1000);
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("DeferredInterstitial.waitUntilShownOrTimeout", th);
            ErrorReportingHelper.m3506("DeferredInterstitial.waitUntilShownOrTimeout", b.INTERSTITIAL, th);
        }
    }
}
