package com.adincube.sdk.f.b.c;

import com.adincube.sdk.g.c.d;
import com.adincube.sdk.g.c.e;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class c extends d {

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f2316 = a.a;

    public enum a {
        ;

        static {
            a = 1;
            b = 2;
            f2317 = new int[]{a, b};
        }
    }

    public c(d dVar, k kVar, j jVar, m mVar, int i, int i2, long j) {
        super(dVar, kVar, jVar, mVar, i, i2, j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final boolean m2862() {
        return m2839();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final List<e> m2863() {
        ArrayList arrayList = new ArrayList(super.m2846());
        if (this.f2316 == a.a) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                com.adincube.sdk.mediation.d dVar = ((e) it2.next()).f2665;
                if (dVar == null || !dVar.f2947) {
                    it2.remove();
                }
            }
        }
        return arrayList;
    }
}
