package com.adincube.sdk.f.b.a.a;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.adincube.sdk.AdinCubeBannerEventListener;
import com.adincube.sdk.BannerView;
import com.adincube.sdk.f.a.b;
import com.adincube.sdk.f.b.a.c;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.b.j;
import com.adincube.sdk.util.o;

public abstract class a {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected com.adincube.sdk.f.a f2185;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected b f2186;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected com.adincube.sdk.f.b.a.a f2187;

    /* renamed from: ˑ  reason: contains not printable characters */
    protected c f2188;

    /* renamed from: ٴ  reason: contains not printable characters */
    protected String f2189 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    protected c f2190;

    /* renamed from: 靐  reason: contains not printable characters */
    protected Integer f2191 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    protected BannerView f2192;

    /* renamed from: 齉  reason: contains not printable characters */
    protected int f2193 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    protected com.adincube.sdk.g.c.c f2194;

    public a(BannerView bannerView, c cVar, com.adincube.sdk.f.a aVar, b bVar, com.adincube.sdk.f.b.a.a aVar2, c cVar2) {
        this.f2192 = bannerView;
        this.f2190 = cVar;
        this.f2185 = aVar;
        this.f2186 = bVar;
        this.f2188 = cVar2;
        this.f2187 = aVar2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m2672() {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2673() {
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m2674() {
        return this.f2189;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m2675() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2676() {
        boolean a;
        if (this.f2194 != com.adincube.sdk.g.c.c.BANNER_AUTO) {
            if (!this.f2194.a(this.f2192.getContext(), j.m3546(this.f2192.getContext()).widthPixels, j.m3546(this.f2192.getContext()).heightPixels)) {
                throw new com.adincube.sdk.c.a.a.c(this.f2192, this.f2194);
            }
        }
        if (this.f2192.getMeasuredWidth() > 0 || this.f2192.getMeasuredHeight() > 0) {
            ViewGroup.LayoutParams layoutParams = this.f2192.getLayoutParams();
            int i = layoutParams.width;
            int i2 = layoutParams.height;
            int measuredWidth = i != -2 ? this.f2192.getMeasuredWidth() : i;
            int measuredHeight = i2 != -2 ? this.f2192.getMeasuredHeight() : i2;
            com.adincube.sdk.g.c.c cVar = this.f2194;
            Context context = this.f2192.getContext();
            if (!(context instanceof Activity)) {
                a = cVar.a(context, measuredWidth, measuredHeight);
            } else {
                View findViewById = ((Activity) context).findViewById(16908290);
                if (findViewById == null) {
                    a = cVar.a(context, measuredWidth, measuredHeight);
                } else {
                    int width = findViewById.getWidth();
                    if (width == 0) {
                        width = findViewById.getMeasuredWidth();
                    }
                    int height = findViewById.getHeight();
                    if (height == 0) {
                        height = findViewById.getMeasuredHeight();
                    }
                    if (width == 0 || height == 0) {
                        a = cVar.a(context, measuredWidth, measuredHeight);
                    } else {
                        int r4 = j.m3545(context, measuredWidth);
                        int r2 = j.m3545(context, measuredHeight);
                        int r0 = j.m3545(context, width);
                        j.m3545(context, height);
                        a = cVar.a(r4, r2, r0);
                    }
                }
            }
            if (!a) {
                throw new com.adincube.sdk.c.a.a.c(this.f2192, this.f2194);
            }
            return;
        }
        ViewGroup.LayoutParams layoutParams2 = this.f2192.getLayoutParams();
        if (!(layoutParams2 == null ? true : this.f2194.a(this.f2192.getContext(), layoutParams2.width, layoutParams2.height))) {
            throw new com.adincube.sdk.c.a.a.c(this.f2192, this.f2194);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m2677(int i) {
        this.f2193 = i;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m2678() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m2679() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final LinearLayout.LayoutParams m2680(View view) {
        LinearLayout.LayoutParams layoutParams = view.getLayoutParams() == null ? this.f2194 == com.adincube.sdk.g.c.c.BANNER_AUTO ? new LinearLayout.LayoutParams(-1, -2) : new LinearLayout.LayoutParams(this.f2194.a(this.f2192.getResources().getDisplayMetrics()), this.f2194.b(this.f2192.getResources().getDisplayMetrics())) : new LinearLayout.LayoutParams(view.getLayoutParams());
        layoutParams.gravity = 1;
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final com.adincube.sdk.g.c.a m2681() {
        return this.f2188.m2738();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2682(int i) {
        this.f2191 = Integer.valueOf(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2683(com.adincube.sdk.c.a.c cVar) {
        try {
            cVar.a();
            com.adincube.sdk.f.b.a.a aVar = this.f2187;
            new StringBuilder("onError - ").append(cVar.a);
            o.m3699(aVar.f2169, new com.adincube.sdk.util.c.a<AdinCubeBannerEventListener>(cVar) {

                /* renamed from: 龘 */
                final /* synthetic */ com.adincube.sdk.c.a.c f2182;

                /* renamed from: 龘 */
                public final /* synthetic */ void m2669(
/*
Method generation error in method: com.adincube.sdk.f.b.a.a.5.龘(java.lang.Object):void, dex: classes.dex
                jadx.core.utils.exceptions.JadxRuntimeException: Method args not loaded: com.adincube.sdk.f.b.a.a.5.￩ﾾﾘ(java.lang.Object):void, class status: UNLOADED
                	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:278)
                	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:116)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:313)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:271)
                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:240)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:483)
                	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:472)
                	at java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                	at java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:485)
                	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:236)
                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:227)
                	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:676)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:607)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:364)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:231)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:123)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:107)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:787)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:728)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:368)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:250)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:221)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
                	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:311)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:68)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:211)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:204)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:318)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:271)
                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:240)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:483)
                	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:472)
                	at java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                	at java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:485)
                	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:236)
                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:227)
                	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:112)
                	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:78)
                	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:33)
                	at jadx.core.codegen.CodeGen.generate(CodeGen.java:21)
                	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
                	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:273)
                
*/
            });
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerView.printShowError", th);
            ErrorReportingHelper.m3511("BannerView.printShowError", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2684(com.adincube.sdk.g.c.c cVar) {
        this.f2194 = cVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2685(boolean z) {
    }
}
