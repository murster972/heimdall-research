package com.adincube.sdk.f.b.d;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.adincube.sdk.g.c.a;
import com.adincube.sdk.k.c;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a.b;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.s;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class d {

    /* renamed from: 龘  reason: contains not printable characters */
    private static d f2392 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    private b f2393;

    /* renamed from: ʼ  reason: contains not printable characters */
    private b.a f2394 = new b.a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2992(boolean z) {
            if (!z) {
                try {
                    d.this.m2984();
                } catch (Throwable th) {
                    new Object[1][0] = th;
                    ErrorReportingHelper.m3506("NativeAdImpressionTrackingManager#OnScreenStateChangedListener.onScreenStateChanged", com.adincube.sdk.g.c.b.NATIVE, th);
                }
            } else {
                d.this.m2986();
            }
        }
    };

    /* renamed from: ʽ  reason: contains not printable characters */
    private c.a f2395 = new c.a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2993() {
            synchronized (d.this) {
                if (!d.this.m2980()) {
                    d.this.m2984();
                } else {
                    d.this.m2986();
                }
            }
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f2396 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private Context f2397 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f2398 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private Set<com.adincube.sdk.mediation.r.b> f2399 = new HashSet();

    private d(Context context) {
        this.f2397 = context;
        this.f2393 = new b(context, com.adincube.sdk.g.c.b.NATIVE, (a) null);
        this.f2393.f2975 = this.f2394;
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public synchronized void m2979() {
        try {
            if (this.f2398) {
                if (!this.f2396) {
                    Iterator<com.adincube.sdk.mediation.r.b> it2 = this.f2399.iterator();
                    while (it2.hasNext()) {
                        c cVar = it2.next().连任;
                        if (cVar.m2978()) {
                            cVar.m2975();
                            it2.remove();
                        }
                    }
                    if (this.f2399.size() > 0) {
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            public final void run() {
                                d.this.m2979();
                            }
                        }, 500);
                    } else {
                        m2981();
                    }
                }
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdImpressionTrackingManager.checkTracked", new Object[0]);
        }
        return;
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m2980() {
        Iterator<com.adincube.sdk.mediation.r.b> it2 = this.f2399.iterator();
        boolean z = false;
        while (!z && it2.hasNext()) {
            c cVar = it2.next().连任.f2388;
            z = cVar != null ? cVar.f2937 == 0 : z;
        }
        return z;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private synchronized void m2981() {
        if (this.f2398) {
            this.f2393.m3517();
            this.f2398 = false;
            this.f2396 = false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private synchronized void m2982() {
        if (!this.f2398) {
            this.f2398 = true;
            this.f2396 = true;
            this.f2393.m3518();
        }
        m2986();
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized void m2984() {
        if (!this.f2396) {
            this.f2396 = true;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized void m2986() {
        if (this.f2398) {
            if (this.f2396 && s.m3713(this.f2397) && m2980()) {
                this.f2396 = false;
                m2979();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static d m2988() {
        if (f2392 == null) {
            synchronized (d.class) {
                if (f2392 == null) {
                    f2392 = new d(f.m3605());
                }
            }
        }
        return f2392;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized void m2990(com.adincube.sdk.mediation.r.b bVar) {
        this.f2399.remove(bVar);
        bVar.连任.f2388.f2936 = null;
        if (this.f2399.size() == 0) {
            m2981();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m2991(com.adincube.sdk.mediation.r.b bVar) {
        this.f2399.add(bVar);
        bVar.连任.f2388.f2936 = this.f2395;
        m2982();
    }
}
