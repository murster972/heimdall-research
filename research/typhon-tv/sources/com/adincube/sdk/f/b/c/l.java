package com.adincube.sdk.f.b.c;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import com.adincube.sdk.f.b.c.k;
import com.adincube.sdk.g.c.e;
import com.adincube.sdk.util.ErrorReportingHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class l implements k {

    /* renamed from: ʻ  reason: contains not printable characters */
    Object f2334 = new Object();

    /* renamed from: ʼ  reason: contains not printable characters */
    List<b> f2335 = new ArrayList();

    /* renamed from: ʽ  reason: contains not printable characters */
    a f2336;

    /* renamed from: 连任  reason: contains not printable characters */
    k.a f2337;

    /* renamed from: 靐  reason: contains not printable characters */
    com.adincube.sdk.g.c.a f2338;

    /* renamed from: 麤  reason: contains not printable characters */
    long f2339;

    /* renamed from: 齉  reason: contains not printable characters */
    i f2340;

    /* renamed from: 龘  reason: contains not printable characters */
    com.adincube.sdk.g.c.b f2341;

    private class a extends CountDownTimer {
        public a() {
            super(Long.MAX_VALUE, 1000);
        }

        public final void onFinish() {
        }

        public final void onTick(long j) {
            try {
                l.this.f2340.m2922();
                l lVar = l.this;
                if (((long) lVar.f2340.m2911()) != lVar.f2339) {
                    l lVar2 = l.this;
                    lVar2.f2340.m2920();
                    lVar2.f2339 = (long) lVar2.f2340.m2911();
                }
                l lVar3 = l.this;
                ArrayList<e> arrayList = new ArrayList<>();
                synchronized (lVar3.f2334) {
                    long currentTimeMillis = System.currentTimeMillis();
                    Iterator<b> it2 = lVar3.f2335.iterator();
                    while (it2.hasNext()) {
                        b next = it2.next();
                        if (next.f2345 < currentTimeMillis) {
                            it2.remove();
                            arrayList.add(next.f2347);
                        }
                    }
                }
                for (e r0 : arrayList) {
                    lVar3.f2337.m2932(r0);
                }
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("NetworkOrderLoaderTimerAndroidImpl#NetworkOrderLoaderTimerInternalCountDown.onTick", th);
                ErrorReportingHelper.m3505("NetworkOrderLoaderTimerAndroidImpl#NetworkOrderLoaderTimerInternalCountDown.onTick", l.this.f2341, l.this.f2338, th);
            }
        }
    }

    private class b {

        /* renamed from: 靐  reason: contains not printable characters */
        public long f2345;

        /* renamed from: 龘  reason: contains not printable characters */
        public e f2347;

        private b() {
        }

        /* synthetic */ b(l lVar, byte b) {
            this();
        }
    }

    public l(com.adincube.sdk.g.c.b bVar, com.adincube.sdk.g.c.a aVar) {
        this.f2341 = bVar;
        this.f2338 = aVar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized void m2933() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                try {
                    l.this.f2336 = new a();
                    l.this.f2336.start();
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("NetworkOrderLoaderTimerAndroidImpl.start", th);
                    ErrorReportingHelper.m3505("NetworkOrderLoaderTimerAndroidImpl.start", l.this.f2341, l.this.f2338, th);
                }
            }
        });
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final synchronized void m2934() {
        if (this.f2336 != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        if (l.this.f2336 != null) {
                            l.this.f2336.cancel();
                            l.this.f2336 = null;
                        }
                    } catch (Throwable th) {
                        com.adincube.sdk.util.a.m3513("NetworkOrderLoaderTimerAndroidImpl.stop", th);
                        ErrorReportingHelper.m3505("NetworkOrderLoaderTimerAndroidImpl.stop", l.this.f2341, l.this.f2338, th);
                    }
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2935(i iVar) {
        this.f2340 = iVar;
        if (this.f2340 != null) {
            this.f2339 = (long) iVar.m2911();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2936(k.a aVar) {
        this.f2337 = aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2937(e eVar) {
        synchronized (this.f2334) {
            Iterator<b> it2 = this.f2335.iterator();
            while (it2.hasNext()) {
                if (it2.next().f2347 == eVar) {
                    it2.remove();
                    return;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2938(e eVar, long j) {
        synchronized (this.f2334) {
            b bVar = new b(this, (byte) 0);
            bVar.f2347 = eVar;
            bVar.f2345 = System.currentTimeMillis() + j;
            this.f2335.add(bVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2939() {
        return this.f2336 != null;
    }
}
