package com.adincube.sdk.f.b.d;

import android.graphics.Rect;
import android.os.Build;
import android.view.ViewGroup;
import com.adincube.sdk.g.c.a;
import com.adincube.sdk.mediation.r.b;
import com.adincube.sdk.util.ErrorReportingHelper;

public final class c {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Rect f2386 = new Rect();

    /* renamed from: 连任  reason: contains not printable characters */
    private double f2387 = 1.0d;

    /* renamed from: 靐  reason: contains not printable characters */
    com.adincube.sdk.k.c f2388 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private b f2389;

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f2390 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewGroup f2391 = null;

    public c(b bVar) {
        this.f2389 = bVar;
        this.f2387 = bVar.齉.ʼ().m3474().f2943;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized void m2975() {
        if (!this.f2390) {
            this.f2390 = true;
            com.adincube.sdk.util.e.b.m3592().m3598(this.f2389.齉.ʼ().m3473(), com.adincube.sdk.g.c.b.NATIVE, (a) null);
            com.adincube.sdk.mediation.r.c cVar = this.f2389.齉;
            if (cVar.ˑ()) {
                cVar.龘(this.f2389);
            }
            this.f2389.靐.f2154.m3061(false, this.f2389.ʼ, cVar, cVar.麤());
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final synchronized void m2976() {
        if (this.f2391 != null) {
            d.m2988().m2990(this.f2389);
            this.f2391.removeView(this.f2388);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m2977(ViewGroup viewGroup) {
        if (!this.f2390) {
            this.f2391 = viewGroup;
            this.f2388 = new com.adincube.sdk.k.c(viewGroup.getContext());
            viewGroup.addView(this.f2388);
            d.m2988().m2991(this.f2389);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2978() {
        try {
            if (this.f2391.getParent() == null || this.f2391.getWindowToken() == null || this.f2391.getVisibility() != 0 || this.f2391.getWindowVisibility() != 0) {
                return false;
            }
            if (Build.VERSION.SDK_INT >= 11 && this.f2391.getAlpha() < 0.9f) {
                return false;
            }
            if ((this.f2391.getMeasuredHeight() == 0 && this.f2391.getMeasuredWidth() == 0) || !this.f2391.getGlobalVisibleRect(this.f2386)) {
                return false;
            }
            return ((long) (this.f2386.width() * this.f2386.height())) >= ((long) (((double) (this.f2391.getWidth() * this.f2391.getHeight())) * this.f2387));
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdImpressionManager.isVisibleEnough", th);
            ErrorReportingHelper.m3506("NativeAdImpressionManager.isVisibleEnough", com.adincube.sdk.g.c.b.NATIVE, th);
            return false;
        }
    }
}
