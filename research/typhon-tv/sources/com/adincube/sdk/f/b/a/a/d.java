package com.adincube.sdk.f.b.a.a;

import android.os.Handler;
import android.os.Looper;
import com.adincube.sdk.BannerView;
import com.adincube.sdk.c.a.c;
import com.adincube.sdk.c.a.h;
import com.adincube.sdk.c.a.p;
import com.adincube.sdk.f.a;
import com.adincube.sdk.f.b.a.b;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.f;

public final class d extends a {

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f2216 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f2217 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f2218 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean f2219 = false;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f2220 = false;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private b f2221 = new b() {
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m2717(boolean z) {
            if (!z) {
                d.this.f2219 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2718(boolean z) {
            if (!z) {
                d.this.f2219 = true;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2719(boolean z, c cVar) {
        }
    };

    public d(BannerView bannerView, c cVar, a aVar, com.adincube.sdk.f.a.b bVar, com.adincube.sdk.f.b.a.a aVar2, com.adincube.sdk.f.b.a.c cVar2) {
        super(bannerView, cVar, aVar, bVar, aVar2, cVar2);
        aVar2.m2661(this.f2221);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m2706() {
        if (this.f2194 == null) {
            throw new h();
        } else if (!this.f2194.f) {
            throw new com.adincube.sdk.c.a.a.b();
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m2707() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {

            /* renamed from: 龘  reason: contains not printable characters */
            final /* synthetic */ boolean f2225 = true;

            public final void run() {
                d.this.m2713(this.f2225);
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m2708(boolean z) {
        if (this.f2194 == null) {
            this.f2218 = true;
        } else if (!m2713(z)) {
            c e = null;
            try {
                m2706();
                m2676();
                this.f2188.m2734();
            } catch (c e2) {
                e = e2;
            } catch (Throwable th) {
                p pVar = new p(th);
                ErrorReportingHelper.m3505("BannerViewLoadManager.load", com.adincube.sdk.g.c.b.BANNER, m2681(), th);
                e = pVar;
            }
            if (e != null) {
                m2709(e, true);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2709(c cVar, boolean z) {
        try {
            cVar.a();
            if (z) {
                this.f2187.m2660(false, cVar);
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerView.printLoadError", th);
            ErrorReportingHelper.m3511("BannerView.printLoadError", th);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m2711() {
        c e = null;
        try {
            if (!this.f2219) {
                f.m3606(this.f2192.getContext());
                com.adincube.sdk.util.b.a.m3525();
                this.f2186.m2617(this.f2192.getContext());
                a.m2608(this.f2192.getContext(), true);
                if (com.adincube.sdk.util.e.a.m3585()) {
                    com.adincube.sdk.util.a.m3515("Configuration changed.", new Object[0]);
                    com.adincube.sdk.util.e.b.m3592().m3596();
                    a.m2607().m2609();
                    com.adincube.sdk.util.d.m3554();
                    com.adincube.sdk.util.e.a.m3580();
                }
                m2708(false);
                if (e != null) {
                    m2709(e, true);
                }
            }
        } catch (c e2) {
            e = e2;
        } catch (Throwable th) {
            p pVar = new p(th);
            ErrorReportingHelper.m3505("BannerViewLoadManager.preLoadOrLoad", com.adincube.sdk.g.c.b.BANNER, m2681(), th);
            e = pVar;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2712(int i) {
        super.m2677(i);
        if (i == 0 && this.f2220) {
            this.f2220 = false;
            m2707();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0020  */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m2713(boolean r7) {
        /*
            r6 = this;
            r0 = 0
            r1 = 1
            r2 = 0
            r6.m2706()     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            r6.m2676()     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            com.adincube.sdk.BannerView r3 = r6.f2192     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            android.os.IBinder r3 = r3.getWindowToken()     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            if (r3 != 0) goto L_0x0015
            r2 = 1
            r6.f2217 = r2     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
        L_0x0014:
            return r0
        L_0x0015:
            int r3 = r6.f2193     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            if (r3 == 0) goto L_0x0025
            r2 = 1
            r6.f2220 = r2     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            goto L_0x0014
        L_0x001d:
            r0 = move-exception
        L_0x001e:
            if (r0 == 0) goto L_0x0023
            r6.m2709((com.adincube.sdk.c.a.c) r0, (boolean) r1)
        L_0x0023:
            r0 = r1
            goto L_0x0014
        L_0x0025:
            java.lang.Integer r3 = r6.f2191     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            if (r3 == 0) goto L_0x0049
            java.lang.Integer r3 = r6.f2191     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            int r3 = r3.intValue()     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            if (r3 == 0) goto L_0x0049
            r2 = 1
            r6.f2216 = r2     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            goto L_0x0014
        L_0x0035:
            r0 = move-exception
            com.adincube.sdk.c.a.p r2 = new com.adincube.sdk.c.a.p
            r2.<init>(r0)
            java.lang.String r3 = "BannerViewLoadManager.load"
            com.adincube.sdk.g.c.b r4 = com.adincube.sdk.g.c.b.BANNER
            com.adincube.sdk.g.c.a r5 = r6.m2681()
            com.adincube.sdk.util.ErrorReportingHelper.m3505((java.lang.String) r3, (com.adincube.sdk.g.c.b) r4, (com.adincube.sdk.g.c.a) r5, (java.lang.Throwable) r0)
            r0 = r2
            goto L_0x001e
        L_0x0049:
            com.adincube.sdk.f.b.a.c r3 = r6.f2188     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            int r0 = com.adincube.sdk.f.b.c.c.a.b     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            r3.m2743(r0)     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            com.adincube.sdk.g.c.d r4 = r3.f2151     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            boolean r0 = com.adincube.sdk.g.c.d.m3225((com.adincube.sdk.g.c.d) r4)     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            if (r0 != 0) goto L_0x0067
            if (r7 == 0) goto L_0x0062
            com.adincube.sdk.f.b.j r0 = r3.f2160     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            boolean r0 = r0.m3048()     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            if (r0 != 0) goto L_0x0074
        L_0x0062:
            r3.m2644()     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            r0 = r2
            goto L_0x001e
        L_0x0067:
            com.adincube.sdk.f.b.c.i r0 = r4.f2656     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            com.adincube.sdk.f.b.c.c r0 = (com.adincube.sdk.f.b.c.c) r0     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            int r3 = r3.f2239     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            r0.f2316 = r3     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            com.adincube.sdk.f.b.c.i r0 = r4.f2656     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
            r0.m2913()     // Catch:{ c -> 0x001d, Throwable -> 0x0035 }
        L_0x0074:
            r0 = r2
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.f.b.a.a.d.m2713(boolean):boolean");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m2714() {
        if (this.f2217) {
            this.f2217 = false;
            m2707();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2715() {
        if (this.f2218) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                /* renamed from: 龘  reason: contains not printable characters */
                final /* synthetic */ boolean f2223 = false;

                public final void run() {
                    d.this.m2708(this.f2223);
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2716(int i) {
        super.m2682(i);
        if (i == 0 && this.f2216) {
            this.f2216 = false;
            m2707();
        }
    }
}
