package com.adincube.sdk.f.b.c;

import com.adincube.sdk.f.b.c.a;
import com.adincube.sdk.g.c.e;
import com.adincube.sdk.mediation.h;
import com.adincube.sdk.util.ErrorReportingHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class d extends b<h> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private com.adincube.sdk.g.c.d f2318;

    /* renamed from: ʼ  reason: contains not printable characters */
    List<o> f2319;

    /* renamed from: ʽ  reason: contains not printable characters */
    long f2320;

    /* renamed from: ˑ  reason: contains not printable characters */
    private m f2321;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f2322;

    private class a extends a {
        public a(i iVar, e eVar, a.C0005a aVar, boolean z) {
            super(iVar, eVar, aVar, z);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2885() {
            if (this.f2296.f2663 != null && !d.this.m2869()) {
                d.this.f2310.m2929(this.f2296);
                o oVar = new o(this.f2296, d.this.f2320);
                if (oVar.m2949()) {
                    d.this.m2879(this.f2296).m2910(e.LOADED);
                    m2830(this.f2296);
                    return;
                }
                Object[] objArr = {this.f2296.f2667, Long.valueOf(d.this.f2320)};
                d.this.f2319.add(oVar);
            }
        }
    }

    public d(com.adincube.sdk.g.c.d dVar, k kVar, j jVar, m mVar, int i, int i2, long j) {
        this(dVar, kVar, jVar, mVar, i, i2, j, (byte) 0);
    }

    private d(com.adincube.sdk.g.c.d dVar, k kVar, j jVar, m mVar, int i, int i2, long j, byte b) {
        super(dVar, kVar, jVar, i, i2, j);
        this.f2319 = new ArrayList();
        this.f2318 = dVar;
        this.f2322 = true;
        this.f2320 = 3000;
        this.f2321 = mVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m2864(int i, int i2) {
        return i > 0 || i2 > 1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ int m2865() {
        return super.m2836();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ boolean m2866() {
        return super.m2838();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m2867() {
        super.m2840();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ Map m2868() {
        return super.m2841();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ boolean m2869() {
        return super.m2842();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ boolean m2870() {
        return super.m2843();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m2871() {
        return super.m2843();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final void m2872() {
        super.m2845();
        m mVar = this.f2321;
        synchronized (mVar.f2351) {
            mVar.f2351.remove(this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final h m2873(e eVar) {
        return new h(eVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2874(e eVar, e eVar2) {
        super.m2849(eVar, eVar2);
        if (eVar2 == e.ERROR || eVar2 == e.NO_MORE_INVENTORY) {
            this.f2321.m2942(this.f2318.f2662, eVar.f2667);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m2875() {
        ArrayList arrayList = new ArrayList();
        Iterator<e> it2 = m2846().iterator();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (it2.hasNext() && !m2864(i, i2)) {
            e next = it2.next();
            h r8 = super.m2855(next);
            if (r8.m2906() == e.LIAR) {
                arrayList.add(next.f2667);
            }
            if (r8.m2908() && !arrayList.contains(next.f2667)) {
                i4++;
            }
            if (r8.m2909()) {
                i3++;
            }
            if (r8.m2907()) {
                if (next.f2665.m3463(this.f2318.f2662)) {
                    i++;
                }
                i2++;
            }
            i = i;
        }
        if (i4 > 0) {
            return false;
        }
        if (i4 == 0 && i3 == 0 && i2 > 0) {
            return true;
        }
        return m2864(i, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final List<e> m2876() {
        ArrayList arrayList = new ArrayList(m2846());
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        int r8 = super.m2836();
        Iterator it2 = arrayList.iterator();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (it2.hasNext()) {
            e eVar = (e) it2.next();
            h r10 = super.m2855(eVar);
            if (arrayList2.contains(eVar.f2667) || arrayList3.contains(eVar.f2667)) {
                it2.remove();
            } else if (eVar.f2665.m3466(this.f2318.f2662, arrayList2)) {
                it2.remove();
            } else if (arrayList4.contains(eVar.f2667)) {
                it2.remove();
            } else if (m2864(i, i2)) {
                it2.remove();
            } else if (r10.m2906() == e.LOADED) {
                int i4 = eVar.f2665.m3463(this.f2318.f2662) ? i + 1 : i;
                i2++;
                it2.remove();
                i = i4;
            } else if (r10.m2906() == e.LIAR) {
                arrayList4.add(eVar.f2667);
                it2.remove();
            } else if (r10.m2909()) {
                i3++;
                arrayList2.add(eVar.f2667);
                it2.remove();
            } else if (r10.m2906().l) {
                if (r10.m2906() == e.TIMEOUT) {
                    arrayList3.add(eVar.f2667);
                }
                it2.remove();
            } else if (i3 >= r8) {
                it2.remove();
            } else {
                i3++;
                arrayList2.add(eVar.f2667);
            }
        }
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2877() {
        for (e next : m2846()) {
            h r2 = next.f2663.m3429();
            if (r2 != null && !r2.m3476()) {
                h r22 = super.m2855(next);
                boolean r3 = this.f2321.m2941(this.f2318.f2662, next);
                if (r22.m2906() == e.WAITING_FOR_OTHER_AD_TYPE && !r3) {
                    r22.m2910(e.WAITING);
                    m2859(next, r22.m2906());
                } else if (r22.m2906() == e.WAITING && r3) {
                    m mVar = this.f2321;
                    synchronized (mVar.f2351) {
                        mVar.f2351.add(this);
                    }
                    r22.m2910(e.WAITING_FOR_OTHER_AD_TYPE);
                    m2859(next, r22.m2906());
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2878(e eVar) {
        if (this.f2321.m2943(this.f2318.f2662, eVar)) {
            h r6 = super.m2855(eVar);
            r6.m2910(e.LOADING);
            try {
                eVar.f2663.m3434((com.adincube.sdk.mediation.a) new a(this, eVar, this.f2311, this.f2322));
                m2847(eVar);
                eVar.f2663.m3432();
            } catch (com.adincube.sdk.c.b.a e) {
                com.adincube.sdk.util.a.m3513("Error caught when loading ad from network '%s'. Mediation will continue.", eVar.f2667, e);
                r6.m2910(e.ERROR);
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("Error caught when loading ad from network '%s'. Mediation will continue.", eVar.f2667, th);
                ErrorReportingHelper.m3510("NetworkOrderLoader.loadNetwork", eVar.f2667, this.f2318.f2662, th);
                r6.m2910(e.ERROR);
            }
            if (r6.m2906() == e.LOADING) {
                this.f2310.m2930(eVar, eVar.f2665.m3464(this.f2318.f2662));
            }
            m2852(eVar);
            if (r6.m2906() == e.ERROR) {
                m2859(eVar, r6.m2906());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ h m2879(e eVar) {
        return super.m2855(eVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ e m2880(e eVar) {
        return super.m2856(eVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m2881() {
        super.m2857();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* bridge */ /* synthetic */ void m2882(n nVar) {
        super.m2858(nVar);
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final synchronized void m2883() {
        if (!this.f2319.isEmpty()) {
            if (!this.f2312) {
                Iterator<o> it2 = this.f2319.iterator();
                while (it2.hasNext()) {
                    o next = it2.next();
                    long currentTimeMillis = System.currentTimeMillis();
                    if (currentTimeMillis < next.f2354 || next.f2354 + next.f2355 <= currentTimeMillis) {
                        e eVar = next.f2356;
                        h r3 = super.m2855(eVar);
                        if (next.m2949()) {
                            r3.m2910(e.LOADED);
                        } else {
                            r3.m2910(e.LIAR);
                            m2837(eVar);
                        }
                        m2859(eVar, r3.m2906());
                        it2.remove();
                    }
                }
            }
        }
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final boolean m2884() {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        int i2 = 0;
        for (e next : m2846()) {
            h r7 = super.m2855(next);
            if (r7.m2906() == e.LIAR) {
                arrayList.add(next.f2667);
            }
            if (!arrayList.contains(next.f2667)) {
                if (r7.m2907()) {
                    int i3 = next.f2665.m3463(this.f2318.f2662) ? i + 1 : i;
                    i2++;
                    if (m2864(i3, i2)) {
                        return true;
                    }
                    i = i3;
                } else if (r7.m2906() != e.WAITING_FOR_OTHER_AD_TYPE && !r7.m2906().l) {
                    return false;
                }
            }
        }
        return i2 > 0;
    }
}
