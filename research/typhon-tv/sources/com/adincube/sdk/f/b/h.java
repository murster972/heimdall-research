package com.adincube.sdk.f.b;

import android.content.Context;
import com.adincube.sdk.mediation.c;
import com.adincube.sdk.util.a;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.i;
import java.util.HashMap;
import java.util.Map;

public class h {

    /* renamed from: 齉  reason: contains not printable characters */
    private static h f2437 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private Map<String, com.adincube.sdk.mediation.h> f2438 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    Map<String, String> f2439 = new HashMap();

    public h() {
        Context r2 = f.m3605();
        i.m3673(r2);
        for (c cVar : c.a()) {
            com.adincube.sdk.mediation.h hVar = cVar.z;
            if (hVar != null) {
                try {
                    this.f2439.put(hVar.m3473(), hVar.m3475(r2));
                    this.f2438.put(hVar.m3473(), hVar);
                } catch (NoClassDefFoundError e) {
                }
            }
        }
        if (this.f2438.size() > 1) {
            a.m3515("Detected networks: ", new Object[0]);
            for (com.adincube.sdk.mediation.h next : this.f2438.values()) {
                if (!"RTB".equals(next.m3473())) {
                    a.m3515("    " + next.m3473() + " - " + next.m3475(r2), new Object[0]);
                }
            }
            return;
        }
        a.m3513("No network detected. Do not forget to include partner network jar/aar.", new Object[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static h m3040() {
        if (f2437 == null) {
            synchronized (h.class) {
                if (f2437 == null) {
                    f2437 = new h();
                }
            }
        }
        return f2437;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized com.adincube.sdk.mediation.h m3041(String str) {
        return this.f2438.get(str);
    }
}
