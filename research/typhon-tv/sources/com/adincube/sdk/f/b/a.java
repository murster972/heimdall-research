package com.adincube.sdk.f.b;

import android.content.Context;
import com.adincube.sdk.c.a.h;
import com.adincube.sdk.c.a.o;
import com.adincube.sdk.f.b;
import com.adincube.sdk.f.b.c.i;
import com.adincube.sdk.f.b.c.j;
import com.adincube.sdk.f.b.c.l;
import com.adincube.sdk.f.b.c.m;
import com.adincube.sdk.f.b.c.n;
import com.adincube.sdk.f.b.g;
import com.adincube.sdk.f.b.j;
import com.adincube.sdk.f.c;
import com.adincube.sdk.g.c.d;
import com.adincube.sdk.g.c.e;
import com.adincube.sdk.g.g;
import com.adincube.sdk.mediation.f;
import com.adincube.sdk.util.ErrorReportingHelper;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONObject;

public abstract class a implements j, n, j.b {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected g f2146;

    /* renamed from: ʼ  reason: contains not printable characters */
    public b f2147;

    /* renamed from: ʽ  reason: contains not printable characters */
    public com.adincube.sdk.f.a f2148;

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f2149 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private i f2150;

    /* renamed from: ˈ  reason: contains not printable characters */
    public d f2151 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f2152 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final f f2153 = new f() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2658(com.adincube.sdk.mediation.b bVar, JSONObject jSONObject) {
            e eVar;
            d dVar = a.this.f2151;
            if (dVar != null) {
                Iterator<e> it2 = dVar.f2660.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        eVar = null;
                        break;
                    }
                    eVar = it2.next();
                    if (eVar.f2663 == bVar) {
                        break;
                    }
                }
                if (eVar != null) {
                    eVar.f2664 = jSONObject;
                    a.this.f2158.m3027(a.this.m2637(), dVar);
                }
            }
        }
    };

    /* renamed from: ˑ  reason: contains not printable characters */
    public c f2154;

    /* renamed from: ٴ  reason: contains not printable characters */
    public com.adincube.sdk.util.e.b f2155;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public d f2156;

    /* renamed from: 连任  reason: contains not printable characters */
    public f f2157;

    /* renamed from: 靐  reason: contains not printable characters */
    protected com.adincube.sdk.f.b.e.b.b f2158;

    /* renamed from: 麤  reason: contains not printable characters */
    public m f2159;

    /* renamed from: 齉  reason: contains not printable characters */
    public j f2160;

    /* renamed from: 龘  reason: contains not printable characters */
    protected h f2161;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private k f2162;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private long f2163 = 0;

    public a(com.adincube.sdk.f.a aVar, com.adincube.sdk.util.e.b bVar, d dVar, h hVar, com.adincube.sdk.f.b.e.b.b bVar2, j jVar, m mVar, f fVar, g gVar, k kVar) {
        this.f2161 = hVar;
        this.f2158 = bVar2;
        this.f2160 = jVar;
        this.f2159 = mVar;
        this.f2157 = fVar;
        this.f2146 = gVar;
        this.f2162 = kVar;
        this.f2148 = aVar;
        this.f2155 = bVar;
        this.f2156 = dVar;
        this.f2154 = new c(m2646());
        this.f2147 = new b(aVar, this.f2154);
        this.f2150 = new i(m2646(), gVar);
        jVar.m3050((j.b) this);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m2625(d dVar) {
        this.f2151 = dVar;
        this.f2158.m3027(m2637(), dVar);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m2626(d dVar) {
        if (dVar != null && !dVar.m3226() && this.f2162.m3057()) {
            g r2 = this.f2162.m3058();
            for (e next : dVar.f2660) {
                try {
                    next.f2663.m3429().m3483(r2);
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("Error catched while updating user information for network '%s'. Mediation will continue without.", next.f2667);
                    ErrorReportingHelper.m3509("AbstractNetworkMediationManager.updateUserInformationForNetworkOrder", next.f2667, dVar.f2662, m2637(), th);
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m2627();

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2628() {
        d r0 = m2632();
        if (r0.m3227().next() == null) {
            this.f2147.m2623(m2629(), r0);
            if (r0.f2656 == null || !r0.f2656.m2916()) {
                throw new h();
            }
            this.f2147.m2624(m2629(), r0);
            throw new com.adincube.sdk.c.a.m();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract com.adincube.sdk.g.b.c m2629();

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m2630() {
        this.f2149 = true;
        m2649(this.f2151, (com.adincube.sdk.mediation.b) null);
        this.f2160.m3049();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final String m2631() {
        i iVar;
        e r1;
        d dVar = this.f2151;
        if (dVar == null || (iVar = dVar.f2656) == null || iVar.m2915() || !iVar.m2923() || (r1 = iVar.m2919(com.adincube.sdk.f.b.c.e.LOADED)) == null) {
            return null;
        }
        return r1.f2667;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final d m2632() {
        d dVar = this.f2151;
        if (dVar == null) {
            if (this.f2160.m3046()) {
                throw new h();
            } else if (this.f2160.m3048()) {
                throw this.f2160.m3047();
            } else {
                m2633();
                m2634();
            }
        }
        return dVar;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m2633() {
        if (this.f2152) {
            throw new o();
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public abstract void m2634();

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract Context m2635();

    /* renamed from: 连任  reason: contains not printable characters */
    public void m2636(d dVar) {
        this.f2163 = System.currentTimeMillis();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract com.adincube.sdk.g.c.a m2637();

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2638(d dVar) {
        m2649(dVar, (com.adincube.sdk.mediation.b) null);
        m2627();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m2639(com.adincube.sdk.mediation.b bVar);

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0022  */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m2640(com.adincube.sdk.g.c.d r9, com.adincube.sdk.g.b.b r10) {
        /*
            r8 = this;
            r0 = 1
            r1 = 0
            com.adincube.sdk.f.b.c.i r2 = r9.f2656
            boolean r2 = r2.m2916()
            if (r2 == 0) goto L_0x002d
            if (r10 == 0) goto L_0x0026
            long r2 = java.lang.System.currentTimeMillis()
            long r4 = r8.f2163
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 < 0) goto L_0x001f
            long r4 = r8.f2163
            long r6 = r10.f2605
            long r4 = r4 + r6
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0026
        L_0x001f:
            r2 = r0
        L_0x0020:
            if (r2 == 0) goto L_0x0028
            r8.m2638((com.adincube.sdk.g.c.d) r9)
        L_0x0025:
            return r0
        L_0x0026:
            r2 = r1
            goto L_0x0020
        L_0x0028:
            r8.m2641()
            r0 = r1
            goto L_0x0025
        L_0x002d:
            r0 = r1
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.f.b.a.m2640(com.adincube.sdk.g.c.d, com.adincube.sdk.g.b.b):boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m2641() {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m2642(d dVar) {
        this.f2156.m2951(dVar.f2659);
        this.f2163 = 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public i m2643(d dVar, com.adincube.sdk.g.b.b bVar) {
        com.adincube.sdk.f.b.c.d dVar2 = new com.adincube.sdk.f.b.c.d(dVar, new l(m2646(), m2637()), this, this.f2159, bVar.f2621, bVar.m3220(m2646()), bVar.f2597);
        dVar2.m2921((n) this);
        return dVar2;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m2644() {
        if (!this.f2149) {
            this.f2152 = true;
            d dVar = this.f2151;
            if (dVar == null && (dVar = this.f2158.m3026(m2637())) != null && dVar.f2657) {
                dVar = null;
            }
            if (m2656(dVar)) {
                this.f2156.m2951(dVar.f2659);
            } else {
                m2651(dVar, true);
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2645(final d dVar) {
        com.adincube.sdk.util.o.m3701("AbstractNetworkMediationManager.onNextNetworkOrderReceived", (Runnable) new Runnable() {
            public final void run() {
                try {
                    a.this.m2651(dVar, false);
                } catch (com.adincube.sdk.c.a.c e) {
                    a.this.m2649(dVar, (com.adincube.sdk.mediation.b) null);
                    e.a();
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract com.adincube.sdk.g.c.b m2646();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract com.adincube.sdk.mediation.b m2647(boolean z, com.adincube.sdk.mediation.h hVar, Context context);

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2648(d dVar, e eVar) {
        com.adincube.sdk.g.b.b r0 = this.f2148.m2610(true, true);
        if (!r0.f2606.contains(eVar.f2667)) {
            c cVar = this.f2154;
            Object[] objArr = {eVar.f2667, cVar.f2445};
            com.adincube.sdk.i.h hVar = new com.adincube.sdk.i.h();
            hVar.f2765 = cVar.f2445;
            hVar.f2763 = cVar.f2444;
            hVar.f2764 = Boolean.valueOf(dVar.f2659);
            hVar.f2837 = eVar.f2667;
            hVar.m3296();
        }
        if (r0.f2620.contains(eVar.f2667)) {
            com.adincube.sdk.util.a.m3512("Network '%s' reported to be loaded but was not. Deactivating network until app relaunch.", eVar.f2667);
            this.f2157.m3033(eVar.f2667);
            return;
        }
        com.adincube.sdk.util.a.m3512("Network '%s' reported to be loaded but was not. Mediation will continue.", eVar.f2667);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2649(d dVar, com.adincube.sdk.mediation.b bVar) {
        if (dVar != null) {
            if (dVar.f2656 != null) {
                dVar.f2656.m2921((n) null);
                dVar.f2656.m2917();
            }
            dVar.f2656 = null;
            for (e next : dVar.f2660) {
                if (next.f2663 != bVar) {
                    m2654(next.f2663);
                }
                next.f2665 = null;
                next.f2664 = null;
                next.f2663 = null;
            }
        }
        m2625((d) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2650(d dVar, List<e> list) {
        com.adincube.sdk.g.b.b r1 = this.f2148.m2610(true, true);
        StringBuilder sb = new StringBuilder();
        for (e next : list) {
            dVar.f2656.m2918(next).m2910(com.adincube.sdk.f.b.c.e.LIAR);
            if (!r1.f2602.contains(next.f2667)) {
                this.f2157.m3033(next.f2667);
            }
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(String.format(Locale.US, "'%s'", new Object[]{next.f2667}));
        }
        com.adincube.sdk.util.a.m3512("Networks %s were detected loaded but were not. Deactivating networks until app relaunch.", sb);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m2651(com.adincube.sdk.g.c.d r14, boolean r15) {
        /*
            r13 = this;
            r7 = 0
            r6 = 1
            com.adincube.sdk.f.a r0 = r13.f2148     // Catch:{ a -> 0x001a }
            r1 = 0
            r2 = 1
            com.adincube.sdk.g.b.b r8 = r0.m2610((boolean) r1, (boolean) r2)     // Catch:{ a -> 0x001a }
            boolean r0 = com.adincube.sdk.g.c.d.m3225((com.adincube.sdk.g.c.d) r14)     // Catch:{ a -> 0x001a }
            if (r0 == 0) goto L_0x0014
            r13.m2657((com.adincube.sdk.g.c.d) r14, (com.adincube.sdk.g.b.b) r8)     // Catch:{ a -> 0x001a }
        L_0x0013:
            return
        L_0x0014:
            if (r8 != 0) goto L_0x0032
            r13.m2638((com.adincube.sdk.g.c.d) r14)     // Catch:{ a -> 0x001a }
            goto L_0x0013
        L_0x001a:
            r0 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[r6]
            r1[r7] = r0
            com.adincube.sdk.f.a r0 = r13.f2148
            com.adincube.sdk.g.b.b r1 = r0.m2610((boolean) r6, (boolean) r6)
            if (r1 == 0) goto L_0x002e
            r1.f2633 = r6
            com.adincube.sdk.f.b.e.a.a r0 = r0.f2131
            r0.m3017()
        L_0x002e:
            r13.m2638((com.adincube.sdk.g.c.d) r14)
            goto L_0x0013
        L_0x0032:
            if (r14 != 0) goto L_0x0047
            r0 = r7
        L_0x0035:
            if (r0 == 0) goto L_0x0184
            com.adincube.sdk.f.b.f r0 = r13.f2157     // Catch:{ a -> 0x001a }
            r0.m3031((com.adincube.sdk.g.c.d) r14)     // Catch:{ a -> 0x001a }
            if (r15 == 0) goto L_0x006c
            if (r14 != 0) goto L_0x0067
            r0 = r6
        L_0x0041:
            if (r0 == 0) goto L_0x006c
            r13.m2638((com.adincube.sdk.g.c.d) r14)     // Catch:{ a -> 0x001a }
            goto L_0x0013
        L_0x0047:
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ a -> 0x001a }
            long r2 = r14.f2658     // Catch:{ a -> 0x001a }
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x005e
            long r2 = r14.f2658     // Catch:{ a -> 0x001a }
            int r4 = r14.f2661     // Catch:{ a -> 0x001a }
            long r4 = (long) r4     // Catch:{ a -> 0x001a }
            r10 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 * r10
            long r2 = r2 + r4
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0063
        L_0x005e:
            r0 = r6
        L_0x005f:
            if (r0 == 0) goto L_0x0065
            r0 = r7
            goto L_0x0035
        L_0x0063:
            r0 = r7
            goto L_0x005f
        L_0x0065:
            r0 = r6
            goto L_0x0035
        L_0x0067:
            boolean r0 = r14.m3226()     // Catch:{ a -> 0x001a }
            goto L_0x0041
        L_0x006c:
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ a -> 0x001a }
            r1 = 0
            java.lang.String r2 = r14.toString()     // Catch:{ a -> 0x001a }
            r0[r1] = r2     // Catch:{ a -> 0x001a }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ a -> 0x001a }
            android.content.Context r2 = r13.m2635()     // Catch:{ a -> 0x001a }
            if (r14 == 0) goto L_0x0086
            boolean r0 = r14.m3226()     // Catch:{ a -> 0x001a }
            if (r0 == 0) goto L_0x00ac
        L_0x0086:
            r13.m2626(r14)     // Catch:{ a -> 0x001a }
            com.adincube.sdk.f.b.i r0 = r13.f2150     // Catch:{ a -> 0x001a }
            java.util.HashSet r2 = new java.util.HashSet     // Catch:{ a -> 0x001a }
            r2.<init>()     // Catch:{ a -> 0x001a }
            java.util.List<com.adincube.sdk.g.c.e> r1 = r14.f2660     // Catch:{ a -> 0x001a }
            java.util.Iterator r3 = r1.iterator()     // Catch:{ a -> 0x001a }
        L_0x0096:
            boolean r1 = r3.hasNext()     // Catch:{ a -> 0x001a }
            if (r1 == 0) goto L_0x014d
            java.lang.Object r1 = r3.next()     // Catch:{ a -> 0x001a }
            com.adincube.sdk.g.c.e r1 = (com.adincube.sdk.g.c.e) r1     // Catch:{ a -> 0x001a }
            com.adincube.sdk.mediation.b r1 = r1.f2663     // Catch:{ a -> 0x001a }
            com.adincube.sdk.mediation.h r1 = r1.m3429()     // Catch:{ a -> 0x001a }
            r2.add(r1)     // Catch:{ a -> 0x001a }
            goto L_0x0096
        L_0x00ac:
            java.util.List<com.adincube.sdk.g.c.e> r0 = r14.f2660     // Catch:{ a -> 0x001a }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ a -> 0x001a }
        L_0x00b2:
            boolean r0 = r3.hasNext()     // Catch:{ a -> 0x001a }
            if (r0 == 0) goto L_0x0086
            java.lang.Object r0 = r3.next()     // Catch:{ a -> 0x001a }
            com.adincube.sdk.g.c.e r0 = (com.adincube.sdk.g.c.e) r0     // Catch:{ a -> 0x001a }
            java.lang.String r1 = r0.f2667     // Catch:{ a -> 0x001a }
            org.json.JSONObject r1 = r8.m3222((java.lang.String) r1)     // Catch:{ a -> 0x001a }
            if (r1 != 0) goto L_0x00d6
            com.adincube.sdk.c.b.b r1 = new com.adincube.sdk.c.b.b     // Catch:{ a -> 0x001a }
            java.lang.String r0 = r0.f2667     // Catch:{ a -> 0x001a }
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ a -> 0x001a }
            java.lang.String r3 = "No network configuration available for the network in config."
            r2.<init>(r3)     // Catch:{ a -> 0x001a }
            r1.<init>(r0, r2)     // Catch:{ a -> 0x001a }
            throw r1     // Catch:{ a -> 0x001a }
        L_0x00d6:
            com.adincube.sdk.f.b.h r9 = r13.f2161     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            java.lang.String r10 = r0.f2667     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            com.adincube.sdk.mediation.h r9 = r9.m3041(r10)     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            if (r9 != 0) goto L_0x00f0
            com.adincube.sdk.c.b.e r1 = new com.adincube.sdk.c.b.e     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            java.lang.String r9 = r0.f2667     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            r1.<init>(r9)     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            throw r1     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
        L_0x00e8:
            r0 = move-exception
            r0.a()     // Catch:{ a -> 0x001a }
            r3.remove()     // Catch:{ a -> 0x001a }
            goto L_0x00b2
        L_0x00f0:
            boolean r10 = r9.m3484()     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            if (r10 != 0) goto L_0x00f9
            r9.m3482(r2, r1)     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
        L_0x00f9:
            com.adincube.sdk.mediation.d r1 = r9.m3474()     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            r0.f2665 = r1     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            boolean r1 = r14.f2659     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            com.adincube.sdk.mediation.b r1 = r13.m2647(r1, r9, r2)     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            r0.f2663 = r1     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            com.adincube.sdk.mediation.b r1 = r0.f2663     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            r1.m3433()     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            com.adincube.sdk.mediation.b r1 = r0.f2663     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            org.json.JSONObject r9 = r0.f2664     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            r1.m3436((org.json.JSONObject) r9)     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            com.adincube.sdk.mediation.b r1 = r0.f2663     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            com.adincube.sdk.mediation.f r9 = r13.f2153     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            r1.m3435((com.adincube.sdk.mediation.f) r9)     // Catch:{ d -> 0x00e8, a -> 0x011b, Throwable -> 0x0128 }
            goto L_0x00b2
        L_0x011b:
            r1 = move-exception
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ a -> 0x001a }
            r3 = 0
            java.lang.String r0 = r0.f2667     // Catch:{ a -> 0x001a }
            r2[r3] = r0     // Catch:{ a -> 0x001a }
            r0 = 1
            r2[r0] = r1     // Catch:{ a -> 0x001a }
            throw r1     // Catch:{ a -> 0x001a }
        L_0x0128:
            r1 = move-exception
            java.lang.String r9 = "Error catched while initializing network '%s'. Mediation will continue without."
            r10 = 2
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ a -> 0x001a }
            r11 = 0
            java.lang.String r12 = r0.f2667     // Catch:{ a -> 0x001a }
            r10[r11] = r12     // Catch:{ a -> 0x001a }
            r11 = 1
            r10[r11] = r1     // Catch:{ a -> 0x001a }
            com.adincube.sdk.util.a.m3513(r9, r10)     // Catch:{ a -> 0x001a }
            java.lang.String r9 = "AbstractNetworkMediationManager.configureMediationAdaptersForNetworkOrder"
            java.lang.String r0 = r0.f2667     // Catch:{ a -> 0x001a }
            com.adincube.sdk.g.c.b r10 = r14.f2662     // Catch:{ a -> 0x001a }
            com.adincube.sdk.g.c.a r11 = r13.m2637()     // Catch:{ a -> 0x001a }
            com.adincube.sdk.util.ErrorReportingHelper.m3509((java.lang.String) r9, (java.lang.String) r0, (com.adincube.sdk.g.c.b) r10, (com.adincube.sdk.g.c.a) r11, (java.lang.Throwable) r1)     // Catch:{ a -> 0x001a }
            r3.remove()     // Catch:{ a -> 0x001a }
            goto L_0x00b2
        L_0x014d:
            java.util.Iterator r9 = r2.iterator()     // Catch:{ a -> 0x001a }
        L_0x0151:
            boolean r1 = r9.hasNext()     // Catch:{ a -> 0x001a }
            if (r1 == 0) goto L_0x0170
            java.lang.Object r1 = r9.next()     // Catch:{ a -> 0x001a }
            com.adincube.sdk.mediation.h r1 = (com.adincube.sdk.mediation.h) r1     // Catch:{ a -> 0x001a }
            boolean r2 = r1.m3478()     // Catch:{ a -> 0x001a }
            if (r2 == 0) goto L_0x0151
            com.adincube.sdk.mediation.d r3 = r1.m3474()     // Catch:{ a -> 0x001a }
            java.lang.String r2 = r1.m3473()     // Catch:{ a -> 0x001a }
            r1 = r14
            r0.m3045((com.adincube.sdk.g.c.d) r1, (java.lang.String) r2, (com.adincube.sdk.mediation.d) r3, (long) r4)     // Catch:{ a -> 0x001a }
            goto L_0x0151
        L_0x0170:
            r13.m2625(r14)     // Catch:{ a -> 0x001a }
            com.adincube.sdk.f.b.c.i r0 = r14.f2656     // Catch:{ a -> 0x001a }
            if (r0 != 0) goto L_0x0013
            com.adincube.sdk.f.b.c.i r0 = r13.m2643(r14, r8)     // Catch:{ a -> 0x001a }
            r14.f2656 = r0     // Catch:{ a -> 0x001a }
            com.adincube.sdk.f.b.c.i r0 = r14.f2656     // Catch:{ a -> 0x001a }
            r0.m2920()     // Catch:{ a -> 0x001a }
            goto L_0x0013
        L_0x0184:
            r13.m2638((com.adincube.sdk.g.c.d) r14)     // Catch:{ a -> 0x001a }
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.f.b.a.m2651(com.adincube.sdk.g.c.d, boolean):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2652(e eVar) {
        Object[] objArr = {eVar.f2667, Long.valueOf(eVar.f2665.m3464(m2646())), eVar.f2664};
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2653(e eVar, com.adincube.sdk.f.b.c.e eVar2) {
        Object[] objArr = {eVar.f2667, eVar2};
        g gVar = this.f2146;
        com.adincube.sdk.g.c.b r4 = m2646();
        Integer num = null;
        com.adincube.sdk.mediation.e r5 = eVar.f2663.m3431();
        if (r5 != null) {
            num = r5.f2948;
        }
        String str = eVar.f2667;
        if (eVar2 == com.adincube.sdk.f.b.c.e.LOADED || eVar2 == com.adincube.sdk.f.b.c.e.NO_MORE_INVENTORY) {
            g.a aVar = new g.a();
            aVar.f2436 = str;
            aVar.f2433 = r4;
            aVar.f2435 = num;
            aVar.f2434 = eVar2 == com.adincube.sdk.f.b.c.e.NO_MORE_INVENTORY;
            aVar.f2432 = System.currentTimeMillis();
            gVar.m3039(aVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2654(com.adincube.sdk.mediation.b bVar) {
        if (bVar != null) {
            this.f2159.m2942(m2646(), bVar.m3429().m3473());
            bVar.m3434((com.adincube.sdk.mediation.a) null);
            m2639(bVar);
            try {
                bVar.m3428();
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("Error caught while dismissing network '%s'. Mediation will continue.", bVar.m3429().m3473(), th);
                ErrorReportingHelper.m3509("AbstractNetworkMediationManager.dismissMediationAdapter", bVar.m3429().m3473(), m2646(), m2637(), th);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2655(boolean z, com.adincube.sdk.c.a.c cVar) {
        this.f2156.m2952(z, cVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m2656(d dVar) {
        return d.m3225(dVar) && dVar.f2656.m2923();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m2657(d dVar, com.adincube.sdk.g.b.b bVar) {
        return m2640(dVar, bVar);
    }
}
