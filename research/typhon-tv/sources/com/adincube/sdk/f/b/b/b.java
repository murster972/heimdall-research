package com.adincube.sdk.f.b.b;

import com.adincube.sdk.AdinCubeInterstitialEventListener;
import com.adincube.sdk.c.a.c;
import com.adincube.sdk.f.b.d;
import com.adincube.sdk.util.c.a;
import com.adincube.sdk.util.o;
import java.util.HashSet;
import java.util.Set;

public class b implements d {

    /* renamed from: 齉  reason: contains not printable characters */
    private static b f2257 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public Set<AdinCubeInterstitialEventListener> f2258 = new HashSet();

    /* renamed from: 龘  reason: contains not printable characters */
    public AdinCubeInterstitialEventListener f2259 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public static b m2777() {
        if (f2257 == null) {
            synchronized (b.class) {
                if (f2257 == null) {
                    f2257 = new b();
                }
            }
        }
        return f2257;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2778(boolean z, c cVar) {
        new Object[1][0] = cVar.a;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2779() {
        o.m3703("InterstitialEventListenerManager.onInterstitialAdHidden", this.f2258, new a<AdinCubeInterstitialEventListener>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* synthetic */ void m2785(Object obj) {
                ((AdinCubeInterstitialEventListener) obj).m2335();
            }
        });
        o.m3699(this.f2259, new a<AdinCubeInterstitialEventListener>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* synthetic */ void m2786(Object obj) {
                ((AdinCubeInterstitialEventListener) obj).m2335();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2780(final c cVar) {
        new StringBuilder("onError - ").append(cVar.a);
        o.m3703("InterstitialEventListenerManager.onInterstitialError", this.f2258, new a<AdinCubeInterstitialEventListener>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* synthetic */ void m2792(Object obj) {
                ((AdinCubeInterstitialEventListener) obj).m2338(cVar.a);
            }
        });
        o.m3699(this.f2259, new a<AdinCubeInterstitialEventListener>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* synthetic */ void m2793(Object obj) {
                ((AdinCubeInterstitialEventListener) obj).m2338(cVar.a);
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2781(boolean z) {
        o.m3703("InterstitialEventListenerManager.onAdCached", this.f2258, new a<AdinCubeInterstitialEventListener>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* synthetic */ void m2784(Object obj) {
                ((AdinCubeInterstitialEventListener) obj).m2337();
            }
        });
        o.m3699(this.f2259, new a<AdinCubeInterstitialEventListener>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* synthetic */ void m2787(Object obj) {
                ((AdinCubeInterstitialEventListener) obj).m2337();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2782(boolean z, c cVar) {
        cVar.a();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2783() {
        return this.f2259 != null;
    }
}
