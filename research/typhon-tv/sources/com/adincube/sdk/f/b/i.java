package com.adincube.sdk.f.b;

import com.adincube.sdk.f.b.g;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.g.c.d;
import com.adincube.sdk.g.c.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class i {

    /* renamed from: 靐  reason: contains not printable characters */
    private g f2440;

    /* renamed from: 龘  reason: contains not printable characters */
    private b f2441;

    public i(b bVar, g gVar) {
        this.f2441 = bVar;
        this.f2440 = gVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m3042(List<Integer> list, g.a aVar, long j, long j2) {
        int i;
        if (j <= 0 || aVar.f2435 == null) {
            return list.get(list.size() - 1).intValue();
        }
        long j3 = j2 - aVar.f2432;
        long j4 = j3 < 0 ? 0 : j3 / j;
        int intValue = list.get(0).intValue();
        Iterator<Integer> it2 = list.iterator();
        int i2 = 0;
        int i3 = intValue;
        while (it2.hasNext() && ((long) i2) <= j4) {
            int intValue2 = it2.next().intValue();
            if (intValue2 < aVar.f2435.intValue()) {
                i3 = intValue2;
            } else if (!aVar.f2434 || intValue2 < aVar.f2435.intValue()) {
                if (aVar.f2434 || intValue2 <= aVar.f2435.intValue()) {
                    i = i2;
                    intValue2 = i3;
                } else {
                    i = i2 + 1;
                }
                i2 = i;
                i3 = intValue2;
            } else {
                i2++;
                i3 = intValue2;
            }
        }
        return i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<Integer> m3043(List<e> list) {
        ArrayList arrayList = new ArrayList();
        for (e eVar : list) {
            Integer num = eVar.f2663.m3431().f2948;
            if (num != null) {
                arrayList.add(num);
            }
        }
        Collections.reverse(arrayList);
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<e> m3044(List<e> list, int i) {
        ArrayList arrayList = new ArrayList();
        for (e next : list) {
            Integer num = next.f2663.m3431().f2948;
            if (num != null && num.intValue() > i) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3045(d dVar, String str, com.adincube.sdk.mediation.d dVar2, long j) {
        g.a r1;
        Long l = dVar2.f2946;
        if (l != null && (r1 = this.f2440.m3037(this.f2441, str)) != null) {
            ArrayList arrayList = new ArrayList();
            for (e next : dVar.f2660) {
                if (next.f2667.equals(str)) {
                    arrayList.add(next);
                }
            }
            List<Integer> r0 = m3043(arrayList);
            if (!r0.isEmpty()) {
                List<e> r02 = m3044(arrayList, m3042(r0, r1, l.longValue(), j));
                if (!r02.isEmpty()) {
                    dVar.f2660.removeAll(r02);
                    StringBuilder sb = new StringBuilder();
                    for (e next2 : r02) {
                        if (sb.length() > 0) {
                            sb.append(", ");
                        }
                        sb.append(next2.f2663.m3431().f2948);
                    }
                    Object[] objArr = {str, sb};
                }
            }
        }
    }
}
