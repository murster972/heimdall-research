package com.adincube.sdk.f.b;

import android.content.Context;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.e.c;
import com.adincube.sdk.util.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class g {

    /* renamed from: 麤  reason: contains not printable characters */
    private static g f2428 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public static String f2429 = "fhe";

    /* renamed from: 靐  reason: contains not printable characters */
    List<a> f2430 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    private Context f2431;

    public static class a {

        /* renamed from: 连任  reason: contains not printable characters */
        public long f2432;

        /* renamed from: 靐  reason: contains not printable characters */
        public b f2433;

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean f2434;

        /* renamed from: 齉  reason: contains not printable characters */
        public Integer f2435;

        /* renamed from: 龘  reason: contains not printable characters */
        public String f2436;
    }

    private g(Context context) {
        this.f2431 = context;
        m3036(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized a m3034(b bVar, String str, boolean z) {
        a aVar;
        Iterator<a> it2 = this.f2430.iterator();
        while (true) {
            if (!it2.hasNext()) {
                aVar = null;
                break;
            }
            aVar = it2.next();
            if (aVar.f2433 == bVar && aVar.f2436.equals(str) && aVar.f2434 == z) {
                break;
            }
        }
        return aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static g m3035() {
        if (f2428 == null) {
            synchronized (g.class) {
                if (f2428 == null) {
                    f2428 = new g(f.m3605());
                }
            }
        }
        return f2428;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m3036(Context context) {
        synchronized (this) {
            if (context != null) {
                try {
                    JSONArray jSONArray = new JSONArray(context.getSharedPreferences("AIC-prefs", 0).getString(f2429, "[]"));
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject jSONObject = jSONArray.getJSONObject(i);
                        a aVar = new a();
                        aVar.f2436 = jSONObject.getString("n");
                        aVar.f2433 = b.a(jSONObject.getString("a"));
                        if (jSONObject.has("f")) {
                            aVar.f2435 = Integer.valueOf(jSONObject.getInt("f"));
                        }
                        aVar.f2434 = jSONObject.getBoolean("nf");
                        aVar.f2432 = jSONObject.getLong("t");
                        this.f2430.add(aVar);
                    }
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("NetworkFillHistoryManager.reloadEntries", th);
                    ErrorReportingHelper.m3511("NetworkFillHistoryManager.reloadEntries", th);
                }
            }
        }
        return;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized a m3037(b bVar, String str) {
        a aVar;
        aVar = null;
        for (a next : this.f2430) {
            if (next.f2433 != bVar || !next.f2436.equals(str) || (aVar != null && aVar.f2432 >= next.f2432)) {
                next = aVar;
            }
            aVar = next;
        }
        return aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized List<a> m3038(b bVar) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (a next : this.f2430) {
            if (next.f2433 == bVar && next.f2435 != null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m3039(a aVar) {
        a r0 = m3034(aVar.f2433, aVar.f2436, aVar.f2434);
        if (r0 != null && r0.f2432 < aVar.f2432) {
            this.f2430.remove(r0);
        }
        this.f2430.add(aVar);
        Context context = this.f2431;
        if (context != null) {
            try {
                JSONArray jSONArray = new JSONArray();
                for (a next : this.f2430) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("n", (Object) next.f2436);
                    jSONObject.put("a", (Object) next.f2433.e);
                    if (next.f2435 != null) {
                        jSONObject.put("f", (Object) next.f2435);
                    }
                    jSONObject.put("nf", next.f2434);
                    jSONObject.put("t", next.f2432);
                    jSONArray.put((Object) jSONObject);
                }
                c.m3601(context, f2429, jSONArray.toString());
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("NetworkFillHistoryManager.saveEntries", th);
                ErrorReportingHelper.m3511("NetworkFillHistoryManager.saveEntries", th);
            }
        }
        return;
    }
}
