package com.adincube.sdk.f.b.d;

import com.adincube.sdk.AdinCubeNativeEventListener;
import com.adincube.sdk.c.a.c;
import com.adincube.sdk.f.b.d;
import com.adincube.sdk.util.o;

public final class b implements d {

    /* renamed from: 靐  reason: contains not printable characters */
    public AdinCubeNativeEventListener f2364;

    /* renamed from: 龘  reason: contains not printable characters */
    a f2365;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2972(AdinCubeNativeEventListener adinCubeNativeEventListener);

        /* renamed from: 龘  reason: contains not printable characters */
        void m2973(AdinCubeNativeEventListener adinCubeNativeEventListener, c cVar);
    }

    public b(AdinCubeNativeEventListener adinCubeNativeEventListener, a aVar) {
        this.f2364 = adinCubeNativeEventListener;
        this.f2365 = aVar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2962(boolean z, final c cVar) {
        new StringBuilder("onLoadError - ").append(cVar.a);
        o.m3699(this.f2364, new com.adincube.sdk.util.c.a<AdinCubeNativeEventListener>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* bridge */ /* synthetic */ void m2968(Object obj) {
                b.this.f2365.m2973((AdinCubeNativeEventListener) obj, cVar);
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2963(boolean z) {
        o.m3700("NativeAdEventListenerManager.onAdCached", this.f2365, new com.adincube.sdk.util.c.a<a>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final /* bridge */ /* synthetic */ void m2966(Object obj) {
                ((a) obj).m2972(b.this.f2364);
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2964(boolean z, c cVar) {
        m2962(false, cVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2965() {
        return this.f2364 != null;
    }
}
