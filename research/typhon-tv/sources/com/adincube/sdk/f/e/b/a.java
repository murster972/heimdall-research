package com.adincube.sdk.f.e.b;

import android.view.MotionEvent;
import android.view.View;
import com.adincube.sdk.f.e.b;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.g.b.c;
import com.adincube.sdk.mediation.v.f;
import com.adincube.sdk.util.ErrorReportingHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public final class a implements View.OnTouchListener {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public AtomicBoolean f2510 = new AtomicBoolean(false);

    /* renamed from: ʼ  reason: contains not printable characters */
    private Long f2511 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private com.adincube.sdk.f.e.a f2512 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public List<C0009a> f2513 = new ArrayList();

    /* renamed from: 麤  reason: contains not printable characters */
    private f f2514 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private e f2515 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Long f2516 = null;

    /* renamed from: com.adincube.sdk.f.e.b.a$a  reason: collision with other inner class name */
    private static class C0009a {

        /* renamed from: 靐  reason: contains not printable characters */
        public String f2518;

        /* renamed from: 麤  reason: contains not printable characters */
        public Long f2519;

        /* renamed from: 齉  reason: contains not printable characters */
        public long f2520;

        /* renamed from: 龘  reason: contains not printable characters */
        public long f2521;

        private C0009a() {
            this.f2521 = System.currentTimeMillis();
        }

        public /* synthetic */ C0009a(byte b) {
            this();
        }
    }

    public a(e eVar, f fVar, com.adincube.sdk.f.e.a aVar) {
        this.f2515 = eVar;
        this.f2514 = fVar;
        this.f2512 = aVar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static /* synthetic */ void m3135(a aVar) {
        boolean z;
        try {
            synchronized (aVar.f2513) {
                long currentTimeMillis = System.currentTimeMillis();
                Iterator<C0009a> it2 = aVar.f2513.iterator();
                while (it2.hasNext()) {
                    C0009a next = it2.next();
                    if (currentTimeMillis - next.f2521 >= aVar.f2514.ˊ || next.f2519 != null) {
                        aVar.m3140(next);
                        it2.remove();
                    }
                }
                z = !aVar.f2513.isEmpty();
            }
            if (z) {
                aVar.m3137();
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("MRAIDAutoRedirectDetector.sendWaitingReports", th);
            ErrorReportingHelper.m3511("MRAIDAutoRedirectDetector.sendWaitingReports", th);
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        try {
            if (motionEvent.getAction() == 0) {
                this.f2511 = Long.valueOf(System.currentTimeMillis());
                synchronized (this.f2513) {
                    for (C0009a next : this.f2513) {
                        next.f2519 = Long.valueOf(System.currentTimeMillis() - next.f2521);
                    }
                }
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("MRAIDAutoRedirectDetector.onTouch", th);
            ErrorReportingHelper.m3511("MRAIDAutoRedirectDetector.onTouch", th);
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        if (r5.f2510.compareAndSet(false, true) == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0018, code lost:
        new java.lang.Object[1][0] = java.lang.Long.valueOf(r5.f2514.ˊ);
        com.adincube.sdk.util.o.m3702("MRAIDAutoRedirectDetector.scheduleAutoRedirectReportIfNecessary", (java.lang.Runnable) new com.adincube.sdk.f.e.b.a.AnonymousClass1(r5), r5.f2514.ˊ);
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m3137() {
        /*
            r5 = this;
            r2 = 1
            r4 = 0
            java.util.List<com.adincube.sdk.f.e.b.a$a> r1 = r5.f2513
            monitor-enter(r1)
            java.util.List<com.adincube.sdk.f.e.b.a$a> r0 = r5.f2513     // Catch:{ all -> 0x0034 }
            int r0 = r0.size()     // Catch:{ all -> 0x0034 }
            if (r0 != 0) goto L_0x000f
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
        L_0x000e:
            return
        L_0x000f:
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
            java.util.concurrent.atomic.AtomicBoolean r0 = r5.f2510
            boolean r0 = r0.compareAndSet(r4, r2)
            if (r0 == 0) goto L_0x000e
            java.lang.Object[] r0 = new java.lang.Object[r2]
            com.adincube.sdk.mediation.v.f r1 = r5.f2514
            long r2 = r1.ˊ
            java.lang.Long r1 = java.lang.Long.valueOf(r2)
            r0[r4] = r1
            java.lang.String r0 = "MRAIDAutoRedirectDetector.scheduleAutoRedirectReportIfNecessary"
            com.adincube.sdk.f.e.b.a$1 r1 = new com.adincube.sdk.f.e.b.a$1
            r1.<init>()
            com.adincube.sdk.mediation.v.f r2 = r5.f2514
            long r2 = r2.ˊ
            com.adincube.sdk.util.o.m3702((java.lang.String) r0, (java.lang.Runnable) r1, (long) r2)
            goto L_0x000e
        L_0x0034:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.f.e.b.a.m3137():void");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m3138() {
        return this.f2511 == null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3139() {
        this.f2516 = Long.valueOf(System.currentTimeMillis());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3140(C0009a aVar) {
        com.adincube.sdk.f.e.a aVar2 = this.f2512;
        e eVar = this.f2515;
        String str = aVar.f2518;
        long j = aVar.f2520;
        Long l = aVar.f2519;
        if (aVar2.f2500.m3716(aVar2.f2501.m2610(true, true), com.adincube.sdk.g.e.AutoRedirect, c.SHOW)) {
            b bVar = aVar2.f2499;
            com.adincube.sdk.i.b.b bVar2 = new com.adincube.sdk.i.b.b();
            bVar2.f2765 = bVar.f2509;
            bVar2.f2818 = eVar;
            bVar2.f2819 = str;
            bVar2.f2817 = Long.valueOf(j);
            bVar2.f2816 = l;
            bVar2.m3296();
        }
        aVar2.f2500.m3715(com.adincube.sdk.g.e.AutoRedirect, c.SHOW);
    }
}
