package com.adincube.sdk.f.e.c;

import android.net.Uri;
import com.adincube.sdk.j.b.b.d;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public final class a {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f2522;

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean f2523;

    public a(boolean z, boolean z2) {
        this.f2523 = z;
        this.f2522 = z2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3141(com.adincube.sdk.g.a.a.a aVar, com.adincube.sdk.j.a.a aVar2, Long l) {
        new Object[1][0] = Integer.valueOf(aVar2.k);
        m3143("Error-" + aVar2.k, aVar.m3175("e"), aVar, l);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3142(com.adincube.sdk.g.a.a.a aVar, d dVar, Long l) {
        new Object[1][0] = dVar.w;
        m3143("Event-" + dVar.w, com.adincube.sdk.j.d.a.m3399(aVar, dVar), aVar, l);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3143(String str, List<String> list, com.adincube.sdk.g.a.a.a aVar, Long l) {
        if (!this.f2522) {
            for (String next : list) {
                try {
                    if (aVar != null) {
                        next = next.replace("[ASSETURI]", Uri.encode(aVar.m3180("u")));
                    }
                    if (l != null) {
                        next = next.replace("[CONTENTPLAYHEAD]", String.format(Locale.US, "%02d:%02d:%02d.%03d", new Object[]{Long.valueOf(l.longValue() / 3600000), Long.valueOf((l.longValue() / 60000) % 60), Long.valueOf((l.longValue() / 1000) % 60), Long.valueOf(l.longValue() % 100)}));
                    }
                    int nextInt = new Random(System.currentTimeMillis()).nextInt();
                    String str2 = str;
                    new com.adincube.sdk.j.c.a(str2, new URL(next.replace("[CACHEBUSTING]", String.format(Locale.US, "%08d", new Object[]{Integer.valueOf(nextInt)}).substring(0, 8)))).m3296();
                } catch (MalformedURLException e) {
                }
            }
        }
    }
}
