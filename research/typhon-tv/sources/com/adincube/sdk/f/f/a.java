package com.adincube.sdk.f.f;

import android.os.Handler;
import android.os.HandlerThread;
import com.adincube.sdk.util.ErrorReportingHelper;

public abstract class a implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    b f2524 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    Handler f2525 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f2526 = false;

    public void run() {
        b bVar = this.f2524;
        try {
            synchronized (bVar.f2528) {
                Thread currentThread = Thread.currentThread();
                for (HandlerThread next : bVar.f2531) {
                    if (next == currentThread) {
                        new Object[1][0] = next.getName();
                        bVar.f2528.remove(next);
                    }
                }
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("ThreadManager.notifyWillStart", th);
            ErrorReportingHelper.m3511("ThreadManager.notifyWillStart", th);
        }
        try {
            m3145();
        } catch (Throwable th2) {
            com.adincube.sdk.util.a.m3513("ManagedThreadRunnable.run", th2);
            ErrorReportingHelper.m3511("ManagedThreadRunnable.run", th2);
        } finally {
            m3144();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3144() {
        if (!this.f2526) {
            this.f2526 = true;
            b bVar = this.f2524;
            try {
                synchronized (bVar.f2528) {
                    Thread currentThread = Thread.currentThread();
                    for (HandlerThread next : bVar.f2531) {
                        if (next == currentThread) {
                            new Object[1][0] = next.getName();
                            bVar.f2528.add(next);
                        }
                    }
                }
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("ThreadManager.notifyWillFinish", th);
                ErrorReportingHelper.m3511("ThreadManager.notifyWillFinish", th);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3145() {
    }
}
