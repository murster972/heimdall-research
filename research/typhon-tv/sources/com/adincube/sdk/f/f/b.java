package com.adincube.sdk.f.f;

import android.os.Handler;
import android.os.HandlerThread;
import com.adincube.sdk.f.a;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class b {

    /* renamed from: 连任  reason: contains not printable characters */
    private a f2527 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public final Deque<HandlerThread> f2528 = new ArrayDeque();

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f2529 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    public Map<HandlerThread, Handler> f2530 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    public List<HandlerThread> f2531 = new ArrayList();

    public b(a aVar) {
        this.f2527 = aVar;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private int m3146() {
        return this.f2527.m2610(true, true).f2615;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m3147() {
        boolean z = false;
        synchronized (this.f2528) {
            if (!this.f2529) {
                if (this.f2528.size() < m3146()) {
                    if (this.f2531.size() >= m3146()) {
                        z = true;
                    }
                }
            }
        }
        return z;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final HandlerThread m3148() {
        if (this.f2531.size() >= m3146()) {
            return null;
        }
        String format = String.format(Locale.US, "AIC-Download-%d", new Object[]{Integer.valueOf(this.f2531.size())});
        Object[] objArr = {format, Integer.valueOf(this.f2531.size()), Integer.valueOf(m3146())};
        HandlerThread handlerThread = new HandlerThread(format, 10);
        handlerThread.start();
        Handler handler = new Handler(handlerThread.getLooper());
        this.f2531.add(handlerThread);
        this.f2530.put(handlerThread, handler);
        return handlerThread;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3149(a aVar, Handler handler) {
        aVar.f2524 = this;
        aVar.f2525 = handler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3150() {
        synchronized (this.f2528) {
            if (this.f2529) {
                return false;
            }
            if (this.f2531.size() < m3146()) {
                return true;
            }
            return this.f2528.size() > 0;
        }
    }
}
