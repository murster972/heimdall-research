package com.adincube.sdk.f.d;

import com.adincube.sdk.f.f.a;
import com.adincube.sdk.g.d.b;
import com.adincube.sdk.util.ErrorReportingHelper;
import java.io.IOException;

public final class d extends a {

    /* renamed from: 龘  reason: contains not printable characters */
    private e f2496 = null;

    public d(e eVar) {
        this.f2496 = eVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008f A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00bd  */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m3108(com.adincube.sdk.g.d.b r12) {
        /*
            r11 = 500(0x1f4, float:7.0E-43)
            r2 = 1
            r4 = 0
            r1 = 0
            java.util.concurrent.atomic.AtomicBoolean r0 = r12.f2695
            boolean r0 = r0.get()
            if (r0 == 0) goto L_0x000e
        L_0x000d:
            return r1
        L_0x000e:
            android.content.Context r0 = com.adincube.sdk.util.f.m3605()
            java.io.File r6 = r12.f2682
            java.io.File r5 = com.adincube.sdk.util.n.m3692((android.content.Context) r0)     // Catch:{ InterruptedIOException -> 0x0105, all -> 0x00f6 }
            java.net.URL r0 = new java.net.URL     // Catch:{ InterruptedIOException -> 0x010b, all -> 0x00fc }
            java.lang.String r3 = r12.f2693     // Catch:{ InterruptedIOException -> 0x010b, all -> 0x00fc }
            r0.<init>(r3)     // Catch:{ InterruptedIOException -> 0x010b, all -> 0x00fc }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ InterruptedIOException -> 0x010b, all -> 0x00fc }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ InterruptedIOException -> 0x010b, all -> 0x00fc }
            java.lang.String r3 = r12.f2692     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            if (r3 == 0) goto L_0x0031
            java.lang.String r3 = "User-Agent"
            java.lang.String r7 = r12.f2692     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            r0.setRequestProperty(r3, r7)     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
        L_0x0031:
            java.lang.Integer r3 = r12.f2685     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            if (r3 == 0) goto L_0x003e
            java.lang.Integer r3 = r12.f2685     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            int r3 = r3.intValue()     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            r0.setConnectTimeout(r3)     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
        L_0x003e:
            java.lang.Integer r3 = r12.f2686     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            if (r3 == 0) goto L_0x004b
            java.lang.Integer r3 = r12.f2686     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            int r3 = r3.intValue()     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            r0.setReadTimeout(r3)     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
        L_0x004b:
            int r7 = r0.getResponseCode()     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r7 != r3) goto L_0x0114
            java.io.InputStream r8 = com.adincube.sdk.util.q.m3709(r0)     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            java.lang.String r9 = r5.getAbsolutePath()     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            r3.<init>(r9)     // Catch:{ InterruptedIOException -> 0x0110, all -> 0x0101 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
        L_0x0064:
            java.util.concurrent.atomic.AtomicBoolean r9 = r12.f2695     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            boolean r9 = r9.get()     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            if (r9 != 0) goto L_0x0096
            int r9 = r8.read(r4)     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            r10 = -1
            if (r9 == r10) goto L_0x0096
            r10 = 0
            r3.write(r4, r10, r9)     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            goto L_0x0064
        L_0x0078:
            r4 = move-exception
        L_0x0079:
            if (r0 == 0) goto L_0x007e
            r0.disconnect()
        L_0x007e:
            if (r5 == 0) goto L_0x0083
            r5.delete()
        L_0x0083:
            r3.close()     // Catch:{ Throwable -> 0x00ee }
            r0 = r2
        L_0x0087:
            java.util.concurrent.atomic.AtomicBoolean r3 = r12.f2695
            boolean r3 = r3.get()
            if (r3 != 0) goto L_0x0091
            if (r0 == 0) goto L_0x00f1
        L_0x0091:
            r6.delete()
            goto L_0x000d
        L_0x0096:
            r3.close()     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            r6.delete()     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            r5.renameTo(r6)     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            boolean r4 = r12.m3238()     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            if (r4 != 0) goto L_0x00a8
            r12.m3235()     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
        L_0x00a8:
            r4 = 400(0x190, float:5.6E-43)
            if (r7 < r4) goto L_0x00c4
            if (r7 >= r11) goto L_0x00c4
            com.adincube.sdk.c.c.a r4 = new com.adincube.sdk.c.c.a     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            r4.<init>(r7)     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            throw r4     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
        L_0x00b4:
            r1 = move-exception
            r2 = r0
        L_0x00b6:
            if (r2 == 0) goto L_0x00bb
            r2.disconnect()
        L_0x00bb:
            if (r5 == 0) goto L_0x00c0
            r5.delete()
        L_0x00c0:
            r3.close()     // Catch:{ Throwable -> 0x00f4 }
        L_0x00c3:
            throw r1
        L_0x00c4:
            if (r7 < r11) goto L_0x00dc
            com.adincube.sdk.c.c.d r4 = new com.adincube.sdk.c.c.d     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            java.lang.String r9 = "Server Error : "
            r8.<init>(r9)     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            java.lang.String r7 = r7.toString()     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            r4.<init>(r7)     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
            throw r4     // Catch:{ InterruptedIOException -> 0x0078, all -> 0x00b4 }
        L_0x00dc:
            if (r0 == 0) goto L_0x00e1
            r0.disconnect()
        L_0x00e1:
            if (r5 == 0) goto L_0x00e6
            r5.delete()
        L_0x00e6:
            r3.close()     // Catch:{ Throwable -> 0x00eb }
            r0 = r1
            goto L_0x0087
        L_0x00eb:
            r0 = move-exception
            r0 = r1
            goto L_0x0087
        L_0x00ee:
            r0 = move-exception
            r0 = r2
            goto L_0x0087
        L_0x00f1:
            r1 = r2
            goto L_0x000d
        L_0x00f4:
            r0 = move-exception
            goto L_0x00c3
        L_0x00f6:
            r0 = move-exception
            r1 = r0
            r3 = r4
            r5 = r4
            r2 = r4
            goto L_0x00b6
        L_0x00fc:
            r0 = move-exception
            r1 = r0
            r3 = r4
            r2 = r4
            goto L_0x00b6
        L_0x0101:
            r1 = move-exception
            r3 = r4
            r2 = r0
            goto L_0x00b6
        L_0x0105:
            r0 = move-exception
            r3 = r4
            r5 = r4
            r0 = r4
            goto L_0x0079
        L_0x010b:
            r0 = move-exception
            r3 = r4
            r0 = r4
            goto L_0x0079
        L_0x0110:
            r3 = move-exception
            r3 = r4
            goto L_0x0079
        L_0x0114:
            r3 = r4
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.f.d.d.m3108(com.adincube.sdk.g.d.b):boolean");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m3109() {
        boolean z;
        int i = -1;
        b bVar = this.f2496.f2498;
        try {
            i = bVar.f2691.incrementAndGet();
            com.adincube.sdk.util.a.m3515("Start downloading resource '%s'. Try: %d. Url: %s", bVar.m3236(), Integer.valueOf(i), bVar.f2693);
            z = !bVar.f2693.startsWith("file://") ? m3108(bVar) : m3111(bVar);
            if (z) {
                try {
                    com.adincube.sdk.util.a.m3515("Resource '%s' downloaded. Url: %s", bVar.m3236(), bVar.f2693);
                    m3144();
                    this.f2496.f2497.m3114(this.f2496);
                } catch (com.adincube.sdk.c.c.a e) {
                    e = e;
                } catch (com.adincube.sdk.c.c.d e2) {
                    e = e2;
                    com.adincube.sdk.util.a.m3513("Download failed due to server error. Http error code: %s. Url: %s", e.getMessage(), bVar.f2693);
                    m3110(i, true, true);
                    return z;
                } catch (IOException e3) {
                    e = e3;
                    com.adincube.sdk.util.a.m3513("Download failed. IOException: %s - %s. Url: %s", e.getClass().getName(), e.getMessage(), bVar.f2693);
                    m3110(i, false, true);
                    return z;
                } catch (Throwable th) {
                    th = th;
                    com.adincube.sdk.util.a.m3513("Unexpected error while downloading. Url: %s.", bVar.f2693, th);
                    ErrorReportingHelper.m3511("ResourceDownloader.download", th);
                    m3110(i, false, false);
                    return z;
                }
                return z;
            }
            Object[] objArr = {bVar.m3236(), bVar.f2693};
            m3144();
            this.f2496.f2497.m3113(this.f2496);
            return z;
        } catch (com.adincube.sdk.c.c.a e4) {
            e = e4;
            z = false;
        } catch (com.adincube.sdk.c.c.d e5) {
            e = e5;
            z = false;
            com.adincube.sdk.util.a.m3513("Download failed due to server error. Http error code: %s. Url: %s", e.getMessage(), bVar.f2693);
            m3110(i, true, true);
            return z;
        } catch (IOException e6) {
            e = e6;
            z = false;
            com.adincube.sdk.util.a.m3513("Download failed. IOException: %s - %s. Url: %s", e.getClass().getName(), e.getMessage(), bVar.f2693);
            m3110(i, false, true);
            return z;
        } catch (Throwable th2) {
            th = th2;
            z = false;
            com.adincube.sdk.util.a.m3513("Unexpected error while downloading. Url: %s.", bVar.f2693, th);
            ErrorReportingHelper.m3511("ResourceDownloader.download", th);
            m3110(i, false, false);
            return z;
        }
        com.adincube.sdk.util.a.m3513("Download failed due to client error. Http error code: %s. Url: %s", e.getMessage(), bVar.f2693);
        m3110(i, false, false);
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3110(int i, boolean z, boolean z2) {
        m3144();
        this.f2496.f2497.m3115(this.f2496, i, z, z2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005b A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00b8  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m3111(com.adincube.sdk.g.d.b r9) {
        /*
            r2 = 1
            r3 = 0
            r1 = 0
            java.util.concurrent.atomic.AtomicBoolean r0 = r9.f2695
            boolean r0 = r0.get()
            if (r0 == 0) goto L_0x000c
        L_0x000b:
            return r1
        L_0x000c:
            android.content.Context r0 = com.adincube.sdk.util.f.m3605()
            java.io.File r6 = r9.f2682
            java.io.File r5 = com.adincube.sdk.util.n.m3692((android.content.Context) r0)     // Catch:{ InterruptedIOException -> 0x00ac, all -> 0x0080 }
            java.net.URL r0 = new java.net.URL     // Catch:{ InterruptedIOException -> 0x00b1, all -> 0x00a0 }
            java.lang.String r4 = r9.f2693     // Catch:{ InterruptedIOException -> 0x00b1, all -> 0x00a0 }
            r0.<init>(r4)     // Catch:{ InterruptedIOException -> 0x00b1, all -> 0x00a0 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ InterruptedIOException -> 0x00b1, all -> 0x00a0 }
            java.io.InputStream r4 = r0.getInputStream()     // Catch:{ InterruptedIOException -> 0x00b1, all -> 0x00a0 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ InterruptedIOException -> 0x00b5, all -> 0x00a5 }
            java.lang.String r7 = r5.getAbsolutePath()     // Catch:{ InterruptedIOException -> 0x00b5, all -> 0x00a5 }
            r0.<init>(r7)     // Catch:{ InterruptedIOException -> 0x00b5, all -> 0x00a5 }
            r3 = 8192(0x2000, float:1.14794E-41)
            byte[] r3 = new byte[r3]     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
        L_0x0032:
            java.util.concurrent.atomic.AtomicBoolean r7 = r9.f2695     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
            boolean r7 = r7.get()     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
            if (r7 != 0) goto L_0x0061
            int r7 = r4.read(r3)     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
            r8 = -1
            if (r7 == r8) goto L_0x0061
            r8 = 0
            r0.write(r3, r8, r7)     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
            goto L_0x0032
        L_0x0046:
            r3 = move-exception
        L_0x0047:
            r4.close()     // Catch:{ Throwable -> 0x0098 }
        L_0x004a:
            r0.close()     // Catch:{ Throwable -> 0x009a }
        L_0x004d:
            if (r5 == 0) goto L_0x00b8
            r5.delete()
            r0 = r2
        L_0x0053:
            java.util.concurrent.atomic.AtomicBoolean r3 = r9.f2695
            boolean r3 = r3.get()
            if (r3 != 0) goto L_0x005d
            if (r0 == 0) goto L_0x0091
        L_0x005d:
            r6.delete()
            goto L_0x000b
        L_0x0061:
            r0.close()     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
            r6.delete()     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
            r5.renameTo(r6)     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
            boolean r3 = r9.m3238()     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
            if (r3 != 0) goto L_0x0073
            r9.m3235()     // Catch:{ InterruptedIOException -> 0x0046, all -> 0x00a9 }
        L_0x0073:
            r4.close()     // Catch:{ Throwable -> 0x0094 }
        L_0x0076:
            r0.close()     // Catch:{ Throwable -> 0x0096 }
        L_0x0079:
            if (r5 == 0) goto L_0x00ba
            r5.delete()
            r0 = r1
            goto L_0x0053
        L_0x0080:
            r0 = move-exception
            r1 = r0
            r2 = r3
            r4 = r3
            r5 = r3
        L_0x0085:
            r4.close()     // Catch:{ Throwable -> 0x009c }
        L_0x0088:
            r2.close()     // Catch:{ Throwable -> 0x009e }
        L_0x008b:
            if (r5 == 0) goto L_0x0090
            r5.delete()
        L_0x0090:
            throw r1
        L_0x0091:
            r1 = r2
            goto L_0x000b
        L_0x0094:
            r3 = move-exception
            goto L_0x0076
        L_0x0096:
            r0 = move-exception
            goto L_0x0079
        L_0x0098:
            r3 = move-exception
            goto L_0x004a
        L_0x009a:
            r0 = move-exception
            goto L_0x004d
        L_0x009c:
            r0 = move-exception
            goto L_0x0088
        L_0x009e:
            r0 = move-exception
            goto L_0x008b
        L_0x00a0:
            r0 = move-exception
            r1 = r0
            r2 = r3
            r4 = r3
            goto L_0x0085
        L_0x00a5:
            r0 = move-exception
            r1 = r0
            r2 = r3
            goto L_0x0085
        L_0x00a9:
            r1 = move-exception
            r2 = r0
            goto L_0x0085
        L_0x00ac:
            r0 = move-exception
            r0 = r3
            r4 = r3
            r5 = r3
            goto L_0x0047
        L_0x00b1:
            r0 = move-exception
            r0 = r3
            r4 = r3
            goto L_0x0047
        L_0x00b5:
            r0 = move-exception
            r0 = r3
            goto L_0x0047
        L_0x00b8:
            r0 = r2
            goto L_0x0053
        L_0x00ba:
            r0 = r1
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.f.d.d.m3111(com.adincube.sdk.g.d.b):boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3112() {
        m3109();
    }
}
