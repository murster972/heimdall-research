package com.adincube.sdk.f.d;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.adincube.sdk.util.o;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class a {

    /* renamed from: 齉  reason: contains not printable characters */
    private static a f2449 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public ExecutorService f2450 = null;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public b f2451 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<com.adincube.sdk.g.d.b, Set<b>> f2452 = new HashMap();

    /* renamed from: com.adincube.sdk.f.d.a$a  reason: collision with other inner class name */
    private class C0006a implements Runnable {

        /* renamed from: 靐  reason: contains not printable characters */
        private com.adincube.sdk.g.d.b f2459;

        private C0006a(com.adincube.sdk.g.d.b bVar) {
            this.f2459 = bVar;
        }

        public /* synthetic */ C0006a(a aVar, com.adincube.sdk.g.d.b bVar, byte b) {
            this(bVar);
        }

        public final void run() {
            try {
                if (a.this.m3069(this.f2459)) {
                    a.this.f2451.m3093(this.f2459);
                    a.m3066(a.this, this.f2459, BitmapFactory.decodeFile(this.f2459.f2682.getAbsolutePath()));
                }
            } catch (Throwable th) {
                a.m3067(a.this, this.f2459, th);
            }
        }
    }

    public interface b {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3072(com.adincube.sdk.g.d.b bVar, Bitmap bitmap);

        /* renamed from: 龘  reason: contains not printable characters */
        void m3073(Throwable th);
    }

    private a(b bVar) {
        this.f2451 = bVar;
        this.f2450 = Executors.newSingleThreadExecutor();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static a m3064() {
        if (f2449 == null) {
            synchronized (b.class) {
                if (f2449 == null) {
                    f2449 = new a(b.m3088());
                }
            }
        }
        return f2449;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m3066(a aVar, final com.adincube.sdk.g.d.b bVar, final Bitmap bitmap) {
        synchronized (aVar.f2452) {
            Set<b> remove = aVar.f2452.remove(bVar);
            if (remove != null) {
                for (b r0 : remove) {
                    o.m3699(r0, new com.adincube.sdk.util.c.a<b>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public final /* bridge */ /* synthetic */ void m3070(Object obj) {
                            ((b) obj).m3072(bVar, bitmap);
                        }
                    });
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m3067(a aVar, final com.adincube.sdk.g.d.b bVar, final Throwable th) {
        synchronized (aVar.f2452) {
            Set<b> remove = aVar.f2452.remove(bVar);
            if (remove != null) {
                for (b r0 : remove) {
                    o.m3699(r0, new com.adincube.sdk.util.c.a<b>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public final /* bridge */ /* synthetic */ void m3071(Object obj) {
                            ((b) obj).m3073(th);
                        }
                    });
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m3069(com.adincube.sdk.g.d.b bVar) {
        boolean containsKey;
        synchronized (this.f2452) {
            containsKey = this.f2452.containsKey(bVar);
        }
        return containsKey;
    }
}
