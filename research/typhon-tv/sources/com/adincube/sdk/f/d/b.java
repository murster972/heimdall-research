package com.adincube.sdk.f.d;

import com.adincube.sdk.f.a;
import com.adincube.sdk.g.d.b;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.n;
import com.adincube.sdk.util.o;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class b {

    /* renamed from: 龘  reason: contains not printable characters */
    private static b f2461 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    private com.adincube.sdk.f.a f2462;

    /* renamed from: ʼ  reason: contains not printable characters */
    private c f2463;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public ExecutorService f2464 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private b.C0013b f2465 = new b.C0013b() {
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m3095(com.adincube.sdk.g.d.b bVar) {
            b.m3082(b.this, bVar);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m3096(com.adincube.sdk.g.d.b bVar) {
            b.m3082(b.this, bVar);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3097(com.adincube.sdk.g.d.b bVar) {
            b.this.m3077(bVar);
            synchronized (b.this.f2469) {
                a r0 = b.m3087(bVar.f2682);
                b.this.f2469.add(r0);
                long r2 = b.this.m3084();
                Object[] objArr = {r0.f2484, Long.valueOf(r2), Long.valueOf(b.this.f2468)};
                if (r2 > b.this.f2468) {
                    b.m3079(b.this);
                }
            }
        }
    };

    /* renamed from: ٴ  reason: contains not printable characters */
    private final a.C0003a f2466 = new a.C0003a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3098(com.adincube.sdk.g.b.b bVar) {
            if (b.this.f2468 != bVar.f2595 && bVar.f2595 > 0) {
                Object[] objArr = {Long.valueOf(b.this.f2468), Long.valueOf(bVar.f2595)};
                long unused = b.this.f2468 = bVar.f2595;
                if (b.this.m3084() > bVar.f2595) {
                    b.this.f2464.submit(new C0007b(b.this, (byte) 0));
                }
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public Set<com.adincube.sdk.g.d.b> f2467 = new HashSet();
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public long f2468;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public List<a> f2469 = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public File f2470;

    private static class a {

        /* renamed from: 靐  reason: contains not printable characters */
        public long f2482;

        /* renamed from: 齉  reason: contains not printable characters */
        public long f2483;

        /* renamed from: 龘  reason: contains not printable characters */
        public String f2484;

        private a() {
        }

        /* synthetic */ a(byte b) {
            this();
        }
    }

    /* renamed from: com.adincube.sdk.f.d.b$b  reason: collision with other inner class name */
    private class C0007b implements Runnable {
        private C0007b() {
        }

        /* synthetic */ C0007b(b bVar, byte b) {
            this();
        }

        public final void run() {
            try {
                b.m3079(b.this);
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("ResourceCacheManager#CleanCacheRunnable.run", th);
                ErrorReportingHelper.m3511("ResourceCacheManager#CleanCacheRunnable.run", th);
            }
        }
    }

    private class c implements Runnable {

        /* renamed from: 靐  reason: contains not printable characters */
        private com.adincube.sdk.g.d.b f2486 = null;

        public c(com.adincube.sdk.g.d.b bVar) {
            this.f2486 = bVar;
        }

        public final void run() {
            try {
                if (this.f2486.f2695.get()) {
                    try {
                        synchronized (b.this.f2467) {
                            b.this.f2467.remove(this.f2486);
                        }
                    } catch (Throwable th) {
                    }
                } else {
                    if (!this.f2486.m3238()) {
                        this.f2486.m3235();
                        new Object[1][0] = this.f2486.m3236();
                    }
                    b.this.m3077(this.f2486);
                    try {
                        synchronized (b.this.f2467) {
                            b.this.f2467.remove(this.f2486);
                        }
                    } catch (Throwable th2) {
                    }
                }
            } catch (Throwable th3) {
            }
        }
    }

    private b(com.adincube.sdk.f.a aVar, File file) {
        this.f2462 = aVar;
        this.f2463 = new c(aVar);
        this.f2470 = file;
        com.adincube.sdk.g.b.b r0 = aVar.m2610(true, true);
        this.f2468 = r0 != null ? r0.f2595 : 104857600;
        this.f2464 = Executors.newSingleThreadExecutor();
        com.adincube.sdk.f.a aVar2 = this.f2462;
        a.C0003a aVar3 = this.f2466;
        synchronized (aVar2.f2130) {
            aVar2.f2130.add(aVar3);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m3075(com.adincube.sdk.g.d.b bVar) {
        if (bVar.f2682 == null) {
            bVar.f2682 = m3078(bVar);
        }
        return bVar.f2682.exists();
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m3077(final com.adincube.sdk.g.d.b bVar) {
        if (bVar.f2688 != null) {
            o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) new Runnable() {
                public final void run() {
                    try {
                        b.a aVar = bVar.f2688;
                        if (aVar != null) {
                            aVar.m3240(bVar);
                        }
                    } catch (Throwable th) {
                        com.adincube.sdk.util.a.m3513("ResourceCacheManager.notifyResourceCached", th);
                        ErrorReportingHelper.m3511("ResourceCacheManager.notifyResourceCached", th);
                    }
                }
            });
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private File m3078(com.adincube.sdk.g.d.b bVar) {
        return n.m3693(this.f2470, "AIC_RC_" + bVar.m3236());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    static /* synthetic */ void m3079(b bVar) {
        synchronized (bVar.f2469) {
            Collections.sort(bVar.f2469, new Comparator<a>() {
                public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                    a aVar = (a) obj;
                    a aVar2 = (a) obj2;
                    if (aVar.f2483 == aVar2.f2483) {
                        return 0;
                    }
                    return aVar.f2483 < aVar2.f2483 ? -1 : 1;
                }
            });
            long r2 = bVar.m3084();
            while (r2 > bVar.f2468 && bVar.f2469.size() > 0) {
                a remove = bVar.f2469.remove(0);
                n.m3693(bVar.f2470, remove.f2484).delete();
                r2 -= remove.f2482;
                Object[] objArr = {remove.f2484, Long.valueOf(r2), Long.valueOf(bVar.f2468)};
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static /* synthetic */ String m3081() {
        return "AIC_RC_";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static /* synthetic */ void m3082(b bVar, final com.adincube.sdk.g.d.b bVar2) {
        if (bVar2.f2688 != null) {
            o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) new Runnable() {
                public final void run() {
                    try {
                        b.a aVar = bVar2.f2688;
                        if (aVar != null) {
                            aVar.m3239(bVar2);
                        }
                    } catch (Throwable th) {
                        com.adincube.sdk.util.a.m3513("ResourceCacheManager.notifyResourceCachingFailed", th);
                        ErrorReportingHelper.m3511("ResourceCacheManager.notifyResourceCachingFailed", th);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m3084() {
        long j;
        synchronized (this.f2469) {
            j = 0;
            for (a aVar : this.f2469) {
                j = aVar.f2482 + j;
            }
        }
        return j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ a m3087(File file) {
        a aVar = new a((byte) 0);
        aVar.f2484 = file.getName();
        aVar.f2482 = file.length();
        aVar.f2483 = file.lastModified();
        return aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m3088() {
        if (f2461 == null) {
            synchronized (b.class) {
                if (f2461 == null) {
                    b bVar = new b(com.adincube.sdk.f.a.m2607(), f.m3605().getCacheDir());
                    f2461 = bVar;
                    bVar.f2464.submit(new Runnable() {
                        public final void run() {
                            try {
                                synchronized (b.this.f2469) {
                                    b.this.f2469.clear();
                                    File r3 = b.this.f2470;
                                    String r4 = b.m3081();
                                    ArrayList<File> arrayList = new ArrayList<>();
                                    String[] list = r3.list();
                                    if (list != null) {
                                        for (String str : list) {
                                            if (str.startsWith(r4)) {
                                                arrayList.add(n.m3693(r3, str));
                                            }
                                        }
                                    }
                                    for (File r0 : arrayList) {
                                        b.this.f2469.add(b.m3087(r0));
                                    }
                                    long r42 = b.this.m3084();
                                    Object[] objArr = {Long.valueOf(r42), Long.valueOf(b.this.f2468)};
                                    if (r42 > b.this.f2468) {
                                        b.m3079(b.this);
                                    }
                                }
                            } catch (Throwable th) {
                                com.adincube.sdk.util.a.m3513("ResourceCacheManager.rebuildCacheEntries", th);
                                ErrorReportingHelper.m3511("ResourceCacheManager.rebuildCacheEntries", th);
                            }
                        }
                    });
                }
            }
        }
        return f2461;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m3091(com.adincube.sdk.g.d.b bVar) {
        return m3075(bVar) && bVar.m3238();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m3092(com.adincube.sdk.g.d.b bVar) {
        c cVar = this.f2463;
        String str = bVar.f2693;
        synchronized (cVar.f2494) {
            com.adincube.sdk.g.d.b r1 = cVar.m3103(str);
            if (r1 != null && r1.f2684.decrementAndGet() == 0) {
                Object[] objArr = {r1.m3236(), r1.f2693};
                r1.f2695.set(true);
                cVar.f2494.remove(r1);
            }
        }
        synchronized (this.f2467) {
            this.f2467.remove(bVar);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3093(final com.adincube.sdk.g.d.b bVar) {
        final long currentTimeMillis = System.currentTimeMillis();
        bVar.f2682.setLastModified(currentTimeMillis);
        this.f2464.submit(new Runnable() {
            public final void run() {
                try {
                    synchronized (b.this.f2469) {
                        for (a aVar : b.this.f2469) {
                            if (aVar.f2484.equals(b.m3081() + bVar.m3236())) {
                                aVar.f2483 = currentTimeMillis;
                            }
                        }
                    }
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("ResourceCacheManager.touch", th);
                    ErrorReportingHelper.m3511("ResourceCacheManager.touch", th);
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final com.adincube.sdk.g.d.b m3094(com.adincube.sdk.g.d.b bVar) {
        if (bVar.f2682 == null) {
            bVar.f2682 = m3078(bVar);
        }
        if (m3075(bVar)) {
            synchronized (this.f2467) {
                if (!this.f2467.contains(bVar)) {
                    if (!bVar.m3238()) {
                        new Object[1][0] = bVar.m3236();
                        this.f2467.add(bVar);
                        this.f2464.submit(new c(bVar));
                    } else {
                        m3077(bVar);
                    }
                }
            }
            return bVar;
        }
        bVar.f2696 = this.f2465;
        com.adincube.sdk.g.d.b r0 = this.f2463.m3102(bVar);
        if (r0 == bVar) {
            return bVar;
        }
        r0.f2696 = this.f2465;
        r0.f2688 = bVar.f2688;
        return r0;
    }
}
