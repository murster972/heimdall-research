package com.adincube.sdk.f.d;

import com.adincube.sdk.f.d.e;
import com.adincube.sdk.f.f.b;
import com.adincube.sdk.g.d.b;
import com.adincube.sdk.util.q;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public final class c implements e.a {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f2488 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final com.adincube.sdk.f.f.a f2489 = new com.adincube.sdk.f.f.a() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3107() {
            m3144();
            c.this.f2491.compareAndSet(true, false);
            c.this.m3104();
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private b f2490 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    AtomicBoolean f2491 = new AtomicBoolean(false);

    /* renamed from: 麤  reason: contains not printable characters */
    private final Set<e> f2492 = new HashSet();

    /* renamed from: 齉  reason: contains not printable characters */
    private AtomicInteger f2493 = new AtomicInteger(0);

    /* renamed from: 龘  reason: contains not printable characters */
    final Queue<com.adincube.sdk.g.d.b> f2494 = new PriorityBlockingQueue(1, new a());

    static class a implements Comparator<com.adincube.sdk.g.d.b> {
        a() {
        }

        public final /* synthetic */ int compare(Object obj, Object obj2) {
            com.adincube.sdk.g.d.b bVar = (com.adincube.sdk.g.d.b) obj;
            com.adincube.sdk.g.d.b bVar2 = (com.adincube.sdk.g.d.b) obj2;
            long r2 = bVar.m3237();
            long r4 = bVar2.m3237();
            if (r2 <= r4) {
                if (r2 < r4) {
                    return -1;
                }
                if (bVar.f2683 == bVar2.f2683) {
                    return bVar.f2694.intValue() <= bVar2.f2694.intValue() ? 1 : -1;
                }
                if (bVar.f2683 >= bVar2.f2683) {
                    return -1;
                }
            }
            return 1;
        }
    }

    public c(com.adincube.sdk.f.a aVar) {
        this.f2490 = new b(aVar);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m3099(com.adincube.sdk.g.d.b bVar) {
        boolean z;
        synchronized (this.f2494) {
            Iterator<e> it2 = this.f2492.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (it2.next().f2498.f2693.equals(bVar.f2693)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
        }
        return z;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m3100(e eVar) {
        synchronized (this.f2494) {
            this.f2492.remove(eVar);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3101(e eVar) {
        m3100(eVar);
        com.adincube.sdk.g.d.b bVar = eVar.f2498;
        b.C0013b bVar2 = bVar.f2696;
        if (bVar2 != null) {
            bVar2.m3242(bVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized com.adincube.sdk.g.d.b m3102(com.adincube.sdk.g.d.b bVar) {
        if (!this.f2488) {
            synchronized (this.f2494) {
                com.adincube.sdk.g.d.b r0 = m3103(bVar.f2693);
                if (r0 == null) {
                    Object[] objArr = {bVar.m3236(), Integer.valueOf(bVar.f2683), bVar.f2693};
                    bVar.f2694 = Integer.valueOf(this.f2493.getAndIncrement());
                    this.f2494.add(bVar);
                    m3104();
                } else if (m3099(bVar)) {
                    Object[] objArr2 = {bVar.m3236(), bVar.f2693};
                    bVar = r0;
                } else {
                    r0.f2694 = Integer.valueOf(this.f2493.getAndIncrement());
                    r0.f2696 = bVar.f2696;
                    int incrementAndGet = r0.f2684.incrementAndGet();
                    this.f2494.remove(r0);
                    this.f2494.add(r0);
                    Object[] objArr3 = {r0.m3236(), Integer.valueOf(incrementAndGet), r0.f2693};
                    bVar = r0;
                }
            }
        }
        return bVar;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        r2 = r4.f2494.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        if (r2.hasNext() == false) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002f, code lost:
        r0 = (com.adincube.sdk.g.d.b) r2.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        if (r0.f2693.equals(r5) == false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0042, code lost:
        r0 = null;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.adincube.sdk.g.d.b m3103(java.lang.String r5) {
        /*
            r4 = this;
            java.util.Queue<com.adincube.sdk.g.d.b> r1 = r4.f2494
            monitor-enter(r1)
            java.util.Set<com.adincube.sdk.f.d.e> r0 = r4.f2492     // Catch:{ all -> 0x003f }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x003f }
        L_0x0009:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x0023
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x003f }
            com.adincube.sdk.f.d.e r0 = (com.adincube.sdk.f.d.e) r0     // Catch:{ all -> 0x003f }
            com.adincube.sdk.g.d.b r3 = r0.f2498     // Catch:{ all -> 0x003f }
            java.lang.String r3 = r3.f2693     // Catch:{ all -> 0x003f }
            boolean r3 = r3.equals(r5)     // Catch:{ all -> 0x003f }
            if (r3 == 0) goto L_0x0009
            com.adincube.sdk.g.d.b r0 = r0.f2498     // Catch:{ all -> 0x003f }
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
        L_0x0022:
            return r0
        L_0x0023:
            java.util.Queue<com.adincube.sdk.g.d.b> r0 = r4.f2494     // Catch:{ all -> 0x003f }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x003f }
        L_0x0029:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x0042
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x003f }
            com.adincube.sdk.g.d.b r0 = (com.adincube.sdk.g.d.b) r0     // Catch:{ all -> 0x003f }
            java.lang.String r3 = r0.f2693     // Catch:{ all -> 0x003f }
            boolean r3 = r3.equals(r5)     // Catch:{ all -> 0x003f }
            if (r3 == 0) goto L_0x0029
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            goto L_0x0022
        L_0x003f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            throw r0
        L_0x0042:
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.f.d.c.m3103(java.lang.String):com.adincube.sdk.g.d.b");
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processLoop(RegionMaker.java:225)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:106)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    /* renamed from: 龘  reason: contains not printable characters */
    final void m3104() {
        /*
            r8 = this;
            java.util.Queue<com.adincube.sdk.g.d.b> r2 = r8.f2494
            monitor-enter(r2)
        L_0x0003:
            java.util.Queue<com.adincube.sdk.g.d.b> r0 = r8.f2494     // Catch:{ all -> 0x004d }
            int r0 = r0.size()     // Catch:{ all -> 0x004d }
            if (r0 <= 0) goto L_0x00d6
            com.adincube.sdk.f.f.b r0 = r8.f2490     // Catch:{ all -> 0x004d }
            boolean r0 = r0.m3150()     // Catch:{ all -> 0x004d }
            if (r0 == 0) goto L_0x00d6
            java.util.Queue<com.adincube.sdk.g.d.b> r0 = r8.f2494     // Catch:{ all -> 0x004d }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x004d }
            com.adincube.sdk.g.d.b r0 = (com.adincube.sdk.g.d.b) r0     // Catch:{ all -> 0x004d }
            if (r0 == 0) goto L_0x0003
            long r4 = r0.m3237()     // Catch:{ a -> 0x0046 }
            r6 = 0
            int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r1 != 0) goto L_0x0076
            com.adincube.sdk.f.d.e r1 = new com.adincube.sdk.f.d.e     // Catch:{ a -> 0x0046 }
            r1.<init>(r0)     // Catch:{ a -> 0x0046 }
            r1.f2497 = r8     // Catch:{ a -> 0x0046 }
            java.util.Set<com.adincube.sdk.f.d.e> r3 = r8.f2492     // Catch:{ a -> 0x0046 }
            r3.add(r1)     // Catch:{ a -> 0x0046 }
            com.adincube.sdk.f.f.b r3 = r8.f2490     // Catch:{ a -> 0x0046 }
            com.adincube.sdk.f.d.d r4 = new com.adincube.sdk.f.d.d     // Catch:{ a -> 0x0046 }
            r4.<init>(r1)     // Catch:{ a -> 0x0046 }
            java.util.Deque<android.os.HandlerThread> r5 = r3.f2528     // Catch:{ a -> 0x0046 }
            monitor-enter(r5)     // Catch:{ a -> 0x0046 }
            boolean r1 = r3.f2529     // Catch:{ all -> 0x0043 }
            if (r1 == 0) goto L_0x0050
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x0003
        L_0x0043:
            r1 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            throw r1     // Catch:{ a -> 0x0046 }
        L_0x0046:
            r1 = move-exception
            java.util.Queue<com.adincube.sdk.g.d.b> r1 = r8.f2494     // Catch:{ all -> 0x004d }
            r1.add(r0)     // Catch:{ all -> 0x004d }
            goto L_0x0003
        L_0x004d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x004d }
            throw r0
        L_0x0050:
            java.util.Deque<android.os.HandlerThread> r1 = r3.f2528     // Catch:{ all -> 0x0043 }
            java.lang.Object r1 = r1.poll()     // Catch:{ all -> 0x0043 }
            android.os.HandlerThread r1 = (android.os.HandlerThread) r1     // Catch:{ all -> 0x0043 }
            if (r1 != 0) goto L_0x005e
            android.os.HandlerThread r1 = r3.m3148()     // Catch:{ all -> 0x0043 }
        L_0x005e:
            if (r1 != 0) goto L_0x0066
            com.adincube.sdk.c.d.a r1 = new com.adincube.sdk.c.d.a     // Catch:{ all -> 0x0043 }
            r1.<init>()     // Catch:{ all -> 0x0043 }
            throw r1     // Catch:{ all -> 0x0043 }
        L_0x0066:
            java.util.Map<android.os.HandlerThread, android.os.Handler> r6 = r3.f2530     // Catch:{ all -> 0x0043 }
            java.lang.Object r1 = r6.get(r1)     // Catch:{ all -> 0x0043 }
            android.os.Handler r1 = (android.os.Handler) r1     // Catch:{ all -> 0x0043 }
            r3.m3149(r4, r1)     // Catch:{ all -> 0x0043 }
            r1.post(r4)     // Catch:{ all -> 0x0043 }
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x0003
        L_0x0076:
            com.adincube.sdk.f.f.b r1 = r8.f2490     // Catch:{ a -> 0x0046 }
            boolean r1 = r1.m3147()     // Catch:{ a -> 0x0046 }
            if (r1 != 0) goto L_0x00cf
            java.util.concurrent.atomic.AtomicBoolean r1 = r8.f2491     // Catch:{ a -> 0x0046 }
            r3 = 0
            r4 = 1
            boolean r1 = r1.compareAndSet(r3, r4)     // Catch:{ a -> 0x0046 }
            if (r1 == 0) goto L_0x00cf
            java.util.Queue<com.adincube.sdk.g.d.b> r1 = r8.f2494     // Catch:{ a -> 0x0046 }
            r1.add(r0)     // Catch:{ a -> 0x0046 }
            com.adincube.sdk.f.f.b r3 = r8.f2490     // Catch:{ a -> 0x0046 }
            com.adincube.sdk.f.f.a r4 = r8.f2489     // Catch:{ a -> 0x0046 }
            java.util.Deque<android.os.HandlerThread> r5 = r3.f2528     // Catch:{ a -> 0x0046 }
            monitor-enter(r5)     // Catch:{ a -> 0x0046 }
            boolean r1 = r3.f2529     // Catch:{ all -> 0x009b }
            if (r1 == 0) goto L_0x009e
            monitor-exit(r5)     // Catch:{ all -> 0x009b }
            goto L_0x0003
        L_0x009b:
            r1 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x009b }
            throw r1     // Catch:{ a -> 0x0046 }
        L_0x009e:
            java.util.List<android.os.HandlerThread> r1 = r3.f2531     // Catch:{ all -> 0x009b }
            int r1 = r1.size()     // Catch:{ all -> 0x009b }
            if (r1 <= 0) goto L_0x00b7
            java.util.List<android.os.HandlerThread> r1 = r3.f2531     // Catch:{ all -> 0x009b }
            r6 = 0
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x009b }
            android.os.HandlerThread r1 = (android.os.HandlerThread) r1     // Catch:{ all -> 0x009b }
        L_0x00af:
            if (r1 != 0) goto L_0x00bc
            com.adincube.sdk.c.d.a r1 = new com.adincube.sdk.c.d.a     // Catch:{ all -> 0x009b }
            r1.<init>()     // Catch:{ all -> 0x009b }
            throw r1     // Catch:{ all -> 0x009b }
        L_0x00b7:
            android.os.HandlerThread r1 = r3.m3148()     // Catch:{ all -> 0x009b }
            goto L_0x00af
        L_0x00bc:
            java.util.Map<android.os.HandlerThread, android.os.Handler> r6 = r3.f2530     // Catch:{ all -> 0x009b }
            java.lang.Object r1 = r6.get(r1)     // Catch:{ all -> 0x009b }
            android.os.Handler r1 = (android.os.Handler) r1     // Catch:{ all -> 0x009b }
            r3.m3149(r4, r1)     // Catch:{ all -> 0x009b }
            r6 = 1000(0x3e8, double:4.94E-321)
            r1.postDelayed(r4, r6)     // Catch:{ all -> 0x009b }
            monitor-exit(r5)     // Catch:{ all -> 0x009b }
            goto L_0x0003
        L_0x00cf:
            java.util.Queue<com.adincube.sdk.g.d.b> r1 = r8.f2494     // Catch:{ a -> 0x0046 }
            r1.add(r0)     // Catch:{ a -> 0x0046 }
            goto L_0x0003
        L_0x00d6:
            monitor-exit(r2)     // Catch:{ all -> 0x004d }
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.f.d.c.m3104():void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3105(e eVar) {
        m3100(eVar);
        com.adincube.sdk.g.d.b bVar = eVar.f2498;
        b.C0013b bVar2 = bVar.f2696;
        if (bVar2 != null) {
            bVar2.m3243(bVar);
        }
        m3104();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3106(e eVar, int i, boolean z, boolean z2) {
        m3100(eVar);
        com.adincube.sdk.g.d.b bVar = eVar.f2498;
        b.C0013b bVar2 = bVar.f2696;
        if (!z2 || i >= bVar.f2687) {
            bVar2.m3241(bVar);
        } else {
            bVar.f2690 = (long) q.m3708(i, z);
            bVar.f2689 = System.currentTimeMillis();
            Object[] objArr = {bVar.m3236(), Long.valueOf(bVar.m3237()), bVar.f2693};
            synchronized (this.f2494) {
                this.f2494.add(bVar);
            }
        }
        m3104();
    }
}
