package com.adincube.sdk.f.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import java.util.concurrent.LinkedBlockingQueue;

public final class d implements a {

    private static final class a implements ServiceConnection {

        /* renamed from: 靐  reason: contains not printable characters */
        final LinkedBlockingQueue<IBinder> f2140;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f2141;

        private a() {
            this.f2141 = false;
            this.f2140 = new LinkedBlockingQueue<>(1);
        }

        /* synthetic */ a(byte b) {
            this();
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.f2140.put(iBinder);
            } catch (InterruptedException e) {
            }
        }

        public final void onServiceDisconnected(ComponentName componentName) {
        }
    }

    private static final class b implements IInterface {

        /* renamed from: 龘  reason: contains not printable characters */
        private IBinder f2142;

        public b(IBinder iBinder) {
            this.f2142 = iBinder;
        }

        public final IBinder asBinder() {
            return this.f2142;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final boolean m2621() {
            boolean z = true;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(1);
                this.f2142.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z = false;
                }
                return z;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m2622() {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.f2142.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readString();
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final com.adincube.sdk.g.a m2620(Context context) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("Cannot be called from the main thread");
        }
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            a aVar = new a((byte) 0);
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            if (context.bindService(intent, aVar, 1)) {
                try {
                    if (aVar.f2141) {
                        throw new IllegalStateException();
                    }
                    aVar.f2141 = true;
                    b bVar = new b(aVar.f2140.take());
                    com.adincube.sdk.g.a aVar2 = new com.adincube.sdk.g.a(bVar.m2622(), bVar.m2621());
                    context.unbindService(aVar);
                    return aVar2;
                } catch (Exception e) {
                    throw new RuntimeException(e);
                } catch (Throwable th) {
                    context.unbindService(aVar);
                    throw th;
                }
            } else {
                throw new RuntimeException("Google Play connection failed");
            }
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }
}
