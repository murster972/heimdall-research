package com.adincube.sdk.f.a;

import android.content.Context;
import android.os.Build;
import com.adincube.sdk.g.a;
import com.adincube.sdk.util.o;
import java.util.concurrent.Semaphore;

public final class b {

    /* renamed from: 齉  reason: contains not printable characters */
    private static b f2132;

    /* renamed from: 靐  reason: contains not printable characters */
    public Semaphore f2133 = new Semaphore(0);

    /* renamed from: 龘  reason: contains not printable characters */
    public a f2134 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized b m2615() {
        b bVar;
        synchronized (b.class) {
            if (f2132 == null) {
                f2132 = new b();
            }
            bVar = f2132;
        }
        return bVar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized a m2616() {
        return this.f2134;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m2617(Context context) {
        boolean z;
        final Context applicationContext = context.getApplicationContext();
        if (this.f2134 == null) {
            a r0 = com.adincube.sdk.util.e.a.m3581(applicationContext);
            if (!r0.m3171()) {
                z = false;
            } else {
                m2618(applicationContext, r0);
                z = true;
            }
            if (!z) {
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            o.m3701("AdvertisingInfoManager.notifyAdvertiserInfoFetched", (Runnable) new Runnable(applicationContext, ("amazon".equalsIgnoreCase(Build.MANUFACTURER) ? new c() : new d()).m2614(applicationContext)) {

                                /* renamed from: 靐  reason: contains not printable characters */
                                final /* synthetic */ a f2137;

                                /* renamed from: 龘  reason: contains not printable characters */
                                final /* synthetic */ Context f2139;

                                {
                                    this.f2139 = r2;
                                    this.f2137 = r3;
                                }

                                public final void run() {
                                    b.this.m2618(this.f2139, this.f2137);
                                }
                            });
                        } catch (Throwable th) {
                            com.adincube.sdk.util.a.m3513("AdvertisingInfoManager.extractAdvertisingInfoFromEnv", th);
                        }
                    }
                }).start();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m2618(Context context, a aVar) {
        Object[] objArr = {aVar.f2549, Boolean.valueOf(aVar.f2547)};
        this.f2134 = aVar;
        if (aVar.m3171()) {
            com.adincube.sdk.util.e.a.m3584(context, aVar);
        }
        this.f2133.release();
    }
}
