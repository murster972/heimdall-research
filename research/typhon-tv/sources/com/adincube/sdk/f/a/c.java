package com.adincube.sdk.f.a;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings;
import com.adincube.sdk.g.a;

public final class c implements a {
    /* renamed from: 龘  reason: contains not printable characters */
    public final a m2619(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        return new a(Settings.Secure.getString(contentResolver, "advertising_id"), Settings.Secure.getInt(contentResolver, "limit_ad_tracking") != 0);
    }
}
