package com.adincube.sdk;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.adincube.sdk.AdinCube;
import com.adincube.sdk.f.b.d.b.b;
import com.adincube.sdk.k.a;
import com.adincube.sdk.k.a.c;
import com.adincube.sdk.util.ErrorReportingHelper;

public class NativeAdMediaView extends a {

    /* renamed from: 靐  reason: contains not printable characters */
    private com.adincube.sdk.f.a f1948 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private com.adincube.sdk.f.b.d.b.a f1949 = new com.adincube.sdk.f.b.d.b.a();

    /* renamed from: 齉  reason: contains not printable characters */
    private b f1950 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private com.adincube.sdk.mediation.r.b f1951 = null;

    public NativeAdMediaView(Context context) {
        super(context);
        m2367();
    }

    public NativeAdMediaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m2367();
    }

    public NativeAdMediaView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m2367();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m2367() {
        try {
            this.f1948 = com.adincube.sdk.f.a.m2607();
            this.f1950 = new b();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdMediaView.init", th);
            ErrorReportingHelper.m3511("NativeAdMediaView.init", th);
        }
    }

    public void setAdjustViewBounds(boolean z) {
        this.f1949.f2384 = Boolean.valueOf(z);
    }

    public void setAutoPlay(boolean z) {
        this.f1949.f2381 = Boolean.valueOf(z);
    }

    public void setMaxHeight(int i) {
        this.f1949.f2385 = Integer.valueOf(i);
    }

    public void setMaxWidth(int i) {
        this.f1949.f2382 = Integer.valueOf(i);
    }

    public void setNativeAd(NativeAd nativeAd) {
        View view;
        try {
            removeAllViews();
            this.f1951 = null;
            if (nativeAd != null && (nativeAd instanceof com.adincube.sdk.mediation.r.b) && this.f1951 != nativeAd) {
                com.adincube.sdk.mediation.r.b bVar = (com.adincube.sdk.mediation.r.b) nativeAd;
                this.f1951 = bVar;
                com.adincube.sdk.g.b.b r2 = this.f1948.m2610(true, true);
                com.adincube.sdk.f.b.d.b.a aVar = new com.adincube.sdk.f.b.d.b.a();
                aVar.f2384 = true;
                aVar.f2383 = ImageView.ScaleType.FIT_CENTER;
                aVar.f2381 = false;
                aVar.f2377 = Double.valueOf(0.5d);
                aVar.f2378 = false;
                aVar.f2379 = 200;
                aVar.f2380 = true;
                if (!(r2 == null || r2.f2598 == null)) {
                    aVar.m2974(r2.f2598);
                }
                aVar.m2974(this.f1949);
                Context context = getContext();
                FrameLayout.LayoutParams r22 = m3402();
                if (bVar.ʾ) {
                    view = bVar.齉.龘(context, bVar, aVar, r22);
                } else {
                    if (Build.VERSION.SDK_INT >= 14 && bVar.ᐧ() != null) {
                        c cVar = new c(context, aVar);
                        com.adincube.sdk.mediation.r.b bVar2 = cVar.f2912;
                        view = cVar;
                        if (bVar != bVar2) {
                            if (cVar.f2909 != null) {
                                cVar.f2909.f2688 = null;
                                cVar.f2910.m3092(cVar.f2909);
                            }
                            cVar.f2909 = null;
                            cVar.f2911.m2471();
                            if (bVar != null) {
                                com.adincube.sdk.g.d.b r3 = cVar.f2908.m3010(bVar.ᐧ());
                                r3.f2688 = cVar;
                                if (cVar.f2910.m3091(r3) || !cVar.f2898.f2380.booleanValue()) {
                                    r3 = cVar.f2910.m3094(r3);
                                } else {
                                    cVar.m3411(r3);
                                }
                                cVar.f2909 = r3;
                            }
                            cVar.f2912 = bVar;
                            view = cVar;
                        }
                    } else {
                        com.adincube.sdk.k.a.b bVar3 = new com.adincube.sdk.k.a.b(context, aVar);
                        if (bVar != bVar3.f2897) {
                            bVar3.f2897 = null;
                            bVar3.setImageDrawable((Drawable) null);
                            if (bVar != null && (bVar instanceof com.adincube.sdk.mediation.r.b)) {
                                bVar3.f2897 = bVar;
                                AdinCube.Native.m2312((ImageView) bVar3, bVar.m2358());
                            }
                        }
                        view = bVar3;
                    }
                }
                addView(view);
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdMediaView.setNativeAd", th);
            ErrorReportingHelper.m3506("NativeAdMediaView.setNativeAd", com.adincube.sdk.g.c.b.NATIVE, th);
        }
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        this.f1949.f2383 = scaleType;
    }

    public void setStartsMuted(boolean z) {
        this.f1949.f2378 = Boolean.valueOf(z);
    }
}
