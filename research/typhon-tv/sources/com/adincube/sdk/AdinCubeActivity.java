package com.adincube.sdk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import com.adincube.sdk.a.a;
import com.adincube.sdk.f.e.a;
import com.adincube.sdk.f.e.b;
import com.adincube.sdk.g.a.c;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.g.d;
import com.adincube.sdk.i.g;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.f.b;
import com.adincube.sdk.util.t;
import org.json.JSONObject;

public class AdinCubeActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static EventListener f1913 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public b f1914 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private a f1915 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f1916 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    private a.C0001a f1917 = new a.C0001a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2325(Throwable th) {
            if (AdinCubeActivity.f1913 != null) {
                AdinCubeActivity.f1913.m2328();
            }
            ErrorReportingHelper.m3503("AdinCubeActivity.onError", (e) AdinCubeActivity.this.f1923, th);
            AdinCubeActivity.this.finish();
        }
    };

    /* renamed from: ˈ  reason: contains not printable characters */
    private a.b f1918 = new a.b() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2324() {
            if (AdinCubeActivity.f1913 != null) {
                AdinCubeActivity.f1913.m2327();
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean f1919 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private a.c f1920 = new a.c() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2322(c cVar, Boolean bool) {
            if (!AdinCubeActivity.this.f1919) {
                boolean unused = AdinCubeActivity.this.f1919 = true;
                b r0 = AdinCubeActivity.this.f1914;
                g gVar = new g();
                gVar.f2765 = r0.f2509;
                gVar.f2834 = "RTB";
                gVar.f2835 = cVar;
                gVar.f2831 = bool;
                gVar.m3296();
            }
        }
    };

    /* renamed from: ᐧ  reason: contains not printable characters */
    private b.a f1921 = new b.a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2323(e eVar) {
            if (AdinCubeActivity.f1913 != null) {
                AdinCubeActivity.f1913.m2326();
            }
            com.adincube.sdk.f.e.b r0 = AdinCubeActivity.this.f1914;
            com.adincube.sdk.i.c cVar = new com.adincube.sdk.i.c();
            cVar.f2765 = r0.f2509;
            cVar.f2820 = "RTB";
            cVar.f2821 = eVar;
            cVar.m3296();
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private com.adincube.sdk.f.a f1922 = null;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public c f1923;

    /* renamed from: 麤  reason: contains not printable characters */
    private com.adincube.sdk.a.a f1924;

    /* renamed from: 齉  reason: contains not printable characters */
    private com.adincube.sdk.g.c.b f1925;

    public interface EventListener {
        /* renamed from: 靐  reason: contains not printable characters */
        void m2326();

        /* renamed from: 齉  reason: contains not printable characters */
        void m2327();

        /* renamed from: 龘  reason: contains not printable characters */
        void m2328();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m2319(EventListener eventListener) {
        f1913 = eventListener;
    }

    public void finish() {
        super.finish();
        f1913 = null;
    }

    public void onBackPressed() {
        try {
            if (!this.f1916) {
                this.f1916 = this.f1924.m2379();
            }
        } catch (Throwable th) {
            ErrorReportingHelper.m3503("AdinCubeActivity.onBackPressed", (e) this.f1923, th);
            com.adincube.sdk.util.a.m3513("AdinCubeActivity.onBackPressed() Exception : ", th);
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onCreate(Bundle bundle) {
        boolean z;
        int i;
        try {
            super.onCreate(bundle);
            f.m3606((Context) this);
            Bundle extras = getIntent().getExtras();
            if (extras == null || !extras.containsKey("a") || !extras.containsKey("at")) {
                com.adincube.sdk.util.a.m3513("invalid AdinCubeActivity intent.", new Object[0]);
                if (f1913 != null) {
                    f1913.m2328();
                }
                z = false;
            } else {
                this.f1923 = new c(new JSONObject(extras.getString("a")));
                this.f1925 = com.adincube.sdk.g.c.b.a(extras.getString("at"));
                z = true;
            }
            if (!z) {
                finish();
                return;
            }
            t r0 = t.m3714();
            this.f1922 = com.adincube.sdk.f.a.m2607();
            this.f1914 = new com.adincube.sdk.f.e.b(this.f1925);
            this.f1915 = new com.adincube.sdk.f.e.a(this.f1922, this.f1914, r0);
            com.adincube.sdk.util.b bVar = new com.adincube.sdk.util.b(this);
            c cVar = this.f1923;
            if (!(cVar.f2564 == null || cVar.f2564 == d.BOTH)) {
                switch (cVar.f2564) {
                    case LAND:
                        i = 0;
                        break;
                    case PORT:
                        i = 1;
                        break;
                    default:
                        i = getRequestedOrientation();
                        break;
                }
                setRequestedOrientation(i);
            }
            bVar.m3523(getIntent().getExtras());
            getWindow().getDecorView().setBackgroundColor(getResources().getColor(17170446));
            if (bundle != null) {
                this.f1919 = bundle.getBoolean("hasImpressionBeenSent");
            }
            if (this.f1922.m2610(true, true) == null) {
                com.adincube.sdk.f.b.b.b.m2777().m2779();
                finish();
                return;
            }
            com.adincube.sdk.mediation.v.f fVar = com.adincube.sdk.f.a.m2607().m2610(true, true).f2608;
            this.f1924 = this.f1923.m3208().a();
            this.f1924.m2387(m2321());
            this.f1924.m2385(this.f1915);
            this.f1924.f1957 = this.f1920;
            this.f1924.m2386(this.f1921);
            this.f1924.m2384(this.f1918);
            this.f1924.m2383(this.f1917);
            Window window = getWindow();
            com.adincube.sdk.a.a aVar = this.f1924;
            if (Build.VERSION.SDK_INT >= 19) {
                if ((window.getAttributes().flags & 67108864) != 0) {
                    int r02 = com.adincube.sdk.util.b.m3520((Context) this);
                    aVar.f1954 = r02;
                    if (aVar.f1953 != null) {
                        aVar.f1953.m2603(r02);
                    }
                }
            }
            View r6 = this.f1924.m2380(this, bundle, this.f1923, fVar);
            Bundle extras2 = getIntent().getExtras();
            if (Build.VERSION.SDK_INT >= 19 && extras2.containsKey("suf")) {
                int i2 = extras2.getInt("suf");
                boolean z2 = (i2 & 512) != 0;
                boolean z3 = (i2 & 1024) != 0;
                boolean z4 = (i2 & 2) != 0;
                boolean z5 = (i2 & 4) != 0;
                if ((z3 && !z5) || (z2 && !z4)) {
                    r6.setFitsSystemWindows(true);
                }
            }
        } catch (Throwable th) {
            ErrorReportingHelper.m3503("AdinCubeActivity.onCreate", (e) this.f1923, th);
            com.adincube.sdk.util.a.m3513("AdinCubeActivity.onCreate() Exception : ", th);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.f1924 != null) {
                this.f1924.m2372();
            }
        } catch (Throwable th) {
            ErrorReportingHelper.m3503("AdinCubeActivity.onDestroy", (e) this.f1923, th);
            com.adincube.sdk.util.a.m3513("AdinCubeActivity.onDestroy() Exception : ", th);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        try {
            bundle.putBoolean("hasImpressionBeenSent", this.f1919);
            if (this.f1924 != null) {
                this.f1924.m2382(bundle);
            }
        } catch (Throwable th) {
            ErrorReportingHelper.m3503("AdinCubeActivity.onSaveInstanceState", (e) this.f1923, th);
            com.adincube.sdk.util.a.m3513("AdinCubeActivity.onSaveInstanceState() Exception : ", th);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        try {
            super.onStart();
            new com.adincube.sdk.util.b(this).m3521(getIntent().getExtras());
            this.f1924.m2381();
        } catch (Throwable th) {
            ErrorReportingHelper.m3503("AdinCubeActivity.onStart", (e) this.f1923, th);
            com.adincube.sdk.util.a.m3513("AdinCubeActivity.onStart() Exception : ", th);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        try {
            if (this.f1924 != null) {
                this.f1924.m2370();
            }
        } catch (Throwable th) {
            ErrorReportingHelper.m3503("AdinCubeActivity.onStop", (e) this.f1923, th);
            com.adincube.sdk.util.a.m3513("AdinCubeActivity.onStop() Exception : ", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m2321() {
        try {
            if (!((getWindow() == null || (getWindow().getAttributes().flags & 16777216) == 0) && (getPackageManager().getActivityInfo(getComponentName(), 0).flags & 512) == 0)) {
                return true;
            }
        } catch (Throwable th) {
        }
        return false;
    }
}
