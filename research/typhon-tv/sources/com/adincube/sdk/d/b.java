package com.adincube.sdk.d;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Looper;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.adincube.sdk.AdinCubeNativeEventListener;
import com.adincube.sdk.NativeAd;
import com.adincube.sdk.c.a.c;
import com.adincube.sdk.c.a.p;
import com.adincube.sdk.f.b.c.m;
import com.adincube.sdk.f.b.d.b;
import com.adincube.sdk.f.b.d.e;
import com.adincube.sdk.f.b.d.f;
import com.adincube.sdk.f.b.g;
import com.adincube.sdk.f.b.h;
import com.adincube.sdk.f.b.k;
import com.adincube.sdk.f.d.a;
import com.adincube.sdk.g.d.b;
import com.adincube.sdk.mediation.r.b;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.d;
import com.adincube.sdk.util.o;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public class b implements b.a, a.b {

    /* renamed from: 龘  reason: contains not printable characters */
    private static b f2093 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Map<com.adincube.sdk.g.d.b, Set<e>> f2094;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final Map<com.adincube.sdk.g.d.b, List<WeakReference<ImageView>>> f2095;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Map<ImageView, com.adincube.sdk.g.d.b> f2096;

    /* renamed from: ˑ  reason: contains not printable characters */
    private b.a f2097;

    /* renamed from: 连任  reason: contains not printable characters */
    private Map<AdinCubeNativeEventListener, e> f2098;

    /* renamed from: 靐  reason: contains not printable characters */
    private com.adincube.sdk.f.a f2099;

    /* renamed from: 麤  reason: contains not printable characters */
    private f f2100;

    /* renamed from: 齉  reason: contains not printable characters */
    private com.adincube.sdk.f.a.b f2101;

    public b() {
        this.f2099 = null;
        this.f2101 = null;
        this.f2100 = null;
        this.f2098 = new HashMap();
        this.f2094 = new HashMap();
        this.f2095 = new HashMap();
        this.f2096 = new WeakHashMap();
        this.f2097 = new b.a() {
            /* renamed from: 靐  reason: contains not printable characters */
            private static boolean m2591(com.adincube.sdk.g.d.b bVar, com.adincube.sdk.mediation.r.b bVar2) {
                return bVar2.麤() != null && bVar.f2693.equals(bVar2.麤().m2366());
            }

            /* renamed from: 麤  reason: contains not printable characters */
            private static boolean m2592(com.adincube.sdk.g.d.b bVar, com.adincube.sdk.mediation.r.b bVar2) {
                return bVar2.ʿ != null && bVar.f2693.equals(bVar2.ʿ.龘);
            }

            /* renamed from: 齉  reason: contains not printable characters */
            private static boolean m2593(com.adincube.sdk.g.d.b bVar, com.adincube.sdk.mediation.r.b bVar2) {
                return bVar2.ᐧ() != null && bVar.f2693.equals(bVar2.ᐧ().龘);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            private static List<com.adincube.sdk.mediation.r.b> m2594(e eVar, com.adincube.sdk.g.d.b bVar) {
                ArrayList arrayList = new ArrayList();
                try {
                    for (com.adincube.sdk.mediation.r.b next : eVar.m3005(false)) {
                        if (m2591(bVar, next) || m2597(bVar, next) || m2593(bVar, next) || m2592(bVar, next)) {
                            arrayList.add(next);
                        }
                    }
                } catch (c e) {
                }
                return arrayList;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            private static void m2595(com.adincube.sdk.g.d.b bVar, b.c cVar) {
                if (cVar != null && (bVar instanceof com.adincube.sdk.g.d.a) && cVar.龘().equals(bVar.f2693)) {
                    com.adincube.sdk.g.d.a aVar = (com.adincube.sdk.g.d.a) bVar;
                    cVar.靐 = aVar.f2679;
                    cVar.齉 = aVar.f2678;
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            private void m2596(Set<e> set) {
                boolean z;
                synchronized (b.this.f2094) {
                    for (e next : set) {
                        Iterator it2 = b.this.f2094.values().iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                if (((Set) it2.next()).contains(next)) {
                                    z = false;
                                    break;
                                }
                            } else {
                                z = true;
                                break;
                            }
                        }
                        if (z) {
                            b.this.m2579(next);
                        }
                    }
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            private static boolean m2597(com.adincube.sdk.g.d.b bVar, com.adincube.sdk.mediation.r.b bVar2) {
                return bVar2.连任() != null && bVar.f2693.equals(bVar2.连任().m2366());
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public final void m2598(com.adincube.sdk.g.d.b bVar) {
                try {
                    synchronized (b.this.f2095) {
                        b.this.f2095.remove(bVar);
                    }
                    synchronized (b.this.f2094) {
                        Set<e> set = (Set) b.this.f2094.remove(bVar);
                        if (set != null) {
                            for (e r1 : set) {
                                for (com.adincube.sdk.mediation.r.b next : m2594(r1, bVar)) {
                                    if (m2597(bVar, next)) {
                                        com.adincube.sdk.util.a.m3512("Failed to download cover for '%s' from '%s'. Removing cover from ad.", next.龘(), next.ʼ());
                                        next.ᐧ = null;
                                    }
                                    if (m2591(bVar, next)) {
                                        com.adincube.sdk.util.a.m3512("Failed to download icon for '%s' from '%s'. Replacing by a transparent pixel.", next.龘(), next.ʼ());
                                        next.麤().麤 = true;
                                    }
                                    if (m2593(bVar, next)) {
                                        com.adincube.sdk.util.a.m3512("Failed to download video cover for '%s' from '%s'. Removing cover from ad.", next.龘(), next.ʼ());
                                        next.ˈ = null;
                                    }
                                    if (m2592(bVar, next)) {
                                        com.adincube.sdk.util.a.m3512("Failed to download ad choices icon for '%s' from '%s'. Will not display it for ad.", next.龘(), next.ʼ());
                                        next.ʿ = null;
                                    }
                                }
                            }
                            m2596(set);
                        }
                    }
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("AdinCubeNative#OnResourceCacheListener.onResourceCached", th);
                    ErrorReportingHelper.m3506("AdinCubeNative#OnResourceCacheListener.onResourceCached", com.adincube.sdk.g.c.b.NATIVE, th);
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final void m2599(com.adincube.sdk.g.d.b bVar) {
                try {
                    synchronized (b.this.f2094) {
                        Set<e> set = (Set) b.this.f2094.remove(bVar);
                        if (set != null) {
                            for (e r1 : set) {
                                for (com.adincube.sdk.mediation.r.b next : r1.m3005(false)) {
                                    m2595(bVar, next.麤());
                                    m2595(bVar, next.连任());
                                }
                            }
                            m2596(set);
                        }
                    }
                    synchronized (b.this.f2095) {
                        List list = (List) b.this.f2095.get(bVar);
                        if (list != null) {
                            Iterator it2 = list.iterator();
                            while (true) {
                                if (!it2.hasNext()) {
                                    b.this.f2095.remove(bVar);
                                    break;
                                }
                                ImageView imageView = (ImageView) ((WeakReference) it2.next()).get();
                                if (imageView == null) {
                                    b.this.f2095.remove(bVar);
                                    break;
                                }
                                b.m2577(b.this, imageView, bVar);
                            }
                        }
                    }
                    b.this.m2569();
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("AdinCubeNative#OnResourceCacheListener.onResourceCached", th);
                    ErrorReportingHelper.m3506("AdinCubeNative#OnResourceCacheListener.onResourceCached", com.adincube.sdk.g.c.b.NATIVE, th);
                }
            }
        };
        this.f2099 = com.adincube.sdk.f.a.m2607();
        this.f2101 = com.adincube.sdk.f.a.b.m2615();
        this.f2100 = f.m3008();
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m2569() {
        com.adincube.sdk.f.d.b r3 = com.adincube.sdk.f.d.b.m3088();
        synchronized (this.f2095) {
            Iterator<Map.Entry<com.adincube.sdk.g.d.b, List<WeakReference<ImageView>>>> it2 = this.f2095.entrySet().iterator();
            while (it2.hasNext()) {
                Map.Entry next = it2.next();
                List list = (List) next.getValue();
                Iterator it3 = list.iterator();
                while (it3.hasNext()) {
                    if (((WeakReference) it3.next()).get() == null) {
                        it3.remove();
                    }
                }
                if (list.isEmpty()) {
                    r3.m3092((com.adincube.sdk.g.d.b) next.getKey());
                    it2.remove();
                }
            }
        }
        synchronized (this.f2094) {
            Iterator<Map.Entry<com.adincube.sdk.g.d.b, Set<e>>> it4 = this.f2094.entrySet().iterator();
            while (it4.hasNext()) {
                Map.Entry next2 = it4.next();
                Set set = (Set) next2.getValue();
                Iterator it5 = set.iterator();
                while (it5.hasNext()) {
                    if (!((e) it5.next()).f2156.m2953()) {
                        it5.remove();
                    }
                }
                if (set.isEmpty()) {
                    r3.m3092((com.adincube.sdk.g.d.b) next2.getKey());
                    it4.remove();
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m2570(AdinCubeNativeEventListener adinCubeNativeEventListener) {
        e remove;
        if (adinCubeNativeEventListener != null) {
            try {
                synchronized (this.f2098) {
                    remove = this.f2098.remove(adinCubeNativeEventListener);
                }
                if (remove != null) {
                    ((com.adincube.sdk.f.b.d.b) remove.f2156).f2364 = null;
                    com.adincube.sdk.f.d.b r3 = com.adincube.sdk.f.d.b.m3088();
                    synchronized (this.f2094) {
                        Iterator<Map.Entry<com.adincube.sdk.g.d.b, Set<e>>> it2 = this.f2094.entrySet().iterator();
                        while (it2.hasNext()) {
                            Map.Entry next = it2.next();
                            if (next.getValue() != null && ((Set) next.getValue()).remove(remove) && ((Set) next.getValue()).isEmpty()) {
                                r3.m3092((com.adincube.sdk.g.d.b) next.getKey());
                                it2.remove();
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("AdinCubeNative.removeEventListenerAndCancelAllResources", th);
                ErrorReportingHelper.m3506("AdinCubeNative.removeEventListenerAndCancelAllResources", com.adincube.sdk.g.c.b.NATIVE, th);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m2573() {
        if (f2093 == null) {
            synchronized (b.class) {
                if (f2093 == null) {
                    f2093 = new b();
                }
            }
        }
        return f2093;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized e m2574(Context context, int i, AdinCubeNativeEventListener adinCubeNativeEventListener) {
        e eVar;
        e eVar2 = this.f2098.get(adinCubeNativeEventListener);
        if (eVar2 == null) {
            com.adincube.sdk.util.e.b r11 = com.adincube.sdk.util.e.b.m3592();
            h r7 = h.m3040();
            com.adincube.sdk.f.b.f r17 = com.adincube.sdk.f.b.f.m3030(com.adincube.sdk.g.c.b.NATIVE);
            com.adincube.sdk.f.b.e.b.c cVar = new com.adincube.sdk.f.b.e.b.c();
            com.adincube.sdk.f.b.e eVar3 = new com.adincube.sdk.f.b.e(com.adincube.sdk.g.c.b.NATIVE, this.f2099);
            m r16 = m.m2940();
            com.adincube.sdk.f.b.d.b bVar = new com.adincube.sdk.f.b.d.b(adinCubeNativeEventListener, this);
            g r6 = g.m3035();
            Context context2 = context;
            eVar = new e(context2, this.f2099, r11, bVar, r7, cVar, new com.adincube.sdk.f.b.c(com.adincube.sdk.g.c.b.NATIVE, this.f2099, eVar3, r6, r7), r16, r17, r6, k.m3056(), i);
            this.f2098.put(adinCubeNativeEventListener, eVar);
        } else {
            eVar = eVar2;
        }
        return eVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m2576(c cVar, com.adincube.sdk.f.b.d.b bVar) {
        try {
            cVar.a();
            if (bVar != null) {
                new StringBuilder("onLoadError - ").append(cVar.a);
                o.m3699(bVar.f2364, new com.adincube.sdk.util.c.a<AdinCubeNativeEventListener>(cVar) {

                    /* renamed from: 龘  reason: contains not printable characters */
                    final /* synthetic */ c f2372;

                    {
                        this.f2372 = r2;
                    }

                    /* renamed from: 龘  reason: contains not printable characters */
                    public final /* synthetic */ void m2969(Object obj) {
                        ((AdinCubeNativeEventListener) obj).onLoadError(this.f2372.a);
                    }
                });
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AdinCubeNative.printSdkError", th);
            ErrorReportingHelper.m3511("AdinCubeNative.printSdkError", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m2577(b bVar, ImageView imageView, com.adincube.sdk.g.d.b bVar2) {
        synchronized (bVar.f2096) {
            a r3 = a.m3064();
            com.adincube.sdk.g.d.b remove = bVar.f2096.remove(imageView);
            if (remove != null && !bVar.f2096.containsValue(bVar2)) {
                synchronized (r3.f2452) {
                    Set set = r3.f2452.get(remove);
                    if (set != null) {
                        set.remove(bVar);
                        if (set.isEmpty()) {
                            r3.f2452.remove(remove);
                        }
                    }
                }
            }
            synchronized (r3.f2452) {
                Set set2 = r3.f2452.get(bVar2);
                if (set2 == null) {
                    set2 = new HashSet();
                    r3.f2452.put(bVar2, set2);
                    r3.f2450.submit(new a.C0006a(r3, bVar2, (byte) 0));
                }
                set2.add(bVar);
            }
            bVar.f2096.put(imageView, bVar2);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2579(e eVar) {
        c e;
        e remove;
        com.adincube.sdk.f.b.d.b bVar = (com.adincube.sdk.f.b.d.b) eVar.f2156;
        try {
            synchronized (this.f2098) {
                remove = this.f2098.remove(bVar.f2364);
            }
            List<com.adincube.sdk.mediation.r.b> r1 = remove.m3005(true);
            if (r1.size() > 0) {
                o.m3699(bVar.f2364, new com.adincube.sdk.util.c.a<AdinCubeNativeEventListener>(new ArrayList(r1)) {

                    /* renamed from: 龘  reason: contains not printable characters */
                    final /* synthetic */ List f2368;

                    {
                        this.f2368 = r2;
                    }

                    /* renamed from: 龘  reason: contains not printable characters */
                    public final /* synthetic */ void m2967(Object obj) {
                        ((AdinCubeNativeEventListener) obj).onAdLoaded(this.f2368);
                    }
                });
            }
            e = null;
        } catch (c e2) {
            e = e2;
        } catch (Throwable th) {
            p pVar = new p(th);
            ErrorReportingHelper.m3506("AdinCubeNative.notifyNativeAdLoaded", com.adincube.sdk.g.c.b.NATIVE, th);
            e = pVar;
        }
        if (e != null) {
            m2576(e, bVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m2580(NativeAd.Image image, e eVar) {
        com.adincube.sdk.g.d.a r1 = this.f2100.m3009(image);
        r1.f2688 = this.f2097;
        if (m2581((com.adincube.sdk.g.d.b) r1, eVar)) {
            return true;
        }
        b.c cVar = (b.c) image;
        if (image.m2363() == null || image.m2365() == null) {
            cVar.靐 = r1.f2679;
            cVar.齉 = r1.f2678;
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m2581(com.adincube.sdk.g.d.b bVar, e eVar) {
        com.adincube.sdk.f.d.b r0 = com.adincube.sdk.f.d.b.m3088();
        if (r0.m3091(bVar)) {
            return false;
        }
        synchronized (this.f2094) {
            com.adincube.sdk.g.d.b r2 = r0.m3094(bVar);
            Set set = this.f2094.get(r2);
            if (set == null) {
                set = new HashSet();
                this.f2094.put(r2, set);
            }
            set.add(eVar);
        }
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2582(final NativeAd nativeAd) {
        if (nativeAd != null && (nativeAd instanceof com.adincube.sdk.mediation.r.b)) {
            try {
                com.adincube.sdk.mediation.r.b bVar = (com.adincube.sdk.mediation.r.b) nativeAd;
                if (bVar.ﹶ) {
                    return;
                }
                if (!Looper.getMainLooper().equals(Looper.myLooper())) {
                    o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) new Runnable() {
                        public final void run() {
                            b.this.m2588(nativeAd);
                        }
                    });
                    return;
                }
                bVar.齉.靐(nativeAd);
                Iterator it2 = bVar.齉.ʽ().iterator();
                boolean z = true;
                while (z && it2.hasNext()) {
                    com.adincube.sdk.mediation.r.b bVar2 = (com.adincube.sdk.mediation.r.b) it2.next();
                    z = bVar2 != bVar ? bVar2.ﹶ & z : z;
                }
                if (z) {
                    new Object[1][0] = bVar.齉.ʼ().m3473();
                    bVar.齉.ʻ();
                }
                bVar.ʽ();
                m2569();
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("AdinCube.Native.internalDestroy", th);
                ErrorReportingHelper.m3506("AdinCube.Native.internalDestroy", com.adincube.sdk.g.c.b.NATIVE, th);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2583(Context context, int i, boolean z, AdinCubeNativeEventListener adinCubeNativeEventListener) {
        c e = null;
        try {
            com.adincube.sdk.util.f.m3606(context);
            if (!Looper.getMainLooper().equals(Looper.myLooper())) {
                final Context context2 = context;
                final int i2 = i;
                final boolean z2 = z;
                final AdinCubeNativeEventListener adinCubeNativeEventListener2 = adinCubeNativeEventListener;
                o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) new Runnable() {
                    public final void run() {
                        b.this.m2583(context2, i2, z2, adinCubeNativeEventListener2);
                    }
                });
                return;
            }
            com.adincube.sdk.util.a.m3515("AdinCube.Native.load()", new Object[0]);
            com.adincube.sdk.util.b.a.m3525();
            if (context == null) {
                throw new com.adincube.sdk.c.a.g("load()");
            } else if (i > 10) {
                throw new com.adincube.sdk.c.a.c.a();
            } else {
                this.f2101.m2617(context);
                com.adincube.sdk.util.e.a.m3583(context);
                com.adincube.sdk.f.a.m2608(context, z);
                if (com.adincube.sdk.util.e.a.m3585()) {
                    com.adincube.sdk.util.a.m3515("Configuration changed.", new Object[0]);
                    com.adincube.sdk.util.e.b.m3592().m3596();
                    com.adincube.sdk.f.a.m2607().m2609();
                    d.m3554();
                    com.adincube.sdk.util.e.a.m3580();
                }
                m2574(context, i, adinCubeNativeEventListener).m2644();
                m2569();
                if (e != null) {
                    m2576(e, new com.adincube.sdk.f.b.d.b(adinCubeNativeEventListener, this));
                    m2570(adinCubeNativeEventListener);
                }
            }
        } catch (c e2) {
            e = e2;
        } catch (Throwable th) {
            p pVar = new p(th);
            ErrorReportingHelper.m3506("AdinCubeNative.load", com.adincube.sdk.g.c.b.NATIVE, th);
            e = pVar;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2584(final ViewGroup viewGroup, final NativeAd nativeAd) {
        if (viewGroup == null) {
            try {
                throw new IllegalStateException("viewGroup must not be null");
            } catch (RuntimeException e) {
                com.adincube.sdk.util.a.m3515("AdinCube.Native.link()", new Object[0]);
                throw e;
            }
        } else if (nativeAd == null) {
            throw new IllegalStateException("nativeAd must not be null");
        } else if (!(nativeAd instanceof com.adincube.sdk.mediation.r.b)) {
            throw new IllegalStateException("invalid nativeAd");
        } else {
            ((com.adincube.sdk.mediation.r.b) nativeAd).ٴ();
            try {
                if (!Looper.getMainLooper().equals(Looper.myLooper())) {
                    o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) new Runnable() {
                        public final void run() {
                            b.this.m2584(viewGroup, nativeAd);
                        }
                    });
                    return;
                }
                com.adincube.sdk.util.a.m3515("AdinCube.Native.link()", new Object[0]);
                ((com.adincube.sdk.mediation.r.b) nativeAd).齉.龘(nativeAd, viewGroup);
                m2569();
            } catch (com.adincube.sdk.c.a.c.b e2) {
                throw e2;
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("AdinCube.Native.link", th);
                ErrorReportingHelper.m3506("AdinCubeNative.link", com.adincube.sdk.g.c.b.NATIVE, th);
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m2585(final android.widget.ImageView r8, final com.adincube.sdk.NativeAd.Image r9) {
        /*
            r7 = this;
            r2 = 1
            r6 = 0
            android.os.Looper r1 = android.os.Looper.getMainLooper()     // Catch:{ Throwable -> 0x0032 }
            android.os.Looper r3 = android.os.Looper.myLooper()     // Catch:{ Throwable -> 0x0032 }
            boolean r1 = r1.equals(r3)     // Catch:{ Throwable -> 0x0032 }
            if (r1 != 0) goto L_0x001c
            com.adincube.sdk.d.b$3 r1 = new com.adincube.sdk.d.b$3     // Catch:{ Throwable -> 0x0032 }
            r1.<init>(r8, r9)     // Catch:{ Throwable -> 0x0032 }
            java.lang.String r3 = "HandlerUtil.dispatchOnUiThread"
            com.adincube.sdk.util.o.m3701((java.lang.String) r3, (java.lang.Runnable) r1)     // Catch:{ Throwable -> 0x0032 }
        L_0x001b:
            return
        L_0x001c:
            java.lang.String r1 = "AdinCube.Native.setImageBitmap()"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0032 }
            com.adincube.sdk.util.a.m3515(r1, r3)     // Catch:{ Throwable -> 0x0032 }
            if (r8 == 0) goto L_0x001b
            if (r9 == 0) goto L_0x002d
            boolean r1 = r9 instanceof com.adincube.sdk.mediation.r.b.c     // Catch:{ Throwable -> 0x0032 }
            if (r1 != 0) goto L_0x0046
        L_0x002d:
            r1 = 0
            r8.setImageBitmap(r1)     // Catch:{ Throwable -> 0x0032 }
            goto L_0x001b
        L_0x0032:
            r1 = move-exception
            java.lang.String r3 = "AdinCube.Native.setImageBitmap"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r6] = r1
            com.adincube.sdk.util.a.m3513(r3, r2)
            java.lang.String r2 = "AdinCube.Native.setImageBitmap"
            com.adincube.sdk.g.c.b r3 = com.adincube.sdk.g.c.b.NATIVE
            com.adincube.sdk.util.ErrorReportingHelper.m3506((java.lang.String) r2, (com.adincube.sdk.g.c.b) r3, (java.lang.Throwable) r1)
            goto L_0x001b
        L_0x0046:
            r0 = r9
            com.adincube.sdk.mediation.r.b$c r0 = (com.adincube.sdk.mediation.r.b.c) r0     // Catch:{ Throwable -> 0x0032 }
            r1 = r0
            boolean r1 = r1.麤     // Catch:{ Throwable -> 0x0032 }
            if (r1 == 0) goto L_0x007e
            java.lang.Integer r1 = r9.m2363()     // Catch:{ Throwable -> 0x0032 }
            if (r1 == 0) goto L_0x00c5
            java.lang.Integer r1 = r9.m2363()     // Catch:{ Throwable -> 0x0032 }
            int r1 = r1.intValue()     // Catch:{ Throwable -> 0x0032 }
            r3 = r1
        L_0x005d:
            java.lang.Integer r1 = r9.m2365()     // Catch:{ Throwable -> 0x0032 }
            if (r1 == 0) goto L_0x00c3
            java.lang.Integer r1 = r9.m2365()     // Catch:{ Throwable -> 0x0032 }
            int r1 = r1.intValue()     // Catch:{ Throwable -> 0x0032 }
        L_0x006b:
            android.graphics.Bitmap$Config r4 = android.graphics.Bitmap.Config.ALPHA_8     // Catch:{ Throwable -> 0x0032 }
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createBitmap(r3, r1, r4)     // Catch:{ Throwable -> 0x0032 }
            android.graphics.Canvas r3 = new android.graphics.Canvas     // Catch:{ Throwable -> 0x0032 }
            r3.<init>(r1)     // Catch:{ Throwable -> 0x0032 }
            r4 = 0
            r3.drawColor(r4)     // Catch:{ Throwable -> 0x0032 }
            r8.setImageBitmap(r1)     // Catch:{ Throwable -> 0x0032 }
            goto L_0x001b
        L_0x007e:
            android.content.Context r1 = r8.getContext()     // Catch:{ Throwable -> 0x0032 }
            com.adincube.sdk.util.f.m3606((android.content.Context) r1)     // Catch:{ Throwable -> 0x0032 }
            r1 = 0
            r8.setImageBitmap(r1)     // Catch:{ Throwable -> 0x0032 }
            java.util.Map<com.adincube.sdk.g.d.b, java.util.List<java.lang.ref.WeakReference<android.widget.ImageView>>> r3 = r7.f2095     // Catch:{ Throwable -> 0x0032 }
            monitor-enter(r3)     // Catch:{ Throwable -> 0x0032 }
            com.adincube.sdk.f.b.d.f r1 = r7.f2100     // Catch:{ all -> 0x00c0 }
            com.adincube.sdk.g.d.a r1 = r1.m3009((com.adincube.sdk.NativeAd.Image) r9)     // Catch:{ all -> 0x00c0 }
            com.adincube.sdk.g.d.b$a r4 = r7.f2097     // Catch:{ all -> 0x00c0 }
            r1.f2688 = r4     // Catch:{ all -> 0x00c0 }
            com.adincube.sdk.f.d.b r4 = com.adincube.sdk.f.d.b.m3088()     // Catch:{ all -> 0x00c0 }
            com.adincube.sdk.g.d.b r4 = r4.m3094((com.adincube.sdk.g.d.b) r1)     // Catch:{ all -> 0x00c0 }
            java.util.Map<com.adincube.sdk.g.d.b, java.util.List<java.lang.ref.WeakReference<android.widget.ImageView>>> r1 = r7.f2095     // Catch:{ all -> 0x00c0 }
            java.lang.Object r1 = r1.get(r4)     // Catch:{ all -> 0x00c0 }
            java.util.List r1 = (java.util.List) r1     // Catch:{ all -> 0x00c0 }
            if (r1 != 0) goto L_0x00b2
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x00c0 }
            r1.<init>()     // Catch:{ all -> 0x00c0 }
            java.util.Map<com.adincube.sdk.g.d.b, java.util.List<java.lang.ref.WeakReference<android.widget.ImageView>>> r5 = r7.f2095     // Catch:{ all -> 0x00c0 }
            r5.put(r4, r1)     // Catch:{ all -> 0x00c0 }
        L_0x00b2:
            java.lang.ref.WeakReference r4 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x00c0 }
            r4.<init>(r8)     // Catch:{ all -> 0x00c0 }
            r1.add(r4)     // Catch:{ all -> 0x00c0 }
            monitor-exit(r3)     // Catch:{ all -> 0x00c0 }
            r7.m2569()     // Catch:{ Throwable -> 0x0032 }
            goto L_0x001b
        L_0x00c0:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00c0 }
            throw r1     // Catch:{ Throwable -> 0x0032 }
        L_0x00c3:
            r1 = r2
            goto L_0x006b
        L_0x00c5:
            r3 = r2
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.d.b.m2585(android.widget.ImageView, com.adincube.sdk.NativeAd$Image):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2586(AdinCubeNativeEventListener adinCubeNativeEventListener) {
        boolean z;
        int i;
        int i2 = 0;
        if (adinCubeNativeEventListener != null) {
            try {
                e eVar = this.f2098.get(adinCubeNativeEventListener);
                if (eVar != null) {
                    f r4 = f.m3008();
                    f.a[] a = f.a.a();
                    int length = a.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length) {
                            z = true;
                            break;
                        } else if (!r4.m3013(a[i3])) {
                            z = false;
                            break;
                        } else {
                            i3++;
                        }
                    }
                    if (!z) {
                        m2579(eVar);
                        e = null;
                    } else {
                        for (com.adincube.sdk.mediation.r.b next : eVar.m3005(false)) {
                            if (next.麤() != null && this.f2100.m3013(f.a.ICON) && m2580(next.麤(), eVar)) {
                                i2++;
                            }
                            if (next.连任() != null && this.f2100.m3013(f.a.COVER_IMAGE) && m2580(next.连任(), eVar)) {
                                i2++;
                            }
                            if (next.ᐧ() != null && this.f2100.m3013(f.a.COVER_VIDEO)) {
                                com.adincube.sdk.g.d.b r5 = this.f2100.m3010(next.ᐧ());
                                r5.f2688 = this.f2097;
                                if (m2581(r5, eVar)) {
                                    i2++;
                                }
                            }
                            if (next.ʿ != null && next.ʿ.龘()) {
                                b.a aVar = next.ʿ;
                                f fVar = this.f2100;
                                com.adincube.sdk.g.d.b bVar = new com.adincube.sdk.g.d.b(aVar.龘);
                                bVar.f2694 = Integer.valueOf(f.m3007(f.a.AD_CHOICES_ICON));
                                fVar.m3012(bVar);
                                bVar.f2688 = this.f2097;
                                if (m2581(bVar, eVar)) {
                                    i = i2 + 1;
                                    i2 = i;
                                }
                            }
                            i = i2;
                            i2 = i;
                        }
                        if (i2 == 0) {
                            m2579(eVar);
                        }
                        e = null;
                    }
                    if (e != null) {
                        m2570(adinCubeNativeEventListener);
                        m2576(e, new com.adincube.sdk.f.b.d.b(adinCubeNativeEventListener, this));
                    }
                }
            } catch (c e) {
                e = e;
            } catch (Throwable th) {
                p pVar = new p(th);
                ErrorReportingHelper.m3506("AdinCubeNative.onNativeAdLoadedListener", com.adincube.sdk.g.c.b.NATIVE, th);
                e = pVar;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2587(AdinCubeNativeEventListener adinCubeNativeEventListener, c cVar) {
        m2570(adinCubeNativeEventListener);
        m2576(cVar, new com.adincube.sdk.f.b.d.b(adinCubeNativeEventListener, this));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2588(NativeAd nativeAd) {
        try {
            com.adincube.sdk.util.a.m3515("AdinCube.Native.dismiss()", new Object[0]);
            m2582(nativeAd);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AdinCube.Native.dismiss", th);
            ErrorReportingHelper.m3506("AdinCube.Native.dismiss", com.adincube.sdk.g.c.b.NATIVE, th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2589(com.adincube.sdk.g.d.b bVar, Bitmap bitmap) {
        try {
            synchronized (this.f2096) {
                Iterator<Map.Entry<ImageView, com.adincube.sdk.g.d.b>> it2 = this.f2096.entrySet().iterator();
                while (it2.hasNext()) {
                    Map.Entry next = it2.next();
                    if (next.getValue() == bVar) {
                        try {
                            ((ImageView) next.getKey()).setImageBitmap(bitmap);
                        } catch (Throwable th) {
                            com.adincube.sdk.util.a.m3513("AdinCubeNative.setImageBitmap", th);
                            ErrorReportingHelper.m3506("AdinCubeNative.setImageBitmap", com.adincube.sdk.g.c.b.NATIVE, th);
                        }
                        it2.remove();
                    }
                }
            }
        } catch (Throwable th2) {
            com.adincube.sdk.util.a.m3513("AdinCubeNative.onResourceLoaded", th2);
            ErrorReportingHelper.m3506("AdinCubeNative.onResourceLoaded", com.adincube.sdk.g.c.b.NATIVE, th2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2590(Throwable th) {
        com.adincube.sdk.util.a.m3513("AdinCubeNative.onResourceLoadFailed", th);
        ErrorReportingHelper.m3506("AdinCubeNative.onResourceLoadFailed", com.adincube.sdk.g.c.b.NATIVE, th);
    }
}
