package com.adincube.sdk.d;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import com.adincube.sdk.c.a.p;
import com.adincube.sdk.f.b.b.a;
import com.adincube.sdk.f.b.b.b;
import com.adincube.sdk.f.b.b.c;
import com.adincube.sdk.f.b.c.m;
import com.adincube.sdk.f.b.e;
import com.adincube.sdk.f.b.f;
import com.adincube.sdk.f.b.g;
import com.adincube.sdk.f.b.h;
import com.adincube.sdk.f.b.k;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a;
import com.adincube.sdk.util.d;
import com.adincube.sdk.util.o;
import java.util.concurrent.Callable;

public class a {

    /* renamed from: 龘  reason: contains not printable characters */
    private static a f2077 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f2078;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public com.adincube.sdk.f.b.b.a f2079;

    /* renamed from: ʽ  reason: contains not printable characters */
    private a.C0004a f2080;

    /* renamed from: 连任  reason: contains not printable characters */
    private c f2081;

    /* renamed from: 靐  reason: contains not printable characters */
    private com.adincube.sdk.f.a f2082;

    /* renamed from: 麤  reason: contains not printable characters */
    private b f2083;

    /* renamed from: 齉  reason: contains not printable characters */
    private com.adincube.sdk.f.a.b f2084;

    private a() {
        this.f2082 = null;
        this.f2084 = null;
        this.f2083 = null;
        this.f2081 = null;
        this.f2078 = 0;
        this.f2079 = null;
        this.f2080 = new a.C0004a() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final void m2568() {
                com.adincube.sdk.f.b.b.a unused = a.this.f2079 = null;
            }
        };
        this.f2082 = com.adincube.sdk.f.a.m2607();
        this.f2084 = com.adincube.sdk.f.a.b.m2615();
        this.f2083 = b.m2777();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private c m2560() {
        if (this.f2081 == null) {
            synchronized (this) {
                if (this.f2081 == null) {
                    com.adincube.sdk.util.e.b r9 = com.adincube.sdk.util.e.b.m3592();
                    h r6 = h.m3040();
                    com.adincube.sdk.f.b.e.b.a aVar = new com.adincube.sdk.f.b.e.b.a(com.adincube.sdk.g.c.b.INTERSTITIAL);
                    e eVar = new e(com.adincube.sdk.g.c.b.INTERSTITIAL, this.f2082);
                    m r14 = m.m2940();
                    f r15 = f.m3030(com.adincube.sdk.g.c.b.INTERSTITIAL);
                    g r5 = g.m3035();
                    this.f2081 = new c(this.f2082, r9, this.f2083, r6, aVar, new com.adincube.sdk.f.b.c(com.adincube.sdk.g.c.b.INTERSTITIAL, this.f2082, eVar, r5, r6), r14, r15, r5, k.m3056());
                }
            }
        }
        return this.f2081;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static a m2561() {
        if (f2077 == null) {
            synchronized (a.class) {
                if (f2077 == null) {
                    f2077 = new a();
                }
            }
        }
        return f2077;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m2563(com.adincube.sdk.c.a.c cVar, a.C0019a aVar, boolean z) {
        try {
            cVar.a(aVar);
            if (z) {
                b.m2777().m2780(cVar);
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AdinCubeInterstitial.displayError", th);
            ErrorReportingHelper.m3511("AdinCubeInterstitial.displayError", th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m2564() {
        try {
            return !Looper.getMainLooper().equals(Looper.myLooper()) ? (String) o.m3698(new Callable<String>() {
                public final /* synthetic */ Object call() {
                    return a.this.m2564();
                }
            }, null) : m2560().m2631();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AdinCubeInterstitial.getLastDisplayedNetwork", th);
            ErrorReportingHelper.m3511("AdinCubeInterstitial.getLastDisplayedNetwork", th);
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m2565(final Activity activity) {
        boolean z = true;
        com.adincube.sdk.c.a.c cVar = null;
        try {
            com.adincube.sdk.util.f.m3606((Context) activity);
            if (!Looper.getMainLooper().equals(Looper.myLooper())) {
                return o.m3705(new Callable<Boolean>() {
                    public final /* synthetic */ Object call() {
                        return Boolean.valueOf(a.this.m2565(activity));
                    }
                });
            }
            com.adincube.sdk.util.a.m3515("AdinCube.Interstitial.isReady()", new Object[0]);
            com.adincube.sdk.util.b.a.m3525();
            if (activity == null) {
                throw new com.adincube.sdk.c.a.a("isReady()");
            }
            this.f2084.m2617(activity);
            com.adincube.sdk.util.k.m3684(activity);
            com.adincube.sdk.f.a.m2608((Context) activity, true);
            m2560().m2749();
            if (cVar == null) {
                return z;
            }
            m2563(cVar, cVar.b(), false);
            return z;
        } catch (com.adincube.sdk.c.a.c e) {
            cVar = e;
            z = false;
        } catch (Throwable th) {
            ErrorReportingHelper.m3511("AdinCubeInterstitial.isReady", th);
            cVar = new p(th);
            z = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r12.f2079 = new com.adincube.sdk.f.b.b.a(r13, r12.f2082, r12.f2083, m2560());
        r12.f2079.f2251 = r12.f2080;
        r0 = r12.f2079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        com.adincube.sdk.util.a.m3515("SDK is currently caching next interstitial. show() has been deferred until next ad is available.", new java.lang.Object[0]);
        r0.f2253 = java.lang.System.currentTimeMillis();
        r1 = r0.f2254;
        r2 = r0.f2252;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0066, code lost:
        monitor-enter(r1.f2258);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r1.f2258.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r0.m2770();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0070, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0071, code lost:
        if (r0 != null) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        com.adincube.sdk.f.b.b.a.m2768((com.adincube.sdk.c.a.c) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0076, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0077, code lost:
        if (r0 != null) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0079, code lost:
        m2563(r0, com.adincube.sdk.util.a.C0019a.d, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007e, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0095, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c3, code lost:
        com.adincube.sdk.util.ErrorReportingHelper.m3511("AdinCubeInterstitial.show", r0);
        r0 = new com.adincube.sdk.c.a.p(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0110, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        com.adincube.sdk.util.ErrorReportingHelper.m3506("DeferredInterstitial.defer", com.adincube.sdk.g.c.b.INTERSTITIAL, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x011e, code lost:
        r0 = new com.adincube.sdk.c.a.p(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0121, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0122, code lost:
        com.adincube.sdk.util.ErrorReportingHelper.m3511("AdinCubeInterstitial.deferShowUntilAdCached", r0);
        r0 = new com.adincube.sdk.c.a.p(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0130, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0131, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a A[SYNTHETIC, Splitter:B:11:0x003a] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0095 A[Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }, ExcHandler: c (e com.adincube.sdk.c.a.c), Splitter:B:1:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00c2 A[ExcHandler: Throwable (r0v0 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0103 A[Catch:{ h -> 0x0130, c -> 0x0095, Throwable -> 0x00c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:? A[Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }, RETURN, SYNTHETIC] */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m2566(final android.app.Activity r13) {
        /*
            r12 = this;
            r3 = 0
            r1 = 0
            r4 = 1
            com.adincube.sdk.util.f.m3606((android.content.Context) r13)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            android.os.Looper r0 = android.os.Looper.getMainLooper()     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            android.os.Looper r2 = android.os.Looper.myLooper()     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            boolean r0 = r0.equals(r2)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            if (r0 != 0) goto L_0x0020
            com.adincube.sdk.d.a$3 r0 = new com.adincube.sdk.d.a$3     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            r0.<init>(r13)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            java.lang.String r2 = "HandlerUtil.dispatchOnUiThread"
            com.adincube.sdk.util.o.m3701((java.lang.String) r2, (java.lang.Runnable) r0)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
        L_0x001f:
            return
        L_0x0020:
            java.lang.String r0 = "AdinCube.Interstitial.show()"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            com.adincube.sdk.util.a.m3515(r0, r2)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            com.adincube.sdk.util.b.a.m3525()     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            if (r13 != 0) goto L_0x0087
            com.adincube.sdk.c.a.a r0 = new com.adincube.sdk.c.a.a     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            java.lang.String r2 = "show()"
            r0.<init>(r2)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            throw r0     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
        L_0x0037:
            r0 = move-exception
        L_0x0038:
            if (r1 == 0) goto L_0x007f
            com.adincube.sdk.f.b.b.a r0 = new com.adincube.sdk.f.b.b.a     // Catch:{ Throwable -> 0x0121 }
            com.adincube.sdk.f.a r1 = r12.f2082     // Catch:{ Throwable -> 0x0121 }
            com.adincube.sdk.f.b.b.b r2 = r12.f2083     // Catch:{ Throwable -> 0x0121 }
            com.adincube.sdk.f.b.b.c r5 = r12.m2560()     // Catch:{ Throwable -> 0x0121 }
            r0.<init>(r13, r1, r2, r5)     // Catch:{ Throwable -> 0x0121 }
            r12.f2079 = r0     // Catch:{ Throwable -> 0x0121 }
            com.adincube.sdk.f.b.b.a r0 = r12.f2079     // Catch:{ Throwable -> 0x0121 }
            com.adincube.sdk.f.b.b.a$a r1 = r12.f2080     // Catch:{ Throwable -> 0x0121 }
            r0.f2251 = r1     // Catch:{ Throwable -> 0x0121 }
            com.adincube.sdk.f.b.b.a r0 = r12.f2079     // Catch:{ Throwable -> 0x0121 }
            java.lang.String r1 = "SDK is currently caching next interstitial. show() has been deferred until next ad is available."
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0110 }
            com.adincube.sdk.util.a.m3515(r1, r2)     // Catch:{ Throwable -> 0x0110 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0110 }
            r0.f2253 = r6     // Catch:{ Throwable -> 0x0110 }
            com.adincube.sdk.f.b.b.b r1 = r0.f2254     // Catch:{ Throwable -> 0x0110 }
            com.adincube.sdk.AdinCubeInterstitialEventListener r2 = r0.f2252     // Catch:{ Throwable -> 0x0110 }
            java.util.Set<com.adincube.sdk.AdinCubeInterstitialEventListener> r5 = r1.f2258     // Catch:{ Throwable -> 0x0110 }
            monitor-enter(r5)     // Catch:{ Throwable -> 0x0110 }
            java.util.Set<com.adincube.sdk.AdinCubeInterstitialEventListener> r1 = r1.f2258     // Catch:{ all -> 0x010d }
            r1.add(r2)     // Catch:{ all -> 0x010d }
            monitor-exit(r5)     // Catch:{ all -> 0x010d }
            r0.m2770()     // Catch:{ Throwable -> 0x0110 }
            r0 = r3
        L_0x0071:
            if (r0 == 0) goto L_0x0076
            com.adincube.sdk.f.b.b.a.m2768((com.adincube.sdk.c.a.c) r0)     // Catch:{ Throwable -> 0x0121 }
        L_0x0076:
            r0 = r3
        L_0x0077:
            if (r0 == 0) goto L_0x007e
            com.adincube.sdk.util.a$a r1 = com.adincube.sdk.util.a.C0019a.d
            m2563(r0, r1, r4)
        L_0x007e:
            r0 = r3
        L_0x007f:
            if (r0 == 0) goto L_0x001f
            com.adincube.sdk.util.a$a r1 = com.adincube.sdk.util.a.C0019a.d
            m2563(r0, r1, r4)
            goto L_0x001f
        L_0x0087:
            com.adincube.sdk.f.b.b.a r0 = r12.f2079     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            if (r0 == 0) goto L_0x0097
            java.lang.String r0 = "Previous show call has been deferred."
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            com.adincube.sdk.util.a.m3515(r0, r2)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            goto L_0x001f
        L_0x0095:
            r0 = move-exception
            goto L_0x007f
        L_0x0097:
            com.adincube.sdk.f.a r0 = r12.f2082     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            r2 = 1
            r5 = 1
            com.adincube.sdk.g.b.b r0 = r0.m2610((boolean) r2, (boolean) r5)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            long r8 = com.adincube.sdk.f.b.b.a.m2764()     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            int r2 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r2 > 0) goto L_0x00d0
            long r10 = com.adincube.sdk.g.b.b.m3217((com.adincube.sdk.g.b.b) r0)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            long r8 = r8 + r10
            int r0 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x00d0
            r0 = r4
        L_0x00b5:
            if (r0 == 0) goto L_0x00d2
            java.lang.String r0 = "Calling show() after init() will automatically display an ad when one is available.\nPlease remove any call to show() in onAdLoaded callback."
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            com.adincube.sdk.util.a.m3512(r0, r2)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            goto L_0x001f
        L_0x00c2:
            r0 = move-exception
            java.lang.String r1 = "AdinCubeInterstitial.show"
            com.adincube.sdk.util.ErrorReportingHelper.m3511(r1, r0)
            com.adincube.sdk.c.a.p r1 = new com.adincube.sdk.c.a.p
            r1.<init>(r0)
            r0 = r1
            goto L_0x007f
        L_0x00d0:
            r0 = r1
            goto L_0x00b5
        L_0x00d2:
            boolean r0 = com.adincube.sdk.util.f.m3604(r13)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            if (r0 != 0) goto L_0x010b
            com.adincube.sdk.f.a r0 = r12.f2082     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            r2 = 1
            r5 = 1
            com.adincube.sdk.g.b.b r0 = r0.m2610((boolean) r2, (boolean) r5)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            long r8 = r12.f2078     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            int r2 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r2 > 0) goto L_0x010b
            long r8 = r12.f2078     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            long r10 = com.adincube.sdk.g.b.b.m3215((com.adincube.sdk.g.b.b) r0)     // Catch:{ h -> 0x0037, c -> 0x0095, Throwable -> 0x00c2 }
            long r8 = r8 + r10
            int r0 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x010b
            r2 = r4
        L_0x00f6:
            r0 = 1
            com.adincube.sdk.f.a.m2608((android.content.Context) r13, (boolean) r0)     // Catch:{ h -> 0x0130, c -> 0x0095, Throwable -> 0x00c2 }
            com.adincube.sdk.util.k.m3684(r13)     // Catch:{ h -> 0x0130, c -> 0x0095, Throwable -> 0x00c2 }
            com.adincube.sdk.f.b.b.c r0 = r12.m2560()     // Catch:{ h -> 0x0130, c -> 0x0095, Throwable -> 0x00c2 }
            if (r2 != 0) goto L_0x0104
            r1 = r4
        L_0x0104:
            r5 = 0
            r0.m2759((boolean) r1, (boolean) r5)     // Catch:{ h -> 0x0130, c -> 0x0095, Throwable -> 0x00c2 }
            r0 = r3
            goto L_0x007f
        L_0x010b:
            r2 = r1
            goto L_0x00f6
        L_0x010d:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x010d }
            throw r0     // Catch:{ Throwable -> 0x0110 }
        L_0x0110:
            r0 = move-exception
            java.lang.String r1 = "DeferredInterstitial.defer"
            com.adincube.sdk.g.c.b r2 = com.adincube.sdk.g.c.b.INTERSTITIAL     // Catch:{ Throwable -> 0x0121 }
            com.adincube.sdk.util.ErrorReportingHelper.m3506((java.lang.String) r1, (com.adincube.sdk.g.c.b) r2, (java.lang.Throwable) r0)     // Catch:{ Throwable -> 0x0121 }
            com.adincube.sdk.c.a.p r1 = new com.adincube.sdk.c.a.p     // Catch:{ Throwable -> 0x0121 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x0121 }
            r0 = r1
            goto L_0x0071
        L_0x0121:
            r0 = move-exception
            java.lang.String r1 = "AdinCubeInterstitial.deferShowUntilAdCached"
            com.adincube.sdk.util.ErrorReportingHelper.m3511(r1, r0)
            com.adincube.sdk.c.a.p r1 = new com.adincube.sdk.c.a.p
            r1.<init>(r0)
            r0 = r1
            goto L_0x0077
        L_0x0130:
            r0 = move-exception
            r1 = r2
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.d.a.m2566(android.app.Activity):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2567(final Activity activity) {
        com.adincube.sdk.c.a.c e = null;
        try {
            com.adincube.sdk.util.f.m3606((Context) activity);
            if (!Looper.getMainLooper().equals(Looper.myLooper())) {
                o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) new Runnable() {
                    public final void run() {
                        a.this.m2567(activity);
                    }
                });
                return;
            }
            com.adincube.sdk.util.a.m3515("AdinCube.Interstitial.init()", new Object[0]);
            com.adincube.sdk.util.b.a.m3525();
            if (activity == null) {
                throw new com.adincube.sdk.c.a.a("init()");
            }
            this.f2084.m2617(activity);
            com.adincube.sdk.util.e.a.m3583(activity);
            com.adincube.sdk.util.k.m3684(activity);
            com.adincube.sdk.f.a.m2608((Context) activity, false);
            if (com.adincube.sdk.util.e.a.m3585()) {
                com.adincube.sdk.util.a.m3515("Configuration changed.", new Object[0]);
                com.adincube.sdk.util.e.b.m3592().m3596();
                com.adincube.sdk.f.a.m2607().m2609();
                d.m3554();
                com.adincube.sdk.util.e.a.m3580();
            }
            m2560().m2757();
            this.f2078 = System.currentTimeMillis();
            if (e != null) {
                m2563(e, a.C0019a.d, false);
            }
        } catch (com.adincube.sdk.c.a.c e2) {
            e = e2;
        } catch (Throwable th) {
            ErrorReportingHelper.m3511("AdinCubeInterstitial.init", th);
            e = new p(th);
        }
    }
}
