package com.adincube.sdk.e;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.internal.view.SupportMenu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.b.i;

@SuppressLint({"NewApi"})
public final class a extends ViewGroup {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f2116;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f2117;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f2118;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f2119;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Double f2120;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f2121 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f2122;

    /* renamed from: 靐  reason: contains not printable characters */
    public C0002a f2123 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f2124;

    /* renamed from: 齉  reason: contains not printable characters */
    public final Drawable.Callback f2125 = new Drawable.Callback() {
        public final void invalidateDrawable(Drawable drawable) {
            a.this.invalidate();
        }

        public final void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        }

        public final void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    public com.adincube.sdk.b.a f2126 = null;

    /* renamed from: com.adincube.sdk.e.a$a  reason: collision with other inner class name */
    public interface C0002a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2606();
    }

    public a(Context context) {
        super(context);
        setClipToPadding(false);
        setDrawingCacheEnabled(true);
        setClickable(true);
        this.f2120 = null;
        this.f2119 = false;
        this.f2118 = i.m3540(getContext(), 45);
        this.f2124 = i.m3540(getContext(), -7);
        this.f2116 = i.m3540(getContext(), 7);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m2600() {
        return (this.f2126.getBounds().height() / 2) + getPaddingTop() + this.f2116 + this.f2117;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m2601() {
        return ((getMeasuredWidth() - getPaddingLeft()) - (this.f2126.getBounds().width() / 2)) + this.f2124 + this.f2122;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m2602(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 0) {
            double sqrt = Math.sqrt(Math.pow((double) (motionEvent.getX() - ((float) m2601())), 2.0d) + Math.pow((double) (motionEvent.getY() - ((float) m2600())), 2.0d));
            if (action == 0) {
                if (sqrt < ((double) this.f2118)) {
                    this.f2121 = true;
                    return true;
                }
                this.f2121 = false;
            } else if (action == 1) {
                if (!this.f2121 || sqrt >= ((double) this.f2118)) {
                    this.f2121 = false;
                } else if (!this.f2126.m2556()) {
                    return true;
                } else {
                    playSoundEffect(0);
                    if (this.f2123 == null) {
                        return true;
                    }
                    this.f2123.m2606();
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        try {
            super.dispatchDraw(canvas);
            if (getChildCount() > 0) {
                canvas.save();
                canvas.translate((float) (m2601() - (this.f2126.getBounds().width() / 2)), (float) (m2600() - (this.f2126.getBounds().height() / 2)));
                this.f2126.draw(canvas);
                canvas.restore();
                if (this.f2119) {
                    Paint paint = new Paint();
                    paint.setColor(SupportMenu.CATEGORY_MASK);
                    paint.setAlpha(127);
                    canvas.drawCircle((float) m2601(), (float) m2600(), (float) this.f2118, paint);
                }
            }
        } catch (Throwable th) {
            ErrorReportingHelper.m3506("ABInterstitialAdLayout.dispatchDraw", b.INTERSTITIAL, th);
            com.adincube.sdk.util.a.m3513("ABInterstitialLayout.dispatchDraw()", th);
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        try {
            return m2602(motionEvent);
        } catch (Throwable th) {
            ErrorReportingHelper.m3506("ABInterstitialAdLayout.onInterceptTouchEvent", b.INTERSTITIAL, th);
            com.adincube.sdk.util.a.m3513("ABInterstitialLayout.onInterceptTouchEvent()", th);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        try {
            if (getChildCount() > 0) {
                View childAt = getChildAt(0);
                int measuredWidth = ((((i3 - i) - childAt.getMeasuredWidth()) - getPaddingRight()) - getPaddingLeft()) / 2;
                int measuredHeight = ((((i4 - i2) - childAt.getMeasuredHeight()) - getPaddingTop()) - getPaddingBottom()) / 2;
                childAt.layout(i + measuredWidth + getPaddingLeft(), i2 + measuredHeight + getPaddingTop(), (i3 - measuredWidth) - getPaddingLeft(), (i4 - measuredHeight) - getPaddingBottom());
                for (int i5 = 1; i5 < getChildCount(); i5++) {
                    getChildAt(i5).layout(i, i4 - getChildAt(i5).getMeasuredHeight(), i3, i4);
                }
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("ABInterstitialLayout.onLayout()", th);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        try {
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            int paddingRight = (size - getPaddingRight()) - getPaddingLeft();
            int paddingBottom = (size2 - getPaddingBottom()) - getPaddingTop();
            if (getChildCount() > 0) {
                View childAt = getChildAt(0);
                int i3 = Integer.MIN_VALUE;
                int i4 = Integer.MIN_VALUE;
                if (this.f2120 != null) {
                    if (((double) paddingRight) / this.f2120.doubleValue() < ((double) paddingBottom)) {
                        paddingBottom = (int) Math.floor(((double) paddingRight) / this.f2120.doubleValue());
                    }
                    if (((double) paddingBottom) * this.f2120.doubleValue() < ((double) paddingRight)) {
                        paddingRight = (int) Math.floor(((double) paddingBottom) * this.f2120.doubleValue());
                    }
                    i3 = 1073741824;
                    i4 = 1073741824;
                }
                childAt.measure(View.MeasureSpec.makeMeasureSpec(paddingRight, i3), View.MeasureSpec.makeMeasureSpec(paddingBottom, i4));
                for (int i5 = 1; i5 < getChildCount(); i5++) {
                    getChildAt(i5).measure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
                }
            }
        } catch (Throwable th) {
            ErrorReportingHelper.m3506("ABInterstitialAdLayout.onMeasure", b.INTERSTITIAL, th);
            com.adincube.sdk.util.a.m3513("ABInterstitialLayout.onMeasure()", th);
        }
        super.onMeasure(i, i2);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            return m2602(motionEvent);
        } catch (Throwable th) {
            ErrorReportingHelper.m3506("ABInterstitialAdLayout.onTouchEvent", b.INTERSTITIAL, th);
            com.adincube.sdk.util.a.m3513("ABInterstitialAdLayout.onTouchEvent()", th);
            return super.onTouchEvent(motionEvent);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2603(int i) {
        if (this.f2117 != i) {
            this.f2117 = i;
            requestLayout();
            invalidate();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2604(Double d) {
        if (this.f2120 != d) {
            this.f2120 = d;
            requestLayout();
            invalidate();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2605(boolean z) {
        if (this.f2126.m2556() != z) {
            if (z) {
                this.f2126.setState(new int[]{16842910});
            } else {
                this.f2126.setState(new int[0]);
            }
            invalidate();
        }
    }
}
