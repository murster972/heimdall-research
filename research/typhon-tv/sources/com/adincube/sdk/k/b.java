package com.adincube.sdk.k;

import android.content.Context;
import android.graphics.Canvas;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.adincube.sdk.h.c;
import com.adincube.sdk.h.e;
import com.adincube.sdk.mediation.v.f;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.f.a.a;
import com.adincube.sdk.util.o;
import com.adincube.sdk.util.t;

public final class b extends ViewGroup {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public c f2921 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public a f2922 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public e f2923 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    private c.d f2924 = new c.d() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3425(String str, String str2) {
            b.this.f2923.m3278(str, str2);
            com.adincube.sdk.util.f.a.b r0 = b.this.f2929;
            if (r0.f3026.m3279(false) && r0.m3614(true)) {
                r0.m3613();
            }
        }
    };

    /* renamed from: ˑ  reason: contains not printable characters */
    private com.adincube.sdk.f.e.b.a f2925 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f2926 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private c.C0015c f2927 = new c.C0015c() {
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m3422(Uri uri) {
            b.this.f2922.m3611(uri);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3423() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3424(Uri uri) {
            b.this.f2922.m3612(uri);
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private WebView f2928 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public com.adincube.sdk.util.f.a.b f2929 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private com.adincube.sdk.g.c.c f2930 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private com.adincube.sdk.g.a.b f2931 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public com.adincube.sdk.util.f.b f2932 = null;

    public b(Context context, com.adincube.sdk.g.c.c cVar, com.adincube.sdk.g.a.b bVar, f fVar) {
        super(context);
        this.f2931 = bVar;
        this.f2930 = cVar;
        this.f2925 = new com.adincube.sdk.f.e.b.a(bVar, fVar, new com.adincube.sdk.f.e.a(com.adincube.sdk.f.a.m2607(), new com.adincube.sdk.f.e.b(com.adincube.sdk.g.c.b.BANNER), t.m3714()));
        this.f2932 = new com.adincube.sdk.util.f.b();
        this.f2922 = new a(context, fVar, this.f2932, this.f2925);
        this.f2923 = new e();
        this.f2929 = new com.adincube.sdk.util.f.a.b(bVar, cVar, fVar, this.f2923);
        this.f2932.f3036 = bVar;
        this.f2932.m3621(fVar);
        try {
            this.f2928 = new WebView(context);
            c cVar2 = new c(context, bVar, fVar);
            cVar2.f2725 = this.f2927;
            cVar2.f2723 = this.f2924;
            this.f2921 = cVar2;
            this.f2921.m3264(this.f2928);
            this.f2928.setOnTouchListener(this.f2925);
            addView(this.f2928, new ViewGroup.LayoutParams(-1, -1));
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("RTBBannerView", th);
            ErrorReportingHelper.m3505("RTBBannerView", com.adincube.sdk.g.c.b.BANNER, cVar.i, th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static /* synthetic */ void m3418(b bVar) {
        try {
            if (!bVar.f2926) {
                bVar.f2926 = true;
                bVar.f2921.m3265(com.adincube.sdk.h.f.DEFAULT);
                bVar.f2921.m3260();
                bVar.f2921.m3263();
                bVar.f2925.m3139();
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("RTBBannerView.loadAdContent", new Object[0]);
            ErrorReportingHelper.m3505("RTBBannerView.loadAdContent", com.adincube.sdk.g.c.b.BANNER, bVar.f2930.i, th);
        }
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            this.f2932.m3620();
            this.f2921.m3266(getVisibility() == 0);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("RTBBannerView.onAttachedToWindow", th);
            ErrorReportingHelper.m3502("RTBBannerView.onAttachedToWindow", this.f2931, this.f2930, th);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            com.adincube.sdk.util.f.a.b bVar = this.f2929;
            if (bVar.m3614(bVar.f3026.m3279(true))) {
                bVar.m3613();
            }
            this.f2921.m3266(false);
            this.f2921.m3265(com.adincube.sdk.h.f.HIDDEN);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("RTBBannerView.onDetachedFromWindow", th);
            ErrorReportingHelper.m3502("RTBBannerView.onDetachedFromWindow", this.f2931, this.f2930, th);
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (z) {
            this.f2928.layout(getPaddingLeft(), getPaddingTop(), ((i3 - i) - getPaddingLeft()) - getPaddingRight(), ((i4 - i2) - getPaddingTop()) - getPaddingBottom());
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        try {
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            this.f2928.measure(View.MeasureSpec.makeMeasureSpec((size - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((size2 - getPaddingTop()) - getPaddingBottom(), 1073741824));
            setMeasuredDimension(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(size2, 1073741824));
            o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) new Runnable() {
                public final void run() {
                    b.this.f2921.m3261();
                    b.this.f2921.m3262();
                    b.m3418(b.this);
                }
            });
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("RTBBannerView.onMeasure", th);
            ErrorReportingHelper.m3502("RTBBannerView.onMeasure", this.f2931, this.f2930, th);
        }
    }

    public final void setVisibility(int i) {
        super.setVisibility(i);
        try {
            this.f2921.m3266(i == 0);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("RTBBannerView.setVisibility", th);
            ErrorReportingHelper.m3502("RTBBannerView.setVisibility", this.f2931, this.f2930, th);
        }
    }
}
