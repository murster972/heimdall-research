package com.adincube.sdk.k;

import android.content.Context;
import android.view.View;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.util.ErrorReportingHelper;

public final class c extends View {

    /* renamed from: 靐  reason: contains not printable characters */
    public a f2936 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public int f2937 = 8;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3427();
    }

    public c(Context context) {
        super(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3426(int i) {
        if (i != this.f2937) {
            this.f2937 = i;
            if (this.f2936 != null) {
                this.f2936.m3427();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            m3426(getWindowVisibility());
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("WindowVisibilityDetectionView.onAttachedToWindow", th);
            ErrorReportingHelper.m3506("WindowVisibilityDetectionView.onAttachedToWindow", b.NATIVE, th);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            m3426(8);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("WindowVisibilityDetectionView.onDetachedFromWindow", th);
            ErrorReportingHelper.m3506("WindowVisibilityDetectionView.onDetachedFromWindow", b.NATIVE, th);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        try {
            setMeasuredDimension(View.MeasureSpec.makeMeasureSpec(0, 1073741824), View.MeasureSpec.makeMeasureSpec(0, 1073741824));
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("WindowVisibilityDetectionView.onMeasure", th);
            ErrorReportingHelper.m3506("WindowVisibilityDetectionView.onMeasure", b.NATIVE, th);
        }
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        try {
            m3426(i);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("WindowVisibilityDetectionView.onWindowVisibilityChanged", th);
            ErrorReportingHelper.m3506("WindowVisibilityDetectionView.onWindowVisibilityChanged", b.NATIVE, th);
        }
    }
}
