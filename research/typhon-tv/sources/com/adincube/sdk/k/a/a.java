package com.adincube.sdk.k.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import com.adincube.sdk.f.b.d.f;
import com.adincube.sdk.g.d.b;
import com.adincube.sdk.mediation.r.c;
import com.adincube.sdk.util.ErrorReportingHelper;

@SuppressLint({"AppCompatCustomView"})
public final class a extends ImageView implements View.OnClickListener, b.a {

    /* renamed from: 靐  reason: contains not printable characters */
    public b f2893 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private f f2894 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public com.adincube.sdk.f.d.b f2895 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public com.adincube.sdk.mediation.r.b f2896 = null;

    public a(Context context) {
        super(context);
        try {
            this.f2895 = com.adincube.sdk.f.d.b.m3088();
            this.f2894 = f.m3008();
            setOnClickListener(this);
            setScaleType(ImageView.ScaleType.FIT_CENTER);
            setAdjustViewBounds(true);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdAdChoicesView.init", th);
            ErrorReportingHelper.m3506("NativeAdAdChoicesView.init", com.adincube.sdk.g.c.b.NATIVE, th);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r8) {
        /*
            r7 = this;
            r6 = 1
            r5 = 0
            android.content.Context r0 = r7.getContext()     // Catch:{ Throwable -> 0x0038 }
            com.adincube.sdk.mediation.r.b r1 = r7.f2896     // Catch:{ Throwable -> 0x0038 }
            if (r1 == 0) goto L_0x000e
            boolean r2 = r1.ﹶ     // Catch:{ Throwable -> 0x0038 }
            if (r2 == 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            com.adincube.sdk.mediation.r.c r2 = r1.齉     // Catch:{ Throwable -> 0x0038 }
            if (r2 == 0) goto L_0x000e
            r2.齉(r0, r1)     // Catch:{ Throwable -> 0x0017 }
            goto L_0x000e
        L_0x0017:
            r0 = move-exception
            com.adincube.sdk.mediation.h r1 = r2.ʼ()     // Catch:{ Throwable -> 0x0038 }
            java.lang.String r1 = r1.m3473()     // Catch:{ Throwable -> 0x0038 }
            java.lang.String r2 = "Error caught while performing click on ad choices icon for network '%s'."
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0038 }
            r4 = 0
            r3[r4] = r1     // Catch:{ Throwable -> 0x0038 }
            r4 = 1
            r3[r4] = r0     // Catch:{ Throwable -> 0x0038 }
            com.adincube.sdk.util.a.m3512(r2, r3)     // Catch:{ Throwable -> 0x0038 }
            java.lang.String r2 = "AdChoicesImageView.performClick"
            com.adincube.sdk.g.c.b r3 = com.adincube.sdk.g.c.b.NATIVE     // Catch:{ Throwable -> 0x0038 }
            com.adincube.sdk.util.ErrorReportingHelper.m3510((java.lang.String) r2, (java.lang.String) r1, (com.adincube.sdk.g.c.b) r3, (java.lang.Throwable) r0)     // Catch:{ Throwable -> 0x0038 }
            goto L_0x000e
        L_0x0038:
            r0 = move-exception
            java.lang.String r1 = "NativeAdAdChoicesView.onClick"
            java.lang.Object[] r2 = new java.lang.Object[r6]
            r2[r5] = r0
            com.adincube.sdk.util.a.m3513(r1, r2)
            java.lang.String r1 = "NativeAdAdChoicesView.onClick"
            com.adincube.sdk.g.c.b r2 = com.adincube.sdk.g.c.b.NATIVE
            com.adincube.sdk.util.ErrorReportingHelper.m3506((java.lang.String) r1, (com.adincube.sdk.g.c.b) r2, (java.lang.Throwable) r0)
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.k.a.a.onClick(android.view.View):void");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3403(b bVar) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Drawable m3404(c cVar) {
        try {
            return cVar.龘(getContext());
        } catch (Throwable th) {
            String r1 = cVar.ʼ().m3473();
            com.adincube.sdk.util.a.m3513("Cannot retrieve ad choices icon for network '%s'. ", r1, th);
            ErrorReportingHelper.m3510("AdChoicesImageView.getAdChoicesDrawable", r1, com.adincube.sdk.g.c.b.NATIVE, th);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3405(b bVar) {
        try {
            if (this.f2896 != null && !this.f2896.ﹶ) {
                this.f2895.m3093(bVar);
                setImageBitmap(BitmapFactory.decodeFile(bVar.f2682.getAbsolutePath()));
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AdChoicesImageView.onResourceCached", th);
            ErrorReportingHelper.m3511("AdChoicesImageView.onResourceCached", th);
        }
    }
}
