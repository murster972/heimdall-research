package com.adincube.sdk.k.a;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.util.TypedValue;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.adincube.sdk.a.a.a.e;
import com.adincube.sdk.a.a.a.g;
import com.adincube.sdk.a.a.a.h;
import com.adincube.sdk.a.a.a.i;
import com.adincube.sdk.f.b.d.f;
import com.adincube.sdk.f.g.a;
import com.adincube.sdk.g.c.h;
import com.adincube.sdk.g.d.b;
import com.adincube.sdk.util.ErrorReportingHelper;

public final class c extends ViewGroup implements View.OnClickListener, e.a, a.b, b.a {

    /* renamed from: ʻ  reason: contains not printable characters */
    public com.adincube.sdk.f.b.d.b.a f2898 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private com.adincube.sdk.a.a.e f2899 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    private com.adincube.sdk.f.g.a f2900 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f2901 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f2902 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private com.adincube.sdk.g.b.b f2903 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f2904;

    /* renamed from: ˑ  reason: contains not printable characters */
    private View f2905 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private View f2906 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private View f2907 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public f f2908 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public com.adincube.sdk.g.d.b f2909;

    /* renamed from: 麤  reason: contains not printable characters */
    public com.adincube.sdk.f.d.b f2910 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public i f2911 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public com.adincube.sdk.mediation.r.b f2912;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private double f2913 = 1.7777777777777777d;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private double f2914 = 0.0d;

    /* renamed from: com.adincube.sdk.k.a.c$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f2915 = new int[b.m3416().length];

        static {
            try {
                f2915[b.f2920 - 1] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f2915[b.f2917 - 1] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f2915[b.f2919 - 1] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    private static class a extends ViewGroup.LayoutParams {

        /* renamed from: 龘  reason: contains not printable characters */
        public int f2916;

        public a(int i) {
            super(0, 0);
            this.f2916 = i;
        }
    }

    private enum b {
        ;

        static {
            f2920 = 1;
            f2917 = 2;
            f2919 = 3;
            f2918 = new int[]{f2920, f2917, f2919};
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static int[] m3416() {
            return (int[]) f2918.clone();
        }
    }

    public c(Context context, com.adincube.sdk.f.b.d.b.a aVar) {
        super(context);
        try {
            this.f2903 = com.adincube.sdk.f.a.m2607().m2610(true, true);
            this.f2898 = aVar;
            this.f2904 = (int) TypedValue.applyDimension(1, (float) aVar.f2379.intValue(), context.getResources().getDisplayMetrics());
            this.f2910 = com.adincube.sdk.f.d.b.m3088();
            this.f2908 = f.m3008();
            this.f2900 = com.adincube.sdk.f.g.a.m3158(context);
            this.f2911 = new i(getContext(), new com.adincube.sdk.a.a.a.f());
            this.f2911.m2479((e.a) this);
            this.f2899 = new com.adincube.sdk.a.a.e(context, this.f2911);
            this.f2899.m2523(aVar.f2378.booleanValue(), true);
            i iVar = this.f2911;
            TextureView textureView = new TextureView(context);
            textureView.setLayoutParams(new a(b.f2920));
            addView(textureView);
            iVar.m2454(textureView);
            this.f2905 = textureView;
            ImageView imageView = new ImageView(context);
            imageView.setImageDrawable(new com.adincube.sdk.b.b(context));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setLayoutParams(new a(b.f2917));
            imageView.setBackgroundColor(Color.argb(170, 0, 0, 0));
            addView(imageView);
            this.f2906 = imageView;
            ImageButton r0 = this.f2899.m2522();
            r0.setLayoutParams(new a(b.f2919));
            r0.setVisibility(8);
            addView(r0);
            this.f2907 = r0;
            setBackgroundColor(-16777216);
            setClickable(true);
            setOnClickListener(this);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.init", th);
            ErrorReportingHelper.m3506("NativeAdVideoMediaView.init", com.adincube.sdk.g.c.b.NATIVE, th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m3406() {
        this.f2901 = false;
        this.f2906.setVisibility(0);
        this.f2907.setVisibility(8);
        if (this.f2911.m2472() == h.READY || this.f2911.m2472() == h.PLAYING) {
            this.f2911.m2464();
        } else {
            this.f2902 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3407() {
        if (com.adincube.sdk.f.g.a.m3154(this, this.f2898.f2377.doubleValue())) {
            this.f2901 = true;
            this.f2906.setVisibility(8);
            this.f2907.setVisibility(0);
            if (this.f2911.m2472() == h.READY || this.f2911.m2472() == h.PLAYING) {
                this.f2911.m2466();
                this.f2902 = false;
                return;
            }
            this.f2902 = true;
        }
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            this.f2900.m3168((a.b) this);
            this.f2900.m3167((View) this, this.f2898.f2377.doubleValue());
            this.f2911.m2475(getContext());
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.onAttachedToWindow", th);
            ErrorReportingHelper.m3511("NativeAdVideoMediaView.onAttachedToWindow", th);
        }
    }

    public final void onClick(View view) {
        try {
            if (this.f2901) {
                m3406();
            } else {
                m3407();
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.onClick", th);
            ErrorReportingHelper.m3511("NativeAdVideoMediaView.onClick", th);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            this.f2900.m3166((View) this);
            this.f2900.m3165((a.b) this);
            this.f2911.m2470();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.onDetachedFromWindow", th);
            ErrorReportingHelper.m3511("NativeAdVideoMediaView.onDetachedFromWindow", th);
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = 0;
        while (true) {
            int i6 = i5;
            if (i6 < getChildCount()) {
                View childAt = getChildAt(i6);
                switch (AnonymousClass1.f2915[((a) childAt.getLayoutParams()).f2916 - 1]) {
                    case 1:
                        int measuredWidth = childAt.getMeasuredWidth();
                        int measuredHeight = childAt.getMeasuredHeight();
                        int floor = (int) Math.floor(((double) ((i3 - i) - measuredWidth)) / 2.0d);
                        int floor2 = (int) Math.floor(((double) ((i4 - i2) - measuredHeight)) / 2.0d);
                        childAt.layout(i + floor, i2 + floor2, i3 - floor, i4 - floor2);
                        break;
                    case 2:
                        childAt.layout(i, i2, i3, i4);
                        break;
                    case 3:
                        childAt.layout(i3 - childAt.getMeasuredWidth(), i4 - childAt.getMeasuredHeight(), i3, i4);
                        break;
                }
                i5 = i6 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int i3;
        int size = View.MeasureSpec.getSize(i);
        int mode = View.MeasureSpec.getMode(i);
        int size2 = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i2);
        int i4 = this.f2904;
        if (mode2 == 0) {
            if (mode == 0) {
                size = (int) (((double) i4) * this.f2913);
                size2 = i4;
            } else {
                size2 = Math.max(i4, (int) (((double) size) / this.f2913));
            }
        } else if (mode2 == Integer.MIN_VALUE) {
            if (mode == 0) {
                int min = Math.min(i4, size2);
                size = (int) (((double) min) * this.f2913);
                size2 = min;
            } else {
                size2 = Math.max(i4, (int) (((double) size) / this.f2913));
            }
        } else if (mode2 == 1073741824) {
            if (size == 0) {
                size = (int) (((double) size2) * this.f2913);
            } else if (size == Integer.MIN_VALUE) {
                size = Math.min(size, (int) (((double) size2) * this.f2913));
            } else if (size == 1073741824) {
            }
        }
        setMeasuredDimension(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(size2, 1073741824));
        int i5 = 0;
        while (true) {
            int i6 = i5;
            if (i6 < getChildCount()) {
                View childAt = getChildAt(i6);
                switch (AnonymousClass1.f2915[((a) childAt.getLayoutParams()).f2916 - 1]) {
                    case 1:
                        if (this.f2913 >= 0.001d) {
                            int floor = (int) Math.floor(((double) size) / this.f2914);
                            if (floor > size2) {
                                i3 = (int) Math.floor(((double) size2) * this.f2914);
                                floor = size2;
                            } else {
                                i3 = size;
                            }
                            childAt.measure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), View.MeasureSpec.makeMeasureSpec(floor, 1073741824));
                            break;
                        } else {
                            childAt.measure(View.MeasureSpec.makeMeasureSpec(0, 1073741824), View.MeasureSpec.makeMeasureSpec(0, 1073741824));
                            break;
                        }
                    case 2:
                        childAt.measure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(size2, 1073741824));
                        break;
                    case 3:
                        childAt.measure(View.MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
                        break;
                }
                i5 = i6 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3408(e eVar) {
        try {
            this.f2912.ʻ.m3014(new com.adincube.sdk.g.c.h(h.a.COMPLETED));
            m3406();
            eVar.m2456(false);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.onPlayerCompleted", th);
            ErrorReportingHelper.m3511("NativeAdVideoMediaView.onPlayerCompleted", th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3409(com.adincube.sdk.g.d.b bVar) {
        try {
            new Object[1][0] = this.f2912.龘();
            m3411(bVar);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.onResourceCachingFailed", th);
            ErrorReportingHelper.m3506("NativeAdVideoMediaView.onResourceCachingFailed", com.adincube.sdk.g.c.b.NATIVE, th);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3410() {
        try {
            this.f2912.ʻ.m3014(new com.adincube.sdk.g.c.h(h.a.STARTED));
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.onPlayerPlaying", th);
            ErrorReportingHelper.m3511("NativeAdVideoMediaView.onPlayerPlaying", th);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3411(com.adincube.sdk.g.d.b bVar) {
        Uri parse = Uri.parse(bVar.f2693);
        new Object[1][0] = bVar.f2693;
        this.f2911.m2476(parse);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3412(View view, boolean z) {
        if (view == this) {
            try {
                if (this.f2898.f2381.booleanValue() && z) {
                    m3407();
                } else if (!z) {
                    m3406();
                }
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.onVisibilityChanged", th);
                ErrorReportingHelper.m3511("NativeAdVideoMediaView.onVisibilityChanged", th);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3413(e eVar) {
        try {
            double r0 = ((double) eVar.m2445()) / ((double) eVar.m2446());
            if (r0 < this.f2914 - 0.001d || r0 > this.f2914 + 0.001d) {
                this.f2914 = r0;
                requestLayout();
            }
            if (this.f2902) {
                m3407();
            }
            this.f2902 = false;
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.onPlayerReady", th);
            ErrorReportingHelper.m3511("NativeAdVideoMediaView.onPlayerReady", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3414(e eVar, g gVar) {
        if (this.f2903.f2639) {
            ErrorReportingHelper.m3501(gVar, Uri.parse(this.f2912.ᐧ().龘), com.adincube.sdk.g.c.b.NATIVE, (com.adincube.sdk.g.c.a) null, (Boolean) null);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3415(com.adincube.sdk.g.d.b bVar) {
        try {
            this.f2910.m3093(bVar);
            Uri parse = Uri.parse("file://" + bVar.f2682.getAbsolutePath());
            new Object[1][0] = bVar.f2682;
            this.f2911.m2476(parse);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdVideoMediaView.onResourceCached", th);
            ErrorReportingHelper.m3506("NativeAdVideoMediaView.onResourceCached", com.adincube.sdk.g.c.b.NATIVE, th);
        }
    }
}
