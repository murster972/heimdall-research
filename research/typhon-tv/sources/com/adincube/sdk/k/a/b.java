package com.adincube.sdk.k.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.adincube.sdk.f.b.d.b.a;
import com.adincube.sdk.util.ErrorReportingHelper;

@SuppressLint({"AppCompatCustomView"})
public final class b extends ImageView implements View.OnClickListener {

    /* renamed from: 龘  reason: contains not printable characters */
    public com.adincube.sdk.mediation.r.b f2897 = null;

    public b(Context context, a aVar) {
        super(context);
        try {
            setOnClickListener(this);
            if (aVar.f2385 != null) {
                setMaxHeight(aVar.f2385.intValue());
            }
            if (aVar.f2382 != null) {
                setMaxWidth(aVar.f2382.intValue());
            }
            setScaleType(aVar.f2383);
            setAdjustViewBounds(aVar.f2384.booleanValue());
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("NativeAdImageMediaView.init", th);
            ErrorReportingHelper.m3511("NativeAdImageMediaView.init", th);
        }
    }

    public final void onClick(View view) {
        this.f2897.麤.m2957(getContext());
    }
}
