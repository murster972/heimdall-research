package com.adincube.sdk.k;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.adincube.sdk.util.h.g;

public class a extends FrameLayout {
    public a(Context context) {
        super(context);
    }

    public a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public a(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void addView(View view) {
        if (view.getLayoutParams() == null) {
            view.setLayoutParams(m3402());
        }
        super.addView(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final FrameLayout.LayoutParams m3402() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(g.m3662(getLayoutParams()), g.m3661(getLayoutParams()));
        layoutParams.gravity = 17;
        return layoutParams;
    }
}
