package com.adincube.sdk.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import com.adincube.sdk.a.a.a.g;
import com.adincube.sdk.a.a.a.h;
import com.adincube.sdk.a.a.b;
import com.adincube.sdk.a.a.d;
import com.adincube.sdk.a.a.e;
import com.adincube.sdk.f.e.c.a;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.n;

@SuppressLint({"NewApi"})
public final class c extends b implements d.a, e.a {

    /* renamed from: ˆ  reason: contains not printable characters */
    private a f2054 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    private com.adincube.sdk.j.c f2055 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private com.adincube.sdk.g.a.a.a f2056 = null;

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public final com.adincube.sdk.g.a.a.a m2539() {
        if (this.f2056 == null) {
            this.f2056 = com.adincube.sdk.util.c.m3550(this.f1961, com.adincube.sdk.g.a.a.b.MEDIA);
        }
        return this.f2056;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʿ  reason: contains not printable characters */
    public final Uri m2540() {
        com.adincube.sdk.g.a.a.a r0 = m2539();
        return r0.m3176() ? Uri.parse("file://" + f.m3605().getFileStreamPath(n.m3687(r0)).getAbsolutePath()) : Uri.parse(r0.m3174());
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final String m2541() {
        return this.f1961.f2565;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final long m2542() {
        String str = null;
        String[] strArr = {this.f2056.m3180("so"), this.f1961.f2580};
        int i = 0;
        while (i < 2) {
            String str2 = strArr[i];
            if (str2 == null) {
                str2 = str;
            }
            i++;
            str = str2;
        }
        if (str != null) {
            return com.adincube.sdk.g.e.a.m3246(str, com.adincube.sdk.util.g.b.m3640(this.f2056)).f2698;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m2543() {
        boolean z = true;
        super.m2375();
        if (this.f2015 != null && !this.f2015.m2395()) {
            com.adincube.sdk.a.a.a.e eVar = this.f2017;
            com.adincube.sdk.g.a.a.a aVar = this.f2056;
            long r4 = m2542();
            if (!(aVar.m3180("so") != null) || eVar.m2449() != h.PLAYING || eVar.m2457() <= r4) {
                z = false;
            }
            if (z) {
                this.f2054.m3142(this.f2056, com.adincube.sdk.j.b.b.d.skip, Long.valueOf(this.f2017.m2457()));
                this.f2014.m2517(com.adincube.sdk.j.b.b.d.skip);
                return;
            }
            this.f2054.m3142(this.f2056, com.adincube.sdk.j.b.b.d.closeLinear, Long.valueOf(this.f2017.m2457()));
            this.f2014.m2517(com.adincube.sdk.j.b.b.d.closeLinear);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2544(com.adincube.sdk.a.a.a.e eVar) {
        super.m2494(eVar);
        this.f2054.m3142(m2539(), com.adincube.sdk.j.b.b.d.complete, Long.valueOf(eVar.m2457()));
        this.f2014.m2517(com.adincube.sdk.j.b.b.d.complete);
        if (m2539().m3180("c") == null) {
            m2376();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2545(Context context, com.adincube.sdk.g.a.c cVar) {
        super.m2498(context, cVar);
        this.f2012.f2032.add(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2546(Bundle bundle) {
        super.m2499(bundle);
        bundle.putBoolean("it", this.f2054.f2523);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2547(View view) {
        view.playSoundEffect(0);
        if (this.f1956.m3617() && this.f1956.m3619()) {
            this.f1956.m3618();
            a aVar = this.f2054;
            com.adincube.sdk.g.a.a.a aVar2 = this.f2056;
            aVar.m3143("Click", aVar2.m3175("ct"), aVar2, Long.valueOf(this.f2017.m2457()));
            try {
                this.f1964.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(m2539().m3180("c"))));
            } catch (Exception e) {
            }
            this.f1956.m3616();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2548(com.adincube.sdk.a.a.a.e eVar, g gVar) {
        super.m2501(eVar, gVar);
        if (gVar.f1995 != h.COMPLETED && ((double) gVar.f1997) < this.f1962.ˆ * ((double) com.adincube.sdk.util.g.b.m3640(this.f2056))) {
            this.f2054.m3141(this.f2056, com.adincube.sdk.j.a.a.PROBLEM_DISPLAYING_MEDIA_FILE, Long.valueOf(eVar.m2457()));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2549(com.adincube.sdk.g.a.c cVar, Boolean bool) {
        a aVar = this.f2054;
        com.adincube.sdk.g.a.a.a r1 = m2539();
        if (!aVar.f2523) {
            aVar.f2523 = true;
            aVar.m3143("Impression", r1.m3175("i"), r1, (Long) null);
        }
        super.m2502(cVar, bool);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2550(boolean z, boolean z2) {
        if (!z2) {
            com.adincube.sdk.j.b.b.d dVar = com.adincube.sdk.j.b.b.d.mute;
            if (!z) {
                dVar = com.adincube.sdk.j.b.b.d.unmute;
            }
            this.f2054.m3142(this.f2056, dVar, Long.valueOf(this.f2017.m2457()));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2551(Context context, Bundle bundle) {
        boolean z;
        boolean z2;
        Long l;
        boolean r8 = super.m2504(context, bundle);
        this.f2018.f2039.add(this);
        if (bundle != null) {
            l = Long.valueOf(bundle.getLong(TtmlNode.TAG_P));
            z2 = bundle.getBoolean("it");
            z = bundle.getBoolean("c");
        } else {
            z = false;
            z2 = false;
            l = null;
        }
        this.f2054 = new a(z2, z);
        try {
            this.f2055 = new com.adincube.sdk.j.c(this.f2017, this.f2014, this.f2054, m2539(), l);
            return r8;
        } catch (com.adincube.sdk.j.a.b e) {
            this.f2054.m3141(m2539(), e.a, (Long) null);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public final Uri m2552() {
        return com.adincube.sdk.j.d.a.m3398(m2539());
    }

    /* access modifiers changed from: protected */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public final long m2553() {
        return com.adincube.sdk.util.g.b.m3640(this.f2056);
    }
}
