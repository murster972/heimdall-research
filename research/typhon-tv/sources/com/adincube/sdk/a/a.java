package com.adincube.sdk.a;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import com.adincube.sdk.e.a;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.mediation.v.f;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.f.b;
import com.adincube.sdk.util.o;

public abstract class a {

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f1952;

    /* renamed from: ʼ  reason: contains not printable characters */
    public com.adincube.sdk.e.a f1953;

    /* renamed from: ʽ  reason: contains not printable characters */
    public int f1954 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected Long f1955 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected com.adincube.sdk.util.f.b f1956 = new com.adincube.sdk.util.f.b();

    /* renamed from: ˑ  reason: contains not printable characters */
    public c f1957 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    protected b f1958 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected C0001a f1959 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    protected com.adincube.sdk.f.e.a f1960;

    /* renamed from: 靐  reason: contains not printable characters */
    public com.adincube.sdk.g.a.c f1961;

    /* renamed from: 麤  reason: contains not printable characters */
    public f f1962;

    /* renamed from: 齉  reason: contains not printable characters */
    public com.adincube.sdk.g.b.b f1963 = com.adincube.sdk.f.a.m2607().m2610(true, true);

    /* renamed from: 龘  reason: contains not printable characters */
    protected Activity f1964;

    /* renamed from: com.adincube.sdk.a.a$a  reason: collision with other inner class name */
    public interface C0001a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2389(Throwable th);
    }

    public interface b {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2482();
    }

    public interface c {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2507(com.adincube.sdk.g.a.c cVar, Boolean bool);
    }

    /* access modifiers changed from: package-private */
    public final void o_() {
        try {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(350);
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                public final void onAnimationEnd(Animation animation) {
                    try {
                        a.this.m2368();
                        a.this.m2369();
                    } catch (Throwable th) {
                        ErrorReportingHelper.m3503("AdDisplayer.hideAd.onAnimationEnd", (e) a.this.f1961, th);
                        com.adincube.sdk.util.a.m3513("AdDisplayer#AnimationListener.onAnimationEnd() Exception : ", th);
                    }
                }

                public final void onAnimationRepeat(Animation animation) {
                }

                public final void onAnimationStart(Animation animation) {
                }
            });
            this.f1953.startAnimation(alphaAnimation);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AdDisplayer.animateHideAd", th);
            ErrorReportingHelper.m3506("AdDisplayer.animateHideAd", com.adincube.sdk.g.c.b.INTERSTITIAL, th);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m2368() {
        this.f1958.m2482();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m2369() {
        this.f1964.finish();
        this.f1964.overridePendingTransition(0, 0);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m2370() {
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public abstract ViewGroup.LayoutParams m2371();

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m2372() {
    }

    @SuppressLint({"NewApi"})
    /* renamed from: ٴ  reason: contains not printable characters */
    public com.adincube.sdk.e.a m2373() {
        com.adincube.sdk.e.a aVar = new com.adincube.sdk.e.a(this.f1964);
        com.adincube.sdk.b.a aVar2 = new com.adincube.sdk.b.a(this.f1964);
        aVar2.setBounds(0, 0, aVar2.getIntrinsicWidth(), aVar2.getIntrinsicHeight());
        aVar.f2126 = aVar2;
        aVar.f2126.setCallback(aVar.f2125);
        aVar.m2605(true);
        aVar.f2123 = new a.C0002a() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final void m2388() {
                try {
                    if (a.this.f1955 == null) {
                        a.this.f1955 = Long.valueOf(System.currentTimeMillis());
                        a.this.m2375();
                        a.this.m2376();
                    }
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("AdDisplayer#OnCloseButtonClickListener.onCloseButtonClicked()", th);
                }
            }
        };
        aVar.addView(m2374(), m2371());
        return aVar;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public abstract View m2374();

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m2375() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2376() {
        m2378(true);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m2377(Bundle bundle) {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2378(boolean z) {
        if (this.f1953 == null || !this.f1952 || !z) {
            m2368();
            m2369();
        } else if (Looper.myLooper() == Looper.getMainLooper()) {
            o_();
        } else {
            o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) new Runnable() {
                public final void run() {
                    a.this.o_();
                }
            });
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m2379() {
        if (this.f1953 == null || !this.f1953.f2126.m2556()) {
            return false;
        }
        m2375();
        m2378(true);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public View m2380(Activity activity, Bundle bundle, com.adincube.sdk.g.a.c cVar, f fVar) {
        this.f1964 = activity;
        this.f1961 = cVar;
        this.f1962 = fVar;
        this.f1956.f3036 = cVar;
        this.f1956.m3621(fVar);
        if (bundle != null) {
            m2377(bundle);
        }
        this.f1953 = m2373();
        this.f1953.m2603(this.f1954);
        activity.setContentView(this.f1953, new ViewGroup.LayoutParams(-1, -1));
        if (this.f1952) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(400);
            this.f1953.startAnimation(alphaAnimation);
        }
        return this.f1953;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2381() {
        this.f1956.m3620();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2382(Bundle bundle) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2383(C0001a aVar) {
        this.f1959 = aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2384(b bVar) {
        this.f1958 = bVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2385(com.adincube.sdk.f.e.a aVar) {
        this.f1960 = aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2386(b.a aVar) {
        this.f1956.f3033 = aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2387(boolean z) {
        this.f1952 = z;
    }
}
