package com.adincube.sdk.a;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import com.adincube.sdk.f.e.b.a;
import com.adincube.sdk.h.c;
import com.adincube.sdk.h.e;
import com.adincube.sdk.h.f;
import java.io.IOException;

public final class b extends a implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: ˎ  reason: contains not printable characters */
    private static CountDownTimer f2043 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    private c f2044;

    /* renamed from: ˆ  reason: contains not printable characters */
    private c.d f2045 = new c.d() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2535(String str, String str2) {
            b.this.f2048.m3278(str, str2);
        }
    };

    /* renamed from: ˉ  reason: contains not printable characters */
    private c.C0015c f2046 = new c.C0015c() {
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m2536(Uri uri) {
            b.this.f2049.m3611(uri);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2537() {
            b.this.m2376();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2538(Uri uri) {
            b.this.f2049.m3612(uri);
        }
    };

    /* renamed from: ˊ  reason: contains not printable characters */
    private a f2047 = null;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public e f2048 = null;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public com.adincube.sdk.util.f.a.a f2049 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private com.adincube.sdk.h.b f2050;

    public final void onGlobalLayout() {
        boolean z = true;
        c cVar = this.f2044;
        boolean z2 = (cVar.f2728.x == 0 && cVar.f2728.y == 0) ? false : true;
        this.f2044.m3261();
        c cVar2 = this.f2044;
        if (cVar2.f2730.getVisibility() != 0) {
            z = false;
        }
        cVar2.m3266(z);
        if (!z2) {
            this.f2044.m3262();
            this.f2044.m3265(f.DEFAULT);
            this.f2044.m3260();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m2527() {
        super.m2369();
        this.f2044.m3265(f.HIDDEN);
        c cVar = this.f2044;
        if (cVar.f2730 != null && Build.VERSION.SDK_INT >= 11) {
            cVar.f2730.onPause();
        }
        if (this.f2050 != null) {
            this.f2050.cancel();
        }
        a aVar = this.f2047;
        synchronized (aVar.f2513) {
            for (a.C0009a r0 : aVar.f2513) {
                aVar.m3140(r0);
            }
            aVar.f2513.clear();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m2528() {
        super.m2370();
        AnonymousClass1 r0 = new CountDownTimer() {
            public final void onFinish() {
                b.this.f1957.m2507(b.this.f1961, Boolean.valueOf(b.this.f2048.m3279(true)));
            }

            public final void onTick(long j) {
            }
        };
        f2043 = r0;
        r0.start();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public final ViewGroup.LayoutParams m2529() {
        return new ViewGroup.LayoutParams(-1, -1);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m2530() {
        if (this.f1964 != null) {
            this.f1964.findViewById(16908290).getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final com.adincube.sdk.e.a m2531() {
        com.adincube.sdk.e.a r0 = super.m2373();
        if (!(this.f1961.f2587 == null || this.f1961.f2586 == null)) {
            r0.m2604(Double.valueOf(((double) this.f1961.f2587.intValue()) / ((double) this.f1961.f2586.intValue())));
        }
        return r0;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final View m2532() {
        WebView webView = new WebView(this.f1964);
        this.f2044 = new c(this.f1964, this.f1961, this.f1962);
        this.f2044.f2725 = this.f2046;
        this.f2044.f2723 = this.f2045;
        this.f2044.m3264(webView);
        try {
            this.f2044.m3263();
        } catch (IOException e) {
            this.f1959.m2389(e);
        }
        this.f2047 = new a(this.f1961, this.f1962, this.f1960);
        webView.setOnTouchListener(this.f2047);
        this.f2047.m3139();
        return webView;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m2533(Activity activity, Bundle bundle, com.adincube.sdk.g.a.c cVar, com.adincube.sdk.mediation.v.f fVar) {
        if (f2043 != null) {
            f2043.cancel();
            f2043 = null;
        }
        this.f2048 = new e();
        View r0 = super.m2380(activity, bundle, cVar, fVar);
        activity.findViewById(16908290).getViewTreeObserver().addOnGlobalLayoutListener(this);
        this.f2049 = new com.adincube.sdk.util.f.a.a(activity, fVar, this.f1956, this.f2047);
        this.f1953.m2605(true);
        if (this.f1962.靐 != null && (bundle == null || !bundle.getBoolean("canClose", false))) {
            this.f1953.m2605(false);
            this.f2050 = com.adincube.sdk.h.b.m3258(this.f1962, this.f1953, this.f2048);
            this.f2050.start();
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2534(Bundle bundle) {
        bundle.putBoolean("canClose", this.f2050 == null || this.f2050.f2715);
    }
}
