package com.adincube.sdk.a.a.a;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.view.Surface;
import android.view.SurfaceView;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import java.lang.reflect.InvocationTargetException;

public final class b extends a implements e, ExoPlayer.EventListener, VideoRendererEventListener {

    /* renamed from: 麤  reason: contains not printable characters */
    private Uri f1979 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private SimpleExoPlayer f1980 = null;

    public b(Context context) {
        super(context);
        TrackSelector r0 = m2412();
        SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this.f1978, r0 == null ? new DefaultTrackSelector() : r0, (LoadControl) new DefaultLoadControl());
        newSimpleInstance.addListener(this);
        newSimpleInstance.setVideoDebugListener(this);
        this.f1980 = newSimpleInstance;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m2411() {
        if (this.f1975 == h.PREPARED && this.f1980.getPlaybackState() == 3) {
            m2398();
            if (this.f1980.getPlayWhenReady()) {
                m2399();
            }
        }
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static TrackSelector m2412() {
        try {
            return DefaultTrackSelector.class.getDeclaredConstructor(new Class[]{Handler.class}).newInstance(new Object[]{new Handler(Looper.getMainLooper())});
        } catch (NoSuchMethodException e) {
            return null;
        } catch (InstantiationException e2) {
            return null;
        } catch (IllegalAccessException e3) {
            return null;
        } catch (InvocationTargetException e4) {
            return null;
        }
    }

    public final void onDroppedFrames(int i, long j) {
    }

    public final void onLoadingChanged(boolean z) {
    }

    public final void onPlayerError(ExoPlaybackException exoPlaybackException) {
        try {
            m2409(new g((e) this, this.f1979, (Throwable) exoPlaybackException));
        } catch (Throwable th) {
            a.m3513("ExoPlayer2PlayerController.onPlayerError", th);
            ErrorReportingHelper.m3511("ExoPlayer2PlayerController.onPlayerError", th);
        }
    }

    public final void onPlayerStateChanged(boolean z, int i) {
        if (i == 3) {
            try {
                m2411();
            } catch (Throwable th) {
                a.m3513("ExoPlayer2PlayerController.onPlayerStateChanged", th);
                ErrorReportingHelper.m3511("ExoPlayer2PlayerController.onPlayerStateChanged", th);
                return;
            }
        }
        if (i == 4) {
            m2400();
        }
    }

    public final void onPositionDiscontinuity() {
    }

    public final void onRenderedFirstFrame(Surface surface) {
    }

    public final void onTimelineChanged(Timeline timeline, Object obj) {
    }

    public final void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }

    public final void onVideoDecoderInitialized(String str, long j, long j2) {
    }

    public final void onVideoDisabled(DecoderCounters decoderCounters) {
    }

    public final void onVideoEnabled(DecoderCounters decoderCounters) {
    }

    public final void onVideoInputFormatChanged(Format format) {
        try {
            if (this.f1975 == h.PREPARING && format != null) {
                m2401();
                m2411();
            }
        } catch (Throwable th) {
            a.m3513("ExoPlayer2PlayerController.onVideoInputFormatChanged", th);
            ErrorReportingHelper.m3511("ExoPlayer2PlayerController.onVideoInputFormatChanged", th);
            m2409(new g((e) this, this.f1979, th));
        }
    }

    public final void onVideoSizeChanged(int i, int i2, int i3, float f) {
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m2413() {
        if (!this.f1975.i) {
            this.f1980.setPlayWhenReady(false);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final boolean m2414() {
        return this.f1975 == h.PLAYING && this.f1980.getPlayWhenReady();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final void m2415() {
        if (!this.f1975.i) {
            this.f1980.setPlayWhenReady(true);
            m2399();
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int m2416() {
        if (this.f1975 != h.READY && this.f1975 != h.PLAYING && this.f1975 != h.COMPLETED) {
            return 0;
        }
        Format videoFormat = this.f1980.getVideoFormat();
        return (int) Math.floor((double) (videoFormat.pixelWidthHeightRatio * ((float) videoFormat.width)));
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final int m2417() {
        if (this.f1975 == h.READY || this.f1975 == h.PLAYING || this.f1975 == h.COMPLETED) {
            return this.f1980.getVideoFormat().height;
        }
        return 0;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String m2418() {
        return "ExoPlayer2";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2419() {
        super.m2402();
        this.f1980.removeListener(this);
        this.f1980.release();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2420() {
        super.m2404();
        this.f1980.setPlayWhenReady(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2421(float f) {
        this.f1980.setVolume(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2422(long j) {
        if (!this.f1975.i) {
            this.f1980.seekTo(j);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2423(Uri uri) {
        if (this.f1975 == h.CREATED) {
            m2403();
            this.f1979 = uri;
            this.f1980.prepare(new ExtractorMediaSource(uri, uri.getScheme().equals("file") ? new FileDataSourceFactory() : new DefaultHttpDataSourceFactory(Util.getUserAgent(this.f1978, "AdinCube")), new DefaultExtractorsFactory(), (Handler) null, (ExtractorMediaSource.EventListener) null), true, true);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2424(Surface surface) {
        this.f1980.setVideoSurface(surface);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2425(SurfaceView surfaceView) {
        this.f1980.setVideoSurfaceView(surfaceView);
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final long m2426() {
        if (this.f1975 == h.PLAYING) {
            return this.f1980.getCurrentPosition();
        }
        if (this.f1975 == h.COMPLETED) {
            return this.f1980.getDuration();
        }
        return 0;
    }
}
