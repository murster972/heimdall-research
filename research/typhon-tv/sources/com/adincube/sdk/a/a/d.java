package com.adincube.sdk.a.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import com.adincube.sdk.g.a.c;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.j;
import java.util.HashSet;
import java.util.Set;

@TargetApi(14)
public final class d implements View.OnClickListener, j {

    /* renamed from: 连任  reason: contains not printable characters */
    private SurfaceHolder f2031 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public Set<a> f2032 = new HashSet();

    /* renamed from: 麤  reason: contains not printable characters */
    private SurfaceView f2033 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private c f2034;

    /* renamed from: 龘  reason: contains not printable characters */
    Context f2035;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2521(View view);
    }

    public d(Context context, c cVar) {
        this.f2035 = context;
        this.f2034 = cVar;
    }

    public final void onClick(View view) {
        try {
            if (view == this.f2033) {
                for (a r0 : this.f2032) {
                    r0.m2521(view);
                }
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("VideoPlayerUIContainer.onClick", th);
            ErrorReportingHelper.m3503("VideoPlayerUIContainer.onClick", (e) this.f2034, th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2519() {
        if (this.f2031 != null && this.f2031.getSurface() != null) {
            this.f2031.getSurface().release();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final SurfaceView m2520() {
        if (this.f2033 == null) {
            this.f2033 = new SurfaceView(this.f2035);
            this.f2033.setZOrderMediaOverlay(true);
            this.f2033.setSoundEffectsEnabled(true);
            this.f2033.setOnClickListener(this);
            this.f2031 = this.f2033.getHolder();
            this.f2031.setKeepScreenOn(true);
            this.f2031.setSizeFromLayout();
        }
        return this.f2033;
    }
}
