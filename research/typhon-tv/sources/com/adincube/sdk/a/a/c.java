package com.adincube.sdk.a.a;

import android.os.CountDownTimer;
import com.adincube.sdk.a.a;
import com.adincube.sdk.a.a.a.e;
import com.adincube.sdk.a.a.a.g;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.j.b.b.d;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.g.a;
import com.adincube.sdk.util.j;

public final class c implements e.a, j {

    /* renamed from: ʻ  reason: contains not printable characters */
    private a f2019 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Boolean f2020;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Boolean f2021;

    /* renamed from: ˑ  reason: contains not printable characters */
    private a.c f2022;

    /* renamed from: ٴ  reason: contains not printable characters */
    private a.C0023a f2023 = new a.C0023a() {
        /* renamed from: ˎ  reason: contains not printable characters */
        public final void m2518() {
            c.this.m2514();
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private com.adincube.sdk.util.g.a f2024 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f2025;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f2026;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f2027;

    /* renamed from: 龘  reason: contains not printable characters */
    private com.adincube.sdk.g.a.c f2028;

    private class a extends CountDownTimer {
        public a(long j) {
            super(j, 250);
        }

        public final void onFinish() {
            try {
                c.this.m2514();
            } catch (Throwable th) {
                ErrorReportingHelper.m3506("VideoImpressionManager#VideoImpressionTimer.onFinish", b.INTERSTITIAL, th);
                com.adincube.sdk.util.a.m3513("VideoImpressionManager#VideoImpressionTimer.onFinish", th);
            }
        }

        public final void onTick(long j) {
        }
    }

    public c(e eVar, com.adincube.sdk.util.g.a aVar, com.adincube.sdk.g.a.c cVar, String str, long j, long j2, a.c cVar2) {
        this.f2028 = cVar;
        this.f2025 = str;
        this.f2027 = j;
        this.f2026 = j2;
        this.f2024 = aVar;
        this.f2022 = cVar2;
        if (j > j2) {
            eVar.m2455((e.a) this);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m2508() {
        return !m2510() && !m2509();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean m2509() {
        if (this.f2021 == null) {
            this.f2021 = Boolean.valueOf(com.adincube.sdk.g.e.a.m3246(this.f2025, this.f2027) != null);
        }
        return this.f2021.booleanValue();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m2510() {
        if (this.f2020 == null) {
            this.f2020 = Boolean.valueOf("impression".equals(this.f2025));
        }
        return this.f2020.booleanValue();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2511() {
        if (m2509() && this.f2019 != null) {
            this.f2019.cancel();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2512(e eVar) {
        if (m2509()) {
            if (this.f2019 != null) {
                this.f2019.cancel();
            }
            m2514();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2513() {
        if (m2510()) {
            m2514();
        } else if (m2509()) {
            Long valueOf = Long.valueOf(com.adincube.sdk.g.e.a.m3246(this.f2025, this.f2027).f2698 - this.f2026);
            if (valueOf.longValue() < 0) {
                new StringBuilder("Impression will be send in ").append(valueOf).append("ms");
                this.f2019 = new a(valueOf.longValue());
                this.f2019.start();
            }
        } else if (m2508()) {
            new StringBuilder("Impression will be send on event '").append(this.f2025).append("'.");
            if ("skipoffset".equals(this.f2025)) {
                this.f2024.m3636(this.f2023);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2514() {
        this.f2022.m2507(this.f2028, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2515(e eVar) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2516(e eVar, g gVar) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2517(d dVar) {
        if (m2508() && dVar.w.equals(this.f2025)) {
            m2514();
        }
    }
}
