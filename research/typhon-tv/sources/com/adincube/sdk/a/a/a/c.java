package com.adincube.sdk.a.a.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a;
import java.util.Locale;

@TargetApi(14)
public final class c extends a implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, SurfaceHolder.Callback {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Uri f1981 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f1982 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f1983 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f1984 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Long f1985 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f1986 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f1987 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Float f1988 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private Surface f1989 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private SurfaceHolder f1990 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private MediaPlayer f1991 = null;

    public c(Context context) {
        super(context);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private void m2427() {
        if (this.f1991 != null) {
            try {
                this.f1991.reset();
                this.f1991.release();
            } catch (IllegalStateException e) {
            }
            this.f1991 = null;
        }
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        try {
            m2400();
        } catch (Throwable th) {
            a.m3513("MediaPlayerPlayerController.onCompletion", th);
            ErrorReportingHelper.m3511("MediaPlayerPlayerController.onCompletion", th);
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        try {
            m2409(new g((e) this, this.f1981, String.format(Locale.US, "What: %d - Extra: %d", new Object[]{Integer.valueOf(i), Integer.valueOf(i2)})));
        } catch (Throwable th) {
            a.m3513("MediaPlayerPlayerController.onError", th);
            ErrorReportingHelper.m3511("MediaPlayerPlayerController.onError", th);
        }
        return false;
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        try {
            m2398();
            if (this.f1986) {
                m2439(this.f1989);
            }
            if (this.f1985 != null) {
                m2437(this.f1985.longValue());
            }
            this.f1982 = this.f1991.getVideoWidth();
            this.f1983 = this.f1991.getVideoHeight();
            if (this.f1987) {
                m2430();
            }
        } catch (Throwable th) {
            m2409(new g((e) this, this.f1981, th));
        }
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            this.f1986 = true;
            m2439(surfaceHolder.getSurface());
        } catch (Throwable th) {
            a.m3513("MediaPlayerPlayerController.surfaceCreated", th);
            ErrorReportingHelper.m3511("MediaPlayerPlayerController.surfaceCreated", th);
        }
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        try {
            this.f1986 = false;
            if (this.f1991 != null) {
                m2439((Surface) null);
            }
        } catch (Throwable th) {
            a.m3513("MediaPlayerPlayerController.surfaceDestroyed", th);
            ErrorReportingHelper.m3511("MediaPlayerPlayerController.surfaceDestroyed", th);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m2428() {
        try {
            h hVar = this.f1975;
            if (!hVar.i) {
                if (hVar == h.READY || hVar == h.PREPARING || hVar == h.PREPARED) {
                    this.f1987 = false;
                } else if (this.f1991 != null) {
                    this.f1991.pause();
                    this.f1984 = false;
                }
            }
        } catch (Throwable th) {
            m2409(new g((e) this, this.f1981, th));
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final boolean m2429() {
        if (this.f1975 == h.PLAYING) {
            return this.f1984;
        }
        return false;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final void m2430() {
        try {
            h hVar = this.f1975;
            if (!hVar.i) {
                if (hVar == h.CREATED || hVar == h.PREPARING || hVar == h.PREPARED) {
                    this.f1987 = true;
                } else if (this.f1991 != null) {
                    m2399();
                    this.f1987 = false;
                    this.f1991.start();
                    this.f1984 = true;
                }
            }
        } catch (Throwable th) {
            m2409(new g((e) this, this.f1981, th));
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int m2431() {
        if (this.f1975 == h.READY || this.f1975 == h.PLAYING || this.f1975 == h.COMPLETED) {
            return this.f1982;
        }
        return 0;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final int m2432() {
        if (this.f1975 == h.READY || this.f1975 == h.PLAYING || this.f1975 == h.COMPLETED) {
            return this.f1983;
        }
        return 0;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String m2433() {
        return "MediaPlayer";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2434() {
        super.m2402();
        if (this.f1990 != null) {
            this.f1990.removeCallback(this);
            this.f1990 = null;
        }
        m2427();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2435() {
        super.m2404();
        try {
            m2427();
            this.f1981 = null;
            this.f1987 = false;
            this.f1988 = null;
            this.f1985 = null;
        } catch (Throwable th) {
            m2409(new g((e) this, this.f1981, th));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2436(float f) {
        try {
            if (this.f1991 == null) {
                this.f1988 = Float.valueOf(f);
                return;
            }
            this.f1991.setVolume(f, f);
            this.f1988 = null;
        } catch (Throwable th) {
            m2409(new g((e) this, this.f1981, th));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2437(long j) {
        try {
            h hVar = this.f1975;
            if (!hVar.i) {
                if (hVar == h.CREATED || hVar == h.PREPARING || hVar == h.PREPARED) {
                    this.f1985 = Long.valueOf(j);
                } else if (this.f1991 != null) {
                    this.f1991.seekTo((int) j);
                    this.f1985 = null;
                }
            }
        } catch (Throwable th) {
            m2409(new g((e) this, this.f1981, th));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2438(Uri uri) {
        try {
            if (this.f1975 == h.CREATED) {
                m2403();
                this.f1981 = uri;
                this.f1991 = new MediaPlayer();
                this.f1991.setDataSource(this.f1978, uri);
                this.f1991.setOnPreparedListener(this);
                this.f1991.setOnErrorListener(this);
                this.f1991.setOnCompletionListener(this);
                this.f1991.prepareAsync();
                m2401();
                if (this.f1988 != null) {
                    m2436(this.f1988.floatValue());
                }
            }
        } catch (Throwable th) {
            m2409(new g((e) this, uri, th));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2439(Surface surface) {
        this.f1989 = surface;
        if (this.f1991 != null) {
            this.f1991.setSurface(surface);
            this.f1986 = false;
            return;
        }
        this.f1986 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2440(SurfaceView surfaceView) {
        this.f1990 = surfaceView.getHolder();
        this.f1990.addCallback(this);
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final long m2441() {
        try {
            h hVar = this.f1975;
            if (this.f1991 == null) {
                return 0;
            }
            if (hVar == h.PLAYING || hVar == h.COMPLETED) {
                return (long) this.f1991.getCurrentPosition();
            }
            return 0;
        } catch (Throwable th) {
            a.m3513("MediaPlayerPlayerController.getCurrentPosition", th);
            ErrorReportingHelper.m3511("MediaPlayerPlayerController.getCurrentPosition", th);
            return 0;
        }
    }
}
