package com.adincube.sdk.a.a;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.adincube.sdk.a.a;
import com.adincube.sdk.a.a.a.d;
import com.adincube.sdk.a.a.a.e;
import com.adincube.sdk.a.a.a.g;
import com.adincube.sdk.a.a.a.h;
import com.adincube.sdk.g.a.c;
import com.adincube.sdk.mediation.v.f;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.g.a;

@TargetApi(14)
public abstract class b extends a implements a.c, e.a, a.C0023a {

    /* renamed from: ʿ  reason: contains not printable characters */
    public d f2012 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    private long f2013 = 0;

    /* renamed from: ˊ  reason: contains not printable characters */
    public c f2014 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    public a f2015 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private com.adincube.sdk.util.g.a f2016 = null;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public e f2017 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public e f2018 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m2483() {
        super.m2370();
        this.f2017.m2442();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract com.adincube.sdk.g.a.a.a m2484();

    /* renamed from: ʿ  reason: contains not printable characters */
    public abstract Uri m2485();

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public final ViewGroup.LayoutParams m2486() {
        return new ViewGroup.LayoutParams(-2, -2);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public abstract String m2487();

    /* renamed from: ˋ  reason: contains not printable characters */
    public abstract long m2488();

    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m2489() {
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m2490() {
        this.f2014.m2511();
        this.f2017.m3676();
        this.f2016.m3631();
        this.f2012.m2519();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final com.adincube.sdk.e.a m2491() {
        Double d;
        com.adincube.sdk.e.a r1 = super.m2373();
        com.adincube.sdk.g.a.a.a r0 = m2484();
        if (r0 != null) {
            d = Double.valueOf(((double) com.adincube.sdk.util.g.b.m3638(r0)) / ((double) com.adincube.sdk.util.g.b.m3639(r0)));
        } else {
            d = null;
        }
        r1.m2604(d);
        return r1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final View m2492() {
        return this.f2012.m2520();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2493(Bundle bundle) {
        this.f2013 = bundle.getLong("mp");
        if (bundle.getBoolean("c")) {
            m2378(false);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m2494(e eVar) {
        this.f1953.m2605(true);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2495() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m2496(Activity activity, Bundle bundle, c cVar, f fVar) {
        m2498((Context) activity, cVar);
        View r3 = super.m2380(activity, bundle, cVar, fVar);
        if (m2504((Context) activity, bundle)) {
            boolean z = false;
            boolean z2 = this.f1962.ˎ;
            Long l = null;
            if (bundle != null) {
                z = bundle.getBoolean("c");
                z2 = bundle.getBoolean("m");
                l = Long.valueOf(bundle.getLong(TtmlNode.TAG_P));
            }
            if (!z) {
                this.f2017.m2452(m2485());
                if (l != null) {
                    this.f2017.m2451(l.longValue());
                }
            }
            this.f2018.m2523(z2, true);
        }
        com.adincube.sdk.e.a aVar = this.f1953;
        d dVar = this.f2012;
        e eVar = this.f2018;
        RelativeLayout relativeLayout = new RelativeLayout(dVar.f2035);
        ImageButton r1 = eVar.m2522();
        if (r1 != null) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(10);
            layoutParams.addRule(11);
            relativeLayout.addView(r1, layoutParams);
        }
        aVar.addView(relativeLayout, 1);
        return r3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2497() {
        super.m2381();
        this.f2017.m2444();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2498(Context context, c cVar) {
        this.f2012 = new d(context, cVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2499(Bundle bundle) {
        long r2 = this.f2017.m2457();
        if (r2 > this.f2013) {
            this.f2013 = r2;
        }
        bundle.putBoolean("m", this.f2018.f2042);
        bundle.putBoolean("c", this.f2017.m2449() == h.COMPLETED);
        bundle.putLong(TtmlNode.TAG_P, r2);
        bundle.putLong("mp", this.f2013);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2500(e eVar) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2501(e eVar, g gVar) {
        this.f1953.m2605(true);
        if (this.f1963.f2639) {
            ErrorReportingHelper.m3501(gVar, m2505(), com.adincube.sdk.g.c.b.INTERSTITIAL, (com.adincube.sdk.g.c.a) null, (Boolean) null);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2502(c cVar, Boolean bool) {
        this.f1957.m2507(cVar, bool);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2503(boolean z) {
        this.f1952 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m2504(Context context, Bundle bundle) {
        this.f2017 = new com.adincube.sdk.a.a.a.f(new d(this.f1962)).m2463(context);
        this.f2018 = new e(context, this.f2017);
        this.f2016 = new com.adincube.sdk.util.g.a(this.f2017, this.f1953, m2488());
        this.f2015 = new a(this.f2017, this.f1953, this.f1962.麤);
        this.f2014 = new c(this.f2017, this.f2016, this.f1961, m2487(), m2506(), this.f2013, this);
        this.f2017.m2455((e.a) this);
        this.f2016.m3636((a.C0023a) this);
        this.f2017.m2453(this.f2012.m2520());
        return true;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Uri m2505() {
        return m2485();
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public abstract long m2506();
}
