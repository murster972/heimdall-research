package com.adincube.sdk.a.a.a;

import android.net.Uri;
import com.adincube.sdk.util.m;
import java.util.Locale;

public final class g {

    /* renamed from: 连任  reason: contains not printable characters */
    public String f1994;

    /* renamed from: 靐  reason: contains not printable characters */
    public h f1995;

    /* renamed from: 麤  reason: contains not printable characters */
    public String f1996;

    /* renamed from: 齉  reason: contains not printable characters */
    public long f1997;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f1998;

    private g(e eVar, Uri uri) {
        this.f1998 = eVar.m2447();
        this.f1995 = eVar.m2449();
        this.f1997 = eVar.m2457();
        this.f1996 = uri.toString();
    }

    public g(e eVar, Uri uri, String str) {
        this(eVar, uri);
        this.f1994 = str;
    }

    public g(e eVar, Uri uri, Throwable th) {
        this(eVar, uri);
        this.f1994 = m.m3686(th);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(Locale.US, "Player: %s\n", new Object[]{this.f1998}));
        sb.append(String.format(Locale.US, "State: %s\n", new Object[]{this.f1995.h}));
        sb.append(String.format(Locale.US, "Position: %d\n", new Object[]{Long.valueOf(this.f1997)}));
        if (this.f1996 != null) {
            sb.append(String.format(Locale.US, "Media: %s\n", new Object[]{this.f1996}));
        }
        if (this.f1994 != null) {
            sb.append(this.f1994);
        }
        return sb.toString();
    }
}
