package com.adincube.sdk.a.a;

import android.os.CountDownTimer;
import com.adincube.sdk.a.a.a.e;
import com.adincube.sdk.a.a.a.g;
import com.adincube.sdk.util.j;

public final class a extends CountDownTimer implements e.a, j {

    /* renamed from: 连任  reason: contains not printable characters */
    private Long f1968;

    /* renamed from: 靐  reason: contains not printable characters */
    private e f1969;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f1970 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f1971;

    /* renamed from: 龘  reason: contains not printable characters */
    private com.adincube.sdk.e.a f1972;

    public a(e eVar, com.adincube.sdk.e.a aVar, long j) {
        super(2147483647L, 1000);
        this.f1972 = aVar;
        this.f1969 = eVar;
        this.f1971 = j;
        this.f1969.m2455((e.a) this);
    }

    public final void onFinish() {
    }

    public final void onTick(long j) {
        try {
            if (this.f1969.m2443()) {
                this.f1968 = Long.valueOf(Math.abs((this.f1969.m2457() - 1000) - this.f1970));
                if (m2395()) {
                    new Object[1][0] = this.f1968;
                    this.f1972.m2605(true);
                }
            }
            this.f1970 = this.f1969.m2457();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("StuckMediaPlayerDetector.onTick()", th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2390() {
        cancel();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2391(e eVar) {
        cancel();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2392() {
        start();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2393(e eVar) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2394(e eVar, g gVar) {
        cancel();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2395() {
        return this.f1968 != null && this.f1969.m2443() && this.f1968.longValue() > this.f1971;
    }
}
