package com.adincube.sdk.a.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageButton;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.l;
import java.util.HashSet;
import java.util.Set;

public final class e implements View.OnClickListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Drawable f2036 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Drawable f2037 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private ImageButton f2038 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public Set<a> f2039 = new HashSet();

    /* renamed from: 麤  reason: contains not printable characters */
    private com.adincube.sdk.a.a.a.e f2040;

    /* renamed from: 齉  reason: contains not printable characters */
    private Context f2041;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean f2042 = false;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m2524(boolean z, boolean z2);
    }

    public e(Context context, com.adincube.sdk.a.a.a.e eVar) {
        this.f2041 = context;
        this.f2040 = eVar;
    }

    public final void onClick(View view) {
        try {
            if (view == this.f2038) {
                m2523(!this.f2042, false);
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("VideoPlayerUIContainer.onClick", th);
            ErrorReportingHelper.m3511("VideoPlayerUIContainer.onClick", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ImageButton m2522() {
        if (this.f2038 == null) {
            this.f2038 = new ImageButton(this.f2041);
            this.f2038.setBackgroundDrawable((Drawable) null);
            this.f2038.setOnClickListener(this);
            this.f2038.setAdjustViewBounds(true);
        }
        return this.f2038;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2523(boolean z, boolean z2) {
        float f;
        if (this.f2042 != z || z2) {
            if (this.f2037 == null) {
                Context context = this.f2041;
                byte[] r1 = com.adincube.sdk.util.e.m3577("iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA5VJREFUeNrsmz1oFEEUx3cvJwomnBi0ESSIRgSjiZLCQhCChagg2iSFICJRG7EJiincFCqihXZGxSKKdoIflRYpoogmpyQGMVFO8QMFFT9AxTNk/S/uwRU3M29u9zYzO/Pgx8G8vTdz/9uZnY+3ru/7jjW2ZawEViArkBXICmQFsgJZgQy1LOUi13VnvKGY0Lbg42nS9boazaSDhn4At8EVMBSWWYHKBCq3SXAaDIA/poxBnsS1zeA8eAG6atm3VcHz/xvLL7JB0BR3uzIK3TlHI8bYAMZAZ9q6GFUcyjjTAK6BE8H4moYu5lXoKqxrc2AXGPFpdglkorZRNXF84nc3gjxBpAHg6iiQx/lR1BjB3XEQ/BKIdFw3gTzBD5KN1wImBTE7dRHII3SLauI2ggecmD/AUtUF8oiDa7XxGwQiDaosEFUcP2I988FLTuydsjGTWIvJTgJdzpxtmvD9lWAYzKngewOWgaIqE8U4Zsgly4N1hOvGwRGGbzHYo8pEUaZbUbpYYFPgMKHuOjDKiD8hMzdSTRzqYvUsoQ2bOHV0xC1QkkZtwyFCux8z6rio22q+GjtGGJP6GeVbqeMv9SmW5LajK9GGUdDGaV8OfGbsva8BT9J+qrEabOP4v4N7DB/liZiKY59ugX+IUb7WFIE6QD3HP8IoX2KKQLNAO8dfYJQ3mXSy2szxvecM4MYI1Mjx/WSU15skUBwHkakW6AvHN1fyzkqlQBMc3yJG+TdTBPob7v84ko/z16YIdFfQXdolH/+pE6hf4F8vOYFM1WI1H94hrPbNA58Yi9XWcLGb2jtoCuwX/HldDHE+OsRsNZ0F6hEMzoHtY5TfcmgHAGSBXEn6aizOSXBGcM0WsIrhu0qfTuq1aV8EBwh1Z8E4I/4zlQ4OvRgFug9aifX2cOJ3p/XgkGpBt3oEZlfwvQLLw8mlo8og7SUwJpVsAbjBECewXhlxaj0GJZ28kBNkn90xOf1lIRjmxPxabQZsGhKo2kBBEHOHiSl4deHT6rdAnD4Tkzg3gzFCd72gc5arbBpwkBy1lyhMYOeiZriqIFAlkVjXFYnCTIcxY2mfau9pRHlXo/S02h5n27KKrMy98DNKNlqws7gbvIuzYaoIVC6SrBXCrY/rNdm90/iFuiAX8VS4dTFVq0qzjl72FtwEl8HDJCrUSaAV4HnSlerUxWbE7HvzViArkBXICmQFsgJZgUy1fwIMAL0jl379L/YCAAAAAElFTkSuQmCC".toCharArray());
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(r1, 0, r1.length);
                decodeByteArray.setDensity(480);
                this.f2037 = l.m3685(context, decodeByteArray);
            }
            Drawable drawable = this.f2037;
            if (z) {
                if (this.f2036 == null) {
                    Context context2 = this.f2041;
                    byte[] r12 = com.adincube.sdk.util.e.m3577("iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAONJREFUeNrs3EEOgkAMQFEx3Ft68jEeQKImzLT1/YQdC/ICm2nDNsa46X13BIAAAQIECBAgQAIEqD/QAegc5wGoEE4WoLQ4GYBS46wGSo+zEqgEziqgMjgrgErhzAYqhzMTqCTOLKCyODOASuNcDVQe50qgFjiv9g/v+9v5tAMzQIAAAQIECBAgAQIECBAgQIAA6Qeg7csrvEHnHV2Qrp5qBKDmSLMmqwGoKdLs7Y4A1Axp1YZZAGqCtHrLNQAVR8qyaR+AiiLtiZ7lyAi0+XdHjU8MECBAgAQIECBAgAC16inAAAx7JBiffjKJAAAAAElFTkSuQmCC".toCharArray());
                    Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(r12, 0, r12.length);
                    decodeByteArray2.setDensity(480);
                    this.f2036 = l.m3685(context2, decodeByteArray2);
                }
                drawable = this.f2036;
                f = 0.0f;
            } else {
                f = 1.0f;
            }
            this.f2042 = z;
            m2522().setImageDrawable(drawable);
            for (a r0 : this.f2039) {
                r0.m2524(z, z2);
            }
            this.f2040.m2450(f);
        }
    }
}
