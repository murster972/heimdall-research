package com.adincube.sdk.a.a.a;

public enum h {
    CREATED("CREATED", false),
    PREPARING("PREPARING", false),
    PREPARED("PREPARED", false),
    READY("READY", false),
    PLAYING("PLAYING", false),
    COMPLETED("COMPLETED", true),
    ERROR("ERROR", true);
    
    public String h;
    boolean i;

    private h(String str, boolean z) {
        this.h = str;
        this.i = z;
    }
}
