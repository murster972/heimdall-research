package com.adincube.sdk.a.a.a;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.view.TextureView;
import com.adincube.sdk.a.a.a.e;
import com.adincube.sdk.util.ErrorReportingHelper;
import java.util.HashSet;
import java.util.Set;

public abstract class a implements TextureView.SurfaceTextureListener, e {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f1973 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private Surface f1974 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    h f1975 = h.CREATED;

    /* renamed from: 麤  reason: contains not printable characters */
    private SurfaceTexture f1976 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private Set<e.a> f1977 = new HashSet();

    /* renamed from: 龘  reason: contains not printable characters */
    Context f1978 = null;

    public a(Context context) {
        this.f1978 = context;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m2396(SurfaceTexture surfaceTexture) {
        if (this.f1976 != surfaceTexture) {
            if (this.f1974 != null && this.f1973) {
                this.f1974.release();
            }
            if (surfaceTexture != null) {
                this.f1976 = surfaceTexture;
                this.f1973 = true;
                this.f1974 = new Surface(surfaceTexture);
            } else {
                this.f1976 = null;
                this.f1974 = null;
            }
            m2406(this.f1974);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2397(h hVar) {
        Object[] objArr = {this.f1975.h, hVar.h};
        this.f1975 = hVar;
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        try {
            m2396(surfaceTexture);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AbstractPlayerController.onSurfaceTextureAvailable", th);
            ErrorReportingHelper.m3511("AbstractPlayerController.onSurfaceTextureAvailable", th);
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m2398() {
        if (this.f1975 == h.PREPARED) {
            m2397(h.READY);
            for (e.a r0 : this.f1977) {
                try {
                    r0.m2460(this);
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("AbstractPlayerController.changeStateToReady", th);
                    ErrorReportingHelper.m3511("AbstractPlayerController.changeStateToReady", th);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m2399() {
        if (this.f1975 == h.READY) {
            m2397(h.PLAYING);
            for (e.a r0 : this.f1977) {
                try {
                    r0.m2459();
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("AbstractPlayerController.changeStateToPlaying", th);
                    ErrorReportingHelper.m3511("AbstractPlayerController.changeStateToPlaying", th);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m2400() {
        if (!this.f1975.i) {
            m2397(h.COMPLETED);
            for (e.a r0 : this.f1977) {
                try {
                    r0.m2458(this);
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("AbstractPlayerController.changeStateToCompleted", th);
                    ErrorReportingHelper.m3511("AbstractPlayerController.changeStateToCompleted", th);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final void m2401() {
        if (this.f1975 == h.PREPARING) {
            m2397(h.PREPARED);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m2402() {
        Surface surface = this.f1974;
        if (surface != null && this.f1973) {
            m2406((Surface) null);
            surface.release();
        }
        this.f1977.clear();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m2403() {
        if (this.f1975 == h.CREATED) {
            m2397(h.PREPARING);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m2404() {
        if (this.f1975 != h.ERROR) {
            m2397(h.CREATED);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final h m2405() {
        return this.f1975;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m2406(Surface surface);

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2407(TextureView textureView) {
        if (textureView != null) {
            textureView.setSurfaceTextureListener(this);
            if (textureView.isAvailable()) {
                m2396(textureView.getSurfaceTexture());
                return;
            }
            return;
        }
        m2396((SurfaceTexture) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2408(e.a aVar) {
        this.f1977.add(aVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2409(g gVar) {
        if (!this.f1975.i) {
            com.adincube.sdk.util.a.m3513("Player did fail with error:\n%s", gVar);
            m2397(h.ERROR);
            for (e.a r0 : this.f1977) {
                try {
                    r0.m2461(this, gVar);
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("AbstractPlayerController.changeStateToError", th);
                    ErrorReportingHelper.m3511("AbstractPlayerController.changeStateToError", th);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2410(boolean z) {
        if (this.f1975 == h.PLAYING || this.f1975 == h.COMPLETED) {
            m2397(h.READY);
            m2442();
            m2451(0);
            if (z) {
                m2444();
            }
        }
    }
}
