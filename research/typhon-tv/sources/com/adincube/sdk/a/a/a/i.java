package com.adincube.sdk.a.a.a;

import android.content.Context;
import android.net.Uri;
import android.view.SurfaceView;
import android.view.TextureView;
import com.adincube.sdk.a.a.a.e;
import java.util.HashSet;
import java.util.Set;

public final class i implements e {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f2000 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f2001 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f2002 = 1.0f;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Set<e.a> f2003 = new HashSet();

    /* renamed from: ˑ  reason: contains not printable characters */
    private long f2004 = 0;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f2005 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f2006 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private TextureView f2007 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private e f2008 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private SurfaceView f2009 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private Uri f2010 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private f f2011 = null;

    public i(Context context, f fVar) {
        this.f2011 = fVar;
        m2475(context);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m2464() {
        this.f2005 = false;
        if (this.f2008 != null) {
            this.f2008.m2442();
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final boolean m2465() {
        if (this.f2008 != null) {
            return this.f2008.m2443();
        }
        return false;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final void m2466() {
        this.f2005 = true;
        if (this.f2008 != null) {
            this.f2008.m2444();
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int m2467() {
        if (this.f2000 == 0 && this.f2008 != null) {
            this.f2000 = this.f2008.m2445();
        }
        return this.f2000;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final int m2468() {
        if (this.f2001 == 0 && this.f2008 != null) {
            this.f2001 = this.f2008.m2446();
        }
        return this.f2001;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final String m2469() {
        return this.f2008 == null ? "Proxy" : this.f2008.m2447();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m2470() {
        if (this.f2008 != null) {
            this.f2008.m3676();
        }
        this.f2008 = null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m2471() {
        this.f2010 = null;
        this.f2000 = 0;
        this.f2001 = 0;
        this.f2004 = 0;
        this.f2005 = false;
        if (this.f2008 != null) {
            this.f2008.m2448();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final h m2472() {
        return this.f2008 != null ? this.f2008.m2449() : h.PREPARING;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2473(float f) {
        this.f2002 = f;
        if (this.f2008 != null) {
            this.f2008.m2450(f);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2474(long j) {
        this.f2004 = j;
        if (this.f2008 != null) {
            this.f2008.m2451(j);
            this.f2004 = this.f2008.m2457();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2475(Context context) {
        if (this.f2008 == null) {
            this.f2008 = this.f2011.m2463(context);
            this.f2008.m2450(this.f2002);
            for (e.a r0 : this.f2003) {
                this.f2008.m2455(r0);
            }
            if (this.f2009 != null) {
                this.f2008.m2453(this.f2009);
            }
            if (this.f2007 != null) {
                this.f2008.m2454(this.f2007);
            }
            if (this.f2010 != null) {
                this.f2008.m2452(this.f2010);
                if (this.f2004 > 0) {
                    this.f2008.m2451(this.f2004);
                }
                if (this.f2005) {
                    this.f2008.m2444();
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2476(Uri uri) {
        this.f2010 = uri;
        if (this.f2008 != null) {
            this.f2008.m2452(uri);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2477(SurfaceView surfaceView) {
        this.f2009 = surfaceView;
        this.f2007 = null;
        if (this.f2008 != null) {
            this.f2008.m2453(surfaceView);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2478(TextureView textureView) {
        this.f2009 = null;
        this.f2007 = textureView;
        if (this.f2008 != null) {
            this.f2008.m2454(textureView);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2479(e.a aVar) {
        this.f2003.add(aVar);
        if (this.f2008 != null) {
            this.f2008.m2455(aVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2480(boolean z) {
        this.f2004 = 0;
        this.f2005 = z;
        if (this.f2008 != null) {
            this.f2008.m2456(z);
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final long m2481() {
        if (this.f2008 != null) {
            return this.f2008.m2457();
        }
        return 0;
    }
}
