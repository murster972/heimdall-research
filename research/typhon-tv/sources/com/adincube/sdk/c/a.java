package com.adincube.sdk.c;

import com.adincube.sdk.util.a;
import java.util.ArrayList;
import java.util.List;

public class a extends Exception {
    private List<String> a;

    public a(String str) {
        this(str, (List<String>) null, (Throwable) null);
    }

    public a(String str, Throwable th) {
        this(str, (List<String>) null, th);
    }

    public a(String str, List<String> list) {
        this(str, list, (Throwable) null);
    }

    public a(String str, List<String> list, Throwable th) {
        super(str, th);
        this.a = new ArrayList();
        if (list != null) {
            this.a.addAll(list);
        }
    }

    public void a() {
        a(a.C0019a.d);
    }

    public final void a(a.C0019a aVar) {
        com.adincube.sdk.util.a.m3514(aVar, getMessage(), new Object[0]);
        for (String r0 : this.a) {
            com.adincube.sdk.util.a.m3514(aVar, r0, new Object[0]);
        }
    }
}
