package com.adincube.sdk.c.b;

import java.util.Locale;

public final class b extends a {
    public b(String str, Throwable th) {
        super(String.format(Locale.US, "Cannot read configuration for network '%s'.", new Object[]{str}), th);
    }
}
