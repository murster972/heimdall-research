package com.adincube.sdk.c.b;

import java.util.Locale;

public class c extends a {
    public c(String str, String str2) {
        super(String.format(Locale.US, "Cannot read configuration for network '%s': %s", new Object[]{str, str2}));
    }

    public c(String str, Throwable th) {
        super(String.format(Locale.US, "Cannot read configuration for network '%s'.", new Object[]{str}), th);
    }
}
