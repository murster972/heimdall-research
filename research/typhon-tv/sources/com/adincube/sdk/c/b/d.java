package com.adincube.sdk.c.b;

import java.util.List;
import java.util.Locale;

public final class d extends a {
    public d(String str, List<String> list) {
        super(String.format(Locale.US, "Integration for %s is not complete. Check the following points:", new Object[]{str}), list);
    }
}
