package com.adincube.sdk.c.a;

import java.util.Arrays;
import java.util.List;

public final class i extends f {
    public i() {
        super("INVALID_INTERNET", (List<String>) Arrays.asList(new String[]{"Server request responded HTTP '403 Forbidden'.", "You may be behind a proxy blocking all communications from the SDK.", "Try to open to www.adincube.com on the browser of your device to check if our service is reachable."}));
    }
}
