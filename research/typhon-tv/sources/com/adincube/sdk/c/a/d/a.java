package com.adincube.sdk.c.a.d;

import com.adincube.sdk.c.a.c;
import java.util.Arrays;
import java.util.List;

public final class a extends c {
    public a() {
        super("FETCH_NOT_CALLED", (List<String>) Arrays.asList(new String[]{"Rewarded ads must be fetched manually.", "Use AdinCube.Rewarded.fetch(); to fetch one."}));
    }
}
