package com.adincube.sdk.c.a.a;

public final class c extends com.adincube.sdk.c.a.c {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c(com.adincube.sdk.BannerView r12, com.adincube.sdk.g.c.c r13) {
        /*
            r11 = this;
            r10 = 2
            r9 = 1
            r3 = 0
            java.lang.String r4 = "VIEW_TOO_SMALL"
            com.adincube.sdk.g.c.c r0 = com.adincube.sdk.g.c.c.BANNER_AUTO
            if (r13 == r0) goto L_0x0029
            java.lang.String[] r0 = new java.lang.String[r9]
            java.util.Locale r1 = java.util.Locale.US
            java.lang.String r2 = "Banner view must measure at least %ddp wide."
            java.lang.Object[] r5 = new java.lang.Object[r9]
            int r6 = r13.g
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r5[r3] = r6
            java.lang.String r1 = java.lang.String.format(r1, r2, r5)
            r0[r3] = r1
            java.util.List r0 = java.util.Arrays.asList(r0)
        L_0x0025:
            r11.<init>((java.lang.String) r4, (java.util.List<java.lang.String>) r0)
            return
        L_0x0029:
            android.view.ViewGroup$LayoutParams r0 = r12.getLayoutParams()
            int r1 = r0.width
            r2 = -1
            if (r1 == r2) goto L_0x0046
            java.lang.String[] r0 = new java.lang.String[r9]
            java.util.Locale r1 = java.util.Locale.US
            java.lang.String r2 = "BannerView layout_width must be set to \"match_parent\"."
            java.lang.Object[] r5 = new java.lang.Object[r3]
            java.lang.String r1 = java.lang.String.format(r1, r2, r5)
            r0[r3] = r1
            java.util.List r0 = java.util.Arrays.asList(r0)
            goto L_0x0025
        L_0x0046:
            int r0 = r0.height
            r1 = -2
            if (r0 == r1) goto L_0x005f
            java.lang.String[] r0 = new java.lang.String[r9]
            java.util.Locale r1 = java.util.Locale.US
            java.lang.String r2 = "BannerView layout_height must be set to \"wrap_content\"."
            java.lang.Object[] r5 = new java.lang.Object[r3]
            java.lang.String r1 = java.lang.String.format(r1, r2, r5)
            r0[r3] = r1
            java.util.List r0 = java.util.Arrays.asList(r0)
            goto L_0x0025
        L_0x005f:
            android.content.Context r1 = r12.getContext()
            int r0 = r12.getWidth()
            int r2 = r12.getWidth()
            if (r2 != 0) goto L_0x00d1
            int r0 = r12.getMeasuredWidth()
            r2 = r0
        L_0x0072:
            boolean r0 = r1 instanceof android.app.Activity
            if (r0 != 0) goto L_0x00b0
            android.util.DisplayMetrics r0 = com.adincube.sdk.util.b.j.m3546(r1)
            int r0 = r0.widthPixels
        L_0x007c:
            r5 = 3
            java.lang.String[] r5 = new java.lang.String[r5]
            java.lang.String r6 = "BannerView width must be equals to the screen width."
            r5[r3] = r6
            java.lang.String r6 = "Check that you do not have any padding or margin arround the BannerView."
            r5[r9] = r6
            java.util.Locale r6 = java.util.Locale.US
            java.lang.String r7 = "BannerView width: %ddp, Screen width: %ddp"
            java.lang.Object[] r8 = new java.lang.Object[r10]
            int r2 = com.adincube.sdk.util.b.j.m3545(r1, r2)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r8[r3] = r2
            int r0 = com.adincube.sdk.util.b.j.m3545(r1, r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r8[r9] = r0
            java.lang.String r0 = java.lang.String.format(r6, r7, r8)
            r5[r10] = r0
            java.util.List r0 = java.util.Arrays.asList(r5)
            goto L_0x0025
        L_0x00b0:
            r0 = r1
            android.app.Activity r0 = (android.app.Activity) r0
            r5 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r5 = r0.findViewById(r5)
            if (r5 != 0) goto L_0x00c6
            r0 = r3
        L_0x00bd:
            if (r0 != 0) goto L_0x007c
            android.util.DisplayMetrics r0 = com.adincube.sdk.util.b.j.m3546(r1)
            int r0 = r0.widthPixels
            goto L_0x007c
        L_0x00c6:
            int r0 = r5.getWidth()
            if (r0 != 0) goto L_0x00bd
            int r0 = r5.getMeasuredWidth()
            goto L_0x00bd
        L_0x00d1:
            r2 = r0
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.c.a.a.c.<init>(com.adincube.sdk.BannerView, com.adincube.sdk.g.c.c):void");
    }
}
