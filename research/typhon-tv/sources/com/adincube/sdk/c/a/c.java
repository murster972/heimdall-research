package com.adincube.sdk.c.a;

import com.adincube.sdk.c.a;
import com.adincube.sdk.util.a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class c extends a {
    public String a;

    public c(String str) {
        this(str, (List<String>) null);
    }

    public c(String str, String str2) {
        this(str, (List<String>) Arrays.asList(new String[]{str2}));
    }

    public c(String str, String str2, Throwable th) {
        this(str, (List<String>) Arrays.asList(new String[]{str2}), th);
    }

    public c(String str, List<String> list) {
        super(m2558(str, list), m2559(list));
        this.a = str;
    }

    private c(String str, List<String> list, Throwable th) {
        super(m2558(str, list), m2559(list), th);
        this.a = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m2558(String str, List<String> list) {
        String format = String.format(Locale.US, "Following error occurred: %s", new Object[]{str});
        return (list == null || list.isEmpty()) ? format : format + ": " + list.get(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<String> m2559(List<String> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 1; i < list.size(); i++) {
            arrayList.add(list.get(i));
        }
        return arrayList;
    }

    public a.C0019a b() {
        return a.C0019a.c;
    }
}
