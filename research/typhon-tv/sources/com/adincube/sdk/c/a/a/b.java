package com.adincube.sdk.c.a.a;

import com.adincube.sdk.c.a.c;
import java.util.ArrayList;
import java.util.List;

public final class b extends c {
    public b() {
        super("UNSUPPORTED_BANNER_SIZE", m2557());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<String> m2557() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("Banner view height must be one the following value: ");
        arrayList.add("\t- wrap_content");
        for (Integer intValue : com.adincube.sdk.g.c.c.b()) {
            arrayList.add("\t- " + intValue.intValue() + "dp");
        }
        return arrayList;
    }
}
