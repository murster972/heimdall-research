package com.adincube.sdk.c.a.d;

import com.adincube.sdk.c.a.c;

public final class b extends c {
    public b() {
        super("SHOW_CALLED_TOO_EARLY", "Wait before calling AdinCube.Rewarded.show() again.");
    }
}
