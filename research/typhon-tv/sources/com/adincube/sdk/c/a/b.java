package com.adincube.sdk.c.a;

import com.adincube.sdk.mediation.i;
import java.util.Locale;

public final class b extends c {
    public b(i iVar) {
        super("AD_NETWORK_ERROR", String.format(Locale.US, "Ad network %s failed to show with error code: %s", new Object[]{iVar.f2952.m3473(), iVar.m3485()}));
    }
}
