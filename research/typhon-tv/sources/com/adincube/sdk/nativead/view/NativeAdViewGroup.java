package com.adincube.sdk.nativead.view;

import android.widget.FrameLayout;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.nativead.pool.NativeAdWrapper;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a;

public class NativeAdViewGroup extends FrameLayout {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f2963;

    /* renamed from: 齉  reason: contains not printable characters */
    private NativeAdWrapper.EventListener f2964;

    /* renamed from: 龘  reason: contains not printable characters */
    private NativeAdWrapper f2965;

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f2963) {
            m3500();
        }
    }

    public void setAutoDestroyOnDetach(boolean z) {
        this.f2963 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3500() {
        try {
            if (!this.f2965.m3497()) {
                this.f2965.m3499(this.f2964);
                this.f2965.m3498();
            }
        } catch (Throwable th) {
            a.m3513("NativeAdViewGroup.dismiss", th);
            ErrorReportingHelper.m3506("NativeAdViewGroup.dismiss", b.NATIVE, th);
        }
    }
}
