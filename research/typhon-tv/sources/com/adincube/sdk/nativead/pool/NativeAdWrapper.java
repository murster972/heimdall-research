package com.adincube.sdk.nativead.pool;

import android.view.ViewGroup;
import com.adincube.sdk.AdinCube;
import com.adincube.sdk.NativeAd;
import java.util.Set;

public class NativeAdWrapper {

    /* renamed from: 靐  reason: contains not printable characters */
    private NativeAd f2959;

    /* renamed from: 麤  reason: contains not printable characters */
    private ViewGroup f2960;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f2961;

    /* renamed from: 龘  reason: contains not printable characters */
    Set<EventListener> f2962;

    public interface EventListener {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m3497() {
        return this.f2961;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3498() {
        if (!this.f2961) {
            this.f2961 = true;
            this.f2960 = null;
            synchronized (this.f2962) {
                this.f2962.clear();
            }
            AdinCube.Native.m2313(this.f2959);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m3499(EventListener eventListener) {
        synchronized (this.f2962) {
            this.f2962.remove(eventListener);
        }
    }
}
