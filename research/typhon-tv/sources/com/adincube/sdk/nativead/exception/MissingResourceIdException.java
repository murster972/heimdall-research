package com.adincube.sdk.nativead.exception;

public class MissingResourceIdException extends NativeAdBindingException {
    public MissingResourceIdException(String str) {
        super(String.format("A resource id must be provided for '%s'", new Object[]{str}));
    }
}
