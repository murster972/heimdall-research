package com.adincube.sdk.nativead.exception;

public class NativeAdBindingException extends IllegalStateException {
    public NativeAdBindingException() {
    }

    public NativeAdBindingException(String str) {
        super(str);
    }

    public NativeAdBindingException(String str, Throwable th) {
        super(str, th);
    }

    public NativeAdBindingException(Throwable th) {
        super(th);
    }
}
