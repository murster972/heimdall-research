package com.adincube.sdk.nativead.exception;

public class InvalidLayoutIdException extends NativeAdBindingException {
    public InvalidLayoutIdException() {
        super("Provided resource id ");
    }
}
