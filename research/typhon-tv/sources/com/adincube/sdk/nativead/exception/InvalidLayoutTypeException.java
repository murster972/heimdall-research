package com.adincube.sdk.nativead.exception;

public class InvalidLayoutTypeException extends NativeAdBindingException {
    public InvalidLayoutTypeException(int i) {
        super(String.format("Root element of layout with id %x is not a subclass of ViewGroup.", new Object[]{Integer.valueOf(i)}));
    }
}
