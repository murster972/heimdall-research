package com.adincube.sdk.nativead.exception;

public class InvalidResourceIdException extends NativeAdBindingException {
    public InvalidResourceIdException(String str, int i) {
        super(String.format("No view with id %x for '%s' can be found in provided layout.", new Object[]{Integer.valueOf(i), str}));
    }
}
