package com.adincube.sdk.nativead.exception;

public class InvalidLayoutException extends NativeAdBindingException {
    public InvalidLayoutException(String str) {
        super(str);
    }
}
