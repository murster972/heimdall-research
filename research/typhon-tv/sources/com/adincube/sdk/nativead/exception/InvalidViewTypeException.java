package com.adincube.sdk.nativead.exception;

public class InvalidViewTypeException extends NativeAdBindingException {
    public InvalidViewTypeException(String str, int i, Class<?> cls) {
        super(String.format("View with id %x for '%s' in provided layout is not an instance of '%s'", new Object[]{Integer.valueOf(i), str, cls.getSimpleName()}));
    }
}
