package com.adincube.sdk.b;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import com.adincube.sdk.util.b.i;

public final class b extends Drawable {

    /* renamed from: 靐  reason: contains not printable characters */
    private Path f2074;

    /* renamed from: 齉  reason: contains not printable characters */
    private Paint f2075;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f2076;

    public b(Context context) {
        this.f2076 = i.m3540(context, 48);
        Path path = new Path();
        path.lineTo((float) this.f2076, (float) (this.f2076 / 2));
        path.lineTo(0.0f, (float) this.f2076);
        path.lineTo(0.0f, 0.0f);
        this.f2074 = path;
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(10.0f);
        this.f2075 = paint;
    }

    public final void draw(Canvas canvas) {
        canvas.save();
        canvas.translate((float) (getBounds().centerX() - (this.f2076 / 2)), (float) (getBounds().centerY() - (this.f2076 / 2)));
        canvas.drawPath(this.f2074, this.f2075);
        canvas.restore();
    }

    public final int getOpacity() {
        return -3;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
