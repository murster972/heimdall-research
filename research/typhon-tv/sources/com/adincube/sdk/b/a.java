package com.adincube.sdk.b;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.TypedValue;
import com.adincube.sdk.util.b.i;

public final class a extends Drawable {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final float f2057 = ((float) Math.sqrt(2.0d));

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f2058;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f2059;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Integer f2060 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private TextPaint f2061 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f2062;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Paint f2063 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f2064;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f2065 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Paint f2066 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Paint f2067 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f2068;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f2069;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f2070;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f2071;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f2072;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f2073;

    public a(Context context) {
        this.f2069 = i.m3539(context, 17.0f);
        this.f2071 = i.m3539(context, 8.5f);
        this.f2070 = i.m3539(context, 3.0f);
        this.f2058 = i.m3539(context, 3.0f);
        this.f2068 = i.m3539(context, 1.0f);
        this.f2059 = (int) TypedValue.applyDimension(2, 16.0f, context.getResources().getDisplayMetrics());
        this.f2072 = i.m3539(context, 5.3333335f);
        this.f2073 = i.m3539(context, 4.6666665f);
        this.f2062 = i.m3539(context, 5.3333335f);
        this.f2064 = i.m3539(context, 4.6666665f);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-16777216);
        paint.setAlpha(168);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(0.0f);
        this.f2066 = paint;
        Paint paint2 = new Paint();
        paint2.setStyle(Paint.Style.STROKE);
        paint2.setColor(-1);
        paint2.setStrokeWidth((float) this.f2058);
        paint2.setAntiAlias(true);
        this.f2067 = paint2;
        Paint paint3 = new Paint();
        paint3.setStyle(Paint.Style.STROKE);
        paint3.setColor(-1);
        paint3.setStrokeWidth((float) this.f2070);
        paint3.setStrokeCap(Paint.Cap.ROUND);
        paint3.setAntiAlias(true);
        this.f2063 = paint3;
        TextPaint textPaint = new TextPaint();
        textPaint.setColor(-1);
        textPaint.setTypeface(Typeface.DEFAULT);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setAntiAlias(true);
        this.f2061 = textPaint;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2554(String str, TextPaint textPaint, int i, int i2, int i3) {
        if (this.f2065 == null || this.f2065.length() != str.length()) {
            this.f2065 = "9";
            for (int i4 = 1; i4 < str.length(); i4++) {
                this.f2065 += "9";
            }
            Rect rect = new Rect();
            while (true) {
                textPaint.setTextSize((float) i);
                textPaint.getTextBounds(this.f2065, 0, this.f2065.length(), rect);
                i--;
                if (i <= 0) {
                    return;
                }
                if (rect.width() <= i2 && rect.height() <= i3) {
                    return;
                }
            }
        }
    }

    public final void draw(Canvas canvas) {
        canvas.drawCircle(((float) getBounds().width()) / 2.0f, ((float) getBounds().height()) / 2.0f, ((float) getBounds().width()) / 2.0f, this.f2066);
        canvas.drawCircle(((float) getBounds().width()) / 2.0f, ((float) getBounds().height()) / 2.0f, (((float) (getBounds().width() - this.f2058)) / 2.0f) - ((float) this.f2068), this.f2067);
        if (m2556()) {
            float f = (((float) this.f2071) / f2057) * 2.0f;
            canvas.save();
            canvas.translate(((float) getBounds().centerX()) - (f / 2.0f), ((float) getBounds().centerY()) - (f / 2.0f));
            canvas.drawLine(0.0f, 0.0f, f, f, this.f2063);
            canvas.translate(f, 0.0f);
            canvas.drawLine(0.0f, 0.0f, -f, f, this.f2063);
            canvas.restore();
            return;
        }
        String str = "...";
        if (this.f2060 != null) {
            str = this.f2060.toString();
        }
        int intrinsicWidth = (getIntrinsicWidth() - this.f2072) - this.f2062;
        int intrinsicHeight = (getIntrinsicHeight() - this.f2073) - this.f2064;
        Rect rect = new Rect();
        m2554(str, this.f2061, this.f2059, intrinsicWidth, intrinsicHeight);
        float descent = ((this.f2061.descent() - this.f2061.ascent()) / 2.0f) - this.f2061.descent();
        this.f2061.getTextBounds(str, 0, str.length(), rect);
        canvas.translate((float) (this.f2072 + (intrinsicWidth / 2)), descent + ((float) (this.f2073 + (intrinsicHeight / 2))));
        canvas.drawText(str, 0.0f, 0.0f, this.f2061);
    }

    public final int getIntrinsicHeight() {
        return this.f2069 * 2;
    }

    public final int getIntrinsicWidth() {
        return this.f2069 * 2;
    }

    public final int getOpacity() {
        return -3;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m2555(Integer num) {
        if ((this.f2060 == null && num != null) || (this.f2060 != null && !this.f2060.equals(num))) {
            this.f2060 = num;
            invalidateSelf();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m2556() {
        if (getState() == null) {
            return false;
        }
        for (int i : getState()) {
            if (i == 16842910) {
                return true;
            }
        }
        return false;
    }
}
