package com.adincube.sdk.j;

import com.adincube.sdk.g.b.d;
import com.adincube.sdk.j.b.b.f;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public final class a {

    /* renamed from: 靐  reason: contains not printable characters */
    private double f2851;

    /* renamed from: 龘  reason: contains not printable characters */
    private d f2852;

    /* renamed from: com.adincube.sdk.j.a$a  reason: collision with other inner class name */
    private static class C0018a {

        /* renamed from: 靐  reason: contains not printable characters */
        public double f2854;

        /* renamed from: 龘  reason: contains not printable characters */
        public f f2855;

        private C0018a() {
        }

        /* synthetic */ C0018a(byte b) {
            this();
        }
    }

    public a(d dVar, int i, int i2, float f) {
        this.f2852 = dVar;
        this.f2851 = (((double) i) + ((double) i2)) / ((double) f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m3349(double d, double d2, double d3) {
        return (int) Math.floor(((d2 / d) - 1.0d) / d3);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3350(List<C0018a> list) {
        for (C0018a next : list) {
            int r0 = m3349(this.f2851, (double) (next.f2855.m3384().intValue() + next.f2855.m3383().intValue()), this.f2852.f2646);
            if (r0 >= 0) {
                next.f2854 = (((double) r0) * this.f2852.f2648) + next.f2854;
            } else {
                next.f2854 = (((double) (-r0)) * this.f2852.f2647) + next.f2854;
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m3351(List<C0018a> list) {
        for (C0018a next : list) {
            if (next.f2855.m3381() != null) {
                int r0 = m3349((double) this.f2852.f2642, (double) next.f2855.m3381().intValue(), this.f2852.f2643);
                next.f2854 = (((double) Math.abs(r0)) * this.f2852.f2644) + next.f2854;
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3352(List<C0018a> list) {
        for (C0018a next : list) {
            Double d = this.f2852.f2645.get(next.f2855.m3382());
            if (d != null) {
                next.f2854 += d.doubleValue();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<C0018a> m3353(List<f> list) {
        ArrayList arrayList = new ArrayList();
        ListIterator<f> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            f next = listIterator.next();
            if (next.m3382().matches(this.f2852.f2649)) {
                C0018a aVar = new C0018a((byte) 0);
                aVar.f2855 = next;
                aVar.f2854 = 0.0d;
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }
}
