package com.adincube.sdk.j.d;

import com.adincube.sdk.g.a.a.a;
import com.adincube.sdk.g.a.a.b;
import com.adincube.sdk.g.a.a.d;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.j.b.b.f;
import java.net.URL;
import java.util.Locale;

public final class c extends d {
    public c(e eVar, f fVar, URL url) {
        super(eVar, b.MEDIA, url.toString());
        a.m3400((a) this, fVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m3401() {
        int r0 = com.adincube.sdk.util.g.b.m3638(this);
        int r1 = com.adincube.sdk.util.g.b.m3639(this);
        return String.format(Locale.US, "MEDIA-%dx%d", new Object[]{Integer.valueOf(r0), Integer.valueOf(r1)});
    }
}
