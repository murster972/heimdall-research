package com.adincube.sdk.j.d;

import android.net.Uri;
import com.adincube.sdk.j.b.b.d;
import com.adincube.sdk.j.b.b.e;
import com.adincube.sdk.j.b.b.f;
import com.adincube.sdk.j.b.b.i;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public final class a {
    /* renamed from: 靐  reason: contains not printable characters */
    public static List<i> m3397(com.adincube.sdk.g.a.a.a aVar, d dVar) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(aVar.m3180("t"));
            for (int i = 0; i < jSONArray.length(); i++) {
                i r3 = i.m3387(jSONArray.getJSONObject(i));
                if (r3.f2879 == dVar) {
                    arrayList.add(r3);
                }
            }
        } catch (JSONException e) {
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Uri m3398(com.adincube.sdk.g.a.a.a aVar) {
        if (!aVar.m3176()) {
            return Uri.parse(aVar.m3174());
        }
        try {
            return Uri.parse(aVar.m3178().toString());
        } catch (MalformedURLException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<String> m3399(com.adincube.sdk.g.a.a.a aVar, d dVar) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(aVar.m3180("t"));
            for (int i = 0; i < jSONArray.length(); i++) {
                i r3 = i.m3387(jSONArray.getJSONObject(i));
                if (r3.f2879 == dVar) {
                    arrayList.add(r3.f2877);
                }
            }
        } catch (JSONException e) {
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3400(com.adincube.sdk.g.a.a.a aVar, f fVar) {
        e eVar = (e) fVar.f2875;
        com.adincube.sdk.j.b.a.a aVar2 = eVar.f2870;
        aVar.m3181("u", fVar.m3380());
        aVar.m3181("d", Long.toString(eVar.m3378()));
        if (eVar.m3373()) {
            aVar.m3181("so", eVar.m3374());
        }
        aVar.m3181("w", Integer.toString(fVar.m3384().intValue()));
        aVar.m3181("h", Integer.toString(fVar.m3383().intValue()));
        aVar.m3182("e", aVar2.m3360());
        aVar.m3182("i", aVar2.m3363());
        aVar.m3181("c", eVar.m3377());
        aVar.m3182("ct", eVar.m3376());
        JSONArray jSONArray = new JSONArray();
        for (i r0 : eVar.m3371()) {
            try {
                jSONArray.put((Object) r0.m3388());
            } catch (JSONException e) {
            }
        }
        aVar.m3181("t", jSONArray.toString());
    }
}
