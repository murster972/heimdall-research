package com.adincube.sdk.j;

import android.os.CountDownTimer;
import com.adincube.sdk.a.a.a.e;
import com.adincube.sdk.a.a.a.g;
import com.adincube.sdk.a.a.a.h;
import com.adincube.sdk.j.b.b.d;
import com.adincube.sdk.j.b.b.i;
import com.adincube.sdk.util.g.b;
import com.adincube.sdk.util.j;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public final class c extends CountDownTimer implements e.a, j {

    /* renamed from: ʻ  reason: contains not printable characters */
    private List<a> f2880 = new ArrayList();

    /* renamed from: 连任  reason: contains not printable characters */
    private long f2881;

    /* renamed from: 靐  reason: contains not printable characters */
    private e f2882;

    /* renamed from: 麤  reason: contains not printable characters */
    private com.adincube.sdk.f.e.c.a f2883;

    /* renamed from: 齉  reason: contains not printable characters */
    private com.adincube.sdk.a.a.c f2884;

    /* renamed from: 龘  reason: contains not printable characters */
    private com.adincube.sdk.g.a.a.a f2885;

    private class a {

        /* renamed from: 靐  reason: contains not printable characters */
        public d f2887;

        /* renamed from: 齉  reason: contains not printable characters */
        public List<String> f2889;

        /* renamed from: 龘  reason: contains not printable characters */
        public int f2890;

        private a() {
        }

        /* synthetic */ a(c cVar, byte b) {
            this();
        }
    }

    public c(e eVar, com.adincube.sdk.a.a.c cVar, com.adincube.sdk.f.e.c.a aVar, com.adincube.sdk.g.a.a.a aVar2, Long l) {
        super(2147483647L, 1000);
        this.f2882 = eVar;
        this.f2884 = cVar;
        this.f2883 = aVar;
        this.f2885 = aVar2;
        this.f2881 = b.m3640(aVar2);
        m3389(d.firstQuartile, 0.25d);
        m3389(d.midpoint, 0.5d);
        m3389(d.thirdQuartile, 0.75d);
        for (i next : com.adincube.sdk.j.d.a.m3397(aVar2, d.u)) {
            a aVar3 = new a(this, (byte) 0);
            aVar3.f2890 = (int) com.adincube.sdk.g.e.a.m3246(next.f2878.get(VastIconXmlManager.OFFSET), this.f2881).f2698;
            aVar3.f2887 = d.u;
            aVar3.f2889 = Collections.singletonList(next.f2877);
            this.f2880.add(aVar3);
        }
        Collections.sort(this.f2880, new Comparator<a>() {
            public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                return ((a) obj).f2890 - ((a) obj2).f2890;
            }
        });
        if (l != null) {
            Iterator<a> it2 = this.f2880.iterator();
            while (it2.hasNext()) {
                if (((long) it2.next().f2890) <= l.longValue()) {
                    it2.remove();
                }
            }
        }
        eVar.m2455((e.a) this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3389(d dVar, double d) {
        long r0 = b.m3640(this.f2885);
        List<String> r2 = com.adincube.sdk.j.d.a.m3399(this.f2885, dVar);
        if (!r2.isEmpty()) {
            a aVar = new a(this, (byte) 0);
            aVar.f2890 = (int) (((double) r0) * d);
            aVar.f2887 = dVar;
            aVar.f2889 = r2;
            this.f2880.add(aVar);
        }
    }

    public final void onFinish() {
    }

    public final void onTick(long j) {
        try {
            if (this.f2882.m2443()) {
                boolean z = this.f2882.m2449() == h.COMPLETED;
                long r4 = this.f2882.m2457();
                ListIterator<a> listIterator = this.f2880.listIterator();
                while (listIterator.hasNext()) {
                    a next = listIterator.next();
                    if (z || ((long) next.f2890) <= r4) {
                        if (!z) {
                            Object[] objArr = {next.f2887.w, Long.valueOf(r4), Long.valueOf(this.f2881)};
                        } else {
                            new Object[1][0] = next.f2887.w;
                        }
                        this.f2883.m3143("TimeEvent", next.f2889, this.f2885, Long.valueOf(r4));
                        this.f2884.m2517(next.f2887);
                        listIterator.remove();
                    } else {
                        return;
                    }
                }
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("VASTTimeBasedTrackingEventController.onTick()", th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3390() {
        cancel();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3391(e eVar) {
        onTick(0);
        cancel();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3392() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3393(e eVar) {
        start();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3394(e eVar, g gVar) {
        cancel();
    }
}
