package com.adincube.sdk.j.c;

import com.adincube.sdk.c.c.d;
import com.adincube.sdk.i.b;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.oltu.oauth2.common.OAuth;

public final class a extends b {

    /* renamed from: 靐  reason: contains not printable characters */
    private URL f2891;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f2892;

    public a(String str, URL url) {
        this.f2892 = str;
        this.f2891 = url;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3395() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = super.m3299(this.f2891, OAuth.HttpMethod.GET);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode != 404 && responseCode >= 500) {
                throw new d("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3396() {
        return this.f2892 != null ? "VASTTrackingRequest-" + this.f2892 : "VASTTrackingRequest";
    }
}
