package com.adincube.sdk.j.a;

import com.adincube.sdk.j.b.a.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public final class b extends Exception {
    public a a;
    public List<a> b;

    public b(a aVar) {
        this(aVar, (List<? extends a>) new ArrayList(), (String) null);
    }

    public b(a aVar, a aVar2) {
        this(aVar, (List<? extends a>) Collections.singletonList(aVar2), (String) null);
    }

    public b(a aVar, a aVar2, String str) {
        this(aVar, (List<? extends a>) Collections.singletonList(aVar2), str);
    }

    public b(a aVar, String str) {
        this(aVar, (List<? extends a>) new ArrayList(), str);
    }

    public b(a aVar, Throwable th) {
        this(aVar, (List<? extends a>) new ArrayList(), th);
    }

    public b(a aVar, List<? extends a> list) {
        this(aVar, list, (String) null);
    }

    private b(a aVar, List<? extends a> list, String str) {
        super(m3354(aVar, str));
        this.b = new ArrayList();
        this.a = aVar;
        this.b.addAll(list);
    }

    private b(a aVar, List<? extends a> list, Throwable th) {
        super(m3354(aVar, (String) null), th);
        this.b = new ArrayList();
        this.a = aVar;
        this.b.addAll(list);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m3354(a aVar, String str) {
        if (str == null) {
            return String.format(Locale.US, "VAST Error: %d", new Object[]{Integer.valueOf(aVar.k)});
        }
        return String.format(Locale.US, "VAST Error: %d. %s", new Object[]{Integer.valueOf(aVar.k), str});
    }
}
