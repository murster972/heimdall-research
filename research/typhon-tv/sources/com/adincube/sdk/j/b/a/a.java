package com.adincube.sdk.j.b.a;

import android.annotation.SuppressLint;
import com.adincube.sdk.j.b.b.b;
import com.adincube.sdk.util.x;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressLint({"NewApi"})
public abstract class a {

    /* renamed from: 靐  reason: contains not printable characters */
    private List<String> f2863 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private List<b> f2864 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private List<String> f2865 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Node f2866;

    public a(Node node) {
        this.f2866 = node;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract int m3359();

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<String> m3360() {
        if (this.f2863 == null) {
            this.f2863 = x.m3740(this.f2866, "Error");
        }
        return this.f2863;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<b> m3361(int i) {
        ArrayList arrayList = new ArrayList();
        for (b next : m3362()) {
            if (next.m3372() == i) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final List<b> m3362() {
        if (this.f2864 == null) {
            this.f2864 = new ArrayList();
            NodeList r1 = x.m3743(this.f2866, "Creatives/Creative/Linear | Creatives/Creative/CompanionAds | Creatives/Creative/NonLinearAds");
            for (int i = 0; i < r1.getLength(); i++) {
                b r2 = b.m3370(this, r1.item(i));
                if (r2 != null) {
                    this.f2864.add(r2);
                }
            }
        }
        return this.f2864;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final List<String> m3363() {
        if (this.f2865 == null) {
            this.f2865 = x.m3740(this.f2866, "Impression");
        }
        return this.f2865;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Integer m3364() {
        return x.m3736(this.f2866.getParentNode(), "sequence");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3365(int i) {
        return !m3361(i).isEmpty();
    }
}
