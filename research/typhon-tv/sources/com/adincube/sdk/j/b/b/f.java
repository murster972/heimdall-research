package com.adincube.sdk.j.b.b;

import android.annotation.SuppressLint;
import com.adincube.sdk.util.x;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.mopub.mobileads.VastIconXmlManager;
import org.w3c.dom.Node;

@SuppressLint({"NewApi"})
public final class f {

    /* renamed from: 靐  reason: contains not printable characters */
    private Node f2874;

    /* renamed from: 龘  reason: contains not printable characters */
    public b f2875;

    public f(b bVar, Node node) {
        this.f2875 = bVar;
        this.f2874 = node;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m3380() {
        return x.m3741(this.f2874.getTextContent());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Integer m3381() {
        return x.m3736(this.f2874, "bitrate");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m3382() {
        return x.m3737(this.f2874, VastExtensionXmlManager.TYPE);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Integer m3383() {
        return x.m3736(this.f2874, VastIconXmlManager.HEIGHT);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Integer m3384() {
        return x.m3736(this.f2874, VastIconXmlManager.WIDTH);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final g m3385() {
        try {
            return g.a(x.m3737(this.f2874, "delivery"));
        } catch (Throwable th) {
            return g.UNKNOWN;
        }
    }
}
