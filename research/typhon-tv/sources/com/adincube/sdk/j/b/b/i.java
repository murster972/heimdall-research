package com.adincube.sdk.j.b.b;

import android.annotation.SuppressLint;
import com.adincube.sdk.util.x;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Node;

@SuppressLint({"NewApi"})
public final class i {

    /* renamed from: 靐  reason: contains not printable characters */
    public String f2877;

    /* renamed from: 齉  reason: contains not printable characters */
    public Map<String, String> f2878 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    public d f2879 = null;

    private i() {
    }

    i(d dVar, Node node) {
        this.f2879 = dVar;
        this.f2877 = x.m3741(node.getTextContent());
        this.f2878 = x.m3742(node);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static i m3387(JSONObject jSONObject) {
        i iVar = new i();
        iVar.f2879 = d.a(jSONObject.getString("e"));
        iVar.f2877 = jSONObject.getString("u");
        JSONArray jSONArray = jSONObject.getJSONArray("a");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            iVar.f2878.put(jSONObject2.getString("k"), jSONObject2.getString("v"));
        }
        return iVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3388() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("e", (Object) this.f2879.w);
        jSONObject.put("u", (Object) this.f2877);
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry next : this.f2878.entrySet()) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("k", next.getKey());
            jSONObject2.put("v", next.getValue());
            jSONArray.put((Object) jSONObject2);
        }
        jSONObject.put("a", (Object) jSONArray);
        return jSONObject;
    }
}
