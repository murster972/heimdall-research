package com.adincube.sdk.j.b.b;

public enum g {
    PROGRESSIVE("progressive"),
    STREAMING("streaming"),
    UNKNOWN((String) null);
    
    private String d;

    private g(String str) {
        this.d = str;
    }

    public static g a(String str) {
        for (g gVar : (g[]) f2876.clone()) {
            if (gVar.d.equals(str)) {
                return gVar;
            }
        }
        throw new IllegalArgumentException(str + " is not a valid delivery.");
    }
}
