package com.adincube.sdk.j.b.b;

import android.support.v4.app.NotificationCompat;
import com.adincube.sdk.j.b.a.a;
import com.adincube.sdk.util.x;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class b {

    /* renamed from: 靐  reason: contains not printable characters */
    protected Node f2868;

    /* renamed from: 齉  reason: contains not printable characters */
    private List<i> f2869;

    /* renamed from: 龘  reason: contains not printable characters */
    public a f2870;

    protected b(a aVar, Node node) {
        this.f2870 = aVar;
        this.f2868 = node;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m3370(a aVar, Node node) {
        if ("Linear".equals(node.getNodeName())) {
            return new e(aVar, node);
        }
        if ("NonLinear".equals(node.getNodeName())) {
            return new h(aVar, node);
        }
        if ("CompanionAds".equals(node.getNodeName())) {
            return new a(aVar, node);
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<i> m3371() {
        if (this.f2869 == null) {
            this.f2869 = new ArrayList();
            NodeList r2 = x.m3743(this.f2868, "TrackingEvents/Tracking");
            for (int i = 0; i < r2.getLength(); i++) {
                Node item = r2.item(i);
                d a = d.a(x.m3737(item, NotificationCompat.CATEGORY_EVENT));
                i iVar = a == d.v ? null : new i(a, item);
                if (iVar != null) {
                    this.f2869.add(iVar);
                }
            }
        }
        return this.f2869;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m3372();
}
