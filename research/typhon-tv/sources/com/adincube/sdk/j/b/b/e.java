package com.adincube.sdk.j.b.b;

import com.adincube.sdk.j.a.b;
import com.adincube.sdk.j.b.a.a;
import com.adincube.sdk.util.x;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class e extends b {

    /* renamed from: 齉  reason: contains not printable characters */
    private List<f> f2873 = null;

    public e(a aVar, Node node) {
        super(aVar, node);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m3373() {
        return m3374() != null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m3374() {
        String r0 = x.m3737(this.f2868, "skipoffset");
        if (r0 == null || com.adincube.sdk.g.e.a.m3246(r0, 2147483647L) != null) {
            return r0;
        }
        throw new b(com.adincube.sdk.j.a.a.XML_PARSING_ERROR, "Offset '" + r0 + "' cannot be parsed into a valid duration.");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final List<f> m3375() {
        if (this.f2873 == null) {
            this.f2873 = new ArrayList();
            NodeList r1 = x.m3743(this.f2868, "MediaFiles/MediaFile");
            for (int i = 0; i < r1.getLength(); i++) {
                this.f2873.add(new f(this, r1.item(i)));
            }
        }
        return this.f2873;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final List<String> m3376() {
        Node r0 = x.m3738(this.f2868, "VideoClicks");
        return r0 == null ? new ArrayList() : x.m3740(r0, "ClickTracking");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m3377() {
        Node r0 = x.m3738(this.f2868, "VideoClicks");
        if (r0 == null) {
            return null;
        }
        return x.m3739(r0, "ClickThrough");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final long m3378() {
        String r1 = x.m3739(this.f2868, "Duration");
        com.adincube.sdk.g.e.a r0 = r1.contains("%") ? null : com.adincube.sdk.g.e.a.m3244(r1, Long.MAX_VALUE);
        if (r0 != null) {
            return r0.f2698;
        }
        throw new b(com.adincube.sdk.j.a.a.XML_PARSING_ERROR, "Time '" + r1 + "' cannot be parse into a valid duration.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m3379() {
        return c.a;
    }
}
