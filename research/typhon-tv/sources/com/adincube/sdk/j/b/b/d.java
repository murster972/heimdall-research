package com.adincube.sdk.j.b.b;

import android.support.v4.app.NotificationCompat;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum d {
    creativeView("createView", new String[0]),
    start(TtmlNode.START, new String[0]),
    firstQuartile("firstQuartile", new String[0]),
    midpoint("midpoint", new String[0]),
    thirdQuartile("thirdQuartile", new String[0]),
    complete("complete", new String[0]),
    mute("mute", new String[0]),
    unmute("unmute", new String[0]),
    pause("pause", new String[0]),
    rewind("rewind", new String[0]),
    resume("resume", new String[0]),
    fullscreen("fullscreen", new String[0]),
    exitFullscreen("exitFullscreen", new String[0]),
    expand("expand", new String[0]),
    collapse("collapse", new String[0]),
    acceptInvitation("acceptInvitation", new String[0]),
    acceptInvitationLinear("acceptInvitationLinear", new String[0]),
    close("close", new String[0]),
    closeLinear("closeLinear", new String[0]),
    skip("skip", new String[0]),
    u(NotificationCompat.CATEGORY_PROGRESS, VastIconXmlManager.OFFSET),
    v((String) null, new String[0]);
    
    public String w;
    private List<String> x;

    private d(String str, String... strArr) {
        this.x = new ArrayList();
        this.w = str;
        this.x.addAll(Arrays.asList(strArr));
    }

    public static d a(String str) {
        for (d dVar : (d[]) f2872.clone()) {
            if (dVar.w != null && dVar.w.equals(str)) {
                return dVar;
            }
        }
        return v;
    }
}
