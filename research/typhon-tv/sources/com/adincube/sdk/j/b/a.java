package com.adincube.sdk.j.b;

import android.annotation.SuppressLint;
import com.adincube.sdk.j.b.a.c;
import com.adincube.sdk.j.b.a.d;
import com.adincube.sdk.util.x;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressLint({"NewApi"})
public final class a {

    /* renamed from: 靐  reason: contains not printable characters */
    private List<com.adincube.sdk.j.b.a.a> f2861 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private Node f2862;

    public a(Node node) {
        this.f2862 = node;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<com.adincube.sdk.j.b.a.a> m3357() {
        if (this.f2861 == null) {
            this.f2861 = new ArrayList();
            NodeList r2 = x.m3743(this.f2862, "Ad/InLine | Ad/Wrapper");
            for (int i = 0; i < r2.getLength(); i++) {
                Node item = r2.item(i);
                Object cVar = "InLine".equals(item.getNodeName()) ? new c(item) : "Wrapper".equals(item.getNodeName()) ? new d(item) : null;
                if (cVar != null) {
                    this.f2861.add(cVar);
                }
            }
        }
        return this.f2861;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List<com.adincube.sdk.j.b.a.a> m3358(int i) {
        ArrayList arrayList = new ArrayList();
        for (com.adincube.sdk.j.b.a.a next : m3357()) {
            if (next.m3359() == i && next.m3364() == null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
