package com.adincube.sdk;

import android.content.Context;
import android.content.IntentFilter;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.adincube.sdk.AdinCube;
import com.adincube.sdk.f.b.a.a;
import com.adincube.sdk.f.b.a.a.d;
import com.adincube.sdk.f.b.a.a.e;
import com.adincube.sdk.f.b.a.c;
import com.adincube.sdk.f.b.c.m;
import com.adincube.sdk.f.b.g;
import com.adincube.sdk.f.b.h;
import com.adincube.sdk.f.b.k;
import com.adincube.sdk.g.f;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a.b;
import com.adincube.sdk.util.b.j;
import com.adincube.sdk.util.o;

public class BannerView extends LinearLayout {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public Integer f1931 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f1932 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private c f1933 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private com.adincube.sdk.f.b.a.a.c f1934 = new com.adincube.sdk.f.b.a.a.c() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2355(Integer num, boolean z) {
            if (BannerView.this.f1931 == null || z) {
                if (z) {
                    Integer unused = BannerView.this.f1931 = num;
                }
                if (num != null) {
                    BannerView.super.setVisibility(num.intValue());
                }
            }
        }
    };

    /* renamed from: ˈ  reason: contains not printable characters */
    private b.a f1935 = new b.a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m2354(boolean z) {
            try {
                BannerView.this.f1942.m2685(z);
                BannerView.this.f1941.m2685(z);
                BannerView.this.f1939.m2699(z);
            } catch (Throwable th) {
                new Object[1][0] = th;
                ErrorReportingHelper.m3505("BannerView#OnScreenStateChangedListener.onScreenStateChanged", com.adincube.sdk.g.c.b.BANNER, BannerView.this.m2340(), th);
            }
        }
    };

    /* renamed from: ˑ  reason: contains not printable characters */
    private a f1936 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private b f1937 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private DisplayMetrics f1938 = null;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public com.adincube.sdk.f.b.a.a.b f1939 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f1940 = true;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public e f1941 = null;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public d f1942 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private com.adincube.sdk.g.c.c f1943 = null;

    public BannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m2347();
        m2351((com.adincube.sdk.g.c.c) null);
    }

    public BannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m2347();
        m2351((com.adincube.sdk.g.c.c) null);
    }

    public BannerView(Context context, AdinCube.Banner.Size size) {
        super(context);
        if (size == null) {
            throw new IllegalArgumentException("size must not be null.");
        }
        m2347();
        m2351(com.adincube.sdk.g.c.c.a(size));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private f m2339() {
        f fVar = null;
        if (getChildCount() != 0) {
            View childAt = getChildAt(0);
            f fVar2 = new f(childAt.getWidth(), childAt.getHeight());
            if (!(fVar2.f2700 == 0 || fVar2.f2699 == 0)) {
                fVar = fVar2;
            }
        }
        if (fVar == null) {
            fVar = this.f1933.m2744();
        }
        if (this.f1943 != com.adincube.sdk.g.c.c.BANNER_AUTO) {
            return fVar;
        }
        int i = j.m3546(getContext()).widthPixels;
        return (fVar == null || fVar.f2700 >= i) ? fVar : new f(i, fVar.f2699);
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public com.adincube.sdk.g.c.a m2340() {
        if (this.f1943 == null) {
            return null;
        }
        return this.f1943.i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m2343(com.adincube.sdk.g.c.c cVar) {
        this.f1943 = cVar;
        c cVar2 = this.f1933;
        cVar2.f2233 = cVar;
        if (cVar != null) {
            cVar2.f2154.f2444 = cVar.i;
            cVar2.f2160.m3051(cVar.i);
        }
        this.f1942.m2684(cVar);
        this.f1941.m2684(cVar);
        this.f1939.m2684(cVar);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static void m2345() {
        if (!o.m3704()) {
            com.adincube.sdk.util.a.m3513("Update on the UI in background thread is not supported by Android and will lead to unpredictable behavior.", new Object[0]);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m2347() {
        setOrientation(1);
        this.f1934.m2705(8, true);
        this.f1938 = getResources().getDisplayMetrics();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m2351(com.adincube.sdk.g.c.c cVar) {
        com.adincube.sdk.util.f.m3606(getContext());
        com.adincube.sdk.util.e.a.m3583(getContext());
        com.adincube.sdk.f.a r3 = com.adincube.sdk.f.a.m2607();
        com.adincube.sdk.f.a.b r19 = com.adincube.sdk.f.a.b.m2615();
        this.f1937 = new b(getContext(), com.adincube.sdk.g.c.b.BANNER, m2340());
        this.f1937.f2975 = this.f1935;
        com.adincube.sdk.util.e.b r10 = com.adincube.sdk.util.e.b.m3592();
        h r6 = h.m3040();
        com.adincube.sdk.f.b.e.b.c cVar2 = new com.adincube.sdk.f.b.e.b.c();
        com.adincube.sdk.f.b.e eVar = new com.adincube.sdk.f.b.e(com.adincube.sdk.g.c.b.BANNER, r3);
        m r15 = m.m2940();
        com.adincube.sdk.f.b.f r16 = com.adincube.sdk.f.b.f.m3030(com.adincube.sdk.g.c.b.BANNER);
        g r5 = g.m3035();
        com.adincube.sdk.f.g.a r20 = com.adincube.sdk.f.g.a.m3158(getContext());
        com.adincube.sdk.f.b.c cVar3 = new com.adincube.sdk.f.b.c(com.adincube.sdk.g.c.b.BANNER, r3, eVar, r5, r6);
        k r18 = k.m3056();
        this.f1936 = new a(this, r3);
        this.f1933 = new c(getContext(), r3, r10, this.f1936, r6, cVar2, cVar3, r15, r16, r5, r18);
        this.f1942 = new d(this, this.f1934, r3, r19, this.f1936, this.f1933);
        this.f1939 = new com.adincube.sdk.f.b.a.a.b(this, this.f1934, r3, r19, r20, this.f1936, this.f1933);
        this.f1941 = new e(this, this.f1934, r3, r19, this.f1936, this.f1933, this.f1939);
        m2343(cVar);
    }

    public Integer getExpectedHeight() {
        try {
            f r1 = m2339();
            if (r1 != null) {
                return Integer.valueOf(r1.f2699);
            }
            return null;
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerView.getExpectedHeight", th);
            ErrorReportingHelper.m3506("BannerView.getExpectedHeight", com.adincube.sdk.g.c.b.BANNER, th);
            return null;
        }
    }

    public String getExpectedNetwork() {
        try {
            return this.f1933.m2631();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerView.getExpectedNetwork", th);
            ErrorReportingHelper.m3506("BannerView.getExpectedNetwork", com.adincube.sdk.g.c.b.BANNER, th);
            return null;
        }
    }

    public Integer getExpectedWidth() {
        try {
            f r1 = m2339();
            if (r1 != null) {
                return Integer.valueOf(r1.f2700);
            }
            return null;
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerView.getExpectedWidth", th);
            ErrorReportingHelper.m3506("BannerView.getExpectedWidth", com.adincube.sdk.g.c.b.BANNER, th);
            return null;
        }
    }

    public String getNetwork() {
        try {
            String r0 = this.f1939.m2674();
            if (r0 == null) {
                r0 = this.f1941.m2674();
            }
            return r0 == null ? this.f1942.m2674() : r0;
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerView.getNetwork", th);
            ErrorReportingHelper.m3506("BannerView.getNetwork", com.adincube.sdk.g.c.b.BANNER, th);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            m2345();
            if (!this.f1932) {
                getContext().registerReceiver(this.f1937, new IntentFilter("android.intent.action.SCREEN_ON"));
                getContext().registerReceiver(this.f1937, new IntentFilter("android.intent.action.SCREEN_OFF"));
                this.f1932 = true;
            }
            this.f1942.m2714();
            this.f1941.m2678();
            this.f1939.m2697();
        } catch (Throwable th) {
            new Object[1][0] = th;
            ErrorReportingHelper.m3505("BannerView.onAttachedToWindow", com.adincube.sdk.g.c.b.BANNER, m2340(), th);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            m2345();
            if (this.f1932) {
                getContext().unregisterReceiver(this.f1937);
                this.f1932 = false;
            }
        } catch (IllegalArgumentException e) {
            if (e.getMessage() == null || !e.getMessage().startsWith("Receiver not registered")) {
                throw e;
            }
        } catch (Exception e2) {
            com.adincube.sdk.util.a.m3513("BannerView.onDetachedFromWindow", e2);
            ErrorReportingHelper.m3505("BannerView.onDetachedFromWindow", com.adincube.sdk.g.c.b.BANNER, m2340(), (Throwable) e2);
        }
        this.f1942.m2672();
        this.f1941.m2672();
        this.f1939.m2693();
        if (this.f1940) {
            m2352();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        try {
            m2345();
            int size = View.MeasureSpec.getSize(i);
            if (View.MeasureSpec.getMode(i) != 1073741824) {
                if (this.f1943 == com.adincube.sdk.g.c.c.BANNER_AUTO) {
                    setMeasuredDimension(View.MeasureSpec.makeMeasureSpec(size, 1073741824), getMeasuredHeightAndState());
                } else if (getMeasuredWidth() == 0) {
                    setMeasuredDimension(View.MeasureSpec.makeMeasureSpec(Math.min(size, this.f1943.a(this.f1938)), 1073741824), getMeasuredHeightAndState());
                }
            }
            this.f1942.m2675();
            this.f1941.m2722();
            this.f1939.m2675();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerView.onMeasure", th);
            ErrorReportingHelper.m3505("BannerView.onMeasure", com.adincube.sdk.g.c.b.BANNER, m2340(), th);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        try {
            m2345();
            this.f1942.m2712(i);
            this.f1941.m2677(i);
            this.f1939.m2696(i);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerView.onWindowVisibilityChanged", th);
            ErrorReportingHelper.m3505("BannerView.onWindowVisibilityChanged", com.adincube.sdk.g.c.b.BANNER, m2340(), th);
        }
    }

    public void setAutoDestroyOnDetach(boolean z) {
        this.f1940 = z;
    }

    public void setEventListener(AdinCubeBannerEventListener adinCubeBannerEventListener) {
        this.f1936.f2169 = adinCubeBannerEventListener;
    }

    public void setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        super.setLayoutParams(layoutParams);
        try {
            if (this.f1943 == null) {
                if (layoutParams == null) {
                    m2343(com.adincube.sdk.g.c.c.INVALID);
                } else {
                    float f = this.f1938.density;
                    int i = layoutParams.width;
                    int i2 = layoutParams.height;
                    if (i2 > 0) {
                        i2 = (int) (((float) i2) / f);
                    }
                    m2343(com.adincube.sdk.g.c.c.a(i2));
                }
                new Object[1][0] = this.f1943;
                if (this.f1943 == com.adincube.sdk.g.c.c.BANNER_AUTO) {
                    setVisibility(8);
                }
            }
            this.f1942.m2715();
            this.f1941.m2679();
            this.f1939.m2679();
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerView.setLayoutParams", th);
            ErrorReportingHelper.m3505("BannerView.setLayoutParams", com.adincube.sdk.g.c.b.BANNER, m2340(), th);
        }
    }

    public void setVisibility(int i) {
        this.f1934.m2705(Integer.valueOf(i), false);
        this.f1942.m2716(i);
        this.f1941.m2723(i);
        this.f1939.m2698(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m2352() {
        try {
            if (!o.m3704()) {
                o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) new Runnable() {
                    public final void run() {
                        BannerView.this.m2352();
                    }
                });
                return;
            }
            if (getChildCount() > 0) {
                removeAllViews();
            }
            this.f1933.m2733();
            this.f1942.m2673();
            this.f1941.m2673();
            this.f1939.m2694();
        } catch (IllegalArgumentException e) {
            if (e.getMessage() == null || !e.getMessage().startsWith("Receiver not registered: android.webkit.WebViewClassic")) {
                throw e;
            }
        } catch (Throwable th) {
            new Object[1][0] = th;
            ErrorReportingHelper.m3505("BannerView.dismiss", com.adincube.sdk.g.c.b.BANNER, m2340(), th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m2353() {
        this.f1942.m2711();
    }
}
