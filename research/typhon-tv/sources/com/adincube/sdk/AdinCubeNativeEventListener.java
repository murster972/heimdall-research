package com.adincube.sdk;

import java.util.List;

public abstract class AdinCubeNativeEventListener {
    public void onAdClicked(NativeAd nativeAd) {
    }

    public abstract void onAdLoaded(List<NativeAd> list);

    public void onLoadError(String str) {
    }
}
