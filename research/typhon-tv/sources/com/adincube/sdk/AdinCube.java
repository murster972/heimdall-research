package com.adincube.sdk;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.adincube.sdk.NativeAd;
import com.adincube.sdk.d.a;
import com.adincube.sdk.f.b.b.b;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.d;
import java.util.List;

public final class AdinCube {

    public static class Banner {

        public enum Size {
            BANNER_AUTO,
            BANNER_320x50,
            BANNER_728x90,
            BANNER_300x250
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static BannerView m2302(Context context, Size size) {
            if (size != null) {
                return new BannerView(context, size);
            }
            throw new IllegalArgumentException("size must not be null");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m2303(BannerView bannerView, AdinCubeBannerEventListener adinCubeBannerEventListener) {
            if (bannerView == null) {
                throw new IllegalArgumentException("bannerView must not be null");
            }
            bannerView.setEventListener(adinCubeBannerEventListener);
        }
    }

    public static class Interstitial {
        /* renamed from: 靐  reason: contains not printable characters */
        public static void m2304(Activity activity) {
            a.m2561().m2566(activity);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public static boolean m2305(Activity activity) {
            return a.m2561().m2565(activity);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static String m2306() {
            return a.m2561().m2564();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m2307(Activity activity) {
            a.m2561().m2567(activity);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m2308(AdinCubeInterstitialEventListener adinCubeInterstitialEventListener) {
            try {
                b.m2777().f2259 = adinCubeInterstitialEventListener;
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("AdinCube.Interstitial.setEventListener", th);
                ErrorReportingHelper.m3511("AdinCube.Interstitial.setEventListener", th);
            }
        }
    }

    public static class Native {
        /* renamed from: 龘  reason: contains not printable characters */
        public static void m2309(Context context, int i, AdinCubeNativeEventListener adinCubeNativeEventListener) {
            com.adincube.sdk.d.b.m2573().m2583(context, i, true, adinCubeNativeEventListener);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m2310(Context context, AdinCubeNativeEventListener adinCubeNativeEventListener) {
            com.adincube.sdk.d.b.m2573().m2583(context, 1, true, adinCubeNativeEventListener);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m2311(ViewGroup viewGroup, NativeAd nativeAd) {
            com.adincube.sdk.d.b.m2573().m2584(viewGroup, nativeAd);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m2312(ImageView imageView, NativeAd.Image image) {
            com.adincube.sdk.d.b.m2573().m2585(imageView, image);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m2313(NativeAd nativeAd) {
            com.adincube.sdk.d.b.m2573().m2588(nativeAd);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static void m2314(List<NativeAd> list) {
            com.adincube.sdk.d.b r1 = com.adincube.sdk.d.b.m2573();
            try {
                com.adincube.sdk.util.a.m3515("AdinCube.Native.dismiss()", new Object[0]);
                if (list != null) {
                    for (NativeAd r0 : list) {
                        r1.m2582(r0);
                    }
                }
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("AdinCube.Native.dismiss", th);
                ErrorReportingHelper.m3506("AdinCube.Native.dismiss", com.adincube.sdk.g.c.b.NATIVE, th);
            }
        }
    }

    public static class UserInfo {

        public enum Gender {
            MALE,
            FEMALE
        }

        public enum MaritalStatus {
            SINGLE,
            MARRIED
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m2301(String str) {
        try {
            d.m3555(str);
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("AdinCube.setAppKey", th);
            ErrorReportingHelper.m3511("AdinCube.setAppKey", th);
        }
    }
}
