package com.adincube.sdk.i;

import com.adincube.sdk.c.c.a;
import com.adincube.sdk.c.c.d;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.util.w;
import java.net.HttpURLConnection;
import org.json.JSONObject;

public final class c extends a {

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f2820;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public e f2821;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3311() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = m3300(w.m3728(), m3313());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400 && responseCode < 500) {
                throw new a(responseCode);
            } else if (responseCode >= 500) {
                throw new d("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3312() {
        return "Click";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3313() {
        JSONObject r0 = super.m3288();
        r0.put("n", (Object) this.f2820);
        if (this.f2821 != null) {
            r0.put("i", (Object) this.f2821.f2584);
        }
        return r0;
    }
}
