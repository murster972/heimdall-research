package com.adincube.sdk.i;

import android.net.Uri;
import com.adincube.sdk.a.a.a.g;
import com.adincube.sdk.c.c.a;
import com.adincube.sdk.c.c.d;
import com.adincube.sdk.util.w;
import java.net.HttpURLConnection;
import org.json.JSONObject;

public final class k extends a {

    /* renamed from: ٴ  reason: contains not printable characters */
    public g f2849 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Uri f2850 = null;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3346() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = m3300(w.m3729(), m3348());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400 && responseCode < 500) {
                throw new a(responseCode);
            } else if (responseCode >= 500) {
                throw new d("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3347() {
        return "PlayerErrorReport";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3348() {
        JSONObject r0 = super.m3288();
        r0.put("pn", (Object) this.f2849.f1998);
        r0.put("ps", (Object) this.f2849.f1995.h);
        r0.put("cp", this.f2849.f1997);
        if (this.f2850 != null) {
            r0.put("mu", (Object) this.f2850);
        } else {
            r0.put("mu", (Object) this.f2849.f1996);
        }
        r0.put("m", (Object) this.f2849.f1994);
        return r0;
    }
}
