package com.adincube.sdk.i;

import com.adincube.sdk.c.c.a;
import com.adincube.sdk.c.c.d;
import com.adincube.sdk.util.w;
import java.net.HttpURLConnection;
import org.json.JSONObject;

public final class h extends a {

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f2837 = null;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3328() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = m3300(w.m3726(), m3330());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400 && responseCode < 500) {
                throw new a(responseCode);
            } else if (responseCode >= 500) {
                throw new d("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3329() {
        return "LiarNetworkReport";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3330() {
        JSONObject r0 = super.m3288();
        r0.put("n", (Object) this.f2837);
        return r0;
    }
}
