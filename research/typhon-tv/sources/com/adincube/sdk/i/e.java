package com.adincube.sdk.i;

import com.adincube.sdk.c.c.a;
import com.adincube.sdk.c.c.d;
import com.adincube.sdk.util.w;
import java.net.HttpURLConnection;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.json.JSONObject;

public final class e extends a {

    /* renamed from: ʾ  reason: contains not printable characters */
    public String f2824;

    /* renamed from: ʿ  reason: contains not printable characters */
    public String f2825;

    /* renamed from: ˈ  reason: contains not printable characters */
    public com.adincube.sdk.g.a.e f2826;

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f2827;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public String f2828;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3318() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = m3300(w.m3731(), m3320());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400 && responseCode < 500) {
                throw new a(responseCode);
            } else if (responseCode >= 500) {
                throw new d("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3319() {
        return "EventReport";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3320() {
        JSONObject r0 = super.m3288();
        if (this.f2827 != null) {
            r0.put("e", (Object) this.f2827);
        }
        r0.put("erc", (Object) this.f2828);
        if (this.f2826 != null) {
            r0.put("i", (Object) this.f2826.f2584);
        }
        if (this.f2824 != null) {
            r0.put("n", (Object) this.f2824);
        }
        if (!(this.f2825 == null || this.f2825.length() == 0)) {
            r0.put(InternalZipTyphoonApp.READ_MODE, (Object) this.f2825);
        }
        return r0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3321(Throwable th) {
    }
}
