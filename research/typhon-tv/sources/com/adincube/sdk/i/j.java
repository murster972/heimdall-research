package com.adincube.sdk.i;

import android.content.Context;
import android.graphics.Point;
import android.location.Location;
import com.adincube.sdk.f.b.g;
import com.adincube.sdk.g.c.d;
import com.adincube.sdk.i.a.c;
import com.adincube.sdk.i.a.e;
import com.adincube.sdk.i.b;
import com.adincube.sdk.util.b.h;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.k;
import com.adincube.sdk.util.q;
import com.adincube.sdk.util.u;
import com.adincube.sdk.util.w;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class j extends a {

    /* renamed from: ʾ  reason: contains not printable characters */
    public c f2842;

    /* renamed from: ʿ  reason: contains not printable characters */
    public a f2843;

    /* renamed from: ˈ  reason: contains not printable characters */
    public List<g.a> f2844;

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f2845;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Map<String, String> f2846;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public b f2847;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private com.adincube.sdk.f.a.b f2848;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3341(com.adincube.sdk.g.b.b bVar);
    }

    public interface b {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3342();

        /* renamed from: 龘  reason: contains not printable characters */
        void m3343(j jVar);

        /* renamed from: 龘  reason: contains not printable characters */
        void m3344(String str);
    }

    public interface c {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3345(d dVar);
    }

    public j() {
        this.f2848 = null;
        this.f2845 = null;
        this.f2846 = null;
        this.f2844 = null;
        this.f2842 = null;
        this.f2843 = null;
        this.f2847 = null;
        this.f2848 = com.adincube.sdk.f.a.b.m2615();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m3334(int i, String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("c");
            if (jSONObject.has("d")) {
                jSONObject.getString("d");
            }
            if (this.f2847 == null) {
                return true;
            }
            this.f2847.m3344(string);
            return true;
        } catch (JSONException e) {
            if (i != 403) {
                return false;
            }
            if (this.f2847 == null) {
                return true;
            }
            this.f2847.m3342();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3335() {
        HttpURLConnection httpURLConnection = null;
        try {
            com.adincube.sdk.f.a.b bVar = this.f2848;
            if (bVar.f2134 == null) {
                try {
                    bVar.f2133.acquire();
                    bVar.f2133.release();
                } catch (InterruptedException e) {
                }
            }
            m3291();
            httpURLConnection = m3300(w.m3734(), m3340());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 500) {
                if (this.f2842 != null) {
                    this.f2847.m3343(this);
                }
                throw new com.adincube.sdk.c.c.d("Server Error : " + responseCode);
            } else if (!m3292()) {
                String r2 = u.m3717(q.m3709(httpURLConnection));
                if (responseCode == 200) {
                    if (r2 == null || r2.isEmpty()) {
                        throw new com.adincube.sdk.c.c.b();
                    }
                    JSONObject jSONObject = new JSONObject(r2);
                    if (jSONObject.has("c")) {
                        com.adincube.sdk.g.b.b r4 = com.adincube.sdk.g.b.b.m3218(jSONObject.getJSONObject("c"));
                        if (this.f2843 != null) {
                            this.f2843.m3341(r4);
                        }
                    }
                    d r3 = d.m3224(jSONObject);
                    r3.f2659 = this.f2764.booleanValue();
                    if (this.f2842 != null) {
                        this.f2842.m3345(r3);
                    }
                }
                if (responseCode >= 400) {
                    if (!m3334(responseCode, r2)) {
                        throw new com.adincube.sdk.c.c.a(responseCode);
                    }
                }
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            } else if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        } catch (JSONException e2) {
            throw new com.adincube.sdk.c.c.c(e2);
        } catch (com.adincube.sdk.c.b.b e3) {
            throw new com.adincube.sdk.c.c.c(e3);
        } catch (Throwable th) {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final b.C0017b m3336() {
        return b.C0017b.HIGH;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3337() {
        return "Next";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final com.adincube.sdk.g.b.b m3338() {
        return com.adincube.sdk.f.a.m2607().m2610(false, false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final Long m3339() {
        return com.adincube.sdk.f.a.m2607().m2611(false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3340() {
        e eVar;
        com.adincube.sdk.i.a.c cVar;
        JSONObject r3 = super.m3288();
        Context r4 = f.m3605();
        com.adincube.sdk.g.b.b r5 = m3338();
        if (this.f2845 != null) {
            r3.put("arn", (Object) this.f2845);
        }
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry next : this.f2846.entrySet()) {
            String str = (String) next.getValue();
            if (str == null) {
                str = "";
            }
            jSONObject.put((String) next.getKey(), (Object) str);
        }
        r3.put("anv", (Object) jSONObject);
        e.a aVar = new e.a();
        if (aVar.f2806 == null) {
            eVar = null;
        } else {
            eVar = new e();
            eVar.f2805 = aVar.f2806.f2705;
            eVar.f2802 = aVar.f2806.f2702;
            eVar.f2804 = aVar.f2806.m3248();
            Location location = aVar.f2806.f2704;
            if (location != null) {
                eVar.f2803 = Double.valueOf(location.getLatitude());
                eVar.f2801 = Double.valueOf(location.getLongitude());
            }
        }
        if (eVar != null && com.adincube.sdk.g.b.b.m3219(r5, com.adincube.sdk.g.b.a.UserInformation)) {
            JSONObject jSONObject2 = new JSONObject();
            if (eVar.f2805 != null) {
                jSONObject2.put("g", (Object) eVar.f2805.c);
            }
            if (eVar.f2802 != null) {
                jSONObject2.put("ms", (Object) eVar.f2802.c);
            }
            if (eVar.f2804 != null) {
                jSONObject2.put("by", eVar.f2804.intValue());
            }
            if (!(eVar.f2803 == null || eVar.f2801 == null)) {
                jSONObject2.put("uplat", (Object) eVar.f2803);
                jSONObject2.put("uplon", (Object) eVar.f2801);
            }
            r3.put("u", (Object) com.adincube.sdk.util.g.m3624(r5, jSONObject2.toString()));
        }
        new c.a();
        Location r1 = com.adincube.sdk.util.b.e.m3535(r4);
        if (r1 == null) {
            cVar = null;
        } else {
            cVar = new com.adincube.sdk.i.a.c();
            cVar.f2797 = r1.getLatitude();
            cVar.f2794 = r1.getLongitude();
            cVar.f2796 = r1.getTime();
            cVar.f2795 = r1.getAccuracy();
            cVar.f2793 = r1.getProvider();
        }
        if (cVar != null) {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("a", cVar.f2797);
            jSONObject3.put("o", cVar.f2794);
            jSONObject3.put("t", cVar.f2796);
            jSONObject3.put("c", (double) cVar.f2795);
            jSONObject3.put(TtmlNode.TAG_P, (Object) cVar.f2793);
            r3.put("g", (Object) com.adincube.sdk.util.g.m3624(r5, jSONObject3.toString()));
        }
        List<String> r0 = h.m3538(r4);
        if (!r0.isEmpty()) {
            JSONArray jSONArray = new JSONArray();
            for (String put : r0) {
                jSONArray.put((Object) put);
            }
            r3.put(TtmlNode.TAG_P, (Object) com.adincube.sdk.util.g.m3624(r5, jSONArray.toString()));
        }
        if (this.f2844 != null) {
            JSONArray jSONArray2 = new JSONArray();
            for (g.a next2 : this.f2844) {
                JSONObject jSONObject4 = new JSONObject();
                jSONObject4.put("n", (Object) next2.f2436);
                jSONObject4.put("f", (Object) next2.f2435);
                jSONObject4.put("nf", next2.f2434);
                jSONObject4.put("t", next2.f2432);
                jSONArray2.put((Object) jSONObject4);
            }
            r3.put("fh", (Object) jSONArray2);
        }
        Point r02 = k.m3679(r4);
        r3.put("mw", r02.x);
        r3.put("mh", r02.y);
        r3.put("pi", (Object) com.adincube.sdk.util.e.b.m3592().m3597(this.f2765));
        return r3;
    }
}
