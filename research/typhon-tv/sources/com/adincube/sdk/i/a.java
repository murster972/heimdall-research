package com.adincube.sdk.i;

import android.content.Context;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.i.a.a;
import com.adincube.sdk.i.a.b;
import com.adincube.sdk.i.a.d;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.b.d;
import com.adincube.sdk.util.b.j;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.g;
import com.adincube.sdk.util.p;
import com.adincube.sdk.util.w;
import java.util.Locale;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONObject;

public abstract class a extends b {

    /* renamed from: 靐  reason: contains not printable characters */
    public com.adincube.sdk.g.c.a f2763;

    /* renamed from: 齉  reason: contains not printable characters */
    public Boolean f2764;

    /* renamed from: 龘  reason: contains not printable characters */
    public b f2765;

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public com.adincube.sdk.g.b.b m3285() {
        return com.adincube.sdk.f.a.m2607().m2610(true, false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final String m3286() {
        String r0 = m3295();
        if (this.f2765 == null) {
            return r0;
        }
        String str = r0 + " - " + this.f2765.e;
        return this.f2763 != null ? str + " - " + this.f2763.e : str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public Long m3287() {
        return com.adincube.sdk.f.a.m2607().m2611(true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public JSONObject m3288() {
        Integer num = null;
        JSONObject jSONObject = new JSONObject();
        Context r3 = f.m3605();
        new a.C0016a();
        com.adincube.sdk.i.a.a aVar = new com.adincube.sdk.i.a.a();
        aVar.f2768 = r3.getPackageName();
        aVar.f2766 = com.adincube.sdk.util.b.b.m3528(r3);
        aVar.f2767 = com.adincube.sdk.util.b.b.m3526(r3);
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("pa", (Object) aVar.f2768);
        jSONObject2.put("vc", aVar.f2766);
        jSONObject2.put("vn", (Object) aVar.f2767);
        jSONObject.put("a", (Object) jSONObject2);
        new b.a();
        com.adincube.sdk.g.b.b r5 = m3285();
        com.adincube.sdk.i.a.b bVar = new com.adincube.sdk.i.a.b();
        com.adincube.sdk.g.b.b r0 = com.adincube.sdk.f.a.m2607().m2610(true, true);
        String string = (r0 == null || !com.adincube.sdk.g.b.b.m3219(r0, com.adincube.sdk.g.b.a.AndroidId)) ? null : Settings.Secure.getString(r3.getContentResolver(), "android_id");
        if (string != null) {
            bVar.f2790 = g.m3624(r5, string);
        }
        com.adincube.sdk.g.a r02 = com.adincube.sdk.f.a.b.m2615().m2616();
        if (r02 != null) {
            if (r02.f2549 != null) {
                bVar.f2787 = g.m3624(r5, r02.f2549);
            }
            bVar.f2789 = r02.f2547;
        }
        bVar.f2788 = com.adincube.sdk.util.e.a.m3579(r3);
        bVar.f2781 = com.adincube.sdk.util.b.a.m3524();
        bVar.f2784 = Build.VERSION.RELEASE;
        if (d.m3532(r3) != null) {
            bVar.f2786 = g.m3624(r5, d.m3532(r3));
            String r03 = d.m3531(r3);
            bVar.f2769 = g.m3624(r5, r03 == null ? null : p.m3706("SHA-1", r03));
        }
        if (d.m3533(r3) != null) {
            bVar.f2770 = g.m3624(r5, d.m3533(r3));
            String r04 = d.m3530(r3);
            bVar.f2771 = g.m3624(r5, r04 == null ? null : p.m3706("SHA-1", r04));
        }
        bVar.f2785 = j.m3546(r3).widthPixels;
        bVar.f2775 = j.m3546(r3).heightPixels;
        DisplayMetrics r05 = j.m3546(r3);
        bVar.f2772 = (int) Math.max(r05.xdpi, r05.ydpi);
        bVar.f2773 = j.m3546(r3).density;
        NetworkInfo r06 = com.adincube.sdk.util.b.g.m3537(r3);
        bVar.f2791 = r06 == null ? null : Integer.valueOf(r06.getType());
        NetworkInfo r07 = com.adincube.sdk.util.b.g.m3537(r3);
        if (r07 != null) {
            num = Integer.valueOf(r07.getSubtype());
        }
        bVar.f2792 = num;
        bVar.f2777 = ((TelephonyManager) r3.getSystemService("phone")).getNetworkOperator();
        bVar.f2778 = ((TelephonyManager) r3.getSystemService("phone")).getNetworkOperatorName();
        TelephonyManager telephonyManager = (TelephonyManager) r3.getSystemService("phone");
        String simCountryIso = telephonyManager.getSimCountryIso();
        String networkCountryIso = ((simCountryIso == null || simCountryIso.length() == 0) && telephonyManager.getPhoneType() != 2) ? telephonyManager.getNetworkCountryIso() : simCountryIso;
        if (networkCountryIso == null || networkCountryIso.length() == 0) {
            networkCountryIso = r3.getResources().getConfiguration().locale.getCountry();
        }
        if (networkCountryIso != null) {
            networkCountryIso = networkCountryIso.toUpperCase(Locale.US);
        }
        if (networkCountryIso != null && networkCountryIso.length() > 2) {
            networkCountryIso = networkCountryIso.substring(0, 2);
        }
        bVar.f2779 = networkCountryIso;
        String language = r3.getResources().getConfiguration().locale.getLanguage();
        if (language == null) {
            language = Locale.getDefault().getLanguage();
        }
        bVar.f2774 = language;
        bVar.f2776 = Build.MANUFACTURER;
        bVar.f2780 = Build.MODEL;
        bVar.f2782 = Build.PRODUCT;
        bVar.f2783 = Build.CPU_ABI;
        JSONObject jSONObject3 = new JSONObject();
        if (bVar.f2790 != null) {
            jSONObject3.put("a", (Object) bVar.f2790);
        }
        jSONObject3.put("ai", (Object) bVar.f2787);
        jSONObject3.put(PubnativeRequest.Parameters.LAT, bVar.f2789);
        jSONObject3.put("bua", (Object) bVar.f2788);
        jSONObject3.put("s", bVar.f2781);
        jSONObject3.put("v", (Object) bVar.f2784);
        if (bVar.f2786 != null) {
            jSONObject3.put("im", (Object) bVar.f2786);
            jSONObject3.put("is", (Object) bVar.f2769);
        }
        if (bVar.f2770 != null) {
            jSONObject3.put("mm", (Object) bVar.f2770);
            jSONObject3.put("ms", (Object) bVar.f2771);
        }
        jSONObject3.put("sw", bVar.f2785);
        jSONObject3.put("sh", bVar.f2775);
        jSONObject3.put("sd", bVar.f2772);
        jSONObject3.put("sdf", (double) bVar.f2773);
        jSONObject3.put("nt", (Object) bVar.f2791);
        jSONObject3.put("nst", (Object) bVar.f2792);
        jSONObject3.put("no", (Object) bVar.f2777);
        jSONObject3.put("non", (Object) bVar.f2778);
        jSONObject3.put("c", (Object) bVar.f2779);
        jSONObject3.put("l", (Object) bVar.f2774);
        jSONObject3.put("ma", (Object) bVar.f2776);
        jSONObject3.put("mo", (Object) bVar.f2780);
        jSONObject3.put(TtmlNode.TAG_P, (Object) bVar.f2782);
        jSONObject3.put("cp", (Object) bVar.f2783);
        jSONObject.put("d", (Object) jSONObject3);
        new d.a();
        com.adincube.sdk.i.a.d dVar = new com.adincube.sdk.i.a.d();
        dVar.f2800 = com.adincube.sdk.util.d.m3553();
        dVar.f2798 = "1.23.5";
        dVar.f2799 = "Java";
        JSONObject jSONObject4 = new JSONObject();
        jSONObject4.put("pk", (Object) dVar.f2800);
        jSONObject4.put("v", (Object) dVar.f2798);
        jSONObject4.put("t", (Object) dVar.f2799);
        jSONObject.put("s", (Object) jSONObject4);
        jSONObject.put("lct", (Object) m3287());
        if (this.f2765 != null) {
            jSONObject.put("at", (Object) this.f2765.e);
            if (this.f2763 != null) {
                jSONObject.put("ast", (Object) this.f2763.e);
            }
            jSONObject.put("ar", (Object) this.f2764);
        }
        jSONObject.put("t", System.currentTimeMillis());
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3289(Throwable th) {
        ErrorReportingHelper.m3504("GenericAdinCubeRequest.run", this.f2765, this.f2763, this.f2764, th);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3290(boolean z) {
        if (w.m3735()) {
            w.m3732();
            this.f2811--;
            m3296();
            return;
        }
        super.m3304(z);
    }
}
