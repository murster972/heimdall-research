package com.adincube.sdk.i;

import android.content.Context;
import com.adincube.sdk.i.b;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.n;
import com.adincube.sdk.util.q;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import org.apache.oltu.oauth2.common.OAuth;

public final class d extends b {

    /* renamed from: 靐  reason: contains not printable characters */
    public a f2822 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public com.adincube.sdk.g.a.a.a f2823 = null;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3317(com.adincube.sdk.g.a.a.a aVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3314() {
        Context r2;
        com.adincube.sdk.g.a.a.a aVar;
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = super.m3299(this.f2823.m3178(), OAuth.HttpMethod.GET);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                r2 = f.m3605();
                InputStream r3 = q.m3709(httpURLConnection);
                aVar = this.f2823;
                FileOutputStream openFileOutput = r2.openFileOutput(n.m3696(aVar), 0);
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = r3.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    openFileOutput.write(bArr, 0, read);
                }
                openFileOutput.flush();
                openFileOutput.close();
                r2.getFileStreamPath(n.m3696(aVar)).renameTo(r2.getFileStreamPath(n.m3687(aVar)));
                this.f2822.m3317(this.f2823);
            }
            if (responseCode >= 400 && responseCode < 500) {
                throw new com.adincube.sdk.c.c.a(responseCode);
            } else if (responseCode >= 500) {
                throw new com.adincube.sdk.c.c.d("Server Error : " + responseCode);
            } else if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        } catch (IOException e) {
            com.adincube.sdk.util.a.m3513("writeAdResource() IOException", e);
            r2.getFileStreamPath(n.m3696(aVar)).delete();
            r2.getFileStreamPath(n.m3687(aVar)).delete();
            throw e;
        } catch (Throwable th) {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final b.C0017b m3315() {
        return b.C0017b.LOW;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3316() {
        return "DownloadAdResource";
    }
}
