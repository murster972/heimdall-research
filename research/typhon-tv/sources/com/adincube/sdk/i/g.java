package com.adincube.sdk.i;

import com.adincube.sdk.c.c.a;
import com.adincube.sdk.c.c.d;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.util.e.b;
import com.adincube.sdk.util.h;
import com.adincube.sdk.util.w;
import java.net.HttpURLConnection;
import org.json.JSONObject;

public final class g extends a {

    /* renamed from: ʾ  reason: contains not printable characters */
    public Boolean f2831;

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean f2832 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    public JSONObject f2833;

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f2834;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public e f2835;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private long f2836 = h.m3641().getTime();

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3325() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = m3300(w.m3733(), m3327());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400 && responseCode < 500) {
                throw new a(responseCode);
            } else if (responseCode >= 500) {
                throw new d("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3326() {
        return "Impression";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3327() {
        JSONObject r0 = super.m3288();
        r0.put("n", (Object) this.f2834);
        if (this.f2835 != null) {
            r0.put("i", (Object) this.f2835.f2584);
        }
        if (this.f2833 != null) {
            r0.put("ndc", (Object) this.f2833);
        }
        r0.put("b", (Object) this.f2831);
        if (this.f2832) {
            r0.put("f", this.f2832);
        }
        r0.put("ct", this.f2836);
        r0.put("pi", (Object) b.m3592().m3597(this.f2765));
        return r0;
    }
}
