package com.adincube.sdk.i;

import android.content.Context;
import com.adincube.sdk.c.c.c;
import com.adincube.sdk.c.c.d;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.q;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.zip.GZIPOutputStream;
import javax.net.ssl.SSLHandshakeException;
import org.apache.oltu.oauth2.common.OAuth;
import org.json.JSONObject;

public abstract class b implements Runnable {

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f2807 = 15000;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f2808 = 15000;

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean f2809 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    protected a f2810;

    /* renamed from: 连任  reason: contains not printable characters */
    protected int f2811 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f2812 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f2813 = 10;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3305(b bVar);
    }

    /* renamed from: com.adincube.sdk.i.b$b  reason: collision with other inner class name */
    public enum C0017b {
        LOW,
        DEFAULT,
        HIGH;

        public static C0017b[] a() {
            return (C0017b[]) f2815.clone();
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    protected static void m3291() {
        Context r2 = f.m3605();
        long j = 0;
        while (com.adincube.sdk.util.e.a.m3579(r2) == null && j < 1000) {
            try {
                Thread.sleep(10);
                j += 10;
            } catch (InterruptedException e) {
            }
        }
    }

    public void run() {
        boolean z;
        boolean z2;
        if (m3292()) {
            com.adincube.sdk.util.a.m3515("Request Canceled : " + m3298(), new Object[0]);
            return;
        }
        try {
            com.adincube.sdk.util.a.m3515("LaunchRequest : " + m3298(), new Object[0]);
            if (this.f2812) {
                m3291();
            }
            m3293();
            if (!m3292()) {
                com.adincube.sdk.util.a.m3515("Request Ok : " + m3298(), new Object[0]);
            } else {
                com.adincube.sdk.util.a.m3515("Request Canceled : " + m3298(), new Object[0]);
            }
            z = false;
            z2 = true;
        } catch (d e) {
            com.adincube.sdk.util.a.m3513("ServerException : " + m3298() + " - " + e.getMessage(), new Object[0]);
            z = true;
            z2 = false;
        } catch (com.adincube.sdk.c.c.a e2) {
            com.adincube.sdk.util.a.m3513("Unhandled client error : " + m3298() + " - " + e2.getMessage(), new Object[0]);
            z = false;
            z2 = false;
        } catch (com.adincube.sdk.c.c.b e3) {
            com.adincube.sdk.util.a.m3513("Unhandled error: Empty response received from server.", new Object[0]);
            z = true;
            z2 = false;
        } catch (c e4) {
            com.adincube.sdk.util.a.m3513("Unhandled error: Cannot parse response received from server.", new Object[0]);
            z = true;
            z2 = false;
        } catch (UnknownHostException e5) {
            com.adincube.sdk.util.a.m3513("No network available (UnknownHostException) : " + m3298(), new Object[0]);
            z = false;
            z2 = false;
        } catch (ConnectException e6) {
            com.adincube.sdk.util.a.m3513("No network available (ConnectException) : " + m3298(), new Object[0]);
            z = false;
            z2 = false;
        } catch (SocketTimeoutException e7) {
            com.adincube.sdk.util.a.m3513("No network available (SocketTimeoutException) : " + m3298(), new Object[0]);
            z = false;
            z2 = false;
        } catch (SSLHandshakeException e8) {
            com.adincube.sdk.util.a.m3515("Error with HTTPS, fallback to HTTP: " + m3298(), new Object[0]);
            z = false;
            z2 = false;
        } catch (IOException e9) {
            com.adincube.sdk.util.a.m3513("IOException in Request : " + m3298(), e9);
            z = false;
            z2 = false;
        } catch (Throwable th) {
            m3303(th);
            com.adincube.sdk.util.a.m3513("Throwable thrown in Request : " + m3298(), th);
            z = false;
            z2 = false;
        }
        if (z2) {
            return;
        }
        if (m3292()) {
            com.adincube.sdk.util.a.m3515("Request canceled before #" + (this.f2811 + 1) + " try: " + m3298(), new Object[0]);
        } else {
            m3304(z);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m3292() {
        boolean z;
        synchronized (this) {
            z = this.f2809;
        }
        return z;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract void m3293();

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public C0017b m3294() {
        return C0017b.DEFAULT;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public abstract String m3295();

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final void m3296() {
        this.f2811++;
        try {
            com.adincube.sdk.f.c.a.m3062().m3063(m3294()).execute(this);
        } catch (RejectedExecutionException e) {
            ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
            newSingleThreadExecutor.execute(this);
            newSingleThreadExecutor.shutdown();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final int m3297() {
        return this.f2811;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m3298() {
        return m3295();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final HttpURLConnection m3299(URL url, String str) {
        String r1;
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setConnectTimeout(this.f2807);
        httpURLConnection.setReadTimeout(this.f2808);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
        httpURLConnection.setDoInput(true);
        httpURLConnection.setRequestMethod(str);
        if (this.f2812 && (r1 = com.adincube.sdk.util.e.a.m3579(f.m3605())) != null && r1.length() > 0) {
            httpURLConnection.setRequestProperty(AbstractSpiCall.HEADER_USER_AGENT, r1);
        }
        return httpURLConnection;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final HttpURLConnection m3300(URL url, JSONObject jSONObject) {
        String jSONObject2 = jSONObject.toString();
        HttpURLConnection r1 = m3299(url, OAuth.HttpMethod.POST);
        r1.setRequestProperty(OAuth.HeaderType.CONTENT_TYPE, "application/json; charset=utf-8");
        r1.setRequestProperty("Content-Encoding", "gzip");
        r1.setDoOutput(true);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new GZIPOutputStream(r1.getOutputStream()));
        bufferedOutputStream.write(jSONObject2.getBytes("UTF-8"));
        bufferedOutputStream.flush();
        bufferedOutputStream.close();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3301(int i) {
        this.f2813 = Math.max(1, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3302(a aVar) {
        this.f2810 = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3303(Throwable th) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3304(boolean z) {
        try {
            if (this.f2811 < this.f2813) {
                int r0 = q.m3708(this.f2811, z);
                com.adincube.sdk.util.a.m3515("Request sleeps " + r0 + "ms before #" + (this.f2811 + 1) + " try: " + m3298(), new Object[0]);
                Thread.sleep((long) r0);
                m3296();
            } else if (this.f2810 != null) {
                this.f2810.m3305(this);
            }
        } catch (InterruptedException e) {
        }
    }
}
