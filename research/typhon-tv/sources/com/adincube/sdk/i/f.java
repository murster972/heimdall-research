package com.adincube.sdk.i;

import com.adincube.sdk.c.c.a;
import com.adincube.sdk.c.c.d;
import com.adincube.sdk.g.b.c;
import com.adincube.sdk.util.w;
import java.net.HttpURLConnection;
import org.json.JSONObject;

public final class f extends a {

    /* renamed from: ٴ  reason: contains not printable characters */
    public c f2829;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f2830 = false;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3322() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = m3300(w.m3725(), m3324());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400 && responseCode < 500) {
                throw new a(responseCode);
            } else if (responseCode >= 500) {
                throw new d("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3323() {
        return "FillRateReport";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3324() {
        JSONObject r0 = super.m3288();
        r0.put("rc", (Object) this.f2829.c);
        if (this.f2830) {
            r0.put("f", this.f2830);
        }
        return r0;
    }
}
