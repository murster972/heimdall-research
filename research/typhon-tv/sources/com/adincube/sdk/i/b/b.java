package com.adincube.sdk.i.b;

import com.adincube.sdk.c.c.d;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.i.a;
import com.adincube.sdk.util.w;
import java.net.HttpURLConnection;
import org.json.JSONObject;

public final class b extends a {

    /* renamed from: ʾ  reason: contains not printable characters */
    public Long f2816;

    /* renamed from: ˈ  reason: contains not printable characters */
    public Long f2817;

    /* renamed from: ٴ  reason: contains not printable characters */
    public e f2818;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public String f2819;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3308() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = m3300(w.m3730(), m3310());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400 && responseCode < 500) {
                throw new com.adincube.sdk.c.c.a(responseCode);
            } else if (responseCode >= 500) {
                throw new d("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3309() {
        return "AutoRedirectReport";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3310() {
        JSONObject r0 = super.m3288();
        r0.put("i", (Object) this.f2818.f2584);
        r0.put("c", (Object) this.f2818.f2582);
        r0.put("tu", (Object) this.f2819);
        r0.put("tsal", (Object) this.f2817);
        if (this.f2816 != null) {
            r0.put("tbrc", (Object) this.f2816);
        }
        return r0;
    }
}
