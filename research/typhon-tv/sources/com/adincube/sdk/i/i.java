package com.adincube.sdk.i;

import com.adincube.sdk.c.c.a;
import com.adincube.sdk.f.b.c.h;
import com.adincube.sdk.g.b.c;
import com.adincube.sdk.g.c.d;
import com.adincube.sdk.g.c.e;
import com.adincube.sdk.util.w;
import java.net.HttpURLConnection;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class i extends a {

    /* renamed from: ˈ  reason: contains not printable characters */
    public Map<String, h> f2838;

    /* renamed from: ٴ  reason: contains not printable characters */
    public c f2839;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public d f2840;

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m3331() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = m3300(w.m3724(), m3333());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400 && responseCode < 500) {
                throw new a(responseCode);
            } else if (responseCode >= 500) {
                throw new com.adincube.sdk.c.c.d("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final String m3332() {
        return "LoadingStatusReport";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3333() {
        Long r0;
        JSONObject r4 = super.m3288();
        long currentTimeMillis = System.currentTimeMillis();
        JSONArray jSONArray = new JSONArray();
        JSONArray jSONArray2 = new JSONArray();
        for (e next : this.f2840.f2660) {
            jSONArray.put((Object) next.f2667);
            JSONObject jSONObject = new JSONObject();
            h hVar = this.f2838.get(next.f2667);
            com.adincube.sdk.f.b.c.e eVar = com.adincube.sdk.f.b.c.e.WAITING;
            if (hVar != null) {
                eVar = hVar.m2906();
            }
            jSONObject.put("s", (Object) eVar.k);
            switch (eVar) {
                case WAITING:
                case WAITING_FOR_RESPONSE:
                    r0 = null;
                    break;
                case LOADED:
                    r0 = Long.valueOf(hVar.f2331.longValue() - hVar.f2332.longValue());
                    break;
                case LOADING:
                case NO_MORE_INVENTORY:
                case ERROR:
                case TIMEOUT:
                    r0 = Long.valueOf(currentTimeMillis - hVar.f2332.longValue());
                    break;
                case EXPIRED:
                    if (hVar.f2332 == null) {
                        r0 = next.f2663.m3431().m3468();
                        break;
                    } else {
                        r0 = Long.valueOf(currentTimeMillis - hVar.f2332.longValue());
                        break;
                    }
                default:
                    r0 = null;
                    break;
            }
            if (r0 != null) {
                jSONObject.put("t", (Object) r0);
            }
            jSONArray2.put((Object) jSONObject);
        }
        r4.put("o", (Object) jSONArray);
        r4.put("ls", (Object) jSONArray2);
        if (this.f2840.f2657) {
            r4.put("f", this.f2840.f2657);
        }
        r4.put("rc", (Object) this.f2839.c);
        return r4;
    }
}
