package com.adincube.sdk.mediation;

import com.adincube.sdk.c.b.c;
import java.util.Collections;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class e {

    /* renamed from: ˋ  reason: contains not printable characters */
    public Integer f2948 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    protected Integer f2949 = null;

    public e(JSONObject jSONObject) {
        try {
            if (jSONObject.has("f")) {
                this.f2948 = Integer.valueOf(jSONObject.getInt("f"));
            }
            if (jSONObject.has("ce")) {
                this.f2949 = Integer.valueOf(jSONObject.getInt("ce"));
            }
        } catch (JSONException e) {
            throw new c(m3469(), (Throwable) e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Map<String, String> m3467() {
        return Collections.emptyMap();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final Long m3468() {
        if (this.f2949 == null) {
            return null;
        }
        return Long.valueOf(((long) this.f2949.intValue()) * 1000);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract String m3469();
}
