package com.adincube.sdk.mediation;

import com.adincube.sdk.g.c.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class d {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f2939;

    /* renamed from: ʼ  reason: contains not printable characters */
    Map<b, Integer> f2940 = new HashMap();

    /* renamed from: ʽ  reason: contains not printable characters */
    Map<b, List<String>> f2941 = new HashMap();

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean f2942;

    /* renamed from: ʿ  reason: contains not printable characters */
    public double f2943;

    /* renamed from: ˈ  reason: contains not printable characters */
    public int f2944;

    /* renamed from: ˑ  reason: contains not printable characters */
    List<b> f2945 = new ArrayList();

    /* renamed from: ٴ  reason: contains not printable characters */
    public Long f2946 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean f2947;

    public d(JSONObject jSONObject) {
        try {
            this.f2939 = jSONObject.getInt("to");
            JSONObject jSONObject2 = jSONObject.getJSONObject("oto");
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                try {
                    b a = b.a((String) keys.next());
                    this.f2940.put(a, Integer.valueOf(jSONObject2.getInt(a.e)));
                } catch (IllegalStateException e) {
                }
            }
            JSONObject jSONObject3 = jSONObject.getJSONObject("nnacl");
            Iterator keys2 = jSONObject3.keys();
            while (keys2.hasNext()) {
                try {
                    b a2 = b.a((String) keys2.next());
                    ArrayList arrayList = new ArrayList();
                    JSONArray jSONArray = jSONObject3.getJSONArray(a2.e);
                    for (int i = 0; i < jSONArray.length(); i++) {
                        arrayList.add(jSONArray.getString(i));
                    }
                    this.f2941.put(a2, arrayList);
                } catch (IllegalStateException e2) {
                }
            }
            JSONArray jSONArray2 = jSONObject.getJSONArray("uat");
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                try {
                    this.f2945.add(b.a(jSONArray2.getString(i2)));
                } catch (IllegalStateException e3) {
                }
            }
            if (jSONObject.has("fnfrt")) {
                this.f2946 = Long.valueOf(jSONObject.getLong("fnfrt"));
            }
            this.f2947 = jSONObject.getBoolean("bcp");
            this.f2944 = jSONObject.getInt("mnatl");
            this.f2942 = jSONObject.getBoolean("nve");
            this.f2943 = jSONObject.getDouble("mpvvi");
        } catch (JSONException e4) {
            throw new com.adincube.sdk.c.b.b(m3465(), e4);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m3463(b bVar) {
        return !this.f2945.contains(bVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m3464(b bVar) {
        Integer num = this.f2940.get(bVar);
        if (num == null) {
            num = Integer.valueOf(this.f2939);
        }
        return ((long) num.intValue()) * 1000;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m3465();

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3466(b bVar, List<String> list) {
        List list2 = this.f2941.get(bVar);
        if (list2 == null) {
            list2 = Collections.emptyList();
        }
        Iterator it2 = list2.iterator();
        boolean z = true;
        while (z && it2.hasNext()) {
            z = !list.contains(it2.next());
        }
        return !z;
    }
}
