package com.adincube.sdk.mediation;

import com.adincube.sdk.mediation.i;
import com.adincube.sdk.mediation.u.b;

public class j {

    /* renamed from: 连任  reason: contains not printable characters */
    private b f2954;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f2955 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private com.adincube.sdk.mediation.n.b f2956;

    /* renamed from: 齉  reason: contains not printable characters */
    public a f2957 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public b f2958 = null;

    public j(b bVar) {
        this.f2958 = bVar;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f2958.equals(((j) obj).f2958);
    }

    public int hashCode() {
        return this.f2958.hashCode();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final void m3486() {
        if (this.f2956 != null) {
            this.f2956.麤(this.f2958);
        }
        if (this.f2954 != null) {
            this.f2954.麤(this.f2958);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3487() {
        if (this.f2956 != null) {
            this.f2956.ˋ();
        }
        if (this.f2954 != null) {
            this.f2954.龘();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m3488() {
        if (this.f2956 != null) {
            this.f2956.龘(this.f2958);
        }
        if (this.f2954 != null) {
            this.f2954.龘(this.f2958);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3489() {
        if (this.f2954 != null) {
            this.f2954.靐();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3490() {
        if (this.f2957 != null) {
            this.f2957.龘();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3491(i.a aVar) {
        i iVar = new i(this.f2958, aVar);
        if (this.f2957 != null) {
            this.f2957.龘(iVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3492(i.a aVar, String str) {
        i iVar = new i(this.f2958, aVar, str);
        if (this.f2956 != null) {
            this.f2956.龘(this.f2958, iVar);
        }
        if (this.f2954 != null) {
            this.f2954.龘(this.f2958, iVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3493(com.adincube.sdk.mediation.n.b bVar) {
        if (this.f2954 != null) {
            throw new IllegalStateException("Cannot set interstitial event listener when rewarded event listener has been set.");
        }
        this.f2956 = bVar;
    }
}
