package com.adincube.sdk.mediation;

import com.adincube.sdk.util.m;

public final class i {

    /* renamed from: 靐  reason: contains not printable characters */
    public a f2950;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f2951;

    /* renamed from: 龘  reason: contains not printable characters */
    public h f2952;

    public enum a {
        INTEGRATION("INTEGRATION"),
        NO_MORE_INVENTORY("NO_MORE_INVENTORY"),
        NETWORK("NETWORK"),
        UNKNOWN("UNKNOWN");
        
        public String e;

        private a(String str) {
            this.e = str;
        }

        public static a[] a() {
            return (a[]) f2953.clone();
        }
    }

    public i(b bVar, a aVar) {
        this.f2952 = bVar.m3429();
        this.f2950 = aVar;
    }

    public i(b bVar, a aVar, String str) {
        this(bVar, aVar);
        this.f2951 = str;
    }

    public i(b bVar, a aVar, Throwable th) {
        this(bVar, aVar);
        this.f2951 = m.m3686(th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m3485() {
        return this.f2951 == null ? "Unknown" : this.f2951;
    }
}
