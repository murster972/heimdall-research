package com.adincube.sdk.util.e;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.b.k;
import com.adincube.sdk.util.d;
import com.adincube.sdk.util.f;
import com.adincube.sdk.util.o;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import net.pubnative.library.request.PubnativeRequest;

public final class a {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean f2997 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m3579(Context context) {
        return c.m3600(context, "ua");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m3580() {
        Context r1 = f.m3605();
        List<String> asList = Arrays.asList(new String[]{"lak", "tm", "ai", "aif", PubnativeRequest.Parameters.LAT, "ua"});
        SharedPreferences.Editor edit = r1.getSharedPreferences("AIC-prefs", 0).edit();
        for (String remove : asList) {
            edit.remove(remove);
        }
        edit.commit();
        c.m3601(r1, "lak", d.m3553());
        m3583(r1);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static com.adincube.sdk.g.a m3581(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("AIC-prefs", 0);
        return new com.adincube.sdk.g.a(sharedPreferences.getString("ai", (String) null), sharedPreferences.getBoolean(PubnativeRequest.Parameters.LAT, false), sharedPreferences.getLong("aif", -1));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3583(final Context context) {
        if (!f2997) {
            f2997 = true;
            if (c.m3600(context, "ua") == null) {
                AnonymousClass1 r0 = new Runnable() {
                    public final void run() {
                        try {
                            Context context = context;
                            String r0 = k.m3549(context);
                            if (r0 == null) {
                                r0 = k.m3547(context);
                            }
                            if (r0 == null) {
                                r0 = k.m3548(context);
                            }
                            c.m3601(context, "ua", r0);
                            boolean unused = a.f2997 = false;
                        } catch (Throwable th) {
                            com.adincube.sdk.util.a.m3513("ConfigPreferencesHelper.updateUserAgent", th);
                            ErrorReportingHelper.m3511("ConfigPreferencesHelper.updateUserAgent", th);
                        }
                    }
                };
                if (Looper.getMainLooper() != Looper.myLooper()) {
                    o.m3701("HandlerUtil.dispatchOnUiThread", (Runnable) r0);
                } else {
                    r0.run();
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3584(Context context, com.adincube.sdk.g.a aVar) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("AIC-prefs", 0);
        com.adincube.sdk.g.a r1 = m3581(context);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (!r1.m3171() || !r1.equals(aVar)) {
            edit.putString("ai", aVar.f2549);
            edit.putBoolean(PubnativeRequest.Parameters.LAT, aVar.f2547);
        }
        edit.putLong("aif", new Date().getTime());
        edit.apply();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3585() {
        Context r1 = f.m3605();
        String r2 = d.m3553();
        if (!r1.getSharedPreferences("AIC-prefs", 0).contains("lak")) {
            c.m3601(r1, "lak", r2);
            return false;
        } else if (r2.equals(c.m3600(r1, "lak"))) {
            return false;
        } else {
            c.m3601(r1, "lak", r2);
            return true;
        }
    }
}
