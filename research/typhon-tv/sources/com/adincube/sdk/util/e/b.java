package com.adincube.sdk.util.e;

import android.content.SharedPreferences;
import com.adincube.sdk.f.a;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b {

    /* renamed from: 龘  reason: contains not printable characters */
    private static b f2999 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public Lock f3000 = this.f3003.writeLock();

    /* renamed from: ʼ  reason: contains not printable characters */
    private Lock f3001 = this.f3003.readLock();
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public Lock f3002 = new ReentrantLock();

    /* renamed from: 连任  reason: contains not printable characters */
    private ReadWriteLock f3003 = new ReentrantReadWriteLock();

    /* renamed from: 靐  reason: contains not printable characters */
    private a f3004;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public Map<String, List<Long>> f3005 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private ExecutorService f3006;

    private b(a aVar) {
        this.f3004 = aVar;
        this.f3006 = Executors.newSingleThreadExecutor();
        this.f3006.submit(new Runnable() {
            public final void run() {
                try {
                    b.this.f3000.lock();
                    for (Map.Entry next : f.m3605().getSharedPreferences("NPB-impr", 0).getAll().entrySet()) {
                        if (next.getValue() instanceof String) {
                            ArrayList arrayList = new ArrayList();
                            try {
                                JSONArray jSONArray = new JSONArray((String) next.getValue());
                                for (int i = 0; i < jSONArray.length(); i++) {
                                    arrayList.add(Long.valueOf(jSONArray.getLong(i)));
                                }
                            } catch (JSONException e) {
                            }
                            b.this.f3002.lock();
                            List list = (List) b.this.f3005.get(next.getKey());
                            if (list != null) {
                                arrayList.addAll(list);
                            }
                            b.this.f3005.put(next.getKey(), arrayList);
                            b.this.f3002.unlock();
                        }
                    }
                    try {
                        b.this.f3000.unlock();
                    } catch (Throwable th) {
                    }
                } catch (Throwable th2) {
                }
            }
        });
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m3587(Map<String, List<Long>> map, com.adincube.sdk.g.c.b bVar) {
        List list;
        long j = Long.MAX_VALUE;
        List list2 = null;
        String r6 = m3589(bVar);
        for (Map.Entry next : map.entrySet()) {
            if (!((String) next.getKey()).startsWith(r6) || ((List) next.getValue()).size() <= 0 || ((Long) ((List) next.getValue()).get(0)).longValue() >= j) {
                list = list2;
            } else {
                long longValue = ((Long) ((List) next.getValue()).get(0)).longValue();
                list = (List) next.getValue();
                j = longValue;
            }
            list2 = list;
        }
        if (list2 != null) {
            list2.remove(0);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String m3589(com.adincube.sdk.g.c.b bVar) {
        return bVar.g + "-";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m3591(Map<String, List<Long>> map, com.adincube.sdk.g.c.b bVar) {
        long j = 0;
        String r4 = m3589(bVar);
        Iterator<Map.Entry<String, List<Long>>> it2 = map.entrySet().iterator();
        while (true) {
            long j2 = j;
            if (!it2.hasNext()) {
                return j2;
            }
            Map.Entry next = it2.next();
            j = ((String) next.getKey()).startsWith(r4) ? ((long) ((List) next.getValue()).size()) + j2 : j2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m3592() {
        if (f2999 == null) {
            synchronized (b.class) {
                f2999 = new b(a.m2607());
            }
        }
        return f2999;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m3594(final com.adincube.sdk.g.c.b bVar, final com.adincube.sdk.g.c.a aVar) {
        String r2 = m3589(bVar);
        final HashMap hashMap = new HashMap();
        synchronized (this.f3005) {
            for (Map.Entry next : this.f3005.entrySet()) {
                if (((String) next.getKey()).startsWith(r2)) {
                    hashMap.put(next.getKey(), new ArrayList((Collection) next.getValue()));
                }
            }
        }
        this.f3006.submit(new Runnable() {
            public final void run() {
                try {
                    SharedPreferences.Editor edit = f.m3605().getSharedPreferences("NPB-impr", 0).edit();
                    for (Map.Entry entry : hashMap.entrySet()) {
                        if (((List) entry.getValue()).size() == 0) {
                            edit.remove((String) entry.getKey());
                        } else {
                            JSONArray jSONArray = new JSONArray();
                            for (int i = 0; i < ((List) entry.getValue()).size(); i++) {
                                jSONArray.put(((List) entry.getValue()).get(i));
                            }
                            edit.putString((String) entry.getKey(), jSONArray.toString());
                        }
                    }
                    edit.commit();
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("ImpressionCounter.saveImpressionTimestampsForNetworkInSharedPreferences", th);
                    ErrorReportingHelper.m3505("ImpressionCounter.saveImpressionTimestampsForNetworkInSharedPreferences", bVar, aVar, th);
                }
            }
        });
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final long m3595(com.adincube.sdk.g.c.b bVar) {
        long j = 0;
        this.f3001.lock();
        String r4 = m3589(bVar);
        synchronized (this.f3005) {
            for (Map.Entry next : this.f3005.entrySet()) {
                if (((String) next.getKey()).startsWith(r4)) {
                    for (Long l : (List) next.getValue()) {
                        j = l.longValue() > j ? l.longValue() : j;
                    }
                }
            }
        }
        this.f3001.unlock();
        return j;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3596() {
        this.f3002.lock();
        this.f3005.clear();
        this.f3002.unlock();
        this.f3006.submit(new Runnable() {
            public final void run() {
                try {
                    f.m3605().getSharedPreferences("NPB-impr", 0).edit().clear().commit();
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("ImpressionCounter.cleanAll", th);
                    ErrorReportingHelper.m3511("ImpressionCounter.cleanAll", th);
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3597(com.adincube.sdk.g.c.b bVar) {
        this.f3001.lock();
        JSONObject jSONObject = new JSONObject();
        String r4 = m3589(bVar);
        synchronized (this.f3005) {
            for (Map.Entry next : this.f3005.entrySet()) {
                if (((String) next.getKey()).startsWith(r4)) {
                    JSONArray jSONArray = new JSONArray();
                    for (int i = 0; i < ((List) next.getValue()).size(); i++) {
                        jSONArray.put(((List) next.getValue()).get(i));
                    }
                    jSONObject.put(((String) next.getKey()).substring(r4.length()), (Object) jSONArray);
                }
            }
        }
        this.f3001.unlock();
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3598(String str, com.adincube.sdk.g.c.b bVar, com.adincube.sdk.g.c.a aVar) {
        this.f3002.lock();
        long currentTimeMillis = System.currentTimeMillis();
        String str2 = m3589(bVar) + str;
        synchronized (this.f3005) {
            List list = this.f3005.get(str2);
            if (list == null) {
                list = new ArrayList();
                this.f3005.put(str2, list);
            }
            list.add(Long.valueOf(currentTimeMillis));
            long j = (long) this.f3004.m2610(true, true).f2632;
            for (long r0 = m3591(this.f3005, bVar); r0 > j; r0--) {
                m3587(this.f3005, bVar);
            }
        }
        m3594(bVar, aVar);
        this.f3002.unlock();
    }
}
