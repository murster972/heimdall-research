package com.adincube.sdk.util.e;

import android.content.Context;
import android.content.SharedPreferences;

public final class c {
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m3599(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("AIC-prefs", 0).edit();
        edit.remove(str);
        edit.commit();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3600(Context context, String str) {
        return context.getSharedPreferences("AIC-prefs", 0).getString(str, (String) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3601(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences("AIC-prefs", 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }
}
