package com.adincube.sdk.util;

import java.util.Map;

public final class v {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m3718(String str) {
        String str2;
        int i = 0;
        if (str == null || str.length() == 0) {
            str2 = str;
        } else {
            int length = str.length();
            while (length > 0 && m3723(str.charAt(length - 1))) {
                length--;
            }
            str2 = length > 0 ? str.substring(0, length) : "";
        }
        if (str2 == null || str2.length() == 0) {
            return str2;
        }
        while (i < str2.length() && m3723(str2.charAt(i))) {
            i++;
        }
        return i < str2.length() ? str2.substring(i, str2.length()) : "";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m3719(Map<?, ?> map, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append(str2);
            }
            sb.append(next.getValue().toString());
            sb.append(str);
            sb.append(next.getKey().toString());
        }
        return sb.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m3720(String str) {
        return str == null || str.isEmpty();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3721(String str) {
        if (str == null) {
            return null;
        }
        return str.length() > 40 ? str.substring(0, 39) + "…" : str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3722(Map<?, ?> map, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append(str2);
            }
            sb.append(next.getKey().toString());
            sb.append(str);
            sb.append(next.getValue().toString());
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m3723(char c) {
        return c == ' ' || c == 10;
    }
}
