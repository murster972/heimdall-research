package com.adincube.sdk.util.h;

import android.content.Context;
import android.view.ViewGroup;
import com.adincube.sdk.c.a.c.b;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class f {

    /* renamed from: 靐  reason: contains not printable characters */
    private List<e> f3068 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f3069;

    public f(Context context) {
        this.f3069 = context;
        this.f3068.add(new b());
        this.f3068.add(new c());
        this.f3068.add(new d());
        this.f3068.add(new a());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final e m3659(ViewGroup viewGroup) {
        for (e next : this.f3068) {
            if (next.m3658(viewGroup)) {
                return next;
            }
        }
        throw new b(String.format(Locale.US, "'%s' is not currently supported.", new Object[]{viewGroup.getClass().getSimpleName()}));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ViewGroup m3660(ViewGroup viewGroup) {
        try {
            return (ViewGroup) viewGroup.getClass().getConstructor(new Class[]{Context.class}).newInstance(new Object[]{this.f3069});
        } catch (Throwable th) {
            throw new b("Cannot link to view group due to underlying exception.", th);
        }
    }
}
