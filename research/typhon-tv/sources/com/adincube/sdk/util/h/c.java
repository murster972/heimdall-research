package com.adincube.sdk.util.h;

import android.os.Build;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public final class c implements e<LinearLayout> {
    /* renamed from: 龘  reason: contains not printable characters */
    public final ViewGroup.LayoutParams m3650(ViewGroup.LayoutParams layoutParams) {
        return new LinearLayout.LayoutParams(g.m3662(layoutParams), g.m3661(layoutParams));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m3651(ViewGroup viewGroup, ViewGroup viewGroup2) {
        LinearLayout linearLayout = (LinearLayout) viewGroup;
        LinearLayout linearLayout2 = (LinearLayout) viewGroup2;
        linearLayout2.setOrientation(linearLayout.getOrientation());
        if (Build.VERSION.SDK_INT >= 24) {
            linearLayout2.setGravity(linearLayout.getGravity());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3652(ViewGroup viewGroup) {
        return viewGroup instanceof LinearLayout;
    }
}
