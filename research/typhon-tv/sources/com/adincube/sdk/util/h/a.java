package com.adincube.sdk.util.h;

import android.view.ViewGroup;
import com.adincube.sdk.c.a.c.b;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public final class a implements e {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Method f3046 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Method f3047 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Method f3048 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Method f3049 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Method f3050 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Field f3051 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Method f3052 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    private Field f3053 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Field f3054 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Field f3055 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private Field f3056 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Field f3057 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Method f3058 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Method f3059 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Method f3060 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private Constructor<?> f3061 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private Class<?> f3062 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private Constructor<?> f3063 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private Class<?> f3064 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private Class<?> f3065 = null;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Method f3066 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private Method f3067 = null;

    public a() {
        try {
            this.f3065 = Class.forName("android.support.constraint.ConstraintLayout");
            this.f3062 = Class.forName("android.support.constraint.ConstraintSet");
            this.f3064 = Class.forName("android.support.constraint.ConstraintLayout$LayoutParams");
            this.f3063 = this.f3062.getConstructor(new Class[0]);
            this.f3066 = this.f3062.getDeclaredMethod("clone", new Class[]{this.f3065});
            this.f3061 = this.f3064.getConstructor(new Class[]{Integer.TYPE, Integer.TYPE});
            this.f3067 = this.f3064.getDeclaredMethod("validate", new Class[0]);
            this.f3054 = this.f3064.getDeclaredField("topToTop");
            this.f3055 = this.f3064.getDeclaredField("bottomToBottom");
            this.f3056 = this.f3064.getDeclaredField("rightToRight");
            this.f3051 = this.f3064.getDeclaredField("leftToLeft");
            this.f3053 = this.f3064.getDeclaredField("startToStart");
            this.f3057 = this.f3064.getDeclaredField("endToEnd");
            this.f3050 = this.f3065.getDeclaredMethod("setConstraintSet", new Class[]{this.f3062});
            this.f3046 = this.f3065.getDeclaredMethod("getMaxWidth", new Class[0]);
            this.f3047 = this.f3065.getDeclaredMethod("setMaxWidth", new Class[]{Integer.TYPE});
            this.f3048 = this.f3065.getDeclaredMethod("getMinWidth", new Class[0]);
            this.f3058 = this.f3065.getDeclaredMethod("setMinWidth", new Class[]{Integer.TYPE});
            this.f3059 = this.f3065.getDeclaredMethod("getMaxHeight", new Class[0]);
            this.f3060 = this.f3065.getDeclaredMethod("setMaxHeight", new Class[]{Integer.TYPE});
            this.f3052 = this.f3065.getDeclaredMethod("getMinHeight", new Class[0]);
            this.f3049 = this.f3065.getDeclaredMethod("setMinHeight", new Class[]{Integer.TYPE});
        } catch (Throwable th) {
            new Object[1][0] = th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m3643(ViewGroup viewGroup, Method method, ViewGroup viewGroup2, Method method2) {
        try {
            method2.invoke(viewGroup2, new Object[]{method.invoke(viewGroup, new Object[0])});
        } catch (Throwable th) {
            new Object[1][0] = th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ViewGroup.LayoutParams m3644(ViewGroup.LayoutParams layoutParams) {
        try {
            ViewGroup.LayoutParams layoutParams2 = (ViewGroup.LayoutParams) this.f3061.newInstance(new Object[]{0, 0});
            this.f3054.set(layoutParams2, 0);
            this.f3055.set(layoutParams2, 0);
            this.f3056.set(layoutParams2, 0);
            this.f3051.set(layoutParams2, 0);
            this.f3053.set(layoutParams2, 0);
            this.f3057.set(layoutParams2, 0);
            this.f3067.invoke(layoutParams2, new Object[0]);
            return layoutParams2;
        } catch (Throwable th) {
            throw new b("Failed to create ConstraintLayout.LayoutParams due to underlying exception.", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3645(ViewGroup viewGroup, ViewGroup viewGroup2) {
        m3643(viewGroup, this.f3046, viewGroup2, this.f3047);
        m3643(viewGroup, this.f3048, viewGroup2, this.f3058);
        m3643(viewGroup, this.f3059, viewGroup2, this.f3060);
        m3643(viewGroup, this.f3052, viewGroup2, this.f3049);
        try {
            Object newInstance = this.f3063.newInstance(new Object[0]);
            this.f3066.invoke(newInstance, new Object[]{viewGroup});
            this.f3050.invoke(viewGroup2, new Object[]{newInstance});
        } catch (Throwable th) {
            new Object[1][0] = th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3646(ViewGroup viewGroup) {
        if (this.f3065 == null || this.f3064 == null || this.f3061 == null || this.f3067 == null || this.f3054 == null || this.f3055 == null || this.f3056 == null || this.f3051 == null || this.f3053 == null || this.f3057 == null || this.f3062 == null || this.f3063 == null || this.f3050 == null || this.f3066 == null) {
            return false;
        }
        return this.f3065.isAssignableFrom(viewGroup.getClass());
    }
}
