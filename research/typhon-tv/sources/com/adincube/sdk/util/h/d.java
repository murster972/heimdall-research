package com.adincube.sdk.util.h;

import android.os.Build;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public final class d implements e<RelativeLayout> {
    /* renamed from: 龘  reason: contains not printable characters */
    public final ViewGroup.LayoutParams m3653(ViewGroup.LayoutParams layoutParams) {
        return new RelativeLayout.LayoutParams(g.m3662(layoutParams), g.m3661(layoutParams));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final /* synthetic */ void m3654(ViewGroup viewGroup, ViewGroup viewGroup2) {
        RelativeLayout relativeLayout = (RelativeLayout) viewGroup;
        RelativeLayout relativeLayout2 = (RelativeLayout) viewGroup2;
        if (Build.VERSION.SDK_INT >= 16) {
            relativeLayout2.setGravity(relativeLayout.getGravity());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3655(ViewGroup viewGroup) {
        return viewGroup instanceof RelativeLayout;
    }
}
