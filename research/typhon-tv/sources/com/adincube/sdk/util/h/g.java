package com.adincube.sdk.util.h;

import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public final class g {

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3670(View view);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m3661(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams == null) {
            return -2;
        }
        int i = layoutParams.height;
        if (i == 0) {
            return -1;
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m3662(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams == null) {
            return -2;
        }
        int i = layoutParams.width;
        if (i == 0) {
            return -1;
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T extends View> T m3663(ViewGroup viewGroup, Class<T> cls) {
        if (viewGroup == null) {
            return null;
        }
        Stack stack = new Stack();
        stack.add(viewGroup);
        while (!stack.isEmpty()) {
            ViewGroup viewGroup2 = (ViewGroup) stack.pop();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < viewGroup2.getChildCount()) {
                    T childAt = viewGroup2.getChildAt(i2);
                    if (cls.isAssignableFrom(childAt.getClass())) {
                        return childAt;
                    }
                    if (childAt instanceof ViewGroup) {
                        stack.add((ViewGroup) childAt);
                    }
                    i = i2 + 1;
                }
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<View> m3664(ViewGroup viewGroup, List<Class<?>> list) {
        if (viewGroup == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.add(viewGroup);
        while (arrayDeque.size() > 0) {
            View view = (View) arrayDeque.poll();
            if ((view instanceof ViewGroup) && !m3669(view, list)) {
                ViewGroup viewGroup2 = (ViewGroup) view;
                for (int i = 0; i < viewGroup2.getChildCount(); i++) {
                    View childAt = viewGroup2.getChildAt(i);
                    if (childAt != null) {
                        arrayDeque.add(childAt);
                    }
                }
            }
            if (view != viewGroup) {
                arrayList.add(view);
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<View> m3665(ViewGroup viewGroup, List<Class<?>> list, List<Class<?>> list2) {
        List<View> r0 = m3664(viewGroup, list);
        Iterator<View> it2 = r0.iterator();
        while (it2.hasNext()) {
            if (m3669(it2.next(), list2)) {
                it2.remove();
            }
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3666(ViewGroup viewGroup, ViewGroup viewGroup2) {
        ArrayList<View> arrayList = new ArrayList<>(viewGroup.getChildCount());
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            arrayList.add(viewGroup.getChildAt(i));
        }
        viewGroup.removeAllViews();
        for (View addView : arrayList) {
            viewGroup2.addView(addView);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3667(ViewGroup viewGroup, a aVar) {
        m3668(viewGroup, (List<Class<? extends View>>) Collections.emptyList(), aVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3668(ViewGroup viewGroup, List<Class<? extends View>> list, a aVar) {
        if (viewGroup != null) {
            ArrayDeque arrayDeque = new ArrayDeque();
            arrayDeque.add(viewGroup);
            while (arrayDeque.size() > 0) {
                View view = (View) arrayDeque.poll();
                if (!list.contains(view.getClass())) {
                    if (view instanceof ViewGroup) {
                        ViewGroup viewGroup2 = (ViewGroup) view;
                        for (int i = 0; i < viewGroup2.getChildCount(); i++) {
                            View childAt = viewGroup2.getChildAt(i);
                            if (childAt != null) {
                                arrayDeque.add(childAt);
                            }
                        }
                    }
                    aVar.m3670(view);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T> boolean m3669(T t, List<Class<?>> list) {
        Class<?> cls = t.getClass();
        for (Class<?> isAssignableFrom : list) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }
}
