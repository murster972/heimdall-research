package com.adincube.sdk.util;

import android.app.Activity;
import android.content.Context;
import android.os.Build;

public final class f {

    /* renamed from: 靐  reason: contains not printable characters */
    private static Context f3013 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Activity f3014 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public static Activity m3602() {
        return f3014;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m3603(Activity activity) {
        if (activity == null) {
            return false;
        }
        return activity.isFinishing();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m3604(Activity activity) {
        return m3603(activity) || m3607(activity);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Context m3605() {
        return f3013;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized void m3606(Context context) {
        synchronized (f.class) {
            if (context != null) {
                if (context instanceof Activity) {
                    f3014 = (Activity) context;
                }
                if (f3013 == null) {
                    f3013 = context.getApplicationContext();
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3607(Activity activity) {
        if (activity != null && Build.VERSION.SDK_INT >= 17) {
            return activity.isDestroyed();
        }
        return false;
    }
}
