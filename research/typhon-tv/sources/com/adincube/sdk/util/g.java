package com.adincube.sdk.util;

import android.annotation.SuppressLint;
import com.adincube.sdk.g.b.b;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class g {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3624(b bVar, String str) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(str.getBytes("UTF-8"));
            gZIPOutputStream.flush();
            gZIPOutputStream.close();
            return m3625(bVar, byteArrayOutputStream.toByteArray());
        } catch (Exception e) {
            a.m3513("DataEncryptionHelper.gzipAndEncode()", e);
            return null;
        }
    }

    @SuppressLint({"TrulyRandom"})
    /* renamed from: 龘  reason: contains not printable characters */
    private static String m3625(b bVar, byte[] bArr) {
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(e.m3576("c1hLaUM5dDhkVnFvMTVOYw=="));
            SecretKeySpec r1 = m3626(bVar);
            if (r1 == null) {
                return null;
            }
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, r1, ivParameterSpec);
            return new String(e.m3578(instance.doFinal(bArr)));
        } catch (Exception e) {
            a.m3513("DataEncryptionHelper.encrypt()", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static SecretKeySpec m3626(b bVar) {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        if (bVar != null) {
            while (i < 11) {
                sb.append(bVar.f2634.charAt(i));
                sb.append("HTXDVXV3Fag".charAt(i));
                i++;
            }
        } else {
            while (i < 11) {
                sb.append("0");
                sb.append("HTXDVXV3Fag".charAt(i));
                i++;
            }
        }
        sb.append("==");
        String sb2 = sb.toString();
        if (sb2 == null) {
            return null;
        }
        return new SecretKeySpec(e.m3576(sb2), "AES");
    }
}
