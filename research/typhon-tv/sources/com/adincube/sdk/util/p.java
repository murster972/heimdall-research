package com.adincube.sdk.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public final class p {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3706(String str, String str2) {
        if (str2 == null) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            instance.reset();
            instance.update(str2.getBytes("UTF-8"));
            return m3707(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return "";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m3707(byte[] bArr) {
        Formatter formatter = new Formatter();
        try {
            int length = bArr.length;
            for (int i = 0; i < length; i++) {
                formatter.format("%02x", new Object[]{Byte.valueOf(bArr[i])});
            }
            return formatter.toString();
        } finally {
            formatter.close();
        }
    }
}
