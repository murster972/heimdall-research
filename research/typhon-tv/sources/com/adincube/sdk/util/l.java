package com.adincube.sdk.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.adincube.sdk.util.b.j;

public final class l {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Drawable m3685(Context context, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        float r1 = j.m3543(context) / ((float) (bitmap.getDensity() / 160));
        BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), bitmap);
        bitmapDrawable.setBounds(0, 0, (int) (((float) bitmap.getHeight()) * r1), (int) (r1 * ((float) bitmap.getWidth())));
        return bitmapDrawable;
    }
}
