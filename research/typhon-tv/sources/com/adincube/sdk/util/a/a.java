package com.adincube.sdk.util.a;

import android.os.CountDownTimer;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.g.c.c;
import com.adincube.sdk.util.ErrorReportingHelper;

public final class a extends CountDownTimer {

    /* renamed from: 靐  reason: contains not printable characters */
    public C0020a f2970 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private c f2971 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public long f2972;

    /* renamed from: com.adincube.sdk.util.a.a$a  reason: collision with other inner class name */
    public interface C0020a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3516();
    }

    public a(c cVar, long j) {
        super(j, 1000);
        this.f2971 = cVar;
        this.f2972 = j;
    }

    public final void onFinish() {
        try {
            this.f2972 = 0;
            if (this.f2970 != null) {
                this.f2970.m3516();
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("BannerAutoRefreshTimer.onFinish", th);
            ErrorReportingHelper.m3504("BannerAutoRefreshTimer.onFinish", b.BANNER, this.f2971.i, (Boolean) true, th);
        }
    }

    public final void onTick(long j) {
        this.f2972 = j;
    }
}
