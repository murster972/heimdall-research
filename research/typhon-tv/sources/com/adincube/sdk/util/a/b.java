package com.adincube.sdk.util.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.s;

public final class b extends BroadcastReceiver {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f2973 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f2974 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    public a f2975 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private com.adincube.sdk.g.c.a f2976 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private com.adincube.sdk.g.c.b f2977 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Context f2978 = null;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3519(boolean z);
    }

    public b(Context context, com.adincube.sdk.g.c.b bVar, com.adincube.sdk.g.c.a aVar) {
        this.f2978 = context;
        this.f2977 = bVar;
        this.f2976 = aVar;
        this.f2973 = s.m3713(this.f2978);
    }

    public final void onReceive(Context context, Intent intent) {
        try {
            boolean z = "android.intent.action.SCREEN_ON".equals(intent.getAction());
            if (this.f2973 != z) {
                this.f2973 = z;
                if (this.f2975 != null) {
                    this.f2975.m3519(z);
                }
            }
        } catch (Throwable th) {
            new Object[1][0] = th;
            ErrorReportingHelper.m3505("ScreenStateManager.onReceive", this.f2977, this.f2976, th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3517() {
        if (this.f2974) {
            try {
                this.f2978.unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                if (e.getMessage() == null || !e.getMessage().startsWith("Receiver not registered")) {
                    throw e;
                }
            }
            this.f2974 = false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3518() {
        if (!this.f2974) {
            this.f2978.registerReceiver(this, new IntentFilter("android.intent.action.SCREEN_ON"));
            this.f2978.registerReceiver(this, new IntentFilter("android.intent.action.SCREEN_OFF"));
            this.f2974 = true;
        }
    }
}
