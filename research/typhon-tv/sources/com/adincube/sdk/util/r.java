package com.adincube.sdk.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;

public final class r {

    /* renamed from: 龘  reason: contains not printable characters */
    private static Integer f3089 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public static Integer m3710(Context context) {
        if (f3089 == null) {
            f3089 = m3711(context);
        }
        return f3089;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static Integer m3711(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo.metaData != null && applicationInfo.metaData.containsKey("com.google.android.gms.version")) {
                return Integer.valueOf(applicationInfo.metaData.getInt("com.google.android.gms.version"));
            }
        } catch (Throwable th) {
            a.m3513("PlayServicesHelper.doGetGooglePlayServicesVersion", th);
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3712(Context context) {
        Integer r0 = m3710(context);
        if (r0 != null) {
            return r0.toString();
        }
        return null;
    }
}
