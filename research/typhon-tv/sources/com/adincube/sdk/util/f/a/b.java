package com.adincube.sdk.util.f.a;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import com.adincube.sdk.g.a.e;
import com.adincube.sdk.g.c.c;
import com.adincube.sdk.mediation.v.f;
import com.adincube.sdk.util.ErrorReportingHelper;

public final class b {

    /* renamed from: ʻ  reason: contains not printable characters */
    private e f3022 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f3023 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    public C0022b f3024 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    long f3025;

    /* renamed from: 麤  reason: contains not printable characters */
    public com.adincube.sdk.h.e f3026 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    a f3027 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    c f3028 = null;

    private class a extends CountDownTimer {
        public a(long j) {
            super(j, 250);
        }

        public final void onFinish() {
            try {
                b.this.m3614(b.this.f3026.m3279(true));
            } catch (Throwable th) {
                new Object[1][0] = th;
                ErrorReportingHelper.m3505("MRAIDBannerImpressionHelper#ImpressionTimer.onFinish", com.adincube.sdk.g.c.b.BANNER, b.this.f3028.i, th);
            }
        }

        public final void onTick(long j) {
        }
    }

    /* renamed from: com.adincube.sdk.util.f.a.b$b  reason: collision with other inner class name */
    public interface C0022b {
        /* renamed from: 龘  reason: contains not printable characters */
        void m3615(e eVar, Boolean bool);
    }

    public b(e eVar, c cVar, f fVar, com.adincube.sdk.h.e eVar2) {
        this.f3022 = eVar;
        this.f3028 = cVar;
        this.f3026 = eVar2;
        this.f3025 = fVar.连任;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                try {
                    new Object[1][0] = Long.valueOf(b.this.f3025);
                    b.this.f3027 = new a(b.this.f3025);
                    b.this.f3027.start();
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("MRAIDBannerImpressionHelper.startImpressionTimer", th);
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3613() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                try {
                    if (b.this.f3027 != null) {
                        b.this.f3027.cancel();
                        b.this.f3027 = null;
                    }
                } catch (Throwable th) {
                    com.adincube.sdk.util.a.m3513("MRAIDBannerImpressionHelper.stopImpressionTimer", th);
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized boolean m3614(boolean z) {
        boolean z2 = true;
        synchronized (this) {
            if (!this.f3023) {
                this.f3023 = true;
                this.f3024.m3615(this.f3022, Boolean.valueOf(z));
            } else {
                z2 = false;
            }
        }
        return z2;
    }
}
