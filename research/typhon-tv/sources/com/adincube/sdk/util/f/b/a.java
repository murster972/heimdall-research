package com.adincube.sdk.util.f.b;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.adincube.sdk.g.a.d;

public final class a {

    /* renamed from: 龘  reason: contains not printable characters */
    public d f3037;

    public a(d dVar) {
        this.f3037 = dVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3623(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            if (!(context instanceof Activity)) {
                intent.setFlags(268435456);
            }
            context.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }
}
