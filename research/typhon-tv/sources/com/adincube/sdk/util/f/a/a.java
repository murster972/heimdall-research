package com.adincube.sdk.util.f.a;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.adincube.sdk.f.e.b.a;
import com.adincube.sdk.mediation.v.f;
import com.adincube.sdk.util.f.b;
import com.google.android.exoplayer2.util.MimeTypes;

public final class a {

    /* renamed from: 靐  reason: contains not printable characters */
    private f f3018 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private com.adincube.sdk.f.e.b.a f3019 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private b f3020 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f3021 = null;

    public a(Context context, f fVar, b bVar, com.adincube.sdk.f.e.b.a aVar) {
        this.f3021 = context;
        this.f3018 = fVar;
        this.f3020 = bVar;
        this.f3019 = aVar;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m3609(Uri uri) {
        if (!this.f3019.m3138()) {
            return true;
        }
        com.adincube.sdk.f.e.b.a aVar = this.f3019;
        String uri2 = uri.toString();
        if (aVar.m3138()) {
            a.C0009a aVar2 = new a.C0009a((byte) 0);
            aVar2.f2518 = uri2;
            aVar2.f2520 = System.currentTimeMillis() - aVar.f2516.longValue();
            synchronized (aVar.f2513) {
                aVar.f2513.add(aVar2);
            }
            aVar.m3137();
        }
        return !this.f3018.ﾞ;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m3610() {
        return this.f3020.m3617() && this.f3020.m3619();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3611(Uri uri) {
        if (m3610() && m3609(uri)) {
            this.f3020.m3618();
            Intent intent = new Intent("android.intent.action.VIEW", uri);
            intent.setDataAndType(uri, MimeTypes.VIDEO_MP4);
            try {
                this.f3021.startActivity(intent);
            } catch (Exception e) {
                com.adincube.sdk.util.a.m3513("MRAIDAdDisplayer.playVideo()", e);
            }
            this.f3020.m3616();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3612(Uri uri) {
        if (m3610() && m3609(uri)) {
            this.f3020.m3618();
            try {
                this.f3021.startActivity(new Intent("android.intent.action.VIEW", uri));
            } catch (Exception e) {
                com.adincube.sdk.util.a.m3513("MRAIDAdDisplayer.openUri()", e);
            }
            this.f3020.m3616();
        }
    }
}
