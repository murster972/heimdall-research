package com.adincube.sdk.util;

import android.annotation.SuppressLint;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathTyphoonApp;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressLint({"NewApi"})
public final class x {

    /* renamed from: 龘  reason: contains not printable characters */
    private static XPathFactory f3094 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Integer m3736(Node node, String str) {
        String r0 = m3737(node, str);
        if (r0 != null) {
            return Integer.valueOf(Integer.parseInt(r0));
        }
        return null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static String m3737(Node node, String str) {
        Node namedItem;
        if (node.getAttributes() == null || (namedItem = node.getAttributes().getNamedItem(str)) == null) {
            return null;
        }
        return namedItem.getTextContent();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Node m3738(Node node, String str) {
        ArrayList arrayList = new ArrayList();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            if (str.equals(childNodes.item(i).getNodeName())) {
                arrayList.add(childNodes.item(i));
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return (Node) arrayList.get(0);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m3739(Node node, String str) {
        Node r0 = m3738(node, str);
        if (r0 == null) {
            return null;
        }
        return m3741(r0.getTextContent());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static List<String> m3740(Node node, String str) {
        ArrayList arrayList = new ArrayList();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            if (str.equals(childNodes.item(i).getNodeName())) {
                arrayList.add(m3741(childNodes.item(i).getTextContent()));
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3741(String str) {
        return str.replaceAll("\n|\t", "");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Map<String, String> m3742(Node node) {
        HashMap hashMap = new HashMap();
        if (node.getAttributes() != null) {
            for (int i = 0; i < node.getAttributes().getLength(); i++) {
                Node item = node.getAttributes().item(i);
                hashMap.put(item.getNodeName(), item.getNodeValue());
            }
        }
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static NodeList m3743(Node node, String str) {
        try {
            if (f3094 == null) {
                f3094 = XPathFactory.newInstance();
            }
            return (NodeList) f3094.newXPath().evaluate(str, node, XPathTyphoonApp.NODESET);
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }
}
