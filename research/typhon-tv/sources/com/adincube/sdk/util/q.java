package com.adincube.sdk.util;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Random;
import java.util.zip.GZIPInputStream;

public final class q {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m3708(int i, boolean z) {
        if (z) {
            switch (i) {
                case 1:
                    return new Random().nextInt(2000) + 2000;
                case 2:
                    return new Random().nextInt(2000) + 2000;
                case 3:
                    return new Random().nextInt(5000) + 5000;
                case 4:
                    return new Random().nextInt(10000) + 10000;
                case 5:
                    return new Random().nextInt(30000) + 30000;
                case 6:
                    return new Random().nextInt(60000) + 60000;
                case 7:
                    return new Random().nextInt(900000) + 900000;
                default:
                    return new Random().nextInt(3600000) + 3600000;
            }
        } else {
            switch (i) {
                case 1:
                    return 1000;
                case 2:
                    return 3000;
                case 3:
                    return 7000;
                case 4:
                    return 30000;
                case 5:
                    return 60000;
                case 6:
                    return 900000;
                default:
                    return 3600000;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static InputStream m3709(HttpURLConnection httpURLConnection) {
        InputStream errorStream = httpURLConnection.getResponseCode() >= 400 ? httpURLConnection.getErrorStream() : httpURLConnection.getInputStream();
        return "gzip".equalsIgnoreCase(httpURLConnection.getHeaderField("Content-Encoding")) ? new GZIPInputStream(errorStream) : errorStream;
    }
}
