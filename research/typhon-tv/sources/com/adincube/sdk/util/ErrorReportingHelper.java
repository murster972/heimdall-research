package com.adincube.sdk.util;

import android.net.Uri;
import com.adincube.sdk.a.a.a.g;
import com.adincube.sdk.f.a;
import com.adincube.sdk.g.b.c;
import com.adincube.sdk.g.c.b;
import com.adincube.sdk.g.e;
import com.adincube.sdk.i.k;

public class ErrorReportingHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f2966 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private static a f2967 = a.m2607();

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3501(g gVar, Uri uri, b bVar, com.adincube.sdk.g.c.a aVar, Boolean bool) {
        if (!f2966) {
            try {
                t r0 = t.m3714();
                if (r0.m3716(f2967.m2610(true, true), e.PlayerError, c.SHOW)) {
                    k kVar = new k();
                    kVar.f2765 = bVar;
                    kVar.f2763 = aVar;
                    kVar.f2764 = bool;
                    kVar.f2849 = gVar;
                    kVar.f2850 = uri;
                    kVar.m3296();
                }
                r0.m3715(e.Event, c.SHOW);
            } catch (Throwable th) {
                a.m3513("ErrorReportingHelper.report", th);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3502(String str, com.adincube.sdk.g.a.b bVar, com.adincube.sdk.g.c.c cVar, Throwable th) {
        m3507(str, "RTB", b.BANNER, cVar.i, (Boolean) null, bVar, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3503(String str, com.adincube.sdk.g.a.e eVar, Throwable th) {
        m3507(str, "RTB", b.INTERSTITIAL, (com.adincube.sdk.g.c.a) null, false, eVar, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3504(String str, b bVar, com.adincube.sdk.g.c.a aVar, Boolean bool, Throwable th) {
        m3507(str, (String) null, bVar, aVar, bool, (com.adincube.sdk.g.a.e) null, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3505(String str, b bVar, com.adincube.sdk.g.c.a aVar, Throwable th) {
        m3508(str, (String) null, bVar, aVar, (Boolean) null, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3506(String str, b bVar, Throwable th) {
        m3508(str, (String) null, bVar, (com.adincube.sdk.g.c.a) null, (Boolean) null, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m3507(String str, String str2, b bVar, com.adincube.sdk.g.c.a aVar, Boolean bool, com.adincube.sdk.g.a.e eVar, Throwable th) {
        if (!f2966) {
            try {
                t r0 = t.m3714();
                if (r0.m3716(f2967.m2610(true, true), e.Event, c.SHOW)) {
                    com.adincube.sdk.i.e eVar2 = new com.adincube.sdk.i.e();
                    eVar2.f2765 = bVar;
                    eVar2.f2763 = aVar;
                    eVar2.f2764 = bool;
                    eVar2.f2824 = str2;
                    eVar2.f2826 = eVar;
                    eVar2.f2827 = "UNKNOWN_EXCEPTION";
                    eVar2.f2828 = str;
                    eVar2.f2825 = m.m3686(th);
                    eVar2.m3296();
                }
                r0.m3715(e.Event, c.SHOW);
            } catch (Throwable th2) {
                a.m3513("ErrorReportingHelper.report", th2);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3508(String str, String str2, b bVar, com.adincube.sdk.g.c.a aVar, Boolean bool, Throwable th) {
        m3507(str, str2, bVar, aVar, bool, (com.adincube.sdk.g.a.e) null, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3509(String str, String str2, b bVar, com.adincube.sdk.g.c.a aVar, Throwable th) {
        m3507(str, str2, bVar, aVar, (Boolean) null, (com.adincube.sdk.g.a.e) null, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3510(String str, String str2, b bVar, Throwable th) {
        m3507(str, str2, bVar, (com.adincube.sdk.g.c.a) null, (Boolean) null, (com.adincube.sdk.g.a.e) null, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3511(String str, Throwable th) {
        m3506(str, (b) null, th);
    }
}
