package com.adincube.sdk.util;

public final class e {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final byte[] f2995 = new byte[128];

    /* renamed from: 龘  reason: contains not printable characters */
    private static final char[] f2996 = new char[64];

    static {
        char c = 'A';
        int i = 0;
        while (c <= 'Z') {
            f2996[i] = c;
            c = (char) (c + 1);
            i++;
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            f2996[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            f2996[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        f2996[i] = '+';
        f2996[i + 1] = '/';
        for (int i2 = 0; i2 < f2995.length; i2++) {
            f2995[i2] = -1;
        }
        for (int i3 = 0; i3 < 64; i3++) {
            f2995[f2996[i3]] = (byte) i3;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m3574(String str) {
        return new String(m3576(str));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m3575(String str) {
        return new String(m3578(str.getBytes()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m3576(String str) {
        return m3577(str.toCharArray());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m3577(char[] cArr) {
        char c;
        char c2;
        int i;
        int i2;
        int length = cArr.length;
        if (length % 4 != 0) {
            throw new com.adincube.sdk.c.a.e();
        }
        while (length > 0 && cArr[(length + 0) - 1] == '=') {
            length--;
        }
        int i3 = (length * 3) / 4;
        byte[] bArr = new byte[i3];
        int i4 = length + 0;
        int i5 = 0;
        int i6 = 0;
        while (i6 < i4) {
            int i7 = i6 + 1;
            char c3 = cArr[i6];
            int i8 = i7 + 1;
            char c4 = cArr[i7];
            if (i8 < i4) {
                i6 = i8 + 1;
                c = cArr[i8];
            } else {
                c = 'A';
                i6 = i8;
            }
            if (i6 < i4) {
                c2 = cArr[i6];
                i6++;
            } else {
                c2 = 'A';
            }
            if (c3 > 127 || c4 > 127 || c > 127 || c2 > 127) {
                throw new com.adincube.sdk.c.a.e();
            }
            byte b = f2995[c3];
            byte b2 = f2995[c4];
            byte b3 = f2995[c];
            byte b4 = f2995[c2];
            if (b < 0 || b2 < 0 || b3 < 0 || b4 < 0) {
                throw new com.adincube.sdk.c.a.e();
            }
            int i9 = (b << 2) | (b2 >>> 4);
            int i10 = ((b2 & 15) << 4) | (b3 >>> 2);
            byte b5 = ((b3 & 3) << 6) | b4;
            int i11 = i5 + 1;
            bArr[i5] = (byte) i9;
            if (i11 < i3) {
                i = i11 + 1;
                bArr[i11] = (byte) i10;
            } else {
                i = i11;
            }
            if (i < i3) {
                i2 = i + 1;
                bArr[i] = (byte) b5;
            } else {
                i2 = i;
            }
            i5 = i2;
        }
        return bArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static char[] m3578(byte[] bArr) {
        byte b;
        int i;
        byte b2;
        int length = bArr.length;
        int i2 = ((length * 4) + 2) / 3;
        char[] cArr = new char[(((length + 2) / 3) * 4)];
        int i3 = length + 0;
        int i4 = 0;
        int i5 = 0;
        while (i5 < i3) {
            int i6 = i5 + 1;
            byte b3 = bArr[i5] & 255;
            if (i6 < i3) {
                i = i6 + 1;
                b = bArr[i6] & 255;
            } else {
                b = 0;
                i = i6;
            }
            if (i < i3) {
                i5 = i + 1;
                b2 = bArr[i] & 255;
            } else {
                b2 = 0;
                i5 = i;
            }
            int i7 = b3 >>> 2;
            int i8 = ((b3 & 3) << 4) | (b >>> 4);
            int i9 = ((b & 15) << 2) | (b2 >>> 6);
            byte b4 = b2 & 63;
            int i10 = i4 + 1;
            cArr[i4] = f2996[i7];
            int i11 = i10 + 1;
            cArr[i10] = f2996[i8];
            cArr[i11] = i11 < i2 ? f2996[i9] : '=';
            int i12 = i11 + 1;
            cArr[i12] = i12 < i2 ? f2996[b4] : '=';
            i4 = i12 + 1;
        }
        return cArr;
    }
}
