package com.adincube.sdk.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import io.fabric.sdk.android.services.common.AbstractSpiCall;

public final class b {

    /* renamed from: 龘  reason: contains not printable characters */
    private Activity f2979 = null;

    public b(Context context) {
        if (context instanceof Activity) {
            this.f2979 = (Activity) context;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m3520(Context context) {
        try {
            int identifier = context.getResources().getIdentifier("status_bar_height", "dimen", AbstractSpiCall.ANDROID_CLIENT_TYPE);
            if (identifier > 0) {
                return context.getResources().getDimensionPixelSize(identifier);
            }
            return 0;
        } catch (Throwable th) {
            a.m3513("ActivityThemeHelper.getStatusBarHeightInPixels", th);
            ErrorReportingHelper.m3511("ActivityThemeHelper.getStatusBarHeightInPixels", th);
            return 0;
        }
    }

    @TargetApi(19)
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3521(Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 19 && bundle.containsKey("suf")) {
            int i = bundle.getInt("suf");
            if ((i & 2048) != 0) {
                i = (i & -2049) | 4096;
            }
            this.f2979.getWindow().getDecorView().setSystemUiVisibility(i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3522(Intent intent) {
        intent.putExtra("fs", k.m3681(this.f2979));
        intent.putExtra("wt", k.m3683(this.f2979));
        intent.putExtra("suf", k.m3682(this.f2979));
        Integer r0 = k.m3677(this.f2979);
        if (r0 != null) {
            intent.putExtra("sbc", r0);
        }
        Integer r02 = k.m3678(this.f2979);
        if (r02 != null) {
            intent.putExtra("snc", r02);
        }
    }

    @TargetApi(21)
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3523(Bundle bundle) {
        this.f2979.getWindow().addFlags(128);
        if (bundle.containsKey("wt") && bundle.getBoolean("wt")) {
            this.f2979.requestWindowFeature(1);
        }
        if (bundle.containsKey("fs") && bundle.getBoolean("fs")) {
            this.f2979.getWindow().addFlags(1024);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            if (bundle.containsKey("sbc") || bundle.containsKey("snc")) {
                this.f2979.getWindow().addFlags(Integer.MIN_VALUE);
            }
            if (bundle.containsKey("sbc")) {
                this.f2979.getWindow().setStatusBarColor(bundle.getInt("sbc"));
            }
            if (bundle.containsKey("snc")) {
                this.f2979.getWindow().setNavigationBarColor(bundle.getInt("snc"));
            }
        }
    }
}
