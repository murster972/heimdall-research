package com.adincube.sdk.util.d;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.support.v4.app.NotificationCompat;
import com.adincube.sdk.c.b.d;
import com.adincube.sdk.util.b.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class b {

    /* renamed from: 靐  reason: contains not printable characters */
    public Context f2985;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f2986;

    /* renamed from: 齉  reason: contains not printable characters */
    public ApplicationInfo f2987;

    /* renamed from: 龘  reason: contains not printable characters */
    public List<a> f2988 = new ArrayList();

    public static class a {

        /* renamed from: 靐  reason: contains not printable characters */
        List<String> f2989 = new ArrayList();

        /* renamed from: 龘  reason: contains not printable characters */
        String f2990;

        /* renamed from: 靐  reason: contains not printable characters */
        public final void m3568(String str, Object... objArr) {
            this.f2989.add(String.format(Locale.US, str, objArr));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3569(String str, Object... objArr) {
            this.f2990 = String.format(Locale.US, str, objArr);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3570(Collection<String> collection) {
            this.f2989.addAll(collection);
        }
    }

    /* renamed from: com.adincube.sdk.util.d.b$b  reason: collision with other inner class name */
    public static class C0021b {

        /* renamed from: 靐  reason: contains not printable characters */
        private List<String> f2991 = new ArrayList();

        /* renamed from: 龘  reason: contains not printable characters */
        public List<String> f2992 = new ArrayList();

        /* renamed from: 龘  reason: contains not printable characters */
        public final List<String> m3571() {
            ArrayList arrayList = new ArrayList();
            arrayList.add("    <intent-filter>");
            for (String str : this.f2992) {
                arrayList.add(String.format("        <category android:name=\"%s\" />", new Object[]{str}));
            }
            for (String str2 : this.f2991) {
                arrayList.add(String.format("        <action android:name=\"%s\" />", new Object[]{str2}));
            }
            arrayList.add("    </intent-filter>");
            return arrayList;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m3572(String str) {
            this.f2991.add(str);
        }
    }

    public b(String str, Context context) {
        this.f2986 = str;
        this.f2985 = context;
        try {
            this.f2987 = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
        } catch (Exception e) {
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private List<String> m3558() {
        ArrayList arrayList = new ArrayList();
        for (a next : this.f2988) {
            arrayList.add("    - " + next.f2990);
            for (String str : next.f2989) {
                arrayList.add("        " + str);
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<String> m3559(String str, String str2, Map<String, String> map, List<C0021b> list) {
        ArrayList arrayList = new ArrayList();
        arrayList.add("Add the following lines in <application> of your AndroidManifest.xml:");
        if (map == null || map.isEmpty()) {
            arrayList.add(String.format(Locale.US, "<%s android:name=\"%s\"></%s>", new Object[]{str, str2, str}));
        } else {
            arrayList.add("<" + str);
            arrayList.add(String.format(Locale.US, "    android:name=\"%s\"", new Object[]{str2}));
            Iterator<Map.Entry<String, String>> it2 = map.entrySet().iterator();
            while (it2.hasNext()) {
                Map.Entry next = it2.next();
                String format = String.format(Locale.US, "    %s=\"%s\"", new Object[]{next.getKey(), next.getValue()});
                if (!it2.hasNext()) {
                    if (list.size() == 0) {
                        format = format + String.format("></%s>", new Object[]{str});
                    } else {
                        format = format + ">";
                    }
                }
                arrayList.add(format);
            }
            if (list.size() > 0) {
                for (C0021b r0 : list) {
                    arrayList.addAll(r0.m3571());
                }
                arrayList.add(String.format("</%s>", new Object[]{str}));
            }
        }
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3560(String str) {
        m3561(str, (Map<String, String>) null, Collections.emptyList());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3561(String str, Map<String, String> map, List<C0021b> list) {
        try {
            if (this.f2985.getPackageManager().resolveService(new Intent(this.f2985, Class.forName(str)), 65536) == null) {
                a aVar = new a();
                aVar.m3569("Missing service '%s'.", str);
                aVar.m3570(m3559(NotificationCompat.CATEGORY_SERVICE, str, map, list));
                this.f2988.add(aVar);
            }
        } catch (ClassNotFoundException e) {
            a aVar2 = new a();
            aVar2.m3569("Missing service class '%s'", str);
            aVar2.m3568("Add '-keep public class %s { *; }' in your proguard config.", str);
            this.f2988.add(aVar2);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m3562(String str) {
        if (this.f2987 != null && !this.f2987.metaData.containsKey(str)) {
            a aVar = new a();
            aVar.m3569("Missing meta-data '%s'", str);
            aVar.m3568("Add <meta-data android:name=\"%s\" android:value=... /> in <application> of you AndroidManifest.xml", str);
            aVar.m3568("Check the online documentation to know the value you must configure.", new Object[0]);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3563(String str) {
        try {
            Class.forName(str);
        } catch (ClassNotFoundException e) {
            a aVar = new a();
            aVar.m3569("Missing receiver class '%s'", str);
            aVar.m3568("Add '-keep public class %s { *; }' in your proguard config.", str);
            this.f2988.add(aVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3564() {
        if (!this.f2988.isEmpty()) {
            throw new d(this.f2986, m3558());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3565(String str) {
        if (!f.m3536(this.f2985, str)) {
            a aVar = new a();
            aVar.m3569("Missing permission '%s'", str);
            aVar.m3568("Add <uses-permission android:name=\"%s\" /> in AndroidManifest.xml", str);
            this.f2988.add(aVar);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3566(String str, Map<String, String> map) {
        m3567(str, map, Collections.emptyList());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3567(String str, Map<String, String> map, List<C0021b> list) {
        try {
            if (this.f2985.getPackageManager().resolveActivity(new Intent(this.f2985, Class.forName(str)), 65536) == null) {
                a aVar = new a();
                aVar.m3569("Missing activity '%s'.", str);
                aVar.m3570(m3559("activity", str, map, list));
                this.f2988.add(aVar);
            }
        } catch (ClassNotFoundException e) {
            a aVar2 = new a();
            aVar2.m3569("Missing activity class '%s'", str);
            aVar2.m3568("Add '-keep public class %s { *; }' in your proguard config.", str);
            this.f2988.add(aVar2);
        }
    }
}
