package com.adincube.sdk.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import java.lang.reflect.Method;
import java.util.Stack;

@SuppressLint({"NewApi"})
public class k {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static Integer m3677(Context context) {
        if ((context instanceof Activity) && Build.VERSION.SDK_INT >= 21) {
            return Integer.valueOf(((Activity) context).getWindow().getStatusBarColor());
        }
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static Integer m3678(Context context) {
        if ((context instanceof Activity) && Build.VERSION.SDK_INT >= 21) {
            return Integer.valueOf(((Activity) context).getWindow().getNavigationBarColor());
        }
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static Point m3679(Context context) {
        Point point = new Point();
        if (Build.VERSION.SDK_INT < 19 || !m3680(context)) {
            DisplayMetrics displayMetrics = f.m3605().getResources().getDisplayMetrics();
            point.x = displayMetrics.widthPixels;
            point.y = displayMetrics.heightPixels;
        } else {
            ((DisplayManager) f.m3605().getSystemService("display")).getDisplay(0).getRealSize(point);
        }
        return point;
    }

    @SuppressLint({"InlinedApi"})
    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean m3680(Context context) {
        try {
            return (m3682(context) & 2048) != 0;
        } catch (RuntimeException e) {
            return context.getSharedPreferences("AIC-prefs", 0).getBoolean("im", false);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m3681(Context context) {
        return !(context instanceof Activity) || (((Activity) context).getWindow().getAttributes().flags & 1024) != 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m3682(Context context) {
        if (!(context instanceof Activity) || Build.VERSION.SDK_INT < 11) {
            return 0;
        }
        Stack stack = new Stack();
        stack.add(((Activity) context).getWindow().getDecorView());
        int i = 0;
        while (!stack.isEmpty() && i == 0) {
            View view = (View) stack.pop();
            int systemUiVisibility = view.getSystemUiVisibility();
            if (systemUiVisibility == 0 && (view instanceof ViewGroup)) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                    stack.add(viewGroup.getChildAt(i2));
                }
            }
            i = systemUiVisibility;
        }
        return i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m3683(Context context) {
        int i;
        if (!(context instanceof Activity)) {
            return true;
        }
        Window window = ((Activity) context).getWindow();
        if (Build.VERSION.SDK_INT >= 11) {
            return window.hasFeature(1) || window.hasFeature(8);
        }
        try {
            Method declaredMethod = window.getClass().getSuperclass().getDeclaredMethod("getFeatures", new Class[0]);
            declaredMethod.setAccessible(true);
            i = ((Integer) declaredMethod.invoke(window, new Object[0])).intValue();
            try {
                declaredMethod.setAccessible(false);
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            i = 0;
        }
        return (i & 2) != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3684(final Context context) {
        if (Build.VERSION.SDK_INT >= 19) {
            AnonymousClass1 r0 = new Runnable() {
                public final void run() {
                    try {
                        Context context = context;
                        boolean r2 = k.m3680(context);
                        SharedPreferences.Editor edit = context.getSharedPreferences("AIC-prefs", 0).edit();
                        edit.putBoolean("im", r2);
                        edit.commit();
                    } catch (Throwable th) {
                        ErrorReportingHelper.m3511("DisplayHelper.updateImmersiveModeFlag", th);
                        a.m3513("DisplayHelper.updateImmersiveModeFlag", th);
                    }
                }
            };
            if (!Looper.getMainLooper().equals(Looper.myLooper())) {
                new Handler(Looper.getMainLooper()).post(r0);
            } else {
                r0.run();
            }
        }
    }
}
