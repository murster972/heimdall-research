package com.adincube.sdk.util.g;

import android.os.CountDownTimer;
import com.adincube.sdk.a.a.a.e;
import com.adincube.sdk.a.a.a.g;
import com.adincube.sdk.a.a.a.h;
import com.adincube.sdk.util.j;
import com.adincube.sdk.util.o;

public final class a extends CountDownTimer implements e.a, j {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f3039 = false;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public C0023a f3040;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public com.adincube.sdk.e.a f3041;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f3042 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f3043;

    /* renamed from: 龘  reason: contains not printable characters */
    private e f3044;

    /* renamed from: com.adincube.sdk.util.g.a$a  reason: collision with other inner class name */
    public interface C0023a {
        /* renamed from: ˎ  reason: contains not printable characters */
        void m3637();
    }

    public a(e eVar, com.adincube.sdk.e.a aVar, long j) {
        super(j, 250);
        this.f3044 = eVar;
        this.f3041 = aVar;
        this.f3043 = j;
        eVar.m2455((e.a) this);
        if (eVar.m2449() == h.COMPLETED || j < 0) {
            m3629();
        } else {
            aVar.m2605(false);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m3629() {
        if (!this.f3042) {
            this.f3042 = true;
            cancel();
            o.m3701("SkipController.enableSkip", (Runnable) new Runnable() {
                public final void run() {
                    if (!a.this.f3039) {
                        a.this.f3041.m2605(true);
                    }
                    a.this.f3041.f2126.m2555((Integer) null);
                    if (a.this.f3040 != null) {
                        a.this.f3040.m3637();
                    }
                }
            });
        }
    }

    public final void onFinish() {
        m3629();
    }

    public final void onTick(long j) {
        try {
            long r0 = this.f3043 - this.f3044.m2457();
            if (this.f3044.m2449() == h.COMPLETED || r0 <= 0) {
                m3629();
            } else {
                this.f3041.f2126.m2555(Integer.valueOf((int) Math.ceil((double) (((float) r0) / 1000.0f))));
            }
        } catch (Throwable th) {
            com.adincube.sdk.util.a.m3513("ABVASTSkippableLinearHelper.onTick()", th);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3631() {
        cancel();
        m3629();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3632(e eVar) {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m3633() {
        onTick(this.f3043);
        start();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3634(e eVar) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3635(e eVar, g gVar) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3636(C0023a aVar) {
        this.f3040 = aVar;
        if (aVar != null && this.f3042) {
            aVar.m3637();
        }
    }
}
