package com.adincube.sdk.util;

import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

public final class s {
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3713(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        return Build.VERSION.SDK_INT >= 21 ? powerManager.isInteractive() : powerManager.isScreenOn();
    }
}
