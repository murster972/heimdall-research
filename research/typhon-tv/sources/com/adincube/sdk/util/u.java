package com.adincube.sdk.util;

public final class u {
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0059 A[SYNTHETIC, Splitter:B:26:0x0059] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m3717(java.io.InputStream r7) {
        /*
            r6 = 1
            r5 = 0
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r2 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x006b, all -> 0x0055 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x006b, all -> 0x0055 }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r7, r4)     // Catch:{ IOException -> 0x006b, all -> 0x0055 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x006b, all -> 0x0055 }
        L_0x0015:
            java.lang.String r2 = r1.readLine()     // Catch:{ IOException -> 0x001f }
            if (r2 == 0) goto L_0x0035
            r0.append(r2)     // Catch:{ IOException -> 0x001f }
            goto L_0x0015
        L_0x001f:
            r0 = move-exception
        L_0x0020:
            java.lang.String r2 = "readStream() IOException"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0069 }
            r4 = 0
            r3[r4] = r0     // Catch:{ all -> 0x0069 }
            com.adincube.sdk.util.a.m3513(r2, r3)     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0049 }
        L_0x0031:
            java.lang.String r0 = ""
        L_0x0034:
            return r0
        L_0x0035:
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x001f }
            r1.close()     // Catch:{ IOException -> 0x003d }
            goto L_0x0034
        L_0x003d:
            r1 = move-exception
            java.lang.String r2 = "readStream() IOException finally"
            java.lang.Object[] r3 = new java.lang.Object[r6]
            r3[r5] = r1
            com.adincube.sdk.util.a.m3513(r2, r3)
            goto L_0x0034
        L_0x0049:
            r0 = move-exception
            java.lang.String r1 = "readStream() IOException finally"
            java.lang.Object[] r2 = new java.lang.Object[r6]
            r2[r5] = r0
            com.adincube.sdk.util.a.m3513(r1, r2)
            goto L_0x0031
        L_0x0055:
            r0 = move-exception
            r1 = r2
        L_0x0057:
            if (r1 == 0) goto L_0x005c
            r1.close()     // Catch:{ IOException -> 0x005d }
        L_0x005c:
            throw r0
        L_0x005d:
            r1 = move-exception
            java.lang.String r2 = "readStream() IOException finally"
            java.lang.Object[] r3 = new java.lang.Object[r6]
            r3[r5] = r1
            com.adincube.sdk.util.a.m3513(r2, r3)
            goto L_0x005c
        L_0x0069:
            r0 = move-exception
            goto L_0x0057
        L_0x006b:
            r0 = move-exception
            r1 = r2
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.util.u.m3717(java.io.InputStream):java.lang.String");
    }
}
