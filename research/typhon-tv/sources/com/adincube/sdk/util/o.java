package com.adincube.sdk.util;

import android.os.Handler;
import android.os.Looper;
import com.adincube.sdk.util.c.a;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;

public final class o {
    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T m3698(final Callable<T> callable, T t) {
        final Semaphore semaphore = new Semaphore(0);
        final AtomicReference atomicReference = new AtomicReference((Object) null);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                try {
                    atomicReference.set(callable.call());
                } catch (Throwable th) {
                    a.m3513("HandlerUtil.dispatchOnUiThread", th);
                    ErrorReportingHelper.m3511("HandlerUtil.dispatchOnUiThread", th);
                } finally {
                    semaphore.release();
                }
            }
        });
        try {
            semaphore.acquire();
            return atomicReference.get();
        } catch (InterruptedException e) {
            return t;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> void m3699(final T t, final a<T> aVar) {
        if (t != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    aVar.m3552(t);
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> void m3700(final String str, final T t, final a<T> aVar) {
        if (t != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        aVar.m3552(t);
                    } catch (Throwable th) {
                        a.m3513(str, th);
                        ErrorReportingHelper.m3511(str, th);
                    }
                }
            });
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3701(final String str, final Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    a.m3513(str, th);
                    ErrorReportingHelper.m3511("HandlerUtil.dispatchOnUiThread", th);
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3702(final String str, final Runnable runnable, long j) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            public final void run() {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    a.m3513(str, th);
                    ErrorReportingHelper.m3511(str, th);
                }
            }
        }, j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> void m3703(final String str, Collection<T> collection, final a<T> aVar) {
        if (collection != null) {
            synchronized (collection) {
                Handler handler = new Handler(Looper.getMainLooper());
                for (final T next : collection) {
                    handler.post(new Runnable() {
                        public final void run() {
                            try {
                                aVar.m3552(next);
                            } catch (Throwable th) {
                                a.m3513(str, th);
                                ErrorReportingHelper.m3511(str, th);
                            }
                        }
                    });
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3704() {
        return Looper.getMainLooper() == Looper.myLooper();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3705(Callable<Boolean> callable) {
        return ((Boolean) m3698(callable, false)).booleanValue();
    }
}
