package com.adincube.sdk.util;

import android.content.Context;
import com.adincube.sdk.g.a.a.a;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public final class n {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m3687(a aVar) {
        return m3691(aVar) + aVar.m3179();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m3688(Context context, String str) {
        context.deleteFile("AIC_" + str + ".aic");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static synchronized void m3689(Context context, String str, String str2) {
        synchronized (n.class) {
            try {
                FileOutputStream openFileOutput = context.openFileOutput(str, 0);
                openFileOutput.flush();
                openFileOutput.write(str2.getBytes());
                openFileOutput.close();
            } catch (IOException e) {
                a.m3513("writeConfig() IOException", e);
            }
        }
        return;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        r0 = null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static synchronized java.lang.String m3690(android.content.Context r4, java.lang.String r5) {
        /*
            r1 = 0
            java.lang.Class<com.adincube.sdk.util.n> r2 = com.adincube.sdk.util.n.class
            monitor-enter(r2)
            java.io.FileInputStream r3 = r4.openFileInput(r5)     // Catch:{ FileNotFoundException -> 0x0011, IOException -> 0x0014, all -> 0x0017 }
            java.lang.String r0 = com.adincube.sdk.util.u.m3717(r3)     // Catch:{ FileNotFoundException -> 0x0011, IOException -> 0x0014, all -> 0x0017 }
            r3.close()     // Catch:{ FileNotFoundException -> 0x0011, IOException -> 0x0014, all -> 0x0017 }
        L_0x000f:
            monitor-exit(r2)
            return r0
        L_0x0011:
            r0 = move-exception
            r0 = r1
            goto L_0x000f
        L_0x0014:
            r0 = move-exception
            r0 = r1
            goto L_0x000f
        L_0x0017:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adincube.sdk.util.n.m3690(android.content.Context, java.lang.String):java.lang.String");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String m3691(a aVar) {
        return "AIC_RTB_" + aVar.m3173() + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized File m3692(Context context) {
        File createTempFile;
        synchronized (n.class) {
            createTempFile = File.createTempFile("AIC_TMP_", ".aic", context.getCacheDir());
        }
        return createTempFile;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static File m3693(File file, String str) {
        String absolutePath = file.getAbsolutePath();
        if (absolutePath.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
            absolutePath = absolutePath.substring(0, absolutePath.length() - 1);
        }
        return !str.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) ? new File(absolutePath + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str) : new File(absolutePath + str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static InputStream m3694(Context context, a aVar) {
        if (aVar.m3176()) {
            return context.openFileInput(m3687(aVar));
        }
        try {
            return new ByteArrayInputStream(aVar.m3174().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3695(Context context, String str) {
        return m3690(context, "AIC_" + str + ".aic");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3696(a aVar) {
        return m3691(aVar) + aVar.m3179() + "_tmp";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3697(Context context, String str, String str2) {
        m3689(context, "AIC_" + str + ".aic", str2);
    }
}
