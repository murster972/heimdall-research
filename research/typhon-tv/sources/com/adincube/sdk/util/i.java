package com.adincube.sdk.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import dalvik.system.BaseDexClassLoader;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@TargetApi(14)
public class i {

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean f3070 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    private static Object m3671(BaseDexClassLoader baseDexClassLoader) {
        Field declaredField = BaseDexClassLoader.class.getDeclaredField("pathList");
        declaredField.setAccessible(true);
        Object obj = declaredField.get(baseDexClassLoader);
        Field declaredField2 = obj.getClass().getDeclaredField("dexElements");
        declaredField2.setAccessible(true);
        return declaredField2.get(obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Object m3672(Object obj, Object obj2) {
        Class<?> componentType = obj.getClass().getComponentType();
        if (componentType != obj2.getClass().getComponentType()) {
            throw new IllegalArgumentException();
        }
        int length = Array.getLength(obj);
        int length2 = Array.getLength(obj2);
        Object newInstance = Array.newInstance(componentType, length + length2);
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            Array.set(newInstance, i, Array.get(obj, i2));
            i++;
        }
        for (int i3 = 0; i3 < length2; i3++) {
            Array.set(newInstance, i, Array.get(obj2, i3));
            i++;
        }
        return newInstance;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m3673(final Context context) {
        if (!f3070) {
            if (Build.VERSION.SDK_INT >= 14) {
                try {
                    ArrayList<String> arrayList = new ArrayList<>();
                    for (String str : context.getAssets().list("")) {
                        if (str.startsWith("aic-") && str.endsWith(".dex")) {
                            arrayList.add(str);
                        }
                    }
                    if (!arrayList.isEmpty()) {
                        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Math.min(arrayList.size(), 5));
                        a.m3515("Detected dex files:", new Object[0]);
                        for (final String str2 : arrayList) {
                            a.m3515("    " + str2, new Object[0]);
                            newFixedThreadPool.submit(new Runnable() {
                                public final void run() {
                                    try {
                                        i.m3674(context, str2);
                                    } catch (Exception e) {
                                        a.m3513("Unable to load DEX file [" + str2 + "]", e);
                                        ErrorReportingHelper.m3511("DexLoader.loadAssetFile", e);
                                    }
                                }
                            });
                        }
                        newFixedThreadPool.shutdown();
                        try {
                            newFixedThreadPool.awaitTermination(4900, TimeUnit.MILLISECONDS);
                        } catch (InterruptedException e) {
                            a.m3513("Dex loading took more than 10 seconds. Aborted.", e);
                        }
                    } else {
                        return;
                    }
                } catch (IOException e2) {
                    a.m3513("Unable to read assets files", e2);
                }
            }
            f3070 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m3674(Context context, String str) {
        File file = new File(context.getDir("aic-dex-tmp", 0), str);
        if (!file.exists()) {
            m3675(context, str, file);
        }
        File file2 = new File(context.getDir(new File("aic-dex").toString(), 0), str);
        file2.mkdir();
        ClassLoader classLoader = i.class.getClassLoader();
        if (classLoader instanceof BaseDexClassLoader) {
            Object r3 = m3671((BaseDexClassLoader) new DexClassLoader(file.getAbsolutePath(), file2.getAbsolutePath(), (String) null, classLoader));
            synchronized (i.class) {
                Object r1 = m3672(m3671((BaseDexClassLoader) classLoader), r3);
                Field declaredField = BaseDexClassLoader.class.getDeclaredField("pathList");
                declaredField.setAccessible(true);
                Object obj = declaredField.get((BaseDexClassLoader) classLoader);
                Field declaredField2 = obj.getClass().getDeclaredField("dexElements");
                declaredField2.setAccessible(true);
                declaredField2.set(obj, r1);
            }
            return;
        }
        throw new UnsupportedOperationException("Class loader not supported");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m3675(Context context, String str, File file) {
        InputStream open = context.getAssets().open(str);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = open.read(bArr);
            if (read != -1) {
                fileOutputStream.write(bArr, 0, read);
            } else {
                fileOutputStream.close();
                open.close();
                return;
            }
        }
    }
}
