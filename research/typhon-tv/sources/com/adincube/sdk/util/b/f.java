package com.adincube.sdk.util.b;

import android.content.Context;

public final class f {
    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3536(Context context, String str) {
        int i;
        try {
            i = context.checkCallingOrSelfPermission(str);
        } catch (RuntimeException e) {
            if (e.getMessage() == null || !e.getMessage().startsWith("Unknown exception code: 1")) {
                throw e;
            }
            i = -1;
        }
        return i == 0;
    }
}
