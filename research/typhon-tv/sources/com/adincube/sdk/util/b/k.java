package com.adincube.sdk.util.b;

import android.content.Context;
import android.os.Build;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.lang.reflect.Constructor;

public final class k {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m3547(Context context) {
        if (Build.VERSION.SDK_INT >= 17) {
            return null;
        }
        try {
            Class<?> cls = Class.forName("android.webkit.WebSettingsClassic");
            Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{Context.class, Class.forName("android.webkit.WebViewClassic")});
            declaredConstructor.setAccessible(true);
            return (String) cls.getMethod("getUserAgentString", new Class[0]).invoke(declaredConstructor.newInstance(new Object[]{context, null}), new Object[0]);
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m3548(Context context) {
        try {
            return new WebView(context).getSettings().getUserAgentString();
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3549(Context context) {
        if (Build.VERSION.SDK_INT < 17) {
            return null;
        }
        try {
            return WebSettings.getDefaultUserAgent(context);
        } catch (Throwable th) {
            return null;
        }
    }
}
