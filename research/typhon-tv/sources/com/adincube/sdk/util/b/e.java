package com.adincube.sdk.util.b;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import com.adincube.sdk.f.a;
import com.adincube.sdk.g.b.b;
import java.util.List;

public final class e {
    /* renamed from: 靐  reason: contains not printable characters */
    private static Location m3534(Context context) {
        Location location = null;
        boolean r0 = f.m3536(context, "android.permission.ACCESS_COARSE_LOCATION");
        boolean r1 = f.m3536(context, "android.permission.ACCESS_FINE_LOCATION");
        if (r0 || r1) {
            try {
                LocationManager locationManager = (LocationManager) context.getSystemService("location");
                List<String> allProviders = locationManager.getAllProviders();
                if (allProviders != null) {
                    for (String lastKnownLocation : allProviders) {
                        try {
                            Location lastKnownLocation2 = locationManager.getLastKnownLocation(lastKnownLocation);
                            if (lastKnownLocation2 == null || (location != null && lastKnownLocation2.getTime() <= location.getTime())) {
                                lastKnownLocation2 = location;
                            }
                            location = lastKnownLocation2;
                        } catch (SecurityException e) {
                        }
                    }
                }
            } catch (Throwable th) {
            }
        }
        return location;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Location m3535(Context context) {
        b r0 = a.m2607().m2610(true, true);
        if (r0 == null || !b.m3219(r0, com.adincube.sdk.g.b.a.Geolocation)) {
            return null;
        }
        return m3534(context);
    }
}
