package com.adincube.sdk.util.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;

public final class j {
    /* renamed from: 靐  reason: contains not printable characters */
    public static double m3541(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return ((double) displayMetrics.widthPixels) / ((double) displayMetrics.density);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m3542(Context context, int i) {
        return Math.round(TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics()));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static float m3543(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static double m3544(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return ((double) displayMetrics.heightPixels) / ((double) displayMetrics.density);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m3545(Context context, int i) {
        return i <= 0 ? i : Math.round(((float) i) / context.getResources().getDisplayMetrics().density);
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static DisplayMetrics m3546(Context context) {
        if (Build.VERSION.SDK_INT < 17) {
            return context.getResources().getDisplayMetrics();
        }
        Display display = ((DisplayManager) context.getSystemService("display")).getDisplay(0);
        if (display == null) {
            return null;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getRealMetrics(displayMetrics);
        return displayMetrics;
    }
}
