package com.adincube.sdk.util.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import com.adincube.sdk.f.a;
import com.adincube.sdk.g.b.b;
import com.adincube.sdk.util.p;

public final class d {
    @SuppressLint({"MissingPermission", "HardwareIds"})
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m3530(Context context) {
        b r0 = a.m2607().m2610(true, true);
        if (r0 == null || !b.m3219(r0, com.adincube.sdk.g.b.a.MacAddress)) {
            return null;
        }
        if (!f.m3536(context, "android.permission.ACCESS_WIFI_STATE")) {
            return null;
        }
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService("wifi");
        if (wifiManager == null) {
            return null;
        }
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        if (connectionInfo == null) {
            return null;
        }
        return connectionInfo.getMacAddress();
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    /* renamed from: 麤  reason: contains not printable characters */
    public static String m3531(Context context) {
        b r0 = a.m2607().m2610(true, true);
        if (r0 == null || !b.m3219(r0, com.adincube.sdk.g.b.a.IMEI)) {
            return null;
        }
        if (!f.m3536(context, "android.permission.READ_PHONE_STATE")) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return null;
        }
        return telephonyManager.getDeviceId();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m3532(Context context) {
        String r0 = m3531(context);
        if (r0 == null) {
            return null;
        }
        return p.m3706("MD5", r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m3533(Context context) {
        String r0 = m3530(context);
        if (r0 == null) {
            return null;
        }
        return p.m3706("MD5", r0);
    }
}
