package com.adincube.sdk.util.b;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public final class b {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m3526(Context context) {
        try {
            return m3527(context).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static PackageInfo m3527(Context context) {
        return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m3528(Context context) {
        try {
            return m3527(context).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }
}
