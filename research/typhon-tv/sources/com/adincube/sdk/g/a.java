package com.adincube.sdk.g;

import java.util.Date;

public final class a {

    /* renamed from: 齉  reason: contains not printable characters */
    private static long f2546 = 86400000;

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean f2547;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f2548;

    /* renamed from: 龘  reason: contains not printable characters */
    public final String f2549;

    public a(String str, boolean z) {
        this(str, z, new Date().getTime());
    }

    public a(String str, boolean z, long j) {
        this.f2549 = str;
        this.f2547 = z;
        this.f2548 = j;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        if (this.f2547 == aVar.f2547) {
            return this.f2549.equals(aVar.f2549);
        }
        return false;
    }

    public final int hashCode() {
        return (this.f2547 ? 1 : 0) + (this.f2549.hashCode() * 31);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3171() {
        boolean z;
        if (this.f2549 == null) {
            return false;
        }
        if (this.f2548 <= 0) {
            z = true;
        } else {
            long time = new Date().getTime() - this.f2548;
            z = time < 0 ? true : time > f2546;
        }
        return !z;
    }
}
