package com.adincube.sdk.g.d;

import android.graphics.BitmapFactory;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public final class a extends b {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Map<String, C0012a> f2677 = new HashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    public Integer f2678;

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f2679;

    /* renamed from: com.adincube.sdk.g.d.a$a  reason: collision with other inner class name */
    public static class C0012a {

        /* renamed from: 靐  reason: contains not printable characters */
        Integer f2680;

        /* renamed from: 龘  reason: contains not printable characters */
        Integer f2681;

        C0012a(File file) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            try {
                BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                this.f2681 = Integer.valueOf(options.outWidth);
                this.f2680 = Integer.valueOf(options.outHeight);
            } catch (Throwable th) {
                com.adincube.sdk.util.a.m3513("ImageSize", th);
            }
        }
    }

    public a(String str) {
        super(str);
        synchronized (f2677) {
            C0012a aVar = f2677.get(str);
            if (aVar != null) {
                this.f2679 = aVar.f2681;
                this.f2678 = aVar.f2680;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m3233() {
        C0012a aVar = new C0012a(this.f2682);
        synchronized (f2677) {
            f2677.put(this.f2693, aVar);
        }
        this.f2679 = aVar.f2681;
        this.f2678 = aVar.f2680;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3234() {
        return (this.f2679 == null || this.f2678 == null) ? false : true;
    }
}
