package com.adincube.sdk.g;

import com.adincube.sdk.AdinCube;

public enum c {
    SINGLE("SINGLE"),
    MARRIED("MARRIED");
    
    public String c;

    private c(String str) {
        this.c = str;
    }

    public static c a(AdinCube.UserInfo.MaritalStatus maritalStatus) {
        if (maritalStatus == null) {
            return null;
        }
        switch (maritalStatus) {
            case SINGLE:
                return SINGLE;
            case MARRIED:
                return MARRIED;
            default:
                return null;
        }
    }

    public static c[] a() {
        return (c[]) f2650.clone();
    }
}
