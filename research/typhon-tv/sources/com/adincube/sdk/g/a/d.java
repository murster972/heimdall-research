package com.adincube.sdk.g.a;

import com.adincube.sdk.c.b.c;
import com.adincube.sdk.g.c.b;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.ArrayList;
import java.util.List;
import net.pubnative.library.request.PubnativeAsset;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class d extends e {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f2566;

    /* renamed from: ʼ  reason: contains not printable characters */
    public List<String> f2567 = new ArrayList();

    /* renamed from: ʽ  reason: contains not printable characters */
    public Float f2568;

    /* renamed from: ˑ  reason: contains not printable characters */
    public a f2569;

    /* renamed from: ٴ  reason: contains not printable characters */
    public a f2570;

    /* renamed from: 连任  reason: contains not printable characters */
    public String f2571;

    /* renamed from: 靐  reason: contains not printable characters */
    public String f2572;

    /* renamed from: 麤  reason: contains not printable characters */
    public List<String> f2573 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    public String f2574;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f2575;

    public class a {

        /* renamed from: 靐  reason: contains not printable characters */
        public Integer f2576;

        /* renamed from: 齉  reason: contains not printable characters */
        public String f2578;

        /* renamed from: 龘  reason: contains not printable characters */
        public Integer f2579;

        public a(JSONObject jSONObject) {
            if (!jSONObject.has("url")) {
                throw new c("RTB", (Throwable) new Exception("Image must have an url."));
            }
            this.f2578 = jSONObject.getString("url");
            if (jSONObject.has(VastIconXmlManager.WIDTH)) {
                this.f2579 = Integer.valueOf(jSONObject.getInt(VastIconXmlManager.WIDTH));
            }
            if (jSONObject.has(VastIconXmlManager.HEIGHT)) {
                this.f2576 = Integer.valueOf(jSONObject.getInt(VastIconXmlManager.HEIGHT));
            }
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(JSONObject jSONObject) {
        super(jSONObject);
        try {
            JSONObject jSONObject2 = new JSONObject(this.f2582);
            if (!jSONObject2.has("clickUrl")) {
                throw new c("RTB", (Throwable) new Exception("Native ad must have a click url."));
            }
            this.f2571 = jSONObject2.getString("clickUrl");
            if (jSONObject2.has(PubnativeAsset.TITLE)) {
                this.f2575 = jSONObject2.getString(PubnativeAsset.TITLE);
            }
            if (jSONObject2.has("callToAction")) {
                this.f2574 = jSONObject2.getString("callToAction");
            }
            if (jSONObject2.has(PubnativeAsset.DESCRIPTION)) {
                this.f2572 = jSONObject2.getString(PubnativeAsset.DESCRIPTION);
            }
            if (jSONObject2.has("impressionTrackingUrls")) {
                JSONArray jSONArray = jSONObject2.getJSONArray("impressionTrackingUrls");
                for (int i = 0; i < jSONArray.length(); i++) {
                    this.f2573.add(jSONArray.getString(i));
                }
            }
            if (jSONObject2.has("fallbackClickUrl")) {
                this.f2566 = jSONObject2.getString("fallbackClickUrl");
            }
            if (jSONObject2.has("clickTrackingUrls")) {
                JSONArray jSONArray2 = jSONObject2.getJSONArray("clickTrackingUrls");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    this.f2567.add(jSONArray2.getString(i2));
                }
            }
            if (jSONObject2.has(PubnativeAsset.RATING)) {
                this.f2568 = Float.valueOf((float) jSONObject2.getDouble(PubnativeAsset.RATING));
            }
            if (jSONObject2.has(PubnativeAsset.ICON)) {
                this.f2569 = new a(jSONObject2.getJSONObject(PubnativeAsset.ICON));
            }
            if (jSONObject2.has("cover")) {
                this.f2570 = new a(jSONObject2.getJSONObject("cover"));
            }
        } catch (JSONException e) {
            throw new c("RTB", (Throwable) e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final a m3203() {
        return super.m3208();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final b m3204() {
        return b.NATIVE;
    }
}
