package com.adincube.sdk.g.a.a;

import com.adincube.sdk.g.a.e;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class a {

    /* renamed from: 靐  reason: contains not printable characters */
    public b f2551 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private Map<String, String> f2552 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f2553 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    public e f2554 = null;

    /* renamed from: com.adincube.sdk.g.a.a.a$a  reason: collision with other inner class name */
    private static class C0011a extends a {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f2555 = false;

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f2556 = null;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f2557 = false;

        /* renamed from: ˑ  reason: contains not printable characters */
        private String f2558 = null;

        /* renamed from: 连任  reason: contains not printable characters */
        private String f2559 = null;

        /* renamed from: 麤  reason: contains not printable characters */
        private String f2560 = null;

        public C0011a(e eVar, b bVar, boolean z, JSONObject jSONObject) {
            super(eVar, bVar, z);
            this.f2560 = jSONObject.getString("ai");
            this.f2559 = jSONObject.getString("n");
            if (jSONObject.has("u")) {
                this.f2555 = true;
                this.f2556 = jSONObject.getString("u");
            }
            if (jSONObject.has("c")) {
                this.f2557 = true;
                this.f2558 = jSONObject.getString("c");
            }
            JSONObject jSONObject2 = jSONObject.getJSONObject("ps");
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                m3181(str, jSONObject2.getString(str));
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public final String m3183() {
            return this.f2560;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public final String m3184() {
            return this.f2558;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final boolean m3185() {
            return this.f2555;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final boolean m3186() {
            return this.f2557;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final URL m3187() {
            if (this.f2556 == null) {
                return null;
            }
            return new URL(this.f2556);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m3188() {
            return this.f2559;
        }
    }

    public a(e eVar, b bVar, boolean z) {
        this.f2554 = eVar;
        this.f2551 = bVar;
        this.f2553 = z;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof a) {
            return m3179().equals(((a) obj).m3179());
        }
        return false;
    }

    public int hashCode() {
        return m3179().hashCode();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final JSONObject m3172() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("t", (Object) this.f2551.c);
        jSONObject.put(TtmlNode.TAG_P, this.f2553);
        jSONObject.put("ai", (Object) m3173());
        jSONObject.put("n", (Object) m3179());
        if (m3176()) {
            try {
                jSONObject.put("u", (Object) m3178());
            } catch (MalformedURLException e) {
            }
        }
        if (m3177()) {
            jSONObject.put("c", (Object) m3174());
        }
        HashMap hashMap = new HashMap(this.f2552);
        JSONObject jSONObject2 = new JSONObject();
        for (Map.Entry entry : hashMap.entrySet()) {
            jSONObject2.put((String) entry.getKey(), entry.getValue());
        }
        jSONObject.put("ps", (Object) jSONObject2);
        return jSONObject;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m3173() {
        return this.f2554.f2584;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract String m3174();

    /* renamed from: 靐  reason: contains not printable characters */
    public final List<String> m3175(String str) {
        ArrayList arrayList = new ArrayList();
        String str2 = this.f2552.get(str);
        if (str2 != null) {
            try {
                JSONArray jSONArray = new JSONArray(str2);
                for (int i = 0; i < jSONArray.length(); i++) {
                    arrayList.add(jSONArray.getString(i));
                }
            } catch (JSONException e) {
            }
        }
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract boolean m3176();

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract boolean m3177();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract URL m3178();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m3179();

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m3180(String str) {
        return this.f2552.get(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3181(String str, String str2) {
        this.f2552.put(str, str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3182(String str, List<String> list) {
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put((Object) put);
        }
        this.f2552.put(str, jSONArray.toString());
    }
}
