package com.adincube.sdk.g.a;

import com.adincube.sdk.f.e.a.b;
import com.adincube.sdk.f.e.a.c;

public enum a {
    MRAID(new b(), com.adincube.sdk.g.a.a.b.MEDIA) {
        public final com.adincube.sdk.a.a a() {
            return new com.adincube.sdk.a.b();
        }
    },
    VAST(new c(), com.adincube.sdk.g.a.a.b.META_DATA) {
        public final com.adincube.sdk.a.a a() {
            return new com.adincube.sdk.a.c();
        }
    };
    
    public com.adincube.sdk.f.e.a.a c;
    com.adincube.sdk.g.a.a.b d;
    private String e;

    private a(String str, com.adincube.sdk.f.e.a.a aVar, com.adincube.sdk.g.a.a.b bVar) {
        this.e = str;
        this.c = aVar;
        this.d = bVar;
    }

    public com.adincube.sdk.a.a a() {
        return null;
    }
}
