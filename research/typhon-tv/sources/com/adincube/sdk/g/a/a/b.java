package com.adincube.sdk.g.a.a;

public enum b {
    META_DATA("D"),
    MEDIA("M");
    
    String c;

    private b(String str) {
        this.c = str;
    }

    public static b a(String str) {
        for (b bVar : (b[]) f2561.clone()) {
            if (bVar.c.equals(str)) {
                return bVar;
            }
        }
        throw new IllegalArgumentException(str + " is not a valid resource type");
    }
}
