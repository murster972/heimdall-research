package com.adincube.sdk.g.a;

import com.adincube.sdk.c.b.c;
import com.adincube.sdk.g.a.a.a;
import com.adincube.sdk.g.a.a.d;
import com.adincube.sdk.g.c.b;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class e extends com.adincube.sdk.mediation.e {

    /* renamed from: ʾ  reason: contains not printable characters */
    public String f2580;

    /* renamed from: ʿ  reason: contains not printable characters */
    public String f2581;

    /* renamed from: ˈ  reason: contains not printable characters */
    public String f2582;

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean f2583;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public String f2584;

    /* renamed from: 龘  reason: contains not printable characters */
    private Set<a> f2585 = new HashSet();

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Integer f2586;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public Integer f2587;

    public e(JSONObject jSONObject) {
        super(jSONObject);
        try {
            m3214(jSONObject);
        } catch (JSONException e) {
            throw new c("RTB", (Throwable) e);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private Set<a> m3205() {
        HashSet hashSet;
        synchronized (this.f2585) {
            hashSet = new HashSet(this.f2585);
        }
        return hashSet;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof c)) {
            return this.f2584.equals(((c) obj).f2584);
        }
        return false;
    }

    public int hashCode() {
        return this.f2584.hashCode();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Map<String, String> m3206() {
        HashMap hashMap = new HashMap();
        hashMap.put("Id", this.f2584);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final String m3207() {
        return "RTB";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public a m3208() {
        return this.f2582.contains("<VAST") ? a.VAST : a.MRAID;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m3209(JSONObject jSONObject) {
        synchronized (this.f2585) {
            if (!this.f2585.isEmpty()) {
                JSONArray jSONArray = new JSONArray();
                for (a r0 : this.f2585) {
                    jSONArray.put((Object) r0.m3172());
                    jSONObject.put(InternalZipTyphoonApp.READ_MODE, (Object) jSONArray);
                }
            }
        }
        jSONObject.put("i", (Object) this.f2584);
        jSONObject.put("c", (Object) this.f2582);
        jSONObject.put("bua", this.f2583);
        if (this.f2581 != null) {
            jSONObject.put("afs", (Object) this.f2581);
        }
        if (!(this.f2586 == null || this.f2587 == null)) {
            jSONObject.put("ow", (Object) this.f2587);
            jSONObject.put("oh", (Object) this.f2586);
        }
        if (this.f2580 != null) {
            jSONObject.put("ts", (Object) this.f2580);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Collection<? extends a> m3210() {
        ArrayList arrayList = new ArrayList();
        if (this.f2582 != null && this.f2582.length() > 0) {
            arrayList.add(new d(this, m3208().d, this.f2582));
        }
        arrayList.addAll(m3205());
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final JSONObject m3211() {
        JSONObject jSONObject = new JSONObject();
        try {
            m3209(jSONObject);
        } catch (JSONException e) {
        }
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract b m3212();

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m3213(a aVar) {
        synchronized (this.f2585) {
            this.f2585.add(aVar);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m3214(JSONObject jSONObject) {
        if (jSONObject.has(InternalZipTyphoonApp.READ_MODE)) {
            JSONArray jSONArray = jSONObject.getJSONArray(InternalZipTyphoonApp.READ_MODE);
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                m3213((a) new a.C0011a(this, com.adincube.sdk.g.a.a.b.a(jSONObject2.getString("t")), jSONObject2.getBoolean(TtmlNode.TAG_P), jSONObject2));
            }
        }
        this.f2584 = jSONObject.getString("i");
        this.f2582 = jSONObject.getString("c");
        this.f2583 = jSONObject.getBoolean("bua");
        if (jSONObject.has("afs")) {
            this.f2581 = jSONObject.getString("afs");
        }
        if (jSONObject.has("ow") && jSONObject.has("oh")) {
            this.f2586 = Integer.valueOf(jSONObject.getInt("oh"));
            this.f2587 = Integer.valueOf(jSONObject.getInt("ow"));
        }
        if (jSONObject.has("ts")) {
            this.f2580 = jSONObject.getString("ts");
        }
        if (jSONObject.has("cr")) {
            JSONArray jSONArray2 = jSONObject.getJSONArray("cr");
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                try {
                    m3213((a) new com.adincube.sdk.g.a.a.c(this, com.adincube.sdk.g.a.a.b.MEDIA, false, new URL(jSONArray2.getString(i2))));
                } catch (MalformedURLException e) {
                }
            }
        }
    }
}
