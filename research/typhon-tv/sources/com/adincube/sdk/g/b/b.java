package com.adincube.sdk.g.b;

import com.adincube.sdk.f.b.d.b.a;
import com.adincube.sdk.mediation.v.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class b {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final int f2591 = (a.Geolocation.g | a.UserInformation.g);

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public int f2592;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f2593 = 0;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public List<Integer> f2594 = new ArrayList();

    /* renamed from: ʽ  reason: contains not printable characters */
    public long f2595;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public int f2596;

    /* renamed from: ʾ  reason: contains not printable characters */
    public long f2597;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public a f2598;

    /* renamed from: ʿ  reason: contains not printable characters */
    public List<c> f2599 = new ArrayList();

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public int f2600;

    /* renamed from: ˆ  reason: contains not printable characters */
    public long f2601;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public List<String> f2602 = new ArrayList();

    /* renamed from: ˈ  reason: contains not printable characters */
    public Map<com.adincube.sdk.g.c.b, Integer> f2603 = new HashMap();

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public long f2604;

    /* renamed from: ˉ  reason: contains not printable characters */
    public long f2605;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public List<String> f2606 = new ArrayList();

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean f2607;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public f f2608;

    /* renamed from: ˋ  reason: contains not printable characters */
    public long f2609;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    public Map<String, JSONObject> f2610 = new HashMap();

    /* renamed from: ˎ  reason: contains not printable characters */
    public long f2611;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    public Integer f2612;

    /* renamed from: ˏ  reason: contains not printable characters */
    public long f2613;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public Integer f2614;

    /* renamed from: ˑ  reason: contains not printable characters */
    public int f2615;

    /* renamed from: ˑˑ  reason: contains not printable characters */
    public int f2616;

    /* renamed from: י  reason: contains not printable characters */
    public long f2617;

    /* renamed from: יי  reason: contains not printable characters */
    public Integer f2618;

    /* renamed from: ـ  reason: contains not printable characters */
    public long f2619;

    /* renamed from: ــ  reason: contains not printable characters */
    public List<String> f2620 = new ArrayList();

    /* renamed from: ٴ  reason: contains not printable characters */
    public int f2621;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int f2622;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public boolean f2623;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public long f2624;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public int f2625;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public long f2626;

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    public int f2627;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public boolean f2628;

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    public Integer f2629;

    /* renamed from: ᵢ  reason: contains not printable characters */
    public boolean f2630;

    /* renamed from: ⁱ  reason: contains not printable characters */
    public boolean f2631;

    /* renamed from: 连任  reason: contains not printable characters */
    public int f2632;

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean f2633 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    public String f2634;

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f2635 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    public long f2636;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public long f2637;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public List<c> f2638 = new ArrayList();

    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean f2639;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public double f2640;

    /* renamed from: 靐  reason: contains not printable characters */
    public static long m3215(b bVar) {
        if (bVar != null) {
            return bVar.f2619;
        }
        return 3000;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static long m3216(b bVar) {
        if (bVar != null) {
            return bVar.f2624;
        }
        return 8000;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m3217(b bVar) {
        if (bVar != null) {
            return bVar.f2617;
        }
        return 1000;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m3218(JSONObject jSONObject) {
        b bVar = new b();
        bVar.f2636 = jSONObject.getLong("ct");
        bVar.f2634 = jSONObject.getString("a");
        bVar.f2632 = jSONObject.getInt("npits");
        bVar.f2593 = jSONObject.getInt("ai");
        bVar.f2595 = jSONObject.getLong("mcs");
        bVar.f2615 = jSONObject.getInt("mnpd");
        bVar.f2621 = jSONObject.getInt("milps");
        bVar.f2622 = jSONObject.getInt("malps");
        bVar.f2597 = jSONObject.getLong("tilp");
        if (jSONObject.has("atsmalps")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("atsmalps");
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                try {
                    String str = (String) keys.next();
                    bVar.f2603.put(com.adincube.sdk.g.c.b.a(str), Integer.valueOf(jSONObject2.getInt(str)));
                } catch (IllegalStateException e) {
                }
            }
        }
        bVar.f2607 = jSONObject.getBoolean("rar");
        bVar.f2639 = jSONObject.getBoolean("rpe");
        bVar.f2609 = jSONObject.getLong("mtlsr");
        bVar.f2611 = jSONObject.getLong("mtfrr");
        bVar.f2601 = jSONObject.getLong("mter");
        bVar.f2605 = jSONObject.getLong("mtlnf");
        bVar.f2613 = jSONObject.getLong("mtsah");
        bVar.f2617 = jSONObject.getLong("mtsads");
        bVar.f2619 = jSONObject.getLong("mtisd");
        bVar.f2624 = jSONObject.getLong("mtfds");
        bVar.f2626 = jSONObject.getLong("icp");
        bVar.f2628 = jSONObject.getBoolean("ifnfnml");
        bVar.f2630 = jSONObject.getBoolean("rfnfnml");
        bVar.f2631 = jSONObject.getBoolean("bflnf");
        bVar.f2637 = jSONObject.getLong("barp");
        bVar.f2623 = jSONObject.getBoolean("basr");
        bVar.f2640 = jSONObject.getDouble("bmvapr");
        bVar.f2598 = new a(jSONObject);
        bVar.f2625 = jSONObject.getInt("nalfb");
        bVar.f2592 = jSONObject.getInt("nalpb");
        bVar.f2596 = jSONObject.getInt("nmalk");
        JSONArray jSONArray = jSONObject.getJSONArray("nspp");
        for (int i = 0; i < jSONArray.length(); i++) {
            bVar.f2594.add(Integer.valueOf(jSONArray.getInt(i)));
        }
        bVar.f2600 = jSONObject.getInt("nsrf");
        JSONArray jSONArray2 = jSONObject.getJSONArray("bdll");
        for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
            bVar.f2620.add(jSONArray2.getString(i2));
        }
        JSONArray jSONArray3 = jSONObject.getJSONArray("bdls");
        for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
            bVar.f2602.add(jSONArray3.getString(i3));
        }
        JSONArray jSONArray4 = jSONObject.getJSONArray("nwlr");
        for (int i4 = 0; i4 < jSONArray4.length(); i4++) {
            bVar.f2606.add(jSONArray4.getString(i4));
        }
        bVar.f2604 = jSONObject.getLong("tnc");
        JSONArray jSONArray5 = jSONObject.getJSONArray("c");
        for (int i5 = 0; i5 < jSONArray5.length(); i5++) {
            JSONObject jSONObject3 = jSONArray5.getJSONObject(i5);
            String string = jSONObject3.getString("n");
            bVar.f2610.put(string, jSONObject3);
            if ("RTB".equals(string)) {
                bVar.f2608 = new f(jSONObject3);
            }
        }
        JSONArray jSONArray6 = jSONObject.getJSONArray("rclsr");
        for (int i6 = 0; i6 < jSONArray6.length(); i6++) {
            bVar.f2599.add(c.a(jSONArray6.getString(i6)));
        }
        JSONArray jSONArray7 = jSONObject.getJSONArray("rcfrr");
        for (int i7 = 0; i7 < jSONArray7.length(); i7++) {
            bVar.f2638.add(c.a(jSONArray7.getString(i7)));
        }
        if (jSONObject.has("rrct")) {
            bVar.f2614 = Integer.valueOf(jSONObject.getInt("rrct"));
        }
        if (jSONObject.has("rrrt")) {
            bVar.f2612 = Integer.valueOf(jSONObject.getInt("rrrt"));
        }
        bVar.f2616 = jSONObject.getInt("rrnt");
        if (jSONObject.has("nrct")) {
            bVar.f2629 = Integer.valueOf(jSONObject.getInt("nrct"));
        }
        if (jSONObject.has("nrrt")) {
            bVar.f2618 = Integer.valueOf(jSONObject.getInt("nrrt"));
        }
        bVar.f2627 = jSONObject.getInt("nrnt");
        return bVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3219(b bVar, a aVar) {
        int i = f2591;
        if (bVar != null) {
            i = bVar.f2593;
        }
        return (i & aVar.g) != 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m3220(com.adincube.sdk.g.c.b bVar) {
        Integer num = this.f2603.get(bVar);
        return num != null ? num.intValue() : this.f2622;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3221() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("ct", this.f2636);
            jSONObject.put("a", (Object) this.f2634);
            jSONObject.put("npits", this.f2632);
            jSONObject.put("ai", this.f2593);
            jSONObject.put("mcs", this.f2595);
            jSONObject.put("mnpd", this.f2615);
            jSONObject.put("milps", this.f2621);
            jSONObject.put("malps", this.f2622);
            jSONObject.put("tilp", this.f2597);
            JSONObject jSONObject2 = new JSONObject();
            for (Map.Entry next : this.f2603.entrySet()) {
                jSONObject2.put(((com.adincube.sdk.g.c.b) next.getKey()).e, next.getValue());
            }
            jSONObject.put("atsmalps", (Object) jSONObject2);
            jSONObject.put("rar", this.f2607);
            jSONObject.put("rpe", this.f2639);
            jSONObject.put("mtlsr", this.f2609);
            jSONObject.put("mtfrr", this.f2611);
            jSONObject.put("mter", this.f2601);
            jSONObject.put("mtlnf", this.f2605);
            jSONObject.put("mtsah", this.f2613);
            jSONObject.put("mtsads", this.f2617);
            jSONObject.put("mtisd", this.f2619);
            jSONObject.put("mtfds", this.f2624);
            jSONObject.put("bflnf", this.f2631);
            jSONObject.put("barp", this.f2637);
            jSONObject.put("basr", this.f2623);
            jSONObject.put("bmvapr", this.f2640);
            jSONObject.put("icp", this.f2626);
            jSONObject.put("ifnfnml", this.f2628);
            jSONObject.put("rfnfnml", this.f2630);
            jSONObject.put("nalfb", this.f2625);
            jSONObject.put("nalpb", this.f2592);
            jSONObject.put("nmalk", this.f2596);
            JSONArray jSONArray = new JSONArray();
            for (Integer put : this.f2594) {
                jSONArray.put((Object) put);
            }
            jSONObject.put("nspp", (Object) jSONArray);
            jSONObject.put("nsrf", this.f2600);
            a aVar = this.f2598;
            jSONObject.put("nmvap", (Object) aVar.f2381);
            jSONObject.put("nmvsm", (Object) aVar.f2378);
            jSONObject.put("nmvmpvpap", (Object) aVar.f2377);
            jSONObject.put("nmvsnc", (Object) aVar.f2380);
            JSONArray jSONArray2 = new JSONArray();
            for (String put2 : this.f2620) {
                jSONArray2.put((Object) put2);
            }
            jSONObject.put("bdll", (Object) jSONArray2);
            JSONArray jSONArray3 = new JSONArray();
            for (String put3 : this.f2602) {
                jSONArray3.put((Object) put3);
            }
            jSONObject.put("bdls", (Object) jSONArray3);
            JSONArray jSONArray4 = new JSONArray();
            for (String put4 : this.f2606) {
                jSONArray3.put((Object) put4);
            }
            jSONObject.put("nwlr", (Object) jSONArray4);
            jSONObject.put("tnc", this.f2604);
            JSONArray jSONArray5 = new JSONArray();
            for (JSONObject put5 : this.f2610.values()) {
                jSONArray5.put((Object) put5);
            }
            jSONObject.put("c", (Object) jSONArray5);
            JSONArray jSONArray6 = new JSONArray();
            for (c cVar : this.f2599) {
                jSONArray6.put((Object) cVar.c);
            }
            jSONObject.put("rclsr", (Object) jSONArray6);
            JSONArray jSONArray7 = new JSONArray();
            for (c cVar2 : this.f2638) {
                jSONArray7.put((Object) cVar2.c);
            }
            jSONObject.put("rcfrr", (Object) jSONArray7);
            if (this.f2614 != null) {
                jSONObject.put("rrct", (Object) this.f2614);
            }
            if (this.f2612 != null) {
                jSONObject.put("rrrt", (Object) this.f2612);
            }
            jSONObject.put("rrnt", this.f2616);
            if (this.f2629 != null) {
                jSONObject.put("nrct", (Object) this.f2629);
            }
            if (this.f2618 != null) {
                jSONObject.put("nrrt", (Object) this.f2618);
            }
            jSONObject.put("nrnt", this.f2627);
        } catch (JSONException e) {
        }
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3222(String str) {
        return this.f2610.get(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m3223(com.adincube.sdk.g.c.b bVar) {
        if (bVar == com.adincube.sdk.g.c.b.INTERSTITIAL) {
            return this.f2628;
        }
        if (bVar == com.adincube.sdk.g.c.b.REWARDED) {
            return this.f2630;
        }
        return false;
    }
}
