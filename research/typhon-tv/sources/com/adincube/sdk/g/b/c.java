package com.adincube.sdk.g.b;

public enum c {
    SHOW("SHOW"),
    IS_READY("IS_READY");
    
    public String c;

    private c(String str) {
        this.c = str;
    }

    public static c a(String str) {
        for (c cVar : a()) {
            if (cVar.c.equals(str)) {
                return cVar;
            }
        }
        throw new IllegalArgumentException(str + " is not a valid reporting context.");
    }

    public static c[] a() {
        return (c[]) f2641.clone();
    }
}
