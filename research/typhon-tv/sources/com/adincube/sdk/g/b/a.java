package com.adincube.sdk.g.b;

public enum a {
    IMEI(1),
    MacAddress(2),
    Geolocation(4),
    AndroidId(8),
    InstalledPackages(16),
    UserInformation(32);
    
    int g;

    private a(int i) {
        this.g = i;
    }
}
