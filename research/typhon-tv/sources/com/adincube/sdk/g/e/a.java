package com.adincube.sdk.g.e;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class a {

    /* renamed from: 龘  reason: contains not printable characters */
    public long f2698;

    private a(long j) {
        this.f2698 = j;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static a m3244(String str, long j) {
        Matcher matcher = Pattern.compile("([0-9]{2}):([0-9]{2}):([0-9]{2})(\\.([0-9]{3}))?").matcher(str);
        if (!matcher.matches()) {
            return null;
        }
        try {
            long parseInt = (long) Integer.parseInt(matcher.group(1));
            long parseInt2 = (long) Integer.parseInt(matcher.group(2));
            long parseInt3 = (long) Integer.parseInt(matcher.group(3));
            long j2 = 0;
            if (matcher.groupCount() > 3 && matcher.group(5) != null) {
                j2 = (long) Integer.parseInt(matcher.group(5));
            }
            long j3 = j2 + (parseInt * 3600000) + (parseInt2 * 60000) + (1000 * parseInt3);
            if (j3 <= j) {
                j = j3;
            }
            return new a(j);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static a m3245(String str, long j) {
        try {
            int parseInt = Integer.parseInt(str.substring(0, str.length() - 1));
            if (parseInt >= 0 && parseInt <= 100) {
                return new a((long) ((((double) parseInt) / 100.0d) * ((double) j)));
            }
            throw new IllegalArgumentException();
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static a m3246(String str, long j) {
        return str.contains("%") ? m3245(str, j) : m3244(str, j);
    }
}
