package com.adincube.sdk.g.c;

import android.content.Context;
import android.util.DisplayMetrics;
import com.adincube.sdk.AdinCube;
import com.adincube.sdk.g.f;
import com.adincube.sdk.util.b.j;
import java.util.HashSet;
import java.util.Set;

public enum c {
    INVALID,
    BANNER_AUTO("AUTO", a.BANNER_AUTO, 0, 0),
    BANNER_320x50("320x50", a.BANNER_320x50, 320, 50),
    BANNER_300x250("300x250", a.BANNER_300x250, 300, 250),
    BANNER_728x90("728x90", a.BANNER_728x90, 728, 90);
    
    public boolean f;
    public int g;
    public String h;
    public a i;
    private int j;

    private c(String str, a aVar, int i2, int i3) {
        this.f = true;
        this.h = str;
        this.i = aVar;
        this.g = i2;
        this.j = i3;
    }

    private c() {
        this.f = false;
        this.h = r3;
    }

    public static c a(int i2) {
        if (i2 == -1) {
            return INVALID;
        }
        if (i2 == -2) {
            return BANNER_AUTO;
        }
        c cVar = INVALID;
        for (c cVar2 : a()) {
            if (cVar2.f && Math.abs(i2 - cVar2.j) <= 2) {
                return cVar2;
            }
        }
        return cVar;
    }

    public static c a(AdinCube.Banner.Size size) {
        switch (size) {
            case BANNER_AUTO:
                return BANNER_AUTO;
            case BANNER_320x50:
                return BANNER_320x50;
            case BANNER_300x250:
                return BANNER_300x250;
            case BANNER_728x90:
                return BANNER_728x90;
            default:
                return INVALID;
        }
    }

    public static c[] a() {
        return (c[]) f2654.clone();
    }

    public static Set<Integer> b() {
        HashSet hashSet = new HashSet();
        for (c cVar : a()) {
            if (cVar.f && cVar != BANNER_AUTO) {
                hashSet.add(Integer.valueOf(cVar.j));
            }
        }
        return hashSet;
    }

    public final int a(DisplayMetrics displayMetrics) {
        return (int) Math.floor((double) (((float) this.g) * displayMetrics.density));
    }

    public final f a(Context context) {
        return f.m3247(context, this.g, this.j);
    }

    public final boolean a(int i2, int i3, int i4) {
        if (i2 < 0) {
            return true;
        }
        if (this == BANNER_AUTO && i2 != i4) {
            return false;
        }
        if (this.g > i2) {
            return false;
        }
        return i3 < 0 || this.j <= i3;
    }

    public final boolean a(Context context, int i2, int i3) {
        int round = Math.round((float) j.m3541(context));
        Math.round((float) j.m3544(context));
        if (i2 > 0) {
            i2 = j.m3545(context, i2);
        }
        if (i3 > 0) {
            i3 = j.m3545(context, i3);
        }
        return a(i2, i3, round);
    }

    public final int b(DisplayMetrics displayMetrics) {
        return (int) Math.floor((double) (((float) this.j) * displayMetrics.density));
    }
}
