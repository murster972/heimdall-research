package com.adincube.sdk.g.c;

import com.adincube.sdk.mediation.b;
import com.adincube.sdk.mediation.d;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class e {

    /* renamed from: 连任  reason: contains not printable characters */
    public b f2663;

    /* renamed from: 靐  reason: contains not printable characters */
    public JSONObject f2664;

    /* renamed from: 麤  reason: contains not printable characters */
    public d f2665;

    /* renamed from: 齉  reason: contains not printable characters */
    public long f2666;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f2667;

    public final String toString() {
        if (this.f2664 == null) {
            return this.f2667;
        }
        return String.format(Locale.US, "%s %s", new Object[]{this.f2667, this.f2664.toString()});
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m3229() {
        b bVar = this.f2663;
        if (bVar == null) {
            return false;
        }
        try {
            if (bVar.m3431() == null || bVar.m3431().m3468() == null) {
                return false;
            }
            long currentTimeMillis = System.currentTimeMillis();
            return currentTimeMillis < this.f2666 || currentTimeMillis > bVar.m3431().m3468().longValue() + this.f2666;
        } catch (Throwable th) {
            a.m3513("Error caught when reading config for network '%s'. Mediation will continue.", this.f2667);
            ErrorReportingHelper.m3510("NetworkOrderElement.isExpired", this.f2667, (b) null, th);
            return true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3230() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("n", (Object) this.f2667);
            if (this.f2664 != null) {
                jSONObject.put("c", (Object) this.f2664);
            }
        } catch (JSONException e) {
        }
        return jSONObject;
    }
}
