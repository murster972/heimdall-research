package com.adincube.sdk.g.c;

import com.adincube.sdk.f.b.c.i;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class d {

    /* renamed from: ʻ  reason: contains not printable characters */
    public i f2656 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean f2657 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    public long f2658;

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean f2659 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    public List<e> f2660 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    public int f2661;

    /* renamed from: 龘  reason: contains not printable characters */
    public b f2662;

    /* renamed from: 龘  reason: contains not printable characters */
    public static d m3224(JSONObject jSONObject) {
        d dVar = new d();
        dVar.f2662 = b.a(jSONObject.getString("at"));
        dVar.f2661 = jSONObject.getInt("ce");
        JSONArray jSONArray = jSONObject.getJSONArray("no");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            e eVar = new e();
            eVar.f2667 = jSONObject2.getString("n");
            if (jSONObject2.has("c")) {
                eVar.f2664 = jSONObject2.getJSONObject("c");
            }
            if (jSONObject2.has("t")) {
                eVar.f2666 = jSONObject2.getLong("t");
            } else {
                eVar.f2666 = System.currentTimeMillis();
            }
            dVar.f2660.add(eVar);
        }
        if (jSONObject.has("f")) {
            dVar.f2657 = jSONObject.getBoolean("f");
        }
        if (jSONObject.has("t")) {
            dVar.f2658 = jSONObject.getLong("t");
        } else {
            dVar.f2658 = System.currentTimeMillis();
        }
        return dVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m3225(d dVar) {
        return (dVar == null || dVar.f2656 == null) ? false : true;
    }

    public final String toString() {
        return this.f2660.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m3226() {
        return this.f2660.isEmpty();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final f m3227() {
        return new f(this, Collections.emptyList());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m3228() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("at", (Object) this.f2662.e);
            jSONObject.put("ce", this.f2661);
            jSONObject.put("f", this.f2657);
            JSONArray jSONArray = new JSONArray();
            for (e r0 : this.f2660) {
                jSONArray.put((Object) r0.m3230());
            }
            jSONObject.put("no", (Object) jSONArray);
            jSONObject.put("t", this.f2658);
        } catch (JSONException e) {
        }
        return jSONObject;
    }
}
