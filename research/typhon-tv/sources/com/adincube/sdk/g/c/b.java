package com.adincube.sdk.g.c;

import com.mopub.common.AdType;
import java.util.Locale;
import net.pubnative.library.request.PubnativeAsset;

public enum b {
    INTERSTITIAL("INTERSTITIAL", AdType.INTERSTITIAL, "I", AdType.INTERSTITIAL),
    BANNER("BANNER", PubnativeAsset.BANNER, "B", "inline"),
    NATIVE("NATIVE", "native", "N", (String) null),
    REWARDED("REWARDED", "rewarded", "R", (String) null);
    
    public String e;
    public String f;
    public String g;
    public String h;

    private b(String str, String str2, String str3, String str4) {
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.h = str4;
    }

    public static b a(String str) {
        for (b bVar : a()) {
            if (bVar.e.equals(str)) {
                return bVar;
            }
        }
        throw new IllegalStateException(String.format(Locale.US, "'%s' is not a valid ad type", new Object[]{str}));
    }

    public static b[] a() {
        return (b[]) f2653.clone();
    }
}
