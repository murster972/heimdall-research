package com.adincube.sdk.g.c;

import com.adincube.sdk.f.b.c.e;
import com.adincube.sdk.util.ErrorReportingHelper;
import com.adincube.sdk.util.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class f implements Iterator<e> {

    /* renamed from: 连任  reason: contains not printable characters */
    private e f2668 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private d f2669 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private List<e> f2670 = new ArrayList();

    /* renamed from: 齉  reason: contains not printable characters */
    private List<e> f2671 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public List<e> f2672 = new ArrayList();

    public f(d dVar, List<e> list) {
        this.f2669 = dVar;
        this.f2671 = list;
        this.f2670.addAll(dVar.f2660);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private e m3231() {
        Iterator<e> it2 = this.f2670.iterator();
        while (it2.hasNext()) {
            e next = it2.next();
            if (next.m3229()) {
                it2.remove();
            } else {
                try {
                    if (next.f2663 != null) {
                        e r1 = this.f2669.f2656.m2918(next).m2906();
                        if (this.f2671.contains(r1)) {
                            break;
                        } else if (next.f2663.m3430()) {
                            return next;
                        } else {
                            if (r1 == e.LOADED) {
                                this.f2672.add(next);
                            }
                        }
                    } else {
                        continue;
                    }
                } catch (Throwable th) {
                    a.m3513("Error caught while inquiring network '%s' for status. Mediation will continue.", next.f2667, th);
                    ErrorReportingHelper.m3510("ReadyNetworkOrderElementIterator.next", next.f2667, this.f2669.f2662, th);
                    it2.remove();
                }
            }
        }
        return null;
    }

    public final boolean hasNext() {
        if (this.f2670.size() == 0) {
            return false;
        }
        if (this.f2668 == null) {
            this.f2668 = m3231();
        }
        return this.f2668 != null;
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final e next() {
        if (!hasNext()) {
            return null;
        }
        e eVar = this.f2668;
        this.f2670.remove(this.f2668);
        this.f2668 = null;
        return eVar;
    }
}
