package com.adincube.sdk.g;

import com.adincube.sdk.AdinCube;

public enum b {
    MALE("MALE"),
    FEMALE("FEMALE");
    
    public String c;

    private b(String str) {
        this.c = str;
    }

    public static b a(AdinCube.UserInfo.Gender gender) {
        if (gender == null) {
            return null;
        }
        switch (gender) {
            case MALE:
                return MALE;
            case FEMALE:
                return FEMALE;
            default:
                return null;
        }
    }

    public static b[] a() {
        return (b[]) f2588.clone();
    }
}
