package com.adincube.sdk.g;

public enum d {
    PORT("PORT"),
    LAND("LAND"),
    BOTH("BOTH");
    
    public String d;

    private d(String str) {
        this.d = str;
    }

    public static d a(String str) {
        for (d dVar : a()) {
            if (dVar.d.equals(str)) {
                return dVar;
            }
        }
        throw new IllegalArgumentException(str + " is not a valid orientation.");
    }

    public static d[] a() {
        return (d[]) f2676.clone();
    }
}
