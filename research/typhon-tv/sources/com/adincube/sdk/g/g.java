package com.adincube.sdk.g;

import android.location.Location;
import com.adincube.sdk.util.h;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class g {

    /* renamed from: 连任  reason: contains not printable characters */
    public Date f2701;

    /* renamed from: 靐  reason: contains not printable characters */
    public c f2702;

    /* renamed from: 麤  reason: contains not printable characters */
    public Integer f2703;

    /* renamed from: 齉  reason: contains not printable characters */
    public Location f2704;

    /* renamed from: 龘  reason: contains not printable characters */
    public b f2705;

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UserInformation: \n");
        if (this.f2703 != null) {
            sb.append(String.format(Locale.US, "age - %d\n", new Object[]{this.f2703}));
        }
        if (this.f2705 != null) {
            sb.append(String.format(Locale.US, "gender - %s\n", new Object[]{this.f2705.c}));
        }
        if (this.f2702 != null) {
            sb.append(String.format(Locale.US, "marital status - %s\n", new Object[]{this.f2702.c}));
        }
        if (this.f2704 != null) {
            sb.append(String.format(Locale.US, "location - %.2f - %.2f\n", new Object[]{Double.valueOf(this.f2704.getLatitude()), Double.valueOf(this.f2704.getLongitude())}));
        }
        return sb.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Integer m3248() {
        if (m3249() == null) {
            return null;
        }
        Date r0 = m3249();
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        instance.setTime(r0);
        return Integer.valueOf(instance.get(1));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Date m3249() {
        if (this.f2701 == null && this.f2703 == null) {
            return null;
        }
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        instance.setTime(new Date());
        h.m3642(instance);
        Date time = instance.getTime();
        int intValue = this.f2703.intValue();
        Calendar instance2 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        instance2.setTime(time);
        instance2.add(1, -intValue);
        Date time2 = instance2.getTime();
        Calendar instance3 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        instance3.setTime(time2);
        h.m3642(instance3);
        instance3.set(6, 1);
        return instance3.getTime();
    }
}
