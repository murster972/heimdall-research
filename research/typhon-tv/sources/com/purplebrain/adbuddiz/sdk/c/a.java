package com.purplebrain.adbuddiz.sdk.c;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.AdBuddizActivity;
import com.purplebrain.adbuddiz.sdk.AdBuddizDelegate;
import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import com.purplebrain.adbuddiz.sdk.b.b;
import com.purplebrain.adbuddiz.sdk.b.d;
import com.purplebrain.adbuddiz.sdk.b.j;
import com.purplebrain.adbuddiz.sdk.b.q;
import com.purplebrain.adbuddiz.sdk.b.r;
import com.purplebrain.adbuddiz.sdk.b.t;
import com.purplebrain.adbuddiz.sdk.b.u;
import com.purplebrain.adbuddiz.sdk.b.v;
import com.purplebrain.adbuddiz.sdk.f.d;
import com.purplebrain.adbuddiz.sdk.h.g;
import com.purplebrain.adbuddiz.sdk.i.a.e;
import com.purplebrain.adbuddiz.sdk.i.a.i;
import com.purplebrain.adbuddiz.sdk.i.b.c;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.p;
import java.util.ArrayList;

public final class a {

    /* renamed from: 连任  reason: contains not printable characters */
    private static Boolean f11875 = true;

    /* renamed from: 麤  reason: contains not printable characters */
    private static a f11876 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public AdBuddizDelegate f11877 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private Long f11878 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Context f11879 = null;

    private a() {
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public static AdBuddizError m14998(Context context, String str) {
        try {
            m15001(context, str);
            return null;
        } catch (b e) {
            return e.a;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m15000(AdBuddizError adBuddizError) {
        if (adBuddizError != null) {
            return AdBuddizError.CONFIG_NOT_READY == adBuddizError || AdBuddizError.CONFIG_EXPIRED == adBuddizError || AdBuddizError.NO_MORE_AVAILABLE_CACHED_ADS == adBuddizError;
        }
        try {
            com.purplebrain.adbuddiz.sdk.e.a.m15046().m15054(false);
            return false;
        } catch (q e) {
            return true;
        } catch (b e2) {
            return false;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static void m15001(Context context, String str) {
        if (context == null) {
            throw new com.purplebrain.adbuddiz.sdk.b.a();
        } else if (!com.purplebrain.adbuddiz.sdk.i.a.b.m15314()) {
            throw new v();
        } else if (AdBuddizActivity.m14870()) {
            throw new d();
        } else if (!i.m15336(context)) {
            throw new r();
        } else {
            try {
                com.purplebrain.adbuddiz.sdk.e.b.m15135().m15141();
                p r0 = p.m15457(false);
                if (r0.m15459() || r0.m15460()) {
                    throw new u();
                } else if (com.purplebrain.adbuddiz.sdk.e.b.m15135().m15144(str)) {
                    throw new t();
                }
            } catch (r e) {
                com.purplebrain.adbuddiz.sdk.e.b.m15135().m15137();
                throw e;
            } catch (j e2) {
                com.purplebrain.adbuddiz.sdk.e.b.m15135().m15137();
                throw e2;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static a m15004() {
        a aVar;
        synchronized (f11875) {
            if (f11876 == null) {
                f11876 = new a();
            }
            aVar = f11876;
        }
        return aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m15005(AdBuddizError adBuddizError, String str) {
        o.m15451("Can't show Ad: " + adBuddizError.getName() + ". " + str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m15006(com.purplebrain.adbuddiz.sdk.f.d dVar, AdBuddizError adBuddizError, boolean z, boolean z2) {
        switch (adBuddizError) {
            case UNSUPPORTED_ANDROID_SDK:
                m15005(adBuddizError, "AdBuddiz only shows ads for Android SDK >= 2.1 - Eclair.");
                break;
            case ACTIVITY_PARAMETER_IS_NULL:
                m15005(adBuddizError, "showAd() activity parameter is null.");
                break;
            case MISSING_INTERNET_PERMISSION_IN_MANIFEST:
                m15005(adBuddizError, "Add <uses-permission android:name=\"android.permission.INTERNET\" /> in AndroidManifest.xml.");
                break;
            case MISSING_PUBLISHER_KEY:
                m15005(adBuddizError, "Add AdBuddiz.setPublisherKey(); call before calling cacheAds();");
                break;
            case MISSING_ADBUDDIZ_ACTIVITY_IN_MANIFEST:
                m15005(adBuddizError, "");
                o.m15451(" ---------------------------------------------------------------------- ");
                o.m15451(" Missing AdBuddizActivity in Manifest, add this :                       ");
                o.m15451(" <activity android:name=\"com.purplebrain.adbuddiz.sdk.AdBuddizActivity\" ");
                o.m15451("           android:theme=\"@android:style/Theme.Translucent\" />          ");
                o.m15451(" ---------------------------------------------------------------------- ");
                break;
            case CONFIG_NOT_READY:
                m15005(adBuddizError, "AdBuddiz Config isn't ready yet. Did you call cacheAds ? Please wait for cache initialization...");
                break;
            case CONFIG_EXPIRED:
                m15005(adBuddizError, "AdBuddiz Config is expired. Currently updating...");
                break;
            case INVALID_PUBLISHER_KEY:
                m15005(adBuddizError, "Check Publisher Key value in AdBuddiz.setPublisherKey(); call.");
                break;
            case PLATFORM_MISMATCH_PUBLISHER_KEY:
                m15005(adBuddizError, "Check Publisher Key value in AdBuddiz.setPublisherKey(); call. That key can only work with an iOS application.");
                break;
            case PLACEMENT_BLOCKED:
                m15005(adBuddizError, "This placement is blocked, no Ad will be shown.");
                break;
            case NO_NETWORK_AVAILABLE:
                m15005(adBuddizError, "Connect device to Internet.");
                break;
            case FORBIDDEN_RECEIVED_FROM_NETWORK:
                m15005(adBuddizError, "");
                o.m15451("Server request responded HTTP '403 Forbidden'.");
                o.m15451("You may be behind a proxy blocking all communications from the SDK.");
                o.m15451("Try to open to www.adbuddiz.com on the browser of your device to check if our service is reachable.");
                break;
            case NO_MORE_AVAILABLE_CACHED_ADS:
                m15005(adBuddizError, "Currently caching ads...");
                break;
            case NO_MORE_AVAILABLE_ADS:
                m15005(adBuddizError, "No ad inventory is currently available for your parameters (Country, Device, OS...).");
                break;
            case SHOW_AD_CALLS_TOO_CLOSE:
                StringBuilder sb = new StringBuilder("Ad was already shown less than ");
                p.m15457(false);
                m15005(adBuddizError, sb.append(p.m15456()).append("ms ago. Please wait between calls.").toString());
                break;
            case AD_IS_ALREADY_ON_SCREEN:
                m15005(adBuddizError, "Ad is already displayed on screen.");
                break;
            case UNKNOWN_EXCEPTION_RAISED:
                m15005(adBuddizError, "");
                break;
        }
        if (z) {
            new StringBuilder("didFailToShowAd ").append(adBuddizError.name());
            com.purplebrain.adbuddiz.sdk.i.i.m15418(new Runnable(adBuddizError) {

                /* renamed from: 龘  reason: contains not printable characters */
                final /* synthetic */ AdBuddizError f12235;

                {
                    this.f12235 = r1;
                }

                public final void run() {
                    AdBuddiz.m14859().m14886(this.f12235);
                }
            });
        }
        if (z2 && AdBuddiz.m14865() != null) {
            try {
                com.purplebrain.adbuddiz.sdk.e.b.m15135().m15141();
                ArrayList arrayList = new ArrayList();
                com.purplebrain.adbuddiz.sdk.f.b r1 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
                d.a aVar = dVar.f12072;
                if (aVar != null) {
                    switch (aVar) {
                        case IS_READY_TO_SHOW_AD:
                            arrayList.addAll(r1.f12060);
                            break;
                        case SHOW_AD:
                            arrayList.addAll(r1.f12059);
                            break;
                    }
                    if (arrayList.contains(adBuddizError)) {
                        g gVar = new g();
                        gVar.m15296(adBuddizError);
                        gVar.f12172 = dVar;
                        gVar.m15266();
                    }
                }
            } catch (b e) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00bb A[SYNTHETIC, Splitter:B:35:0x00bb] */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void m15008(final android.content.Context r9, final java.lang.String r10) {
        /*
            r8 = this;
            r3 = 0
            r2 = 1
            monitor-enter(r8)
            com.purplebrain.adbuddiz.sdk.f.d r4 = new com.purplebrain.adbuddiz.sdk.f.d     // Catch:{ all -> 0x006c }
            com.purplebrain.adbuddiz.sdk.f.d$a r0 = com.purplebrain.adbuddiz.sdk.f.d.a.SHOW_AD     // Catch:{ all -> 0x006c }
            r4.<init>((com.purplebrain.adbuddiz.sdk.f.d.a) r0, (java.lang.String) r10)     // Catch:{ all -> 0x006c }
            android.content.Context r0 = r8.f11879     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            if (r0 != 0) goto L_0x003a
            if (r9 != 0) goto L_0x0019
            com.purplebrain.adbuddiz.sdk.AdBuddizError r0 = com.purplebrain.adbuddiz.sdk.AdBuddizError.ACTIVITY_PARAMETER_IS_NULL     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r1 = 1
            r2 = 1
            m15006(r4, r0, r1, r2)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
        L_0x0017:
            monitor-exit(r8)
            return
        L_0x0019:
            java.lang.String r0 = " ----------------------------------------------------------------- "
            com.purplebrain.adbuddiz.sdk.i.o.m15451(r0)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            java.lang.String r0 = " WARNING - cacheAds wasn't called! You MUST call cacheAds before   "
            com.purplebrain.adbuddiz.sdk.i.o.m15451(r0)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            java.lang.String r0 = " showAd.                                                           "
            com.purplebrain.adbuddiz.sdk.i.o.m15451(r0)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            java.lang.String r0 = " cacheAds will be called automatically. showAd will be delayed.    "
            com.purplebrain.adbuddiz.sdk.i.o.m15451(r0)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            java.lang.String r0 = " ----------------------------------------------------------------- "
            com.purplebrain.adbuddiz.sdk.i.o.m15451(r0)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r8.m15009((android.content.Context) r9)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
        L_0x003a:
            android.os.Looper r0 = android.os.Looper.getMainLooper()     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            android.os.Looper r1 = android.os.Looper.myLooper()     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            boolean r0 = r0.equals(r1)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            if (r0 != 0) goto L_0x006f
            if (r9 != 0) goto L_0x0051
            com.purplebrain.adbuddiz.sdk.AdBuddizError r0 = com.purplebrain.adbuddiz.sdk.AdBuddizError.ACTIVITY_PARAMETER_IS_NULL     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r1 = 1
            r2 = 1
            m15006(r4, r0, r1, r2)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
        L_0x0051:
            android.os.Handler r0 = new android.os.Handler     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            android.os.Looper r1 = android.os.Looper.getMainLooper()     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r0.<init>(r1)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            com.purplebrain.adbuddiz.sdk.c.a$1 r1 = new com.purplebrain.adbuddiz.sdk.c.a$1     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r1.<init>(r9, r10)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r0.post(r1)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            goto L_0x0017
        L_0x0063:
            r0 = move-exception
            com.purplebrain.adbuddiz.sdk.AdBuddizError r0 = com.purplebrain.adbuddiz.sdk.AdBuddizError.MISSING_ADBUDDIZ_ACTIVITY_IN_MANIFEST     // Catch:{ all -> 0x006c }
            r1 = 1
            r2 = 1
            m15006(r4, r0, r1, r2)     // Catch:{ all -> 0x006c }
            goto L_0x0017
        L_0x006c:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x006f:
            java.lang.String r0 = "showAd"
            com.purplebrain.adbuddiz.sdk.i.o.m15452(r0)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r0 = 0
            m15001(r9, r10)     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            r8.f11879 = r9     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            com.purplebrain.adbuddiz.sdk.i.j.m15426(r9)     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            com.purplebrain.adbuddiz.sdk.e.a r1 = com.purplebrain.adbuddiz.sdk.e.a.m15046()     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            r5 = 0
            com.purplebrain.adbuddiz.sdk.f.a.a r1 = r1.m15053((android.content.Context) r9, (boolean) r5)     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            r5 = 0
            com.purplebrain.adbuddiz.sdk.i.p r5 = com.purplebrain.adbuddiz.sdk.i.p.m15457(r5)     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            r5.m15464()     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            com.purplebrain.adbuddiz.sdk.i.a r5 = new com.purplebrain.adbuddiz.sdk.i.a     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            android.content.Context r6 = r8.f11879     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            r5.<init>(r6)     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            r5.f12188 = r1     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            r5.f12186 = r10     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            android.content.Intent r5 = r5.m15311()     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            android.content.Context r6 = r8.f11879     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            r6.startActivity(r5)     // Catch:{ p -> 0x00f0, b -> 0x0104 }
            com.purplebrain.adbuddiz.sdk.i.i$3 r5 = new com.purplebrain.adbuddiz.sdk.i.i$3     // Catch:{ p -> 0x0115, b -> 0x0112 }
            r5.<init>()     // Catch:{ p -> 0x0115, b -> 0x0112 }
            com.purplebrain.adbuddiz.sdk.i.i.m15418(r5)     // Catch:{ p -> 0x0115, b -> 0x0112 }
            java.lang.Thread r5 = new java.lang.Thread     // Catch:{ p -> 0x0115, b -> 0x0112 }
            com.purplebrain.adbuddiz.sdk.c.a$3 r6 = new com.purplebrain.adbuddiz.sdk.c.a$3     // Catch:{ p -> 0x0115, b -> 0x0112 }
            r6.<init>(r1)     // Catch:{ p -> 0x0115, b -> 0x0112 }
            r5.<init>(r6)     // Catch:{ p -> 0x0115, b -> 0x0112 }
            r5.start()     // Catch:{ p -> 0x0115, b -> 0x0112 }
            r1 = r2
        L_0x00b9:
            if (r1 != 0) goto L_0x0017
            java.lang.Long r1 = r8.f11878     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            if (r1 == 0) goto L_0x0110
            java.lang.Long r1 = r8.f11878     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r5 = 500(0x1f4, float:7.0E-43)
            boolean r1 = com.purplebrain.adbuddiz.sdk.i.h.m15416(r1, r5)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            if (r1 == 0) goto L_0x0110
        L_0x00c9:
            if (r2 == 0) goto L_0x0017
            boolean r0 = m15000(r0)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            if (r0 == 0) goto L_0x0017
            java.lang.Thread r0 = new java.lang.Thread     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            com.purplebrain.adbuddiz.sdk.c.a$2 r1 = new com.purplebrain.adbuddiz.sdk.c.a$2     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r1.<init>(r9, r10)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r0.<init>(r1)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r0.start()     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            goto L_0x0017
        L_0x00e0:
            r0 = move-exception
            com.purplebrain.adbuddiz.sdk.AdBuddizError r1 = com.purplebrain.adbuddiz.sdk.AdBuddizError.UNKNOWN_EXCEPTION_RAISED     // Catch:{ all -> 0x006c }
            r2 = 1
            r3 = 1
            m15006(r4, r1, r2, r3)     // Catch:{ all -> 0x006c }
            java.lang.String r1 = "AdBuddiz.showAd() Exception : "
            com.purplebrain.adbuddiz.sdk.i.o.m15453(r1, r0)     // Catch:{ all -> 0x006c }
            goto L_0x0017
        L_0x00f0:
            r0 = move-exception
            r1 = r3
        L_0x00f2:
            com.purplebrain.adbuddiz.sdk.e.a r5 = com.purplebrain.adbuddiz.sdk.e.a.m15046()     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r6 = 0
            r5.m15052((boolean) r6)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            com.purplebrain.adbuddiz.sdk.AdBuddizError r5 = r0.a     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r6 = 1
            r7 = 1
            m15006(r4, r5, r6, r7)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            com.purplebrain.adbuddiz.sdk.AdBuddizError r0 = r0.a     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            goto L_0x00b9
        L_0x0104:
            r0 = move-exception
            r1 = r3
        L_0x0106:
            com.purplebrain.adbuddiz.sdk.AdBuddizError r5 = r0.a     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            r6 = 1
            r7 = 1
            m15006(r4, r5, r6, r7)     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            com.purplebrain.adbuddiz.sdk.AdBuddizError r0 = r0.a     // Catch:{ ActivityNotFoundException -> 0x0063, Throwable -> 0x00e0 }
            goto L_0x00b9
        L_0x0110:
            r2 = r3
            goto L_0x00c9
        L_0x0112:
            r0 = move-exception
            r1 = r2
            goto L_0x0106
        L_0x0115:
            r0 = move-exception
            r1 = r2
            goto L_0x00f2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.c.a.m15008(android.content.Context, java.lang.String):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m15009(Context context) {
        try {
            o.m15452("cacheAds");
            if (context == null) {
                o.m15451("Can't cache ads: cacheAds() context parameter is null.");
            } else {
                this.f11879 = context;
                this.f11878 = Long.valueOf(System.currentTimeMillis());
                com.purplebrain.adbuddiz.sdk.i.j.m15426(context);
                if (com.purplebrain.adbuddiz.sdk.i.q.m15465(context) == null) {
                    o.m15451("Can't cache ads: no Publisher Key set. Call AdBuddiz.setPublisherKey(); before calling cacheAds();");
                } else if (com.purplebrain.adbuddiz.sdk.i.a.b.m15314()) {
                    e.m15319(context);
                    boolean r0 = com.purplebrain.adbuddiz.sdk.i.b.b.m15351();
                    boolean r1 = com.purplebrain.adbuddiz.sdk.i.b.b.m15348();
                    if (r0 || r1) {
                        o.m15452("Configuration changed.");
                        c.m15361(context);
                        if (r1) {
                            com.purplebrain.adbuddiz.sdk.e.a.m15046().m15051();
                            com.purplebrain.adbuddiz.sdk.e.b.m15135().m15136();
                            com.purplebrain.adbuddiz.sdk.i.q.m15466(context);
                            com.purplebrain.adbuddiz.sdk.i.b.b.m15349();
                        }
                    }
                    com.purplebrain.adbuddiz.sdk.e.b.m15135().m15140();
                    com.purplebrain.adbuddiz.sdk.i.e.m15406(context);
                    com.purplebrain.adbuddiz.sdk.i.d.m15403(context);
                    com.purplebrain.adbuddiz.sdk.e.a r2 = com.purplebrain.adbuddiz.sdk.e.a.m15046();
                    r2.m15055(context);
                    r2.m15056(context, (com.purplebrain.adbuddiz.sdk.e.a.a.b) r2.f11951);
                    if (r0 || r1) {
                        com.purplebrain.adbuddiz.sdk.e.a.m15046().m15052(false);
                    }
                }
            }
        } catch (Throwable th) {
            l.m15428(d.a.CACHE_AD, th);
            o.m15453("AdBuddiz.cacheAds() Exception : ", th);
        }
        return;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized boolean m15010(Context context, String str) {
        boolean z = true;
        boolean z2 = false;
        synchronized (this) {
            com.purplebrain.adbuddiz.sdk.f.d dVar = new com.purplebrain.adbuddiz.sdk.f.d(d.a.IS_READY_TO_SHOW_AD, str);
            p r5 = p.m15457(false);
            try {
                if (this.f11879 == null) {
                    o.m15451(" ----------------------------------------------------------------- ");
                    o.m15451(" WARNING - cacheAds wasn't called! You MUST call cacheAds before   ");
                    o.m15451(" isReadyToShowAd.                                                  ");
                    o.m15451(" isReadyToShowAd will ALWAYS return false.                         ");
                    o.m15451(" ----------------------------------------------------------------- ");
                } else {
                    this.f11879 = context;
                    com.purplebrain.adbuddiz.sdk.i.j.m15426(context);
                    boolean z3 = !r5.m15461();
                    try {
                        o.m15452("isReadyToShowAd");
                        m15001(context, str);
                        com.purplebrain.adbuddiz.sdk.e.a.m15046().m15054(false);
                        z2 = true;
                    } catch (b e) {
                        e = e;
                        z = z3;
                        m15006(dVar, e.a, false, z);
                        r5.m15462();
                        return z2;
                    } catch (Throwable th) {
                        th = th;
                        z = z3;
                        m15006(dVar, AdBuddizError.UNKNOWN_EXCEPTION_RAISED, false, z);
                        o.m15453("AdBuddiz.isReadyToShowAd() Exception : ", th);
                        r5.m15462();
                        return z2;
                    }
                    r5.m15462();
                }
            } catch (b e2) {
                e = e2;
            } catch (Throwable th2) {
                th = th2;
                m15006(dVar, AdBuddizError.UNKNOWN_EXCEPTION_RAISED, false, z);
                o.m15453("AdBuddiz.isReadyToShowAd() Exception : ", th);
                r5.m15462();
                return z2;
            }
        }
        return z2;
    }
}
