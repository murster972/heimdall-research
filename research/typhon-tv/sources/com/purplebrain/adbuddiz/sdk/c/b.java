package com.purplebrain.adbuddiz.sdk.c;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.AdBuddizRewardedVideoDelegate;
import com.purplebrain.adbuddiz.sdk.AdBuddizRewardedVideoError;
import com.purplebrain.adbuddiz.sdk.f.d;
import com.purplebrain.adbuddiz.sdk.h.g;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.p;
import com.purplebrain.adbuddiz.sdk.i.r;
import java.util.ArrayList;

public final class b {

    /* renamed from: 连任  reason: contains not printable characters */
    private static Boolean f11890 = true;

    /* renamed from: 麤  reason: contains not printable characters */
    private static b f11891 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public Long f11892 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public AdBuddizRewardedVideoDelegate f11893 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Context f11894 = null;

    private b() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m15011() {
        b bVar;
        synchronized (f11890) {
            if (f11891 == null) {
                f11891 = new b();
            }
            bVar = f11891;
        }
        return bVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m15012(AdBuddizRewardedVideoError adBuddizRewardedVideoError, String str) {
        o.m15451("Can't show Ad: " + adBuddizRewardedVideoError.getName() + ". " + str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15013(d dVar, AdBuddizRewardedVideoError adBuddizRewardedVideoError, String str, boolean z, boolean z2) {
        switch (adBuddizRewardedVideoError) {
            case UNSUPPORTED_ANDROID_SDK:
                m15012(adBuddizRewardedVideoError, "AdBuddiz SDK only boot up for Android SDK >= 2.1 - Eclair.");
                break;
            case ACTIVITY_PARAMETER_IS_NULL:
                m15012(adBuddizRewardedVideoError, "show() activity parameter is null.");
                break;
            case MISSING_INTERNET_PERMISSION_IN_MANIFEST:
                m15012(adBuddizRewardedVideoError, "Add <uses-permission android:name=\"android.permission.INTERNET\" /> in AndroidManifest.xml.");
                break;
            case MISSING_PUBLISHER_KEY:
                m15012(adBuddizRewardedVideoError, "Add AdBuddiz.setPublisherKey(); call before calling fetch();");
                break;
            case MISSING_ADBUDDIZ_ACTIVITY_IN_MANIFEST:
                m15012(adBuddizRewardedVideoError, "");
                o.m15451(" ---------------------------------------------------------------------- ");
                o.m15451(" Missing AdBuddizActivity in Manifest, add this :                       ");
                o.m15451(" <activity android:name=\"com.purplebrain.adbuddiz.sdk.AdBuddizActivity\" ");
                o.m15451("           android:theme=\"@android:style/Theme.Translucent\" />          ");
                o.m15451(" ---------------------------------------------------------------------- ");
                break;
            case CONFIG_NOT_READY:
                m15012(adBuddizRewardedVideoError, "AdBuddiz Config isn't ready yet. Did you call fetch()? Please wait for cache initialization...");
                break;
            case CONFIG_EXPIRED:
                m15012(adBuddizRewardedVideoError, "AdBuddiz Config is expired. Currently updating...");
                break;
            case INVALID_PUBLISHER_KEY:
                m15012(adBuddizRewardedVideoError, "Check Publisher Key value in AdBuddiz.setPublisherKey(); call.");
                break;
            case PLATFORM_MISMATCH_PUBLISHER_KEY:
                m15012(adBuddizRewardedVideoError, "Check Publisher Key value in AdBuddiz.setPublisherKey(); call. That key can only work with an iOS application.");
                break;
            case NO_NETWORK_AVAILABLE:
                m15012(adBuddizRewardedVideoError, "Connect device to Internet.");
                break;
            case FORBIDDEN_RECEIVED_FROM_NETWORK:
                m15012(adBuddizRewardedVideoError, "");
                o.m15451("Server request responded HTTP '403 Forbidden'.");
                o.m15451("You may be behind a proxy blocking all communications from the SDK.");
                o.m15451("Try to open to www.adbuddiz.com on the browser of your device to check if our service is reachable.");
                break;
            case NETWORK_TOO_SLOW:
                m15012(adBuddizRewardedVideoError, "Current network is too slow.");
                break;
            case UNSUPPORTED_DEVICE:
                m15012(adBuddizRewardedVideoError, "Device is not supported.");
                break;
            case UNSUPPORTED_OS_VERSION:
                m15012(adBuddizRewardedVideoError, "Rewarded video ads are only available on Android " + str + ".");
                break;
            case VIDEO_AD_EXPIRED:
                m15012(adBuddizRewardedVideoError, "You wait too much time before calling show().");
                break;
            case FETCH_VIDEO_AD_NOT_CALLED:
                m15012(adBuddizRewardedVideoError, "No video ad is currently being fetched. Did you call AdBuddiz.RewardedVideo.fetch()?");
                break;
            case FETCHING_AD:
                m15012(adBuddizRewardedVideoError, "Currently fetching video ad...");
                break;
            case NO_MORE_AVAILABLE_ADS:
                m15012(adBuddizRewardedVideoError, "No ad inventory is currently available for your parameters (Country, Device, OS...).");
                break;
            case SHOW_AD_CALLS_TOO_CLOSE:
                StringBuilder sb = new StringBuilder("Ad was already shown less than ");
                p.m15457(true);
                m15012(adBuddizRewardedVideoError, sb.append(p.m15456()).append("ms ago. Please wait between calls.").toString());
                break;
            case AD_IS_ALREADY_ON_SCREEN:
                m15012(adBuddizRewardedVideoError, "Ad is already displayed on screen.");
                break;
            case UNKNOWN_EXCEPTION_RAISED:
                m15012(adBuddizRewardedVideoError, "");
                break;
        }
        if (z) {
            r.m15470(new Runnable(adBuddizRewardedVideoError) {

                /* renamed from: 龘  reason: contains not printable characters */
                final /* synthetic */ AdBuddizRewardedVideoError f12248;

                {
                    this.f12248 = r1;
                }

                public final void run() {
                    AdBuddiz.RewardedVideo.m14869().m14890(this.f12248);
                }
            });
        }
        if (z2 && AdBuddiz.m14865() != null) {
            try {
                com.purplebrain.adbuddiz.sdk.e.b.m15135().m15141();
                ArrayList arrayList = new ArrayList();
                com.purplebrain.adbuddiz.sdk.f.b r1 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
                d.a aVar = dVar.f12072;
                if (aVar != null) {
                    switch (aVar) {
                        case IS_READY_TO_SHOW_AD:
                            arrayList.addAll(r1.f12060);
                            break;
                        case SHOW_AD:
                            arrayList.addAll(r1.f12059);
                            break;
                    }
                    if (arrayList.contains(adBuddizRewardedVideoError)) {
                        g gVar = new g();
                        gVar.f12173 = null;
                        gVar.f12169 = adBuddizRewardedVideoError;
                        gVar.f12172 = dVar;
                        gVar.m15266();
                    }
                }
            } catch (com.purplebrain.adbuddiz.sdk.b.b e) {
            }
        }
    }
}
