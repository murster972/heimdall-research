package com.purplebrain.adbuddiz.sdk.f;

import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class c {
    /* renamed from: 龘  reason: contains not printable characters */
    public static b m15227(JSONObject jSONObject) {
        try {
            b bVar = new b();
            bVar.f12066 = jSONObject.getLong("updateTimestamp");
            bVar.f12063 = jSONObject.getLong("updateFrequency");
            bVar.f12064 = jSONObject.getLong("cacheExpiration");
            bVar.f12065 = jSONObject.getString("a");
            bVar.f12062 = jSONObject.getBoolean("isTablet");
            bVar.f12039 = jSONObject.getLong("publisherId");
            bVar.f12040 = jSONObject.getString("downloadAdBaseUrl");
            bVar.f12041 = jSONObject.getString("downloadVideoAdBaseUrl");
            bVar.f12051 = jSONObject.getString("byAdBuddizLinkUrl");
            JSONArray jSONArray = jSONObject.getJSONArray("inactivePlacementIds");
            for (int i = 0; i < jSONArray.length(); i++) {
                bVar.f12054.add(jSONArray.getString(i));
            }
            bVar.f12055 = jSONObject.getInt("maxNbAdBuddizAdInCache");
            bVar.f12045 = jSONObject.getInt("maxSizeRTBAdContent");
            bVar.f12042 = jSONObject.getInt("maxNbAdBuddizImpressionTimestampSent");
            bVar.f12043 = jSONObject.getInt("maxNbRTBImpressionTimestampSent");
            bVar.f12068 = jSONObject.getLong("maxTimeToKeepRequestInformation");
            bVar.f12069 = jSONObject.getInt("additionalInformationToSend");
            bVar.f12047 = jSONObject.getBoolean("automaticallyHideAdOnClick");
            bVar.f12046 = jSONObject.getLong("minTimeBeforeClickEnabled");
            if (jSONObject.has("minTimeBeforeClickEnabledForEndInterstitial")) {
                bVar.f12050 = Long.valueOf(jSONObject.getLong("minTimeBeforeClickEnabledForEndInterstitial"));
            }
            bVar.f12052 = jSONObject.getLong("minTimeBeforeCloseEnabledWhileFollowingLinks");
            bVar.f12058 = jSONObject.getLong("minTimeBeforeCloseEnabledWhenMediaPlayerStuck");
            if (jSONObject.has("minTimeBeforeCloseEnabledOnMRAID")) {
                bVar.f12053 = Long.valueOf(jSONObject.getLong("minTimeBeforeCloseEnabledOnMRAID"));
            }
            if (jSONObject.has("minTimeBeforeCloseEnabledOnMRAIDIfFullyLoaded")) {
                bVar.f12057 = Long.valueOf(jSONObject.getLong("minTimeBeforeCloseEnabledOnMRAIDIfFullyLoaded"));
            }
            bVar.f12048 = jSONObject.getLong("minTimeBetween2ShowAdCalls");
            bVar.f12049 = jSONObject.getLong("minTimeBetween2ReportOnIsReady");
            bVar.f12044 = jSONObject.getLong("minTimeBetween2RequestNextAdAfterNoFill");
            if (jSONObject.has("showAdEventsToReport")) {
                bVar.f12059.clear();
                JSONArray jSONArray2 = jSONObject.getJSONArray("showAdEventsToReport");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    bVar.f12059.add(AdBuddizError.valueOf(jSONArray2.getString(i2)));
                }
            }
            if (jSONObject.has("isReadyToShowAdEventsToReport")) {
                bVar.f12060.clear();
                JSONArray jSONArray3 = jSONObject.getJSONArray("isReadyToShowAdEventsToReport");
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    bVar.f12060.add(AdBuddizError.valueOf(jSONArray3.getString(i3)));
                }
            }
            if (jSONObject.has("afterLoadScript")) {
                bVar.f12061 = jSONObject.getString("afterLoadScript");
            }
            bVar.f12067 = jSONObject.getBoolean("mediaPlayerStartsMuted");
            bVar.f12070 = jSONObject.getDouble("percentageBeforeNotReportingVASTError");
            bVar.f12056 = f.m15229(jSONObject.getJSONObject("mediaFileSelectorParameters"));
            return bVar;
        } catch (JSONException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static JSONObject m15228(b bVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("updateTimestamp", bVar.f12066);
            jSONObject.put("updateFrequency", bVar.f12063);
            jSONObject.put("cacheExpiration", bVar.f12064);
            jSONObject.put("a", (Object) bVar.f12065);
            jSONObject.put("isTablet", bVar.f12062);
            jSONObject.put("publisherId", bVar.f12039);
            jSONObject.put("downloadAdBaseUrl", (Object) bVar.f12040);
            jSONObject.put("downloadVideoAdBaseUrl", (Object) bVar.f12041);
            jSONObject.put("byAdBuddizLinkUrl", (Object) bVar.f12051);
            JSONArray jSONArray = new JSONArray();
            for (String put : bVar.f12054) {
                jSONArray.put((Object) put);
            }
            jSONObject.put("inactivePlacementIds", (Object) jSONArray);
            jSONObject.put("maxNbAdBuddizAdInCache", bVar.f12055);
            jSONObject.put("maxSizeRTBAdContent", bVar.f12045);
            jSONObject.put("maxNbAdBuddizImpressionTimestampSent", bVar.f12042);
            jSONObject.put("maxNbRTBImpressionTimestampSent", bVar.f12043);
            jSONObject.put("maxTimeToKeepRequestInformation", bVar.f12068);
            jSONObject.put("additionalInformationToSend", bVar.f12069);
            jSONObject.put("automaticallyHideAdOnClick", bVar.f12047);
            jSONObject.put("minTimeBeforeClickEnabled", bVar.f12046);
            if (bVar.f12050 != null) {
                jSONObject.put("minTimeBeforeClickEnabledForEndInterstitial", (Object) bVar.f12050);
            }
            jSONObject.put("minTimeBeforeCloseEnabledWhileFollowingLinks", bVar.f12052);
            jSONObject.put("minTimeBeforeCloseEnabledWhenMediaPlayerStuck", bVar.f12058);
            if (bVar.f12053 != null) {
                jSONObject.put("minTimeBeforeCloseEnabledOnMRAID", (Object) bVar.f12053);
            }
            if (bVar.f12057 != null) {
                jSONObject.put("minTimeBeforeCloseEnabledOnMRAIDIfFullyLoaded", (Object) bVar.f12057);
            }
            jSONObject.put("minTimeBetween2ShowAdCalls", bVar.f12048);
            jSONObject.put("minTimeBetween2ReportOnIsReady", bVar.f12049);
            jSONObject.put("minTimeBetween2RequestNextAdAfterNoFill", bVar.f12044);
            JSONArray jSONArray2 = new JSONArray();
            for (AdBuddizError name : bVar.f12059) {
                jSONArray2.put((Object) name.name());
            }
            jSONObject.put("showAdEventsToReport", (Object) jSONArray2);
            JSONArray jSONArray3 = new JSONArray();
            for (AdBuddizError name2 : bVar.f12060) {
                jSONArray3.put((Object) name2.name());
            }
            jSONObject.put("isReadyToShowAdEventsToReport", (Object) jSONArray3);
            if (bVar.f12061 != null) {
                jSONObject.put("afterLoadScript", (Object) bVar.f12061);
            }
            jSONObject.put("mediaPlayerStartsMuted", bVar.f12067);
            jSONObject.put("percentageBeforeNotReportingVASTError", bVar.f12070);
            jSONObject.put("mediaFileSelectorParameters", (Object) bVar.f12056.m15230());
            return jSONObject;
        } catch (JSONException e) {
            return null;
        }
    }
}
