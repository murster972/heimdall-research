package com.purplebrain.adbuddiz.sdk.f.a;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.f.a.a.c;
import com.purplebrain.adbuddiz.sdk.f.a.a.d;
import com.purplebrain.adbuddiz.sdk.f.e;
import com.purplebrain.adbuddiz.sdk.i.h;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class a {

    /* renamed from: 连任  reason: contains not printable characters */
    public Set f11994 = new HashSet();

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean f11995;

    /* renamed from: 麤  reason: contains not printable characters */
    protected int f11996 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    protected long f11997;

    /* renamed from: 龘  reason: contains not printable characters */
    public e f11998 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public static a m15155(JSONObject jSONObject) {
        a aVar = null;
        switch (e.a(jSONObject.getString("at"))) {
            case ADBUDDIZ:
                aVar = new b();
                break;
            case ADBUDDIZ_VIDEO:
                aVar = new c();
                break;
            case RTB:
                aVar = new f();
                break;
        }
        if (jSONObject.has("fo")) {
            aVar.f11998 = e.a(jSONObject.getString("fo"));
        }
        if (jSONObject.has("in")) {
            aVar.f11995 = jSONObject.getBoolean("in");
        }
        if (jSONObject.has("ct")) {
            aVar.f11997 = jSONObject.getLong("ct");
        } else {
            aVar.f11997 = h.m15415().getTime();
        }
        aVar.f11996 = jSONObject.getInt("ce");
        if (jSONObject.has(InternalZipTyphoonApp.READ_MODE)) {
            JSONArray jSONArray = jSONObject.getJSONArray(InternalZipTyphoonApp.READ_MODE);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < jSONArray.length()) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    aVar.m15169((c) new c.a(aVar, d.a(jSONObject2.getString("t")), jSONObject2.getBoolean(TtmlNode.TAG_P), e.a(jSONObject2.getString("o")), jSONObject2));
                    i = i2 + 1;
                }
            }
        }
        aVar.m15163(jSONObject);
        return aVar;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof a)) {
            return m15162().equals(((a) obj).m15162());
        }
        return false;
    }

    public int hashCode() {
        return m15162().hashCode();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract d m15156();

    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean m15157() {
        return (this.f11998 == null || this.f11998 == e.BOTH) ? false : true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final Collection m15158() {
        Collection unmodifiableCollection;
        synchronized (this.f11994) {
            unmodifiableCollection = Collections.unmodifiableCollection(this.f11994);
        }
        return unmodifiableCollection;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final long m15159() {
        return this.f11997;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final int m15160() {
        return this.f11996 * 1000;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract e m15161();

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract String m15162();

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m15163(JSONObject jSONObject);

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract Collection m15164();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract e m15165();

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m15166(JSONObject jSONObject);

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m15167() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("at", (Object) m15161().d);
        jSONObject.put("in", this.f11995);
        if (this.f11998 != null) {
            jSONObject.put("fo", (Object) this.f11998);
        }
        jSONObject.put("ct", this.f11997);
        jSONObject.put("ce", this.f11996);
        synchronized (this.f11994) {
            if (!this.f11994.isEmpty()) {
                JSONArray jSONArray = new JSONArray();
                for (c r0 : this.f11994) {
                    jSONArray.put((Object) r0.m15173());
                    jSONObject.put(InternalZipTyphoonApp.READ_MODE, (Object) jSONArray);
                }
            }
        }
        m15166(jSONObject);
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15168(Context context, JSONObject jSONObject) {
        jSONObject.put("at", (Object) m15161().d);
        jSONObject.put("in", this.f11995);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15169(c cVar) {
        synchronized (this.f11994) {
            this.f11994.add(cVar);
        }
    }
}
