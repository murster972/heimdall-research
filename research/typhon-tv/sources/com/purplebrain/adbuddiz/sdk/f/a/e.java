package com.purplebrain.adbuddiz.sdk.f.a;

public enum e {
    ADBUDDIZ("ADBUDDIZ", "I", true),
    ADBUDDIZ_VIDEO("ADBUDDIZ_VIDEO", "V", true),
    RTB("RTB", "R", false);
    
    String d;
    public String e;
    public boolean f;

    private e(String str, String str2, boolean z) {
        this.d = str;
        this.e = str2;
        this.f = z;
    }

    public static e a(String str) {
        for (e eVar : a()) {
            if (eVar.d.equals(str)) {
                return eVar;
            }
        }
        throw new IllegalArgumentException("'" + str + "' is not a valid ad type.");
    }

    public static e[] a() {
        return (e[]) f12031.clone();
    }
}
