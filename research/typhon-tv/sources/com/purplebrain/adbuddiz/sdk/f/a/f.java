package com.purplebrain.adbuddiz.sdk.f.a;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.f.a.a.c;
import com.purplebrain.adbuddiz.sdk.f.a.a.d;
import com.purplebrain.adbuddiz.sdk.f.a.a.e;
import com.purplebrain.adbuddiz.sdk.f.a.a.g;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import org.json.JSONArray;
import org.json.JSONObject;

public final class f extends a {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f12032;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f12033;

    /* renamed from: ʽ  reason: contains not printable characters */
    public String f12034;

    /* renamed from: ˈ  reason: contains not printable characters */
    public String f12035;

    /* renamed from: ˑ  reason: contains not printable characters */
    public String f12036;

    /* renamed from: ٴ  reason: contains not printable characters */
    public Integer f12037;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Integer f12038;

    /* renamed from: ʻ  reason: contains not printable characters */
    public final d m15215() {
        return this.f12033.contains("<VAST") ? d.VAST : d.MRAID;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final e m15216() {
        return e.RTB;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m15217() {
        return this.f12032;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m15218(JSONObject jSONObject) {
        int i = 0;
        this.f12032 = jSONObject.getString("i");
        this.f12033 = jSONObject.getString("c");
        if (jSONObject.has("afs")) {
            this.f12036 = jSONObject.getString("afs");
        }
        if (jSONObject.has("ow") && jSONObject.has("oh")) {
            this.f12037 = Integer.valueOf(jSONObject.getInt("oh"));
            this.f12038 = Integer.valueOf(jSONObject.getInt("ow"));
        }
        this.f12035 = jSONObject.getString("w");
        if (jSONObject.has("ts")) {
            this.f12034 = jSONObject.getString("ts");
        }
        if (jSONObject.has("cr")) {
            JSONArray jSONArray = jSONObject.getJSONArray("cr");
            while (true) {
                int i2 = i;
                if (i2 < jSONArray.length()) {
                    try {
                        m15169((c) new e(this, d.MEDIA, false, com.purplebrain.adbuddiz.sdk.f.e.BOTH, new URL(jSONArray.getString(i2))));
                    } catch (MalformedURLException e) {
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Collection m15219() {
        ArrayList arrayList = new ArrayList();
        if (this.f12033 != null && this.f12033.length() > 0) {
            arrayList.add(new g(this, m15215().f, com.purplebrain.adbuddiz.sdk.f.e.BOTH, this.f12033));
        }
        arrayList.addAll(m15158());
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final com.purplebrain.adbuddiz.sdk.f.e m15220() {
        return com.purplebrain.adbuddiz.sdk.f.e.BOTH;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m15221(JSONObject jSONObject) {
        jSONObject.put("i", (Object) this.f12032);
        jSONObject.put("c", (Object) this.f12033);
        jSONObject.put("w", (Object) this.f12035);
        if (this.f12036 != null) {
            jSONObject.put("afs", (Object) this.f12036);
        }
        if (!(this.f12037 == null || this.f12038 == null)) {
            jSONObject.put("ow", (Object) this.f12038);
            jSONObject.put("oh", (Object) this.f12037);
        }
        if (this.f12034 != null) {
            jSONObject.put("ts", (Object) this.f12034);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15222(Context context, JSONObject jSONObject) {
        super.m15168(context, jSONObject);
        jSONObject.put("ir", (Object) this.f12032);
        jSONObject.put("ri", (Object) com.purplebrain.adbuddiz.sdk.i.b.c.m15358(context, false));
        if (this.f11995) {
            jSONObject.put("rii", (Object) com.purplebrain.adbuddiz.sdk.i.b.c.m15358(context, true));
        }
    }
}
