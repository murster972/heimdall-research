package com.purplebrain.adbuddiz.sdk.f;

import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import com.purplebrain.adbuddiz.sdk.b.h;
import com.purplebrain.adbuddiz.sdk.i.f;
import com.purplebrain.adbuddiz.sdk.i.m;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class b {

    /* renamed from: ʻ  reason: contains not printable characters */
    public long f12039;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f12040;

    /* renamed from: ʽ  reason: contains not printable characters */
    public String f12041;

    /* renamed from: ʾ  reason: contains not printable characters */
    public int f12042;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f12043;

    /* renamed from: ˆ  reason: contains not printable characters */
    public long f12044;

    /* renamed from: ˈ  reason: contains not printable characters */
    public int f12045;

    /* renamed from: ˉ  reason: contains not printable characters */
    public long f12046;

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean f12047;

    /* renamed from: ˋ  reason: contains not printable characters */
    public long f12048;

    /* renamed from: ˎ  reason: contains not printable characters */
    public long f12049;

    /* renamed from: ˏ  reason: contains not printable characters */
    public Long f12050;

    /* renamed from: ˑ  reason: contains not printable characters */
    public String f12051;

    /* renamed from: י  reason: contains not printable characters */
    public long f12052;

    /* renamed from: ـ  reason: contains not printable characters */
    public Long f12053;

    /* renamed from: ٴ  reason: contains not printable characters */
    public List f12054 = new ArrayList();

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int f12055;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public f f12056;

    /* renamed from: ᴵ  reason: contains not printable characters */
    public Long f12057;

    /* renamed from: ᵎ  reason: contains not printable characters */
    public long f12058;

    /* renamed from: ᵔ  reason: contains not printable characters */
    public List f12059 = new ArrayList();

    /* renamed from: ᵢ  reason: contains not printable characters */
    public List f12060 = new ArrayList();

    /* renamed from: ⁱ  reason: contains not printable characters */
    public String f12061;

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean f12062;

    /* renamed from: 靐  reason: contains not printable characters */
    public long f12063;

    /* renamed from: 麤  reason: contains not printable characters */
    public long f12064;

    /* renamed from: 齉  reason: contains not printable characters */
    public String f12065;

    /* renamed from: 龘  reason: contains not printable characters */
    public long f12066;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public boolean f12067;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public long f12068;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public int f12069 = 0;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public double f12070;

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m15223() {
        try {
            String r0 = m.m15431(AdBuddiz.m14865(), "ABZ_31_cfg.abz");
            if (r0 != null) {
                return c.m15227(new JSONObject(f.m15407(r0)));
            }
        } catch (h | JSONException e) {
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m15224() {
        return this.f12066 + this.f12064 < System.currentTimeMillis() || this.f12066 > System.currentTimeMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15225(String str) {
        Long l = null;
        this.f12066 = System.currentTimeMillis();
        JSONObject jSONObject = new JSONObject(str);
        this.f12063 = jSONObject.getLong("uf");
        this.f12064 = jSONObject.getLong("ce");
        this.f12065 = jSONObject.getString("a");
        this.f12062 = jSONObject.getBoolean("t");
        this.f12039 = jSONObject.getLong("pi");
        this.f12040 = jSONObject.getString("bu");
        this.f12041 = jSONObject.getString("vbu");
        this.f12051 = jSONObject.getString("ba");
        this.f12054.clear();
        JSONArray jSONArray = jSONObject.getJSONArray("ip");
        for (int i = 0; i < jSONArray.length(); i++) {
            this.f12054.add(jSONArray.getString(i));
        }
        this.f12055 = jSONObject.getInt("aac");
        this.f12045 = jSONObject.getInt("rac");
        this.f12042 = jSONObject.getInt("naits");
        this.f12043 = jSONObject.getInt("nrits");
        this.f12068 = (long) jSONObject.getInt("mkri");
        this.f12069 = jSONObject.getInt("ai");
        this.f12047 = jSONObject.getBoolean("ah");
        this.f12048 = jSONObject.getLong("msa");
        this.f12049 = jSONObject.getLong("mir");
        this.f12044 = jSONObject.getLong("mna");
        this.f12046 = jSONObject.getLong("mc");
        this.f12050 = jSONObject.has("mcei") ? Long.valueOf(jSONObject.getLong("mcei")) : null;
        this.f12052 = jSONObject.getLong("mcfl");
        this.f12053 = jSONObject.has("mcm") ? Long.valueOf(jSONObject.getLong("mcm")) : null;
        if (jSONObject.has("mcmfl")) {
            l = Long.valueOf(jSONObject.getLong("mcmfl"));
        }
        this.f12057 = l;
        this.f12058 = jSONObject.getLong("mcms");
        this.f12059.clear();
        JSONArray jSONArray2 = jSONObject.getJSONArray("sae");
        for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
            try {
                this.f12059.add(AdBuddizError.valueOf(jSONArray2.getString(i2)));
            } catch (IllegalArgumentException e) {
            }
        }
        this.f12060.clear();
        JSONArray jSONArray3 = jSONObject.getJSONArray("ire");
        for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
            try {
                this.f12060.add(AdBuddizError.valueOf(jSONArray3.getString(i3)));
            } catch (IllegalArgumentException e2) {
            }
        }
        if (jSONObject.has("afs")) {
            this.f12061 = jSONObject.getString("afs");
        }
        this.f12067 = jSONObject.getBoolean("mpm");
        this.f12070 = jSONObject.getDouble("pbnr");
        this.f12056 = f.m15229(jSONObject.getJSONObject("mfsp"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m15226(a aVar) {
        return (aVar.f & this.f12069) != 0;
    }
}
