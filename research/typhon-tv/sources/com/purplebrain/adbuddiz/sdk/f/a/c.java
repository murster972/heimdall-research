package com.purplebrain.adbuddiz.sdk.f.a;

import com.purplebrain.adbuddiz.sdk.f.a.a.b;
import com.purplebrain.adbuddiz.sdk.f.a.a.d;
import com.purplebrain.adbuddiz.sdk.f.a.a.f;
import com.purplebrain.adbuddiz.sdk.f.e;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

public final class c extends b {

    /* renamed from: ʾ  reason: contains not printable characters */
    public Map f12023;

    /* renamed from: ʿ  reason: contains not printable characters */
    public String f12024;

    /* renamed from: ˈ  reason: contains not printable characters */
    public String f12025;

    /* renamed from: ˊ  reason: contains not printable characters */
    public Integer f12026;

    /* renamed from: ˋ  reason: contains not printable characters */
    public Integer f12027;

    /* renamed from: ﹶ  reason: contains not printable characters */
    public String f12028;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public Long f12029;

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15209(JSONObject jSONObject, String str, d dVar, boolean z) {
        if (jSONObject.has(str)) {
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                try {
                    String str2 = (String) keys.next();
                    d dVar2 = dVar;
                    boolean z2 = z;
                    m15169((com.purplebrain.adbuddiz.sdk.f.a.a.c) new f(this, dVar2, str2, z2, e.BOTH, new URL(jSONObject2.getString(str2))));
                } catch (MalformedURLException e) {
                }
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final d m15210() {
        return d.ADBUDDIZ_VIDEO;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final e m15211() {
        return e.ADBUDDIZ_VIDEO;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m15212(JSONObject jSONObject) {
        super.m15204(jSONObject);
        this.f12025 = jSONObject.getString("h");
        this.f12028 = jSONObject.getString("w");
        this.f12023 = new HashMap();
        JSONObject jSONObject2 = jSONObject.getJSONObject("tk");
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            this.f12023.put(str, jSONObject2.getString(str));
        }
        if (jSONObject.has("ts")) {
            this.f12024 = jSONObject.getString("ts");
        }
        if (jSONObject.has("d") && jSONObject.has("vw") && jSONObject.has("vh")) {
            this.f12029 = Long.valueOf(jSONObject.getLong("d"));
            this.f12026 = Integer.valueOf(jSONObject.getInt("vw"));
            this.f12027 = Integer.valueOf(jSONObject.getInt("vh"));
        }
        m15209(jSONObject, "cr", d.MEDIA, false);
        m15209(jSONObject, "a", d.ASSET, true);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Collection m15213() {
        ArrayList arrayList = new ArrayList(m15158());
        try {
            b bVar = new b(this);
            bVar.m15181("isVideo", "true");
            if (!(this.f12029 == null || this.f12026 == null || this.f12027 == null)) {
                com.purplebrain.adbuddiz.sdk.i.c.d.m15392((com.purplebrain.adbuddiz.sdk.f.a.a.c) bVar, this.f12029.longValue());
                com.purplebrain.adbuddiz.sdk.i.c.d.m15391((com.purplebrain.adbuddiz.sdk.f.a.a.c) bVar, this.f12026.intValue());
                com.purplebrain.adbuddiz.sdk.i.c.d.m15388(bVar, this.f12027.intValue());
            }
            arrayList.add(bVar);
        } catch (MalformedURLException e) {
        }
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m15214(JSONObject jSONObject) {
        super.m15207(jSONObject);
        jSONObject.put("h", (Object) this.f12025);
        jSONObject.put("w", (Object) this.f12028);
        JSONObject jSONObject2 = new JSONObject();
        for (Map.Entry entry : this.f12023.entrySet()) {
            jSONObject2.put((String) entry.getKey(), entry.getValue());
        }
        jSONObject.put("tk", (Object) jSONObject2);
        if (this.f12024 != null) {
            jSONObject.put("ts", (Object) this.f12024);
        }
        if (this.f12029 != null && this.f12026 != null && this.f12027 != null) {
            jSONObject.put("d", (Object) this.f12029);
            jSONObject.put("vw", (Object) this.f12026);
            jSONObject.put("vh", (Object) this.f12027);
        }
    }
}
