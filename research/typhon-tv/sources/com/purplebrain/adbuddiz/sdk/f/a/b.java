package com.purplebrain.adbuddiz.sdk.f.a;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.f.a.a.a;
import com.purplebrain.adbuddiz.sdk.f.e;
import com.purplebrain.adbuddiz.sdk.i.b.c;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import org.json.JSONObject;

public class b extends a {

    /* renamed from: ʻ  reason: contains not printable characters */
    public long f12017;

    /* renamed from: ʼ  reason: contains not printable characters */
    public long f12018;

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean f12019;

    /* renamed from: ˑ  reason: contains not printable characters */
    public String f12020;

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f12021;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public e f12022;

    /* renamed from: ʻ  reason: contains not printable characters */
    public d m15201() {
        return d.ADBUDDIZ;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public e m15202() {
        return e.ADBUDDIZ;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m15203() {
        return Long.toString(this.f12017);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m15204(JSONObject jSONObject) {
        this.f12017 = jSONObject.getLong("i");
        this.f12018 = jSONObject.getLong("c");
        this.f12019 = jSONObject.getBoolean("t");
        this.f12020 = jSONObject.getString(TtmlNode.TAG_P);
        this.f12021 = jSONObject.getString("l");
        this.f12022 = e.a(jSONObject.getString("o"));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Collection m15205() {
        try {
            if (this.f12022 == e.BOTH) {
                return Arrays.asList(new a[]{new a(this, e.LAND), new a(this, e.PORT)});
            }
            return Arrays.asList(new a[]{new a(this, this.f12022)});
        } catch (MalformedURLException e) {
            return Collections.emptyList();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final e m15206() {
        return this.f12022;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m15207(JSONObject jSONObject) {
        jSONObject.put("i", this.f12017);
        jSONObject.put("c", this.f12018);
        jSONObject.put("t", this.f12019);
        jSONObject.put(TtmlNode.TAG_P, (Object) this.f12020);
        jSONObject.put("l", (Object) this.f12021);
        jSONObject.put("o", (Object) this.f12022.d);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15208(Context context, JSONObject jSONObject) {
        super.m15168(context, jSONObject);
        jSONObject.put("ia", this.f12017);
        jSONObject.put("ai", (Object) c.m15357(context, (a) this, false));
        if (this.f11995) {
            jSONObject.put("aii", (Object) c.m15357(context, (a) this, true));
        }
    }
}
