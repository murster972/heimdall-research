package com.purplebrain.adbuddiz.sdk.f.a.a;

import com.purplebrain.adbuddiz.sdk.f.e;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class c {

    /* renamed from: 连任  reason: contains not printable characters */
    private Map f12002 = new HashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    public d f12003 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f12004 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    public e f12005 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public com.purplebrain.adbuddiz.sdk.f.a.a f12006 = null;

    private static class a extends c {

        /* renamed from: ʻ  reason: contains not printable characters */
        private String f12007 = null;

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f12008 = false;

        /* renamed from: ʽ  reason: contains not printable characters */
        private String f12009 = null;

        /* renamed from: ˑ  reason: contains not printable characters */
        private boolean f12010 = false;

        /* renamed from: ٴ  reason: contains not printable characters */
        private String f12011 = null;

        /* renamed from: 连任  reason: contains not printable characters */
        private String f12012 = null;

        public a(com.purplebrain.adbuddiz.sdk.f.a.a aVar, d dVar, boolean z, e eVar, JSONObject jSONObject) {
            super(aVar, dVar, z, eVar);
            this.f12012 = jSONObject.getString("ai");
            this.f12007 = jSONObject.getString("n");
            if (jSONObject.has("u")) {
                this.f12008 = true;
                this.f12009 = jSONObject.getString("u");
            }
            if (jSONObject.has("c")) {
                this.f12010 = true;
                this.f12011 = jSONObject.getString("c");
            }
            JSONObject jSONObject2 = jSONObject.getJSONObject("ps");
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                m15181(str, jSONObject2.getString(str));
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public final String m15184() {
            return this.f12011;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public final boolean m15185() {
            return this.f12010;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final String m15186() {
            return this.f12007;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public final URL m15187() {
            if (this.f12009 == null) {
                return null;
            }
            return new URL(this.f12009);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final boolean m15188() {
            return this.f12008;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m15189() {
            return this.f12012;
        }
    }

    public c(com.purplebrain.adbuddiz.sdk.f.a.a aVar, d dVar, boolean z, e eVar) {
        this.f12006 = aVar;
        this.f12003 = dVar;
        this.f12004 = z;
        this.f12005 = eVar;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof c) {
            return m15175().equals(((c) obj).m15175());
        }
        return false;
    }

    public int hashCode() {
        return m15175().hashCode();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract String m15172();

    /* renamed from: ʼ  reason: contains not printable characters */
    public final JSONObject m15173() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("t", (Object) this.f12003.d);
        jSONObject.put(TtmlNode.TAG_P, this.f12004);
        jSONObject.put("o", (Object) this.f12005.d);
        jSONObject.put("ai", (Object) m15180());
        jSONObject.put("n", (Object) m15175());
        if (m15179()) {
            try {
                jSONObject.put("u", (Object) m15177());
            } catch (MalformedURLException e) {
            }
        }
        if (m15174()) {
            jSONObject.put("c", (Object) m15172());
        }
        HashMap hashMap = new HashMap(this.f12002);
        JSONObject jSONObject2 = new JSONObject();
        for (Map.Entry entry : hashMap.entrySet()) {
            jSONObject2.put((String) entry.getKey(), entry.getValue());
        }
        jSONObject.put("ps", (Object) jSONObject2);
        return jSONObject;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract boolean m15174();

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract String m15175();

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m15176(String str) {
        return (String) this.f12002.get(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract URL m15177();

    /* renamed from: 齉  reason: contains not printable characters */
    public final List m15178(String str) {
        ArrayList arrayList = new ArrayList();
        String str2 = (String) this.f12002.get(str);
        if (str2 != null) {
            try {
                JSONArray jSONArray = new JSONArray(str2);
                for (int i = 0; i < jSONArray.length(); i++) {
                    arrayList.add(jSONArray.getString(i));
                }
            } catch (JSONException e) {
            }
        }
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract boolean m15179();

    /* renamed from: 龘  reason: contains not printable characters */
    public String m15180() {
        return this.f12006.m15162();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15181(String str, String str2) {
        this.f12002.put(str, str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15182(String str, List list) {
        JSONArray jSONArray = new JSONArray();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            jSONArray.put((Object) (String) it2.next());
        }
        this.f12002.put(str, jSONArray.toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m15183(String str) {
        return this.f12002.containsKey(str);
    }
}
