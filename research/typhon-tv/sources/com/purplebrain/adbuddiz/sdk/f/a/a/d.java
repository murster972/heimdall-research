package com.purplebrain.adbuddiz.sdk.f.a.a;

public enum d {
    META_DATA("D", false),
    MEDIA("M", false),
    ASSET("A", true);
    
    String d;
    public boolean e;

    private d(String str, boolean z) {
        this.d = str;
        this.e = z;
    }

    public static d a(String str) {
        for (d dVar : (d[]) f12013.clone()) {
            if (dVar.d.equals(str)) {
                return dVar;
            }
        }
        throw new IllegalArgumentException(str + " is not a valid resource type");
    }
}
