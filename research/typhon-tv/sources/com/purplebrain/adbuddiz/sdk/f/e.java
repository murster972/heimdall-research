package com.purplebrain.adbuddiz.sdk.f;

public enum e {
    PORT("PORT", "Portrait"),
    LAND("LAND", "Landscape"),
    BOTH("BOTH", "Both");
    
    public String d;
    public String e;

    private e(String str, String str2) {
        this.d = str;
        this.e = str2;
    }

    public static e a(String str) {
        for (e eVar : a()) {
            if (eVar.d.equals(str)) {
                return eVar;
            }
        }
        throw new IllegalArgumentException(str + " is not a valid orientation.");
    }

    public static e[] a() {
        return (e[]) f12075.clone();
    }

    public static e b(e eVar) {
        return LAND.equals(eVar) ? PORT : LAND;
    }

    public final boolean a(e eVar) {
        if (eVar == BOTH) {
            return false;
        }
        if (this == BOTH) {
            return true;
        }
        return equals(eVar);
    }
}
