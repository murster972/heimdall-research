package com.purplebrain.adbuddiz.sdk.f;

public final class d {

    /* renamed from: 靐  reason: contains not printable characters */
    public String f12071;

    /* renamed from: 齉  reason: contains not printable characters */
    public a f12072;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f12073;

    public enum a {
        CACHE_AD("CACHE_AD"),
        SHOW_AD("SHOW_AD"),
        IS_READY_TO_SHOW_AD("IS_READY_TO_SHOW_AD"),
        FETCH_VIDEO_AD("FETCH_VIDEO_AD"),
        IS_READY_TO_SHOW_VIDEO_AD("IS_READY_TO_SHOW_VIDEO_AD"),
        SHOW_VIDEO_AD("SHOW_VIDEO_AD"),
        SET_LOG_LEVEL("SET_LOG_LEVEL"),
        SET_PUBLISHER_KEY("SET_PUBLISHER_KEY"),
        SET_TEST_MODE_ACTIVE("SET_TEST_MODE_ACTIVE"),
        ON_DESTROY("ON_DESTROY");
        
        String k;

        private a(String str) {
            this.k = str;
        }

        public static a[] a() {
            return (a[]) f12074.clone();
        }
    }

    public d(a aVar, String str) {
        this(aVar.k, str);
        this.f12072 = aVar;
    }

    public d(String str, String str2) {
        this.f12071 = str;
        this.f12072 = null;
        if (str2 != null) {
            this.f12073 = str2;
        } else {
            this.f12073 = "";
        }
    }
}
