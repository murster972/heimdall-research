package com.purplebrain.adbuddiz.sdk.f.a;

import android.graphics.RectF;
import com.purplebrain.adbuddiz.sdk.a.c;
import com.purplebrain.adbuddiz.sdk.e.a.b.a;
import com.purplebrain.adbuddiz.sdk.e.a.b.b;
import com.purplebrain.adbuddiz.sdk.f.e;

public enum d {
    ADBUDDIZ(new a()) {
        public final c a() {
            return new com.purplebrain.adbuddiz.sdk.a.a();
        }
    },
    ADBUDDIZ_VIDEO(new b()) {
        public final RectF a(e eVar) {
            RectF a = d.super.a(eVar);
            if (eVar == e.PORT) {
                a.left = 0.35f;
                a.right = 0.35f;
            }
            return a;
        }

        public final c a() {
            return new com.purplebrain.adbuddiz.sdk.a.b();
        }
    },
    MRAID(new com.purplebrain.adbuddiz.sdk.e.a.b.d(), com.purplebrain.adbuddiz.sdk.f.a.a.d.MEDIA) {
        public final c a() {
            return new com.purplebrain.adbuddiz.sdk.a.d();
        }
    },
    VAST(new com.purplebrain.adbuddiz.sdk.e.a.b.e(), com.purplebrain.adbuddiz.sdk.f.a.a.d.META_DATA) {
        public final RectF a(e eVar) {
            RectF a = d.super.a(eVar);
            if (eVar == e.PORT) {
                a.left = 0.35f;
                a.right = 0.35f;
            }
            return a;
        }

        public final c a() {
            return new com.purplebrain.adbuddiz.sdk.a.e();
        }
    };
    
    public com.purplebrain.adbuddiz.sdk.e.a.b.c e;
    com.purplebrain.adbuddiz.sdk.f.a.a.d f;
    public boolean g;
    private String h;

    private d(String str, com.purplebrain.adbuddiz.sdk.e.a.b.c cVar, com.purplebrain.adbuddiz.sdk.f.a.a.d dVar, boolean z) {
        this.h = str;
        this.e = cVar;
        this.f = dVar;
        this.g = z;
    }

    public RectF a(e eVar) {
        RectF rectF = new RectF();
        rectF.top = 1.0f;
        rectF.right = 1.0f;
        rectF.bottom = 1.0f;
        rectF.left = 1.0f;
        return rectF;
    }

    public c a() {
        return null;
    }
}
