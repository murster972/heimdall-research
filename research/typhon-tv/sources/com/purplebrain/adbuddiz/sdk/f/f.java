package com.purplebrain.adbuddiz.sdk.f;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

public final class f {

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f12076;

    /* renamed from: ʼ  reason: contains not printable characters */
    public double f12077;

    /* renamed from: ʽ  reason: contains not printable characters */
    public double f12078;

    /* renamed from: 连任  reason: contains not printable characters */
    public Map f12079 = new HashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    public double f12080;

    /* renamed from: 麤  reason: contains not printable characters */
    public double f12081;

    /* renamed from: 齉  reason: contains not printable characters */
    public double f12082;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f12083;

    /* renamed from: 龘  reason: contains not printable characters */
    public static f m15229(JSONObject jSONObject) {
        f fVar = new f();
        fVar.f12083 = jSONObject.getString("t");
        fVar.f12080 = jSONObject.getDouble("s");
        fVar.f12082 = jSONObject.getDouble("d");
        fVar.f12081 = jSONObject.getDouble("u");
        JSONObject jSONObject2 = jSONObject.getJSONObject("f");
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            fVar.f12079.put(str, Double.valueOf(jSONObject2.getDouble(str)));
        }
        fVar.f12076 = jSONObject.getInt("b");
        fVar.f12077 = jSONObject.getDouble("bs");
        fVar.f12078 = jSONObject.getDouble("bps");
        return fVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m15230() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("t", (Object) this.f12083);
        jSONObject.put("s", this.f12080);
        jSONObject.put("d", this.f12082);
        jSONObject.put("u", this.f12081);
        JSONObject jSONObject2 = new JSONObject();
        for (Map.Entry entry : this.f12079.entrySet()) {
            jSONObject2.put((String) entry.getKey(), entry.getValue());
        }
        jSONObject.put("f", (Object) jSONObject2);
        jSONObject.put("b", this.f12076);
        jSONObject.put("bs", this.f12077);
        jSONObject.put("bps", this.f12078);
        return jSONObject;
    }
}
