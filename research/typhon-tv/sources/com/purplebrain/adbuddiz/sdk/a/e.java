package com.purplebrain.adbuddiz.sdk.a;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.View;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.f.a.a.c;
import com.purplebrain.adbuddiz.sdk.f.a.f;
import com.purplebrain.adbuddiz.sdk.i.m;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.j.a.a;
import com.purplebrain.adbuddiz.sdk.j.a.b;
import com.purplebrain.adbuddiz.sdk.j.d;

@SuppressLint({"NewApi"})
public final class e extends f {

    /* renamed from: 靐  reason: contains not printable characters */
    private d f11843 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private c f11844 = null;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final String[] m14958() {
        return com.purplebrain.adbuddiz.sdk.j.c.m15542((f) this.f11827, this.f11844);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005f  */
    /* renamed from: ˉ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m14959() {
        /*
            r10 = this;
            r1 = 1
            r2 = 0
            super.m14915()
            com.purplebrain.adbuddiz.sdk.i.c.b r0 = r10.f11862
            if (r0 == 0) goto L_0x005a
            com.purplebrain.adbuddiz.sdk.i.c.b r0 = r10.f11862
            boolean r0 = r0.m15380()
            if (r0 != 0) goto L_0x005a
            android.media.MediaPlayer r4 = r10.f11850
            com.purplebrain.adbuddiz.sdk.f.a.a r0 = r10.f11827
            com.purplebrain.adbuddiz.sdk.f.a.f r0 = (com.purplebrain.adbuddiz.sdk.f.a.f) r0
            com.purplebrain.adbuddiz.sdk.f.a.a.c r5 = r10.f11844
            java.lang.String r3 = "so"
            java.lang.String r3 = r5.m15176(r3)
            if (r3 == 0) goto L_0x005b
            r3 = r1
        L_0x0023:
            if (r3 == 0) goto L_0x005d
            boolean r3 = r4.isPlaying()
            if (r3 == 0) goto L_0x005d
            int r3 = r4.getCurrentPosition()
            long r6 = (long) r3
            java.lang.Integer.valueOf(r2)
            long r8 = com.purplebrain.adbuddiz.sdk.i.c.d.m15387(r5)
            java.lang.String[] r0 = com.purplebrain.adbuddiz.sdk.j.c.m15542(r0, r5)
            java.lang.Long r0 = com.purplebrain.adbuddiz.sdk.i.c.e.m15397(r8, r0)
            long r4 = r0.longValue()
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x005d
            r0 = r1
        L_0x0048:
            if (r0 == 0) goto L_0x005f
            com.purplebrain.adbuddiz.sdk.f.a.a.c r0 = r10.f11844
            android.media.MediaPlayer r1 = r10.f11850
            com.purplebrain.adbuddiz.sdk.j.b.b.d r2 = com.purplebrain.adbuddiz.sdk.j.b.b.d.skip
            com.purplebrain.adbuddiz.sdk.j.e.m15552((com.purplebrain.adbuddiz.sdk.f.a.a.c) r0, (android.media.MediaPlayer) r1, (com.purplebrain.adbuddiz.sdk.j.b.b.d) r2)
            com.purplebrain.adbuddiz.sdk.i.c.c r0 = r10.f11861
            com.purplebrain.adbuddiz.sdk.j.b.b.d r1 = com.purplebrain.adbuddiz.sdk.j.b.b.d.skip
            r0.m15384(r1)
        L_0x005a:
            return
        L_0x005b:
            r3 = r2
            goto L_0x0023
        L_0x005d:
            r0 = r2
            goto L_0x0048
        L_0x005f:
            com.purplebrain.adbuddiz.sdk.f.a.a.c r0 = r10.f11844
            android.media.MediaPlayer r1 = r10.f11850
            com.purplebrain.adbuddiz.sdk.j.b.b.d r2 = com.purplebrain.adbuddiz.sdk.j.b.b.d.closeLinear
            com.purplebrain.adbuddiz.sdk.j.e.m15552((com.purplebrain.adbuddiz.sdk.f.a.a.c) r0, (android.media.MediaPlayer) r1, (com.purplebrain.adbuddiz.sdk.j.b.b.d) r2)
            com.purplebrain.adbuddiz.sdk.i.c.c r0 = r10.f11861
            com.purplebrain.adbuddiz.sdk.j.b.b.d r1 = com.purplebrain.adbuddiz.sdk.j.b.b.d.closeLinear
            r0.m15384(r1)
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.a.e.m14959():void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final void m14960() {
        super.m14976();
        if (!this.f11855 && this.f11850 != null) {
            try {
                this.f11843 = new d(m14967(), this.f11850, this.f11853, this.f11861);
            } catch (b e) {
                o.m15453("ABVASTAdDisplayer.prepareTimerForTrackingEvent()", e);
                com.purplebrain.adbuddiz.sdk.j.e.m15551(this.f11844, this.f11850, e.a);
                this.f11821.m14940(e);
            }
        }
    }

    /* renamed from: י  reason: contains not printable characters */
    public final void m14961() {
        super.m14977();
        if (this.f11843 != null) {
            this.f11843.cancel();
        }
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public final void m14962() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final void m14963() {
        this.f11843.cancel();
        this.f11843.onTick(0);
        com.purplebrain.adbuddiz.sdk.j.e.m15552(m14967(), this.f11850, com.purplebrain.adbuddiz.sdk.j.b.b.d.complete);
        this.f11861.m15384(com.purplebrain.adbuddiz.sdk.j.b.b.d.complete);
        if (m14967().m15176("c") == null) {
            m14912();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final String m14964() {
        return ((f) this.f11827).f12035;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m14965(boolean z) {
        super.m14987(z);
        if (!this.f11855) {
            this.f11843.start();
        }
        if (this.f11853 == null && !this.f11855) {
            c cVar = this.f11844;
            com.purplebrain.adbuddiz.sdk.j.e.m15553("Impression", cVar.m15178("i"), cVar, (Integer) null);
            com.purplebrain.adbuddiz.sdk.j.e.m15552(this.f11844, this.f11850, com.purplebrain.adbuddiz.sdk.j.b.b.d.creativeView);
            this.f11861.m15384(com.purplebrain.adbuddiz.sdk.j.b.b.d.creativeView);
            com.purplebrain.adbuddiz.sdk.j.e.m15552(this.f11844, this.f11850, com.purplebrain.adbuddiz.sdk.j.b.b.d.b);
            this.f11861.m15384(com.purplebrain.adbuddiz.sdk.j.b.b.d.b);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final Uri m14966() {
        c r0 = m14967();
        return r0.m15179() ? Uri.parse("file://" + m.m15440(AdBuddiz.m14865(), r0)) : Uri.parse(r0.m15172());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final c m14967() {
        if (this.f11844 == null) {
            this.f11844 = com.purplebrain.adbuddiz.sdk.i.c.m15373(this.f11827, com.purplebrain.adbuddiz.sdk.f.a.a.d.MEDIA, m14914());
        }
        return this.f11844;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14968(MediaPlayer mediaPlayer) {
        com.purplebrain.adbuddiz.sdk.j.e.m15551(this.f11844, mediaPlayer, a.PROBLEM_DISPLAYING_MEDIA_FILE);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14969(MediaPlayer mediaPlayer, int i, int i2) {
        boolean z = true;
        com.purplebrain.adbuddiz.sdk.f.b r1 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
        if (this.f11843 != null && ((double) this.f11843.f12301) >= r1.f12070 * ((double) this.f11843.f12298)) {
            z = false;
        }
        if (z) {
            com.purplebrain.adbuddiz.sdk.j.e.m15551(this.f11844, mediaPlayer, a.PROBLEM_DISPLAYING_MEDIA_FILE);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14970(View view) {
        view.playSoundEffect(0);
        if (m14936() && m14916()) {
            m14917();
            c cVar = this.f11844;
            MediaPlayer mediaPlayer = this.f11850;
            com.purplebrain.adbuddiz.sdk.j.e.m15553("Click", cVar.m15178("ct"), cVar, mediaPlayer != null ? Integer.valueOf(mediaPlayer.getCurrentPosition()) : null);
            try {
                this.f11828.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(m14967().m15176("c"))));
            } catch (Exception e) {
            }
            m14918();
        }
    }
}
