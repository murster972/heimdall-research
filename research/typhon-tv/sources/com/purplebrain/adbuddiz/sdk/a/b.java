package com.purplebrain.adbuddiz.sdk.a;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.webkit.WebView;
import android.widget.ImageButton;
import com.purplebrain.adbuddiz.sdk.f.a.a.d;
import com.purplebrain.adbuddiz.sdk.f.a.c;
import com.purplebrain.adbuddiz.sdk.f.e;
import com.purplebrain.adbuddiz.sdk.i.b.f;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.m;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.s;
import com.purplebrain.adbuddiz.sdk.i.t;
import com.purplebrain.adbuddiz.sdk.i.v;
import com.purplebrain.adbuddiz.sdk.k.a;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public final class b extends f {

    /* renamed from: 靐  reason: contains not printable characters */
    protected a f11806 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    protected v f11807 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    protected WebView f11808 = null;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private a.c f11809 = new a.c() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m14909() {
            b.this.x_();
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m14896(b bVar) {
        if (!bVar.f11825.f11926) {
            com.purplebrain.adbuddiz.sdk.f.b r0 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
            if (r0 == null || r0.f12050 == null) {
                bVar.f11825.m15032(true);
                return;
            }
            bVar.f11825.m15032(false);
            bVar.f11825.m15031((Integer) null);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                public final void run() {
                    try {
                        b.this.f11825.m15032(true);
                    } catch (Throwable th) {
                        o.m15453("ABAdBuddizVideoAdDisplayer.enableCloseAfterMinTime", th);
                        l.m15430("ABAdBuddizVideoAdDisplayer.enableCloseAfterMinTime", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                    }
                }
            }, r0.f12050.longValue());
        }
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"NewApi"})
    public final WebView w_() {
        InputStream inputStream = null;
        WebView webView = new WebView(this.f11828);
        webView.setBackgroundColor(-16777216);
        webView.clearCache(true);
        this.f11806 = new a(this.f11828, (c) this.f11827);
        a aVar = this.f11806;
        aVar.f12310 = webView;
        a.m15558(webView);
        a.m15557();
        webView.setWebViewClient(new a.b(aVar, (byte) 0));
        webView.setWebChromeClient(new a.C0033a(aVar, (byte) 0));
        this.f11806.f12309 = this.f11809;
        try {
            a aVar2 = this.f11806;
            try {
                InputStream r6 = com.purplebrain.adbuddiz.sdk.e.c.m15146(aVar2.f12311, com.purplebrain.adbuddiz.sdk.i.c.m15374((com.purplebrain.adbuddiz.sdk.f.a.a) aVar2.f12308, aVar2.f12308.f12025));
                try {
                    String r2 = s.m15472(r6);
                    if (r2 == null || r2.length() <= 0) {
                        throw new IOException("Cannot read ad content from resource.");
                    }
                    aVar2.f12310.loadDataWithBaseURL((String) null, r2, "text/html", "UTF-8", (String) null);
                    try {
                        r6.close();
                    } catch (Throwable th) {
                    }
                    return webView;
                } catch (Throwable th2) {
                    th = th2;
                    inputStream = r6;
                    try {
                        inputStream.close();
                    } catch (Throwable th3) {
                    }
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
            }
        } catch (IOException e) {
            o.m15453("Cannot load end interstitial into WebView.", e);
            this.f11821.m14940(e);
        }
    }

    /* access modifiers changed from: protected */
    public final void x_() {
        try {
            if (m14936() && m14916()) {
                m14917();
                this.f11824.setVisibility(0);
                this.f11825.m15032(false);
                String r0 = t.m15473(this.f11828, ((com.purplebrain.adbuddiz.sdk.f.a.b) this.f11827).f12021);
                this.f11807 = new v(this.f11828);
                this.f11807.f12251 = new v.b() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public final void m14908() {
                        b.this.f11824.setVisibility(8);
                        if (b.this.f11855) {
                            b.this.f11825.m15032(true);
                        }
                        b.this.m14918();
                    }
                };
                this.f11807.m15478(r0);
                new Handler().postDelayed(new Runnable() {
                    public final void run() {
                        try {
                            b.this.f11825.m15032(true);
                        } catch (Throwable th) {
                        }
                    }
                }, com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138().f12052);
            }
        } catch (Throwable th) {
            this.f11830 = null;
            l.m15430("ABAdBuddizViewAdDisplayer#onEndInterstitialClickListener.onClick", this.f11827, th);
            o.m15453("ABAdBuddizViewAdDisplayer#onEndInterstitialClickListener.onClick", th);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final String[] m14897() {
        return new String[]{((c) this.f11827).f12024};
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final Double m14898() {
        if (!this.f11855) {
            return super.m14974();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final ImageButton m14899() {
        if (this.f11855) {
            return null;
        }
        return super.m14975();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    /* renamed from: ٴ  reason: contains not printable characters */
    public final void m14900() {
        this.f11850.reset();
        this.f11850.release();
        this.f11850 = null;
        com.purplebrain.adbuddiz.sdk.d.a aVar = this.f11825;
        if (aVar.f11925 != -16777216) {
            aVar.f11925 = -16777216;
            aVar.f11923 = aVar.m15027();
            aVar.invalidate();
        }
        this.f11852.setMinimumHeight(this.f11852.getHeight());
        this.f11852.setMinimumWidth(this.f11852.getWidth());
        com.purplebrain.adbuddiz.sdk.d.a aVar2 = this.f11825;
        RectF rectF = new RectF();
        Rect rect = new Rect();
        aVar2.m15026(rect);
        aVar2.m15029(rectF, aVar2.getWidth(), aVar2.getHeight(), rect, true);
        float width = rectF.width() / rectF.height();
        this.f11825.removeView(this.f11851);
        final View view = new View(this.f11828);
        view.setBackgroundColor(-16777216);
        view.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.f11825.addView(view, 1);
        AnonymousClass1 r2 = new Animator.AnimatorListener() {
            public final void onAnimationCancel(Animator animator) {
            }

            public final void onAnimationEnd(Animator animator) {
                try {
                    b.this.f11825.removeViewInLayout(view);
                    b.this.f11808 = b.this.w_();
                    b.this.f11825.addView(b.this.f11808, 0);
                    b.m14896(b.this);
                } catch (Throwable th) {
                    o.m15453("ABAdBuddizVideoAdDisplayer.onMediaPlayerComplete#AnimatorListener.onAnimationEnd", th);
                    l.m15430("ABAdBuddizVideoAdDisplayer.onMediaPlayerComplete#AnimatorListener.onAnimationEnd", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                }
            }

            public final void onAnimationRepeat(Animator animator) {
            }

            public final void onAnimationStart(Animator animator) {
                try {
                    b.this.f11825.removeView(b.this.f11852);
                } catch (Throwable th) {
                    o.m15453("ABAdBuddizVideoAdDisplayer.onMediaPlayerComplete#AnimatorListener.onAnimationStart", th);
                    l.m15430("ABAdBuddizVideoAdDisplayer.onMediaPlayerComplete#AnimatorListener.onAnimationStart", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                }
            }
        };
        int i = 300;
        if (m14914() == e.LAND) {
            i = 100;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{this.f11825.f11897.floatValue(), width});
        ofFloat.setStartDelay(50);
        ofFloat.setDuration((long) i);
        ofFloat.setInterpolator(new AccelerateInterpolator());
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                try {
                    b.this.f11825.m15030(Double.valueOf(((Float) valueAnimator.getAnimatedValue()).doubleValue()));
                } catch (Throwable th) {
                    o.m15453("ABAdBuddizVideoAdDisplayer.onMediaPlayerComplete#AnimatorUpdateListener.onAnimationUpdate", th);
                    l.m15430("ABAdBuddizVideoAdDisplayer.onMediaPlayerComplete#AnimatorUpdateListener.onAnimationUpdate", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                }
            }
        });
        ofFloat.addListener(r2);
        ofFloat.start();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final String m14901() {
        return ((c) this.f11827).f12028;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final View m14902() {
        if (!this.f11855) {
            return super.m14984();
        }
        this.f11808 = w_();
        return this.f11808;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final Uri m14903() {
        com.purplebrain.adbuddiz.sdk.f.a.a.c r0 = m14904();
        if (r0 == null) {
            throw new Exception("Cannot find video resource");
        }
        return Uri.parse("file://" + m.m15440((Context) this.f11828, r0));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final com.purplebrain.adbuddiz.sdk.f.a.a.c m14904() {
        com.purplebrain.adbuddiz.sdk.f.a.a.c cVar = null;
        Iterator it2 = com.purplebrain.adbuddiz.sdk.i.c.m15375(this.f11827, d.MEDIA, true, m14914()).iterator();
        while (cVar == null && it2.hasNext()) {
            com.purplebrain.adbuddiz.sdk.f.a.a.c cVar2 = (com.purplebrain.adbuddiz.sdk.f.a.a.c) it2.next();
            if (!cVar2.m15183("isVideo")) {
                cVar2 = cVar;
            }
            cVar = cVar2;
        }
        return cVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14905(MediaPlayer mediaPlayer) {
        f.m15371(this.f11828, (c) this.f11827);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m14906(android.media.MediaPlayer r3, int r4, int r5) {
        /*
            r2 = this;
            super.m14992(r3, r4, r5)
            java.lang.Integer r0 = r2.f11853
            if (r0 == 0) goto L_0x000f
            java.lang.Integer r0 = r2.f11853
            int r0 = r0.intValue()
            if (r0 > 0) goto L_0x002d
        L_0x000f:
            r0 = 0
            com.purplebrain.adbuddiz.sdk.i.c.b r1 = r2.f11862
            if (r1 == 0) goto L_0x0018
            com.purplebrain.adbuddiz.sdk.i.c.b r0 = r2.f11862
            java.lang.Integer r0 = r0.f12208
        L_0x0018:
            if (r0 == 0) goto L_0x0020
            int r0 = r0.intValue()
            if (r0 != 0) goto L_0x002d
        L_0x0020:
            r0 = 1
        L_0x0021:
            if (r0 == 0) goto L_0x002c
            android.app.Activity r1 = r2.f11828
            com.purplebrain.adbuddiz.sdk.f.a.a r0 = r2.f11827
            com.purplebrain.adbuddiz.sdk.f.a.c r0 = (com.purplebrain.adbuddiz.sdk.f.a.c) r0
            com.purplebrain.adbuddiz.sdk.i.b.f.m15371(r1, r0)
        L_0x002c:
            return
        L_0x002d:
            r0 = 0
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.a.b.m14906(android.media.MediaPlayer, int, int):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14907(View view) {
        if (!this.f11827.f11995) {
            view.playSoundEffect(0);
            x_();
        }
    }
}
