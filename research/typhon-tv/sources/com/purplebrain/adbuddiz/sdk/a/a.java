package com.purplebrain.adbuddiz.sdk.a;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.purplebrain.adbuddiz.sdk.f.a.a.c;
import com.purplebrain.adbuddiz.sdk.f.a.a.d;
import com.purplebrain.adbuddiz.sdk.f.a.b;
import com.purplebrain.adbuddiz.sdk.f.e;
import com.purplebrain.adbuddiz.sdk.i.b.f;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.t;
import com.purplebrain.adbuddiz.sdk.i.v;
import java.io.InputStream;

public final class a extends c {

    /* renamed from: 龘  reason: contains not printable characters */
    protected v f11802 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private Bitmap m14891() {
        InputStream inputStream;
        e r2 = m14914();
        c r0 = com.purplebrain.adbuddiz.sdk.i.c.m15373(this.f11827, d.MEDIA, r2);
        if (r0 == null) {
            o.m15451("No matching creative available for '" + r2.e + "' orientation.");
            o.m15451("Please check that your app has the following orientation on the AdBuddiz publisher portal: '" + r2.e + "' or 'Both'.");
            r0 = com.purplebrain.adbuddiz.sdk.i.c.m15373(this.f11827, d.MEDIA, e.b(r2));
        }
        try {
            inputStream = com.purplebrain.adbuddiz.sdk.e.c.m15146((Context) this.f11828, r0);
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
                try {
                    inputStream.close();
                } catch (Throwable th) {
                }
                if (decodeStream != null) {
                    return decodeStream;
                }
                o.m15451("ABAdBuddizAdDisplayer.getBitmap() : Decode returned null.");
                f.m15371(this.f11828, this.f11827);
                this.f11821.m14940(new Exception("Bitmap returned null."));
                return decodeStream;
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            try {
                inputStream.close();
            } catch (Throwable th4) {
            }
            throw th;
        }
        return null;
        try {
            o.m15453("ABAdBuddizAdDisplayer.getBitmap() : Cannot decode bitmap.", th);
            f.m15371(this.f11828, this.f11827);
            this.f11821.m14940(th);
            try {
                inputStream.close();
            } catch (Throwable th5) {
            }
            return null;
        } catch (Throwable th6) {
            th = th6;
            inputStream.close();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final View m14892() {
        Bitmap r0 = m14891();
        ImageView imageView = new ImageView(this.f11828);
        imageView.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        if (r0 != null) {
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setImageBitmap(r0);
            imageView.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    try {
                        if (a.this.m14936() && a.this.m14916()) {
                            a.this.m14917();
                            a.this.f11824.setVisibility(0);
                            a.this.f11825.m15032(false);
                            String r0 = t.m15473(a.this.f11828, ((b) a.this.f11827).f12021);
                            a.this.f11802 = new v(a.this.f11828);
                            a.this.f11802.f12251 = new v.b() {
                                /* renamed from: 龘  reason: contains not printable characters */
                                public final void m14895() {
                                    a.this.f11824.setVisibility(8);
                                    a.this.f11825.m15032(true);
                                    a.this.m14918();
                                }
                            };
                            a.this.f11802.m15478(r0);
                            new Handler().postDelayed(new Runnable() {
                                public final void run() {
                                    try {
                                        a.this.f11825.m15032(true);
                                    } catch (Throwable th) {
                                    }
                                }
                            }, com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138().f12052);
                        }
                    } catch (Throwable th) {
                        a.this.f11830 = null;
                        l.m15430("ABAdBuddizAdDisplayer.createAdView.onClick", a.this.f11827, th);
                        o.m15453("ABAdDisplayer.createAdView().onClick() Ad Exception : ", th);
                    }
                }
            });
        }
        return imageView;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m14893(Activity activity, Bundle bundle, com.purplebrain.adbuddiz.sdk.f.a.a aVar, String str) {
        View r0 = super.m14927(activity, bundle, aVar, str);
        this.f11826.m14942(aVar, str, true);
        return r0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14894() {
        super.m14928();
        if (this.f11802 != null) {
            v vVar = this.f11802;
            if (vVar.f12252 != null) {
                vVar.f12252.m15480();
            }
        }
    }
}
