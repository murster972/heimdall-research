package com.purplebrain.adbuddiz.sdk.a;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import com.google.android.exoplayer2.util.MimeTypes;
import com.purplebrain.adbuddiz.sdk.d.a;
import com.purplebrain.adbuddiz.sdk.g.b;
import com.purplebrain.adbuddiz.sdk.g.c;
import com.purplebrain.adbuddiz.sdk.g.e;
import com.purplebrain.adbuddiz.sdk.g.f;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.s;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.lang3.StringUtils;

public final class d extends c implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static CountDownTimer f11834 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    private c.C0030c f11835 = new c.C0030c() {
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m14955(Uri uri) {
            d.m14943(d.this, uri);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m14956() {
            d.this.m14912();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m14957(Uri uri) {
            d.m14945(d.this, uri);
        }
    };

    /* renamed from: ˎ  reason: contains not printable characters */
    private c.d f11836 = new c.d() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m14954(String str, String str2) {
            e r0 = d.this.f11838;
            e.a a = e.a.a(str);
            synchronized (r0.f12114) {
                r0.f12114.put(str2, a);
                new StringBuilder().append(r0.m15255()).append(StringUtils.SPACE).append(a.name()).append(" URL ").append(str2);
            }
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private b f11837;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public e f11838 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private c f11839;

    /* renamed from: 靐  reason: contains not printable characters */
    static /* synthetic */ void m14943(d dVar, Uri uri) {
        if (dVar.m14936()) {
            Intent intent = new Intent("android.intent.action.VIEW", uri);
            intent.setDataAndType(uri, MimeTypes.VIDEO_MP4);
            try {
                dVar.f11828.startActivity(intent);
            } catch (Exception e) {
                o.m15453("ABMRAIDAdDisplayer.playVideo()", e);
            }
            if (dVar.m14916()) {
                dVar.m14917();
                dVar.m14918();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m14945(d dVar, Uri uri) {
        if (dVar.m14936()) {
            try {
                dVar.f11828.startActivity(new Intent("android.intent.action.VIEW", uri));
            } catch (Exception e) {
                o.m15453("ABMRAIDAdDisplayer.openUri()", e);
            }
            if (dVar.m14916()) {
                dVar.m14917();
                dVar.m14918();
            }
        }
    }

    public final void onGlobalLayout() {
        c cVar = this.f11839;
        boolean z = (cVar.f12096.x == 0 && cVar.f12096.y == 0) ? false : true;
        c cVar2 = this.f11839;
        DisplayMetrics r4 = cVar2.m15247();
        Point point = new Point();
        if (cVar2.f12102.f12038 == null || cVar2.f12102.f12037 == null) {
            point.x = (int) Math.floor((double) (((float) cVar2.f12104.getWidth()) / r4.density));
            point.y = (int) Math.floor((double) (((float) cVar2.f12104.getHeight()) / r4.density));
        } else {
            point.x = cVar2.f12102.f12038.intValue();
            point.y = cVar2.f12102.f12037.intValue();
        }
        if (!(point.x == cVar2.f12096.x && point.y == cVar2.f12096.y)) {
            cVar2.f12103.m15235("_setMaxSize", Integer.valueOf(point.x), Integer.valueOf(point.y));
            cVar2.f12103.m15235("_setScreenSize", Integer.valueOf(point.x), Integer.valueOf(point.y));
            if (cVar2.f12094) {
                cVar2.f12103.m15234(com.purplebrain.adbuddiz.sdk.g.d.SIZE_CHANGE, Integer.valueOf(cVar2.f12096.x), Integer.valueOf(cVar2.f12096.y));
            }
            cVar2.f12096 = point;
        }
        c cVar3 = this.f11839;
        boolean z2 = cVar3.f12104.getVisibility() == 0;
        if (cVar3.f12095 != z2) {
            cVar3.f12095 = z2;
            cVar3.f12103.m15235("_setVisible", Boolean.valueOf(z2));
            if (cVar3.f12094) {
                cVar3.f12103.m15234(com.purplebrain.adbuddiz.sdk.g.d.VIEWABLE_CHANGE, Boolean.valueOf(z2));
            }
        }
        if (!z) {
            c cVar4 = this.f11839;
            if (!(cVar4.f12102.f12037 == null || cVar4.f12102.f12038 == null)) {
                DisplayMetrics r3 = cVar4.m15247();
                cVar4.f12104.setInitialScale((int) Math.round(Math.min(((double) cVar4.f12104.getWidth()) / (((double) cVar4.f12102.f12038.intValue()) * ((double) r3.density)), ((double) cVar4.f12104.getHeight()) / (((double) cVar4.f12102.f12037.intValue()) * ((double) r3.density))) * ((double) r3.density) * 100.0d));
            }
            this.f11839.m15248(f.DEFAULT);
            c cVar5 = this.f11839;
            if (!cVar5.f12094) {
                cVar5.f12094 = true;
                cVar5.f12103.m15234(com.purplebrain.adbuddiz.sdk.g.d.READY, new Object[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final void m14946() {
        super.m14919();
        this.f11839.m15248(f.HIDDEN);
        c cVar = this.f11839;
        if (cVar.f12104 != null && Build.VERSION.SDK_INT >= 11) {
            cVar.f12104.onPause();
        }
        if (this.f11837 != null) {
            this.f11837.cancel();
        }
    }

    /* renamed from: י  reason: contains not printable characters */
    public final void m14947() {
        super.m14920();
        AnonymousClass1 r0 = new CountDownTimer() {
            public final void onFinish() {
                d.this.f11826.m14942(d.this.f11827, d.this.f11817, Boolean.valueOf(d.this.f11838.m15254()));
            }

            public final void onTick(long j) {
            }
        };
        f11834 = r0;
        r0.start();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public final void m14948() {
        if (this.f11828 != null) {
            this.f11828.findViewById(16908290).getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public final a m14949() {
        com.purplebrain.adbuddiz.sdk.f.a.f fVar = (com.purplebrain.adbuddiz.sdk.f.a.f) this.f11827;
        a r1 = super.m14922();
        if (!(fVar.f12038 == null || fVar.f12037 == null)) {
            r1.m15030(Double.valueOf(((double) fVar.f12038.intValue()) / ((double) fVar.f12037.intValue())));
        }
        return r1;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    /* renamed from: 靐  reason: contains not printable characters */
    public final View m14950() {
        InputStream inputStream = null;
        WebView webView = new WebView(this.f11828);
        this.f11839 = new c(this.f11828, (com.purplebrain.adbuddiz.sdk.f.a.f) this.f11827);
        this.f11839.f12099 = this.f11835;
        this.f11839.f12100 = this.f11836;
        c cVar = this.f11839;
        cVar.f12101 = f.LOADING;
        cVar.f12094 = false;
        cVar.f12095 = false;
        cVar.f12104 = webView;
        webView.setWebViewClient(new c.b(cVar, (byte) 0));
        webView.setWebChromeClient(new c.a(cVar, (byte) 0));
        c.m15244(webView);
        c.m15243();
        cVar.f12103 = new com.purplebrain.adbuddiz.sdk.g.a(webView);
        com.purplebrain.adbuddiz.sdk.g.a.m15232(webView);
        if (Build.VERSION.SDK_INT >= 18) {
            cVar.f12103.m15235("_setSupportFor", "inlineVideo", "true");
        }
        try {
            c cVar2 = this.f11839;
            try {
                InputStream r6 = com.purplebrain.adbuddiz.sdk.e.c.m15146(cVar2.f12105, com.purplebrain.adbuddiz.sdk.i.c.m15373(cVar2.f12102, com.purplebrain.adbuddiz.sdk.f.a.a.d.MEDIA, com.purplebrain.adbuddiz.sdk.i.a.d.m15318()));
                try {
                    String r2 = s.m15472(r6);
                    if (r2 == null || r2.length() <= 0) {
                        throw new IOException("Cannot read ad content from resource.");
                    }
                    cVar2.f12104.loadDataWithBaseURL((String) null, r2, "text/html", "UTF-8", (String) null);
                    try {
                        r6.close();
                    } catch (Throwable th) {
                    }
                    return webView;
                } catch (Throwable th2) {
                    th = th2;
                    inputStream = r6;
                    try {
                        inputStream.close();
                    } catch (Throwable th3) {
                    }
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
            }
        } catch (IOException e) {
            this.f11821.m14940(e);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final ViewGroup.LayoutParams m14951(View view) {
        return new ViewGroup.LayoutParams(-1, -1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m14952(Activity activity, Bundle bundle, com.purplebrain.adbuddiz.sdk.f.a.a aVar, String str) {
        if (f11834 != null) {
            f11834.cancel();
            f11834 = null;
        }
        this.f11838 = new e();
        View r0 = super.m14927(activity, bundle, aVar, str);
        activity.findViewById(16908290).getViewTreeObserver().addOnGlobalLayoutListener(this);
        this.f11825.m15032(true);
        if (com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138().f12053 != null && (bundle == null || !bundle.getBoolean("canClose", false))) {
            this.f11825.m15032(false);
            this.f11837 = b.m15236(this.f11825, this.f11838);
            this.f11837.start();
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14953(Bundle bundle) {
        bundle.putBoolean("canClose", this.f11837 == null || this.f11837.f12086);
    }
}
