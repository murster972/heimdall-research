package com.purplebrain.adbuddiz.sdk.a;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.purplebrain.adbuddiz.sdk.b.h;
import com.purplebrain.adbuddiz.sdk.d.a;
import com.purplebrain.adbuddiz.sdk.i.c.b;
import com.purplebrain.adbuddiz.sdk.i.c.c;
import com.purplebrain.adbuddiz.sdk.i.c.d;
import com.purplebrain.adbuddiz.sdk.i.c.e;
import com.purplebrain.adbuddiz.sdk.i.o;
import java.util.Locale;

@SuppressLint({"NewApi"})
public abstract class f extends c {

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private View.OnClickListener f11845 = new View.OnClickListener() {
        public final void onClick(View view) {
            boolean z = false;
            try {
                f fVar = f.this;
                if (!f.this.f11859) {
                    z = true;
                }
                fVar.m14996(z, false);
            } catch (Throwable th) {
                o.m15453("ABVASTAdDisplayer#onToggleMuteButtonClicked.onClick()", th);
            }
        }
    };

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private MediaPlayer.OnCompletionListener f11846 = new MediaPlayer.OnCompletionListener() {
        public final void onCompletion(MediaPlayer mediaPlayer) {
            try {
                f.this.m14979();
                if (f.this.f11862 != null) {
                    f.this.f11862.cancel();
                }
                if (!f.this.f11855 && !f.this.f11857) {
                    f.this.f11855 = true;
                    if (f.this.f11860 != null) {
                        e eVar = f.this.f11860;
                        eVar.cancel();
                        eVar.m15400();
                    }
                    f.this.m14980();
                }
                f.this.m14982();
                f.this.f11849.setKeepScreenOn(false);
            } catch (Throwable th) {
                o.m15453("ABVideoAdDisplayer#OnCompletionListener.onCompletion()", th);
            }
        }
    };

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private MediaPlayer.OnInfoListener f11847 = new MediaPlayer.OnInfoListener() {
        public final boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
            if (i != 3) {
                return false;
            }
            f.this.f11852.setBackgroundColor(0);
            return true;
        }
    };

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private MediaPlayer.OnErrorListener f11848 = new MediaPlayer.OnErrorListener() {
        public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            try {
                f.this.f11857 = true;
                String format = String.format(Locale.US, "Error while playing the media. What: %d. Extra: %d.", new Object[]{Integer.valueOf(i), Integer.valueOf(i2)});
                o.m15451(format);
                if (f.m14972(f.this)) {
                    int i3 = -1;
                    if (!(f.this.f11862 == null || f.this.f11862.f12208 == null)) {
                        i3 = f.this.f11862.f12208.intValue();
                    }
                    long r2 = d.m15387(f.this.m14989());
                    f.this.f11821.m14940(new Exception(format + String.format(" Position: %d / %d", new Object[]{Integer.valueOf(i3), Long.valueOf(r2)})));
                }
                f.this.m14992(mediaPlayer, i, i2);
            } catch (Throwable th) {
                o.m15453("ABVASTAdDisplayer#OnErrorListener.onError()", th);
            }
            return false;
        }
    };

    /* renamed from: ˆ  reason: contains not printable characters */
    protected SurfaceHolder f11849 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    protected MediaPlayer f11850 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    protected RelativeLayout f11851 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    protected SurfaceView f11852 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    protected Integer f11853 = null;

    /* renamed from: י  reason: contains not printable characters */
    protected int f11854 = 0;

    /* renamed from: ـ  reason: contains not printable characters */
    protected boolean f11855 = false;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private MediaPlayer.OnPreparedListener f11856 = new MediaPlayer.OnPreparedListener() {
        public final void onPrepared(MediaPlayer mediaPlayer) {
            try {
                f.this.m14976();
                f.this.m14987(false);
            } catch (Throwable th) {
                o.m15453("ABVASTAdDisplayer#OnPreparedListener.onPrepared()", th);
            }
        }
    };

    /* renamed from: ᴵ  reason: contains not printable characters */
    protected boolean f11857 = false;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private View.OnClickListener f11858 = new View.OnClickListener() {
        public final void onClick(View view) {
            try {
                f.this.m14994(view);
            } catch (Throwable th) {
                o.m15453("ABVASTAdDisplayer#OnClickListener.onClick", th);
            }
        }
    };

    /* renamed from: ᵎ  reason: contains not printable characters */
    boolean f11859;

    /* renamed from: ᵔ  reason: contains not printable characters */
    e f11860 = null;

    /* renamed from: ᵢ  reason: contains not printable characters */
    protected c f11861 = null;

    /* renamed from: ⁱ  reason: contains not printable characters */
    protected b f11862 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private Drawable f11863 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private Drawable f11864 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private ImageButton f11865 = null;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private WifiManager.WifiLock f11866 = null;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private SurfaceHolder.Callback f11867 = new SurfaceHolder.Callback() {
        public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        public final void surfaceCreated(SurfaceHolder surfaceHolder) {
            try {
                if (f.this.f11850 != null) {
                    f.this.f11850.setSurface(surfaceHolder.getSurface());
                }
            } catch (Throwable th) {
                o.m15453("ABVASTAdDisplayer#Callback.surfaceCreated()", th);
            }
        }

        public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            try {
                if (f.this.f11850 != null) {
                    f.this.f11850.setSurface((Surface) null);
                }
            } catch (Throwable th) {
                o.m15453("ABVASTAdDisplayer#Callback.surfaceDestroyed()", th);
            }
        }
    };

    /* renamed from: ᵔ  reason: contains not printable characters */
    private MediaPlayer m14971() {
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(this.f11828, m14988());
            mediaPlayer.setOnPreparedListener(this.f11856);
            mediaPlayer.setOnInfoListener(this.f11847);
            mediaPlayer.setOnCompletionListener(this.f11846);
            mediaPlayer.setOnErrorListener(this.f11848);
            return mediaPlayer;
        } catch (Exception e) {
            m14991(mediaPlayer);
            this.f11821.m14940(new Exception("MediaPlayer.create() returned null.", e));
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ boolean m14972(f fVar) {
        return !fVar.f11828.isFinishing();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract String[] m14973();

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public Double m14974() {
        com.purplebrain.adbuddiz.sdk.f.a.a.c r0 = m14989();
        if (r0 == null) {
            return null;
        }
        return Double.valueOf(((double) d.m15390(r0)) / ((double) d.m15389(r0)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public ImageButton m14975() {
        if (this.f11827.f11995) {
            return null;
        }
        this.f11865 = new ImageButton(this.f11828);
        this.f11865.setBackgroundDrawable((Drawable) null);
        this.f11865.setOnClickListener(this.f11845);
        try {
            byte[] r0 = com.purplebrain.adbuddiz.sdk.i.f.m15410("iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAONJREFUeNrs3EEOgkAMQFEx3Ft68jEeQKImzLT1/YQdC/ICm2nDNsa46X13BIAAAQIECBAgQAIEqD/QAegc5wGoEE4WoLQ4GYBS46wGSo+zEqgEziqgMjgrgErhzAYqhzMTqCTOLKCyODOASuNcDVQe50qgFjiv9g/v+9v5tAMzQIAAAQIECBAgAQIECBAgQIAA6Qeg7csrvEHnHV2Qrp5qBKDmSLMmqwGoKdLs7Y4A1Axp1YZZAGqCtHrLNQAVR8qyaR+AiiLtiZ7lyAi0+XdHjU8MECBAgAQIECBAgAC16inAAAx7JBiffjKJAAAAAElFTkSuQmCC".toCharArray());
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(r0, 0, r0.length);
            decodeByteArray.setDensity(480);
            this.f11863 = m14926(decodeByteArray);
            byte[] r02 = com.purplebrain.adbuddiz.sdk.i.f.m15410("iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA5VJREFUeNrsmz1oFEEUx3cvJwomnBi0ESSIRgSjiZLCQhCChagg2iSFICJRG7EJiincFCqihXZGxSKKdoIflRYpoogmpyQGMVFO8QMFFT9AxTNk/S/uwRU3M29u9zYzO/Pgx8G8vTdz/9uZnY+3ru/7jjW2ZawEViArkBXICmQFsgJZgQy1LOUi13VnvKGY0Lbg42nS9boazaSDhn4At8EVMBSWWYHKBCq3SXAaDIA/poxBnsS1zeA8eAG6atm3VcHz/xvLL7JB0BR3uzIK3TlHI8bYAMZAZ9q6GFUcyjjTAK6BE8H4moYu5lXoKqxrc2AXGPFpdglkorZRNXF84nc3gjxBpAHg6iiQx/lR1BjB3XEQ/BKIdFw3gTzBD5KN1wImBTE7dRHII3SLauI2ggecmD/AUtUF8oiDa7XxGwQiDaosEFUcP2I988FLTuydsjGTWIvJTgJdzpxtmvD9lWAYzKngewOWgaIqE8U4Zsgly4N1hOvGwRGGbzHYo8pEUaZbUbpYYFPgMKHuOjDKiD8hMzdSTRzqYvUsoQ2bOHV0xC1QkkZtwyFCux8z6rio22q+GjtGGJP6GeVbqeMv9SmW5LajK9GGUdDGaV8OfGbsva8BT9J+qrEabOP4v4N7DB/liZiKY59ugX+IUb7WFIE6QD3HP8IoX2KKQLNAO8dfYJQ3mXSy2szxvecM4MYI1Mjx/WSU15skUBwHkakW6AvHN1fyzkqlQBMc3yJG+TdTBPob7v84ko/z16YIdFfQXdolH/+pE6hf4F8vOYFM1WI1H94hrPbNA58Yi9XWcLGb2jtoCuwX/HldDHE+OsRsNZ0F6hEMzoHtY5TfcmgHAGSBXEn6aizOSXBGcM0WsIrhu0qfTuq1aV8EBwh1Z8E4I/4zlQ4OvRgFug9aifX2cOJ3p/XgkGpBt3oEZlfwvQLLw8mlo8og7SUwJpVsAbjBECewXhlxaj0GJZ28kBNkn90xOf1lIRjmxPxabQZsGhKo2kBBEHOHiSl4deHT6rdAnD4Tkzg3gzFCd72gc5arbBpwkBy1lyhMYOeiZriqIFAlkVjXFYnCTIcxY2mfau9pRHlXo/S02h5n27KKrMy98DNKNlqws7gbvIuzYaoIVC6SrBXCrY/rNdm90/iFuiAX8VS4dTFVq0qzjl72FtwEl8HDJCrUSaAV4HnSlerUxWbE7HvzViArkBXICmQFsgJZgUy1fwIMAL0jl379L/YCAAAAAElFTkSuQmCC".toCharArray());
            Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(r02, 0, r02.length);
            decodeByteArray2.setDensity(480);
            this.f11864 = m14926(decodeByteArray2);
        } catch (h e) {
        }
        m14996(this.f11859, true);
        return this.f11865;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m14976() {
        if (this.f11850 != null) {
            this.f11862 = new b(this.f11825, this.f11850);
        }
        this.f11860 = null;
        com.purplebrain.adbuddiz.sdk.f.a.a.c r0 = m14989();
        if (!(this.f11850 == null || r0 == null || this.f11855)) {
            this.f11860 = e.m15396(this.f11827, this.f11825, this.f11850, d.m15387(r0), this.f11854, m14973());
        }
        this.f11825.m15032(this.f11860 == null);
        com.purplebrain.adbuddiz.sdk.f.a.a.c r02 = m14989();
        if (this.f11850 != null && r02 != null) {
            this.f11861 = new c(this.f11827, this.f11817, m14983(), d.m15387(r02), this.f11854, this.f11860, this.f11826);
        }
    }

    /* renamed from: י  reason: contains not printable characters */
    public void m14977() {
        if (this.f11850 != null) {
            try {
                if (this.f11850.isPlaying()) {
                    this.f11850.pause();
                }
            } catch (IllegalStateException e) {
            }
        }
        if (this.f11860 != null) {
            this.f11860.cancel();
        }
        if (this.f11861 != null) {
            c cVar = this.f11861;
            if (cVar.m15381() && cVar.f12215 != null) {
                cVar.f12215.cancel();
            }
        }
        if (this.f11862 != null) {
            this.f11862.cancel();
        }
        m14982();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public void m14978() {
        if (this.f11850 != null) {
            try {
                this.f11850.reset();
                this.f11850.release();
            } catch (IllegalStateException e) {
            }
            this.f11850 = null;
        }
        if (this.f11849 != null && this.f11849.getSurface() != null) {
            this.f11849.getSurface().release();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m14979() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m14980() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public final a m14981() {
        a r0 = super.m14922();
        r0.m15030(m14974());
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public final void m14982() {
        if (this.f11866 != null) {
            this.f11866.release();
            this.f11866 = null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public abstract String m14983();

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public View m14984() {
        this.f11852 = new SurfaceView(this.f11828);
        this.f11852.setZOrderMediaOverlay(true);
        this.f11852.setSoundEffectsEnabled(true);
        this.f11852.setOnClickListener(this.f11858);
        if (Build.VERSION.SDK_INT >= 17) {
            this.f11852.setBackgroundColor(-16777216);
        }
        this.f11849 = this.f11852.getHolder();
        this.f11849.setKeepScreenOn(true);
        this.f11849.addCallback(this.f11867);
        this.f11849.setSizeFromLayout();
        this.f11850 = m14971();
        try {
            if (this.f11850 != null) {
                this.f11850.prepareAsync();
            }
        } catch (Throwable th) {
            m14991(this.f11850);
            this.f11821.m14940(th);
        }
        return this.f11852;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final ViewGroup.LayoutParams m14985(View view) {
        return new ViewGroup.LayoutParams(-2, -2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m14986(Bundle bundle) {
        this.f11855 = bundle.getBoolean("complete");
        m14996(bundle.getBoolean("muted"), true);
        if (bundle.containsKey("pos")) {
            this.f11853 = Integer.valueOf(bundle.getInt("pos"));
        }
        this.f11854 = bundle.getInt("max");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0073  */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m14987(boolean r7) {
        /*
            r6 = this;
            java.lang.Integer r0 = r6.f11853
            if (r0 == 0) goto L_0x0011
            if (r7 != 0) goto L_0x0011
            android.media.MediaPlayer r0 = r6.f11850
            java.lang.Integer r1 = r6.f11853
            int r1 = r1.intValue()
            r0.seekTo(r1)
        L_0x0011:
            android.media.MediaPlayer r0 = r6.f11850
            r0.start()
            java.lang.String r0 = "file"
            android.net.Uri r1 = r6.m14988()     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r1 = r1.getScheme()     // Catch:{ Exception -> 0x00a4 }
            boolean r0 = r0.equalsIgnoreCase(r1)     // Catch:{ Exception -> 0x00a4 }
            if (r0 == 0) goto L_0x007e
        L_0x0027:
            boolean r0 = r6.f11855
            if (r0 != 0) goto L_0x007d
            com.purplebrain.adbuddiz.sdk.i.c.c r0 = r6.f11861
            boolean r1 = r0.m15385()
            if (r1 != 0) goto L_0x00d6
            boolean r1 = r0.m15381()
            if (r1 == 0) goto L_0x00a6
            java.lang.String r1 = r0.f12219
            long r2 = r0.f12216
            com.purplebrain.adbuddiz.sdk.i.c.a r1 = com.purplebrain.adbuddiz.sdk.i.c.a.m15379(r1, r2)
            long r2 = r1.f12203
            int r1 = r0.f12218
            long r4 = (long) r1
            long r2 = r2 - r4
            java.lang.Long r1 = java.lang.Long.valueOf(r2)
            if (r1 == 0) goto L_0x006f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Impression will be send in "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r3 = "ms"
            r2.append(r3)
            com.purplebrain.adbuddiz.sdk.i.c.c$a r2 = new com.purplebrain.adbuddiz.sdk.i.c.c$a
            long r4 = r1.longValue()
            r2.<init>(r4)
            r0.f12215 = r2
            com.purplebrain.adbuddiz.sdk.i.c.c$a r0 = r0.f12215
            r0.start()
        L_0x006f:
            com.purplebrain.adbuddiz.sdk.i.c.e r0 = r6.f11860
            if (r0 == 0) goto L_0x0078
            com.purplebrain.adbuddiz.sdk.i.c.e r0 = r6.f11860
            r0.start()
        L_0x0078:
            com.purplebrain.adbuddiz.sdk.i.c.b r0 = r6.f11862
            r0.start()
        L_0x007d:
            return
        L_0x007e:
            android.app.Activity r0 = r6.f11828     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r1 = "android.permission.WAKE_LOCK"
            boolean r0 = com.purplebrain.adbuddiz.sdk.i.a.h.m15332(r0, r1)     // Catch:{ Exception -> 0x00a4 }
            if (r0 == 0) goto L_0x0027
            android.app.Activity r0 = r6.f11828     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r1 = "wifi"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x00a4 }
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0     // Catch:{ Exception -> 0x00a4 }
            r1 = 1
            java.lang.String r2 = "mediaPlayerLock"
            android.net.wifi.WifiManager$WifiLock r0 = r0.createWifiLock(r1, r2)     // Catch:{ Exception -> 0x00a4 }
            r6.f11866 = r0     // Catch:{ Exception -> 0x00a4 }
            android.net.wifi.WifiManager$WifiLock r0 = r6.f11866     // Catch:{ Exception -> 0x00a4 }
            r0.acquire()     // Catch:{ Exception -> 0x00a4 }
            goto L_0x0027
        L_0x00a4:
            r0 = move-exception
            goto L_0x0027
        L_0x00a6:
            boolean r1 = r0.m15383()
            if (r1 == 0) goto L_0x006f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Impression will be send on event '"
            r1.<init>(r2)
            java.lang.String r2 = r0.f12219
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "'."
            r1.append(r2)
            java.lang.String r1 = "skipoffset"
            java.lang.String r2 = r0.f12219
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006f
            com.purplebrain.adbuddiz.sdk.i.c.e r1 = r0.f12217
            if (r1 == 0) goto L_0x00d6
            com.purplebrain.adbuddiz.sdk.i.c.e r1 = r0.f12217
            com.purplebrain.adbuddiz.sdk.i.c.e$a r0 = r0.f12209
            r1.f12227 = r0
            goto L_0x006f
        L_0x00d6:
            r0.m15382()
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.a.f.m14987(boolean):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public abstract Uri m14988();

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract com.purplebrain.adbuddiz.sdk.f.a.a.c m14989();

    /* renamed from: 龘  reason: contains not printable characters */
    public final View m14990(Activity activity, Bundle bundle, com.purplebrain.adbuddiz.sdk.f.a.a aVar, String str) {
        View r1 = super.m14927(activity, bundle, aVar, str);
        a aVar2 = this.f11825;
        this.f11851 = new RelativeLayout(this.f11828);
        ImageButton r2 = m14975();
        if (r2 != null) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(10);
            layoutParams.addRule(11);
            this.f11851.addView(r2, layoutParams);
        }
        aVar2.addView(this.f11851, 1);
        if (bundle == null) {
            boolean z = false;
            if (!aVar.f11995) {
                z = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138().f12067;
            }
            m14996(z, true);
        }
        return r1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14991(MediaPlayer mediaPlayer) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14992(MediaPlayer mediaPlayer, int i, int i2) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14993(Bundle bundle) {
        bundle.putBoolean("complete", this.f11855);
        bundle.putBoolean("muted", this.f11859);
        if (this.f11850 != null) {
            if (!this.f11855 || this.f11850.getDuration() == -1) {
                this.f11853 = Integer.valueOf(this.f11850.getCurrentPosition());
            } else {
                this.f11853 = Integer.valueOf(this.f11850.getDuration());
            }
            bundle.putInt("pos", this.f11853.intValue());
        }
        if (this.f11853 != null && this.f11853.intValue() > this.f11854) {
            this.f11854 = this.f11853.intValue();
        }
        bundle.putInt("max", this.f11854);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14994(View view) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14995(boolean z) {
        this.f11818 = false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14996(boolean z, boolean z2) {
        if (this.f11859 != z || z2) {
            Drawable drawable = this.f11864;
            float f = 1.0f;
            if (z) {
                drawable = this.f11863;
                f = 0.0f;
            }
            this.f11859 = z;
            if (this.f11865 != null) {
                this.f11865.setImageDrawable(drawable);
            }
            if (this.f11850 != null) {
                this.f11850.setVolume(f, f);
            }
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public final void m14997() {
        if (this.f11850 != null) {
            m14976();
            m14987(true);
        }
    }
}
