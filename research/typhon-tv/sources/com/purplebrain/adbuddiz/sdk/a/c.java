package com.purplebrain.adbuddiz.sdk.a;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.purplebrain.adbuddiz.sdk.b.h;
import com.purplebrain.adbuddiz.sdk.d.a;
import com.purplebrain.adbuddiz.sdk.f.e;
import com.purplebrain.adbuddiz.sdk.i.a.k;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;

public abstract class c {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected String f11817;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected boolean f11818;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected View f11819;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected C0027c f11820 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected b f11821 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected a f11822 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    protected Long f11823 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    protected RelativeLayout f11824;

    /* renamed from: ٴ  reason: contains not printable characters */
    protected com.purplebrain.adbuddiz.sdk.d.a f11825;

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected d f11826 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    protected com.purplebrain.adbuddiz.sdk.f.a.a f11827;

    /* renamed from: 麤  reason: contains not printable characters */
    protected Activity f11828;

    /* renamed from: ﹶ  reason: contains not printable characters */
    protected Long f11829 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    protected Long f11830 = null;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m14938();

        /* renamed from: 龘  reason: contains not printable characters */
        void m14939(com.purplebrain.adbuddiz.sdk.f.a.a aVar);
    }

    public interface b {
        /* renamed from: 龘  reason: contains not printable characters */
        void m14940(Throwable th);
    }

    /* renamed from: com.purplebrain.adbuddiz.sdk.a.c$c  reason: collision with other inner class name */
    public interface C0027c {
        /* renamed from: 龘  reason: contains not printable characters */
        void m14941(com.purplebrain.adbuddiz.sdk.f.a.a aVar);
    }

    public interface d {
        /* renamed from: 龘  reason: contains not printable characters */
        void m14942(com.purplebrain.adbuddiz.sdk.f.a.a aVar, String str, Boolean bool);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m14910(boolean z) {
        if (this.f11819 == null || this.f11819.getParent() == null || !this.f11818 || !z) {
            m14928();
            m14919();
            return;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(350);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            public final void onAnimationEnd(Animation animation) {
                try {
                    c.this.f11819 = null;
                    c.this.m14928();
                    c.this.m14919();
                } catch (Throwable th) {
                    l.m15430("ABAdDisplayer.hideAd.onAnimationEnd", c.this.f11827, th);
                    o.m15453("ABAdDisplayer#AnimationListener.onAnimationEnd() Exception : ", th);
                }
            }

            public final void onAnimationRepeat(Animation animation) {
            }

            public final void onAnimationStart(Animation animation) {
            }
        });
        this.f11819.startAnimation(alphaAnimation);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final void m14911() {
        this.f11829 = Long.valueOf(System.currentTimeMillis());
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m14912() {
        m14910(true);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public final boolean m14913() {
        if (this.f11825 == null || !this.f11825.f11926) {
            return false;
        }
        m14915();
        m14910(true);
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public final e m14914() {
        return this.f11827.m15157() ? this.f11827.f11998 : com.purplebrain.adbuddiz.sdk.i.a.d.m15318();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˉ  reason: contains not printable characters */
    public void m14915() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m14916() {
        if (this.f11830 == null) {
            return true;
        }
        if (this.f11830.longValue() > System.currentTimeMillis()) {
            return false;
        }
        return System.currentTimeMillis() - this.f11830.longValue() > 1000;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m14917() {
        this.f11830 = Long.valueOf(System.currentTimeMillis());
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m14918() {
        this.f11822.m14939(this.f11827);
        if (com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138().f12047) {
            m14910(false);
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m14919() {
        this.f11828.finish();
        this.f11828.overridePendingTransition(0, 0);
    }

    /* renamed from: י  reason: contains not printable characters */
    public void m14920() {
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public void m14921() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public com.purplebrain.adbuddiz.sdk.d.a m14922() {
        com.purplebrain.adbuddiz.sdk.d.a aVar = new com.purplebrain.adbuddiz.sdk.d.a(this.f11828);
        try {
            aVar.f11927 = m14926(com.purplebrain.adbuddiz.sdk.i.e.m15404(this.f11828));
            aVar.f11924 = m14926(com.purplebrain.adbuddiz.sdk.i.e.m15405());
        } catch (h e) {
        }
        aVar.m15032(true);
        aVar.f11899 = new a.C0028a() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final void m14937() {
                try {
                    if (c.this.f11823 == null) {
                        c.this.f11823 = Long.valueOf(System.currentTimeMillis());
                        c.this.m14915();
                        c.this.m14912();
                    }
                } catch (Throwable th) {
                    o.m15453("ABAdDisplayer#OnCloseButtonClickListener.onCloseButtonClicked()", th);
                }
            }
        };
        com.purplebrain.adbuddiz.sdk.f.a.a aVar2 = this.f11827;
        Rect r1 = com.purplebrain.adbuddiz.sdk.i.a.a.m15312(aVar.getContext(), aVar2.m15156().a(com.purplebrain.adbuddiz.sdk.i.a.d.m15318()));
        aVar.setPadding(r1.left, r1.top, r1.right, r1.bottom);
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract View m14923();

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public ViewGroup.LayoutParams m14924(View view) {
        return view.getLayoutParams();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m14925(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public final Drawable m14926(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        float r1 = com.purplebrain.adbuddiz.sdk.i.a.l.m15341(this.f11828) / ((float) (bitmap.getDensity() / 160));
        BitmapDrawable bitmapDrawable = new BitmapDrawable(this.f11828.getResources(), bitmap);
        bitmapDrawable.setBounds(0, 0, (int) (((float) bitmap.getHeight()) * r1), (int) (r1 * ((float) bitmap.getWidth())));
        return bitmapDrawable;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public View m14927(Activity activity, Bundle bundle, com.purplebrain.adbuddiz.sdk.f.a.a aVar, String str) {
        Button button;
        this.f11828 = activity;
        this.f11827 = aVar;
        this.f11817 = str;
        if (bundle != null) {
            m14925(bundle);
        }
        com.purplebrain.adbuddiz.sdk.d.b bVar = new com.purplebrain.adbuddiz.sdk.d.b(this.f11828);
        bVar.f11938.setAlpha(com.purplebrain.adbuddiz.sdk.i.a.h.m15329(this.f11828));
        bVar.f11932 = this.f11827.m15156().g;
        bVar.setClickable(true);
        this.f11825 = m14922();
        View r0 = m14923();
        ViewGroup.LayoutParams r2 = m14924(r0);
        this.f11824 = new RelativeLayout(this.f11828);
        this.f11824.setBackgroundColor(-16777216);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12);
        this.f11824.setLayoutParams(layoutParams);
        ProgressBar progressBar = new ProgressBar(this.f11828, (AttributeSet) null, 16842872);
        progressBar.setIndeterminate(true);
        int r4 = k.m15340((Context) this.f11828, 12);
        progressBar.setPadding(r4, r4, r4, r4);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(13);
        progressBar.setLayoutParams(layoutParams2);
        this.f11824.addView(progressBar);
        if (this.f11830 == null) {
            this.f11824.setVisibility(4);
        }
        RelativeLayout relativeLayout = this.f11824;
        this.f11825.addView(r0, r2);
        this.f11825.addView(relativeLayout);
        bVar.addView(this.f11825, new ViewGroup.LayoutParams(-1, -1));
        Bitmap r02 = com.purplebrain.adbuddiz.sdk.i.d.m15402(this.f11828);
        if (r02 == null) {
            button = null;
        } else {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(r02);
            button = new Button(this.f11828);
            button.setBackgroundDrawable(bitmapDrawable);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams3.addRule(12);
            layoutParams3.addRule(11);
            layoutParams3.width = k.m15340((Context) this.f11828, 70);
            layoutParams3.height = k.m15340((Context) this.f11828, 10);
            layoutParams3.bottomMargin = k.m15340((Context) this.f11828, 5);
            layoutParams3.rightMargin = k.m15340((Context) this.f11828, 5);
            button.setLayoutParams(layoutParams3);
            button.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    try {
                        c.this.f11822.m14938();
                    } catch (Throwable th) {
                        l.m15430("ABAdDisplayer.createByAdBuddizLayout.onClick", c.this.f11827, th);
                        o.m15453("ABAdDisplayer#OnClickListener.onClick()", th);
                    }
                }
            });
        }
        if (button != null) {
            bVar.addView(button);
        }
        this.f11819 = bVar;
        activity.setContentView(this.f11819, new ViewGroup.LayoutParams(-1, -1));
        if (this.f11818) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(400);
            this.f11819.startAnimation(alphaAnimation);
        }
        return this.f11819;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14928() {
        this.f11820.m14941(this.f11827);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14929(Bundle bundle) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14930(a aVar) {
        this.f11822 = aVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14931(b bVar) {
        this.f11821 = bVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14932(C0027c cVar) {
        this.f11820 = cVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14933(d dVar) {
        this.f11826 = dVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14934(boolean z) {
        this.f11818 = z;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public void m14935() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public final boolean m14936() {
        if (this.f11829 == null) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.f11829.longValue() > com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138().f12046) {
            return true;
        }
        new Object[1][0] = Long.valueOf(com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138().f12046 - (currentTimeMillis - this.f11829.longValue()));
        return false;
    }
}
