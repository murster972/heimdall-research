package com.purplebrain.adbuddiz.sdk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.a.c;
import com.purplebrain.adbuddiz.sdk.e.b;
import com.purplebrain.adbuddiz.sdk.f.a.a;
import com.purplebrain.adbuddiz.sdk.f.d;
import com.purplebrain.adbuddiz.sdk.f.e;
import com.purplebrain.adbuddiz.sdk.h.d;
import com.purplebrain.adbuddiz.sdk.h.g;
import com.purplebrain.adbuddiz.sdk.i.i;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.p;
import com.purplebrain.adbuddiz.sdk.i.r;
import org.json.JSONObject;

public class AdBuddizActivity extends Activity {

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean f11783 = false;

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f11784 = false;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean f11785 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private c.d f11786 = new c.d() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m14877(a aVar, String str, Boolean bool) {
            if (!AdBuddizActivity.this.f11785) {
                boolean unused = AdBuddizActivity.this.f11785 = true;
                d dVar = new d();
                dVar.f12164 = aVar;
                dVar.f12161 = str;
                dVar.f12162 = bool;
                dVar.m15266();
            }
        }
    };

    /* renamed from: ˑ  reason: contains not printable characters */
    private c.a f11787 = new c.a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m14878() {
            AdBuddizActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(b.m15135().m15138().f12051)));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m14879(a aVar) {
            com.purplebrain.adbuddiz.sdk.h.c cVar = new com.purplebrain.adbuddiz.sdk.h.c();
            cVar.f12160 = aVar;
            cVar.f12159 = AdBuddizActivity.this.f11793;
            cVar.m15266();
            if (!aVar.f11995) {
                i.m15418(new Runnable() {
                    public final void run() {
                        AdBuddiz.m14859().m14884();
                    }
                });
            }
        }
    };

    /* renamed from: ٴ  reason: contains not printable characters */
    private c.C0027c f11788 = new c.C0027c() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m14880(a aVar) {
            if (!aVar.f11995) {
                i.m15417();
            }
            if (aVar.f11995) {
                if (AdBuddizActivity.this.f11785) {
                    r.m15469();
                } else {
                    r.m15468();
                }
            }
            p.m15457(aVar.f11995).f12243 = Long.valueOf(System.currentTimeMillis());
        }
    };

    /* renamed from: ᐧ  reason: contains not printable characters */
    private c.b f11789 = new c.b() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m14881(Throwable th) {
            if (b.m15135().m15138().f12059.contains(AdBuddizError.INVALID_AD)) {
                com.purplebrain.adbuddiz.sdk.f.d dVar = new com.purplebrain.adbuddiz.sdk.f.d(d.a.SHOW_AD, AdBuddizActivity.this.f11793);
                g gVar = new g();
                gVar.f12170 = AdBuddizActivity.this.f11791;
                gVar.m15296(AdBuddizError.INVALID_AD);
                gVar.m15297(th);
                gVar.f12172 = dVar;
                gVar.m15266();
            }
            if (AdBuddizActivity.this.f11791.f11995) {
                if (AdBuddizActivity.this.f11785) {
                    r.m15469();
                } else {
                    r.m15468();
                }
            }
            AdBuddizActivity.this.finish();
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private View f11790;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public a f11791;

    /* renamed from: 麤  reason: contains not printable characters */
    private c f11792;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public String f11793;

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m14870() {
        return f11783;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14874(View view) {
        view.setBackgroundDrawable((Drawable) null);
        if (view.getBackground() != null) {
            view.getBackground().setCallback((Drawable.Callback) null);
        }
        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            if (imageView.getDrawable() != null) {
                imageView.getDrawable().setCallback((Drawable.Callback) null);
                imageView.setImageDrawable((Drawable) null);
            }
        }
        if (view.getResources() != null) {
            view.getResources().flushLayoutCache();
        }
        view.destroyDrawingCache();
        if (view instanceof ViewGroup) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < ((ViewGroup) view).getChildCount()) {
                    m14874(((ViewGroup) view).getChildAt(i2));
                    i = i2 + 1;
                } else {
                    ((ViewGroup) view).removeAllViews();
                    return;
                }
            }
        }
    }

    public void onBackPressed() {
        try {
            if (!this.f11784) {
                this.f11784 = this.f11792.m14913();
            }
        } catch (Throwable th) {
            l.m15430("AdBuddizActivity.onBackPressed", this.f11791, th);
            o.m15453("AdBuddizActivity.onBackPressed() Exception : ", th);
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onCreate(Bundle bundle) {
        int i;
        boolean z = false;
        try {
            f11783 = true;
            super.onCreate(bundle);
            if (AdBuddiz.m14865() == null) {
                AdBuddiz.m14862(this);
            }
            Bundle extras = getIntent().getExtras();
            if (extras == null || !extras.containsKey(TtmlNode.TAG_P) || !extras.containsKey("a")) {
                o.m15451("invalid AdBuddizActivity intent.");
                finish();
                return;
            }
            this.f11793 = extras.getString(TtmlNode.TAG_P);
            this.f11791 = a.m15155(new JSONObject(extras.getString("a")));
            Bundle extras2 = getIntent().getExtras();
            getWindow().addFlags(128);
            if (extras2.containsKey("wt") && extras2.getBoolean("wt")) {
                requestWindowFeature(1);
            }
            if (extras2.containsKey("fs") && extras2.getBoolean("fs")) {
                getWindow().addFlags(1024);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (extras2.containsKey("sbc") || extras2.containsKey("snc")) {
                    getWindow().addFlags(Integer.MIN_VALUE);
                }
                if (extras2.containsKey("sbc")) {
                    getWindow().setStatusBarColor(extras2.getInt("sbc"));
                }
                if (extras2.containsKey("snc")) {
                    getWindow().setNavigationBarColor(extras2.getInt("snc"));
                }
            }
            a aVar = this.f11791;
            if (!(aVar.f11998 == null || aVar.f11998 == e.BOTH)) {
                switch (aVar.f11998) {
                    case LAND:
                        i = 0;
                        break;
                    case PORT:
                        i = 1;
                        break;
                    default:
                        i = getRequestedOrientation();
                        break;
                }
                setRequestedOrientation(i);
            }
            if (bundle != null) {
                this.f11785 = bundle.getBoolean("hasImpressionBeenSent");
            }
            a aVar2 = this.f11791;
            boolean z2 = b.m15135().m15138() == null;
            com.purplebrain.adbuddiz.sdk.e.a.m15046();
            boolean r3 = com.purplebrain.adbuddiz.sdk.e.a.m15050(aVar2);
            if (z2 || r3) {
                z = true;
            }
            if (z) {
                if (!this.f11791.f11995) {
                    i.m15417();
                } else {
                    r.m15468();
                }
                finish();
                return;
            }
            this.f11792 = this.f11791.m15156().a();
            this.f11792.m14934(m14876());
            this.f11792.m14933(this.f11786);
            this.f11792.m14930(this.f11787);
            this.f11792.m14932(this.f11788);
            this.f11792.m14931(this.f11789);
            this.f11790 = this.f11792.m14927(this, bundle, this.f11791, this.f11793);
        } catch (Throwable th) {
            l.m15430("AdBuddizActivity.onCreate", this.f11791, th);
            o.m15453("AdBuddizActivity.onCreate() Exception : ", th);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.f11792 != null) {
                this.f11792.m14921();
            }
            if (isFinishing()) {
                f11783 = false;
            }
            if (this.f11790 != null) {
                m14874(this.f11790);
                this.f11790 = null;
            }
        } catch (Throwable th) {
            l.m15430("AdBuddizActivity.onDestroy", this.f11791, th);
            o.m15453("AdBuddizActivity.onDestroy() Exception : ", th);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        try {
            if (this.f11792 != null) {
                this.f11792.m14935();
            }
        } catch (Throwable th) {
            l.m15430("AdBuddizActivity.onRestart", this.f11791, th);
            o.m15453("AdBuddizActivity.onRestart() Exception : ", th);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        try {
            bundle.putBoolean("hasImpressionBeenSent", this.f11785);
            if (this.f11792 != null) {
                this.f11792.m14929(bundle);
            }
        } catch (Throwable th) {
            l.m15430("AdBuddizActivity.onSaveInstanceState", this.f11791, th);
            o.m15453("AdBuddizActivity.onSaveInstanceState() Exception : ", th);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        try {
            super.onStart();
            Bundle extras = getIntent().getExtras();
            if (Build.VERSION.SDK_INT >= 19 && extras.containsKey("suf")) {
                int i = extras.getInt("suf");
                if ((i & 2048) != 0) {
                    i = (i & -2049) | 4096;
                }
                getWindow().getDecorView().setSystemUiVisibility(i);
            }
            this.f11792.m14911();
        } catch (Throwable th) {
            l.m15430("AdBuddizActivity.onStart", this.f11791, th);
            o.m15453("AdBuddizActivity.onStart() Exception : ", th);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        try {
            if (this.f11792 != null) {
                this.f11792.m14920();
            }
        } catch (Throwable th) {
            l.m15430("AdBuddizActivity.onStop", this.f11791, th);
            o.m15453("AdBuddizActivity.onStop() Exception : ", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m14876() {
        try {
            if (!((getWindow() == null || (getWindow().getAttributes().flags & 16777216) == 0) && (getPackageManager().getActivityInfo(getComponentName(), 0).flags & 512) == 0)) {
                return true;
            }
        } catch (Throwable th) {
        }
        return false;
    }
}
