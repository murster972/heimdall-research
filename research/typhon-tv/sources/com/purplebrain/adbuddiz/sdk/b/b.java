package com.purplebrain.adbuddiz.sdk.b;

import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import com.purplebrain.adbuddiz.sdk.AdBuddizRewardedVideoError;

public abstract class b extends Exception {
    public AdBuddizError a;
    public AdBuddizRewardedVideoError b;
    public String c;

    protected b(AdBuddizError adBuddizError) {
        this(adBuddizError, (AdBuddizRewardedVideoError) null);
    }

    protected b(AdBuddizError adBuddizError, AdBuddizRewardedVideoError adBuddizRewardedVideoError) {
        this(adBuddizError, adBuddizRewardedVideoError, (String) null);
    }

    protected b(AdBuddizError adBuddizError, AdBuddizRewardedVideoError adBuddizRewardedVideoError, String str) {
        this.a = null;
        this.b = null;
        this.c = null;
        this.a = adBuddizError;
        this.b = adBuddizRewardedVideoError;
        this.c = str;
    }
}
