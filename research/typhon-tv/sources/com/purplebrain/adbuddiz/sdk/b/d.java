package com.purplebrain.adbuddiz.sdk.b;

import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import com.purplebrain.adbuddiz.sdk.AdBuddizRewardedVideoError;

public final class d extends b {
    public d() {
        super(AdBuddizError.AD_IS_ALREADY_ON_SCREEN, AdBuddizRewardedVideoError.AD_IS_ALREADY_ON_SCREEN);
    }
}
