package com.purplebrain.adbuddiz.sdk.h;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.i.a.i;
import com.purplebrain.adbuddiz.sdk.i.b.d;
import com.purplebrain.adbuddiz.sdk.i.b.e;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.s;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.net.ssl.SSLHandshakeException;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.oltu.oauth2.common.OAuth;
import org.json.JSONArray;
import org.json.JSONException;

public abstract class b implements Runnable {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static ExecutorService f12151;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static ExecutorService f12152;

    /* renamed from: ʻ  reason: contains not printable characters */
    private h f12153 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f12154 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    int f12155 = 10;

    /* renamed from: 麤  reason: contains not printable characters */
    protected a f12156;

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f12157 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    protected int f12158 = 0;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15279(b bVar);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static ExecutorService m15263() {
        if (f12151 == null || f12151.isShutdown()) {
            synchronized (b.class) {
                if (f12151 == null || f12151.isShutdown()) {
                    f12151 = Executors.newFixedThreadPool(3);
                }
            }
        }
        return f12151;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private static ExecutorService m15264() {
        if (f12152 == null || f12152.isShutdown()) {
            synchronized (b.class) {
                if (f12152 == null || f12152.isShutdown()) {
                    f12152 = Executors.newSingleThreadExecutor();
                }
            }
        }
        return f12152;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static HttpURLConnection m15265(URL url, String str) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setConnectTimeout(15000);
        httpURLConnection.setReadTimeout(15000);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
        httpURLConnection.setDoInput(true);
        httpURLConnection.setRequestMethod(str);
        return httpURLConnection;
    }

    public void run() {
        boolean z;
        boolean z2;
        if (m15271()) {
            o.m15452("Request Canceled : " + m15268());
            return;
        }
        try {
            o.m15452("LaunchRequest : " + m15268());
            Context r0 = AdBuddiz.m14865();
            this.f12153 = new h();
            this.f12153.f12180 = m15268();
            this.f12153.f12177 = System.currentTimeMillis();
            if (r0 != null) {
                this.f12153.f12174 = i.m15333(r0);
                this.f12153.f12175 = i.m15335(r0);
            }
            m15273();
            this.f12153.f12179 = System.currentTimeMillis() - this.f12153.f12177;
            if (r0 != null && this.f12157) {
                h hVar = this.f12153;
                try {
                    JSONArray r4 = e.m15370(r0);
                    r4.put((Object) hVar.m15298());
                    d.m15369(r0, InternalZipTyphoonApp.READ_MODE, r4);
                } catch (JSONException e) {
                }
            }
            if (!m15271()) {
                o.m15452("Request Ok : " + m15268());
            } else {
                o.m15452("Request Canceled : " + m15268());
            }
            z = false;
            z2 = true;
        } catch (k e2) {
            o.m15451("AdBuddizServerException : " + getClass().getSimpleName() + " - " + e2.getMessage());
            z = true;
            z2 = false;
        } catch (UnknownHostException e3) {
            o.m15451("No network available (UnknownHostException) : " + m15268());
            z = false;
            z2 = false;
        } catch (ConnectException e4) {
            o.m15451("No network available (ConnectException) : " + m15268());
            z = false;
            z2 = false;
        } catch (SocketTimeoutException e5) {
            o.m15451("No network available (SocketTimeoutException) : " + m15268());
            z = false;
            z2 = false;
        } catch (SSLHandshakeException e6) {
            o.m15452("Error with HTTPS, fallback to HTTP: " + m15268());
            z = false;
            z2 = false;
        } catch (Exception e7) {
            o.m15451("Exception in Request : " + m15268() + " - " + e7.toString());
            z = false;
            z2 = false;
        } catch (Throwable th) {
            l.m15430("ABGenericRequest.run", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
            o.m15451("Throwable thrown in Request : " + m15268());
            z = false;
            z2 = false;
        }
        if (z2) {
            return;
        }
        if (m15271()) {
            o.m15452("Request canceled before #" + (this.f12158 + 1) + " try : " + getClass().getSimpleName());
        } else {
            m15278(z);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m15266() {
        this.f12158++;
        try {
            (m15272() ? m15264() : m15263()).execute(this);
        } catch (RejectedExecutionException e) {
            ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
            newSingleThreadExecutor.execute(this);
            newSingleThreadExecutor.shutdown();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m15267() {
        synchronized (this) {
            if (!this.f12154) {
                this.f12154 = true;
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract String m15268();

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final String m15269(HttpURLConnection httpURLConnection) {
        return s.m15472(m15275(httpURLConnection));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final HttpURLConnection m15270(URL url, String str) {
        HttpURLConnection r0 = m15265(url, OAuth.HttpMethod.POST);
        m15277(r0, str);
        return r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m15271() {
        boolean z;
        synchronized (this) {
            z = this.f12154;
        }
        return z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract boolean m15272();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m15273();

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m15274() {
        return this.f12158;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final InputStream m15275(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("Content-Length");
        if (headerField != null) {
            this.f12153.f12176 = Long.parseLong(headerField);
        }
        InputStream errorStream = httpURLConnection.getResponseCode() >= 400 ? httpURLConnection.getErrorStream() : httpURLConnection.getInputStream();
        return "gzip".equalsIgnoreCase(httpURLConnection.getHeaderField("Content-Encoding")) ? new GZIPInputStream(errorStream) : errorStream;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15276(a aVar) {
        this.f12156 = aVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15277(HttpURLConnection httpURLConnection, String str) {
        this.f12153.f12178 = (long) str.getBytes().length;
        httpURLConnection.setRequestProperty(OAuth.HeaderType.CONTENT_TYPE, "application/json; charset=utf-8");
        httpURLConnection.setRequestProperty("Content-Encoding", "gzip");
        httpURLConnection.setDoOutput(true);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new GZIPOutputStream(httpURLConnection.getOutputStream()));
        bufferedOutputStream.write(str.getBytes("UTF-8"));
        bufferedOutputStream.flush();
        bufferedOutputStream.close();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m15278(boolean z) {
        int i = 30000;
        try {
            if (this.f12158 < this.f12155) {
                if (!z) {
                    switch (this.f12158) {
                        case 1:
                            i = 1000;
                            break;
                        case 2:
                            i = 3000;
                            break;
                        case 3:
                            i = 7000;
                            break;
                        case 4:
                            break;
                        case 5:
                            i = 60000;
                            break;
                        case 6:
                            i = 900000;
                            break;
                        default:
                            i = 3600000;
                            break;
                    }
                } else {
                    switch (this.f12158) {
                        case 1:
                            i = new Random().nextInt(10000) + 10000;
                            break;
                        case 2:
                            i = new Random().nextInt(30000) + 30000;
                            break;
                        case 3:
                            i = new Random().nextInt(60000) + 60000;
                            break;
                        case 4:
                            i = new Random().nextInt(900000) + 900000;
                            break;
                        default:
                            i = new Random().nextInt(3600000) + 3600000;
                            break;
                    }
                }
                o.m15452("Request sleeps " + i + "ms before #" + (this.f12158 + 1) + " try : " + getClass().getSimpleName());
                Thread.sleep((long) i);
                m15266();
            } else if (this.f12156 != null) {
                this.f12156.m15279(this);
            }
        } catch (InterruptedException e) {
        }
    }
}
