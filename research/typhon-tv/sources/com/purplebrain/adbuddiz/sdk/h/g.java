package com.purplebrain.adbuddiz.sdk.h;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import com.purplebrain.adbuddiz.sdk.AdBuddizRewardedVideoError;
import com.purplebrain.adbuddiz.sdk.f.a.a;
import com.purplebrain.adbuddiz.sdk.f.d;
import com.purplebrain.adbuddiz.sdk.h.a.a;
import com.purplebrain.adbuddiz.sdk.h.a.b;
import com.purplebrain.adbuddiz.sdk.h.a.d;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.w;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.json.JSONObject;

public final class g extends a {

    /* renamed from: ʻ  reason: contains not printable characters */
    public AdBuddizRewardedVideoError f12169;

    /* renamed from: ʼ  reason: contains not printable characters */
    public a f12170;

    /* renamed from: ʽ  reason: contains not printable characters */
    public String f12171;

    /* renamed from: ˑ  reason: contains not printable characters */
    public d f12172;

    /* renamed from: 连任  reason: contains not printable characters */
    public AdBuddizError f12173;

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final String m15293() {
        return "EventReport";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m15294() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m15295() {
        HttpURLConnection httpURLConnection = null;
        try {
            URL r0 = w.m15482();
            Context r2 = AdBuddiz.m14865();
            JSONObject jSONObject = new JSONObject();
            new a.C0031a();
            jSONObject.put("a", (Object) a.C0031a.m15258(r2).m15257());
            new b.a();
            jSONObject.put("d", (Object) b.a.m15260(r2).m15259());
            new d.a();
            jSONObject.put("s", (Object) d.a.m15262(r2).m15261());
            if (this.f12173 != null) {
                jSONObject.put("e", (Object) this.f12173.getName());
            }
            if (this.f12169 != null) {
                jSONObject.put("e", (Object) this.f12169.getName());
            }
            if (this.f12170 != null) {
                jSONObject.put("i", (Object) this.f12170.m15162());
                jSONObject.put("in", this.f12170.f11995);
            }
            if (!(this.f12171 == null || this.f12171.length() == 0)) {
                jSONObject.put(InternalZipTyphoonApp.READ_MODE, (Object) this.f12171);
            }
            jSONObject.put("erc", (Object) this.f12172.f12071);
            jSONObject.put(TtmlNode.TAG_P, (Object) this.f12172.f12073);
            jSONObject.put("or", (Object) com.purplebrain.adbuddiz.sdk.i.a.d.m15318().d);
            jSONObject.put("t", System.currentTimeMillis());
            httpURLConnection = m15270(r0, jSONObject.toString());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 500) {
                throw new k("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15296(AdBuddizError adBuddizError) {
        this.f12173 = adBuddizError;
        this.f12169 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15297(Throwable th) {
        if (th == null) {
            this.f12171 = null;
            return;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            th.printStackTrace(new PrintStream(byteArrayOutputStream));
            this.f12171 = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
        } catch (Throwable th2) {
            o.m15453("ABRequestEventReport.setReason()", th2);
        }
    }
}
