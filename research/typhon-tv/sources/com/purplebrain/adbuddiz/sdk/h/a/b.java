package com.purplebrain.adbuddiz.sdk.h.a;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.purplebrain.adbuddiz.sdk.i.a.e;
import com.purplebrain.adbuddiz.sdk.i.a.i;
import com.purplebrain.adbuddiz.sdk.i.a.l;
import com.purplebrain.adbuddiz.sdk.i.g;
import com.purplebrain.adbuddiz.sdk.i.n;
import java.util.Locale;
import net.pubnative.library.request.PubnativeRequest;
import org.json.JSONObject;

public final class b {

    /* renamed from: ʻ  reason: contains not printable characters */
    String f12121;

    /* renamed from: ʼ  reason: contains not printable characters */
    String f12122;

    /* renamed from: ʽ  reason: contains not printable characters */
    int f12123;

    /* renamed from: ʾ  reason: contains not printable characters */
    float f12124;

    /* renamed from: ʿ  reason: contains not printable characters */
    Integer f12125;

    /* renamed from: ˆ  reason: contains not printable characters */
    String f12126;

    /* renamed from: ˈ  reason: contains not printable characters */
    int f12127;

    /* renamed from: ˉ  reason: contains not printable characters */
    String f12128;

    /* renamed from: ˊ  reason: contains not printable characters */
    String f12129;

    /* renamed from: ˋ  reason: contains not printable characters */
    String f12130;

    /* renamed from: ˎ  reason: contains not printable characters */
    String f12131;

    /* renamed from: ˏ  reason: contains not printable characters */
    String f12132;

    /* renamed from: ˑ  reason: contains not printable characters */
    String f12133;

    /* renamed from: י  reason: contains not printable characters */
    String f12134;

    /* renamed from: ٴ  reason: contains not printable characters */
    int f12135;

    /* renamed from: ᐧ  reason: contains not printable characters */
    int f12136;

    /* renamed from: 连任  reason: contains not printable characters */
    String f12137;

    /* renamed from: 靐  reason: contains not printable characters */
    String f12138;

    /* renamed from: 麤  reason: contains not printable characters */
    String f12139;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean f12140;

    /* renamed from: 龘  reason: contains not printable characters */
    String f12141;

    /* renamed from: ﹶ  reason: contains not printable characters */
    Integer f12142;

    /* renamed from: ﾞ  reason: contains not printable characters */
    String f12143;

    public static class a {
        /* renamed from: 龘  reason: contains not printable characters */
        public static b m15260(Context context) {
            String str = null;
            b bVar = new b();
            String r0 = e.m15324(context);
            if (r0 != null) {
                bVar.f12141 = g.m15412(r0);
            }
            String r02 = com.purplebrain.adbuddiz.sdk.i.b.b.m15350(context);
            if (r02 != null) {
                bVar.f12138 = g.m15412(r02);
            }
            bVar.f12140 = context.getSharedPreferences("ABZ-prefs", 0).getBoolean(PubnativeRequest.Parameters.LAT, false);
            bVar.f12123 = com.purplebrain.adbuddiz.sdk.i.a.b.m15313();
            bVar.f12133 = Build.VERSION.RELEASE;
            if (e.m15322(context) != null) {
                bVar.f12139 = g.m15412(e.m15322(context));
                String r03 = e.m15320(context);
                bVar.f12137 = g.m15412(r03 == null ? null : n.m15449("SHA-1", r03));
            }
            if (e.m15321(context) != null) {
                bVar.f12121 = g.m15412(e.m15321(context));
                String r04 = e.m15323(context);
                if (r04 != null) {
                    str = n.m15449("SHA-1", r04);
                }
                bVar.f12122 = g.m15412(str);
            }
            bVar.f12135 = l.m15342(context).widthPixels;
            bVar.f12136 = l.m15342(context).heightPixels;
            DisplayMetrics r05 = l.m15342(context);
            bVar.f12127 = (int) Math.max(r05.xdpi, r05.ydpi);
            bVar.f12124 = l.m15342(context).density;
            bVar.f12125 = i.m15333(context);
            bVar.f12142 = i.m15335(context);
            bVar.f12143 = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
            bVar.f12129 = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            String simCountryIso = telephonyManager.getSimCountryIso();
            String networkCountryIso = ((simCountryIso == null || simCountryIso.length() == 0) && telephonyManager.getPhoneType() != 2) ? telephonyManager.getNetworkCountryIso() : simCountryIso;
            if (networkCountryIso == null || networkCountryIso.length() == 0) {
                networkCountryIso = context.getResources().getConfiguration().locale.getCountry();
            }
            if (networkCountryIso != null) {
                networkCountryIso = networkCountryIso.toUpperCase(Locale.US);
            }
            if (networkCountryIso != null && networkCountryIso.length() > 2) {
                networkCountryIso = networkCountryIso.substring(0, 2);
            }
            bVar.f12130 = networkCountryIso;
            String language = context.getResources().getConfiguration().locale.getLanguage();
            if (language == null) {
                language = Locale.getDefault().getLanguage();
            }
            bVar.f12131 = language;
            bVar.f12126 = Build.MANUFACTURER;
            bVar.f12128 = Build.MODEL;
            bVar.f12132 = Build.PRODUCT;
            bVar.f12134 = Build.CPU_ABI;
            return bVar;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m15259() {
        JSONObject jSONObject = new JSONObject();
        if (this.f12141 != null) {
            jSONObject.put("a", (Object) this.f12141);
        }
        jSONObject.put("ai", (Object) this.f12138);
        jSONObject.put(PubnativeRequest.Parameters.LAT, this.f12140);
        jSONObject.put("s", this.f12123);
        jSONObject.put("v", (Object) this.f12133);
        if (this.f12139 != null) {
            jSONObject.put("im", (Object) this.f12139);
            jSONObject.put("is", (Object) this.f12137);
        }
        if (this.f12121 != null) {
            jSONObject.put("mm", (Object) this.f12121);
            jSONObject.put("ms", (Object) this.f12122);
        }
        jSONObject.put("sw", this.f12135);
        jSONObject.put("sh", this.f12136);
        jSONObject.put("sd", this.f12127);
        jSONObject.put("sdf", (double) this.f12124);
        jSONObject.put("nt", (Object) this.f12125);
        jSONObject.put("nst", (Object) this.f12142);
        jSONObject.put("no", (Object) this.f12143);
        jSONObject.put("non", (Object) this.f12129);
        jSONObject.put("c", (Object) this.f12130);
        jSONObject.put("l", (Object) this.f12131);
        jSONObject.put("ma", (Object) this.f12126);
        jSONObject.put("mo", (Object) this.f12128);
        jSONObject.put(TtmlNode.TAG_P, (Object) this.f12132);
        jSONObject.put("cp", (Object) this.f12134);
        return jSONObject;
    }
}
