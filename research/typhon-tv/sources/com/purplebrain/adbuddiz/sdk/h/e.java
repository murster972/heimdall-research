package com.purplebrain.adbuddiz.sdk.h;

import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.f.a.a.c;
import com.purplebrain.adbuddiz.sdk.i.m;
import java.net.HttpURLConnection;
import org.apache.oltu.oauth2.common.OAuth;

public final class e extends b {

    /* renamed from: ʻ  reason: contains not printable characters */
    public a f12165 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public c f12166 = null;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15289(c cVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final String m15286() {
        return "DownloadAdResource";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m15287() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m15288() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = b.m15265(this.f12166.m15177(), OAuth.HttpMethod.GET);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                m.m15446(AdBuddiz.m14865(), m15275(httpURLConnection), this.f12166);
                this.f12165.m15289(this.f12166);
            } else if (responseCode >= 500) {
                throw new k("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }
}
