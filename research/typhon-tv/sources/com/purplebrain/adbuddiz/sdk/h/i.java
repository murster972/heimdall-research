package com.purplebrain.adbuddiz.sdk.h;

import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import com.purplebrain.adbuddiz.sdk.AdBuddizRewardedVideoError;
import com.purplebrain.adbuddiz.sdk.i.p;

public final class i extends a {

    /* renamed from: ʻ  reason: contains not printable characters */
    public com.purplebrain.adbuddiz.sdk.f.a.a f12181 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public b f12182 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean f12183;

    public enum a {
        UNKNOWN("UNKNOWN", AdBuddizError.NO_MORE_AVAILABLE_ADS, AdBuddizRewardedVideoError.NO_MORE_AVAILABLE_ADS),
        NETWORK_TOO_SLOW("NETWORK_TOO_SLOW", AdBuddizError.NO_MORE_AVAILABLE_ADS, AdBuddizRewardedVideoError.NETWORK_TOO_SLOW),
        UNSUPPORTED_DEVICE("UNSUPPORTED_DEVICE", AdBuddizError.NO_MORE_AVAILABLE_ADS, AdBuddizRewardedVideoError.UNSUPPORTED_DEVICE),
        UNSUPPORTED_OS_VERSION("UNSUPPORTED_OS_VERSION", AdBuddizError.NO_MORE_AVAILABLE_ADS, AdBuddizRewardedVideoError.UNSUPPORTED_OS_VERSION);
        
        public AdBuddizError e;
        public AdBuddizRewardedVideoError f;
        private String g;

        private a(String str, AdBuddizError adBuddizError, AdBuddizRewardedVideoError adBuddizRewardedVideoError) {
            this.g = str;
            this.e = adBuddizError;
            this.f = adBuddizRewardedVideoError;
        }

        public static a a(String str) {
            for (a aVar : (a[]) f12184.clone()) {
                if (aVar.g.equals(str)) {
                    return aVar;
                }
            }
            return null;
        }
    }

    public interface b {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15303(i iVar);

        /* renamed from: 龘  reason: contains not printable characters */
        void m15304(i iVar, com.purplebrain.adbuddiz.sdk.f.a.a aVar);

        /* renamed from: 龘  reason: contains not printable characters */
        void m15305(i iVar, a aVar, String str);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m15299() {
        p r1 = p.m15457(this.f12183);
        boolean r0 = r1.m15458();
        if (!r0) {
            return r0;
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        return r1.m15458();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final String m15300() {
        return "NextAd";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m15301() {
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.lang.String} */
    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r1v1, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r1v2 */
    /* JADX WARNING: type inference failed for: r1v10 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00b2  */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m15302() {
        /*
            r10 = this;
            r1 = 0
            boolean r2 = r10.m15299()     // Catch:{ all -> 0x00af }
            java.net.URL r3 = com.purplebrain.adbuddiz.sdk.i.w.m15489()     // Catch:{ all -> 0x00af }
            android.content.Context r4 = com.purplebrain.adbuddiz.sdk.AdBuddiz.m14865()     // Catch:{ all -> 0x00af }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ all -> 0x00af }
            r5.<init>()     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "a"
            com.purplebrain.adbuddiz.sdk.h.a.a$a r6 = new com.purplebrain.adbuddiz.sdk.h.a.a$a     // Catch:{ all -> 0x00af }
            r6.<init>()     // Catch:{ all -> 0x00af }
            com.purplebrain.adbuddiz.sdk.h.a.a r6 = com.purplebrain.adbuddiz.sdk.h.a.a.C0031a.m15258(r4)     // Catch:{ all -> 0x00af }
            org.json.JSONObject r6 = r6.m15257()     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r6)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "d"
            com.purplebrain.adbuddiz.sdk.h.a.b$a r6 = new com.purplebrain.adbuddiz.sdk.h.a.b$a     // Catch:{ all -> 0x00af }
            r6.<init>()     // Catch:{ all -> 0x00af }
            com.purplebrain.adbuddiz.sdk.h.a.b r6 = com.purplebrain.adbuddiz.sdk.h.a.b.a.m15260(r4)     // Catch:{ all -> 0x00af }
            org.json.JSONObject r6 = r6.m15259()     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r6)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "s"
            com.purplebrain.adbuddiz.sdk.h.a.d$a r6 = new com.purplebrain.adbuddiz.sdk.h.a.d$a     // Catch:{ all -> 0x00af }
            r6.<init>()     // Catch:{ all -> 0x00af }
            com.purplebrain.adbuddiz.sdk.h.a.d r6 = com.purplebrain.adbuddiz.sdk.h.a.d.a.m15262(r4)     // Catch:{ all -> 0x00af }
            org.json.JSONObject r6 = r6.m15261()     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r6)     // Catch:{ all -> 0x00af }
            com.purplebrain.adbuddiz.sdk.h.a.c$a r0 = new com.purplebrain.adbuddiz.sdk.h.a.c$a     // Catch:{ all -> 0x00af }
            r0.<init>()     // Catch:{ all -> 0x00af }
            android.location.Location r6 = com.purplebrain.adbuddiz.sdk.i.a.f.m15325(r4)     // Catch:{ all -> 0x00af }
            if (r6 != 0) goto L_0x00b6
            r0 = r1
        L_0x0057:
            if (r0 == 0) goto L_0x008c
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ all -> 0x00af }
            r6.<init>()     // Catch:{ all -> 0x00af }
            java.lang.String r7 = "a"
            double r8 = r0.f12147     // Catch:{ all -> 0x00af }
            r6.put((java.lang.String) r7, (double) r8)     // Catch:{ all -> 0x00af }
            java.lang.String r7 = "o"
            double r8 = r0.f12144     // Catch:{ all -> 0x00af }
            r6.put((java.lang.String) r7, (double) r8)     // Catch:{ all -> 0x00af }
            java.lang.String r7 = "t"
            long r8 = r0.f12146     // Catch:{ all -> 0x00af }
            r6.put((java.lang.String) r7, (long) r8)     // Catch:{ all -> 0x00af }
            java.lang.String r7 = "p"
            java.lang.String r0 = r0.f12145     // Catch:{ all -> 0x00af }
            r6.put((java.lang.String) r7, (java.lang.Object) r0)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = r6.toString()     // Catch:{ all -> 0x00af }
            java.lang.String r6 = "g"
            java.lang.String r0 = com.purplebrain.adbuddiz.sdk.i.g.m15412((java.lang.String) r0)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r6, (java.lang.Object) r0)     // Catch:{ all -> 0x00af }
        L_0x008c:
            java.util.List r0 = com.purplebrain.adbuddiz.sdk.i.a.j.m15337(r4)     // Catch:{ all -> 0x00af }
            boolean r6 = r0.isEmpty()     // Catch:{ all -> 0x00af }
            if (r6 != 0) goto L_0x00e2
            org.json.JSONArray r6 = new org.json.JSONArray     // Catch:{ all -> 0x00af }
            r6.<init>()     // Catch:{ all -> 0x00af }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x00af }
        L_0x009f:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x00af }
            if (r0 == 0) goto L_0x00d4
            java.lang.Object r0 = r7.next()     // Catch:{ all -> 0x00af }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x00af }
            r6.put((java.lang.Object) r0)     // Catch:{ all -> 0x00af }
            goto L_0x009f
        L_0x00af:
            r0 = move-exception
        L_0x00b0:
            if (r1 == 0) goto L_0x00b5
            r1.disconnect()
        L_0x00b5:
            throw r0
        L_0x00b6:
            com.purplebrain.adbuddiz.sdk.h.a.c r0 = new com.purplebrain.adbuddiz.sdk.h.a.c     // Catch:{ all -> 0x00af }
            r0.<init>()     // Catch:{ all -> 0x00af }
            double r8 = r6.getLatitude()     // Catch:{ all -> 0x00af }
            r0.f12147 = r8     // Catch:{ all -> 0x00af }
            double r8 = r6.getLongitude()     // Catch:{ all -> 0x00af }
            r0.f12144 = r8     // Catch:{ all -> 0x00af }
            long r8 = r6.getTime()     // Catch:{ all -> 0x00af }
            r0.f12146 = r8     // Catch:{ all -> 0x00af }
            java.lang.String r6 = r6.getProvider()     // Catch:{ all -> 0x00af }
            r0.f12145 = r6     // Catch:{ all -> 0x00af }
            goto L_0x0057
        L_0x00d4:
            java.lang.String r0 = "p"
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x00af }
            java.lang.String r6 = com.purplebrain.adbuddiz.sdk.i.g.m15412((java.lang.String) r6)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r6)     // Catch:{ all -> 0x00af }
        L_0x00e2:
            java.lang.String r0 = "in"
            boolean r6 = r10.f12183     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (boolean) r6)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "sd"
            r5.put((java.lang.String) r0, (boolean) r2)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "tm"
            boolean r2 = com.purplebrain.adbuddiz.sdk.i.u.m15475(r4)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (boolean) r2)     // Catch:{ all -> 0x00af }
            android.graphics.Point r0 = com.purplebrain.adbuddiz.sdk.i.j.m15421(r4)     // Catch:{ all -> 0x00af }
            java.lang.String r2 = "mw"
            int r6 = r0.x     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r2, (int) r6)     // Catch:{ all -> 0x00af }
            java.lang.String r2 = "mh"
            int r0 = r0.y     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r2, (int) r0)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "or"
            com.purplebrain.adbuddiz.sdk.f.e r2 = com.purplebrain.adbuddiz.sdk.i.a.d.m15318()     // Catch:{ all -> 0x00af }
            java.lang.String r2 = r2.d     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r2)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "ai"
            r2 = 0
            com.purplebrain.adbuddiz.sdk.f.a.a r6 = r10.f12181     // Catch:{ all -> 0x00af }
            org.json.JSONObject r2 = com.purplebrain.adbuddiz.sdk.i.b.c.m15360((android.content.Context) r4, (boolean) r2, (com.purplebrain.adbuddiz.sdk.f.a.a) r6)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r2)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "ri"
            r2 = 0
            org.json.JSONArray r2 = com.purplebrain.adbuddiz.sdk.i.b.c.m15358((android.content.Context) r4, (boolean) r2)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r2)     // Catch:{ all -> 0x00af }
            boolean r0 = r10.f12183     // Catch:{ all -> 0x00af }
            if (r0 == 0) goto L_0x014e
            java.lang.String r0 = "aii"
            r2 = 1
            com.purplebrain.adbuddiz.sdk.f.a.a r6 = r10.f12181     // Catch:{ all -> 0x00af }
            org.json.JSONObject r2 = com.purplebrain.adbuddiz.sdk.i.b.c.m15360((android.content.Context) r4, (boolean) r2, (com.purplebrain.adbuddiz.sdk.f.a.a) r6)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r2)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "rii"
            r2 = 1
            org.json.JSONArray r2 = com.purplebrain.adbuddiz.sdk.i.b.c.m15358((android.content.Context) r4, (boolean) r2)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r2)     // Catch:{ all -> 0x00af }
        L_0x014e:
            java.lang.String r0 = "aia"
            org.json.JSONArray r2 = com.purplebrain.adbuddiz.sdk.i.b.a.m15347(r4)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r2)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "uc"
            java.lang.String r2 = "uc"
            org.json.JSONArray r2 = com.purplebrain.adbuddiz.sdk.i.b.d.m15365(r4, r2)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r2)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "r"
            org.json.JSONArray r2 = com.purplebrain.adbuddiz.sdk.i.b.e.m15370(r4)     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (java.lang.Object) r2)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = "t"
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00af }
            r5.put((java.lang.String) r0, (long) r6)     // Catch:{ all -> 0x00af }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x00af }
            java.net.HttpURLConnection r2 = r10.m15270(r3, r0)     // Catch:{ all -> 0x00af }
            int r0 = r2.getResponseCode()     // Catch:{ all -> 0x019f }
            r3 = 500(0x1f4, float:7.0E-43)
            if (r0 < r3) goto L_0x01a3
            com.purplebrain.adbuddiz.sdk.h.k r1 = new com.purplebrain.adbuddiz.sdk.h.k     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x019f }
            java.lang.String r4 = "Server Error : "
            r3.<init>(r4)     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x019f }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x019f }
            r1.<init>(r0)     // Catch:{ all -> 0x019f }
            throw r1     // Catch:{ all -> 0x019f }
        L_0x019f:
            r0 = move-exception
            r1 = r2
            goto L_0x00b0
        L_0x01a3:
            boolean r3 = r10.m15271()     // Catch:{ all -> 0x019f }
            if (r3 == 0) goto L_0x01af
            if (r2 == 0) goto L_0x01ae
            r2.disconnect()
        L_0x01ae:
            return
        L_0x01af:
            r3 = 204(0xcc, float:2.86E-43)
            if (r0 != r3) goto L_0x01be
            com.purplebrain.adbuddiz.sdk.h.i$b r0 = r10.f12182     // Catch:{ all -> 0x019f }
            r0.m15303(r10)     // Catch:{ all -> 0x019f }
        L_0x01b8:
            if (r2 == 0) goto L_0x01ae
            r2.disconnect()
            goto L_0x01ae
        L_0x01be:
            r3 = 200(0xc8, float:2.8E-43)
            if (r0 != r3) goto L_0x01b8
            java.lang.String r0 = r10.m15269(r2)     // Catch:{ all -> 0x019f }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ all -> 0x019f }
            r3.<init>((java.lang.String) r0)     // Catch:{ all -> 0x019f }
            java.lang.String r4 = "at"
            boolean r4 = r3.has(r4)     // Catch:{ all -> 0x019f }
            if (r4 == 0) goto L_0x01e7
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ all -> 0x019f }
            r1.<init>((java.lang.String) r0)     // Catch:{ all -> 0x019f }
            com.purplebrain.adbuddiz.sdk.f.a.a r0 = com.purplebrain.adbuddiz.sdk.f.a.a.m15155((org.json.JSONObject) r1)     // Catch:{ all -> 0x019f }
            boolean r1 = r10.f12183     // Catch:{ all -> 0x019f }
            r0.f11995 = r1     // Catch:{ all -> 0x019f }
            com.purplebrain.adbuddiz.sdk.h.i$b r1 = r10.f12182     // Catch:{ all -> 0x019f }
            r1.m15304(r10, r0)     // Catch:{ all -> 0x019f }
            goto L_0x01b8
        L_0x01e7:
            java.lang.String r0 = "c"
            java.lang.String r0 = r3.getString(r0)     // Catch:{ all -> 0x019f }
            com.purplebrain.adbuddiz.sdk.h.i$a r0 = com.purplebrain.adbuddiz.sdk.h.i.a.a(r0)     // Catch:{ all -> 0x019f }
            java.lang.String r4 = "d"
            boolean r4 = r3.has(r4)     // Catch:{ all -> 0x019f }
            if (r4 == 0) goto L_0x0202
            java.lang.String r1 = "d"
            java.lang.String r1 = r3.getString(r1)     // Catch:{ all -> 0x019f }
        L_0x0202:
            com.purplebrain.adbuddiz.sdk.h.i$b r3 = r10.f12182     // Catch:{ all -> 0x019f }
            r3.m15305(r10, r0, r1)     // Catch:{ all -> 0x019f }
            goto L_0x01b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.h.i.m15302():void");
    }
}
