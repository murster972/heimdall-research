package com.purplebrain.adbuddiz.sdk.h;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.f.a.a;
import com.purplebrain.adbuddiz.sdk.h.a.a;
import com.purplebrain.adbuddiz.sdk.h.a.b;
import com.purplebrain.adbuddiz.sdk.h.a.d;
import com.purplebrain.adbuddiz.sdk.i.w;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

public final class c extends a {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f12159;

    /* renamed from: 连任  reason: contains not printable characters */
    public a f12160;

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final String m15280() {
        return "AdClick";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m15281() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m15282() {
        HttpURLConnection httpURLConnection = null;
        try {
            URL r0 = w.m15483();
            Context r2 = AdBuddiz.m14865();
            JSONObject jSONObject = new JSONObject();
            new a.C0031a();
            jSONObject.put("a", (Object) a.C0031a.m15258(r2).m15257());
            new b.a();
            jSONObject.put("d", (Object) b.a.m15260(r2).m15259());
            new d.a();
            jSONObject.put("s", (Object) d.a.m15262(r2).m15261());
            jSONObject.put(TtmlNode.TAG_P, (Object) this.f12159);
            jSONObject.put("or", (Object) com.purplebrain.adbuddiz.sdk.i.a.d.m15318().name());
            jSONObject.put("t", System.currentTimeMillis());
            this.f12160.m15168(r2, jSONObject);
            httpURLConnection = m15270(r0, jSONObject.toString());
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 500) {
                throw new k("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }
}
