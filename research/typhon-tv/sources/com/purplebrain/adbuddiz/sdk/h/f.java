package com.purplebrain.adbuddiz.sdk.h;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.i.m;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.s;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.oltu.oauth2.common.OAuth;

public final class f extends a {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f12167;

    /* renamed from: 连任  reason: contains not printable characters */
    public URL f12168;

    public f() {
        this.f12155 = 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final String m15290() {
        return "DownloadAsset";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m15291() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m15292() {
        Context r2;
        String str;
        String r5;
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = b.m15265(this.f12168, OAuth.HttpMethod.GET);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                r2 = AdBuddiz.m14865();
                String str2 = this.f12167;
                InputStream r3 = m15275(httpURLConnection);
                str = "ABZ_31_" + str2 + "_tmp.abz";
                r5 = m.m15444(str2);
                FileOutputStream openFileOutput = r2.openFileOutput(str, 0);
                s.m15471(r3, openFileOutput);
                openFileOutput.flush();
                openFileOutput.close();
                r2.getFileStreamPath(str).renameTo(r2.getFileStreamPath(r5));
            } else if (responseCode >= 500) {
                throw new k("Server Error : " + responseCode);
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        } catch (IOException e) {
            o.m15453("writeAsset() IOException", e);
            r2.getFileStreamPath(str).delete();
            r2.getFileStreamPath(r5).delete();
            throw e;
        } catch (Throwable th) {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }
}
