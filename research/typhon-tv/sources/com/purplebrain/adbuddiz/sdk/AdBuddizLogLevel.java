package com.purplebrain.adbuddiz.sdk;

public enum AdBuddizLogLevel {
    Info,
    Error,
    Silent
}
