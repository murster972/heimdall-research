package com.purplebrain.adbuddiz.sdk.g;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import com.purplebrain.adbuddiz.sdk.d.a;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;

public final class b extends CountDownTimer {

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean f12086 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private a f12087;

    /* renamed from: 齉  reason: contains not printable characters */
    private com.purplebrain.adbuddiz.sdk.f.b f12088;

    /* renamed from: 龘  reason: contains not printable characters */
    e f12089;

    private b(com.purplebrain.adbuddiz.sdk.f.b bVar, a aVar, e eVar) {
        super(bVar.f12053.longValue(), 500);
        this.f12088 = bVar;
        this.f12087 = aVar;
        this.f12089 = eVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m15236(a aVar, e eVar) {
        return new b(com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138(), aVar, eVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15237() {
        if (!this.f12086) {
            this.f12086 = true;
            this.f12087.m15032(true);
        }
    }

    public final void onFinish() {
        try {
            m15237();
        } catch (Throwable th) {
            l.m15430("ABMRAIDCloseTimer.onFinish()", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
            o.m15453("ABMRAIDCloseTimer.onFinish()", th);
        }
    }

    public final void onTick(long j) {
        try {
            if (this.f12088.f12057 != null && this.f12088.f12053.longValue() - j >= this.f12088.f12057.longValue() && this.f12089.m15254()) {
                m15237();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            b.this.cancel();
                        } catch (Throwable th) {
                            l.m15430("ABMRAIDCloseTimer.stopTimer()", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                            o.m15453("ABMRAIDCloseTimer.stopTimer()", th);
                        }
                    }
                });
            }
        } catch (Throwable th) {
            l.m15430("ABMRAIDCloseTimer.onTick()", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
            o.m15453("ABMRAIDCloseTimer.onFinish()", th);
        }
    }
}
