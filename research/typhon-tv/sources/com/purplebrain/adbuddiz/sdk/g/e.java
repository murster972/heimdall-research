package com.purplebrain.adbuddiz.sdk.g;

import android.annotation.SuppressLint;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@SuppressLint({"NewApi"})
public final class e {

    /* renamed from: 龘  reason: contains not printable characters */
    public Map f12114 = new HashMap();

    /* renamed from: com.purplebrain.adbuddiz.sdk.g.e$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f12115 = new int[a.a().length];

        static {
            try {
                f12115[a.LOADING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f12115[a.LOADED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f12115[a.ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public enum a {
        LOADING("new"),
        LOADED("loaded"),
        ERROR("error");
        
        private String d;

        private a(String str) {
            this.d = str;
        }

        public static a a(String str) {
            for (a aVar : a()) {
                if (aVar.d.equals(str)) {
                    return aVar;
                }
            }
            return null;
        }

        public static a[] a() {
            return (a[]) f12116.clone();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m15254() {
        synchronized (this.f12114) {
            if (this.f12114.size() == 0) {
                return false;
            }
            for (String str : this.f12114.keySet()) {
                if (this.f12114.get(str) != a.LOADED) {
                    new StringBuilder("wasFullyLoaded: false - ").append(m15255());
                    return false;
                }
            }
            return true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m15255() {
        int i;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        for (String str : this.f12114.keySet()) {
            switch (AnonymousClass1.f12115[((a) this.f12114.get(str)).ordinal()]) {
                case 1:
                    i4++;
                    continue;
                case 2:
                    i3++;
                    continue;
                case 3:
                    i = i2 + 1;
                    break;
                default:
                    i = i2;
                    break;
            }
            i2 = i;
        }
        return String.format(Locale.US, "%d / %d err: %d", new Object[]{Integer.valueOf(i3), Integer.valueOf(i3 + i4 + i2), Integer.valueOf(i2)});
    }
}
