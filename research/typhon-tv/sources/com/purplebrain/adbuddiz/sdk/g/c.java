package com.purplebrain.adbuddiz.sdk.g;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mopub.common.AdType;
import com.purplebrain.adbuddiz.sdk.f.a.f;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.m;
import com.purplebrain.adbuddiz.sdk.i.o;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public final class c {
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public static boolean f12091 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private static boolean f12092 = false;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static boolean f12093 = false;

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f12094 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean f12095 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    public Point f12096 = new Point(0, 0);

    /* renamed from: ʿ  reason: contains not printable characters */
    private List f12097 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f12098 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    public C0030c f12099 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    public d f12100 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public f f12101 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public f f12102;

    /* renamed from: 麤  reason: contains not printable characters */
    public a f12103 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public WebView f12104 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public Context f12105;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f12106 = true;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private String f12107 = "none";

    private class a extends WebChromeClient {
        private a() {
        }

        public /* synthetic */ a(c cVar, byte b) {
            this();
        }

        @SuppressLint({"NewApi"})
        public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            return !c.f12091;
        }
    }

    private class b extends WebViewClient {
        private b() {
        }

        public /* synthetic */ b(c cVar, byte b) {
            this();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private com.purplebrain.adbuddiz.sdk.f.a.a.c m15249(URL url) {
            for (com.purplebrain.adbuddiz.sdk.f.a.a.c cVar : com.purplebrain.adbuddiz.sdk.i.c.m15375(c.this.f12102, com.purplebrain.adbuddiz.sdk.f.a.a.d.MEDIA, false, com.purplebrain.adbuddiz.sdk.i.a.d.m15318())) {
                if (url.equals(cVar.m15177())) {
                    return cVar;
                }
            }
            return null;
        }

        public final void onLoadResource(WebView webView, String str) {
            try {
                Uri parse = Uri.parse(str);
                if (AdType.MRAID.equals(parse.getScheme())) {
                    c.m15245(c.this, parse);
                }
                if ("adbuddiz".equals(parse.getScheme())) {
                    c.this.f12100.m15253(parse.getLastPathSegment(), parse.getQueryParameter("url"));
                }
            } catch (Throwable th) {
                l.m15430("ABMRAIDController#ABWebViewClient.onLoadResource()", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                o.m15453("ABMRAIDController#ABWebViewClient.onLoadResource()", th);
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            try {
                c.m15246(c.this, webView);
            } catch (Throwable th) {
                l.m15430("ABMRAIDController#ABWebViewClient.onPageFinished", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                o.m15453("ABMRAIDController#ABWebViewClient.onPageFinished()", th);
            }
        }

        @SuppressLint({"NewApi"})
        public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            if (!str.startsWith("http://") && !str.startsWith("https://")) {
                return null;
            }
            c.m15246(c.this, webView);
            try {
                com.purplebrain.adbuddiz.sdk.f.a.a.c r1 = m15249(new URL(str));
                if (r1 == null) {
                    return null;
                }
                new Object[1][0] = str;
                return new WebResourceResponse("application/octet-stream", (String) null, m.m15437(c.this.f12105, r1));
            } catch (IOException e) {
                l.m15430("ABMRAIDController#ABWebViewClient.shouldInterceptRequest", (com.purplebrain.adbuddiz.sdk.f.a.a) null, (Throwable) e);
                o.m15453("ABMRAIDController#ABWebViewClient.shouldInterceptRequest", e);
                return null;
            }
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            try {
                if (!str.startsWith("data:") && !str.startsWith("adbuddiz:") && !str.startsWith("mraid:")) {
                    c.this.f12099.m15252(Uri.parse(str));
                    return true;
                }
            } catch (Throwable th) {
                l.m15430("ABMRAIDController#ABWebViewClient.shouldOverrideUrlLoading", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                o.m15453("ABMRAIDController#ABWebViewClient.shouldOverrideUrlLoading()", th);
            }
            return false;
        }
    }

    /* renamed from: com.purplebrain.adbuddiz.sdk.g.c$c  reason: collision with other inner class name */
    public interface C0030c {
        /* renamed from: 靐  reason: contains not printable characters */
        void m15250(Uri uri);

        /* renamed from: 龘  reason: contains not printable characters */
        void m15251();

        /* renamed from: 龘  reason: contains not printable characters */
        void m15252(Uri uri);
    }

    public interface d {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15253(String str, String str2);
    }

    public c(Context context, f fVar) {
        this.f12102 = fVar;
        this.f12105 = context;
        this.f12097 = Arrays.asList(new String[]{"none", "portrait", "landscape"});
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15243() {
        if (f12093 && Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    @SuppressLint({"SetJavaScriptEnabled", "NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15244(WebView webView) {
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        WebSettings settings = webView.getSettings();
        if (Build.VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            settings.setAllowContentAccess(false);
        }
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        if (f12092) {
            webView.clearCache(true);
            settings.setCacheMode(2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m15245(c cVar, Uri uri) {
        String queryParameter;
        if ("/close".equals(uri.getPath())) {
            cVar.f12099.m15251();
        } else if ("/open".equals(uri.getPath())) {
            String queryParameter2 = uri.getQueryParameter("url");
            if (queryParameter2 != null) {
                try {
                    new URI(queryParameter2);
                    cVar.f12099.m15252(Uri.parse(queryParameter2));
                } catch (URISyntaxException e) {
                    cVar.f12103.m15234(d.ERROR, "'" + queryParameter2 + "' is not a valid URL. Will not be open", "open");
                }
            }
        } else if ("/setOrientationProperties".equals(uri.getPath())) {
            String queryParameter3 = uri.getQueryParameter("allowOrientationChange");
            String queryParameter4 = uri.getQueryParameter("forceOrientation");
            if (queryParameter3 != null && queryParameter4 != null) {
                if (!cVar.f12097.contains(queryParameter4)) {
                    cVar.f12103.m15234(d.ERROR, "'" + queryParameter4 + "' is not one of the orientation defined in MRAID 2.0. Allowed values are " + cVar.f12097.toString(), "open");
                    return;
                }
                cVar.f12106 = Boolean.parseBoolean(queryParameter4);
                cVar.f12107 = queryParameter4;
            }
        } else if ("/playVideo".equals(uri.getPath()) && (queryParameter = uri.getQueryParameter("uri")) != null) {
            try {
                new URI(queryParameter);
                cVar.f12099.m15250(Uri.parse(queryParameter));
            } catch (URISyntaxException e2) {
                cVar.f12103.m15234(d.ERROR, "'" + queryParameter + "' is not a valid URI. Will not be played", "playVideo");
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m15246(c cVar, final WebView webView) {
        if (!cVar.f12098) {
            cVar.f12098 = true;
            final String str = (cVar.f12102.f12036 == null || cVar.f12102.f12036.length() <= 0) ? com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138().f12061 : cVar.f12102.f12036;
            if (str != null && str.length() != 0) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        a.m15233(webView, str);
                    }
                });
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final DisplayMetrics m15247() {
        return this.f12104.getContext().getResources().getDisplayMetrics();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15248(f fVar) {
        if (this.f12101 != fVar) {
            this.f12101 = fVar;
            this.f12103.m15235("_setState", fVar);
            this.f12103.m15234(d.STATE_CHANGE, fVar);
        }
    }
}
