package com.purplebrain.adbuddiz.sdk.g;

public enum d {
    READY("ready", 0),
    ERROR("error", 1),
    STATE_CHANGE("stateChange", 1),
    VIEWABLE_CHANGE("viewableChange", 1),
    SIZE_CHANGE("sizeChange", 2);
    
    private String f;
    private int g;

    private d(String str, int i) {
        this.f = str;
        this.g = i;
    }

    public final String toString() {
        return this.f;
    }
}
