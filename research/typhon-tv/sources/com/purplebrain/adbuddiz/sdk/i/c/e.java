package com.purplebrain.adbuddiz.sdk.i.c;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import com.purplebrain.adbuddiz.sdk.f.b;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;

public final class e extends CountDownTimer {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f12222 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f12223 = false;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public com.purplebrain.adbuddiz.sdk.d.a f12224;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f12225;

    /* renamed from: 齉  reason: contains not printable characters */
    private MediaPlayer f12226;

    /* renamed from: 龘  reason: contains not printable characters */
    public a f12227;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15401();
    }

    private e(com.purplebrain.adbuddiz.sdk.d.a aVar, MediaPlayer mediaPlayer, long j) {
        super(j, 250);
        this.f12225 = j;
        this.f12224 = aVar;
        this.f12226 = mediaPlayer;
        m15398(this.f12225);
        this.f12224.m15032(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static e m15396(com.purplebrain.adbuddiz.sdk.f.a.a aVar, com.purplebrain.adbuddiz.sdk.d.a aVar2, MediaPlayer mediaPlayer, long j, int i, String... strArr) {
        if (aVar.f11995) {
            b r1 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
            Long r2 = m15397(j, strArr);
            if (r2 == null || r2.longValue() < ((long) i)) {
                return null;
            }
            e eVar = new e(aVar2, mediaPlayer, r2.longValue());
            if (r2.longValue() < j || r1.f12050 == null) {
                return eVar;
            }
            eVar.f12222 = true;
            return eVar;
        }
        Long r12 = m15397(j, strArr);
        if (r12 == null || r12.longValue() < ((long) i)) {
            return null;
        }
        return new e(aVar2, mediaPlayer, r12.longValue());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Long m15397(long j, String... strArr) {
        int length = strArr.length;
        int i = 0;
        String str = null;
        while (i < length) {
            String str2 = strArr[i];
            if (str2 == null) {
                str2 = str;
            }
            i++;
            str = str2;
        }
        if (str != null) {
            return Long.valueOf(a.m15379(str, j).f12203);
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15398(long j) {
        this.f12224.m15031(Integer.valueOf((int) Math.ceil((double) (((float) j) / 1000.0f))));
    }

    public final void onFinish() {
        m15400();
    }

    public final void onTick(long j) {
        try {
            long currentPosition = this.f12225 - ((long) this.f12226.getCurrentPosition());
            if (currentPosition <= 0) {
                m15400();
            } else {
                m15398(currentPosition);
            }
        } catch (Throwable th) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m15400() {
        if (!this.f12223) {
            this.f12223 = true;
            cancel();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        if (!e.this.f12222) {
                            e.this.f12224.m15032(true);
                        }
                        e.this.f12224.m15031((Integer) null);
                        if (e.this.f12227 != null) {
                            e.this.f12227.m15401();
                        }
                    } catch (Throwable th) {
                        l.m15430("ABVideoSkipTimer.onSkipEnabled", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                        o.m15451("ABVideoSkipTimer.onSkipEnabled()");
                    }
                }
            });
        }
    }
}
