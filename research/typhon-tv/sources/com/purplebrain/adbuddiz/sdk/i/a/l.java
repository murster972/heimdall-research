package com.purplebrain.adbuddiz.sdk.i.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;

public final class l {

    public enum a {
        PHONE("P"),
        TABLET("T");
        
        public String c;

        private a(String str) {
            this.c = str;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static float m15341(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static DisplayMetrics m15342(Context context) {
        if (Build.VERSION.SDK_INT < 17) {
            return context.getResources().getDisplayMetrics();
        }
        Display display = ((DisplayManager) context.getSystemService("display")).getDisplay(0);
        if (display == null) {
            return null;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getRealMetrics(displayMetrics);
        return displayMetrics;
    }
}
