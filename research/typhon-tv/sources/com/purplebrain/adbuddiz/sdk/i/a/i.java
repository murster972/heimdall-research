package com.purplebrain.adbuddiz.sdk.i.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class i {
    /* renamed from: 靐  reason: contains not printable characters */
    public static Integer m15333(Context context) {
        NetworkInfo r0 = m15334(context);
        if (r0 == null) {
            return null;
        }
        return Integer.valueOf(r0.getType());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static NetworkInfo m15334(Context context) {
        if (!h.m15332(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return null;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || 0 == 0) {
            return null;
        }
        return activeNetworkInfo;
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 齉  reason: contains not printable characters */
    public static Integer m15335(Context context) {
        NetworkInfo r0 = m15334(context);
        if (r0 == null) {
            return null;
        }
        return Integer.valueOf(r0.getSubtype());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15336(Context context) {
        return !h.m15332(context, "android.permission.ACCESS_NETWORK_STATE") || m15334(context) != null;
    }
}
