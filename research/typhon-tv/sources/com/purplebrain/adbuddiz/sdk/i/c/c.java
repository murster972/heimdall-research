package com.purplebrain.adbuddiz.sdk.i.c;

import android.os.CountDownTimer;
import com.purplebrain.adbuddiz.sdk.a.c;
import com.purplebrain.adbuddiz.sdk.i.c.e;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.j.b.b.d;

public final class c {

    /* renamed from: ʻ  reason: contains not printable characters */
    public e.a f12209 = new e.a() {
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m15386() {
            c.this.m15382();
        }
    };

    /* renamed from: ʼ  reason: contains not printable characters */
    private com.purplebrain.adbuddiz.sdk.f.a.a f12210;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f12211;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Boolean f12212;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Boolean f12213;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private c.d f12214;

    /* renamed from: 连任  reason: contains not printable characters */
    public a f12215 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    public long f12216;

    /* renamed from: 麤  reason: contains not printable characters */
    public e f12217 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public int f12218;

    /* renamed from: 龘  reason: contains not printable characters */
    public String f12219;

    private class a extends CountDownTimer {
        public a(long j) {
            super(j, 250);
        }

        public final void onFinish() {
            try {
                c.this.m15382();
            } catch (Throwable th) {
                l.m15430("ABVASTImpressionHelper#ABVASTImpressionTimer.onFinish", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                o.m15453("ABVASTImpressionHelper#ABVASTImpressionTimer.onFinish()", th);
            }
        }

        public final void onTick(long j) {
        }
    }

    public c(com.purplebrain.adbuddiz.sdk.f.a.a aVar, String str, String str2, long j, int i, e eVar, c.d dVar) {
        this.f12210 = aVar;
        this.f12211 = str;
        this.f12219 = str2;
        this.f12216 = j;
        this.f12218 = i;
        this.f12217 = eVar;
        this.f12214 = dVar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m15381() {
        if (this.f12213 == null) {
            this.f12213 = Boolean.valueOf(a.m15379(this.f12219, this.f12216) != null);
        }
        return this.f12213.booleanValue();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m15382() {
        this.f12214.m14942(this.f12210, this.f12211, true);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m15383() {
        return !m15385() && !m15381();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15384(d dVar) {
        if (m15383() && dVar.w.equals(this.f12219)) {
            m15382();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m15385() {
        if (this.f12212 == null) {
            this.f12212 = Boolean.valueOf("impression".equals(this.f12219));
        }
        return this.f12212.booleanValue();
    }
}
