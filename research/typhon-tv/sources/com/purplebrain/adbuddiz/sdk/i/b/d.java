package com.purplebrain.adbuddiz.sdk.i.b;

import android.content.Context;
import android.content.SharedPreferences;
import org.json.JSONArray;

public final class d {
    /* renamed from: 靐  reason: contains not printable characters */
    public static JSONArray m15365(Context context, String str) {
        return new JSONArray(context.getSharedPreferences("ABZ-prefs", 0).getString(str, "[]"));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static void m15366(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ABZ-prefs", 0).edit();
        edit.remove(str);
        edit.commit();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15367(Context context, String str) {
        return context.getSharedPreferences("ABZ-prefs", 0).getString(str, (String) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15368(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ABZ-prefs", 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15369(Context context, String str, JSONArray jSONArray) {
        m15368(context, str, jSONArray.toString());
    }
}
