package com.purplebrain.adbuddiz.sdk.i.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.purplebrain.adbuddiz.sdk.f.a.e;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class c {

    @SuppressLint({"NewApi"})
    private static class a {

        /* renamed from: 靐  reason: contains not printable characters */
        int f12198;

        /* renamed from: 齉  reason: contains not printable characters */
        int f12199;

        /* renamed from: 龘  reason: contains not printable characters */
        Context f12200;

        public a(Context context, int i, int i2) {
            this.f12200 = context;
            this.f12198 = i;
            this.f12199 = i2;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m15352(boolean z) {
        return z ? "RI" : "MRAID";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static Map m15353(SharedPreferences sharedPreferences, String str) {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : sharedPreferences.getAll().entrySet()) {
            try {
                if (((String) next.getKey()).startsWith(str)) {
                    hashMap.put(((String) next.getKey()).substring(str.length()), new JSONArray(next.getValue().toString()));
                }
            } catch (JSONException e) {
            }
        }
        return hashMap;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static /* synthetic */ void m15354(Context context, String str, int i) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("ABZ-impr", 0);
        try {
            JSONArray r2 = m15359(sharedPreferences, str);
            if (r2.length() > i) {
                JSONArray jSONArray = new JSONArray();
                for (int length = r2.length() - i; length < r2.length(); length++) {
                    jSONArray.put(r2.getLong(length));
                }
                m15364(sharedPreferences, "MRAID", jSONArray);
            }
        } catch (JSONException e) {
            m15364(sharedPreferences, "MRAID", new JSONArray());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15355(Map map) {
        long j = 0;
        String str = null;
        for (Map.Entry entry : map.entrySet()) {
            JSONArray jSONArray = (JSONArray) entry.getValue();
            String str2 = str;
            for (int i = 0; i < jSONArray.length(); i++) {
                if (jSONArray.getLong(i) > j) {
                    str2 = (String) entry.getKey();
                    j = jSONArray.getLong(i);
                }
            }
            str = str2;
        }
        if (System.currentTimeMillis() - j > 10000) {
            return null;
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15356(boolean z) {
        return z ? "I-" : "A-";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static JSONArray m15357(Context context, com.purplebrain.adbuddiz.sdk.f.a.a aVar, boolean z) {
        return m15359(context.getSharedPreferences("ABZ-impr", 0), m15356(z) + aVar.m15162());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static JSONArray m15358(Context context, boolean z) {
        return m15359(context.getSharedPreferences("ABZ-impr", 0), m15352(z));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static JSONArray m15359(SharedPreferences sharedPreferences, String str) {
        try {
            return new JSONArray(sharedPreferences.getString(str, "[]"));
        } catch (JSONException e) {
            return new JSONArray();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static JSONObject m15360(Context context, boolean z, com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
        String r0;
        JSONObject jSONObject = new JSONObject();
        Map r1 = m15353(context.getSharedPreferences("ABZ-impr", 0), m15356(z));
        if (aVar != null && aVar.m15161().f && ((r0 = m15355(r1)) == null || !r0.equals(aVar.m15162()))) {
            JSONArray jSONArray = (JSONArray) r1.get(aVar.m15162());
            if (jSONArray == null) {
                jSONArray = new JSONArray();
                r1.put(aVar.m15162(), jSONArray);
            }
            jSONArray.put(System.currentTimeMillis());
        }
        for (Map.Entry entry : r1.entrySet()) {
            jSONObject.put((String) entry.getKey(), entry.getValue());
        }
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15361(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("ABZ-impr", 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        for (Map.Entry next : sharedPreferences.getAll().entrySet()) {
            if (((String) next.getKey()).startsWith("A-") || ((String) next.getKey()).startsWith("I-") || ((String) next.getKey()).equals("MRAID") || ((String) next.getKey()).equals("RI")) {
                edit.remove((String) next.getKey());
            }
        }
        edit.commit();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15362(Context context, com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
        if (aVar.m15161() == e.ADBUDDIZ || aVar.m15161() == e.ADBUDDIZ_VIDEO) {
            Date date = new Date();
            SharedPreferences sharedPreferences = context.getSharedPreferences("ABZ-impr", 0);
            String r2 = m15356(aVar.f11995);
            JSONArray r3 = m15359(sharedPreferences, m15356(aVar.f11995) + aVar.m15162());
            r3.put(date.getTime());
            m15364(sharedPreferences, r2 + aVar.m15162(), r3);
            return;
        }
        Date date2 = new Date();
        SharedPreferences sharedPreferences2 = context.getSharedPreferences("ABZ-impr", 0);
        JSONArray r22 = m15359(sharedPreferences2, m15352(aVar.f11995));
        r22.put(date2.getTime());
        m15364(sharedPreferences2, m15352(aVar.f11995), r22);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static /* synthetic */ void m15363(Context context, String str, int i) {
        int i2;
        int i3 = 0;
        SharedPreferences sharedPreferences = context.getSharedPreferences("ABZ-impr", 0);
        Map r8 = m15353(sharedPreferences, str);
        Iterator it2 = r8.entrySet().iterator();
        while (true) {
            i2 = i3;
            if (!it2.hasNext()) {
                break;
            }
            i3 = ((JSONArray) ((Map.Entry) it2.next()).getValue()).length() + i2;
        }
        if (i2 > i) {
            for (int i4 = i2; i4 > i; i4--) {
                long j = Long.MAX_VALUE;
                Map.Entry entry = null;
                try {
                    for (Map.Entry entry2 : r8.entrySet()) {
                        if (((JSONArray) entry2.getValue()).length() <= 0 || ((JSONArray) entry2.getValue()).getLong(0) >= j) {
                            entry2 = entry;
                        } else {
                            j = ((JSONArray) entry2.getValue()).getLong(0);
                        }
                        entry = entry2;
                    }
                    if (entry != null) {
                        String str2 = (String) entry.getKey();
                        if (((JSONArray) entry.getValue()).length() == 1) {
                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.remove(str2);
                            edit.commit();
                        } else {
                            JSONArray jSONArray = new JSONArray();
                            for (int i5 = 1; i5 < ((JSONArray) entry.getValue()).length(); i5++) {
                                jSONArray.put(((JSONArray) entry.getValue()).getLong(i5));
                            }
                            m15364(sharedPreferences, "A-" + str2, jSONArray);
                        }
                    }
                } catch (JSONException e) {
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m15364(SharedPreferences sharedPreferences, String str, JSONArray jSONArray) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(str, jSONArray.toString());
        edit.commit();
    }
}
