package com.purplebrain.adbuddiz.sdk.i;

import java.io.InputStream;
import java.io.OutputStream;

public final class s {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m15471(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[1024];
        int i = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return i;
            }
            outputStream.write(bArr, 0, read);
            i += read;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0049 A[SYNTHETIC, Splitter:B:26:0x0049] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m15472(java.io.InputStream r5) {
        /*
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r2 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0057, all -> 0x0045 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0057, all -> 0x0045 }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r5, r4)     // Catch:{ IOException -> 0x0057, all -> 0x0045 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0057, all -> 0x0045 }
        L_0x0013:
            java.lang.String r2 = r1.readLine()     // Catch:{ IOException -> 0x001d }
            if (r2 == 0) goto L_0x002d
            r0.append(r2)     // Catch:{ IOException -> 0x001d }
            goto L_0x0013
        L_0x001d:
            r0 = move-exception
        L_0x001e:
            java.lang.String r2 = "readStream() IOException"
            com.purplebrain.adbuddiz.sdk.i.o.m15453(r2, r0)     // Catch:{ all -> 0x0055 }
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ IOException -> 0x003d }
        L_0x0029:
            java.lang.String r0 = ""
        L_0x002c:
            return r0
        L_0x002d:
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x001d }
            r1.close()     // Catch:{ IOException -> 0x0035 }
            goto L_0x002c
        L_0x0035:
            r1 = move-exception
            java.lang.String r2 = "readStream() IOException finally"
            com.purplebrain.adbuddiz.sdk.i.o.m15453(r2, r1)
            goto L_0x002c
        L_0x003d:
            r0 = move-exception
            java.lang.String r1 = "readStream() IOException finally"
            com.purplebrain.adbuddiz.sdk.i.o.m15453(r1, r0)
            goto L_0x0029
        L_0x0045:
            r0 = move-exception
            r1 = r2
        L_0x0047:
            if (r1 == 0) goto L_0x004c
            r1.close()     // Catch:{ IOException -> 0x004d }
        L_0x004c:
            throw r0
        L_0x004d:
            r1 = move-exception
            java.lang.String r2 = "readStream() IOException finally"
            com.purplebrain.adbuddiz.sdk.i.o.m15453(r2, r1)
            goto L_0x004c
        L_0x0055:
            r0 = move-exception
            goto L_0x0047
        L_0x0057:
            r0 = move-exception
            r1 = r2
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.i.s.m15472(java.io.InputStream):java.lang.String");
    }
}
