package com.purplebrain.adbuddiz.sdk.i.a;

import android.content.Context;
import android.util.DisplayMetrics;
import com.purplebrain.adbuddiz.sdk.f.e;

public enum m {
    SMALL(1.0d, 0.3d, 0),
    PHONE(1.0d, 1.0d, -10),
    SMALL_TABLET(2.0d, 2.0d, -10),
    BIG_TABLET(4.0d, 4.0d, -10);
    
    double e;
    double f;
    int g;

    private m(double d, double d2, int i) {
        this.e = d;
        this.f = d2;
        this.g = i;
    }

    public static m a(Context context) {
        boolean z = true;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        double d = ((double) displayMetrics.widthPixels) / ((double) displayMetrics.densityDpi);
        double d2 = ((double) displayMetrics.heightPixels) / ((double) displayMetrics.densityDpi);
        double sqrt = (double) ((float) Math.sqrt((d2 * d2) + (d * d)));
        if (d.m15318() == e.PORT) {
            DisplayMetrics displayMetrics2 = context.getResources().getDisplayMetrics();
            if (((double) displayMetrics2.heightPixels) / ((double) displayMetrics2.density) >= 470.0d) {
                z = false;
            }
        } else {
            DisplayMetrics displayMetrics3 = context.getResources().getDisplayMetrics();
            if (((double) displayMetrics3.widthPixels) / ((double) displayMetrics3.density) >= 470.0d) {
                z = false;
            }
        }
        return z ? SMALL : sqrt >= 9.0d ? BIG_TABLET : sqrt >= 6.0d ? SMALL_TABLET : PHONE;
    }
}
