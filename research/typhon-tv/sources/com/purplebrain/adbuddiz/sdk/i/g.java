package com.purplebrain.adbuddiz.sdk.i;

import android.annotation.SuppressLint;
import com.purplebrain.adbuddiz.sdk.f.b;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class g {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15412(String str) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(str.getBytes("UTF-8"));
            gZIPOutputStream.flush();
            gZIPOutputStream.close();
            return m15413(byteArrayOutputStream.toByteArray());
        } catch (Exception e) {
            o.m15453("ABDataEncryptionHelper.gzipAndEncode()", e);
            return null;
        }
    }

    @SuppressLint({"TrulyRandom"})
    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15413(byte[] bArr) {
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(f.m15409("bWZSYjVSNUtlc2tkYjJ4Tg=="));
            SecretKeySpec r1 = m15414();
            if (r1 == null) {
                return null;
            }
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, r1, ivParameterSpec);
            return new String(f.m15411(instance.doFinal(bArr)));
        } catch (Exception e) {
            o.m15453("ABDataEncryptionHelper.encrypt()", e);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static SecretKeySpec m15414() {
        String sb;
        b r2 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
        if (r2 == null) {
            sb = null;
        } else {
            StringBuilder sb2 = new StringBuilder();
            for (int i = 0; i < 12; i++) {
                sb2.append(r2.f12065.charAt(i));
                sb2.append("D01FX2jKW5g=".charAt(i));
            }
            sb = sb2.toString();
        }
        if (sb == null) {
            return null;
        }
        return new SecretKeySpec(f.m15409(sb), "AES");
    }
}
