package com.purplebrain.adbuddiz.sdk.i.b;

import android.content.Context;
import android.content.SharedPreferences;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.i.q;
import com.purplebrain.adbuddiz.sdk.i.u;

public final class b {
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m15348() {
        String r1 = q.m15465(AdBuddiz.m14865());
        SharedPreferences sharedPreferences = AdBuddiz.m14865().getSharedPreferences("ABZ-prefs", 0);
        if (!sharedPreferences.contains("LAST_PUBLISHER_KEY")) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("LAST_PUBLISHER_KEY", r1);
            edit.commit();
            return false;
        } else if (r1.equals(sharedPreferences.getString("LAST_PUBLISHER_KEY", (String) null))) {
            return false;
        } else {
            SharedPreferences.Editor edit2 = sharedPreferences.edit();
            edit2.putString("LAST_PUBLISHER_KEY", r1);
            edit2.commit();
            return true;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static void m15349() {
        SharedPreferences.Editor edit = AdBuddiz.m14865().getSharedPreferences("ABZ-prefs", 0).edit();
        edit.clear();
        edit.putString("LAST_PUBLISHER_KEY", q.m15465(AdBuddiz.m14865()));
        edit.putBoolean("tm", u.m15475(AdBuddiz.m14865()));
        edit.commit();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15350(Context context) {
        return context.getSharedPreferences("ABZ-prefs", 0).getString("ai", (String) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15351() {
        boolean r1 = u.m15475(AdBuddiz.m14865());
        SharedPreferences sharedPreferences = AdBuddiz.m14865().getSharedPreferences("ABZ-prefs", 0);
        if (!sharedPreferences.contains("tm")) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean("tm", r1);
            edit.commit();
            return false;
        } else if (sharedPreferences.getBoolean("tm", false) == r1) {
            return false;
        } else {
            SharedPreferences.Editor edit2 = sharedPreferences.edit();
            edit2.putBoolean("tm", r1);
            edit2.commit();
            return true;
        }
    }
}
