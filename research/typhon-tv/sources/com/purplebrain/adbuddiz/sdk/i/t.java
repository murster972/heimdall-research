package com.purplebrain.adbuddiz.sdk.i;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.e.b;
import com.purplebrain.adbuddiz.sdk.i.a.e;

public final class t {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15473(Context context, String str) {
        if (str.contains("{publisherId}")) {
            str = m15474(str, "{publisherId}", Long.toString(b.m15135().m15138().f12039));
        }
        if (str.contains("{androidId}")) {
            str = m15474(str, "{androidId}", e.m15324(context));
        }
        if (str.contains("{androidIdMD5}")) {
            str = m15474(str, "{androidIdMD5}", n.m15449("MD5", e.m15324(context)));
        }
        if (str.contains("{androidIdSHA1}")) {
            str = m15474(str, "{androidIdSHA1}", n.m15449("SHA-1", e.m15324(context)));
        }
        if (str.contains("{googleAid}")) {
            str = m15474(str, "{googleAid}", com.purplebrain.adbuddiz.sdk.i.b.b.m15350(context));
        }
        if (str.contains("{googleAidMD5}")) {
            str = m15474(str, "{googleAidMD5}", n.m15449("MD5", com.purplebrain.adbuddiz.sdk.i.b.b.m15350(context)));
        }
        return str.contains("{googleAidSHA1}") ? m15474(str, "{googleAidSHA1}", n.m15449("SHA-1", com.purplebrain.adbuddiz.sdk.i.b.b.m15350(context))) : str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m15474(String str, String str2, String str3) {
        return str3 != null ? str.replace(str2, str3) : str.replace(str2, "");
    }
}
