package com.purplebrain.adbuddiz.sdk.i.a;

import android.content.Context;
import android.util.TypedValue;

public final class k {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m15339(Context context, float f) {
        return (int) TypedValue.applyDimension(1, f, context.getResources().getDisplayMetrics());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m15340(Context context, int i) {
        return (int) TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics());
    }
}
