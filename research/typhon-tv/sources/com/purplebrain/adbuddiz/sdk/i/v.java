package com.purplebrain.adbuddiz.sdk.i;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import java.util.Arrays;
import java.util.List;

@SuppressLint({"NewApi"})
public final class v {

    /* renamed from: 麤  reason: contains not printable characters */
    private static List f12250 = Arrays.asList(new String[]{"http://play.google.com/store/apps/details?", "https://play.google.com/store/apps/details?", "http://market.android.com/details?", "https://market.android.com/details?"});

    /* renamed from: 靐  reason: contains not printable characters */
    public b f12251 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public a f12252 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    Context f12253;

    private class a {

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f12254;

        /* renamed from: 龘  reason: contains not printable characters */
        Thread f12256;

        private a() {
            this.f12256 = null;
            this.f12254 = false;
        }

        /* synthetic */ a(v vVar, byte b) {
            this();
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00b5  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00be  */
        /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.String m15479(java.lang.String r8) {
            /*
                r7 = this;
                r2 = 0
                java.lang.Thread r0 = java.lang.Thread.currentThread()
                boolean r0 = r0.isInterrupted()
                if (r0 == 0) goto L_0x000d
                r1 = r2
            L_0x000c:
                return r1
            L_0x000d:
                java.lang.String r1 = com.purplebrain.adbuddiz.sdk.i.v.m15476(r8)
                java.lang.String r0 = "market"
                boolean r0 = r1.startsWith(r0)
                if (r0 != 0) goto L_0x000c
                java.net.URL r4 = new java.net.URL     // Catch:{ InterruptedIOException -> 0x00c7, IOException -> 0x00b1, all -> 0x00ba }
                r4.<init>(r1)     // Catch:{ InterruptedIOException -> 0x00c7, IOException -> 0x00b1, all -> 0x00ba }
                java.net.URLConnection r0 = r4.openConnection()     // Catch:{ InterruptedIOException -> 0x00c7, IOException -> 0x00b1, all -> 0x00ba }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ InterruptedIOException -> 0x00c7, IOException -> 0x00b1, all -> 0x00ba }
                r3 = 8000(0x1f40, float:1.121E-41)
                r0.setConnectTimeout(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r3 = 8000(0x1f40, float:1.121E-41)
                r0.setReadTimeout(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r3 = 0
                r0.setInstanceFollowRedirects(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r3 = 0
                r0.setUseCaches(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r3 = 0
                r0.setAllowUserInteraction(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.String r3 = "Accept-Encoding"
                java.lang.String r5 = "gzip"
                r0.setRequestProperty(r3, r5)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r3 = 1
                r0.setDoInput(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.String r3 = "GET"
                r0.setRequestMethod(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                int r3 = r0.getResponseCode()     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r5 = 302(0x12e, float:4.23E-43)
                if (r3 != r5) goto L_0x009d
                java.lang.String r3 = "location"
                java.lang.String r3 = r0.getHeaderField(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                if (r3 == 0) goto L_0x0096
                java.lang.String r5 = "/"
                boolean r5 = r3.startsWith(r5)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                if (r5 == 0) goto L_0x008c
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r5.<init>()     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.String r6 = r4.getProtocol()     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.String r6 = "://"
                java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.String r4 = r4.getHost()     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.String r3 = r3.toString()     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
            L_0x008c:
                r4 = 1
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r5 = 0
                r4[r5] = r3     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                java.lang.String r1 = r7.m15479(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
            L_0x0096:
                if (r0 == 0) goto L_0x000c
                r0.disconnect()
                goto L_0x000c
            L_0x009d:
                r4 = 1
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r5 = 0
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                r4[r5] = r3     // Catch:{ InterruptedIOException -> 0x00a8, IOException -> 0x00c5, all -> 0x00c2 }
                goto L_0x0096
            L_0x00a8:
                r1 = move-exception
            L_0x00a9:
                if (r0 == 0) goto L_0x00ae
                r0.disconnect()
            L_0x00ae:
                r1 = r2
                goto L_0x000c
            L_0x00b1:
                r0 = move-exception
                r0 = r2
            L_0x00b3:
                if (r0 == 0) goto L_0x000c
                r0.disconnect()
                goto L_0x000c
            L_0x00ba:
                r0 = move-exception
                r1 = r0
            L_0x00bc:
                if (r2 == 0) goto L_0x00c1
                r2.disconnect()
            L_0x00c1:
                throw r1
            L_0x00c2:
                r1 = move-exception
                r2 = r0
                goto L_0x00bc
            L_0x00c5:
                r2 = move-exception
                goto L_0x00b3
            L_0x00c7:
                r0 = move-exception
                r0 = r2
                goto L_0x00a9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.i.v.a.m15479(java.lang.String):java.lang.String");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final synchronized void m15480() {
            this.f12254 = true;
            if (this.f12256 != null) {
                this.f12256.interrupt();
            }
        }
    }

    public interface b {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15481();
    }

    public v(Context context) {
        this.f12253 = context;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static /* synthetic */ String m15476(String str) {
        for (String str2 : f12250) {
            if (str.startsWith(str2)) {
                return str.replace(str2, "market://details?");
            }
        }
        return str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15478(String str) {
        this.f12252 = new a(this, (byte) 0);
        a aVar = this.f12252;
        aVar.f12256 = new Thread(new Runnable(str) {

            /* renamed from: 龘  reason: contains not printable characters */
            final /* synthetic */ String f12258;

            {
                this.f12258 = r2;
            }

            public final void run() {
                try {
                    final String r0 = a.this.m15479(this.f12258);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            v vVar;
                            String str;
                            try {
                                if (r0 != null && !a.this.f12254) {
                                    v.this.f12251.m15481();
                                    vVar = v.this;
                                    str = r0;
                                    vVar.f12253.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                                }
                            } catch (ActivityNotFoundException e) {
                                if (str.startsWith("market")) {
                                    if (str.startsWith("market://details?")) {
                                        str = str.replace("market://details?", "https://play.google.com/store/apps/details?");
                                    } else if (str.startsWith("market://search?")) {
                                        str = str.replace("market://search?", "https://play.google.com/store/search?");
                                    }
                                    try {
                                        vVar.f12253.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                                    } catch (ActivityNotFoundException e2) {
                                    }
                                }
                            } catch (Throwable th) {
                                l.m15430("ABTrackingLinkHelper#FollowTrackingLinkAsyncTask.execute", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                            }
                        }
                    });
                } catch (Throwable th) {
                    l.m15430("ABTrackingLinkHelper#FollowTrackingLinkAsyncTask.execute", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                }
            }
        });
        aVar.f12256.start();
    }
}
