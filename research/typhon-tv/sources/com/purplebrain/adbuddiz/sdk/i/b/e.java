package com.purplebrain.adbuddiz.sdk.i.b;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.f.b;
import com.purplebrain.adbuddiz.sdk.h.h;
import java.util.ArrayList;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.json.JSONArray;
import org.json.JSONObject;

public final class e {
    /* renamed from: 龘  reason: contains not printable characters */
    public static JSONArray m15370(Context context) {
        b r1 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
        JSONArray r2 = d.m15365(context, InternalZipTyphoonApp.READ_MODE);
        ArrayList<h> arrayList = new ArrayList<>();
        for (int i = 0; i < r2.length(); i++) {
            JSONObject jSONObject = r2.getJSONObject(i);
            h hVar = new h();
            hVar.f12180 = jSONObject.getString(InternalZipTyphoonApp.READ_MODE);
            hVar.f12177 = jSONObject.getLong("t");
            hVar.f12179 = jSONObject.getLong("d");
            hVar.f12178 = jSONObject.getLong("rs");
            hVar.f12176 = jSONObject.getLong("s");
            if (jSONObject.has("nt")) {
                hVar.f12174 = Integer.valueOf(jSONObject.getInt("nt"));
            }
            if (jSONObject.has("nst")) {
                hVar.f12175 = Integer.valueOf(jSONObject.getInt("nst"));
            }
            long currentTimeMillis = System.currentTimeMillis() - hVar.f12177;
            if (r1 == null || (currentTimeMillis >= 0 && currentTimeMillis < r1.f12068)) {
                arrayList.add(hVar);
            }
        }
        JSONArray jSONArray = new JSONArray();
        for (h r0 : arrayList) {
            jSONArray.put((Object) r0.m15298());
        }
        return jSONArray;
    }
}
