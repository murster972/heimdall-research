package com.purplebrain.adbuddiz.sdk.i.a;

import android.content.Context;
import android.view.Display;
import android.view.WindowManager;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.f.e;

public final class d {
    /* renamed from: 龘  reason: contains not printable characters */
    public static e m15318() {
        Context r1 = AdBuddiz.m14865();
        Display defaultDisplay = ((WindowManager) r1.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay.getWidth() < defaultDisplay.getHeight()) {
            return e.PORT;
        }
        if (defaultDisplay.getWidth() > defaultDisplay.getHeight()) {
            return e.LAND;
        }
        int i = r1.getResources().getConfiguration().orientation;
        return i == 1 ? e.PORT : i == 2 ? e.LAND : e.PORT;
    }
}
