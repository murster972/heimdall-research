package com.purplebrain.adbuddiz.sdk.i.a;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

public final class g {

    public static final class a {

        /* renamed from: 靐  reason: contains not printable characters */
        final boolean f12190;

        /* renamed from: 龘  reason: contains not printable characters */
        final String f12191;

        a(String str, boolean z) {
            this.f12191 = str;
            this.f12190 = z;
        }
    }

    private static final class b implements ServiceConnection {

        /* renamed from: 靐  reason: contains not printable characters */
        final LinkedBlockingQueue f12192;

        /* renamed from: 龘  reason: contains not printable characters */
        boolean f12193;

        private b() {
            this.f12193 = false;
            this.f12192 = new LinkedBlockingQueue(1);
        }

        /* synthetic */ b(byte b) {
            this();
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.f12192.put(iBinder);
            } catch (InterruptedException e) {
            }
        }

        public final void onServiceDisconnected(ComponentName componentName) {
        }
    }

    private static final class c implements IInterface {

        /* renamed from: 龘  reason: contains not printable characters */
        private IBinder f12194;

        public c(IBinder iBinder) {
            this.f12194 = iBinder;
        }

        public final IBinder asBinder() {
            return this.f12194;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public final boolean m15327() {
            boolean z = true;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(1);
                this.f12194.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z = false;
                }
                return z;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final String m15328() {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.f12194.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readString();
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static a m15326(Context context) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("Cannot be called from the main thread");
        }
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            b bVar = new b((byte) 0);
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            if (context.bindService(intent, bVar, 1)) {
                try {
                    if (bVar.f12193) {
                        throw new IllegalStateException();
                    }
                    bVar.f12193 = true;
                    c cVar = new c((IBinder) bVar.f12192.take());
                    a aVar = new a(cVar.m15328(), cVar.m15327());
                    context.unbindService(bVar);
                    return aVar;
                } catch (Exception e) {
                    throw e;
                } catch (Throwable th) {
                    context.unbindService(bVar);
                    throw th;
                }
            } else {
                throw new IOException("Google Play connection failed");
            }
        } catch (Exception e2) {
            throw e2;
        }
    }
}
