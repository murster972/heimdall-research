package com.purplebrain.adbuddiz.sdk.i.a;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public final class c {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m15315(Context context) {
        try {
            return m15316(context).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static PackageInfo m15316(Context context) {
        return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m15317(Context context) {
        try {
            return m15316(context).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }
}
