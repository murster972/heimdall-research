package com.purplebrain.adbuddiz.sdk.i;

import android.util.Log;
import com.purplebrain.adbuddiz.sdk.AdBuddizLogLevel;

public final class o {

    /* renamed from: 龘  reason: contains not printable characters */
    private static AdBuddizLogLevel f12237 = AdBuddizLogLevel.Info;

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m15451(String str) {
        if (!AdBuddizLogLevel.Silent.equals(f12237)) {
            Log.e("AdBuddiz", str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15452(String str) {
        if (AdBuddizLogLevel.Info.equals(f12237)) {
            Log.i("AdBuddiz", str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15453(String str, Throwable th) {
        if (!AdBuddizLogLevel.Silent.equals(f12237)) {
            Log.e("AdBuddiz", str, th);
        }
    }
}
