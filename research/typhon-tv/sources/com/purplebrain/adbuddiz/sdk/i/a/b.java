package com.purplebrain.adbuddiz.sdk.i.a;

import android.os.Build;

public final class b {
    /* renamed from: 靐  reason: contains not printable characters */
    public static int m15313() {
        if (Build.VERSION.RELEASE.startsWith("1.5")) {
            return 3;
        }
        try {
            return Build.VERSION.SDK_INT;
        } catch (VerifyError e) {
            return 3;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15314() {
        return m15313() >= 7;
    }
}
