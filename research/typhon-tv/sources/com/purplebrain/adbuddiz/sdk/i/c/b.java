package com.purplebrain.adbuddiz.sdk.i.c;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import com.purplebrain.adbuddiz.sdk.d.a;
import com.purplebrain.adbuddiz.sdk.i.o;

public final class b extends CountDownTimer {

    /* renamed from: 连任  reason: contains not printable characters */
    private Integer f12204;

    /* renamed from: 靐  reason: contains not printable characters */
    private a f12205;

    /* renamed from: 麤  reason: contains not printable characters */
    private com.purplebrain.adbuddiz.sdk.f.b f12206 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();

    /* renamed from: 齉  reason: contains not printable characters */
    private MediaPlayer f12207;

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer f12208;

    public b(a aVar, MediaPlayer mediaPlayer) {
        super(2147483647L, 1000);
        this.f12205 = aVar;
        this.f12207 = mediaPlayer;
    }

    public final void onFinish() {
    }

    public final void onTick(long j) {
        try {
            if (this.f12208 != null && this.f12207.isPlaying()) {
                this.f12204 = Integer.valueOf((this.f12207.getCurrentPosition() - 1000) - this.f12208.intValue());
                if (m15380()) {
                    new Object[1][0] = this.f12204;
                    this.f12205.m15032(true);
                }
            }
            this.f12208 = Integer.valueOf(this.f12207.getCurrentPosition());
        } catch (Throwable th) {
            o.m15453("ABVASTStuckMediaPlayerDetector.onTick()", th);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m15380() {
        return this.f12204 != null && this.f12207 != null && this.f12207.isPlaying() && ((long) this.f12204.intValue()) > this.f12206.f12058;
    }
}
