package com.purplebrain.adbuddiz.sdk.i;

import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.f.a.a;
import com.purplebrain.adbuddiz.sdk.f.a.e;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class b {
    /* renamed from: 龘  reason: contains not printable characters */
    private static Map m15343(e eVar) {
        List<File> r0 = m.m15445(AdBuddiz.m14865(), eVar, false);
        HashMap hashMap = new HashMap();
        for (File file : r0) {
            try {
                String r1 = m.m15443(file, eVar);
                int indexOf = r1.indexOf(95);
                if (indexOf == -1) {
                    throw new Exception();
                }
                long parseLong = Long.parseLong(r1.substring(0, indexOf));
                List list = (List) hashMap.get(Long.valueOf(parseLong));
                if (list == null) {
                    list = new ArrayList();
                    hashMap.put(Long.valueOf(parseLong), list);
                }
                list.add(file);
            } catch (Exception e) {
                new StringBuilder("Could not determine ad id from file name: '").append(file.getName()).append("'. May not be a cached resource.");
            }
        }
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15344(Collection collection) {
        com.purplebrain.adbuddiz.sdk.f.b r1 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
        if (r1 != null) {
            HashMap hashMap = new HashMap();
            m15346((Map) hashMap, e.ADBUDDIZ);
            m15346((Map) hashMap, e.ADBUDDIZ_VIDEO);
            Object[] objArr = {Integer.valueOf(hashMap.size()), Integer.valueOf(r1.f12055)};
            for (int size = hashMap.size(); size > r1.f12055; size--) {
                m15345(collection, (Map) hashMap);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m15345(Collection collection, Map map) {
        boolean z;
        long j;
        boolean z2;
        Map.Entry entry = null;
        long j2 = Long.MAX_VALUE;
        for (Map.Entry entry2 : map.entrySet()) {
            Long l = (Long) entry2.getKey();
            if (collection != null) {
                Iterator it2 = collection.iterator();
                z = false;
                while (!z && it2.hasNext()) {
                    a aVar = (a) it2.next();
                    if (aVar != null) {
                        switch (aVar.m15161()) {
                            case ADBUDDIZ:
                            case ADBUDDIZ_VIDEO:
                                if (((com.purplebrain.adbuddiz.sdk.f.a.b) aVar).f12018 != l.longValue()) {
                                    z2 = false;
                                    break;
                                } else {
                                    z2 = true;
                                    break;
                                }
                        }
                    }
                    z2 = z;
                    z = z2;
                }
            } else {
                z = false;
            }
            if (!z) {
                j = ((File) ((List) entry2.getValue()).get(0)).lastModified();
                if (j < j2) {
                    j2 = j;
                    entry = entry2;
                }
            }
            j = j2;
            entry2 = entry;
            j2 = j;
            entry = entry2;
        }
        if (entry != null) {
            for (File file : (List) entry.getValue()) {
                new Object[1][0] = m.m15443(file, e.ADBUDDIZ);
                file.delete();
            }
            map.remove(entry.getKey());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m15346(Map map, e eVar) {
        for (Map.Entry entry : m15343(eVar).entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
    }
}
