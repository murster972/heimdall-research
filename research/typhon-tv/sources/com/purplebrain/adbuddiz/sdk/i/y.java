package com.purplebrain.adbuddiz.sdk.i;

import android.annotation.SuppressLint;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathTyphoonApp;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressLint({"NewApi"})
public final class y {

    /* renamed from: 龘  reason: contains not printable characters */
    private static XPathFactory f12265 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Integer m15496(Node node, String str) {
        String r0 = m15497(node, str);
        if (r0 != null) {
            return Integer.valueOf(Integer.parseInt(r0));
        }
        return null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static String m15497(Node node, String str) {
        Node namedItem;
        if (node.getAttributes() == null || (namedItem = node.getAttributes().getNamedItem(str)) == null) {
            return null;
        }
        return namedItem.getTextContent();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Node m15498(Node node, String str) {
        ArrayList arrayList = new ArrayList();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            if (str.equals(childNodes.item(i).getNodeName())) {
                arrayList.add(childNodes.item(i));
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return (Node) arrayList.get(0);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m15499(Node node, String str) {
        Node r0 = m15498(node, str);
        if (r0 == null) {
            return null;
        }
        return m15501(r0.getTextContent());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static List m15500(Node node, String str) {
        ArrayList arrayList = new ArrayList();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            if (str.equals(childNodes.item(i).getNodeName())) {
                arrayList.add(m15501(childNodes.item(i).getTextContent()));
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15501(String str) {
        return str.replaceAll("\n|\t", "");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Map m15502(Node node) {
        HashMap hashMap = new HashMap();
        if (node.getAttributes() != null) {
            for (int i = 0; i < node.getAttributes().getLength(); i++) {
                Node item = node.getAttributes().item(i);
                hashMap.put(item.getNodeName(), item.getNodeValue());
            }
        }
        return hashMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static NodeList m15503(Node node, String str) {
        try {
            if (f12265 == null) {
                f12265 = XPathFactory.newInstance();
            }
            return (NodeList) f12265.newXPath().evaluate(str, node, XPathTyphoonApp.NODESET);
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }
}
