package com.purplebrain.adbuddiz.sdk.i;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.f.a.a.c;
import com.purplebrain.adbuddiz.sdk.f.a.e;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class m {
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        r0 = null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* renamed from: 连任  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.lang.String m15431(android.content.Context r4, java.lang.String r5) {
        /*
            r1 = 0
            java.lang.Class<com.purplebrain.adbuddiz.sdk.i.m> r2 = com.purplebrain.adbuddiz.sdk.i.m.class
            monitor-enter(r2)
            java.io.FileInputStream r3 = r4.openFileInput(r5)     // Catch:{ FileNotFoundException -> 0x0011, IOException -> 0x0014, all -> 0x0017 }
            java.lang.String r0 = com.purplebrain.adbuddiz.sdk.i.s.m15472(r3)     // Catch:{ FileNotFoundException -> 0x0011, IOException -> 0x0014, all -> 0x0017 }
            r3.close()     // Catch:{ FileNotFoundException -> 0x0011, IOException -> 0x0014, all -> 0x0017 }
        L_0x000f:
            monitor-exit(r2)
            return r0
        L_0x0011:
            r0 = move-exception
            r0 = r1
            goto L_0x000f
        L_0x0014:
            r0 = move-exception
            r0 = r1
            goto L_0x000f
        L_0x0017:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.i.m.m15431(android.content.Context, java.lang.String):java.lang.String");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static InputStream m15432(Context context, String str) {
        return context.openFileInput(m15444(str));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m15433(c cVar) {
        return cVar.f12003.e ? "ABZ_31_" : m15442(cVar.f12006.m15161()) + cVar.m15180() + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m15434(Context context, c cVar) {
        File fileStreamPath = context.getFileStreamPath(m15441(cVar));
        if (fileStreamPath.exists()) {
            fileStreamPath.setLastModified(new Date().getTime());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static synchronized void m15435(Context context, String str, String str2) {
        synchronized (m.class) {
            try {
                FileOutputStream openFileOutput = context.openFileOutput(str, 0);
                openFileOutput.flush();
                openFileOutput.write(str2.getBytes());
                openFileOutput.close();
            } catch (IOException e) {
                o.m15453("writeConfig() IOException", e);
            }
        }
        return;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static void m15436(Context context, String str) {
        context.deleteFile("ABZ_31_" + str + ".abz");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static InputStream m15437(Context context, c cVar) {
        return context.openFileInput(m15441(cVar));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m15438(Context context, String str) {
        return m15431(context, "ABZ_31_" + str + ".abz");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static String m15439(c cVar) {
        return m15433(cVar) + cVar.m15175() + "_tmp";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15440(Context context, c cVar) {
        return context.getFileStreamPath(m15441(cVar)).getAbsolutePath();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15441(c cVar) {
        return m15433(cVar) + cVar.m15175();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15442(e eVar) {
        return "ABZ_31_" + eVar.e + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15443(File file, e eVar) {
        return file.getName().substring(m15442(eVar).length());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15444(String str) {
        return "ABZ_31_" + str + ".abz";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List m15445(Context context, e eVar, boolean z) {
        ArrayList arrayList = new ArrayList();
        String r2 = m15442(eVar);
        for (String str : context.fileList()) {
            if (str.startsWith(r2) && (z || (!z && !str.endsWith("_tmp")))) {
                arrayList.add(context.getFileStreamPath(str));
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15446(Context context, InputStream inputStream, c cVar) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(m15439(cVar), 0);
            s.m15471(inputStream, openFileOutput);
            openFileOutput.flush();
            openFileOutput.close();
            context.getFileStreamPath(m15439(cVar)).renameTo(context.getFileStreamPath(m15441(cVar)));
        } catch (IOException e) {
            o.m15453("writeAdResource() IOException", e);
            context.getFileStreamPath(m15439(cVar)).delete();
            context.getFileStreamPath(m15441(cVar)).delete();
            throw e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15447(Context context, String str, String str2) {
        m15435(context, "ABZ_31_" + str + ".abz", str2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15448(Context context, String str) {
        return context.getFileStreamPath(m15444(str)).exists();
    }
}
