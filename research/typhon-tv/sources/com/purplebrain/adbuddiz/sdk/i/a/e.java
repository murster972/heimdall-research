package com.purplebrain.adbuddiz.sdk.i.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.f.a;
import com.purplebrain.adbuddiz.sdk.f.b;
import com.purplebrain.adbuddiz.sdk.i.a.g;
import com.purplebrain.adbuddiz.sdk.i.n;
import java.util.Date;
import net.pubnative.library.request.PubnativeRequest;

public final class e {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m15319(final Context context) {
        boolean z = true;
        Long valueOf = Long.valueOf(context.getSharedPreferences("ABZ-prefs", 0).getLong("aif", -1));
        if (valueOf.longValue() != -1 && new Date().getTime() - valueOf.longValue() <= 86400000 && new Date().getTime() >= valueOf.longValue()) {
            z = false;
        }
        if (z) {
            new Thread(new Runnable() {
                public final void run() {
                    String str = null;
                    boolean z = false;
                    try {
                        g.a r2 = g.m15326(context);
                        str = r2.f12191;
                        z = r2.f12190;
                    } catch (Throwable th) {
                    }
                    try {
                        SharedPreferences sharedPreferences = AdBuddiz.m14865().getSharedPreferences("ABZ-prefs", 0);
                        String string = sharedPreferences.getString("ai", (String) null);
                        boolean z2 = sharedPreferences.getBoolean(PubnativeRequest.Parameters.LAT, false);
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        if ((string == null && str != null) || ((string != null && !string.equals(str)) || z2 != z)) {
                            edit.putString("ai", str);
                            edit.putBoolean(PubnativeRequest.Parameters.LAT, z);
                        }
                        edit.putLong("aif", new Date().getTime());
                        edit.commit();
                    } catch (Throwable th2) {
                    }
                }
            }).start();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static String m15320(Context context) {
        b r1 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
        if (r1 == null || !r1.m15226(a.IMEI) || !h.m15332(context, "android.permission.READ_PHONE_STATE")) {
            return null;
        }
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m15321(Context context) {
        String r0 = m15323(context);
        if (r0 == null) {
            return null;
        }
        return n.m15449("MD5", r0);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m15322(Context context) {
        String r0 = m15320(context);
        if (r0 == null) {
            return null;
        }
        return n.m15449("MD5", r0);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m15323(Context context) {
        b r0 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
        if (r0 == null || !r0.m15226(a.MacAddress)) {
            return null;
        }
        if (!h.m15332(context, "android.permission.ACCESS_WIFI_STATE")) {
            return null;
        }
        WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
        if (connectionInfo == null) {
            return null;
        }
        return connectionInfo.getMacAddress();
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15324(Context context) {
        b r0 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
        if (r0 == null || !r0.m15226(a.AndroidId)) {
            return null;
        }
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }
}
