package com.purplebrain.adbuddiz.sdk.i.a;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import com.purplebrain.adbuddiz.sdk.f.a;
import com.purplebrain.adbuddiz.sdk.f.b;

public final class f {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Location m15325(Context context) {
        Location location = null;
        b r0 = com.purplebrain.adbuddiz.sdk.e.b.m15135().m15138();
        if (r0 != null && r0.m15226(a.Geolocation)) {
            boolean r02 = h.m15332(context, "android.permission.ACCESS_COARSE_LOCATION");
            boolean r1 = h.m15332(context, "android.permission.ACCESS_FINE_LOCATION");
            if (r02 || r1) {
                LocationManager locationManager = (LocationManager) context.getSystemService("location");
                for (String lastKnownLocation : locationManager.getAllProviders()) {
                    try {
                        Location lastKnownLocation2 = locationManager.getLastKnownLocation(lastKnownLocation);
                        if (lastKnownLocation2 == null || (location != null && lastKnownLocation2.getTime() <= location.getTime())) {
                            lastKnownLocation2 = location;
                        }
                        location = lastKnownLocation2;
                    } catch (SecurityException e) {
                    }
                }
            }
        }
        return location;
    }
}
