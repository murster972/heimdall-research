package com.purplebrain.adbuddiz.sdk.i.a;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.purplebrain.adbuddiz.sdk.i.o;

public final class h {
    /* renamed from: 靐  reason: contains not printable characters */
    public static int m15329(Context context) {
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle == null) {
                return 150;
            }
            return bundle.getInt("ADBUDDIZ_BACKGROUND_ALPHA", 150);
        } catch (PackageManager.NameNotFoundException | RuntimeException e) {
            return 150;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m15330(Context context) {
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            return bundle != null && bundle.getBoolean("ADBUDDIZ_TEST_MODE", false);
        } catch (PackageManager.NameNotFoundException e) {
            o.m15451("Failed to load meta-data, ADBUDDIZ_TEST_MODE not found in AndroidManifest.xml");
            return false;
        } catch (RuntimeException e2) {
            o.m15451("Failed to load meta-data, ADBUDDIZ_TEST_MODE not found in AndroidManifest.xml");
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m15331(Context context) {
        String str;
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle == null || (str = bundle.getString("ADBUDDIZ_PUBLISHER_KEY")) == null || str.length() <= 0) {
                str = null;
            }
            return str;
        } catch (PackageManager.NameNotFoundException e) {
            o.m15451("Failed to load ApplicationInfo");
            return null;
        } catch (RuntimeException e2) {
            o.m15451("Failed to load ApplicationInfo");
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15332(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }
}
