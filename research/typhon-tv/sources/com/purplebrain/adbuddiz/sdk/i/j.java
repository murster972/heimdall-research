package com.purplebrain.adbuddiz.sdk.i;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.i.a.a;
import com.purplebrain.adbuddiz.sdk.i.a.k;
import java.lang.reflect.Method;
import java.util.Stack;

@SuppressLint({"NewApi"})
public class j {
    /* renamed from: ʻ  reason: contains not printable characters */
    public static Integer m15419(Context context) {
        if ((context instanceof Activity) && Build.VERSION.SDK_INT >= 21) {
            return Integer.valueOf(((Activity) context).getWindow().getStatusBarColor());
        }
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static Integer m15420(Context context) {
        if ((context instanceof Activity) && Build.VERSION.SDK_INT >= 21) {
            return Integer.valueOf(((Activity) context).getWindow().getNavigationBarColor());
        }
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static Point m15421(Context context) {
        Point point;
        RectF rectF = new RectF(1.0f, 1.0f, 1.0f, 1.0f);
        Point point2 = new Point();
        if (Build.VERSION.SDK_INT < 19 || !m15422(context)) {
            DisplayMetrics displayMetrics = AdBuddiz.m14865().getResources().getDisplayMetrics();
            point2.x = displayMetrics.widthPixels;
            point2.y = displayMetrics.heightPixels;
            point = point2;
        } else {
            ((DisplayManager) AdBuddiz.m14865().getSystemService("display")).getDisplay(0).getRealSize(point2);
            point = point2;
        }
        Rect r1 = a.m15312(context, rectF);
        int r2 = k.m15340(context, 8);
        int r3 = k.m15340(context, 13);
        int r4 = k.m15340(context, 25);
        point.x -= (r1.right + r1.left) + r2;
        point.y -= (((r1.bottom + r1.top) + r2) + r3) + r4;
        return point;
    }

    @SuppressLint({"InlinedApi"})
    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean m15422(Context context) {
        try {
            return (m15424(context) & 2048) != 0;
        } catch (RuntimeException e) {
            return context.getSharedPreferences("ABZ-prefs", 0).getBoolean("im", false);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m15423(Context context) {
        if (!(context instanceof Activity)) {
            return true;
        }
        return (((Activity) AdBuddiz.m14865()).getWindow().getAttributes().flags & 1024) != 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static int m15424(Context context) {
        if (!(context instanceof Activity) || Build.VERSION.SDK_INT < 11) {
            return 0;
        }
        Stack stack = new Stack();
        stack.add(((Activity) AdBuddiz.m14865()).getWindow().getDecorView());
        int i = 0;
        while (!stack.isEmpty() && i == 0) {
            View view = (View) stack.pop();
            int systemUiVisibility = view.getSystemUiVisibility();
            if (systemUiVisibility == 0 && (view instanceof ViewGroup)) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                    stack.add(viewGroup.getChildAt(i2));
                }
            }
            i = systemUiVisibility;
        }
        return i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m15425(Context context) {
        int i;
        if (!(context instanceof Activity)) {
            return true;
        }
        Window window = ((Activity) AdBuddiz.m14865()).getWindow();
        if (Build.VERSION.SDK_INT >= 11) {
            return window.hasFeature(1) || window.hasFeature(8);
        }
        try {
            Method declaredMethod = window.getClass().getSuperclass().getDeclaredMethod("getFeatures", new Class[0]);
            declaredMethod.setAccessible(true);
            i = ((Integer) declaredMethod.invoke(window, new Object[0])).intValue();
            try {
                declaredMethod.setAccessible(false);
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            i = 0;
        }
        return (i & 2) != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15426(final Context context) {
        if (Build.VERSION.SDK_INT >= 19) {
            AnonymousClass1 r0 = new Runnable() {
                public final void run() {
                    try {
                        Context context = context;
                        boolean r2 = j.m15422(context);
                        SharedPreferences.Editor edit = context.getSharedPreferences("ABZ-prefs", 0).edit();
                        edit.putBoolean("im", r2);
                        edit.commit();
                    } catch (Throwable th) {
                        l.m15430("ABDisplayHelper.updateImmersiveModeFlag", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                        o.m15453("ABDisplayHelper.updateImmersiveModeFlag", th);
                    }
                }
            };
            if (!Looper.getMainLooper().equals(Looper.myLooper())) {
                new Handler(Looper.getMainLooper()).post(r0);
            } else {
                r0.run();
            }
        }
    }
}
