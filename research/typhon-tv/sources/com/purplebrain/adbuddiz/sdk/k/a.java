package com.purplebrain.adbuddiz.sdk.k;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.m;
import com.purplebrain.adbuddiz.sdk.i.o;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public final class a {

    /* renamed from: 连任  reason: contains not printable characters */
    private static boolean f12307 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    public com.purplebrain.adbuddiz.sdk.f.a.c f12308;

    /* renamed from: 麤  reason: contains not printable characters */
    public c f12309;

    /* renamed from: 齉  reason: contains not printable characters */
    public WebView f12310;

    /* renamed from: 龘  reason: contains not printable characters */
    public Context f12311;

    /* renamed from: com.purplebrain.adbuddiz.sdk.k.a$a  reason: collision with other inner class name */
    private class C0033a extends WebChromeClient {
        private C0033a() {
        }

        public /* synthetic */ C0033a(a aVar, byte b) {
            this();
        }

        @SuppressLint({"NewApi"})
        public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            return true;
        }
    }

    private class b extends WebViewClient {
        private b() {
        }

        public /* synthetic */ b(a aVar, byte b) {
            this();
        }

        @SuppressLint({"NewApi"})
        public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            if (!str.startsWith("adbuddiz")) {
                return null;
            }
            new Object[1][0] = str;
            try {
                String path = Uri.parse(str).getPath();
                if (path.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                    path = path.substring(1);
                }
                com.purplebrain.adbuddiz.sdk.f.a.a.c r1 = com.purplebrain.adbuddiz.sdk.i.c.m15374((com.purplebrain.adbuddiz.sdk.f.a.a) a.this.f12308, path);
                if (r1 != null) {
                    return new WebResourceResponse("application/octet-stream", (String) null, m.m15437(a.this.f12311, r1));
                }
                throw new IOException(String.format("Resource '%s' is not in the list of additional resources.", new Object[]{str}));
            } catch (Throwable th) {
                l.m15430("ABHTMLEndInterstitialController#ABWebViewClient.shouldInterceptRequest", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                o.m15453("ABHTMLEndInterstitialController#ABWebViewClient.shouldInterceptRequest", th);
                return new WebResourceResponse("application/octet-stream", "UTF-8", new ByteArrayInputStream(new byte[0]));
            }
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            try {
                if ("adbuddiz:///click".equals(str)) {
                    a.this.f12309.m15559();
                    return true;
                }
            } catch (Throwable th) {
                l.m15430("ABHTMLEndInterstitialController#ABWebViewClient.shouldOverrideUrlLoading", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                o.m15453("ABHTMLEndInterstitialController#ABWebViewClient.shouldOverrideUrlLoading()", th);
            }
            return false;
        }
    }

    public interface c {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15559();
    }

    public a(Context context, com.purplebrain.adbuddiz.sdk.f.a.c cVar) {
        this.f12311 = context;
        this.f12308 = cVar;
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15557() {
        if (f12307 && Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    @SuppressLint({"SetJavaScriptEnabled", "NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15558(WebView webView) {
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAppCacheEnabled(false);
        settings.setCacheMode(2);
    }
}
