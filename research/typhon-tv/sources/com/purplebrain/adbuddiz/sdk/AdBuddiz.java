package com.purplebrain.adbuddiz.sdk;

import android.app.Activity;
import android.content.Context;
import com.purplebrain.adbuddiz.sdk.c.a;
import com.purplebrain.adbuddiz.sdk.c.b;
import com.purplebrain.adbuddiz.sdk.f.d;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.q;

public final class AdBuddiz {

    public static class RewardedVideo {
        /* renamed from: 龘  reason: contains not printable characters */
        public static AdBuddizRewardedVideoDelegate m14869() {
            return b.m15011().f11893;
        }
    }

    private AdBuddiz() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static AdBuddizDelegate m14859() {
        return a.m15004().f11877;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static synchronized boolean m14860(Activity activity) {
        boolean r0;
        synchronized (AdBuddiz.class) {
            r0 = a.m15004().m15010((Context) activity, "Default");
        }
        return r0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static String m14861() {
        return "Java";
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static void m14862(Activity activity) {
        a.m15004().f11879 = activity;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m14863() {
        return "3.1.11";
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static synchronized void m14864(Activity activity) {
        synchronized (AdBuddiz.class) {
            a.m15004().m15008(activity, "Default");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Context m14865() {
        return a.m15004().f11879;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized void m14866(Activity activity) {
        synchronized (AdBuddiz.class) {
            a.m15004().m15009((Context) activity);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m14867(AdBuddizDelegate adBuddizDelegate) {
        a.m15004().f11877 = adBuddizDelegate;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized void m14868(String str) {
        synchronized (AdBuddiz.class) {
            try {
                q.m15467(str);
            } catch (Throwable th) {
                l.m15428(d.a.SET_PUBLISHER_KEY, th);
                o.m15453("AdBuddiz.setPublisherKey() Exception : ", th);
            }
        }
        return;
    }
}
