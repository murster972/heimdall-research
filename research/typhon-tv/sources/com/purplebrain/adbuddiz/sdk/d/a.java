package com.purplebrain.adbuddiz.sdk.d;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v4.internal.view.SupportMenu;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.purplebrain.adbuddiz.sdk.i.a.k;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.o;

@SuppressLint({"NewApi"})
public final class a extends ViewGroup {

    /* renamed from: ʻ  reason: contains not printable characters */
    public Double f11897;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private Rect f11898 = new Rect();

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0028a f11899 = null;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f11900;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private boolean f11901 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f11902;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Drawable f11903 = null;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f11904;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f11905;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f11906;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f11907;

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f11908 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private TextPaint f11909 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f11910;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f11911;

    /* renamed from: י  reason: contains not printable characters */
    private int f11912;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f11913;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f11914;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f11915;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private int f11916;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f11917;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private boolean f11918;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private Integer f11919;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f11920;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private Rect f11921 = new Rect();

    /* renamed from: ⁱ  reason: contains not printable characters */
    private RectF f11922 = new RectF();

    /* renamed from: 连任  reason: contains not printable characters */
    public Paint f11923;

    /* renamed from: 靐  reason: contains not printable characters */
    public Drawable f11924 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    public int f11925;

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f11926;

    /* renamed from: 龘  reason: contains not printable characters */
    public Drawable f11927 = null;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private int f11928;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private Drawable f11929 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private Drawable f11930 = null;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private int f11931;

    /* renamed from: com.purplebrain.adbuddiz.sdk.d.a$a  reason: collision with other inner class name */
    public interface C0028a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15033();
    }

    public a(Context context) {
        super(context);
        setClipToPadding(false);
        setDrawingCacheEnabled(true);
        setClickable(true);
        this.f11897 = null;
        this.f11918 = false;
        this.f11911 = k.m15340(getContext(), 2);
        this.f11900 = -1;
        this.f11915 = k.m15340(getContext(), 3);
        this.f11914 = -16777216;
        this.f11905 = k.m15340(getContext(), 15);
        this.f11902 = k.m15340(getContext(), 12);
        GradientDrawable r0 = m15020();
        r0.setStroke(this.f11915 + this.f11911, this.f11914);
        this.f11903 = r0;
        GradientDrawable r02 = m15020();
        r02.setStroke(this.f11911, this.f11900);
        this.f11929 = r02;
        this.f11920 = k.m15340(getContext(), 45);
        this.f11904 = k.m15340(getContext(), -18);
        this.f11906 = k.m15340(getContext(), -2);
        this.f11912 = k.m15339(getContext(), 5.3333335f);
        this.f11913 = k.m15339(getContext(), 4.6666665f);
        this.f11910 = k.m15339(getContext(), 8.0f);
        this.f11917 = k.m15339(getContext(), 8.0f);
        TextPaint textPaint = new TextPaint();
        textPaint.setColor(-1);
        textPaint.setTypeface(Typeface.DEFAULT);
        textPaint.setTextAlign(Paint.Align.CENTER);
        this.f11909 = textPaint;
        this.f11907 = (int) TypedValue.applyDimension(2, 16.0f, getContext().getResources().getDisplayMetrics());
        this.f11926 = true;
        this.f11925 = 0;
        this.f11923 = m15027();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m15014() {
        return getPaddingLeft() + this.f11916;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m15015() {
        int height;
        if (this.f11927 == null || (height = (this.f11927.getBounds().height() / 2) + this.f11906) < 0) {
            return 0;
        }
        return height;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private int m15016() {
        return (m15021() / 2) + m15015();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m15017() {
        return m15014() + (m15021() / 2) + getChildAt(0).getMeasuredWidth() + this.f11904;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private int m15018() {
        return m15019() + m15016() + this.f11906;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private int m15019() {
        return getPaddingTop() + this.f11928;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private GradientDrawable m15020() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setGradientType(0);
        gradientDrawable.setColor(0);
        gradientDrawable.setCornerRadii(new float[]{(float) this.f11905, (float) this.f11905, (float) this.f11905, (float) this.f11905, (float) this.f11905, (float) this.f11905, (float) this.f11905, (float) this.f11905});
        return gradientDrawable;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private int m15021() {
        return (this.f11915 + this.f11911) * 2;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private Path m15022() {
        View childAt = getChildAt(0);
        Path path = new Path();
        RectF rectF = new RectF();
        rectF.left = (float) (m15014() + (m15021() / 2));
        rectF.top = (float) (m15019() + m15016());
        rectF.right = rectF.left + ((float) childAt.getMeasuredWidth());
        rectF.bottom = ((float) childAt.getMeasuredHeight()) + rectF.top;
        path.addRoundRect(rectF, new float[]{(float) this.f11902, (float) this.f11902, (float) this.f11902, (float) this.f11902, (float) this.f11902, (float) this.f11902, (float) this.f11902, (float) this.f11902}, Path.Direction.CW);
        return path;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15023(Canvas canvas, String str) {
        int width = (this.f11924.getBounds().width() - this.f11912) - this.f11910;
        int height = (this.f11924.getBounds().height() - this.f11913) - this.f11917;
        this.f11924.draw(canvas);
        Rect rect = new Rect();
        m15024(str, this.f11909, this.f11907, width, height);
        float descent = ((this.f11909.descent() - this.f11909.ascent()) / 2.0f) - this.f11909.descent();
        this.f11909.getTextBounds(str, 0, str.length(), rect);
        canvas.translate((float) (this.f11912 + (width / 2)), descent + ((float) (this.f11913 + (height / 2))));
        canvas.drawText(str, 0.0f, 0.0f, this.f11909);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15024(String str, TextPaint textPaint, int i, int i2, int i3) {
        if (this.f11908 == null || this.f11908.length() != str.length()) {
            this.f11908 = "9";
            for (int i4 = 1; i4 < str.length(); i4++) {
                this.f11908 += "9";
            }
            Rect rect = new Rect();
            while (true) {
                textPaint.setTextSize((float) i);
                textPaint.getTextBounds(this.f11908, 0, this.f11908.length(), rect);
                i--;
                if (i <= 0) {
                    return;
                }
                if (rect.width() <= i2 && rect.height() <= i3) {
                    return;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m15025(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 0) {
            double sqrt = Math.sqrt(Math.pow((double) (motionEvent.getX() - ((float) m15017())), 2.0d) + Math.pow((double) (motionEvent.getY() - ((float) m15018())), 2.0d));
            if (action == 0) {
                if (sqrt < ((double) this.f11920)) {
                    this.f11901 = true;
                    return true;
                }
                this.f11901 = false;
            } else if (action == 1) {
                if (!this.f11901 || sqrt >= ((double) this.f11920)) {
                    this.f11901 = false;
                } else if (!this.f11926) {
                    return true;
                } else {
                    playSoundEffect(0);
                    if (this.f11899 == null) {
                        return true;
                    }
                    this.f11899.m15033();
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        try {
            if (getChildCount() > 0) {
                canvas.save();
                if (Build.VERSION.SDK_INT >= 18 && canvas.isHardwareAccelerated()) {
                    canvas.clipPath(m15022());
                }
                if (this.f11923 != null) {
                    canvas.drawPath(m15022(), this.f11923);
                }
                super.dispatchDraw(canvas);
                canvas.restore();
                m15028(this.f11898);
                canvas.save();
                canvas.translate((float) this.f11898.left, (float) this.f11898.top);
                this.f11903.setBounds(0, 0, this.f11898.width(), this.f11898.height());
                this.f11903.draw(canvas);
                this.f11929.setBounds(0, 0, this.f11898.width(), this.f11898.height());
                this.f11929.draw(canvas);
                canvas.translate((float) this.f11911, (float) this.f11911);
                canvas.restore();
                if (this.f11924 != null) {
                    canvas.save();
                    canvas.translate((float) (m15017() - (this.f11924.getBounds().width() / 2)), (float) (m15018() - (this.f11924.getBounds().height() / 2)));
                    if (this.f11926) {
                        if (this.f11927 != null) {
                            this.f11927.draw(canvas);
                        } else {
                            m15023(canvas, "X");
                        }
                    } else if (this.f11919 != null && !this.f11919.equals(0)) {
                        m15023(canvas, Integer.toString(this.f11919.intValue()));
                    } else if (this.f11930 != null) {
                        this.f11930.draw(canvas);
                    } else {
                        m15023(canvas, "…");
                    }
                    canvas.restore();
                }
                if (this.f11918) {
                    Paint paint = new Paint();
                    paint.setColor(SupportMenu.CATEGORY_MASK);
                    paint.setAlpha(127);
                    canvas.drawCircle((float) m15017(), (float) m15018(), (float) this.f11920, paint);
                }
            }
        } catch (Throwable th) {
            l.m15430("ABInterstitialAdLayout.dispatchDraw", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
            o.m15453("ABInterstitialLayout.dispatchDraw()", th);
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        try {
            return m15025(motionEvent);
        } catch (Throwable th) {
            l.m15430("ABIntersitialAdLayout.onInterceptTouchEvent", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
            o.m15453("ABInterstitialLayout.onInterceptTouchEvent()", th);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        try {
            if (getChildCount() != 0) {
                int r1 = i + m15014() + (m15021() / 2);
                int r2 = i2 + m15019() + m15016();
                View childAt = getChildAt(0);
                childAt.layout(r1, r2, childAt.getMeasuredWidth() + r1, childAt.getMeasuredHeight() + r2);
                for (int i5 = 1; i5 < getChildCount(); i5++) {
                    getChildAt(i5).layout(r1, (childAt.getMeasuredHeight() + r2) - getChildAt(i5).getMeasuredHeight(), childAt.getMeasuredWidth() + r1, childAt.getMeasuredHeight() + r2);
                }
            }
        } catch (Throwable th) {
            o.m15453("ABInterstitialLayout.onLayout()", th);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        try {
            int size = View.MeasureSpec.getSize(i);
            int mode = View.MeasureSpec.getMode(i);
            int size2 = View.MeasureSpec.getSize(i2);
            int mode2 = View.MeasureSpec.getMode(i2);
            m15026(this.f11921);
            m15029(this.f11922, size, size2, this.f11921, false);
            int width = this.f11921.width();
            int height = this.f11921.height();
            double width2 = (double) this.f11922.width();
            double height2 = (double) this.f11922.height();
            if (getChildCount() != 0) {
                View childAt = getChildAt(0);
                childAt.measure(View.MeasureSpec.makeMeasureSpec((int) width2, Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec((int) height2, Integer.MIN_VALUE));
                double min = Math.min(width2 / ((double) childAt.getMeasuredWidth()), height2 / ((double) childAt.getMeasuredHeight()));
                i = childAt.getMeasuredWidth() != 0 ? View.MeasureSpec.makeMeasureSpec((int) Math.floor(((double) childAt.getMeasuredWidth()) * min), 1073741824) : View.MeasureSpec.makeMeasureSpec((int) Math.floor(width2 * min), 1073741824);
                i2 = childAt.getMeasuredHeight() != 0 ? View.MeasureSpec.makeMeasureSpec((int) Math.floor(((double) childAt.getMeasuredHeight()) * min), 1073741824) : View.MeasureSpec.makeMeasureSpec((int) Math.floor(height2 * min), 1073741824);
                childAt.measure(i, i2);
                for (int i3 = 1; i3 < getChildCount(); i3++) {
                    getChildAt(i3).measure(View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i), 1073741824), View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i2), Integer.MIN_VALUE));
                }
                if (mode2 == 1073741824) {
                    i2 = View.MeasureSpec.makeMeasureSpec(size2, 1073741824);
                    this.f11928 = ((size2 - childAt.getMeasuredHeight()) - height) / 2;
                } else {
                    i2 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i2) + height, 1073741824);
                    this.f11928 = 0;
                }
                if (mode == 1073741824) {
                    i = View.MeasureSpec.makeMeasureSpec(size, 1073741824);
                    this.f11931 = ((size - childAt.getMeasuredWidth()) - width) / 2;
                    this.f11916 = this.f11931;
                } else {
                    i = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i) + width, 1073741824);
                    this.f11931 = 0;
                    this.f11916 = 0;
                }
            }
        } catch (Throwable th) {
            l.m15430("ABInterstitialAdLayout.onMeasure", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
            o.m15453("ABInterstitialLayout.onMeasure()", th);
        }
        super.onMeasure(i, i2);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            return m15025(motionEvent);
        } catch (Throwable th) {
            l.m15430("ABInterstitialAdLayout.onTouchEvent", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
            o.m15453("ABIntersitialAdLayout.onTouchEvent()", th);
            return super.onTouchEvent(motionEvent);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m15026(Rect rect) {
        int i;
        int r2 = (m15021() / 2) + getPaddingLeft() + getPaddingRight();
        int r3 = m15021() / 2;
        if (this.f11927 != null) {
            i = (this.f11927.getBounds().width() / 2) + this.f11904;
            if (i < 0) {
                i = 0;
            }
        } else {
            i = 0;
        }
        rect.left = 0;
        rect.top = 0;
        rect.right = i + r3 + r2;
        rect.bottom = getPaddingTop() + getPaddingBottom() + m15016() + (m15021() / 2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Paint m15027() {
        if (this.f11925 == 0) {
            return null;
        }
        Paint paint = new Paint();
        paint.setColor(this.f11925);
        return paint;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15028(Rect rect) {
        View childAt = getChildAt(0);
        rect.left = m15014();
        rect.top = m15019() + m15015();
        rect.right = rect.left + childAt.getMeasuredWidth() + m15021();
        rect.bottom = childAt.getMeasuredHeight() + rect.top + m15021();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15029(RectF rectF, int i, int i2, Rect rect, boolean z) {
        double width = (double) (i - rect.width());
        double height = (double) (i2 - rect.height());
        if (!z && this.f11897 != null) {
            if (width / this.f11897.doubleValue() < height) {
                height = Math.floor(width / this.f11897.doubleValue());
            }
            if (this.f11897.doubleValue() * height < width) {
                width = Math.floor(this.f11897.doubleValue() * height);
            }
        }
        rectF.left = 0.0f;
        rectF.top = 0.0f;
        rectF.right = (float) width;
        rectF.bottom = (float) height;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15030(Double d) {
        if (this.f11897 != d) {
            this.f11897 = d;
            requestLayout();
            invalidate();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15031(Integer num) {
        this.f11919 = num;
        if (!this.f11926) {
            invalidate();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15032(boolean z) {
        if (this.f11926 != z) {
            this.f11926 = z;
            invalidate();
        }
    }
}
