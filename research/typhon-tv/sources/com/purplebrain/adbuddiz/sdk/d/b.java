package com.purplebrain.adbuddiz.sdk.d;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.widget.RelativeLayout;
import com.purplebrain.adbuddiz.sdk.i.a.k;

@SuppressLint({"NewApi"})
public final class b extends RelativeLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f11932 = true;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f11933;

    /* renamed from: 连任  reason: contains not printable characters */
    boolean f11934 = true;

    /* renamed from: 靐  reason: contains not printable characters */
    int f11935;

    /* renamed from: 麤  reason: contains not printable characters */
    Bitmap f11936 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    int f11937;

    /* renamed from: 龘  reason: contains not printable characters */
    public Paint f11938 = null;

    public b(Context context) {
        super(context);
        setClickable(true);
        this.f11933 = k.m15340(getContext(), 15);
        this.f11938 = new Paint();
        this.f11938.setColor(-16777216);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15034(Canvas canvas) {
        if (this.f11932) {
            Path path = new Path();
            Rect rect = new Rect();
            ((a) getChildAt(0)).m15028(rect);
            RectF rectF = new RectF();
            rectF.left = (float) rect.left;
            rectF.top = (float) rect.top;
            rectF.right = (float) rect.right;
            rectF.bottom = (float) rect.bottom;
            path.addRoundRect(rectF, new float[]{(float) this.f11933, (float) this.f11933, (float) this.f11933, (float) this.f11933, (float) this.f11933, (float) this.f11933, (float) this.f11933, (float) this.f11933}, Path.Direction.CCW);
            canvas.clipPath(path, Region.Op.DIFFERENCE);
        }
        canvas.drawColor(this.f11938.getColor());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005d, code lost:
        if (r7.f11934 != false) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void dispatchDraw(android.graphics.Canvas r8) {
        /*
            r7 = this;
            r6 = 0
            r8.save()     // Catch:{ Throwable -> 0x0031 }
            boolean r0 = r7.f11932     // Catch:{ Throwable -> 0x0031 }
            if (r0 != 0) goto L_0x0021
            r1 = 0
            r2 = 0
            int r0 = r7.getWidth()     // Catch:{ Throwable -> 0x0031 }
            float r3 = (float) r0     // Catch:{ Throwable -> 0x0031 }
            int r0 = r7.getHeight()     // Catch:{ Throwable -> 0x0031 }
            float r4 = (float) r0     // Catch:{ Throwable -> 0x0031 }
            android.graphics.Paint r5 = r7.f11938     // Catch:{ Throwable -> 0x0031 }
            r0 = r8
            r0.drawRect(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0031 }
        L_0x001a:
            r8.restore()     // Catch:{ Throwable -> 0x0031 }
            super.dispatchDraw(r8)     // Catch:{ Throwable -> 0x0031 }
        L_0x0020:
            return
        L_0x0021:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x0031 }
            r1 = 18
            if (r0 < r1) goto L_0x003f
            boolean r0 = r8.isHardwareAccelerated()     // Catch:{ Throwable -> 0x0031 }
            if (r0 == 0) goto L_0x003f
            r7.m15034(r8)     // Catch:{ Throwable -> 0x0031 }
            goto L_0x001a
        L_0x0031:
            r0 = move-exception
            java.lang.String r1 = "ABInterstitialParentLayout.dispatchDraw"
            com.purplebrain.adbuddiz.sdk.i.l.m15430((java.lang.String) r1, (com.purplebrain.adbuddiz.sdk.f.a.a) r6, (java.lang.Throwable) r0)
            java.lang.String r1 = "ABInterstitialParentLayout.dispatchDraw()"
            com.purplebrain.adbuddiz.sdk.i.o.m15453(r1, r0)
            goto L_0x0020
        L_0x003f:
            int r0 = r7.getMeasuredWidth()     // Catch:{ Throwable -> 0x0031 }
            int r1 = r7.getMeasuredHeight()     // Catch:{ Throwable -> 0x0031 }
            android.graphics.Bitmap r2 = r7.f11936     // Catch:{ Throwable -> 0x0031 }
            if (r2 == 0) goto L_0x005f
            int r2 = r7.f11935     // Catch:{ Throwable -> 0x0031 }
            if (r0 != r2) goto L_0x0053
            int r2 = r7.f11937     // Catch:{ Throwable -> 0x0031 }
            if (r1 == r2) goto L_0x005b
        L_0x0053:
            android.graphics.Bitmap r2 = r7.f11936     // Catch:{ Throwable -> 0x0031 }
            r2.recycle()     // Catch:{ Throwable -> 0x0031 }
            r2 = 0
            r7.f11936 = r2     // Catch:{ Throwable -> 0x0031 }
        L_0x005b:
            boolean r2 = r7.f11934     // Catch:{ Throwable -> 0x0031 }
            if (r2 == 0) goto L_0x0084
        L_0x005f:
            boolean r2 = r7.f11934     // Catch:{ Throwable -> 0x0031 }
            if (r2 != 0) goto L_0x0067
            android.graphics.Bitmap r2 = r7.f11936     // Catch:{ Throwable -> 0x0031 }
            if (r2 != 0) goto L_0x0084
        L_0x0067:
            android.graphics.Bitmap r2 = r7.f11936     // Catch:{ Throwable -> 0x0031 }
            if (r2 != 0) goto L_0x0077
            r7.f11935 = r0     // Catch:{ Throwable -> 0x0031 }
            r7.f11937 = r1     // Catch:{ Throwable -> 0x0031 }
            android.graphics.Bitmap$Config r2 = android.graphics.Bitmap.Config.ALPHA_8     // Catch:{ Throwable -> 0x0031 }
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r0, r1, r2)     // Catch:{ Throwable -> 0x0031 }
            r7.f11936 = r0     // Catch:{ Throwable -> 0x0031 }
        L_0x0077:
            android.graphics.Canvas r0 = new android.graphics.Canvas     // Catch:{ Throwable -> 0x0031 }
            android.graphics.Bitmap r1 = r7.f11936     // Catch:{ Throwable -> 0x0031 }
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0031 }
            r7.m15034(r0)     // Catch:{ Throwable -> 0x0031 }
            r0 = 0
            r7.f11934 = r0     // Catch:{ Throwable -> 0x0031 }
        L_0x0084:
            android.graphics.Bitmap r0 = r7.f11936     // Catch:{ Throwable -> 0x0031 }
            r1 = 0
            r2 = 0
            android.graphics.Paint r3 = r7.f11938     // Catch:{ Throwable -> 0x0031 }
            r8.drawBitmap(r0, r1, r2, r3)     // Catch:{ Throwable -> 0x0031 }
            r0 = 0
            r7.f11934 = r0     // Catch:{ Throwable -> 0x0031 }
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.d.b.dispatchDraw(android.graphics.Canvas):void");
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.f11936 != null) {
            this.f11934 = true;
        }
    }
}
