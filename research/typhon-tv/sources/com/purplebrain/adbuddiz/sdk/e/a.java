package com.purplebrain.adbuddiz.sdk.e;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.OrientationEventListener;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import com.purplebrain.adbuddiz.sdk.b.h;
import com.purplebrain.adbuddiz.sdk.b.k;
import com.purplebrain.adbuddiz.sdk.b.p;
import com.purplebrain.adbuddiz.sdk.b.s;
import com.purplebrain.adbuddiz.sdk.e.b;
import com.purplebrain.adbuddiz.sdk.e.c;
import com.purplebrain.adbuddiz.sdk.f.a.f;
import com.purplebrain.adbuddiz.sdk.f.d;
import com.purplebrain.adbuddiz.sdk.f.e;
import com.purplebrain.adbuddiz.sdk.h.b;
import com.purplebrain.adbuddiz.sdk.h.g;
import com.purplebrain.adbuddiz.sdk.h.i;
import com.purplebrain.adbuddiz.sdk.i.a.d;
import com.purplebrain.adbuddiz.sdk.i.a.j;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.m;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.r;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: ʽ  reason: contains not printable characters */
    private static a f11939 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public List f11940;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public OrientationEventListener f11941;

    /* renamed from: ʾ  reason: contains not printable characters */
    private c.a f11942;

    /* renamed from: ˈ  reason: contains not printable characters */
    private c.b f11943;

    /* renamed from: ˑ  reason: contains not printable characters */
    private b.a f11944;

    /* renamed from: ٴ  reason: contains not printable characters */
    private i.b f11945;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private b.a f11946;

    /* renamed from: 连任  reason: contains not printable characters */
    private com.purplebrain.adbuddiz.sdk.f.a.a f11947;

    /* renamed from: 靐  reason: contains not printable characters */
    public com.purplebrain.adbuddiz.sdk.e.a.a.c f11948;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f11949;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public c f11950;

    /* renamed from: 龘  reason: contains not printable characters */
    public com.purplebrain.adbuddiz.sdk.e.a.a.a f11951;

    @SuppressLint({"NewApi"})
    /* renamed from: com.purplebrain.adbuddiz.sdk.e.a$a  reason: collision with other inner class name */
    private class C0029a extends OrientationEventListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private e f11959;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f11960;

        public C0029a(Context context) {
            super(context, 3);
            this.f11959 = d.m15318();
            this.f11959 = d.m15318();
            this.f11960 = 0;
        }

        public final void onOrientationChanged(int i) {
            e r1;
            com.purplebrain.adbuddiz.sdk.f.a.a aVar;
            if (this.f11960 % 20 == 0 && this.f11959 != (r1 = d.m15318())) {
                for (com.purplebrain.adbuddiz.sdk.e.a.a.b bVar : a.this.f11940) {
                    if (bVar.m15077() && (aVar = bVar.f11970) != null && !aVar.m15165().a(r1)) {
                        bVar.m15084((Collection) a.this.f11940);
                        a.this.m15048(bVar, (com.purplebrain.adbuddiz.sdk.f.a.a) null);
                    }
                }
                this.f11959 = r1;
            }
        }
    }

    public a() {
        this.f11950 = null;
        this.f11949 = false;
        this.f11947 = null;
        this.f11940 = null;
        this.f11951 = null;
        this.f11948 = null;
        this.f11941 = null;
        this.f11944 = new b.a() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final void m15057(com.purplebrain.adbuddiz.sdk.h.b bVar) {
                com.purplebrain.adbuddiz.sdk.e.a.a.b r0 = a.this.m15039(((i) bVar).f12183);
                synchronized (r0.f11962) {
                    if (r0.f11962 == bVar) {
                        r0.f11962 = null;
                    }
                }
            }
        };
        this.f11945 = new i.b() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final void m15058(i iVar) {
                a.this.m15039(iVar.f12183).m15082(iVar);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final void m15059(i iVar, com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
                Object[] objArr = {aVar.m15162(), Boolean.valueOf(iVar.f12183)};
                com.purplebrain.adbuddiz.sdk.e.a.a.b r1 = a.this.m15039(iVar.f12183);
                r1.f11967 = false;
                r1.f11969 = null;
                r1.f11968 = null;
                synchronized (r1.f11966) {
                    if (!(r1.f11962 == null || r1.f11962 == iVar)) {
                        r1.f11962.m15267();
                    }
                    r1.f11962 = null;
                }
                if (aVar.m15161() == com.purplebrain.adbuddiz.sdk.f.a.e.RTB) {
                    f fVar = (f) aVar;
                    com.purplebrain.adbuddiz.sdk.f.b r2 = b.m15135().m15138();
                    if (fVar.f12033.length() > r2.f12045) {
                        Object[] objArr2 = {Integer.valueOf(fVar.f12033.length()), Integer.valueOf(r2.f12045)};
                        g gVar = new g();
                        gVar.f12170 = aVar;
                        gVar.m15296(AdBuddizError.INVALID_AD);
                        gVar.f12172 = new com.purplebrain.adbuddiz.sdk.f.d(d.a.CACHE_AD, (String) null);
                        gVar.f12171 = String.format("RTB ad content size is over the limit fixed in the configuration. %d > %d", new Object[]{Integer.valueOf(fVar.f12033.length()), Integer.valueOf(r2.f12045)});
                        gVar.m15266();
                        a.this.m15048(r1, (com.purplebrain.adbuddiz.sdk.f.a.a) null);
                        return;
                    }
                }
                if (a.m15038(aVar)) {
                    Context r0 = AdBuddiz.m14865();
                    com.purplebrain.adbuddiz.sdk.f.a.b bVar = (com.purplebrain.adbuddiz.sdk.f.a.b) aVar;
                    new JSONArray();
                    try {
                        JSONArray r22 = com.purplebrain.adbuddiz.sdk.i.b.d.m15365(r0, "aia");
                        r22.put(bVar.f12017);
                        com.purplebrain.adbuddiz.sdk.i.b.d.m15369(r0, "aia", r22);
                    } catch (JSONException e) {
                        o.m15453("ABAlreadyInstalledApps.appendAlreadyInstalledApp()", e);
                    }
                    a.this.m15048(r1, (com.purplebrain.adbuddiz.sdk.f.a.a) null);
                    return;
                }
                r1.m15081(aVar);
                a.this.f11950.m15147(aVar);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final void m15060(i iVar, i.a aVar, String str) {
                Object[] objArr = {aVar, str};
                a.this.m15039(iVar.f12183).m15083(iVar, aVar, str);
            }
        };
        this.f11946 = new b.a() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final void m15061() {
                b.m15135().m15143((b.a) null);
                for (com.purplebrain.adbuddiz.sdk.e.a.a.b bVar : a.this.f11940) {
                    com.purplebrain.adbuddiz.sdk.f.a.a aVar = bVar.f11970;
                    if (bVar.f11963 && aVar == null) {
                        a.this.m15048(bVar, (com.purplebrain.adbuddiz.sdk.f.a.a) null);
                    }
                    if (bVar.f11964 && aVar != null) {
                        a.this.f11950.m15147(aVar);
                    }
                    bVar.f11963 = false;
                    bVar.f11964 = false;
                }
            }
        };
        this.f11943 = new c.b() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final void m15062(com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
                com.purplebrain.adbuddiz.sdk.e.a.a.b r0 = a.this.m15039(aVar.f11995);
                if (aVar == r0.f11970) {
                    try {
                        r0.m15080(AdBuddiz.m14865(), aVar);
                    } catch (JSONException e) {
                        o.m15453("ABAdManager.onNextAdModificationListener onResourceChanged()", e);
                    }
                }
            }
        };
        this.f11942 = new c.a() {
            /* renamed from: 龘  reason: contains not printable characters */
            public final void m15063(com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
                if (!aVar.f11995) {
                    com.purplebrain.adbuddiz.sdk.i.i.m15418(new Runnable() {
                        public final void run() {
                            AdBuddiz.m14859().m14885();
                        }
                    });
                } else {
                    r.m15470(new Runnable() {
                        public final void run() {
                            AdBuddiz.RewardedVideo.m14869().m14889();
                        }
                    });
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public final void m15064(com.purplebrain.adbuddiz.sdk.f.a.a aVar, Exception exc) {
                com.purplebrain.adbuddiz.sdk.f.b r0 = b.m15135().m15138();
                if (r0 != null && r0.f12059.contains(AdBuddizError.INVALID_AD)) {
                    com.purplebrain.adbuddiz.sdk.f.d dVar = new com.purplebrain.adbuddiz.sdk.f.d(d.a.CACHE_AD, (String) null);
                    g gVar = new g();
                    gVar.f12170 = aVar;
                    gVar.m15296(AdBuddizError.INVALID_AD);
                    gVar.f12172 = dVar;
                    gVar.m15297((Throwable) exc);
                    gVar.m15266();
                }
                a.this.m15052(aVar.f11995);
            }
        };
        this.f11950 = new c();
        this.f11950.f11986 = this.f11943;
        this.f11950.f11984 = this.f11942;
        this.f11951 = new com.purplebrain.adbuddiz.sdk.e.a.a.a(this.f11950);
        this.f11948 = new com.purplebrain.adbuddiz.sdk.e.a.a.c(this.f11950);
        this.f11940 = new ArrayList();
        this.f11940.add(this.f11951);
        this.f11940.add(this.f11948);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static com.purplebrain.adbuddiz.sdk.f.a.a m15036(Context context) {
        String r1 = m.m15438(context, "prev");
        if (r1 == null) {
            return null;
        }
        try {
            return com.purplebrain.adbuddiz.sdk.f.a.a.m15155(new JSONObject(com.purplebrain.adbuddiz.sdk.i.f.m15407(r1)));
        } catch (h e) {
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private com.purplebrain.adbuddiz.sdk.f.a.a m15037(com.purplebrain.adbuddiz.sdk.e.a.a.b bVar) {
        if (!this.f11949) {
            throw new k();
        }
        try {
            com.purplebrain.adbuddiz.sdk.f.a.a aVar = bVar.f11970;
            i iVar = bVar.f11962;
            if (bVar.f11969 != null) {
                throw new com.purplebrain.adbuddiz.sdk.b.c(bVar.f11969, bVar.f11968);
            } else if (bVar.f11967) {
                throw new p();
            } else if (aVar != null && m15050(aVar)) {
                throw new com.purplebrain.adbuddiz.sdk.b.f();
            } else if (aVar == null && iVar == null) {
                throw new s();
            } else if (aVar != null || iVar == null) {
                return aVar;
            } else {
                throw new com.purplebrain.adbuddiz.sdk.b.g();
            }
        } catch (com.purplebrain.adbuddiz.sdk.b.b e) {
            if (bVar.m15076()) {
                m15048(bVar, (com.purplebrain.adbuddiz.sdk.f.a.a) null);
            }
            throw e;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static /* synthetic */ boolean m15038(com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
        if (aVar.m15161() != com.purplebrain.adbuddiz.sdk.f.a.e.ADBUDDIZ) {
            return false;
        }
        return j.m15338(AdBuddiz.m14865(), ((com.purplebrain.adbuddiz.sdk.f.a.b) aVar).f12020);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public com.purplebrain.adbuddiz.sdk.e.a.a.b m15039(boolean z) {
        return z ? this.f11948 : this.f11951;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m15041(com.purplebrain.adbuddiz.sdk.e.a.a.b bVar) {
        bVar.m15084((Collection) this.f11940);
        m15048(bVar, (com.purplebrain.adbuddiz.sdk.f.a.a) null);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m15042(com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
        try {
            this.f11947 = aVar;
            if (aVar != null) {
                m.m15447(AdBuddiz.m14865(), "prev", com.purplebrain.adbuddiz.sdk.i.f.m15408(aVar.m15167().toString()));
            } else {
                m.m15436(AdBuddiz.m14865(), "prev");
            }
        } catch (JSONException e) {
            this.f11947 = null;
            m.m15436(AdBuddiz.m14865(), "prev");
            o.m15453("ABAdManager.setLastDismissedAdAndSaveOnDisk() Exception :", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static a m15046() {
        if (f11939 == null) {
            synchronized (a.class) {
                if (f11939 == null) {
                    f11939 = new a();
                }
            }
        }
        return f11939;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private com.purplebrain.adbuddiz.sdk.f.a.a m15047(com.purplebrain.adbuddiz.sdk.e.a.a.b bVar) {
        com.purplebrain.adbuddiz.sdk.f.a.a r0 = m15037(bVar);
        if (r0.m15156().e.m15116(r0, com.purplebrain.adbuddiz.sdk.i.a.d.m15318())) {
            return r0;
        }
        throw new com.purplebrain.adbuddiz.sdk.b.e();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x007b, code lost:
        r9.f11963 = true;
        com.purplebrain.adbuddiz.sdk.e.b.m15135().m15143(r8.f11946);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0089, code lost:
        r9.f11963 = true;
        com.purplebrain.adbuddiz.sdk.e.b.m15135().m15143(r8.f11946);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m15048(com.purplebrain.adbuddiz.sdk.e.a.a.b r9, com.purplebrain.adbuddiz.sdk.f.a.a r10) {
        /*
            r8 = this;
            r1 = 0
            r0 = 1
            com.purplebrain.adbuddiz.sdk.i.p r2 = r9.m15074()
            boolean r3 = r9.f11967
            if (r3 == 0) goto L_0x0029
            java.lang.Long r3 = r2.f12242
            if (r3 == 0) goto L_0x0027
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.Long r3 = r2.f12242
            long r6 = r3.longValue()
            long r4 = r4 - r6
            long r6 = com.purplebrain.adbuddiz.sdk.i.p.m15455()
            int r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r3 >= 0) goto L_0x0027
        L_0x0021:
            if (r0 == 0) goto L_0x0029
            r9.m15073()
        L_0x0026:
            return
        L_0x0027:
            r0 = r1
            goto L_0x0021
        L_0x0029:
            java.lang.Object r1 = r9.f11966
            monitor-enter(r1)
            r0 = 0
            r9.f11963 = r0     // Catch:{ all -> 0x0042 }
            com.purplebrain.adbuddiz.sdk.h.i r0 = r9.f11962     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x0045
            boolean r3 = r0.m15271()     // Catch:{ all -> 0x0042 }
            if (r3 != 0) goto L_0x0045
            int r3 = r0.m15274()     // Catch:{ all -> 0x0042 }
            r4 = 4
            if (r3 >= r4) goto L_0x0045
            monitor-exit(r1)     // Catch:{ all -> 0x0042 }
            goto L_0x0026
        L_0x0042:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0042 }
            throw r0
        L_0x0045:
            com.purplebrain.adbuddiz.sdk.e.b r3 = com.purplebrain.adbuddiz.sdk.e.b.m15135()     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            r3.m15141()     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            com.purplebrain.adbuddiz.sdk.f.a.a r3 = r9.f11970     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            if (r3 == 0) goto L_0x0055
            java.util.List r3 = r8.f11940     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            r9.m15084((java.util.Collection) r3)     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
        L_0x0055:
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            java.lang.Long r3 = java.lang.Long.valueOf(r4)     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            r2.f12242 = r3     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            if (r0 == 0) goto L_0x0064
            r0.m15267()     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
        L_0x0064:
            com.purplebrain.adbuddiz.sdk.h.i r0 = r9.m15072()     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            r0.f12181 = r10     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            com.purplebrain.adbuddiz.sdk.h.i$b r2 = r8.f11945     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            r0.f12182 = r2     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            com.purplebrain.adbuddiz.sdk.h.b$a r2 = r8.f11944     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            r0.m15276((com.purplebrain.adbuddiz.sdk.h.b.a) r2)     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            r0.m15266()     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
            r9.f11962 = r0     // Catch:{ k -> 0x007a, j -> 0x0088, i -> 0x0096 }
        L_0x0078:
            monitor-exit(r1)     // Catch:{ all -> 0x0042 }
            goto L_0x0026
        L_0x007a:
            r0 = move-exception
            r0 = 1
            r9.f11963 = r0     // Catch:{ all -> 0x0042 }
            com.purplebrain.adbuddiz.sdk.e.b r0 = com.purplebrain.adbuddiz.sdk.e.b.m15135()     // Catch:{ all -> 0x0042 }
            com.purplebrain.adbuddiz.sdk.e.b$a r2 = r8.f11946     // Catch:{ all -> 0x0042 }
            r0.m15143((com.purplebrain.adbuddiz.sdk.e.b.a) r2)     // Catch:{ all -> 0x0042 }
            goto L_0x0078
        L_0x0088:
            r0 = move-exception
            r0 = 1
            r9.f11963 = r0     // Catch:{ all -> 0x0042 }
            com.purplebrain.adbuddiz.sdk.e.b r0 = com.purplebrain.adbuddiz.sdk.e.b.m15135()     // Catch:{ all -> 0x0042 }
            com.purplebrain.adbuddiz.sdk.e.b$a r2 = r8.f11946     // Catch:{ all -> 0x0042 }
            r0.m15143((com.purplebrain.adbuddiz.sdk.e.b.a) r2)     // Catch:{ all -> 0x0042 }
            goto L_0x0078
        L_0x0096:
            r0 = move-exception
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.e.a.m15048(com.purplebrain.adbuddiz.sdk.e.a.a.b, com.purplebrain.adbuddiz.sdk.f.a.a):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m15050(com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
        if (aVar == null) {
            return true;
        }
        long time = com.purplebrain.adbuddiz.sdk.i.h.m15415().getTime();
        return aVar.m15159() > time || aVar.m15159() + ((long) aVar.m15160()) < time;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m15051() {
        for (com.purplebrain.adbuddiz.sdk.e.a.a.b r0 : this.f11940) {
            r0.m15081((com.purplebrain.adbuddiz.sdk.f.a.a) null);
        }
        m15042((com.purplebrain.adbuddiz.sdk.f.a.a) null);
        for (com.purplebrain.adbuddiz.sdk.f.a.e r02 : com.purplebrain.adbuddiz.sdk.f.a.e.a()) {
            for (File delete : m.m15445(AdBuddiz.m14865(), r02, true)) {
                delete.delete();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m15052(boolean z) {
        m15041(m15039(z));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final com.purplebrain.adbuddiz.sdk.f.a.a m15053(Context context, boolean z) {
        com.purplebrain.adbuddiz.sdk.e.a.a.b r1 = m15039(z);
        com.purplebrain.adbuddiz.sdk.f.a.a r2 = m15047(r1);
        ArrayList arrayList = new ArrayList();
        for (com.purplebrain.adbuddiz.sdk.e.a.a.b bVar : this.f11940) {
            arrayList.add(bVar.f11970);
        }
        this.f11950.m15148(this.f11947, (Collection) arrayList);
        m15042(r2);
        r1.m15081((com.purplebrain.adbuddiz.sdk.f.a.a) null);
        if (r1.m15075()) {
            m15048(r1, r2);
        }
        com.purplebrain.adbuddiz.sdk.f.b r0 = b.m15135().m15138();
        new Thread(new Runnable() {
            public final void run() {
                try {
                    c.m15363(a.this.f12200, "A-", a.this.f12198);
                    c.m15363(a.this.f12200, "I-", a.this.f12198);
                    c.m15354(a.this.f12200, "MRAID", a.this.f12199);
                    c.m15354(a.this.f12200, "RI", a.this.f12199);
                } catch (Throwable th) {
                    l.m15430("ABImpressionCounterHelper#AsyncCleanTask.execute", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                    o.m15453("ABImpressionCounterHelper#AsyncCleanTask.execute()", th);
                }
            }
        }).start();
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final com.purplebrain.adbuddiz.sdk.f.a.a m15054(boolean z) {
        return m15047(m15039(z));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15055(final Context context) {
        if (!this.f11949) {
            if (Build.VERSION.SDK_INT > 9) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            if (a.this.f11941 != null) {
                                try {
                                    a.this.f11941.disable();
                                } catch (IllegalArgumentException e) {
                                }
                            }
                            OrientationEventListener unused = a.this.f11941 = new C0029a(context);
                            a.this.f11941.enable();
                        } catch (Throwable th) {
                            l.m15430("ABAdManager.registerOrientationEventListener", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                            o.m15453("ABAdManager.registerOrientationEventListener() Exception : ", th);
                        }
                    }
                });
            }
            this.f11949 = true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15056(Context context, com.purplebrain.adbuddiz.sdk.e.a.a.b bVar) {
        com.purplebrain.adbuddiz.sdk.f.a.a aVar;
        com.purplebrain.adbuddiz.sdk.f.a.a aVar2 = null;
        com.purplebrain.adbuddiz.sdk.f.a.a r2 = bVar.m15078(context);
        if (this.f11947 == null) {
            try {
                aVar = m15036(context);
            } catch (JSONException e) {
                m.m15436(context, "prev");
                aVar = null;
            }
            this.f11947 = aVar;
        }
        if (r2 == null) {
            m15041(bVar);
        } else if (m15050(r2)) {
            m15041(bVar);
        } else {
            if (bVar.m15077()) {
                if (!(r2 == null ? false : r2.m15165().a(com.purplebrain.adbuddiz.sdk.i.a.d.m15318()))) {
                    m15041(bVar);
                }
            }
            aVar2 = r2;
        }
        if (aVar2 == null) {
            return;
        }
        if (b.m15135().m15138() == null) {
            bVar.f11964 = true;
            b.m15135().m15143(this.f11946);
            return;
        }
        this.f11950.m15147(aVar2);
    }
}
