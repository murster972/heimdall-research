package com.purplebrain.adbuddiz.sdk.e.a.b;

import android.media.MediaMetadataRetriever;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.f.a.a;
import com.purplebrain.adbuddiz.sdk.f.a.a.c;
import com.purplebrain.adbuddiz.sdk.f.e;
import com.purplebrain.adbuddiz.sdk.i.b.f;
import com.purplebrain.adbuddiz.sdk.i.c.d;
import com.purplebrain.adbuddiz.sdk.i.m;
import java.util.Collection;
import java.util.Iterator;

public final class b extends c {
    public b() {
        super((byte) 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m15101(c cVar) {
        boolean z;
        boolean z2;
        Boolean bool = null;
        int i = 0;
        while (bool == null && i < 2) {
            try {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                mediaMetadataRetriever.setDataSource(m.m15440(AdBuddiz.m14865(), cVar));
                String extractMetadata = mediaMetadataRetriever.extractMetadata(9);
                String extractMetadata2 = mediaMetadataRetriever.extractMetadata(18);
                String extractMetadata3 = mediaMetadataRetriever.extractMetadata(19);
                if (extractMetadata3 == null || extractMetadata2 == null || extractMetadata == null) {
                    z2 = false;
                } else {
                    com.purplebrain.adbuddiz.sdk.f.a.c cVar2 = (com.purplebrain.adbuddiz.sdk.f.a.c) cVar.f12006;
                    cVar2.f12029 = Long.valueOf(Long.parseLong(extractMetadata));
                    cVar2.f12026 = Integer.valueOf(Integer.parseInt(extractMetadata2));
                    cVar2.f12027 = Integer.valueOf(Integer.parseInt(extractMetadata3));
                    d.m15392(cVar, cVar2.f12029.longValue());
                    d.m15391(cVar, cVar2.f12026.intValue());
                    d.m15388(cVar, cVar2.f12027.intValue());
                    this.f11975.m15120(cVar2);
                    z2 = true;
                }
                z = Boolean.valueOf(z2);
            } catch (IllegalStateException e) {
                z = bool;
            } catch (Throwable th) {
                z = false;
            }
            i++;
            bool = z;
        }
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:32:0x0087=Splitter:B:32:0x0087, B:17:0x006c=Splitter:B:17:0x006c} */
    /* renamed from: 连任  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m15102(com.purplebrain.adbuddiz.sdk.f.a.a.c r10) {
        /*
            r9 = this;
            r8 = 0
            r4 = 1
            boolean r0 = super.m15111(r10)
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return r4
        L_0x0009:
            com.purplebrain.adbuddiz.sdk.f.a.a r0 = r10.f12006
            r7 = r0
            com.purplebrain.adbuddiz.sdk.f.a.c r7 = (com.purplebrain.adbuddiz.sdk.f.a.c) r7
            com.purplebrain.adbuddiz.sdk.f.a.a.f r0 = new com.purplebrain.adbuddiz.sdk.f.a.a.f
            com.purplebrain.adbuddiz.sdk.f.a.a r1 = r10.f12006
            com.purplebrain.adbuddiz.sdk.f.a.a.d r2 = com.purplebrain.adbuddiz.sdk.f.a.a.d.MEDIA
            java.lang.String r3 = r10.m15175()
            com.purplebrain.adbuddiz.sdk.f.e r5 = com.purplebrain.adbuddiz.sdk.f.e.BOTH
            java.net.URL r6 = r10.m15177()
            r0.<init>(r1, r2, r3, r4, r5, r6)
            android.content.Context r1 = com.purplebrain.adbuddiz.sdk.AdBuddiz.m14865()     // Catch:{ all -> 0x0099 }
            java.io.InputStream r3 = com.purplebrain.adbuddiz.sdk.i.m.m15437((android.content.Context) r1, (com.purplebrain.adbuddiz.sdk.f.a.a.c) r10)     // Catch:{ all -> 0x0099 }
            java.lang.String r1 = com.purplebrain.adbuddiz.sdk.i.s.m15472(r3)     // Catch:{ all -> 0x0088 }
            java.util.Map r2 = r7.f12023     // Catch:{ all -> 0x0088 }
            java.util.Set r2 = r2.entrySet()     // Catch:{ all -> 0x0088 }
            java.util.Iterator r5 = r2.iterator()     // Catch:{ all -> 0x0088 }
            r4 = r1
        L_0x0038:
            boolean r1 = r5.hasNext()     // Catch:{ all -> 0x0088 }
            if (r1 == 0) goto L_0x0056
            java.lang.Object r1 = r5.next()     // Catch:{ all -> 0x0088 }
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1     // Catch:{ all -> 0x0088 }
            java.lang.Object r2 = r1.getKey()     // Catch:{ all -> 0x0088 }
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2     // Catch:{ all -> 0x0088 }
            java.lang.Object r1 = r1.getValue()     // Catch:{ all -> 0x0088 }
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1     // Catch:{ all -> 0x0088 }
            java.lang.String r1 = r4.replace(r2, r1)     // Catch:{ all -> 0x0088 }
            r4 = r1
            goto L_0x0038
        L_0x0056:
            android.content.Context r2 = com.purplebrain.adbuddiz.sdk.AdBuddiz.m14865()     // Catch:{ all -> 0x0088 }
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x0082 }
            java.lang.String r5 = "UTF-8"
            byte[] r4 = r4.getBytes(r5)     // Catch:{ all -> 0x0082 }
            r1.<init>(r4)     // Catch:{ all -> 0x0082 }
            com.purplebrain.adbuddiz.sdk.i.m.m15446((android.content.Context) r2, (java.io.InputStream) r1, (com.purplebrain.adbuddiz.sdk.f.a.a.c) r0)     // Catch:{ all -> 0x009b }
            r1.close()     // Catch:{ Throwable -> 0x0091 }
        L_0x006c:
            java.util.Set r1 = r7.f11994     // Catch:{ all -> 0x0088 }
            monitor-enter(r1)     // Catch:{ all -> 0x0088 }
            java.util.Set r2 = r7.f11994     // Catch:{ all -> 0x008e }
            r2.remove(r10)     // Catch:{ all -> 0x008e }
            monitor-exit(r1)     // Catch:{ all -> 0x008e }
            r7.m15169((com.purplebrain.adbuddiz.sdk.f.a.a.c) r0)     // Catch:{ all -> 0x0088 }
            com.purplebrain.adbuddiz.sdk.e.a.b.c$a r0 = r9.f11975     // Catch:{ all -> 0x0088 }
            r0.m15120(r7)     // Catch:{ all -> 0x0088 }
            r3.close()     // Catch:{ Throwable -> 0x0095 }
        L_0x0080:
            r4 = 0
            goto L_0x0008
        L_0x0082:
            r0 = move-exception
            r1 = r8
        L_0x0084:
            r1.close()     // Catch:{ Throwable -> 0x0093 }
        L_0x0087:
            throw r0     // Catch:{ all -> 0x0088 }
        L_0x0088:
            r0 = move-exception
            r8 = r3
        L_0x008a:
            r8.close()     // Catch:{ Throwable -> 0x0097 }
        L_0x008d:
            throw r0
        L_0x008e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x008e }
            throw r0     // Catch:{ all -> 0x0088 }
        L_0x0091:
            r1 = move-exception
            goto L_0x006c
        L_0x0093:
            r1 = move-exception
            goto L_0x0087
        L_0x0095:
            r0 = move-exception
            goto L_0x0080
        L_0x0097:
            r1 = move-exception
            goto L_0x008d
        L_0x0099:
            r0 = move-exception
            goto L_0x008a
        L_0x009b:
            r0 = move-exception
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.purplebrain.adbuddiz.sdk.e.a.b.b.m15102(com.purplebrain.adbuddiz.sdk.f.a.a.c):boolean");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m15103(a aVar) {
        boolean z;
        try {
            Iterator it2 = aVar.m15164().iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (m15111((c) it2.next())) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z && this.f11975 != null) {
                this.f11975.m15119(aVar);
            }
        } catch (Exception e) {
            this.f11975.m15121(aVar, e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m15104(c cVar) {
        boolean z = false;
        if (!cVar.m15183("isVideo")) {
            if (cVar.f12003 == com.purplebrain.adbuddiz.sdk.f.a.a.d.ASSET) {
                z = ((com.purplebrain.adbuddiz.sdk.f.a.c) cVar.f12006).f12025.equals(cVar.m15175());
            }
            return z ? m15102(cVar) : super.m15111(cVar);
        } else if (super.m15111(cVar)) {
            return true;
        } else {
            if (d.m15393(cVar) || m15101(cVar)) {
                return false;
            }
            f.m15371(AdBuddiz.m14865(), (com.purplebrain.adbuddiz.sdk.f.a.c) cVar.f12006);
            throw new Exception("Cannot extract metadata information from the video.");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15105(c cVar) {
        m15103(cVar.f12006);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15106(a aVar) {
        m15103(aVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15107(a aVar, Collection collection) {
        new com.purplebrain.adbuddiz.sdk.i.b();
        com.purplebrain.adbuddiz.sdk.i.b.m15344(collection);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final boolean m15108(a aVar, e eVar) {
        for (c cVar : aVar.m15164()) {
            if (!m15110(cVar)) {
                return false;
            }
            if (cVar.m15183("isVideo") && !d.m15393(cVar)) {
                return false;
            }
        }
        return true;
    }
}
