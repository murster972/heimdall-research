package com.purplebrain.adbuddiz.sdk.e.a.a;

import android.content.Context;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.b.h;
import com.purplebrain.adbuddiz.sdk.e.c;
import com.purplebrain.adbuddiz.sdk.f.a.a;
import com.purplebrain.adbuddiz.sdk.h.i;
import com.purplebrain.adbuddiz.sdk.i.f;
import com.purplebrain.adbuddiz.sdk.i.m;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.i.p;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class b {

    /* renamed from: ʻ  reason: contains not printable characters */
    public i f11962 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean f11963 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean f11964 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private c f11965 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    public Object f11966 = new Object();

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean f11967 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    public String f11968 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    public i.a f11969 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    public a f11970 = null;

    protected b(c cVar) {
        this.f11965 = cVar;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private a m15071(Context context) {
        String r1 = m.m15438(context, m15079());
        if (r1 == null) {
            return null;
        }
        try {
            return a.m15155(new JSONObject(f.m15407(r1)));
        } catch (h e) {
            return null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract i m15072();

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m15073() {
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract p m15074();

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract boolean m15075();

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract boolean m15076();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract boolean m15077();

    /* renamed from: 龘  reason: contains not printable characters */
    public final a m15078(Context context) {
        if (this.f11970 == null) {
            a aVar = null;
            try {
                aVar = m15071(context);
            } catch (JSONException e) {
                m.m15436(context, m15079());
            }
            this.f11970 = aVar;
        }
        return this.f11970;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m15079();

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15080(Context context, a aVar) {
        m.m15447(context, m15079(), f.m15408(aVar.m15167().toString()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15081(a aVar) {
        try {
            this.f11970 = aVar;
            if (aVar != null) {
                m15080(AdBuddiz.m14865(), aVar);
            } else {
                m.m15436(AdBuddiz.m14865(), m15079());
            }
        } catch (JSONException e) {
            this.f11970 = null;
            m.m15436(AdBuddiz.m14865(), m15079());
            o.m15453("ABAdManager.setNextAdToShowAndSaveOnDisk() Exception :", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15082(i iVar) {
        m15081((a) null);
        this.f11967 = true;
        this.f11969 = null;
        this.f11968 = null;
        synchronized (this.f11966) {
            if (this.f11962 == iVar) {
                this.f11962 = null;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15083(i iVar, i.a aVar, String str) {
        m15081((a) null);
        this.f11967 = true;
        this.f11969 = aVar;
        this.f11968 = str;
        synchronized (this.f11966) {
            if (!(this.f11962 == null || this.f11962 == iVar)) {
                this.f11962.m15267();
            }
            this.f11962 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15084(Collection collection) {
        ArrayList arrayList = new ArrayList();
        Iterator it2 = collection.iterator();
        while (it2.hasNext()) {
            b bVar = (b) it2.next();
            if (!(bVar == this || bVar.f11970 == null)) {
                arrayList.add(bVar.f11970);
            }
        }
        this.f11965.m15148(this.f11970, (Collection) arrayList);
        m15081((a) null);
    }
}
