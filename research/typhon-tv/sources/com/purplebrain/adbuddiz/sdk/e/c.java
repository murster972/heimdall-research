package com.purplebrain.adbuddiz.sdk.e;

import android.annotation.SuppressLint;
import android.content.Context;
import com.purplebrain.adbuddiz.sdk.e.a.b.c;
import com.purplebrain.adbuddiz.sdk.i.l;
import com.purplebrain.adbuddiz.sdk.i.m;
import com.purplebrain.adbuddiz.sdk.i.o;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collection;

public final class c {

    /* renamed from: 靐  reason: contains not printable characters */
    a f11984 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private c.a f11985 = new c.a() {
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m15149(com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
            c.this.f11984.m15152(aVar);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m15150(com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
            c.this.f11986.m15154(aVar);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m15151(com.purplebrain.adbuddiz.sdk.f.a.a aVar, Exception exc) {
            c.this.f11984.m15153(aVar, exc);
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    b f11986 = null;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15152(com.purplebrain.adbuddiz.sdk.f.a.a aVar);

        /* renamed from: 龘  reason: contains not printable characters */
        void m15153(com.purplebrain.adbuddiz.sdk.f.a.a aVar, Exception exc);
    }

    public interface b {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15154(com.purplebrain.adbuddiz.sdk.f.a.a aVar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static InputStream m15146(Context context, com.purplebrain.adbuddiz.sdk.f.a.a.c cVar) {
        if (cVar.m15179()) {
            return m.m15437(context, cVar);
        }
        try {
            return new ByteArrayInputStream(cVar.m15172().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15147(final com.purplebrain.adbuddiz.sdk.f.a.a aVar) {
        if (aVar != null) {
            if (b.m15135().m15138() == null) {
                this.f11984.m15153(aVar, new Exception("Cannot cache an ad without config."));
            } else if (aVar.m15164().isEmpty()) {
                this.f11984.m15153(aVar, new Exception("Cannot cache an ad without resource."));
            } else {
                aVar.m15156().e.m15112(this.f11985);
                try {
                    new Thread(new Runnable() {
                        public final void run() {
                            try {
                                aVar.m15156().e.m15114(aVar);
                            } catch (Throwable th) {
                                l.m15430("ABResourceManager.cacheAd", aVar, th);
                                o.m15453("ABResourceManager.cacheAd()", th);
                            }
                        }
                    }).start();
                } catch (InternalError e) {
                }
            }
        }
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15148(final com.purplebrain.adbuddiz.sdk.f.a.a aVar, final Collection collection) {
        if (aVar != null && !collection.contains(aVar)) {
            new Thread(new Runnable() {
                public final void run() {
                    try {
                        aVar.m15156().e.m15115(aVar, collection);
                    } catch (Throwable th) {
                        l.m15430("ABResourceManager.clean", (com.purplebrain.adbuddiz.sdk.f.a.a) null, th);
                        o.m15453("ABResourceManager.clean()", th);
                    }
                }
            }).start();
        }
    }
}
