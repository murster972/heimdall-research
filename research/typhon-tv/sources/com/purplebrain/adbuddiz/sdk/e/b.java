package com.purplebrain.adbuddiz.sdk.e;

import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.AdBuddizError;
import com.purplebrain.adbuddiz.sdk.AdBuddizRewardedVideoError;
import com.purplebrain.adbuddiz.sdk.b.i;
import com.purplebrain.adbuddiz.sdk.b.k;
import com.purplebrain.adbuddiz.sdk.b.l;
import com.purplebrain.adbuddiz.sdk.b.n;
import com.purplebrain.adbuddiz.sdk.b.o;
import com.purplebrain.adbuddiz.sdk.b.r;
import com.purplebrain.adbuddiz.sdk.f.c;
import com.purplebrain.adbuddiz.sdk.h.j;
import com.purplebrain.adbuddiz.sdk.i.f;
import com.purplebrain.adbuddiz.sdk.i.h;
import com.purplebrain.adbuddiz.sdk.i.m;
import com.purplebrain.adbuddiz.sdk.i.q;

public final class b implements j.a {

    /* renamed from: 靐  reason: contains not printable characters */
    private static com.purplebrain.adbuddiz.sdk.f.b f11978;

    /* renamed from: 麤  reason: contains not printable characters */
    private static String f11979;

    /* renamed from: 齉  reason: contains not printable characters */
    private static Integer f11980;

    /* renamed from: 龘  reason: contains not printable characters */
    private static b f11981;

    /* renamed from: ʻ  reason: contains not printable characters */
    private a f11982 = null;

    /* renamed from: 连任  reason: contains not printable characters */
    private Long f11983;

    public interface a {
        /* renamed from: 龘  reason: contains not printable characters */
        void m15145();
    }

    private b() {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private synchronized boolean m15132() {
        boolean z = true;
        synchronized (this) {
            if (m15138() != null) {
                if (System.currentTimeMillis() - m15138().f12066 <= m15138().f12063 && m15138().f12066 <= System.currentTimeMillis()) {
                    z = false;
                }
            }
        }
        return z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private synchronized void m15133(String str) {
        if (m15138() == null) {
            f11978 = new com.purplebrain.adbuddiz.sdk.f.b();
        }
        f11978.m15225(str);
        m.m15435(AdBuddiz.m14865(), "ABZ_31_cfg.abz", f.m15408(c.m15228(f11978).toString()));
        if (this.f11982 != null) {
            this.f11982.m15145();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private synchronized boolean m15134(String str) {
        new com.purplebrain.adbuddiz.sdk.f.b().m15225(str);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized b m15135() {
        b bVar;
        synchronized (b.class) {
            if (f11981 == null) {
                f11981 = new b();
            }
            bVar = f11981;
        }
        return bVar;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m15136() {
        if (m15138() != null) {
            AdBuddiz.m14865().deleteFile("ABZ_31_cfg.abz");
            f11978 = null;
        }
        f11979 = null;
        f11980 = null;
        this.f11983 = null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final synchronized void m15137() {
        if (this.f11983 == null || !h.m15416(this.f11983, 5000)) {
            j jVar = new j();
            jVar.f12185 = this;
            jVar.m15266();
            this.f11983 = Long.valueOf(System.currentTimeMillis());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized com.purplebrain.adbuddiz.sdk.f.b m15138() {
        if (f11978 == null) {
            f11978 = com.purplebrain.adbuddiz.sdk.f.b.m15223();
        }
        return f11978;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final synchronized void m15139(String str) {
        m15134(str);
        m15133(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final synchronized void m15140() {
        if (m15132()) {
            m15137();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final synchronized void m15141() {
        if (!com.purplebrain.adbuddiz.sdk.i.a.h.m15332(AdBuddiz.m14865(), "android.permission.INTERNET")) {
            throw new n();
        } else if (q.m15465(AdBuddiz.m14865()) == null) {
            throw new o();
        } else if (f11980 != null && f11980.intValue() == -1) {
            throw new r();
        } else if (f11980 == null || f11980.intValue() != 403) {
            com.purplebrain.adbuddiz.sdk.f.b r0 = m15138();
            if (r0 == null) {
                throw new k();
            } else if (r0.m15224()) {
                throw new com.purplebrain.adbuddiz.sdk.b.j();
            }
        } else {
            String r02 = com.purplebrain.adbuddiz.sdk.i.k.m15427(f11979);
            if (r02 == null) {
                throw new l();
            }
            try {
                throw new i(AdBuddizError.valueOf(r02), AdBuddizRewardedVideoError.valueOf(r02));
            } catch (IllegalArgumentException e) {
                throw new com.purplebrain.adbuddiz.sdk.b.m();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized void m15142(int i, String str) {
        f11980 = Integer.valueOf(i);
        f11979 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15143(a aVar) {
        this.f11982 = aVar;
        if (this.f11982 != null && f11978 != null && !f11978.m15224()) {
            this.f11982.m15145();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final synchronized boolean m15144(String str) {
        return m15138().f12054.contains(str);
    }
}
