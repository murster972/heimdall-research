package com.purplebrain.adbuddiz.sdk.e.a.a;

import com.purplebrain.adbuddiz.sdk.AdBuddizRewardedVideoError;
import com.purplebrain.adbuddiz.sdk.c.b;
import com.purplebrain.adbuddiz.sdk.f.d;
import com.purplebrain.adbuddiz.sdk.h.i;
import com.purplebrain.adbuddiz.sdk.i.p;
import java.util.Arrays;
import java.util.List;

public final class c extends b {

    /* renamed from: ˑ  reason: contains not printable characters */
    private static List f11971 = Arrays.asList(new AdBuddizRewardedVideoError[]{AdBuddizRewardedVideoError.NETWORK_TOO_SLOW, AdBuddizRewardedVideoError.UNSUPPORTED_DEVICE, AdBuddizRewardedVideoError.UNSUPPORTED_OS_VERSION});

    public c(com.purplebrain.adbuddiz.sdk.e.c cVar) {
        super(cVar);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final i m15085() {
        i iVar = new i();
        iVar.f12183 = true;
        return iVar;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m15086() {
        String str = null;
        super.m15073();
        d dVar = new d(d.a.FETCH_VIDEO_AD, (String) null);
        AdBuddizRewardedVideoError adBuddizRewardedVideoError = AdBuddizRewardedVideoError.NO_MORE_AVAILABLE_ADS;
        if (this.f11969 != null) {
            adBuddizRewardedVideoError = this.f11969.f;
            str = this.f11968;
        }
        b.m15011();
        b.m15013(dVar, adBuddizRewardedVideoError, str, true, true);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final p m15087() {
        return p.m15457(true);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m15088() {
        return false;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m15089() {
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m15090() {
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final String m15091() {
        return "next_incent";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15092(i iVar) {
        super.m15082(iVar);
        d dVar = new d(d.a.FETCH_VIDEO_AD, (String) null);
        b.m15011();
        b.m15013(dVar, AdBuddizRewardedVideoError.NO_MORE_AVAILABLE_ADS, (String) null, true, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m15093(i iVar, i.a aVar, String str) {
        boolean z = false;
        super.m15083(iVar, aVar, str);
        AdBuddizRewardedVideoError adBuddizRewardedVideoError = aVar.f;
        if (adBuddizRewardedVideoError != null && f11971.contains(adBuddizRewardedVideoError)) {
            z = true;
        }
        if (z) {
            d dVar = new d(d.a.FETCH_VIDEO_AD, (String) null);
            b.m15011();
            b.m15013(dVar, adBuddizRewardedVideoError, str, true, true);
        }
    }
}
