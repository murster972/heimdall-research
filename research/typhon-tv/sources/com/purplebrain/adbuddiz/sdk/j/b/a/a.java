package com.purplebrain.adbuddiz.sdk.j.b.a;

import android.annotation.SuppressLint;
import com.purplebrain.adbuddiz.sdk.i.y;
import com.purplebrain.adbuddiz.sdk.j.b.b.b;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressLint({"NewApi"})
public abstract class a {

    /* renamed from: 靐  reason: contains not printable characters */
    private List f12277 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private List f12278 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private List f12279 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Node f12280;

    public a(Node node) {
        this.f12280 = node;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public abstract int m15513();

    /* renamed from: 靐  reason: contains not printable characters */
    public final List m15514() {
        if (this.f12277 == null) {
            this.f12277 = y.m15500(this.f12280, "Error");
        }
        return this.f12277;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final List m15515() {
        if (this.f12278 == null) {
            this.f12278 = new ArrayList();
            NodeList r1 = y.m15503(this.f12280, "Creatives/Creative/Linear | Creatives/Creative/CompanionAds | Creatives/Creative/NonLinearAds");
            for (int i = 0; i < r1.getLength(); i++) {
                b r2 = b.m15523(this, r1.item(i));
                if (r2 != null) {
                    this.f12278.add(r2);
                }
            }
        }
        return this.f12278;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final List m15516() {
        if (this.f12279 == null) {
            this.f12279 = y.m15500(this.f12280, "Impression");
        }
        return this.f12279;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Integer m15517() {
        return y.m15496(this.f12280.getParentNode(), "sequence");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List m15518(int i) {
        ArrayList arrayList = new ArrayList();
        for (b bVar : m15515()) {
            if (bVar.m15525() == i) {
                arrayList.add(bVar);
            }
        }
        return arrayList;
    }
}
