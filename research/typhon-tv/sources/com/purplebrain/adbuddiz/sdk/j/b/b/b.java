package com.purplebrain.adbuddiz.sdk.j.b.b;

import com.purplebrain.adbuddiz.sdk.i.y;
import com.purplebrain.adbuddiz.sdk.j.b.a.a;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class b {

    /* renamed from: 靐  reason: contains not printable characters */
    protected Node f12282;

    /* renamed from: 齉  reason: contains not printable characters */
    private List f12283;

    /* renamed from: 龘  reason: contains not printable characters */
    public a f12284;

    protected b(a aVar, Node node) {
        this.f12284 = aVar;
        this.f12282 = node;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static b m15523(a aVar, Node node) {
        if ("Linear".equals(node.getNodeName())) {
            return new e(aVar, node);
        }
        if ("NonLinear".equals(node.getNodeName())) {
            return new h(aVar, node);
        }
        if ("CompanionAds".equals(node.getNodeName())) {
            return new a(aVar, node);
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final List m15524() {
        if (this.f12283 == null) {
            this.f12283 = new ArrayList();
            NodeList r1 = y.m15503(this.f12282, "TrackingEvents/Tracking");
            for (int i = 0; i < r1.getLength(); i++) {
                this.f12283.add(new i(r1.item(i)));
            }
        }
        return this.f12283;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m15525();
}
