package com.purplebrain.adbuddiz.sdk.j.d;

import com.purplebrain.adbuddiz.sdk.f.a.a;
import com.purplebrain.adbuddiz.sdk.f.a.a.d;
import com.purplebrain.adbuddiz.sdk.f.a.a.g;
import com.purplebrain.adbuddiz.sdk.f.e;
import com.purplebrain.adbuddiz.sdk.j.b.b.f;
import java.net.URL;
import java.util.Locale;

public final class c extends g {
    public c(a aVar, f fVar, URL url) {
        super(aVar, d.MEDIA, e.BOTH, url.toString());
        a.m15549((com.purplebrain.adbuddiz.sdk.f.a.a.c) this, fVar);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m15550() {
        int r0 = com.purplebrain.adbuddiz.sdk.i.c.d.m15390(this);
        int r1 = com.purplebrain.adbuddiz.sdk.i.c.d.m15389(this);
        return String.format(Locale.US, "MEDIA-%dx%d", new Object[]{Integer.valueOf(r0), Integer.valueOf(r1)});
    }
}
