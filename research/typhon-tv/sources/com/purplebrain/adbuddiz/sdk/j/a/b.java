package com.purplebrain.adbuddiz.sdk.j.a;

import com.purplebrain.adbuddiz.sdk.j.b.a.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public final class b extends Exception {
    public a a;
    public List b;

    public b(a aVar) {
        this(aVar, (List) new ArrayList(), (String) null);
    }

    public b(a aVar, a aVar2) {
        this(aVar, Collections.singletonList(aVar2), (String) null);
    }

    public b(a aVar, a aVar2, String str) {
        this(aVar, Collections.singletonList(aVar2), str);
    }

    public b(a aVar, String str) {
        this(aVar, (List) new ArrayList(), str);
    }

    public b(a aVar, Throwable th) {
        this(aVar, (List) new ArrayList(), th);
    }

    public b(a aVar, List list) {
        this(aVar, list, (String) null);
    }

    private b(a aVar, List list, String str) {
        super(m15508(aVar, str));
        this.b = new ArrayList();
        this.a = aVar;
        this.b.addAll(list);
    }

    private b(a aVar, List list, Throwable th) {
        super(m15508(aVar, (String) null), th);
        this.b = new ArrayList();
        this.a = aVar;
        this.b.addAll(list);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m15508(a aVar, String str) {
        if (str == null) {
            return String.format(Locale.US, "VAST Error: %d", new Object[]{Integer.valueOf(aVar.j)});
        }
        return String.format(Locale.US, "VAST Error: %d. %s", new Object[]{Integer.valueOf(aVar.j), str});
    }
}
