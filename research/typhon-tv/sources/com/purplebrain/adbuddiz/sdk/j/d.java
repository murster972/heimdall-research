package com.purplebrain.adbuddiz.sdk.j;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import com.mopub.mobileads.VastIconXmlManager;
import com.purplebrain.adbuddiz.sdk.i.c.c;
import com.purplebrain.adbuddiz.sdk.i.o;
import com.purplebrain.adbuddiz.sdk.j.b.b.i;
import com.purplebrain.adbuddiz.sdk.j.d.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public final class d extends CountDownTimer {

    /* renamed from: ʻ  reason: contains not printable characters */
    private List f12296 = new ArrayList();

    /* renamed from: 连任  reason: contains not printable characters */
    private c f12297;

    /* renamed from: 靐  reason: contains not printable characters */
    public long f12298;

    /* renamed from: 麤  reason: contains not printable characters */
    private MediaPlayer f12299;

    /* renamed from: 齉  reason: contains not printable characters */
    private com.purplebrain.adbuddiz.sdk.f.a.a.c f12300;

    /* renamed from: 龘  reason: contains not printable characters */
    public long f12301;

    private class a {

        /* renamed from: 靐  reason: contains not printable characters */
        public com.purplebrain.adbuddiz.sdk.j.b.b.d f12303;

        /* renamed from: 齉  reason: contains not printable characters */
        public List f12305;

        /* renamed from: 龘  reason: contains not printable characters */
        public int f12306;

        private a() {
        }

        /* synthetic */ a(d dVar, byte b) {
            this();
        }
    }

    public d(com.purplebrain.adbuddiz.sdk.f.a.a.c cVar, MediaPlayer mediaPlayer, Integer num, c cVar2) {
        super(2147483647L, 1000);
        this.f12300 = cVar;
        this.f12299 = mediaPlayer;
        this.f12297 = cVar2;
        this.f12301 = 0;
        this.f12298 = com.purplebrain.adbuddiz.sdk.i.c.d.m15387(cVar);
        m15546(com.purplebrain.adbuddiz.sdk.j.b.b.d.firstQuartile, 0.25d);
        m15546(com.purplebrain.adbuddiz.sdk.j.b.b.d.midpoint, 0.5d);
        m15546(com.purplebrain.adbuddiz.sdk.j.b.b.d.thirdQuartile, 0.75d);
        for (i iVar : a.m15547(cVar, com.purplebrain.adbuddiz.sdk.j.b.b.d.u)) {
            a aVar = new a(this, (byte) 0);
            aVar.f12306 = (int) com.purplebrain.adbuddiz.sdk.i.c.a.m15379((String) iVar.f12292.get(VastIconXmlManager.OFFSET), this.f12298).f12203;
            aVar.f12303 = com.purplebrain.adbuddiz.sdk.j.b.b.d.u;
            aVar.f12305 = Collections.singletonList(iVar.f12291);
            this.f12296.add(aVar);
        }
        Collections.sort(this.f12296, new Comparator() {
            public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                return ((a) obj).f12306 - ((a) obj2).f12306;
            }
        });
        if (num != null) {
            this.f12301 = (long) num.intValue();
            Iterator it2 = this.f12296.iterator();
            while (it2.hasNext()) {
                if (((a) it2.next()).f12306 <= num.intValue()) {
                    it2.remove();
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15546(com.purplebrain.adbuddiz.sdk.j.b.b.d dVar, double d) {
        long r0 = com.purplebrain.adbuddiz.sdk.i.c.d.m15387(this.f12300);
        List r2 = a.m15548(this.f12300, dVar);
        if (!r2.isEmpty()) {
            a aVar = new a(this, (byte) 0);
            aVar.f12306 = (int) (((double) r0) * d);
            aVar.f12303 = dVar;
            aVar.f12305 = r2;
            this.f12296.add(aVar);
        }
    }

    public final void onFinish() {
    }

    public final void onTick(long j) {
        try {
            this.f12301 = (long) this.f12299.getCurrentPosition();
            ListIterator listIterator = this.f12296.listIterator();
            while (listIterator.hasNext()) {
                a aVar = (a) listIterator.next();
                if (aVar.f12306 <= this.f12299.getCurrentPosition()) {
                    Object[] objArr = {aVar.f12303.w, Integer.valueOf(this.f12299.getCurrentPosition()), Long.valueOf(this.f12298)};
                    e.m15553("TimeEvent", aVar.f12305, this.f12300, Integer.valueOf(this.f12299.getCurrentPosition()));
                    this.f12297.m15384(aVar.f12303);
                    listIterator.remove();
                } else {
                    return;
                }
            }
        } catch (Throwable th) {
            o.m15453("ABVASTTimeBasedTrackingEventHelper.onTick()", th);
        }
    }
}
