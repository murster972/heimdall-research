package com.purplebrain.adbuddiz.sdk.j;

import android.media.MediaPlayer;
import android.net.Uri;
import com.purplebrain.adbuddiz.sdk.f.a.a.c;
import com.purplebrain.adbuddiz.sdk.j.a.a;
import com.purplebrain.adbuddiz.sdk.j.b.b.d;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public final class e {
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15551(c cVar, MediaPlayer mediaPlayer, a aVar) {
        new Object[1][0] = Integer.valueOf(aVar.j);
        m15553("Error-" + aVar.j, cVar.m15178("e"), cVar, mediaPlayer != null ? Integer.valueOf(mediaPlayer.getCurrentPosition()) : null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15552(c cVar, MediaPlayer mediaPlayer, d dVar) {
        new Object[1][0] = dVar.w;
        m15553("Event-" + dVar.w, com.purplebrain.adbuddiz.sdk.j.d.a.m15548(cVar, dVar), cVar, mediaPlayer != null ? Integer.valueOf(mediaPlayer.getCurrentPosition()) : null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m15553(String str, List list, c cVar, Integer num) {
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            String str2 = (String) it2.next();
            try {
                if (cVar != null) {
                    str2 = str2.replace("[ASSETURI]", Uri.encode(cVar.m15176("u")));
                }
                if (num != null) {
                    str2 = str2.replace("[CONTENTPLAYHEAD]", String.format(Locale.US, "%02d:%02d:%02d.%03d", new Object[]{Integer.valueOf(num.intValue() / 3600000), Integer.valueOf((num.intValue() / 60000) % 60), Integer.valueOf((num.intValue() / 1000) % 60), Integer.valueOf(num.intValue() % 100)}));
                }
                int nextInt = new Random(System.currentTimeMillis()).nextInt();
                new com.purplebrain.adbuddiz.sdk.j.c.a(str, new URL(str2.replace("[CACHEBUSTING]", String.format(Locale.US, "%08d", new Object[]{Integer.valueOf(nextInt)}).substring(0, 8)))).m15266();
            } catch (MalformedURLException e) {
            }
        }
    }
}
