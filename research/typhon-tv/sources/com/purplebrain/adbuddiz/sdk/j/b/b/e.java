package com.purplebrain.adbuddiz.sdk.j.b.b;

import com.purplebrain.adbuddiz.sdk.i.y;
import com.purplebrain.adbuddiz.sdk.j.a.b;
import com.purplebrain.adbuddiz.sdk.j.b.a.a;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class e extends b {

    /* renamed from: 齉  reason: contains not printable characters */
    private List f12287 = null;

    public e(a aVar, Node node) {
        super(aVar, node);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m15526() {
        return m15527() != null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final String m15527() {
        String r0 = y.m15497(this.f12282, "skipoffset");
        if (r0 == null || com.purplebrain.adbuddiz.sdk.i.c.a.m15379(r0, 2147483647L) != null) {
            return r0;
        }
        throw new b(com.purplebrain.adbuddiz.sdk.j.a.a.XML_PARSING_ERROR, "Offset '" + r0 + "' cannot be parsed into a valid duration.");
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final List m15528() {
        if (this.f12287 == null) {
            this.f12287 = new ArrayList();
            NodeList r1 = y.m15503(this.f12282, "MediaFiles/MediaFile");
            for (int i = 0; i < r1.getLength(); i++) {
                this.f12287.add(new f(this, r1.item(i)));
            }
        }
        return this.f12287;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final List m15529() {
        Node r0 = y.m15498(this.f12282, "VideoClicks");
        return r0 == null ? new ArrayList() : y.m15500(r0, "ClickTracking");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final String m15530() {
        Node r0 = y.m15498(this.f12282, "VideoClicks");
        if (r0 == null) {
            return null;
        }
        return y.m15499(r0, "ClickThrough");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final long m15531() {
        String r1 = y.m15499(this.f12282, "Duration");
        com.purplebrain.adbuddiz.sdk.i.c.a r0 = r1.contains("%") ? null : com.purplebrain.adbuddiz.sdk.i.c.a.m15377(r1, Long.MAX_VALUE);
        if (r0 != null) {
            return r0.f12203;
        }
        throw new b(com.purplebrain.adbuddiz.sdk.j.a.a.XML_PARSING_ERROR, "Time '" + r1 + "' cannot be parse into a valid duration.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final int m15532() {
        return c.a;
    }
}
