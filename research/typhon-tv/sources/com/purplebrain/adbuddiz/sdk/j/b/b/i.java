package com.purplebrain.adbuddiz.sdk.j.b.b;

import android.annotation.SuppressLint;
import android.support.v4.app.NotificationCompat;
import com.purplebrain.adbuddiz.sdk.i.y;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Node;

@SuppressLint({"NewApi"})
public final class i {

    /* renamed from: 靐  reason: contains not printable characters */
    public String f12291;

    /* renamed from: 齉  reason: contains not printable characters */
    public Map f12292 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    public d f12293 = null;

    private i() {
    }

    public i(Node node) {
        this.f12293 = d.a(y.m15497(node, NotificationCompat.CATEGORY_EVENT));
        this.f12291 = y.m15501(node.getTextContent());
        this.f12292 = y.m15502(node);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static i m15540(JSONObject jSONObject) {
        i iVar = new i();
        iVar.f12293 = d.a(jSONObject.getString("e"));
        iVar.f12291 = jSONObject.getString("u");
        JSONArray jSONArray = jSONObject.getJSONArray("a");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            iVar.f12292.put(jSONObject2.getString("k"), jSONObject2.getString("v"));
        }
        return iVar;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final JSONObject m15541() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("e", (Object) this.f12293.w);
        jSONObject.put("u", (Object) this.f12291);
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry entry : this.f12292.entrySet()) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("k", entry.getKey());
            jSONObject2.put("v", entry.getValue());
            jSONArray.put((Object) jSONObject2);
        }
        jSONObject.put("a", (Object) jSONArray);
        return jSONObject;
    }
}
