package com.purplebrain.adbuddiz.sdk.j.b.b;

import android.annotation.SuppressLint;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.mopub.mobileads.VastIconXmlManager;
import com.purplebrain.adbuddiz.sdk.i.y;
import org.w3c.dom.Node;

@SuppressLint({"NewApi"})
public final class f {

    /* renamed from: 靐  reason: contains not printable characters */
    private Node f12288;

    /* renamed from: 龘  reason: contains not printable characters */
    public b f12289;

    public f(b bVar, Node node) {
        this.f12289 = bVar;
        this.f12288 = node;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m15533() {
        return y.m15501(this.f12288.getTextContent());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Integer m15534() {
        return y.m15496(this.f12288, "bitrate");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final String m15535() {
        return y.m15497(this.f12288, VastExtensionXmlManager.TYPE);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Integer m15536() {
        return y.m15496(this.f12288, VastIconXmlManager.HEIGHT);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Integer m15537() {
        return y.m15496(this.f12288, VastIconXmlManager.WIDTH);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final g m15538() {
        try {
            return g.a(y.m15497(this.f12288, "delivery"));
        } catch (Throwable th) {
            return g.UNKNOWN;
        }
    }
}
