package com.purplebrain.adbuddiz.sdk.j.c;

import com.purplebrain.adbuddiz.sdk.h.b;
import com.purplebrain.adbuddiz.sdk.h.k;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.oltu.oauth2.common.OAuth;

public final class a extends b {

    /* renamed from: ʻ  reason: contains not printable characters */
    private URL f12294;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f12295;

    public a(String str, URL url) {
        this.f12295 = str;
        this.f12294 = url;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public final String m15543() {
        return this.f12295 != null ? "ABVASTTrackingRequest-" + this.f12295 : "ABVASTTrackingRequest";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final boolean m15544() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m15545() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = b.m15265(this.f12294, OAuth.HttpMethod.GET);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode != 404 && responseCode >= 500) {
                throw new k("Server Error : " + responseCode);
            }
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }
}
