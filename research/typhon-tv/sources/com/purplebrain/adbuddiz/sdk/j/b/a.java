package com.purplebrain.adbuddiz.sdk.j.b;

import android.annotation.SuppressLint;
import com.purplebrain.adbuddiz.sdk.i.y;
import com.purplebrain.adbuddiz.sdk.j.b.a.c;
import com.purplebrain.adbuddiz.sdk.j.b.a.d;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressLint({"NewApi"})
public final class a {

    /* renamed from: 靐  reason: contains not printable characters */
    private List f12275 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private Node f12276;

    public a(Node node) {
        this.f12276 = node;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List m15511() {
        if (this.f12275 == null) {
            this.f12275 = new ArrayList();
            NodeList r2 = y.m15503(this.f12276, "Ad/InLine | Ad/Wrapper");
            for (int i = 0; i < r2.getLength(); i++) {
                Node item = r2.item(i);
                Object cVar = "InLine".equals(item.getNodeName()) ? new c(item) : "Wrapper".equals(item.getNodeName()) ? new d(item) : null;
                if (cVar != null) {
                    this.f12275.add(cVar);
                }
            }
        }
        return this.f12275;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final List m15512(int i) {
        ArrayList arrayList = new ArrayList();
        for (com.purplebrain.adbuddiz.sdk.j.b.a.a aVar : m15511()) {
            if (aVar.m15513() == i && aVar.m15517() == null) {
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }
}
