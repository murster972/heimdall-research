package com.purplebrain.adbuddiz.sdk.j.a;

public enum a {
    XML_PARSING_ERROR(100),
    TRAFFICKING_ERROR(200),
    EXPECTING_DIFFERENT_LINEARITY(201),
    GENERAL_WRAPPER_ERROR(300),
    WRAPPER_LIMIT_REACHED(302),
    NO_ADS_AFTER_WRAPPER(303),
    UNSUPPORTED_MEDIA_FILES(403),
    PROBLEM_DISPLAYING_MEDIA_FILE(405),
    UNDEFINED_ERROR(900);
    
    public int j;

    private a(int i) {
        this.j = i;
    }
}
