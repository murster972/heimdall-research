package com.crashlytics.android.beta;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class CheckForUpdatesRequest extends AbstractSpiCall {
    static final String BETA_SOURCE = "3";
    static final String BUILD_VERSION = "build_version";
    static final String DISPLAY_VERSION = "display_version";
    static final String HEADER_BETA_TOKEN = "X-CRASHLYTICS-BETA-TOKEN";
    static final String INSTANCE = "instance";
    static final String SDK_ANDROID_DIR_TOKEN_TYPE = "3";
    static final String SOURCE = "source";
    private final CheckForUpdatesResponseTransform responseTransform;

    public CheckForUpdatesRequest(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, CheckForUpdatesResponseTransform checkForUpdatesResponseTransform) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.GET);
        this.responseTransform = checkForUpdatesResponseTransform;
    }

    private HttpRequest applyHeadersTo(HttpRequest httpRequest, String str, String str2) {
        return httpRequest.m19358(AbstractSpiCall.HEADER_ACCEPT, "application/json").m19358(AbstractSpiCall.HEADER_USER_AGENT, AbstractSpiCall.CRASHLYTICS_USER_AGENT + this.kit.getVersion()).m19358(AbstractSpiCall.HEADER_DEVELOPER_TOKEN, AbstractSpiCall.CLS_ANDROID_SDK_DEVELOPER_TOKEN).m19358(AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE).m19358(AbstractSpiCall.HEADER_CLIENT_VERSION, this.kit.getVersion()).m19358(AbstractSpiCall.HEADER_API_KEY, str).m19358(HEADER_BETA_TOKEN, createBetaTokenHeaderValueFor(str2));
    }

    static String createBetaTokenHeaderValueFor(String str) {
        return "3:" + str;
    }

    private Map<String, String> getQueryParamsFor(BuildProperties buildProperties) {
        HashMap hashMap = new HashMap();
        hashMap.put(BUILD_VERSION, buildProperties.versionCode);
        hashMap.put(DISPLAY_VERSION, buildProperties.versionName);
        hashMap.put(INSTANCE, buildProperties.buildId);
        hashMap.put(SOURCE, "3");
        return hashMap;
    }

    public CheckForUpdatesResponse invoke(String str, String str2, BuildProperties buildProperties) {
        HttpRequest httpRequest = null;
        try {
            Map<String, String> queryParamsFor = getQueryParamsFor(buildProperties);
            httpRequest = applyHeadersTo(getHttpRequest(queryParamsFor), str, str2);
            Fabric.m19034().m19090(Beta.TAG, "Checking for updates from " + getUrl());
            Fabric.m19034().m19090(Beta.TAG, "Checking for updates query params are: " + queryParamsFor);
            if (httpRequest.m19353()) {
                Fabric.m19034().m19090(Beta.TAG, "Checking for updates was successful");
                CheckForUpdatesResponse fromJson = this.responseTransform.fromJson(new JSONObject(httpRequest.m19343()));
                if (httpRequest == null) {
                    return fromJson;
                }
                Fabric.m19034().m19090("Fabric", "Checking for updates request ID: " + httpRequest.m19346(AbstractSpiCall.HEADER_REQUEST_ID));
                return fromJson;
            }
            Fabric.m19034().m19082(Beta.TAG, "Checking for updates failed. Response code: " + httpRequest.m19344());
            if (httpRequest != null) {
                Fabric.m19034().m19090("Fabric", "Checking for updates request ID: " + httpRequest.m19346(AbstractSpiCall.HEADER_REQUEST_ID));
            }
            return null;
        } catch (Exception e) {
            Fabric.m19034().m19083(Beta.TAG, "Error while checking for updates from " + getUrl(), e);
            if (httpRequest != null) {
                Fabric.m19034().m19090("Fabric", "Checking for updates request ID: " + httpRequest.m19346(AbstractSpiCall.HEADER_REQUEST_ID));
            }
        } catch (Throwable th) {
            if (httpRequest != null) {
                Fabric.m19034().m19090("Fabric", "Checking for updates request ID: " + httpRequest.m19346(AbstractSpiCall.HEADER_REQUEST_ID));
            }
            throw th;
        }
    }
}
