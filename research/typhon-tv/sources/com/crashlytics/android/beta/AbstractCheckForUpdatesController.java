package com.crashlytics.android.beta;

import android.annotation.SuppressLint;
import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.common.ApiKey;
import io.fabric.sdk.android.services.common.CurrentTimeProvider;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.persistence.PreferenceStore;
import io.fabric.sdk.android.services.settings.BetaSettingsData;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class AbstractCheckForUpdatesController implements UpdatesController {
    static final long LAST_UPDATE_CHECK_DEFAULT = 0;
    static final String LAST_UPDATE_CHECK_KEY = "last_update_check";
    private static final long MILLIS_PER_SECOND = 1000;
    private Beta beta;
    private BetaSettingsData betaSettings;
    private BuildProperties buildProps;
    private Context context;
    private CurrentTimeProvider currentTimeProvider;
    private final AtomicBoolean externallyReady;
    private HttpRequestFactory httpRequestFactory;
    private IdManager idManager;
    private final AtomicBoolean initialized;
    private long lastCheckTimeMillis;
    private PreferenceStore preferenceStore;

    public AbstractCheckForUpdatesController() {
        this(false);
    }

    public AbstractCheckForUpdatesController(boolean z) {
        this.initialized = new AtomicBoolean();
        this.lastCheckTimeMillis = 0;
        this.externallyReady = new AtomicBoolean(z);
    }

    private void performUpdateCheck() {
        Fabric.m19034().m19090(Beta.TAG, "Performing update check");
        new CheckForUpdatesRequest(this.beta, this.beta.getOverridenSpiEndpoint(), this.betaSettings.f15435, this.httpRequestFactory, new CheckForUpdatesResponseTransform()).invoke(new ApiKey().m19134(this.context), this.idManager.m19202().get(IdManager.DeviceIdentifierType.FONT_TOKEN), this.buildProps);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"CommitPrefEdits"})
    public void checkForUpdates() {
        synchronized (this.preferenceStore) {
            if (this.preferenceStore.m19397().contains(LAST_UPDATE_CHECK_KEY)) {
                this.preferenceStore.m19398(this.preferenceStore.m19396().remove(LAST_UPDATE_CHECK_KEY));
            }
        }
        long r0 = this.currentTimeProvider.m19184();
        long j = ((long) this.betaSettings.f15434) * 1000;
        Fabric.m19034().m19090(Beta.TAG, "Check for updates delay: " + j);
        Fabric.m19034().m19090(Beta.TAG, "Check for updates last check time: " + getLastCheckTimeMillis());
        long lastCheckTimeMillis2 = getLastCheckTimeMillis() + j;
        Fabric.m19034().m19090(Beta.TAG, "Check for updates current time: " + r0 + ", next check time: " + lastCheckTimeMillis2);
        if (r0 >= lastCheckTimeMillis2) {
            try {
                performUpdateCheck();
            } finally {
                setLastCheckTimeMillis(r0);
            }
        } else {
            Fabric.m19034().m19090(Beta.TAG, "Check for updates next check time was not passed");
        }
    }

    /* access modifiers changed from: package-private */
    public long getLastCheckTimeMillis() {
        return this.lastCheckTimeMillis;
    }

    public void initialize(Context context2, Beta beta2, IdManager idManager2, BetaSettingsData betaSettingsData, BuildProperties buildProperties, PreferenceStore preferenceStore2, CurrentTimeProvider currentTimeProvider2, HttpRequestFactory httpRequestFactory2) {
        this.context = context2;
        this.beta = beta2;
        this.idManager = idManager2;
        this.betaSettings = betaSettingsData;
        this.buildProps = buildProperties;
        this.preferenceStore = preferenceStore2;
        this.currentTimeProvider = currentTimeProvider2;
        this.httpRequestFactory = httpRequestFactory2;
        if (signalInitialized()) {
            checkForUpdates();
        }
    }

    /* access modifiers changed from: package-private */
    public void setLastCheckTimeMillis(long j) {
        this.lastCheckTimeMillis = j;
    }

    /* access modifiers changed from: protected */
    public boolean signalExternallyReady() {
        this.externallyReady.set(true);
        return this.initialized.get();
    }

    /* access modifiers changed from: package-private */
    public boolean signalInitialized() {
        this.initialized.set(true);
        return this.externallyReady.get();
    }
}
