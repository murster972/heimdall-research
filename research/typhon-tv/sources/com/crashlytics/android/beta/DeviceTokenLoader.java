package com.crashlytics.android.beta;

import android.content.Context;
import android.content.pm.PackageManager;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.cache.ValueLoader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DeviceTokenLoader implements ValueLoader<String> {
    private static final String BETA_APP_PACKAGE_NAME = "io.crash.air";
    private static final String DIRFACTOR_DEVICE_TOKEN_PREFIX = "assets/com.crashlytics.android.beta/dirfactor-device-token=";

    /* access modifiers changed from: package-private */
    public String determineDeviceToken(ZipInputStream zipInputStream) throws IOException {
        ZipEntry nextEntry = zipInputStream.getNextEntry();
        if (nextEntry != null) {
            String name = nextEntry.getName();
            if (name.startsWith(DIRFACTOR_DEVICE_TOKEN_PREFIX)) {
                return name.substring(DIRFACTOR_DEVICE_TOKEN_PREFIX.length(), name.length() - 1);
            }
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public ZipInputStream getZipInputStreamOfApkFrom(Context context, String str) throws PackageManager.NameNotFoundException, FileNotFoundException {
        return new ZipInputStream(new FileInputStream(context.getPackageManager().getApplicationInfo(str, 0).sourceDir));
    }

    public String load(Context context) throws Exception {
        long nanoTime = System.nanoTime();
        String str = "";
        ZipInputStream zipInputStream = null;
        try {
            zipInputStream = getZipInputStreamOfApkFrom(context, "io.crash.air");
            str = determineDeviceToken(zipInputStream);
            if (zipInputStream != null) {
                try {
                    zipInputStream.close();
                } catch (IOException e) {
                    Fabric.m19034().m19083(Beta.TAG, "Failed to close the APK file", e);
                }
            }
        } catch (PackageManager.NameNotFoundException e2) {
            Fabric.m19034().m19090(Beta.TAG, "Beta by Crashlytics app is not installed");
            if (zipInputStream != null) {
                try {
                    zipInputStream.close();
                } catch (IOException e3) {
                    Fabric.m19034().m19083(Beta.TAG, "Failed to close the APK file", e3);
                }
            }
        } catch (FileNotFoundException e4) {
            Fabric.m19034().m19083(Beta.TAG, "Failed to find the APK file", e4);
            if (zipInputStream != null) {
                try {
                    zipInputStream.close();
                } catch (IOException e5) {
                    Fabric.m19034().m19083(Beta.TAG, "Failed to close the APK file", e5);
                }
            }
        } catch (IOException e6) {
            Fabric.m19034().m19083(Beta.TAG, "Failed to read the APK file", e6);
            if (zipInputStream != null) {
                try {
                    zipInputStream.close();
                } catch (IOException e7) {
                    Fabric.m19034().m19083(Beta.TAG, "Failed to close the APK file", e7);
                }
            }
        } catch (Throwable th) {
            if (zipInputStream != null) {
                try {
                    zipInputStream.close();
                } catch (IOException e8) {
                    Fabric.m19034().m19083(Beta.TAG, "Failed to close the APK file", e8);
                }
            }
            throw th;
        }
        Fabric.m19034().m19090(Beta.TAG, "Beta device token load took " + (((double) (System.nanoTime() - nanoTime)) / 1000000.0d) + "ms");
        return str;
    }
}
