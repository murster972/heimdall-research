package com.crashlytics.android.beta;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.services.cache.MemoryValueCache;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeviceIdentifierProvider;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.common.SystemCurrentTimeProvider;
import io.fabric.sdk.android.services.network.DefaultHttpRequestFactory;
import io.fabric.sdk.android.services.persistence.PreferenceStoreImpl;
import io.fabric.sdk.android.services.settings.BetaSettingsData;
import io.fabric.sdk.android.services.settings.Settings;
import io.fabric.sdk.android.services.settings.SettingsData;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Beta extends Kit<Boolean> implements DeviceIdentifierProvider {
    private static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
    private static final String CRASHLYTICS_BUILD_PROPERTIES = "crashlytics-build.properties";
    static final String NO_DEVICE_TOKEN = "";
    public static final String TAG = "Beta";
    private final MemoryValueCache<String> deviceTokenCache = new MemoryValueCache<>();
    private final DeviceTokenLoader deviceTokenLoader = new DeviceTokenLoader();
    private UpdatesController updatesController;

    private String getBetaDeviceToken(Context context, String str) {
        String str2 = null;
        try {
            String r0 = this.deviceTokenCache.m19104(context, this.deviceTokenLoader);
            str2 = "".equals(r0) ? null : r0;
        } catch (Exception e) {
            Fabric.m19034().m19083(TAG, "Failed to load the Beta device token", e);
        }
        Fabric.m19034().m19090(TAG, "Beta device token present: " + (!TextUtils.isEmpty(str2)));
        return str2;
    }

    private BetaSettingsData getBetaSettingsData() {
        SettingsData r0 = Settings.m19434().m19436();
        if (r0 != null) {
            return r0.f15473;
        }
        return null;
    }

    public static Beta getInstance() {
        return (Beta) Fabric.m19045(Beta.class);
    }

    private BuildProperties loadBuildProperties(Context context) {
        InputStream inputStream = null;
        BuildProperties buildProperties = null;
        try {
            InputStream open = context.getAssets().open(CRASHLYTICS_BUILD_PROPERTIES);
            if (open != null) {
                buildProperties = BuildProperties.fromPropertiesStream(open);
                Fabric.m19034().m19090(TAG, buildProperties.packageName + " build properties: " + buildProperties.versionName + " (" + buildProperties.versionCode + ") - " + buildProperties.buildId);
            }
            if (open != null) {
                try {
                    open.close();
                } catch (IOException e) {
                    Fabric.m19034().m19083(TAG, "Error closing Beta build properties asset", e);
                }
            }
        } catch (Exception e2) {
            Fabric.m19034().m19083(TAG, "Error reading Beta build properties", e2);
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    Fabric.m19034().m19083(TAG, "Error closing Beta build properties asset", e3);
                }
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    Fabric.m19034().m19083(TAG, "Error closing Beta build properties asset", e4);
                }
            }
            throw th;
        }
        return buildProperties;
    }

    /* access modifiers changed from: package-private */
    public boolean canCheckForUpdates(BetaSettingsData betaSettingsData, BuildProperties buildProperties) {
        return (betaSettingsData == null || TextUtils.isEmpty(betaSettingsData.f15435) || buildProperties == null) ? false : true;
    }

    /* access modifiers changed from: package-private */
    @TargetApi(14)
    public UpdatesController createUpdatesController(int i, Application application) {
        return i >= 14 ? new ActivityLifecycleCheckForUpdatesController(getFabric().m19051(), getFabric().m19049()) : new ImmediateCheckForUpdatesController();
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground() {
        Fabric.m19034().m19090(TAG, "Beta kit initializing...");
        Context context = getContext();
        IdManager idManager = getIdManager();
        if (TextUtils.isEmpty(getBetaDeviceToken(context, idManager.m19204()))) {
            Fabric.m19034().m19090(TAG, "A Beta device token was not found for this app");
            return false;
        }
        Fabric.m19034().m19090(TAG, "Beta device token is present, checking for app updates.");
        BetaSettingsData betaSettingsData = getBetaSettingsData();
        BuildProperties loadBuildProperties = loadBuildProperties(context);
        if (canCheckForUpdates(betaSettingsData, loadBuildProperties)) {
            this.updatesController.initialize(context, this, idManager, betaSettingsData, loadBuildProperties, new PreferenceStoreImpl(this), new SystemCurrentTimeProvider(), new DefaultHttpRequestFactory(Fabric.m19034()));
        }
        return true;
    }

    public Map<IdManager.DeviceIdentifierType, String> getDeviceIdentifiers() {
        String betaDeviceToken = getBetaDeviceToken(getContext(), getIdManager().m19204());
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(betaDeviceToken)) {
            hashMap.put(IdManager.DeviceIdentifierType.FONT_TOKEN, betaDeviceToken);
        }
        return hashMap;
    }

    public String getIdentifier() {
        return "com.crashlytics.sdk.android:beta";
    }

    /* access modifiers changed from: package-private */
    public String getOverridenSpiEndpoint() {
        return CommonUtils.m19148(getContext(), CRASHLYTICS_API_ENDPOINT);
    }

    public String getVersion() {
        return "1.2.8.25";
    }

    /* access modifiers changed from: protected */
    @TargetApi(14)
    public boolean onPreExecute() {
        this.updatesController = createUpdatesController(Build.VERSION.SDK_INT, (Application) getContext().getApplicationContext());
        return true;
    }
}
