package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.IdManager;
import java.util.Map;
import java.util.UUID;

class SessionMetadataCollector {
    private final Context context;
    private final IdManager idManager;
    private final String versionCode;
    private final String versionName;

    public SessionMetadataCollector(Context context2, IdManager idManager2, String str, String str2) {
        this.context = context2;
        this.idManager = idManager2;
        this.versionCode = str;
        this.versionName = str2;
    }

    public SessionEventMetadata getMetadata() {
        Map<IdManager.DeviceIdentifierType, String> r11 = this.idManager.m19202();
        String r6 = CommonUtils.m19138(this.context);
        String r7 = this.idManager.m19209();
        String r8 = this.idManager.m19201();
        return new SessionEventMetadata(this.idManager.m19210(), UUID.randomUUID().toString(), this.idManager.m19208(), this.idManager.m19205(), r11.get(IdManager.DeviceIdentifierType.FONT_TOKEN), r6, r7, r8, this.versionCode, this.versionName);
    }
}
