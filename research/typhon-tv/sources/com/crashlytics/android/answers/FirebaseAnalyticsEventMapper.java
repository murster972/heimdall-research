package com.crashlytics.android.answers;

import android.os.Bundle;
import com.crashlytics.android.answers.SessionEvent;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FirebaseAnalyticsEventMapper {
    private static final Set<String> EVENT_NAMES = new HashSet(Arrays.asList(new String[]{"app_clear_data", "app_exception", "app_remove", "app_upgrade", "app_install", "app_update", "firebase_campaign", "error", "first_open", "first_visit", "in_app_purchase", "notification_dismiss", "notification_foreground", "notification_open", "notification_receive", "os_update", "session_start", "user_engagement", "ad_exposure", "adunit_exposure", "ad_query", "ad_activeview", "ad_impression", "ad_click", "screen_view", "firebase_extra_parameter"}));
    private static final String FIREBASE_LEVEL_NAME = "level_name";
    private static final String FIREBASE_METHOD = "method";
    private static final String FIREBASE_RATING = "rating";
    private static final String FIREBASE_SUCCESS = "success";

    private String mapAttribute(String str) {
        if (str == null || str.length() == 0) {
            String str2 = str;
            return "fabric_unnamed_parameter";
        }
        String replaceAll = str.replaceAll("[^\\p{Alnum}_]+", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        if (replaceAll.startsWith("ga_") || replaceAll.startsWith("google_") || replaceAll.startsWith("firebase_") || !Character.isLetter(replaceAll.charAt(0))) {
            replaceAll = "fabric_" + replaceAll;
        }
        if (replaceAll.length() > 40) {
            String str3 = replaceAll;
            return replaceAll.substring(0, 40);
        }
        String str4 = replaceAll;
        return replaceAll;
    }

    private Integer mapBooleanValue(String str) {
        if (str == null) {
            return null;
        }
        return Integer.valueOf(str.equals("true") ? 1 : 0);
    }

    private void mapCustomEventAttributes(Bundle bundle, Map<String, Object> map) {
        for (Map.Entry next : map.entrySet()) {
            Object value = next.getValue();
            String mapAttribute = mapAttribute((String) next.getKey());
            if (value instanceof String) {
                bundle.putString(mapAttribute, next.getValue().toString());
            } else if (value instanceof Double) {
                bundle.putDouble(mapAttribute, ((Double) next.getValue()).doubleValue());
            } else if (value instanceof Long) {
                bundle.putLong(mapAttribute, ((Long) next.getValue()).longValue());
            } else if (value instanceof Integer) {
                bundle.putInt(mapAttribute, ((Integer) next.getValue()).intValue());
            }
        }
    }

    private String mapCustomEventName(String str) {
        if (str == null || str.length() == 0) {
            String str2 = str;
            return "fabric_unnamed_event";
        } else if (EVENT_NAMES.contains(str)) {
            String str3 = str;
            return "fabric_" + str;
        } else {
            String replaceAll = str.replaceAll("[^\\p{Alnum}_]+", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            if (replaceAll.startsWith("ga_") || replaceAll.startsWith("google_") || replaceAll.startsWith("firebase_") || !Character.isLetter(replaceAll.charAt(0))) {
                replaceAll = "fabric_" + replaceAll;
            }
            if (replaceAll.length() > 40) {
                replaceAll = replaceAll.substring(0, 40);
            }
            String str4 = replaceAll;
            return replaceAll;
        }
    }

    private Double mapDouble(Object obj) {
        String valueOf = String.valueOf(obj);
        if (valueOf == null) {
            return null;
        }
        return Double.valueOf(valueOf);
    }

    private Bundle mapPredefinedEvent(SessionEvent sessionEvent) {
        Bundle bundle = new Bundle();
        if ("purchase".equals(sessionEvent.predefinedType)) {
            putString(bundle, "item_id", (String) sessionEvent.predefinedAttributes.get("itemId"));
            putString(bundle, "item_name", (String) sessionEvent.predefinedAttributes.get("itemName"));
            putString(bundle, "item_category", (String) sessionEvent.predefinedAttributes.get("itemType"));
            putDouble(bundle, "value", mapPriceValue(sessionEvent.predefinedAttributes.get("itemPrice")));
            putString(bundle, "currency", (String) sessionEvent.predefinedAttributes.get("currency"));
        } else if ("addToCart".equals(sessionEvent.predefinedType)) {
            putString(bundle, "item_id", (String) sessionEvent.predefinedAttributes.get("itemId"));
            putString(bundle, "item_name", (String) sessionEvent.predefinedAttributes.get("itemName"));
            putString(bundle, "item_category", (String) sessionEvent.predefinedAttributes.get("itemType"));
            putDouble(bundle, "price", mapPriceValue(sessionEvent.predefinedAttributes.get("itemPrice")));
            putDouble(bundle, "value", mapPriceValue(sessionEvent.predefinedAttributes.get("itemPrice")));
            putString(bundle, "currency", (String) sessionEvent.predefinedAttributes.get("currency"));
            bundle.putLong("quantity", 1);
        } else if ("startCheckout".equals(sessionEvent.predefinedType)) {
            putLong(bundle, "quantity", Long.valueOf((long) ((Integer) sessionEvent.predefinedAttributes.get("itemCount")).intValue()));
            putDouble(bundle, "value", mapPriceValue(sessionEvent.predefinedAttributes.get("totalPrice")));
            putString(bundle, "currency", (String) sessionEvent.predefinedAttributes.get("currency"));
        } else if ("contentView".equals(sessionEvent.predefinedType)) {
            putString(bundle, "content_type", (String) sessionEvent.predefinedAttributes.get("contentType"));
            putString(bundle, "item_id", (String) sessionEvent.predefinedAttributes.get("contentId"));
            putString(bundle, "item_name", (String) sessionEvent.predefinedAttributes.get("contentName"));
        } else if ("search".equals(sessionEvent.predefinedType)) {
            putString(bundle, "search_term", (String) sessionEvent.predefinedAttributes.get("query"));
        } else if ("share".equals(sessionEvent.predefinedType)) {
            putString(bundle, FIREBASE_METHOD, (String) sessionEvent.predefinedAttributes.get(FIREBASE_METHOD));
            putString(bundle, "content_type", (String) sessionEvent.predefinedAttributes.get("contentType"));
            putString(bundle, "item_id", (String) sessionEvent.predefinedAttributes.get("contentId"));
            putString(bundle, "item_name", (String) sessionEvent.predefinedAttributes.get("contentName"));
        } else if ("rating".equals(sessionEvent.predefinedType)) {
            putString(bundle, "rating", String.valueOf(sessionEvent.predefinedAttributes.get("rating")));
            putString(bundle, "content_type", (String) sessionEvent.predefinedAttributes.get("contentType"));
            putString(bundle, "item_id", (String) sessionEvent.predefinedAttributes.get("contentId"));
            putString(bundle, "item_name", (String) sessionEvent.predefinedAttributes.get("contentName"));
        } else if ("signUp".equals(sessionEvent.predefinedType)) {
            putString(bundle, FIREBASE_METHOD, (String) sessionEvent.predefinedAttributes.get(FIREBASE_METHOD));
        } else if ("login".equals(sessionEvent.predefinedType)) {
            putString(bundle, FIREBASE_METHOD, (String) sessionEvent.predefinedAttributes.get(FIREBASE_METHOD));
        } else if ("invite".equals(sessionEvent.predefinedType)) {
            putString(bundle, FIREBASE_METHOD, (String) sessionEvent.predefinedAttributes.get(FIREBASE_METHOD));
        } else if ("levelStart".equals(sessionEvent.predefinedType)) {
            putString(bundle, FIREBASE_LEVEL_NAME, (String) sessionEvent.predefinedAttributes.get("levelName"));
        } else if ("levelEnd".equals(sessionEvent.predefinedType)) {
            putDouble(bundle, "score", mapDouble(sessionEvent.predefinedAttributes.get("score")));
            putString(bundle, FIREBASE_LEVEL_NAME, (String) sessionEvent.predefinedAttributes.get("levelName"));
            putInt(bundle, FIREBASE_SUCCESS, mapBooleanValue((String) sessionEvent.predefinedAttributes.get(FIREBASE_SUCCESS)));
        }
        mapCustomEventAttributes(bundle, sessionEvent.customAttributes);
        return bundle;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0055, code lost:
        if (r6.equals("purchase") != false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String mapPredefinedEventName(java.lang.String r6, boolean r7) {
        /*
            r5 = this;
            r3 = 2
            r2 = 1
            r0 = 0
            r1 = -1
            if (r7 == 0) goto L_0x0011
            int r4 = r6.hashCode()
            switch(r4) {
                case -902468296: goto L_0x002c;
                case 103149417: goto L_0x0037;
                case 1743324417: goto L_0x0021;
                default: goto L_0x000d;
            }
        L_0x000d:
            r4 = r1
        L_0x000e:
            switch(r4) {
                case 0: goto L_0x0042;
                case 1: goto L_0x0046;
                case 2: goto L_0x004a;
                default: goto L_0x0011;
            }
        L_0x0011:
            int r4 = r6.hashCode()
            switch(r4) {
                case -2131650889: goto L_0x00cd;
                case -1183699191: goto L_0x00b3;
                case -938102371: goto L_0x008f;
                case -906336856: goto L_0x0079;
                case -902468296: goto L_0x009a;
                case -389087554: goto L_0x006e;
                case 23457852: goto L_0x0058;
                case 103149417: goto L_0x00a6;
                case 109400031: goto L_0x0084;
                case 196004670: goto L_0x00c0;
                case 1664021448: goto L_0x0063;
                case 1743324417: goto L_0x004e;
                default: goto L_0x0018;
            }
        L_0x0018:
            r0 = r1
        L_0x0019:
            switch(r0) {
                case 0: goto L_0x00da;
                case 1: goto L_0x00df;
                case 2: goto L_0x00e4;
                case 3: goto L_0x00e9;
                case 4: goto L_0x00ee;
                case 5: goto L_0x00f3;
                case 6: goto L_0x00f8;
                case 7: goto L_0x00fd;
                case 8: goto L_0x0102;
                case 9: goto L_0x0107;
                case 10: goto L_0x010c;
                case 11: goto L_0x0111;
                default: goto L_0x001c;
            }
        L_0x001c:
            java.lang.String r0 = r5.mapCustomEventName(r6)
        L_0x0020:
            return r0
        L_0x0021:
            java.lang.String r4 = "purchase"
            boolean r4 = r6.equals(r4)
            if (r4 == 0) goto L_0x000d
            r4 = r0
            goto L_0x000e
        L_0x002c:
            java.lang.String r4 = "signUp"
            boolean r4 = r6.equals(r4)
            if (r4 == 0) goto L_0x000d
            r4 = r2
            goto L_0x000e
        L_0x0037:
            java.lang.String r4 = "login"
            boolean r4 = r6.equals(r4)
            if (r4 == 0) goto L_0x000d
            r4 = r3
            goto L_0x000e
        L_0x0042:
            java.lang.String r0 = "failed_ecommerce_purchase"
            goto L_0x0020
        L_0x0046:
            java.lang.String r0 = "failed_sign_up"
            goto L_0x0020
        L_0x004a:
            java.lang.String r0 = "failed_login"
            goto L_0x0020
        L_0x004e:
            java.lang.String r2 = "purchase"
            boolean r2 = r6.equals(r2)
            if (r2 == 0) goto L_0x0018
            goto L_0x0019
        L_0x0058:
            java.lang.String r0 = "addToCart"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = r2
            goto L_0x0019
        L_0x0063:
            java.lang.String r0 = "startCheckout"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = r3
            goto L_0x0019
        L_0x006e:
            java.lang.String r0 = "contentView"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = 3
            goto L_0x0019
        L_0x0079:
            java.lang.String r0 = "search"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = 4
            goto L_0x0019
        L_0x0084:
            java.lang.String r0 = "share"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = 5
            goto L_0x0019
        L_0x008f:
            java.lang.String r0 = "rating"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = 6
            goto L_0x0019
        L_0x009a:
            java.lang.String r0 = "signUp"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = 7
            goto L_0x0019
        L_0x00a6:
            java.lang.String r0 = "login"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = 8
            goto L_0x0019
        L_0x00b3:
            java.lang.String r0 = "invite"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = 9
            goto L_0x0019
        L_0x00c0:
            java.lang.String r0 = "levelStart"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = 10
            goto L_0x0019
        L_0x00cd:
            java.lang.String r0 = "levelEnd"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0018
            r0 = 11
            goto L_0x0019
        L_0x00da:
            java.lang.String r0 = "ecommerce_purchase"
            goto L_0x0020
        L_0x00df:
            java.lang.String r0 = "add_to_cart"
            goto L_0x0020
        L_0x00e4:
            java.lang.String r0 = "begin_checkout"
            goto L_0x0020
        L_0x00e9:
            java.lang.String r0 = "select_content"
            goto L_0x0020
        L_0x00ee:
            java.lang.String r0 = "search"
            goto L_0x0020
        L_0x00f3:
            java.lang.String r0 = "share"
            goto L_0x0020
        L_0x00f8:
            java.lang.String r0 = "rate_content"
            goto L_0x0020
        L_0x00fd:
            java.lang.String r0 = "sign_up"
            goto L_0x0020
        L_0x0102:
            java.lang.String r0 = "login"
            goto L_0x0020
        L_0x0107:
            java.lang.String r0 = "invite"
            goto L_0x0020
        L_0x010c:
            java.lang.String r0 = "level_start"
            goto L_0x0020
        L_0x0111:
            java.lang.String r0 = "level_end"
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crashlytics.android.answers.FirebaseAnalyticsEventMapper.mapPredefinedEventName(java.lang.String, boolean):java.lang.String");
    }

    private Double mapPriceValue(Object obj) {
        if (((Long) obj) == null) {
            return null;
        }
        return Double.valueOf(new BigDecimal(((Long) obj).longValue()).divide(AddToCartEvent.MICRO_CONSTANT).doubleValue());
    }

    private void putDouble(Bundle bundle, String str, Double d) {
        Double mapDouble = mapDouble(d);
        if (mapDouble != null) {
            bundle.putDouble(str, mapDouble.doubleValue());
        }
    }

    private void putInt(Bundle bundle, String str, Integer num) {
        if (num != null) {
            bundle.putInt(str, num.intValue());
        }
    }

    private void putLong(Bundle bundle, String str, Long l) {
        if (l != null) {
            bundle.putLong(str, l.longValue());
        }
    }

    private void putString(Bundle bundle, String str, String str2) {
        if (str2 != null) {
            bundle.putString(str, str2);
        }
    }

    public FirebaseAnalyticsEvent mapEvent(SessionEvent sessionEvent) {
        Bundle bundle;
        String mapCustomEventName;
        boolean z = SessionEvent.Type.CUSTOM.equals(sessionEvent.type) && sessionEvent.customType != null;
        boolean z2 = SessionEvent.Type.PREDEFINED.equals(sessionEvent.type) && sessionEvent.predefinedType != null;
        if (!z && !z2) {
            return null;
        }
        if (z2) {
            bundle = mapPredefinedEvent(sessionEvent);
        } else {
            bundle = new Bundle();
            if (sessionEvent.customAttributes != null) {
                mapCustomEventAttributes(bundle, sessionEvent.customAttributes);
            }
        }
        if (z2) {
            String str = (String) sessionEvent.predefinedAttributes.get(FIREBASE_SUCCESS);
            mapCustomEventName = mapPredefinedEventName(sessionEvent.predefinedType, str != null && !Boolean.parseBoolean(str));
        } else {
            mapCustomEventName = mapCustomEventName(sessionEvent.customType);
        }
        Fabric.m19034().m19090(Answers.TAG, "Logging event into firebase...");
        return new FirebaseAnalyticsEvent(mapCustomEventName, bundle);
    }
}
