package com.nononsenseapps.filepicker;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.nononsenseapps.filepicker.AbstractFilePickerFragment;

public class FileItemAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: 靐  reason: contains not printable characters */
    protected SortedList<T> f11756 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final LogicHandler<T> f11757;

    public FileItemAdapter(LogicHandler<T> logicHandler) {
        this.f11757 = logicHandler;
    }

    public int getItemCount() {
        if (this.f11756 == null) {
            return 0;
        }
        return this.f11756.m1411() + 1;
    }

    public int getItemViewType(int i) {
        if (i == 0) {
            return 0;
        }
        int i2 = i - 1;
        return this.f11757.m14810(i2, this.f11756.m1413(i2));
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (i == 0) {
            this.f11757.m14813((AbstractFilePickerFragment.HeaderViewHolder) viewHolder);
            return;
        }
        int i2 = i - 1;
        this.f11757.m14812((AbstractFilePickerFragment.DirViewHolder) viewHolder, i2, this.f11756.m1413(i2));
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return this.f11757.m14811(viewGroup, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14773(SortedList<T> sortedList) {
        this.f11756 = sortedList;
        notifyDataSetChanged();
    }
}
