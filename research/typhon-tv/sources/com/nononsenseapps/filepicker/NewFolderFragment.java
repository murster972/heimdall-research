package com.nononsenseapps.filepicker;

import android.support.v4.app.FragmentManager;
import com.nononsenseapps.filepicker.NewItemFragment;

public class NewFolderFragment extends NewItemFragment {
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m14814(FragmentManager fragmentManager, NewItemFragment.OnNewFolderListener onNewFolderListener) {
        NewFolderFragment newFolderFragment = new NewFolderFragment();
        newFolderFragment.m14817(onNewFolderListener);
        newFolderFragment.show(fragmentManager, "new_folder_fragment");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m14815(String str) {
        return Utils.m14821(str);
    }
}
