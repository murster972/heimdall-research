package com.nononsenseapps.filepicker;

import android.net.Uri;
import android.os.FileObserver;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.util.SortedList;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.widget.Toast;
import java.io.File;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class FilePickerFragment extends AbstractFilePickerFragment<File> {

    /* renamed from: ˆ  reason: contains not printable characters */
    protected boolean f11758 = false;

    /* renamed from: ˉ  reason: contains not printable characters */
    private File f11759 = null;

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (strArr.length == 0) {
            if (this.f11731 != null) {
                this.f11731.m14770();
            }
        } else if (iArr[0] != 0) {
            Toast.makeText(getContext(), R.string.nnf_permission_external_write_denied, 0).show();
            if (this.f11731 != null) {
                this.f11731.m14770();
            }
        } else if (this.f11759 != null) {
            m14746(this.f11759);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m14778(File file) {
        return file.getPath();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Uri m14782(File file) {
        return Uri.fromFile(file);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public File m14783() {
        return new File(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Loader<SortedList<File>> m14779() {
        return new AsyncTaskLoader<SortedList<File>>(getActivity()) {

            /* renamed from: 龘  reason: contains not printable characters */
            FileObserver f11761;

            /* access modifiers changed from: protected */
            public void onReset() {
                super.onReset();
                if (this.f11761 != null) {
                    this.f11761.stopWatching();
                    this.f11761 = null;
                }
            }

            /* access modifiers changed from: protected */
            public void onStartLoading() {
                super.onStartLoading();
                if (FilePickerFragment.this.f11736 == null || !((File) FilePickerFragment.this.f11736).isDirectory()) {
                    FilePickerFragment.this.f11736 = FilePickerFragment.this.m14783();
                }
                this.f11761 = new FileObserver(((File) FilePickerFragment.this.f11736).getPath(), 960) {
                    public void onEvent(int i, String str) {
                        AnonymousClass1.this.onContentChanged();
                    }
                };
                this.f11761.startWatching();
                forceLoad();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public SortedList<File> loadInBackground() {
                File[] listFiles = ((File) FilePickerFragment.this.f11736).listFiles();
                SortedList<File> sortedList = new SortedList<>(File.class, new SortedListAdapterCallback<File>(FilePickerFragment.this.m14756()) {
                    /* renamed from: 靐  reason: contains not printable characters */
                    public boolean areContentsTheSame(File file, File file2) {
                        return file.getAbsolutePath().equals(file2.getAbsolutePath()) && file.isFile() == file2.isFile();
                    }

                    /* renamed from: 齉  reason: contains not printable characters */
                    public boolean areItemsTheSame(File file, File file2) {
                        return areContentsTheSame(file, file2);
                    }

                    /* renamed from: 龘  reason: contains not printable characters */
                    public int compare(File file, File file2) {
                        return FilePickerFragment.this.m14795(file, file2);
                    }
                }, listFiles == null ? 0 : listFiles.length);
                sortedList.m1409();
                if (listFiles != null) {
                    for (File file : listFiles) {
                        if (FilePickerFragment.this.m14787(file)) {
                            sortedList.m1412(file);
                        }
                    }
                }
                sortedList.m1410();
                return sortedList;
            }
        };
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m14787(File file) {
        if (this.f11758 || !file.isHidden()) {
            return super.m14743(file);
        }
        return false;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public File m14780(File file) {
        return (!file.getPath().equals(m14783().getPath()) && file.getParentFile() != null) ? file.getParentFile() : file;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m14793(File file) {
        this.f11759 = file;
        requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14789(String str) {
        File file = new File((File) this.f11736, str);
        if (file.mkdir()) {
            m14746(file);
        } else {
            Toast.makeText(getActivity(), R.string.nnf_create_folder_error, 0).show();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m14784(File file) {
        return file.getName();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m14785(File file) {
        return file.isDirectory();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m14795(File file, File file2) {
        if (file.isDirectory() && !file2.isDirectory()) {
            return -1;
        }
        if (!file2.isDirectory() || file.isDirectory()) {
            return file.getName().compareToIgnoreCase(file2.getName());
        }
        return 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public File m14792(String str) {
        return new File(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m14791(File file) {
        return ContextCompat.checkSelfPermission(getContext(), "android.permission.WRITE_EXTERNAL_STORAGE") == 0;
    }
}
