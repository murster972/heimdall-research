package com.nononsenseapps.filepicker;

import android.net.Uri;
import android.support.v4.content.Loader;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public interface LogicHandler<T> {
    /* renamed from: ʼ  reason: contains not printable characters */
    String m14802(T t);

    /* renamed from: ʽ  reason: contains not printable characters */
    Loader<SortedList<T>> m14803();

    /* renamed from: ʽ  reason: contains not printable characters */
    T m14804(T t);

    /* renamed from: ˑ  reason: contains not printable characters */
    Uri m14805(T t);

    /* renamed from: ˑ  reason: contains not printable characters */
    T m14806();

    /* renamed from: ٴ  reason: contains not printable characters */
    String m14807(T t);

    /* renamed from: ᐧ  reason: contains not printable characters */
    boolean m14808(T t);

    /* renamed from: 齉  reason: contains not printable characters */
    T m14809(String str);

    /* renamed from: 龘  reason: contains not printable characters */
    int m14810(int i, T t);

    /* renamed from: 龘  reason: contains not printable characters */
    RecyclerView.ViewHolder m14811(ViewGroup viewGroup, int i);

    /* renamed from: 龘  reason: contains not printable characters */
    void m14812(AbstractFilePickerFragment<T>.DirViewHolder dirViewHolder, int i, T t);

    /* renamed from: 龘  reason: contains not printable characters */
    void m14813(AbstractFilePickerFragment<T>.HeaderViewHolder headerViewHolder);
}
