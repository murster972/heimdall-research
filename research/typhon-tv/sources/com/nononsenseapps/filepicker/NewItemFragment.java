package com.nononsenseapps.filepicker;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public abstract class NewItemFragment extends DialogFragment {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public OnNewFolderListener f11764 = null;

    public interface OnNewFolderListener {
        /* renamed from: 靐  reason: contains not printable characters */
        void m14819(String str);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public Dialog onCreateDialog(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.m526(R.layout.nnf_dialog_folder_name).m530(R.string.nnf_new_folder).m521(17039360, (DialogInterface.OnClickListener) null).m531(17039370, (DialogInterface.OnClickListener) null);
        AlertDialog r1 = builder.m525();
        r1.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface dialogInterface) {
                final AlertDialog alertDialog = (AlertDialog) dialogInterface;
                final EditText editText = (EditText) alertDialog.findViewById(R.id.edit_text);
                if (editText == null) {
                    throw new NullPointerException("Could not find an edit text in the dialog");
                }
                alertDialog.m515(-2).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        alertDialog.cancel();
                    }
                });
                final Button r3 = alertDialog.m515(-1);
                r3.setEnabled(false);
                r3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        String obj = editText.getText().toString();
                        if (NewItemFragment.this.m14818(obj)) {
                            if (NewItemFragment.this.f11764 != null) {
                                NewItemFragment.this.f11764.m14819(obj);
                            }
                            alertDialog.dismiss();
                        }
                    }
                });
                editText.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable editable) {
                        r3.setEnabled(NewItemFragment.this.m14818(editable.toString()));
                    }

                    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    }

                    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    }
                });
            }
        });
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14817(OnNewFolderListener onNewFolderListener) {
        this.f11764 = onNewFolderListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m14818(String str);
}
