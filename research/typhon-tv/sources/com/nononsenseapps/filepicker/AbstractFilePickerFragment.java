package com.nononsenseapps.filepicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.SortedList;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.nononsenseapps.filepicker.NewItemFragment;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public abstract class AbstractFilePickerFragment<T> extends Fragment implements LoaderManager.LoaderCallbacks<SortedList<T>>, LogicHandler<T>, NewItemFragment.OnNewFolderListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected boolean f11722 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected boolean f11723 = true;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected boolean f11724 = false;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected RecyclerView f11725;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected LinearLayoutManager f11726;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected EditText f11727;

    /* renamed from: ˊ  reason: contains not printable characters */
    protected boolean f11728 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    protected View f11729 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    protected View f11730 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    protected OnFilePickedListener f11731;

    /* renamed from: ٴ  reason: contains not printable characters */
    protected FileItemAdapter<T> f11732 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected TextView f11733;

    /* renamed from: 连任  reason: contains not printable characters */
    protected boolean f11734 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    protected final HashSet<AbstractFilePickerFragment<T>.CheckableViewHolder> f11735 = new HashSet<>();

    /* renamed from: 麤  reason: contains not printable characters */
    protected T f11736 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    protected int f11737 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final HashSet<T> f11738 = new HashSet<>();

    /* renamed from: ﹶ  reason: contains not printable characters */
    protected SortedList<T> f11739 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    protected Toast f11740 = null;

    public class CheckableViewHolder extends AbstractFilePickerFragment<T>.DirViewHolder {

        /* renamed from: 靐  reason: contains not printable characters */
        final /* synthetic */ AbstractFilePickerFragment f11745;

        /* renamed from: 龘  reason: contains not printable characters */
        public CheckBox f11746;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public CheckableViewHolder(final AbstractFilePickerFragment abstractFilePickerFragment, View view) {
            super(view);
            int i = 0;
            this.f11745 = abstractFilePickerFragment;
            boolean z = abstractFilePickerFragment.f11737 == 3;
            this.f11746 = (CheckBox) view.findViewById(R.id.checkbox);
            this.f11746.setVisibility((z || abstractFilePickerFragment.f11724) ? 8 : i);
            this.f11746.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    CheckableViewHolder.this.f11745.m14765((AbstractFilePickerFragment<T>.CheckableViewHolder) CheckableViewHolder.this);
                }
            });
        }

        public void onClick(View view) {
            this.f11745.m14762(view, (AbstractFilePickerFragment<T>.CheckableViewHolder) this);
        }

        public boolean onLongClick(View view) {
            return this.f11745.m14747(view, (AbstractFilePickerFragment<T>.CheckableViewHolder) this);
        }
    }

    public class DirViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        /* renamed from: 连任  reason: contains not printable characters */
        public T f11750;

        /* renamed from: 麤  reason: contains not printable characters */
        public TextView f11751;

        /* renamed from: 齉  reason: contains not printable characters */
        public View f11752;

        public DirViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
            this.f11752 = view.findViewById(R.id.item_icon);
            this.f11751 = (TextView) view.findViewById(16908308);
        }

        public void onClick(View view) {
            AbstractFilePickerFragment.this.m14763(view, (AbstractFilePickerFragment<T>.DirViewHolder) this);
        }

        public boolean onLongClick(View view) {
            return AbstractFilePickerFragment.this.m14748(view, (AbstractFilePickerFragment<T>.DirViewHolder) this);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /* renamed from: 龘  reason: contains not printable characters */
        final TextView f11754;

        public HeaderViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.f11754 = (TextView) view.findViewById(16908308);
        }

        public void onClick(View view) {
            AbstractFilePickerFragment.this.m14764(view, (AbstractFilePickerFragment<T>.HeaderViewHolder) this);
        }
    }

    public interface OnFilePickedListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m14770();

        /* renamed from: 龘  reason: contains not printable characters */
        void m14771(Uri uri);

        /* renamed from: 龘  reason: contains not printable characters */
        void m14772(List<Uri> list);
    }

    public AbstractFilePickerFragment() {
        setRetainInstance(true);
    }

    public void onActivityCreated(Bundle bundle) {
        String string;
        super.onActivityCreated(bundle);
        if (this.f11736 == null) {
            if (bundle != null) {
                this.f11737 = bundle.getInt("KEY_MODE", this.f11737);
                this.f11734 = bundle.getBoolean("KEY_ALLOW_DIR_CREATE", this.f11734);
                this.f11722 = bundle.getBoolean("KEY_ALLOW_MULTIPLE", this.f11722);
                this.f11723 = bundle.getBoolean("KEY_ALLOW_EXISTING_FILE", this.f11723);
                this.f11724 = bundle.getBoolean("KEY_SINGLE_CLICK", this.f11724);
                String string2 = bundle.getString("KEY_CURRENT_PATH");
                if (string2 != null) {
                    this.f11736 = m14809(string2.trim());
                }
            } else if (getArguments() != null) {
                this.f11737 = getArguments().getInt("KEY_MODE", this.f11737);
                this.f11734 = getArguments().getBoolean("KEY_ALLOW_DIR_CREATE", this.f11734);
                this.f11722 = getArguments().getBoolean("KEY_ALLOW_MULTIPLE", this.f11722);
                this.f11723 = getArguments().getBoolean("KEY_ALLOW_EXISTING_FILE", this.f11723);
                this.f11724 = getArguments().getBoolean("KEY_SINGLE_CLICK", this.f11724);
                if (getArguments().containsKey("KEY_START_PATH") && (string = getArguments().getString("KEY_START_PATH")) != null) {
                    T r0 = m14809(string.trim());
                    if (m14808(r0)) {
                        this.f11736 = r0;
                    } else {
                        this.f11736 = m14804(r0);
                        this.f11727.setText(m14807(r0));
                    }
                }
            }
        }
        m14749();
        if (this.f11736 == null) {
            this.f11736 = m14806();
        }
        m14746(this.f11736);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.f11731 = (OnFilePickedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFilePickedListener");
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    public Loader<SortedList<T>> onCreateLoader(int i, Bundle bundle) {
        return m14803();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.picker_actions, menu);
        menu.findItem(R.id.nnf_action_createdir).setVisible(this.f11734);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View r1 = m14755(layoutInflater, viewGroup);
        Toolbar toolbar = (Toolbar) r1.findViewById(R.id.nnf_picker_toolbar);
        if (toolbar != null) {
            m14759(toolbar);
        }
        this.f11725 = (RecyclerView) r1.findViewById(16908298);
        this.f11725.setHasFixedSize(true);
        this.f11726 = new LinearLayoutManager(getActivity());
        this.f11725.setLayoutManager(this.f11726);
        m14760(layoutInflater, this.f11725);
        this.f11732 = new FileItemAdapter<>(this);
        this.f11725.setAdapter(this.f11732);
        r1.findViewById(R.id.nnf_button_cancel).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AbstractFilePickerFragment.this.m14761(view);
            }
        });
        r1.findViewById(R.id.nnf_button_ok).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AbstractFilePickerFragment.this.m14745(view);
            }
        });
        r1.findViewById(R.id.nnf_button_ok_newfile).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AbstractFilePickerFragment.this.m14745(view);
            }
        });
        this.f11729 = r1.findViewById(R.id.nnf_newfile_button_container);
        this.f11730 = r1.findViewById(R.id.nnf_button_container);
        this.f11727 = (EditText) r1.findViewById(R.id.nnf_text_filename);
        this.f11727.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                AbstractFilePickerFragment.this.m14742();
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        this.f11733 = (TextView) r1.findViewById(R.id.nnf_current_dir);
        if (!(this.f11736 == null || this.f11733 == null)) {
            this.f11733.setText(m14802(this.f11736));
        }
        return r1;
    }

    public void onDetach() {
        super.onDetach();
        this.f11731 = null;
    }

    public void onLoaderReset(Loader<SortedList<T>> loader) {
        this.f11728 = false;
        this.f11732.m14773((SortedList) null);
        this.f11739 = null;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (R.id.nnf_action_createdir != menuItem.getItemId()) {
            return false;
        }
        FragmentActivity activity = getActivity();
        if (activity instanceof AppCompatActivity) {
            NewFolderFragment.m14814(((AppCompatActivity) activity).getSupportFragmentManager(), this);
        }
        return true;
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("KEY_CURRENT_PATH", this.f11736.toString());
        bundle.putBoolean("KEY_ALLOW_MULTIPLE", this.f11722);
        bundle.putBoolean("KEY_ALLOW_EXISTING_FILE", this.f11723);
        bundle.putBoolean("KEY_ALLOW_DIR_CREATE", this.f11734);
        bundle.putBoolean("KEY_SINGLE_CLICK", this.f11724);
        bundle.putInt("KEY_MODE", this.f11737);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m14740() {
        m14741(m14804(this.f11736));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m14741(T t) {
        if (!this.f11728) {
            this.f11738.clear();
            this.f11735.clear();
            m14746(t);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m14742() {
        Iterator<AbstractFilePickerFragment<T>.CheckableViewHolder> it2 = this.f11735.iterator();
        while (it2.hasNext()) {
            it2.next().f11746.setChecked(false);
        }
        this.f11735.clear();
        this.f11738.clear();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m14743(T t) {
        return m14808(t) || this.f11737 == 0 || this.f11737 == 2 || (this.f11737 == 3 && this.f11723);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m14744() {
        return this.f11727.getText().toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14745(View view) {
        if (this.f11731 != null) {
            if ((this.f11722 || this.f11737 == 0) && (this.f11738.isEmpty() || m14751() == null)) {
                if (this.f11740 == null) {
                    this.f11740 = Toast.makeText(getActivity(), R.string.nnf_select_something_first, 0);
                }
                this.f11740.show();
            } else if (this.f11737 == 3) {
                String r0 = m14744();
                this.f11731.m14771(r0.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) ? m14805(m14809(r0)) : m14805(m14809(Utils.m14820(m14802(this.f11736), r0))));
            } else if (this.f11722) {
                this.f11731.m14772(m14757(this.f11738));
            } else if (this.f11737 == 0) {
                this.f11731.m14771(m14805(m14751()));
            } else if (this.f11737 == 1) {
                this.f11731.m14771(m14805(this.f11736));
            } else if (this.f11738.isEmpty()) {
                this.f11731.m14771(m14805(this.f11736));
            } else {
                this.f11731.m14771(m14805(m14751()));
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m14746(T t) {
        if (m14750(t)) {
            this.f11736 = t;
            this.f11728 = true;
            getLoaderManager().restartLoader(0, (Bundle) null, this);
            return;
        }
        m14752(t);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m14747(View view, AbstractFilePickerFragment<T>.CheckableViewHolder checkableViewHolder) {
        if (3 == this.f11737) {
            this.f11727.setText(m14807(checkableViewHolder.f11750));
        }
        m14765(checkableViewHolder);
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m14748(View view, AbstractFilePickerFragment<T>.DirViewHolder dirViewHolder) {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m14749() {
        int i = 0;
        boolean z = this.f11737 == 3;
        this.f11729.setVisibility(z ? 0 : 8);
        View view = this.f11730;
        if (z) {
            i = 8;
        }
        view.setVisibility(i);
        if (!z && this.f11724) {
            getActivity().findViewById(R.id.nnf_button_ok).setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m14750(T t) {
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public T m14751() {
        Iterator<T> it2 = this.f11738.iterator();
        if (it2.hasNext()) {
            return it2.next();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m14752(T t) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m14753(int i, T t) {
        return m14769(t) ? 2 : 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public RecyclerView.ViewHolder m14754(ViewGroup viewGroup, int i) {
        switch (i) {
            case 0:
                return new HeaderViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.nnf_filepicker_listitem_dir, viewGroup, false));
            case 2:
                return new CheckableViewHolder(this, LayoutInflater.from(getActivity()).inflate(R.layout.nnf_filepicker_listitem_checkable, viewGroup, false));
            default:
                return new DirViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.nnf_filepicker_listitem_dir, viewGroup, false));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public View m14755(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.nnf_fragment_filepicker, viewGroup, false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public FileItemAdapter<T> m14756() {
        return new FileItemAdapter<>(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<Uri> m14757(Iterable<T> iterable) {
        ArrayList arrayList = new ArrayList();
        for (T r0 : iterable) {
            arrayList.add(m14805(r0));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void onLoadFinished(Loader<SortedList<T>> loader, SortedList<T> sortedList) {
        this.f11728 = false;
        this.f11738.clear();
        this.f11735.clear();
        this.f11739 = sortedList;
        this.f11732.m14773(sortedList);
        if (this.f11733 != null) {
            this.f11733.setText(m14802(this.f11736));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14759(Toolbar toolbar) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14760(LayoutInflater layoutInflater, RecyclerView recyclerView) {
        TypedArray obtainStyledAttributes = getActivity().obtainStyledAttributes(new int[]{R.attr.nnf_list_item_divider});
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        if (drawable != null) {
            recyclerView.addItemDecoration(new DividerItemDecoration(drawable));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14761(View view) {
        if (this.f11731 != null) {
            this.f11731.m14770();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14762(View view, AbstractFilePickerFragment<T>.CheckableViewHolder checkableViewHolder) {
        if (m14808(checkableViewHolder.f11750)) {
            m14741(checkableViewHolder.f11750);
            return;
        }
        m14747(view, checkableViewHolder);
        if (this.f11724) {
            m14745(view);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14763(View view, AbstractFilePickerFragment<T>.DirViewHolder dirViewHolder) {
        if (m14808(dirViewHolder.f11750)) {
            m14741(dirViewHolder.f11750);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14764(View view, AbstractFilePickerFragment<T>.HeaderViewHolder headerViewHolder) {
        m14740();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14765(AbstractFilePickerFragment<T>.CheckableViewHolder checkableViewHolder) {
        if (this.f11738.contains(checkableViewHolder.f11750)) {
            checkableViewHolder.f11746.setChecked(false);
            this.f11738.remove(checkableViewHolder.f11750);
            this.f11735.remove(checkableViewHolder);
            return;
        }
        if (!this.f11722) {
            m14742();
        }
        checkableViewHolder.f11746.setChecked(true);
        this.f11738.add(checkableViewHolder.f11750);
        this.f11735.add(checkableViewHolder);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14766(AbstractFilePickerFragment<T>.DirViewHolder dirViewHolder, int i, T t) {
        dirViewHolder.f11750 = t;
        dirViewHolder.f11752.setVisibility(m14808(t) ? 0 : 8);
        dirViewHolder.f11751.setText(m14807(t));
        if (!m14769(t)) {
            return;
        }
        if (this.f11738.contains(t)) {
            this.f11735.add((CheckableViewHolder) dirViewHolder);
            ((CheckableViewHolder) dirViewHolder).f11746.setChecked(true);
            return;
        }
        this.f11735.remove(dirViewHolder);
        ((CheckableViewHolder) dirViewHolder).f11746.setChecked(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14767(AbstractFilePickerFragment<T>.HeaderViewHolder headerViewHolder) {
        headerViewHolder.f11754.setText("..");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14768(String str, int i, boolean z, boolean z2, boolean z3, boolean z4) {
        if (i == 3 && z) {
            throw new IllegalArgumentException("MODE_NEW_FILE does not support 'allowMultiple'");
        } else if (!z4 || !z) {
            Bundle arguments = getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            if (str != null) {
                arguments.putString("KEY_START_PATH", str);
            }
            arguments.putBoolean("KEY_ALLOW_DIR_CREATE", z2);
            arguments.putBoolean("KEY_ALLOW_MULTIPLE", z);
            arguments.putBoolean("KEY_ALLOW_EXISTING_FILE", z3);
            arguments.putBoolean("KEY_SINGLE_CLICK", z4);
            arguments.putInt("KEY_MODE", i);
            setArguments(arguments);
        } else {
            throw new IllegalArgumentException("'singleClick' can not be used with 'allowMultiple'");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m14769(T t) {
        return m14808(t) ? (this.f11737 == 1 && this.f11722) || (this.f11737 == 2 && this.f11722) : this.f11737 == 0 || this.f11737 == 2 || this.f11723;
    }
}
