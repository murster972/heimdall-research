package com.nononsenseapps.filepicker;

import android.text.TextUtils;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class Utils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m14820(String str, String str2) {
        String str3 = str + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + str2;
        while (str3.contains("//")) {
            str3 = str3.replaceAll("//", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
        }
        return (str3.length() <= 1 || !str3.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) ? str3 : str3.substring(0, str3.length() - 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m14821(String str) {
        return !TextUtils.isEmpty(str) && !str.contains(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) && !str.equals(".") && !str.equals("..");
    }
}
