package com.nononsenseapps.filepicker;

import android.annotation.SuppressLint;
import android.os.Environment;
import java.io.File;

@SuppressLint({"Registered"})
public class FilePickerActivity extends AbstractFilePickerActivity<File> {
    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public AbstractFilePickerFragment<File> m14774(String str, int i, boolean z, boolean z2, boolean z3, boolean z4) {
        FilePickerFragment filePickerFragment = new FilePickerFragment();
        filePickerFragment.m14768(str != null ? str : Environment.getExternalStorageDirectory().getPath(), i, z, z2, z3, z4);
        return filePickerFragment;
    }
}
