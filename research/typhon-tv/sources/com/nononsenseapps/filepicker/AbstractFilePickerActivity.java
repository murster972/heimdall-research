package com.nononsenseapps.filepicker;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import com.nononsenseapps.filepicker.AbstractFilePickerFragment;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFilePickerActivity<T> extends AppCompatActivity implements AbstractFilePickerFragment.OnFilePickedListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f11716 = true;

    /* renamed from: 连任  reason: contains not printable characters */
    protected boolean f11717 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    protected int f11718 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    protected boolean f11719 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    protected boolean f11720 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    protected String f11721 = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.nnf_activity_filepicker);
        Intent intent = getIntent();
        if (intent != null) {
            this.f11721 = intent.getStringExtra("nononsense.intent.START_PATH");
            this.f11718 = intent.getIntExtra("nononsense.intent.MODE", this.f11718);
            this.f11720 = intent.getBooleanExtra("nononsense.intent.ALLOW_CREATE_DIR", this.f11720);
            this.f11719 = intent.getBooleanExtra("android.intent.extra.ALLOW_MULTIPLE", this.f11719);
            this.f11716 = intent.getBooleanExtra("android.intent.extra.ALLOW_EXISTING_FILE", this.f11716);
            this.f11717 = intent.getBooleanExtra("nononsense.intent.SINGLE_CLICK", this.f11717);
        }
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        AbstractFilePickerFragment abstractFilePickerFragment = (AbstractFilePickerFragment) supportFragmentManager.findFragmentByTag("filepicker_fragment");
        if (abstractFilePickerFragment == null) {
            abstractFilePickerFragment = m14736(this.f11721, this.f11718, this.f11719, this.f11720, this.f11716, this.f11717);
        }
        if (abstractFilePickerFragment != null) {
            supportFragmentManager.beginTransaction().replace(R.id.fragment, abstractFilePickerFragment, "filepicker_fragment").commit();
        }
        setResult(0);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract AbstractFilePickerFragment<T> m14736(String str, int i, boolean z, boolean z2, boolean z3, boolean z4);

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14737() {
        setResult(0);
        finish();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14738(Uri uri) {
        Intent intent = new Intent();
        intent.setData(uri);
        setResult(-1, intent);
        finish();
    }

    @TargetApi(16)
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14739(List<Uri> list) {
        Intent intent = new Intent();
        intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
        if (Build.VERSION.SDK_INT >= 16) {
            ClipData clipData = null;
            for (Uri next : list) {
                if (clipData == null) {
                    clipData = new ClipData("Paths", new String[0], new ClipData.Item(next));
                } else {
                    clipData.addItem(new ClipData.Item(next));
                }
            }
            intent.setClipData(clipData);
        } else {
            ArrayList arrayList = new ArrayList();
            for (Uri uri : list) {
                arrayList.add(uri.toString());
            }
            intent.putStringArrayListExtra("nononsense.intent.PATHS", arrayList);
        }
        setResult(-1, intent);
        finish();
    }
}
