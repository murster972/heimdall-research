package com.mucommander.commons.io;

import java.io.IOException;
import java.io.InputStream;

public class StreamUtils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m14547(InputStream inputStream, byte[] bArr) throws IOException {
        return m14548(inputStream, bArr, 0, bArr.length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m14548(InputStream inputStream, byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        if (i2 > 0) {
            do {
                int read = inputStream.read(bArr, i + i3, i2 - i3);
                if (read < 0) {
                    break;
                }
                i3 += read;
            } while (i3 < i2);
        }
        return i3;
    }
}
