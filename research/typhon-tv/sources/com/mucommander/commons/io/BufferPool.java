package com.mucommander.commons.io;

import java.util.Vector;

public class BufferPool {

    /* renamed from: 靐  reason: contains not printable characters */
    public static long f11578 = 10485760;

    /* renamed from: 麤  reason: contains not printable characters */
    private static Vector<BufferContainer> f11579 = new Vector<>();

    /* renamed from: 齉  reason: contains not printable characters */
    public static long f11580;

    /* renamed from: 龘  reason: contains not printable characters */
    public static int f11581 = 65536;

    public static abstract class BufferContainer {

        /* renamed from: 龘  reason: contains not printable characters */
        protected Object f11582;

        protected BufferContainer(Object obj) {
            this.f11582 = obj;
        }

        public boolean equals(Object obj) {
            return (obj instanceof BufferContainer) && this.f11582 == ((BufferContainer) obj).f11582;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public abstract int m14533();

        /* access modifiers changed from: protected */
        /* renamed from: 齉  reason: contains not printable characters */
        public abstract int m14534();

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Object m14535() {
            return this.f11582;
        }
    }

    public static abstract class BufferFactory {
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract BufferContainer m14536(Object obj);

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract Class<?> m14537();

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract Object m14538(int i);

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m14539(Class<?> cls) {
            return m14537().isAssignableFrom(cls);
        }
    }

    public static class ByteArrayFactory extends BufferFactory {
        /* renamed from: 龘  reason: contains not printable characters */
        public BufferContainer m14540(Object obj) {
            return new BufferContainer(obj) {
                /* access modifiers changed from: protected */
                /* renamed from: 靐  reason: contains not printable characters */
                public int m14543() {
                    return ((byte[]) this.f11582).length;
                }

                /* access modifiers changed from: protected */
                /* renamed from: 齉  reason: contains not printable characters */
                public int m14544() {
                    return m14543();
                }
            };
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Class<?> m14541() {
            return byte[].class;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Object m14542(int i) {
            return new byte[i];
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r0 = r10.m14538(r11);
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.lang.Object m14529(com.mucommander.commons.io.BufferPool.BufferFactory r10, int r11) {
        /*
            java.lang.Class<com.mucommander.commons.io.BufferPool> r5 = com.mucommander.commons.io.BufferPool.class
            monitor-enter(r5)
            java.util.Vector<com.mucommander.commons.io.BufferPool$BufferContainer> r4 = f11579     // Catch:{ all -> 0x0041 }
            int r3 = r4.size()     // Catch:{ all -> 0x0041 }
            r2 = 0
        L_0x000a:
            if (r2 >= r3) goto L_0x003c
            java.util.Vector<com.mucommander.commons.io.BufferPool$BufferContainer> r4 = f11579     // Catch:{ all -> 0x0041 }
            java.lang.Object r1 = r4.elementAt(r2)     // Catch:{ all -> 0x0041 }
            com.mucommander.commons.io.BufferPool$BufferContainer r1 = (com.mucommander.commons.io.BufferPool.BufferContainer) r1     // Catch:{ all -> 0x0041 }
            java.lang.Object r0 = r1.m14535()     // Catch:{ all -> 0x0041 }
            int r4 = r1.m14533()     // Catch:{ all -> 0x0041 }
            if (r4 != r11) goto L_0x0039
            java.lang.Class r4 = r0.getClass()     // Catch:{ all -> 0x0041 }
            boolean r4 = r10.m14539((java.lang.Class<?>) r4)     // Catch:{ all -> 0x0041 }
            if (r4 == 0) goto L_0x0039
            java.util.Vector<com.mucommander.commons.io.BufferPool$BufferContainer> r4 = f11579     // Catch:{ all -> 0x0041 }
            r4.removeElementAt(r2)     // Catch:{ all -> 0x0041 }
            long r6 = f11580     // Catch:{ all -> 0x0041 }
            int r4 = r1.m14534()     // Catch:{ all -> 0x0041 }
            long r8 = (long) r4     // Catch:{ all -> 0x0041 }
            long r6 = r6 - r8
            f11580 = r6     // Catch:{ all -> 0x0041 }
        L_0x0037:
            monitor-exit(r5)
            return r0
        L_0x0039:
            int r2 = r2 + 1
            goto L_0x000a
        L_0x003c:
            java.lang.Object r0 = r10.m14538((int) r11)     // Catch:{ all -> 0x0041 }
            goto L_0x0037
        L_0x0041:
            r4 = move-exception
            monitor-exit(r5)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mucommander.commons.io.BufferPool.m14529(com.mucommander.commons.io.BufferPool$BufferFactory, int):java.lang.Object");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized boolean m14530(Object obj, BufferFactory bufferFactory) {
        boolean z = false;
        synchronized (BufferPool.class) {
            if (obj == null) {
                throw new IllegalArgumentException("specified buffer is null");
            }
            BufferContainer r0 = bufferFactory.m14536(obj);
            if (!f11579.contains(r0)) {
                long r2 = (long) r0.m14534();
                if (f11578 == -1 || f11580 + r2 <= f11578) {
                    f11579.add(r0);
                    f11580 += r2;
                    z = true;
                }
            }
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized boolean m14531(byte[] bArr) {
        boolean r0;
        synchronized (BufferPool.class) {
            r0 = m14530((Object) bArr, (BufferFactory) new ByteArrayFactory());
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static synchronized byte[] m14532(int i) {
        byte[] bArr;
        synchronized (BufferPool.class) {
            bArr = (byte[]) m14529((BufferFactory) new ByteArrayFactory(), i);
        }
        return bArr;
    }
}
