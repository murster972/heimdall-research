package com.mucommander.commons.io;

import com.icu.text.CharsetDetector;
import com.icu.text.CharsetMatch;
import java.io.IOException;
import java.io.InputStream;

public class EncodingDetector {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m14545(InputStream inputStream) throws IOException {
        byte[] r0 = BufferPool.m14532(4096);
        try {
            return m14546(r0, 0, StreamUtils.m14547(inputStream, r0));
        } finally {
            BufferPool.m14531(r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m14546(byte[] bArr, int i, int i2) {
        if (i2 < 4) {
            return null;
        }
        if (i2 > 4096) {
            i2 = 4096;
        }
        if (i > 0 || i2 < bArr.length) {
            byte[] bArr2 = new byte[i2];
            System.arraycopy(bArr, i, bArr2, 0, i2);
            bArr = bArr2;
        }
        CharsetDetector charsetDetector = new CharsetDetector();
        charsetDetector.m13941(bArr);
        CharsetMatch r1 = charsetDetector.m13942();
        if (r1 != null) {
            return r1.m13944();
        }
        return null;
    }
}
