package com.flask.colorpicker.slider;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.Utils;
import com.flask.colorpicker.builder.PaintBuilder;

public class AlphaSlider extends AbsCustomSlider {

    /* renamed from: ʾ  reason: contains not printable characters */
    private Paint f20292 = PaintBuilder.m26242().m26243();

    /* renamed from: ʿ  reason: contains not printable characters */
    private Paint f20293 = PaintBuilder.m26242().m26245(-1).m26247(PorterDuff.Mode.CLEAR).m26243();

    /* renamed from: ˈ  reason: contains not printable characters */
    private Paint f20294 = PaintBuilder.m26242().m26243();

    /* renamed from: ٴ  reason: contains not printable characters */
    public int f20295;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Paint f20296 = PaintBuilder.m26242().m26243();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private ColorPickerView f20297;

    public AlphaSlider(Context context) {
        super(context);
    }

    public AlphaSlider(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AlphaSlider(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setColor(int i) {
        this.f20295 = i;
        this.f20286 = Utils.m26218(i);
        if (this.f20290 != null) {
            m26262();
            invalidate();
        }
    }

    public void setColorPicker(ColorPickerView colorPickerView) {
        this.f20297 = colorPickerView;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26266() {
        super.m26260();
        this.f20296.setShader(PaintBuilder.m26241(this.f20285 / 2));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26267(float f) {
        if (this.f20297 != null) {
            this.f20297.setAlphaValue(f);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26268(Canvas canvas) {
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        canvas.drawRect(0.0f, 0.0f, (float) width, (float) height, this.f20296);
        int max = Math.max(2, width / 256);
        for (int i = 0; i <= width; i += max) {
            this.f20294.setColor(this.f20295);
            this.f20294.setAlpha(Math.round(255.0f * (((float) i) / ((float) (width - 1)))));
            canvas.drawRect((float) i, 0.0f, (float) (i + max), (float) height, this.f20294);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26269(Canvas canvas, float f, float f2) {
        this.f20292.setColor(this.f20295);
        this.f20292.setAlpha(Math.round(this.f20286 * 255.0f));
        canvas.drawCircle(f, f2, (float) this.f20284, this.f20293);
        if (this.f20286 < 1.0f) {
            canvas.drawCircle(f, f2, ((float) this.f20284) * 0.75f, this.f20296);
        }
        canvas.drawCircle(f, f2, ((float) this.f20284) * 0.75f, this.f20292);
    }
}
