package com.flask.colorpicker.slider;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.flask.colorpicker.R;

public abstract class AbsCustomSlider extends View {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected int f20283;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected int f20284 = 20;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected int f20285 = 5;

    /* renamed from: ˑ  reason: contains not printable characters */
    protected float f20286 = 1.0f;

    /* renamed from: 连任  reason: contains not printable characters */
    protected OnValueChangedListener f20287;

    /* renamed from: 靐  reason: contains not printable characters */
    protected Canvas f20288;

    /* renamed from: 麤  reason: contains not printable characters */
    protected Canvas f20289;

    /* renamed from: 齉  reason: contains not printable characters */
    protected Bitmap f20290;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Bitmap f20291;

    public AbsCustomSlider(Context context) {
        super(context);
    }

    public AbsCustomSlider(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AbsCustomSlider(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f20290 != null && this.f20288 != null) {
            this.f20288.drawColor(0, PorterDuff.Mode.CLEAR);
            this.f20288.drawBitmap(this.f20290, (float) this.f20283, (float) ((getHeight() - this.f20290.getHeight()) / 2), (Paint) null);
            m26265(this.f20288, ((float) this.f20284) + (this.f20286 * ((float) (getWidth() - (this.f20284 * 2)))), ((float) getHeight()) / 2.0f);
            canvas.drawBitmap(this.f20291, 0.0f, 0.0f, (Paint) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i);
        int i3 = 0;
        if (mode == 0) {
            i3 = i;
        } else if (mode == Integer.MIN_VALUE) {
            i3 = View.MeasureSpec.getSize(i);
        } else if (mode == 1073741824) {
            i3 = View.MeasureSpec.getSize(i);
        }
        int mode2 = View.MeasureSpec.getMode(i2);
        int i4 = 0;
        if (mode2 == 0) {
            i4 = i2;
        } else if (mode2 == Integer.MIN_VALUE) {
            i4 = View.MeasureSpec.getSize(i2);
        } else if (mode2 == 1073741824) {
            i4 = View.MeasureSpec.getSize(i2);
        }
        setMeasuredDimension(i3, i4);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        m26262();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
            case 2:
                if (this.f20290 == null) {
                    return true;
                }
                this.f20286 = (motionEvent.getX() - ((float) this.f20283)) / ((float) this.f20290.getWidth());
                this.f20286 = Math.max(0.0f, Math.min(this.f20286, 1.0f));
                m26263(this.f20286);
                invalidate();
                return true;
            case 1:
                m26263(this.f20286);
                if (this.f20287 != null) {
                    this.f20287.m26273(this.f20286);
                }
                invalidate();
                return true;
            default:
                return true;
        }
    }

    public void setOnValueChangedListener(OnValueChangedListener onValueChangedListener) {
        this.f20287 = onValueChangedListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26260() {
        int width = getWidth();
        int height = getHeight();
        this.f20290 = Bitmap.createBitmap(width - (this.f20283 * 2), this.f20285, Bitmap.Config.ARGB_8888);
        this.f20289 = new Canvas(this.f20290);
        if (this.f20291 == null || this.f20291.getWidth() != width || this.f20291.getHeight() != height) {
            if (this.f20291 != null) {
                this.f20291.recycle();
            }
            this.f20291 = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            this.f20288 = new Canvas(this.f20291);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m26261(int i) {
        return getResources().getDimensionPixelSize(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26262() {
        this.f20284 = m26261(R.dimen.default_slider_handler_radius);
        this.f20285 = m26261(R.dimen.default_slider_bar_height);
        this.f20283 = this.f20284;
        if (this.f20290 == null) {
            m26260();
        }
        m26264(this.f20289);
        invalidate();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m26263(float f);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m26264(Canvas canvas);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m26265(Canvas canvas, float f, float f2);
}
