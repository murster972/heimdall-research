package com.flask.colorpicker.slider;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.Utils;
import com.flask.colorpicker.builder.PaintBuilder;

public class LightnessSlider extends AbsCustomSlider {

    /* renamed from: ʾ  reason: contains not printable characters */
    private Paint f20298 = PaintBuilder.m26242().m26245(-1).m26247(PorterDuff.Mode.CLEAR).m26243();

    /* renamed from: ʿ  reason: contains not printable characters */
    private ColorPickerView f20299;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Paint f20300 = PaintBuilder.m26242().m26243();

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f20301;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Paint f20302 = PaintBuilder.m26242().m26243();

    public LightnessSlider(Context context) {
        super(context);
    }

    public LightnessSlider(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LightnessSlider(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setColor(int i) {
        this.f20301 = i;
        this.f20286 = Utils.m26217(i);
        if (this.f20290 != null) {
            m26262();
            invalidate();
        }
    }

    public void setColorPicker(ColorPickerView colorPickerView) {
        this.f20299 = colorPickerView;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26270(float f) {
        if (this.f20299 != null) {
            this.f20299.setLightness(f);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26271(Canvas canvas) {
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        float[] fArr = new float[3];
        Color.colorToHSV(this.f20301, fArr);
        int max = Math.max(2, width / 256);
        for (int i = 0; i <= width; i += max) {
            fArr[2] = ((float) i) / ((float) (width - 1));
            this.f20302.setColor(Color.HSVToColor(fArr));
            canvas.drawRect((float) i, 0.0f, (float) (i + max), (float) height, this.f20302);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26272(Canvas canvas, float f, float f2) {
        this.f20300.setColor(Utils.m26221(this.f20301, this.f20286));
        canvas.drawCircle(f, f2, (float) this.f20284, this.f20298);
        canvas.drawCircle(f, f2, ((float) this.f20284) * 0.75f, this.f20300);
    }
}
