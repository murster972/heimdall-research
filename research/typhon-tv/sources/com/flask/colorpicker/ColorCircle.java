package com.flask.colorpicker;

import android.graphics.Color;

public class ColorCircle {

    /* renamed from: 连任  reason: contains not printable characters */
    private int f20218;

    /* renamed from: 靐  reason: contains not printable characters */
    private float f20219;

    /* renamed from: 麤  reason: contains not printable characters */
    private float[] f20220;

    /* renamed from: 齉  reason: contains not printable characters */
    private float[] f20221 = new float[3];

    /* renamed from: 龘  reason: contains not printable characters */
    private float f20222;

    public ColorCircle(float f, float f2, float[] fArr) {
        m26207(f, f2, fArr);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public float m26203() {
        return this.f20219;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public float[] m26204() {
        return this.f20221;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public double m26205(float f, float f2) {
        double d = (double) (this.f20222 - f);
        double d2 = (double) (this.f20219 - f2);
        return (d * d) + (d2 * d2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public float m26206() {
        return this.f20222;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26207(float f, float f2, float[] fArr) {
        this.f20222 = f;
        this.f20219 = f2;
        this.f20221[0] = fArr[0];
        this.f20221[1] = fArr[1];
        this.f20221[2] = fArr[2];
        this.f20218 = Color.HSVToColor(this.f20221);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public float[] m26208(float f) {
        if (this.f20220 == null) {
            this.f20220 = (float[]) this.f20221.clone();
        }
        this.f20220[0] = this.f20221[0];
        this.f20220[1] = this.f20221[1];
        this.f20220[2] = f;
        return this.f20220;
    }
}
