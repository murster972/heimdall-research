package com.flask.colorpicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.flask.colorpicker.builder.ColorWheelRendererBuilder;
import com.flask.colorpicker.builder.PaintBuilder;
import com.flask.colorpicker.renderer.ColorWheelRenderOption;
import com.flask.colorpicker.renderer.ColorWheelRenderer;
import com.flask.colorpicker.slider.AlphaSlider;
import com.flask.colorpicker.slider.LightnessSlider;
import java.util.ArrayList;
import java.util.Iterator;

public class ColorPickerView extends View {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f20223 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Integer[] f20224 = {null, null, null, null, null};

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f20225 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Paint f20226 = PaintBuilder.m26242().m26245(-16777216).m26243();

    /* renamed from: ʿ  reason: contains not printable characters */
    private Paint f20227 = PaintBuilder.m26242().m26243();

    /* renamed from: ˆ  reason: contains not printable characters */
    private EditText f20228;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Paint f20229 = PaintBuilder.m26242().m26245(-1).m26243();

    /* renamed from: ˉ  reason: contains not printable characters */
    private TextWatcher f20230 = new TextWatcher() {
        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            try {
                ColorPickerView.this.setColor(Color.parseColor(charSequence.toString()), false);
            } catch (Exception e) {
            }
        }
    };

    /* renamed from: ˊ  reason: contains not printable characters */
    private ArrayList<OnColorSelectedListener> f20231 = new ArrayList<>();

    /* renamed from: ˋ  reason: contains not printable characters */
    private LightnessSlider f20232;

    /* renamed from: ˎ  reason: contains not printable characters */
    private AlphaSlider f20233;

    /* renamed from: ˏ  reason: contains not printable characters */
    private LinearLayout f20234;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Integer f20235;

    /* renamed from: י  reason: contains not printable characters */
    private ColorWheelRenderer f20236;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f20237;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Integer f20238;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Paint f20239 = PaintBuilder.m26242().m26245(0).m26243();

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f20240;

    /* renamed from: 连任  reason: contains not printable characters */
    private float f20241 = 1.0f;

    /* renamed from: 靐  reason: contains not printable characters */
    private Canvas f20242;

    /* renamed from: 麤  reason: contains not printable characters */
    private float f20243 = 1.0f;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f20244 = 10;

    /* renamed from: 龘  reason: contains not printable characters */
    private Bitmap f20245;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private ColorCircle f20246;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private ArrayList<OnColorChangedListener> f20247 = new ArrayList<>();

    public enum WHEEL_TYPE {
        FLOWER,
        CIRCLE;

        public static WHEEL_TYPE indexOf(int i) {
            switch (i) {
                case 0:
                    return FLOWER;
                case 1:
                    return CIRCLE;
                default:
                    return FLOWER;
            }
        }
    }

    public ColorPickerView(Context context) {
        super(context);
        m26213(context, (AttributeSet) null);
    }

    public ColorPickerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m26213(context, attributeSet);
    }

    public ColorPickerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m26213(context, attributeSet);
    }

    private void setColorPreviewColor(int i) {
        if (this.f20234 != null && this.f20224 != null && this.f20225 <= this.f20224.length && this.f20224[this.f20225] != null && this.f20234.getChildCount() != 0 && this.f20234.getVisibility() == 0) {
            View childAt = this.f20234.getChildAt(this.f20225);
            if (childAt instanceof LinearLayout) {
                ((ImageView) ((LinearLayout) childAt).findViewById(R.id.image_preview)).setImageDrawable(new CircleColorDrawable(i));
            }
        }
    }

    private void setColorText(int i) {
        if (this.f20228 != null) {
            this.f20228.setText(Utils.m26222(i, this.f20233 != null));
        }
    }

    private void setColorToSliders(int i) {
        if (this.f20232 != null) {
            this.f20232.setColor(i);
        }
        if (this.f20233 != null) {
            this.f20233.setColor(i);
        }
    }

    private void setHighlightedColor(int i) {
        int childCount = this.f20234.getChildCount();
        if (childCount != 0 && this.f20234.getVisibility() == 0) {
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = this.f20234.getChildAt(i2);
                if (childAt instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (i2 == i) {
                        linearLayout.setBackgroundColor(-1);
                    } else {
                        linearLayout.setBackgroundColor(0);
                    }
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26209() {
        this.f20242.drawColor(0, PorterDuff.Mode.CLEAR);
        if (this.f20236 != null) {
            float width = ((float) this.f20242.getWidth()) / 2.0f;
            float f = (width - 2.05f) - (width / ((float) this.f20244));
            float f2 = (f / ((float) (this.f20244 - 1))) / 2.0f;
            ColorWheelRenderOption r1 = this.f20236.m26256();
            r1.f20277 = this.f20244;
            r1.f20274 = f;
            r1.f20276 = f2;
            r1.f20275 = 2.05f;
            r1.f20273 = this.f20241;
            r1.f20271 = this.f20243;
            r1.f20272 = this.f20242;
            this.f20236.m26257(r1);
            this.f20236.m26255();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ColorCircle m26210(float f, float f2) {
        ColorCircle colorCircle = null;
        double d = Double.MAX_VALUE;
        for (ColorCircle next : this.f20236.m26254()) {
            double r2 = next.m26205(f, f2);
            if (d > r2) {
                d = r2;
                colorCircle = next;
            }
        }
        return colorCircle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ColorCircle m26211(int i) {
        float[] fArr = new float[3];
        Color.colorToHSV(i, fArr);
        ColorCircle colorCircle = null;
        double d = Double.MAX_VALUE;
        double cos = ((double) fArr[1]) * Math.cos((((double) fArr[0]) * 3.141592653589793d) / 180.0d);
        double sin = ((double) fArr[1]) * Math.sin((((double) fArr[0]) * 3.141592653589793d) / 180.0d);
        for (ColorCircle next : this.f20236.m26254()) {
            float[] r10 = next.m26204();
            double cos2 = cos - (((double) r10[1]) * Math.cos((((double) r10[0]) * 3.141592653589793d) / 180.0d));
            double sin2 = sin - (((double) r10[1]) * Math.sin((((double) r10[0]) * 3.141592653589793d) / 180.0d));
            double d2 = (cos2 * cos2) + (sin2 * sin2);
            if (d2 < d) {
                d = d2;
                colorCircle = next;
            }
        }
        return colorCircle;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26212() {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (measuredHeight < measuredWidth) {
            measuredWidth = measuredHeight;
        }
        if (measuredWidth > 0) {
            if (this.f20245 == null) {
                this.f20245 = Bitmap.createBitmap(measuredWidth, measuredWidth, Bitmap.Config.ARGB_8888);
                this.f20242 = new Canvas(this.f20245);
                this.f20227.setShader(PaintBuilder.m26241(8));
            }
            m26209();
            invalidate();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26213(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ColorPickerPreference);
        this.f20244 = obtainStyledAttributes.getInt(R.styleable.ColorPickerPreference_density, 10);
        this.f20235 = Integer.valueOf(obtainStyledAttributes.getInt(R.styleable.ColorPickerPreference_initialColor, -1));
        this.f20238 = Integer.valueOf(obtainStyledAttributes.getInt(R.styleable.ColorPickerPreference_pickerColorEditTextColor, -1));
        ColorWheelRenderer r0 = ColorWheelRendererBuilder.m26239(WHEEL_TYPE.indexOf(obtainStyledAttributes.getInt(R.styleable.ColorPickerPreference_wheelType, 0)));
        this.f20237 = obtainStyledAttributes.getResourceId(R.styleable.ColorPickerPreference_alphaSliderView, 0);
        this.f20240 = obtainStyledAttributes.getResourceId(R.styleable.ColorPickerPreference_lightnessSliderView, 0);
        setRenderer(r0);
        setDensity(this.f20244);
        setInitialColor(this.f20235.intValue(), true);
        obtainStyledAttributes.recycle();
    }

    public Integer[] getAllColors() {
        return this.f20224;
    }

    public int getSelectedColor() {
        int i = 0;
        if (this.f20246 != null) {
            i = Color.HSVToColor(this.f20246.m26208(this.f20243));
        }
        return Utils.m26220(this.f20241, i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(this.f20223);
        if (this.f20245 != null) {
            canvas.drawBitmap(this.f20245, 0.0f, 0.0f, (Paint) null);
        }
        if (this.f20246 != null) {
            float width = (((((float) canvas.getWidth()) / 2.0f) - 2.05f) / ((float) this.f20244)) / 2.0f;
            this.f20239.setColor(Color.HSVToColor(this.f20246.m26208(this.f20243)));
            this.f20239.setAlpha((int) (this.f20241 * 255.0f));
            canvas.drawCircle(this.f20246.m26206(), this.f20246.m26203(), width * 2.0f, this.f20229);
            canvas.drawCircle(this.f20246.m26206(), this.f20246.m26203(), 1.5f * width, this.f20226);
            canvas.drawCircle(this.f20246.m26206(), this.f20246.m26203(), width, this.f20227);
            canvas.drawCircle(this.f20246.m26206(), this.f20246.m26203(), width, this.f20239);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.f20237 != 0) {
            setAlphaSlider((AlphaSlider) getRootView().findViewById(this.f20237));
        }
        if (this.f20240 != 0) {
            setLightnessSlider((LightnessSlider) getRootView().findViewById(this.f20240));
        }
        m26212();
        this.f20246 = m26211(this.f20235.intValue());
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i);
        int i3 = 0;
        if (mode == 0) {
            i3 = i;
        } else if (mode == Integer.MIN_VALUE) {
            i3 = View.MeasureSpec.getSize(i);
        } else if (mode == 1073741824) {
            i3 = View.MeasureSpec.getSize(i);
        }
        int mode2 = View.MeasureSpec.getMode(i2);
        int i4 = 0;
        if (mode2 == 0) {
            i4 = i;
        } else if (mode2 == Integer.MIN_VALUE) {
            i4 = View.MeasureSpec.getSize(i2);
        } else if (mode == 1073741824) {
            i4 = View.MeasureSpec.getSize(i2);
        }
        int i5 = i3;
        if (i4 < i3) {
            i5 = i4;
        }
        setMeasuredDimension(i5, i5);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        m26212();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
            case 2:
                int selectedColor = getSelectedColor();
                this.f20246 = m26210(motionEvent.getX(), motionEvent.getY());
                int selectedColor2 = getSelectedColor();
                m26214(selectedColor, selectedColor2);
                this.f20235 = Integer.valueOf(selectedColor2);
                setColorToSliders(selectedColor2);
                invalidate();
                return true;
            case 1:
                int selectedColor3 = getSelectedColor();
                if (this.f20231 != null) {
                    Iterator<OnColorSelectedListener> it2 = this.f20231.iterator();
                    while (it2.hasNext()) {
                        try {
                            it2.next().m26216(selectedColor3);
                        } catch (Exception e) {
                        }
                    }
                }
                setColorToSliders(selectedColor3);
                setColorText(selectedColor3);
                setColorPreviewColor(selectedColor3);
                invalidate();
                return true;
            default:
                return true;
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        m26212();
        this.f20246 = m26211(this.f20235.intValue());
    }

    public void setAlphaSlider(AlphaSlider alphaSlider) {
        this.f20233 = alphaSlider;
        if (alphaSlider != null) {
            this.f20233.setColorPicker(this);
            this.f20233.setColor(getSelectedColor());
        }
    }

    public void setAlphaValue(float f) {
        int selectedColor = getSelectedColor();
        this.f20241 = f;
        this.f20235 = Integer.valueOf(Color.HSVToColor(Utils.m26219(this.f20241), this.f20246.m26208(this.f20243)));
        if (this.f20228 != null) {
            this.f20228.setText(Utils.m26222(this.f20235.intValue(), this.f20233 != null));
        }
        if (!(this.f20232 == null || this.f20235 == null)) {
            this.f20232.setColor(this.f20235.intValue());
        }
        m26214(selectedColor, this.f20235.intValue());
        m26212();
        invalidate();
    }

    public void setColor(int i, boolean z) {
        setInitialColor(i, z);
        m26212();
        invalidate();
    }

    public void setColorEdit(EditText editText) {
        this.f20228 = editText;
        if (this.f20228 != null) {
            this.f20228.setVisibility(0);
            this.f20228.addTextChangedListener(this.f20230);
            setColorEditTextColor(this.f20238.intValue());
        }
    }

    public void setColorEditTextColor(int i) {
        this.f20238 = Integer.valueOf(i);
        if (this.f20228 != null) {
            this.f20228.setTextColor(i);
        }
    }

    public void setColorPreview(LinearLayout linearLayout, Integer num) {
        if (linearLayout != null) {
            this.f20234 = linearLayout;
            if (num == null) {
                num = 0;
            }
            int childCount = linearLayout.getChildCount();
            if (childCount != 0 && linearLayout.getVisibility() == 0) {
                for (int i = 0; i < childCount; i++) {
                    View childAt = linearLayout.getChildAt(i);
                    if (childAt instanceof LinearLayout) {
                        LinearLayout linearLayout2 = (LinearLayout) childAt;
                        if (i == num.intValue()) {
                            linearLayout2.setBackgroundColor(-1);
                        }
                        ImageView imageView = (ImageView) linearLayout2.findViewById(R.id.image_preview);
                        imageView.setClickable(true);
                        imageView.setTag(Integer.valueOf(i));
                        imageView.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                Object tag;
                                if (view != null && (tag = view.getTag()) != null && (tag instanceof Integer)) {
                                    ColorPickerView.this.setSelectedColor(((Integer) tag).intValue());
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    public void setDensity(int i) {
        this.f20244 = Math.max(2, i);
        invalidate();
    }

    public void setInitialColor(int i, boolean z) {
        float[] fArr = new float[3];
        Color.colorToHSV(i, fArr);
        this.f20241 = Utils.m26218(i);
        this.f20243 = fArr[2];
        this.f20224[this.f20225] = Integer.valueOf(i);
        this.f20235 = Integer.valueOf(i);
        setColorPreviewColor(i);
        setColorToSliders(i);
        if (this.f20228 != null && z) {
            setColorText(i);
        }
        this.f20246 = m26211(i);
    }

    public void setInitialColors(Integer[] numArr, int i) {
        this.f20224 = numArr;
        this.f20225 = i;
        Integer num = this.f20224[this.f20225];
        if (num == null) {
            num = -1;
        }
        setInitialColor(num.intValue(), true);
    }

    public void setLightness(float f) {
        int selectedColor = getSelectedColor();
        this.f20243 = f;
        this.f20235 = Integer.valueOf(Color.HSVToColor(Utils.m26219(this.f20241), this.f20246.m26208(f)));
        if (this.f20228 != null) {
            this.f20228.setText(Utils.m26222(this.f20235.intValue(), this.f20233 != null));
        }
        if (!(this.f20233 == null || this.f20235 == null)) {
            this.f20233.setColor(this.f20235.intValue());
        }
        m26214(selectedColor, this.f20235.intValue());
        m26212();
        invalidate();
    }

    public void setLightnessSlider(LightnessSlider lightnessSlider) {
        this.f20232 = lightnessSlider;
        if (lightnessSlider != null) {
            this.f20232.setColorPicker(this);
            this.f20232.setColor(getSelectedColor());
        }
    }

    public void setRenderer(ColorWheelRenderer colorWheelRenderer) {
        this.f20236 = colorWheelRenderer;
        invalidate();
    }

    public void setSelectedColor(int i) {
        if (this.f20224 != null && this.f20224.length >= i) {
            this.f20225 = i;
            setHighlightedColor(i);
            Integer num = this.f20224[i];
            if (num != null) {
                setColor(num.intValue(), true);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26214(int i, int i2) {
        if (this.f20247 != null && i != i2) {
            Iterator<OnColorChangedListener> it2 = this.f20247.iterator();
            while (it2.hasNext()) {
                try {
                    it2.next().m26215(i2);
                } catch (Exception e) {
                }
            }
        }
    }
}
