package com.flask.colorpicker;

import android.graphics.Color;
import android.support.v4.view.ViewCompat;

public class Utils {
    /* renamed from: 靐  reason: contains not printable characters */
    public static float m26217(int i) {
        float[] fArr = new float[3];
        Color.colorToHSV(i, fArr);
        return fArr[2];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static float m26218(int i) {
        return ((float) Color.alpha(i)) / 255.0f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m26219(float f) {
        return Math.round(255.0f * f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m26220(float f, int i) {
        return (m26219(f) << 24) | (16777215 & i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m26221(int i, float f) {
        float[] fArr = new float[3];
        Color.colorToHSV(i, fArr);
        fArr[2] = f;
        return Color.HSVToColor(fArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m26222(int i, boolean z) {
        return String.format(z ? "#%08X" : "#%06X", new Object[]{Integer.valueOf((z ? -1 : ViewCompat.MEASURED_SIZE_MASK) & i)}).toUpperCase();
    }
}
