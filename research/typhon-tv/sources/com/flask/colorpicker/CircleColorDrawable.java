package com.flask.colorpicker;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import com.flask.colorpicker.builder.PaintBuilder;

public class CircleColorDrawable extends ColorDrawable {

    /* renamed from: 靐  reason: contains not printable characters */
    private Paint f20214 = PaintBuilder.m26242().m26246(Paint.Style.STROKE).m26244(this.f20217).m26245(-1).m26243();

    /* renamed from: 麤  reason: contains not printable characters */
    private Paint f20215 = PaintBuilder.m26242().m26248(PaintBuilder.m26241(16)).m26243();

    /* renamed from: 齉  reason: contains not printable characters */
    private Paint f20216 = PaintBuilder.m26242().m26246(Paint.Style.FILL).m26245(0).m26243();

    /* renamed from: 龘  reason: contains not printable characters */
    private float f20217;

    public CircleColorDrawable() {
    }

    public CircleColorDrawable(int i) {
        super(i);
    }

    public void draw(Canvas canvas) {
        canvas.drawColor(0);
        float width = ((float) canvas.getWidth()) / 2.0f;
        this.f20217 = width / 12.0f;
        this.f20214.setStrokeWidth(this.f20217);
        this.f20216.setColor(getColor());
        canvas.drawCircle(width, width, width - (this.f20217 * 1.5f), this.f20215);
        canvas.drawCircle(width, width, width - (this.f20217 * 1.5f), this.f20216);
        canvas.drawCircle(width, width, width - this.f20217, this.f20214);
    }

    public void setColor(int i) {
        super.setColor(i);
        invalidateSelf();
    }
}
