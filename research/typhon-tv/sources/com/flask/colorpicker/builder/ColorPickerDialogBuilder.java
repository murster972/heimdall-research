package com.flask.colorpicker.builder;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.R;
import com.flask.colorpicker.Utils;
import com.flask.colorpicker.slider.AlphaSlider;
import com.flask.colorpicker.slider.LightnessSlider;

public class ColorPickerDialogBuilder {

    /* renamed from: ʻ  reason: contains not printable characters */
    private EditText f20251;

    /* renamed from: ʼ  reason: contains not printable characters */
    private LinearLayout f20252;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f20253;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f20254;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Integer[] f20255;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f20256;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f20257;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f20258;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f20259;

    /* renamed from: 连任  reason: contains not printable characters */
    private AlphaSlider f20260;

    /* renamed from: 靐  reason: contains not printable characters */
    private LinearLayout f20261;

    /* renamed from: 麤  reason: contains not printable characters */
    private LightnessSlider f20262;

    /* renamed from: 齉  reason: contains not printable characters */
    private ColorPickerView f20263;

    /* renamed from: 龘  reason: contains not printable characters */
    private AlertDialog.Builder f20264;

    private ColorPickerDialogBuilder(Context context) {
        this(context, 0);
    }

    private ColorPickerDialogBuilder(Context context, int i) {
        this.f20253 = true;
        this.f20257 = true;
        this.f20258 = false;
        this.f20259 = false;
        this.f20256 = 1;
        this.f20254 = 0;
        this.f20255 = new Integer[]{null, null, null, null, null};
        this.f20254 = m26225(context, R.dimen.default_slider_margin);
        int r0 = m26225(context, R.dimen.default_slider_margin_btw_title);
        this.f20264 = new AlertDialog.Builder(context, i);
        this.f20261 = new LinearLayout(context);
        this.f20261.setOrientation(1);
        this.f20261.setGravity(1);
        this.f20261.setPadding(this.f20254, r0, this.f20254, this.f20254);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, 0);
        layoutParams.weight = 1.0f;
        this.f20263 = new ColorPickerView(context);
        this.f20261.addView(this.f20263, layoutParams);
        this.f20264.m522((View) this.f20261);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m26224(Integer[] numArr) {
        Integer r0 = m26227(numArr);
        if (r0 == null) {
            return -1;
        }
        return numArr[r0.intValue()].intValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m26225(Context context, int i) {
        return (int) (context.getResources().getDimension(i) + 0.5f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ColorPickerDialogBuilder m26226(Context context) {
        return new ColorPickerDialogBuilder(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Integer m26227(Integer[] numArr) {
        int i = 0;
        int i2 = 0;
        while (i2 < numArr.length && numArr[i2] != null) {
            i = Integer.valueOf((i2 + 1) / 2);
            i2++;
        }
        return i;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26228(DialogInterface dialogInterface, ColorPickerClickListener colorPickerClickListener) {
        colorPickerClickListener.m26223(dialogInterface, this.f20263.getSelectedColor(), this.f20263.getAllColors());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ColorPickerDialogBuilder m26230(int i) throws IndexOutOfBoundsException {
        if (i < 1 || i > 5) {
            throw new IndexOutOfBoundsException("Picker Can Only Support 1-5 Colors");
        }
        this.f20256 = i;
        if (this.f20256 > 1) {
            this.f20259 = true;
        }
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ColorPickerDialogBuilder m26231(boolean z) {
        this.f20258 = z;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public AlertDialog m26232() {
        Context r2 = this.f20264.m529();
        this.f20263.setInitialColors(this.f20255, m26227(this.f20255).intValue());
        if (this.f20253) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, m26225(r2, R.dimen.default_slider_height));
            this.f20262 = new LightnessSlider(r2);
            this.f20262.setLayoutParams(layoutParams);
            this.f20261.addView(this.f20262);
            this.f20263.setLightnessSlider(this.f20262);
            this.f20262.setColor(m26224(this.f20255));
        }
        if (this.f20257) {
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, m26225(r2, R.dimen.default_slider_height));
            this.f20260 = new AlphaSlider(r2);
            this.f20260.setLayoutParams(layoutParams2);
            this.f20261.addView(this.f20260);
            this.f20263.setAlphaSlider(this.f20260);
            this.f20260.setColor(m26224(this.f20255));
        }
        if (this.f20258) {
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
            this.f20251 = (EditText) View.inflate(r2, R.layout.picker_edit, (ViewGroup) null);
            this.f20251.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            this.f20251.setSingleLine();
            this.f20251.setVisibility(8);
            this.f20251.setFilters(new InputFilter[]{new InputFilter.LengthFilter(this.f20257 ? 9 : 7)});
            this.f20261.addView(this.f20251, layoutParams3);
            this.f20251.setText(Utils.m26222(m26224(this.f20255), this.f20257));
            this.f20263.setColorEdit(this.f20251);
        }
        if (this.f20259) {
            this.f20252 = (LinearLayout) View.inflate(r2, R.layout.color_preview, (ViewGroup) null);
            this.f20252.setVisibility(8);
            this.f20261.addView(this.f20252);
            if (this.f20255.length == 0) {
                ((ImageView) View.inflate(r2, R.layout.color_selector, (ViewGroup) null)).setImageDrawable(new ColorDrawable(-1));
            } else {
                int i = 0;
                while (i < this.f20255.length && i < this.f20256 && this.f20255[i] != null) {
                    LinearLayout linearLayout = (LinearLayout) View.inflate(r2, R.layout.color_selector, (ViewGroup) null);
                    ((ImageView) linearLayout.findViewById(R.id.image_preview)).setImageDrawable(new ColorDrawable(this.f20255[i].intValue()));
                    this.f20252.addView(linearLayout);
                    i++;
                }
            }
            this.f20252.setVisibility(0);
            this.f20263.setColorPreview(this.f20252, m26227(this.f20255));
        }
        return this.f20264.m525();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ColorPickerDialogBuilder m26233(int i) {
        this.f20255[0] = Integer.valueOf(i);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ColorPickerDialogBuilder m26234(ColorPickerView.WHEEL_TYPE wheel_type) {
        this.f20263.setRenderer(ColorWheelRendererBuilder.m26239(wheel_type));
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ColorPickerDialogBuilder m26235(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.f20264.m524(charSequence, onClickListener);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ColorPickerDialogBuilder m26236(CharSequence charSequence, final ColorPickerClickListener colorPickerClickListener) {
        this.f20264.m537(charSequence, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ColorPickerDialogBuilder.this.m26228(dialogInterface, colorPickerClickListener);
            }
        });
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ColorPickerDialogBuilder m26237(String str) {
        this.f20264.m536((CharSequence) str);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ColorPickerDialogBuilder m26238(boolean z) {
        this.f20257 = z;
        return this;
    }
}
