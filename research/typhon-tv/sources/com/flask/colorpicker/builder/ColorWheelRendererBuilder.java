package com.flask.colorpicker.builder;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.renderer.ColorWheelRenderer;
import com.flask.colorpicker.renderer.FlowerColorWheelRenderer;
import com.flask.colorpicker.renderer.SimpleColorWheelRenderer;

public class ColorWheelRendererBuilder {
    /* renamed from: 龘  reason: contains not printable characters */
    public static ColorWheelRenderer m26239(ColorPickerView.WHEEL_TYPE wheel_type) {
        switch (wheel_type) {
            case CIRCLE:
                return new SimpleColorWheelRenderer();
            case FLOWER:
                return new FlowerColorWheelRenderer();
            default:
                throw new IllegalArgumentException("wrong WHEEL_TYPE");
        }
    }
}
