package com.flask.colorpicker.builder;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;

public class PaintBuilder {

    public static class PaintHolder {

        /* renamed from: 龘  reason: contains not printable characters */
        private Paint f20268;

        private PaintHolder() {
            this.f20268 = new Paint(1);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Paint m26243() {
            return this.f20268;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PaintHolder m26244(float f) {
            this.f20268.setStrokeWidth(f);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PaintHolder m26245(int i) {
            this.f20268.setColor(i);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PaintHolder m26246(Paint.Style style) {
            this.f20268.setStyle(style);
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PaintHolder m26247(PorterDuff.Mode mode) {
            this.f20268.setXfermode(new PorterDuffXfermode(mode));
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PaintHolder m26248(Shader shader) {
            this.f20268.setShader(shader);
            return this;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static Bitmap m26240(int i) {
        Paint r5 = m26242().m26243();
        Bitmap createBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        int round = Math.round(((float) i) / 2.0f);
        for (int i2 = 0; i2 < 2; i2++) {
            for (int i3 = 0; i3 < 2; i3++) {
                if ((i2 + i3) % 2 == 0) {
                    r5.setColor(-1);
                } else {
                    r5.setColor(-3092272);
                }
                canvas.drawRect((float) (i2 * round), (float) (i3 * round), (float) ((i2 + 1) * round), (float) ((i3 + 1) * round), r5);
            }
        }
        return createBitmap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Shader m26241(int i) {
        return new BitmapShader(m26240(Math.max(8, (i / 2) * 2)), Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static PaintHolder m26242() {
        return new PaintHolder();
    }
}
