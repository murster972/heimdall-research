package com.flask.colorpicker.renderer;

import android.graphics.Color;
import android.graphics.Paint;
import com.flask.colorpicker.ColorCircle;
import com.flask.colorpicker.builder.PaintBuilder;

public class SimpleColorWheelRenderer extends AbsColorWheelRenderer {

    /* renamed from: 麤  reason: contains not printable characters */
    private float[] f20281 = new float[3];

    /* renamed from: 齉  reason: contains not printable characters */
    private Paint f20282 = PaintBuilder.m26242().m26243();

    /* renamed from: 麤  reason: contains not printable characters */
    public void m26259() {
        int size = this.f20269.size();
        int i = 0;
        float width = ((float) this.f20270.f20272.getWidth()) / 2.0f;
        int i2 = this.f20270.f20277;
        float f = this.f20270.f20274;
        for (int i3 = 0; i3 < i2; i3++) {
            float f2 = f * (((float) i3) / ((float) (i2 - 1)));
            float f3 = this.f20270.f20276;
            int r18 = m26251(f2, f3);
            for (int i4 = 0; i4 < r18; i4++) {
                double d = ((6.283185307179586d * ((double) i4)) / ((double) r18)) + ((3.141592653589793d / ((double) r18)) * ((double) ((i3 + 1) % 2)));
                float cos = width + ((float) (((double) f2) * Math.cos(d)));
                float sin = width + ((float) (((double) f2) * Math.sin(d)));
                this.f20281[0] = (float) ((180.0d * d) / 3.141592653589793d);
                this.f20281[1] = f2 / f;
                this.f20281[2] = this.f20270.f20271;
                this.f20282.setColor(Color.HSVToColor(this.f20281));
                this.f20282.setAlpha(m26250());
                this.f20270.f20272.drawCircle(cos, sin, f3 - this.f20270.f20275, this.f20282);
                if (i >= size) {
                    this.f20269.add(new ColorCircle(cos, sin, this.f20281));
                } else {
                    ((ColorCircle) this.f20269.get(i)).m26207(cos, sin, this.f20281);
                }
                i++;
            }
        }
    }
}
