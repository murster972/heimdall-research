package com.flask.colorpicker.renderer;

import com.flask.colorpicker.ColorCircle;
import java.util.List;

public interface ColorWheelRenderer {
    /* renamed from: 靐  reason: contains not printable characters */
    List<ColorCircle> m26254();

    /* renamed from: 麤  reason: contains not printable characters */
    void m26255();

    /* renamed from: 龘  reason: contains not printable characters */
    ColorWheelRenderOption m26256();

    /* renamed from: 龘  reason: contains not printable characters */
    void m26257(ColorWheelRenderOption colorWheelRenderOption);
}
