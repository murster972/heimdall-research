package com.flask.colorpicker.renderer;

import android.graphics.Color;
import android.graphics.Paint;
import com.flask.colorpicker.ColorCircle;
import com.flask.colorpicker.builder.PaintBuilder;

public class FlowerColorWheelRenderer extends AbsColorWheelRenderer {

    /* renamed from: 连任  reason: contains not printable characters */
    private float f20278 = 1.2f;

    /* renamed from: 麤  reason: contains not printable characters */
    private float[] f20279 = new float[3];

    /* renamed from: 齉  reason: contains not printable characters */
    private Paint f20280 = PaintBuilder.m26242().m26243();

    /* renamed from: 麤  reason: contains not printable characters */
    public void m26258() {
        int size = this.f20269.size();
        int i = 0;
        float width = ((float) this.f20270.f20272.getWidth()) / 2.0f;
        int i2 = this.f20270.f20277;
        float f = this.f20270.f20275;
        float f2 = this.f20270.f20274;
        float f3 = this.f20270.f20276;
        int i3 = 0;
        while (i3 < i2) {
            float f4 = f2 * (((float) i3) / ((float) (i2 - 1)));
            float max = Math.max(1.5f + f, (i3 == 0 ? 0.0f : this.f20278 * f3 * ((((float) i3) - (((float) i2) / 2.0f)) / ((float) i2))) + f3);
            int min = Math.min(m26251(f4, max), i2 * 2);
            for (int i4 = 0; i4 < min; i4++) {
                double d = ((6.283185307179586d * ((double) i4)) / ((double) min)) + ((3.141592653589793d / ((double) min)) * ((double) ((i3 + 1) % 2)));
                float cos = width + ((float) (((double) f4) * Math.cos(d)));
                float sin = width + ((float) (((double) f4) * Math.sin(d)));
                this.f20279[0] = (float) ((180.0d * d) / 3.141592653589793d);
                this.f20279[1] = f4 / f2;
                this.f20279[2] = this.f20270.f20271;
                this.f20280.setColor(Color.HSVToColor(this.f20279));
                this.f20280.setAlpha(m26250());
                this.f20270.f20272.drawCircle(cos, sin, max - f, this.f20280);
                if (i >= size) {
                    this.f20269.add(new ColorCircle(cos, sin, this.f20279));
                } else {
                    ((ColorCircle) this.f20269.get(i)).m26207(cos, sin, this.f20279);
                }
                i++;
            }
            i3++;
        }
    }
}
