package com.flask.colorpicker.renderer;

import com.flask.colorpicker.ColorCircle;
import java.util.ArrayList;
import java.util.List;

public abstract class AbsColorWheelRenderer implements ColorWheelRenderer {

    /* renamed from: 靐  reason: contains not printable characters */
    protected List<ColorCircle> f20269 = new ArrayList();

    /* renamed from: 龘  reason: contains not printable characters */
    protected ColorWheelRenderOption f20270;

    /* renamed from: 靐  reason: contains not printable characters */
    public List<ColorCircle> m26249() {
        return this.f20269;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m26250() {
        return Math.round(this.f20270.f20273 * 255.0f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m26251(float f, float f2) {
        return Math.max(1, (int) ((3.063052912151454d / Math.asin((double) (f2 / f))) + 0.5d));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ColorWheelRenderOption m26252() {
        if (this.f20270 == null) {
            this.f20270 = new ColorWheelRenderOption();
        }
        return this.f20270;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m26253(ColorWheelRenderOption colorWheelRenderOption) {
        this.f20270 = colorWheelRenderOption;
        this.f20269.clear();
    }
}
