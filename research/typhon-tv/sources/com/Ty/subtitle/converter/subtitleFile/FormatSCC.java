package com.Ty.subtitle.converter.subtitleFile;

import com.Ty.subtitle.converter.subtitleFile.exception.FatalParsingException;
import com.Ty.subtitle.converter.subtitleFile.model.Caption;
import com.Ty.subtitle.converter.subtitleFile.model.Style;
import com.Ty.subtitle.converter.subtitleFile.model.Time;
import com.Ty.subtitle.converter.subtitleFile.model.TimedTextObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.commons.lang3.StringUtils;

public class FormatSCC implements TimedTextFileFormat {
    /* renamed from: 齉  reason: contains not printable characters */
    private void m7469(TimedTextObject timedTextObject) {
        Style style = new Style("white");
        timedTextObject.f6535.put(style.f6532, style);
        Style style2 = new Style("whiteU", style);
        style2.f6527 = true;
        timedTextObject.f6535.put(style2.f6532, style2);
        Style style3 = new Style("whiteUI", style2);
        style3.f6525 = true;
        timedTextObject.f6535.put(style3.f6532, style3);
        Style style4 = new Style("whiteI", style3);
        style4.f6527 = false;
        timedTextObject.f6535.put(style4.f6532, style4);
        Style style5 = new Style("green");
        style5.f6530 = Style.m7491("name", "green");
        timedTextObject.f6535.put(style5.f6532, style5);
        Style style6 = new Style("greenU", style5);
        style6.f6527 = true;
        timedTextObject.f6535.put(style6.f6532, style6);
        Style style7 = new Style("greenUI", style6);
        style7.f6525 = true;
        timedTextObject.f6535.put(style7.f6532, style7);
        Style style8 = new Style("greenI", style7);
        style8.f6527 = false;
        timedTextObject.f6535.put(style8.f6532, style8);
        Style style9 = new Style("blue");
        style9.f6530 = Style.m7491("name", "blue");
        timedTextObject.f6535.put(style9.f6532, style9);
        Style style10 = new Style("blueU", style9);
        style10.f6527 = true;
        timedTextObject.f6535.put(style10.f6532, style10);
        Style style11 = new Style("blueUI", style10);
        style11.f6525 = true;
        timedTextObject.f6535.put(style11.f6532, style11);
        Style style12 = new Style("blueI", style11);
        style12.f6527 = false;
        timedTextObject.f6535.put(style12.f6532, style12);
        Style style13 = new Style("cyan");
        style13.f6530 = Style.m7491("name", "cyan");
        timedTextObject.f6535.put(style13.f6532, style13);
        Style style14 = new Style("cyanU", style13);
        style14.f6527 = true;
        timedTextObject.f6535.put(style14.f6532, style14);
        Style style15 = new Style("cyanUI", style14);
        style15.f6525 = true;
        timedTextObject.f6535.put(style15.f6532, style15);
        Style style16 = new Style("cyanI", style15);
        style16.f6527 = false;
        timedTextObject.f6535.put(style16.f6532, style16);
        Style style17 = new Style("red");
        style17.f6530 = Style.m7491("name", "red");
        timedTextObject.f6535.put(style17.f6532, style17);
        Style style18 = new Style("redU", style17);
        style18.f6527 = true;
        timedTextObject.f6535.put(style18.f6532, style18);
        Style style19 = new Style("redUI", style18);
        style19.f6525 = true;
        timedTextObject.f6535.put(style19.f6532, style19);
        Style style20 = new Style("redI", style19);
        style20.f6527 = false;
        timedTextObject.f6535.put(style20.f6532, style20);
        Style style21 = new Style("yellow");
        style21.f6530 = Style.m7491("name", "yellow");
        timedTextObject.f6535.put(style21.f6532, style21);
        Style style22 = new Style("yellowU", style21);
        style22.f6527 = true;
        timedTextObject.f6535.put(style22.f6532, style22);
        Style style23 = new Style("yellowUI", style22);
        style23.f6525 = true;
        timedTextObject.f6535.put(style23.f6532, style23);
        Style style24 = new Style("yellowI", style23);
        style24.f6527 = false;
        timedTextObject.f6535.put(style24.f6532, style24);
        Style style25 = new Style("magenta");
        style25.f6530 = Style.m7491("name", "magenta");
        timedTextObject.f6535.put(style25.f6532, style25);
        Style style26 = new Style("magentaU", style25);
        style26.f6527 = true;
        timedTextObject.f6535.put(style26.f6532, style26);
        Style style27 = new Style("magentaUI", style26);
        style27.f6525 = true;
        timedTextObject.f6535.put(style27.f6532, style27);
        Style style28 = new Style("magentaI", style27);
        style28.f6527 = false;
        timedTextObject.f6535.put(style28.f6532, style28);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m7470(byte b) {
        switch (b) {
            case 0:
                return "";
            case 42:
                return "�";
            case 92:
                return "é";
            case 94:
                return "í";
            case 95:
                return "ó";
            case 96:
                return "ú";
            case 123:
                return "ç";
            case 124:
                return "�";
            case 125:
                return "Ñ";
            case 126:
                return "ñ";
            case Byte.MAX_VALUE:
                return "|";
            default:
                return Character.toString((char) b);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m7471(int i) {
        switch (i) {
            case 0:
                return "�";
            case 1:
                return "�";
            case 2:
                return "�";
            case 3:
                return "�";
            case 4:
                return "�";
            case 5:
                return "�";
            case 6:
                return "�";
            case 7:
                return "♪";
            case 8:
                return "�";
            case 9:
                return " ";
            case 10:
                return "�";
            case 11:
                return "�";
            case 12:
                return "�";
            case 13:
                return "�";
            case 14:
                return "�";
            case 15:
                return "�";
            default:
                return "";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m7472(Caption caption) {
        String[] split = caption.f6517.split("<br />");
        if (split[0].length() > 32) {
            split[0] = split[0].substring(0, 32);
        }
        String str = "" + "1340 1340 ";
        if (((32 - split[0].length()) / 2) % 4 != 0) {
        }
        String str2 = str + m7473(split[0].toCharArray());
        if (split.length <= 1) {
            return str2;
        }
        int i = 0 + 1;
        if (split[i].length() > 32) {
            split[i] = split[i].substring(0, 32);
        }
        String str3 = str2 + "13e0 13e0 ";
        if (((32 - split[i].length()) / 2) % 4 != 0) {
        }
        String str4 = str3 + m7473(split[i].toCharArray());
        if (split.length <= 2) {
            return str4;
        }
        int i2 = i + 1;
        if (split[i2].length() > 32) {
            split[i2] = split[i2].substring(0, 32);
        }
        String str5 = str4 + "9440 9440 ";
        if (((32 - split[i2].length()) / 2) % 4 != 0) {
        }
        String str6 = str5 + m7473(split[i2].toCharArray());
        if (split.length <= 3) {
            return str6;
        }
        int i3 = i2 + 1;
        if (split[i3].length() > 32) {
            split[i3] = split[i3].substring(0, 32);
        }
        String str7 = str6 + "94e0 94e0 ";
        if (((32 - split[i3].length()) / 2) % 4 != 0) {
        }
        return str7 + m7473(split[i3].toCharArray());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m7473(char[] cArr) {
        String str = "";
        int i = 0;
        while (i < cArr.length) {
            switch (cArr[i]) {
                case ' ':
                    str = str + "20";
                    break;
                case '!':
                    str = str + "a1";
                    break;
                case '\"':
                    str = str + "a2";
                    break;
                case '#':
                    str = str + "23";
                    break;
                case '$':
                    str = str + "a4";
                    break;
                case '%':
                    str = str + "25";
                    break;
                case '&':
                    str = str + "26";
                    break;
                case '\'':
                    str = str + "a7";
                    break;
                case '(':
                    str = str + "a8";
                    break;
                case ')':
                    str = str + "29";
                    break;
                case '+':
                    str = str + "ab";
                    break;
                case ',':
                    str = str + "2c";
                    break;
                case '-':
                    str = str + "ad";
                    break;
                case '.':
                    str = str + "ae";
                    break;
                case '/':
                    str = str + "2f";
                    break;
                case '0':
                    str = str + "b0";
                    break;
                case '1':
                    str = str + "31";
                    break;
                case '2':
                    str = str + "32";
                    break;
                case '3':
                    str = str + "b3";
                    break;
                case '4':
                    str = str + "34";
                    break;
                case '5':
                    str = str + "b5";
                    break;
                case '6':
                    str = str + "b6";
                    break;
                case '7':
                    str = str + "37";
                    break;
                case '8':
                    str = str + "38";
                    break;
                case '9':
                    str = str + "b9";
                    break;
                case ':':
                    str = str + "ba";
                    break;
                case ';':
                    str = str + "3b";
                    break;
                case '<':
                    str = str + "bc";
                    break;
                case '=':
                    str = str + "3d";
                    break;
                case '>':
                    str = str + "3e";
                    break;
                case '?':
                    str = str + "bf";
                    break;
                case '@':
                    str = str + "40";
                    break;
                case 'A':
                    str = str + "c1";
                    break;
                case 'B':
                    str = str + "c2";
                    break;
                case 'C':
                    str = str + "43";
                    break;
                case 'D':
                    str = str + "c4";
                    break;
                case 'E':
                    str = str + "45";
                    break;
                case 'F':
                    str = str + "46";
                    break;
                case 'G':
                    str = str + "c7";
                    break;
                case 'H':
                    str = str + "c8";
                    break;
                case 'I':
                    str = str + "49";
                    break;
                case 'J':
                    str = str + "4a";
                    break;
                case 'K':
                    str = str + "cb";
                    break;
                case 'L':
                    str = str + "4c";
                    break;
                case 'M':
                    str = str + "cd";
                    break;
                case 'N':
                    str = str + "ce";
                    break;
                case 'O':
                    str = str + "4f";
                    break;
                case 'P':
                    str = str + "d0";
                    break;
                case 'Q':
                    str = str + "51";
                    break;
                case 'R':
                    str = str + "52";
                    break;
                case 'S':
                    str = str + "d3";
                    break;
                case 'T':
                    str = str + "54";
                    break;
                case 'U':
                    str = str + "d5";
                    break;
                case 'V':
                    str = str + "d6";
                    break;
                case 'W':
                    str = str + "57";
                    break;
                case 'X':
                    str = str + "58";
                    break;
                case 'Y':
                    str = str + "d9";
                    break;
                case 'Z':
                    str = str + "da";
                    break;
                case '[':
                    str = str + "5b";
                    break;
                case ']':
                    str = str + "5d";
                    break;
                case 'a':
                    str = str + "61";
                    break;
                case 'b':
                    str = str + "62";
                    break;
                case 'c':
                    str = str + "e3";
                    break;
                case 'd':
                    str = str + "64";
                    break;
                case 'e':
                    str = str + "e5";
                    break;
                case 'f':
                    str = str + "e6";
                    break;
                case 'g':
                    str = str + "67";
                    break;
                case 'h':
                    str = str + "68";
                    break;
                case 'i':
                    str = str + "e9";
                    break;
                case 'j':
                    str = str + "ea";
                    break;
                case 'k':
                    str = str + "6b";
                    break;
                case 'l':
                    str = str + "ec";
                    break;
                case 'm':
                    str = str + "6d";
                    break;
                case 'n':
                    str = str + "6e";
                    break;
                case 'o':
                    str = str + "ef";
                    break;
                case 'p':
                    str = str + "70";
                    break;
                case 'q':
                    str = str + "f1";
                    break;
                case 'r':
                    str = str + "f2";
                    break;
                case 's':
                    str = str + "73";
                    break;
                case 't':
                    str = str + "f4";
                    break;
                case 'u':
                    str = str + "75";
                    break;
                case 'v':
                    str = str + "76";
                    break;
                case 'w':
                    str = str + "f7";
                    break;
                case 'x':
                    str = str + "f8";
                    break;
                case 'y':
                    str = str + "79";
                    break;
                case 'z':
                    str = str + "7a";
                    break;
                case '|':
                    str = str + "7f";
                    break;
                case 209:
                    str = str + "fd";
                    break;
                case 225:
                    str = str + "2a";
                    break;
                case 231:
                    str = str + "fb";
                    break;
                case 233:
                    str = str + "dc";
                    break;
                case 237:
                    str = str + "5e";
                    break;
                case 241:
                    str = str + "fe";
                    break;
                case 243:
                    str = str + "df";
                    break;
                case 247:
                    str = str + "7c";
                    break;
                case 250:
                    str = str + "e0";
                    break;
                default:
                    str = str + "7f";
                    break;
            }
            if (i % 2 == 1) {
                str = str + StringUtils.SPACE;
            }
            i++;
        }
        return i % 2 == 1 ? str + "80 " : str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7474(String str, int i) {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public TimedTextObject m7476(String str, InputStream inputStream, String str2) throws IOException, FatalParsingException {
        InputStreamReader inputStreamReader;
        Caption caption;
        TimedTextObject timedTextObject = new TimedTextObject();
        Caption caption2 = null;
        String str3 = "";
        boolean z = false;
        boolean z2 = true;
        boolean z3 = false;
        boolean z4 = false;
        String str4 = null;
        try {
            inputStreamReader = new InputStreamReader(inputStream, str2);
        } catch (UnsupportedEncodingException e) {
            inputStreamReader = new InputStreamReader(inputStream);
        }
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        timedTextObject.f6542 = str;
        timedTextObject.f6546 = str;
        int i = 0 + 1;
        try {
            if (!bufferedReader.readLine().trim().equalsIgnoreCase("Scenarist_SCC V1.0")) {
                throw new FatalParsingException("The fist line should define the file type: \"Scenarist_SCC V1.0\"");
            }
            m7469(timedTextObject);
            timedTextObject.f6540 += "Only data from CC channel 1 will be extracted.\n\n";
            for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                String trim = readLine.trim();
                i++;
                if (!trim.isEmpty()) {
                    String[] split = trim.split("\t");
                    Time time = new Time("h:m:s:f/fps", split[0] + "/29.97");
                    String[] split2 = split[1].split(StringUtils.SPACE);
                    int i2 = 0;
                    Caption caption3 = caption2;
                    while (true) {
                        try {
                            if (i2 < split2.length) {
                                int parseInt = Integer.parseInt(split2[i2], 16) & 32639;
                                if ((parseInt & 24576) != 0) {
                                    if (z) {
                                        byte b = (byte) ((65280 & parseInt) >>> 8);
                                        byte b2 = (byte) (parseInt & 255);
                                        if (z2) {
                                            str3 = (str3 + m7470(b)) + m7470(b2);
                                        } else {
                                            caption3.f6517 += m7470(b);
                                            caption3.f6517 += m7470(b2);
                                        }
                                        caption = caption3;
                                    }
                                    caption = caption3;
                                } else if (parseInt == 0) {
                                    time.f6533 = (int) (((double) time.f6533) + 33.366700033366705d);
                                    caption = caption3;
                                } else {
                                    if (i2 + 1 < split2.length && split2[i2].equals(split2[i2 + 1])) {
                                        i2++;
                                    }
                                    if ((parseInt & 2048) != 0) {
                                        z = false;
                                        caption = caption3;
                                    } else if ((parseInt & 5744) != 5152) {
                                        if (z) {
                                            if ((parseInt & 4160) == 4160) {
                                                str4 = "white";
                                                z3 = false;
                                                z4 = false;
                                                if (z2) {
                                                    if (!str3.isEmpty()) {
                                                        str3 = str3 + "<br />";
                                                    }
                                                }
                                                if (!z2 && !caption3.f6517.isEmpty()) {
                                                    caption3.f6517 += "<br />";
                                                }
                                                if ((parseInt & 1) == 1) {
                                                    z3 = true;
                                                }
                                                if ((parseInt & 16) != 16) {
                                                    switch ((short) ((parseInt & 14) >> 1)) {
                                                        case 0:
                                                            str4 = "white";
                                                            caption = caption3;
                                                            break;
                                                        case 1:
                                                            str4 = "green";
                                                            caption = caption3;
                                                            break;
                                                        case 2:
                                                            str4 = "blue";
                                                            caption = caption3;
                                                            break;
                                                        case 3:
                                                            str4 = "cyan";
                                                            caption = caption3;
                                                            break;
                                                        case 4:
                                                            str4 = "red";
                                                            caption = caption3;
                                                            break;
                                                        case 5:
                                                            str4 = "yellow";
                                                            caption = caption3;
                                                            break;
                                                        case 6:
                                                            str4 = "magenta";
                                                            caption = caption3;
                                                            break;
                                                        case 7:
                                                            z4 = true;
                                                            caption = caption3;
                                                            break;
                                                        default:
                                                            caption = caption3;
                                                            break;
                                                    }
                                                } else {
                                                    str4 = "white";
                                                    caption = caption3;
                                                }
                                            } else if ((parseInt & 6000) == 4384) {
                                                z3 = (parseInt & 1) == 1;
                                                switch ((short) ((parseInt & 14) >> 1)) {
                                                    case 0:
                                                        str4 = "white";
                                                        z4 = false;
                                                        caption = caption3;
                                                        break;
                                                    case 1:
                                                        str4 = "green";
                                                        z4 = false;
                                                        caption = caption3;
                                                        break;
                                                    case 2:
                                                        str4 = "blue";
                                                        z4 = false;
                                                        caption = caption3;
                                                        break;
                                                    case 3:
                                                        str4 = "cyan";
                                                        z4 = false;
                                                        caption = caption3;
                                                        break;
                                                    case 4:
                                                        str4 = "red";
                                                        z4 = false;
                                                        caption = caption3;
                                                        break;
                                                    case 5:
                                                        str4 = "yellow";
                                                        z4 = false;
                                                        caption = caption3;
                                                        break;
                                                    case 6:
                                                        str4 = "magenta";
                                                        z4 = false;
                                                        caption = caption3;
                                                        break;
                                                    case 7:
                                                        z4 = true;
                                                        caption = caption3;
                                                        break;
                                                    default:
                                                        caption = caption3;
                                                        break;
                                                }
                                            } else if ((parseInt & 6012) == 5920) {
                                                caption = caption3;
                                            } else if ((parseInt & 6000) == 4400) {
                                                int i3 = parseInt & 15;
                                                if (z2) {
                                                    str3 = str3 + m7471(i3);
                                                    caption = caption3;
                                                } else {
                                                    caption3.f6517 += m7471(i3);
                                                    caption = caption3;
                                                }
                                            } else if ((parseInt & 5728) == 4640) {
                                                int i4 = parseInt & 287;
                                                if (z2) {
                                                    m7474(str3, i4);
                                                    caption = caption3;
                                                } else {
                                                    m7474(caption3.f6517, i4);
                                                    caption = caption3;
                                                }
                                            }
                                        }
                                        caption = caption3;
                                    } else if ((parseInt & 256) == 0) {
                                        z = true;
                                        switch (parseInt & 15) {
                                            case 0:
                                                z2 = true;
                                                str3 = "";
                                                caption = caption3;
                                                break;
                                            case 5:
                                            case 6:
                                            case 7:
                                                str3 = "";
                                                if (caption3 != null) {
                                                    caption3.f6520 = time;
                                                    String str5 = "" + str4;
                                                    if (z3) {
                                                        str5 = str5 + "U";
                                                    }
                                                    if (z4) {
                                                        str5 = str5 + "I";
                                                    }
                                                    caption3.f6522 = timedTextObject.f6535.get(str5);
                                                    timedTextObject.f6539.put(Integer.valueOf(caption3.f6521.f6533), caption3);
                                                }
                                                caption = new Caption();
                                                caption.f6521 = time;
                                                z2 = false;
                                                break;
                                            case 9:
                                                z2 = false;
                                                caption = new Caption();
                                                caption.f6521 = time;
                                                break;
                                            case 12:
                                                if (caption3 != null) {
                                                    caption3.f6520 = time;
                                                    if (caption3.f6521 != null) {
                                                        for (int i5 = caption3.f6521.f6533; timedTextObject.f6539.containsKey(Integer.valueOf(i5)); i5++) {
                                                        }
                                                        timedTextObject.f6539.put(Integer.valueOf(caption3.f6521.f6533), caption3);
                                                        caption = new Caption();
                                                        break;
                                                    }
                                                }
                                                break;
                                            case 14:
                                                str3 = "";
                                                caption = caption3;
                                                break;
                                            case 15:
                                                caption = new Caption();
                                                caption.f6521 = time;
                                                caption.f6517 += str3;
                                                break;
                                            default:
                                                caption = caption3;
                                                break;
                                        }
                                        caption = caption3;
                                    } else {
                                        z = false;
                                        caption = caption3;
                                    }
                                }
                                i2++;
                                caption3 = caption;
                            } else {
                                caption2 = caption3;
                            }
                        } catch (NullPointerException e2) {
                            Caption caption4 = caption3;
                            try {
                                timedTextObject.f6540 += "unexpected end of file at line " + i + ", maybe last caption is not complete.\n\n";
                                bufferedReader.close();
                                timedTextObject.f6537 = true;
                                return timedTextObject;
                            } catch (Throwable th) {
                                th = th;
                                bufferedReader.close();
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            Caption caption5 = caption3;
                            bufferedReader.close();
                            throw th;
                        }
                    }
                }
            }
            if (caption2 != null) {
                caption2.f6520 = new Time("h:m:s:f/fps", "99:59:59:29/29.97");
            }
            if (caption2.f6521 != null) {
                for (int i6 = caption2.f6521.f6533; timedTextObject.f6539.containsKey(Integer.valueOf(i6)); i6++) {
                }
                timedTextObject.f6539.put(Integer.valueOf(caption2.f6521.f6533), caption2);
            }
            timedTextObject.m7494();
            bufferedReader.close();
            timedTextObject.f6537 = true;
            return timedTextObject;
        } catch (NullPointerException e3) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String[] m7475(TimedTextObject timedTextObject) {
        int i;
        String str;
        if (!timedTextObject.f6537) {
            return null;
        }
        ArrayList arrayList = new ArrayList((timedTextObject.f6539.size() * 2) + 20);
        int i2 = 0 + 1;
        arrayList.add(0, "Scenarist_SCC V1.0\n");
        Caption caption = new Caption();
        caption.f6517 = "";
        caption.f6520 = new Time("h:mm:ss.cs", "0:00:00.00");
        Iterator<Caption> it2 = timedTextObject.f6539.values().iterator();
        while (true) {
            i = i2;
            if (!it2.hasNext()) {
                break;
            }
            Caption caption2 = caption;
            caption = it2.next();
            if (caption2.f6520.f6533 > caption.f6521.f6533) {
                caption.f6517 += "<br />" + caption2.f6517;
                Time time = caption.f6521;
                time.f6533 = (int) (((double) time.f6533) - 33.366700033366705d);
                Time time2 = caption.f6521;
                time2.f6533 = (int) (((double) time2.f6533) + 33.366700033366705d);
                str = ("" + caption.f6521.m7492("hh:mm:ss:ff/29.97") + "\t942c 942c ") + "94ae 94ae 9420 9420 ";
            } else if (caption2.f6520.f6533 < caption.f6521.f6533) {
                Time time3 = caption.f6521;
                time3.f6533 = (int) (((double) time3.f6533) - 33.366700033366705d);
                str = ("" + caption2.f6520.m7492("hh:mm:ss:ff/29.97") + "\t942c 942c\n\n") + caption.f6521.m7492("hh:mm:ss:ff/29.97") + "\t94ae 94ae 9420 9420 ";
                Time time4 = caption.f6521;
                time4.f6533 = (int) (((double) time4.f6533) + 33.366700033366705d);
            } else {
                Time time5 = caption.f6521;
                time5.f6533 = (int) (((double) time5.f6533) - 33.366700033366705d);
                str = "" + caption.f6521.m7492("hh:mm:ss:ff/29.97") + "\t942c 942c 94ae 94ae 9420 9420 ";
                Time time6 = caption.f6521;
                time6.f6533 = (int) (((double) time6.f6533) + 33.366700033366705d);
            }
            i2 = i + 1;
            arrayList.add(i, (str + m7472(caption)) + "8080 8080 942f 942f\n");
        }
        int i3 = i + 1;
        arrayList.add(i, "");
        String[] strArr = new String[arrayList.size()];
        for (int i4 = 0; i4 < strArr.length; i4++) {
            strArr[i4] = (String) arrayList.get(i4);
        }
        return strArr;
    }
}
